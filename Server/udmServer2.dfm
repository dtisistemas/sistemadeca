object dbDecaServer2: TdbDecaServer2
  Tag = 1
  OldCreateOrder = False
  Left = 322
  Top = 181
  Height = 640
  Width = 870
  object adoConn2: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=Deca;Data Source=SRVBACKUP;Use Proc' +
      'edure for Prepare=1;Auto Translate=True;Packet Size=4096;Worksta' +
      'tion ID=FABIO;Use Encryption for Data=False;Tag with column coll' +
      'ation when possible=False'
    DefaultDatabase = 'Deca'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 24
    Top = 16
  end
  object spSel_SerieEscolar: TADOStoredProc
    Connection = adoConn2
    ProcedureName = 'SP_Sel_Serie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 72
  end
  object dspSel_SerieEscolar: TDataSetProvider
    DataSet = spSel_SerieEscolar
    Constraints = True
    Left = 88
    Top = 72
  end
  object spInc_SerieEscolar: TADOStoredProc
    Connection = adoConn2
    ProcedureName = 'SP_Inc_Serie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 120
  end
  object dspInc_SerieEscolar: TDataSetProvider
    DataSet = spInc_SerieEscolar
    Constraints = True
    Left = 88
    Top = 120
  end
end
