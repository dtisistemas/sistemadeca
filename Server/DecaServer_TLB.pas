unit DecaServer_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 03/10/2017 15:07:47 from Type Library described below.

// ************************************************************************ //
// Type Lib: E:\Deca\Deca v1.0\Server\DecaServer.tlb (1)
// IID\LCID: {90CDB93C-8708-4A1E-A771-30AA42B5D2F0}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (c:\program files (x86)\embarcadero\studio\19.0\bin\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\Windows\SysWow64\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DecaServerMajorVersion = 1;
  DecaServerMinorVersion = 0;

  LIBID_DecaServer: TGUID = '{90CDB93C-8708-4A1E-A771-30AA42B5D2F0}';

  IID_IdbDecaServer: TGUID = '{E9A8532E-FD23-4F53-BE09-DC09AE5DFD1D}';
  CLASS_dbDecaServer: TGUID = '{B639710A-7455-4622-B853-8DF37B43912D}';
  IID_IdbDecaServer2: TGUID = '{4E539F45-7497-4290-98BB-E5E90DA864F7}';
  CLASS_dbDecaServer2: TGUID = '{FB94BCE5-F254-4FBD-B679-C727EECAA6CA}';
  IID_IdbDeca: TGUID = '{EE347D33-3C0E-4764-A544-1912A737753A}';
  CLASS_dbDeca: TGUID = '{8422685A-B0FA-42F9-A798-760EBA3F7ADB}';
  IID_IdbTriagemDeca: TGUID = '{7B0F7B3E-C1EC-435E-9CFB-21F76BD5AEEC}';
  CLASS_dbTriagemDeca: TGUID = '{A6ED21FD-41FF-49C8-BEA5-B677F1ACF8A4}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdbDecaServer = interface;
  IdbDecaServerDisp = dispinterface;
  IdbDecaServer2 = interface;
  IdbDecaServer2Disp = dispinterface;
  IdbDeca = interface;
  IdbDecaDisp = dispinterface;
  IdbTriagemDeca = interface;
  IdbTriagemDecaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dbDecaServer = IdbDecaServer;
  dbDecaServer2 = IdbDecaServer2;
  dbDeca = IdbDeca;
  dbTriagemDeca = IdbTriagemDeca;


// *********************************************************************//
// Interface: IdbDecaServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E9A8532E-FD23-4F53-BE09-DC09AE5DFD1D}
// *********************************************************************//
  IdbDecaServer = interface(IAppServer)
    ['{E9A8532E-FD23-4F53-BE09-DC09AE5DFD1D}']
  end;

// *********************************************************************//
// DispIntf:  IdbDecaServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E9A8532E-FD23-4F53-BE09-DC09AE5DFD1D}
// *********************************************************************//
  IdbDecaServerDisp = dispinterface
    ['{E9A8532E-FD23-4F53-BE09-DC09AE5DFD1D}']
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT; 
                              out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT; 
                            Options: SYSINT; const CommandText: WideString; var Params: OleVariant; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IdbDecaServer2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4E539F45-7497-4290-98BB-E5E90DA864F7}
// *********************************************************************//
  IdbDecaServer2 = interface(IAppServer)
    ['{4E539F45-7497-4290-98BB-E5E90DA864F7}']
  end;

// *********************************************************************//
// DispIntf:  IdbDecaServer2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4E539F45-7497-4290-98BB-E5E90DA864F7}
// *********************************************************************//
  IdbDecaServer2Disp = dispinterface
    ['{4E539F45-7497-4290-98BB-E5E90DA864F7}']
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT; 
                              out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT; 
                            Options: SYSINT; const CommandText: WideString; var Params: OleVariant; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IdbDeca
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EE347D33-3C0E-4764-A544-1912A737753A}
// *********************************************************************//
  IdbDeca = interface(IAppServer)
    ['{EE347D33-3C0E-4764-A544-1912A737753A}']
  end;

// *********************************************************************//
// DispIntf:  IdbDecaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EE347D33-3C0E-4764-A544-1912A737753A}
// *********************************************************************//
  IdbDecaDisp = dispinterface
    ['{EE347D33-3C0E-4764-A544-1912A737753A}']
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT; 
                              out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT; 
                            Options: SYSINT; const CommandText: WideString; var Params: OleVariant; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IdbTriagemDeca
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7B0F7B3E-C1EC-435E-9CFB-21F76BD5AEEC}
// *********************************************************************//
  IdbTriagemDeca = interface(IAppServer)
    ['{7B0F7B3E-C1EC-435E-9CFB-21F76BD5AEEC}']
  end;

// *********************************************************************//
// DispIntf:  IdbTriagemDecaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7B0F7B3E-C1EC-435E-9CFB-21F76BD5AEEC}
// *********************************************************************//
  IdbTriagemDecaDisp = dispinterface
    ['{7B0F7B3E-C1EC-435E-9CFB-21F76BD5AEEC}']
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT; 
                              out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT; 
                            Options: SYSINT; const CommandText: WideString; var Params: OleVariant; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodbDecaServer provides a Create and CreateRemote method to          
// create instances of the default interface IdbDecaServer exposed by              
// the CoClass dbDecaServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodbDecaServer = class
    class function Create: IdbDecaServer;
    class function CreateRemote(const MachineName: string): IdbDecaServer;
  end;

// *********************************************************************//
// The Class CodbDecaServer2 provides a Create and CreateRemote method to          
// create instances of the default interface IdbDecaServer2 exposed by              
// the CoClass dbDecaServer2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodbDecaServer2 = class
    class function Create: IdbDecaServer2;
    class function CreateRemote(const MachineName: string): IdbDecaServer2;
  end;

// *********************************************************************//
// The Class CodbDeca provides a Create and CreateRemote method to          
// create instances of the default interface IdbDeca exposed by              
// the CoClass dbDeca. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodbDeca = class
    class function Create: IdbDeca;
    class function CreateRemote(const MachineName: string): IdbDeca;
  end;

// *********************************************************************//
// The Class CodbTriagemDeca provides a Create and CreateRemote method to          
// create instances of the default interface IdbTriagemDeca exposed by              
// the CoClass dbTriagemDeca. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodbTriagemDeca = class
    class function Create: IdbTriagemDeca;
    class function CreateRemote(const MachineName: string): IdbTriagemDeca;
  end;

implementation

uses ComObj;

class function CodbDecaServer.Create: IdbDecaServer;
begin
  Result := CreateComObject(CLASS_dbDecaServer) as IdbDecaServer;
end;

class function CodbDecaServer.CreateRemote(const MachineName: string): IdbDecaServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dbDecaServer) as IdbDecaServer;
end;

class function CodbDecaServer2.Create: IdbDecaServer2;
begin
  Result := CreateComObject(CLASS_dbDecaServer2) as IdbDecaServer2;
end;

class function CodbDecaServer2.CreateRemote(const MachineName: string): IdbDecaServer2;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dbDecaServer2) as IdbDecaServer2;
end;

class function CodbDeca.Create: IdbDeca;
begin
  Result := CreateComObject(CLASS_dbDeca) as IdbDeca;
end;

class function CodbDeca.CreateRemote(const MachineName: string): IdbDeca;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dbDeca) as IdbDeca;
end;

class function CodbTriagemDeca.Create: IdbTriagemDeca;
begin
  Result := CreateComObject(CLASS_dbTriagemDeca) as IdbTriagemDeca;
end;

class function CodbTriagemDeca.CreateRemote(const MachineName: string): IdbTriagemDeca;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dbTriagemDeca) as IdbTriagemDeca;
end;

end.
