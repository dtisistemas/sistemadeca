object dbTriagemDeca: TdbTriagemDeca
  OldCreateOrder = False
  Left = 301
  Top = 105
  Height = 890
  Width = 1461
  object adoConnTriagem: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=TRIAGEM_TST;Data Source=192.168.0.2' +
      '0;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=40' +
      '96;Workstation ID=DTI04;Use Encryption for Data=False;Tag with c' +
      'olumn collation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 89
    Top = 24
  end
  object spSel_Par_EstadoCivil: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_estadoCivil;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_estadocivil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 88
  end
  object dspSel_Par_EstadoCivil: TDataSetProvider
    DataSet = spSel_Par_EstadoCivil
    Constraints = True
    Left = 144
    Top = 88
  end
  object spSel_Par_Cidades: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_cadastro_cidades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_cidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 144
  end
  object dspSel_Par_Cidades: TDataSetProvider
    DataSet = spSel_Par_Cidades
    Constraints = True
    Left = 144
    Top = 143
  end
  object spSel_Par_Nacionalidade: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_PAR_CADASTRO_NACIONALIDADE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_nacionalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 200
  end
  object dspSel_Par_Nacionalidade: TDataSetProvider
    DataSet = spSel_Par_Nacionalidade
    Constraints = True
    Left = 144
    Top = 199
  end
  object spSel_Cadastro_Bairro: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_cad_bairros;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_cod_unidade_referencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 256
  end
  object dspSel_Cadastro_Bairro: TDataSetProvider
    DataSet = spSel_Cadastro_Bairro
    Constraints = True
    Left = 144
    Top = 255
  end
  object spSel_Cadastro_Emissor: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_CADASTRO_EMISSOR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_EMISSOR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_IND_STATUS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 312
  end
  object dspSel_Cadastro_Emissor: TDataSetProvider
    DataSet = spSel_Cadastro_Emissor
    Constraints = True
    Left = 144
    Top = 311
  end
  object spSel_Par_Posicao: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_posicao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_posicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_ativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 87
    Top = 375
  end
  object dspSel_Par_Posicao: TDataSetProvider
    DataSet = spSel_Par_Posicao
    Constraints = True
    Left = 144
    Top = 375
  end
  object spSel_Pesquisa_Inscritos_Triagem: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_PESQUISA_INSCRITOS_TRIAGEM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_nom_familiar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao_digt'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo_consulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 87
    Top = 439
  end
  object dspSel_Pesquisa_Inscritos_Triagem: TDataSetProvider
    DataSet = spSel_Pesquisa_Inscritos_Triagem
    Constraints = True
    Left = 144
    Top = 439
  end
  object spSel_Par_RelResponsabilidade: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_par_RelResponsabilidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_relResponsabilidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 504
  end
  object dspSel_Par_RelResponsabilidade: TDataSetProvider
    DataSet = spSel_Par_RelResponsabilidade
    Constraints = True
    Left = 144
    Top = 503
  end
  object spSel_Par_Natureza: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_natureza;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_natureza_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 568
  end
  object dspSel_Par_Natureza: TDataSetProvider
    DataSet = spSel_Par_Natureza
    Constraints = True
    Left = 144
    Top = 567
  end
  object spSel_Par_Tipo_Habitacao: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_tipo_hab;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_tipo_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 88
  end
  object dspSel_Par_Tipo_Habitacao: TDataSetProvider
    DataSet = spSel_Par_Tipo_Habitacao
    Constraints = True
    Left = 338
    Top = 88
  end
  object spSel_Par_Infraestrutura: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_infra_estrutura;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_infra_estrutura'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 143
  end
  object dspSel_Par_Infrestrutura: TDataSetProvider
    DataSet = spSel_Par_Infraestrutura
    Constraints = True
    Left = 338
    Top = 143
  end
  object spSel_Par_InstalacaoesSanitarias: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_inst_sanitaria;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_inst_sanitaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 197
  end
  object dspSel_Par_InstalacoesSanitarias: TDataSetProvider
    DataSet = spSel_Par_InstalacaoesSanitarias
    Constraints = True
    Left = 338
    Top = 198
  end
  object spSel_Par_CondicaoHabitabilidade: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_condicao_habitabilidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_cond_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 254
  end
  object dspSel_Par_CondicaoHabitabilidade: TDataSetProvider
    DataSet = spSel_Par_CondicaoHabitabilidade
    Constraints = True
    Left = 338
    Top = 254
  end
  object spSel_Par_Escolaridade: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_escolaridade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_escolaridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 310
  end
  object dspSel_Par_Escolaridade: TDataSetProvider
    DataSet = spSel_Par_Escolaridade
    Constraints = True
    Left = 338
    Top = 309
  end
  object spIns_Cadastro_TriagemDeca: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_INS_CADASTRO_TRIAGEMDECA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao_digt'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_moto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprovadespmedic'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_carro'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_outroimovel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_drogadicto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_alcoolatra'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_paimaeadolescente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_gestante'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_medicamentopsico'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_prostituicao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_traficodrogas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_convivedrogadiccao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_convivealcool'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conviveportdoencamental'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conviveportdoencainfecto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_membroautorinfracao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_aquarela'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_criancasozinha'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprovoualuguel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dta_renovacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dta_inscricao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_qtdehabitantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_TempoResidencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_NumResidencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_RelResp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_motoano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_usoveic'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_qtdecomodos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_tipo_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inst_sanitaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_infra_estrutura'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_cond_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_natureza_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_carroano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_portaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_motivo_nestuda'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_encaminhamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@pe_cod_estadocivil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_bairro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_religiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_NumCertNasc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_RG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_NomeEscola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_cidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_Complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_fone_contato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_num_pontuacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_recado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_parentesco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_despmedicas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_valoraluguel'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_grau_parentesc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_SeMatric'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaSerie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_carromodel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_motomodel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaPeriodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_ind_TELPROPRIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_TempoResid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_idadeXserie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_razaoincomp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_ResideFam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_MotivoProcuraCompl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@pe_dsc_NResideFam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_PontoRef'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_telRecado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_CPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_cod_sias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_cod_UsuarioCadastro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_DiagSaude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_IncapazTrab'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_DoencaInfecto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_InvalTempDef'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_dtacadastro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dta_atualizaEscolar'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_inscritopor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_matricula'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_readmissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade_referencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_sus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@pe_num_cra'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_passe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_emissao_rg'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_ra'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_ind_invalidez_temporaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_invalidez_permanente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_trabalho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_cursos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_nao_ficar_sozinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_tirar_rua'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_creas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_cras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_vij'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_as'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_ct'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_outros'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_veiculo_outras_priori'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_veiculo_trabalho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_veiculo_unico_transp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_medidas_socioeducativas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_fisica'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_psicologica'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_sexual'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_negligencia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_abandono'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conv_port_doencas_incap'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_nacionalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_emissor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 374
  end
  object dspIns_Cadastro_TriagemDeca: TDataSetProvider
    DataSet = spIns_Cadastro_TriagemDeca
    Constraints = True
    Left = 338
    Top = 374
  end
  object spIns_Cad_Inscricao: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_INS_CAD_INSCRICAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_digito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 438
  end
  object spSel_Retorna_NIF_Valido: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_RETORNA_NIF_VALIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_digito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 502
  end
  object dspIns_Cad_Inscricao: TDataSetProvider
    DataSet = spIns_Cad_Inscricao
    Constraints = True
    Left = 338
    Top = 438
  end
  object dspSel_Retorna_NIF_Valido: TDataSetProvider
    DataSet = spSel_Retorna_NIF_Valido
    Constraints = True
    Left = 338
    Top = 502
  end
  object spDel_Cad_Inscricao: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_DEL_CAD_INSCRICAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_digito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 566
  end
  object dspDel_Cad_Inscricao: TDataSetProvider
    DataSet = spDel_Cad_Inscricao
    Constraints = True
    Left = 338
    Top = 566
  end
  object spSel_Par_Cadastro_Regioes: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_Cadastro_regioes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 88
  end
  object dspSel_Par_Cadastro_Regioes: TDataSetProvider
    DataSet = spSel_Par_Cadastro_Regioes
    Constraints = True
    Left = 538
    Top = 88
  end
  object spSel_Cadastro_Full: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_CADASTRO_FULL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao_digt'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 142
  end
  object dspSel_Cadastro_Full: TDataSetProvider
    DataSet = spSel_Cadastro_Full
    Constraints = True
    Left = 538
    Top = 141
  end
  object spIns_Par_Cad_Bairro: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_INS_PAR_CAD_BAIRRO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_regiao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade_referencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 198
  end
  object dspIns_Par_Cad_Bairro: TDataSetProvider
    DataSet = spIns_Par_Cad_Bairro
    Constraints = True
    Left = 538
    Top = 197
  end
  object spUpd_Par_Cad_Bairro: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_UPD_PAR_CAD_BAIRRO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_bairro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_regiao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade_referencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 254
  end
  object dspUpd_Par_Cad_Bairro: TDataSetProvider
    DataSet = spUpd_Par_Cad_Bairro
    Constraints = True
    Left = 538
    Top = 253
  end
  object spUpd_Cadastro_TriagemDeca: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_UPD_CADASTRO_TRIAGEMDECA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao_digt'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_moto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprovadespmedic'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_carro'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_outroimovel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_drogadicto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_alcoolatra'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_paimaeadolescente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_gestante'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_medicamentopsico'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_prostituicao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_traficodrogas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_convivedrogadiccao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_convivealcool'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conviveportdoencamental'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conviveportdoencainfecto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_membroautorinfracao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_aquarela'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_criancasozinha'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprovoualuguel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dta_renovacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dta_inscricao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_qtdehabitantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_TempoResidencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_NumResidencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_RelResp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_motoano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_usoveic'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_qtdecomodos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_tipo_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inst_sanitaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_infra_estrutura'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_cond_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_natureza_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_carroano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_portaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_motivo_nestuda'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_encaminhamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@pe_cod_estadocivil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_bairro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_religiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_NumCertNasc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_RG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_NomeEscola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_cidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_Complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_fone_contato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_num_pontuacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_recado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_parentesco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_despmedicas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_valoraluguel'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_grau_parentesc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_SeMatric'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaSerie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_carromodel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_motomodel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaPeriodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_ind_TELPROPRIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_TempoResid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_idadeXserie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_razaoincomp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_ResideFam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_MotivoProcuraCompl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@pe_dsc_NResideFam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_PontoRef'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_telRecado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_CPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_cod_sias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_cod_UsuarioCadastro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_DiagSaude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_IncapazTrab'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_DoencaInfecto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_InvalTempDef'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_dtacadastro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dta_atualizaEscolar'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_inscritopor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_matricula'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_readmissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade_referencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_sus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@pe_num_cra'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_passe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_emissao_rg'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_ra'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_ind_invalidez_temporaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_invalidez_permanente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_trabalho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_cursos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_nao_ficar_sozinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_tirar_rua'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_creas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_cras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_vij'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_as'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_enc_ct'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_proc_outros'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_veiculo_outras_priori'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_veiculo_trabalho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_veiculo_unico_transp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_medidas_socioeducativas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_fisica'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_psicologica'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_sexual'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_negligencia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia_abandono'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conv_port_doencas_incap'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_nacionalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_emissor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 309
  end
  object dspUpd_Cadastro_TriagemDeca: TDataSetProvider
    DataSet = spUpd_Cadastro_TriagemDeca
    Constraints = True
    Left = 538
    Top = 309
  end
  object spSel_PontosFicha: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'Pontosficha;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_alcoolatra'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_aquarela'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_carro'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprovoualuguel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_convivealcool'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_convivedrogadiccao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conviveportdoencainfecto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conviveportdoencamental'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_conv_port_doencas_incap'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_criancasozinha'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprovadespmedic'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_drogadicto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_gestante'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_outroimovel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_medicamentopsico'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_membroautorinfracao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_moto'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_paimaeadolescente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_prostituicao'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_traficodrogas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_violencia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_Cod_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_TempoResid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_cond_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_natureza_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_estadocivil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_infra_estrutura'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inst_sanitaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_RelResp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_tipo_hab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_qtdecomodos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_qtdehabitantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_TempoResidencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_despmedicas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_valoraluguel'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@Valor_pontos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@qtde_InvalidosFamilia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@renda_Familiar'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@renda_beneficios'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@temp'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pnt_situacaosocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pnt_situacaomoradia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pnt_condSaude'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pnt_tempResidencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pnt_bens'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_motivodesc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_renda'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@num_rendaTotal'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@num_despesas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_ind_IncapazTrab'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_NumPortaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_habtcomodo'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@temp1'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@temp2'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@temp3'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end>
    Left = 88
    Top = 633
  end
  object dspSel_PontosFicha: TDataSetProvider
    DataSet = spSel_PontosFicha
    Constraints = True
    Left = 145
    Top = 633
  end
  object spSel_BeneficiosSociais: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_BeneficioSocialFamilia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_idBeneficio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 480
    Top = 375
  end
  object dspSel_BeneficiosSociais: TDataSetProvider
    DataSet = spSel_BeneficiosSociais
    Constraints = True
    Left = 538
    Top = 374
  end
  object spUpd_BeneficioSocial: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_upd_BeneficioFamiliar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_idBeneficio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@dsc_beneficio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@num_valorbeneficio'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end>
    Left = 480
    Top = 439
  end
  object spIns_BeneficioSocial: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_ins_BeneficioSocialFamilia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@dsc_beneficio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@num_valorbeneficio'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 480
    Top = 502
  end
  object spDel_BeneficioSocial: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_DEL_Cadastro_BeneficioFamiliar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_idBeneficio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 566
  end
  object dspUpd_BeneficioSocial: TDataSetProvider
    DataSet = spUpd_BeneficioSocial
    Constraints = True
    Left = 538
    Top = 438
  end
  object dspIns_BeneficioSocial: TDataSetProvider
    DataSet = spIns_BeneficioSocial
    Constraints = True
    Left = 538
    Top = 501
  end
  object dspDel_BeneficioSocial: TDataSetProvider
    DataSet = spDel_BeneficioSocial
    Constraints = True
    Left = 538
    Top = 565
  end
  object spFamilia_InsUpdDel: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_familia_INSUPDDEL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_cod_membrofamilia'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@dsc_nome_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ind_responsavel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_parentesco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_escolaridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ind_incapaztrab'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ind_portdoencacontagia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ind_cond_recluso'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ind_cond_falecido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ind_cond_nregcrianca'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ind_cond_morafamilia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ind_sexo_par'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ind_estadocivil_par'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ind_temguarda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@num_rendamensal_fam'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@dat_nasc_fam'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@dsc_diagnostico_par'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@dsc_cpf_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@dsc_rg_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dat_emissao_rg'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_emissor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_cidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_nacionalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_sus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@pe_num_cra'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_passe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dsc_numcertnasc_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_ind_inv_temp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_inv_perm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dsc_localtrab_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@dsc_tel_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@dsc_ocupacao_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@dsc_outrarenda'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@dsc_relacionamento_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@pe_cod_sitsaude_fam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@acao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 480
    Top = 631
  end
  object dspFamilia_InsUpdDel: TDataSetProvider
    DataSet = spFamilia_InsUpdDel
    Constraints = True
    Left = 538
    Top = 630
  end
  object dspSel_Par_Parentesco: TDataSetProvider
    DataSet = spSel_Par_Parentesco
    Constraints = True
    Left = 338
    Top = 633
  end
  object spSel_Par_Parentesco: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_Parentesco;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_parentesco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 633
  end
  object spSel_Cadastro_CompFamiliar: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Cadastro_CompFamiliar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cod_membrofamilia'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 688
    Top = 88
  end
  object dspSel_Cadastro_CompFamiliar: TDataSetProvider
    DataSet = spSel_Cadastro_CompFamiliar
    Constraints = True
    Left = 746
    Top = 88
  end
  object spSel_Cadastro_RegAtendimento: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_Cadastro_RegAtendimento_SEL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 688
    Top = 140
  end
  object dspSel_Cadastro_RegAtendimento: TDataSetProvider
    DataSet = spSel_Cadastro_RegAtendimento
    Constraints = True
    Left = 746
    Top = 140
  end
  object spIns_Cadastro_Relatorios: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_Cadastro_Relatorios_insert;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@id_relatorio'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@dta_digitacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@tipo_relatorio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_responsavel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@txt_relatorio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@dsc_entrevistador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_deca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 688
    Top = 196
  end
  object dspIns_Cadastro_Relatorio: TDataSetProvider
    DataSet = spIns_Cadastro_Relatorios
    Constraints = True
    Left = 746
    Top = 197
  end
  object spUpd_Cadastro_Relatorios: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_Cadastro_Relatorios_update;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@id_relatorio'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@dta_digitacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@tipo_relatorio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_responsavel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@txt_relatorio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@dsc_entrevistador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_deca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 688
    Top = 252
  end
  object dspUpd_Cadastro_Relatorio: TDataSetProvider
    DataSet = spUpd_Cadastro_Relatorios
    Constraints = True
    Left = 746
    Top = 252
  end
  object spSel_Cadastro_Relatorios: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_Cadastro_Relatorios_select;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_deca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo_relatorio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 688
    Top = 310
  end
  object dspSel_Cadastro_Relatorios: TDataSetProvider
    DataSet = spSel_Cadastro_Relatorios
    Constraints = True
    Left = 746
    Top = 310
  end
  object spUpd_AtualizaUsuarioDecaRelatorio: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'spUpd_AtualizaUsuarioDecaRelatorio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_deca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_responsavel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 688
    Top = 438
  end
  object spSel_AtualizaUsuarioDecaRelatorio: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'spSel_UsuariosDecaRelatorios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_deca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 688
    Top = 374
  end
  object dspSel_AtualizaUsuariosDecaRelatorio: TDataSetProvider
    DataSet = spSel_AtualizaUsuarioDecaRelatorio
    Constraints = True
    Left = 746
    Top = 374
  end
  object dspUpd_AtualizaUsuariosDecaRelatorio: TDataSetProvider
    DataSet = spUpd_AtualizaUsuarioDecaRelatorio
    Constraints = True
    Left = 746
    Top = 438
  end
  object spSel_PendenciaDocumentos: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_PendenciaDocumentos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 688
    Top = 501
  end
  object spIns_PendenciaDocumentos: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_INS_PendenciaDocumentos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_cartprof'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprenda'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_compaluguel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comptemporesid'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_nis'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_declaraescola'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_declescolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_sus'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_sus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_cpf'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_outros'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_outros'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 688
    Top = 565
  end
  object spUpd_PendenciaDocumentos: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_UPD_PendenciaDocumentos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_cartprof'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comprenda'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_compaluguel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_comptemporesid'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_ind_nis'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_declaraescola'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_declescolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_sus'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_sus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_cpf'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_ind_outros'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes_outros'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 688
    Top = 629
  end
  object dspSel_PendenciaDocumentos: TDataSetProvider
    DataSet = spSel_PendenciaDocumentos
    Constraints = True
    Left = 746
    Top = 501
  end
  object dspIns_PendenciaDocumentos: TDataSetProvider
    DataSet = spIns_PendenciaDocumentos
    Constraints = True
    Left = 746
    Top = 565
  end
  object dspUpd_PendenciaDocumentos: TDataSetProvider
    DataSet = spUpd_PendenciaDocumentos
    Constraints = True
    Left = 746
    Top = 629
  end
  object spUpd_AlteraStatusInscricao: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'spUpd_AlteraStatusInscricao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_inscricao_digt'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 688
    Top = 695
  end
  object dspUpd_AlteraStatusInscricao: TDataSetProvider
    DataSet = spUpd_AlteraStatusInscricao
    Constraints = True
    Left = 746
    Top = 695
  end
  object spGERA_DEMANDA: TADOStoredProc
    Connection = adoConnTriagem
    CursorType = ctStatic
    ProcedureName = 'spGERA_DEMANDA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_faixaetaria1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_faixaetaria2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaperiodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_cod_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_flg_selecionado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 87
    Top = 695
  end
  object dspGERA_DEMANDA: TDataSetProvider
    DataSet = spGERA_DEMANDA
    Constraints = True
    Left = 145
    Top = 696
  end
  object spINS_DEMANDA: TADOStoredProc
    Connection = adoConnTriagem
    CursorType = ctStatic
    ProcedureName = 'spINS_DEMANDA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_dat_ligacao1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_quem_ligacao1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_ligacao2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_quem_ligacao2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_ligacao3'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_quem_ligacao3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_selecionado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 87
    Top = 759
  end
  object dspINS_DEMANDA: TDataSetProvider
    DataSet = spINS_DEMANDA
    Constraints = True
    Left = 146
    Top = 759
  end
  object spUPD_DEMANDA: TADOStoredProc
    Connection = adoConnTriagem
    CursorType = ctStatic
    ProcedureName = 'spUPD_DEMANDA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_dat_ligacao1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_quem_ligacao1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_ligacao2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_quem_ligacao2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_ligacao3'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_quem_ligacao3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_selecionado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 279
    Top = 759
  end
  object dspUPD_DEMANDA: TDataSetProvider
    DataSet = spUPD_DEMANDA
    Constraints = True
    Left = 338
    Top = 759
  end
  object dspDEL_DEMANDA: TDataSetProvider
    DataSet = spDEL_DEMANDA
    Constraints = True
    Left = 538
    Top = 759
  end
  object spDEL_DEMANDA: TADOStoredProc
    Connection = adoConnTriagem
    CursorType = ctStatic
    ProcedureName = 'spDEL_DEMANDA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 479
    Top = 759
  end
  object spSel_DEMANDA: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'spSel_DEMANDA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_inscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 688
    Top = 759
  end
  object dspSel_DEMANDA: TDataSetProvider
    DataSet = spSel_DEMANDA
    Constraints = True
    Left = 746
    Top = 759
  end
  object spMIGRACAO_TRIAGEM_DECA: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'spMIGRACAO_TRAGEM_DECA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_codMatricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_codInscricao'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pe_Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_Bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_PeriodoFundhas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_CodUnidadeDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_NumSecao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_DatAdmissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_CodUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_EmissorRG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_Naturalidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_Nacionalidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_DatHistorico'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 896
    Top = 88
  end
  object dspMIGRACAO_TRIAGEM_DECA: TDataSetProvider
    DataSet = spMIGRACAO_TRIAGEM_DECA
    Constraints = True
    Left = 975
    Top = 88
  end
  object spDEL_ComposicaoFamiliar: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_DEL_Cadastro_CompFamiliar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cod_membrofamilia'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 896
    Top = 152
  end
  object dspDel_ComposicaoFamiliar: TDataSetProvider
    DataSet = spDEL_ComposicaoFamiliar
    Constraints = True
    Left = 976
    Top = 152
  end
end
