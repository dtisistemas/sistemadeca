program DecaServer;

uses
  Forms,
  DecaServer_TLB in 'DecaServer_TLB.pas',
  uRemDM in 'uRemDM.pas' {dbDecaServer: TRemoteDataModule} {dbDecaServer: CoClass},
  uServer in 'uServer.pas' {frmServer},
  udbTriagemDeca in 'udbTriagemDeca.pas' {dbTriagemDeca: TRemoteDataModule} {dbTriagemDeca: CoClass};

{$R *.TLB}

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Sistema DECA - Servidor de Aplica��o';
  Application.CreateForm(TfrmServer, frmServer);
  Application.Run;
end.
