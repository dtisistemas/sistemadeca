object dbDecaServer: TdbDecaServer
  OldCreateOrder = False
  Left = 63
  Top = 117
  Height = 934
  Width = 1797
  object adoConn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=DECA_TST;Data Source=192.168.0.20;U' +
      'se Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;' +
      'Workstation ID=FABIO;Use Encryption for Data=False;Tag with colu' +
      'mn collation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 24
    Top = 16
  end
  object spInc_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_municipio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_uf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_fone_celular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone_recado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_cadastro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_treinado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 137
    Top = 72
  end
  object dspInc_Usuario: TDataSetProvider
    DataSet = spInc_Usuario
    Constraints = True
    Left = 105
    Top = 72
  end
  object spExc_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 63
    Top = 353
  end
  object spAlt_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_municipio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_uf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_fone_celular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone_recado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 218
    Top = 72
  end
  object dspAlt_Usuario: TDataSetProvider
    DataSet = spAlt_Usuario
    Constraints = True
    Left = 186
    Top = 72
  end
  object dspExc_Usuario: TDataSetProvider
    DataSet = spExc_Usuario
    Constraints = True
    Left = 31
    Top = 353
  end
  object spSel_Unidade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 222
    Top = 315
  end
  object dspSel_Unidade: TDataSetProvider
    DataSet = spSel_Unidade
    Constraints = True
    Left = 190
    Top = 315
  end
  object spSel_Cadastro_Move: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Move;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_PosicaoRegistro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_MATRICULA_atual'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 137
    Top = 104
  end
  object dspSel_Cadastro_Move: TDataSetProvider
    DataSet = spSel_Cadastro_Move
    Constraints = True
    Left = 105
    Top = 104
  end
  object spSel_Cadastro_Pesquisa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Pesquisa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 142
    Top = 354
  end
  object dspSel_Cadastro_Pesquisa: TDataSetProvider
    DataSet = spSel_Cadastro_Pesquisa
    Constraints = True
    Left = 110
    Top = 354
  end
  object spInc_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_etnia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_calcado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tam_camisa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_pontuacao_triagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_projeto_vida'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_admissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tam_calca'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_sanguineo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_possui_bolsa_familia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_participa_ativ_externa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ativ_ext_quais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_cor_olhos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_cor_cabelos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_ind_fumante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_portador_necess_especiais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_necessidades_esp_quais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_num_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_certidao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ctps_num'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ctps_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_emissor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_emissao_rg'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_uf_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_local_nascimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_num_titulo_eleitor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 13
        Value = Null
      end
      item
        Name = '@pe_num_zona_eleitoral'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao_eleitoral'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_emissao_ctps'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_uf_ctps'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_num_pis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end
      item
        Name = '@pe_dat_emissao_pis'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_banco_pis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_reservista'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_reservista_csm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_num_cartao_sus'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@pe_dsc_municipio_residencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_uf_residencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_num_telefone_celular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_ind_grau_instrucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_estado_civil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_nacionalidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_conjuge'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_guarda_regularizada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 61
    Top = 112
  end
  object spAlt_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_etnia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_calcado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tam_camisa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_pontuacao_triagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_projeto_vida'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_admissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tam_calca'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_sanguineo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_certidao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_possui_bolsa_familia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_participa_ativ_externa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ativ_ext_quais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 218
    Top = 105
  end
  object dspInc_Cadastro: TDataSetProvider
    DataSet = spInc_Cadastro
    Constraints = True
    Left = 29
    Top = 104
  end
  object dspAlt_Cadastro: TDataSetProvider
    DataSet = spAlt_Cadastro
    Constraints = True
    Left = 186
    Top = 105
  end
  object spAlt_Cadastro_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_turma_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_obs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 63
    Top = 390
  end
  object dspAlt_Cadastro_Escola: TDataSetProvider
    DataSet = spAlt_Cadastro_Escola
    Constraints = True
    Left = 31
    Top = 390
  end
  object spSel_Cadastro_NovaMatricula999: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Cadastro_NovaMatricula999;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 62
    Top = 141
  end
  object dspSel_Cadastro_NovaMatricula999: TDataSetProvider
    DataSet = spSel_Cadastro_NovaMatricula999
    Constraints = True
    Left = 30
    Top = 141
  end
  object spSel_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 219
    Top = 140
  end
  object dspSel_Escola: TDataSetProvider
    DataSet = spSel_Escola
    Constraints = True
    Left = 187
    Top = 140
  end
  object dspSel_Cadastro_Programa_Social: TDataSetProvider
    DataSet = spSel_Cadastro_Programa_Social
    Constraints = True
    Left = 31
    Top = 176
  end
  object spSel_Cadastro_Programa_Social: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Programa_Social;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_cad_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_insercao1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_insercao2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_desligamento1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_desligamento2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 63
    Top = 176
  end
  object spInc_Cadastro_Programa_Social: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Programa_Social;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_quem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_insercao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 138
    Top = 140
  end
  object dspInc_Cadastro_Programa_Social: TDataSetProvider
    DataSet = spInc_Cadastro_Programa_Social
    Constraints = True
    Left = 106
    Top = 140
  end
  object spAlt_Cadastro_Programa_Social: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Programa_Social(old);1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_programa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_local'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 219
    Top = 174
  end
  object dspAlt_Cadastro_Programa_Social: TDataSetProvider
    DataSet = spAlt_Cadastro_Programa_Social
    Constraints = True
    Left = 187
    Top = 174
  end
  object spAlt_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_responsavel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_familiar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_ocupacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_dsc_local_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_val_renda_mensal'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_contato_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaridade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto_fundhas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_vinculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_ind_moracasa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_rg_fam'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@pe_num_cpf_fam'
        DataType = ftSmallint
        Value = Null
      end>
    Left = 143
    Top = 390
  end
  object dspAlt_Cadastro_Familia: TDataSetProvider
    DataSet = spAlt_Cadastro_Familia
    Constraints = True
    Left = 111
    Top = 390
  end
  object spExc_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 63
    Top = 212
  end
  object spInc_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_responsavel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_nom_familiar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_ocupacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_dsc_local_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_val_renda_mensal'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_contato_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaridade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto_fundhas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_vinculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_moracasa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_rg_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cpf_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 139
    Top = 175
  end
  object spSel_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 220
    Top = 213
  end
  object dspExc_Cadastro_Familia: TDataSetProvider
    DataSet = spExc_Cadastro_Familia
    Constraints = True
    Left = 31
    Top = 212
  end
  object dspInc_Cadastro_Familia: TDataSetProvider
    DataSet = spInc_Cadastro_Familia
    Constraints = True
    Left = 107
    Top = 175
  end
  object dspSel_Cadastro_Familia: TDataSetProvider
    DataSet = spSel_Cadastro_Familia
    Constraints = True
    Left = 188
    Top = 213
  end
  object spSel_Cadastro_Contador: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Cadastro_Contador;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 223
    Top = 352
  end
  object dspSel_Cadastro_Contador: TDataSetProvider
    DataSet = spSel_Cadastro_Contador
    Constraints = True
    Left = 191
    Top = 352
  end
  object spSel_Cadastro_Familia_SomaRenda: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Cadastro_Familia_SomaRenda;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 63
    Top = 246
  end
  object dspSel_Cadastro_Familia_SomaRenda: TDataSetProvider
    DataSet = spSel_Cadastro_Familia_SomaRenda
    Constraints = True
    Left = 31
    Top = 246
  end
  object spSel_Cadastro_Atendimento_Rede: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Atendimento_Rede;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_atendimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 63
    Top = 280
  end
  object dspSel_Cadastro_Atendimento_Rede: TDataSetProvider
    DataSet = spSel_Cadastro_Atendimento_Rede
    Constraints = True
    Left = 31
    Top = 280
  end
  object spInc_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_historico'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipo_historico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_ind_motivo2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dsc_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_final_suspensao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_materia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_ind_motivo_transf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_orig_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dest_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dest_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dest_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_inicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_final'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_flg_passe'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_num_cotas_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_orig_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dest_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dat_curso_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_curso_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_estagio_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@pe_num_estagio_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 139
    Top = 213
  end
  object dspInc_Cadastro_Historico: TDataSetProvider
    DataSet = spInc_Cadastro_Historico
    Constraints = True
    Left = 107
    Top = 213
  end
  object spEXC_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_id_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 248
  end
  object dspEXC_Cadastro_Historico: TDataSetProvider
    DataSet = spEXC_Cadastro_Historico
    Constraints = True
    Left = 189
    Top = 248
  end
  object spAlt_Cadastro_Atendimento_Rede: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Atendimento_Rede;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_atendimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_atendimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_atendimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_local'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 140
    Top = 248
  end
  object dspAlt_Cadastro_Atendimento_Rede: TDataSetProvider
    DataSet = spAlt_Cadastro_Atendimento_Rede
    Constraints = True
    Left = 108
    Top = 248
  end
  object dspInc_Cadastro_Atendimento_Rede: TDataSetProvider
    DataSet = spInc_Cadastro_Atendimento_Rede
    Constraints = True
    Left = 189
    Top = 281
  end
  object spInc_Cadastro_Atendimento_Rede: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Atendimento_Rede;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_atendimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_atendimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_local'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 281
  end
  object dspSel_Cadastro_Status: TDataSetProvider
    DataSet = spSel_Cadastro_Status
    Constraints = True
    Left = 189
    Top = 425
  end
  object dspAlt_Cadastro_Tira_Suspensao: TDataSetProvider
    DataSet = spAlt_Cadastro_Tira_Suspensao
    Constraints = True
    Left = 32
    Top = 314
  end
  object spAlt_Cadastro_Tira_Suspensao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Tira_Suspensao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 314
  end
  object spSel_Cadastro_Status: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Status;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 221
    Top = 425
  end
  object spSel_Historico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_HISTORICO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 142
    Top = 281
  end
  object dspSel_Historico: TDataSetProvider
    DataSet = spSel_Historico
    Constraints = True
    Left = 110
    Top = 281
  end
  object spAlt_Cadastro_Status: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Status;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 142
    Top = 314
  end
  object dspAlt_Cadastro_Status: TDataSetProvider
    DataSet = spAlt_Cadastro_Status
    Constraints = True
    Left = 110
    Top = 314
  end
  object spSel_Cadastro_Familia_Responsavel: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Familia_Responsavel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_responsavel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 305
    Top = 248
  end
  object dspSel_Cadastro_Familia_Responsavel: TDataSetProvider
    DataSet = spSel_Cadastro_Familia_Responsavel
    Constraints = True
    Left = 273
    Top = 248
  end
  object spSel_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_evolucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 424
  end
  object dspSel_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spSel_Cadastro_Evolucao_SC
    Constraints = True
    Left = 32
    Top = 424
  end
  object spAlt_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_evolucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_evolucao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 143
    Top = 425
  end
  object dspAlt_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spAlt_Cadastro_Evolucao_SC
    Constraints = True
    Left = 111
    Top = 425
  end
  object spExc_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_evolucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 300
    Top = 73
  end
  object dspExc_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spExc_Cadastro_Evolucao_SC
    Constraints = True
    Left = 268
    Top = 73
  end
  object dspInc_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spInc_Cadastro_Evolucao_SC
    Constraints = True
    Left = 269
    Top = 105
  end
  object spInc_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_evolucao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 301
    Top = 105
  end
  object spSel_Lista_presenca: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_Lista_presenca;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 302
    Top = 142
  end
  object dspSel_Lista_Presenca: TDataSetProvider
    DataSet = spSel_Lista_presenca
    Constraints = True
    Left = 270
    Top = 142
  end
  object spSel_Peti: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_PETI;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 303
    Top = 176
  end
  object dspSel_Peti: TDataSetProvider
    DataSet = spSel_Peti
    Constraints = True
    Left = 270
    Top = 176
  end
  object spSel_Ordena_Matricula: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_matricula;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 304
    Top = 212
  end
  object dspSel_Orderna_Matricula: TDataSetProvider
    DataSet = spSel_Ordena_Matricula
    Constraints = True
    Left = 272
    Top = 212
  end
  object spSel_Ordena_Matricula_Adm: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Matricula_Adm;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 306
    Top = 281
  end
  object dspSel_Ordena_Matricula_Adm: TDataSetProvider
    DataSet = spSel_Ordena_Matricula_Adm
    Constraints = True
    Left = 274
    Top = 281
  end
  object spSel_Cadastro_Escolaridade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Escolaridade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 307
    Top = 315
  end
  object dspSel_Cadastro_Escolaridade: TDataSetProvider
    DataSet = spSel_Cadastro_Escolaridade
    Constraints = True
    Left = 275
    Top = 315
  end
  object spSel_Ordena_Nome_Adm: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Nome_Adm;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 309
    Top = 353
  end
  object dspSel_Ordena_Nome_Adm: TDataSetProvider
    DataSet = spSel_Ordena_Nome_Adm
    Constraints = True
    Left = 277
    Top = 353
  end
  object spSel_Ordena_Nome: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_NomeTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 309
    Top = 392
  end
  object dspSel_Ordena_Nome: TDataSetProvider
    DataSet = spSel_Ordena_Nome
    Constraints = True
    Left = 277
    Top = 392
  end
  object spSel_Cep_Adm: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CEP_Adm;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 308
    Top = 426
  end
  object spSel_Cep: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CEP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 388
    Top = 72
  end
  object dspSel_Cep_Adm: TDataSetProvider
    DataSet = spSel_Cep_Adm
    Constraints = True
    Left = 276
    Top = 426
  end
  object dspSel_Cep: TDataSetProvider
    DataSet = spSel_Cep
    Constraints = True
    Left = 356
    Top = 72
  end
  object spSel_Afastamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Afastamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 388
    Top = 104
  end
  object dspSel_Afastamento: TDataSetProvider
    DataSet = spSel_Afastamento
    Constraints = True
    Left = 356
    Top = 104
  end
  object spSel_Advertencias: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Advertencias;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 388
    Top = 144
  end
  object dspSel_Advertencias: TDataSetProvider
    DataSet = spSel_Advertencias
    Constraints = True
    Left = 356
    Top = 144
  end
  object spSel_Desligados: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Desligados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 388
    Top = 184
  end
  object dspSel_Desligados: TDataSetProvider
    DataSet = spSel_Desligados
    Constraints = True
    Left = 356
    Top = 184
  end
  object spSel_Aniversariantes_mes: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Aniversariantes_mes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 388
    Top = 216
  end
  object dspSel_Aniversariantes_mes: TDataSetProvider
    DataSet = spSel_Aniversariantes_mes
    Constraints = True
    Left = 356
    Top = 217
  end
  object spAlt_Cadastro_Status_Transf: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Status_Transf;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 388
    Top = 248
  end
  object dspAlt_Cadastro_Status_Transf: TDataSetProvider
    DataSet = spAlt_Cadastro_Status_Transf
    Constraints = True
    Left = 356
    Top = 248
  end
  object spSel_Conta_Em_Transferencia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Conta_em_transferencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 388
    Top = 280
  end
  object dspSel_Conta_Em_Transferencia: TDataSetProvider
    DataSet = spSel_Conta_Em_Transferencia
    Constraints = True
    Left = 356
    Top = 280
  end
  object spSel_Ultima_Transferencia_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Ultima_Transferencia_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 388
    Top = 315
  end
  object dspSel_Ultima_Transferencia_Unidade: TDataSetProvider
    DataSet = spSel_Ultima_Transferencia_Unidade
    Constraints = True
    Left = 356
    Top = 315
  end
  object spSel_Ultima_Transferencia_Convenio: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ultima_Transferencia_Convenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 388
    Top = 352
  end
  object dspSel_Ultima_Transferencia_Convenio: TDataSetProvider
    DataSet = spSel_Ultima_Transferencia_Convenio
    Constraints = True
    Left = 356
    Top = 352
  end
  object spSel_Transferencias: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Transferencias;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 388
    Top = 392
  end
  object dspSel_Transferencias: TDataSetProvider
    DataSet = spSel_Transferencias
    Constraints = True
    Left = 356
    Top = 392
  end
  object spAlt_Cadastro_Muda_Matricula: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Muda_Matricula;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula_atual'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_matricula_novo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 389
    Top = 426
  end
  object dspAlt_Cadastro_Muda_Matricula: TDataSetProvider
    DataSet = spAlt_Cadastro_Muda_Matricula
    Constraints = True
    Left = 356
    Top = 426
  end
  object spSel_Cadastro_Historico_Periodo: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Historico_Periodo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 65
    Top = 464
  end
  object dspSel_Cadastro_Historico_Periodo: TDataSetProvider
    DataSet = spSel_Cadastro_Historico_Periodo
    Constraints = True
    Left = 32
    Top = 463
  end
  object spSel_Cadastro_Evolucao_Periodo: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Evolucao_Periodo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 141
    Top = 464
  end
  object dspSel_Cadastro_Evolucao_Periodo: TDataSetProvider
    DataSet = spSel_Cadastro_Evolucao_Periodo
    Constraints = True
    Left = 110
    Top = 464
  end
  object spSel_Indicadores: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Indicadores;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 464
  end
  object dspSel_Indicadores: TDataSetProvider
    DataSet = spSel_Indicadores
    Constraints = True
    Left = 189
    Top = 464
  end
  object spSel_Total_Familia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 72
  end
  object dspSel_Total_Familia: TDataSetProvider
    DataSet = spSel_Total_Familia
    Constraints = True
    Left = 448
    Top = 72
  end
  object spSel_Total_Crianca: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Criancas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 105
  end
  object dspSel_Total_Crianca: TDataSetProvider
    DataSet = spSel_Total_Crianca
    Constraints = True
    Left = 448
    Top = 105
  end
  object spSel_Total_Adolescente: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Adolescente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 144
  end
  object spSel_Total_Profissionais: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Profissionais;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 184
  end
  object spSel_Total_Projeto_de_Vida: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Projeto_de_Vida;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 216
  end
  object dspSel_Total_Adolescente: TDataSetProvider
    DataSet = spSel_Total_Adolescente
    Constraints = True
    Left = 448
    Top = 144
  end
  object dspSel_Total_Profissionais: TDataSetProvider
    DataSet = spSel_Total_Profissionais
    Constraints = True
    Left = 448
    Top = 184
  end
  object dspSel_Total_Projeto_de_Vida: TDataSetProvider
    DataSet = spSel_Total_Projeto_de_Vida
    Constraints = True
    Left = 448
    Top = 216
  end
  object spSel_Fam_Visita_Domiciliar: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Visita_Domiciliar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 248
  end
  object dspSel_Fam_Visita_Domiciliar: TDataSetProvider
    DataSet = spSel_Fam_Visita_Domiciliar
    Constraints = True
    Left = 448
    Top = 248
  end
  object spSel_Fam_Atendimento_Individual: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Atendimento_Individual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 280
  end
  object dspSel_Fam_Atendimento_Individual: TDataSetProvider
    DataSet = spSel_Fam_Atendimento_Individual
    Constraints = True
    Left = 448
    Top = 280
  end
  object spSel_Fam_Anamnese: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Anamnese;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 314
  end
  object dspSEL_Fam_Anamnese: TDataSetProvider
    DataSet = spSel_Fam_Anamnese
    Constraints = True
    Left = 448
    Top = 315
  end
  object spSel_Fam_Abordagem_Grupal: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Abordagem_Grupal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 352
  end
  object dspSEL_Fam_Abordagem_Grupal: TDataSetProvider
    DataSet = spSel_Fam_Abordagem_Grupal
    Constraints = True
    Left = 448
    Top = 352
  end
  object spSel_Fam_Integracao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Integracao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 392
  end
  object dspSEL_Fam_Integracao: TDataSetProvider
    DataSet = spSel_Fam_Integracao
    Constraints = True
    Left = 448
    Top = 392
  end
  object spSel_Fam_Reuniao_Informativa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Reuniao_Informativa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 571
    Top = 229
  end
  object dspSEL_Fam_Reuniao_Informativa: TDataSetProvider
    DataSet = spSel_Fam_Reuniao_Informativa
    Constraints = True
    Left = 539
    Top = 73
  end
  object spSel_Criadol_Atendimento_Individual: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Criadol_Atendimento_Individual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 107
  end
  object dspSEL_Criadol_Atendimento_Individual: TDataSetProvider
    DataSet = spSel_Criadol_Atendimento_Individual
    Constraints = True
    Left = 539
    Top = 107
  end
  object spSel_Criadol_Abordagem_Grupal: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Criadol_Abordagem_Grupal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 302
  end
  object dspSEL_Criadol_Abordagem_Grupal: TDataSetProvider
    DataSet = spSel_Criadol_Abordagem_Grupal
    Constraints = True
    Left = 539
    Top = 145
  end
  object spSel_Criadol_Treinamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Criadol_Treinamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 184
  end
  object dspSEL_Criadol_Treinamento: TDataSetProvider
    DataSet = spSel_Criadol_Treinamento
    Constraints = True
    Left = 539
    Top = 184
  end
  object spSel_Profissional_Comunidade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Profissional_Comunidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 217
  end
  object dspSEL_Profissional_Comunidade: TDataSetProvider
    DataSet = spSel_Profissional_Comunidade
    Constraints = True
    Left = 539
    Top = 217
  end
  object spSel_RelDiversos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_RelDiversos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 571
    Top = 248
  end
  object dspSEL_RelDiversos: TDataSetProvider
    DataSet = spSel_RelDiversos
    Constraints = True
    Left = 538
    Top = 248
  end
  object spSel_Cursos_Capacitacao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cursos_Capacitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 280
  end
  object dspSEL_Cursos_Capacitacao: TDataSetProvider
    DataSet = spSel_Cursos_Capacitacao
    Constraints = True
    Left = 539
    Top = 280
  end
  object spSel_Atividades_Extras: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Atividades_Extras;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 573
    Top = 314
  end
  object dspSEL_Atividades_Extras: TDataSetProvider
    DataSet = spSel_Atividades_Extras
    Constraints = True
    Left = 540
    Top = 314
  end
  object spSel_Visitas_Diversas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Visitas_Diversas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 573
    Top = 351
  end
  object dspSEL_Visitas_Diversas: TDataSetProvider
    DataSet = spSel_Visitas_Diversas
    Constraints = True
    Left = 540
    Top = 351
  end
  object spSel_Supervisao_Estagio: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Supervisao_Estagio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 481
    Top = 427
  end
  object dspSEL_Supervisao_Estagio: TDataSetProvider
    DataSet = spSel_Supervisao_Estagio
    Constraints = True
    Left = 449
    Top = 426
  end
  object spSel_Reunioes_Tecnicas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Reunioes_Tecnicas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 392
  end
  object dspSel_Reunioes_Tecnicas: TDataSetProvider
    DataSet = spSel_Reunioes_Tecnicas
    Constraints = True
    Left = 539
    Top = 392
  end
  object spSel_Discussao_Casos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Discussao_Casos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 309
    Top = 464
  end
  object spSel_Projeto_Vida_Visita: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Projeto_Vida_Visita;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 389
    Top = 464
  end
  object spSel_Projeto_Vida_Abordagem: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Projeto_Vida_Abordagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 464
  end
  object spSel_Projeto_Vida_AtIndividual: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Projeto_Vida_AtIndividual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 424
  end
  object dspSel_Discussao_Casos: TDataSetProvider
    DataSet = spSel_Discussao_Casos
    Constraints = True
    Left = 277
    Top = 464
  end
  object dspSel_Projeto_Vida_Visita: TDataSetProvider
    DataSet = spSel_Projeto_Vida_Visita
    Constraints = True
    Left = 356
    Top = 464
  end
  object dspSel_Projeto_Vida_Abordagem: TDataSetProvider
    DataSet = spSel_Projeto_Vida_Abordagem
    Constraints = True
    Left = 452
    Top = 464
  end
  object dspSel_Projeto_Vida_AtIndividual: TDataSetProvider
    DataSet = spSel_Projeto_Vida_AtIndividual
    Constraints = True
    Left = 540
    Top = 424
  end
  object dspSel_Usuario: TDataSetProvider
    DataSet = spSel_Usuario
    Constraints = True
    Left = 29
    Top = 71
  end
  object spSel_Usuario: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 62
    Top = 72
  end
  object spSel_Lista_PresencaTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_Lista_PresencaTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 72
  end
  object dspSel_Lista_PresencaTodos: TDataSetProvider
    DataSet = spSel_Lista_PresencaTodos
    Constraints = True
    Left = 620
    Top = 72
  end
  object spSel_PetiTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_PetiTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 653
    Top = 105
  end
  object dspSel_PetiTodos: TDataSetProvider
    DataSet = spSel_PetiTodos
    Constraints = True
    Left = 620
    Top = 106
  end
  object spSel_Ordena_MatriculaTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_matriculaTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 144
  end
  object dspSel_Ordena_MatriculaTodos: TDataSetProvider
    DataSet = spSel_Ordena_MatriculaTodos
    Constraints = True
    Left = 620
    Top = 144
  end
  object spSel_Ordena_Matricula_AdmTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Matricula_AdmTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 184
  end
  object dspSel_Ordena_Matricula_AdmTodos: TDataSetProvider
    DataSet = spSel_Ordena_Matricula_AdmTodos
    Constraints = True
    Left = 620
    Top = 184
  end
  object spSel_Ordena_NomeTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_NomeTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 216
  end
  object dspSel_Ordena_NomeTodos: TDataSetProvider
    DataSet = spSel_Ordena_NomeTodos
    Constraints = True
    Left = 620
    Top = 216
  end
  object spSel_Ordena_Nome_AdmTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Nome_AdmTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 248
  end
  object dspSel_Ordena_Nome_AdmTodos: TDataSetProvider
    DataSet = spSel_Ordena_Nome_AdmTodos
    Constraints = True
    Left = 620
    Top = 248
  end
  object spSel_CepTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_CEPTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 280
  end
  object spSel_Cep_AdmTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CEP_AdmTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 312
  end
  object dspSel_CepTodos: TDataSetProvider
    DataSet = spSel_CepTodos
    Constraints = True
    Left = 620
    Top = 280
  end
  object dspSel_Cep_AdmTodos: TDataSetProvider
    DataSet = spSel_Cep_AdmTodos
    Constraints = True
    Left = 620
    Top = 312
  end
  object spSel_Aniversariantes_MesTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Aniversariantes_mesTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 352
  end
  object dspSel_Aniversariantes_MesTodos: TDataSetProvider
    DataSet = spSel_Aniversariantes_MesTodos
    Constraints = True
    Left = 620
    Top = 352
  end
  object spExc_DadosGrafico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_DadosGrafico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 504
  end
  object dspExc_DadosGrafico: TDataSetProvider
    DataSet = spExc_DadosGrafico
    Constraints = True
    Left = 32
    Top = 504
  end
  object spInc_DadosGrafico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_DadosGrafico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_total'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_num_total'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 141
    Top = 503
  end
  object dspInc_DadosGrafico: TDataSetProvider
    DataSet = spInc_DadosGrafico
    Constraints = True
    Left = 109
    Top = 503
  end
  object spSel_MeninosManha: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninosManha;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 503
  end
  object spSel_MeninosTarde: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninosTarde;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 309
    Top = 503
  end
  object spSel_MeninasManha: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninasManha;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 388
    Top = 503
  end
  object spSel_MeninasTarde: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninasTarde;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 503
  end
  object spSel_TotalUnidade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TotalUnidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 660
    Top = 511
  end
  object dspSel_MeninosManha: TDataSetProvider
    DataSet = spSel_MeninosManha
    Constraints = True
    Left = 189
    Top = 503
  end
  object dspSel_MeninosTarde: TDataSetProvider
    DataSet = spSel_MeninosTarde
    Constraints = True
    Left = 277
    Top = 504
  end
  object dspSel_MeninasManha: TDataSetProvider
    DataSet = spSel_MeninasManha
    Constraints = True
    Left = 356
    Top = 504
  end
  object dspSel_MeninasTarde: TDataSetProvider
    DataSet = spSel_MeninasTarde
    Constraints = True
    Left = 316
    Top = 504
  end
  object dspSel_TotalUnidade: TDataSetProvider
    DataSet = spSel_TotalUnidade
    Constraints = True
    Left = 628
    Top = 512
  end
  object spSel_DadosGrafico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_DadosGrafico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 741
    Top = 72
  end
  object dspSel_DadosGrafico: TDataSetProvider
    DataSet = spSel_DadosGrafico
    Constraints = True
    Left = 709
    Top = 72
  end
  object spSel_Mostra_a_Transferir: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Mostra_a_Transferir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 496
  end
  object dspSel_Mostra_a_Transferir: TDataSetProvider
    DataSet = spSel_Mostra_a_Transferir
    Constraints = True
    Left = 515
    Top = 497
  end
  object spSel_Transferencias_Encaminhadas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Transferencias_Encaminhadas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 291
    Top = 504
  end
  object dspSel_Transferencias_Encaminhadas: TDataSetProvider
    DataSet = spSel_Transferencias_Encaminhadas
    Constraints = True
    Left = 452
    Top = 401
  end
  object spSel_AtivosSuspensosAfastados: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_AtivosSuspensosAfastados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 742
    Top = 280
  end
  object dspSel_AtivosSuspensosAfastados: TDataSetProvider
    DataSet = spSel_AtivosSuspensosAfastados
    Constraints = True
    Left = 710
    Top = 280
  end
  object spExc_Idades: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Idade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 743
    Top = 144
  end
  object spSel_Idades: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Idades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 742
    Top = 104
  end
  object spInc_Idades: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_Idade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_idade_anos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_idade_meses'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 745
    Top = 247
  end
  object dspInc_Idades: TDataSetProvider
    DataSet = spInc_Idades
    Constraints = True
    Left = 711
    Top = 248
  end
  object dspExc_Idades: TDataSetProvider
    DataSet = spExc_Idades
    Constraints = True
    Left = 711
    Top = 144
  end
  object dspSel_Idades: TDataSetProvider
    DataSet = spSel_Idades
    Constraints = True
    Left = 710
    Top = 104
  end
  object spSel_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_id_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 392
  end
  object dspSel_Cadastro_Historico: TDataSetProvider
    DataSet = spSel_Cadastro_Historico
    Constraints = True
    Left = 191
    Top = 392
  end
  object spAlt_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_id_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipo_historico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_ind_motivo2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dsc_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_final_suspensao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_materia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_ind_motivo_transf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_orig_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_orig_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dest_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dest_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dest_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_inicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_final'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_flg_passe'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_num_cotas_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_orig_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dest_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dat_curso_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_curso_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_estagio_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_estagio_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 741
    Top = 216
  end
  object dspAlt_Cadastro_Historico: TDataSetProvider
    DataSet = spAlt_Cadastro_Historico
    Constraints = True
    Left = 709
    Top = 216
  end
  object spSel_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cotas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 742
    Top = 184
  end
  object dspSel_Cadastro_Frequencia: TDataSetProvider
    DataSet = spSel_Cadastro_Frequencia
    Constraints = True
    Left = 710
    Top = 184
  end
  object spAlt_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dia01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia13'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia14'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia15'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia16'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia17'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia18'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia19'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia20'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia21'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia22'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia23'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia24'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia25'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia26'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia27'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia28'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia29'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia30'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia31'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsr_acumulado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dias_uteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_comvenc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semvenc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 69
    Top = 552
  end
  object dspAlt_Cadastro_Frequencia: TDataSetProvider
    DataSet = spAlt_Cadastro_Frequencia
    Constraints = True
    Left = 37
    Top = 552
  end
  object spInc_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_dia01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia13'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia14'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia15'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia16'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia17'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia18'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia19'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia20'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia21'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia22'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia23'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia24'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia25'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia26'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia27'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia28'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia29'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia30'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia31'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia06'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia07'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia08'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia09'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia13'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia14'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia15'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia16'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia17'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia18'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia19'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia20'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia21'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia22'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia23'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia24'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia25'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia26'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia27'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia28'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia29'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia30'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia31'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsr_acumulado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_exportou'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_num_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dias_uteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_comvenc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semvenc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 141
    Top = 552
  end
  object dspInc_Cadastro_Frequencia: TDataSetProvider
    DataSet = spInc_Cadastro_Frequencia
    Constraints = True
    Left = 109
    Top = 552
  end
  object spExc_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 749
    Top = 528
  end
  object dspExc_Cadastro_Frequencia: TDataSetProvider
    DataSet = spExc_Cadastro_Frequencia
    Constraints = True
    Left = 717
    Top = 528
  end
  object spSel_Cadastro: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1168
    Top = 233
  end
  object dspSel_Cadastro: TDataSetProvider
    DataSet = spSel_Cadastro
    Constraints = True
    Left = 1206
    Top = 235
  end
  object spSel_Cadastro_Turmas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Turmas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 229
    Top = 544
  end
  object dspSel_Cadastro_Turmas: TDataSetProvider
    DataSet = spSel_Cadastro_Turmas
    Constraints = True
    Left = 189
    Top = 552
  end
  object spInc_Cadastro_Turmas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Turmas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_professor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 301
    Top = 552
  end
  object dspInc_Cadastro_Turmas: TDataSetProvider
    DataSet = spInc_Cadastro_Turmas
    Constraints = True
    Left = 269
    Top = 552
  end
  object spAlt_Cadastro_Turmas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Turmas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_professor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 552
  end
  object dspAlt_Cadastro_Turmas: TDataSetProvider
    DataSet = spAlt_Cadastro_Turmas
    Constraints = True
    Left = 340
    Top = 552
  end
  object dspSel_Idades2: TDataSetProvider
    DataSet = spSel_Idades2
    Constraints = True
    Left = 464
    Top = 693
  end
  object spSel_Idades2: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Idades2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 693
  end
  object spSel_Biometricos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Biometricos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 740
    Top = 328
  end
  object dspSel_Biometricos: TDataSetProvider
    DataSet = spSel_Biometricos
    Constraints = True
    Left = 708
    Top = 328
  end
  object spInc_Biometricos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Biometricos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_imc'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao_imc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_flg_problema_saude'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_problema_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 516
    Top = 449
  end
  object dspInc_Biometricos: TDataSetProvider
    DataSet = spInc_Biometricos
    Constraints = True
    Left = 484
    Top = 449
  end
  object spAlt_Cadastro_Dados_Biometricos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Dados_Biometricos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end>
    Left = 571
    Top = 392
  end
  object dspAlt_Cadastro_Dados_Biometricos: TDataSetProvider
    DataSet = spAlt_Cadastro_Dados_Biometricos
    Constraints = True
    Left = 539
    Top = 392
  end
  object spSel_Frequencia_Ano: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Frequencia_Ano;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 253
    Top = 65528
  end
  object spSel_Frequencia_Mes: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Frequencia_Mes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 32
  end
  object dspSel_Frequencia_Ano: TDataSetProvider
    DataSet = spSel_Frequencia_Ano
    Constraints = True
    Left = 221
    Top = 65528
  end
  object dspSel_Frequencia_Mes: TDataSetProvider
    DataSet = spSel_Frequencia_Mes
    Constraints = True
    Left = 189
    Top = 32
  end
  object spSel_Mensagem: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Mensagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_para'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 313
    Top = 32
  end
  object dspSel_Mensagem: TDataSetProvider
    DataSet = spSel_Mensagem
    Constraints = True
    Left = 281
    Top = 32
  end
  object spInc_Mensagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Mensagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_mensagem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_para'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 396
    Top = 32
  end
  object dspInc_Mensagem: TDataSetProvider
    DataSet = spInc_Mensagem
    Constraints = True
    Left = 364
    Top = 32
  end
  object spSel_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 484
    Top = 32
  end
  object spInc_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_img_foto'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 572
    Top = 32
  end
  object spExc_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 652
    Top = 32
  end
  object dspSel_Cadastro_Foto: TDataSetProvider
    DataSet = spSel_Cadastro_Foto
    Constraints = True
    Left = 452
    Top = 32
  end
  object dspInc_Cadastro_Foto: TDataSetProvider
    DataSet = spInc_Cadastro_Foto
    Constraints = True
    Left = 540
    Top = 32
  end
  object dspExc_Cadastro_Foto: TDataSetProvider
    DataSet = spExc_Cadastro_Foto
    Constraints = True
    Left = 620
    Top = 32
  end
  object spAlt_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_img_foto'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 740
    Top = 32
  end
  object dspAlt_Cadastro_Foto: TDataSetProvider
    DataSet = spAlt_Cadastro_Foto
    Constraints = True
    Left = 708
    Top = 32
  end
  object dspSel_Acompanhamento: TDataSetProvider
    DataSet = spSel_Acompanhamento
    Constraints = True
    Left = 516
    Top = 545
  end
  object spSel_Acompanhamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 545
  end
  object spExc_Acompanhamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompanhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 410
    Top = 32
  end
  object dspExc_Acompanhamento: TDataSetProvider
    DataSet = spExc_Acompanhamento
    Constraints = True
    Left = 378
    Top = 33
  end
  object dspInc_Acompanhamento: TDataSetProvider
    DataSet = spInc_Acompanhamento
    Constraints = True
    Left = 715
    Top = 432
  end
  object spInc_Acompanhamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_dias_letivos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 747
    Top = 432
  end
  object spAlt_Status_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Status_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 343
    Top = 702
  end
  object dspAlt_Status_Usuario: TDataSetProvider
    DataSet = spAlt_Status_Usuario
    Constraints = True
    Left = 311
    Top = 702
  end
  object spAlt_Acompanhamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dias_letivos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompanhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 652
    Top = 432
  end
  object dspAlt_Acompanhamento: TDataSetProvider
    DataSet = spAlt_Acompanhamento
    Constraints = True
    Left = 644
    Top = 464
  end
  object dspSel_EscolaAcompanhamento: TDataSetProvider
    DataSet = spSel_EscolaAcompanhamento
    Constraints = True
    Left = 383
    Top = 593
  end
  object spSel_EscolaAcompanhamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_EscolaAcompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 415
    Top = 593
  end
  object spSel_Empresa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 70
    Top = 600
  end
  object spAlt_Empresa: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_empresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_contato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_fone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_as_social'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 141
    Top = 600
  end
  object spInc_Empresa: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_empresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_contato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_fone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_as_social'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 221
    Top = 600
  end
  object dspSel_Empresa: TDataSetProvider
    DataSet = spSel_Empresa
    Constraints = True
    Left = 38
    Top = 600
  end
  object dspAlt_Empresa: TDataSetProvider
    DataSet = spAlt_Empresa
    Constraints = True
    Left = 110
    Top = 600
  end
  object dspInc_Empresa: TDataSetProvider
    DataSet = spInc_Empresa
    Constraints = True
    Left = 190
    Top = 600
  end
  object spSel_Total_Adolescentes_Empresa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Adolescentes_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 301
    Top = 600
  end
  object dspSel_Total_Adolescentes_Empresa: TDataSetProvider
    DataSet = spSel_Total_Adolescentes_Empresa
    Constraints = True
    Left = 269
    Top = 600
  end
  object spSel_Ficha_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Ficha_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 372
    Top = 600
  end
  object dspSel_Ficha_Cadastro: TDataSetProvider
    DataSet = spSel_Ficha_Cadastro
    Constraints = True
    Left = 340
    Top = 600
  end
  object spSel_Suspensos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Suspensos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 416
    Top = 545
  end
  object dspSel_Suspensos: TDataSetProvider
    DataSet = spSel_Suspensos
    Constraints = True
    Left = 384
    Top = 545
  end
  object dspSel_Cadastro_NovaMatricula777: TDataSetProvider
    DataSet = spSel_Cadastro_NovaMatricula777
    Constraints = True
    Left = 181
    Top = 456
  end
  object spSel_Cadastro_NovaMatricula777: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_NovaMatricula777;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 213
    Top = 456
  end
  object spSel_Versao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_VERSAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 745
    Top = 391
  end
  object dspSel_Versao: TDataSetProvider
    DataSet = spSel_Versao
    Constraints = True
    Left = 713
    Top = 391
  end
  object spSel_Transferencias_para_DRH: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Transferencias_DRH;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_tipo_folha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 484
    Top = 640
  end
  object dspSel_Transferencias_para_DRH: TDataSetProvider
    DataSet = spSel_Transferencias_para_DRH
    Constraints = True
    Left = 516
    Top = 640
  end
  object spSel_FichaCadastro: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_FichaCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 109
    Top = 682
  end
  object dspSel_FichaCadastro: TDataSetProvider
    DataSet = spSel_FichaCadastro
    Constraints = True
    Left = 144
    Top = 682
  end
  object spSel_Secao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Secao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_secao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 694
  end
  object dspSel_Secao: TDataSetProvider
    DataSet = spSel_Secao
    Constraints = True
    Left = 38
    Top = 694
  end
  object dspInc_Unidade: TDataSetProvider
    DataSet = spInc_Unidade
    Constraints = True
    Left = 37
    Top = 648
  end
  object spInc_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_gestor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_telefone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_telefone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_capacidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_divisao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_gestao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 648
  end
  object spAlt_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_gestor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_telefone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_telefone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_capacidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data_altera'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_divisao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_gestao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 141
    Top = 648
  end
  object dspAlt_Unidade: TDataSetProvider
    DataSet = spAlt_Unidade
    Constraints = True
    Left = 109
    Top = 648
  end
  object spInc_Secao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Secao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 707
    Top = 452
  end
  object dspInc_Secao: TDataSetProvider
    DataSet = spInc_Secao
    Constraints = True
    Left = 549
    Top = 465
  end
  object dspAlt_Secao: TDataSetProvider
    DataSet = spAlt_Secao
    Constraints = True
    Left = 189
    Top = 648
  end
  object spAlt_Secao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Secao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_secao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 223
    Top = 648
  end
  object dspSel_Psicopedagogico: TDataSetProvider
    DataSet = spSel_Psicopedagogico
    Constraints = True
    Left = 261
    Top = 329
  end
  object dspInc_Psicopedagogico: TDataSetProvider
    DataSet = spInc_Psicopedagogico
    Constraints = True
    Left = 340
    Top = 648
  end
  object spSel_Psicopedagogico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Psicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 301
    Top = 648
  end
  object spInc_Psicopedagogico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Psicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_oficina'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_professor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_professor_resp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_profissional_resp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 648
  end
  object spSel_ItensPsicopedagogico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ItensPsicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_psico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 444
    Top = 545
  end
  object spInc_ItensPsicopedagogico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ItensPsicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_psico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 444
    Top = 593
  end
  object dspSel_ItensPsicopedagogico: TDataSetProvider
    DataSet = spSel_ItensPsicopedagogico
    Constraints = True
    Left = 412
    Top = 545
  end
  object dspInc_ItensPsicopedagogico: TDataSetProvider
    DataSet = spInc_ItensPsicopedagogico
    Constraints = True
    Left = 412
    Top = 593
  end
  object spSel_ObsAprendizagem: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ObsAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_observacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 225
    Top = 696
  end
  object spInc_ObsAprendizagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ObsAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_aspectosgerais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_expressaoral'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_matematica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_especifica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 302
    Top = 696
  end
  object dspSel_ObsAprendizagem: TDataSetProvider
    DataSet = spSel_ObsAprendizagem
    Constraints = True
    Left = 189
    Top = 696
  end
  object dspInc_ObsAprendizagem: TDataSetProvider
    DataSet = spInc_ObsAprendizagem
    Constraints = True
    Left = 269
    Top = 696
  end
  object spSel_Perfil: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Perfil;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 681
  end
  object dspSel_Perfil: TDataSetProvider
    DataSet = spSel_Perfil
    Constraints = True
    Left = 516
    Top = 681
  end
  object spTotaliza_por_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_POR_UNIDADE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_IND_TIPO_UNIDADE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_IND_TIPO_GESTAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 696
  end
  object dspSel_TotalizaPorUnidade: TDataSetProvider
    DataSet = spTotaliza_por_Unidade
    Constraints = True
    Left = 340
    Top = 696
  end
  object spInc_Perfil: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Perfil;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_perfil'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_funcoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 483
    Top = 593
  end
  object dspInc_Perfil: TDataSetProvider
    DataSet = spInc_Perfil
    Constraints = True
    Left = 516
    Top = 593
  end
  object dspSel_CadastroAMB: TDataSetProvider
    DataSet = spSel_CadastroAMB
    Constraints = True
    Left = 37
    Top = 742
  end
  object spSel_CadastroAMB: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CadastroAMB;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 742
  end
  object spAlt_ObsAprendizagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ObsAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_observacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_aspectos_gerais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_expressaoral'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_matematica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_especifica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 419
    Top = 681
  end
  object dspAlt_ObsAprendizagem: TDataSetProvider
    DataSet = spAlt_ObsAprendizagem
    Constraints = True
    Left = 387
    Top = 681
  end
  object dspDesabilitaTriggers_Cadastro: TDataSetProvider
    DataSet = spDesabilitaTriggers_Cadastro
    Constraints = True
    Left = 109
    Top = 742
  end
  object dspHabilitaTriggers_Cadastro: TDataSetProvider
    DataSet = spHabilitaTriggers_Cadastro
    Constraints = True
    Left = 189
    Top = 743
  end
  object spDesabilitaTriggers_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_DesabilitaTriggers_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 142
    Top = 742
  end
  object spHabilitaTriggers_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_HabilitaTriggers_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 224
    Top = 744
  end
  object spDesabilitaTriggers_Responsavel: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_DesabilitaTriggers_Responsavel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 301
    Top = 744
  end
  object dspDesabilitaTriggers_Responsavel: TDataSetProvider
    DataSet = spDesabilitaTriggers_Responsavel
    Constraints = True
    Left = 269
    Top = 744
  end
  object dspHabilitaTriggers_Responsavel: TDataSetProvider
    DataSet = spHabilitaTriggers_Responsavel
    Constraints = True
    Left = 340
    Top = 641
  end
  object spHabilitaTriggers_Responsavel: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_HabilitaTriggers_Responsavel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 373
    Top = 641
  end
  object spSel_Fatores_Intervencao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fatores_Intervencao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_fator'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 444
    Top = 641
  end
  object dspSel_Fatores_Intervencao: TDataSetProvider
    DataSet = spSel_Fatores_Intervencao
    Constraints = True
    Left = 412
    Top = 641
  end
  object spInc_Fatores_Intervencao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Fatores_Intervencao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_fator'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 444
    Top = 681
  end
  object dspInc_Fatores_Intervencao: TDataSetProvider
    DataSet = spInc_Fatores_Intervencao
    Constraints = True
    Left = 412
    Top = 681
  end
  object spAlt_ItensPsicopedagogico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ItensPsicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data_altera'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 72
    Top = 784
  end
  object dspAlt_ItensPsicopedagogico: TDataSetProvider
    DataSet = spAlt_ItensPsicopedagogico
    Constraints = True
    Left = 38
    Top = 784
  end
  object spSel_HistoricoEscola: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_HistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 225
    Top = 784
  end
  object spInc_HistoricoEscola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_HistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 301
    Top = 784
  end
  object spAlt_HistoricoEscola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_HistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_log_data_altera'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 681
  end
  object dspSel_HistoricoEscola: TDataSetProvider
    DataSet = spSel_HistoricoEscola
    Constraints = True
    Left = 189
    Top = 784
  end
  object dspInc_HistoricoEscola: TDataSetProvider
    DataSet = spInc_HistoricoEscola
    Constraints = True
    Left = 269
    Top = 784
  end
  object dspAlt_HistoricoEscola: TDataSetProvider
    DataSet = spAlt_HistoricoEscola
    Constraints = True
    Left = 340
    Top = 681
  end
  object spSel_ProgSocial: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_progsocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 444
    Top = 497
  end
  object dspSel_ProgSocial: TDataSetProvider
    DataSet = spSel_ProgSocial
    Constraints = True
    Left = 412
    Top = 497
  end
  object dspInc_ProgSocial: TDataSetProvider
    DataSet = spInc_ProgSocial
    Constraints = True
    Left = 383
    Top = 641
  end
  object spInc_ProgSocial: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_progsocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_origem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 415
    Top = 641
  end
  object spSel_Calendario: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Calendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_calendario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 744
  end
  object dspSel_Calendario: TDataSetProvider
    DataSet = spSel_Calendario
    Constraints = True
    Left = 340
    Top = 744
  end
  object spInc_Calendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Calendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_calendario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 750
    Top = 624
  end
  object dspInc_Calendario: TDataSetProvider
    DataSet = spInc_Calendario
    Constraints = True
    Left = 718
    Top = 624
  end
  object spAlt_Calendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Calendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 748
    Top = 576
  end
  object dspAlt_Calendario: TDataSetProvider
    DataSet = spAlt_Calendario
    Constraints = True
    Left = 716
    Top = 576
  end
  object spSel_ItensCalendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ItensCalendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_feriado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 581
    Top = 392
  end
  object dspSel_ItensCalendario: TDataSetProvider
    DataSet = spSel_ItensCalendario
    Constraints = True
    Left = 548
    Top = 392
  end
  object spInc_ItensCalendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ItensCalendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_feriado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 452
    Top = 640
  end
  object dspInc_ItensCalendario: TDataSetProvider
    DataSet = spInc_ItensCalendario
    Constraints = True
    Left = 420
    Top = 640
  end
  object spContaFeriados: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ContaFeriado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 660
    Top = 544
  end
  object dspConta_Feriados: TDataSetProvider
    DataSet = spContaFeriados
    Constraints = True
    Left = 628
    Top = 544
  end
  object spAlt_Secao_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Secao_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 588
    Top = 544
  end
  object dspAlt_Secao_Cadastro: TDataSetProvider
    DataSet = spAlt_Secao_Cadastro
    Constraints = True
    Left = 556
    Top = 544
  end
  object dspAlt_UnidadesNutricao: TDataSetProvider
    DataSet = spAlt_UnidadesNutricao
    Constraints = True
    Left = 716
    Top = 480
  end
  object spAlt_UnidadesNutricao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_UnidadesNutricao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_gestao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 748
    Top = 480
  end
  object dspInc_Lanc_Fat_Int: TDataSetProvider
    DataSet = spInc_Lanc_Fat_Int
    Constraints = True
    Left = 39
    Top = 835
  end
  object spInc_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_LANC_FAT_INT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_vinculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_entrevista'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_meta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 71
    Top = 835
  end
  object spAlt_Status_ProgSocial: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Status_ProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_cad_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_desligamento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_insercao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 141
    Top = 784
  end
  object dspAlt_Status_ProgSocial: TDataSetProvider
    DataSet = spAlt_Status_ProgSocial
    Constraints = True
    Left = 109
    Top = 784
  end
  object dspSel_Classificacao_Atendimento: TDataSetProvider
    DataSet = spSel_Classificacao_Atendimento
    Constraints = True
    Left = 716
    Top = 688
  end
  object spSel_Classificacao_Atendimento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Classificacao_Atendimento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_crianca'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_flg_adolescente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_flg_familia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_flg_profissional'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 748
    Top = 688
  end
  object dspInc_Indicador: TDataSetProvider
    DataSet = spInc_Indicador
    Constraints = True
    Left = 340
    Top = 784
  end
  object spInc_Indicador: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Indicador;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 372
    Top = 784
  end
  object spTotaliza_Atendimento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_ATENDIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_indicador'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_registro2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 436
    Top = 744
  end
  object dspTotaliza_Atendimento: TDataSetProvider
    DataSet = spTotaliza_Atendimento
    Constraints = True
    Left = 406
    Top = 744
  end
  object dspSel_Lanc_Fat_Int: TDataSetProvider
    DataSet = spSel_Lancamento_Fatores_Intervencao
    Constraints = True
    Left = 109
    Top = 835
  end
  object spSel_Lancamento_Fatores_Intervencao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Lancamentos_Fatores_Intervencao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_entrevista'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 143
    Top = 835
  end
  object spInc_Itens_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Itens_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_situacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 224
    Top = 835
  end
  object dspInc_Itens_Lanc_Fat_Int: TDataSetProvider
    DataSet = spInc_Itens_Lanc_Fat_Int
    Constraints = True
    Left = 189
    Top = 835
  end
  object spSel_Itens_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Itens_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_registro1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_registro2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 300
    Top = 835
  end
  object dspSel_Itens_Lanc_Fat_Int: TDataSetProvider
    DataSet = spSel_Itens_Lanc_Fat_Int
    Constraints = True
    Left = 269
    Top = 835
  end
  object dspSel_Motivos_Possiveis: TDataSetProvider
    DataSet = spSel_Motivos_Possiveis
    Constraints = True
    Left = 340
    Top = 835
  end
  object spSel_Motivos_Possiveis: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MotivosPossiveis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 835
  end
  object spSel_MotivosPossiveis: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MotivosPossiveis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 372
    Top = 835
  end
  object dspSel_Consulta: TDataSetProvider
    DataSet = spSel_Consulta
    Constraints = True
    Left = 404
    Top = 784
  end
  object spSel_Consulta: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_consulta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_hor_consulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_flg_status_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 436
    Top = 784
  end
  object spInc_Consulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_consulta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_hor_consulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_flg_status_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 436
    Top = 832
  end
  object dspInc_Consulta: TDataSetProvider
    DataSet = spInc_Consulta
    Constraints = True
    Left = 404
    Top = 832
  end
  object spAlt_Consulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_anotacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 508
    Top = 744
  end
  object dspAlt_Consulta: TDataSetProvider
    DataSet = spAlt_Consulta
    Constraints = True
    Left = 476
    Top = 744
  end
  object spAlt_ConsultaStatus: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ConsultaStatus;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 508
    Top = 784
  end
  object dspAlt_ConsultaStatus: TDataSetProvider
    DataSet = spAlt_ConsultaStatus
    Constraints = True
    Left = 476
    Top = 784
  end
  object spSel_TratamentoOdontologico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TratamentoOdontologico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 580
    Top = 744
  end
  object dspSel_TratamentoOdontologico: TDataSetProvider
    DataSet = spSel_TratamentoOdontologico
    Constraints = True
    Left = 548
    Top = 744
  end
  object spSel_TratamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TratamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratcons'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 40
    Top = 741
  end
  object dspSel_TratamentosConsultas: TDataSetProvider
    DataSet = spSel_TratamentosConsultas
    Constraints = True
    Left = 73
    Top = 742
  end
  object spInc_TratamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_TratamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_dente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 580
    Top = 824
  end
  object dspInc_TratamentosConsultas: TDataSetProvider
    DataSet = spInc_TratamentosConsultas
    Constraints = True
    Left = 548
    Top = 824
  end
  object spSel_Encaminhamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Encaminhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_encaminhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 744
  end
  object dspSel_Encaminhamento: TDataSetProvider
    DataSet = spSel_Encaminhamento
    Constraints = True
    Left = 620
    Top = 744
  end
  object spSel_EncaminhamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_EncaminhamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_enccons'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_encaminhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 784
  end
  object dspSel_EncaminhamentosConsultas: TDataSetProvider
    DataSet = spSel_EncaminhamentosConsultas
    Constraints = True
    Left = 620
    Top = 784
  end
  object spInc_EncaminhamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_EncaminhamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_encaminhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 824
  end
  object dspInc_EncaminhamentosConsultas: TDataSetProvider
    DataSet = spInc_EncaminhamentosConsultas
    Constraints = True
    Left = 722
    Top = 508
  end
  object spSel_Dente: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Dente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo_dente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_tipo_arcada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_tipo_hemiarcada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 724
    Top = 744
  end
  object dspSel_Dente: TDataSetProvider
    DataSet = spSel_Dente
    Constraints = True
    Left = 692
    Top = 744
  end
  object spAlt_Itens_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_Itens_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 588
    Top = 592
  end
  object dspAlt_Itens_Lanc_Fat_Int: TDataSetProvider
    DataSet = spAlt_Itens_Lanc_Fat_Int
    Constraints = True
    Left = 556
    Top = 592
  end
  object spAlt_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 588
    Top = 640
  end
  object dspAlt_Lanc_Fat_Int: TDataSetProvider
    DataSet = spAlt_Lanc_Fat_Int
    Constraints = True
    Left = 556
    Top = 640
  end
  object spSel_ProcuraIMC: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ProcuraIMC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_indicebiometrico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_num_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_valorIMC'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end>
    Left = 724
    Top = 784
  end
  object dspSel_ProcuraIMC: TDataSetProvider
    DataSet = spSel_ProcuraIMC
    Constraints = True
    Left = 692
    Top = 784
  end
  object spSel_CadastroParaBiometrico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CadastroParaBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 688
  end
  object dspSel_CadastroParaBiometrico: TDataSetProvider
    DataSet = spSel_CadastroParaBiometrico
    Constraints = True
    Left = 620
    Top = 688
  end
  object spSel_UltimoLancamentoBiometrico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_UltimoLancamentoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 652
    Top = 640
  end
  object dspSel_UltimoLancamentoBiometrico: TDataSetProvider
    DataSet = spSel_UltimoLancamentoBiometrico
    Constraints = True
    Left = 620
    Top = 640
  end
  object spInc_TempBiometrica: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_TempBiometrica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_imc'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 592
  end
  object dspInc_TempBiometrica: TDataSetProvider
    DataSet = spInc_TempBiometrica
    Constraints = True
    Left = 620
    Top = 592
  end
  object spExc_TempBiometrica: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_EXC_TempBiometrica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 588
    Top = 688
  end
  object dspExc_TempBiometrica: TDataSetProvider
    DataSet = spExc_TempBiometrica
    Constraints = True
    Left = 556
    Top = 688
  end
  object spSel_TMPBiometricos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TMPBIOMETRICOS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 724
    Top = 824
  end
  object dspSel_TMPBiometricos: TDataSetProvider
    DataSet = spSel_TMPBiometricos
    Constraints = True
    Left = 692
    Top = 824
  end
  object spAlt_Biometrico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_BIOMETRICO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 796
    Top = 744
  end
  object dspAlt_Biometrico: TDataSetProvider
    DataSet = spAlt_Biometrico
    Constraints = True
    Left = 764
    Top = 744
  end
  object spSel_GraficoBiometrico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_GraficoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 71
    Top = 886
  end
  object dspSel_GraficoBiometrico: TDataSetProvider
    DataSet = spSel_GraficoBiometrico
    Constraints = True
    Left = 39
    Top = 887
  end
  object spInc_GraficoBiometrico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_GraficoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_num_total'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 143
    Top = 886
  end
  object dspInc_GraficoBiometrico: TDataSetProvider
    DataSet = spInc_GraficoBiometrico
    Constraints = True
    Left = 109
    Top = 886
  end
  object spExc_DadosGraficoBiometrico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_DadosGraficoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 224
    Top = 887
  end
  object dspExc_DadosGraficoBiometrico: TDataSetProvider
    DataSet = spExc_DadosGraficoBiometrico
    Constraints = True
    Left = 189
    Top = 886
  end
  object spTotalizaAvaliacoesBiometricas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_AVALIACOES_BIOMETRICAS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 302
    Top = 887
  end
  object dspSelTotalizaAvaliacoesBiometricas: TDataSetProvider
    DataSet = spTotalizaAvaliacoesBiometricas
    Constraints = True
    Left = 270
    Top = 886
  end
  object spAlt_EnderecoFamilia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_EnderecoFamilia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end>
    Left = 374
    Top = 887
  end
  object dspAlt_EnderecoFamilia: TDataSetProvider
    DataSet = spAlt_EnderecoFamilia
    Constraints = True
    Left = 342
    Top = 886
  end
  object spSel_AvaliacaoApr: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_AVALIACAOAPR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dat_av1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_av2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_lanc1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_lanc2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 394
    Top = 700
  end
  object dspSel_AvaliacoesApr: TDataSetProvider
    DataSet = spSel_AvaliacaoApr
    Constraints = True
    Left = 406
    Top = 886
  end
  object spInc_AvaliacaoApr: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_AvaliacaoApr;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dat_inicio_pratica'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item06'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item07'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item08'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item09'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_visto_adol'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 560
    Top = 484
  end
  object dspInc_AvaliacaoApr: TDataSetProvider
    DataSet = spInc_AvaliacaoApr
    Constraints = True
    Left = 428
    Top = 699
  end
  object spSel_Distinto_Asocial_em_Secao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_DISTINTO_ASOCIAL_EM_SECAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 253
    Top = 592
  end
  object dspSel_Distinto_ASocial_em_Secao: TDataSetProvider
    DataSet = spSel_Distinto_Asocial_em_Secao
    Constraints = True
    Left = 221
    Top = 592
  end
  object spSEL_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_uso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 517
    Top = 36
  end
  object spINC_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_medicamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_uso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 85
    Top = 117
  end
  object spALT_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_medicamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 381
    Top = 68
  end
  object dspSEL_Medicamento: TDataSetProvider
    DataSet = spSEL_Medicamento
    Constraints = True
    Left = 485
    Top = 36
  end
  object dspINC_Medicamento: TDataSetProvider
    DataSet = spINC_Medicamento
    Constraints = True
    Left = 117
    Top = 117
  end
  object dspALT_Medicamento: TDataSetProvider
    DataSet = spALT_Medicamento
    Constraints = True
    Left = 349
    Top = 68
  end
  object spSel_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 501
    Top = 190
  end
  object spINC_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 277
    Top = 218
  end
  object dspSel_Receituario: TDataSetProvider
    DataSet = spSel_Receituario
    Constraints = True
    Left = 519
    Top = 208
  end
  object dspInc_Receituario: TDataSetProvider
    DataSet = spINC_Receituario
    Constraints = True
    Left = 245
    Top = 216
  end
  object SP_Ultimo_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Ultimo_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 77
    Top = 193
  end
  object dspUltimo_Receituario: TDataSetProvider
    DataSet = SP_Ultimo_Receituario
    Constraints = True
    Left = 109
    Top = 192
  end
  object spINC_Item_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Inc_Item_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 661
    Top = 33
  end
  object spSel_Item_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Item_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_item_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 485
    Top = 40
  end
  object spExc_Item_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Itens_Receituarios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_item_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 181
    Top = 255
  end
  object dspSel_Item_Receituario: TDataSetProvider
    DataSet = spSel_Item_Receituario
    Constraints = True
    Left = 517
    Top = 40
  end
  object dspInc_Item_Receituario: TDataSetProvider
    DataSet = spINC_Item_Receituario
    Constraints = True
    Left = 693
    Top = 176
  end
  object dspExc_Item_Receituario: TDataSetProvider
    DataSet = spExc_Item_Receituario
    Constraints = True
    Left = 237
    Top = 247
  end
  object spAlt_EncerraReceituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_EncerraReceituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 293
    Top = 65528
  end
  object dspAlt_EncerraReceituario: TDataSetProvider
    DataSet = spAlt_EncerraReceituario
    Constraints = True
    Left = 325
    Top = 65528
  end
  object spSel_MontaReceituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_MONTA_RECEITUARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 110
    Top = 743
  end
  object dspSel_MontaReceituario: TDataSetProvider
    DataSet = spSel_MontaReceituario
    Constraints = True
    Left = 147
    Top = 743
  end
  object spAlt_Desativa_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Desativa_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 621
    Top = 61
  end
  object dspAlt_Desativa_Medicamento: TDataSetProvider
    DataSet = spAlt_Desativa_Medicamento
    Constraints = True
    Left = 653
    Top = 77
  end
  object spExc_TratamentosConsultas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_TratamentoConsulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_ID_TRATCONS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 557
    Top = 746
  end
  object dspExc_TratamentoConsulta: TDataSetProvider
    DataSet = spExc_TratamentosConsultas
    Constraints = True
    Left = 661
    Top = 746
  end
  object dspExc_EncaminhamentoConsulta: TDataSetProvider
    DataSet = spEsc_EncaminhamentoConsulta
    Constraints = True
    Left = 77
    Top = 168
  end
  object spEsc_EncaminhamentoConsulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_EncaminhamentoConsulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_enccons'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 149
    Top = 72
  end
  object spExc_RegistroCadProgSocial: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_RegistroCadProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 613
    Top = 792
  end
  object dspExc_RegistroCadProgSocial: TDataSetProvider
    DataSet = spExc_RegistroCadProgSocial
    Constraints = True
    Left = 661
    Top = 792
  end
  object spAlt_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_nom_diretor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_publica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 493
    Top = 713
  end
  object dspAlt_Escola: TDataSetProvider
    DataSet = spAlt_Escola
    Constraints = True
    Left = 453
    Top = 712
  end
  object spInc_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_nom_diretor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_publica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 157
    Top = 104
  end
  object dspInc_Escola: TDataSetProvider
    DataSet = spInc_Escola
    Constraints = True
    Left = 197
    Top = 104
  end
  object spSel_IndicadoresProfissionais: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_sel_indicadores_profissionais;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 677
    Top = 32
  end
  object dspSel_IndicadoresProfissionais: TDataSetProvider
    DataSet = spSel_IndicadoresProfissionais
    Constraints = True
    Left = 677
    Top = 72
  end
  object spSel_Alunos_Turma: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Seleciona_Alunos_Turma;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 677
    Top = 32
  end
  object dspSel_Seleciona_Alunos_Turma: TDataSetProvider
    DataSet = spSel_Alunos_Turma
    Constraints = True
    Left = 677
    Top = 80
  end
  object spAlt_Turma_PosTransferencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Alt_Turma_PosTransferencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 669
    Top = 32
  end
  object dspAlt_Turma_PosTransferencia: TDataSetProvider
    DataSet = spAlt_Turma_PosTransferencia
    Constraints = True
    Left = 701
    Top = 32
  end
  object spSel_AdolescentesEmpresasConvenio: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Sel_AdolescentesEmpresasConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 837
    Top = 32
  end
  object dspSel_AdolescentesEmpresasConvenio: TDataSetProvider
    DataSet = spSel_AdolescentesEmpresasConvenio
    Constraints = True
    Left = 805
    Top = 32
  end
  object SP_AsSocial_EmpresasConvenio: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'sp_AsSocial_EmpresasConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 837
    Top = 80
  end
  object dsp_AsSocial_EmpresasConvenio: TDataSetProvider
    DataSet = SP_AsSocial_EmpresasConvenio
    Constraints = True
    Left = 805
    Top = 80
  end
  object spSel_Caroscopio: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Sel_Caroscopio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 787
    Top = 128
  end
  object dspSel_Caroscopio: TDataSetProvider
    DataSet = spSel_Caroscopio
    Constraints = True
    Left = 753
    Top = 128
  end
  object spSel_DadosVisitaTecnica: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Dados_Visita_Tecnica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 654
    Top = 176
  end
  object dspSel_DadosVisitaTecnica: TDataSetProvider
    DataSet = spSel_DadosVisitaTecnica
    Constraints = True
    Left = 653
    Top = 224
  end
  object spALT_AvaliacoesAprendiz: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Alt_AvaliacoesAprendiz;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_inicio_pratica'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item06'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item07'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item08'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item09'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 669
    Top = 200
  end
  object dspALT_AvaliacoesAprendiz: TDataSetProvider
    DataSet = spALT_AvaliacoesAprendiz
    Constraints = True
    Left = 669
    Top = 248
  end
  object spSel_SerieEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Sel_Serie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 32
  end
  object dspSel_SerieEscolar: TDataSetProvider
    DataSet = spSel_SerieEscolar
    Constraints = True
    Left = 917
    Top = 32
  end
  object spInc_SerieEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Inc_Serie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 80
  end
  object dspInc_SerieEscolar: TDataSetProvider
    DataSet = spInc_SerieEscolar
    Constraints = True
    Left = 917
    Top = 80
  end
  object spAlt_SerieEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Alt_SerieEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 128
  end
  object dspAlt_SerieEscolar: TDataSetProvider
    DataSet = spAlt_SerieEscolar
    Constraints = True
    Left = 917
    Top = 128
  end
  object spSel_TotalRegistrosUsuarioUnidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SelTotalRegistrosUsuarioUnidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 877
    Top = 144
  end
  object dspSel_TotalRegistrosUsuarioUnidade: TDataSetProvider
    DataSet = spSel_TotalRegistrosUsuarioUnidade
    Constraints = True
    Left = 917
    Top = 144
  end
  object spSel_TotalRegistrosUnidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TotalRegistrosUnidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 877
    Top = 184
  end
  object dspSel_TotalRegistrosUnidade: TDataSetProvider
    DataSet = spSel_TotalRegistrosUnidade
    Constraints = True
    Left = 917
    Top = 184
  end
  object spSel_TransferenciasDAPA: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TransferenciasDAPA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 227
  end
  object dspSel_TransferenciasDAPA: TDataSetProvider
    DataSet = spSel_TransferenciasDAPA
    Constraints = True
    Left = 917
    Top = 227
  end
  object spSel_Acessos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Acessos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acesso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_ativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_online'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 877
    Top = 269
  end
  object dspSel_Acesso: TDataSetProvider
    DataSet = spSel_Acessos
    Constraints = True
    Left = 917
    Top = 269
  end
  object spSel_EscolaSerie: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Sel_EscolaSerie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_escola_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 312
  end
  object dspSel_EscolaSerie: TDataSetProvider
    DataSet = spSel_EscolaSerie
    Constraints = True
    Left = 917
    Top = 312
  end
  object spInc_EscolaSerie: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_EscolaSerie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 356
  end
  object dspInc_EscolaSerie: TDataSetProvider
    DataSet = spInc_EscolaSerie
    Constraints = True
    Left = 917
    Top = 356
  end
  object spSel_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompescolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 877
    Top = 399
  end
  object dspSel_AcompEscolar: TDataSetProvider
    DataSet = spSel_AcompEscolar
    Constraints = True
    Left = 917
    Top = 399
  end
  object spInc_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end>
    Left = 955
    Top = 376
  end
  object dspInc_AcompEscolar: TDataSetProvider
    DataSet = spInc_AcompEscolar
    Constraints = True
    Left = 995
    Top = 377
  end
  object spAlt_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompescolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 955
    Top = 424
  end
  object dspAlt_AcompEscolar: TDataSetProvider
    DataSet = spAlt_AcompEscolar
    Constraints = True
    Left = 995
    Top = 425
  end
  object spInc_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_DefasagemEsc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_ideal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_defasada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 955
    Top = 468
  end
  object dspInc_DefasagemEscolar: TDataSetProvider
    DataSet = spInc_DefasagemEscolar
    Constraints = True
    Left = 995
    Top = 467
  end
  object spSel_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DefasagemEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_defasagemesc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 875
    Top = 423
  end
  object dspSel_DefasagemEscolar: TDataSetProvider
    DataSet = spSel_DefasagemEscolar
    Constraints = True
    Left = 915
    Top = 422
  end
  object spAlt_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_DefasagemEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_defasagemesc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_ideal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_defasada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 875
    Top = 467
  end
  object dspAlt_DefasagemEscolar: TDataSetProvider
    DataSet = spAlt_DefasagemEscolar
    Constraints = True
    Left = 915
    Top = 466
  end
  object spAlt_Cadastro_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_DefasagemEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 875
    Top = 509
  end
  object dspAlt_Cadastro_DefasagemEscolar: TDataSetProvider
    DataSet = spAlt_Cadastro_DefasagemEscolar
    Constraints = True
    Left = 915
    Top = 508
  end
  object spSel_Solicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Solicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_prioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipo_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 955
    Top = 506
  end
  object dspSel_Solicitacao: TDataSetProvider
    DataSet = spSel_Solicitacao
    Constraints = True
    Left = 995
    Top = 506
  end
  object spInc_Solicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Solicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_solicitacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipo_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_prioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_solicitacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 875
    Top = 554
  end
  object dspInc_Solicitacao: TDataSetProvider
    DataSet = spInc_Solicitacao
    Constraints = True
    Left = 915
    Top = 554
  end
  object spSel_TipoSolicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TipoSolicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipo_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_ativa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 955
    Top = 555
  end
  object dspSel_TipoSolicitacao: TDataSetProvider
    DataSet = spSel_TipoSolicitacao
    Constraints = True
    Left = 995
    Top = 555
  end
  object spAlt_Solicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Solicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_devolutiva'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 875
    Top = 604
  end
  object dspAlt_Solicitacao: TDataSetProvider
    DataSet = spAlt_Solicitacao
    Constraints = True
    Left = 915
    Top = 604
  end
  object spInc_SatisfConv: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_SatisfConv;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_avaliador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_setor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_adolescentes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_media'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end>
    Left = 548
    Top = 887
  end
  object dspInc_SatisfConv: TDataSetProvider
    DataSet = spInc_SatisfConv
    Constraints = True
    Left = 582
    Top = 886
  end
  object spSel_SatisfConv: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_SatisfConv;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_satisfconv'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_mes_inicio_avaliacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_mes_final_avaliacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_ano_avaliacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 628
    Top = 887
  end
  object dspSel_SastisfConv: TDataSetProvider
    DataSet = spSel_SatisfConv
    Constraints = True
    Left = 662
    Top = 886
  end
  object spExc_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompescolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 605
    Top = 464
  end
  object dspExc_AcompEscolar: TDataSetProvider
    DataSet = spExc_AcompEscolar
    Constraints = True
    Left = 609
    Top = 384
  end
  object spSel_PesquisaAcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_PesquisaAcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 571
    Top = 402
  end
  object dspSel_PesquisaAcompEscolar: TDataSetProvider
    DataSet = spSel_PesquisaAcompEscolar
    Constraints = True
    Left = 605
    Top = 400
  end
  object spSel_ResumoMensal: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ResumoMensal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_ID_RESUMOMENSAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_UNIDADE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_ANO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_MES'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_FLG_SITUACAO_RESUMO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 88
  end
  object dspSel_ResumoMensal: TDataSetProvider
    DataSet = spSel_ResumoMensal
    Constraints = True
    Left = 1037
    Top = 88
  end
  object dspAlt_DadosUltimaEscolaHistoricoEscola: TDataSetProvider
    DataSet = spAlt_DadosUltimaEscolaHistoricoEscola
    Constraints = True
    Left = 796
    Top = 648
  end
  object spAlt_DadosUltimaEscolaHistoricoEscola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_DadosUltimaEscolaHistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 756
    Top = 648
  end
  object spSel_AlunosSemAcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AlunosSemAcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 625
    Top = 604
  end
  object dspSel_AlunosSemAcompEscolar: TDataSetProvider
    DataSet = spSel_AlunosSemAcompEscolar
    Constraints = True
    Left = 669
    Top = 600
  end
  object spSEL_Boletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Boletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_defasagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 869
    Top = 664
  end
  object dspSel_Boletim: TDataSetProvider
    DataSet = spSEL_Boletim
    Constraints = True
    Left = 909
    Top = 664
  end
  object dspInc_Boletim: TDataSetProvider
    DataSet = spInc_Boletim
    Constraints = True
    Left = 909
    Top = 760
  end
  object dspExc_Boletim: TDataSetProvider
    DataSet = spExc_Boletim
    Constraints = True
    Left = 909
    Top = 712
  end
  object spInc_Boletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Boletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_num_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_freq_1b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_freq_2b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_freq_3b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_aprov_1b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_aprov_2b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_aprov_3b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_sit_1b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_dsc_sit_2b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_dsc_sit_3b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_per_1b'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_per_2b'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_per_3b'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 869
    Top = 760
  end
  object spExc_Boletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Boletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 869
    Top = 712
  end
  object spInc_ResumoMensal: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ResumoMensal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_ae_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_ae_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_aprendiz_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_aprendiz_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_calc_freq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_matriculados_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_fora_ensino_regular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_freq_dentro_esperado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_final_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_dias_atividades'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_presencas_esperadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_geral_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_injustificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_apedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_abandono'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_maioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_lei'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_outros'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_grau_aprov_programa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_disciplinar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_socioeconomica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_violencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_abrigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_interacao_relacional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ato_infracional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_saude'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_guarda_irregular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_gravidez'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_moradia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_trab_infantil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_inscritos_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aptos_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_encaminhados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_inseridos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_familias'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_convidados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_familia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_empresas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_ult_alteracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao_resumo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_desemp_pratica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_descricao_acoes_familia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_efetivo_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_pesq_satisf_ccaadol_resp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_pesq_satisf_familia_resp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_Pesq_satisf_convenio_resp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprov_escolar_dentroesp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_adol_realizaram_prova'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_EmAprendizagemFUNDHAS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_EmAprendizagemExterna'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprendizagem_dentroesp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_aprov_dapa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_familias_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_empresas_parceiras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_calc_freq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 32
  end
  object dspInc_ResumoMensal: TDataSetProvider
    DataSet = spInc_ResumoMensal
    Constraints = True
    Left = 1037
    Top = 32
  end
  object spAlt_ResumoMensal: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ResumoMensal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_resumomensal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_ae_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_ae_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_aprendiz_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_aprendiz_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_calc_freq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_matriculados_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_fora_ensino_regular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_freq_dentro_esperado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_final_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_dias_atividades'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_presencas_esperadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_geral_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_injustificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_apedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_abandono'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_maioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_lei'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_outros'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_grau_aprov_programa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_disciplinar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_socioeconomica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_violencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_abrigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_interacao_relacional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ato_infracional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_saude'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_guarda_irregular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_gravidez'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_moradia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_trab_infantil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_inscritos_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aptos_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_encaminhados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_inseridos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_familias'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_convidados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_familia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_empresas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_ult_alteracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_aprov_programa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_desemp_pratica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_descricao_acoes_familia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_efetivo_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_pesq_satisf_ccaadol_resp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_pesq_satisf_familia_resp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_Pesq_satisf_convenio_resp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprov_escolar_dentroesp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_adol_realizaram_prova'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_EmAprendizagemFUNDHAS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_EmAprendizagemExterna'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprendizagem_dentroesp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_aprov_dapa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_familias_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_empresas_parceiras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 136
  end
  object dspAlt_ResumoMensal: TDataSetProvider
    DataSet = spAlt_ResumoMensal
    Constraints = True
    Left = 1037
    Top = 136
  end
  object dspSel_ObtemDataServer: TDataSetProvider
    DataSet = spSel_ObtemDataServer
    Constraints = True
    Left = 1037
    Top = 184
  end
  object spSel_ObtemDataServer: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ObtemDataServer;1'
    Parameters = <>
    Left = 997
    Top = 184
  end
  object spAlt_Encerra_Reverte_Resumo: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Encerra_ou_Reverte_Resumo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_resumomensal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 228
  end
  object dspAlt_Encerra_Reverte_Resumo: TDataSetProvider
    DataSet = spAlt_Encerra_Reverte_Resumo
    Constraints = True
    Left = 1037
    Top = 227
  end
  object spSel_DiasUteis: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DiasUteis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_diasuteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 277
  end
  object dspSel_DiasUteis: TDataSetProvider
    DataSet = spSel_DiasUteis
    Constraints = True
    Left = 1037
    Top = 277
  end
  object spInc_DiasUteis: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_DiasUteis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dia_util'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 325
  end
  object dspInc_DiasUteis: TDataSetProvider
    DataSet = spInc_DiasUteis
    Constraints = True
    Left = 1037
    Top = 325
  end
  object spAlt_MudaFlgExportouFrequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_MudaFlgExportouFrequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 997
    Top = 479
  end
  object dspAlt_MudaFlgExportouFrequencia: TDataSetProvider
    DataSet = spAlt_MudaFlgExportouFrequencia
    Constraints = True
    Left = 1037
    Top = 479
  end
  object spSel_EscolasBoletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Sel_EscolasBoletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 778
    Top = 32
  end
  object dspSel_EscolasBoletim: TDataSetProvider
    DataSet = spSel_EscolasBoletim
    Constraints = True
    Left = 746
    Top = 32
  end
  object spAlt_Cadastro_Frequencia_Observacoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_CADASTRO_FREQUENCIA_OBSERVACOES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 634
    Top = 635
  end
  object dspAlt_Cadastro_Frequencia_Observacoes: TDataSetProvider
    DataSet = spAlt_Cadastro_Frequencia_Observacoes
    Constraints = True
    Left = 674
    Top = 635
  end
  object spSel_EscolaBoletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_EscolaBoletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 345
    Top = 808
  end
  object dspSel_EscolaBoletim: TDataSetProvider
    DataSet = spSel_EscolaBoletim
    Constraints = True
    Left = 310
    Top = 809
  end
  object dspSel_EstatisticasAcompEscolar: TDataSetProvider
    DataSet = spSel_EstatisticasAcompEscolar
    Constraints = True
    Left = 682
    Top = 136
  end
  object spSel_EstatisticasAcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ESTATISTICAS_ACOMPESCOLAR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 722
    Top = 136
  end
  object spInc_Acessos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Acessos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_cadastro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_ativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_online'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_senha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 685
    Top = 128
  end
  object dspInc_Acessos: TDataSetProvider
    DataSet = spInc_Acessos
    Constraints = True
    Left = 653
    Top = 128
  end
  object sp_INSUPD_RelatorioAICAObs: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_INSUPD_RelatorioAICAObs;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@nm_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@tx_obs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5000
        Value = Null
      end
      item
        Name = '@teste'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 485
    Top = 238
  end
  object dsp_INSUPD_RelatorioAICAObs: TDataSetProvider
    DataSet = sp_INSUPD_RelatorioAICAObs
    Constraints = True
    Left = 514
    Top = 238
  end
  object sp_sel_RELATORIOAicaDP: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'sp_sel_RELATORIOAicaDP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 474
    Top = 19
  end
  object dsp_sel_RELATORIOAicaDP: TDataSetProvider
    DataSet = sp_sel_RELATORIOAicaDP
    Constraints = True
    Left = 506
    Top = 19
  end
  object dsp_sel_RELATORIOAica: TDataSetProvider
    DataSet = sp_sel_RELATORIOAica
    Constraints = True
    Left = 512
    Top = 83
  end
  object sp_sel_RELATORIOAica: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_sel_RELATORIOAica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 83
  end
  object sp_sel_RELATORIOAicaobs: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_sel_RELATORIOAicaobs;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 131
  end
  object dsp_sel_RELATORIOAicaobs: TDataSetProvider
    DataSet = sp_sel_RELATORIOAicaobs
    Constraints = True
    Left = 512
    Top = 131
  end
  object sp_INSUPD_RelatorioAICA: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_INSUPD_RelatorioAICA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@cd_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nm_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@tx_criterio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@tx_opcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@bimestre1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@bimestre2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@bimestre3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@bimestre4'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@bt_travado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@tx_logusuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@teste'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 483
    Top = 187
  end
  object dspINSUPD_RelatorioAICA: TDataSetProvider
    DataSet = sp_INSUPD_RelatorioAICA
    Constraints = True
    Left = 515
    Top = 187
  end
  object dspAlt_CotasPasse: TDataSetProvider
    DataSet = spAlt_CotasPasse
    Constraints = True
    Left = 687
    Top = 376
  end
  object spAlt_CotasPasse: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_CotasPasse;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_cotas_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end>
    Left = 655
    Top = 376
  end
  object adoConnRM: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=CorporeCriancas;Data Source=SRVBACK' +
      'UP;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4' +
      '096;Workstation ID=DI04;Use Encryption for Data=False;Tag with c' +
      'olumn collation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 56
    Top = 16
  end
  object sp_SelCriancasParSisDeca: TADOStoredProc
    Connection = adoConnRM
    ProcedureName = 'spSEL_CriancasParSisDeca;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_chapa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end>
    Left = 656
    Top = 352
  end
  object dsp_SelCriancasParSisDeca: TDataSetProvider
    DataSet = sp_SelCriancasParSisDeca
    Constraints = True
    Left = 688
    Top = 352
  end
  object spSel_HistoricoEnderecos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_HistoricoEndereco;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 656
    Top = 384
  end
  object dspSel_HistoricoEnderecos: TDataSetProvider
    DataSet = spSel_HistoricoEnderecos
    Constraints = True
    Left = 688
    Top = 384
  end
  object spInc_HistoricoEnderecos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_HistoricoEnderecos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_cotas'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 528
    Top = 440
  end
  object spAlt_EnderecosFLG: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_EnderecoFLG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 528
    Top = 496
  end
  object dspInc_HistoricoEnderecos: TDataSetProvider
    DataSet = spInc_HistoricoEnderecos
    Constraints = True
    Left = 560
    Top = 440
  end
  object dspAlt_EnderecosFLG: TDataSetProvider
    DataSet = spAlt_EnderecosFLG
    Constraints = True
    Left = 560
    Top = 496
  end
  object spSel_TotalizaFaltasAno: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZAFALTASANO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 716
    Top = 795
  end
  object dsp_selTotalizaFaltasAno: TDataSetProvider
    DataSet = spSel_TotalizaFaltasAno
    Constraints = True
    Left = 748
    Top = 795
  end
  object dspSel_Totaliza_faltas_Unidade_Mes_DRH: TDataSetProvider
    DataSet = spTotaliza_Faltas_Unidade_Mes_DRH
    Constraints = True
    Left = 752
    Top = 840
  end
  object spTotaliza_Faltas_Unidade_Mes_DRH: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZAFALTAS_UNIDADE_MES_DRH;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 712
    Top = 840
  end
  object spAlt_Perfil: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Perfil;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_perfil'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_funcoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 776
    Top = 872
  end
  object dspAlt_Perfil: TDataSetProvider
    DataSet = spAlt_Perfil
    Constraints = True
    Left = 744
    Top = 872
  end
  object spAlt_Acesso: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Acesso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acesso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_ativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_senha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 784
    Top = 288
  end
  object dspAlt_Acesso: TDataSetProvider
    DataSet = spAlt_Acesso
    Constraints = True
    Left = 816
    Top = 288
  end
  object SP_GeraDadosMatriz: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_GERA_DADOS_MATRIZ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalMatriculados'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCriancasUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIdadeSerieAdequadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFrequenciaDentroEsperado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAproveitamentoDentroEsperado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasJustificadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasInjustificadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligados'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosPedido'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosAbandono'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosMaioridade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosLei'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosOutros'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAprendDentroEsp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtividadesSocioCulturais'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtividadesRecreativas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAcoesSociais'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoDisciplinar'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoSocioEconomica'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoEscolar'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoViolencia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoAbrigo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoInteracaoRelacional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoAtoInfracional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoAprendizagem'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoSaude'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoGuardaIrregular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoGravidezAdolescencia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoMoradia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoTrabalhoInfantil'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtendimentoIndividualizado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPesquisasRespondidasCcas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAcoesFamilias'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFamiliasParticipantes'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFamiliasUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFamiliasResponderamPesq'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalForaEnsinoRegular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesOP'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalOrientacaoProfissional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesOE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalOrientacaoEducacional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesModulo1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalModulo1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesModulo2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalModulo2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesIntensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIntensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluitesIntensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAprovadosVestibulinho'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalInscritosVestibulinho'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAptosAprendizagem'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalModulo2_e_Intensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalInseridosAprendizagem'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmAprendizagemFUNDHAS'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmAprendizagemExterna'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPesquisasEmpresaResp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmpresasParceiras'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Pedido'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Abandono'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Maioridade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Lei'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Outros'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14a'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14c'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14d'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14e'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14f'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14g'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14h'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14i'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14j'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14l'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14m'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14n'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25_Modulo1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25_Modulo2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25_Intensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador33'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 664
    Top = 32
  end
  object dspGeraDadosMatriz: TDataSetProvider
    DataSet = SP_GeraDadosMatriz
    Constraints = True
    Left = 704
    Top = 32
  end
  object spAlt_AlteraSenha: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_AlteraSenha;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_senha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 600
    Top = 32
  end
  object dspAlt_AlteraSenha: TDataSetProvider
    DataSet = spAlt_AlteraSenha
    Constraints = True
    Left = 640
    Top = 32
  end
  object spAlt_EnderecoCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_EnderecoCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end>
    Left = 784
    Top = 368
  end
  object dspAlt_EnderecoCadastro: TDataSetProvider
    DataSet = spAlt_EnderecoCadastro
    Constraints = True
    Left = 818
    Top = 368
  end
  object SP_GeraDadosMatrizSemestreAno: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_GERA_DADOS_MATRIZ_SEMESTRE_ANO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes_inicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes_final'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalMatriculados'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCriancasUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIdadeSerieAdequadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFrequenciaDentroEsperado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAproveitamentoDentroEsperado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasEsperadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalGeralFaltas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasJustificadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasInjustificadas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligados'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosPedido'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosAbandono'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosMaioridade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosLei'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDesligadosOutros'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAprendDentroEsp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtividadesSocioCulturais'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtividadesRecreativas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAcoesSociais'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoDisciplinar'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoSocioEconomica'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoEscolar'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoViolencia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoAbrigo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoInteracaoRelacional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoAtoInfracional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoAprendizagem'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoSaude'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoGuardaIrregular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoGravidezAdolescencia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoMoradia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSituacaoTrabalhoInfantil'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtendimentoIndividualizado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPesquisasRespondidasCcas'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAcoesFamilias'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFamiliasParticipantes'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFamiliasUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFamiliasResponderamPesq'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalForaEnsinoRegular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesOP'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalOrientacaoProfissional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesOE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalOrientacaoEducacional'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesModulo1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalModulo1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesModulo2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalModulo2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesIntensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIntensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluitesIntensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAprovadosVestibulinho'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalInscritosVestibulinho'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAptosAprendizagem'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalModulo2_e_Intensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalInseridosAprendizagem'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmAprendizagemFUNDHAS'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmAprendizagemExterna'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPesquisasEmpresaResp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmpresasParceiras'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Pedido'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Abandono'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Maioridade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Lei'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador08_Outros'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14a'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14c'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14d'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14e'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14f'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14g'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14h'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14i'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14j'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14l'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14m'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador14n'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25_Modulo1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25_Modulo2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25_Intensivo'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Indicador33'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 584
    Top = 32
  end
  object dspGeraDadosMatrizSemestreAno: TDataSetProvider
    DataSet = SP_GeraDadosMatrizSemestreAno
    Constraints = True
    Left = 620
    Top = 32
  end
  object spSel_Verifica_AguardandoSituacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Verifica_AguardandoSituacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 652
    Top = 392
  end
  object dspSel_Verifica_AguardandoSituacao: TDataSetProvider
    DataSet = spSel_Verifica_AguardandoSituacao
    Constraints = True
    Left = 684
    Top = 392
  end
  object spAlt_PesquisaSatisfacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_PesquisaSatisfacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_satisfconv'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_avaliador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_setor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_adolescentes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_media'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 656
    Top = 32
  end
  object dspAlt_PesquisaSatisfacao: TDataSetProvider
    DataSet = spAlt_PesquisaSatisfacao
    Constraints = True
    Left = 688
    Top = 32
  end
  object dspSel_CadastroACOMPESCOLAR: TDataSetProvider
    DataSet = spSel_CadastroACOMPESCOLAR
    Constraints = True
    Left = 620
    Top = 32
  end
  object spSel_CadastroACOMPESCOLAR: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_CadastroACOMPESCOLAR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 584
    Top = 32
  end
  object SP_Sel_Fechamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_FECHAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_ID_FECHAMENTO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_USUARIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_MES'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_ANO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_IND_TIPO_UNIDADE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 609
    Top = 184
  end
  object SP_INC_Fechameno: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_FECHAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_USUARIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_MES'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_ANO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_UNIDADE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_EFETIVO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_CEM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_AGINF'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_MATR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_PROM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_RET'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_TT_SESC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 88
  end
  object SP_Exc_Fechamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_FECHAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_USUARIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 136
  end
  object dspSel_Fechamento: TDataSetProvider
    DataSet = SP_Sel_Fechamento
    Constraints = True
    Left = 577
    Top = 184
  end
  object dspIns_Fechamento: TDataSetProvider
    DataSet = SP_INC_Fechameno
    Constraints = True
    Left = 896
    Top = 88
  end
  object dspExc_Fechamento: TDataSetProvider
    DataSet = SP_Exc_Fechamento
    Constraints = True
    Left = 904
    Top = 136
  end
  object dsp_FechamentoAcompEscolar: TDataSetProvider
    DataSet = sp_FechamentoAcompEscolar
    Constraints = True
    Left = 928
    Top = 384
  end
  object sp_FechamentoAcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_FECHAMENTO_ACOMPESCOLAR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEfetivoUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesEnsinoMed'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAguardandoInformacao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalMatriculados'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPromovidos'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalRetidos'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSemEscola'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEfetivoDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesEMDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAguardandoInformacaoDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalMatriculadosDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPromovidosDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalRetidosDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSemEscolaDivisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEfetivoGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalConcluintesEMGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAguardandoInformacaoGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalMatriculadosGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPromovidosGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalRetidosGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSemEscolaGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ConcluintesEM_Divisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_Matriculados_Divisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_SemEscola_Divisao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ConcluintesEM_Geral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_Matriculados_Geral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_SemEscola_Geral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 896
    Top = 384
  end
  object spInc_EstatisticaDS: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_EstatisticaDS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_total'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ativos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_suspensos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_em_transf'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 920
    Top = 32
  end
  object dspInc_EstatisticaDS: TDataSetProvider
    DataSet = spInc_EstatisticaDS
    Constraints = True
    Left = 745
    Top = 64
  end
  object dspSel_EstatisticaDS: TDataSetProvider
    DataSet = spSel_EstatisticaDS
    Constraints = True
    Left = 912
    Top = 32
  end
  object spSel_EstatisticaDS: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Estatistica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 912
    Top = 80
  end
  object spExc_EstatisticaDS: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Estatistica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 912
    Top = 208
  end
  object dspExc_EstatisticaDS: TDataSetProvider
    DataSet = spExc_EstatisticaDS
    Constraints = True
    Left = 912
    Top = 160
  end
  object spGeraTotalPorUnidadeSituacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_GeraTotalPorUnidadeSituacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEfetivoUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAtivos'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAfastados'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalSuspensos'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEmTransferencia'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 920
    Top = 32
  end
  object dspGeraTotaisPorUnidadeSituacao: TDataSetProvider
    DataSet = spGeraTotalPorUnidadeSituacao
    Constraints = True
    Left = 920
    Top = 80
  end
  object spSel_AvDes_Fatores: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AVDES_FATORES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_fator'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 912
    Top = 392
  end
  object spSel_AvDes_Niveis_Desemp: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AVDES_NIVEIS_DESEMP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_nivel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_conceito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_status_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status_nivel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_conceito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 912
    Top = 448
  end
  object dspSel_AvDes_Fatores: TDataSetProvider
    DataSet = spSel_AvDes_Fatores
    Constraints = True
    Left = 872
    Top = 394
  end
  object dspSel_AvDes_Niveis_Desemp: TDataSetProvider
    DataSet = spSel_AvDes_Niveis_Desemp
    Constraints = True
    Left = 872
    Top = 448
  end
  object spSel_AvDesemp: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AvDesemp;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_avdesemp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_semestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 904
    Top = 384
  end
  object dspSel_AvDesemp: TDataSetProvider
    DataSet = spSel_AvDesemp
    Constraints = True
    Left = 870
    Top = 384
  end
  object dspIns_AvDesemp: TDataSetProvider
    DataSet = spIns_AvDesemp
    Constraints = True
    Left = 872
    Top = 432
  end
  object spIns_AvDesemp: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_AVDESEMP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_semestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_vlr_fator1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_consid_ssocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_obs_aprendiz'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_nom_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_inicio_pratica'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 912
    Top = 432
  end
  object sp_GeraEstatisticaDesempAprendiz: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_GeraEstatisticaDesempenhoAprendiz;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 250
      end
      item
        Name = '@pe_num_semestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAssiduidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDisciplina'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIniciativa'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalProdutividade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalResponsabilidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAssiduidadeE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAssiduidadeB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAssiduidadeR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAssiduidadeI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDisciplinaE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDisciplinaB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDisciplinaR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalDisciplinaI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIniciativaE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIniciativaB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIniciativaR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIniciativaI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalProdutividadeE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalProdutividadeB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalProdutividadeR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalProdutividadeI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalResponsabilidadeE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalResponsabilidadeB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalResponsabilidadeR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalResponsabilidadeI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_AssiduidadeE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_AssiduidadeB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_AssiduidadeR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_AssiduidadeI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_DisciplinaE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_DisciplinaB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_DisciplinaR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_DisciplinaI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_IniciativaE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_IniciativaB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_IniciativaR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_IniciativaI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ProdutividadeE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ProdutividadeB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ProdutividadeR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ProdutividadeI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ResponsabilidadeE'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ResponsabilidadeB'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ResponsabilidadeR'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porc_ResponsabilidadeI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 896
    Top = 32
  end
  object dsp_GeraEstatisticaDesempAprendiz: TDataSetProvider
    DataSet = sp_GeraEstatisticaDesempAprendiz
    Constraints = True
    Left = 936
    Top = 32
  end
  object spAlt_AvDesemp: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_AvDesemp;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_avdesemp'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_vlr_fator1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_fator5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_consid_ssocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_obs_aprendiz'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_nom_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_inicio_pratica'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 320
    Top = 376
  end
  object dspAlt_AvDesemp: TDataSetProvider
    DataSet = spAlt_AvDesemp
    Constraints = True
    Left = 360
    Top = 376
  end
  object SP_GERA_DADOS_RAICA: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_GERA_DADOS_RAICA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nm_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_divisao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_1b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_2b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_3b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_ir_4b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_1b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_2b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_3b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_i_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_r_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_mb_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_o_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_nav_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_p_4b_item8'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_1b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_2b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_3b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_a_4b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_1b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_2b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_3b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_e_4b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item1'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item2'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item3'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item4'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item5'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item6'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_1b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_2b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_3b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_i_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_r_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_mb_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_o_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_nav_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_q_4b_item7'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 368
    Top = 416
  end
  object dspGERA_DADOS_RAICA: TDataSetProvider
    DataSet = SP_GERA_DADOS_RAICA
    Constraints = True
    Left = 336
    Top = 416
  end
  object spTOTALIZA_APROVFREQ_ACOMPESCOLAR: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_APROVFREQ_ACOMPESCOLAR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_ab_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_ab_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_ab_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_de_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_de_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_de_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_ac_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_ac_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_ac_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_si_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_si_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_aprov_si_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_ac_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_ac_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_ac_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_ab_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_ab_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_ab_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_si_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_si_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_si_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_fx_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_fx_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_fx_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_de_1b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_de_2b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_freq_de_3b'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 350
    Top = 508
  end
  object dspTOTALIZA_APROVFREQ_ACOMPESCOLAR: TDataSetProvider
    DataSet = spTOTALIZA_APROVFREQ_ACOMPESCOLAR
    Constraints = True
    Left = 391
    Top = 508
  end
  object SP_TOTAIS_AVADESEMP_POR_EMPRESA: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTAIS_AVADESEMP_POR_EMPRESA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Semestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_SECAO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Assiduidade_Insatisfatorio'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Assiduidade_Regular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Assiduidade_Bom'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Assiduidade_Excelente'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Assiduidade_NaoAvaliado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disciplina_Insatisfatorio'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disciplina_Regular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disciplina_Bom'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disciplina_Excelente'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disciplina_NaoAvaliado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Iniciativa_Insatisfatorio'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Iniciativa_Regular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Iniciativa_Bom'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Iniciativa_Excelente'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Iniciativa_NaoAvaliado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Produtividade_Insatisfatorio'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Produtividade_Regular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Produtividade_Bom'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Produtividade_Excelente'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Produtividade_NaoAvaliado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Responsabilidade_Insatisfatorio'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Responsabilidade_Regular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Responsabilidade_Bom'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Responsabilidade_Excelente'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@Responsabilidade_NaoAvaliado'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_avaliacoes_media_excelente'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_avaliacoes_media_bom'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_avaliacoes_media_regular'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@tt_avaliacoes_media_insatisfatorio'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 824
    Top = 32
  end
  object dsp_Totais_Avadesemp_Por_Empresa: TDataSetProvider
    DataSet = SP_TOTAIS_AVADESEMP_POR_EMPRESA
    Constraints = True
    Left = 792
    Top = 32
  end
  object spSel_EjaTelessala: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_EJATELESSALA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 648
    Top = 691
  end
  object dspSel_EjaTelessala: TDataSetProvider
    DataSet = spSel_EjaTelessala
    Constraints = True
    Left = 688
    Top = 691
  end
  object spSel_Cadastro_Desligados_TRF: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_CADASTRO_DESLIGADOS_TRF;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 365
    Top = 65528
  end
  object dspSel_Cadastro_Desligados_TRF: TDataSetProvider
    DataSet = spSel_Cadastro_Desligados_TRF
    Constraints = True
    Left = 397
    Top = 65528
  end
  object adoConnINATIVOS: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=DecaInativos;Data Source=SRVBACKUP;' +
      'Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096' +
      ';Workstation ID=DI04;Use Encryption for Data=False;Tag with colu' +
      'mn collation when possible=False'
    DefaultDatabase = 'DecaInativos'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 96
    Top = 16
  end
  object spINS_CadastroInativos: TADOStoredProc
    Connection = adoConnINATIVOS
    ProcedureName = 'SP_INC_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_etnia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_calcado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tam_camisa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_pontuacao_triagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_projeto_vida'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_admissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tam_calca'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_sanguineo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_certidao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 1167
    Top = 80
  end
  object dspINS_CadastroInativos: TDataSetProvider
    DataSet = spINS_CadastroInativos
    Constraints = True
    Left = 1202
    Top = 80
  end
  object spSel_VerificaUltimoBimestreAnoAluno: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_VerificaUltimoBimestreAnoAluno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 632
    Top = 376
  end
  object dspSel_VerificaUltimoBimestreAnoAluno: TDataSetProvider
    DataSet = spSel_VerificaUltimoBimestreAnoAluno
    Constraints = True
    Left = 664
    Top = 376
  end
  object dspUpd_CTPS: TDataSetProvider
    DataSet = spUpd_Ctps
    Constraints = True
    Left = 464
    Top = 440
  end
  object spUpd_Ctps: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spUPD_CTPS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ctps_num'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ctps_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end>
    Left = 496
    Top = 440
  end
  object spSel_MateriaisOdonto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_MateriaisOdonto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_material'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_cod_tipo_material'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_minima'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_material'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 997
    Top = 575
  end
  object dspSel_MateriaisOdonto: TDataSetProvider
    DataSet = spSel_MateriaisOdonto
    Constraints = True
    Left = 1037
    Top = 575
  end
  object spIns_MateriaisOdonto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_MateriaisOdonto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_tipo_material'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_unidade_estoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_minima'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_qtd_estoque_atual'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_material'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 997
    Top = 624
  end
  object dspIns_MateriaisOdonto: TDataSetProvider
    DataSet = spIns_MateriaisOdonto
    Constraints = True
    Left = 1037
    Top = 624
  end
  object spUPD_MateriaisOdonto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_MateriaisOdonto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_material'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_ind_unidade_estoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_minima'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_qtd_estoque_atual'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_material'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 997
    Top = 680
  end
  object dspUpd_MateriaisOdonto: TDataSetProvider
    DataSet = spUPD_MateriaisOdonto
    Constraints = True
    Left = 1037
    Top = 680
  end
  object spSEL_MateriaisOdontoTIPO: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_MateriaisOdontoTIPO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_tipo_material'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipo_material'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 997
    Top = 527
  end
  object dspSel_MateriaisOdontoTIPO: TDataSetProvider
    DataSet = spSEL_MateriaisOdontoTIPO
    Constraints = True
    Left = 1037
    Top = 527
  end
  object spIns_MovientoEstoque: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spINS_MovimentoEstoque;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo_movimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dat_movimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_material'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_qtd_movimento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 1093
    Top = 480
  end
  object dspIns_MovimentoEstoque: TDataSetProvider
    DataSet = spIns_MovientoEstoque
    Constraints = True
    Left = 1133
    Top = 480
  end
  object spUPD_AtualizaEstoqueOdonto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spUPD_AtualizaEstoqueOdonto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_qtd_movimento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_cod_material'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1093
    Top = 535
  end
  object dspUPD_AtualizaEstoqueOdonto: TDataSetProvider
    DataSet = spUPD_AtualizaEstoqueOdonto
    Constraints = True
    Left = 1133
    Top = 536
  end
  object spSEL_MateriaisOdontoMOVIMENTOS: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spSEL_MateriaisOdontoMOVIMENTOS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_movimento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_cod_material'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_movimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 885
    Top = 695
  end
  object dspSel_MateriaisOdontoMOVIMENTOS: TDataSetProvider
    DataSet = spSEL_MateriaisOdontoMOVIMENTOS
    Constraints = True
    Left = 925
    Top = 696
  end
  object spAlt_PeriodoCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_PeriodoCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 718
    Top = 384
  end
  object dspAlt_PeriodoCadastro: TDataSetProvider
    DataSet = spAlt_PeriodoCadastro
    Constraints = True
    Left = 758
    Top = 384
  end
  object spSel_UltimoBimestreAluno: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UltimoBimestreAluno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 760
    Top = 72
  end
  object dspSel_UltimoBimestreAluno: TDataSetProvider
    DataSet = spSel_UltimoBimestreAluno
    Constraints = True
    Left = 760
    Top = 120
  end
  object spExc_Consulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Exc_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1168
    Top = 32
  end
  object dspExc_Consulta: TDataSetProvider
    DataSet = spExc_Consulta
    Constraints = True
    Left = 1200
    Top = 32
  end
  object spINS_AcompEscolarTRF: TADOStoredProc
    Connection = adoConnINATIVOS
    ProcedureName = 'SP_INC_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end>
    Left = 1167
    Top = 136
  end
  object dspIns_AcompEscolar_TRF: TDataSetProvider
    DataSet = spINS_AcompEscolarTRF
    Constraints = True
    Left = 1202
    Top = 136
  end
  object spINS_Cadatro_HistoricoTRF: TADOStoredProc
    Connection = adoConnINATIVOS
    ProcedureName = 'SP_INC_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_historico'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipo_historico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_ind_motivo2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dsc_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_final_suspensao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_materia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_ind_motivo_transf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_orig_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dest_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dest_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dest_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_inicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_final'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_flg_passe'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_num_cotas_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_orig_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dest_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dat_curso_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_curso_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_estagio_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@pe_num_estagio_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1167
    Top = 184
  end
  object dspInc_Cadastro_Historico_TRF: TDataSetProvider
    DataSet = spINS_Cadatro_HistoricoTRF
    Constraints = True
    Left = 1202
    Top = 184
  end
  object spSel_TipoUniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TIPOUNIFORME;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipouniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipouniforme'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 1168
    Top = 280
  end
  object dspSel_TipoUniforme: TDataSetProvider
    DataSet = spSel_TipoUniforme
    Constraints = True
    Left = 1208
    Top = 280
  end
  object spInc_Uniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_UNIFORME;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_cor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_admissao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_reposicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_reposicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_numeracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 500
        Value = Null
      end
      item
        Name = '@pe_cod_cecam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_giap'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 1168
    Top = 336
  end
  object spUpd_Uniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_UNIFORME;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_cor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_admissao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_reposicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_reposicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_numeracao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 750
        Value = Null
      end
      item
        Name = '@pe_cod_cecam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_giap'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 1168
    Top = 392
  end
  object dspInc_Uniforme: TDataSetProvider
    DataSet = spInc_Uniforme
    Constraints = True
    Left = 1208
    Top = 336
  end
  object dspUpd_Uniforme: TDataSetProvider
    DataSet = spUpd_Uniforme
    Constraints = True
    Left = 1208
    Top = 392
  end
  object spSel_Uniformes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Uniformes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_numeracao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 1168
    Top = 448
  end
  object dspSel_Uniformes: TDataSetProvider
    DataSet = spSel_Uniformes
    Constraints = True
    Left = 1208
    Top = 448
  end
  object spSel_ConsultaUniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ConsultaUniforme;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipouniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_cor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_numeracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1168
    Top = 512
  end
  object dspSel_ConsultaUniforme: TDataSetProvider
    DataSet = spSel_ConsultaUniforme
    Constraints = True
    Left = 1208
    Top = 512
  end
  object spInc_UniformeSolicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_UniformeSolicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_solicitacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_mes_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tam_uniforme'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 1288
    Top = 280
  end
  object dspInc_UniformeSolicitacao: TDataSetProvider
    DataSet = spInc_UniformeSolicitacao
    Constraints = True
    Left = 1336
    Top = 280
  end
  object spSel_UniformesSolicitacoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_UniformesSolicitacoes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_mes_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_bloqueado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 500
        Value = Null
      end
      item
        Name = '@pe_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 1288
    Top = 336
  end
  object dspSel_UniformesSolicitacoes: TDataSetProvider
    DataSet = spSel_UniformesSolicitacoes
    Constraints = True
    Left = 1336
    Top = 336
  end
  object spSel_ConsultaSolicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ConsultaSolicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_mes_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1288
    Top = 392
  end
  object dspSel_ConsultaSolicitacao: TDataSetProvider
    DataSet = spSel_ConsultaSolicitacao
    Constraints = True
    Left = 1336
    Top = 392
  end
  object spSel_DadosEducaSenso: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DadosEducaSenso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipoDado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1288
    Top = 448
  end
  object dspSel_DadosEducaSenso: TDataSetProvider
    DataSet = spSel_DadosEducaSenso
    Constraints = True
    Left = 1336
    Top = 448
  end
  object spSel_UltimoBimestre: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_UltimoBimestre;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 1288
    Top = 512
  end
  object dspSel_UltimoBimestre: TDataSetProvider
    DataSet = spSel_UltimoBimestre
    Constraints = True
    Left = 1336
    Top = 512
  end
  object spSel_CursosAprendizagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_CursosAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cbo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1288
    Top = 568
  end
  object dspSel_CusrsosAprendizagem: TDataSetProvider
    DataSet = spSel_CursosAprendizagem
    Constraints = True
    Left = 1336
    Top = 568
  end
  object spSel_Admissoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Admissoes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_admissao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 1288
    Top = 632
  end
  object dspSel_Admissoes: TDataSetProvider
    DataSet = spSel_Admissoes
    Constraints = True
    Left = 1336
    Top = 632
  end
  object spInc_Admissoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Admissoes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_MATRICULA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@PE_LOCAL_CCUSTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PE_ENDERECO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_COMPLEMENTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_BAIRRO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_CEP'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@PE_TELEFONE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CELULAR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_GRAU_INSTRUCAO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_ESTADO_CIVIL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_EMAIL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PE_NACIONALIDADE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_NASCIMENTO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_LOCAL_NASCIMENTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_RG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_RG_EMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_RG_EMISSOR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_CTPS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CTPS_SERIE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CTPS_EMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_CTPS_UF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PE_CPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_PIS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_PIS_EMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_PIS_BANCO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_DATA_ADMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_TERMINO_CONTRATO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_DATA_EXAMEMEDICO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_PRIMEIRO_EMPREGO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_BANCO_AGENCIA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_BANCO_CONTA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CONTRIB_SINDICAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_VALOR_CONTRIB_SINDICAL'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_SALARIO_BASE'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_CURSO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CARGO_FUNCAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_SEGURO_VIDA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_BENEFICIARIOS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@PE_DEPENDENTES'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@PE_PAI'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_MAE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_RESPONSAVEL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_CONJUGE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_GUARDA_REGULAR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_VT'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_VT_TIPO_ITINERARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_VT_QTD_DIA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_PE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_PE_TIPO_ITINERARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_PE_QTD_DIA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_QTD_ADMISSAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_RACA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_ALTURA'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_PESO'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_OLHOS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CABELO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_TIPO_SANGUINEO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_PORTADOR_NECESSIDADES'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_QUAL_NECESSIDADE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_COD_USUARIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CARTAO_SUS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_FUMANTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1288
    Top = 688
  end
  object dspInc_Admissoes: TDataSetProvider
    DataSet = spInc_Admissoes
    Constraints = True
    Left = 1336
    Top = 688
  end
  object spUpd_UniformesSolicitacoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_UniformesSolicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_qtd_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tam_uniforme'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 1236
    Top = 32
  end
  object dspUpd_UniformesSolicitacao: TDataSetProvider
    DataSet = spUpd_UniformesSolicitacoes
    Constraints = True
    Left = 1284
    Top = 32
  end
  object spDel_UniformesSolicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_DEL_SolicitacaoUniforme;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 1236
    Top = 80
  end
  object dspDel_UniformesSolicitacao: TDataSetProvider
    DataSet = spDel_UniformesSolicitacao
    Constraints = True
    Left = 1284
    Top = 80
  end
  object spUPD_GeraPedidosUniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_GeraPedidosUniforme;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1236
    Top = 136
  end
  object dspUPD_GeraPedidosUniforme: TDataSetProvider
    DataSet = spUPD_GeraPedidosUniforme
    Constraints = True
    Left = 1284
    Top = 136
  end
  object spAlt_CartaoPasse: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_CartaoPasse;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_cartao_passe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_passe_bloqueado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end>
    Left = 1288
    Top = 744
  end
  object dspAlt_CartaoPasse: TDataSetProvider
    DataSet = spAlt_CartaoPasse
    Constraints = True
    Left = 1336
    Top = 744
  end
  object spAlt_CartaoCotasPasse: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_CartaoCotasPasses;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartao_passe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 1288
    Top = 800
  end
  object dspAlt_CartaoCotasPasses: TDataSetProvider
    DataSet = spAlt_CartaoCotasPasse
    Constraints = True
    Left = 1336
    Top = 800
  end
  object sp_EmitirComprovanteRecebimentoUniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EmitirComprovanteRecebimentoUniforme;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1236
    Top = 192
  end
  object dspEmitirComprovanteRecebimentoUniforme: TDataSetProvider
    DataSet = sp_EmitirComprovanteRecebimentoUniforme
    Constraints = True
    Left = 1284
    Top = 192
  end
  object spSel_DiasUteisCalendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DiasUteisCalendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1236
    Top = 236
  end
  object dspSel_DiasUteisCalendario: TDataSetProvider
    DataSet = spSel_DiasUteisCalendario
    Constraints = True
    Left = 1284
    Top = 236
  end
  object spInc_ValoresPasses: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ValoresPasses;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vr_passagem'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_vr_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 888
    Top = 752
  end
  object spSel_ValorPasse: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ValoresPasses;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 888
    Top = 800
  end
  object spUpd_ValorPasse: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_ValorPasse;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 888
    Top = 784
  end
  object dspInc_ValoresPasses: TDataSetProvider
    DataSet = spInc_ValoresPasses
    Constraints = True
    Left = 928
    Top = 688
  end
  object dspSel_ValorPasse: TDataSetProvider
    DataSet = spSel_ValorPasse
    Constraints = True
    Left = 928
    Top = 736
  end
  object dspUpd_ValorPasse: TDataSetProvider
    DataSet = spUpd_ValorPasse
    Constraints = True
    Left = 928
    Top = 784
  end
  object spSel_ValeTransporte: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_VALETRANSPORTE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cota_passes'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pe_dias_uteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_passe_bloqueado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1120
    Top = 624
  end
  object dspSel_ValeTransporte: TDataSetProvider
    DataSet = spSel_ValeTransporte
    Constraints = True
    Left = 1165
    Top = 624
  end
  object dspIns_TmpPasses: TDataSetProvider
    DataSet = spIns_TmpPasses
    Constraints = True
    Left = 1176
    Top = 664
  end
  object dspSel_TmpPasses: TDataSetProvider
    DataSet = spSel_TmpPasses
    Constraints = True
    Left = 1176
    Top = 712
  end
  object dspExc_TmpPasses: TDataSetProvider
    DataSet = spExc_TmpPasses
    Constraints = True
    Left = 1176
    Top = 760
  end
  object spIns_TmpPasses: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_TMPPASSES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_num_cotas'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dias_uteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_bruto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_liquido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nascimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_admissao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_passe_bloqueado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_cpf_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_nom_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 1136
    Top = 664
  end
  object spSel_TmpPasses: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TMPPASSES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1136
    Top = 712
  end
  object spExc_TmpPasses: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_TMPPASSES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1136
    Top = 760
  end
  object spSel_RelacaoVT: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_RelacaoVT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_passe_bloqueado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end>
    Left = 1138
    Top = 688
  end
  object dspSel_RelacaoVT: TDataSetProvider
    DataSet = spSel_RelacaoVT
    Constraints = True
    Left = 1176
    Top = 688
  end
  object sp_TotalizaSeguro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TotalizaSeguro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 1144
    Top = 696
  end
  object dspSel_TotalizaSeguro: TDataSetProvider
    DataSet = sp_TotalizaSeguro
    Constraints = True
    Left = 1184
    Top = 696
  end
  object spUPD_DiasUteisMes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_DiasUteisMes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_du'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dias_uteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1136
    Top = 720
  end
  object dspUpd_DiasUteisMes: TDataSetProvider
    DataSet = spUPD_DiasUteisMes
    Constraints = True
    Left = 1168
    Top = 720
  end
  object spSel_SelecionaDadosGeralPasses: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_SelecionaDadosGeralPasses;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_passe_bloqueado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1136
    Top = 688
  end
  object dspSel_SelecionaDadosGeralPasses: TDataSetProvider
    DataSet = spSel_SelecionaDadosGeralPasses
    Constraints = True
    Left = 1176
    Top = 688
  end
  object spUpd_DadosEducacenso: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_DadosEducacenso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_cadastro_educacenso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_situacao_cei'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes_educacenso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 1136
    Top = 712
  end
  object dspUpd_DadosEducacenso: TDataSetProvider
    DataSet = spUpd_DadosEducacenso
    Constraints = True
    Left = 1176
    Top = 712
  end
  object spUpd_DadosEducacensoCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_DadosEducacensoCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_cadastro_educacenso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_situacao_cei'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes_educacenso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 1136
    Top = 720
  end
  object dspUpd_DadosEducacensoCadastro: TDataSetProvider
    DataSet = spUpd_DadosEducacensoCadastro
    Constraints = True
    Left = 1176
    Top = 720
  end
  object spUpd_ReativarProntuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_ReativarProntuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1288
    Top = 760
  end
  object dspUp_ReativarProntuario: TDataSetProvider
    DataSet = spUpd_ReativarProntuario
    Constraints = True
    Left = 1336
    Top = 760
  end
  object spSel_RelacaoSeguro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_RelacaoSEGURO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1136
    Top = 720
  end
  object dspSel_RelacaoSeguro: TDataSetProvider
    DataSet = spSel_RelacaoSeguro
    Constraints = True
    Left = 1176
    Top = 720
  end
  object spSel_EstatisticasAbsenteismo: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_EstatisticasAbsenteismo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalUnidade'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia14'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia14'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia14'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalPresencasDia31'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalFaltasDia31'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalJustificadasDia31'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia01'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia02'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia03'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia04'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia05'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia06'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia07'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia08'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia09'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia10'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia11'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia12'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia13'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia14'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia14'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia14'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia15'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia16'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia17'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia18'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia19'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia20'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia21'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia22'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia23'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia24'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia25'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia26'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia27'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia28'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia29'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia30'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcPresencasDia31'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcFaltasDia31'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcJustificadasDia31'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end>
    Left = 1000
    Top = 744
  end
  object dspSel_EstatisticasAbsenteismo: TDataSetProvider
    DataSet = spSel_EstatisticasAbsenteismo
    Constraints = True
    Left = 1040
    Top = 744
  end
  object dspUpd_Documentos: TDataSetProvider
    DataSet = spUPD_Documentos
    Constraints = True
    Left = 1048
    Top = 712
  end
  object spUPD_Documentos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_DOCUMENTOS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ctps'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ctps_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_certidao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 1008
    Top = 712
  end
  object dspSel_Fotografias: TDataSetProvider
    DataSet = spSel_Fotografias
    Constraints = True
    Left = 1144
    Top = 696
  end
  object spSel_Fotografias: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Fotografias;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1184
    Top = 696
  end
  object spIns_TipoUniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_TipoUniforme;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipouniforme'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1016
    Top = 716
  end
  object spUpd_TipoUniforme: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_TipoUniforme;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipouniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipouniforme'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1016
    Top = 764
  end
  object dspIns_TipoUniforme: TDataSetProvider
    DataSet = spIns_TipoUniforme
    Constraints = True
    Left = 1048
    Top = 716
  end
  object dspUpd_TipoUniforme: TDataSetProvider
    DataSet = spUpd_TipoUniforme
    Constraints = True
    Left = 1048
    Top = 768
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_DEL_SolicitacoesUniformes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1104
    Top = 392
  end
  object dspDel_SolicitacoesUniformes: TDataSetProvider
    DataSet = spDel_UniformesSolicitacao
    Constraints = True
    Left = 1072
    Top = 392
  end
  object spIns_UniformesRequisicoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_UNIFORMESREQUISICOES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_requisicao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_mes_requisicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_requisicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1016
    Top = 704
  end
  object dsp_InsUniformesRequisicoes: TDataSetProvider
    DataSet = spIns_UniformesRequisicoes
    Constraints = True
    Left = 1056
    Top = 704
  end
  object spSel_UniformesRequisicoes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_UniformesRequisicoes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_requisicao'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_mes_requisicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_requisicao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1128
    Top = 704
  end
  object dspSel_UniformesRequisicoes: TDataSetProvider
    DataSet = spSel_UniformesRequisicoes
    Constraints = True
    Left = 1168
    Top = 704
  end
  object sp_SelGerencial_TotalizaUniformes: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Gerencial_TotalizaUniformes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_mes_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uniforme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_bloqueado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_detalhes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 500
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 1016
    Top = 704
  end
  object dspSel_Gerencial_TOotalizaUniformes: TDataSetProvider
    DataSet = sp_SelGerencial_TotalizaUniformes
    Constraints = True
    Left = 1048
    Top = 704
  end
  object adoSP_Sel_Certificado: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Certificado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_certificado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_certificado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1008
    Top = 736
  end
  object adoSP_InsUpd_Certificado: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_UPD_Certificado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_cod_certificado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_certificado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1008
    Top = 784
  end
  object dspSel_Certificados: TDataSetProvider
    DataSet = adoSP_Sel_Certificado
    Constraints = True
    Left = 1048
    Top = 736
  end
  object dspInsUpd_Certificado: TDataSetProvider
    DataSet = adoSP_InsUpd_Certificado
    Constraints = True
    Left = 1056
    Top = 784
  end
  object spSel_Curso: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Curso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_curso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_curso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_flg_situacao_curso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_certificado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_data_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_termino'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 1448
    Top = 328
  end
  object sp_Ins_Upd_Curso: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_UPD_Cursos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_cod_curso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_curso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_carga_horaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_termino'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_certificado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1448
    Top = 384
  end
  object dspSel_Curso: TDataSetProvider
    DataSet = spSel_Curso
    Constraints = True
    Left = 1407
    Top = 328
  end
  object dspIns_Upd_Curso: TDataSetProvider
    DataSet = sp_Ins_Upd_Curso
    Constraints = True
    Left = 1408
    Top = 384
  end
  object spIns_AlunosCursos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_AlunosCursos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_curso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 1456
    Top = 440
  end
  object dspIns_AlunosCursos: TDataSetProvider
    DataSet = spIns_AlunosCursos
    Constraints = True
    Left = 1416
    Top = 440
  end
  object spSel_AlunosCursos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AlunosCursos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_curso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_certificado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1456
    Top = 496
  end
  object dspSel_AlunosCursos: TDataSetProvider
    DataSet = spSel_AlunosCursos
    Constraints = True
    Left = 1416
    Top = 496
  end
  object dspUPD_AlunosCursos: TDataSetProvider
    DataSet = spUPD_AlunosCursos
    Constraints = True
    Left = 1424
    Top = 560
  end
  object spUPD_AlunosCursos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_AlunosCursos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_curso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 1464
    Top = 560
  end
  object dspSel_EmitirCertificado: TDataSetProvider
    DataSet = spSel_EmitirCertificado
    Constraints = True
    Left = 1432
    Top = 624
  end
  object dspIns_EmitirCertificado: TDataSetProvider
    DataSet = spINS_EmitirCertificado
    Constraints = True
    Left = 1432
    Top = 672
  end
  object dspExc_EmitirCertificado: TDataSetProvider
    DataSet = spEXC_EmitirCertificado
    Constraints = True
    Left = 1433
    Top = 725
  end
  object spSel_EmitirCertificado: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_EmitirCertificado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1467
    Top = 624
  end
  object spINS_EmitirCertificado: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_EmitirCertificado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_curso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ch'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_termino'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1467
    Top = 672
  end
  object spEXC_EmitirCertificado: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_EmitirCertificado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Size = -1
        Value = Null
      end>
    Left = 1469
    Top = 726
  end
  object spSel_DadosFichaCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DadosFichaCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_matricula1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_matricula2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1468
    Top = 687
  end
  object dspSel_DadosFichaCadastro: TDataSetProvider
    DataSet = spSel_DadosFichaCadastro
    Constraints = True
    Left = 1432
    Top = 687
  end
  object spExc_TMPFichaCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_TMPFichaCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1008
    Top = 736
  end
  object spIns_TMPFichaCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_TMP_FichaCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_admissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_img_foto'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_cotas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo_fundhas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_ctps'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cert_nasc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_pai'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_mae'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_rg_pai'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_rg_mae'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_rg_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cpf_pai'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cpf_mae'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cpf_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_nis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1008
    Top = 784
  end
  object spSel_TMPFichaCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Sel_TMPFichaCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1128
    Top = 784
  end
  object dspIns_TMPFichaCadastro: TDataSetProvider
    DataSet = spIns_TMPFichaCadastro
    Constraints = True
    Left = 1048
    Top = 784
  end
  object dspExc_TMPFichaCadastro: TDataSetProvider
    DataSet = spExc_TMPFichaCadastro
    Constraints = True
    Left = 1048
    Top = 736
  end
  object dspSel_TMPFichaCadastro: TDataSetProvider
    DataSet = spSel_TMPFichaCadastro
    Constraints = True
    Left = 1168
    Top = 784
  end
  object sp_UpdRGCPF_Familiar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_RgCPF_Familiar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_rg_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_cpf_fam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 1056
    Top = 744
  end
  object dspUpd_RGCPF_Fam: TDataSetProvider
    DataSet = sp_UpdRGCPF_Familiar
    Constraints = True
    Left = 1016
    Top = 744
  end
  object spSel_UnidadeEscolarCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_UnidadeEscolarCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_uecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 904
    Top = 776
  end
  object dspSel_UnidadeEscolarCIE: TDataSetProvider
    DataSet = spSel_UnidadeEscolarCIE
    Constraints = True
    Left = 936
    Top = 776
  end
  object dspIns_UnidadeEscolarCIE: TDataSetProvider
    DataSet = spIns_UnidadeEscolarCIE
    Constraints = True
    Left = 1096
    Top = 776
  end
  object dspUpd_UnidadeEscolarCIE: TDataSetProvider
    DataSet = spUpd_UnidadeEscolarCIE
    Constraints = True
    Left = 1248
    Top = 776
  end
  object spIns_UnidadeEscolarCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_UnidadeEscolarCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_uecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1062
    Top = 777
  end
  object spUpd_UnidadeEscolarCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_UnidadeEscolarCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_uecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1214
    Top = 776
  end
  object spSel_VerificaInconsistencias_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_SEL_VerificaInconsistencias_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 912
    Top = 752
  end
  object dspSel_VerificaInconsistencias_AcompEscolar: TDataSetProvider
    DataSet = spSel_VerificaInconsistencias_AcompEscolar
    Constraints = True
    Left = 952
    Top = 752
  end
  object spUpd_SemEscol_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_SemEscola_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_sem_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 888
    Top = 760
  end
  object dspUpd_SemEscola_Cadastro: TDataSetProvider
    DataSet = spUpd_SemEscol_Cadastro
    Constraints = True
    Left = 848
    Top = 760
  end
  object spSel_SeriesCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_SeriesCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie_cie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 928
    Top = 768
  end
  object dspSel_SerieCIE: TDataSetProvider
    DataSet = spSel_SeriesCIE
    Constraints = True
    Left = 888
    Top = 768
  end
  object spIns_SerieCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_SerieCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_seriecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 1104
    Top = 768
  end
  object dspIns_SerieCIE: TDataSetProvider
    DataSet = spIns_SerieCIE
    Constraints = True
    Left = 1064
    Top = 768
  end
  object spUpd_SerieCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_SerieEscolarCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_seriecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_seriecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 928
    Top = 760
  end
  object dspUpd_SerieCie: TDataSetProvider
    DataSet = spUpd_SerieCIE
    Constraints = True
    Left = 888
    Top = 760
  end
  object spSel_TurmaCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TurmasCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_turmacie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_turmacie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_cod_id_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_uecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_id_seriecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_seriecie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end>
    Left = 928
    Top = 768
  end
  object dspSel_TurmaCIE: TDataSetProvider
    DataSet = spSel_TurmaCIE
    Constraints = True
    Left = 887
    Top = 768
  end
  object spIns_TurmaCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_TurmaCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_turmacie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_cod_id_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_seriecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end>
    Left = 1032
    Top = 768
  end
  object spUpd_TurmaCIE: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_TurmaCIE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_turmacie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_turmacie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_cod_id_uecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_seriecie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end>
    Left = 1166
    Top = 768
  end
  object dspIns_TurmaCIE: TDataSetProvider
    DataSet = spIns_TurmaCIE
    Constraints = True
    Left = 992
    Top = 768
  end
  object dspUpd_TurmaCIE: TDataSetProvider
    DataSet = spUpd_TurmaCIE
    Constraints = True
    Left = 1128
    Top = 768
  end
  object dspUpd_TurmaCIE_Cadastro: TDataSetProvider
    DataSet = spUpd_TurmaCIE_Cadastro
    Constraints = True
    Left = 896
    Top = 776
  end
  object spUpd_TurmaCIE_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_TurmaCIE_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_turma_cei'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@pe_dat_cadastro_educacenso'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 936
    Top = 776
  end
  object dspSel_ClassesCadastro: TDataSetProvider
    DataSet = spSel_ClassesCadastro
    Constraints = True
    Left = 840
    Top = 768
  end
  object spSel_ClassesCadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ClassesCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_turma_cei'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 880
    Top = 768
  end
  object spSel_ConsultaMudancaVinculo: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ConsultaMudancasVinculo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 768
  end
  object dspSel_ConsultaMudancasVinculo: TDataSetProvider
    DataSet = spSel_ConsultaMudancaVinculo
    Constraints = True
    Left = 904
    Top = 768
  end
  object dspUPD_Admissao: TDataSetProvider
    DataSet = spUpd_Admissao
    Constraints = True
    Left = 864
    Top = 760
  end
  object spUpd_Admissao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_UPD_Admissoes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_ID_ADMISSAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_MATRICULA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@PE_LOCAL_CCUSTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PE_ENDERECO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_COMPLEMENTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_BAIRRO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_CEP'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@PE_TELEFONE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CELULAR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_GRAU_INSTRUCAO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_ESTADO_CIVIL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_EMAIL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PE_NACIONALIDADE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_NASCIMENTO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_LOCAL_NASCIMENTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_RG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_RG_EMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_RG_EMISSOR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_CTPS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CTPS_SERIE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CTPS_EMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_CTPS_UF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PE_CPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_PIS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_PIS_EMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_PIS_BANCO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_DATA_ADMISSAO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_TERMINO_CONTRATO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_DATA_EXAMEMEDICO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_PRIMEIRO_EMPREGO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_BANCO_AGENCIA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_BANCO_CONTA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CONTRIB_SINDICAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_VALOR_CONTRIB_SINDICAL'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_SALARIO_BASE'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_CURSO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_CARGO_FUNCAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_SEGURO_VIDA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_BENEFICIARIOS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@PE_DEPENDENTES'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@PE_PAI'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_MAE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_RESPONSAVEL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_CONJUGE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_GUARDA_REGULAR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_VT'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_VT_TIPO_ITINERARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_VT_QTD_DIA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_PE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_PE_TIPO_ITINERARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_PE_QTD_DIA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_QTD_ADMISSAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_RACA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_ALTURA'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_PESO'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@PE_OLHOS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_CABELO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_TIPO_SANGUINEO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_PORTADOR_NECESSIDADES'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_QUAL_NECESSIDADE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_COD_USUARIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CARTAO_SUS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@PE_FUMANTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 904
    Top = 760
  end
  object adoSP_Alt_CadastroDocumentos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Alt_CadastroDocumentos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_MATRICULA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@PE_NUM_RG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PE_DSC_RG_EMISSOR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_DAT_EMISSAO_RG'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_DSC_UF_RG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PE_NUM_CPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PE_NUM_CERTIDAO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_DSC_LOCAL_NASCIMENTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_NUM_TITULO_ELEITOR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 13
        Value = Null
      end
      item
        Name = '@PE_NUM_ZONA_ELEITORAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_SECAO_ELEITORAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_CTPS_NUM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@PE_CTPS_SERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@PE_DAT_EMISSAO_CTPS'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_DSC_UF_CTPS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PE_NUM_PIS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end
      item
        Name = '@PE_DAT_EMISSAO_PIS'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PE_DSC_BANCO_PIS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_NUM_RESERVISTA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PE_NUM_RESERVISTA_CSM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@PE_NUM_CARTAO_SUS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@PE_DSC_MUNICIPIO_RESIDENCIA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_DSC_UF_RESIDENCIA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PE_NUM_TELEFONE_CELULAR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@PE_IND_GRAU_INSTRUCAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_DSC_EMAIL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@PE_IND_ESTADO_CIVIL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_DSC_NACIONALIDADE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_DSC_NOM_CONJUGE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PE_IND_GUARDA_REGULARIZADA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 443
    Top = 65534
  end
  object dspAlt_CadastroDocumentos: TDataSetProvider
    DataSet = adoSP_Alt_CadastroDocumentos
    Constraints = True
    Left = 480
    Top = 65533
  end
  object spSel_ConsultaReativacaoProntuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ConsultaReativacaoProntuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1448
    Top = 32
  end
  object dspSel_ConsultaReativacaoProntuario: TDataSetProvider
    DataSet = spSel_ConsultaReativacaoProntuario
    Constraints = True
    Left = 1408
    Top = 32
  end
  object adoConnTriagem: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=Triagem;Data Source=192.168.0.20;Us' +
      'e Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;W' +
      'orkstation ID=DTI04;Use Encryption for Data=False;Tag with colum' +
      'n collation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 136
    Top = 16
  end
  object adoSP_SEL_RELATORIO_DEMANDA_TRIAGEM2017: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 1448
    Top = 104
  end
  object dspSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017: TDataSetProvider
    DataSet = adoSP_SEL_RELATORIO_DEMANDA_TRIAGEM2017
    Constraints = True
    Left = 1408
    Top = 104
  end
  object dspSel_TMPRel01: TDataSetProvider
    DataSet = adoSP_Sel_TMPRel01
    Constraints = True
    Left = 1408
    Top = 160
  end
  object dsp_Ins_TMPRel01: TDataSetProvider
    DataSet = adoIns_TMPRel01
    Constraints = True
    Left = 1408
    Top = 216
  end
  object dspExc_TMPRel01: TDataSetProvider
    DataSet = adoSP_Exc_TMPRel01
    Constraints = True
    Left = 1408
    Top = 272
  end
  object adoSP_Sel_TMPRel01: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TMPRel01;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1448
    Top = 160
  end
  object adoIns_TMPRel01: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INS_TMPRel01;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_divisao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_efetivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_integral'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nda_afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1448
    Top = 216
  end
  object adoSP_Exc_TMPRel01: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_TMPRel01;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1448
    Top = 272
  end
  object adoSP_Levantamento_Num_Atendidos_Por_Periodo: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_LEVANTAMENTO_NUM_ATENDIDOS_POR_PERIODO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalEfetivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalManha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalTarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalIntegral'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalND_Afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 904
    Top = 776
  end
  object dspSel_Levantamento_Atendidos_Por_Periodo: TDataSetProvider
    DataSet = adoSP_Levantamento_Num_Atendidos_Por_Periodo
    Constraints = True
    Left = 947
    Top = 776
  end
  object adoSP_Sel_Cad_Bairros: TADOStoredProc
    Connection = adoConnTriagem
    ProcedureName = 'sp_sel_Par_cad_bairros;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cod_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end>
    Left = 1344
    Top = 328
  end
  object dspSel_Cad_Bairros: TDataSetProvider
    DataSet = adoSP_Sel_Cad_Bairros
    Constraints = True
    Left = 1312
    Top = 328
  end
  object spSel_selecionaDadosPassesEscolares: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spSel_SelecionaDadosPasseEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1280
    Top = 792
  end
  object dspSel_SelecionaDadosPassesEscolarers: TDataSetProvider
    DataSet = spSel_selecionaDadosPassesEscolares
    Constraints = True
    Left = 1248
    Top = 792
  end
  object spSel_SelecionaDadosSeguro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spSel_SelecionaDadosSeguro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_referencia'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 1280
    Top = 784
  end
  object dspSel_SelecionaDadosSeguro: TDataSetProvider
    DataSet = spSel_SelecionaDadosSeguro
    Constraints = True
    Left = 1248
    Top = 784
  end
  object dspSel_SelecionaDadosSeguroDESLIGADOS: TDataSetProvider
    DataSet = spSel_SelecionaDadosSeguroDESLIGADOS
    Constraints = True
    Left = 1080
    Top = 776
  end
  object spSel_SelecionaDadosSeguroDESLIGADOS: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'spSel_SelecionaDadosSeguroDESLIGADOS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1120
    Top = 776
  end
end
