unit udbTriagemDeca;

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, DecaServer_TLB, StdVcl, Db, ADODB, Provider;

type
  TdbTriagemDeca = class(TRemoteDataModule, IdbTriagemDeca)
    adoConnTriagem: TADOConnection;
    spSel_Par_EstadoCivil: TADOStoredProc;
    dspSel_Par_EstadoCivil: TDataSetProvider;
    spSel_Par_Cidades: TADOStoredProc;
    dspSel_Par_Cidades: TDataSetProvider;
    spSel_Par_Nacionalidade: TADOStoredProc;
    dspSel_Par_Nacionalidade: TDataSetProvider;
    spSel_Cadastro_Bairro: TADOStoredProc;
    dspSel_Cadastro_Bairro: TDataSetProvider;
    spSel_Cadastro_Emissor: TADOStoredProc;
    dspSel_Cadastro_Emissor: TDataSetProvider;
    spSel_Par_Posicao: TADOStoredProc;
    dspSel_Par_Posicao: TDataSetProvider;
    spSel_Pesquisa_Inscritos_Triagem: TADOStoredProc;
    dspSel_Pesquisa_Inscritos_Triagem: TDataSetProvider;
    spSel_Par_RelResponsabilidade: TADOStoredProc;
    dspSel_Par_RelResponsabilidade: TDataSetProvider;
    spSel_Par_Natureza: TADOStoredProc;
    dspSel_Par_Natureza: TDataSetProvider;
    spSel_Par_Tipo_Habitacao: TADOStoredProc;
    dspSel_Par_Tipo_Habitacao: TDataSetProvider;
    spSel_Par_Infraestrutura: TADOStoredProc;
    dspSel_Par_Infrestrutura: TDataSetProvider;
    spSel_Par_InstalacaoesSanitarias: TADOStoredProc;
    dspSel_Par_InstalacoesSanitarias: TDataSetProvider;
    spSel_Par_CondicaoHabitabilidade: TADOStoredProc;
    dspSel_Par_CondicaoHabitabilidade: TDataSetProvider;
    spSel_Par_Escolaridade: TADOStoredProc;
    dspSel_Par_Escolaridade: TDataSetProvider;
    spIns_Cadastro_TriagemDeca: TADOStoredProc;
    dspIns_Cadastro_TriagemDeca: TDataSetProvider;
    spIns_Cad_Inscricao: TADOStoredProc;
    spSel_Retorna_NIF_Valido: TADOStoredProc;
    dspIns_Cad_Inscricao: TDataSetProvider;
    dspSel_Retorna_NIF_Valido: TDataSetProvider;
    spDel_Cad_Inscricao: TADOStoredProc;
    dspDel_Cad_Inscricao: TDataSetProvider;
    spSel_Par_Cadastro_Regioes: TADOStoredProc;
    dspSel_Par_Cadastro_Regioes: TDataSetProvider;
    spSel_Cadastro_Full: TADOStoredProc;
    dspSel_Cadastro_Full: TDataSetProvider;
    spIns_Par_Cad_Bairro: TADOStoredProc;
    dspIns_Par_Cad_Bairro: TDataSetProvider;
    spUpd_Par_Cad_Bairro: TADOStoredProc;
    dspUpd_Par_Cad_Bairro: TDataSetProvider;
    spUpd_Cadastro_TriagemDeca: TADOStoredProc;
    dspUpd_Cadastro_TriagemDeca: TDataSetProvider;
    spSel_PontosFicha: TADOStoredProc;
    dspSel_PontosFicha: TDataSetProvider;
    spSel_BeneficiosSociais: TADOStoredProc;
    dspSel_BeneficiosSociais: TDataSetProvider;
    spUpd_BeneficioSocial: TADOStoredProc;
    spIns_BeneficioSocial: TADOStoredProc;
    spDel_BeneficioSocial: TADOStoredProc;
    dspUpd_BeneficioSocial: TDataSetProvider;
    dspIns_BeneficioSocial: TDataSetProvider;
    dspDel_BeneficioSocial: TDataSetProvider;
    spFamilia_InsUpdDel: TADOStoredProc;
    dspFamilia_InsUpdDel: TDataSetProvider;
    dspSel_Par_Parentesco: TDataSetProvider;
    spSel_Par_Parentesco: TADOStoredProc;
    spSel_Cadastro_CompFamiliar: TADOStoredProc;
    dspSel_Cadastro_CompFamiliar: TDataSetProvider;
    spSel_Cadastro_RegAtendimento: TADOStoredProc;
    dspSel_Cadastro_RegAtendimento: TDataSetProvider;
    spIns_Cadastro_Relatorios: TADOStoredProc;
    dspIns_Cadastro_Relatorio: TDataSetProvider;
    spUpd_Cadastro_Relatorios: TADOStoredProc;
    dspUpd_Cadastro_Relatorio: TDataSetProvider;
    spSel_Cadastro_Relatorios: TADOStoredProc;
    dspSel_Cadastro_Relatorios: TDataSetProvider;
    spUpd_AtualizaUsuarioDecaRelatorio: TADOStoredProc;
    spSel_AtualizaUsuarioDecaRelatorio: TADOStoredProc;
    dspSel_AtualizaUsuariosDecaRelatorio: TDataSetProvider;
    dspUpd_AtualizaUsuariosDecaRelatorio: TDataSetProvider;
    spSel_PendenciaDocumentos: TADOStoredProc;
    spIns_PendenciaDocumentos: TADOStoredProc;
    spUpd_PendenciaDocumentos: TADOStoredProc;
    dspSel_PendenciaDocumentos: TDataSetProvider;
    dspIns_PendenciaDocumentos: TDataSetProvider;
    dspUpd_PendenciaDocumentos: TDataSetProvider;
    spUpd_AlteraStatusInscricao: TADOStoredProc;
    dspUpd_AlteraStatusInscricao: TDataSetProvider;
    spGERA_DEMANDA: TADOStoredProc;
    dspGERA_DEMANDA: TDataSetProvider;
    spINS_DEMANDA: TADOStoredProc;
    dspINS_DEMANDA: TDataSetProvider;
    spUPD_DEMANDA: TADOStoredProc;
    dspUPD_DEMANDA: TDataSetProvider;
    dspDEL_DEMANDA: TDataSetProvider;
    spDEL_DEMANDA: TADOStoredProc;
    spSel_DEMANDA: TADOStoredProc;
    dspSel_DEMANDA: TDataSetProvider;
    spMIGRACAO_TRIAGEM_DECA: TADOStoredProc;
    dspMIGRACAO_TRIAGEM_DECA: TDataSetProvider;
    spDEL_ComposicaoFamiliar: TADOStoredProc;
    dspDel_ComposicaoFamiliar: TDataSetProvider;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

class procedure TdbTriagemDeca.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

initialization
  TComponentFactory.Create(ComServer, TdbTriagemDeca,
    Class_dbTriagemDeca, ciMultiInstance, tmApartment);
end.
