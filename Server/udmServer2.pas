unit udmServer2;

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, DecaServer_TLB, StdVcl, Db, ADODB, Provider;

type
  TdbDecaServer2 = class(TRemoteDataModule, IdbDecaServer2)
    adoConn2: TADOConnection;
    spSel_SerieEscolar: TADOStoredProc;
    dspSel_SerieEscolar: TDataSetProvider;
    spInc_SerieEscolar: TADOStoredProc;
    dspInc_SerieEscolar: TDataSetProvider;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

class procedure TdbDecaServer2.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

initialization
  TComponentFactory.Create(ComServer, TdbDecaServer2,
    Class_dbDecaServer2, ciMultiInstance, tmApartment);
end.
