object dbDeca: TdbDeca
  OldCreateOrder = False
  Left = 257
  Top = 240
  Height = 640
  Width = 870
  object adoConn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=decaapp;Persist Security Info=True;' +
      'User ID=deca;Initial Catalog=DecaTeste;Data Source=SRVBACKUP;Use' +
      ' Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Wo' +
      'rkstation ID=FABIO;Use Encryption for Data=False;Tag with column' +
      ' collation when possible=False'
    DefaultDatabase = 'DecaTeste'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 24
    Top = 16
  end
  object spInc_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_senha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 124
    Top = 64
  end
  object dspInc_Usuario: TDataSetProvider
    DataSet = spInc_Usuario
    Constraints = True
    Left = 92
    Top = 64
  end
  object spExc_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 50
    Top = 345
  end
  object spAlt_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_senha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 205
    Top = 64
  end
  object dspAlt_Usuario: TDataSetProvider
    DataSet = spAlt_Usuario
    Constraints = True
    Left = 173
    Top = 64
  end
  object dspExc_Usuario: TDataSetProvider
    DataSet = spExc_Usuario
    Constraints = True
    Left = 18
    Top = 345
  end
  object spSel_Unidade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 209
    Top = 307
  end
  object dspSel_Unidade: TDataSetProvider
    DataSet = spSel_Unidade
    Constraints = True
    Left = 177
    Top = 307
  end
  object spSel_Cadastro_Move: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Move;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_PosicaoRegistro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_MATRICULA_atual'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 124
    Top = 96
  end
  object dspSel_Cadastro_Move: TDataSetProvider
    DataSet = spSel_Cadastro_Move
    Constraints = True
    Left = 92
    Top = 96
  end
  object spSel_Cadastro_Pesquisa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Pesquisa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_responsavel'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 129
    Top = 346
  end
  object dspSel_Cadastro_Pesquisa: TDataSetProvider
    DataSet = spSel_Cadastro_Pesquisa
    Constraints = True
    Left = 97
    Top = 346
  end
  object spInc_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_etnia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_calcado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tam_camisa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_pontuacao_triagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_projeto_vida'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_admissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tam_calca'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_sanguineo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_certidao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 48
    Top = 104
  end
  object spAlt_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_prontuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cartaosias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_etnia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_calcado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tam_camisa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_pontuacao_triagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_projeto_vida'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_admissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tam_calca'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_sanguineo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@pe_num_cota_passes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_cpf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_rg'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_num_certidao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 205
    Top = 97
  end
  object dspInc_Cadastro: TDataSetProvider
    DataSet = spInc_Cadastro
    Constraints = True
    Left = 16
    Top = 96
  end
  object dspAlt_Cadastro: TDataSetProvider
    DataSet = spAlt_Cadastro
    Constraints = True
    Left = 173
    Top = 97
  end
  object spAlt_Cadastro_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_periodo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_turma_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_obs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 50
    Top = 382
  end
  object dspAlt_Cadastro_Escola: TDataSetProvider
    DataSet = spAlt_Cadastro_Escola
    Constraints = True
    Left = 18
    Top = 382
  end
  object spSel_Cadastro_NovaMatricula999: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Cadastro_NovaMatricula999;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 49
    Top = 133
  end
  object dspSel_Cadastro_NovaMatricula999: TDataSetProvider
    DataSet = spSel_Cadastro_NovaMatricula999
    Constraints = True
    Left = 17
    Top = 133
  end
  object spSel_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 206
    Top = 132
  end
  object dspSel_Escola: TDataSetProvider
    DataSet = spSel_Escola
    Constraints = True
    Left = 174
    Top = 132
  end
  object dspSel_Cadastro_Programa_Social: TDataSetProvider
    DataSet = spSel_Cadastro_Programa_Social
    Constraints = True
    Left = 18
    Top = 168
  end
  object spSel_Cadastro_Programa_Social: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Programa_Social;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_cad_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_insercao1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_insercao2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_desligamento1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_desligamento2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 50
    Top = 168
  end
  object spInc_Cadastro_Programa_Social: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Programa_Social;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_quem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_insercao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 125
    Top = 132
  end
  object dspInc_Cadastro_Programa_Social: TDataSetProvider
    DataSet = spInc_Cadastro_Programa_Social
    Constraints = True
    Left = 93
    Top = 132
  end
  object spAlt_Cadastro_Programa_Social: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Programa_Social(old);1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_programa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_local'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 206
    Top = 166
  end
  object dspAlt_Cadastro_Programa_Social: TDataSetProvider
    DataSet = spAlt_Cadastro_Programa_Social
    Constraints = True
    Left = 174
    Top = 166
  end
  object spAlt_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_responsavel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_familiar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_ocupacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_dsc_local_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_val_renda_mensal'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_contato_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaridade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto_fundhas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_vinculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_ind_moracasa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 130
    Top = 382
  end
  object dspAlt_Cadastro_Familia: TDataSetProvider
    DataSet = spAlt_Cadastro_Familia
    Constraints = True
    Left = 98
    Top = 382
  end
  object spExc_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 50
    Top = 204
  end
  object spInc_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_responsavel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_nom_familiar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_documento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_ocupacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_dsc_local_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_val_renda_mensal'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_contato_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_telefone_trabalho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_dsc_escolaridade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto_fundhas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_ind_vinculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_moracasa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 126
    Top = 167
  end
  object spSel_Cadastro_Familia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_familiar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 207
    Top = 205
  end
  object dspExc_Cadastro_Familia: TDataSetProvider
    DataSet = spExc_Cadastro_Familia
    Constraints = True
    Left = 18
    Top = 204
  end
  object dspInc_Cadastro_Familia: TDataSetProvider
    DataSet = spInc_Cadastro_Familia
    Constraints = True
    Left = 94
    Top = 167
  end
  object dspSel_Cadastro_Familia: TDataSetProvider
    DataSet = spSel_Cadastro_Familia
    Constraints = True
    Left = 175
    Top = 205
  end
  object spSel_Cadastro_Contador: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Cadastro_Contador;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 210
    Top = 344
  end
  object dspSel_Cadastro_Contador: TDataSetProvider
    DataSet = spSel_Cadastro_Contador
    Constraints = True
    Left = 178
    Top = 344
  end
  object spSel_Cadastro_Familia_SomaRenda: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Cadastro_Familia_SomaRenda;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 50
    Top = 238
  end
  object dspSel_Cadastro_Familia_SomaRenda: TDataSetProvider
    DataSet = spSel_Cadastro_Familia_SomaRenda
    Constraints = True
    Left = 18
    Top = 238
  end
  object spSel_Cadastro_Atendimento_Rede: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Atendimento_Rede;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_atendimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 50
    Top = 272
  end
  object dspSel_Cadastro_Atendimento_Rede: TDataSetProvider
    DataSet = spSel_Cadastro_Atendimento_Rede
    Constraints = True
    Left = 18
    Top = 272
  end
  object spInc_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_historico'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipo_historico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_ind_motivo2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dsc_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_final_suspensao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_materia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_ind_motivo_transf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_orig_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dest_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dest_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dest_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_inicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_final'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_flg_passe'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_num_cotas_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_orig_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dest_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dat_curso_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_curso_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_estagio_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@pe_num_estagio_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 126
    Top = 205
  end
  object dspInc_Cadastro_Historico: TDataSetProvider
    DataSet = spInc_Cadastro_Historico
    Constraints = True
    Left = 94
    Top = 205
  end
  object spEXC_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_id_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 240
  end
  object dspEXC_Cadastro_Historico: TDataSetProvider
    DataSet = spEXC_Cadastro_Historico
    Constraints = True
    Left = 176
    Top = 240
  end
  object spAlt_Cadastro_Atendimento_Rede: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Atendimento_Rede;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_atendimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_atendimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_atendimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_local'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 127
    Top = 240
  end
  object dspAlt_Cadastro_Atendimento_Rede: TDataSetProvider
    DataSet = spAlt_Cadastro_Atendimento_Rede
    Constraints = True
    Left = 95
    Top = 240
  end
  object dspInc_Cadastro_Atendimento_Rede: TDataSetProvider
    DataSet = spInc_Cadastro_Atendimento_Rede
    Constraints = True
    Left = 176
    Top = 273
  end
  object spInc_Cadastro_Atendimento_Rede: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Atendimento_Rede;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_atendimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_atendimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_local'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 273
  end
  object dspSel_Cadastro_Status: TDataSetProvider
    DataSet = spSel_Cadastro_Status
    Constraints = True
    Left = 176
    Top = 417
  end
  object dspAlt_Cadastro_Tira_Suspensao: TDataSetProvider
    DataSet = spAlt_Cadastro_Tira_Suspensao
    Constraints = True
    Left = 19
    Top = 306
  end
  object spAlt_Cadastro_Tira_Suspensao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Tira_Suspensao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 51
    Top = 306
  end
  object spSel_Cadastro_Status: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Status;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 208
    Top = 417
  end
  object spSel_Historico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_HISTORICO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 129
    Top = 273
  end
  object dspSel_Historico: TDataSetProvider
    DataSet = spSel_Historico
    Constraints = True
    Left = 97
    Top = 273
  end
  object spAlt_Cadastro_Status: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Status;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 129
    Top = 306
  end
  object dspAlt_Cadastro_Status: TDataSetProvider
    DataSet = spAlt_Cadastro_Status
    Constraints = True
    Left = 97
    Top = 306
  end
  object spSel_Cadastro_Familia_Responsavel: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Familia_Responsavel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_responsavel'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 292
    Top = 240
  end
  object dspSel_Cadastro_Familia_Responsavel: TDataSetProvider
    DataSet = spSel_Cadastro_Familia_Responsavel
    Constraints = True
    Left = 260
    Top = 240
  end
  object spSel_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_evolucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 51
    Top = 416
  end
  object dspSel_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spSel_Cadastro_Evolucao_SC
    Constraints = True
    Left = 19
    Top = 416
  end
  object spAlt_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_evolucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_evolucao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 130
    Top = 417
  end
  object dspAlt_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spAlt_Cadastro_Evolucao_SC
    Constraints = True
    Left = 98
    Top = 417
  end
  object spExc_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_evolucao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 287
    Top = 65
  end
  object dspExc_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spExc_Cadastro_Evolucao_SC
    Constraints = True
    Left = 255
    Top = 65
  end
  object dspInc_Cadastro_Evolucao_SC: TDataSetProvider
    DataSet = spInc_Cadastro_Evolucao_SC
    Constraints = True
    Left = 256
    Top = 97
  end
  object spInc_Cadastro_Evolucao_SC: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Evolucao_SC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_evolucao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 288
    Top = 97
  end
  object spSel_Lista_presenca: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_Lista_presenca;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 289
    Top = 134
  end
  object dspSel_Lista_Presenca: TDataSetProvider
    DataSet = spSel_Lista_presenca
    Constraints = True
    Left = 257
    Top = 134
  end
  object spSel_Peti: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_PETI;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_modalidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 290
    Top = 168
  end
  object dspSel_Peti: TDataSetProvider
    DataSet = spSel_Peti
    Constraints = True
    Left = 257
    Top = 168
  end
  object spSel_Ordena_Matricula: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_matricula;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 291
    Top = 204
  end
  object dspSel_Orderna_Matricula: TDataSetProvider
    DataSet = spSel_Ordena_Matricula
    Constraints = True
    Left = 259
    Top = 204
  end
  object spSel_Ordena_Matricula_Adm: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Matricula_Adm;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 293
    Top = 273
  end
  object dspSel_Ordena_Matricula_Adm: TDataSetProvider
    DataSet = spSel_Ordena_Matricula_Adm
    Constraints = True
    Left = 261
    Top = 273
  end
  object spSel_Cadastro_Escolaridade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Escolaridade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 294
    Top = 307
  end
  object dspSel_Cadastro_Escolaridade: TDataSetProvider
    DataSet = spSel_Cadastro_Escolaridade
    Constraints = True
    Left = 262
    Top = 307
  end
  object spSel_Ordena_Nome_Adm: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Nome_Adm;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 296
    Top = 345
  end
  object dspSel_Ordena_Nome_Adm: TDataSetProvider
    DataSet = spSel_Ordena_Nome_Adm
    Constraints = True
    Left = 264
    Top = 345
  end
  object spSel_Ordena_Nome: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_NomeTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 296
    Top = 384
  end
  object dspSel_Ordena_Nome: TDataSetProvider
    DataSet = spSel_Ordena_Nome
    Constraints = True
    Left = 264
    Top = 384
  end
  object spSel_Cep_Adm: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CEP_Adm;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 295
    Top = 418
  end
  object spSel_Cep: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CEP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 375
    Top = 64
  end
  object dspSel_Cep_Adm: TDataSetProvider
    DataSet = spSel_Cep_Adm
    Constraints = True
    Left = 263
    Top = 418
  end
  object dspSel_Cep: TDataSetProvider
    DataSet = spSel_Cep
    Constraints = True
    Left = 343
    Top = 64
  end
  object spSel_Afastamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Afastamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 375
    Top = 96
  end
  object dspSel_Afastamento: TDataSetProvider
    DataSet = spSel_Afastamento
    Constraints = True
    Left = 343
    Top = 96
  end
  object spSel_Advertencias: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Advertencias;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 375
    Top = 136
  end
  object dspSel_Advertencias: TDataSetProvider
    DataSet = spSel_Advertencias
    Constraints = True
    Left = 343
    Top = 136
  end
  object spSel_Desligados: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Desligados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 375
    Top = 176
  end
  object dspSel_Desligados: TDataSetProvider
    DataSet = spSel_Desligados
    Constraints = True
    Left = 343
    Top = 176
  end
  object spSel_Aniversariantes_mes: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Aniversariantes_mes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 375
    Top = 208
  end
  object dspSel_Aniversariantes_mes: TDataSetProvider
    DataSet = spSel_Aniversariantes_mes
    Constraints = True
    Left = 343
    Top = 209
  end
  object spAlt_Cadastro_Status_Transf: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Status_Transf;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_ind_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 375
    Top = 240
  end
  object dspAlt_Cadastro_Status_Transf: TDataSetProvider
    DataSet = spAlt_Cadastro_Status_Transf
    Constraints = True
    Left = 343
    Top = 240
  end
  object spSel_Conta_Em_Transferencia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Conta_em_transferencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 375
    Top = 272
  end
  object dspSel_Conta_Em_Transferencia: TDataSetProvider
    DataSet = spSel_Conta_Em_Transferencia
    Constraints = True
    Left = 343
    Top = 272
  end
  object spSel_Ultima_Transferencia_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Ultima_Transferencia_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 375
    Top = 307
  end
  object dspSel_Ultima_Transferencia_Unidade: TDataSetProvider
    DataSet = spSel_Ultima_Transferencia_Unidade
    Constraints = True
    Left = 343
    Top = 307
  end
  object spSel_Ultima_Transferencia_Convenio: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ultima_Transferencia_Convenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 375
    Top = 344
  end
  object dspSel_Ultima_Transferencia_Convenio: TDataSetProvider
    DataSet = spSel_Ultima_Transferencia_Convenio
    Constraints = True
    Left = 343
    Top = 344
  end
  object spSel_Transferencias: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Transferencias;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 375
    Top = 384
  end
  object dspSel_Transferencias: TDataSetProvider
    DataSet = spSel_Transferencias
    Constraints = True
    Left = 343
    Top = 384
  end
  object spAlt_Cadastro_Muda_Matricula: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Muda_Matricula;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula_atual'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_matricula_novo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 376
    Top = 418
  end
  object dspAlt_Cadastro_Muda_Matricula: TDataSetProvider
    DataSet = spAlt_Cadastro_Muda_Matricula
    Constraints = True
    Left = 343
    Top = 418
  end
  object spSel_Cadastro_Historico_Periodo: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Historico_Periodo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 52
    Top = 456
  end
  object dspSel_Cadastro_Historico_Periodo: TDataSetProvider
    DataSet = spSel_Cadastro_Historico_Periodo
    Constraints = True
    Left = 19
    Top = 455
  end
  object spSel_Cadastro_Evolucao_Periodo: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Evolucao_Periodo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 128
    Top = 456
  end
  object dspSel_Cadastro_Evolucao_Periodo: TDataSetProvider
    DataSet = spSel_Cadastro_Evolucao_Periodo
    Constraints = True
    Left = 97
    Top = 456
  end
  object spSel_Indicadores: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Indicadores;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 456
  end
  object dspSel_Indicadores: TDataSetProvider
    DataSet = spSel_Indicadores
    Constraints = True
    Left = 176
    Top = 456
  end
  object spSel_Total_Familia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Familia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 64
  end
  object dspSel_Total_Familia: TDataSetProvider
    DataSet = spSel_Total_Familia
    Constraints = True
    Left = 435
    Top = 64
  end
  object spSel_Total_Crianca: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Criancas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 97
  end
  object dspSel_Total_Crianca: TDataSetProvider
    DataSet = spSel_Total_Crianca
    Constraints = True
    Left = 435
    Top = 97
  end
  object spSel_Total_Adolescente: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Adolescente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 136
  end
  object spSel_Total_Profissionais: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Profissionais;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 176
  end
  object spSel_Total_Projeto_de_Vida: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Projeto_de_Vida;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 208
  end
  object dspSel_Total_Adolescente: TDataSetProvider
    DataSet = spSel_Total_Adolescente
    Constraints = True
    Left = 435
    Top = 136
  end
  object dspSel_Total_Profissionais: TDataSetProvider
    DataSet = spSel_Total_Profissionais
    Constraints = True
    Left = 435
    Top = 176
  end
  object dspSel_Total_Projeto_de_Vida: TDataSetProvider
    DataSet = spSel_Total_Projeto_de_Vida
    Constraints = True
    Left = 435
    Top = 208
  end
  object spSel_Fam_Visita_Domiciliar: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Visita_Domiciliar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 240
  end
  object dspSel_Fam_Visita_Domiciliar: TDataSetProvider
    DataSet = spSel_Fam_Visita_Domiciliar
    Constraints = True
    Left = 435
    Top = 240
  end
  object spSel_Fam_Atendimento_Individual: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Atendimento_Individual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 272
  end
  object dspSel_Fam_Atendimento_Individual: TDataSetProvider
    DataSet = spSel_Fam_Atendimento_Individual
    Constraints = True
    Left = 435
    Top = 272
  end
  object spSel_Fam_Anamnese: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Anamnese;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 306
  end
  object dspSEL_Fam_Anamnese: TDataSetProvider
    DataSet = spSel_Fam_Anamnese
    Constraints = True
    Left = 435
    Top = 307
  end
  object spSel_Fam_Abordagem_Grupal: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Abordagem_Grupal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 344
  end
  object dspSEL_Fam_Abordagem_Grupal: TDataSetProvider
    DataSet = spSel_Fam_Abordagem_Grupal
    Constraints = True
    Left = 435
    Top = 344
  end
  object spSel_Fam_Integracao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Integracao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 467
    Top = 384
  end
  object dspSEL_Fam_Integracao: TDataSetProvider
    DataSet = spSel_Fam_Integracao
    Constraints = True
    Left = 435
    Top = 384
  end
  object spSel_Fam_Reuniao_Informativa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fam_Reuniao_Informativa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 558
    Top = 221
  end
  object dspSEL_Fam_Reuniao_Informativa: TDataSetProvider
    DataSet = spSel_Fam_Reuniao_Informativa
    Constraints = True
    Left = 526
    Top = 65
  end
  object spSel_Criadol_Atendimento_Individual: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Criadol_Atendimento_Individual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 99
  end
  object dspSEL_Criadol_Atendimento_Individual: TDataSetProvider
    DataSet = spSel_Criadol_Atendimento_Individual
    Constraints = True
    Left = 526
    Top = 99
  end
  object spSel_Criadol_Abordagem_Grupal: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Criadol_Abordagem_Grupal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 294
  end
  object dspSEL_Criadol_Abordagem_Grupal: TDataSetProvider
    DataSet = spSel_Criadol_Abordagem_Grupal
    Constraints = True
    Left = 526
    Top = 137
  end
  object spSel_Criadol_Treinamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Criadol_Treinamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 176
  end
  object dspSEL_Criadol_Treinamento: TDataSetProvider
    DataSet = spSel_Criadol_Treinamento
    Constraints = True
    Left = 526
    Top = 176
  end
  object spSel_Profissional_Comunidade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Profissional_Comunidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 209
  end
  object dspSEL_Profissional_Comunidade: TDataSetProvider
    DataSet = spSel_Profissional_Comunidade
    Constraints = True
    Left = 526
    Top = 209
  end
  object spSel_RelDiversos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_RelDiversos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 558
    Top = 240
  end
  object dspSEL_RelDiversos: TDataSetProvider
    DataSet = spSel_RelDiversos
    Constraints = True
    Left = 525
    Top = 240
  end
  object spSel_Cursos_Capacitacao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cursos_Capacitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 272
  end
  object dspSEL_Cursos_Capacitacao: TDataSetProvider
    DataSet = spSel_Cursos_Capacitacao
    Constraints = True
    Left = 526
    Top = 272
  end
  object spSel_Atividades_Extras: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Atividades_Extras;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 560
    Top = 306
  end
  object dspSEL_Atividades_Extras: TDataSetProvider
    DataSet = spSel_Atividades_Extras
    Constraints = True
    Left = 527
    Top = 306
  end
  object spSel_Visitas_Diversas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Visitas_Diversas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 560
    Top = 343
  end
  object dspSEL_Visitas_Diversas: TDataSetProvider
    DataSet = spSel_Visitas_Diversas
    Constraints = True
    Left = 527
    Top = 343
  end
  object spSel_Supervisao_Estagio: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Supervisao_Estagio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 468
    Top = 419
  end
  object dspSEL_Supervisao_Estagio: TDataSetProvider
    DataSet = spSel_Supervisao_Estagio
    Constraints = True
    Left = 436
    Top = 418
  end
  object spSel_Reunioes_Tecnicas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Reunioes_Tecnicas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 384
  end
  object dspSel_Reunioes_Tecnicas: TDataSetProvider
    DataSet = spSel_Reunioes_Tecnicas
    Constraints = True
    Left = 526
    Top = 384
  end
  object spSel_Discussao_Casos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Discussao_Casos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 296
    Top = 456
  end
  object spSel_Projeto_Vida_Visita: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Projeto_Vida_Visita;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 376
    Top = 456
  end
  object spSel_Projeto_Vida_Abordagem: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Projeto_Vida_Abordagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 456
  end
  object spSel_Projeto_Vida_AtIndividual: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Projeto_Vida_AtIndividual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 559
    Top = 416
  end
  object dspSel_Discussao_Casos: TDataSetProvider
    DataSet = spSel_Discussao_Casos
    Constraints = True
    Left = 264
    Top = 456
  end
  object dspSel_Projeto_Vida_Visita: TDataSetProvider
    DataSet = spSel_Projeto_Vida_Visita
    Constraints = True
    Left = 343
    Top = 456
  end
  object dspSel_Projeto_Vida_Abordagem: TDataSetProvider
    DataSet = spSel_Projeto_Vida_Abordagem
    Constraints = True
    Left = 439
    Top = 456
  end
  object dspSel_Projeto_Vida_AtIndividual: TDataSetProvider
    DataSet = spSel_Projeto_Vida_AtIndividual
    Constraints = True
    Left = 527
    Top = 416
  end
  object dspSel_Usuario: TDataSetProvider
    DataSet = spSel_Usuario
    Constraints = True
    Left = 16
    Top = 63
  end
  object spSel_Usuario: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 49
    Top = 64
  end
  object spSel_Lista_PresencaTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_Lista_PresencaTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 64
  end
  object dspSel_Lista_PresencaTodos: TDataSetProvider
    DataSet = spSel_Lista_PresencaTodos
    Constraints = True
    Left = 607
    Top = 64
  end
  object spSel_PetiTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_PetiTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 640
    Top = 97
  end
  object dspSel_PetiTodos: TDataSetProvider
    DataSet = spSel_PetiTodos
    Constraints = True
    Left = 607
    Top = 98
  end
  object spSel_Ordena_MatriculaTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_matriculaTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 136
  end
  object dspSel_Ordena_MatriculaTodos: TDataSetProvider
    DataSet = spSel_Ordena_MatriculaTodos
    Constraints = True
    Left = 607
    Top = 136
  end
  object spSel_Ordena_Matricula_AdmTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Matricula_AdmTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 176
  end
  object dspSel_Ordena_Matricula_AdmTodos: TDataSetProvider
    DataSet = spSel_Ordena_Matricula_AdmTodos
    Constraints = True
    Left = 607
    Top = 176
  end
  object spSel_Ordena_NomeTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_NomeTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 208
  end
  object dspSel_Ordena_NomeTodos: TDataSetProvider
    DataSet = spSel_Ordena_NomeTodos
    Constraints = True
    Left = 607
    Top = 208
  end
  object spSel_Ordena_Nome_AdmTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Ordena_Nome_AdmTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 240
  end
  object dspSel_Ordena_Nome_AdmTodos: TDataSetProvider
    DataSet = spSel_Ordena_Nome_AdmTodos
    Constraints = True
    Left = 607
    Top = 240
  end
  object spSel_CepTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_Sel_CEPTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 272
  end
  object spSel_Cep_AdmTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CEP_AdmTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 304
  end
  object dspSel_CepTodos: TDataSetProvider
    DataSet = spSel_CepTodos
    Constraints = True
    Left = 607
    Top = 272
  end
  object dspSel_Cep_AdmTodos: TDataSetProvider
    DataSet = spSel_Cep_AdmTodos
    Constraints = True
    Left = 607
    Top = 304
  end
  object spSel_Aniversariantes_MesTodos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Aniversariantes_mesTodos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 344
  end
  object dspSel_Aniversariantes_MesTodos: TDataSetProvider
    DataSet = spSel_Aniversariantes_MesTodos
    Constraints = True
    Left = 607
    Top = 344
  end
  object spExc_DadosGrafico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_DadosGrafico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 51
    Top = 496
  end
  object dspExc_DadosGrafico: TDataSetProvider
    DataSet = spExc_DadosGrafico
    Constraints = True
    Left = 19
    Top = 496
  end
  object spInc_DadosGrafico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_DadosGrafico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_total'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_num_total'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 495
  end
  object dspInc_DadosGrafico: TDataSetProvider
    DataSet = spInc_DadosGrafico
    Constraints = True
    Left = 96
    Top = 495
  end
  object spSel_MeninosManha: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninosManha;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 495
  end
  object spSel_MeninosTarde: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninosTarde;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 296
    Top = 495
  end
  object spSel_MeninasManha: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninasManha;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 375
    Top = 495
  end
  object spSel_MeninasTarde: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MeninasTarde;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 495
  end
  object spSel_TotalUnidade: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TotalUnidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 647
    Top = 503
  end
  object dspSel_MeninosManha: TDataSetProvider
    DataSet = spSel_MeninosManha
    Constraints = True
    Left = 176
    Top = 495
  end
  object dspSel_MeninosTarde: TDataSetProvider
    DataSet = spSel_MeninosTarde
    Constraints = True
    Left = 264
    Top = 496
  end
  object dspSel_MeninasManha: TDataSetProvider
    DataSet = spSel_MeninasManha
    Constraints = True
    Left = 343
    Top = 496
  end
  object dspSel_MeninasTarde: TDataSetProvider
    DataSet = spSel_MeninasTarde
    Constraints = True
    Left = 303
    Top = 496
  end
  object dspSel_TotalUnidade: TDataSetProvider
    DataSet = spSel_TotalUnidade
    Constraints = True
    Left = 615
    Top = 504
  end
  object spSel_DadosGrafico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_DadosGrafico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 728
    Top = 64
  end
  object dspSel_DadosGrafico: TDataSetProvider
    DataSet = spSel_DadosGrafico
    Constraints = True
    Left = 696
    Top = 64
  end
  object spSel_Mostra_a_Transferir: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Mostra_a_Transferir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 488
  end
  object dspSel_Mostra_a_Transferir: TDataSetProvider
    DataSet = spSel_Mostra_a_Transferir
    Constraints = True
    Left = 502
    Top = 489
  end
  object spSel_Transferencias_Encaminhadas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Transferencias_Encaminhadas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_historico2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 278
    Top = 496
  end
  object dspSel_Transferencias_Encaminhadas: TDataSetProvider
    DataSet = spSel_Transferencias_Encaminhadas
    Constraints = True
    Left = 439
    Top = 393
  end
  object spSel_AtivosSuspensosAfastados: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_AtivosSuspensosAfastados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 729
    Top = 272
  end
  object dspSel_AtivosSuspensosAfastados: TDataSetProvider
    DataSet = spSel_AtivosSuspensosAfastados
    Constraints = True
    Left = 697
    Top = 272
  end
  object spExc_Idades: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Idade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 730
    Top = 136
  end
  object spSel_Idades: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Idades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 729
    Top = 96
  end
  object spInc_Idades: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_Idade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_idade_anos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_idade_meses'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 732
    Top = 239
  end
  object dspInc_Idades: TDataSetProvider
    DataSet = spInc_Idades
    Constraints = True
    Left = 698
    Top = 240
  end
  object dspExc_Idades: TDataSetProvider
    DataSet = spExc_Idades
    Constraints = True
    Left = 698
    Top = 136
  end
  object dspSel_Idades: TDataSetProvider
    DataSet = spSel_Idades
    Constraints = True
    Left = 697
    Top = 96
  end
  object spSel_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_id_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 384
  end
  object dspSel_Cadastro_Historico: TDataSetProvider
    DataSet = spSel_Cadastro_Historico
    Constraints = True
    Left = 178
    Top = 384
  end
  object spAlt_Cadastro_Historico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Historico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_id_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_historico'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_tipo_historico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_projeto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_ind_motivo2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dsc_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_comentario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dat_final_suspensao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_materia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_ind_motivo_transf'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_orig_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_orig_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_orig_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_orig_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_dest_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dest_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dest_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dest_banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_inicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_hora_final'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_flg_passe'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_num_cotas_passe'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 7
        Value = Null
      end
      item
        Name = '@pe_orig_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dest_funcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_dat_curso_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_curso_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_estagio_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_estagio_duracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_qtd_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_horas'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 728
    Top = 208
  end
  object dspAlt_Cadastro_Historico: TDataSetProvider
    DataSet = spAlt_Cadastro_Historico
    Constraints = True
    Left = 696
    Top = 208
  end
  object spSel_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 729
    Top = 176
  end
  object dspSel_Cadastro_Frequencia: TDataSetProvider
    DataSet = spSel_Cadastro_Frequencia
    Constraints = True
    Left = 697
    Top = 176
  end
  object spAlt_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dia01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia13'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia14'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia15'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia16'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia17'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia18'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia19'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia20'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia21'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia22'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia23'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia24'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia25'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia26'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia27'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia28'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia29'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia30'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_dia31'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsr_acumulado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 56
    Top = 544
  end
  object dspAlt_Cadastro_Frequencia: TDataSetProvider
    DataSet = spAlt_Cadastro_Frequencia
    Constraints = True
    Left = 24
    Top = 544
  end
  object spInc_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_dia01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia13'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia14'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia15'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia16'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia17'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia18'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia19'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia20'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia21'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia22'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia23'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia24'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia25'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia26'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia27'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia28'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia29'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia30'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_flg_dia31'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia06'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia07'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia08'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia09'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia13'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia14'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia15'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia16'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia17'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia18'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia19'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia20'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia21'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia22'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia23'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia24'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia25'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia26'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia27'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia28'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia29'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia30'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_semana_dia31'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsr_acumulado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_exportou'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 544
  end
  object dspInc_Cadastro_Frequencia: TDataSetProvider
    DataSet = spInc_Cadastro_Frequencia
    Constraints = True
    Left = 96
    Top = 544
  end
  object spExc_Cadastro_Frequencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Frequencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 736
    Top = 520
  end
  object dspExc_Cadastro_Frequencia: TDataSetProvider
    DataSet = spExc_Cadastro_Frequencia
    Constraints = True
    Left = 704
    Top = 520
  end
  object spSel_Cadastro: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 572
    Top = 441
  end
  object dspSel_Cadastro: TDataSetProvider
    DataSet = spSel_Cadastro
    Constraints = True
    Left = 540
    Top = 441
  end
  object spSel_Cadastro_Turmas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Turmas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 544
  end
  object dspSel_Cadastro_Turmas: TDataSetProvider
    DataSet = spSel_Cadastro_Turmas
    Constraints = True
    Left = 176
    Top = 544
  end
  object spInc_Cadastro_Turmas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Turmas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_professor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 544
  end
  object dspInc_Cadastro_Turmas: TDataSetProvider
    DataSet = spInc_Cadastro_Turmas
    Constraints = True
    Left = 256
    Top = 544
  end
  object spAlt_Cadastro_Turmas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Turmas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_professor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_periodo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 544
  end
  object dspAlt_Cadastro_Turmas: TDataSetProvider
    DataSet = spAlt_Cadastro_Turmas
    Constraints = True
    Left = 327
    Top = 544
  end
  object dspSel_Idades2: TDataSetProvider
    DataSet = spSel_Idades2
    Constraints = True
    Left = 451
    Top = 685
  end
  object spSel_Idades2: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Idades2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 483
    Top = 685
  end
  object spSel_Biometricos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Biometricos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 727
    Top = 320
  end
  object dspSel_Biometricos: TDataSetProvider
    DataSet = spSel_Biometricos
    Constraints = True
    Left = 695
    Top = 320
  end
  object spInc_Biometricos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Biometricos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_imc'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao_imc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_flg_problema_saude'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_dsc_problema_saude'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 503
    Top = 441
  end
  object dspInc_Biometricos: TDataSetProvider
    DataSet = spInc_Biometricos
    Constraints = True
    Left = 471
    Top = 441
  end
  object spAlt_Cadastro_Dados_Biometricos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Dados_Biometricos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end>
    Left = 558
    Top = 384
  end
  object dspAlt_Cadastro_Dados_Biometricos: TDataSetProvider
    DataSet = spAlt_Cadastro_Dados_Biometricos
    Constraints = True
    Left = 526
    Top = 384
  end
  object spSel_Frequencia_Ano: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Frequencia_Ano;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 24
  end
  object spSel_Frequencia_Mes: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Frequencia_Mes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 24
  end
  object dspSel_Frequencia_Ano: TDataSetProvider
    DataSet = spSel_Frequencia_Ano
    Constraints = True
    Left = 96
    Top = 24
  end
  object dspSel_Frequencia_Mes: TDataSetProvider
    DataSet = spSel_Frequencia_Mes
    Constraints = True
    Left = 176
    Top = 24
  end
  object spSel_Mensagem: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Mensagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_para'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 300
    Top = 24
  end
  object dspSel_Mensagem: TDataSetProvider
    DataSet = spSel_Mensagem
    Constraints = True
    Left = 268
    Top = 24
  end
  object spInc_Mensagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Mensagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_mensagem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_para'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 383
    Top = 24
  end
  object dspInc_Mensagem: TDataSetProvider
    DataSet = spInc_Mensagem
    Constraints = True
    Left = 351
    Top = 24
  end
  object spSel_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 471
    Top = 24
  end
  object spInc_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_img_foto'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 559
    Top = 24
  end
  object spExc_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 639
    Top = 24
  end
  object dspSel_Cadastro_Foto: TDataSetProvider
    DataSet = spSel_Cadastro_Foto
    Constraints = True
    Left = 439
    Top = 24
  end
  object dspInc_Cadastro_Foto: TDataSetProvider
    DataSet = spInc_Cadastro_Foto
    Constraints = True
    Left = 527
    Top = 24
  end
  object dspExc_Cadastro_Foto: TDataSetProvider
    DataSet = spExc_Cadastro_Foto
    Constraints = True
    Left = 607
    Top = 24
  end
  object spAlt_Cadastro_Foto: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_Foto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_img_foto'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 727
    Top = 24
  end
  object dspAlt_Cadastro_Foto: TDataSetProvider
    DataSet = spAlt_Cadastro_Foto
    Constraints = True
    Left = 695
    Top = 24
  end
  object dspSel_Acompanhamento: TDataSetProvider
    DataSet = spSel_Acompanhamento
    Constraints = True
    Left = 503
    Top = 537
  end
  object spSel_Acompanhamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 537
  end
  object spExc_Acompanhamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompanhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 397
    Top = 24
  end
  object dspExc_Acompanhamento: TDataSetProvider
    DataSet = spExc_Acompanhamento
    Constraints = True
    Left = 365
    Top = 25
  end
  object dspInc_Acompanhamento: TDataSetProvider
    DataSet = spInc_Acompanhamento
    Constraints = True
    Left = 702
    Top = 424
  end
  object spInc_Acompanhamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_dias_letivos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_escola_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 734
    Top = 424
  end
  object spAlt_Status_Usuario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Status_Usuario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 330
    Top = 694
  end
  object dspAlt_Status_Usuario: TDataSetProvider
    DataSet = spAlt_Status_Usuario
    Constraints = True
    Left = 298
    Top = 694
  end
  object spAlt_Acompanhamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Acompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dias_letivos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompanhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 639
    Top = 424
  end
  object dspAlt_Acompanhamento: TDataSetProvider
    DataSet = spAlt_Acompanhamento
    Constraints = True
    Left = 607
    Top = 424
  end
  object dspSel_EscolaAcompanhamento: TDataSetProvider
    DataSet = spSel_EscolaAcompanhamento
    Constraints = True
    Left = 370
    Top = 585
  end
  object spSel_EscolaAcompanhamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_EscolaAcompanhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 402
    Top = 585
  end
  object spSel_Empresa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 57
    Top = 592
  end
  object spAlt_Empresa: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_empresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_contato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_fone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_as_social'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 592
  end
  object spInc_Empresa: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_empresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_contato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_fone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_fone3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_as_social'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 592
  end
  object dspSel_Empresa: TDataSetProvider
    DataSet = spSel_Empresa
    Constraints = True
    Left = 25
    Top = 592
  end
  object dspAlt_Empresa: TDataSetProvider
    DataSet = spAlt_Empresa
    Constraints = True
    Left = 97
    Top = 592
  end
  object dspInc_Empresa: TDataSetProvider
    DataSet = spInc_Empresa
    Constraints = True
    Left = 177
    Top = 592
  end
  object spSel_Total_Adolescentes_Empresa: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Total_Adolescentes_Empresa;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 592
  end
  object dspSel_Total_Adolescentes_Empresa: TDataSetProvider
    DataSet = spSel_Total_Adolescentes_Empresa
    Constraints = True
    Left = 256
    Top = 592
  end
  object spSel_Ficha_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Ficha_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 359
    Top = 592
  end
  object dspSel_Ficha_Cadastro: TDataSetProvider
    DataSet = spSel_Ficha_Cadastro
    Constraints = True
    Left = 327
    Top = 592
  end
  object spSel_Suspensos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Suspensos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_historico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 403
    Top = 537
  end
  object dspSel_Suspensos: TDataSetProvider
    DataSet = spSel_Suspensos
    Constraints = True
    Left = 371
    Top = 537
  end
  object dspSel_Cadastro_NovaMatricula777: TDataSetProvider
    DataSet = spSel_Cadastro_NovaMatricula777
    Constraints = True
    Left = 168
    Top = 448
  end
  object spSel_Cadastro_NovaMatricula777: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Cadastro_NovaMatricula777;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 200
    Top = 448
  end
  object spSel_Versao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_VERSAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 732
    Top = 383
  end
  object dspSel_Versao: TDataSetProvider
    DataSet = spSel_Versao
    Constraints = True
    Left = 700
    Top = 383
  end
  object spSel_Transferencias_para_DRH: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Transferencias_DRH;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 471
    Top = 632
  end
  object dspSel_Transferencias_para_DRH: TDataSetProvider
    DataSet = spSel_Transferencias_para_DRH
    Constraints = True
    Left = 503
    Top = 632
  end
  object spSel_FichaCadastro: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_FichaCadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 96
    Top = 674
  end
  object dspSel_FichaCadastro: TDataSetProvider
    DataSet = spSel_FichaCadastro
    Constraints = True
    Left = 131
    Top = 674
  end
  object spSel_Secao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Secao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_secao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 59
    Top = 686
  end
  object dspSel_Secao: TDataSetProvider
    DataSet = spSel_Secao
    Constraints = True
    Left = 25
    Top = 686
  end
  object dspInc_Unidade: TDataSetProvider
    DataSet = spInc_Unidade
    Constraints = True
    Left = 24
    Top = 640
  end
  object spInc_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_gestor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_telefone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_telefone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_capacidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 59
    Top = 640
  end
  object spAlt_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Unidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_ind_regiao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ccusto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_gestor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_num_telefone1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_num_telefone2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_num_capacidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data_altera'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 128
    Top = 640
  end
  object dspAlt_Unidade: TDataSetProvider
    DataSet = spAlt_Unidade
    Constraints = True
    Left = 96
    Top = 640
  end
  object spInc_Secao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Secao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 694
    Top = 444
  end
  object dspInc_Secao: TDataSetProvider
    DataSet = spInc_Secao
    Constraints = True
    Left = 536
    Top = 457
  end
  object dspAlt_Secao: TDataSetProvider
    DataSet = spAlt_Secao
    Constraints = True
    Left = 176
    Top = 640
  end
  object spAlt_Secao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Secao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_secao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 210
    Top = 640
  end
  object dspSel_Psicopedagogico: TDataSetProvider
    DataSet = spSel_Psicopedagogico
    Constraints = True
    Left = 248
    Top = 321
  end
  object dspInc_Psicopedagogico: TDataSetProvider
    DataSet = spInc_Psicopedagogico
    Constraints = True
    Left = 327
    Top = 640
  end
  object spSel_Psicopedagogico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Psicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 288
    Top = 640
  end
  object spInc_Psicopedagogico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Psicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dsc_oficina'
        Attributes = [paNullable]
        DataType = ftString
        Size = 25
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_professor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_professor_resp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_nom_profissional_resp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 640
  end
  object spSel_ItensPsicopedagogico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ItensPsicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_psico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 431
    Top = 537
  end
  object spInc_ItensPsicopedagogico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ItensPsicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_psico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 431
    Top = 585
  end
  object dspSel_ItensPsicopedagogico: TDataSetProvider
    DataSet = spSel_ItensPsicopedagogico
    Constraints = True
    Left = 399
    Top = 537
  end
  object dspInc_ItensPsicopedagogico: TDataSetProvider
    DataSet = spInc_ItensPsicopedagogico
    Constraints = True
    Left = 399
    Top = 585
  end
  object spSel_ObsAprendizagem: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ObsAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_observacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 212
    Top = 688
  end
  object spInc_ObsAprendizagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ObsAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_aspectosgerais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_expressaoral'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_matematica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_especifica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 289
    Top = 688
  end
  object dspSel_ObsAprendizagem: TDataSetProvider
    DataSet = spSel_ObsAprendizagem
    Constraints = True
    Left = 176
    Top = 688
  end
  object dspInc_ObsAprendizagem: TDataSetProvider
    DataSet = spInc_ObsAprendizagem
    Constraints = True
    Left = 256
    Top = 688
  end
  object spSel_Perfil: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Perfil;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 673
  end
  object dspSel_Perfil: TDataSetProvider
    DataSet = spSel_Perfil
    Constraints = True
    Left = 503
    Top = 673
  end
  object spTotaliza_por_Unidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_POR_UNIDADE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_IND_TIPO_UNIDADE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_IND_TIPO_GESTAO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 688
  end
  object dspSel_TotalizaPorUnidade: TDataSetProvider
    DataSet = spTotaliza_por_Unidade
    Constraints = True
    Left = 327
    Top = 688
  end
  object spInc_Perfil: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Perfil;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_perfil'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 470
    Top = 585
  end
  object dspInc_Perfil: TDataSetProvider
    DataSet = spInc_Perfil
    Constraints = True
    Left = 503
    Top = 585
  end
  object dspSel_CadastroAMB: TDataSetProvider
    DataSet = spSel_CadastroAMB
    Constraints = True
    Left = 24
    Top = 734
  end
  object spSel_CadastroAMB: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CadastroAMB;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 59
    Top = 734
  end
  object spAlt_ObsAprendizagem: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ObsAprendizagem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_observacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_aspectos_gerais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_expressaoral'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_matematica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_dsc_especifica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 406
    Top = 673
  end
  object dspAlt_ObsAprendizagem: TDataSetProvider
    DataSet = spAlt_ObsAprendizagem
    Constraints = True
    Left = 374
    Top = 673
  end
  object dspDesabilitaTriggers_Cadastro: TDataSetProvider
    DataSet = spDesabilitaTriggers_Cadastro
    Constraints = True
    Left = 96
    Top = 734
  end
  object dspHabilitaTriggers_Cadastro: TDataSetProvider
    DataSet = spHabilitaTriggers_Cadastro
    Constraints = True
    Left = 176
    Top = 735
  end
  object spDesabilitaTriggers_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_DesabilitaTriggers_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 129
    Top = 734
  end
  object spHabilitaTriggers_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_HabilitaTriggers_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 211
    Top = 736
  end
  object spDesabilitaTriggers_Responsavel: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_DesabilitaTriggers_Responsavel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 736
  end
  object dspDesabilitaTriggers_Responsavel: TDataSetProvider
    DataSet = spDesabilitaTriggers_Responsavel
    Constraints = True
    Left = 256
    Top = 736
  end
  object dspHabilitaTriggers_Responsavel: TDataSetProvider
    DataSet = spHabilitaTriggers_Responsavel
    Constraints = True
    Left = 327
    Top = 633
  end
  object spHabilitaTriggers_Responsavel: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_HabilitaTriggers_Responsavel;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 633
  end
  object spSel_Fatores_Intervencao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Fatores_Intervencao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_fator'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 431
    Top = 633
  end
  object dspSel_Fatores_Intervencao: TDataSetProvider
    DataSet = spSel_Fatores_Intervencao
    Constraints = True
    Left = 399
    Top = 633
  end
  object spInc_Fatores_Intervencao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Fatores_Intervencao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_fator'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 431
    Top = 673
  end
  object dspInc_Fatores_Intervencao: TDataSetProvider
    DataSet = spInc_Fatores_Intervencao
    Constraints = True
    Left = 399
    Top = 673
  end
  object spAlt_ItensPsicopedagogico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ItensPsicopedagogico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_data_altera'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 59
    Top = 776
  end
  object dspAlt_ItensPsicopedagogico: TDataSetProvider
    DataSet = spAlt_ItensPsicopedagogico
    Constraints = True
    Left = 25
    Top = 776
  end
  object spSel_HistoricoEscola: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_HistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 212
    Top = 776
  end
  object spInc_HistoricoEscola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_HistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 776
  end
  object spAlt_HistoricoEscola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_HistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 673
  end
  object dspSel_HistoricoEscola: TDataSetProvider
    DataSet = spSel_HistoricoEscola
    Constraints = True
    Left = 176
    Top = 776
  end
  object dspInc_HistoricoEscola: TDataSetProvider
    DataSet = spInc_HistoricoEscola
    Constraints = True
    Left = 256
    Top = 776
  end
  object dspAlt_HistoricoEscola: TDataSetProvider
    DataSet = spAlt_HistoricoEscola
    Constraints = True
    Left = 327
    Top = 673
  end
  object spSel_ProgSocial: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_progsocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end>
    Left = 431
    Top = 489
  end
  object dspSel_ProgSocial: TDataSetProvider
    DataSet = spSel_ProgSocial
    Constraints = True
    Left = 399
    Top = 489
  end
  object dspInc_ProgSocial: TDataSetProvider
    DataSet = spInc_ProgSocial
    Constraints = True
    Left = 370
    Top = 633
  end
  object spInc_ProgSocial: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_progsocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dsc_origem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 402
    Top = 633
  end
  object spSel_Calendario: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Calendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_calendario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 736
  end
  object dspSel_Calendario: TDataSetProvider
    DataSet = spSel_Calendario
    Constraints = True
    Left = 327
    Top = 736
  end
  object spInc_Calendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Calendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_calendario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 737
    Top = 616
  end
  object dspInc_Calendario: TDataSetProvider
    DataSet = spInc_Calendario
    Constraints = True
    Left = 705
    Top = 616
  end
  object spAlt_Calendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Calendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_calendario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_atual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 735
    Top = 568
  end
  object dspAlt_Calendario: TDataSetProvider
    DataSet = spAlt_Calendario
    Constraints = True
    Left = 703
    Top = 568
  end
  object spSel_ItensCalendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ItensCalendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_feriado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 568
    Top = 384
  end
  object dspSel_ItensCalendario: TDataSetProvider
    DataSet = spSel_ItensCalendario
    Constraints = True
    Left = 535
    Top = 384
  end
  object spInc_ItensCalendario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ItensCalendario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_calendario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_feriado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 439
    Top = 632
  end
  object dspInc_ItensCalendario: TDataSetProvider
    DataSet = spInc_ItensCalendario
    Constraints = True
    Left = 407
    Top = 632
  end
  object spContaFeriados: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ContaFeriado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_semana'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 647
    Top = 536
  end
  object dspConta_Feriados: TDataSetProvider
    DataSet = spContaFeriados
    Constraints = True
    Left = 615
    Top = 536
  end
  object spAlt_Secao_Cadastro: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Secao_Cadastro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 575
    Top = 536
  end
  object dspAlt_Secao_Cadastro: TDataSetProvider
    DataSet = spAlt_Secao_Cadastro
    Constraints = True
    Left = 543
    Top = 536
  end
  object dspAlt_UnidadesNutricao: TDataSetProvider
    DataSet = spAlt_UnidadesNutricao
    Constraints = True
    Left = 703
    Top = 472
  end
  object spAlt_UnidadesNutricao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_UnidadesNutricao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_gestao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 735
    Top = 472
  end
  object dspInc_Lanc_Fat_Int: TDataSetProvider
    DataSet = spInc_Lanc_Fat_Int
    Constraints = True
    Left = 26
    Top = 827
  end
  object spInc_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_LANC_FAT_INT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_ind_vinculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dat_entrevista'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_meta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 58
    Top = 827
  end
  object spAlt_Status_ProgSocial: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Status_ProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_cad_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_desligamento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_insercao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 128
    Top = 776
  end
  object dspAlt_Status_ProgSocial: TDataSetProvider
    DataSet = spAlt_Status_ProgSocial
    Constraints = True
    Left = 96
    Top = 776
  end
  object dspSel_Classificacao_Atendimento: TDataSetProvider
    DataSet = spSel_Classificacao_Atendimento
    Constraints = True
    Left = 703
    Top = 680
  end
  object spSel_Classificacao_Atendimento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Classificacao_Atendimento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_crianca'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_flg_adolescente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_flg_familia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pe_flg_profissional'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 735
    Top = 680
  end
  object dspInc_Indicador: TDataSetProvider
    DataSet = spInc_Indicador
    Constraints = True
    Left = 327
    Top = 776
  end
  object spInc_Indicador: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Indicador;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ocorrencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 359
    Top = 776
  end
  object spTotaliza_Atendimento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_ATENDIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_indicador'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_classificacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_registro2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 423
    Top = 736
  end
  object dspTotaliza_Atendimento: TDataSetProvider
    DataSet = spTotaliza_Atendimento
    Constraints = True
    Left = 393
    Top = 736
  end
  object dspSel_Lanc_Fat_Int: TDataSetProvider
    DataSet = spSel_Lancamento_Fatores_Intervencao
    Constraints = True
    Left = 96
    Top = 827
  end
  object spSel_Lancamento_Fatores_Intervencao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Lancamentos_Fatores_Intervencao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_cod_fator'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_entrevista'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 130
    Top = 827
  end
  object spInc_Itens_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Itens_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_registro'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_situacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 211
    Top = 827
  end
  object dspInc_Itens_Lanc_Fat_Int: TDataSetProvider
    DataSet = spInc_Itens_Lanc_Fat_Int
    Constraints = True
    Left = 176
    Top = 827
  end
  object spSel_Itens_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Itens_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_registro1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_registro2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 287
    Top = 827
  end
  object dspSel_Itens_Lanc_Fat_Int: TDataSetProvider
    DataSet = spSel_Itens_Lanc_Fat_Int
    Constraints = True
    Left = 256
    Top = 827
  end
  object dspSel_Motivos_Possiveis: TDataSetProvider
    DataSet = spSel_Motivos_Possiveis
    Constraints = True
    Left = 327
    Top = 827
  end
  object spSel_Motivos_Possiveis: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MotivosPossiveis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 827
  end
  object spSel_MotivosPossiveis: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_MotivosPossiveis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 827
  end
  object dspSel_Consulta: TDataSetProvider
    DataSet = spSel_Consulta
    Constraints = True
    Left = 391
    Top = 776
  end
  object spSel_Consulta: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_dat_consulta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_hor_consulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_flg_status_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 423
    Top = 776
  end
  object spInc_Consulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_consulta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_hor_consulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pe_flg_status_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 423
    Top = 824
  end
  object dspInc_Consulta: TDataSetProvider
    DataSet = spInc_Consulta
    Constraints = True
    Left = 391
    Top = 824
  end
  object spAlt_Consulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Consulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_anotacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 495
    Top = 736
  end
  object dspAlt_Consulta: TDataSetProvider
    DataSet = spAlt_Consulta
    Constraints = True
    Left = 463
    Top = 736
  end
  object spAlt_ConsultaStatus: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ConsultaStatus;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 495
    Top = 776
  end
  object dspAlt_ConsultaStatus: TDataSetProvider
    DataSet = spAlt_ConsultaStatus
    Constraints = True
    Left = 463
    Top = 776
  end
  object spSel_TratamentoOdontologico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TratamentoOdontologico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 567
    Top = 736
  end
  object dspSel_TratamentoOdontologico: TDataSetProvider
    DataSet = spSel_TratamentoOdontologico
    Constraints = True
    Left = 535
    Top = 736
  end
  object spSel_TratamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TratamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratcons'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 27
    Top = 733
  end
  object dspSel_TratamentosConsultas: TDataSetProvider
    DataSet = spSel_TratamentosConsultas
    Constraints = True
    Left = 60
    Top = 734
  end
  object spInc_TratamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_TratamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tratamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_num_dente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 567
    Top = 816
  end
  object dspInc_TratamentosConsultas: TDataSetProvider
    DataSet = spInc_TratamentosConsultas
    Constraints = True
    Left = 535
    Top = 816
  end
  object spSel_Encaminhamento: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Encaminhamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_encaminhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 736
  end
  object dspSel_Encaminhamento: TDataSetProvider
    DataSet = spSel_Encaminhamento
    Constraints = True
    Left = 607
    Top = 736
  end
  object spSel_EncaminhamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_EncaminhamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_enccons'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_encaminhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 776
  end
  object dspSel_EncaminhamentosConsultas: TDataSetProvider
    DataSet = spSel_EncaminhamentosConsultas
    Constraints = True
    Left = 607
    Top = 776
  end
  object spInc_EncaminhamentosConsultas: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_EncaminhamentosConsultas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_consulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_encaminhamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 816
  end
  object dspInc_EncaminhamentosConsultas: TDataSetProvider
    DataSet = spInc_EncaminhamentosConsultas
    Constraints = True
    Left = 709
    Top = 500
  end
  object spSel_Dente: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_Dente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo_dente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_tipo_arcada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_tipo_hemiarcada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 711
    Top = 736
  end
  object dspSel_Dente: TDataSetProvider
    DataSet = spSel_Dente
    Constraints = True
    Left = 679
    Top = 736
  end
  object spAlt_Itens_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_Itens_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_item'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 575
    Top = 584
  end
  object dspAlt_Itens_Lanc_Fat_Int: TDataSetProvider
    DataSet = spAlt_Itens_Lanc_Fat_Int
    Constraints = True
    Left = 543
    Top = 584
  end
  object spAlt_Lanc_Fat_Int: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_Lanc_Fat_Int;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_lanc_fat_int'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 575
    Top = 632
  end
  object dspAlt_Lanc_Fat_Int: TDataSetProvider
    DataSet = spAlt_Lanc_Fat_Int
    Constraints = True
    Left = 543
    Top = 632
  end
  object spSel_ProcuraIMC: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ProcuraIMC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_id_indicebiometrico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@pe_num_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_valorIMC'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end>
    Left = 711
    Top = 776
  end
  object dspSel_ProcuraIMC: TDataSetProvider
    DataSet = spSel_ProcuraIMC
    Constraints = True
    Left = 679
    Top = 776
  end
  object spSel_CadastroParaBiometrico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_CadastroParaBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 680
  end
  object dspSel_CadastroParaBiometrico: TDataSetProvider
    DataSet = spSel_CadastroParaBiometrico
    Constraints = True
    Left = 607
    Top = 680
  end
  object spSel_UltimoLancamentoBiometrico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_UltimoLancamentoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 639
    Top = 632
  end
  object dspSel_UltimoLancamentoBiometrico: TDataSetProvider
    DataSet = spSel_UltimoLancamentoBiometrico
    Constraints = True
    Left = 607
    Top = 632
  end
  object spInc_TempBiometrica: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_TempBiometrica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_idade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_val_peso'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 3
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_altura'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_val_imc'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 639
    Top = 584
  end
  object dspInc_TempBiometrica: TDataSetProvider
    DataSet = spInc_TempBiometrica
    Constraints = True
    Left = 607
    Top = 584
  end
  object spExc_TempBiometrica: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_EXC_TempBiometrica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 575
    Top = 680
  end
  object dspExc_TempBiometrica: TDataSetProvider
    DataSet = spExc_TempBiometrica
    Constraints = True
    Left = 543
    Top = 680
  end
  object spSel_TMPBiometricos: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_TMPBIOMETRICOS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 711
    Top = 816
  end
  object dspSel_TMPBiometricos: TDataSetProvider
    DataSet = spSel_TMPBiometricos
    Constraints = True
    Left = 679
    Top = 816
  end
  object spAlt_Biometrico: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_ALT_BIOMETRICO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 783
    Top = 736
  end
  object dspAlt_Biometrico: TDataSetProvider
    DataSet = spAlt_Biometrico
    Constraints = True
    Left = 751
    Top = 736
  end
  object spSel_GraficoBiometrico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_GraficoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 58
    Top = 878
  end
  object dspSel_GraficoBiometrico: TDataSetProvider
    DataSet = spSel_GraficoBiometrico
    Constraints = True
    Left = 26
    Top = 879
  end
  object spInc_GraficoBiometrico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_GraficoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@pe_num_total'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 130
    Top = 878
  end
  object dspInc_GraficoBiometrico: TDataSetProvider
    DataSet = spInc_GraficoBiometrico
    Constraints = True
    Left = 96
    Top = 878
  end
  object spExc_DadosGraficoBiometrico: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_DadosGraficoBiometrico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 211
    Top = 879
  end
  object dspExc_DadosGraficoBiometrico: TDataSetProvider
    DataSet = spExc_DadosGraficoBiometrico
    Constraints = True
    Left = 176
    Top = 878
  end
  object spTotalizaAvaliacoesBiometricas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_TOTALIZA_AVALIACOES_BIOMETRICAS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_unidade'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_classificacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 289
    Top = 879
  end
  object dspSelTotalizaAvaliacoesBiometricas: TDataSetProvider
    DataSet = spTotalizaAvaliacoesBiometricas
    Constraints = True
    Left = 257
    Top = 878
  end
  object spAlt_EnderecoFamilia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_EnderecoFamilia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_complemento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end>
    Left = 361
    Top = 879
  end
  object dspAlt_EnderecoFamilia: TDataSetProvider
    DataSet = spAlt_EnderecoFamilia
    Constraints = True
    Left = 329
    Top = 878
  end
  object spSel_AvaliacaoApr: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_AVALIACAOAPR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dat_av1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_av2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_lanc1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_lanc2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_nom_asocial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 381
    Top = 692
  end
  object dspSel_AvaliacoesApr: TDataSetProvider
    DataSet = spSel_AvaliacaoApr
    Constraints = True
    Left = 393
    Top = 878
  end
  object spInc_AvaliacaoApr: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_INC_AvaliacaoApr;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dat_inicio_pratica'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item06'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item07'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item08'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item09'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justificativa12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_log_data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_visto_adol'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 547
    Top = 476
  end
  object dspInc_AvaliacaoApr: TDataSetProvider
    DataSet = spInc_AvaliacaoApr
    Constraints = True
    Left = 415
    Top = 691
  end
  object spSel_Distinto_Asocial_em_Secao: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_DISTINTO_ASOCIAL_EM_SECAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 584
  end
  object dspSel_Distinto_ASocial_em_Secao: TDataSetProvider
    DataSet = spSel_Distinto_Asocial_em_Secao
    Constraints = True
    Left = 208
    Top = 584
  end
  object spSEL_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_uso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 504
    Top = 28
  end
  object spINC_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_medicamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_uso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 109
  end
  object spALT_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_medicamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@pe_dsc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 368
    Top = 60
  end
  object dspSEL_Medicamento: TDataSetProvider
    DataSet = spSEL_Medicamento
    Constraints = True
    Left = 472
    Top = 28
  end
  object dspINC_Medicamento: TDataSetProvider
    DataSet = spINC_Medicamento
    Constraints = True
    Left = 104
    Top = 109
  end
  object dspALT_Medicamento: TDataSetProvider
    DataSet = spALT_Medicamento
    Constraints = True
    Left = 336
    Top = 60
  end
  object spSel_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 488
    Top = 182
  end
  object spINC_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 264
    Top = 210
  end
  object dspSel_Receituario: TDataSetProvider
    DataSet = spSel_Receituario
    Constraints = True
    Left = 506
    Top = 200
  end
  object dspInc_Receituario: TDataSetProvider
    DataSet = spINC_Receituario
    Constraints = True
    Left = 232
    Top = 208
  end
  object SP_Ultimo_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Ultimo_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 185
  end
  object dspUltimo_Receituario: TDataSetProvider
    DataSet = SP_Ultimo_Receituario
    Constraints = True
    Left = 96
    Top = 184
  end
  object spINC_Item_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Inc_Item_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 648
    Top = 25
  end
  object spSel_Item_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Item_Receituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_item_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_emissao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 32
  end
  object spExc_Item_Receituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Itens_Receituarios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_item_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 168
    Top = 247
  end
  object dspSel_Item_Receituario: TDataSetProvider
    DataSet = spSel_Item_Receituario
    Constraints = True
    Left = 504
    Top = 32
  end
  object dspInc_Item_Receituario: TDataSetProvider
    DataSet = spINC_Item_Receituario
    Constraints = True
    Left = 680
    Top = 168
  end
  object dspExc_Item_Receituario: TDataSetProvider
    DataSet = spExc_Item_Receituario
    Constraints = True
    Left = 224
    Top = 239
  end
  object spAlt_EncerraReceituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_EncerraReceituario;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 168
    Top = 32
  end
  object dspAlt_EncerraReceituario: TDataSetProvider
    DataSet = spAlt_EncerraReceituario
    Constraints = True
    Left = 200
    Top = 32
  end
  object spSel_MontaReceituario: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_MONTA_RECEITUARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_receituario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 97
    Top = 735
  end
  object dspSel_MontaReceituario: TDataSetProvider
    DataSet = spSel_MontaReceituario
    Constraints = True
    Left = 134
    Top = 735
  end
  object spAlt_Desativa_Medicamento: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Desativa_Medicamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_medicamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 608
    Top = 53
  end
  object dspAlt_Desativa_Medicamento: TDataSetProvider
    DataSet = spAlt_Desativa_Medicamento
    Constraints = True
    Left = 640
    Top = 69
  end
  object spExc_TratamentosConsultas: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_TratamentoConsulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_ID_TRATCONS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 544
    Top = 738
  end
  object dspExc_TratamentoConsulta: TDataSetProvider
    DataSet = spExc_TratamentosConsultas
    Constraints = True
    Left = 648
    Top = 738
  end
  object dspExc_EncaminhamentoConsulta: TDataSetProvider
    DataSet = spEsc_EncaminhamentoConsulta
    Constraints = True
    Left = 64
    Top = 160
  end
  object spEsc_EncaminhamentoConsulta: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_EncaminhamentoConsulta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_enccons'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 64
  end
  object spExc_RegistroCadProgSocial: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_RegistroCadProgSocial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_progsocial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 600
    Top = 784
  end
  object dspExc_RegistroCadProgSocial: TDataSetProvider
    DataSet = spExc_RegistroCadProgSocial
    Constraints = True
    Left = 648
    Top = 784
  end
  object spAlt_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_nom_diretor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_publica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 480
    Top = 705
  end
  object dspAlt_Escola: TDataSetProvider
    DataSet = spAlt_Escola
    Constraints = True
    Left = 440
    Top = 704
  end
  object spInc_Escola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Escola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_endereco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_nom_bairro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_num_cep'
        Attributes = [paNullable]
        DataType = ftString
        Size = 9
        Value = Null
      end
      item
        Name = '@pe_num_telefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 18
        Value = Null
      end
      item
        Name = '@pe_nom_diretor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_tipo_publica'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 144
    Top = 96
  end
  object dspInc_Escola: TDataSetProvider
    DataSet = spInc_Escola
    Constraints = True
    Left = 184
    Top = 96
  end
  object spSel_IndicadoresProfissionais: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_sel_indicadores_profissionais;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 664
    Top = 24
  end
  object dspSel_IndicadoresProfissionais: TDataSetProvider
    DataSet = spSel_IndicadoresProfissionais
    Constraints = True
    Left = 664
    Top = 64
  end
  object spSel_Alunos_Turma: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Seleciona_Alunos_Turma;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 664
    Top = 24
  end
  object dspSel_Seleciona_Alunos_Turma: TDataSetProvider
    DataSet = spSel_Alunos_Turma
    Constraints = True
    Left = 664
    Top = 72
  end
  object spAlt_Turma_PosTransferencia: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Alt_Turma_PosTransferencia;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_turma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 656
    Top = 24
  end
  object dspAlt_Turma_PosTransferencia: TDataSetProvider
    DataSet = spAlt_Turma_PosTransferencia
    Constraints = True
    Left = 688
    Top = 24
  end
  object spSel_AdolescentesEmpresasConvenio: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Sel_AdolescentesEmpresasConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 824
    Top = 24
  end
  object dspSel_AdolescentesEmpresasConvenio: TDataSetProvider
    DataSet = spSel_AdolescentesEmpresasConvenio
    Constraints = True
    Left = 792
    Top = 24
  end
  object SP_AsSocial_EmpresasConvenio: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'sp_AsSocial_EmpresasConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 824
    Top = 72
  end
  object dsp_AsSocial_EmpresasConvenio: TDataSetProvider
    DataSet = SP_AsSocial_EmpresasConvenio
    Constraints = True
    Left = 792
    Top = 72
  end
  object spSel_Caroscopio: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'sp_Sel_Caroscopio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 774
    Top = 120
  end
  object dspSel_Caroscopio: TDataSetProvider
    DataSet = spSel_Caroscopio
    Constraints = True
    Left = 740
    Top = 120
  end
  object spSel_DadosVisitaTecnica: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Dados_Visita_Tecnica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 641
    Top = 168
  end
  object dspSel_DadosVisitaTecnica: TDataSetProvider
    DataSet = spSel_DadosVisitaTecnica
    Constraints = True
    Left = 640
    Top = 216
  end
  object spALT_AvaliacoesAprendiz: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Alt_AvaliacoesAprendiz;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_avaliacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_inicio_pratica'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_flg_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica01'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica02'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica03'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica04'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica05'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item06'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica06'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item07'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica07'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item08'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica08'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item09'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica09'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica11'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@pe_flg_item12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_justifica12'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 656
    Top = 192
  end
  object dspALT_AvaliacoesAprendiz: TDataSetProvider
    DataSet = spALT_AvaliacoesAprendiz
    Constraints = True
    Left = 656
    Top = 240
  end
  object spSel_SerieEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Sel_Serie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 24
  end
  object dspSel_SerieEscolar: TDataSetProvider
    DataSet = spSel_SerieEscolar
    Constraints = True
    Left = 904
    Top = 24
  end
  object spInc_SerieEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Inc_Serie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 72
  end
  object dspInc_SerieEscolar: TDataSetProvider
    DataSet = spInc_SerieEscolar
    Constraints = True
    Left = 904
    Top = 72
  end
  object spAlt_SerieEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Alt_SerieEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_flg_status'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 120
  end
  object dspAlt_SerieEscolar: TDataSetProvider
    DataSet = spAlt_SerieEscolar
    Constraints = True
    Left = 904
    Top = 120
  end
  object spSel_TotalRegistrosUsuarioUnidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SelTotalRegistrosUsuarioUnidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_inicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_dat_final'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 864
    Top = 136
  end
  object dspSel_TotalRegistrosUsuarioUnidade: TDataSetProvider
    DataSet = spSel_TotalRegistrosUsuarioUnidade
    Constraints = True
    Left = 904
    Top = 136
  end
  object spSel_TotalRegistrosUnidade: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TotalRegistrosUnidade;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 864
    Top = 176
  end
  object dspSel_TotalRegistrosUnidade: TDataSetProvider
    DataSet = spSel_TotalRegistrosUnidade
    Constraints = True
    Left = 904
    Top = 176
  end
  object spSel_TransferenciasDAPA: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TransferenciasDAPA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_data1'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_data2'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_ind_motivo1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 219
  end
  object dspSel_TransferenciasDAPA: TDataSetProvider
    DataSet = spSel_TransferenciasDAPA
    Constraints = True
    Left = 904
    Top = 219
  end
  object spSel_Acessos: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Acesso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acesso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_perfil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_ativo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_online'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 261
  end
  object dspSel_Acesso: TDataSetProvider
    DataSet = spSel_Acessos
    Constraints = True
    Left = 904
    Top = 261
  end
  object spSel_EscolaSerie: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_Sel_EscolaSerie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_escola_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 304
  end
  object dspSel_EscolaSerie: TDataSetProvider
    DataSet = spSel_EscolaSerie
    Constraints = True
    Left = 904
    Top = 304
  end
  object spInc_EscolaSerie: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_EscolaSerie;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 348
  end
  object dspInc_EscolaSerie: TDataSetProvider
    DataSet = spInc_EscolaSerie
    Constraints = True
    Left = 904
    Top = 348
  end
  object spSel_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompescolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 864
    Top = 391
  end
  object dspSel_AcompEscolar: TDataSetProvider
    DataSet = spSel_AcompEscolar
    Constraints = True
    Left = 904
    Top = 391
  end
  object spInc_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end>
    Left = 942
    Top = 368
  end
  object dspInc_AcompEscolar: TDataSetProvider
    DataSet = spInc_AcompEscolar
    Constraints = True
    Left = 982
    Top = 369
  end
  object spAlt_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_frequencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_aproveitamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_periodo_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompescolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_turma'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end>
    Left = 942
    Top = 416
  end
  object dspAlt_AcompEscolar: TDataSetProvider
    DataSet = spAlt_AcompEscolar
    Constraints = True
    Left = 982
    Top = 417
  end
  object spInc_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_DefasagemEsc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_ideal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_defasada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 942
    Top = 460
  end
  object dspInc_DefasagemEscolar: TDataSetProvider
    DataSet = spInc_DefasagemEscolar
    Constraints = True
    Left = 982
    Top = 459
  end
  object spSel_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DefasagemEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_defasagemesc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 862
    Top = 415
  end
  object dspSel_DefasagemEscolar: TDataSetProvider
    DataSet = spSel_DefasagemEscolar
    Constraints = True
    Left = 902
    Top = 414
  end
  object spAlt_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_DefasagemEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_defasagemesc'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_serie'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_ideal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_idade_defasada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 862
    Top = 459
  end
  object dspAlt_DefasagemEscolar: TDataSetProvider
    DataSet = spAlt_DefasagemEscolar
    Constraints = True
    Left = 902
    Top = 458
  end
  object spAlt_Cadastro_DefasagemEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Cadastro_DefasagemEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end>
    Left = 862
    Top = 501
  end
  object dspAlt_Cadastro_DefasagemEscolar: TDataSetProvider
    DataSet = spAlt_Cadastro_DefasagemEscolar
    Constraints = True
    Left = 902
    Top = 500
  end
  object spSel_Solicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Solicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_prioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipo_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 942
    Top = 498
  end
  object dspSel_Solicitacao: TDataSetProvider
    DataSet = spSel_Solicitacao
    Constraints = True
    Left = 982
    Top = 498
  end
  object spInc_Solicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Solicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_solicitacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipo_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_ind_prioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_solicitacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 862
    Top = 546
  end
  object dspInc_Solicitacao: TDataSetProvider
    DataSet = spInc_Solicitacao
    Constraints = True
    Left = 902
    Top = 546
  end
  object spSel_TipoSolicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_TipoSolicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_tipo_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_ativa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 942
    Top = 547
  end
  object dspSel_TipoSolicitacao: TDataSetProvider
    DataSet = spSel_TipoSolicitacao
    Constraints = True
    Left = 982
    Top = 547
  end
  object spAlt_Solicitacao: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Solicitacao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_devolutiva'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_id_solicitacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 862
    Top = 596
  end
  object dspAlt_Solicitacao: TDataSetProvider
    DataSet = spAlt_Solicitacao
    Constraints = True
    Left = 902
    Top = 596
  end
  object spInc_SatisfConv: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_SatisfConv;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dat_avaliacao'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_nom_avaliador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_setor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_adolescentes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_item01'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item02'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item03'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item04'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_item05'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_observacoes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_vlr_media'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Value = Null
      end>
    Left = 535
    Top = 879
  end
  object dspInc_SatisfConv: TDataSetProvider
    DataSet = spInc_SatisfConv
    Constraints = True
    Left = 569
    Top = 878
  end
  object spSel_SatisfConv: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_SatisfConv;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_satisfconv'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_secao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_mes_inicio_avaliacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_mes_final_avaliacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@pe_ano_avaliacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 615
    Top = 879
  end
  object dspSel_SastisfConv: TDataSetProvider
    DataSet = spSel_SatisfConv
    Constraints = True
    Left = 649
    Top = 878
  end
  object spExc_AcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_AcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_acompescolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 560
    Top = 408
  end
  object dspExc_AcompEscolar: TDataSetProvider
    DataSet = spExc_AcompEscolar
    Constraints = True
    Left = 596
    Top = 376
  end
  object spSel_PesquisaAcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_PesquisaAcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano_letivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_bimestre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 558
    Top = 394
  end
  object dspSel_PesquisaAcompEscolar: TDataSetProvider
    DataSet = spSel_PesquisaAcompEscolar
    Constraints = True
    Left = 592
    Top = 392
  end
  object spSel_ResumoMensal: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_ResumoMensal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_ID_RESUMOMENSAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_COD_UNIDADE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_ANO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_NUM_MES'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PE_FLG_SITUACAO_RESUMO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 984
    Top = 80
  end
  object dspSel_ResumoMensal: TDataSetProvider
    DataSet = spSel_ResumoMensal
    Constraints = True
    Left = 1024
    Top = 80
  end
  object dspAlt_DadosUltimaEscolaHistoricoEscola: TDataSetProvider
    DataSet = spAlt_DadosUltimaEscolaHistoricoEscola
    Constraints = True
    Left = 783
    Top = 640
  end
  object spAlt_DadosUltimaEscolaHistoricoEscola: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_DadosUltimaEscolaHistoricoEscola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_log_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 743
    Top = 640
  end
  object spSel_AlunosSemAcompEscolar: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_AlunosSemAcompEscolar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 612
    Top = 596
  end
  object dspSel_AlunosSemAcompEscolar: TDataSetProvider
    DataSet = spSel_AlunosSemAcompEscolar
    Constraints = True
    Left = 656
    Top = 592
  end
  object spSEL_Boletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_Boletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_num_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 856
    Top = 656
  end
  object dspSel_Boletim: TDataSetProvider
    DataSet = spSEL_Boletim
    Constraints = True
    Left = 896
    Top = 656
  end
  object dspInc_Boletim: TDataSetProvider
    DataSet = spInc_Boletim
    Constraints = True
    Left = 896
    Top = 752
  end
  object dspExc_Boletim: TDataSetProvider
    DataSet = spExc_Boletim
    Constraints = True
    Left = 896
    Top = 704
  end
  object spInc_Boletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_Boletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_matricula'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_nom_nome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@pe_num_rg_escolar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@pe_dat_nascimento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@pe_nom_escola'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_serie'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@pe_dsc_freq_1b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_freq_2b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_freq_3b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_aprov_1b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_aprov_2b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@pe_dsc_aprov_3b'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 856
    Top = 752
  end
  object spExc_Boletim: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_EXC_Boletim;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 856
    Top = 704
  end
  object spInc_ResumoMensal: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_ResumoMensal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_matriculados_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_fora_ensino_regular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_freq_dentro_esperado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_final_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_dias_atividades'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_presencas_esperadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_geral_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_injustificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_apedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_abandono'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_maioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_lei'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_outros'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_grau_aprov_programa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_disciplinar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_socioeconomica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_violencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_abrigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_interacao_relacional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ato_infracional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_saude'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_guarda_irregular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_gravidez'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_moradia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_trab_infantil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_inscritos_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aptos_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_encaminhados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_inseridos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovador_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_familias'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_convidados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_familia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_empresas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_ult_alteracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_flg_situacao_resumo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 984
    Top = 24
  end
  object dspInc_ResumoMensal: TDataSetProvider
    DataSet = spInc_ResumoMensal
    Constraints = True
    Left = 1024
    Top = 24
  end
  object spAlt_ResumoMensal: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_ResumoMensal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_resumomensal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_inicio_mes_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_admitidos_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_de_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_ae_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_para_ae_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_aprendiz_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_transf_aprendiz_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_peti_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_manha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_tarde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_afastados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_matriculados_escola'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_fora_ensino_regular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_freq_dentro_esperado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_defasagem_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_final_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_dias_atividades'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_presencas_esperadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_geral_faltas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_justificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_faltas_injustificadas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_apedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_abandono'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_maioridade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_lei'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_desligados_outros'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_grau_aprov_programa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_disciplinar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_socioeconomica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_situacao_escolar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_violencia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_abrigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_interacao_relacional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ato_infracional'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_saude'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_guarda_irregular'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_gravidez'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_moradia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_trab_infantil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_socioculturais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_ativ_recreativas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_dsc_acoes_sociais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@pe_num_tt_inscritos_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_vestibulinho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aptos_aprendizagem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_encaminhados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_adol_inseridos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_concluintes_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_aprovados_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_op'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_oe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_fic2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_reprovados_form_tecnica'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_acoes_familias'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_convidados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_tt_participantes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_criadol1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_familia'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_grau_satisf_empresas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_usuario_ult_alteracao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 984
    Top = 128
  end
  object dspAlt_ResumoMensal: TDataSetProvider
    DataSet = spAlt_ResumoMensal
    Constraints = True
    Left = 1024
    Top = 128
  end
  object dspSel_ObtemDataServer: TDataSetProvider
    DataSet = spSel_ObtemDataServer
    Constraints = True
    Left = 1024
    Top = 176
  end
  object spSel_ObtemDataServer: TADOStoredProc
    Connection = adoConn
    CursorType = ctStatic
    ProcedureName = 'SP_SEL_ObtemDataServer;1'
    Parameters = <>
    Left = 984
    Top = 176
  end
  object spAlt_Encerra_Reverte_Resumo: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_ALT_Encerra_ou_Reverte_Resumo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_tipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_unidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_resumomensal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 984
    Top = 220
  end
  object dspAlt_Encerra_Reverte_Resumo: TDataSetProvider
    DataSet = spAlt_Encerra_Reverte_Resumo
    Constraints = True
    Left = 1024
    Top = 219
  end
  object spSel_DiasUteis: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_SEL_DiasUteis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_cod_id_diasuteis'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 984
    Top = 269
  end
  object dspSel_DiasUteis: TDataSetProvider
    DataSet = spSel_DiasUteis
    Constraints = True
    Left = 1024
    Top = 269
  end
  object spInc_DiasUteis: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'SP_INC_DiasUteis;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pe_num_dia_util'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 984
    Top = 317
  end
  object dspInc_DiasUteis: TDataSetProvider
    DataSet = spInc_DiasUteis
    Constraints = True
    Left = 1024
    Top = 317
  end
end
