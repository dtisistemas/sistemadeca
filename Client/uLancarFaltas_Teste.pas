unit uLancarFaltas_Teste;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Grids, DBGrids, CheckLst, ComCtrls, Db,
  ToolWin;

type
  TfrmLancarFaltas_Teste = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    rgCriterio: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    btnPesquisar: TSpeedButton;
    edPesquisa: TEdit;
    cbTurma: TComboBox;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    btnLancarFalta: TSpeedButton;
    DateTimePicker1: TDateTimePicker;
    cbTipoFalta: TComboBox;
    Panel2: TPanel;
    Bevel1: TBevel;
    Label2: TLabel;
    cklCadastro: TCheckListBox;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DataSource1: TDataSource;
    Label5: TLabel;
    cbTurma2: TComboBox;
    Label6: TLabel;
    edAno: TEdit;
    UpDown1: TUpDown;
    Label7: TLabel;
    cbMes: TComboBox;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure rgCriterioClick(Sender: TObject);
    procedure cbTurmaClick(Sender: TObject);
    procedure cbTurma2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLancarFaltas_Teste: TfrmLancarFaltas_Teste;
  vListaTurma1, vListaTurma2 : TStringList;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmLancarFaltas_Teste.FormShow(Sender: TObject);
begin
  {
  with dmDeca.cdsSel_Cadastro do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := 0;
    Open;

    cklCadastro.Clear;
    //Carregar a lista do cadastro em checklistbox
    while not eof do
    begin
      cklCadastro.Items.Add(FieldByName('cod_matricula').Value + ' - ' + FieldByName('nom_nome').Value);
      Next;
    end;

  end;
  }

  edPesquisa.Clear;
  edPesquisa.Visible := True;
  btnPesquisar.Visible := True;
  cbTurma.ItemIndex := -1;
  cbTurma.Visible := False;

  vListaTurma1 := TStringList.Create();
  vListaTurma2 := TStringList.Create();

  //Carregar a lista de turmas para a unidade selecionada
  try
    with dmDeca.cdsSel_Cadastro_Turmas do
    begin
      Close;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_nom_turma').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      cbTurma.Clear;
      while not eof do
      begin
        cbTurma.Items.Add (FieldByName('nom_turma').Value);
        cbTurma2.Items.Add (FieldByName('nom_turma').Value);
        vListaTurma1.Add (IntToStr(FieldByName('cod_turma').Value));
        vListaTurma2.Add (FieldByName('nom_turma').Value);
        Next;
      end;
    end;
  except end;


end;

procedure TfrmLancarFaltas_Teste.rgCriterioClick(Sender: TObject);
begin
  if rgCriterio.ItemIndex = 0 then  //Matricula
  begin
    edPesquisa.Clear;
    edPesquisa.Visible := True;
    btnPesquisar.Visible := True;
    cbTurma.ItemIndex := -1;
    cbTurma.Visible := False;
  end
  else
  begin
    edPesquisa.Clear;
    edPesquisa.Visible := False;
    btnPesquisar.Visible := False;
    cbTurma.ItemIndex := -1;
    cbTurma.Visible := True;
  end;
end;

procedure TfrmLancarFaltas_Teste.cbTurmaClick(Sender: TObject);
begin

  //Carrega a lista de alunso da turma selecionada para a unidade
  try
    with dmDeca.cdsSel_Seleciona_Alunos_Turma do
    begin
      Close;
      Params.ParamByName ('@pe_cod_turma').Value := StrToInt(vListaTurma1.Strings[cbTurma.ItemIndex]);
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      cklCadastro.Clear;
      while not eof do
      begin
        cklCadastro.Items.Add(FieldByName('cod_matricula').Value + ' - ' + FieldByName('nom_nome').Value);
        Next;
      end;
    end;
  except end;
end;

procedure TfrmLancarFaltas_Teste.cbTurma2Click(Sender: TObject);
begin

  //Carrega a lista de alunso da turma selecionada para a unidade
  try
    with dmDeca.cdsSel_Seleciona_Alunos_Turma do
    begin
      Close;
      Params.ParamByName ('@pe_cod_turma').Value := StrToInt(vListaTurma1.Strings[cbTurma2.ItemIndex]);
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      if RecordCount < 1 then ShowMessage ('N�o existem alunos cadastrados para a turma selecionada...');

    end;
  except end;
end;

procedure TfrmLancarFaltas_Teste.ToolButton1Click(Sender: TObject);
begin
  //Seleciona dados de Frequencia relativas a Turma, M�s e Ano selecionados
  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName ('@pe_num_ano').Value := GetValue(edAno.Text);
      Params.ParamByName ('@pe_num_mes').Value := cbMes.ItemIndex+1;
      Params.ParamByName ('@pe_cod_turma').Value := StrToInt(vListaTurma1.Strings[cbTurma2.ItemIndex]);
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;
      DBGrid2.Refresh;
    end;
  except end;




end;

end.
