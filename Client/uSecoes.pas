unit uSecoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmSecoes = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    Label1: TLabel;
    cbUnidade: TComboBox;
    dbgSecoes: TDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edNumSecao: TEdit;
    edSecao: TEdit;
    edAsSocial: TEdit;
    Label5: TLabel;
    edInclusao: TEdit;
    Label6: TLabel;
    edAlteracao: TEdit;
    Label7: TLabel;
    cbSituacao: TComboBox;
    dsSecoes: TDataSource;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure dbgSecoesDblClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo:String);
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSecoes: TfrmSecoes;
  vListaUnidade1 : TStringList;
  vListaUnidade2 : TStringList;

implementation

uses uDM, uUtil, uPrincipal, uUnidades;

{$R *.DFM}

procedure TfrmSecoes.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Novo');
  edNumSecao.SetFocus;
  edInclusao.Text := DateToStr(Date());
end;

procedure TfrmSecoes.btnSalvarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);//dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := GetValue(edNumSecao.Text);
      Params.ParamByName ('@pe_nom_asocial').Value := GetValue(edAsSocial.Text);
      Params.ParamByName ('@pe_dsc_nom_secao').Value := GetValue(edSecao.Text);
      Params.ParamByName ('@pe_flg_status').Value := cbSituacao.ItemIndex;
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Execute;

      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);//dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;
        dbgSecoes.Refresh;

        AtualizaCamposBotoes ('Padrao');

      end;
    end;
  except end;
end;

procedure TfrmSecoes.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').AsInteger := dmDeca.cdsSel_Secao.FieldByName ('cod_id_secao').AsInteger;
      Params.ParamByName ('@pe_num_secao').Value := GetValue(edNumSecao.Text);
      Params.ParamByName ('@pe_nom_asocial').Value := GetValue(edAsSocial.Text);
      Params.ParamByName ('@pe_dsc_nom_secao').Value := GetValue(edSecao.Text);
      Params.ParamByName ('@pe_flg_status').Value := cbSituacao.ItemIndex;
      Execute;

      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]); 
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;
        dbgSecoes.Refresh;
      end;
    end;

    AtualizaCamposBotoes ('Padrao');

  except end;
end;

procedure TfrmSecoes.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
end;

procedure TfrmSecoes.dbgSecoesDblClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Alterar');
  edSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString;
  edAsSocial.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').AsString;
  edNumSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString;
  edAlteracao.Text := dmDeca.cdsSel_Secao.FieldByName ('vDatAltera').AsString;
  cbSituacao.ItemIndex := dmDeca.cdsSel_Secao.FieldByName ('flg_status').AsInteger;
end;

procedure TfrmSecoes.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    //cbUnidade.ItemIndex := -1;
    //Panel1.Enabled := False;

    edNumSecao.Clear;
    edSecao.Clear;
    edAsSocial.Clear;
    edInclusao.Clear;
    edAlteracao.Clear;
    cbSituacao.ItemIndex := -1;
    Panel2.Enabled := False;

    Panel3.Enabled := True;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    //Panel1.Enabled := True;
    Panel2.Enabled := True;
    Panel3.Enabled := False;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end
  else if Modo = 'Alterar' then
  begin
    //Panel1.Enabled := True;
    Panel2.Enabled := True;
    Panel3.Enabled := False;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmSecoes.btnSairClick(Sender: TObject);
begin
  frmSecoes.Close;
  frmUnidades.AtualizaCamposBotoes ('Padr�o');
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
      frmUnidades.dbgUnidade.Refresh;
    end;
  except end;
end;

procedure TfrmSecoes.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar lista de unidades no combo
  try
    with uDM.dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').value:= NULL;
      Params.ParamByName('@pe_nom_unidade').value:= NULL;
      Open;
      cbUnidade.Items.Clear;
      while not Eof do
      begin
        cbUnidade.Items.Add(FieldByName('nom_unidade').AsString);
        vListaUnidade1.Add(IntToStr(FieldByName('cod_unidade').AsInteger));
        vListaUnidade2.Add(FieldByName('nom_unidade').AsString);
        Next;
      end;
      Close;
      vListaUnidade2.Sort;
    end;
  except end;

  

end;

procedure TfrmSecoes.cbUnidadeClick(Sender: TObject);
begin
  //Carrega as se��es e acordo com a unidade selecionada
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_secao').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Params.ParamByName('@pe_num_secao').Value := Null;
      Params.ParamByName('@pe_flg_status').Value := Null;
      Open;
      dbgSecoes.Refresh;
    end;
  except end;
end;

end.
