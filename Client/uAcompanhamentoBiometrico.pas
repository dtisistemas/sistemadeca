unit uAcompanhamentoBiometrico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, TeeProcs, TeEngine, Chart, Db, DBChart,
  Series, ADODB, Grids, DBGrids;

type
  TfrmAcompanhamentoBiometrico = class(TForm)
    rgCriterio: TRadioGroup;
    Bevel1: TBevel;
    GroupBox1: TGroupBox;
    chkAbaixoDoPeso: TCheckBox;
    chkNormal: TCheckBox;
    chkSobrePeso: TCheckBox;
    chkObesidade: TCheckBox;
    chkTodas: TCheckBox;
    Panel1: TPanel;
    btnVer: TSpeedButton;
    btnLimpar: TSpeedButton;
    Panel2: TPanel;
    btnImprimirGrafico: TSpeedButton;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    GraficoBiometrico: TDBChart;
    Series1: TBarSeries;
    DBGrid1: TDBGrid;
    dsTmpBiometricos: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    btnClassificacao: TSpeedButton;
    procedure chkTodasClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgCriterioClick(Sender: TObject);
    procedure btnImprimirGraficoClick(Sender: TObject);
    procedure btnClassificacaoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAcompanhamentoBiometrico: TfrmAcompanhamentoBiometrico;
  vListaUnidadeG1, vListaUnidadeG2 : TStringList;

implementation

uses uDM, uPrincipal, rGraficoBiometricoUnidade, rClassificacao;

{$R *.DFM}

procedure TfrmAcompanhamentoBiometrico.chkTodasClick(Sender: TObject);
begin
  if chkTodas.Checked = True then
  begin
    chkAbaixoDoPeso.Checked := True;
    chkAbaixoDoPeso.State := cbGrayed;
    chkAbaixoDoPeso.Enabled := False;
    chkNormal.Checked := True;
    chkNormal.State := cbGrayed;
    chkNormal.Enabled := False;
    chkSobrePeso.Checked := True;
    chkSobrePeso.State := cbGrayed;
    chkSobrePeso.Enabled := False;
    chkObesidade.Checked := True;
    chkObesidade.State := cbGrayed;
    chkObesidade.Enabled := False;
  end
  else
  begin
    chkAbaixoDoPeso.Checked := False;
    chkAbaixoDoPeso.State := cbUnchecked;
    chkAbaixoDoPeso.Enabled := True;
    chkNormal.Checked := False;
    chkNormal.State := cbUnchecked;
    chkNormal.Enabled := True;
    chkSobrePeso.Checked := False;
    chkSobrePeso.State := cbUnchecked;
    chkSobrePeso.Enabled := True;
    chkObesidade.Checked := False;
    chkObesidade.State := cbUnchecked;
    chkObesidade.Enabled := True;
  end;
end;

procedure TfrmAcompanhamentoBiometrico.btnVerClick(Sender: TObject);
begin

  //Exclui os registros gravados na tabela TMP_BIOMETRICOS para o usu�rio atual
  with dmDeca.cdsExc_TempBiometrica do
  begin
    Close;
    Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
    Execute;
  end;

  //Seleciona dados dos �ltimos lan�amentos para cada crian�a/adolescente
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      //Verifica se selecionou TUDO ou apenas uma unidade espec�fica
      if rgCriterio.ItemIndex = 0 then
        Params.ParamByName('@pe_cod_unidade').Value := vListaUnidadeG1.Strings[cbUnidade.ItemIndex]
      else
        //Repassa o c�digo da unidade selecionada
        Params.ParamByName('@pe_cod_unidade').Value := 0;
      Open;

      dmDeca.cdsSel_Cadastro.First;
      while not dmDeca.cdsSel_Cadastro.eof do
      begin

        //Para cada um do cadastro, verifica o �ltimo lan�amento biom�trico efetuado e grava na tabela TempBiometrico
        with dmDeca.cdsSel_UltimoLancamentoBiometrico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
          Open;

          with dmDeca.cdsInc_TempBiometrica do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
            Params.ParamByName('@pe_nom_nome').Value := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;

            if rgCriterio.ItemIndex = 0 then
              Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
            else
              Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';

            Params.ParamByName('@pe_idade').Value := dmDeca.cdsSel_Cadastro.FieldByName('idade').Value;
            Params.ParamByName('@pe_ind_sexo').Value := dmDeca.cdsSel_Cadastro.FieldByName('ind_sexo').Value;
            Params.ParamByName('@pe_val_peso').Value := dmDeca.cdsSel_UltimoLancamentoBiometrico.FieldByName('val_peso').Value;
            Params.ParamByName('@pe_val_altura').Value := dmDeca.cdsSel_UltimoLancamentoBiometrico.FieldByName('val_altura').Value;
            Params.ParamByName('@pe_val_imc').Value := dmDeca.cdsSel_UltimoLancamentoBiometrico.FieldByName('val_imc').Value;
            Params.ParamByName('@pe_dsc_classificacao').Value := dmDeca.cdsSel_UltimoLancamentoBiometrico.FieldByName('dsc_classificacao_imc').Value;
            Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Execute;
          end;
        end;
        dmDeca.cdsSel_Cadastro.Next;
      end;

    end;
  except end;

  //Exibir adolescente e classifica��o da unidade
  with dmDeca.cdsSel_TMPBiometricos do
  begin
    Close;
    if rgCriterio.ItemIndex = 0 then
      Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
    else
      Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
    Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
    Open;
    DBGrid1.Refresh;
  end;

  //Excluir dados da tabela GraficoBiometrico referente a Unidade e Usu�rio selecionado
  try
    with dmDeca.cdsExc_DadosGraficoBiometrico do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
      if rgCriterio.ItemIndex = 0 then
        Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
      else
        Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
      Execute;
    end;
  except end;


  //Total "Baixo Peso" pela unidade selecionada ou geral
  with dmDeca.cdsTotalizaAvaliacoesBiometricas do
  begin
    Close;
    if rgCriterio.ItemIndex = 0 then
      Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
    else
      Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
    Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Params.ParamByName('@pe_dsc_classificacao').Value := 'BAIXO PESO';
    Open;

    //Grava o total em GraficoBiometrico
    with dmDeca.cdsInc_GraficoBiometrico do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
      if rgCriterio.ItemIndex = 0 then
        Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
      else
        Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
      Params.ParamByName('@pe_dsc_classificacao').Value := 'BAIXO PESO';
      Params.ParamByName('@pe_num_total').Value := dmDeca.cdsTotalizaAvaliacoesBiometricas.FieldByName('vTotal').Value;
      Execute;
    end;
  end;

  //Total "Condi��o Normal" pela unidade selecionada ou geral
  with dmDeca.cdsTotalizaAvaliacoesBiometricas do
  begin
    Close;
    if rgCriterio.ItemIndex = 0 then
      Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
    else
      Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
    Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Params.ParamByName('@pe_dsc_classificacao').Value := 'CONDI��O NORMAL';
    Open;

    //Grava o total em GraficoBiometrico
    with dmDeca.cdsInc_GraficoBiometrico do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
      if rgCriterio.ItemIndex = 0 then
        Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
      else
        Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
      Params.ParamByName('@pe_dsc_classificacao').Value := 'CONDI��O NORMAL';
      Params.ParamByName('@pe_num_total').Value := dmDeca.cdsTotalizaAvaliacoesBiometricas.FieldByName('vTotal').Value;
      Execute;
    end;
  end;

  //Total "Sobre Peso" pela unidade selecionada ou geral
  with dmDeca.cdsTotalizaAvaliacoesBiometricas do
  begin
    Close;
    if rgCriterio.ItemIndex = 0 then
      Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
    else
      Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
    Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Params.ParamByName('@pe_dsc_classificacao').Value := 'SOBRE PESO';
    Open;

    //Grava o total em GraficoBiometrico
    with dmDeca.cdsInc_GraficoBiometrico do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
      if rgCriterio.ItemIndex = 0 then
        Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
      else
        Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
      Params.ParamByName('@pe_dsc_classificacao').Value := 'SOBRE PESO';
      Params.ParamByName('@pe_num_total').Value := dmDeca.cdsTotalizaAvaliacoesBiometricas.FieldByName('vTotal').Value;
      Execute;
    end;
  end;

  //Total "Obesidade" pela unidade selecionada ou geral
  with dmDeca.cdsTotalizaAvaliacoesBiometricas do
  begin
    Close;
    if rgCriterio.ItemIndex = 0 then
      Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
    else
      Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
    Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Params.ParamByName('@pe_dsc_classificacao').Value := 'OBESIDADE';
    Open;

    //Grava o total em GraficoBiometrico
    with dmDeca.cdsInc_GraficoBiometrico do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
      if rgCriterio.ItemIndex = 0 then
        Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
      else
        Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
      Params.ParamByName('@pe_dsc_classificacao').Value := 'OBESIDADE';
      Params.ParamByName('@pe_num_total').Value := dmDeca.cdsTotalizaAvaliacoesBiometricas.FieldByName('vTotal').Value;
      Execute;
    end;
  end;

  //Seleciona os dados para exibi��o do gr�fico
  with dmDeca.cdsSel_GraficoBiometrico do
  begin
    Close;
    Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
    if rgCriterio.ItemIndex = 0 then
      Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text
    else
      Params.ParamByName('@pe_nom_unidade').Value := 'FUNDHAS';
    Open;
  end;
end;

procedure TfrmAcompanhamentoBiometrico.FormShow(Sender: TObject);
var vInd : Integer;
begin

    vListaUnidadeG1 := TStringList.Create();
    vListaUnidadeG2 := TStringList.Create();

    //Carrega as unidades para o combo no caso de ser utilizado n0o gr�fico
    try
      with dmDeca.cdsSel_Unidade do
      begin
        Close;
        Params.ParamByName('@pe_cod_unidade').Value := Null;
        Params.ParamByName('@pe_nom_unidade').Value := Null;
        Open;

        while not dmDeca.cdsSel_Unidade.eof do
        begin
          vListaUnidadeG1.Add(IntToStr(FieldByName('cod_unidade').Value));
          vListaUnidadeG2.Add(FieldByName('nom_unidade').Value);
          cbUnidade.Items.Add(FieldByName('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;
    except end;

    //Habilita as op��es "padr�es" para se iniciar uma consulta b�sica
    rgCriterio.ItemIndex := 0;

    cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := True;

    chkAbaixoDoPeso.Checked := False;
    chkAbaixoDoPeso.State := cbUnchecked;
    chkNormal.Checked := False;
    chkNormal.State := cbUnchecked;
    chkSobrePeso.Checked := False;
    chkSobrePeso.State := cbUnchecked;
    chkObesidade.Checked := False;
    chkObesidade.State := cbUnchecked;
    chkTodas.Checked := True;

    if (vvIND_PERFIL IN [1,10]) then
    begin
      GroupBox2.Enabled := True;
      vvCOD_UNIDADE := 0
    end
    else if (vvIND_PERFIL IN [2,3,6]) then
    begin
      GroupBox2.Enabled := False;
      vListaUnidadeG2.Find(vvNOM_UNIDADE, vInd);
      cbUnidade.ItemIndex := vInd;
    end;



end;

procedure TfrmAcompanhamentoBiometrico.rgCriterioClick(Sender: TObject);
begin
  if rgCriterio.ItemIndex = 0 then cbUnidade.Enabled := True else cbUnidade.Enabled := False;
end;

procedure TfrmAcompanhamentoBiometrico.btnImprimirGraficoClick(
  Sender: TObject);
begin
  Application.CreateForm (TrelGraficoBiometricoUnidade, relGraficoBiometricoUnidade);
  relGraficoBiometricoUnidade.lbUnidade.CAption := cbUnidade.Text;
  relGraficoBiometricoUnidade.Preview;
  relGraficoBiometricoUnidade.Free;
end;

procedure TfrmAcompanhamentoBiometrico.btnClassificacaoClick(
  Sender: TObject);
begin
  Application.CreateForm(TrelClassificacao, relClassificacao);
  relClassificacao.lbUnidade.Caption := cbUnidade.Text;
  relClassificacao.Preview;
  relClassificacao.Free;
end;

procedure TfrmAcompanhamentoBiometrico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
