unit uNovaAvaliacaoAprendiz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg, ComCtrls, Buttons, StdCtrls, Mask, Grids, DBGrids, Db;

type
  TfrmNovaAvaliacaoAprendiz = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Bevel1: TBevel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    mskMatricula: TMaskEdit;
    edNome: TEdit;
    btnPesquisarAdolescente: TSpeedButton;
    cbCcusto: TComboBox;
    cbSecao: TComboBox;
    edNomeResponsavel: TEdit;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Shape3: TShape;
    Shape4: TShape;
    cbNivelDesempenho: TComboBox;
    lstFatores: TListBox;
    Label3: TLabel;
    Shape5: TShape;
    Shape6: TShape;
    Label4: TLabel;
    Label5: TLabel;
    Shape7: TShape;
    Shape8: TShape;
    mskDataInicioPratica: TMaskEdit;
    GroupBox5: TGroupBox;
    cbSemestre: TComboBox;
    mskAno: TMaskEdit;
    meConsideracoesSocial: TMemo;
    meAdolescenteAprendiz: TMemo;
    btnAgregaNivelFator: TSpeedButton;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    rgCriterio: TRadioGroup;
    gbAprendiz: TGroupBox;
    mskMatriculaP: TMaskEdit;
    btnPesquisar: TSpeedButton;
    edNomeP: TEdit;
    gbUnidade: TGroupBox;
    cbUnidade: TComboBox;
    gbSemestreAno: TGroupBox;
    cbSemestreP: TComboBox;
    mskAnoP: TMaskEdit;
    btnConsultar: TSpeedButton;
    Bevel2: TBevel;
    dbgResultado: TDBGrid;
    Panel2: TPanel;
    btnNova: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnVisualizar: TSpeedButton;
    btnSair: TSpeedButton;
    dsSel_AvDesemp: TDataSource;
    btnAcompanhamento: TSpeedButton;
    GroupBox11: TGroupBox;
    mskDataAvaliacao: TMaskEdit;
    btnRelatorioEstatistico: TSpeedButton;
    procedure btnPesquisarAdolescenteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lstFatoresClick(Sender: TObject);
    procedure btnAgregaNivelFatorClick(Sender: TObject);
    procedure rgCriterioClick(Sender: TObject);
    procedure cbCcustoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure AtivaCamposBotoes(Modo: String);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNovaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure mskAnoExit(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure btnAcompanhamentoClick(Sender: TObject);
    procedure dbgResultadoDblClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnRelatorioEstatisticoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovaAvaliacaoAprendiz: TfrmNovaAvaliacaoAprendiz;
  vListaUnidade1, vListaUnidade2 : TStringList;

  vvASSIDUIDADE,
  vvDISCIPLINA,
  vvINICIATIVA,
  vvPRODUTIVIDADE,
  vvRESPONSABILIDADE,
  vIndex : Integer;

  vvMUDA_ABA : Boolean;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil, rDesempenhoAprendiz,
  uSelecionaSemAnoDesempAprendiz, uSelecionaEmpresaAvDesemp;

{$R *.DFM}

procedure TfrmNovaAvaliacaoAprendiz.btnPesquisarAdolescenteClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    mskMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value        := vvCOD_UNIDADE;
        Open;

        if Copy(dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value,1,3) <> '006' then
        begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'O adolescente selecionado n�o faz parte do Programa Aprendiz.' + #13+#10 +
                                  'Favor selecionar outro.',
                                  '[Sistema Deca] - Avalia��o Aprendiz',
                                  MB_OK + MB_ICONINFORMATION);
          btnPesquisarAdolescente.Click;
        end
        else
        begin
          edNome.Text := FieldByName('nom_nome').AsString;
          mskDataInicioPratica.SetFocus;
        end;

      end;
    except end;
  end
end;

procedure TfrmNovaAvaliacaoAprendiz.FormShow(Sender: TObject);
begin

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();
  gbAprendiz.Visible    := False;
  gbUnidade.Visible     := False;
  gbSemestreAno.Visible := False;

  vvASSIDUIDADE      := 0;
  vvDISCIPLINA       := 0;
  vvINICIATIVA       := 0;
  vvPRODUTIVIDADE    := 0;
  vvRESPONSABILIDADE := 0;

  vvMUDA_ABA := False;

  AtivaCamposBotoes('P');

  try

    //Carrega a lista de unidades do Aprendiz
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      cbCcusto.Items.Clear;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').Value),1,1) = '5') then
        begin
          //cbCcusto.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').Value) + '-' + dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          cbCcusto.Items.Add(dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add(dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        end
        else
        begin

        end;

        dmDeca.cdsSel_Unidade.Next;

      end;
    end;

  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar os dados das unidades.' +#13+#10+
                            'Entre em contato com o Administrador do Sistema e reporte esse erro.',
                            '[Sistema Deca] - Erro carregando unidades...',
                            MB_OK + MB_ICONERROR);



  end;

  //Carrega os fatores para a avalia��o
  lstFatores.Items.Clear;
  with dmDeca.cdsSel_AvDesmp_Fatores do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_fator').Value := Null;
    Params.ParamByName ('@pe_dsc_fator').Value    := Null;
    Params.ParamByName ('@pe_flg_status').Value   := 0; //Apenas os ATIVOS
    Open;

    while not dmDeca.cdsSel_AvDesmp_Fatores.eof do
    begin
      lstFatores.Items.Add (dmDeca.cdsSel_AvDesmp_Fatores.FieldByName ('dsc_fator').Value);
      dmDeca.cdsSel_AvDesmp_Fatores.Next;
    end;
  end;


  dsSel_AvDesemp.DataSet := Nil;


end;

procedure TfrmNovaAvaliacaoAprendiz.lstFatoresClick(Sender: TObject);
begin

  try

    with dmDeca.cdsSel_AvDesemp_Niveis_Desemp do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_nivel').Value     := Null;
      Params.ParamByName ('@pe_cod_id_fator').Value     := lstFatores.ItemIndex + 1;
      Params.ParamByName ('@pe_dsc_conceito').Value     := Null;
      Params.ParamByName ('@pe_flg_status_fator').Value := Null;
      Params.ParamByName ('@pe_flg_status_nivel').Value := 0;
      Params.ParamByName ('@pe_vlr_conceito').Value     := Null;
      Open;

      cbNivelDesempenho.Clear;
      while not dmDeca.cdsSel_AvDesemp_Niveis_Desemp.eof do
      begin
        cbNivelDesempenho.Items.Add ('|' + IntToStr(dmDeca.cdsSel_AvDesemp_Niveis_Desemp.FieldByName ('vlr_conceito').Value) + '| ' + dmDeca.cdsSel_AvDesemp_Niveis_Desemp.FieldByName ('dsc_nivel').Value);
        dmDeca.cdsSel_AvDesemp_Niveis_Desemp.Next;
      end;
    end;

  except end;

end;

procedure TfrmNovaAvaliacaoAprendiz.btnAgregaNivelFatorClick(Sender: TObject);
begin

  if (cbNivelDesempenho.ItemIndex < 0) then
  begin
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O N�VEL DE DESEMPENHO para o fator deve ser selecionado...',
                            '[Sistema Deca] - Selecionar N�VEL DE DESEMPENHO',
                            MB_OK + MB_ICONINFORMATION);
    cbNivelDesempenho.SetFocus;
  end
  else
  begin
    case lstFatores.ItemIndex of
      0: vvASSIDUIDADE      := StrToInt(Copy(cbNivelDesempenho.Text,2,1));
      1: vvDISCIPLINA       := StrToInt(Copy(cbNivelDesempenho.Text,2,1));
      2: vvINICIATIVA       := StrToInt(Copy(cbNivelDesempenho.Text,2,1));
      3: vvPRODUTIVIDADE    := StrToInt(Copy(cbNivelDesempenho.Text,2,1));
      4: vvRESPONSABILIDADE := StrToInt(Copy(cbNivelDesempenho.Text,2,1));
    end;

    Label6.Caption  := IntToStr(vvASSIDUIDADE);
    Label7.Caption  := IntToStr(vvDISCIPLINA);
    Label8.Caption  := IntToStr(vvINICIATIVA);
    Label9.Caption  := IntToStr(vvPRODUTIVIDADE);
    Label10.Caption := IntToStr(vvRESPONSABILIDADE);
    
  end;  
end;

procedure TfrmNovaAvaliacaoAprendiz.rgCriterioClick(Sender: TObject);
begin
  case rgCriterio.ItemIndex of
    0: begin
      gbAprendiz.Visible    := True;
      gbUnidade.Visible     := False;
      gbSemestreAno.Visible := True;
    end;

    1: begin
      gbAprendiz.Visible    := False;
      gbUnidade.Visible     := True;
      gbSemestreAno.Visible := True;
    end;

  end;
end;

procedure TfrmNovaAvaliacaoAprendiz.cbCcustoClick(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value  := StrToInt(vListaUnidade1.Strings[cbCcusto.ItemIndex]);
      Params.ParamByName ('@pe_num_secao').Value    := Null;
      Params.ParamByName ('@pe_flg_status').Value   := 0;
      Open;

      cbSecao.Clear;
      while not dmDeca.cdsSel_Secao.eof do
      begin
        cbSecao.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value);
        dmDeca.cdsSel_Secao.Next;
      end;
    end;
  except end;

end;

procedure TfrmNovaAvaliacaoAprendiz.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    mskMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value        := vvCOD_UNIDADE;
        Open;

        if Copy(dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value,1,3) <> '006' then
        begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'O adolescente selecionado n�o faz parte do Programa Aprendiz.' + #13+#10 +
                                  'Favor selecionar outro.',
                                  '[Sistema Deca] - Avalia��o Aprendiz',
                                  MB_OK + MB_ICONINFORMATION);
          btnPesquisarAdolescente.Click;
        end
        else
        begin
          mskMatriculaP.Text    := FieldByName('cod_matricula').AsString;
          edNomeP.Text          := FieldByName('nom_nome').AsString;
          gbSemestreAno.Visible := True;
          cbSemestreP.SetFocus;
        end;

      end;
    except end;
  end
end;

procedure TfrmNovaAvaliacaoAprendiz.AtivaCamposBotoes(Modo: String);
begin
  if Modo = 'P' then
  begin
    PageControl1.ActivePageIndex := 0;
    vvMUDA_ABA                   := False;
    rgCriterio.ItemIndex         := -1;
    cbUnidade.ItemIndex          := -1;
    cbSemestreP.ItemIndex        := -1;
    mskMatriculaP.Clear;
    edNomeP.Clear;
    mskAno.Clear;

    btnNova.Enabled           := True;
    btnSalvar.Enabled         := False;
    btnAlterar.Enabled        := False;
    btnCancelar.Enabled       := False;
    btnVisualizar.Enabled     := True;
    btnSair.Enabled           := True;
    btnAcompanhamento.Enabled := True;
  end
  else if Modo = 'N' then
  begin
    vvMUDA_ABA                   := True;
    PageControl1.ActivePageIndex := 1;
    vvMUDA_ABA                   := False;
    rgCriterio.ItemIndex         := -1;
    cbUnidade.ItemIndex          := -1;
    cbSemestreP.ItemIndex        := -1;
    mskMatriculaP.Clear;
    mskMatricula.Clear;
    edNome.Clear;
    mskDataInicioPratica.Clear;
    cbSemestre.ItemIndex := -1;
    mskAno.Clear;
    cbCcusto.ItemIndex := -1;
    cbSecao.Clear;
    edNomeResponsavel.Clear;
    Label6.Caption := '000';
    Label7.Caption := '000';
    Label8.Caption := '000';
    Label9.Caption := '000';
    Label10.Caption := '000';
    meConsideracoesSocial.Clear;
    meAdolescenteAprendiz.Clear;
    edNomeP.Clear;
    mskAno.Clear;

    btnNova.Enabled           := False;
    btnSalvar.Enabled         := True;
    btnAlterar.Enabled        := False;
    btnCancelar.Enabled       := True;
    btnVisualizar.Enabled     := False;
    btnSair.Enabled           := False;
    btnAcompanhamento.Enabled := False;
  end
  else if Modo = 'A' then
  begin
    vvMUDA_ABA                   := True;
    PageControl1.ActivePageIndex := 1;
    vvMUDA_ABA                   := False;
    rgCriterio.ItemIndex         := -1;
    cbUnidade.ItemIndex          := -1;
    cbSemestreP.ItemIndex        := -1;
    mskMatriculaP.Clear;
    edNomeP.Clear;
    mskAno.Clear;

    btnNova.Enabled           := False;
    btnSalvar.Enabled         := False;
    btnAlterar.Enabled        := True;
    btnCancelar.Enabled       := True;
    btnVisualizar.Enabled     := False;
    btnSair.Enabled           := False;
    btnAcompanhamento.Enabled := False;
  end;
end;

procedure TfrmNovaAvaliacaoAprendiz.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA = False then AllowChange := False;
end;

procedure TfrmNovaAvaliacaoAprendiz.btnNovaClick(Sender: TObject);
begin
  AtivaCamposBotoes('N');
end;

procedure TfrmNovaAvaliacaoAprendiz.btnCancelarClick(Sender: TObject);
begin
  AtivaCamposBotoes('P');
end;

procedure TfrmNovaAvaliacaoAprendiz.btnConsultarClick(Sender: TObject);
begin

  //Verifica qual o crit�rio escolhido
  //Por adolescente ou Por unidade


  dsSel_AvDesemp.DataSet := dmDeca.cdsSel_AvDesemp;
  with dmDeca.cdsSel_AvDesemp do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_avdesemp').Value := Null;
    if rgCriterio.ItemIndex = 0 then
    begin
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(mskMatriculaP.Text);
      Params.ParamByName ('@pe_cod_unidade').Value   := Null
    end
    else if rgCriterio.ItemIndex = 1 then
    begin
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex])
    end;

    Params.ParamByName ('@pe_num_semestre').Value    := cbSemestreP.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano').Value         := GetValue(mskAnoP.Text);
    Open;
    dbgResultado.Refresh;
  end;
end;

procedure TfrmNovaAvaliacaoAprendiz.mskAnoExit(Sender: TObject);
begin
  //Verifica se existe avalia��o lan�ada para a matricula, semestre/ano...
  try
    with dmDeca.cdsSel_AvDesemp do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_avdesemp').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value   := mskMatricula.Text;
      Params.ParamByName ('@pe_num_semestre').Value    := cbSemestre.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_cod_unidade').Value     := Null;
      Open;

      if dmDeca.cdsSel_AvDesemp.RecordCount > 0 then
      begin
        Application.MessageBox ('Aten��o!!! J� existe um lan�amento para a MATR�CULA, NO SEMESTRE E ANO informados.' +#13+#10 +
                                'Favor consultar os lan�amentos para a matr�cula informada.',
                                '[Sistema Deca] - Lan�amento de Avalia��es Aprendiz',
                                MB_OK + MB_ICONERROR);
        AtivaCamposBotoes ('P');
      end;

    end;
  except end;
end;

procedure TfrmNovaAvaliacaoAprendiz.btnSalvarClick(Sender: TObject);
begin

  try
    with dmDeca.cdsIns_AvDesemp do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value      := GetValue(mskMatricula.Text);
      Params.ParamByName ('@pe_num_semestre').Value       := cbSemestre.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value            := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbCcusto.ItemIndex]);
      Params.ParamByName ('@pe_num_secao').Value          := Copy(cbSecao.Text,1,4);
      Params.ParamByName ('@pe_vlr_fator1').Value         := vvASSIDUIDADE;
      Params.ParamByName ('@pe_vlr_fator2').Value         := vvDISCIPLINA;
      Params.ParamByName ('@pe_vlr_fator3').Value         := vvINICIATIVA;
      Params.ParamByName ('@pe_vlr_fator4').Value         := vvPRODUTIVIDADE;
      Params.ParamByName ('@pe_vlr_fator5').Value         := vvRESPONSABILIDADE;
      Params.ParamByName ('@pe_dsc_consid_ssocial').Value := GetValue(meConsideracoesSocial.Text);
      Params.ParamByName ('@pe_dsc_obs_aprendiz').Value   := GetValue(meAdolescenteAprendiz.Text);
      Params.ParamByName ('@pe_nom_responsavel').Value    := GetValue(edNomeResponsavel.Text);
      Params.ParamByName ('@pe_dat_inicio_pratica').Value := GetValue(mskDataInicioPratica, vtDate);
      Execute;

      with dmDeca.cdsSel_AvDesemp do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_avdesemp').Value := Null;
        Params.ParamByName ('@pe_cod_matricula').Value   := Null;
        Params.ParamByName ('@pe_num_semestre').Value    := Null;
        Params.ParamByName ('@pe_num_ano').Value         := Null;
        Params.ParamByName ('@pe_cod_unidade').Value     := Null;
        Open;
        dbgResultado.Refresh;
        AtivaCamposBotoes('P');
      end;

    end;

  except end;

end;

procedure TfrmNovaAvaliacaoAprendiz.btnVisualizarClick(Sender: TObject);
begin
  Application.CreateForm (TrelDesempenhoAprendiz, relDesempenhoAprendiz);
  relDesempenhoAprendiz.qrlSemestre.Caption := cbSemestreP.Text + '/' + mskAnoP.Text;

  //Calcula as m�dias por item e no geral da empresa
  relDesempenhoAprendiz.Preview;
  relDesempenhoAprendiz.Free;
end;

procedure TfrmNovaAvaliacaoAprendiz.btnAcompanhamentoClick(
  Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaSemAnoDesemAprendiz, frmSelecionaSemAnoDesemAprendiz);
  frmSelecionaSemAnoDesemAprendiz.ShowModal;
end;

procedure TfrmNovaAvaliacaoAprendiz.dbgResultadoDblClick(Sender: TObject);
begin

  AtivaCamposBotoes('A');
  PageControl1.ActivePageIndex := 1;

  //Repassar os dados aos campos do formul�rio e permitir a altera��o do mesmo...
  mskMatricula.Text         := dmDeca.cdsSel_AvDesemp.FieldByName ('cod_matricula').Value;
  edNome.Text               := dmDeca.cdsSel_AvDesemp.FieldByName ('nom_nome').Value;

  mskDataAvaliacao.Text     := dmDeca.cdsSel_AvDesemp.FieldByName ('DAT_AVALIACAO').AsString;

  if dmDeca.cdsSel_AvDesemp.FieldByName ('INICIO_PRATICA').IsNull then
    mskDataInicioPratica.Text := '  /  /    '
  else
    mskDataInicioPratica.Text := dmDeca.cdsSel_AvDesemp.FieldByName ('INICIO_PRATICA').AsString;

  cbSemestre.ItemIndex      := dmDeca.cdsSel_AvDesemp.FieldByName ('num_semestre').Value - 1;
  mskAno.Text               := dmDeca.cdsSel_AvDesemp.FieldByName ('num_ano').Value;

  //Altera��o de CCusto/Se��o
  vListaUnidade2.Find(dmDeca.cdsSel_AvDesemp.FieldByName('nom_unidade').AsString, vIndex);
  cbCcusto.ItemIndex        := vIndex;

  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value  := StrToInt(vListaUnidade1.Strings[cbCcusto.ItemIndex]);
      Params.ParamByName ('@pe_num_secao').Value    := Null;
      Params.ParamByName ('@pe_flg_status').Value   := 0;
      Open;

      cbSecao.Clear;
      while not dmDeca.cdsSel_Secao.eof do
      begin
        cbSecao.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value);
        dmDeca.cdsSel_Secao.Next;
      end;
    end;
  except end;

  cbSecao.Text              := dmDeca.cdsSel_AvDesemp.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_AvDesemp.FieldByName ('dsc_nom_secao').AsString;

  if dmDeca.cdsSel_AvDesemp.FieldByName ('nom_responsavel').IsNull then
    edNomeResponsavel.Text := ''
  else
    edNomeResponsavel.Text  := dmDeca.cdsSel_AvDesemp.FieldByName ('nom_responsavel').Value;

  if dmDeca.cdsSel_AvDesemp.FieldByName ('dsc_consid_ssocial').IsNull then
    meConsideracoesSocial.Text := ''
  else meConsideracoesSocial.Text := dmDeca.cdsSel_AvDesemp.FieldByName ('dsc_consid_ssocial').Value;

  if dmDeca.cdsSel_AvDesemp.FieldByName ('dsc_obs_aprendiz').IsNull then
    meAdolescenteAprendiz.Text := ''
  else meAdolescenteAprendiz.Text := dmDeca.cdsSel_AvDesemp.FieldByName ('dsc_obs_aprendiz').Value;

  Label6.Caption  := IntToStr(dmDeca.cdsSel_AvDesemp.FieldByName ('vlr_fator1').AsInteger);
  Label7.Caption  := IntToStr(dmDeca.cdsSel_AvDesemp.FieldByName ('vlr_fator2').AsInteger);
  Label8.Caption  := IntToStr(dmDeca.cdsSel_AvDesemp.FieldByName ('vlr_fator3').AsInteger);
  Label9.Caption  := IntToStr(dmDeca.cdsSel_AvDesemp.FieldByName ('vlr_fator4').AsInteger);
  Label10.Caption := IntToStr(dmDeca.cdsSel_AvDesemp.FieldByName ('vlr_fator5').AsInteger);

end;

procedure TfrmNovaAvaliacaoAprendiz.btnSairClick(Sender: TObject);
begin
  frmNovaAvaliacaoAprendiz.Close;
end;

procedure TfrmNovaAvaliacaoAprendiz.btnAlterarClick(Sender: TObject);
begin

  try
    with dmDeca.cdsAlt_AvDesemp do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_avdesemp').Value    := dmDeca.cdsSel_AvDesemp.FieldByName ('cod_id_avdesemp').Value;
      Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbCcusto.ItemIndex]);
      Params.ParamByName ('@pe_num_secao').Value          := Copy(cbSecao.Text,1,4);
      Params.ParamByName ('@pe_vlr_fator1').Value         := StrToInt(Label6.Caption);
      Params.ParamByName ('@pe_vlr_fator2').Value         := StrToInt(Label7.Caption);
      Params.ParamByName ('@pe_vlr_fator3').Value         := StrToInt(Label8.Caption);
      Params.ParamByName ('@pe_vlr_fator4').Value         := StrToInt(Label9.Caption);
      Params.ParamByName ('@pe_vlr_fator5').Value         := StrToInt(Label10.Caption);
      Params.ParamByName ('@pe_dsc_consid_ssocial').Value := GetValue(meConsideracoesSocial.Text);
      Params.ParamByName ('@pe_dsc_obs_aprendiz').Value   := GetValue(meAdolescenteAprendiz.Text);
      Params.ParamByName ('@pe_nom_responsavel').Value    := GetValue(edNomeResponsavel.Text);
      Params.ParamByName ('@pe_dat_inicio_pratica').Value := GetValue(mskDataInicioPratica, vtDate);
      Execute;

      //Atualiza os dados do grid
      //AtivaCamposBotoes('P');
      PageControl1.ActivePageIndex := 0;

      //Atualiza os dados do grid de resultados...
      with dmDeca.cdsSel_AvDesemp do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_avdesemp').Value := Null;
        Params.ParamByName ('@pe_cod_matricula').Value   := Null;
        Params.ParamByName ('@pe_num_semestre').Value    := Null;
        Params.ParamByName ('@pe_num_ano').Value         := Null;
        Params.ParamByName ('@pe_cod_unidade').Value     := Null;
        Open;
        dbgResultado.Refresh;
        AtivaCamposBotoes('P');
      end;      
            
    end;
  except end;

end;

procedure TfrmNovaAvaliacaoAprendiz.btnRelatorioEstatisticoClick(
  Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaEmpresaAvDesemp, frmSelecionaEmpresaAvDesemp);
  frmSelecionaEmpresaAvDesemp.lbSemestreAno.Caption := cbSemestreP.Text + ' / ' + mskAnoP.Text;
  frmSelecionaEmpresaAvDesemp.ShowModal;
end;

end.
