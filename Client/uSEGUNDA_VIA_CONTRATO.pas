unit uSEGUNDA_VIA_CONTRATO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls;

type
  TfrmSEGUNDA_VIA_CONTRATO = class(TForm)
    GroupBox1: TGroupBox;
    edtNOME: TEdit;
    SpeedButton1: TSpeedButton;
    chkOCULTAR_MATRICULA: TCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSEGUNDA_VIA_CONTRATO: TfrmSEGUNDA_VIA_CONTRATO;

implementation

uses uFichaPesquisa, uFichaCrianca, uPrincipal, uDM, rCONTRATO,
  udmTriagemDeca, rFICHA_ADMISSAO;

{$R *.DFM}

procedure TfrmSEGUNDA_VIA_CONTRATO.SpeedButton1Click(Sender: TObject);
var data, mmNOME_MAE, mmNOME_PAI, mmNOME_RESPONSAVEL : String;
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := vvCOD_MATRICULA_PESQUISA;
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
        Open;

        edtNOME.Text := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
        mmNOME_RESPONSAVEL := '';

        with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value   := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_ind_responsavel').Value := 1;
          Open;
          mmNOME_RESPONSAVEL := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_familiar').Value;
        end;
      end;

      Application.CreateForm(TrelCONTRATO, relCONTRATO);

      if chkOCULTAR_MATRICULA.Checked then relCONTRATO.qrMATRICULA.Caption  := '' else relCONTRATO.qrMATRICULA.Caption  := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      relCONTRATO.qrNOME1.Caption      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
      relCONTRATO.qrNOME2.Caption      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
      relCONTRATO.qrNASCIMENTO.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value;
      data := dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').Value;
      data := (FORMATDATETIME('dd "de" mmmm "de" yyyy', StrToDate(data)));
      relCONTRATO.qrADMISSAO.Caption   := dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').Value;
      relCONTRATO.qrINFO_DATA.Caption  := 'S�O JOS� DOS CAMPOS, ' + data + '.';

      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').IsNull) then relCONTRATO.qrCPF_ALUNO.Caption := '' else relCONTRATO.qrCPF_ALUNO.Caption  := dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').AsString;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').IsNull)  then relCONTRATO.qrRG_ALUNO.Caption  := '' else relCONTRATO.qrRG_ALUNO.Caption   := dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').AsString;
      if ( (dmDeca.cdsSel_Cadastro.FieldByName ('ind_estado_civil').IsNull) or (dmDeca.cdsSel_Cadastro.FieldByName ('ind_estado_civil').Value = -1) ) then relCONTRATO.qrESTADO_CIVIL.Caption    := '--' else relCONTRATO.qrESTADO_CIVIL.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('vEstadoCivil').Value;  


      if (dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('num_cpf_fam').IsNull)       then relCONTRATO.qrCPF_RESPONSAVEL.Caption := '' else relCONTRATO.qrCPF_RESPONSAVEL.Caption := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('num_cpf_fam').AsString;
      if (dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('num_rg_fam').IsNull)        then relCONTRATO.qrRG_RESPONSAVEL.Caption  := '' else relCONTRATO.qrRG_RESPONSAVEL.Caption  := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('num_rg_fam').AsString;

      if (dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('dsc_ocupacao').IsNull)      then relCONTRATO.qrPROFISSAO.Caption       := '' else relCONTRATO.qrPROFISSAO.Caption       := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('dsc_ocupacao').AsString;
      relCONTRATO.qrNACIONALIDADE.Caption := '';
      relCONTRATO.qrRESPONSAVEL.Caption   := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_familiar').Value;
      relCONTRATO.qrRESPONSAVEL2.Caption  := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_familiar').Value;

      relCONTRATO.qrENDERECO.Caption      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_endereco').AsString + ', ' + dmDeca.cdsSel_Cadastro.FieldByName ('nom_complemento').AsString + '  -  ' + '  ' +
                                          dmDeca.cdsSel_Cadastro.FieldByName ('nom_bairro').AsString;
      relCONTRATO.qrNACIONALIDADE.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nacionalidade').AsString;

      relCONTRATO.Preview;
      relCONTRATO.Free;

      {***************************************************************************}
      
      Application.CreateForm(TrelFICHA_ADMISSAO, relFICHA_ADMISSAO);
      relFICHA_ADMISSAO.qrTITULO_FICHA.Caption            := 'FICHA DE ADMISS�O - CADASTRO DE CRIAN�A/ADOLESCENTE';
      relFICHA_ADMISSAO.qrMATRICULA.Caption               := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      relFICHA_ADMISSAO.qrNOME.Caption                    := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
      relFICHA_ADMISSAO.qrNASCIMENTO.Caption              := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value;
      relFICHA_ADMISSAO.qrCRACHA.Caption                  := '';
      relFICHA_ADMISSAO.qrENDERECO.Caption                := dmDeca.cdsSel_Cadastro.FieldByName ('nom_endereco').AsString;

      if (dmDeca.cdsSel_Cadastro.FieldByName ('nom_complemento').IsNull) then relFICHA_ADMISSAO.qrCOMPLEMENTO.Caption      := '' else relFICHA_ADMISSAO.qrCOMPLEMENTO.Caption      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_complemento').AsString;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('nom_bairro').IsNull)      then relFICHA_ADMISSAO.qrBAIRRO.Caption           := '' else relFICHA_ADMISSAO.qrBAIRRO.Caption           := dmDeca.cdsSel_Cadastro.FieldByName ('nom_bairro').AsString;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cep').IsNull)         then relFICHA_ADMISSAO.qrCEP.Caption              := '' else relFICHA_ADMISSAO.qrCEP.Caption              := dmDeca.cdsSel_Cadastro.FieldByName ('num_cep').AsString;

      //Recuperar o nome da m�e, do pai e do respons�vel da ficha atual
      try
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_cod_familiar').Value  := Null;
          Open;

          mmNOME_MAE         := '';
          mmNOME_PAI         := '';

          while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
          begin
            if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'M�E') then mmNOME_MAE := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value
            else if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'PAI') then mmNOME_PAI := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
            dmDeca.cdsSel_Cadastro_Familia.Next;
          end;
        end;
      except end;

      relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := mmNOME_RESPONSAVEL;
      relFICHA_ADMISSAO.qrNOME_MAE.Caption         := mmNOME_MAE;
      relFICHA_ADMISSAO.qrNOME_PAI.Caption         := mmNOME_PAI;

      relFICHA_ADMISSAO.qrLOCAL_NASCIMENTO.Caption        := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_local_nascimento').Value;
      relFICHA_ADMISSAO.qrMUNICIPIO_RESIDE.Caption        := 'S�O JOS� DOS CAMPOS/SP';

      if ( (dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone').IsNull) or (dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone').AsString = '(99) 9999-9999') ) then
        relFICHA_ADMISSAO.qrTELEFONE.Caption              := ''
      else relFICHA_ADMISSAO.qrTELEFONE.Caption           := dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone').Value;

      if ( (dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone_celular').IsNull) or (dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone_celular').AsString = '(99)99999-9999') ) then
        relFICHA_ADMISSAO.qrCELULAR.Caption               := ''
      else relFICHA_ADMISSAO.qrCELULAR.Caption            := dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone_celular').Value;

      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nom_conjuge').IsNull) then relFICHA_ADMISSAO.qrCONJUGE.Caption := '' else relFICHA_ADMISSAO.qrCONJUGE.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nom_conjuge').Value;

      if (dmDeca.cdsSel_Cadastro.FieldByName ('vEstadoCivil').IsNull) then relFICHA_ADMISSAO.qrESTADO_CIVIL.Caption := '' else relFICHA_ADMISSAO.qrESTADO_CIVIL.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('vEstadoCivil').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nacionalidade').IsNull) then relFICHA_ADMISSAO.qrNACIONALIDADE.Caption := '' else relFICHA_ADMISSAO.qrNACIONALIDADE.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nacionalidade').Value;

      relFICHA_ADMISSAO.qrMORA_SIM.Caption                := 'SIM';
      relFICHA_ADMISSAO.qrMORA_NAO.Caption                := 'N�O';

      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nacionalidade').IsNull) then relFICHA_ADMISSAO.qrNACIONALIDADE.Caption := '' else relFICHA_ADMISSAO.qrNACIONALIDADE.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_nacionalidade').Value;

      relFICHA_ADMISSAO.qrGRAU_INSTRUCAO.Caption          := '';
      relFICHA_ADMISSAO.qrRACA.Caption                    := '';

      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_cor_cabelos').IsNull) then relFICHA_ADMISSAO.qrCABELOS.Caption := ''  else relFICHA_ADMISSAO.qrCABELOS.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_cor_cabelos').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_cor_olhos').IsNull) then relFICHA_ADMISSAO.qrOLHOS.Caption := ''  else relFICHA_ADMISSAO.qrOLHOS.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_cor_olhos').Value;

      relFICHA_ADMISSAO.qrSANGUE.Caption                  := '';

      if (dmDeca.cdsSel_Cadastro.FieldByName ('Val_Peso').IsNull) then relFICHA_ADMISSAO.qrPESO.Caption := ''  else relFICHA_ADMISSAO.qrPESO.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('Val_Peso').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('Val_Altura').IsNull) then relFICHA_ADMISSAO.qrALTURA.Caption := ''  else relFICHA_ADMISSAO.qrALTURA.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('Val_Altura').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_calcado').IsNull) then relFICHA_ADMISSAO.qrCALCADO.Caption := ''  else relFICHA_ADMISSAO.qrCALCADO.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('num_calcado').Value;

      relFICHA_ADMISSAO.qrFUMA_SIM.Caption                := 'SIM';
      relFICHA_ADMISSAO.qrFUMA_NAO.Caption                := 'N�O';
      relFICHA_ADMISSAO.qrGUARDAREGULAR_SIM.Caption       := 'SIM';
      relFICHA_ADMISSAO.qrGUARDAREGULAR_NAO.Caption       := 'N�O';
      relFICHA_ADMISSAO.qrNECESSIDADES_SIM.Caption        := 'SIM';
      relFICHA_ADMISSAO.qrNECESSIDADES_NAO.Caption        := 'N�O';

      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_necessidades_esp_quais').IsNull) then relFICHA_ADMISSAO.qrQUAIS_NECESSIDADES.Caption := '' else relFICHA_ADMISSAO.qrQUAIS_NECESSIDADES.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_necessidades_esp_quais').Value;

      relFICHA_ADMISSAO.qrCENTRO_CUSTO_ATUAL.Caption      := '2027 - SE��O TRIAGEM';
      relFICHA_ADMISSAO.qrCENTRO_CUSTO_DESTINO.Caption    := Trim(dmDeca.cdsSel_Cadastro.FieldByName ('num_ccusto').Value) + '-' + dmDeca.cdsSel_Cadastro.FieldByName ('nom_unidade').Value;

      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').IsNull)              then relFICHA_ADMISSAO.qrRG.Caption                  := '' else relFICHA_ADMISSAO.qrRG.Caption                  := dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dat_emissao_rg').IsNull)      then relFICHA_ADMISSAO.qrDATA_EMISSAO_RG.Caption     := '' else relFICHA_ADMISSAO.qrDATA_EMISSAO_RG.Caption     := dmDeca.cdsSel_Cadastro.FieldByName ('dat_emissao_rg').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_emissor').IsNull)      then relFICHA_ADMISSAO.qrEMISSOR.Caption             := '' else relFICHA_ADMISSAO.qrEMISSOR.Caption             := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_emissor').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_uf_rg').IsNull)           then relFICHA_ADMISSAO.qrUF.Caption                  := '' else relFICHA_ADMISSAO.qrUF.Caption                  := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_uf_rg').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').IsNull)             then relFICHA_ADMISSAO.qrCPF.Caption                 := '' else relFICHA_ADMISSAO.qrCPF.Caption                 := dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_certidao').IsNull)        then relFICHA_ADMISSAO.qrCERTIDAO_NASCIMENTO.Caption := '' else relFICHA_ADMISSAO.qrCERTIDAO_NASCIMENTO.Caption := dmDeca.cdsSel_Cadastro.FieldByName ('num_certidao').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_titulo_eleitor').IsNull)  then relFICHA_ADMISSAO.qrTITULO_ELEITOR.Caption      := '' else relFICHA_ADMISSAO.qrTITULO_ELEITOR.Caption      := dmDeca.cdsSel_Cadastro.FieldByName ('num_titulo_eleitor').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_zona_eleitoral').IsNull)  then relFICHA_ADMISSAO.qrZONA_ELEITORAL.Caption      := '' else relFICHA_ADMISSAO.qrZONA_ELEITORAL.Caption      := dmDeca.cdsSel_Cadastro.FieldByName ('num_zona_eleitoral').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_secao_eleitoral').IsNull) then relFICHA_ADMISSAO.qrSECAO_ELEITORAL.Caption     := '' else relFICHA_ADMISSAO.qrSECAO_ELEITORAL.Caption     := dmDeca.cdsSel_Cadastro.FieldByName ('num_secao_eleitoral').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('ctps_num').isNull)            then relFICHA_ADMISSAO.qrCTPS.Caption                := '' else relFICHA_ADMISSAO.qrCTPS.Caption                := dmDeca.cdsSel_Cadastro.FieldByName ('ctps_num').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('ctps_serie').IsNull)          then relFICHA_ADMISSAO.qrSERIE_CTPS.Caption          := '' else relFICHA_ADMISSAO.qrSERIE_CTPS.Caption          := dmDeca.cdsSel_Cadastro.FieldByName ('ctps_serie').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dat_emissao_ctps').IsNull)    then relFICHA_ADMISSAO.qrEMISSAO_CTPS.Caption        := '' else relFICHA_ADMISSAO.qrEMISSAO_CTPS.Caption        := dmDeca.cdsSel_Cadastro.FieldByName ('dat_emissao_ctps').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_uf_ctps').IsNull)         then relFICHA_ADMISSAO.qrUF_CTPS.Caption             := '' else relFICHA_ADMISSAO.qrUF_CTPS.Caption             := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_uf_ctps').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_pis').IsNull)             then relFICHA_ADMISSAO.qrPIS.Caption                 := '' else relFICHA_ADMISSAO.qrPIS.Caption                 := dmDeca.cdsSel_Cadastro.FieldByName ('num_pis').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dat_emissao_pis').IsNull)     then relFICHA_ADMISSAO.qrPIS_EMISSAO.Caption         := '' else relFICHA_ADMISSAO.qrPIS_EMISSAO.Caption         := dmDeca.cdsSel_Cadastro.FieldByName ('dat_emissao_pis').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_banco_pis').IsNull)       then relFICHA_ADMISSAO.qrPIS_BANCO.Caption           := '' else relFICHA_ADMISSAO.qrPIS_BANCO.Caption           := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_banco_pis').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cartao_sus').IsNull)      then relFICHA_ADMISSAO.qrSUS.Caption                 := '' else relFICHA_ADMISSAO.qrSUS.Caption                 := dmDeca.cdsSel_Cadastro.FieldByName ('num_cartao_sus').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cartaosias').IsNull)      then relFICHA_ADMISSAO.qrSIAS.Caption                := '' else relFICHA_ADMISSAO.qrSIAS.Caption                := dmDeca.cdsSel_Cadastro.FieldByName ('num_cartaosias').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_reservista').IsNull)      then relFICHA_ADMISSAO.qrRESERVISTA.Caption          := '' else relFICHA_ADMISSAO.qrRESERVISTA.Caption          := dmDeca.cdsSel_Cadastro.FieldByName ('num_reservista').Value;
      if (dmDeca.cdsSel_Cadastro.FieldByName ('num_reservista_csm').IsNull)  then relFICHA_ADMISSAO.qrCSM.Caption                 := '' else relFICHA_ADMISSAO.qrCSM.Caption                 := dmDeca.cdsSel_Cadastro.FieldByName ('num_reservista_csm').Value;

      relFICHA_ADMISSAO.qrNATURALIZADO_SIM.Caption        := 'SIM';
      relFICHA_ADMISSAO.qrNATURALIZADO_NAO.Caption        := 'N�O';
      relFICHA_ADMISSAO.qrDATA_CHEGA_BRASIL.Caption       := '';
      relFICHA_ADMISSAO.qrCART_N19.Caption                := '';
      relFICHA_ADMISSAO.qrQTD_FILHOS_BRASIL.Caption       := '';
      relFICHA_ADMISSAO.qrTOTAL_FILHOS.Caption            := '';
      relFICHA_ADMISSAO.qrNUM_REG_GERAL.Caption           := '';
      relFICHA_ADMISSAO.qrVALIDAE_REGISTRO.Caption        := '';
      relFICHA_ADMISSAO.qrVALIDADE_CTPS.Caption           := '';

      relFICHA_ADMISSAO.qrPERIODO_ATIVIDADE.Caption       := '';
      relFICHA_ADMISSAO.qrPRIMEIRO_EMPREGO.Caption        := '';
      relFICHA_ADMISSAO.qrTERMINO_CONTRATO.Caption        := '';
      relFICHA_ADMISSAO.qrEXAME_MEDICO.Caption            := '';
      relFICHA_ADMISSAO.qrCONTRATADO_SIM.Caption          := '';
      relFICHA_ADMISSAO.qrCONTRATADO_NAO.Caption          := '';
      relFICHA_ADMISSAO.qrNUM_AGENCIA.Caption             := '';
      relFICHA_ADMISSAO.qrNUM_CONTA.Caption               := '';
      relFICHA_ADMISSAO.qrSALARIO_BASE.Caption            := '';
      relFICHA_ADMISSAO.qrFUNCAO.Caption                  := '';
      relFICHA_ADMISSAO.qrHORARIO.Caption                 := '';
      relFICHA_ADMISSAO.qrNECESSITA_PASSE_SIM.Caption     := 'SIM';
      relFICHA_ADMISSAO.qrNECESSITA_PASSE_NAO.Caption     := 'N�O';
      relFICHA_ADMISSAO.qrCONTRIBUI_SINDICATO_SIM.Caption := 'SIM';
      relFICHA_ADMISSAO.qrCONTRIBUI_SINDICATO_NAO.Caption := 'N�O';

      data := dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').Value;
      data := (FORMATDATETIME('dd "de" mmmm "de" yyyy', StrToDate(data)));
      relFICHA_ADMISSAO.qrADMISSAO.Caption   := dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').Value;
      relFICHA_ADMISSAO.qrINFO_DATA.Caption  := 'S�O JOS� DOS CAMPOS, ' + data + '.';

      relFICHA_ADMISSAO.Preview;
      relFICHA_ADMISSAO.Free;
    except end;

  end;
end;

end.
