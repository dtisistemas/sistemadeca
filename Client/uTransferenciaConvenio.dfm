object frmTransferenciaConvenio: TfrmTransferenciaConvenio
  Left = 159
  Top = 202
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - Transfer�ncia para Conv�nio [Encaminhamento]'
  ClientHeight = 359
  ClientWidth = 780
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label20: TLabel
    Left = 322
    Top = 277
    Width = 126
    Height = 13
    Caption = 'N�m. Cotas de Passe:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 7
    Top = 8
    Width = 768
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR crian�a/adolescente'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnExit = txtMatriculaExit
    end
  end
  object GroupBox4: TGroupBox
    Left = 7
    Top = 57
    Width = 768
    Height = 45
    TabOrder = 1
    object Label2: TLabel
      Left = 128
      Top = 18
      Width = 122
      Height = 13
      Caption = 'DATA TRANSFER�NCIA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtDataTransferencia: TMaskEdit
      Left = 254
      Top = 14
      Width = 75
      Height = 21
      Hint = 'DATA da Transfer�ncia'
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
    end
    object rgOpcao: TRadioGroup
      Left = 344
      Top = 7
      Width = 228
      Height = 33
      Hint = 'Selecione DIVIS�O'
      Columns = 2
      Items.Strings = (
        'CRIAN�A'
        'ADOLESCENTE')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 7
    Top = 102
    Width = 378
    Height = 111
    TabOrder = 2
    object Label3: TLabel
      Left = 136
      Top = 14
      Width = 43
      Height = 13
      Caption = 'ORIGEM'
    end
    object Label5: TLabel
      Left = 8
      Top = 38
      Width = 40
      Height = 13
      Caption = 'Unidade'
    end
    object Label6: TLabel
      Left = 11
      Top = 59
      Width = 34
      Height = 13
      Caption = 'CCusto'
    end
    object Label7: TLabel
      Left = 101
      Top = 58
      Width = 26
      Height = 13
      Caption = 'Local'
    end
    object Label8: TLabel
      Left = 14
      Top = 81
      Width = 31
      Height = 13
      Caption = 'Banco'
    end
    object lbUnidade: TLabel
      Left = 56
      Top = 38
      Width = 112
      Height = 13
      Hint = 'Nome da UNIDADE Origem'
      Caption = 'XXXXXXXXXXXXXXXX'
      ParentShowHint = False
      ShowHint = True
    end
    object txtCcustoOrigem: TMaskEdit
      Left = 48
      Top = 56
      Width = 48
      Height = 21
      Hint = 'Centro de Custo de Origem da Transfer�ncia'
      EditMask = '0000;1;_'
      MaxLength = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '    '
    end
    object txtLocalOrigem: TEdit
      Left = 130
      Top = 55
      Width = 191
      Height = 21
      Hint = 'Unidade de Origem'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object txtBancoOrigem: TEdit
      Left = 48
      Top = 78
      Width = 153
      Height = 21
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox3: TGroupBox
    Left = 389
    Top = 102
    Width = 386
    Height = 111
    TabOrder = 3
    object Label4: TLabel
      Left = 143
      Top = 14
      Width = 48
      Height = 13
      Caption = 'DESTINO'
    end
    object Label9: TLabel
      Left = 8
      Top = 38
      Width = 40
      Height = 13
      Caption = 'Unidade'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 16
      Top = 59
      Width = 34
      Height = 13
      Caption = 'CCusto'
    end
    object Label11: TLabel
      Left = 107
      Top = 60
      Width = 26
      Height = 13
      Caption = 'Local'
    end
    object Label12: TLabel
      Left = 18
      Top = 81
      Width = 31
      Height = 13
      Caption = 'Banco'
    end
    object cbUnidades: TComboBox
      Left = 53
      Top = 34
      Width = 308
      Height = 22
      Hint = 'Selecione a UNIDADE Destino'
      Style = csOwnerDrawFixed
      ItemHeight = 16
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnExit = cbUnidadesExit
    end
    object txtCcustoDestino: TMaskEdit
      Left = 53
      Top = 56
      Width = 48
      Height = 21
      Hint = 'Centro de Custo Destino'
      EditMask = '0000;1;_'
      MaxLength = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '    '
    end
    object txtLocalDestino: TEdit
      Left = 136
      Top = 56
      Width = 191
      Height = 21
      Hint = 'Unidade Destino'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object txtBancoDestino: TEdit
      Left = 53
      Top = 78
      Width = 153
      Height = 21
      Hint = 'Banco para efeito de Transfer�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
  object GroupBox6: TGroupBox
    Left = 8
    Top = 215
    Width = 185
    Height = 87
    Caption = ' [         Altera��o de Fun��o          ]'
    TabOrder = 4
    object Label14: TLabel
      Left = 13
      Top = 32
      Width = 14
      Height = 13
      Caption = 'De'
    end
    object Label16: TLabel
      Left = 4
      Top = 55
      Width = 22
      Height = 13
      Caption = 'Para'
    end
    object txtFuncaoDe: TEdit
      Left = 32
      Top = 28
      Width = 146
      Height = 21
      Hint = 'Fun��o Anterior'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object txtFuncaoPara: TEdit
      Left = 32
      Top = 52
      Width = 146
      Height = 21
      Hint = 'Fun��o Atual'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox7: TGroupBox
    Left = 196
    Top = 215
    Width = 101
    Height = 87
    Caption = '[      Hor�rio       ]'
    TabOrder = 5
    object Label18: TLabel
      Left = 11
      Top = 29
      Width = 19
      Height = 13
      Caption = 'Das'
    end
    object Label19: TLabel
      Left = 18
      Top = 53
      Width = 12
      Height = 13
      Caption = 'As'
    end
    object txtHorario1: TMaskEdit
      Left = 32
      Top = 25
      Width = 53
      Height = 21
      Hint = 'In�cio das atividades'
      EditMask = '!90:00;1;_'
      MaxLength = 5
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  :  '
    end
    object txtHorario2: TMaskEdit
      Left = 32
      Top = 49
      Width = 53
      Height = 21
      Hint = 'T�rmino das Atividades'
      EditMask = '!90:00;1;_'
      MaxLength = 5
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  :  '
    end
  end
  object rgEntregaPasse: TRadioGroup
    Left = 300
    Top = 215
    Width = 225
    Height = 52
    Hint = 'Entraga COTAS de Passe na Transfer�ncia'
    Caption = 'Cotas de Passe: Entregar na Transfer�cia?'
    Items.Strings = (
      'Sim'
      'N�o')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
  end
  object txtCotas: TEdit
    Left = 451
    Top = 272
    Width = 30
    Height = 21
    Hint = 'N�mero de COTAS de Passe a serem Entregues'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 2
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    Text = '00'
  end
  object GroupBox8: TGroupBox
    Left = 528
    Top = 214
    Width = 121
    Height = 87
    Caption = '     [      Curso       ]'
    TabOrder = 8
    object Label15: TLabel
      Left = 15
      Top = 29
      Width = 27
      Height = 13
      Caption = 'In�cio'
    end
    object Label23: TLabel
      Left = 10
      Top = 53
      Width = 41
      Height = 13
      Caption = 'Dura��o'
    end
    object Label24: TLabel
      Left = 82
      Top = 53
      Width = 30
      Height = 13
      Caption = 'meses'
    end
    object txtInicioCurso: TMaskEdit
      Left = 47
      Top = 25
      Width = 63
      Height = 21
      Hint = 'Data in�cio Curso'
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
    end
    object txtDuracaoCurso: TEdit
      Left = 54
      Top = 50
      Width = 24
      Height = 21
      Hint = 'Dura��o do Curso'
      MaxLength = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '00'
    end
  end
  object GroupBox9: TGroupBox
    Left = 653
    Top = 214
    Width = 121
    Height = 87
    Caption = '   [      Est�gio       ]'
    TabOrder = 9
    object Label25: TLabel
      Left = 15
      Top = 29
      Width = 27
      Height = 13
      Caption = 'In�cio'
    end
    object Label26: TLabel
      Left = 10
      Top = 53
      Width = 41
      Height = 13
      Caption = 'Dura��o'
    end
    object Label27: TLabel
      Left = 82
      Top = 53
      Width = 30
      Height = 13
      Caption = 'meses'
    end
    object txtInicioEstagio: TMaskEdit
      Left = 47
      Top = 25
      Width = 63
      Height = 21
      Hint = 'Data In�cio Est�gio'
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
    end
    object txtDuracaoEstagio: TEdit
      Left = 54
      Top = 50
      Width = 24
      Height = 21
      Hint = 'Dura��o Est�gio'
      MaxLength = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '00'
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 306
    Width = 767
    Height = 49
    TabOrder = 10
    object btnNovo: TBitBtn
      Left = 415
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para lan�ar NOVA Transfer�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 490
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR dados da Transfer�ncia da crian�a/adolescente'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 550
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR a opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnImprimir: TBitBtn
      Left = 625
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para EXIBIR/IMPRIMIR o formul�rio de Transfer�ncias'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnImprimirClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777776B00777777777000777777776E007777777007880077777771007777
        7007780088007777740077770778878800880077770077778887778888008077
        7A00777887777788888800777D007778F7777F888888880780007778F77FF777
        8888880783007778FFF779977788880786007778F77AA7778807880789007777
        88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
        920077777777778FFFFFF0079500777777777778FFF887779800777777777777
        888777779B00777777777777777777779E0077777777777777777777A1007777
        7777777777777777A400}
    end
    object btnSair: TBitBtn
      Left = 700
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de Transfer�ncias'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
end
