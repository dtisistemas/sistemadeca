object frmNovoCadastroSecoes: TfrmNovoCadastroSecoes
  Left = 300
  Top = 187
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = '[Sistema Deca] - Cadastro de Se��es'
  ClientHeight = 434
  ClientWidth = 693
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 3
    Top = 5
    Width = 686
    Height = 426
    ActivePage = TabSheet2
    TabOrder = 0
    OnChange = PageControl1Change
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = 'Dados da sua Unidade'
      object Panel1: TPanel
        Left = 5
        Top = 4
        Width = 668
        Height = 182
        BevelOuter = bvLowered
        TabOrder = 0
        object Label1: TLabel
          Left = 64
          Top = 24
          Width = 77
          Height = 13
          Caption = 'Nome Unidade :'
        end
        object Label2: TLabel
          Left = 376
          Top = 24
          Width = 40
          Height = 13
          Caption = 'Regi�o :'
        end
        object Label3: TLabel
          Left = 73
          Top = 48
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'Centro Custo :'
        end
        object Label4: TLabel
          Left = 209
          Top = 48
          Width = 80
          Height = 13
          Caption = 'Nome Gestor(a) :'
        end
        object Label5: TLabel
          Left = 33
          Top = 72
          Width = 108
          Height = 13
          Alignment = taRightJustify
          Caption = 'Endere�o da unidade :'
        end
        object Label6: TLabel
          Left = 415
          Top = 72
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Complemento :'
        end
        object Label7: TLabel
          Left = 107
          Top = 96
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bairro :'
        end
        object Label8: TLabel
          Left = 367
          Top = 96
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'CEP :'
        end
        object Label9: TLabel
          Left = 83
          Top = 120
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Telefone 1 :'
        end
        object Label10: TLabel
          Left = 230
          Top = 120
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Telefone 2 :'
        end
        object Label11: TLabel
          Left = 106
          Top = 144
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'e-Mail :'
        end
        object Label12: TLabel
          Left = 381
          Top = 120
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Capacidade :'
        end
        object Label13: TLabel
          Left = 494
          Top = 24
          Width = 27
          Height = 13
          Caption = 'Tipo :'
        end
        object Label14: TLabel
          Left = 481
          Top = 120
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'alunos'
        end
        object edNomeUnidade: TEdit
          Left = 145
          Top = 21
          Width = 220
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 100
          TabOrder = 0
        end
        object cbRegiao: TComboBox
          Left = 419
          Top = 21
          Width = 71
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 1
          Items.Strings = (
            'CENTRO'
            'NORTE'
            'SUL'
            'LESTE'
            'OESTE')
        end
        object cbTipo: TComboBox
          Left = 527
          Top = 21
          Width = 92
          Height = 19
          Style = csOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 2
          Items.Strings = (
            'CRIAN�A'
            'ADOLESCENTE'
            'MISTA')
        end
        object edNomeGestor: TEdit
          Left = 291
          Top = 45
          Width = 327
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 100
          TabOrder = 3
        end
        object edEndereco: TEdit
          Left = 145
          Top = 69
          Width = 261
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 4
        end
        object edComplemento: TEdit
          Left = 488
          Top = 69
          Width = 130
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 5
        end
        object edBairro: TEdit
          Left = 145
          Top = 93
          Width = 214
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 6
        end
        object edEmail: TEdit
          Left = 145
          Top = 141
          Width = 368
          Height = 21
          CharCase = ecLowerCase
          TabOrder = 7
        end
        object edCapacidade: TEdit
          Left = 446
          Top = 117
          Width = 32
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 3
          TabOrder = 8
        end
        object mskCCusto: TMaskEdit
          Left = 146
          Top = 45
          Width = 39
          Height = 21
          CharCase = ecUpperCase
          Enabled = False
          EditMask = '0000'
          MaxLength = 4
          TabOrder = 9
          Text = '0000'
          OnExit = mskCCustoExit
        end
        object mskCep: TMaskEdit
          Left = 398
          Top = 93
          Width = 64
          Height = 21
          CharCase = ecUpperCase
          EditMask = '00.000-000'
          MaxLength = 10
          TabOrder = 10
          Text = '  .   -   '
        end
        object mskFone1: TMaskEdit
          Left = 145
          Top = 116
          Width = 79
          Height = 21
          CharCase = ecUpperCase
          EditMask = '(99) 9999-9999'
          MaxLength = 14
          TabOrder = 11
          Text = '(  )     -    '
        end
        object mskFone2: TMaskEdit
          Left = 292
          Top = 116
          Width = 79
          Height = 21
          CharCase = ecUpperCase
          EditMask = '(99) 9999-9999'
          MaxLength = 14
          TabOrder = 12
          Text = '(  )     -    '
        end
      end
      object Panel4: TPanel
        Left = 4
        Top = 360
        Width = 672
        Height = 36
        TabOrder = 1
        object btnNovo: TBitBtn
          Left = 359
          Top = 3
          Width = 55
          Height = 30
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnNovoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvar: TBitBtn
          Left = 426
          Top = 3
          Width = 55
          Height = 30
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnAlterar: TBitBtn
          Left = 490
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnAlterarClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FF00004300004300003C000037000036000036FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000930000930002841518892B
            2D8C2A2A830F0F6200004000003A00003AFF00FFFF00FFFF00FFFF00FFFF00FF
            0004B30104A73D45C3B3B7EAFFFFFFFFFFFFFFFFFFFFFFFFA0A2CC2D2D740000
            3A00004EFF00FFFF00FFFF00FF0005CC0107BA7F89E2FFFFFFFFFFFF9B9CDB5E
            5EB75F5FB6ADADDDFFFFFFFFFFFF5E5E9B00003A000043FF00FFFF00FF0005CC
            6472E5FFFFFFD7DAF52528B30102880A0DA206078C00006A31319CE6E6F7FFFF
            FF36367D000043FF00FF0007E80B1BD8F8F8FFF2F3FC1721C30405A41214A303
            048905068B01017409097630309CFFFFFFC3C3DB01015000004B0007E84358F0
            FFFFFF6476ED0104C00203950507910304870E109807088C03036E0909798686
            C9FFFFFF27278800004B0009F37F94FAFFFFFF1932F05867EAFCFCFDEBEDF8EA
            EAF7EAEBF7FFFFFFFFFFFF03047E3233A2FFFFFF5558BE00004A0218FDA6B4FD
            FFFFFF112CFD0C20F37E89F2FFFFFFFFFFFFFFFFFFFFFFFF0B0EBD0308AE5257
            BEFFFFFF6267C30000781735FFA4B6FFFFFFFF2C4AFF000FFF0518FCA3ACF3FF
            FFFFFFFFFF070EC70107C31017BE5E65CAFDFDFF4A4EBD00007F0318FF91A6FF
            FFFFFFA9B9FF000EFF0008FF0E1FFDA4AEF71420D8060ECA070CC3090FB7BABE
            EDFCFCFD1017AA00007FFF00FF5F7AFFFFFFFFFFFFFF5C75FF000BFF000EFF00
            0AFF0618F80007DB0006CC7380E8FFFFFF8F97E2000198FF00FFFF00FF425FFF
            C4CFFFFFFFFFFFFFFF8B9FFF162FFF0414FF0515FD2037F39FADF7FFFFFFE2E5
            FA101ABA000198FF00FFFF00FFFF00FF5975FFD7DFFFFFFFFFFFFFFFFCFCFFD1
            DBFFD4DFFFFFFFFFFFFFFFC6CDF71825CD0001B0FF00FFFF00FFFF00FFFF00FF
            FF00FF5C76FF97A9FDDAE0FFFFFFFFFFFFFFFFFFFFD4DBFD596FF00514D70001
            B0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF5C75FF5C79FF62
            7DFF4A66FD203CF50619E5FF00FFFF00FFFF00FFFF00FFFF00FF}
        end
        object btnCancelar: TBitBtn
          Left = 550
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnCancelarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnSair: TBitBtn
          Left = 612
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnSairClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
      end
      object Panel6: TPanel
        Left = 4
        Top = 190
        Width = 670
        Height = 168
        TabOrder = 2
        object dbgUnidades: TDBGrid
          Left = 8
          Top = 8
          Width = 651
          Height = 153
          DataSource = dsUnidades
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dbgUnidadesCellClick
          OnDblClick = dbgUnidadesDblClick
          OnKeyDown = dbgUnidadesKeyDown
          OnKeyUp = dbgUnidadesKeyUp
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'num_ccusto'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'C. Custo'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_unidade'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = 'Unidade/Projeto'
              Width = 251
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_gestor'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = 'Gestor(a)'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'num_telefone1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = 'Telefone'
              Visible = True
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Se��es/Departamentos/Empresas cadastradas'
      ImageIndex = 1
      OnShow = TabSheet2Show
      object Panel2: TPanel
        Left = 5
        Top = 4
        Width = 668
        Height = 87
        BevelOuter = bvLowered
        TabOrder = 0
        object Label15: TLabel
          Left = 24
          Top = 16
          Width = 77
          Height = 13
          Caption = 'N�mero Se��o :'
        end
        object Label16: TLabel
          Left = 56
          Top = 38
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nome da Se��o :'
        end
        object Label17: TLabel
          Left = 53
          Top = 60
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Assistente Social :'
        end
        object lbCCusto: TLabel
          Left = 111
          Top = 16
          Width = 29
          Height = 13
          Caption = '0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edNomeSecao: TEdit
          Left = 142
          Top = 35
          Width = 392
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
        end
        object edASocialResponsavel: TEdit
          Left = 142
          Top = 57
          Width = 292
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 1
        end
        object mskNumSecao: TMaskEdit
          Left = 142
          Top = 13
          Width = 39
          Height = 21
          CharCase = ecUpperCase
          EditMask = '0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 4
          ParentFont = False
          TabOrder = 2
          Text = '0000'
          OnExit = mskNumSecaoExit
        end
      end
      object Panel3: TPanel
        Left = 4
        Top = 94
        Width = 669
        Height = 263
        TabOrder = 1
        object Label18: TLabel
          Left = 10
          Top = 246
          Width = 84
          Height = 13
          Caption = 'SE��ES ATIVAS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label19: TLabel
          Left = 106
          Top = 246
          Width = 121
          Height = 13
          Caption = 'SE��ES DESATIVADAS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dbgSecoes: TDBGrid
          Left = 8
          Top = 8
          Width = 651
          Height = 234
          DataSource = dsSecoes
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dbgSecoesCellClick
          OnDrawColumnCell = dbgSecoesDrawColumnCell
          OnDblClick = dbgSecoesDblClick
          OnKeyDown = dbgSecoesKeyDown
          OnKeyUp = dbgSecoesKeyUp
          Columns = <
            item
              Expanded = False
              FieldName = 'nom_unidade'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = '[Unidade]'
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'num_secao'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'Se��o'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dsc_nom_secao'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = '[Nome da Se��o]'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_asocial'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = '[Assistente Social]'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'vStatus'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Visible = True
            end>
        end
      end
      object Panel5: TPanel
        Left = 4
        Top = 359
        Width = 672
        Height = 36
        TabOrder = 2
        object btnNovaSecao: TBitBtn
          Left = 359
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnNovaSecaoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvarSecao: TBitBtn
          Left = 426
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnSalvarSecaoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnAlterarSecao: TBitBtn
          Left = 490
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnAlterarSecaoClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FF00004300004300003C000037000036000036FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000930000930002841518892B
            2D8C2A2A830F0F6200004000003A00003AFF00FFFF00FFFF00FFFF00FFFF00FF
            0004B30104A73D45C3B3B7EAFFFFFFFFFFFFFFFFFFFFFFFFA0A2CC2D2D740000
            3A00004EFF00FFFF00FFFF00FF0005CC0107BA7F89E2FFFFFFFFFFFF9B9CDB5E
            5EB75F5FB6ADADDDFFFFFFFFFFFF5E5E9B00003A000043FF00FFFF00FF0005CC
            6472E5FFFFFFD7DAF52528B30102880A0DA206078C00006A31319CE6E6F7FFFF
            FF36367D000043FF00FF0007E80B1BD8F8F8FFF2F3FC1721C30405A41214A303
            048905068B01017409097630309CFFFFFFC3C3DB01015000004B0007E84358F0
            FFFFFF6476ED0104C00203950507910304870E109807088C03036E0909798686
            C9FFFFFF27278800004B0009F37F94FAFFFFFF1932F05867EAFCFCFDEBEDF8EA
            EAF7EAEBF7FFFFFFFFFFFF03047E3233A2FFFFFF5558BE00004A0218FDA6B4FD
            FFFFFF112CFD0C20F37E89F2FFFFFFFFFFFFFFFFFFFFFFFF0B0EBD0308AE5257
            BEFFFFFF6267C30000781735FFA4B6FFFFFFFF2C4AFF000FFF0518FCA3ACF3FF
            FFFFFFFFFF070EC70107C31017BE5E65CAFDFDFF4A4EBD00007F0318FF91A6FF
            FFFFFFA9B9FF000EFF0008FF0E1FFDA4AEF71420D8060ECA070CC3090FB7BABE
            EDFCFCFD1017AA00007FFF00FF5F7AFFFFFFFFFFFFFF5C75FF000BFF000EFF00
            0AFF0618F80007DB0006CC7380E8FFFFFF8F97E2000198FF00FFFF00FF425FFF
            C4CFFFFFFFFFFFFFFF8B9FFF162FFF0414FF0515FD2037F39FADF7FFFFFFE2E5
            FA101ABA000198FF00FFFF00FFFF00FF5975FFD7DFFFFFFFFFFFFFFFFCFCFFD1
            DBFFD4DFFFFFFFFFFFFFFFC6CDF71825CD0001B0FF00FFFF00FFFF00FFFF00FF
            FF00FF5C76FF97A9FDDAE0FFFFFFFFFFFFFFFFFFFFD4DBFD596FF00514D70001
            B0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF5C75FF5C79FF62
            7DFF4A66FD203CF50619E5FF00FFFF00FFFF00FFFF00FFFF00FF}
        end
        object btnCancelarSecao: TBitBtn
          Left = 550
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnCancelarSecaoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnSairSecao: TBitBtn
          Left = 612
          Top = 3
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnSairSecaoClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
        object btnAtivarDesativar: TBitBtn
          Left = 5
          Top = 3
          Width = 151
          Height = 30
          Caption = 'Ativar/Desativar Se��o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = btnAtivarDesativarClick
        end
        object btnImprimir: TBitBtn
          Left = 159
          Top = 3
          Width = 162
          Height = 30
          Caption = 'Ass. Sociais por Se��es'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = btnImprimirClick
        end
      end
    end
  end
  object dsSecoes: TDataSource
    DataSet = dmDeca.cdsSel_Secao
    Left = 635
    Top = 177
  end
  object dsUnidades: TDataSource
    DataSet = dmDeca.cdsSel_Unidade
    Left = 627
    Top = 235
  end
end
