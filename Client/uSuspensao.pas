unit uSuspensao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmSuspensao = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    txtMotivoSuspensao: TMemo;
    txtDataInicio: TMaskEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    Label4: TLabel;
    Label5: TLabel;
    txtDataTermino: TMaskEdit;
    txtDias: TMaskEdit;
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure txtDias1Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtDataInicioExit(Sender: TObject);
    procedure CalculaTermino;
    procedure btnSalvarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure txtDiasExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSuspensao: TfrmSuspensao;

implementation

uses uFichaPesquisa, uDM, uUtil, uPrincipal, rSuspensao;

{$R *.DFM}

procedure TfrmSuspensao.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtDataInicio.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmSuspensao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmSuspensao.Close;
  end;
end;

procedure TfrmSuspensao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSuspensao.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end;
end;

procedure TfrmSuspensao.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmSuspensao.btnSairClick(Sender: TObject);
begin
  mmINTEGRACAO := False;
  frmSuspensao.Close;
end;

procedure TfrmSuspensao.AtualizaCamposBotoes(Modo: String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtDataInicio.Clear;
    txtDias.Clear;
    txtDataTermino.Clear;
    txtMotivoSuspensao.Clear;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtDataInicio.Enabled := False;
    txtDias.Enabled := False;
    txtDataTermino.Enabled := False;
    txtMotivoSuspensao.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtDataInicio.Clear;
    txtDataInicio.Text := DateToStr(Date());
    txtDataTermino.Clear;
    txtDias.Clear;
    txtMotivoSuspensao.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtDataInicio.Enabled := True;
    txtDias.Enabled := True;
    txtMotivoSuspensao.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;

    txtMatricula.SetFocus;
  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;

    btnImprimir.SetFocus;
  end
end;

procedure TfrmSuspensao.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmSuspensao.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmSuspensao.txtDias1Exit(Sender: TObject);
begin
  // calcula a data de t�rmino da suspens�o
  CalculaTermino;
end;

procedure TfrmSuspensao.FormShow(Sender: TObject);
begin
  //AtualizaCamposBotoes('Padrao');
  if (mmINTEGRACAO = False) then
    AtualizaCamposBotoes('Padrao')
  else if (mmINTEGRACAO = True) then
  begin
    AtualizaCamposBotoes('Novo');
    txtMatricula.Text := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
    lbNome.Caption    := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
    txtDataInicio.Text := DateToStr(Date());
    txtDias.SetFocus;
  end;
end;

procedure TfrmSuspensao.txtDataInicioExit(Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(txtDataInicio.Text);
    //Calcula a data de t�rmino da suspens�o
    CalculaTermino;

    if StrToDate(txtDataInicio.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtDataInicio.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtDataInicio.SetFocus;
    end;
  end;



  // calcula a data de t�rmino da suspens�o
  //CalculaTermino;
end;

procedure TfrmSuspensao.CalculaTermino;
begin
  try
    if (Trim(txtDataInicio.Text)<>'//') and (Length(Trim(txtDias.Text)) > 0) then
    begin
      txtDataTermino.Text := DateToStr(StrToDate(txtDataInicio.Text) + StrToInt(Trim(txtDias.Text)));
    end
    else
    begin
      txtDataTermino.Clear;
    end;
  except
    txtDataTermino.Clear;
  end;

end;

procedure TfrmSuspensao.btnSalvarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      if (StatusCadastro(txtMatricula.Text) = 'Ativo') and (Length(txtDataInicio.Text)>0) then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtDataInicio, vtDate);
          Params.ParamByName('@pe_dat_final_suspensao').Value := GetValue(txtDataTermino, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 4; //C�digo referente a suspens�o
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Suspens�o';
          Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtMotivoSuspensao.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "3" - Suspens�o
          with dmDeca.cdsAlt_Cadastro_Status do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_ind_status').Value := 3;
              Execute;
          end;

        end;
        AtualizaCamposBotoes('Salvar');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text) + 'ou data de In�cio da Suspens�o INV�LIDA!!!');
        AtualizaCamposBotoes('Padrao');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'SUS_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                         Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtDataInicio.Text);       //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Suspens�o');              //Descricao do Tipo de Historico
      Writeln (log_file, ' ');
      Writeln (log_file, Trim(txtMotivoSuspensao.Text));
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - SUSPENS�O',
                             MB_OK + MB_ICONERROR);

      //ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10 +#13+#10 +
      //            'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
                'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmSuspensao.btnImprimirClick(Sender: TObject);
var vvMes : Integer;
begin
  try
    Application.CreateForm(TrelSuspensao, relSuspensao);

    relSuspensao.lblDivisao.Caption := Divisao(txtMatricula.Text);
    relSuspensao.lblUnidade.Caption := vvNOM_UNIDADE;
    relSuspensao.lblNome.Caption := lbNome.Caption;
    relSuspensao.lblDias.Caption := Trim(txtDias.Text);
    relSuspensao.lblMotivo.Lines.Add(txtMotivoSuspensao.Text);

    relSuspensao.lblDia.Caption := Copy(txtDataInicio.Text,1,2);
    relSuspensao.lblMes.Caption := cMeses[StrToInt(Copy(txtDataInicio.Text,4,2))];
    relSuspensao.lblAno.Caption := Copy(txtDataInicio.Text,7,4);

    relSuspensao.Preview;
    relSuspensao.Free;
  except end;
end;

procedure TfrmSuspensao.txtDiasExit(Sender: TObject);
begin

  if (Length(txtDias.Text)=0) or (txtDias.Text = '  ') or (Trim(txtDias.Text) = '0')then
  begin
    ShowMessage ('A quantidade de dias deve ser informada, para efeito do c�lculo da data de retorno...');
    txtDias.SetFocus
  end
  else
    CalculaTermino;
    
end;

end.
