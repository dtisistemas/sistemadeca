object frmSelecionaFrequencia: TfrmSelecionaFrequencia
  Left = 294
  Top = 200
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Sistema Deca - [Emiss�o Frequ�ncia]'
  ClientHeight = 138
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object Panel1: TPanel
    Left = 2
    Top = 2
    Width = 425
    Height = 135
    BevelInner = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 47
      Top = 24
      Width = 106
      Height = 15
      Caption = 'Selecione a Turma'
    end
    object Label2: TLabel
      Left = 62
      Top = 57
      Width = 91
      Height = 15
      Caption = 'Selecione o Ano'
    end
    object Label3: TLabel
      Left = 60
      Top = 89
      Width = 93
      Height = 15
      Caption = 'Selecione o M�s'
    end
    object btnVer: TSpeedButton
      Left = 281
      Top = 62
      Width = 88
      Height = 35
      Caption = '&Ver'
      Enabled = False
      Flat = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000D000000000000DD00D000000D0FF
        FFFFFFFF0D000D000000D0FFFFFFF0000800DD000000D0FFFFFF0877808DDD00
        0000D0FFFFF0877E880DDD000000D0FFFFF07777870DDD000000D0FFFFF07E77
        870DDD000000D0FFFFF08EE7880DDD000000D0FFFFFF087780DDDD000000D0FF
        FFFFF0000DDDDD000000D0FFFFFFFFFF0DDDDD000000D0FFFFFFF0000DDDDD00
        0000D0FFFFFFF070DDDDDD000000D0FFFFFFF00DDDDDDD000000DD00000000DD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
      OnClick = btnVerClick
    end
    object cbTurma: TComboBox
      Left = 157
      Top = 21
      Width = 215
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnClick = cbTurmaClick
    end
    object cbAno: TComboBox
      Left = 157
      Top = 53
      Width = 110
      Height = 21
      Style = csOwnerDrawFixed
      ItemHeight = 15
      TabOrder = 1
      OnClick = cbAnoClick
    end
    object cbMes: TComboBox
      Left = 157
      Top = 85
      Width = 110
      Height = 21
      Style = csOwnerDrawFixed
      ItemHeight = 15
      TabOrder = 2
    end
  end
end
