object frmSelecionaUnidade: TfrmSelecionaUnidade
  Left = 282
  Top = 221
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [...Por Unidade]'
  ClientHeight = 150
  ClientWidth = 318
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 3
    Top = 0
    Width = 313
    Height = 146
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 97
      Height = 13
      Caption = 'Selecione a unidade'
    end
    object cbUnidades: TComboBox
      Left = 16
      Top = 42
      Width = 280
      Height = 22
      Hint = 
        'Selecione UNIDADE para emiss�o da Rela��o de crian�as/adolescent' +
        'es'
      Style = csOwnerDrawFixed
      ItemHeight = 16
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = cbUnidadesClick
      OnExit = cbUnidadesExit
    end
    object btnVer: TBitBtn
      Left = 72
      Top = 88
      Width = 161
      Height = 25
      Hint = 
        'Clique para EXIBIR a Rela��o de crian�as/adolescentes da Unidade' +
        ' selecionada'
      Caption = 'Ver Rela��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnVerClick
    end
  end
end
