object frmRegistroAcompanhamentoPsicopedagogicoPsicologico: TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico
  Left = 194
  Top = 217
  BorderStyle = bsDialog
  Caption = 
    'Sistema DECA - [Registro de Acompanhamento Psicopedag�gico/Psico' +
    'l�gico]'
  ClientHeight = 548
  ClientWidth = 869
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 3
    Top = 3
    Width = 862
    Height = 541
    ActivePage = TabSheet1
    TabOrder = 0
    OnChange = PageControl1Change
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = 'Dados do Acompanhamento Psicopedag�gico/Psicol�gico'
      object GroupBox1: TGroupBox
        Left = 8
        Top = 4
        Width = 839
        Height = 49
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 20
          Width = 59
          Height = 13
          Caption = 'Matr�cula:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNome: TLabel
          Left = 216
          Top = 19
          Width = 262
          Height = 18
          Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnPesquisar: TSpeedButton
          Left = 169
          Top = 14
          Width = 28
          Height = 24
          Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777770000777777777777777700770000777777777777777C440700007777
            7777777777C4C40700007777777777777C4C4077000077777777777784C40777
            0000777777788888F740777700007777770000007807777700007777707EFEE6
            007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
            8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
            0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
            7777777700A077777777777777777777FFA077777777777777777777FFFF7777
            7777777777777777FF81}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnPesquisarClick
        end
        object txtMatricula: TMaskEdit
          Left = 82
          Top = 16
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '!99999999;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '        '
        end
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 54
        Width = 840
        Height = 149
        TabOrder = 1
        object Label4: TLabel
          Left = 8
          Top = 23
          Width = 59
          Height = 13
          Caption = 'Nascimento:'
        end
        object Label5: TLabel
          Left = 168
          Top = 23
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label6: TLabel
          Left = 608
          Top = 23
          Width = 36
          Height = 13
          Caption = 'Oficina:'
        end
        object Label7: TLabel
          Left = 8
          Top = 55
          Width = 90
          Height = 13
          Caption = 'Instrutor/Professor:'
        end
        object Label8: TLabel
          Left = 414
          Top = 55
          Width = 83
          Height = 13
          Caption = 'Assistente Social:'
        end
        object Label9: TLabel
          Left = 8
          Top = 87
          Width = 35
          Height = 13
          Caption = 'Escola:'
        end
        object Label10: TLabel
          Left = 328
          Top = 87
          Width = 27
          Height = 13
          Caption = 'S�rie:'
        end
        object Label11: TLabel
          Left = 720
          Top = 87
          Width = 41
          Height = 13
          Caption = 'Per�odo:'
        end
        object Label12: TLabel
          Left = 8
          Top = 119
          Width = 112
          Height = 13
          Caption = 'Professor Respons�vel:'
        end
        object lbNascimento: TLabel
          Left = 74
          Top = 23
          Width = 6
          Height = 13
          Caption = '*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbUnidade: TLabel
          Left = 219
          Top = 23
          Width = 6
          Height = 13
          Caption = '*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbEscola: TLabel
          Left = 50
          Top = 87
          Width = 6
          Height = 13
          Caption = '*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbSerie: TLabel
          Left = 362
          Top = 87
          Width = 6
          Height = 13
          Caption = '*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbPeriodo: TLabel
          Left = 772
          Top = 87
          Width = 6
          Height = 13
          Caption = '*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 408
          Top = 119
          Width = 121
          Height = 13
          Caption = 'Profissional Respons�vel:'
        end
        object txtOficina: TEdit
          Left = 648
          Top = 19
          Width = 175
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
        end
        object txtInstrutor: TEdit
          Left = 102
          Top = 51
          Width = 304
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 1
        end
        object txtAsocial: TEdit
          Left = 503
          Top = 52
          Width = 320
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 2
        end
        object txtProfessor: TEdit
          Left = 127
          Top = 115
          Width = 271
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 3
        end
        object txtProfissional: TEdit
          Left = 533
          Top = 115
          Width = 288
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 4
        end
      end
      object GroupBox3: TGroupBox
        Left = 8
        Top = 205
        Width = 841
        Height = 248
        TabOrder = 2
        object dbgAcompanhamentos: TDBGrid
          Left = 12
          Top = 16
          Width = 819
          Height = 223
          DataSource = dsAcompanhamentos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dbgAcompanhamentosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'cod_matricula'
              Title.Caption = 'Matricula'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_nome'
              Title.Caption = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dsc_oficina'
              Title.Caption = 'Oficina'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_usuario'
              Title.Caption = 'Usu�rio'
              Visible = True
            end>
        end
      end
      object Panel1: TPanel
        Left = 3
        Top = 459
        Width = 848
        Height = 49
        TabOrder = 3
        object Label13: TLabel
          Left = 9
          Top = 17
          Width = 49
          Height = 13
          Caption = 'Pesquisar:'
        end
        object btnCancelar: TBitBtn
          Left = 711
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para CANCELAR opera��o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnCancelarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnSair: TBitBtn
          Left = 780
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para SAIR da tela de advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnSairClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
        object btnNovo: TBitBtn
          Left = 521
          Top = 10
          Width = 54
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnNovoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvar: TBitBtn
          Left = 580
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para GRAVAR a advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnSalvarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnAlterar: TBitBtn
          Left = 639
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para GRAVAR a advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
        object cbPesquisa: TComboBox
          Left = 62
          Top = 14
          Width = 84
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 5
          Items.Strings = (
            'Matr�cula'
            'Nome'
            '<Todos>')
        end
        object edValor: TEdit
          Left = 149
          Top = 14
          Width = 96
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 6
          OnKeyPress = edValorKeyPress
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Registro do Acompanhamento'
      ImageIndex = 1
      object Panel7: TPanel
        Left = 5
        Top = 11
        Width = 841
        Height = 33
        TabOrder = 0
        object Label3: TLabel
          Left = 16
          Top = 6
          Width = 463
          Height = 22
          Caption = 'MATR�CULA  -  NOME DA CRIAN�A/ADOLESCENTE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 4
        Top = 49
        Width = 842
        Height = 48
        Caption = 'Data Registro'
        TabOrder = 1
        object meData: TMaskEdit
          Left = 12
          Top = 16
          Width = 87
          Height = 21
          EditMask = 'cc/cc/cccc'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
      end
      object GroupBox5: TGroupBox
        Left = 3
        Top = 99
        Width = 843
        Height = 139
        Caption = 'Descri��o do Acompanhamento realizado'
        TabOrder = 2
        object meRegistro: TMemo
          Left = 11
          Top = 17
          Width = 823
          Height = 110
          TabOrder = 0
        end
      end
      object GroupBox6: TGroupBox
        Left = 3
        Top = 243
        Width = 843
        Height = 137
        Caption = 'Hist�rico dos Acompanhamentos'
        TabOrder = 3
        object dbgRegistrosAcompanhamento: TDBGrid
          Left = 13
          Top = 19
          Width = 818
          Height = 104
          DataSource = dsRegistros
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dbgRegistrosAcompanhamentoDblClick
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'dat_registro'
              Title.Alignment = taCenter
              Title.Caption = 'Data Registro'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'log_data'
              Title.Caption = 'Inclus�o'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'log_data_altera'
              Title.Caption = '�ltima Altera��o em...'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_usuario'
              Title.Caption = 'Usu�rio'
              Width = 250
              Visible = True
            end>
        end
      end
      object Panel2: TPanel
        Left = 4
        Top = 459
        Width = 847
        Height = 49
        TabOrder = 4
        object btnNovoH: TBitBtn
          Left = 520
          Top = 10
          Width = 54
          Height = 30
          Hint = 'Clique para NOVA advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnNovoHClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvarH: TBitBtn
          Left = 579
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para GRAVAR a advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnSalvarHClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnCancelarH: TBitBtn
          Left = 710
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para CANCELAR opera��o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnCancelarHClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnVoltarH: TBitBtn
          Left = 779
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para SAIR da tela de advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnVoltarHClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
        object btnAlterarH: TBitBtn
          Left = 638
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para GRAVAR a advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnAlterarHClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
      end
    end
  end
  object dsAcompanhamentos: TDataSource
    DataSet = dmDeca.cdsSel_Psicopedagogico
    Left = 807
    Top = 256
  end
  object dsRegistros: TDataSource
    DataSet = dmDeca.cdsSel_ItensPsicopedagogico
    Left = 807
    Top = 287
  end
end
