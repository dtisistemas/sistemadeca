unit uEmiteRegAtendimento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ExtCtrls;

type
  TfrmEmiteRegAtendimento = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Panel1: TPanel;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txtDataInicial: TMaskEdit;
    txtDataFinal: TMaskEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnSairClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure txtDataInicialExit(Sender: TObject);
    procedure txtDataFinalExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteRegAtendimento: TfrmEmiteRegAtendimento;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil, rRegistroAtendimento,
  rNovoRegistroAtendimento;

{$R *.DFM}

procedure TfrmEmiteRegAtendimento.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin

    txtMatricula.Clear;
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;

    txtDataInicial.Clear;
    txtDataInicial.Enabled := False;
    txtDataFinal.Clear;
    txtDataFinal.Enabled := False;

    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Emissao' then
  begin

    txtDataInicial.Enabled := True;
    txtDataFinal.Enabled := True;

    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := True;

  end;

end;

procedure TfrmEmiteRegAtendimento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEmiteRegAtendimento.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmEmiteRegAtendimento.btnSairClick(Sender: TObject);
begin
  frmEmiteRegAtendimento.Close;
end;

procedure TfrmEmiteRegAtendimento.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        AtualizaCamposBotoes('Emissao');
        txtDataInicial.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmEmiteRegAtendimento.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;
            AtualizaCamposBotoes('Emissao');
            txtDataInicial.SetFocus;
          end;
      end;
    except end;
  end;
end;

procedure TfrmEmiteRegAtendimento.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmEmiteRegAtendimento.Close;
  end;
end;

procedure TfrmEmiteRegAtendimento.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmEmiteRegAtendimento.btnSalvarClick(Sender: TObject);
begin
  with dmDeca.cdsSel_Cadastro_Historico_Periodo do
  begin
    Close;
    Params.ParamByName('@pe_cod_matricula').AsString := GetValue(txtMatricula.Text);
    //Params.ParamByName('@pe_dat_inicial').Value := GetValue(txtDataInicial, vtDate);
    //Params.ParamByName('@pe_dat_final').Value := GetValue(txtDataFinal, vtDate);

    Params.ParamByName('@pe_dat_inicial').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_final').AsDateTime := StrToDate(txtDataFinal.Text);

    Open;

    Application.CreateForm (TrelNovoRegistroAtendimento, relNovoRegistroAtendimento);
    relNovoRegistroAtendimento.lbIdentificacao.Caption := txtMatricula.Text +  ' - ' + lbNome.Caption;
    relNovoRegistroAtendimento.lbPeriodo.Caption := 'De ' + txtDataInicial.Text + ' at� ' + txtDataFinal.Text;
    relNovoRegistroAtendimento.Preview;
    relNovoRegistroAtendimento.Free;

    AtualizaCamposBotoes('Padrao');
  end;
end;

procedure TfrmEmiteRegAtendimento.txtDataInicialExit(Sender: TObject);
begin
  try
    StrToDate(txtDataInicial.Text);
  except
    begin
      ShowMessage('Data inv�lida...');
      txtDataInicial.Clear;
      txtDataInicial.SetFocus;
    end;
  end;
end;

procedure TfrmEmiteRegAtendimento.txtDataFinalExit(Sender: TObject);
begin
try
    StrToDate(txtDataFinal.Text);

    if StrToDate(txtDataFinal.Text) < StrToDate(txtDataInicial.Text) then
        begin
          ShowMessage('Data final do periodo � menor do que a data inicial.' + #13#10#13 +
                      'Favor informe data v�lida para realizar a consulta...');
          txtDataFinal.SetFocus;
        end
    else
      begin
        btnSalvar.Enabled := True;
        btnSalvar.SetFocus;
      end;
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataFinal.Clear;
      txtDataFinal.SetFocus;
    end;
  end;
end;

end.
