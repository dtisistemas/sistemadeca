unit uGeraEstatisticaMatriz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, Mask, Grids, DBGrids, jpeg, Db, ComObj,
  OleServer, Excel97;

type
  TfrmGeraEstatisticaMatriz = class(TForm)
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    mskAno: TMaskEdit;
    cbSemestre: TComboBox;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    btnGerarEstatisticas: TSpeedButton;
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    chkFundhas: TCheckBox;
    DataSource1: TDataSource;
    Memo1: TMemo;
    btnExportar: TSpeedButton;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    procedure chkFundhasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RadioGroup1Click(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure btnGerarEstatisticasClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraEstatisticaMatriz: TfrmGeraEstatisticaMatriz;
  vUnidade1, vUnidade2 : TStringList;
  mes : Integer;
  objExcel, Sheet : Variant;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmGeraEstatisticaMatriz.chkFundhasClick(Sender: TObject);
begin
  if chkFundhas.Checked then
  begin
    cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := False
  end
  else
    cbUnidade.Enabled := True;
end;

procedure TfrmGeraEstatisticaMatriz.FormShow(Sender: TObject);
begin

  vUnidade1 := TStringList.Create();
  vUnidade2 := TStringList.Create();

  //Carregar a lista de unidades...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
    end;

    while not dmDeca.cdsSel_Unidade.eof do
    begin

      if (dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').AsInteger in [1,3,7,9,12,13,40,42,43,47,48,50,71]) then
        dmDeca.cdsSel_Unidade.Next
      else
      begin
        cbUnidade.Items.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
        vUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value));
        vUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end
    end;
  except end;

  GroupBox1.Visible := True;
  mskAno.Visible := True;
  mskAno.Text := Copy(DateToStr(Date()),7,4);
  cbSemestre.Visible := False;
  cbSemestre.ItemIndex := 0;
  
end;

procedure TfrmGeraEstatisticaMatriz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vUnidade1.Free;
  vUnidade2.Free;
  //objExcel.Close;
end;

procedure TfrmGeraEstatisticaMatriz.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex = 0 then
  begin
    //Pesquisar por semestre
    GroupBox1.Visible := True;
    mskAno.Visible := True;
    mskAno.Text := Copy(DateToStr(Date()),7,4);
    cbSemestre.ItemIndex := -1;
    cbSemestre.Visible := True;
  end
  else
  begin
    cbSemestre.Visible := False;
    GroupBox1.Visible := True;
    mskAno.Visible := True;
  end;
end;

procedure TfrmGeraEstatisticaMatriz.btnExportarClick(Sender: TObject);
var
  Planilha : Olevariant;
  lin, col : integer;
  nomARQ : String;
begin

  {
  if (RadioGroup1.ItemIndex = 0) then //and (chkFundhas.Checked = False) then
  begin

    ExcelApplication1 := TExcelApplication.Create(nil);
    ExcelApplication1.Workbooks.Add(EmptyParam, 0);
    ExcelApplication1.Visible[0] := True;

    Planilha := ExcelApplication1.Workbooks[1].Worksheets[1];

    mes := 0;

    try

      with dmDeca.cdsGeraDadosMAtrizSemestreAno do
      begin

        if cbSemestre.ItemIndex = 0 then
        begin

          //Montando o cabe�alho na m�o...
          Planilha.Cells[1,1].Font.Bold := True;
          Planilha.Cells[1,1].Value := 'FUNDHAS - Funda��o H�lio Augusto de Souza  [Estat�sticas de Dados da Matriz]';
          Planilha.Cells[2,1].Value := 'Per�odo: ' + cbSemestre.Text + '/' + mskAno.Text;
          if chkFundhas.Checked then
            Planilha.Cells[3,1].Value := 'Unidade: FUNDA��O H�LIO AUGUSTO DE SOUZA'
          else
            Planilha.Cells[3,1].Value := 'Unidade: ' + cbUnidade.Text;

          Planilha.Cells[4,1].Value := 'M�s';

          Planilha.Cells[4,2].Value := 'Total Unidade';
          Planilha.Cells[4,3].Value := 'Total Matriculados';
          Planilha.Cells[4,4].Value := '% Matriculados';

          Planilha.Cells[4,5].Value := 'Idade x Serie Adequadas';
          Planilha.Cells[4,6].Value := '% S�rie Adeq.';

          Planilha.Cells[4,7].Value := 'Freq. Escolar Dentro esperado';
          Planilha.Cells[4,8].Value := '% Freq. Dentro Esperado';

          Planilha.Cells[4,9].Value := 'Aprov. Escolar Dentro esperado';
          Planilha.Cells[4,10].Value := '% Aprov. Dentro Esperado';

          Planilha.Cells[4,11].Value := 'Total Presen�as Esperadas';
          Planilha.Cells[4,12].Value := 'Total de Faltas';
          Planilha.Cells[4,13].Value := '% Freq. Atividades/Cursos';

          Planilha.Cells[4,14].Value := 'Justificadas';
          Planilha.Cells[4,15].Value := 'Injustificadas';
          Planilha.Cells[4,16].Value := '% Faltas Justificadas';

          Planilha.Cells[4,17].Value := 'Total de Desligados';
          Planilha.Cells[4,18].Value := '% Desligados';

          Planilha.Cells[4,19].Value := 'Total Pedido';
          Planilha.Cells[4,20].Value := '% Pedido';
          Planilha.Cells[4,21].Value := 'Total Abandono';
          Planilha.Cells[4,22].Value := '% Abandono';
          Planilha.Cells[4,23].Value := 'Total Maioridade';
          Planilha.Cells[4,24].Value := '% Maioridade';
          Planilha.Cells[4,25].Value := 'Total Lei';
          Planilha.Cells[4,26].Value := '% Lei';
          Planilha.Cells[4,27].Value := 'Total Outros';
          Planilha.Cells[4,28].Value := '% Outros';
          Planilha.Cells[4,29].Value := 'Aprend. Dentro Esperado';
          Planilha.Cells[4,30].Value := '% Aprend. Dentro Esperado';
          Planilha.Cells[4,31].Value := 'N.� Ativ. Culturais';
          Planilha.Cells[4,32].Value := 'N.� Ativ. Esportivas';
          Planilha.Cells[4,33].Value := 'N.� A��es Sociais';
          Planilha.Cells[4,34].Value := 'Grau Aprov. Programa';

          Planilha.Cells[4,35].Value := 'Total Atend. Ind.';
          Planilha.Cells[4,36].Value := 'Situa��o Disciplinar';
          Planilha.Cells[4,37].Value := '% Sit. Disc.';
          Planilha.Cells[4,38].Value := 'Situa��o Socioecon�mica';
          Planilha.Cells[4,39].Value := '%Sit. SocioEc.';
          Planilha.Cells[4,40].Value := 'Situa��o Escolar';
          Planilha.Cells[4,41].Value := '% Sit. Escolar';
          Planilha.Cells[4,42].Value := 'Viol�ncia';
          Planilha.Cells[4,43].Value := '% Sit. Viol�ncia';
          Planilha.Cells[4,44].Value := 'Abrigo';
          Planilha.Cells[4,45].Value := '% Sit. Abrigo';
          Planilha.Cells[4,46].Value := 'Intera��o Relacional';
          Planilha.Cells[4,47].Value := '% Sit. Int. Relac.';
          Planilha.Cells[4,48].Value := 'Ato Infracional';
          Planilha.Cells[4,49].Value := '% Sit. Ato Infr.';
          Planilha.Cells[4,50].Value := 'Aprendizagem';
          Planilha.Cells[4,51].Value := '% Sit. Aprend.';
          Planilha.Cells[4,52].Value := 'Sa�de';
          Planilha.Cells[4,53].Value := '% Sit. Sa�de';
          Planilha.Cells[4,54].Value := 'Guarda Irregular';
          Planilha.Cells[4,55].Value := '% Sit. Gurda Irr.';
          Planilha.Cells[4,56].Value := 'Gravidez';
          Planilha.Cells[4,57].Value := '% Sit. Gravidez';
          Planilha.Cells[4,58].Value := 'Moradia';
          Planilha.Cells[4,59].Value := '% Sit. Moradia';
          Planilha.Cells[4,60].Value := 'Trab. Infantil';
          Planilha.Cells[4,61].Value := '% Sit. Trab Inf.';
          Planilha.Cells[4,62].Value := 'Grau Satisf. Crian�as';
          Planilha.Cells[4,63].Value := 'Pesq. Respondidas';
          Planilha.Cells[4,64].Value := '% Pesq. Resp.';
          Planilha.Cells[4,65].Value := 'Evol. Peso x Altura';
          Planilha.Cells[4,66].Value := 'N.� A��es com Fam�lias';
          Planilha.Cells[4,67].Value := 'Total Fam�lias';
          Planilha.Cells[4,68].Value := 'Fam�lias Partic.';
          Planilha.Cells[4,69].Value := '% Fam�lias Partic.';
          Planilha.Cells[4,70].Value := 'Grau Satisf. Fam�lias';
          Planilha.Cells[4,71].Value := 'N.� Pesq. Resp. pelas Fam�lias';
          Planilha.Cells[4,72].Value := '% Pesq. Resp. Familias';
          Planilha.Cells[4,73].Value := 'Fora Ensino Regular';
          Planilha.Cells[4,74].Value := '% Fora Ens. Reg.';
          Planilha.Cells[4,75].Value := 'Total Orient. Prof.';
          Planilha.Cells[4,76].Value := 'Concluintes Orient. Prof.';
          Planilha.Cells[4,77].Value := '% Concluintes OP';
          Planilha.Cells[4,78].Value := 'Total Orient. Educ.';
          Planilha.Cells[4,79].Value := 'Concluintes Orient. Educ.';
          Planilha.Cells[4,80].Value := '% Concluintes OE';

          Planilha.Cells[4,81].Value := 'Total Mod. I';
          Planilha.Cells[4,82].Value := 'Concluintes Mod. I';
          Planilha.Cells[4,83].Value := '% Concluintes Mod. I';

          Planilha.Cells[4,84].Value := 'Total Mod. II';
          Planilha.Cells[4,85].Value := 'Concluintes Mod. II';
          Planilha.Cells[4,86].Value := '% Concluintes Mod. II';

          Planilha.Cells[4,87].Value := 'Total Mod. Intensivo';
          Planilha.Cells[4,88].Value := 'Concluintes Mod. Intensivo';
          Planilha.Cells[4,89].Value := '% Concluintes Intensivo';

          Planilha.Cells[4,90].Value := 'Total Inscritos Vest.';
          Planilha.Cells[4,91].Value := 'Total Aprovados';
          Planilha.Cells[4,92].Value := '% Aprova��o';

          Planilha.Cells[4,93].Value := 'Aptos Aprendizagem';
          Planilha.Cells[4,94].Value := '% Aptos Aprend.';

          Planilha.Cells[4,95].Value := 'Inseridos Aprendizagem';
          Planilha.Cells[4,96].Value := '% Inseridos Aprend.';

          Planilha.Cells[4,97].Value := 'Em aprend. FUNDHAS';
          Planilha.Cells[4,98].Value := '% FUNDHAS';

          Planilha.Cells[4,99].Value := 'Em aprend. Externo';
          Planilha.Cells[4,100].Value := '% Externo';

          Planilha.Cells[4,101].Value := 'Grau Satisf. Empresa';
          Planilha.Cells[4,102].Value := '% Empresas que responderam';

          lin := 5; //linha a partir da qual ser�o inseridos os valores

          for mes := 1 to 6 do
          begin
            Close;

            if (chkFundhas.Checked = False) then
              Params.ParamByName ('@pe_cod_unidade').Value      := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex])
            else
              Params.ParamByName ('@pe_cod_unidade').Value      := Null;

            Params.ParamByName ('@pe_num_bimestre').Value       := Null;
            Params.ParamByName ('@pe_num_ano').Value            := StrToInt(mskAno.Text);
            Params.ParamByName ('@pe_num_mes_inicio').Value     := mes;
            Params.ParamByName ('@pe_num_mes_final').Value      := mes;
            Open;

            Planilha.Cells[lin,1].Value := IntToStr(mes);

            //Indicador 01
            //N�mero e percentual de crian�as e adolescentes matriculados na escola
            Planilha.Cells[lin,2].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
            Planilha.Cells[lin,3].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;;
            Planilha.Cells[lin,4].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind01').AsString;

            //Indicador 02
            //N�mero e percentual de crian�as e adolescentes com "idade x s�rie" adequadas
            Planilha.Cells[lin,5].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTIdadeSerieAdequadas').AsString;
            Planilha.Cells[lin,6].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind02').AsString;

            //Indicador 03
            //N�mero e percentual de crian�as e adolescentes com frequ�ncia escolar "dentro do esperado"
            Planilha.Cells[lin,7].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFrequenciaDentroEsperadO').AsString;
            Planilha.Cells[lin,8].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind03').AsString;

            //Indicador 04
            //N�mero e percentual de crian�as e adolescentes com aproveitamento escolar "dentro do esperado"
            Planilha.Cells[lin,9].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAproveitamentoDentroEsperado').AsString;
            Planilha.Cells[lin,10].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind04').AsString;

            //Percentual de frequ�ncia de crian�as e adolescentes nas atividades/cursos
            Planilha.Cells[lin,11].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPresencasEsperadas').AsString;
            Planilha.Cells[lin,12].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTGeralFaltas').AsString;
            Planilha.Cells[lin,13].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind05').AsString;

            //Percentual de Faltas Justificadas
            //Planilha.Cells[lin,14].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltas').AsString;
            Planilha.Cells[lin,14].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasJustificadas').AsString;
            Planilha.Cells[lin,15].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasInJustificadas').AsString;
            Planilha.Cells[lin,16].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind06').AsString;

            //N�mero e percentual de crian�as/adolescentes desligados
            Planilha.Cells[lin,17].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDesligados').AsString;
            Planilha.Cells[lin,18].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind07').AsString;

            //N�mero e percentual dos motivos de desligamento
            //A Pedido
            Planilha.Cells[lin,19].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslPedido').AsString;
            Planilha.Cells[lin,20].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Pedido').AsString;

            //Abandono
            Planilha.Cells[lin,21].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslAbandono').AsString;
            Planilha.Cells[lin,22].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Abandono').AsString;

            //Maioridade
            Planilha.Cells[lin,23].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslMaioridade').AsString;
            Planilha.Cells[lin,24].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Maioridade').AsString;

            //Lei
            Planilha.Cells[lin,25].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslLei').AsString;
            Planilha.Cells[lin,26].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Lei').AsString;

            //Outros
            Planilha.Cells[lin,27].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslOutros').AsString;
            Planilha.Cells[lin,28].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Outros').AsString;

            //N�mero e percentual de crian�as e adolescentes com aprendizagem dentro do esperado
            Planilha.Cells[lin,29].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprendDentroEsp').AsString;
            Planilha.Cells[lin,30].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;

            //N�mero e tipo de atividades culturais desenvolvidas
            Planilha.Cells[lin,31].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind10').AsString;

            //N�mero e tipo de atividades esportivas/recreativas desenvolvidas
            Planilha.Cells[lin,32].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind11').AsString;

            //N�mero e tipo de a��es sociais
            Planilha.Cells[lin,33].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind12').AsString;

            //N�mero e tipo de a��es sociais
            Planilha.Cells[lin,34].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind13').AsString;

            //N�mero e tipo de situa��o de acompanhamento individualizado
            Planilha.Cells[lin,35].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAtendInd').AsString;

            Planilha.Cells[lin,36].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSituacaoDisciplinar').AsString;
            Planilha.Cells[lin,37].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14a').AsString;
            Planilha.Cells[lin,38].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSocEcon').AsString;
            Planilha.Cells[lin,39].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14b').AsString;
            Planilha.Cells[lin,40].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitEscolar').AsString;
            Planilha.Cells[lin,41].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14c').AsString;
            Planilha.Cells[lin,42].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitViolencia').AsString;
            Planilha.Cells[lin,43].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14d').AsString;
            Planilha.Cells[lin,44].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAbrigo').AsString;
            Planilha.Cells[lin,45].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14e').AsString;
            Planilha.Cells[lin,46].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitIntRelac').AsString;
            Planilha.Cells[lin,47].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14f').AsString;
            Planilha.Cells[lin,48].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAtoInfrac').AsString;
            Planilha.Cells[lin,49].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14g').AsString;
            Planilha.Cells[lin,50].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAprendizagem').AsString;
            Planilha.Cells[lin,51].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14h').AsString;
            Planilha.Cells[lin,52].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSaude').AsString;
            Planilha.Cells[lin,53].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14i').AsString;
            Planilha.Cells[lin,54].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGuardaIrreg').AsString;
            Planilha.Cells[lin,55].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14j').AsString;
            Planilha.Cells[lin,56].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGravidez').AsString;
            Planilha.Cells[lin,57].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14l').AsString;
            Planilha.Cells[lin,58].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitMoradia').AsString;
            Planilha.Cells[lin,59].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14m').AsString;
            Planilha.Cells[lin,60].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitTrabInfantil').AsString;
            Planilha.Cells[lin,61].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14n').AsString;
            Planilha.Cells[lin,62].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind15').AsString;
            Planilha.Cells[lin,63].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespCcas').AsString;
            Planilha.Cells[lin,64].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind16').AsString;
            Planilha.Cells[lin,65].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind17').AsString;
            Planilha.Cells[lin,66].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAcoesFamilias').AsString;
            Planilha.Cells[lin,67].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamilias').AsString;
            Planilha.Cells[lin,68].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamiliasPartic').AsString;
            Planilha.Cells[lin,69].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind19').AsString;
            Planilha.Cells[lin,70].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind20').AsString;
            Planilha.Cells[lin,71].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespFam').AsString;
            Planilha.Cells[lin,72].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind21').AsString;
            Planilha.Cells[lin,73].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTForaEnsinoRegular').AsString;
            Planilha.Cells[lin,74].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind22').AsString;
            Planilha.Cells[lin,75].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOP').AsString;
            Planilha.Cells[lin,76].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOP').AsString;
            Planilha.Cells[lin,77].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind23').AsString;
            Planilha.Cells[lin,78].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOE').AsString;
            Planilha.Cells[lin,79].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOe').AsString;
            Planilha.Cells[lin,80].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind24').AsString;

            Planilha.Cells[lin,81].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod1').AsString;
            Planilha.Cells[lin,82].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod1').AsString;
            Planilha.Cells[lin,83].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod1').AsString;

            Planilha.Cells[lin,84].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod2').AsString;
            Planilha.Cells[lin,85].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod2').AsString;
            Planilha.Cells[lin,86].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod2').AsString;

            Planilha.Cells[lin,87].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTModIntensivo').AsString;
            Planilha.Cells[lin,88].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesIntensivo').AsString;
            Planilha.Cells[lin,89].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Intensivo').AsString;

            Planilha.Cells[lin,90].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInscritosVest').AsString;
            Planilha.Cells[lin,91].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprovadosVest').AsString;
            Planilha.Cells[lin,92].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind26').AsString;

            Planilha.Cells[lin,93].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAptosAprendizagem').AsString;
            Planilha.Cells[lin,94].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind27').AsString;

            Planilha.Cells[lin,95].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInseridosAprendizagem').AsString;
            Planilha.Cells[lin,96].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind28').AsString;

            Planilha.Cells[lin,97].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemFUNDHAS').AsString;
            Planilha.Cells[lin,98].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind29').AsString;

            Planilha.Cells[lin,99].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemExterna').AsString;
            Planilha.Cells[lin,100].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind30').AsString;

            Planilha.Cells[lin,101].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('vIndicador31').AsString;
            Planilha.Cells[lin,102].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind32').AsString;
            Planilha.Cells[lin,103].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind33').AsString;

            lin := lin + 1;

          end;

        end
        else if cbSemestre.ItemIndex = 1 then
        begin

          //Montando o cabe�alho na m�o...
          Planilha.Cells[1,1].Font.Bold := True;
          Planilha.Cells[1,1].Value := 'FUNDHAS - Funda��o H�lio Augusto de Souza  [Estat�sticas de Dados da Matriz]';
          Planilha.Cells[2,1].Value := 'Per�odo: ' + cbSemestre.Text + '/' + mskAno.Text;
          if chkFundhas.Checked then
            Planilha.Cells[3,1].Value := 'Unidade: FUNDA��O H�LIO AUGUSTO DE SOUZA'
          else
            Planilha.Cells[3,1].Value := 'Unidade: ' + cbUnidade.Text;

          Planilha.Cells[4,1].Value := 'M�s';

          Planilha.Cells[4,2].Value := 'Total Unidade';
          Planilha.Cells[4,3].Value := 'Total Matriculados';
          Planilha.Cells[4,4].Value := '% Matriculados';

          Planilha.Cells[4,5].Value := 'Idade x Serie Adequadas';
          Planilha.Cells[4,6].Value := '% S�rie Adeq.';

          Planilha.Cells[4,7].Value := 'Freq. Escolar Dentro esperado';
          Planilha.Cells[4,8].Value := '% Freq. Dentro Esperado';

          Planilha.Cells[4,9].Value := 'Aprov. Escolar Dentro esperado';
          Planilha.Cells[4,10].Value := '% Aprov. Dentro Esperado';

          Planilha.Cells[4,11].Value := 'Total Presen�as Esperadas';
          Planilha.Cells[4,12].Value := 'Total de Faltas';
          Planilha.Cells[4,13].Value := '% Freq. Atividades/Cursos';

          Planilha.Cells[4,14].Value := 'Justificadas';
          Planilha.Cells[4,15].Value := 'Injustificadas';
          Planilha.Cells[4,16].Value := '% Faltas Justificadas';

          Planilha.Cells[4,17].Value := 'Total de Desligados';
          Planilha.Cells[4,18].Value := '% Desligados';

          Planilha.Cells[4,19].Value := 'Total Pedido';
          Planilha.Cells[4,20].Value := '% Pedido';
          Planilha.Cells[4,21].Value := 'Total Abandono';
          Planilha.Cells[4,22].Value := '% Abandono';
          Planilha.Cells[4,23].Value := 'Total Maioridade';
          Planilha.Cells[4,24].Value := '% Maioridade';
          Planilha.Cells[4,25].Value := 'Total Lei';
          Planilha.Cells[4,26].Value := '% Lei';
          Planilha.Cells[4,27].Value := 'Total Outros';
          Planilha.Cells[4,28].Value := '% Outros';
          Planilha.Cells[4,29].Value := 'Aprend. Dentro Esperado';
          Planilha.Cells[4,30].Value := '% Aprend. Dentro Esperado';
          Planilha.Cells[4,31].Value := 'N.� Ativ. Culturais';
          Planilha.Cells[4,32].Value := 'N.� Ativ. Esportivas';
          Planilha.Cells[4,33].Value := 'N.� A��es Sociais';
          Planilha.Cells[4,34].Value := 'Grau Aprov. Programa';

          Planilha.Cells[4,35].Value := 'Total Atend. Ind.';
          Planilha.Cells[4,36].Value := 'Situa��o Disciplinar';
          Planilha.Cells[4,37].Value := '% Sit. Disc.';
          Planilha.Cells[4,38].Value := 'Situa��o Socioecon�mica';
          Planilha.Cells[4,39].Value := '%Sit. SocioEc.';
          Planilha.Cells[4,40].Value := 'Situa��o Escolar';
          Planilha.Cells[4,41].Value := '% Sit. Escolar';
          Planilha.Cells[4,42].Value := 'Viol�ncia';
          Planilha.Cells[4,43].Value := '% Sit. Viol�ncia';
          Planilha.Cells[4,44].Value := 'Abrigo';
          Planilha.Cells[4,45].Value := '% Sit. Abrigo';
          Planilha.Cells[4,46].Value := 'Intera��o Relacional';
          Planilha.Cells[4,47].Value := '% Sit. Int. Relac.';
          Planilha.Cells[4,48].Value := 'Ato Infracional';
          Planilha.Cells[4,49].Value := '% Sit. Ato Infr.';
          Planilha.Cells[4,50].Value := 'Aprendizagem';
          Planilha.Cells[4,51].Value := '% Sit. Aprend.';
          Planilha.Cells[4,52].Value := 'Sa�de';
          Planilha.Cells[4,53].Value := '% Sit. Sa�de';
          Planilha.Cells[4,54].Value := 'Guarda Irregular';
          Planilha.Cells[4,55].Value := '% Sit. Gurda Irr.';
          Planilha.Cells[4,56].Value := 'Gravidez';
          Planilha.Cells[4,57].Value := '% Sit. Gravidez';
          Planilha.Cells[4,58].Value := 'Moradia';
          Planilha.Cells[4,59].Value := '% Sit. Moradia';
          Planilha.Cells[4,60].Value := 'Trab. Infantil';
          Planilha.Cells[4,61].Value := '% Sit. Trab Inf.';
          Planilha.Cells[4,62].Value := 'Grau Satisf. Crian�as';
          Planilha.Cells[4,63].Value := 'Pesq. Respondidas';
          Planilha.Cells[4,64].Value := '% Pesq. Resp.';
          Planilha.Cells[4,65].Value := 'Evol. Peso x Altura';
          Planilha.Cells[4,66].Value := 'N.� A��es com Fam�lias';
          Planilha.Cells[4,67].Value := 'Total Fam�lias';
          Planilha.Cells[4,68].Value := 'Fam�lias Partic.';
          Planilha.Cells[4,69].Value := '% Fam�lias Partic.';
          Planilha.Cells[4,70].Value := 'Grau Satisf. Fam�lias';
          Planilha.Cells[4,71].Value := 'N.� Pesq. Resp. pelas Fam�lias';
          Planilha.Cells[4,72].Value := '% Pesq. Resp. Familias';
          Planilha.Cells[4,73].Value := 'Fora Ensino Regular';
          Planilha.Cells[4,74].Value := '% Fora Ens. Reg.';
          Planilha.Cells[4,75].Value := 'Total Orient. Prof.';
          Planilha.Cells[4,76].Value := 'Concluintes Orient. Prof.';
          Planilha.Cells[4,77].Value := '% Concluintes OP';
          Planilha.Cells[4,78].Value := 'Total Orient. Educ.';
          Planilha.Cells[4,79].Value := 'Concluintes Orient. Educ.';
          Planilha.Cells[4,80].Value := '% Concluintes OE';

          Planilha.Cells[4,81].Value := 'Total Mod. I';
          Planilha.Cells[4,82].Value := 'Concluintes Mod. I';
          Planilha.Cells[4,83].Value := '% Concluintes Mod. I';

          Planilha.Cells[4,84].Value := 'Total Mod. II';
          Planilha.Cells[4,85].Value := 'Concluintes Mod. II';
          Planilha.Cells[4,86].Value := '% Concluintes Mod. II';

          Planilha.Cells[4,87].Value := 'Total Mod. Intensivo';
          Planilha.Cells[4,88].Value := 'Concluintes Mod. Intensivo';
          Planilha.Cells[4,89].Value := '% Concluintes Intensivo';

          Planilha.Cells[4,90].Value := 'Total Inscritos Vest.';
          Planilha.Cells[4,91].Value := 'Total Aprovados';
          Planilha.Cells[4,92].Value := '% Aprova��o';

          Planilha.Cells[4,93].Value := 'Aptos Aprendizagem';
          Planilha.Cells[4,94].Value := '% Aptos Aprend.';

          Planilha.Cells[4,95].Value := 'Inseridos Aprendizagem';
          Planilha.Cells[4,96].Value := '% Inseridos Aprend.';

          Planilha.Cells[4,97].Value := 'Em aprend. FUNDHAS';
          Planilha.Cells[4,98].Value := '% FUNDHAS';

          Planilha.Cells[4,99].Value := 'Em aprend. Externo';
          Planilha.Cells[4,100].Value := '% Externo';

          Planilha.Cells[4,101].Value := 'Grau Satisf. Empresa';
          Planilha.Cells[4,102].Value := '% Empresas que responderam';

          lin := 5; //linha a partir da qual ser�o inseridos os valores

          for mes := 7 to 12 do
          begin
            Close;

            if (chkFundhas.Checked = False) then
              Params.ParamByName ('@pe_cod_unidade').Value      := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex])
            else
              Params.ParamByName ('@pe_cod_unidade').Value      := Null;

            Params.ParamByName ('@pe_num_bimestre').Value       := Null;
            Params.ParamByName ('@pe_num_ano').Value            := StrToInt(mskAno.Text);
            Params.ParamByName ('@pe_num_mes_inicio').Value     := mes;
            Params.ParamByName ('@pe_num_mes_final').Value      := mes;
            Open;

            Planilha.Cells[lin,1].Value := IntToStr(mes);

            //Indicador 01
            //N�mero e percentual de crian�as e adolescentes matriculados na escola
            Planilha.Cells[lin,2].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
            Planilha.Cells[lin,3].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;;
            Planilha.Cells[lin,4].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind01').AsString;

            //Indicador 02
            //N�mero e percentual de crian�as e adolescentes com "idade x s�rie" adequadas
            Planilha.Cells[lin,5].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTIdadeSerieAdequadas').AsString;
            Planilha.Cells[lin,6].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind02').AsString;

            //Indicador 03
            //N�mero e percentual de crian�as e adolescentes com frequ�ncia escolar "dentro do esperado"
            Planilha.Cells[lin,7].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFrequenciaDentroEsperadO').AsString;
            Planilha.Cells[lin,8].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind03').AsString;

            //Indicador 04
            //N�mero e percentual de crian�as e adolescentes com aproveitamento escolar "dentro do esperado"
            Planilha.Cells[lin,9].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAproveitamentoDentroEsperado').AsString;
            Planilha.Cells[lin,10].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind04').AsString;

            //Percentual de frequ�ncia de crian�as e adolescentes nas atividades/cursos
            Planilha.Cells[lin,11].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPresencasEsperadas').AsString;
            Planilha.Cells[lin,12].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTGeralFaltas').AsString;
            Planilha.Cells[lin,13].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind05').AsString;

            //Percentual de Faltas Justificadas
            //Planilha.Cells[lin,14].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltas').AsString;
            Planilha.Cells[lin,14].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasJustificadas').AsString;
            Planilha.Cells[lin,15].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasInJustificadas').AsString;
            Planilha.Cells[lin,16].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind06').AsString;

            //N�mero e percentual de crian�as/adolescentes desligados
            Planilha.Cells[lin,17].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDesligados').AsString;
            Planilha.Cells[lin,18].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind07').AsString;

            //N�mero e percentual dos motivos de desligamento
            //A Pedido
            Planilha.Cells[lin,19].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslPedido').AsString;
            Planilha.Cells[lin,20].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Pedido').AsString;

            //Abandono
            Planilha.Cells[lin,21].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslAbandono').AsString;
            Planilha.Cells[lin,22].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Abandono').AsString;

            //Maioridade
            Planilha.Cells[lin,23].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslMaioridade').AsString;
            Planilha.Cells[lin,24].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Maioridade').AsString;

            //Lei
            Planilha.Cells[lin,25].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslLei').AsString;
            Planilha.Cells[lin,26].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Lei').AsString;

            //Outros
            Planilha.Cells[lin,27].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslOutros').AsString;
            Planilha.Cells[lin,28].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Outros').AsString;

            //N�mero e percentual de crian�as e adolescentes com aprendizagem dentro do esperado
            Planilha.Cells[lin,29].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprendDentroEsp').AsString;
            Planilha.Cells[lin,30].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;

            //N�mero e tipo de atividades culturais desenvolvidas
            Planilha.Cells[lin,31].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind10').AsString;

            //N�mero e tipo de atividades esportivas/recreativas desenvolvidas
            Planilha.Cells[lin,32].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind11').AsString;

            //N�mero e tipo de a��es sociais
            Planilha.Cells[lin,33].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind12').AsString;

            //N�mero e tipo de a��es sociais
            Planilha.Cells[lin,34].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind13').AsString;

            //N�mero e tipo de situa��o de acompanhamento individualizado
            Planilha.Cells[lin,35].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAtendInd').AsString;

            Planilha.Cells[lin,36].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSituacaoDisciplinar').AsString;
            Planilha.Cells[lin,37].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14a').AsString;
            Planilha.Cells[lin,38].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSocEcon').AsString;
            Planilha.Cells[lin,39].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14b').AsString;
            Planilha.Cells[lin,40].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitEscolar').AsString;
            Planilha.Cells[lin,41].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14c').AsString;
            Planilha.Cells[lin,42].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitViolencia').AsString;
            Planilha.Cells[lin,43].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14d').AsString;
            Planilha.Cells[lin,44].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAbrigo').AsString;
            Planilha.Cells[lin,45].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14e').AsString;
            Planilha.Cells[lin,46].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitIntRelac').AsString;
            Planilha.Cells[lin,47].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14f').AsString;
            Planilha.Cells[lin,48].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAtoInfrac').AsString;
            Planilha.Cells[lin,49].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14g').AsString;
            Planilha.Cells[lin,50].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAprendizagem').AsString;
            Planilha.Cells[lin,51].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14h').AsString;
            Planilha.Cells[lin,52].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSaude').AsString;
            Planilha.Cells[lin,53].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14i').AsString;
            Planilha.Cells[lin,54].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGuardaIrreg').AsString;
            Planilha.Cells[lin,55].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14j').AsString;
            Planilha.Cells[lin,56].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGravidez').AsString;
            Planilha.Cells[lin,57].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14l').AsString;
            Planilha.Cells[lin,58].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitMoradia').AsString;
            Planilha.Cells[lin,59].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14m').AsString;
            Planilha.Cells[lin,60].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitTrabInfantil').AsString;
            Planilha.Cells[lin,61].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14n').AsString;
            Planilha.Cells[lin,62].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind15').AsString;
            Planilha.Cells[lin,63].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespCcas').AsString;
            Planilha.Cells[lin,64].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind16').AsString;
            Planilha.Cells[lin,65].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind17').AsString;
            Planilha.Cells[lin,66].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAcoesFamilias').AsString;
            Planilha.Cells[lin,67].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamilias').AsString;
            Planilha.Cells[lin,68].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamiliasPartic').AsString;
            Planilha.Cells[lin,69].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind19').AsString;
            Planilha.Cells[lin,70].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind20').AsString;
            Planilha.Cells[lin,71].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespFam').AsString;
            Planilha.Cells[lin,72].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind21').AsString;
            Planilha.Cells[lin,73].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTForaEnsinoRegular').AsString;
            Planilha.Cells[lin,74].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind22').AsString;
            Planilha.Cells[lin,75].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOP').AsString;
            Planilha.Cells[lin,76].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOP').AsString;
            Planilha.Cells[lin,77].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind23').AsString;
            Planilha.Cells[lin,78].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOE').AsString;
            Planilha.Cells[lin,79].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOe').AsString;
            Planilha.Cells[lin,80].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind24').AsString;

            Planilha.Cells[lin,81].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod1').AsString;
            Planilha.Cells[lin,82].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod1').AsString;
            Planilha.Cells[lin,83].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod1').AsString;

            Planilha.Cells[lin,84].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod2').AsString;
            Planilha.Cells[lin,85].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod2').AsString;
            Planilha.Cells[lin,86].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod2').AsString;

            Planilha.Cells[lin,87].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTModIntensivo').AsString;
            Planilha.Cells[lin,88].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesIntensivo').AsString;
            Planilha.Cells[lin,89].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Intensivo').AsString;

            Planilha.Cells[lin,90].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInscritosVest').AsString;
            Planilha.Cells[lin,91].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprovadosVest').AsString;
            Planilha.Cells[lin,92].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind26').AsString;

            Planilha.Cells[lin,93].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAptosAprendizagem').AsString;
            Planilha.Cells[lin,94].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind27').AsString;

            Planilha.Cells[lin,95].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInseridosAprendizagem').AsString;
            Planilha.Cells[lin,96].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind28').AsString;

            Planilha.Cells[lin,97].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemFUNDHAS').AsString;
            Planilha.Cells[lin,98].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind29').AsString;

            Planilha.Cells[lin,99].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemExterna').AsString;
            Planilha.Cells[lin,100].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind30').AsString;

            Planilha.Cells[lin,101].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('vIndicador31').AsString;
            Planilha.Cells[lin,102].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind32').AsString;
            Planilha.Cells[lin,103].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind33').AsString;

            lin := lin + 1;

          end;
          end;
        end

      //end;

    except end;

  end

  else if (RadioGroup1.ItemIndex = 1) then
  begin
    ExcelApplication1 := TExcelApplication.Create(nil);
    ExcelApplication1.Workbooks.Add(EmptyParam, 0);
    ExcelApplication1.Visible[0] := True;

    Planilha := ExcelApplication1.Workbooks[1].Worksheets[1];

    mes := 0;

    try

      with dmDeca.cdsGeraDadosMAtrizSemestreAno do
      begin
        //Montando o cabe�alho na m�o...
        Planilha.Cells[1,1].Font.Bold := True;
        Planilha.Cells[1,1].Value := 'FUNDHAS - Funda��o H�lio Augusto de Souza  [Estat�sticas de Dados da Matriz]';
        Planilha.Cells[2,1].Value := 'Ano de : ' + mskAno.Text;
        if chkFundhas.Checked then
          Planilha.Cells[3,1].Value := 'Unidade: FUNDA��O H�LIO AUGUSTO DE SOUZA'
        else
          Planilha.Cells[3,1].Value := 'Unidade: ' + cbUnidade.Text;

        Planilha.Cells[4,1].Value := 'M�s';
        Planilha.Cells[4,2].Value := 'Total Unidade';
        Planilha.Cells[4,3].Value := 'Total Matriculados';
        Planilha.Cells[4,4].Value := '% Matriculados';
        Planilha.Cells[4,5].Value := 'Idade x Serie Adequadas';
        Planilha.Cells[4,6].Value := '% S�rie Adeq.';
        Planilha.Cells[4,7].Value := 'Freq. Escolar Dentro esperado';
        Planilha.Cells[4,8].Value := '% Freq. Dentro Esperado';

        Planilha.Cells[4,9].Value := 'Aprov. Escolar Dentro esperado';
        Planilha.Cells[4,10].Value := '% Aprov. Dentro Esperado';
        Planilha.Cells[4,11].Value := 'Total Presen�as Esperadas';
        Planilha.Cells[4,12].Value := 'Total de Faltas';
        Planilha.Cells[4,13].Value := '% Freq. Atividades/Cursos';

        Planilha.Cells[4,14].Value := 'Justificadas';
        Planilha.Cells[4,15].Value := 'Injustificadas';
        Planilha.Cells[4,16].Value := '% Faltas Justificadas';

        Planilha.Cells[4,17].Value := 'Total de Desligados';
        Planilha.Cells[4,18].Value := '% Desligados';

        Planilha.Cells[4,19].Value := 'Total Pedido';
        Planilha.Cells[4,20].Value := '% Pedido';
        Planilha.Cells[4,21].Value := 'Total Abandono';
        Planilha.Cells[4,22].Value := '% Abandono';
        Planilha.Cells[4,23].Value := 'Total Maioridade';
        Planilha.Cells[4,24].Value := '% Maioridade';
        Planilha.Cells[4,25].Value := 'Total Lei';
        Planilha.Cells[4,26].Value := '% Lei';
        Planilha.Cells[4,27].Value := 'Total Outros';
        Planilha.Cells[4,28].Value := '% Outros';
        Planilha.Cells[4,29].Value := 'Aprend. Dentro Esperado';
        Planilha.Cells[4,30].Value := '% Aprend. Dentro Esperado';
        Planilha.Cells[4,31].Value := 'N.� Ativ. Culturais';
        Planilha.Cells[4,32].Value := 'N.� Ativ. Esportivas';
        Planilha.Cells[4,33].Value := 'N.� A��es Sociais';
        Planilha.Cells[4,34].Value := 'Grau Aprov. Programa';
        Planilha.Cells[4,35].Value := 'Total Atend. Ind.';
        Planilha.Cells[4,36].Value := 'Situa��o Disciplinar';
        Planilha.Cells[4,37].Value := '% Sit. Disc.';
        Planilha.Cells[4,38].Value := 'Situa��o Socioecon�mica';
        Planilha.Cells[4,39].Value := '%Sit. SocioEc.';
        Planilha.Cells[4,40].Value := 'Situa��o Escolar';
        Planilha.Cells[4,41].Value := '% Sit. Escolar';
        Planilha.Cells[4,42].Value := 'Viol�ncia';
        Planilha.Cells[4,43].Value := '% Sit. Viol�ncia';
        Planilha.Cells[4,44].Value := 'Abrigo';
        Planilha.Cells[4,45].Value := '% Sit. Abrigo';
        Planilha.Cells[4,46].Value := 'Intera��o Relacional';
        Planilha.Cells[4,47].Value := '% Sit. Int. Relac.';
        Planilha.Cells[4,48].Value := 'Ato Infracional';
        Planilha.Cells[4,49].Value := '% Sit. Ato Infr.';
        Planilha.Cells[4,50].Value := 'Aprendizagem';
        Planilha.Cells[4,51].Value := '% Sit. Aprend.';
        Planilha.Cells[4,52].Value := 'Sa�de';
        Planilha.Cells[4,53].Value := '% Sit. Sa�de';
        Planilha.Cells[4,54].Value := 'Guarda Irregular';
        Planilha.Cells[4,55].Value := '% Sit. Gurda Irr.';
        Planilha.Cells[4,56].Value := 'Gravidez';
        Planilha.Cells[4,57].Value := '% Sit. Gravidez';
        Planilha.Cells[4,58].Value := 'Moradia';
        Planilha.Cells[4,59].Value := '% Sit. Moradia';
        Planilha.Cells[4,60].Value := 'Trab. Infantil';
        Planilha.Cells[4,61].Value := '% Sit. Trab Inf.';
        Planilha.Cells[4,62].Value := 'Grau Satisf. Crian�as';
        Planilha.Cells[4,63].Value := 'Pesq. Respondidas';
        Planilha.Cells[4,64].Value := '% Pesq. Resp.';
        Planilha.Cells[4,65].Value := 'Evol. Peso x Altura';
        Planilha.Cells[4,66].Value := 'N.� A��es com Fam�lias';
        Planilha.Cells[4,67].Value := 'Total Fam�lias';
        Planilha.Cells[4,68].Value := 'Fam�lias Partic.';
        Planilha.Cells[4,69].Value := '% Fam�lias Partic.';
        Planilha.Cells[4,70].Value := 'Grau Satisf. Fam�lias';
        Planilha.Cells[4,71].Value := 'N.� Pesq. Resp. pelas Fam�lias';
        Planilha.Cells[4,72].Value := '% Pesq. Resp. Familias';
        Planilha.Cells[4,73].Value := 'Fora Ensino Regular';
        Planilha.Cells[4,74].Value := '% Fora Ens. Reg.';
        Planilha.Cells[4,75].Value := 'Total Orient. Prof.';
        Planilha.Cells[4,76].Value := 'Concluintes Orient. Prof.';
        Planilha.Cells[4,77].Value := '% Concluintes OP';
        Planilha.Cells[4,78].Value := 'Total Orient. Educ.';
        Planilha.Cells[4,79].Value := 'Concluintes Orient. Educ.';
        Planilha.Cells[4,80].Value := '% Concluintes OE';
        Planilha.Cells[4,81].Value := 'Total Mod. I';
        Planilha.Cells[4,82].Value := 'Concluintes Mod. I';
        Planilha.Cells[4,83].Value := '% Concluintes Mod. I';
        Planilha.Cells[4,84].Value := 'Total Mod. II';
        Planilha.Cells[4,85].Value := 'Concluintes Mod. II';
        Planilha.Cells[4,86].Value := '% Concluintes Mod. II';
        Planilha.Cells[4,87].Value := 'Total Mod. Intensivo';
        Planilha.Cells[4,88].Value := 'Concluintes Mod. Intensivo';
        Planilha.Cells[4,89].Value := '% Concluintes Intensivo';
        Planilha.Cells[4,90].Value := 'Total Inscritos Vest.';
        Planilha.Cells[4,91].Value := 'Total Aprovados';
        Planilha.Cells[4,92].Value := '% Aprova��o';
        Planilha.Cells[4,93].Value := 'Aptos Aprendizagem';
        Planilha.Cells[4,94].Value := '% Aptos Aprend.';
        Planilha.Cells[4,95].Value := 'Inseridos Aprendizagem';
        Planilha.Cells[4,96].Value := '% Inseridos Aprend.';
        Planilha.Cells[4,97].Value := 'Em aprend. FUNDHAS';
        Planilha.Cells[4,98].Value := '% FUNDHAS';
        Planilha.Cells[4,99].Value := 'Em aprend. Externo';
        Planilha.Cells[4,100].Value := '% Externo';
        Planilha.Cells[4,101].Value := 'Grau Satisf. Empresa';
        Planilha.Cells[4,102].Value := '% Empresas que responderam';

        lin := 5; //linha a partir da qual ser�o inseridos os valores

        for mes := 1 to 12 do
        begin
          Close;
          if (chkFundhas.Checked = False) then
            Params.ParamByName ('@pe_cod_unidade').Value      := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex])
          else
            Params.ParamByName ('@pe_cod_unidade').Value      := Null;
          Params.ParamByName ('@pe_num_bimestre').Value       := Null;
          Params.ParamByName ('@pe_num_ano').Value            := StrToInt(mskAno.Text);
          Params.ParamByName ('@pe_num_mes_inicio').Value     := mes;
          Params.ParamByName ('@pe_num_mes_final').Value      := mes;
          Open;

          Planilha.Cells[lin,1].Value := IntToStr(mes);

          //Indicador 01
          //N�mero e percentual de crian�as e adolescentes matriculados na escola
          Planilha.Cells[lin,2].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
          Planilha.Cells[lin,3].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;;
          Planilha.Cells[lin,4].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind01').AsString;

          //Indicador 02
          //N�mero e percentual de crian�as e adolescentes com "idade x s�rie" adequadas
          Planilha.Cells[lin,5].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTIdadeSerieAdequadas').AsString;
          Planilha.Cells[lin,6].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind02').AsString;

          //Indicador 03
          //N�mero e percentual de crian�as e adolescentes com frequ�ncia escolar "dentro do esperado"
          Planilha.Cells[lin,7].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFrequenciaDentroEsperadO').AsString;
          Planilha.Cells[lin,8].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind03').AsString;

          //Indicador 04
          //N�mero e percentual de crian�as e adolescentes com aproveitamento escolar "dentro do esperado"
          Planilha.Cells[lin,9].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAproveitamentoDentroEsperado').AsString;
          Planilha.Cells[lin,10].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind04').AsString;

          //Percentual de frequ�ncia de crian�as e adolescentes nas atividades/cursos
          Planilha.Cells[lin,11].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPresencasEsperadas').AsString;
          Planilha.Cells[lin,12].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTGeralFaltas').AsString;
          Planilha.Cells[lin,13].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind05').AsString;

          //Percentual de Faltas Justificadas
          //Planilha.Cells[lin,14].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltas').AsString;
          Planilha.Cells[lin,14].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasJustificadas').AsString;
          Planilha.Cells[lin,15].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasInJustificadas').AsString;
          Planilha.Cells[lin,16].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind06').AsString;

          //N�mero e percentual de crian�as/adolescentes desligados
          Planilha.Cells[lin,17].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDesligados').AsString;
          Planilha.Cells[lin,18].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind07').AsString;

          //N�mero e percentual dos motivos de desligamento
          //A Pedido
          Planilha.Cells[lin,19].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslPedido').AsString;
          Planilha.Cells[lin,20].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Pedido').AsString;

          //Abandono
          Planilha.Cells[lin,21].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslAbandono').AsString;
          Planilha.Cells[lin,22].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Abandono').AsString;

          //Maioridade
          Planilha.Cells[lin,23].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslMaioridade').AsString;
          Planilha.Cells[lin,24].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Maioridade').AsString;

          //Lei
          Planilha.Cells[lin,25].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslLei').AsString;
          Planilha.Cells[lin,26].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Lei').AsString;

          //Outros
          Planilha.Cells[lin,27].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslOutros').AsString;
          Planilha.Cells[lin,28].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Outros').AsString;

          //N�mero e percentual de crian�as e adolescentes com aprendizagem dentro do esperado
          Planilha.Cells[lin,29].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprendDentroEsp').AsString;
          Planilha.Cells[lin,30].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;

          //N�mero e tipo de atividades culturais desenvolvidas
          Planilha.Cells[lin,31].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind10').AsString;

          //N�mero e tipo de atividades esportivas/recreativas desenvolvidas
          Planilha.Cells[lin,32].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind11').AsString;

          //N�mero e tipo de a��es sociais
          Planilha.Cells[lin,33].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind12').AsString;

          //N�mero e tipo de a��es sociais
          Planilha.Cells[lin,34].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind13').AsString;

          //N�mero e tipo de situa��o de acompanhamento individualizado
          Planilha.Cells[lin,35].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAtendInd').AsString;

          Planilha.Cells[lin,36].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSituacaoDisciplinar').AsString;
          Planilha.Cells[lin,37].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14a').AsString;
          Planilha.Cells[lin,38].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSocEcon').AsString;
          Planilha.Cells[lin,39].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14b').AsString;
          Planilha.Cells[lin,40].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitEscolar').AsString;
          Planilha.Cells[lin,41].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14c').AsString;
          Planilha.Cells[lin,42].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitViolencia').AsString;
          Planilha.Cells[lin,43].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14d').AsString;
          Planilha.Cells[lin,44].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAbrigo').AsString;
          Planilha.Cells[lin,45].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14e').AsString;
          Planilha.Cells[lin,46].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitIntRelac').AsString;
          Planilha.Cells[lin,47].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14f').AsString;
          Planilha.Cells[lin,48].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAtoInfrac').AsString;
          Planilha.Cells[lin,49].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14g').AsString;
          Planilha.Cells[lin,50].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAprendizagem').AsString;
          Planilha.Cells[lin,51].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14h').AsString;
          Planilha.Cells[lin,52].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSaude').AsString;
          Planilha.Cells[lin,53].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14i').AsString;
          Planilha.Cells[lin,54].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGuardaIrreg').AsString;
          Planilha.Cells[lin,55].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14j').AsString;
          Planilha.Cells[lin,56].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGravidez').AsString;
          Planilha.Cells[lin,57].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14l').AsString;
          Planilha.Cells[lin,58].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitMoradia').AsString;
          Planilha.Cells[lin,59].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14m').AsString;
          Planilha.Cells[lin,60].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitTrabInfantil').AsString;
          Planilha.Cells[lin,61].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14n').AsString;
          Planilha.Cells[lin,62].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind15').AsString;
          Planilha.Cells[lin,63].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespCcas').AsString;
          Planilha.Cells[lin,64].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind16').AsString;
          Planilha.Cells[lin,65].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind17').AsString;
          Planilha.Cells[lin,66].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAcoesFamilias').AsString;
          Planilha.Cells[lin,67].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamilias').AsString;
          Planilha.Cells[lin,68].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamiliasPartic').AsString;
          Planilha.Cells[lin,69].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind19').AsString;
          Planilha.Cells[lin,70].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind20').AsString;
          Planilha.Cells[lin,71].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespFam').AsString;
          Planilha.Cells[lin,72].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind21').AsString;
          Planilha.Cells[lin,73].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTForaEnsinoRegular').AsString;
          Planilha.Cells[lin,74].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind22').AsString;
          Planilha.Cells[lin,75].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOP').AsString;
          Planilha.Cells[lin,76].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOP').AsString;
          Planilha.Cells[lin,77].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind23').AsString;
          Planilha.Cells[lin,78].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOE').AsString;
          Planilha.Cells[lin,79].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOe').AsString;
          Planilha.Cells[lin,80].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind24').AsString;

          Planilha.Cells[lin,81].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod1').AsString;
          Planilha.Cells[lin,82].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod1').AsString;
          Planilha.Cells[lin,83].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod1').AsString;

          Planilha.Cells[lin,84].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod2').AsString;
          Planilha.Cells[lin,85].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod2').AsString;
          Planilha.Cells[lin,86].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod2').AsString;

          Planilha.Cells[lin,87].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTModIntensivo').AsString;
          Planilha.Cells[lin,88].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesIntensivo').AsString;
          Planilha.Cells[lin,89].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Intensivo').AsString;

          Planilha.Cells[lin,90].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInscritosVest').AsString;
          Planilha.Cells[lin,91].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprovadosVest').AsString;
          Planilha.Cells[lin,92].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind26').AsString;

          Planilha.Cells[lin,93].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAptosAprendizagem').AsString;
          Planilha.Cells[lin,94].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind27').AsString;

          Planilha.Cells[lin,95].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInseridosAprendizagem').AsString;
          Planilha.Cells[lin,96].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind28').AsString;

          Planilha.Cells[lin,97].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemFUNDHAS').AsString;
          Planilha.Cells[lin,98].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind29').AsString;

          Planilha.Cells[lin,99].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemExterna').AsString;
          Planilha.Cells[lin,100].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind30').AsString;

          Planilha.Cells[lin,101].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('vIndicador31').AsString;
          Planilha.Cells[lin,102].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind32').AsString;
          Planilha.Cells[lin,103].Value := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind33').AsString;
          lin := lin + 1;

        end;

      end;
    except end;

  end;
}

end;

procedure TfrmGeraEstatisticaMatriz.btnGerarEstatisticasClick(
  Sender: TObject);

var
  //objExcel, Sheet : Variant;
  lin, lin2, lin3, lin4, lin5, lin6, lin7, lin8, lin9, lin10,
  lin11, lin12, lin13, lin14, lin15, lin16, lin17,
  lin18, lin19, lin20, lin21, lin22, col : integer;
begin

  //if (RadioGroup1.ItemIndex = 1) then
  //begin
    Label1.Visible := True;
    Label1.Caption := 'Aguarde... Gerando dados...';
    //mes := 1;
    objExcel := CreateOleObject('Excel.Application');
    objExcel.Caption := 'FUNDHAS - Matriz de Avalia��o DECA -> Ano refer�ncia:  ' + mskAno.Text;
    objExcel.Workbooks.Add;
    objExcel.Workbooks[1].SaveAs (ExtractFilePath(Application.ExeName) + 'Matriz de Avaliacao FUNDHAS.xlsx');
    objExcel.Visible := False;

    ProgressBar1.Position := 0;
    ProgressBar2.Position := 0;

    ProgressBar1.Min := 1;
    ProgressBar1.Max := 12;

    for mes := 12 downto 1 do
    begin

      //Seleciona as unidades que ser�o gerados os dados estat�sticos
      try
        //Cria uma planilha para cada m�s
        //Planilha := ExcelApplication1.Workbooks[1].Worksheets[mes];
        objExcel.Workbooks[1].Sheets.Add;
        Sheet := objExcel.Workbooks[1].Worksheets[1];

        case mes of
         1:  Sheet.Name := 'JAN' + mskAno.Text;
         2:  Sheet.Name := 'FEV' + mskAno.Text;
         3:  Sheet.Name := 'MAR' + mskAno.Text;
         4:  Sheet.Name := 'ABR' + mskAno.Text;
         5:  Sheet.Name := 'MAI' + mskAno.Text;
         6:  Sheet.Name := 'JUN' + mskAno.Text;
         7:  Sheet.Name := 'JUL' + mskAno.Text;
         8:  Sheet.Name := 'AGO' + mskAno.Text;
         9:  Sheet.Name := 'SET' + mskAno.Text;
         10: Sheet.Name := 'OUT' + mskAno.Text;
         11: Sheet.Name := 'NOV' + mskAno.Text;
         12: Sheet.Name := 'DEZ' + mskAno.Text;
        end;

        //Sheet.Name := mes;

        //Iniciar na linha 5
        lin := 5;

        //Formatar o cabe�alho da Planilha...
        //Indicador 1
        Sheet.Cells[lin-3,1].Font.Bold  := True;
        Sheet.Cells[lin-3,1].Value      := 'Indicador';
        Sheet.Cells[lin-2,1].Font.Bold  := True;
        Sheet.Cells[lin-2,1].Value      := 'DADOS REFERENTES A MATRICULADOS NA ESCOLA';
        Sheet.Cells[lin-1,2].Value      := 'Total Unidade';
        Sheet.Cells[lin-1,3].Value      := 'Total Matriculados';
        Sheet.Cells[lin-1,4].Value      := '% Matriculados';
        Sheet.Cells[lin-1,5].Value      := 'N�o Matriculados';
        Sheet.Cells[lin-1,6].Value      := '% N�o Matriculados';

        //Indicador 1 e 3
        lin2 := 55;
        Sheet.Cells[lin2-3,1].Font.Bold := True;
        Sheet.Cells[lin2-3,1].Value     := 'Indicador';
        Sheet.Cells[lin2-2,1].Font.Bold := True;
        Sheet.Cells[lin2-2,1].Value     := 'DADOS REFERENTES A FREQU�NCIA ESCOLAR';
        Sheet.Cells[lin2-1,2].Value     := 'Total Unidade';
        Sheet.Cells[lin2-1,3].Value     := 'Total Matriculados';
        Sheet.Cells[lin2-1,4].Value     := '% Matriculados';
        Sheet.Cells[lin2-1,5].Value     := 'Freq. Dentro Esperado';
        Sheet.Cells[lin2-1,6].Value     := '% Freq. Dentro Esperado';
        Sheet.Cells[lin2-1,7].Value     := 'Freq. Abaixo Esperado';
        Sheet.Cells[lin2-1,8].Value     := '% Freq. Abaixo Esperado';
        Sheet.Cells[lin2-1,9].Value     := 'Sem Informa��o Freq.';
        Sheet.Cells[lin2-1,10].Value    := '% Sem Informa��o Freq.';

        //Indicador 1, 5 e 6
        lin3 := 105;
        Sheet.Cells[lin3-3,1].Font.Bold := True;
        Sheet.Cells[lin3-3,1].Value     := 'Indicador';
        Sheet.Cells[lin3-2,1].Font.Bold := True;
        Sheet.Cells[lin3-2,1].Value     := 'DADOS REFERENTES A FREQU�NCIA NA UNIDADE';
        Sheet.Cells[lin3-1,2].Value     := 'Total Unidade';
        Sheet.Cells[lin3-1,3].Value     := 'Presen�as Esperadas';
        Sheet.Cells[lin3-1,4].Value     := 'Total Presen�as(Real)';
        Sheet.Cells[lin3-1,5].Value     := '% Presen�as(Real)';
        Sheet.Cells[lin3-1,6].Value     := 'Total Geral de Faltas';
        Sheet.Cells[lin3-1,7].Value     := '% Total de Faltas';
        Sheet.Cells[lin3-1,8].Value     := 'Total Justificadas';
        Sheet.Cells[lin3-1,9].Value     := '% Justificadas';
        Sheet.Cells[lin3-1,10].Value    := 'Total Injustificadas';
        Sheet.Cells[lin3-1,11].Value    := '% Injustificadas';
 
        //Indicador 2
        lin4 := 155;
        Sheet.Cells[lin4-3,1].Font.Bold := True;
        Sheet.Cells[lin4-3,1].Value     := 'Indicador';
        Sheet.Cells[lin4-2,1].Font.Bold := True;
        Sheet.Cells[lin4-2,1].Value     := 'DADOS REFERENTES A DEFASAGEM ESCOLAR - Idade x S�rie adequada';
        Sheet.Cells[lin4-1,2].Value     := 'Matriculados na Escola';
        Sheet.Cells[lin4-1,3].Value     := 'Total Dentro do Esperado';
        Sheet.Cells[lin4-1,4].Value     := '% Dentro Esperado';
        Sheet.Cells[lin4-1,5].Value     := 'Total Def. Escolar';
        Sheet.Cells[lin4-1,6].Value     := '% Def. Escolar';


        //Indicadores 7 e 8
        lin5 := 205;
        Sheet.Cells[lin5-3,1].Font.Bold := True;
        Sheet.Cells[lin5-3,1].Value     := 'Indicador';
        Sheet.Cells[lin5-2,1].Font.Bold := True;
        Sheet.Cells[lin5-2,1].Value     := 'DADOS REFERENTES A DESLIGAMENTOS';
        Sheet.Cells[lin5-1,2].Value     := 'Total Unidade';
        Sheet.Cells[lin5-1,3].Value     := 'Total de Desligados';
        Sheet.Cells[lin5-1,4].Value     := '% Desligados';
        Sheet.Cells[lin5-1,5].Value     := 'Abandono';
        Sheet.Cells[lin5-1,6].Value     := '% Abandono';
        Sheet.Cells[lin5-1,7].Value     := 'Maioridade';
        Sheet.Cells[lin5-1,8].Value     := '% Maioridade';
        Sheet.Cells[lin5-1,9].Value     := 'Lei';
        Sheet.Cells[lin5-1,10].Value    := '% Lei';
        Sheet.Cells[lin5-1,11].Value    := 'A Pedido';
        Sheet.Cells[lin5-1,12].Value    := '% A Pedido';
        Sheet.Cells[lin5-1,13].Value    := 'Outros';
        Sheet.Cells[lin5-1,14].Value    := '% Outros';

        //Indicador 14
        lin6 := 255;
        Sheet.Cells[lin6-3,1].Font.Bold := True;
        Sheet.Cells[lin6-3,1].Value     := 'Indicador';
        Sheet.Cells[lin6-2,1].Font.Bold := True;
        Sheet.Cells[lin6-2,1].Value     := 'DADOS REFERENTES A ACOMPANHAMENTOS INDIVIDUALIZADOS';
        Sheet.Cells[lin6-1,2].Value     := 'Total de Atendimentos';
        Sheet.Cells[lin6-1,3].Value     := 'Disciplinar';
        Sheet.Cells[lin6-1,4].Value     := '% Disciplinar';
        Sheet.Cells[lin6-1,5].Value     := 'Socioecon�mica';
        Sheet.Cells[lin6-1,6].Value     := '% Socioec.';
        Sheet.Cells[lin6-1,7].Value     := 'Escolar';
        Sheet.Cells[lin6-1,8].Value     := '% Escolar';
        Sheet.Cells[lin6-1,9].Value     := 'Viol�ncia';
        Sheet.Cells[lin6-1,10].Value    := '% Viol�ncia';
        Sheet.Cells[lin6-1,11].Value    := 'Abrigo';
        Sheet.Cells[lin6-1,12].Value    := '% Abrigo';
        Sheet.Cells[lin6-1,13].Value    := 'Int. Relac.';
        Sheet.Cells[lin6-1,14].Value    := '% Int. Rel.';
        Sheet.Cells[lin6-1,15].Value    := 'Ato Infrac.';
        Sheet.Cells[lin6-1,16].Value    := '% Ato Infrac.';
        Sheet.Cells[lin6-1,17].Value    := 'Aprendizagem';
        Sheet.Cells[lin6-1,18].Value    := '% Aprend.';
        Sheet.Cells[lin6-1,19].Value    := 'Sa�de';
        Sheet.Cells[lin6-1,20].Value    := '% Sa�de';
        Sheet.Cells[lin6-1,21].Value    := 'Guarda Irreg.';
        Sheet.Cells[lin6-1,22].Value    := '% Guarda Irreg.';
        Sheet.Cells[lin6-1,23].Value    := 'Gravidez Adol.';
        Sheet.Cells[lin6-1,24].Value    := '% Gravidez Adol.';
        Sheet.Cells[lin6-1,25].Value    := 'Moradia';
        Sheet.Cells[lin6-1,26].Value    := '% Moradia';
        Sheet.Cells[lin6-1,27].Value    := 'Trab. Infantil';
        Sheet.Cells[lin6-1,28].Value    := '% Trab. Infantil';

        //Indicadores 10 e 11
        lin7 := 305;
        Sheet.Cells[lin7-3,1].Font.Bold := True;
        Sheet.Cells[lin7-3,1].Value     := 'Indicador';
        Sheet.Cells[lin7-2,1].Font.Bold := True;
        Sheet.Cells[lin7-2,1].Value     := 'DADOS REFERENTES A ATIVIDADES SOCIOCULTURAIS ESPORTIVAS';
        Sheet.Cells[lin7-1,2].Value     := 'Quantidade';
        Sheet.Cells[lin7-1,3].Value     := 'Tipo de Atividades socioculturais';
        Sheet.Cells[lin7-1,4].Value     := 'Quantidade';
        Sheet.Cells[lin7-1,5].Value     := 'Tipo de atividades recreativas/esportivas';

        //Indicador 16
        lin8 := 355;
        Sheet.Cells[lin8-3,1].Font.Bold := True;
        Sheet.Cells[lin8-3,1].Value     := 'Indicador';
        Sheet.Cells[lin8-2,1].Font.Bold := True;
        Sheet.Cells[lin8-2,1].Value     := 'DADOS REFERENTES A SATISFA��O DE CRIAN�AS E ADOLESCENTES';
        Sheet.Cells[lin8-1,2].Value     := 'Total Unidade';
        Sheet.Cells[lin8-1,3].Value     := 'Pesquisados';
        Sheet.Cells[lin8-1,4].Value     := '% Pesquisa';
        Sheet.Cells[lin8-1,5].Value     := 'Estrutura F�sica';
        Sheet.Cells[lin8-1,6].Value     := 'Servi�os Prestados';
        Sheet.Cells[lin8-1,7].Value     := 'Recursos Humanos';
        Sheet.Cells[lin8-1,8].Value     := 'Participa��o';

        //Indicador 18
        lin9 := 405;
        Sheet.Cells[lin9-3,1].Font.Bold := True;
        Sheet.Cells[lin9-3,1].Value     := 'Indicador';
        Sheet.Cells[lin9-2,1].Font.Bold := True;
        Sheet.Cells[lin9-2,1].Value     := 'DADOS REFERENTES A A��ES REALIZADAS COM FAM�LIAS';
        Sheet.Cells[lin9-1,2].Value     := 'Quantidade';
        Sheet.Cells[lin9-1,3].Value     := 'Tipo de a��es realizadas com as fam�lias';

        //Indicador 19
        lin10 := 455;
        Sheet.Cells[lin10-3,1].Font.Bold := True;
        Sheet.Cells[lin10-3,1].Value     := 'Indicador';
        Sheet.Cells[lin10-2,1].Font.Bold := True;
        Sheet.Cells[lin10-2,1].Value     := 'DADOS REFERENTES A PARTICIPA��O DAS FAM�LIAS NAS ATIVIDADES';
        Sheet.Cells[lin10-1,2].Value     := 'Convidadas';
        Sheet.Cells[lin10-1,3].Value     := 'Participantes';
        Sheet.Cells[lin10-1,4].Value     := '% Participa��o';
        Sheet.Cells[lin10-1,5].Value     := 'N�o Participantes';
        Sheet.Cells[lin10-1,6].Value     := '% N�o Participantes';


        //Indicadores 20 e 21
        lin11 := 505;
        Sheet.Cells[lin11-3,1].Font.Bold := True;
        Sheet.Cells[lin11-3,1].Value     := 'Indicador';
        Sheet.Cells[lin11-2,1].Font.Bold := True;
        Sheet.Cells[lin11-2,1].Value     := 'DADOS REFERENTES A SATISFA��O DAS FAM�LIAS';
        Sheet.Cells[lin11-1,2].Value     := 'Total Fam�lias';
        Sheet.Cells[lin11-1,3].Value     := 'Pesquisadas';
        Sheet.Cells[lin11-1,4].Value     := '% Pesquisadas';
        Sheet.Cells[lin11-1,5].Value     := 'Estrutura F�sica';
        Sheet.Cells[lin11-1,6].Value     := 'Servi�os Prestados';
        Sheet.Cells[lin11-1,7].Value     := 'Recursos Humanos';
        Sheet.Cells[lin11-1,8].Value     := 'Participa��o';

        //Indicador 09
        lin12 := 560;
        Sheet.Cells[lin12-3,1].Font.Bold := True;
        Sheet.Cells[lin12-3,1].Value     := 'Indicador';
        Sheet.Cells[lin12-2,1].Font.Bold := True;
        Sheet.Cells[lin12-2,1].Value     := 'DADOS REFERENTES A DEFASAGEM NA APRENDIZAGEM';
        Sheet.Cells[lin12-2,2].Value     := 'Total Unidade';
        Sheet.Cells[lin12-2,3].Value     := 'Total Def. Aprend.';
        Sheet.Cells[lin12-2,4].Value     := '% Def. Aprend.';
        Sheet.Cells[lin12-2,5].Value     := 'Aprend. Dentro Esperado';
        Sheet.Cells[lin12-2,6].Value     := '% Dentro Esperado';

        //Indicador 17
        lin13 := 605;
        Sheet.Cells[lin13-3,1].Font.Bold := True;
        Sheet.Cells[lin13-3,1].Value     := 'Indicador';
        Sheet.Cells[lin13-2,1].Font.Bold := True;
        Sheet.Cells[lin13-2,1].Value     := 'DADOS REFERENTES A EVOLU��O PESO x ALTURA';
        Sheet.Cells[lin13-2,2].Value     := 'Total Unidade';
        Sheet.Cells[lin13-2,3].Value     := 'Avalidados';
        Sheet.Cells[lin13-2,4].Value     := 'Cond. Normal';
        Sheet.Cells[lin13-2,5].Value     := '% Normal';
        Sheet.Cells[lin13-2,6].Value     := 'Abaixo do Peso';
        Sheet.Cells[lin13-2,7].Value     := '% Abaixo do Peso';
        Sheet.Cells[lin13-2,8].Value     := 'Acima do Peso';
        Sheet.Cells[lin13-2,9].Value     := '% Acima do Peso';
        Sheet.Cells[lin13-2,10].Value    := 'Obesos';
        Sheet.Cells[lin13-2,11].Value    := '% Obesos';

        //Indicador 22
        lin14 := 655;
        Sheet.Cells[lin14-3,1].Font.Bold := True;
        Sheet.Cells[lin14-3,1].Value     := 'Indicador';
        Sheet.Cells[lin14-2,1].Font.Bold := True;
        Sheet.Cells[lin14-2,1].Value     := 'FORA DO ENSINO REGULAR';
        Sheet.Cells[lin14-2,2].Value     := 'Total Unidade';
        Sheet.Cells[lin14-2,3].Value     := 'Matriculados';
        Sheet.Cells[lin14-2,4].Value     := '% Matriculados';
        Sheet.Cells[lin14-2,5].Value     := 'Fora Ensino Regular';
        Sheet.Cells[lin14-2,6].Value     := '% Fora Ens. Reg.';


        //Indicador 25
        lin15 := 705;
        Sheet.Cells[lin15-3,1].Font.Bold := True;
        Sheet.Cells[lin15-3,1].Value     := 'Indicador';
        Sheet.Cells[lin15-2,1].Font.Bold := True;
        Sheet.Cells[lin15-2,1].Value     := 'ADOLESCENTES CONCLUINTES DOS CURSOS DE FORMA��O INICIAL E CONTINUADA';
        Sheet.Cells[lin15-2,2].Value     := 'Total Mod.I';
        Sheet.Cells[lin15-2,3].Value     := 'Total Mod. II';
        Sheet.Cells[lin15-2,4].Value     := 'Total Intensivo';
        Sheet.Cells[lin15-2,5].Value     := 'Aprov. Mod. I';
        Sheet.Cells[lin15-2,6].Value     := '% Aprov I';
        Sheet.Cells[lin15-2,7].Value     := 'Aprov. Mod. II';
        Sheet.Cells[lin15-2,8].Value     := '% Aprov. II';
        Sheet.Cells[lin15-2,9].Value     := 'Aprov. Intensivo';
        Sheet.Cells[lin15-2,10].Value    := '% Aprov. Int.';

        //Indicadores 23 e 24
        lin16 := 755;
        Sheet.Cells[lin16-3,1].Font.Bold := True;
        Sheet.Cells[lin16-3,1].Value     := 'Indicador';
        Sheet.Cells[lin16-2,1].Font.Bold := True;
        Sheet.Cells[lin16-2,1].Value     := 'CONCLUINTES NOS PROJETOS ORIENTA��O PROFISSIONAL E EDUCACIONAL';
        Sheet.Cells[lin16-2,2].Value     := 'Total OP';
        Sheet.Cells[lin16-2,3].Value     := 'Total Concl. OP';
        Sheet.Cells[lin16-2,4].Value     := '% Concl. OP';
        Sheet.Cells[lin16-2,5].Value     := 'Total OE';
        Sheet.Cells[lin16-2,6].Value     := 'Total Concl. OE';
        Sheet.Cells[lin16-2,7].Value     := '% Concl. OE';

        //Indicador 26
        lin17 := 805;
        Sheet.Cells[lin17-3,1].Font.Bold := True;
        Sheet.Cells[lin17-3,1].Value     := 'Indicador';
        Sheet.Cells[lin17-2,1].Font.Bold := True;
        Sheet.Cells[lin17-2,1].Value     := 'APROVEITAMENTO VESTIBULINHO';
        Sheet.Cells[lin17-2,2].Value     := 'Inscritos';
        Sheet.Cells[lin17-2,3].Value     := 'Total Realizaram Vest.';
        Sheet.Cells[lin17-2,4].Value     := '% Realizaram';
        Sheet.Cells[lin17-2,5].Value     := 'Total Aprov. Vest.';
        Sheet.Cells[lin17-2,6].Value     := '% Aprovados';
        Sheet.Cells[lin17-2,7].Value     := '% Absente�smo';

        //Indicadores 27 e 28
        lin18 := 855;
        Sheet.Cells[lin18-3,1].Font.Bold := True;
        Sheet.Cells[lin18-3,1].Value     := 'Indicador';
        Sheet.Cells[lin18-2,1].Font.Bold := True;
        Sheet.Cells[lin18-2,1].Value     := 'INSERIDOS NA APRENDIZAGEM PROFISSIONAL';
        Sheet.Cells[lin18-2,2].Value     := 'Aptos';
        Sheet.Cells[lin18-2,3].Value     := 'Total Inseridos';
        Sheet.Cells[lin18-2,4].Value     := '% Inseridos';

        //Indicadores 28 e 29
        lin19 := 905;
        Sheet.Cells[lin19-3,1].Font.Bold := True;
        Sheet.Cells[lin19-3,1].Value     := 'Indicador';
        Sheet.Cells[lin19-2,1].Font.Bold := True;
        Sheet.Cells[lin19-2,1].Value     := 'APRENDIZAGEM PROFISSIONAL';
        Sheet.Cells[lin19-2,2].Value     := 'Total Inseridos';
        Sheet.Cells[lin19-2,3].Value     := 'Total Aprend. FUNDHAS';
        Sheet.Cells[lin19-2,4].Value     := '% FUNDHAS';

        //Indicadores 28 e 30
        lin20 := 955;
        Sheet.Cells[lin20-3,1].Font.Bold := True;
        Sheet.Cells[lin20-3,1].Value     := 'Indicador';
        Sheet.Cells[lin20-2,1].Font.Bold := True;
        Sheet.Cells[lin20-2,1].Value     := 'APRENDIZAGEM PROFISSIONAL';
        Sheet.Cells[lin20-2,2].Value     := 'Inseridos';
        Sheet.Cells[lin20-2,3].Value     := 'Aprend. Externa';
        Sheet.Cells[lin20-2,4].Value     := '% Aprend. Externa';

        //Indicador 31
        lin21 := 1005;
        Sheet.Cells[lin21-3,1].Font.Bold := True;
        Sheet.Cells[lin21-3,1].Value     := 'Indicador';
        Sheet.Cells[lin21-2,1].Font.Bold := True;
        Sheet.Cells[lin21-2,1].Value     := 'DESEMPENHO DO APRENDIZ NO CAMPO PR�TICO';
        Sheet.Cells[lin21-2,2].Value     := 'TABULADO PELAS EQUIPES, CONFORME ORIENTA��O DA EQUIPE MULTIDISCIPLINAR.';


        //Indicadores 32 e 33
        lin22 := 1055;
        Sheet.Cells[lin22-3,1].Font.Bold := True;
        Sheet.Cells[lin22-3,1].Value     := 'Indicador';
        Sheet.Cells[lin22-2,1].Font.Bold := True;
        Sheet.Cells[lin22-2,1].Value     := 'PESQUISA DE SATISFA��O DAS EMPRESAS CONVENIADAS';
        Sheet.Cells[lin22-2,2].Value     := 'Responderam Pesquisa';
        Sheet.Cells[lin22-2,3].Value     := 'Vide em relat�rio do Sistema Deca';

        ProgressBar2.Position := 0;

        //Monta a planilha unidade por unidade dentro do m�s/ano
        with dmDeca.cdsSel_Unidade do
        begin
          Close;
          Params.ParamByName ('@pe_cod_unidade').Value := Null;
          Params.ParamByName ('@pe_nom_unidade').Value := Null;
          Open;

          ProgressBar2.Min := 0;
          ProgressBar2.Max := dmDeca.cdsSel_Unidade.RecordCount;
          while not eof do
          begin

            if (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').AsInteger) in [1,3,7,12,13,40,42,43,47,48,50,71,73,77,79] then
            begin
              lin   := lin - 1;
              lin2  := lin2 - 1;
              lin3  := lin3 - 1;
              lin4  := lin4 - 1;
              lin5  := lin5 - 1;
              lin6  := lin6 - 1;
              lin7  := lin7 - 1;
              lin8  := lin8 - 1;
              lin9  := lin9 - 1;
              lin10 := lin10 - 1;
              lin11 := lin11 - 1;
              lin12 := lin12 - 1;
              lin13 := lin13 - 1;
              lin14 := lin14 - 1;
              lin15 := lin15 - 1;
              lin16 := lin16 - 1;
              lin17 := lin17 - 1;
              lin18 := lin18 - 1;
              lin19 := lin19 - 1;
              lin20 := lin20 - 1;
              lin21 := lin21 - 1;
              lin22 := lin22 - 1;
            end //if FieldByName('cod_unidade')....
            else //if FieldByName('cod_unidade')....
            begin
              with dmDeca.cdsGeraDadosMAtrizSemestreAno do
              begin
                Close;
                Params.ParamByName ('@pe_cod_unidade').Value    := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
                Params.ParamByName ('@pe_num_bimestre').Value   := Null;
                Params.ParamByName ('@pe_num_ano').Value        := mskAno.Text;
                Params.ParamByName ('@pe_num_mes_inicio').Value := mes;
                Params.ParamByName ('@pe_num_mes_final').Value  := mes;
                Open;

                //Monta a planilha com os dados gerados...
                //Indicador:
                // -> N�mero e percentual de crian�as e adolescentes matriculados na escola
                //INDICADOR 01
                Sheet.Cells[lin,1].Value     := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin,2].Value     := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin,3].Value     := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;;
                Sheet.Cells[lin,4].Value     := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind01').AsString;

                //Indicador:
                // -> N�mero e percentual de crian�as e adolescentes matriculados na escola e
                // -> N�mero e percentual de crian�as e adolescentes com frquencia escolar dentro do esperado
                //INDICADORES 01 E 03
                Sheet.Cells[lin2,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin2,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin2,3].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;;
                Sheet.Cells[lin2,4].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind01').AsString;
                Sheet.Cells[lin2,5].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFrequenciaDentroEsperadO').AsString;
                Sheet.Cells[lin2,6].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind03').AsString;
                Sheet.Cells[lin2,7].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin2,8].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin2,9].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin2,10].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> N�mero e percentual de crian�as e adolescentes matriculados na escola e
                // -> Percentual de frequ�ncia de crian�as e adolescentes nas atividades/cursos
                // -> Percentual de faltas justificadas
                //INDICADORES 01, 05 E 06
                Sheet.Cells[lin3,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin3,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin3,3].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPresencasEsperadas').AsString;
                Sheet.Cells[lin3,4].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin3,5].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin3,6].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTGeralFaltas').AsString;
                Sheet.Cells[lin3,7].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind05').AsString;
                Sheet.Cells[lin3,8].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasJustificadas').AsString;
                Sheet.Cells[lin3,9].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind06').AsString;
                Sheet.Cells[lin3,10].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFaltasInJustificadas').AsString;
                Sheet.Cells[lin3,11].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> N�mero e percentual de crian�as e adolescentes com "idade x s�rie" adequadas
                //INDICADOR 02
                Sheet.Cells[lin4,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin4,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;
                Sheet.Cells[lin4,3].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTIdadeSerieAdequadas').AsString;
                Sheet.Cells[lin4,4].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind02').AsString;
                Sheet.Cells[lin4,5].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;
                Sheet.Cells[lin4,6].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;

                //Indicador:
                // -> N�mero e percentual de crian�as e adolescentes desligados e
                // -> N�mero e percentual dos motivos de desligamento
                //INDICADORES 07 E 08
                Sheet.Cells[lin5,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin5,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin5,3].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDesligados').AsString;
                Sheet.Cells[lin5,4].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind07').AsString;
                Sheet.Cells[lin5,5].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslAbandono').AsString;
                Sheet.Cells[lin5,6].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Abandono').AsString;
                Sheet.Cells[lin5,7].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslMaioridade').AsString;
                Sheet.Cells[lin5,8].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Maioridade').AsString;
                Sheet.Cells[lin5,9].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslLei').AsString;
                Sheet.Cells[lin5,10].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Lei').AsString;
                Sheet.Cells[lin5,11].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslPedido').AsString;
                Sheet.Cells[lin5,12].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Pedido').AsString;
                Sheet.Cells[lin5,13].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTDeslOutros').AsString;
                Sheet.Cells[lin5,14].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind08Outros').AsString;

                //Indicador:
                // -> N�mero e tipo de situa��o de acompanhamento individualizado
                //INDICADOR 14
                Sheet.Cells[lin6,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin6,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAtendInd').AsString;
                Sheet.Cells[lin6,3].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSituacaoDisciplinar').AsString;
                Sheet.Cells[lin6,4].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14a').AsString;
                Sheet.Cells[lin6,5].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSocEcon').AsString;
                Sheet.Cells[lin6,6].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14b').AsString;
                Sheet.Cells[lin6,7].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitEscolar').AsString;
                Sheet.Cells[lin6,8].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14c').AsString;
                Sheet.Cells[lin6,9].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitViolencia').AsString;
                Sheet.Cells[lin6,10].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14d').AsString;
                Sheet.Cells[lin6,11].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAbrigo').AsString;
                Sheet.Cells[lin6,12].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14e').AsString;
                Sheet.Cells[lin6,13].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitIntRelac').AsString;
                Sheet.Cells[lin6,14].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14f').AsString;
                Sheet.Cells[lin6,15].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAtoInfrac').AsString;
                Sheet.Cells[lin6,16].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14g').AsString;
                Sheet.Cells[lin6,17].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitAprendizagem').AsString;
                Sheet.Cells[lin6,18].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14h').AsString;
                Sheet.Cells[lin6,19].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitSaude').AsString;
                Sheet.Cells[lin6,20].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14i').AsString;
                Sheet.Cells[lin6,21].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGuardaIrreg').AsString;
                Sheet.Cells[lin6,22].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14j').AsString;
                Sheet.Cells[lin6,23].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitGravidez').AsString;
                Sheet.Cells[lin6,24].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14l').AsString;
                Sheet.Cells[lin6,25].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitMoradia').AsString;
                Sheet.Cells[lin6,26].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14m').AsString;
                Sheet.Cells[lin6,27].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTSitTrabInfantil').AsString;
                Sheet.Cells[lin6,28].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind14n').AsString;

                //Indicador:
                // -> N�mero e tipo de atividades culturais desenvolvidas e
                // -> N�mero e tipo de atividades esportivas e recreativas desenvolvidas
                //INDICADORES 10 E 11
                Sheet.Cells[lin7,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin7,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind10').AsString;
                Sheet.Cells[lin7,3].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin7,4].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind11').AsString;
                Sheet.Cells[lin7,5].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> N�mero e tipo de atividades culturais desenvolvidas e
                //INDICADOR 16
                Sheet.Cells[lin8,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin8,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin8,3].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespCcas').AsString;
                Sheet.Cells[lin8,4].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind16').AsString;
                Sheet.Cells[lin8,5].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin8,6].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin8,7].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin8,8].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> N�mero e rela��o de a��es realizadas com fam�lias
                //INDICADOR 18
                Sheet.Cells[lin9,1].Value    := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin9,2].Value    := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAcoesFamilias').AsString;
                Sheet.Cells[lin9,3].Value    := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespCcas').AsString;

                //Indicador:
                // -> N�mero e rela��o de a��es realizadas com fam�lias
                //INDICADOR 19
                Sheet.Cells[lin10,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin10,2].Value   := ''; //dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamiliasConvidadas').AsString;
                Sheet.Cells[lin10,3].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamiliasPartic').AsString;
                Sheet.Cells[lin10,4].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind19').AsString;
                Sheet.Cells[lin10,5].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin10,6].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> Grau de satisfa��o das fam�lias quanto ao programa
                // -> N�mero e percentual das fam�lias que responderam a pesquisa
                //INDICADORES 20 E 21
                Sheet.Cells[lin11,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin11,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTFamilias').AsString;
                Sheet.Cells[lin11,3].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesqRespFam').AsString;
                Sheet.Cells[lin11,4].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind21').AsString;
                Sheet.Cells[lin11,5].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin11,6].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin11,7].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin11,8].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> N�mero e percentual de crian�as e adolescentes com defasagem de aprendizagem dentro do esperado
                //INDICADOR 09
                Sheet.Cells[lin12,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin12,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin12,3].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin12,4].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin12,5].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprendDentroEsp').AsString;
                Sheet.Cells[lin12,6].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;

                //Indicador:
                // -> Acompanhamento da Evolu��o do peso e altura
                //INDICADOR 17
                Sheet.Cells[lin13,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin13,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin13,3].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin13,4].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin13,5].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprendDentroEsp').AsString;
                Sheet.Cells[lin13,6].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;
                Sheet.Cells[lin13,7].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;
                Sheet.Cells[lin13,8].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;
                Sheet.Cells[lin13,9].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;
                Sheet.Cells[lin13,10].Value  := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;
                Sheet.Cells[lin13,11].Value  := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind09').AsString;

                //Indicador:
                // -> N�emro e percentual de adolescentes cursando EJA/Telessala/Supl�ncia
                //INDICADOR 22
                Sheet.Cells[lin14,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin14,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTCriancasUnidade').AsString;
                Sheet.Cells[lin14,3].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('MatriculadosEscola').AsString;
                Sheet.Cells[lin14,4].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind01').AsString;
                Sheet.Cells[lin14,5].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTForaEnsinoRegular').AsString;
                Sheet.Cells[lin14,6].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind22').AsString;

                //Indicador:
                // -> N�mero e percentual de adolescentes concluintes dos Cursos de Foma��o Inicial e Continuada
                //    Modulo I, Modulo II e Intensivo
                //INDICADOR 22
                Sheet.Cells[lin15,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin15,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod1').AsString;
                Sheet.Cells[lin15,3].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTMod2').AsString;
                Sheet.Cells[lin15,4].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTModIntensivo').AsString;
                Sheet.Cells[lin15,5].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod1').AsString;
                Sheet.Cells[lin15,6].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod1').AsString;
                Sheet.Cells[lin15,7].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesMod2').AsString;
                Sheet.Cells[lin15,8].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Mod2').AsString;
                Sheet.Cells[lin15,9].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesIntensivo').AsString;
                Sheet.Cells[lin15,10].Value  := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind25Intensivo').AsString;

                //Indicador:
                // -> N�mero e percentual de adolescentes concluintes do projeto Orienta��o Profissional
                // -> N�mero e percentual de adolescentes concluintes do projeto Orienta��o Educacional
                //INDICADORES 23 e 24
                Sheet.Cells[lin16,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin16,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOP').AsString;
                Sheet.Cells[lin16,3].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOP').AsString;
                Sheet.Cells[lin16,4].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind23').AsString;
                Sheet.Cells[lin16,5].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTOE').AsString;
                Sheet.Cells[lin16,6].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTConcluintesOE').AsString;
                Sheet.Cells[lin16,7].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind24').AsString;

                //Indicador:
                // -> Grau de aprova��o dos adolescentes no vestibulinho do CEPHAS/outras institui��es de ensino
                //INDICADOR 26
                Sheet.Cells[lin17,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin17,2].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInscritosVest').AsString;
                Sheet.Cells[lin17,3].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTRealizaramProva').AsString;
                Sheet.Cells[lin17,4].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin17,5].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAprovadosVestibulinho').AsString;
                Sheet.Cells[lin17,6].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;
                Sheet.Cells[lin17,7].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('').AsString;

                //Indicador:
                // -> N�mero e percentual de adolescentes aptos para a Aprendizagem Profissional
                // -> N�mero e percentual de adolescentes inseridos na Aprendizagem Profissional
                //INDICADORES 27 E 28
                Sheet.Cells[lin18,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin18,2].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTAptosAprendizagem').AsString;
                Sheet.Cells[lin18,3].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInseridosAprendizagem').AsString;
                Sheet.Cells[lin18,4].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind28').AsString;

                //Indicador:
                // -> N�mero e percentual de adolescentes inseridos na Aprendizagem Profissional
                // -> N�mero e percentual de aprendizes da FUNDHAS
                //INDICADORES 27 E 28
                Sheet.Cells[lin19,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin19,2].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInseridosAprendizagem').AsString;
                Sheet.Cells[lin19,3].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemFUNDHAS').AsString;
                Sheet.Cells[lin19,4].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind29').AsString;

                //Indicador:
                // -> N�mero e percentual de adolescentes inseridos na Aprendizagem Profissional
                // -> N�mero e percentual de aprendizes externos a FUNDHAS
                //INDICADORES 28 E 30
                Sheet.Cells[lin20,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin20,2].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTInseridosAprendizagem').AsString;
                Sheet.Cells[lin20,3].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTEmAprendizagemExterna').AsString;
                Sheet.Cells[lin20,4].Value   := '<--->';//dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('Ind30').AsString;

                //Indicador:
                // -> Grau de desempenho do aprendiz no campo pr�tico
                //INDICADOR 31
                Sheet.Cells[lin21,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;

                //Indicador:
                // -> Grau de desempenho do aprendiz no campo pr�tico
                //INDICADORES 32 E 33
                Sheet.Cells[lin22,1].Value   := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
                Sheet.Cells[lin22,2].Value   := dmDeca.cdsGeraDadosMAtrizSemestreAno.FieldByName ('TTPesquisasEmpresaResp').AsString;

              end;
            end;
            dmDeca.cdsSel_Unidade.Next;
            ProgressBar2.Position := ProgressBar2.Position + 1;

            lin   := lin + 1;
            lin2  := lin2 + 1;
            lin3  := lin3 + 1;
            lin4  := lin4 + 1;
            lin5  := lin5 + 1;
            lin6  := lin6 + 1;
            lin7  := lin7 + 1;
            lin8  := lin8 + 1;
            lin9  := lin9 + 1;
            lin10 := lin10 + 1;
            lin11 := lin11 + 1;
            lin12 := lin12 + 1;
            lin13 := lin13 + 1;
            lin14 := lin14 + 1;
            lin15 := lin15 + 1;
            lin16 := lin16 + 1;
            lin17 := lin17 + 1;
            lin18 := lin18 + 1;
            lin19 := lin19 + 1;
            lin20 := lin20 + 1;
            lin21 := lin21 + 1;
            lin22 := lin22 + 1;

          end;
        end; //with dmDeca.cdsSel_Unidade


      except
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Um erro ocorreu ao tentar selecionar as UNIDADES. Verifique a conex�o ou entre em contato com o Administrador do Sistema.',
                                '[Sistema Deca]-Erro gerando dados da Matriz',
                                MB_OK + MB_ICONWARNING);

        frmGeraEstatisticaMatriz.Close;

      end; //try - except

      lin   := 0;
      lin2  := 0;
      lin3  := 0;
      lin4  := 0;
      lin5  := 0;
      lin6  := 0;
      lin7  := 0;
      lin8  := 0;
      lin9  := 0;
      lin10 := 0;
      lin11 := 0;
      lin12 := 0;
      lin13 := 0;
      lin14 := 0;
      lin15 := 0;
      lin16 := 0;
      lin17 := 0;
      lin18 := 0;
      lin19 := 0;
      lin20 := 0;
      lin21 := 0;
      lin22 := 0;

      ProgressBar1.Position := ProgressBar1.Position + 1;

    end; //for mes := 1 to 12 to ...
    
    ProgressBar1.Position := 0;
    ProgressBar2.Position := 0;
    Label1.Caption := 'Planilha gerada...';
    Application.MessageBox ('Sua planilha com os dados lan�ados no ano informado foi gerada dentro da pasta do Sistema Deca(normalmente em C:\Deca).',
                            '[Sistema Deca]-Gerar dados estat�sticos da Matriz',
                            MB_OK + MB_ICONINFORMATION);

    objExcel.Workbooks[1].SaveAs (ExtractFilePath(Application.ExeName) + 'Matriz de Avaliacao FUNDHAS.xlsx');
    objExcel.Workbooks[1].Close;

  //end; //if (RadioGroup1.ItemIndex = 0) then //and (chkFundhas.Checked = False) then

end;  //procedure...

procedure TfrmGeraEstatisticaMatriz.FormDestroy(Sender: TObject);
begin
if not VarIsEmpty(objExcel) then
    objExcel.Quit;
end;

end.
