unit uTotaisUsuarioUnidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask, Db, TeEngine, Series,
  TeeProcs, Chart, DBChart, ComCtrls;

type
  TfrmTotaisUsuarioUnidade = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    mskData1: TMaskEdit;
    mskData2: TMaskEdit;
    btnPesquisar: TSpeedButton;
    btnLimpar: TSpeedButton;
    Bevel1: TBevel;
    btnRelatorioTotalUnidade: TSpeedButton;
    btnGrafico: TSpeedButton;
    dsTotaliza: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    dbgResultados: TDBGrid;
    btnRelatorioTotalUsuario: TSpeedButton;
    procedure mskData1Exit(Sender: TObject);
    procedure mskData2Exit(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnGraficoClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure btnRelatorioTotalUnidadeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTotaisUsuarioUnidade: TfrmTotaisUsuarioUnidade;

implementation

uses uDM, uUtil, uPrincipal, rTotalPorUnidade, uTotalRegistrosPorUnidade,
  rTotalRegistrosPorUsuarioUnidade;

{$R *.DFM}

procedure TfrmTotaisUsuarioUnidade.mskData1Exit(Sender: TObject);
begin
  try
    StrToDate(mskData1.Text);

    if StrToDate(mskData1.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskData1.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskData1.SetFocus;
    end;
  end;
end;

procedure TfrmTotaisUsuarioUnidade.mskData2Exit(Sender: TObject);
begin
  try
    StrToDate(mskData2.Text);

    if StrToDate(mskData2.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskData2.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskData2.SetFocus;
    end;
  end;
end;

procedure TfrmTotaisUsuarioUnidade.btnPesquisarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_TotalRegistrosUsuarioUnidade do
    begin
      Close;
      Params.ParamByName ('@pe_dat_inicial').Value := GetValue(mskData1, vtDate);
      Params.ParamByName ('@pe_dat_final').Value := GetValue(mskData2, vtDate);
      Open;
      dbgResultados.Refresh;

      PageControl1.ActivePageIndex := 0;
      
    end;
  except end;
end;

procedure TfrmTotaisUsuarioUnidade.btnGraficoClick(Sender: TObject);
begin
{  try
    //Carrega os dados da procedure spSel_TotalRegistrosUnidade para
    //a tabela GRAFICO

    //Esvazia a tabela GRAFICO de acordo com o usu�rio logado
    with dmDeca.cdsExc_DadosGrafico do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Execute;

      //Executa a procedure spSel_TotalRegistrosUnidade
      with dmDeca.cdsSel_TotalRegistrosUnidade do
      begin
        Close;
        Params.ParamByName ('@pe_data1').Value := GetValue(mskData1, vtDate);
        Params.ParamByName ('@pe_data2').Value := GetValue(mskData2, vtDate);
        Open;

        //Carrega os dados selecionados para a tabela GRAFICO
        while not dmDeca.cdsSel_TotalRegistrosUnidade.eof do
        begin
          //Insere os dados na tabela GRAFICO
          with dmDeca.cdsInc_DadosGrafico do
          begin
            Close;
            Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
            Params.ParamByName ('@pe_dsc_total').Value := dmDeca.cdsSel_TotalRegistrosUnidade.FieldByName ('nom_unidade').Value;
            Params.ParamByName ('@pe_num_total').Value := dmDeca.cdsSel_TotalRegistrosUnidade.FieldByName ('registros').Value;
            Execute;
          end;
          dmDeca.cdsSel_TotalRegistrosUnidade.Next;
        end;
      end;

      //Ap�s inseridos os dados na tabela gr�fico, seleciona os dados para o usu�rio
      //logado e exibe os dados ...
      with dmDeca.cdsSel_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Open;
      end;
    end;
  except end;
  PageControl1.ActivePageIndex := 1;
  }
end;

procedure TfrmTotaisUsuarioUnidade.btnRelatorioClick(Sender: TObject);
begin
    try
    with dmDeca.cdsSel_TotalRegistrosUsuarioUnidade do
    begin
      Close;
      Params.ParamByName ('@pe_dat_inicial').Value := GetValue(mskData1, vtDate);
      Params.ParamByName ('@pe_dat_final').Value := GetValue(mskData2, vtDate);
      Open;

      Application.CreateForm (TrelTotalRegistrosPorUsuarioUnidade, relTotalRegistrosPorUsuarioUnidade);
      relTotalRegistrosPorUsuarioUnidade.lbTituloPeriodo.Caption := 'Dados selecionados no per�odo entre ' + mskData1.Text + ' e ' + mskData2.Text;
      relTotalRegistrosPorUsuarioUnidade.lbTituloUsuario.Caption := 'Emitido pelo usu�rio : ' + vvNOM_USUARIO;
      relTotalRegistrosPorUsuarioUnidade.Preview;
      relTotalRegistrosPorUsuarioUnidade.Free;
    end;
  except end;
end;

procedure TfrmTotaisUsuarioUnidade.btnRelatorioTotalUnidadeClick(
  Sender: TObject);
begin
    try
    with dmDeca.cdsSel_TotalRegistrosUnidade do
    begin
      Close;
      Params.ParamByName ('@pe_data1').Value := GetValue(mskData1, vtDate);
      Params.ParamByName ('@pe_data2').Value := GetValue(mskData2, vtDate);
      Open;

      Application.CreateForm (TrelTotalRegistrosPorUnidade, relTotalRegistrosPorUnidade);
      relTotalRegistrosPorUnidade.lbTituloPeriodo.Caption := 'Dados selecionados no per�odo entre ' + mskData1.Text + ' e ' + mskData2.Text;
      relTotalRegistrosPorUnidade.lbTituloUsuario.Caption := 'Emitido pelo usu�rio : ' + vvNOM_USUARIO;
      relTotalRegistrosPorUnidade.Preview;
      relTotalRegistrosPorUnidade.Free;
    end;
  except end;
end;

end.
