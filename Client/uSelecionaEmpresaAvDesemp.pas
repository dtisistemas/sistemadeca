unit uSelecionaEmpresaAvDesemp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls;

type
  TfrmSelecionaEmpresaAvDesemp = class(TForm)
    cbEmpresas: TComboBox;
    lbSemestreAno: TLabel;
    btnVer: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaEmpresaAvDesemp: TfrmSelecionaEmpresaAvDesemp;

implementation

uses uDM, uNovaAvaliacaoAprendiz, rEstatisticoEmpresa;

{$R *.DFM}

procedure TfrmSelecionaEmpresaAvDesemp.FormShow(Sender: TObject);
begin

  cbEmpresas.Clear;
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value  := 44;
      Params.ParamByName ('@pe_num_secao').Value    := Null;
      Params.ParamByName ('@pe_flg_status').Value   := 0;  //Somente as ATIVAS
      Open;

      while not (dmDeca.cdsSel_Secao.eof) do
      begin
        cbEmpresas.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value);
        dmDeca.cdsSel_Secao.Next;
      end;

    end;
  except end;
end;

procedure TfrmSelecionaEmpresaAvDesemp.btnVerClick(Sender: TObject);
begin

  try
    with (dmDeca.cdsTotais_AvaDesemp_Por_Empresa) do
    begin
      Close;
      Params.ParamByName ('@Semestre').Value     :=  frmNovaAvaliacaoAprendiz.cbSemestreP.ItemIndex + 1;
      Params.ParamByName ('@Ano').Value          :=  StrToInt(frmNovaAvaliacaoAprendiz.mskAnoP.Text);
      Params.ParamByName ('@pe_num_secao').Value :=  Copy(cbEmpresas.Text, 1, 4);
      Open;

      Application.CreateForm (TrelEstatisticoEmpresa, relEstatisticoEmpresa);
      relEstatisticoEmpresa.lbEmpresa.Caption := 'EMPRESA: ' + cbEmpresas.Text;
      relEstatisticoEmpresa.lbPeriodo.Caption := 'PER�ODO: ' + lbSemestreAno.Caption;
      relEstatisticoEmpresa.Preview;
      relEstatisticoEmpresa.Free;

    end;

  except end;

end;

end.
