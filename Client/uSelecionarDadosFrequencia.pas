unit uSelecionarDadosFrequencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, jpeg, ExtCtrls, Buttons, StdCtrls, Grids, DBGrids, Db, Mask,
  TeeProcs, TeEngine, Chart, DBChart, Series, QRExport;

type
  TfrmSelecionarDadosFrequencia = class(TForm)
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    btnPesquisar: TSpeedButton;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    dsFrequencia: TDataSource;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    cbUnidade: TComboBox;
    chkTodas: TCheckBox;
    GroupBox5: TGroupBox;
    dsTotalizaFaltasAno: TDataSource;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    btnGerarGrafico: TSpeedButton;
    SpeedButton2: TSpeedButton;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    dsTotalizaFaltasUnidadesMesDRH: TDataSource;
    TabSheet5: TTabSheet;
    GroupBox4: TGroupBox;
    mskAnoE: TMaskEdit;
    GroupBox6: TGroupBox;
    cbMesE: TComboBox;
    btnPesquisarE: TSpeedButton;
    btnExportarE: TSpeedButton;
    TabSheet6: TTabSheet;
    GroupBox7: TGroupBox;
    cbUnidadeC: TComboBox;
    btnPesquisarC: TSpeedButton;
    Panel4: TPanel;
    DBGrid5: TDBGrid;
    dsCadastro: TDataSource;
    Panel5: TPanel;
    Label2: TLabel;
    edNumCartaoPasse: TMaskEdit;
    btnAlterarCartaoPasse: TSpeedButton;
    TabSheet8: TTabSheet;
    btnImportar: TSpeedButton;
    OpenDialog1: TOpenDialog;
    Memo1: TMemo;
    TabSheet9: TTabSheet;
    GroupBox10: TGroupBox;
    edCalendarioAtual: TEdit;
    GroupBox11: TGroupBox;
    dbgDiasUteis: TDBGrid;
    Panel8: TPanel;
    btnAlterar: TSpeedButton;
    btnGravar: TSpeedButton;
    cbMeses: TComboBox;
    mskAnoDiasUteis: TMaskEdit;
    mskDiasUteis: TMaskEdit;
    dsDiasUteisCalendario: TDataSource;
    btnContabilizar: TSpeedButton;
    chkBloqueado: TCheckBox;
    btnRelacaoSeguro: TSpeedButton;
    btnImprimir: TSpeedButton;
    Label5: TLabel;
    SpeedButton1: TSpeedButton;
    btnCarregarDiasUteisAno: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label6: TLabel;
    cbCotas: TComboBox;
    GroupBox9: TGroupBox;
    Label7: TLabel;
    mskMatricula: TMaskEdit;
    btnPesquisaRapida: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Label1: TLabel;
    edNome: TEdit;
    btnDemitidos: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnGerarGraficoClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnPesquisarEClick(Sender: TObject);
    procedure btnPesquisarCClick(Sender: TObject);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure btnExportarEClick(Sender: TObject);
    procedure btnConferenciaClick(Sender: TObject);
    procedure btnAlterarCartaoPasseClick(Sender: TObject);
    procedure DBGrid6CellClick(Column: TColumn);
    procedure btnImportarClick(Sender: TObject);
    procedure dbgDiasUteisDblClick(Sender: TObject);
    procedure btnContabilizarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnCarregarDiasUteisAnoClick(Sender: TObject);
    procedure btnRelacaoSeguroClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure btnPesquisaRapidaClick(Sender: TObject);
    procedure mskMatriculaEnter(Sender: TObject);
    procedure btnDemitidosClick(Sender: TObject);
    function CalcularIdadeSeguro(DatNascimento: TDateTime; DatFinal: TDateTime): String;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionarDadosFrequencia: TfrmSelecionarDadosFrequencia;
  vListaUnidade1, vListaUnidade2 : TStringList;
  vvDIAS_UTEIS, mmDU : Integer;
  strMES, strANO : String;

  mmDIAS_UTEIS_MES : Integer;

  filePASSES : TextFile;

implementation

uses uDM, uPrincipal, rFolhaFrequencia, rRelacaoValeTransporte, uUtil,
  rGraficoFaltasUnidadesMesPeriodo, rSeguro, rTotalizaSeguro,
  uFichaPesquisa, rDadosDesligados_EducaCenso, uFichaCrianca;

{$R *.DFM}

procedure TfrmSelecionarDadosFrequencia.FormShow(Sender: TObject);
begin

  if (vvIND_PERFIL in [2,3]) then   //Perfis que acessam os dados... 2-Gestor;3-Assistente Social;
  begin
    frmSelecionarDadosFrequencia.PageControl1.Pages[0].TabVisible := False;  //Confer�ncia dados Lan�ados
    frmSelecionarDadosFrequencia.PageControl1.Pages[1].TabVisible := False; //Gera��o de Arquivos
    frmSelecionarDadosFrequencia.PageControl1.Pages[2].TabVisible := True;  //Cadastro de Cart�o de Passe
    frmSelecionarDadosFrequencia.PageControl1.Pages[3].TabVisible := False; //Importa��es
    frmSelecionarDadosFrequencia.PageControl1.Pages[4].TabVisible := False;  //Resumo
    frmSelecionarDadosFrequencia.PageControl1.Pages[5].TabVisible := False; //Cadastro de Dias �teis
    //btnRelacaoParaValeTransporte.Visible := False;
  end
  else
  begin

  end;

  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;

  //Habilitar os bot�es de impress�o
  //btnRelacaoParaValeTransporte.Enabled := True;
  //btnRelacaoParaValeTransporte.Visible := True;
  btnImprimir.Enabled := True;

  //if vvIND_PERFIL in [1,8] then
  //  btnRelacaoParaValeTransporte.Visible := True
  //else
  //  btnRelacaoParaValeTransporte.Visible := False;

  //Inicializar as vari�veis de ambiente
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de unidades
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').AsInteger in [1,2,3,7,9,12,13,20,27,42,43,46,47,48,50,71,73,77,79,80]) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          cbUnidadeC.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end
      end
    end
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10+
                            'Um erro ocorreu ao tentar carregar a lista de unidades...' + #13+#10+
                            'Verifique a sua conex�o com a internet ou entre em contato com o Administrador do Sistema.',
                            '[Sistema Deca] - Erro carregando unidades',
                            MB_OK + MB_ICONERROR);
  end;

  //Verificar se o logou como Administrador ou DRH para exibir a lista de unidades
  //Caso perfil diferente, oculta a lista de unidades e utiliza o cod_unidade do login...
  if (vvIND_PERFIL in [1,8,20,23,24,25]) then
    GroupBox3.Visible := True
  else
    GroupBox3.Visible := False;

end;

procedure TfrmSelecionarDadosFrequencia.btnPesquisarClick(Sender: TObject);
begin
  //Realizar a consulta
  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').Value   := Null;
      Params.ParamByname ('@pe_cod_matricula').Value   := Null;

      if (vvIND_PERFIL in [1,8,20,23,24,25]) and (chkTodas.Checked) then  //Administrador e DRH
        Params.ParamByName ('@pe_cod_unidade').Value   := Null
      else if (vvIND_PERFIL in [1,8,20,23,24,25]) and (not (chkTodas.Checked)) then  //Administrador e DRH
        Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex])
      else
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;

      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_num_mes').Value         := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Params.ParamByName ('@pe_cod_turma').Value       := Null;
      Params.ParamByName ('@pe_num_cotas').Value       := Null;
      Open;

    end;
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10+
                            'Ocorreu um erro ao tentar realizar a consulta.',
                            '[Sistema Deca] - Erro de consulta',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmSelecionarDadosFrequencia.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm (TrelFolhaFrequencia, relFolhaFrequencia);
  //relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA'; // ->  ' + vvNOM_UNIDADE;
  if (chkTodas.Checked) then
    relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA -> <Todas as Unidades> '
  else
    relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA -> ' + cbUnidade.Text;

  relFolhaFrequencia.lbMesAno.Caption := cbMes.Text + '/' + mskAno.Text;
  relFolhaFrequencia.QRGroup1.Enabled := True;
  relFolhaFrequencia.Preview;
  relFolhaFrequencia.Free;
end;

procedure TfrmSelecionarDadosFrequencia.btnGerarGraficoClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_TotalizaFaltasAno do
    begin
      Close;
      if vvIND_PERFIL in [1,7] then
        Params.ParamByName ('@pe_cod_unidade').Value := 0
      else
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName ('@pe_num_ano').Value     := StrToInt(mskAno.Text);
      Open;

      DBGrid2.Refresh;
    end;
  except end;
end;

procedure TfrmSelecionarDadosFrequencia.SpeedButton2Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Totaliza_Faltas_Unidade_Mes_DRH do
    begin
      Close;
      Params.ParamByName ('@pe_num_ano').AsInteger := GetValue(mskAno.Text);
      Params.ParamByName ('@pe_num_mes').AsInteger := cbMes.ItemIndex + 1;
      Open;

      DBGrid3.Refresh;


      //Limpa os dados da tabela Grafico para o usuario logado
      with dmDeca.cdsExc_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;

      //Insere os dados na tabela Gr�fico
      dmDeca.cdsSel_Totaliza_Faltas_Unidade_Mes_DRH.First;
      while not dmDeca.cdsSel_Totaliza_Faltas_Unidade_Mes_DRH.eof do
      begin

        //Grava os dados na tabela...
        with dmDeca.cdsInc_DadosGrafico do
        begin
          Close;
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Params.ParamByName ('@pe_dsc_total').Value   := dmDeca.cdsSel_Totaliza_Faltas_Unidade_Mes_DRH.FieldByName ('nom_unidade').Value;
          Params.ParamByName ('@pe_num_total').Value   := dmDeca.cdsSel_Totaliza_Faltas_Unidade_Mes_DRH.FieldByName ('Faltas').Value;
          Execute;
          dmDeca.cdsSel_Totaliza_Faltas_Unidade_Mes_DRH.Next;
        end;

      end;

      //Chama o relat�rio com os dados gravados
      with dmDeca.cdsSel_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Open;

        Application.CreateForm (TrelGraficoFaltasUnidadesMesPeriodo, relGraficoFaltasUnidadesMesPeriodo);
        relGraficoFaltasUnidadesMesPeriodo.Preview;
        relGraficoFaltasUnidadesMesPeriodo.Free;
        
      end;
    end;
  except end;
end;

procedure TfrmSelecionarDadosFrequencia.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  //if vvIND_PERFIL in [1,8,20,23,24,25] then AllowChange := True else AllowChange := False;
end;

procedure TfrmSelecionarDadosFrequencia.btnPesquisarEClick(
  Sender: TObject);
begin
  //Realizar a consulta
  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').Value   := Null;
      Params.ParamByname ('@pe_cod_matricula').Value   := Null;
      Params.ParamByName ('@pe_cod_unidade').Value     := Null;
      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAnoE.Text);
      Params.ParamByName ('@pe_num_mes').Value         := cbMesE.ItemIndex + 1;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Params.ParamByName ('@pe_cod_turma').Value       := Null;
      Params.ParamByName ('@pe_num_cotas').Value       := Null;
      Open;
    end;
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10+
                            'Ocorreu um erro ao tentar realizar a consulta.',
                            '[Sistema Deca] - Erro de consulta',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmSelecionarDadosFrequencia.btnPesquisarCClick(
  Sender: TObject);
begin
  //Realizar a consulta
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidadeC.ItemIndex]);
      Open;
      DBGrid5.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10+
                            'Ocorreu um erro ao tentar realizar a consulta.',
                            '[Sistema Deca] - Erro de consulta',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmSelecionarDadosFrequencia.DBGrid5CellClick(Column: TColumn);
begin
  edNome.Text := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
  if (Length(dmDeca.cdsSel_Cadastro.FieldByName('num_cartao_passe').AsString) = 0) then
    edNumCartaoPasse.Text := '000000'
  else edNumCartaoPasse.Text := dmDeca.cdsSel_Cadastro.FieldByName('num_cartao_passe').Value;

  if (dmDeca.cdsSel_Cadastro.FieldByName('num_cota_passes').IsNull) then
    cbCotas.ItemIndex := 0
  else cbCotas.Text := FloatToStr(dmDeca.cdsSel_Cadastro.FieldByName('num_cota_passes').AsFloat);

  if dmDeca.cdsSel_Cadastro.FieldByName('passe_bloqueado').Value = 0 then chkBloqueado.Checked := False else chkBloqueado.Checked := True;

//  regATUAL := dsCadastro.DataSet.RecNo;  

end;

procedure TfrmSelecionarDadosFrequencia.btnExportarEClick(Sender: TObject);
var
  fileExporta : TextFile;
  mmDIAS_UTEIS, mmTOTAL_FALTAS, mmCOTAS_PASSE : Integer;
  mmVALOR_PASSAGEM, mmVALOR : Real;
  strVALOR : String;
begin
  //Recuperar a quantidade de dias �teis no m�s de refer�ncia
  with dmDeca.cdsSel_DiasUteisCalendario do
  begin
    Close;
    Params.ParamByName ('@pe_flg_atual').Value := 0;
    Params.ParamByName ('@pe_num_mes').Value   := cbMesE.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano').Value   := StrToInt(mskAnoE.Text);
    Open;
    mmDIAS_UTEIS := dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_dias_uteis').Value;
  end;

  //Recuperar o valor atual do passe
  with dmDeca.cdsSel_ValorPasse do
  begin
    Close;
    Params.ParamByName ('@pe_flg_atual').Value := 1;
    Open;
    mmVALOR_PASSAGEM := dmDeca.cdsSel_ValorPasse.FieldByName ('vr_passe').Value;
  end;

  AssignFile (fileExporta, ExtractFilePath(Application.ExeName) + 'CONSORCIO123.TXT');
  ReWrite (fileExporta);

  //Write (fileExporta, 'N.� CART�O PASSE');
  //Write (fileExporta, ';');
  //Writeln (fileExporta, 'CENTRO DE CUSTO');

  try
    with dmDeca.cdsSel_ValeTransporte do
    begin
      Close;
      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAnoE.Text);
      Params.ParamByName ('@pe_num_mes').Value         := cbMesE.ItemIndex + 1;
      Params.ParamByName ('@pe_num_faltas').Value      := 30;
      Params.ParamByName ('@pe_cota_passes').Value     := 0;
      Params.ParamByName ('@pe_dias_uteis').Value      := mmDIAS_UTEIS;
      Params.ParamByName ('@pe_passe_bloqueado').Value := 0; //Apenas passe "livre"
      Open;

      dmDeca.cdsSel_ValeTransporte.First;
      while not dmDeca.cdsSel_ValeTransporte.eof do
      begin
        Write (fileExporta, dmDeca.cdsSel_ValeTransporte.FieldByName ('num_cartao_passe').Value);
        Write (fileExporta, ';');
        mmVALOR := mmVALOR_PASSAGEM * StrToFloat(dmDeca.cdsSel_ValeTransporte.FieldByName ('liquido').AsString);
        //strVALOR := FloatToStr(mmVALOR);
        strVALOR := Format('%3.2f', [mmVALOR]);
        Writeln (fileExporta, strVALOR);
        //Writeln (fileExporta, ';');
        dmDeca.cdsSel_ValeTransporte.Next;
      end;

      CloseFile(fileExporta);

      Application.MessageBox ('Os dados referentes ao arquivo de CONS�RCIO foi gerado ' +#13+#10 +
                              'em arquivo na pasta do sistema com o nome - CONSORCIO.TXT',
                              '[Sistema Deca] - Exportar dados para o Cons�rcio',
                              MB_OK + MB_ICONINFORMATION);

    end;
  except end;

end;


procedure TfrmSelecionarDadosFrequencia.btnConferenciaClick(
  Sender: TObject);
begin
//C�D. SE��O;DESC.SE��O;MATRICULA;NOME;CPF;M�E;PAI;RUA;N�MERO;BAIRRO;CARTEIRINHA;SIT;ADM;NUM.VIAGENS;LINHA
end;

procedure TfrmSelecionarDadosFrequencia.btnAlterarCartaoPasseClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_CartaoPasse do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      Params.ParamByName ('@pe_num_cartao_passe').Value := GetValue(edNumCartaoPasse.Text);
      if chkBloqueado.Checked = True then
        Params.ParamByName ('@pe_passe_bloqueado').Value  := 1
      else
        Params.ParamByName ('@pe_passe_bloqueado').Value := 0;
      Params.ParamByName ('@pe_num_cota_passes').Value := StrToFloat(cbCotas.Text);
      Execute;
    end;

    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidadeC.ItemIndex]);
        Open;
        DBGrid5.Refresh;
      end;
    except
      Application.MessageBox ('Aten��o!!!' + #13+#10+
                              'Ocorreu um erro ao tentar realizar a consulta.',
                              '[Sistema Deca] - Erro de consulta',
                              MB_OK + MB_ICONWARNING);
    end;

  except end;
end;

procedure TfrmSelecionarDadosFrequencia.DBGrid6CellClick(Column: TColumn);
begin
//  edNomeCota.Text := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
//  if (dmDeca.cdsSel_Cadastro.FieldByName('num_cota_passes').IsNull) then
//    cbCotas.ItemIndex := 0
//  else cbCotas.Text := FloatToStr(dmDeca.cdsSel_Cadastro.FieldByName('num_cota_passes').AsFloat);
end;

procedure TfrmSelecionarDadosFrequencia.btnImportarClick(Sender: TObject);
var
  arqIMPORTA : TextFile;
  Linha      : String;
  vvCOD_MATRICULA, vvNUM_CARTEIRINHA, vvCOTAS_PASSE : String;


begin
  if OpenDialog1.Execute then
  begin
    AssignFile (arqIMPORTA, OpenDialog1.FileName);
    Reset (arqIMPORTA);
    Memo1.Clear;
    while not Eoln (arqIMPORTA) do
    begin
      Readln (arqIMPORTA, Linha);
      while not Eoln (arqIMPORTA) do
      begin
        vvCOD_MATRICULA   := '00' + Copy(Linha,1,6);
        vvNUM_CARTEIRINHA := Copy(Linha,8,6);
        vvCOTAS_PASSE     := Copy(Linha,15,1);
        //Memo1.Lines.Add(vvCOD_MATRICULA + ' - ' + vvNUM_CARTEIRINHA + '  -  ' + vvCOTAS_PASSE);

        //Alterar - incluir no banco de dados
        with dmDeca.cdsAlt_CartaoCotasPasses do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value     := vvCOD_MATRICULA;
          Params.ParamByName ('@pe_num_cota_passes').AsFloat := StrToFloat(vvCOTAS_PASSE);
          Params.ParamByName ('@pe_num_cartao_passe').Value  := vvNUM_CARTEIRINHA;
          Execute;
        end;

        Readln (arqIMPORTA, Linha);

      end;
    end;

    Application.MessageBox ('Aten��o !!! ' +#13+#10 +
                            'Todos os registros foram importados com sucesso.',
                            '[Sistema Deca] - Importa��o de Dados',
                            MB_OK + MB_ICONINFORMATION);
  end;

end;

procedure TfrmSelecionarDadosFrequencia.dbgDiasUteisDblClick(
  Sender: TObject);
begin
  cbMeses.ItemIndex    := (dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_mes').Value) - 1;
  mskAnoDiasUteis.Text := IntToStr(dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_ano').Value);
  mskDiasUteis.Text    := IntToStr(dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_dias_uteis').Value);
end;

procedure TfrmSelecionarDadosFrequencia.btnContabilizarClick(
  Sender: TObject);
var
  fileExporta : TextFile;
  mmDIAS_UTEIS, mmTOTAL_FALTAS, mmCOTAS_PASSE : Integer;
  mmVALOR_PASSAGEM, mmVALOR : Real;
  strVALOR : String;
begin
  //Recuperar a quantidade de dias �teis no m�s de refer�ncia
  with dmDeca.cdsSel_DiasUteisCalendario do
  begin
    Close;
    Params.ParamByName ('@pe_flg_atual').Value := 0;
    Params.ParamByName ('@pe_num_mes').Value   := cbMesE.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano').Value   := StrToInt(mskAnoE.Text);
    Open;
    mmDIAS_UTEIS := dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_dias_uteis').Value;
  end;

  //Recuperar o valor atual do passe
  with dmDeca.cdsSel_ValorPasse do
  begin
    Close;
    Params.ParamByName ('@pe_flg_atual').Value := 1;
    Open;
    mmVALOR_PASSAGEM := dmDeca.cdsSel_ValorPasse.FieldByName ('vr_passe').Value;
  end;

  AssignFile (fileExporta, ExtractFilePath(Application.ExeName) + 'CONTABILIZACAO.CSV');
  ReWrite (fileExporta);

  Write (fileExporta, 'N.� CART�O PASSE');
  Write (fileExporta, ';');
  Write (fileExporta, 'VALOR CR�DITO');
  Write (fileExporta, ';');
  Write (fileExporta, 'NOME DO ALUNO');
  Write (fileExporta, ';');
  Write (fileExporta, 'MATR�CULA');
  Write (fileExporta, ';');
  Writeln (fileExporta, 'CENTRO DE CUSTO');

  try
    with dmDeca.cdsSel_ValeTransporte do
    begin
      Close;
      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAnoE.Text);
      Params.ParamByName ('@pe_num_mes').Value         := cbMesE.ItemIndex + 1;
      Params.ParamByName ('@pe_num_faltas').Value      := 30;
      Params.ParamByName ('@pe_cota_passes').Value     := 0;
      Params.ParamByName ('@pe_dias_uteis').Value      := mmDIAS_UTEIS;
      Params.ParamByName ('@pe_passe_bloqueado').Value := 0;    //Apenas passe "livre"
      Open;

      dmDeca.cdsSel_ValeTransporte.First;
      while not dmDeca.cdsSel_ValeTransporte.eof do
      begin

        Write (fileExporta, dmDeca.cdsSel_ValeTransporte.FieldByName ('num_cartao_passe').Value);
        Write (fileExporta, ';');
        mmVALOR := mmVALOR_PASSAGEM * StrToFloat(dmDeca.cdsSel_ValeTransporte.FieldByName ('liquido').AsString);
        strVALOR := FloatToStr(mmVALOR);
        Write (fileExporta, strVALOR);
        Write (fileExporta, ';');
        Write (fileExporta, dmDeca.cdsSel_ValeTransporte.FieldByName ('nom_nome').Value);
        Write (fileExporta, ';');
        Write (fileExporta, dmDeca.cdsSel_ValeTransporte.FieldByName ('cod_matricula').Value);
        Write (fileExporta, ';');
        Write (fileExporta, '1.01.02.00' + Trim(dmDeca.cdsSel_ValeTransporte.FieldByName ('num_ccusto').Value) + '.0000');
        Writeln (fileExporta, ';');
        dmDeca.cdsSel_ValeTransporte.Next;
      end;

      CloseFile(fileExporta);

      Application.MessageBox ('Os dados referentes ao arquivo de CONTABILIZA��O foi gerado ' +#13+#10 +
                              'em arquivo na pasta do sistema com o nome - CONTABILIZACAO.CSV',
                              '[Sistema Deca] - Contabiliza��o',
                              MB_OK + MB_ICONINFORMATION);


    end;
  except end;
end;

procedure TfrmSelecionarDadosFrequencia.btnAlterarClick(Sender: TObject);
begin
  //Efetiva a altera��o do dia �til alterado no per�odo
  try
    with dmDeca.cdsUPD_DiasUteisMes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_du').Value      := dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('cod_id_du').Value;
      Params.ParamByName ('@pe_num_dias_uteis').Value := StrToInt(mskDiasUteis.Text);
      Execute;

      with dmDeca.cdsSel_DiasUteisCalendario do
      begin
        Close;
        Params.ParamByName ('@pe_flg_atual').Value := 0;
        Params.ParamByName ('@pe_num_mes').Value   := Null;
        Params.ParamByName ('@pe_num_ano').Value   := Null;
        Open;
        dbgDiasUteis.Refresh;

        cbMeses.ItemIndex := -1;
        mskAnoDiasUteis.Clear;
        mskDiasUteis.Clear;

      end;

    end;
  except end;
end;

procedure TfrmSelecionarDadosFrequencia.SpeedButton1Click(Sender: TObject);
begin
  //Procedimento alterado a pedido da DRH em 03/04/2018

  if (Application.MessageBox (PChar('Deseja emitir a RELA��O DE PASSES referente ao per�odo ' + cbMes.Text + '/' + mskAno.Text + ' ?'),
                                    '[Sistema Deca] - Rela��o de Passe',
                                    MB_YESNO + MB_ICONWARNING) ) = idYes then
  begin
    with dmDeca.cdsSel_SelecionaDadosPassesEscolares do
    begin
      Close;
      Params.ParamByName ('@pe_num_mes').Value := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value := Trim(mskAno.Text);
      Open;
    end;
  end;

  AssignFile (filePASSES, ExtractFilePath(Application.ExeName) + 'PASSES.CSV');
  ReWrite (filePASSES);

  Write (filePASSES, 'TOTAL FALTAS');
  Write (filePASSES, ';');
  Write (filePASSES, 'UNIDADE');
  Write (filePASSES, ';');
  Write (filePASSES, 'CENTRO CUSTO');
  Write (filePASSES, ';');
  Write (filePASSES, 'MATRICULA');
  Write (filePASSES, ';');
  Write (filePASSES, 'NOME');
  Write (filePASSES, ';');
  Write (filePASSES, 'SITUACAO CADASTRO');
  Write (filePASSES, ';');
  Write (filePASSES, 'ENDERE�O');
  Write (filePASSES, ';');
  Write (filePASSES, 'COTAS');
  Write (filePASSES, ';');
  Write (filePASSES, 'DATA ADMISSAO');
  Write (filePASSES, ';');
  Write (filePASSES, 'CPF ALUNO');
  Write (filePASSES, ';');
  Writeln (filePASSES, 'SITUACAO CARTEIRINHA');

  try
    while not(dmDeca.cdsSel_SelecionaDadosPassesEscolares.eof) do
    begin
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('TOTAL_FALTAS').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('NOM_UNIDADE').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('NUM_CCUSTO').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('COD_MATRICULA').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('NOM_NOME').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('vStatusCadastro').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('NOM_ENDERECO').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('NUM_COTA_PASSES').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('ADMISS�O').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('NUM_CPF').Value);
      Write (filePASSES, ';');
      Write (filePASSES, dmDeca.cdsSel_SelecionaDadosPassesEscolares.FieldByName ('vStatusPasse').Value);
      Writeln (filePASSES, ';');
      dmDeca.cdsSel_SelecionaDadosPassesEscolares.Next;
    end;

    ShowMessage ('Gera��o do arquivo de PASSES finalizado!!!');
    CloseFile(filePASSES);

  except end;

  {
  //Recuperar os dias �teis no M�s
  try
    //Recuperar a quantidade de dias �teis no m�s de refer�ncia
    with dmDeca.cdsSel_DiasUteisCalendario do
    begin
      Close;
      Params.ParamByName ('@pe_flg_atual').Value := 0;
      Params.ParamByName ('@pe_num_mes').Value   := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value   := StrToInt(mskAno.Text);
      Open;
      mmDIAS_UTEIS_MES := dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_dias_uteis').Value;
    end;

    //Zerar os dados do usu�rio atual na tabela TMP_PASSES
    with dmDeca.cdsExc_TmpPasses do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;
    end;

    //Consultar os dados de cadastro, frequ�ncia, fazer os c�lculos e inser�-los no banco de dados para exibir o relat�rio...
    //Faltas diferentes de 30 e cotas de passe maiores de 0
    with dmDeca.cdsSel_ValeTransporte do
    begin
      Close;
      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_num_mes').Value         := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_num_faltas').Value      := 30;
      Params.ParamByName ('@pe_cota_passes').Value     := 0;
      Params.ParamByName ('@pe_dias_uteis').Value      := mmDIAS_UTEIS_MES;
      Params.ParamByName ('@pe_passe_bloqueado').Value := 0;  //Apenas os passes "livres"
      Open;

      while not dmDeca.cdsSel_ValeTransporte.eof do
      begin

        //Incluir os dados na tabela TMP_PASSES
        with dmDeca.cdsIns_TmpPasses do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_ValeTransporte.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_nom_nome').Value         := dmDeca.cdsSel_ValeTransporte.FieldByName ('nom_nome').Value;
          Params.ParamByName ('@pe_num_cotas').Value        := dmDeca.cdsSel_ValeTransporte.FieldByName ('num_cota_passes').Value;
          Params.ParamByName ('@pe_num_faltas').Value       := dmDeca.cdsSel_ValeTransporte.FieldByName ('num_faltas').Value;
          Params.ParamByName ('@pe_num_justificadas').Value := dmDeca.cdsSel_ValeTransporte.FieldByName ('num_justificadas').Value;
          Params.ParamByName ('@pe_dias_uteis').Value       := mmDIAS_UTEIS_MES;
          Params.ParamByName ('@pe_bruto').Value            := dmDeca.cdsSel_ValeTransporte.FieldByName ('BRUTO').Value;
          if dmDeca.cdsSel_ValeTransporte.FieldByName ('num_faltas').Value = 30 then Params.ParamByName ('@pe_liquido').Value := 0
          else Params.ParamByName ('@pe_liquido').Value     := dmDeca.cdsSel_ValeTransporte.FieldByName ('LIQUIDO').Value;
          Params.ParamByName ('@pe_periodo').Value          := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('dsc_periodo').AsString);
          Params.ParamByName ('@pe_nom_unidade').Value      := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('nom_unidade').AsString);
          Params.ParamByName ('@pe_cod_usuario').Value      := vvCOD_USUARIO;
          Params.ParamByName ('@pe_nascimento').Value       := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('nascimento').AsString);
          Params.ParamByName ('@pe_admissao').Value         := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('admissao').AsString);
          Params.ParamByName ('@pe_idade').Value            := dmDeca.cdsSel_ValeTransporte.FieldByName ('idade').AsInteger;
          Params.ParamByName ('@pe_cpf').Value              := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('num_cpf').AsString);
          Params.ParamByName ('@pe_num_ccusto').Value       := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('num_ccusto').AsString);
          Params.ParamByName ('@pe_passe_bloqueado').Value  := dmDeca.cdsSel_ValeTransporte.FieldByName ('vPasseBloqueado').Value;
          Execute;
        end;

        dmDeca.cdsSel_ValeTransporte.Next;

      end;

    end;
  except
    Application.MessageBox ('Um ERRO ocorreu na gera��o dos dados de c�lculo dos passes escolares',
                            '[Sistema Deca] - C�lculo de Passes Escolares',
                            MB_OK + MB_ICONWARNING);
  end;

  //Exibe o relat�rio...
  with dmDeca.cdsSel_RelacaoVT do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value     := vvCOD_USUARIO;
    Params.ParamByName ('@pe_passe_bloqueado').Value := Null; //Apenas os passes "livres"
    Open;

    Application.CreateForm (TrelRelacaoValeTransporte, relRelacaoValeTransporte);
    relRelacaoValeTransporte.Preview;
    relRelacaoValeTransporte.Free;

  end;
  }
end;

procedure TfrmSelecionarDadosFrequencia.btnCarregarDiasUteisAnoClick(Sender: TObject);
begin
  //Carregar os dados de Calend�rios na Aba - Cadastro de Dias �teis
  with dmDeca.cdsSel_Calendario do
  begin
    Close;
    Params.ParamByName ('@pe_cod_calendario').Value := Null;
    Params.ParamByName ('@pe_dsc_calendario').Value := Null;
    Params.ParamByName ('@pe_flg_atual').Value      := 0;
    Open;

    edCalendarioAtual.Text := dmDeca.cdsSel_Calendario.FieldByName ('dsc_calendario').Value;

    with dmDeca.cdsSel_DiasUteisCalendario do
    begin
      Close;
      Params.ParamByName ('@pe_flg_atual').Value := 0;
      Params.ParamByName ('@pe_num_mes').Value   := Null;
      Params.ParamByName ('@pe_num_ano').Value   := Null;
      Open;
      dbgDiasUteis.Refresh;
    end;


  end;
end;

procedure TfrmSelecionarDadosFrequencia.btnRelacaoSeguroClick(
  Sender: TObject);
var fileEXPSEGURO : TextFile;
    mmDATA_REFERENCIA : String;
begin

  repeat
    mmDATA_REFERENCIA := InputBox ('[Data refer�ncia para o c�lculo da IDADE]','Informe a data refer�ncia para o c�lculo da IDADE','00/00/0000');
  until mmDATA_REFERENCIA <> '';

  AssignFile (fileEXPSEGURO, ExtractFilePath(Application.ExeName) + 'SEGURO.CSV');
  ReWrite (fileEXPSEGURO);

  Write (fileEXPSEGURO, 'CENTRO DE CUSTO');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'UNIDADE');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'MATR�CULA');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'NOME COMPLETO');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'NASCIMENTO');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'DATA DE ADMISS�O');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'N�MERO DO CPF');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'IDADE');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'CPF RESP.');
  Write (fileEXPSEGURO, ';');
  Writeln (fileEXPSEGURO, 'NOME RESP.');

  with dmDeca.cdsSel_SelecionaDadosSeguro do
  begin
    Close;
    Params.ParamByName ('@pe_num_mes').Value        := cbMes.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano').Value        := Trim(mskAno.Text);
    Params.ParamByName ('@pe_dat_referencia').Value := StrToDate(mmDATA_REFERENCIA);
    Open;

    while not (dmDeca.cdsSel_SelecionaDadosSeguro.eof) do
    begin

      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('NUM_CCUSTO').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('NOM_UNIDADE').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('COD_MATRICULA').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('NOM_NOME').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('NASCIMENTO').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('ADMISS�O').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('NUM_CPF').Value);
      Write (fileEXPSEGURO, ';');
      //Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('IDADE').Value);
      Write (fileEXPSEGURO, CalcularIdadeSeguro(dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('NASCIMENTO').AsDateTime, StrToDate(mmDATA_REFERENCIA)) );
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('RESPONSAVEL').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguro.FieldByName ('CPF_RESPONSAVEL').Value);
      Writeln (fileEXPSEGURO, ';');

      dmDeca.cdsSel_SelecionaDadosSeguro.Next;

    end;
  end;


  //Incluir no arquivo os desligados no mesmo per�odo
  with dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS do
  begin
    Close;
    Params.ParamByName ('@pe_num_mes').Value        := cbMes.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano').Value        := Trim(mskAno.Text);
    Open;

    while not (dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.eof) do
    begin

      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('NUM_CCUSTO').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('NOM_UNIDADE').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('COD_MATRICULA').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('NOM_NOME').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('NASCIMENTO').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('ADMISS�O').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('NUM_CPF').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, CalcularIdadeSeguro(dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('NASCIMENTO').AsDateTime, StrToDate(mmDATA_REFERENCIA)) );
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('RESPONSAVEL').Value);
      Write (fileEXPSEGURO, ';');
      Write (fileEXPSEGURO, dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.FieldByName ('CPF_RESPONSAVEL').Value);
      Writeln (fileEXPSEGURO, ';');

      dmDeca.cdsSel_SelecionaDadosSeguroDESLIGADOS.Next;

    end;
  end;

  ShowMessage ('Processo finalizado!!!');
  CloseFile(fileEXPSEGURO);

  {
  //Recuperar a quantidade de dias �teis no m�s de refer�ncia
  with dmDeca.cdsSel_DiasUteisCalendario do
  begin
    Close;
    Params.ParamByName ('@pe_flg_atual').Value := 0;
    Params.ParamByName ('@pe_num_mes').Value   := cbMes.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano').Value   := StrToInt(mskAno.Text);
    Open;
    mmDIAS_UTEIS_MES := dmDeca.cdsSel_DiasUteisCalendario.FieldByName ('num_dias_uteis').Value;
  end;

  //Zerar os dados do usu�rio atual na tabela TMP_PASSES
  with dmDeca.cdsExc_TmpPasses do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
    Execute;
  end;

  //Consultar os dados de cadastro, frequ�ncia, fazer os c�lculos e inser�-los no banco de dados para exibir o relat�rio...
  //Faltas diferentes de 30 e cotas de passe maiores de 0
  with dmDeca.cdsSel_ValeTransporte do
  begin
    Close;
    Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAno.Text);
    Params.ParamByName ('@pe_num_mes').Value         := cbMes.ItemIndex + 1;
    Params.ParamByName ('@pe_num_faltas').Value      := Null;
    Params.ParamByName ('@pe_cota_passes').Value     := Null;
    Params.ParamByName ('@pe_dias_uteis').Value      := mmDIAS_UTEIS_MES;
    Params.ParamByName ('@pe_passe_bloqueado').Value := Null;  //Todos os passes (livres e bloqueados)
    Open;

    dmDeca.cdsSel_ValeTransporte.First;
    while not (dmDeca.cdsSel_ValeTransporte.eof) do
    begin

      //Incluir os dados na tabel TMP_PASSES
      with dmDeca.cdsIns_TmpPasses do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_ValeTransporte.FieldByName ('cod_matricula').Value;
        Params.ParamByName ('@pe_nom_nome').Value         := dmDeca.cdsSel_ValeTransporte.FieldByName ('nom_nome').Value;
        Params.ParamByName ('@pe_num_cotas').Value        := dmDeca.cdsSel_ValeTransporte.FieldByName ('num_cota_passes').Value;
        Params.ParamByName ('@pe_num_faltas').Value       := dmDeca.cdsSel_ValeTransporte.FieldByName ('num_faltas').Value;
        Params.ParamByName ('@pe_num_justificadas').Value := dmDeca.cdsSel_ValeTransporte.FieldByName ('num_justificadas').Value;
        Params.ParamByName ('@pe_dias_uteis').Value       := mmDIAS_UTEIS_MES;
        Params.ParamByName ('@pe_bruto').Value            := dmDeca.cdsSel_ValeTransporte.FieldByName ('BRUTO').Value;
        if dmDeca.cdsSel_ValeTransporte.FieldByName ('num_faltas').Value = 30 then Params.ParamByName ('@pe_liquido').Value := 0
        else Params.ParamByName ('@pe_liquido').Value     := dmDeca.cdsSel_ValeTransporte.FieldByName ('LIQUIDO').Value;
        Params.ParamByName ('@pe_periodo').Value          := dmDeca.cdsSel_ValeTransporte.FieldByName ('dsc_periodo').Value;
        Params.ParamByName ('@pe_nom_unidade').Value      := dmDeca.cdsSel_ValeTransporte.FieldByName ('nom_unidade').Value;
        Params.ParamByName ('@pe_cod_usuario').Value      := vvCOD_USUARIO;
        Params.ParamByName ('@pe_nascimento').Value       := dmDeca.cdsSel_ValeTransporte.FieldByName ('nascimento').Value;
        Params.ParamByName ('@pe_admissao').Value         := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('admissao').AsString);
        Params.ParamByName ('@pe_idade').Value            := dmDeca.cdsSel_ValeTransporte.FieldByName ('idade').AsInteger;
        Params.ParamByName ('@pe_cpf').Value              := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('num_cpf').AsString);
        Params.ParamByName ('@pe_num_ccusto').Value       := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('num_ccusto').AsString);
        Params.ParamByName ('@pe_passe_bloqueado').Value  := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('vPasseBloqueado').AsString);
        Params.ParamByName ('@pe_num_cpf_responsavel').Value  := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('NUM_CPF_RESPONSAVEL').AsString);
        Params.ParamByName ('@pe_nom_responsavel').Value  := GetValue(dmDeca.cdsSel_ValeTransporte.FieldByName ('NOM_RESPONSAVEL').AsString);
        Execute;
      end;

      dmDeca.cdsSel_ValeTransporte.Next;

    end;

  end;

  //Executar a procedure para a totaliza��o dos dados das Unidades para o Seguro
  Application.CreateForm (TrelSeguro, relSeguro);
  //Gerar a rela��o de crian�as para seguro...
  with dmDeca.cdsSel_RelacaoSeguro do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Open;
  end;

  relSeguro.qrlREFERENCIA.Caption := 'REFER�NCIA : ' + cbMes.Text + '/' + mskAno.Text;
  //relSeguro.ExportToFilter(TQRAsciiExportFilter.Create('c:\teste.txt'));  //Exportando para XLS
  relSeguro.Preview;
  relSeguro.Free;

  //Ap�s gerar o arquivo em tela, criar o CSV...
  AssignFile (fileEXPSEGURO, ExtractFilePath(Application.ExeName) + 'SEGURO.CSV');
  ReWrite (fileEXPSEGURO);

  Write (fileEXPSEGURO, 'CENTRO DE CUSTO');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'MATR�CULA');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'NOME COMPLETO');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'NASCIMENTO');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'DATA DE ADMISS�O');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'N�MERO DO CPF');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'IDADE');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'CAPITAL');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'COEFICIENTE');
  Write (fileEXPSEGURO, ';');
  Write (fileEXPSEGURO, 'CPF RESP.');
  Write (fileEXPSEGURO, ';');
  Writeln (fileEXPSEGURO, 'NOME RESP.');

  try
    with dmDeca.cdsSel_RelacaoVT do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value     := vvCOD_USUARIO;
      Params.ParamByname ('@pe_passe_bloqueado').Value := Null;
      Open;

      //dmDeca.cdsSel_RelacaoVT.First;
      //while not dmDeca.cdsSel_RelacaoVT.eof do
      dmDeca.cdsSel_RelacaoSeguro.First;
      while not (dmDeca.cdsSel_RelacaoSeguro.eof) do
      begin

        Write (fileEXPSEGURO, '1.01.02.00' + dmDeca.cdsSel_RelacaoSeguro.FieldByName ('num_ccusto').Value + '.0000');
        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('cod_matricula').Value);
        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('nom_nome').Value);
        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('nascimento').Value);
        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('admissao').Value);
        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('cpf').Value);
        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('idade').Value);
        Write (fileEXPSEGURO, ';');

        if ( dmDeca.cdsSel_RelacaoSeguro.FieldByName ('idade').Value <= 13 ) then
        begin
          Write (fileEXPSEGURO, 'R$ 1.000,00');
          Write (fileEXPSEGURO, ';');
          Write (fileEXPSEGURO, '0,1867');
          //Writeln (fileEXPSEGURO, ';');
        end
        else if ( dmDeca.cdsSel_RelacaoSeguro.FieldByName ('idade').Value > 13 ) then
        begin
          Write (fileEXPSEGURO, 'R$ 3.000,00');
          Write (fileEXPSEGURO, ';');
          Write (fileEXPSEGURO, '0,2505');
          //Writeln (fileEXPSEGURO, ';');
        end;

        Write (fileEXPSEGURO, ';');
        Write (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('NUM_CPF_RESPONSAVEL').AsString);
        Write (fileEXPSEGURO, ';');
        Writeln (fileEXPSEGURO, dmDeca.cdsSel_RelacaoSeguro.FieldByName ('nom_responsavel').AsString);

        dmDeca.cdsSel_RelacaoSeguro.Next;

      end;

      Application.MessageBox ('Os dados referentes ao seguro tamb�m foram gerados ' +#13+#10 +
                              'em arquivo CSV na pasta do sistema com o nome - SEGURO.CSV',
                              '[Sistema Deca] - Exportar Dados de Seguro',
                              MB_OK + MB_ICONINFORMATION);
      CloseFile(fileEXPSEGURO);

    end;
  except end;

  {
  //Carregar o totalizador do Seguro por idade
  //Carregar os alunos menores de 14 anos...
  with dmDeca.cdsSel_TotalizaSeguro do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 'C';
    Open;
    Application.CreateForm (TrelTotalizaSeguro, relTotalizaSeguro);
    relTotalizaSeguro.qrlREFERENCIA.Caption := 'REFER�NCIA : ' + cbMes.Text + '/' + mskAno.Text  +  ' - ' + 'Coeficiente: 0,1867';
    relTotalizaSeguro.Preview;
    relTotalizaSeguro.Free;
  end;

  //Carregar os alunos maiores de 14 anos...
  with dmDeca.cdsSel_TotalizaSeguro do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 'A';
    Open;
    Application.CreateForm (TrelTotalizaSeguro, relTotalizaSeguro);
    relTotalizaSeguro.qrlREFERENCIA.Caption := 'REFER�NCIA : ' + cbMes.Text + '/' + mskAno.Text  +  ' - ' + 'Coeficiente: 0,2505';
    relTotalizaSeguro.Preview;
    relTotalizaSeguro.Free;
  end;
  }


end;

procedure TfrmSelecionarDadosFrequencia.SpeedButton3Click(Sender: TObject);
var fileGERAL : TextFile;
    strNOM_FAM, strCPF_FAM : String;
begin
  AssignFile (fileGERAL, ExtractFilePath(Application.ExeName) + 'GERAL.CSV');
  ReWrite (fileGERAL);

  Write (fileGERAL, 'CENTRO DE CUSTO REDUZIDO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'UNIDADE');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'MATR�CULA');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'NOME COMPLETO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'NASCIMENTO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'SITUA��O CADASTRO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'DATA DE ADMISS�O');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'N�MERO DO CPF ALUNO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'NOME DO RESPONS�VEL LEGAL');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'N.� CPF DO RESPONS�VEL LEGAL');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'ENDERE�O');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'COMPLEMENTO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'BAIRRO');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'N�MERO DO CART�O DE PASSE');
  Write (fileGERAL, ';');
  Write (fileGERAL, 'STATUS DO CART�O DE PASSE');
  Write (fileGERAL, ';');
  Writeln (fileGERAL, 'COTAS DE PASSE');

  try
    with dmDeca.cdsSel_SelecionaDadosGeralPasses do
    begin
      Close;
      Params.ParamByName ('@pe_passe_bloqueado').Value := Null;
      Open;

      dmDeca.cdsSel_SelecionaDadosGeralPasses.First;
      while not dmDeca.cdsSel_SelecionaDadosGeralPasses.eof do
      begin
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_ccusto').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('nom_unidade').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('cod_matricula').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('nom_nome').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('nascimento').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('vStatus').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('admissao').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_cpf').Value);
        Write (fileGERAL, ';');

        //Verifica se o CPF do respons�vel � v�lido
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_cod_familiar').Value  := Null;
          Open;
        end;

        //Se encontrou, verifica qual membro da familia tem CPF cadastrado
        if (dmDeca.cdsSel_Cadastro_Familia.RecordCount < 1) then
        begin
          Write (fileGERAL, '<Informa��o n�o cadastrada>');
          Write (fileGERAL, ';');
          Write (fileGERAL, '<Informa��o n�o cadastrada>');
          Write (fileGERAL, ';');
          dmDeca.cdsSel_Cadastro_Familia.Next;
        end
        else
        begin
          while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
          begin
            if ( (dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_cpf_fam').IsNull) or (dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_cpf_fam').Value = '000.000.000-00') or
                 (dmDeca.cdsSel_Cadastro_Familia.FieldByName('ind_responsavel').Value = 0) ) then
            //if (dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_cpf_fam').IsNull) then
            begin
              //Write (fileGERAL, '<Informa��o n�o cadastrada>');
              //Write (fileGERAL, ';');
              //Write (fileGERAL, '<Informa��o n�o cadastrada>');
              //Write (fileGERAL, ';');
              //dmDeca.cdsSel_Cadastro_Familia.Last;
              strNOM_FAM := '<Informa��o n�o cadastrada>';
              strCPF_FAM := '<Informa��o n�o cadastrada>';
            end
            else
            begin
              //Write (fileGERAL, dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value);
              //Write (fileGERAL, ';');
              //Write (fileGERAL, dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value);
              //Write (fileGERAL, ';');
              strNOM_FAM := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
              strCPF_FAM := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value;
              dmDeca.cdsSel_Cadastro_Familia.Last;  //Move para o final da pesquisa, pois j� encontrou CPF v�lido
            end;
            dmDeca.cdsSel_Cadastro_Familia.Next;
          end;

          Write (fileGERAL, strNOM_FAM);
          Write (fileGERAL, ';');
          Write (fileGERAL, strCPF_FAM);
          Write (fileGERAL, ';');

        end;

        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('nom_endereco').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('nom_complemento').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('nom_bairro').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_cartao_passe').AsString);
        Write (fileGERAL, ';');
        Write (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('vStatusCartaoPasse').AsString);
        Write (fileGERAL, ';');
        Writeln (fileGERAL, dmDeca.cdsSel_SelecionaDadosGeralPasses.FieldByName ('num_cota_passes').AsInteger);

        dmDeca.cdsSel_SelecionaDadosGeralPasses.Next;

      end;

      Application.MessageBox ('Aten��o !!! ' +#13+#10 +
                              'O arquivo com os dados solicitados foi gerado na pasta do Sistema Deca. Verifique a exist�ncia do arquivo GERAL.CSV, abra-o e verifique os dados.',
                              '[Sistema Deca] - Dados gerais de passes/DRH',
                              MB_OK + MB_ICONINFORMATION);
      CloseFile(fileGERAL);
    end
  except end;

end;

procedure TfrmSelecionarDadosFrequencia.SpeedButton4Click(Sender: TObject);
begin
  mskMatricula.Clear;
  btnPesquisarC.Click;
end;

procedure TfrmSelecionarDadosFrequencia.btnPesquisaRapidaClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(mskMatricula.Text);
      Params.ParamByName ('@pe_cod_unidade').Value   := 0; //StrToInt(vListaUnidade1.Strings[cbUnidadeC.ItemIndex]);
      Open;
      Dbgrid5.Refresh;
    end;
  except end;
end;

procedure TfrmSelecionarDadosFrequencia.mskMatriculaEnter(Sender: TObject);
begin
  mskMatricula.Clear;
end;

procedure TfrmSelecionarDadosFrequencia.btnDemitidosClick(Sender: TObject);
var fileDESLIGADOS: TextFile;
begin
  //Application.CreateForm(TrelDadosDeligados_EducaCenso, relDadosDeligados_EducaCenso);
  try
    with dmDeca.cdsSel_DadosEducaSenso do
    begin
      Close;
      Params.ParamByName ('@pe_tipoDado').Value := 1;
      Params.ParamByName ('@pe_num_mes').Value  := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value  := mskAno.Text;
      Open;

      //Gerar o arquivo CSV para manipula��o DRH
      AssignFile (fileDESLIGADOS, ExtractFilePath(Application.ExeName) + 'DESLIGADOS.CSV');
      ReWrite (fileDESLIGADOS);

      Write (fileDESLIGADOS, 'RG ESCOLAR');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'MATR�CULA');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'NOME');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'C. CUSTO');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'UNIDADE');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'NASCIMENTO');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'ADMISS�O');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'PER�ODO');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'DESLIGAMENTO');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'MOTIVO');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'CPF ALUNO');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'CPF FAMILIAR');
      Write (fileDESLIGADOS, ';');
      Write (fileDESLIGADOS, 'NOME FAMILIAR');
      Writeln(fileDESLIGADOS, ';');

      while not (dmDeca.cdsSel_DadosEducaSenso.eof) do
      begin
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('dsc_rg_escolar').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('cod_matricula').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('nom_nome').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('num_ccusto').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('nom_unidade').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('nascimento').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('admissao').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('dsc_periodo').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('desligado').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('vMotivo').Value);
        Write (fileDESLIGADOS, ';');
        Write (fileDESLIGADOS, dmDeca.cdsSel_DadosEducaSenso.FieldByName ('num_cpf').Value);
        Write (fileDESLIGADOS, ';');

        //Verifica se o CPF do respons�vel � v�lido
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_DadosEducaSenso.FieldByName ('cod_matricula').AsString;
          Params.ParamByName ('@pe_cod_familiar').Value  := Null;
          Open;
        end;

        //Se encontrou, verifica qual membro da familia tem CPF cadastrado
        if (dmDeca.cdsSel_Cadastro_Familia.RecordCount < 1) then
        begin

        end
        else
        begin
          while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
          begin
            if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').IsNull) or ((dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value = '000.000.000-00')) then
            begin

            end
            else
            begin
              Write (fileDESLIGADOS, dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value);
              Write (fileDESLIGADOS, ';');
              Write (fileDESLIGADOS, dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value);
              //Write(fileDESLIGADOS, ';');
              dmDeca.cdsSel_Cadastro_Familia.Last;  //Move para o final da pesquisa, pois j� encontrou CPF v�lido
            end;

            dmDeca.cdsSel_Cadastro_Familia.Next;

          end;
        end; 
        Writeln(fileDESLIGADOS, ';');
        dmDeca.cdsSel_DadosEducaSenso.Next;
      end;

      CloseFile(fileDESLIGADOS);

      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'Um arquivo chamado DESLIGADOS.CSV foi criado na pasta do sistema.',
                              '[Sistema Deca] - Rela��o de Demitidos',
                              MB_OK + MB_ICONINFORMATION);
      
    end;
  except end;

end;

function TfrmSelecionarDadosFrequencia.CalcularIdadeSeguro(DatNascimento,
  DatFinal: TDateTime): String;
var
  periodo                   : Integer;
  intContAnos, intContMeses : Integer;
  mmDAT_FINAL : TDateTime;

begin

  mmDAT_FINAL := DatFinal;

  //C�lculo de Anos
  intContAnos := 0;
  periodo     := 12;

  Repeat
    Inc(intContAnos);
    DatFinal := IncMonth(DatFinal, periodo * -1);
  Until DatFinal < DatNascimento;

  Inc(intContAnos, -1);

  //C�lculo de meses
  intContMeses := 0;
  periodo      := 1;
  DatFinal     := mmDAT_FINAL;
  Repeat
    Inc(intContMeses);
    DatFinal := IncMonth(DatFinal, periodo * -1);
  Until DatFinal < DatNascimento;

  inc(intContMeses, -1);
  intContMeses := intContMeses mod 12;

  if intContAnos <= 9 then
    Result := '0' + IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses'
  else
    Result := IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses';

end;

end.
