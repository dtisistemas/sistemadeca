unit uDesligamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls;

type
  TfrmDesligamento = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    btnPesquisar: TSpeedButton;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    txtData: TMaskEdit;
    Label3: TLabel;
    rgMotivo2: TRadioGroup;
    Label2: TLabel;
    txtFuncao: TEdit;
    GroupBox3: TGroupBox;
    meMotivoDesligamento: TMemo;
    Label4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo : String);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDesligamento: TfrmDesligamento;
  numDIA : Integer;
  var mmMES_ATUAL, mmMES_ANTERIOR : Integer;

implementation

uses uFichaPesquisa, uPrincipal, uDM, uUtil, rDesligamento,
  rNovoDesligamento, uFichaCadastroModelo;

{$R *.DFM}

procedure TfrmDesligamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  mmINTEGRACAO := False;
end;

procedure TfrmDesligamento.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmDesligamento.Close;
  end;

end;

procedure TfrmDesligamento.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmDesligamento.btnSairClick(Sender: TObject);
begin
    frmDesligamento.Close;
end;

procedure TfrmDesligamento.AtualizaCamposBotoes(Modo : String);
begin
  // atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtFuncao.Clear;
    rgMotivo2.ItemIndex := -1;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtData.Enabled := False;
    txtFuncao.Enabled := False;
    rgMotivo2.Enabled := False;
    meMotivoDesligamento.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtData.Text := DateToSTr(Date());
    txtFuncao.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtFuncao.Enabled := True;
    rgMotivo2.Enabled := True;
    meMotivoDesligamento.Enabled := True;
    meMotivoDesligamento.Lines.Clear;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;

    txtMatricula.SetFocus;
  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;

    btnImprimir.SetFocus;
  end

end;

procedure TfrmDesligamento.btnSalvarClick(Sender: TObject);
begin

if Application.MessageBox ('Confirma o DESLIGAMENTO da crian�a/adolescente ? ',
        'Desligamento de Crian�a/Adolescente',
        MB_ICONQUESTION + MB_YESNO) = id_Yes then
  begin

    if (Length(Trim(txtMatricula.Text)) > 0) and (lbNome.Caption <> 'Ficha n�o cadastrada') then
    begin
    try

      if (rgMotivo2.ItemIndex > -1) then
      begin
        // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
        if StatusCadastro(txtMatricula.Text) = 'Ativo' then
        begin
          with dmDeca.cdsInc_Cadastro_Historico do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').AsString       := txtMatricula.Text;
            //Params.ParamByName('@pe_cod_unidade').AsInteger      := vvCOD_UNIDADE;
            Params.ParamByName('@pe_cod_unidade').AsInteger        := dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('cod_unidade').Value;
            Params.ParamByName('@pe_dat_historico').Value          := GetValue(txtData, vtDate);
            Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 5;  //C�digo para DESLIGAMENTO
            Params.ParamByName('@pe_dsc_tipo_historico').AsString  := 'Desligamento';
            Params.ParamByName('@pe_ind_motivo1').Value            := Null;//IntToStr(RGmOTIVO1.ItemIndex);
            Params.ParamByName('@pe_ind_motivo2').AsString         := IntToStr(rgMotivo2.ItemIndex);
            Params.ParamByName('@pe_log_cod_usuario').AsInteger    := vvCOD_USUARIO;
            Params.ParamByName('@pe_dsc_ocorrencia').Value         := GetValue(meMotivoDesligamento.Text);
            Execute;

            //Atualizar STATUS para "2" Desligado
            with dmDeca.cdsAlt_Cadastro_Status do
            begin
                Close;
                Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Params.ParamByName('@pe_ind_status').Value := 2;
                Execute;
            end;

            //"Transfere" para "Maioridade" ou "Inativos"
            with dmDeca.cdsAlt_Cadastro_Status_Transf do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := txtMatricula.Text;

              //Se o motivo do desligamento for por maioridade, transfere para a unidade MAIORIDADE
              if (rgMotivo2.ItemIndex = 0) then
              begin
                Params.ParamByName('@pe_cod_unidade').AsInteger := 47;
                Params.ParamByName('@pe_ind_status').AsInteger  := 1;   //Inativos ou Maioridade
                Params.ParamByName('@pe_num_secao').Value       := '0000';
              end
              //Se o motivo do desligamento for T�RMINO DE CONTRATO, ABANDONO, A PEDIDO ou JUSTA CAUSA, transfere para unidade INATIVOS
              else if (rgMotivo2.ItemIndex > 0)then
              begin
                Params.ParamByName('@pe_cod_unidade').AsInteger := 48;
                Params.ParamByName('@pe_ind_status').AsInteger  := 1;   //Inativos ou Maioridade
                Params.ParamByName('@pe_num_secao').Value       := '0000';
              end;

              Execute;

              //"Zera" a turma no Desligamento
              with dmDeca.cdsAlt_Turma_PosTransferencia do
              begin
                Close;
                Params.ParamByName ('@pe_cod_turma').Value := Null;
                Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Execute;
              end;

              //Atualizar os dados do educacenso para o padr�o para n�o deixar "em branco"
              try
                with dmDeca.cdsUpd_TurmaCIE_Cadastro do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_matricula').Value           := Trim(txtMatricula.Text);
                    Params.ParamByName ('@pe_num_turma_cei').Value           := '999.999.999';
                    Params.ParamByName ('@pe_dat_cadastro_educacenso').Value := Date();
                    Execute;
                  end;
              except end;

            AtualizaCamposBotoes('Salvar');
            
            end;                                                   

          end;

          frmPrincipal.AtualizaStatusBarUnidade;

        end
        else
        begin
          ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                      'Esta(e) crian�a/adolescente encontra-se ' +
                      StatusCadastro(txtMatricula.Text));
          AtualizaCamposBotoes('Padrao');
        end;


      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                 'Favor selecionar "MOTIVOS DO DESLIGAMENTO"');

      end

    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'DES_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                         Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtData.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Desligamento');//Descricao do Tipo de Historico
      Write   (log_file, ' ');
      Write   (log_file, meMotivoDesligamento.Text);
      Write   (log_file, ' ');

      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);


        //ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10 +#13+#10 +
        //            'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
else
  begin
    ShowMessage ('Opera��o Cancelada pelo USU�RIO...');
    AtualizaCamposBotoes ('Padr�o');
  end;

end;
end;

procedure TfrmDesligamento.btnPesquisarClick(Sender: TObject);

begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;

        //Verificar a quantidade de faltas no m�s atual
        meMotivoDesligamento.Clear;

        mmMES_ATUAL    := StrToInt(Copy(txtData.Text,4,2));
        mmMES_ANTERIOR := mmMES_ATUAL - 1;

        if mmMES_ATUAL = 1 then mmMES_ANTERIOR := 12;



        with dmDeca.cdsSel_Cadastro_Frequencia do
        begin
          Close;
          Params.ParamByName('@pe_id_frequencia').Value   := Null;
          Params.ParamByName('@pe_cod_matricula').Value   := Trim(txtMatricula.Text);
          Params.ParamByName('@pe_cod_unidade').Value     := Null;
          Params.ParamByName('@pe_num_ano').AsInteger     := StrToInt(Copy(txtData.Text,7,4));
          Params.ParamByName('@pe_num_mes').AsInteger     := mmMES_ATUAL; //StrToInt(Copy(txtData.Text,4,2));
          Params.ParamByName('@pe_cod_turma').Value       := Null;
          Params.ParamByName('@pe_log_cod_usuario').Value := Null;
          Params.ParamByName('@pe_num_cotas').Value       := Null;
          Open;

          if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0 then
          begin
            meMotivoDesligamento.Lines.Add('*************************************************************');
            meMotivoDesligamento.Lines.Add('Aten��o !!!');
            meMotivoDesligamento.Lines.Add('O aluno apresenta ' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_faltas').Value) +
                                           ' faltas no per�odo de ' + IntToStr(mmMES_ATUAL) + '/' + Copy(txtData.Text,7,4));
          end;

        end;

        //Verificar a quantidade de faltas no m�s anterior
        with dmDeca.cdsSel_Cadastro_Frequencia do
        begin
          Close;
          Params.ParamByName('@pe_id_frequencia').Value   := Null;
          Params.ParamByName('@pe_cod_matricula').Value   := Trim(txtMatricula.Text);
          Params.ParamByName('@pe_cod_unidade').Value     := Null;
          Params.ParamByName('@pe_num_ano').AsInteger     := (StrToInt(Copy(txtData.Text,7,4)));
          Params.ParamByName('@pe_num_mes').AsInteger     := mmMES_ANTERIOR;
          Params.ParamByName('@pe_cod_turma').Value       := Null;
          Params.ParamByName('@pe_log_cod_usuario').Value := Null;
          Params.ParamByName('@pe_num_cotas').Value       := Null;
          Open;

          if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0 then
          begin
            meMotivoDesligamento.Lines.Add('O aluno apresenta ' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_faltas').Value) +
                                           ' faltas no per�odo de ' + IntToStr(mmMES_ANTERIOR) + '/' + Copy(txtData.Text,7,4));
            meMotivoDesligamento.Lines.Add('*************************************************************');
          end;

        end;

      end;
    except end;
  end

  else

    AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmDesligamento.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');

  TRadioButton(rgMotivo2.Controls[0]).Font.Color := clBlack;
  TRadioButton(rgMotivo2.Controls[1]).Font.Color := clBlack;
  TRadioButton(rgMotivo2.Controls[2]).Font.Color := clBlack;
  TRadioButton(rgMotivo2.Controls[3]).Font.Color := clBlack;
  TRadioButton(rgMotivo2.Controls[4]).Font.Color := clBlack;
  TRadioButton(rgMotivo2.Controls[5]).Font.Color := clBlack;
  TRadioButton(rgMotivo2.Controls[6]).Font.Color := clRed;
  TRadioButton(rgMotivo2.Controls[7]).Font.Color := clRed;
  TRadioButton(rgMotivo2.Controls[8]).Font.Color := clRed;
  TRadioButton(rgMotivo2.Controls[9]).Font.Color := clRed;

end;

procedure TfrmDesligamento.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmDesligamento.FormShow(Sender: TObject);
begin
  if (mmINTEGRACAO = False) then
    AtualizaCamposBotoes('Padrao')
  else if (mmINTEGRACAO = True) then
  begin
    AtualizaCamposBotoes('Novo');
    txtMatricula.Text := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
    lbNome.Caption    := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;

    //Verificar a quantidade de faltas no m�s atual
    meMotivoDesligamento.Clear;

    mmMES_ATUAL    := StrToInt(Copy(txtData.Text,4,2));
    mmMES_ANTERIOR := mmMES_ATUAL - 1;

    if mmMES_ATUAL = 1 then mmMES_ANTERIOR := 12;

    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName('@pe_id_frequencia').Value   := Null;
      Params.ParamByName('@pe_cod_matricula').Value   := Trim(txtMatricula.Text);
      Params.ParamByName('@pe_cod_unidade').Value     := Null;
      Params.ParamByName('@pe_num_ano').AsInteger     := StrToInt(Copy(txtData.Text,7,4));
      Params.ParamByName('@pe_num_mes').AsInteger     := mmMES_ATUAL; //StrToInt(Copy(txtData.Text,4,2));
      Params.ParamByName('@pe_cod_turma').Value       := Null;
      Params.ParamByName('@pe_log_cod_usuario').Value := Null;
      Params.ParamByName('@pe_num_cotas').Value       := Null;
      Open;

      if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0 then
      begin
        meMotivoDesligamento.Lines.Add('*************************************************************');
        meMotivoDesligamento.Lines.Add('Aten��o !!!');
        meMotivoDesligamento.Lines.Add('O aluno apresenta ' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_faltas').Value) +
                                       ' faltas no per�odo de ' + IntToStr(mmMES_ATUAL) + '/' + Copy(txtData.Text,7,4));
      end;

    end;


    //Verificar a quantidade de faltas no m�s anterior
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName('@pe_id_frequencia').Value   := Null;
      Params.ParamByName('@pe_cod_matricula').Value   := Trim(txtMatricula.Text);
      Params.ParamByName('@pe_cod_unidade').Value     := Null;
      Params.ParamByName('@pe_num_ano').AsInteger     := (StrToInt(Copy(txtData.Text,7,4)));
      Params.ParamByName('@pe_num_mes').AsInteger     := mmMES_ANTERIOR;
      Params.ParamByName('@pe_cod_turma').Value       := Null;
      Params.ParamByName('@pe_log_cod_usuario').Value := Null;
      Params.ParamByName('@pe_num_cotas').Value       := Null;
      Open;

      if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0 then
      begin
        meMotivoDesligamento.Lines.Add('O aluno apresenta ' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_faltas').Value) +
                                       ' faltas no per�odo de ' + IntToStr(mmMES_ANTERIOR) + '/' + Copy(txtData.Text,7,4));
        meMotivoDesligamento.Lines.Add('*************************************************************');
      end;
    end;

    AtualizaCamposBotoes ('Padr�o');

  end;
end;


procedure TfrmDesligamento.txtDataExit(Sender: TObject);
begin
  // testa se a data � v�lida
  try
    StrToDate(txtData.Text);

    if StrToDate(txtData.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtData.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtData.SetFocus;
    end;
  end;

end;

procedure TfrmDesligamento.btnImprimirClick(Sender: TObject);
var vvMes : Integer;
begin
  try
    Application.CreateForm (TrelNovoDesligamento, relNovoDesligamento);
    relNovoDesligamento.lbMatricula1.Caption := txtMatricula.Text;
    relNovoDesligamento.lbMatricula2.Caption := txtMatricula.Text;
    relNovoDesligamento.lblProntuario1.Caption := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_prontuario').AsInteger);
    relNovoDesligamento.lblProntuario2.Caption := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_prontuario').AsInteger);
    relNovoDesligamento.lblProjeto1.Caption := Divisao(txtMatricula.Text);
    relNovoDesligamento.lblProjeto2.Caption := Divisao(txtMatricula.Text);
    relNovoDesligamento.lblNome1.Caption := lbNome.Caption;
    relNovoDesligamento.lblNome2.Caption := lbNome.Caption;
    relNovoDesligamento.lblLocal1.Caption := vvNOM_UNIDADE;
    relNovoDesligamento.lblLocal2.Caption := vvNOM_UNIDADE;

    relNovoDesligamento.lbDataDesligamento1.Caption := txtData.Text;
    relNovoDesligamento.lbDataDesligamento2.Caption := txtData.Text;

    relNovoDesligamento.lbMotivoDesligamento1.Lines.Add (meMotivoDesligamento.Text);
    relNovoDesligamento.lbMotivoDesligamento2.Lines.Add (meMotivoDesligamento.Text);
    case rgMotivo2.itemIndex of
      0: begin
           relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR MAIORIDADE';
           relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR MAIORIDADE';
         end;

      1: begin
           relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR T�RMINO DE CONTRATO';
           relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR T�RMINO DE CONTRATO';
         end;

      2: begin
           relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR ABANDONO';
           relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR ABANDONO';
         end;

      3: begin
           relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO A PEDIDO';
           relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO A PEDIDO';
         end;

      4: begin
           relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR JUSTA CAUSA';
           relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR JUSTA CAUSA';
         end;

      5: begin
           relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR MORTE';
           relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR MORTE';
         end;

      //Inclu�do em 09/12/2015 a Pedido da Divis�o de Empregabilidade  MEMORANDO CA 47/2016
      6: begin
          relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESEMPENHO INSUFICIENTE OU INADAPTA��O DO APRENDIZ (TEORIA/PR�TICA)';
          relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESEMPENHO INSUFICIENTE OU INADAPTA��O DO APRENDIZ (TEORIA/PR�TICA)';
         end;

      7: begin
          relNovoDesligamento.lbTipoDesligamento1.Caption := 'FALTA DISCIPLINAR GRAVE (ART. 482 CLT)';
          relNovoDesligamento.lbTipoDesligamento2.Caption := 'FALTA DISCIPLINAR GRAVE (ART. 482 CLT)';
         end;

      8: begin
          relNovoDesligamento.lbTipoDesligamento1.Caption := 'AUS�NCIA INJUSTIFICADA � ESCOLA QUE IMPLIQUE NA PERDA DO ANO LETIVO';
          relNovoDesligamento.lbTipoDesligamento2.Caption := 'AUS�NCIA INJUSTIFICADA � ESCOLA QUE IMPLIQUE NA PERDA DO ANO LETIVO';
         end;

      9: begin
          relNovoDesligamento.lbTipoDesligamento1.Caption := 'FREQU�NCIA INFERIOR A 75% NA APRENDIZAGEM';
          relNovoDesligamento.lbTipoDesligamento2.Caption := 'FREQU�NCIA INFERIOR A 75% NA APRENDIZAGEM';
         end;
      
    end;

    relNovoDesligamento.Preview;
    relNovoDesligamento.Free;

    AtualizaCamposBotoes ('Padr�o');

  except end;
end;

end.
