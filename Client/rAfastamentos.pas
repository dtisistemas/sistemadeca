unit rAfastamentos;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelAfastamentos = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRLabel1: TQRLabel;
    lbUnidade: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    lbPeriodo: TQRLabel;
    QRLabel5: TQRLabel;
    DetailBand1: TQRBand;
    QRShape6: TQRShape;
    QRGroup1: TQRGroup;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText7: TQRDBText;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
  private

  public

  end;

var
  relAfastamentos: TrelAfastamentos;

implementation

uses uDM;

{$R *.DFM}

end.
