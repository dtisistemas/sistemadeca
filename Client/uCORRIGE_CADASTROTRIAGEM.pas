unit uCORRIGE_CADASTROTRIAGEM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, Grids, DBGrids, StdCtrls, Db;

type
  TfrmCORRIGE_CADASTROTRIAGEM = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    dbgCADASTRO_FULL: TDBGrid;
    btnINICIAR_CORRECAO: TSpeedButton;
    dsCORRIGE_CADASTROTRIAGEM: TDataSource;
    medRESULTADO_ALTERACAO: TMemo;
    btnGERAR_TXT: TSpeedButton;
    procedure btnINICIAR_CORRECAOClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCORRIGE_CADASTROTRIAGEM: TfrmCORRIGE_CADASTROTRIAGEM;

implementation

uses uDM, udmTriagemDeca, uPrincipal;

{$R *.DFM}

procedure TfrmCORRIGE_CADASTROTRIAGEM.btnINICIAR_CORRECAOClick(
  Sender: TObject);
var mmNUM_CEP_CORRIGIDO, mmNUM_CPF_CORRIGIDO : String;
begin
  if Application.MessageBox ('Deseja iniciar o processo de CORRE��O e CPF e CEP do cadastro da Triagem?',
                             '[Sistema Deca] - Corre��o de Dados',
                             MB_OK + MB_ICONQUESTION) = idYes then
  begin
{
    //Se o CEP n�o tiver tamanho de 8 caracteres ou for "nulo" ->preenche com zero(s) ao final da string e grava em texto como incosistente
    if dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').IsNull then
      mmNUM_CEP_CORRIGIDO := '00000000'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 7) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '0'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 6) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '00'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 5) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '000'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 4) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '0000'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 3) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '00000'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 2) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '000000'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 1) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '0000000'
    else if ( Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value = 0) ) then
      mmNUM_CEP_CORRIGIDO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').Value + '00000000';


    //Verifica se o CPF tem 14 caracteres
    if dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_cpf').IsNull then
      mmNUM_CPF_CORRIGIDO := '000.000.000-00'
    else if ( Length() =  )


 }








  end;
end;

procedure TfrmCORRIGE_CADASTROTRIAGEM.FormShow(Sender: TObject);
begin
  //Carrega os dados do cadastro para leitura...
  try
    with dmTriagemDeca.cdsSel_CadastroFull do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value      := Null;
      Params.ParamByName ('@pe_num_inscricao').Value      := Null;
      Params.ParamByName ('@pe_num_inscricao_digt').Value := Null;
      Open;
      dbgCADASTRO_FULL.Refresh;
    end;
  except end;
end;

end.
