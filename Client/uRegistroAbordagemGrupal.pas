unit uRegistroAbordagemGrupal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, Mask, ComCtrls, ExtCtrls;

type
  TfrmRegistroAbordagemGrupal = class(TForm)
    btnSair: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    rgClassificacao: TRadioGroup;
    Label2: TLabel;
    clbRelacaoUnidade: TCheckListBox;
    GroupBox5: TGroupBox;
    meDataAtendimento: TMaskEdit;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    GroupBox6: TGroupBox;
    meConteudo: TMemo;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    BitBtn1: TBitBtn;
    btnDesmarcar: TBitBtn;
    TabSheet1: TTabSheet;
    lbUsuarioUnidade: TLabel;
    BitBtn2: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnSalvarXXClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure btnDesmarcarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroAbordagemGrupal: TfrmRegistroAbordagemGrupal;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmRegistroAbordagemGrupal.FormShow(Sender: TObject);
begin
  clbRelacaoUnidade.Clear;
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := vVCOD_UNIDADE;
      Open;

      while not eof do
      begin
        clbRelacaoUnidade.Items.Add(dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value);
        Next;
      end;
    end;
  except end;

  lbUsuarioUnidade.Caption :=  vvNOM_USUARIO + ' / ' + vvNOM_UNIDADE;

end;

procedure TfrmRegistroAbordagemGrupal.btnSalvarXXClick(Sender: TObject);
var x : Integer;
begin

end;

procedure TfrmRegistroAbordagemGrupal.btnSairClick(Sender: TObject);
begin
  frmRegistroAbordagemGrupal.Close;
end;

procedure TfrmRegistroAbordagemGrupal.BitBtn2Click(Sender: TObject);
var x : Integer;
begin
  //Carrega os nomes selecionados no CheckListBox para o MEMO (teste)
  meConteudo.Clear;
  for x := 0 to clbRelacaoUnidade.Items.Count - 1 do
  begin
    if clbRelacaoUnidade.Checked[x] = True then
      meConteudo.Lines.Add(clbRelacaoUnidade.Items.Strings[x]);
  end;
end;

procedure TfrmRegistroAbordagemGrupal.btnDesmarcarClick(Sender: TObject);
var n : Integer;
begin
  for n := 0 to clbRelacaoUnidade.Items.Count - 1 do
    clbRelacaoUnidade.Checked[n] := False;   
end;

procedure TfrmRegistroAbordagemGrupal.btnSalvarClick(Sender: TObject);
begin
  //Valida os campos do formul�rio
  // * Classificac��o de Atendimento
  // * Data do Atendimento
  // * Pelo menos um adolescente selecionado
  // * Descri��o de Conte�do

  if (rgClassificacao.ItemIndex > -1) and (Length(meDataAtendimento.Text)=10) and (Length(meConteudo.Text) > 0) then
  begin
    //Se os campos estiverem validados permite a grava��o dos dados e prepara a tela para novo lan�amento

    try
      //Faz a inclus�o dos dados de apenas UM REGISTRO na tabela INDICADORES
      with dmDeca.cdsInc_Indicador do
      begin
        Close;
        //Params.ParamByName('@pe_cod_id_classificacao').Value := StrToInt(vListaClassificacao1.Strings[cbClassificacao.ItemIndex]);
        Params.ParamByName('@pe_cod_id_classificacao').Value := 1; //Abordagem Grupal

        case rgClassificacao.ItemIndex of
          0: Params.ParamByName('@pe_ind_tipo_classificacao').Value := 0; //Crian�a
          1: Params.ParamByName('@pe_ind_tipo_classificacao').Value := 1; //Adolescente
          2: Params.ParamByName('@pe_ind_tipo_classificacao').Value := 2; //Fam�lia
          3: Params.ParamByName('@pe_ind_tipo_classificacao').Value := 2; //Fam�lia
          4: Params.ParamByName('@pe_ind_tipo_classificacao').Value := 2; //Fam�lia
        end;

        Params.ParamByName('@pe_dat_registro').Value := GetValue(meDataAtendimento, vtDate);
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName('@pe_dsc_ocorrencia').Value := Null;
        Execute;
      end;

      //Grava o conte�do para os prontu�rios selecionados

      //Grava uma registro de atendimento para cadas prontu�rio
      // "EFETUADO REGISTRO DE ABORDAGEM GRUPAL NESTA DATA. PARA MAIS DETALHES, CONSULTE A EVOLU��O DO SERVI�O SOCIAL"


    except end;


  end;
end;

end.
