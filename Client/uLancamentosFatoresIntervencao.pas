unit uLancamentosFatoresIntervencao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, CheckLst, ComCtrls, Grids, DBGrids, Db,
  ImgList;

type
  TfrmLancamentoFatoresIntervencao = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    edMatricula: TMaskEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    cbFator: TComboBox;
    Label4: TLabel;
    meMeta: TMemo;
    edDataEntrevista: TMaskEdit;
    chkIncluir: TCheckBox;
    ImageList1: TImageList;
    dbgFamilia: TDBGrid;
    dsCadastroFamilia: TDataSource;
    btnLancarItens: TBitBtn;
    Label5: TLabel;
    MaskEdit1: TMaskEdit;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnSelecionarTudoClick(Sender: TObject);
    procedure btnDesmarcarTudoClick(Sender: TObject);
    procedure AtualizaCamposBotoesFat_Interv (Modo : String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnLancarItensClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLancamentoFatoresIntervencao: TfrmLancamentoFatoresIntervencao;

implementation

uses uDM, uFichaPesquisa, uPrincipal, uUtil, uLancamentoItensFatores;

{$R *.DFM}

procedure TfrmLancamentoFatoresIntervencao.btnPesquisarClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    edMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := edMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        edMatricula.Text := FieldByName('cod_matricula').AsString;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        edDataEntrevista.SetFocus;
        edDataEntrevista.Text := DateToStr(Date());

        //Carrega a rela��o de composi��o familiar
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := edMatricula.Text;
          Params.ParamByName('@pe_cod_familiar').Value := Null;
          Open;

          dbgFamilia.DataSource := dsCadastroFamilia;

          chkIncluir.Checked := True;

          //clbFamiliares.Clear;
          //clbFamiliares.Items.Add(dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_nome').AsString);
          //while not dmDeca.cdsSel_Cadastro_Familia.eof do
          //begin
          //  clbFamiliares.Items.Add(FieldByName('nom_familiar').AsString);
          //  Next;
          //end;
        end;

        with dmDeca.cdsSel_Fatores_Intervencao do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_fator').Value := Null;
          Params.ParamByName('@pe_dsc_fator').Value := Null;
          Open;

          cbFator.Clear;
          while not eof do
          begin
            cbFator.Items.Add (FieldByName('dsc_fator').Value);
            Next;
          end;
        end;
      end;
    except end;
  end;
end;

procedure TfrmLancamentoFatoresIntervencao.btnSairClick(Sender: TObject);
begin
  frmLancamentoFatoresIntervencao.Close;
end;

procedure TfrmLancamentoFatoresIntervencao.btnSelecionarTudoClick(
  Sender: TObject);
var x : Integer;
begin

end;

procedure TfrmLancamentoFatoresIntervencao.btnDesmarcarTudoClick(
  Sender: TObject);
var i : Integer;
begin

end;

procedure TfrmLancamentoFatoresIntervencao.AtualizaCamposBotoesFat_Interv(
  Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    edMatricula.Clear;
    lbNome.Caption := '';
    GroupBox1.Enabled := False;

    //clbFamiliares.Clear;
    edDataEntrevista.Clear;
    dbgFamilia.DataSource := nil;
    GroupBox2.Enabled := False;

    edDataEntrevista.Clear;
    cbFator.ItemIndex := -1;
    meMeta.Clear;
    chkIncluir.Checked := False;
    GroupBox3.Enabled := False;

    btnLancarItens.Enabled := True;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    edMatricula.Clear;
    lbNome.Caption := '';

    GroupBox2.Enabled := True;
    //clbFamiliares.Clear;
    edDataEntrevista.Clear;

    GroupBox3.Enabled := True;

    btnLancarItens.Enabled := False;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmLancamentoFatoresIntervencao.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoesFat_Interv ('Novo');
end;

procedure TfrmLancamentoFatoresIntervencao.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCamposBotoesFat_Interv ('Padr�o');
end;

procedure TfrmLancamentoFatoresIntervencao.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoesFat_Interv ('Padr�o');
  dbgFamilia.DataSource := nil;
end;

procedure TfrmLancamentoFatoresIntervencao.btnSalvarClick(Sender: TObject);
var x: Integer;
    s: String;
begin
  //Validar a data da entrevista, fator e a meta ao fator.

  if (edDataEntrevista.Text = '  /  /    ') or (cbFator.ItemIndex < 0) then
  begin
    ShowMessage ('Aten��o!!! Verifique campos que n�o foram preenchidos corretamente...');
  end
  else
  begin
    //Efetua a inser��o dos dados no banco
    //Para cada "familiar" selecionado, grava a data de entrevista, a meta e o fator

    if (dbgFamilia.SelectedRows.Count > 0) then
    begin
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        for x:= 0 to (dbgFamilia.SelectedRows.Count - 1) do
        begin
          GotoBookmark(Pointer(dbgFamilia.SelectedRows.Items[x]));

          with dmDeca.cdsInc_Lanc_Fat_Int do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Familia.Fields[0].AsString;
            Params.ParamByName('@pe_ind_vinculo').Value := dmDeca.cdsSel_Cadastro_Familia.Fields[21].AsString;
            Params.ParamByName('@pe_nom_nome').Value := dmDeca.cdsSel_Cadastro_Familia.Fields[4].AsString;
            Params.ParamByName('@pe_dat_entrevista').Value := GetValue(edDataEntrevista, vtDate);
            Params.ParamByName('@pe_dsc_meta').Value := Trim(meMeta.Text);
            Params.ParamByName('@pe_cod_fator').Value := cbFator.ItemIndex + 1;
            Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Params.ParamByName('@pe_flg_status').Value := 0;  //Em atendimento
            Execute;
          end;
        end;

        if (chkIncluir.Checked = True) then
        begin
          with dmDeca.cdsInc_Lanc_Fat_Int do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := Trim(edMatricula.Text);
            Params.ParamByName('@pe_ind_vinculo').Value := 'PPO';
            Params.ParamByName('@pe_nom_nome').Value := lbNome.Caption;
            Params.ParamByName('@pe_dat_entrevista').Value := GetValue(edDataEntrevista, vtDate);
            Params.ParamByName('@pe_dsc_meta').Value := Trim(meMeta.Text);
            Params.ParamByName('@pe_cod_fator').Value := cbFator.ItemIndex + 1;
            Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Params.ParamByName('@pe_flg_status').Value := 0;  //Em atendimento
            Execute;
          end;
        end;
        //end;
      end;
    end;
    AtualizaCamposBotoesFat_Interv('Padr�o');
  end;
end;

procedure TfrmLancamentoFatoresIntervencao.btnLancarItensClick(
  Sender: TObject);
begin
  frmLancamentoFatoresIntervencao.Close;
  Application.CreateForm (TfrmLancamentoItensFatores, frmLancamentoItensFatores);
  frmLancamentoItensFatores.ShowModal;
end;

end.
