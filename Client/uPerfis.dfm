object frmPerfil: TfrmPerfil
  Left = 295
  Top = 126
  Width = 557
  Height = 520
  Caption = 'Sistema DECA -  [Cadastro de Perfis de Usu�rio]'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 5
    Width = 542
    Height = 292
    TabOrder = 0
    object dbgPerfis: TDBGrid
      Left = 8
      Top = 10
      Width = 528
      Height = 273
      DataSource = dsPerfil
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dbgPerfisDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'COD_PERFIL'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '[C�digo]'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DSC_PERFIL'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Caption = '[Perfil]'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 200
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'COLUMN1'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '[Inclus�o]'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 60
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'COLUMN2'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '[Altera��o]'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOM_USUARIO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Caption = '[Usu�rio]'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 200
          Visible = True
        end>
    end
  end
  object Panel2: TPanel
    Left = 4
    Top = 439
    Width = 541
    Height = 49
    TabOrder = 1
    object btnNovo: TBitBtn
      Left = 224
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 291
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 415
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 477
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnAlterar: TBitBtn
      Left = 355
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnAlterarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FF00004300004300003C000037000036000036FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000930000930002841518892B
        2D8C2A2A830F0F6200004000003A00003AFF00FFFF00FFFF00FFFF00FFFF00FF
        0004B30104A73D45C3B3B7EAFFFFFFFFFFFFFFFFFFFFFFFFA0A2CC2D2D740000
        3A00004EFF00FFFF00FFFF00FF0005CC0107BA7F89E2FFFFFFFFFFFF9B9CDB5E
        5EB75F5FB6ADADDDFFFFFFFFFFFF5E5E9B00003A000043FF00FFFF00FF0005CC
        6472E5FFFFFFD7DAF52528B30102880A0DA206078C00006A31319CE6E6F7FFFF
        FF36367D000043FF00FF0007E80B1BD8F8F8FFF2F3FC1721C30405A41214A303
        048905068B01017409097630309CFFFFFFC3C3DB01015000004B0007E84358F0
        FFFFFF6476ED0104C00203950507910304870E109807088C03036E0909798686
        C9FFFFFF27278800004B0009F37F94FAFFFFFF1932F05867EAFCFCFDEBEDF8EA
        EAF7EAEBF7FFFFFFFFFFFF03047E3233A2FFFFFF5558BE00004A0218FDA6B4FD
        FFFFFF112CFD0C20F37E89F2FFFFFFFFFFFFFFFFFFFFFFFF0B0EBD0308AE5257
        BEFFFFFF6267C30000781735FFA4B6FFFFFFFF2C4AFF000FFF0518FCA3ACF3FF
        FFFFFFFFFF070EC70107C31017BE5E65CAFDFDFF4A4EBD00007F0318FF91A6FF
        FFFFFFA9B9FF000EFF0008FF0E1FFDA4AEF71420D8060ECA070CC3090FB7BABE
        EDFCFCFD1017AA00007FFF00FF5F7AFFFFFFFFFFFFFF5C75FF000BFF000EFF00
        0AFF0618F80007DB0006CC7380E8FFFFFF8F97E2000198FF00FFFF00FF425FFF
        C4CFFFFFFFFFFFFFFF8B9FFF162FFF0414FF0515FD2037F39FADF7FFFFFFE2E5
        FA101ABA000198FF00FFFF00FFFF00FF5975FFD7DFFFFFFFFFFFFFFFFCFCFFD1
        DBFFD4DFFFFFFFFFFFFFFFC6CDF71825CD0001B0FF00FFFF00FFFF00FFFF00FF
        FF00FF5C76FF97A9FDDAE0FFFFFFFFFFFFFFFFFFFFD4DBFD596FF00514D70001
        B0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF5C75FF5C79FF62
        7DFF4A66FD203CF50619E5FF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object Panel3: TPanel
    Left = 3
    Top = 299
    Width = 542
    Height = 137
    TabOrder = 2
    object Label1: TLabel
      Left = 141
      Top = 24
      Width = 26
      Height = 13
      Caption = 'Perfil:'
    end
    object Label2: TLabel
      Left = 99
      Top = 47
      Width = 69
      Height = 13
      Caption = 'Data Inclus�o:'
    end
    object Label3: TLabel
      Left = 62
      Top = 70
      Width = 106
      Height = 13
      Caption = 'Data �ltima Altera��o:'
    end
    object Label4: TLabel
      Left = 128
      Top = 92
      Width = 39
      Height = 13
      Caption = 'Usu�rio:'
    end
    object edPerfil: TEdit
      Left = 170
      Top = 20
      Width = 227
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 50
      TabOrder = 0
    end
    object meData: TMaskEdit
      Left = 171
      Top = 43
      Width = 121
      Height = 21
      EditMask = 'cc/cc/cccc'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object meDataAltera: TMaskEdit
      Left = 171
      Top = 66
      Width = 121
      Height = 21
      Enabled = False
      EditMask = 'cc/cc/cccc'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object edUsuario: TEdit
      Left = 170
      Top = 89
      Width = 227
      Height = 21
      Enabled = False
      MaxLength = 50
      TabOrder = 3
    end
  end
  object dsPerfil: TDataSource
    DataSet = dmDeca.cdsSel_Perfil
    Left = 91
    Top = 37
  end
end
