object frmEmiteContratoAdmissao: TfrmEmiteContratoAdmissao
  Left = 575
  Top = 209
  Width = 855
  Height = 545
  Caption = '[Sistema Deca] - Emiss�o de Contrato de Admiss�o'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnEmitirFicha_e_Contrato: TSpeedButton
    Left = 312
    Top = 444
    Width = 185
    Height = 54
    Caption = 'Emitir Ficha e Contrato'
    Glyph.Data = {
      6A030000424D6A0300000000000076000000280000001C0000001B0000000100
      080000000000F402000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00080808080808
      0808080808080808080808080808080808080808080808070707070707070707
      0707070707070707080808080808080808080800000000000000000000000000
      0000000707070808080808080808080008080808080808080808080808080007
      070707070808080808080800080F0F0F0700000F0F0F0F0F0F08000707070707
      0707080808080800080F0F0F000E0E000F0F0F0F0F0800070808080808080808
      08080800080F0F0F000E0E0E000F0F0F0F080007080808080808080808080800
      080F0F0F000E0E0E0E000F0F0F080007080808080808080808080800080F0F0F
      000E0E0E0E0E000F0F080007070708080808080808080800080F0F0F000E0E0E
      0E0E0E000F080007070707070808080808080800080F0F0F000E0E00000E0E0E
      00080007080707070707080808080800080F0F0F000E0E000F000E0E0E000707
      080808070707070708080800080F0F0F000E0E000F0F000E0E0E000708080808
      0807070707070800080F0F0F000E0E000F0F0F000E0E0E000808080808080807
      07070800080F0F0F000E0E000F0F0F0F000E0E0E000808080808080808080800
      080F0F0F0000000F0F0F0F0F0F000E0E0E000808080808080808080008080808
      08080808080808080808000E0E0E000808080808080808000000000000000000
      00000000000007000E0E0E000808080808080808080808080808080808080808
      08080808000E0E0E000808080808080808080808080808080808080808080808
      08000E0E0E00080808080808080808080808080808080808080808080808000E
      0E0E00080808080808080808080808080808080808080808080808000E0E0E00
      080808080808080808080808080808080808080808080808000E0E0E00080808
      080808080808080808080808080808080808080808000E0E0008080808080808
      080808080808080808080808080808080808000E000808080808080808080808
      0808080808080808080808080808080000080808080808080808080808080808
      0808080808080808080808080808}
    OnClick = btnEmitirFicha_e_ContratoClick
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 46
    Width = 828
    Height = 389
    ActivePage = TabSheet2
    TabOrder = 0
    object TabSheet2: TTabSheet
      Caption = 'Dados Contrato'
      ImageIndex = 1
      object Label1: TLabel
        Left = 23
        Top = 300
        Width = 82
        Height = 13
        Caption = 'Tipo do Contrato:'
      end
      object Label2: TLabel
        Left = 280
        Top = 300
        Width = 94
        Height = 13
        Caption = 'Unidade de Destino'
      end
      object GroupBox2: TGroupBox
        Left = 3
        Top = 3
        Width = 110
        Height = 53
        Caption = '[Data de Admiss�o]'
        TabOrder = 0
        object mskAdmissao: TMaskEdit
          Left = 8
          Top = 18
          Width = 93
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
      end
      object GroupBox3: TGroupBox
        Left = 111
        Top = 3
        Width = 120
        Height = 53
        Caption = '[T�rmino de Contrato]'
        TabOrder = 1
        object mskTerminoContrato: TMaskEdit
          Left = 8
          Top = 18
          Width = 103
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
      end
      object GroupBox28: TGroupBox
        Left = 229
        Top = 3
        Width = 121
        Height = 53
        Caption = '[Data Exame M�dico]'
        TabOrder = 2
        object mskExameMedico: TMaskEdit
          Left = 8
          Top = 18
          Width = 103
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
      end
      object rgPrimeiroEmprego: TRadioGroup
        Left = 348
        Top = 3
        Width = 111
        Height = 53
        Caption = '[Primeiro Emprego?]'
        Items.Strings = (
          'SIM'
          'N�O')
        TabOrder = 3
      end
      object GroupBox27: TGroupBox
        Left = 457
        Top = 3
        Width = 361
        Height = 53
        Caption = '[Banco: se Santander Ag�ncia e Conta Corrente] *APENAS N�MEROS'
        TabOrder = 4
        object edNumCCorrente: TEdit
          Left = 274
          Top = 21
          Width = 80
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 1
        end
        object mskNumAgencia: TMaskEdit
          Left = 206
          Top = 21
          Width = 65
          Height = 21
          BorderStyle = bsNone
          EditMask = '99999'
          MaxLength = 5
          TabOrder = 0
          Text = '     '
        end
        object edBanco: TEdit
          Left = 8
          Top = 21
          Width = 194
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 2
        end
      end
      object rgContribuiSindicato: TRadioGroup
        Left = 3
        Top = 57
        Width = 383
        Height = 53
        Caption = '[J� fez contribui��o sindical neste exerc�cio?      Valor R$] '
        Items.Strings = (
          'SIM'
          'N�O')
        TabOrder = 5
      end
      object edValorContribuicaoSindicato: TEdit
        Left = 245
        Top = 75
        Width = 132
        Height = 21
        BorderStyle = bsNone
        CharCase = ecUpperCase
        TabOrder = 6
      end
      object GroupBox30: TGroupBox
        Left = 384
        Top = 57
        Width = 102
        Height = 53
        Caption = '[Sal�rio Base R$]'
        TabOrder = 7
        object edValorSalarioBase: TEdit
          Left = 7
          Top = 17
          Width = 83
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 0
        end
      end
      object GroupBox31: TGroupBox
        Left = 484
        Top = 57
        Width = 334
        Height = 53
        Caption = '[Hor�rio de Trabalho]'
        TabOrder = 8
        object edHorarioTrabalho: TEdit
          Left = 7
          Top = 17
          Width = 318
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 0
        end
      end
      object GroupBox49: TGroupBox
        Left = 4
        Top = 111
        Width = 452
        Height = 54
        Caption = '[Curso]'
        TabOrder = 9
        object cbCurso: TComboBox
          Left = 8
          Top = 18
          Width = 433
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 0
          Items.Strings = (
            'BOLSISTA'
            'APRENDIZAGEM PROFISSIONAL')
        end
      end
      object GroupBox29: TGroupBox
        Left = 454
        Top = 111
        Width = 364
        Height = 54
        Caption = '[Cargo/Fun��o]'
        TabOrder = 10
        object cbCargoFuncao: TComboBox
          Left = 9
          Top = 18
          Width = 346
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 0
          Items.Strings = (
            'APRENDIZ DE ASSISTENTE ADMINISTRATIVO FIC 4H'
            'ASSISTENTE ADMINISTRATIVO FIC 6H'
            'APRENDIZ DE ASSISTENTE ADMINISTRATIVO TEC 6H'
            'APRENDIZ DE ALMOXARIFE (I)'
            'APRENDIZ DE ALMOXARIFE (II)'
            'APRENDIZ DE ASSISTENTE DE LOG�STICA DE TRANSPORTE'
            'APRENDIZ DE RECEPCIONISTA'
            'APRENDIZ DE VENDEDOR COM�RCIO VAREJISTA'
            'APRENDIZ EM SERVI�OS DE PRODU��O AUDIOVISUAL'
            'APRENDIZ DE ASSISTENTE BANC�RIO 8H'
            'APRENDIZ DE ASSISTENTE BANC�RIO 4H'
            'APRENDIZ DE ELETRICISTA PPJA'
            'APRENDIZ DE MEC�NICO DE VE�CULOS AUTOMOTORES'
            'APRENDIZ DE ELETRICISTA MANUTEN��O ELETROELETR�NICA'
            'APRENDIZ DE PADEIRO E CONFEITEIRO'
            'APRENDIZ DE ASSISTENTE DE PRODU��O'
            'APRENDIZ DE AGENTE CULTURAL'
            'BOLSISTA')
        end
      end
      object rgNecessitaVT: TRadioGroup
        Left = 4
        Top = 168
        Width = 165
        Height = 60
        Caption = '[Necessita de Vale-Transporte?'
        Items.Strings = (
          'SIM'
          'N�O')
        TabOrder = 11
      end
      object GroupBox1: TGroupBox
        Left = 167
        Top = 168
        Width = 572
        Height = 60
        Caption = '[Itiner�rio]'
        TabOrder = 12
        object edItinerarioVT: TEdit
          Left = 10
          Top = 22
          Width = 551
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 0
        end
      end
      object GroupBox4: TGroupBox
        Left = 737
        Top = 168
        Width = 82
        Height = 60
        Caption = '[Qtd/dia]'
        TabOrder = 13
        object edQtdVTDia: TEdit
          Left = 9
          Top = 22
          Width = 62
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 0
        end
      end
      object rgNecessitaPE: TRadioGroup
        Left = 4
        Top = 228
        Width = 165
        Height = 60
        Caption = '[Necessita de Passe Escolar?'
        Items.Strings = (
          'SIM'
          'N�O')
        TabOrder = 14
      end
      object GroupBox5: TGroupBox
        Left = 167
        Top = 228
        Width = 572
        Height = 60
        Caption = '[Itiner�rio]'
        TabOrder = 15
        object edItinerarioPE: TEdit
          Left = 10
          Top = 22
          Width = 551
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 0
        end
      end
      object GroupBox6: TGroupBox
        Left = 737
        Top = 228
        Width = 82
        Height = 60
        Caption = '[Qtd/dia]'
        TabOrder = 16
        object edQtdPeDia: TEdit
          Left = 9
          Top = 22
          Width = 62
          Height = 21
          BorderStyle = bsNone
          CharCase = ecUpperCase
          TabOrder = 0
        end
      end
      object cbTipContrato: TComboBox
        Left = 24
        Top = 316
        Width = 248
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 17
        Items.Strings = (
          'BOLSISTA'
          'CELETISTA')
      end
      object cbUnidadeDestino: TComboBox
        Left = 277
        Top = 316
        Width = 524
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 18
        Items.Strings = (
          'BOLSISTA'
          'CELETISTA')
      end
    end
  end
  object Panel17: TPanel
    Left = 7
    Top = 8
    Width = 829
    Height = 33
    TabOrder = 1
    object Label39: TLabel
      Left = 16
      Top = 6
      Width = 463
      Height = 22
      Caption = 'MATR�CULA  -  NOME DA CRIAN�A/ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
  end
end
