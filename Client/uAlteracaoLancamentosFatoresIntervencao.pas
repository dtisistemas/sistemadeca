unit uAlteracaoLancamentosFatoresIntervencao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ExtCtrls, Grids, DBGrids, ComCtrls;

type
  TfrmLancamentoItensFatores = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    edMatricula: TMaskEdit;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    GroupBox5: TGroupBox;
    meDataRegistro: TMaskEdit;
    GroupBox6: TGroupBox;
    meSituacaoApresentada: TMemo;
    GroupBox7: TGroupBox;
    lbStatus: TLabel;
    GroupBox8: TGroupBox;
    meMetas: TMemo;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    rgAvaliacao: TRadioGroup;
    GroupBox4: TGroupBox;
    cbMotivos: TComboBox;
    Panel1: TPanel;
    dbgItensLancamentos: TDBGrid;
    btnAlterar: TBitBtn;
    btnAtivar: TBitBtn;
    BitBtn1: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLancamentoItensFatores: TfrmLancamentoItensFatores;

implementation

{$R *.DFM}

end.
