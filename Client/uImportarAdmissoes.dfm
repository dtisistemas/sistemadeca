object frmImportarAdmissoes: TfrmImportarAdmissoes
  Left = 593
  Top = 216
  Width = 585
  Height = 513
  Caption = '[Sistema Deca] - Importa��o de Admiss�es'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 5
    Top = 6
    Width = 567
    Height = 474
    BorderStyle = bsSingle
    TabOrder = 0
    object btnSelecionarPlanilha: TSpeedButton
      Left = 9
      Top = 10
      Width = 140
      Height = 33
      Caption = 'Selecionar planilha'
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777770000007777777777777777770000007777777777770007770000007444
        4400000006007700000074FFFF08880600080700000074F008000060EE070700
        000074FFFFF8060EE0047700000074F0088060EE00F47700000074FFFF060EE0
        00747700000074F00800EE0EE0047700000074FFFF0EE0F0EE047700000074F0
        080000F000047700000074FFFFFFFFFFFFF47700000074444444444444447700
        000074F444F444F444F477000000744444444444444477000000777777777777
        777777000000777777777777777777000000}
      OnClick = btnSelecionarPlanilhaClick
    end
    object btnImportarDadosDaPlanilha: TSpeedButton
      Left = 10
      Top = 47
      Width = 545
      Height = 33
      Caption = 'Importar'
      OnClick = btnImportarDadosDaPlanilhaClick
    end
    object lbCaminhoPlanilha: TLabel
      Left = 157
      Top = 20
      Width = 384
      Height = 13
      AutoSize = False
      Caption = '<Caminho da planilha>'
    end
    object Label28: TLabel
      Left = 327
      Top = 443
      Width = 138
      Height = 13
      Caption = 'Total de registros importados:'
    end
    object dbgDadosImportados: TDBGrid
      Left = 11
      Top = 85
      Width = 543
      Height = 347
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object DataSource1: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro
    Left = 511
    Top = 104
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.xls'
    FileName = 'Planilha do MS-Excel'
    Filter = 'Planilha do MS-Excel|*.xls'
    InitialDir = 'C:\Deca'
    Left = 477
    Top = 105
  end
end
