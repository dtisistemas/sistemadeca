unit uPESQUISA_INSCRITOS_TRIAGEM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db;

type
  TfrmPESQUISA_INSCRITOS_TRIAGEM = class(TForm)
    GroupBox1: TGroupBox;
    dbgRESULTADO: TDBGrid;
    gpbBUSCA_RAPIDA: TGroupBox;
    btnVER_RESULTADOS: TSpeedButton;
    btnCARREGAR_INSCRICAO: TSpeedButton;
    btnNOVA_INSCRICAO: TSpeedButton;
    btnEXPORTAR_RELATORIO: TSpeedButton;
    GroupBox3: TGroupBox;
    cmbCAMPO_PESQUISA: TComboBox;
    rdgCRITERIO_PESQUISA: TRadioGroup;
    gpbCAMPOS_PESQUISA: TGroupBox;
    cmbSITUACAO_INSCRICAO: TComboBox;
    edtVALOR_PESQUISA2: TEdit;
    edtVALOR_PESQUISA: TEdit;
    Label3: TLabel;
    dtsPESQUISA_INSCRITOS_TRIAGEM: TDataSource;
    lblTOTAL_ENCONTRADO: TLabel;
    Label5: TLabel;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    btnCONSULTA_DADOSDECA: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnVER_RESULTADOSClick(Sender: TObject);
    procedure dbgRESULTADOTitleClick(Column: TColumn);
    procedure btnNOVA_INSCRICAOClick(Sender: TObject);
    procedure edtVALOR_PESQUISAKeyPress(Sender: TObject; var Key: Char);
    procedure btnCARREGAR_INSCRICAOClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cmbCAMPO_PESQUISAClick(Sender: TObject);
    procedure edtVALOR_PESQUISA2KeyPress(Sender: TObject; var Key: Char);
    procedure btnCONSULTA_DADOSDECAClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPESQUISA_INSCRITOS_TRIAGEM: TfrmPESQUISA_INSCRITOS_TRIAGEM;
  vListaSituacao1, vListaSituacao2 : TStringList;

implementation

uses udmTriagemDeca, uCadastroInscricoes, uDM, uUtil,
  rINFORMACOES_INSCRICAO_RENOVACAO, uFichaPesquisaGenerica;

{$R *.DFM}

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.FormShow(Sender: TObject);
begin

  dbgRESULTADO.DataSource := nil;

  btnCARREGAR_INSCRICAO.Enabled := False;
  vListaSituacao1               := TStringList.Create();
  vListaSituacao2               := TStringList.Create();

  //dtsPESQUISA_INSCRITOS_TRIAGEM.DataSet := Nil;
  gpbBUSCA_RAPIDA.Visible    := True;
  gpbCAMPOS_PESQUISA.Visible := False;

  lblTOTAL_ENCONTRADO.Caption := 'Total encontrado: 000';

  //Carregar as situa��es de inscri��o para o combo cmbSITUacao_inscricao
  try
    with dmTriagemDeca.cdsSel_Par_Posicao do
    begin
      Close;
      Params.ParamByName ('@cod_posicao').Value := Null;
      Open;

      if (dmTriagemDeca.cdsSel_Par_Posicao.RecordCount > 0) then
      begin
        while not (dmTriagemDeca.cdsSel_Par_Posicao.eof) do
        begin
          //Carregar os vetores auxiliares
          vListaSituacao1.Add (IntToSTr(dmTriagemDeca.cdsSel_Par_Posicao.FieldByName ('cod_posicao').Value));
          vListaSituacao2.Add (dmTriagemDeca.cdsSel_Par_Posicao.FieldByName ('dsc_posicao').Value);
          cmbSITUACAO_INSCRICAO.Items.Add (dmTriagemDeca.cdsSel_Par_Posicao.FieldByName ('dsc_posicao').Value);
          dmTriagemDeca.cdsSel_Par_Posicao.Next;
        end;
      end;
    end;
  except
    Application.MessageBox ('Aten��o !!!' +#13+#10+
                            'Um erro ocorreu na tentativa de carregar as Situa��es de Inscri��o.' +#13+#10+
                            'Feche o formul�rio e abara-o novamente e caso o erro persista verifique' +#13+#10+
                            'a internet/rede ou entre em contato com o ADministrador do Sistema',
                            '[Sistema Deca] - Erro carregando Situa��es de Inscri��o',
                            MB_OK + MB_ICONERROR);
  end;

end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.btnVER_RESULTADOSClick(
  Sender: TObject);
begin

  if ( (Length(edtVALOR_PESQUISA.Text) <= 3) and (cmbCAMPO_PESQUISA.ItemIndex <= 1)) then
  begin
    Application.MessageBox ('Quantidade de caracteres m�nimo para realizar uma pesquisa s�o 4 (quatro) caracteres. Favor refazer sua pesquisa!',
                            '[Sistema Deca] - Pesquisa inscritos Triagem',
                            MB_OK + MB_ICONWARNING);
  end
  else
  begin

    dbgRESULTADO.DataSource := dtsPESQUISA_INSCRITOS_TRIAGEM;
    //Realiza a consulta de acordo com o que foi selecionado nas op��es de campo, crit�rios e valores
    case cmbCAMPO_PESQUISA.ItemIndex of
      //Pesquisar por nome do inscrito
      0: begin
        with dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem do
        begin
          Close;
          Params.ParamByName ('@pe_nom_nome').Value           := Trim(edtVALOR_PESQUISA.Text);
          Params.ParamByName ('@pe_nom_familiar').Value       := Null;
          Params.ParamByName ('@pe_num_inscricao').Value      := Null;
          Params.ParamByName ('@pe_num_inscricao_digt').Value := Null;
          Params.ParamByName ('@pe_cod_situacao').Value       := Null;
          Params.ParamByName ('@pe_tipo_consulta').Value      := 'N';
          Open;

          if dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount < 1 then
          begin
            Application.MessageBox ('Aten��o !!! ' +#13+#10+
                                    'N�o foram encontrados registros de acordo com os valores e crit�rios definidos' +#13+#10+
                                    'Refa�a a sua busca alterando valores e crit�rios.',
                                    '[Sistema Deca] - Pesquisa de Inscritos Triagem',
                                    MB_OK + MB_ICONINFORMATION);

          end
          else
          begin
            dtsPESQUISA_INSCRITOS_TRIAGEM.DataSet := dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem;
            lblTOTAL_ENCONTRADO.Caption := 'Total encontrado: ' + IntToStr(dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount);
          end;
        end;
      end;  //Finaliza o bloco 0: begin

      //Pesquisar por nome do familiar do inscrito
      1: begin
        with dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem do
        begin
          Close;
          Params.ParamByName ('@pe_nom_familiar').Value       := Trim(edtVALOR_PESQUISA.Text);
          Params.ParamByName ('@pe_nom_nome').Value           := Null;
          Params.ParamByName ('@pe_num_inscricao').Value      := Null;
          Params.ParamByName ('@pe_num_inscricao_digt').Value := Null;
          Params.ParamByName ('@pe_cod_situacao').Value       := Null;
          //Executar a instru��o para FAMILIAR
          Params.ParamByName ('@pe_tipo_consulta').Value      := 'N';
          Open;

          if dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount < 1 then
          begin
            Application.MessageBox ('Aten��o !!! ' +#13+#10+
                                    'N�o foram encontrados registros de acordo com os valores e crit�rios definidos' +#13+#10+
                                    'Refa�a a sua busca alterando valores e crit�rios.',
                                    '[Sistema Deca] - Pesquisa de Inscritos Triagem',
                                    MB_OK + MB_ICONINFORMATION);

          end
          else
          begin
            dtsPESQUISA_INSCRITOS_TRIAGEM.DataSet := dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem;
            lblTOTAL_ENCONTRADO.Caption := 'Total encontrado: ' + IntToStr(dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount);
          end;
        end;
      end;  //Finaliza o bloco 1: begin


      //Pesquisar por n�mero e digito de inscri��o
      2: begin
        with dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem do
        begin
          Close;
          Params.ParamByName ('@pe_nom_nome').Value           := Null;
          Params.ParamByName ('@pe_nom_familiar').Value       := Null;
          Params.ParamByName ('@pe_num_inscricao').Value      := StrToInt(edtVALOR_PESQUISA.Text);
          Params.ParamByName ('@pe_num_inscricao_digt').Value := StrToInt(edtVALOR_PESQUISA2.Text);
          Params.ParamByName ('@pe_cod_situacao').Value       := Null;
          Params.ParamByName ('@pe_tipo_consulta').Value      := 'N';
          Open;

          if dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount < 1 then
          begin
            Application.MessageBox ('Aten��o !!! ' +#13+#10+
                                    'N�o foram encontrados registros de acordo com os valores e crit�rios definidos' +#13+#10+
                                    'Refa�a a sua busca alterando valores e crit�rios.',
                                    '[Sistema Deca] - Pesquisa de Inscritos Triagem',
                                    MB_OK + MB_ICONINFORMATION);

          end
          else
          begin
            dtsPESQUISA_INSCRITOS_TRIAGEM.DataSet := dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem;
            lblTOTAL_ENCONTRADO.Caption := 'Total encontrado: ' + IntToStr(dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount);
          end;
        end;
      end;  //Finaliza o bloco 2: begin


      //Pesquisar por situa��o da inscri��o
      3: begin
        with dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem do
        begin
          Close;
          Params.ParamByName ('@pe_nom_nome').Value           := Null;
          Params.ParamByName ('@pe_nom_familiar').Value       := Null;
          Params.ParamByName ('@pe_num_inscricao').Value      := Null;
          Params.ParamByName ('@pe_num_inscricao_digt').Value := Null;
          Params.ParamByName ('@pe_cod_situacao').Value       := StrToInt(vListaSituacao1.Strings[cmbSITUACAO_INSCRICAO.ItemIndex]);
          Params.ParamByName ('@pe_tipo_consulta').Value      := 'S';
          Open;

          if dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount < 1 then
          begin
            Application.MessageBox ('Aten��o !!! ' +#13+#10+
                                    'N�o foram encontrados registros de acordo com os valores e crit�rios definidos' +#13+#10+
                                    'Refa�a a sua busca alterando valores e crit�rios.',
                                    '[Sistema Deca] - Pesquisa de Inscritos Triagem',
                                    MB_OK + MB_ICONINFORMATION);

          end
          else
          begin
            dtsPESQUISA_INSCRITOS_TRIAGEM.DataSet := dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem;
            lblTOTAL_ENCONTRADO.Caption := 'Total encontrado: ' + IntToStr(dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount);
          end;
        end;
      end;  //Finaliza o bloco 3: begin

    end; //Case

    //Se o resultado for "zerado" o bot�o btnCARREGAR_INSCRICAO ficar� desabilitado, caso contr�rio habilita, pois ter� pelo menos um resultado
    if dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.RecordCount < 1 then btnCARREGAR_INSCRICAO.Enabled := False else btnCARREGAR_INSCRICAO.Enabled := True;

  end;



end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.dbgRESULTADOTitleClick(
  Column: TColumn);
begin
  dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.IndexFieldNames := Column.FieldName;
end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.btnNOVA_INSCRICAOClick(
  Sender: TObject);
begin
  Application.CreateForm (TfrmCadastroInscricoes, frmCadastroInscricoes);
  vvMODO_PESQUISA := False;
  //For�ando o PageControl exibir a primeira aba...
  frmCadastroInscricoes.PageControl2.ActivePageIndex := 0;
  frmCadastroInscricoes.ShowModal;
end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.edtVALOR_PESQUISAKeyPress(
  Sender: TObject; var Key: Char);
begin
  if key = #13 then btnVER_RESULTADOS.Click;

end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.btnCARREGAR_INSCRICAOClick(
  Sender: TObject);
begin
  //Transportar os dados da consulta para a ficha completa....
  with dmTriagemDeca.cdsSel_CadastroFull do
  begin
    Close;
    Params.ParamByName ('@pe_cod_inscricao').Value      := Null;
    Params.ParamByName ('@pe_num_inscricao').Value      := dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.FieldByName ('NUM_INSCRICAO').AsInteger;
    Params.ParamByName ('@pe_num_inscricao_digt').Value := dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem.FieldByName ('NUM_INSCRICAO_DIGT').AsInteger;
    Open;

    if (dmTriagemDeca.cdsSel_CadastroFull.RecordCount > 0) then
    begin
      //ShowMessage ('Registro encontrado...');
      Application.CreateForm(TfrmCadastroInscricoes, frmCadastroInscricoes);
      vvMODO_PESQUISA := True;
      frmCadastroInscricoes.ModoTela('S');
      frmCadastroInscricoes.ShowModal;
    end
    else
    begin
      Application.MessageBox ('N�o foram encontrados registros.' +#13+#10+
                              'Pedimos que refa�a sua busca verificando os dados digitados',
                              '[Sistema Deca] - Pesquisa Ficha de Inscri��o - Triagem',
                              MB_OK + MB_ICONINFORMATION);
    end;
  end;
end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.SpeedButton1Click(
  Sender: TObject);
begin
  Application.CreateForm (TrelINFOrMACOES_INSCRICAO_RENOVACAO, relINFOrMACOES_INSCRICAO_RENOVACAO);
  relINFOrMACOES_INSCRICAO_RENOVACAO.Preview;
  relINFOrMACOES_INSCRICAO_RENOVACAO.Free;

end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.cmbCAMPO_PESQUISAClick(
  Sender: TObject);
begin
  edtVALOR_PESQUISA.Clear;
  edtVALOR_PESQUISA2.Clear;
  cmbSITUACAO_INSCRICAO.ItemIndex := -1;
  //rdgCRITERIO_PESQUISA.ItemIndex  := -1;

  case cmbCAMPO_PESQUISA.ItemIndex of
    //Nome do Inscrito
    0: begin
      gpbBUSCA_RAPIDA.Visible       := True;
      gpbCAMPOS_PESQUISA.Visible    := True;
      //rdgCRITERIO_PESQUISA.Visible  := True;
      edtVALOR_PESQUISA.Visible     := True;
      edtVALOR_PESQUISA2.Visible    := False;
      cmbSITUACAO_INSCRICAO.Visible := False;
      edtVALOR_PESQUISA.Width       := 150;
      edtVALOR_PESQUISA2.Visible    := False;
      Label3.Visible := True;
      edtVALOR_PESQUISA.SetFocus;
    end;

    //Nome do familiar
    1: begin
      gpbBUSCA_RAPIDA.Visible       := True;
      gpbCAMPOS_PESQUISA.Visible    := True;
      //rdgCRITERIO_PESQUISA.Visible  := True;
      edtVALOR_PESQUISA.Visible     := True;
      edtVALOR_PESQUISA2.Visible    := False;
      cmbSITUACAO_INSCRICAO.Visible := False;
      edtVALOR_PESQUISA.Width       := 150;
      edtVALOR_PESQUISA2.Visible    := False;
      Label3.Visible := False;
      edtVALOR_PESQUISA.SetFocus;
    end;

    //N�mero de inscri��o e d�gito
    2: begin
      gpbBUSCA_RAPIDA.Visible       := True;
      gpbCAMPOS_PESQUISA.Visible    := True;
      //rdgCRITERIO_PESQUISA.Visible  := False;
      edtVALOR_PESQUISA.Visible     := True;
      edtVALOR_PESQUISA2.Visible    := True;
      cmbSITUACAO_INSCRICAO.Visible := False;
      edtVALOR_PESQUISA.Width       := 50;
      edtVALOR_PESQUISA2.Width      := 50;
      Label3.Visible                := True;


    end;

    //Situa��o de inscri��o
    3: begin
      gpbBUSCA_RAPIDA.Visible       := True;
      gpbCAMPOS_PESQUISA.Visible    := True;
      //rdgCRITERIO_PESQUISA.Visible  := False;
      edtVALOR_PESQUISA.Visible     := True;
      edtVALOR_PESQUISA2.Visible    := False;
      cmbSITUACAO_INSCRICAO.Visible := True;
      edtVALOR_PESQUISA.Visible     := False;
      edtVALOR_PESQUISA2.Visible    := False;
      Label3.Visible                := False;
    end;
  end;
end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.edtVALOR_PESQUISA2KeyPress(
  Sender: TObject; var Key: Char);
begin
  if key = #13 then btnVER_RESULTADOS.Click;
end;

procedure TfrmPESQUISA_INSCRITOS_TRIAGEM.btnCONSULTA_DADOSDECAClick(
  Sender: TObject);
begin
  Application.CreateForm(TfrmFichaPesquisaGenerica, frmFichaPesquisaGenerica);
  frmFichaPesquisaGenerica.ShowModal;
end;

end.
