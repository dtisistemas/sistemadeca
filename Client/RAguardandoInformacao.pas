unit RAguardandoInformacao;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TRelAguardandoInformacao = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel10: TQRLabel;
    QRImage1: TQRImage;
    QRShape2: TQRShape;
    QRShape5: TQRShape;
    QRRichText1: TQRRichText;
    QRRichText2: TQRRichText;
    QRRichText3: TQRRichText;
  private

  public

  end;

var
  RelAguardandoInformacao: TRelAguardandoInformacao;

implementation

uses uDM;


{$R *.DFM}

end.
