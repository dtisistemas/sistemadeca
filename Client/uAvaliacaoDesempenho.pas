unit uAvaliacaoDesempenho;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, CheckLst, ComCtrls, Grids, DBGrids, Buttons, Mask;

type
  TfrmAvaliacaoDesempenho = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox2: TGroupBox;
    Memo1: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    BitBtn1: TBitBtn;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    edUsuario: TEdit;
    Label2: TLabel;
    meDataAvaliacao: TMaskEdit;
    Panel5: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    edPontosAdolescente: TEdit;
    edPontosEmpresa: TEdit;
    edPontosASocial: TEdit;
    edPontosPofessor: TEdit;
    edPontosInstrutor: TEdit;
    GroupBox1: TGroupBox;
    clbAlunos: TCheckListBox;
    cbUnidade: TComboBox;
    Panel4: TPanel;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Shape12: TShape;
    Shape13: TShape;
    Shape14: TShape;
    Shape15: TShape;
    Shape16: TShape;
    Shape17: TShape;
    Shape18: TShape;
    Shape19: TShape;
    Shape20: TShape;
    Shape21: TShape;
    Shape22: TShape;
    Label5: TLabel;
    mePeriodo: TMaskEdit;
    Label6: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    btnMedia: TSpeedButton;
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAvaliacaoDesempenho: TfrmAvaliacaoDesempenho;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses uDM, uFichaPesquisa, uPrincipal;

{$R *.DFM}

procedure TfrmAvaliacaoDesempenho.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        meDataAvaliacao.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmAvaliacaoDesempenho.FormShow(Sender: TObject);
begin
  edUsuario.Text := vvNOM_USUARIO;
end;

end.
