unit uCadastroFoto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, ExtCtrls, Buttons, Db, DBCtrls, ExtDlgs, jpeg;

type
  TfrmCadastroFoto = class(TForm)
    GroupBox1: TGroupBox;
    dbgAlunosFotos: TDBGrid;
    GroupBox2: TGroupBox;
    btnCapturarImagem: TSpeedButton;
    dsSel_Fotografias: TDataSource;
    OpenPictureDialog1: TOpenPictureDialog;
    Fotografia: TImage;
    procedure btnCapturarImagemClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgAlunosFotosCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroFoto: TfrmCadastroFoto;
  Path : String;
  vv_JPG : TJpegImage;
  b : TStream;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmCadastroFoto.btnCapturarImagemClick(Sender: TObject);
begin

  OpenPictureDialog1.Execute;
  Path := OpenPictureDialog1.FileName;
  Fotografia.Picture.LoadFromFile(Path);

  if Path <> '' then
  begin
    try
      with dmDeca.cdsAlt_Cadastro_Foto do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Fotografias.FieldByName ('cod_matricula').Value;
        Params.ParamByName ('@pe_img_foto').LoadFromFile(Path, ftBlob);
        Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName ('@pe_log_data').Value := Date();
        Execute;

        with dmDeca.cdsSel_Fotografias do
        begin
          Close;
          Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
          Open;
          dbgAlunosFotos.Refresh;
        end;

        
      end
    except end;
  end;         

end;

procedure TfrmCadastroFoto.FormShow(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Fotografias do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;
      dbgAlunosFotos.Refresh;
    end;
  except end;
end;

procedure TfrmCadastroFoto.dbgAlunosFotosCellClick(Column: TColumn);
begin
  try
    with dmDeca.cdsSel_Cadastro_Foto do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Fotografias.FieldByName ('cod_matricula').Value;
      Open;

      b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

      if (b.Size > 0) then
      begin
        try
          vv_JPG := TJPEGImage.Create;
          vv_JPG.LoadFromStream (b);
          Fotografia.Picture.Assign (vv_JPG);
        except end
      end
      else
      begin
        Fotografia.Picture.Assign (nil);
      end;

      vv_JPG.Free;
      b.Destroy;

    end
  except end;
end;

end.
