object frmImportaEscolaCadParaAcompEscolar: TfrmImportaEscolaCadParaAcompEscolar
  Left = 227
  Top = 378
  BorderStyle = bsDialog
  Caption = '[Sistema Deca] - Importa��o de Dados de Acomp. Escolar'
  ClientHeight = 386
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 3
    Top = 93
    Width = 97
    Height = 13
    Caption = 'N�o t�m RG Escolar'
  end
  object Gauge1: TGauge
    Left = 3
    Top = 351
    Width = 806
    Height = 31
    Progress = 0
  end
  object Label1: TLabel
    Left = 427
    Top = 93
    Width = 176
    Height = 13
    Caption = 'Sem infroma��es de Escola e S�rie...'
  end
  object GroupBox1: TGroupBox
    Left = 2
    Top = 2
    Width = 806
    Height = 89
    Caption = 'Dados da Importa��o'
    TabOrder = 0
    object Label3: TLabel
      Left = 50
      Top = 32
      Width = 228
      Height = 20
      Caption = 'Total de fichas cadastradas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 15
      Top = 66
      Width = 264
      Height = 20
      Caption = 'Total de alunos sem RG Escolar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lbTotalFichas: TLabel
      Left = 287
      Top = 32
      Width = 51
      Height = 20
      Caption = '00000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbTotalSemRGEscolar: TLabel
      Left = 287
      Top = 67
      Width = 51
      Height = 20
      Caption = '00000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object btnImportar: TSpeedButton
      Left = 406
      Top = 28
      Width = 157
      Height = 32
      Caption = 'Importar'
      Flat = True
      Glyph.Data = {
        A6030000424DA60300000000000036000000280000000D000000160000000100
        18000000000070030000C30E0000C30E00000000000000000000BFBFBFFF0000
        FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000BFBF
        BF00BFBFBFFF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF0000
        FF0000FF0000BFBFBF00BFBFBFFF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFF0000FF0000FF0000BFBFBF00BFBFBFFF0000FF0000FF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000BFBFBF00BFBFBFFF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000BFBF
        BF00BFBFBFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF0000BFBFBF00BFBFBFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000BFBFBF00BFBFBFFF0000FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000BFBFBF00BFBFBFFF0000
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000BFBF
        BF00BFBFBFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF0000BFBFBF00BFBFBFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000BFBFBF00BFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBF0000FFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BF00BFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000FF0000FF0000FFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBF00BFBFBFBFBFBFBFBFBFBFBFBF0000FF0000FF0000FF00
        00FF0000FFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBFBFBFBF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBF0000FF0000FF0000FFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BF00BFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000FF0000FF0000FFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBF00BFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000FF0000FF00
        00FFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BF0000FF0000FF0000FFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBF0000FF0000FF0000FFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BF00BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBF00}
      OnClick = btnImportarClick
    end
    object btnVerOcorrencias: TSpeedButton
      Left = 568
      Top = 28
      Width = 224
      Height = 32
      Caption = 'Gerar Arquivos de Pend�ncias'
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADAADADADADADADADADDADADADADAD717DA000000000DA111AD0FFFFFFF0AD7
        17DA0F77777F0DADADAD0F7FFF7F0ADA1ADA0F77777F0DA717AD0FFFFFFF0AD1
        11DA0F77888F0DA111AD0FFFF0000AD111DA0F778080ADA111AD0FFFF00ADAD1
        11DA000000ADADA111ADDADADADADADA1ADAADADADADADADADAD}
      OnClick = btnVerOcorrenciasClick
    end
  end
  object meOcorrencias: TMemo
    Left = 3
    Top = 109
    Width = 414
    Height = 236
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object meSemEscola: TMemo
    Left = 420
    Top = 109
    Width = 389
    Height = 236
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
