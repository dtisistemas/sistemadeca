unit uConsultaPedidosUniformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, Mask, Buttons;

type
  TfrmConsultaPedidos = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Label1: TLabel;
    cbUnidade: TComboBox;
    Label2: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cbMes3: TComboBox;
    mskAno3: TMaskEdit;
    Label5: TLabel;
    cbMes1: TComboBox;
    Label6: TLabel;
    mskAno1: TMaskEdit;
    Label7: TLabel;
    cbMes2: TComboBox;
    Label8: TLabel;
    mskAno2: TMaskEdit;
    Label9: TLabel;
    cbTipoUniforme: TComboBox;
    SpeedButton1: TSpeedButton;
    Label10: TLabel;
    cbMes4: TComboBox;
    Label11: TLabel;
    mskAno4: TMaskEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPedidos: TfrmConsultaPedidos;
  vListaTipoUniforme1, vListaTipoUniforme2, vListaUnidade1, vListaUnidade2 : TStringList;  

implementation

uses uDM, uGerarPedidoUniformes;

{$R *.DFM}

procedure TfrmConsultaPedidos.FormShow(Sender: TObject);
begin

  //Carregar a listagem de unidades para a aba "Selecionar Unidade"
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de Unidades para uma poss�vel consulta
  //aos dados da frequencia de determinada unidade...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('flg_status').AsInteger = 1) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;



  //Carregar os tipos de uniformes para a aba "Tipos de Uniforme"
  vListaTipoUniforme1 := TStringList.Create();
  vListaTipoUniforme2 := TStringList.Create();
  cbTipoUniforme.Clear;
  //Carrega a lista de tipos de uniformes...
  try
    with dmDeca.cdsSel_TipoUniforme do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
      Params.ParamByName ('@pe_dsc_tipouniforme').Value   := Null;
      Open;

      while not (dmDeca.cdsSel_TipoUniforme.eof) do
      begin
        cbTipoUniforme.Items.Add (FieldByName('dsc_tipouniforme').Value);
        vListaTipoUniforme1.Add(IntToStr(FieldByName('cod_id_tipouniforme').Value));
        vListaTipoUniforme2.Add(FieldByName('dsc_tipouniforme').Value);
        dmDeca.cdsSel_TipoUniforme.Next;
      end;

    end;
  except end;




















end;

end.
