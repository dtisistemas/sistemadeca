unit uAniversariantes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TfrmAniversariantes = class(TForm)
    GroupBox1: TGroupBox;
    cbMeses: TComboBox;
    btnVer: TSpeedButton;
    procedure FormActivate(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure cbMesesClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAniversariantes: TfrmAniversariantes;

implementation

uses uDM, uPrincipal, uUtil, rAniversariantes;

{$R *.DFM}

procedure TfrmAniversariantes.FormActivate(Sender: TObject);
begin
  cbMeses.Enabled := True;
  btnVer.Enabled := False;
end;

procedure TfrmAniversariantes.btnVerClick(Sender: TObject);
begin
  //Se o perfil � Administrador, Psicologia/Psicopedagogia, Diretor, DRH ou Triagem
  if (vvIND_PERFIL=1) or (vvIND_PERFIL=5) or (vvIND_PERFIL=7) or (vvIND_PERFIL=8) or (vvIND_PERFIL=9) then
  begin
    with dmDeca.cdsSel_Aniversariantes_mes do
    begin
      Close;
      Params.ParamByName('@pe_mes').AsInteger := vvINDEX_MES;
      Open;

      Application.CreateForm(TRelAniversariantes, relAniversariantes);
      relAniversariantes.lbTitulo.Caption := 'RELA��O DE ANIVERSARIANTES DO M�S';
      relAniversariantes.lbUnidade.Caption := 'FUNDHAS - FUNDA��O H�LIO AUGUSTO DE SOUZA';
      relAniversariantes.lbMes.Caption := cbMeses.Items[cbMeses.ItemIndex];

      //"setar" o dataset do relat�rio
      relAniversariantes.DataSet := dmDeca.cdsSel_Aniversariantes_mes;

      relAniversariantes.QRDBText1.DataSet := dmDeca.cdsSel_Aniversariantes_mes;
      relAniversariantes.QRDBText1.DataField := 'dat_nascimento';
      relAniversariantes.QRDBText2.DataSet := dmDeca.cdsSel_Aniversariantes_mes;
      relAniversariantes.QRDBText2.DataField := 'nom_nome';
      relAniversariantes.QRDBText3.DataSet := dmDeca.cdsSel_Aniversariantes_mes;
      relAniversariantes.QRDBText3.DataField := 'cod_matricula';

      relAniversariantes.Preview;
      relAniversariantes.Free;
      cbMeses.ItemIndex := -1;
    end
  end
  else
  begin
    with dmDeca.cdsSel_Aniversariantes_MesTodos do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName('@pe_mes').Value := vvINDEX_MES;
      Open;

      Application.CreateForm(TRelAniversariantes, relAniversariantes);
      relAniversariantes.lbTitulo.Caption := 'RELA��O DE ANIVERSARIANTES DO M�S';
      relAniversariantes.lbUnidade.Caption := vvNOM_UNIDADE;
      relAniversariantes.lbMes.Caption := cbMeses.Items[cbMeses.ItemIndex];

      //"setar" o dataset do relat�rio
      relAniversariantes.DataSet := dmDeca.cdsSel_Aniversariantes_MesTodos;

      relAniversariantes.QRDBText1.DataSet := dmDeca.cdsSel_Aniversariantes_MesTodos;
      relAniversariantes.QRDBText1.DataField := 'dat_nascimento';
      relAniversariantes.QRDBText2.DataSet := dmDeca.cdsSel_Aniversariantes_MesTodos;
      relAniversariantes.QRDBText2.DataField := 'nom_nome';
      relAniversariantes.QRDBText3.DataSet := dmDeca.cdsSel_Aniversariantes_MesTodos;
      relAniversariantes.QRDBText3.DataField := 'cod_matricula';

      relAniversariantes.Preview;
      relAniversariantes.Free;
      cbMeses.ItemIndex := -1;
    end;
  end;
end;

procedure TfrmAniversariantes.cbMesesClick(Sender: TObject);
begin
  if cbMeses.ItemIndex = -1 then
  begin
    ShowMessage('Favor selecionar mes para emiss�o da Lista de Aniversariantes...');
    cbMeses.SetFocus;
  end
else
  begin
    vvINDEX_MES := cbMeses.ItemIndex + 1;
    btnVer.Enabled := True;
    btnVer.Click;
  end;
end;

procedure TfrmAniversariantes.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmAniversariantes.Close;
  end;
end;

end.
