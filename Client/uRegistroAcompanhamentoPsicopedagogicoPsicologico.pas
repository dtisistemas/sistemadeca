unit uRegistroAcompanhamentoPsicopedagogicoPsicologico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls, Db;

type
  TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    lbNascimento: TLabel;
    lbUnidade: TLabel;
    lbEscola: TLabel;
    lbSerie: TLabel;
    lbPeriodo: TLabel;
    txtOficina: TEdit;
    txtInstrutor: TEdit;
    txtAsocial: TEdit;
    txtProfessor: TEdit;
    GroupBox3: TGroupBox;
    Panel1: TPanel;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    txtProfissional: TEdit;
    Panel7: TPanel;
    Label3: TLabel;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    meData: TMaskEdit;
    meRegistro: TMemo;
    dbgRegistrosAcompanhamento: TDBGrid;
    Panel2: TPanel;
    btnNovoH: TBitBtn;
    btnSalvarH: TBitBtn;
    btnCancelarH: TBitBtn;
    btnVoltarH: TBitBtn;
    btnAlterarH: TBitBtn;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    dsAcompanhamentos: TDataSource;
    dbgAcompanhamentos: TDBGrid;
    dsRegistros: TDataSource;
    Label13: TLabel;
    cbPesquisa: TComboBox;
    edValor: TEdit;
    procedure btnSairClick(Sender: TObject);
    procedure btnVoltarHClick(Sender: TObject);
    procedure AtualizaCampos (Modo : String);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgAcompanhamentosDblClick(Sender: TObject);
    procedure AtualizaCampos2 (Modo2 : String);
    procedure btnNovoHClick(Sender: TObject);
    procedure btnCancelarHClick(Sender: TObject);
    procedure btnSalvarHClick(Sender: TObject);
    procedure dbgRegistrosAcompanhamentoDblClick(Sender: TObject);
    procedure btnAlterarHClick(Sender: TObject);
    procedure edValorKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroAcompanhamentoPsicopedagogicoPsicologico: TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico;

implementation

uses uFichaPesquisa, uDM, rFichaCadastro, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.AtualizaCampos(
  Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    txtMatricula.Clear;
    lbNome.Caption := '';
    lbNascimento.Caption := '';
    lbUnidade.Caption := '';
    txtOficina.Clear;
    txtInstrutor.Clear;
    txtAsocial.Clear;
    lbEscola.Caption := '';
    lbSerie.Caption := '';
    lbPeriodo.Caption := '';
    txtProfessor.Clear;
    txtProfissional.Clear;

    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

    PageControl1.ActivePageIndex := 0;

  end
  else if Modo = 'Novo' then
  begin

    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;

  end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnSairClick(
  Sender: TObject);
begin
  frmRegistroAcompanhamentoPsicopedagogicoPsicologico.Close;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnVoltarHClick(
  Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //frmRegistroAcompanhamentoPsicopedagogicoPsicologico.Close;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCampos ('Padr�o');
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnNovoClick(
  Sender: TObject);
begin
  AtualizaCampos ('Novo');
  txtMatricula.SetFocus;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.PageControl1Change(
  Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) = 0 then
  begin
    PageControl1.ActivePageIndex := 0;
  end
  else
  begin
    //Csrrega os dados do Hist�rico de Acompanhamento
  end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  // n�o permite a troca de TabSheet se estiver editando a p�gina atual
  if (Length(Trim(txtMatricula.Text))=0) then AllowChange := false;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnPesquisarClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        lbNascimento.Caption := DateToStr(FieldByName('dat_nascimento').AsDateTime);
        lbUnidade.Caption := FieldByName('vUnidade').AsString;

        lbEscola.Caption := FieldByName ('nom_escola').AsString;
        lbSerie.Caption := FieldByName ('dsc_escola_serie').AsString;
        lbPeriodo.Caption := FieldByName ('dsc_escola_periodo').AsString;

        txtOficina.SetFocus;

        try
          with dmDeca.cdsSel_Psicopedagogico do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
            Open;
            dbgAcompanhamentos.Refresh;
          end;
        except end;

      end;
    except end;
  end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnSalvarClick(
  Sender: TObject);
begin
  try
    //Pede confirma��o para gravar os dados no banco
    if Application.MessageBox('Deseja INSERIR os dados de Acompanhamento Psicopedag�gico/Psicol�gico?',
           'Psicopedag�gico - Inclus�o',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      with dmDeca.cdsInc_Psicopedagogico do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
        Params.ParamByName('@pe_dsc_oficina').Value := GetValue(txtOficina.Text);
        Params.ParamByName('@pe_dsc_nom_professor').Value := GetValue(txtInstrutor.Text);
        Params.ParamByName('@pe_dsc_nom_asocial').Value := GetValue(txtAsocial.Text);
        Params.ParamByName('@pe_dsc_nom_professor_resp').Value := GetValue(txtProfessor.Text);
        Params.ParamByName('@pe_dsc_nom_profissional_resp').Value := GetValue(txtProfissional.Text);
        Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
        Execute;

        //Atualizao grid
        with dmDeca.cdsSel_Psicopedagogico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_log_cod_usuario').Value := vVCOD_USUARIO;
          Open;

          dbgAcompanhamentos.Refresh;
          AtualizaCampos ('Padr�o');

        end;
      end;
    end
    else
    begin
      AtualizaCampos ('Padr�o');
    end;
  except end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.FormShow(
  Sender: TObject);
begin
  //Carrega o grid com os dados de todos os adolescentes
  try
    with dmDeca.cdsSel_Psicopedagogico do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;//vvCOD_USUARIO;
      Open;
      dbgAcompanhamentos.Refresh;
    end;
  except end;
  AtualizaCampos ('Padr�o');
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.dbgAcompanhamentosDblClick(
  Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  AtualizaCampos2 ('Padr�o');
  label3.Caption := dmDeca.cdsSel_Psicopedagogico.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Psicopedagogico.FieldByName('nom_nome').Value; 
  with dmDeca.cdsSel_ItensPsicopedagogico do
  begin
    Close;
    Params.ParamByName('@pe_cod_item').Value := Null;
    Params.ParamByName('@pe_cod_id_psico').Value := dmDeca.cdsSel_Psicopedagogico.FieldByName('cod_id_psico').AsInteger;
    Open;
    dbgRegistrosAcompanhamento.Refresh;
  end;
end;
procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.AtualizaCampos2(
  Modo2: String);
begin
  if Modo2 = 'Padr�o' then
  begin

    meData.Clear;
    meRegistro.Clear;
    GroupBox4.Enabled := False;
    GroupBox5.Enabled := False;
    GroupBox6.Enabled := True;

    btnNovoH.Enabled := True;
    btnSalvarH.Enabled := False;
    btnAlterarH.Enabled := False;
    btnCancelarH.Enabled := False;
    btnVoltarH.Enabled := True;


  end
  else if Modo2 = 'Novo' then
  begin

    meData.Text := DateToStr(Date());;
    GroupBox4.Enabled := True;
    GroupBox5.Enabled := True;
    GroupBox6.Enabled := True;

    btnNovoH.Enabled := False;
    btnSalvarH.Enabled := True;
    btnAlterarH.Enabled := False;
    btnCancelarH.Enabled := True;
    btnVoltarH.Enabled := False;

  end

  else if Modo2 = 'Alterar' then
  begin

    btnNovoH.Enabled := False;
    btnSalvarH.Enabled := False;
    btnAlterarH.Enabled := True;
    btnCancelarH.Enabled := True;
    btnVoltarH.Enabled := False;

    GroupBox4.Enabled := True;
    meRegistro.Clear;
    GroupBox5.Enabled := True;
    meRegistro.Clear;

  end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnNovoHClick(
  Sender: TObject);
begin
  AtualizaCampos2 ('Novo');
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnCancelarHClick(
  Sender: TObject);
begin
  AtualizaCampos2 ('Padr�o');
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnSalvarHClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsInc_ItensPsicopedagogico do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_psico').AsInteger := dmDeca.cdsSel_Psicopedagogico.FieldByName ('cod_id_psico').AsInteger;
      Params.ParamByName('@pe_dat_registro').AsDateTime := StrToDate(meData.Text);
      Params.ParamByName('@pe_dsc_ocorrencia').AsString := meRegistro.Text;
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;

      with dmDeca.cdsSel_ItensPsicopedagogico do
      begin
        Close;
        Params.ParamByName('@pe_cod_item').Value := Null;
        Params.ParamByName('@pe_cod_id_psico').Value := dmDeca.cdsSel_Psicopedagogico.FieldByName ('cod_id_psico').AsInteger;//Null;
        Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
        Open;
        dbgRegistrosAcompanhamento.Refresh;
      end;

      AtualizaCampos2 ('Padr�o');

    end;
  except end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.dbgRegistrosAcompanhamentoDblClick(
  Sender: TObject);
begin
  //Confirma se o usu�rio � o mesmo do registro efetuado
  if (dmDeca.cdsSel_ItensPsicopedagogico.FieldByName('log_cod_usuario').AsInteger = vvCOD_USUARIO) then
  begin
    AtualizaCampos2 ('Alterar');
    meData.Text := DateToStr(dmDeca.cdsSel_ItensPsicopedagogico.FieldByName('dat_registro').AsDateTime);
    meRegistro.Text := dmDeca.cdsSel_ItensPsicopedagogico.FieldByname('dsc_ocorrencia').AsString;
  end
  else
  begin
    ShowMessage ('Aten��o!! Voc� n�o tem permiss�o para alterar esse registro...' + #13+#10 +
                 'Comunique sua Chefia imediata ou entre em contato com o Administrador do Sistema...');
  end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.btnAlterarHClick(
  Sender: TObject);
begin
  with dmDeca.cdsAlt_ItensPsicopedagogico do
  begin
    Close;
    Params.ParamByName('@pe_cod_item').Value := dmDeca.cdsSel_ItensPsicopedagogico.FieldByname('cod_item').Value;
    Params.ParamByName('@pe_dsc_ocorrencia').Value := Trim(meRegistro.Text);
    Execute;

    with dmDeca.cdsSel_ItensPsicopedagogico do
    begin
      Close;
      Params.ParamByName('@pe_cod_item').Value := Null;
      Params.ParamByName('@pe_cod_id_psico').Value := Null;
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Open;
      dbgRegistrosAcompanhamento.Refresh;
    end;

    AtualizaCampos2 ('Padr�o');

  end;
end;

procedure TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico.edValorKeyPress(
  Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    if cbPesquisa.ItemIndex = 0 then    //Pesquisa por Matr�cula
    begin
      //Carrega os Dados de Psicologia/Psicopedagogia
      try
        with dmDeca.cdsSel_Psicopedagogico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Trim(edValor.Text);
          Params.ParamByName('@pe_log_cod_usuario').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Open;
          //dbgResultado.Refresh;
          dbgAcompanhamentos.Refresh;
        end;
      except end;
    end
    else if cbPesquisa.ItemIndex = 1 then   //Pesquisa por nome
    begin
      //Carrega os Dados de Psicologia/Psicopedagogia
      try
        with dmDeca.cdsSel_Psicopedagogico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_log_cod_usuario').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Trim(edValor.Text) + '%';
          Open;
          //dbgResultado.Refresh;
          dbgAcompanhamentos.Refresh;
        end;
      except end;
    end
    else if cbPesquisa.ItemIndex = 2 then  //Pesquisa <Todos>
    begin
      //Carrega as Observa��es de Aprendizagem da Crian�a/Adolescente
      try
        with dmDeca.cdsSel_Psicopedagogico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_log_cod_usuario').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Open;
          //dbgResultado.Refresh;
          dbgAcompanhamentos.Refresh;
        end;
      except end;
    end;
  end;
end;

end.
