unit uImporta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls;

type
  TfrmImporta = class(TForm)
    GroupBox1: TGroupBox;
    edPath: TEdit;
    OpenDialog1: TOpenDialog;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    Memo1: TMemo;
    RadioGroup1: TRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImporta: TfrmImporta;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmImporta.SpeedButton1Click(Sender: TObject);
var
  txtFile : TextFile;
  Entrada : String;

  vvMATRICULA, vvNOME, vvPRONTUARIO, vvNASCIMENTO, vvINDSEXO, vvENDERECO, vvCOMPLEMENTO, vvBAIRRO, vvCEP, vvTELEFONE, vvPESO, vvALTURA,
  vvETNIA, vvCALCADO, vvCAMISA, vvPONTOSTRIAGEM, vvPERIODO, vvMODALIDAE, vvPROJETOVIDA, vvCODUNIDADE, vvCODESCOLA, vvNUMSECAO, vvESCOLASERIE,
  vvSAUDE, vvPERIODOESCOLA, vvOBSESCOLA, vvADMISSAO, vvTAMCAMISA, vvTIPOSANGUE, vvSTATUS, vvCOTAPASSE, vvCPF, vvRG, vvCERTIDAO, vvTURMA,
  vvUSUARIO, vvDATA, vvTURMAESCOLA, vvCARTAOSIAS, vvANOLETIVO, vvRGESCOLAR, vvDEFASAGEMESC, vvDEFAPREND, vvCTPS_NUM, vvCTPS_SERIE : String;

begin

  if OpenDialog1.Execute then
  begin
    Memo1.Lines.Clear;
    AssignFile (txtFile, OpenDialog1.FileName);
    Reset (txtFile);

    while not Eoln (txtFile) do
    begin
      Readln (txtFile, Entrada);

      while not Eoln (txtFile) do
      begin
        Readln (txtFile, Entrada);
        vvMATRICULA     := Copy(Entrada, 1 , 8);
        vvCTPS_NUM      := Copy(Entrada, 10 , 10);
        vvCTPS_SERIE    := Copy(Entrada, 21 , 5);
        //Consulta matricula
        with dmDeca.cdsSel_Cadastro do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(vvMATRICULA);
          Params.ParamByName ('@pe_cod_unidade').Value   := 0;//StrToInt(vvCODUNIDADE);
          Open;

          if (dmDeca.cdsSel_Cadastro.RecordCount = 0) then
            Memo1.Lines.Add ('[N�O]' + vvMATRICULA + ' N�O LOCALIZADA NO CADASTRO DO SISTEMA DECA.')
          else
          begin
            Memo1.Lines.Add ('[OK] ' + vvMATRICULA + ' LOCALIZADA NO CADASTRO DO SISTEMA DECA.');
            //Faz a altera��o da CTPS e S�rie no Cadastro
            try
              with dmDeca.cdsUPD_CTPS do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := vvMATRICULA;
                Params.ParamByName ('@pe_ctps_num').Value      := vvCTPS_NUM;
                Params.ParamByName ('@pe_ctps_serie').Value    := vvCTPS_SERIE;
                Execute;
              end;
            except
              Application.MessageBox ('Aten��o!!!' +#13+#10+
                                      'Houve um erro ao tentar IMPORTAR os dados para o cadastro.' +#13+#10+
                                      'Favor verificar e tentar novamente.',
                                      '[Sistema Deca]-Improta��o de Documentos',
                                      MB_ICONWARNING);
            end;
          end;
        end;
      end;
    end;
  end;
end;

end.
