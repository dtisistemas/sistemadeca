unit rEducacenso;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg, Db;

type
  TrelEducacenso = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRShape1: TQRShape;
    QRImage1: TQRImage;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    DetailBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    PageFooterBand1: TQRBand;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel3: TQRLabel;
    lbUnidadeEscolar: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    lbSerie: TQRLabel;
    lbPeriodo: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRSysData3: TQRSysData;
    QRLabel9: TQRLabel;
    lbClasse: TQRLabel;
    QRSysData2: TQRSysData;
  private

  public

  end;

var
  relEducacenso: TrelEducacenso;

implementation

uses uDM;

{$R *.DFM}

end.
