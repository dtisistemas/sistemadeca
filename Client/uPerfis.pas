unit uPerfis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, Mask;

type
  TfrmPerfil = class(TForm)
    Panel1: TPanel;
    dbgPerfis: TDBGrid;
    Panel2: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    Panel3: TPanel;
    dsPerfil: TDataSource;
    Label1: TLabel;
    edPerfil: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    meData: TMaskEdit;
    meDataAltera: TMaskEdit;
    edUsuario: TEdit;
    btnAlterar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure AtualizaCamposBotoesPerfis(Modo : String);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgPerfisDblClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPerfil: TfrmPerfil;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmPerfil.AtualizaCamposBotoesPerfis(Modo: String);
begin
 if Modo = 'Padr�o' then
 begin

   edPerfil.Clear;
   meData.Clear;
   meDataAltera.Clear;
   edUsuario.Clear;
   Panel3.Enabled := False;

   btnNovo.Enabled := True;
   btnSalvar.Enabled := False;
   btnAlterar.Enabled := False;
   btnCancelar.Enabled := False;
   btnSair.Enabled := True;

 end
 else if Modo = 'Novo' then
 begin
   Panel3.Enabled := True;
   btnNovo.Enabled := False;
   btnSalvar.Enabled := True;
   btnAlterar.Enabled := False;
   btnCancelar.Enabled := True;
   btnSair.Enabled := False;
 end
 else if Modo = 'Alterar' then
 begin
   Panel3.Enabled := True;
   btnNovo.Enabled := False;
   btnSalvar.Enabled := False;
   btnAlterar.Enabled := True;
   btnCancelar.Enabled := True;
   btnSair.Enabled := False;
 end;
end;

procedure TfrmPerfil.FormShow(Sender: TObject);
begin
  //"Inicializa" os campos e bot�es do formul�rio para in�cio
  AtualizaCamposBotoesPerfis('Padr�o');
  //Carrega os dados dos perfis cadastrados no grid 
  try
    with dmDeca.cdsSel_Perfil do
    begin
      Close;
      Params.ParamByName('@pe_cod_perfil').Value := Null;
      Open;
      dbgPerfis.Refresh;
    end;
  except end;
end;

procedure TfrmPerfil.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoesPerfis('Padr�o');
end;

procedure TfrmPerfil.btnSairClick(Sender: TObject);
begin
  frmPerfil.Close;
end;

procedure TfrmPerfil.btnSalvarClick(Sender: TObject);
begin
  //Insere o novo perfil
  try
    with dmDeca.cdsInc_Perfil do
    begin
      Close;
      Params.ParamByName('@pe_dsc_perfil').Value := Trim(edPerfil.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Execute;
    end;

    //Atualiza o grid com as informa��es novas
    with dmDeca.cdsSel_Perfil do
    begin
      Close;
      Params.ParamByName('@pe_cod_perfil').Value := Null; 
      Open;
      dbgPerfis.Refresh;
    end;
  except end;

  AtualizaCamposBotoesPerfis('Padr�o');
end;

procedure TfrmPerfil.btnAlterarClick(Sender: TObject);
begin
  AtualizaCamposBotoesPerfis('Padr�o');
end;

procedure TfrmPerfil.dbgPerfisDblClick(Sender: TObject);
begin
  AtualizaCamposBotoesPerfis('Alterar');
  edPerfil.Text := dmDeca.cdsSel_Perfil.FieldByName('dsc_perfil').AsString;
  meData.Text := dmDeca.cdsSel_Perfil.FieldByName('DataInclusao').AsString;
  meDataAltera.Text := dmDeca.cdsSel_Perfil.FieldByName('DataAlteracao').AsString;
  edUsuario.Text := dmDeca.cdsSel_Perfil.FieldByName('nom_usuario').AsString;
end;

procedure TfrmPerfil.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoesPerfis('Novo');
  edPerfil.SetFocus;
  edUsuario.Text := vvNOM_USUARIO;
end;

end.
