unit uADMISSOES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Buttons, StdCtrls, ExtCtrls, CheckLst, Db, Grids, DBGrids, Comobj, Mask;

type
  TfrmADMISSOES = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    dbgADMISSOES: TDBGrid;
    dsDemanda: TDataSource;
    panFAMILIARES: TPanel;
    DBGrid1: TDBGrid;
    btnADMISSAO_EFETIVAR: TSpeedButton;
    dsFAMILIA: TDataSource;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    cmbUNIDADE_ADM: TComboBox;
    GroupBox10: TGroupBox;
    edtCENTRO_CUSTO: TEdit;
    GroupBox11: TGroupBox;
    cmbPERIODO_UNIDADE: TComboBox;
    GroupBox12: TGroupBox;
    GroupBox13: TGroupBox;
    edtMATRICULA: TEdit;
    btnGERA_MATRICULA_TEMPORARIA: TSpeedButton;
    dsCOMISSAO_TRIAGEM: TDataSource;
    btnFECHAR_FAMILIARES: TSpeedButton;
    rdgTIPO_ADMISSAO: TRadioGroup;
    mskDATA_ADMISSAO: TMaskEdit;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    btnAPLICAR_FILTRO: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    btnLIMPA_REGIAO: TSpeedButton;
    btnLIMPA_FAIXAETARIA: TSpeedButton;
    btnLIMPA_UNIDADE: TSpeedButton;
    btnLIMPA_SITUACAODEMANDA: TSpeedButton;
    btnLIMPA_PERIODOESCOLAR: TSpeedButton;
    btnLIMPA_SITUACAOCADASTRO: TSpeedButton;
    btnLIMPA_SELECIONADOS: TSpeedButton;
    cmbREGIAO: TComboBox;
    cmbFAIXA_ETARIA: TComboBox;
    cmbUNIDADE: TComboBox;
    cmbPERIODO_ESCOLA: TComboBox;
    cmbSITUACAO_CADASTRO: TComboBox;
    cmbSELECIONADOS_DEMANDA: TComboBox;
    cmbSITUACAO_DEMANDA: TComboBox;
    GroupBox4: TGroupBox;
    btnPESQUISA_NOMEDEMANDA: TSpeedButton;
    btnPESQUISA_TODADEMANDA: TSpeedButton;
    edtNOME_DEMANDA: TEdit;
    chkOCULTAR_MATRICULA: TCheckBox;
    btnACESSAR_CADASTRO: TSpeedButton;
    btnADMISSAO_CONTRATO: TSpeedButton;
    procedure btnAPLICAR_FILTROClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnLIMPAR_FILTROSClick(Sender: TObject);
    procedure rdgCRITERIOSClick(Sender: TObject);
    procedure cmbUNIDADE_ADMClick(Sender: TObject);
    procedure btnGERA_MATRICULA_TEMPORARIAClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSAIRClick(Sender: TObject);
    procedure btnADICIONAR_UNICO_ADMISSAOClick(Sender: TObject);
    procedure btnADICIONAR_TODOS_ADMISSAOClick(Sender: TObject);
    procedure btnADMISSAO_EFETIVARClick(Sender: TObject);
    procedure btnFECHAR_FAMILIARESClick(Sender: TObject);
    procedure dbgADMISSOESDblClick(Sender: TObject);
    procedure btnPESQUISA_NOMEDEMANDAClick(Sender: TObject);
    procedure btnPESQUISA_TODADEMANDAClick(Sender: TObject);
    procedure btnACESSAR_CADASTROClick(Sender: TObject);
    procedure mskDATA_ADMISSAOExit(Sender: TObject);
    procedure ValidaDataAdmissao;
    procedure btnLIMPA_REGIAOClick(Sender: TObject);
    procedure btnLIMPA_FAIXAETARIAClick(Sender: TObject);
    procedure btnLIMPA_UNIDADEClick(Sender: TObject);
    procedure btnLIMPA_SITUACAODEMANDAClick(Sender: TObject);
    procedure btnLIMPA_PERIODOESCOLARClick(Sender: TObject);
    procedure btnLIMPA_SITUACAOCADASTROClick(Sender: TObject);
    procedure edtNOME_DEMANDAKeyPress(Sender: TObject; var Key: Char);
    procedure btnADMISSAO_CONTRATOClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmADMISSOES: TfrmADMISSOES;
  vListaUnidadeAdm1, vListaUnidadeAdm2,
  vListaRegiaoAdm1, vListaRegiaoAdm2 : TStringList;

  mmNOME_ALUNO, mmNOME_ALUNO2, mmNUMDOC_ALUNO, mmDATANASC_ALUNO, mmNUM_MATRICULA, mmENDERECO,
  mmNOME_RESPONSAVEL, mmNOME_RESPONSAVEL2, mmRG_RESPONSAVEL, mmCPF_RESPONSAVEL, mmESTCIVIL_RESPONSAVEL,
  mmPROFISSAO_RESPONSAVEL, mmNACIONALIDADE_RESPONSAVEL : String;

implementation

uses udmTriagemDeca, uDM, uPrincipal, rCONTRATO, rFICHA_ADMISSAO, uUtil,
  uPESQUISA_INSCRITOS_TRIAGEM, uCadastroInscricoes;

{$R *.DFM}

procedure TfrmADMISSOES.btnAPLICAR_FILTROClick(Sender: TObject);
begin
  //Verificar qual(is) filtros est�o selecionados...
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;

      //REGI�O
      if (cmbREGIAO.ItemIndex = -1) then Params.ParamByName ('@pe_cod_regiao').Value := Null else Params.ParamByName ('@pe_cod_regiao').AsInteger := StrToInt(vListaRegiaoAdm1.Strings[cmbREGIAO.ItemIndex]);

      //FAIXA ET�RIA
      if (cmbFAIXA_ETARIA.ItemIndex = -1) then
      begin
        Params.ParamByName ('@pe_faixaetaria1').Value := Null;
        Params.ParamByName ('@pe_faixaetaria2').Value := Null;
      end
      else
      begin
        if (cmbFAIXA_ETARIA.ItemIndex = -1) then
        begin
          Params.ParamByName ('@pe_faixaetaria1').Value := Null;
          Params.ParamByName ('@pe_faixaetaria2').Value := Null;
        end
        else
        begin
          case (cmbFAIXA_ETARIA.ItemIndex) of
          0: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 72;
               Params.ParamByName ('@pe_faixaetaria2').Value := 83;
             end;

          1: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 84;
               Params.ParamByName ('@pe_faixaetaria2').Value := 95;
             end;

          2: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 96;
               Params.ParamByName ('@pe_faixaetaria2').Value := 107;
             end;

          3: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 108;
               Params.ParamByName ('@pe_faixaetaria2').Value := 119;
             end;

          4: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 120;
               Params.ParamByName ('@pe_faixaetaria2').Value := 131;
             end;

          5: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 132;
               Params.ParamByName ('@pe_faixaetaria2').Value := 143;
             end;

          6: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 144;
               Params.ParamByName ('@pe_faixaetaria2').Value := 155;
             end;

          7: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 156;
               Params.ParamByName ('@pe_faixaetaria2').Value := 167;
             end;

          8: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 168;
               Params.ParamByName ('@pe_faixaetaria2').Value := 179;
             end;

          9: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 180;
               Params.ParamByName ('@pe_faixaetaria2').Value := 191;
             end;

          10: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 192;
               Params.ParamByName ('@pe_faixaetaria2').Value := 203;
             end;

          11: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 204;
               Params.ParamByName ('@pe_faixaetaria2').Value := 215;
             end;

          12: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 216;
               Params.ParamByName ('@pe_faixaetaria2').Value := 1000;
             end;
          end; //case
        end;
      end;

      //UNIDADE
      if (cmbUNIDADE.ItemIndex = -1) then Params.ParamByName ('@pe_cod_unidade').Value := Null else Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidadeAdm1.Strings[cmbUNIDADE.ItemIndex]);

      //SITUA��O DE DEMANDA
      if (cmbSITUACAO_DEMANDA.ItemIndex = -1) then
        Params.ParamByName ('@pe_flg_situacao').Value := Null
      else
      begin
        case (cmbSITUACAO_DEMANDA.ItemIndex) of
          0: Params.ParamByName ('@pe_flg_situacao').Value := 0;
          1: Params.ParamByName ('@pe_flg_situacao').Value := 1;
          2: Params.ParamByName ('@pe_flg_situacao').Value := 2;
          3: Params.ParamByName ('@pe_flg_situacao').Value := 3;
        end;
      end;

      //PER�ODO ESCOLAR
      if (cmbPERIODO_ESCOLA.ItemIndex = -1) then Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null else Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Trim(cmbPERIODO_ESCOLA.Text);

      //SITUA��O CADASTRO
      if (cmbSITUACAO_CADASTRO.ItemIndex = -1) then
        Params.ParamByName ('@pe_cod_situacao').Value     := Null
      else
      begin
        case (cmbSITUACAO_CADASTRO.ItemIndex) of
          0: Params.ParamByName ('@pe_cod_situacao').Value := 12; //INSCRITO = 12
          1: Params.ParamByName ('@pe_cod_situacao').Value := 15; //NECESSIDADE ESPECIAL = 15
          2: Params.ParamByName ('@pe_cod_situacao').Value := 16; //PEND�NCIA DE INSCRI��O = 16
          3: Params.ParamByName ('@pe_cod_situacao').Value := 19; //ESCOLA INTEGRAL = 19
          4: Params.ParamByName ('@pe_cod_situacao').Value := 22; //COMISSAO TRIAGEM = 22
        end;
      end;

      //NOME
      Params.ParamByName ('@pe_nom_nome').Value           := Null;

      //SELECIONADO
      Params.ParamByName ('@pe_flg_selecionado').Value := 1; //SELECIONADOS
      //if (cmbSELECIONADOS_DEMANDA.ItemIndex = -1) then
      //  Params.ParamByName ('@pe_flg_selecionado').Value  := Null
      //else
      //begin
      //  case (cmbSELECIONADOS_DEMANDA.ItemIndex) of
      //    0: Params.ParamByName ('@pe_flg_selecionado').Value := 0; //N�O SELECIONADOS
      //    1: Params.ParamByName ('@pe_flg_selecionado').Value := 1; //SELECIONADOS
      //  end;
      //end;
      
      Open;
      Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de acordo com os crit�rios'),
                                    '[Sistema Deca] - Totlizador de consulta',
                                    MB_OK + MB_ICONINFORMATION);
      dbgADMISSOES.Refresh;
    end
  except end;
end;

procedure TfrmADMISSOES.FormShow(Sender: TObject);
begin
  panFAMILIARES.Visible            := False;

  Panel2.Caption                   := ''; 


  //Carregar as regi�es
  vListaRegiaoAdm1 := TStringList.Create();
  vListaRegiaoAdm2 := TStringList.Create();

  with dmTriagemDeca.cdsSel_Par_Cadastro_Regioes do
  begin
    Close;
    Params.ParamByName ('@cod_regiao').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.eof) do
      begin
        cmbREGIAO.Items.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        vListaRegiaoAdm1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('cod_regiao').Value));
        vListaRegiaoAdm2.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.Next;
      end;
    end;
  end;

  //****************************************************************************

  //Carregar a lista de Unidades
  vListaUnidadeAdm1 := TStringList.Create();
  vListaUnidadeAdm2 := TStringList.Create();

  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := Null;
    Params.ParamByName ('@pe_nom_unidade').Value := Null;
    Open;

    if (dmDeca.cdsSel_Unidade.RecordCount > 0) then
    begin
      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        //Carrega apenas as unidades "ATIVAS"
        if (dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0) then
        begin
          cmbUNIDADE.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          cmbUNIDADE_ADM.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidadeAdm1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidadeAdm2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  end;


  //dbgADMISSOES.DataSource := nil;

end;

procedure TfrmADMISSOES.btnLIMPAR_FILTROSClick(Sender: TObject);
begin
  btnAPLICAR_FILTRO.OnClick(Self);
  cmbREGIAO.ItemIndex         := -1;
  cmbFAIXA_ETARIA.ItemIndex   := -1;
  cmbUNIDADE.ItemIndex        := -1;
  cmbPERIODO_ESCOLA.ItemIndex := -1;
end;

procedure TfrmADMISSOES.rdgCRITERIOSClick(Sender: TObject);
begin
   {
   case rdgCRITERIOS.ItemIndex of
     0: begin
       //N�o visualiza o gpbInscricao
       panDADOS_COMISSAOTRIAGEM.Visible := False;
       with dmTriagemDeca.cdsGERA_DEMANDA do
       begin
         Close;
         Params.ParamByName ('@pe_cod_regiao').Value        := Null;
         Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
         Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
         Params.ParamByName ('@pe_cod_unidade').Value       := Null;
         Params.ParamByName ('@pe_flg_situacao').Value      := 1;
         Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
         Params.ParamByName ('@pe_cod_situacao').Value      := Null;
         Open;
         dbgADMISSOES.DataSource := dsDemanda;
         dbgADMISSOES.Refresh;
         Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de acordo com os crit�rios'),
                                '[Sistema Deca] - Totlizador de consulta',
                                MB_OK + MB_ICONINFORMATION);
       end;
     end;

     1: begin
         panDADOS_COMISSAOTRIAGEM.Visible := True;
         //Carregar os dados dos inscritos com status "COMISS�O TRIAGEM"
         with dmTriagemDeca.cdsGERA_DEMANDA do
         begin
           Close;
           Params.ParamByName ('@pe_cod_regiao').Value        := Null;
           Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
           Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
           Params.ParamByName ('@pe_cod_unidade').Value       := Null;
           Params.ParamByName ('@pe_flg_situacao').Value      := Null; //1;
           Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
           Params.ParamByName ('@pe_cod_situacao').Value      := 22;   //COMISS�O TRIAGEM
           Open;
           dbgCOMISSAO_TRIAGEM.DataSource := dsCOMISSAO_TRIAGEM;
           dbgCOMISSAO_TRIAGEM.Refresh;
           Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de COMISS�O TRIAGEM'),
                                  '[Sistema Deca] - Totlizador de consulta',
                                  MB_OK + MB_ICONINFORMATION);
         end;
     end;
   end; //case
   }
end;

procedure TfrmADMISSOES.cmbUNIDADE_ADMClick(Sender: TObject);
begin
  edtCENTRO_CUSTO.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := cmbUNIDADE_ADM.Text;
      Open;

      edtCENTRO_CUSTO.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').Value;

    end;
  except end;
end;

procedure TfrmADMISSOES.btnGERA_MATRICULA_TEMPORARIAClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro_NovaMatricula777 do
    begin
      Close;
      Open;
      edtMATRICULA.Text := dmDeca.cdsSel_Cadastro_NovaMatricula777.FieldByName ('NovaMatricula').Value;
    end;
  except end;
end;

procedure TfrmADMISSOES.FormCreate(Sender: TObject);
begin
  //mskDATA_ADMISSAO.Text := DateToStr(Date());
end;

procedure TfrmADMISSOES.btnSAIRClick(Sender: TObject);
begin
  //panDADOS_COMISSAOTRIAGEM.Visible := False;
  {
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := 1;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := Null;
      Open;
      dbgADMISSOES.DataSource := dsDemanda;
      dbgADMISSOES.Refresh;
      Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de acordo com os crit�rios'),
                             '[Sistema Deca] - Totlizador de consulta',
                             MB_OK + MB_ICONINFORMATION);
    end;
  except end;}
end;

procedure TfrmADMISSOES.btnADICIONAR_UNICO_ADMISSAOClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja incluir o inscrito selecionado para o processo de Admiss�o?',
                             '[Sistema Deca] - Inclus�o de Inscrito em Comiss�o Triagem para Demanda de Admiss�o',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    //Verificar se o inscrito selecionado j� est� cadastrado em Demanda
    try
      with dmTriagemDeca.cdsSel_DEMANDA do
      begin
        Close;
        Params.ParamByName ('@pe_cod_inscricao').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
        Open;

        if (dmTriagemDeca.cdsSel_DEMANDA.RecordCount > 0) then
        begin
          Application.MessageBox ('Aten��o!!!' +#13+#10+
                                  'O inscrito selecionado j� se encontra em demanda para Admiss�o.' +#13+#10+
                                  'Por favor, verifique a rela��o de demanda.',
                                  '[Sistema Deca] - Aviso de Inclus�o em Demanda',
                                  MB_OK + MB_ICONINFORMATION);
        end
        else
        begin
          //Faz a inclus�o dos dados em DEMANDA
          try
            with dmTriagemDeca.cdsINS_DEMANDA do
            begin
              Close;
              Params.ParamByName ('@pe_cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
              Params.ParamByName ('@pe_dat_ligacao1').Value      := Null;
              Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := Null;
              Params.ParamByName ('@pe_dat_ligacao2').Value      := Null;
              Params.ParamByName ('@pe_dsc_quem_ligacao2').Value := Null;
              Params.ParamByName ('@pe_dat_ligacao3').Value      := Null;
              Params.ParamByName ('@pe_dsc_quem_ligacao3').Value := Null;
              Params.ParamByName ('@pe_dsc_observacoes').Value   := Null;
              Params.ParamByName ('@pe_flg_situacao').Value      := 1; //Como vemd e Comiss�o Triagem, j� entra como CONVOCADO para aparecer na listagem de Admiss�es
              Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
              Execute;
            end;
          except
            Application.MessageBox ('Aten��o!!!' +#13+#10+
                                   'Um erro ocorreu na tentativa de inserir dados para DEMANDA.' +#13+#10+
                                   'Por favor, tente novamente.',
                                   '[Sistema Deca] - Erro Inclus�o em Demanda',
                                   MB_OK + MB_ICONERROR);
          end;
        end;
      end;
    except
      Application.MessageBox ('Aten��o!!!' +#13+#10+
                              'Um erro ocorreu na tentativa de consultar o isncrito nos dados da DEMANDA.' +#13+#10+
                              'Por favor, tente novamente.',
                              '[Sistema Deca] - Erro consulta em Demanda',
                              MB_OK + MB_ICONERROR);
    end;
  end;
end;

procedure TfrmADMISSOES.btnADICIONAR_TODOS_ADMISSAOClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja incluir todos os inscritos em situa��o Comiss�o Triagem no processo de Admiss�o?',
                             '[Sistema Deca] - Inclus�o de todos de Comiss�o Triagem para Demanda de Admiss�o',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    dmTriagemDeca.cdsGERA_DEMANDA.First;
    while not (dmTriagemDeca.cdsGERA_DEMANDA.eof) do
    begin
      //Verificar se o registro atual est� inclu�do na tabela de DEMANDA
      with dmTriagemDeca.cdsSel_DEMANDA do
      begin
        Close;
        Params.ParamByName ('@pe_cod_inscricao').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
        Open;

        if (dmTriagemDeca.cdsSel_DEMANDA.RecordCount = 0) then
        begin
          try
            with dmTriagemDeca.cdsINS_DEMANDA do
            begin
              Close;
              Params.ParamByName ('@pe_cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
              Params.ParamByName ('@pe_dat_ligacao1').Value      := Null;
              Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := Null;
              Params.ParamByName ('@pe_dat_ligacao2').Value      := Null;
              Params.ParamByName ('@pe_dsc_quem_ligacao2').Value := Null;
              Params.ParamByName ('@pe_dat_ligacao3').Value      := Null;
              Params.ParamByName ('@pe_dsc_quem_ligacao3').Value := Null;
              Params.ParamByName ('@pe_dsc_observacoes').Value   := Null;
              Params.ParamByName ('@pe_flg_situacao').Value      := 1; //Como vemd e Comiss�o Triagem, j� entra como CONVOCADO para aparecer na listagem de Admiss�es
              Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
              Execute;
            end;
          except
            Application.MessageBox ('Aten��o!!!' +#13+#10+
                                   'Um erro ocorreu na tentativa de inserir dados para DEMANDA.' +#13+#10+
                                   'Por favor, tente novamente.',
                                   '[Sistema Deca] - Erro Inclus�o em Demanda',
                                   MB_OK + MB_ICONERROR);
          end;
        end;
        dmTriagemDeca.cdsGERA_DEMANDA.Next;
      end;
    end;
  end;
end;

procedure TfrmADMISSOES.btnADMISSAO_EFETIVARClick(Sender: TObject);
var
// DESATIVADO O CONTRATO/FICHA PELO WORD  -->   appWord, wordDoc, contratoDoc, fichaDoc : OleVariant;
   contrato, ficha, data, mmRESPONSAVEL, mmPAI, mmMAE, mmCPF_RESP, mmRG_RESP, mmMATRICULA, mmOCUPACAO_RESP : String;
begin
  //Gera matr�cula tempor�ria para inserir na admiss�o
  mmMATRICULA := '';
  try
    with dmDeca.cdsSel_Cadastro_NovaMatricula777 do
    begin
      Close;
      Open;
      edtMATRICULA.Text := dmDeca.cdsSel_Cadastro_NovaMatricula777.FieldByName ('NovaMatricula').Value;
      mmMATRICULA       := dmDeca.cdsSel_Cadastro_NovaMatricula777.FieldByName ('NovaMatricula').Value;
    end;
  except end;

  

  if (Application.MessageBox (PChar('Deseja realizar a ADMISS�O do(a) aluno(a) ' + ' - ' + UpperCase(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').Value) + ' na Unidade ' + cmbUNIDADE_ADM.Text + ' para o Per�odo ' + cmbPERIODO_UNIDADE.Text + ' ?'),
                                    '[Sistema Deca] - Efetivar ADMISS�O',
                                    MB_YESNO + MB_ICONWARNING) ) = idYes then
  begin

    //Recuperar PAI, M�E e RESPONS�VEL PELO INSCRITO E TRANSPORTAR OS DADOS PARA O DOCUMENTO
    mmRESPONSAVEL   := '';
    mmPAI           := '';
    mmMAE           := '';
    mmCPF_RESP      := '';
    mmRG_RESP       := '';
    mmOCUPACAO_RESP := '';

    //Pega os dados do selecionado no DBGrid/Composi��o Familiar
    mmRESPONSAVEL   := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
    mmCPF_RESP      := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_CPF_fam').AsString;
    mmRG_RESP       := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_RG_fam').AsString;
    mmOCUPACAO_RESP := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').AsString;

    //Roda a procedure novamente para recuperar MAE e PAI
    with dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar do
    begin
      Close;
      Params.ParamByName ('@Cod_membrofamilia').Value := Null;
      Params.ParamByName ('@Cod_inscricao').Value     := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('COD_INSCRICAO').Value);
      Open;

      if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.RecordCount > 0) then
      begin
        dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.First;
        while not (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.eof) do
        begin
          //if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_responsavel').Value = 1) then
          //begin
          //  mmRESPONSAVEL := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
          //  mmCPF_RESP    := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_CPF_fam').AsString;
          //  mmRG_RESP     := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_RG_fam').AsString;
          //end;
          if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('cod_parentesco').Value = 1) then  //Recuperar o nome do pai
            mmPAI := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
          if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('cod_parentesco').Value = 2) then  //Recuperar o nome da m�e
            mmMAE := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
          dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.Next;
        end;
      end;
    end;

    Application.CreateForm(TrelCONTRATO, relCONTRATO);

    if chkOCULTAR_MATRICULA.Checked then relCONTRATO.qrMATRICULA.Caption  := '' else relCONTRATO.qrMATRICULA.Caption  := mmMATRICULA;
    relCONTRATO.qrNOME1.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').AsString;
    relCONTRATO.qrNOME2.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').AsString;
    relCONTRATO.qrNASCIMENTO.Caption := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DATA_NASCIMENTO').AsString;

    data := mskDATA_ADMISSAO.Text;
    data := (FORMATDATETIME('dd "de" mmmm "de" yyyy', StrToDate(data)));
    relCONTRATO.qrADMISSAO.Caption   := mskDATA_ADMISSAO.Text;
    relCONTRATO.qrINFO_DATA.Caption  := 'S�O JOS� DOS CAMPOS, ' + data + '.';

    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CPF').IsNull) then relCONTRATO.qrCPF_ALUNO.Caption := '' else relCONTRATO.qrCPF_ALUNO.Caption  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CPF').AsString;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_RG').IsNull) then relCONTRATO.qrRG_ALUNO.Caption := '' else relCONTRATO.qrRG_ALUNO.Caption  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_RG').AsString;
    if mmRESPONSAVEL = '' then relCONTRATO.qrRESPONSAVEL.Caption  := '' else relCONTRATO.qrRESPONSAVEL.Caption  := mmRESPONSAVEL;
    if mmRESPONSAVEL = '' then relCONTRATO.qrRESPONSAVEL2.Caption := '' else relCONTRATO.qrRESPONSAVEL2.Caption := mmRESPONSAVEL;

    relCONTRATO.qrCPF_RESPONSAVEL.Caption := mmCPF_RESP;
    relCONTRATO.qrRG_RESPONSAVEL.Caption  := mmRG_RESP;

    if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_estadocivil').IsNull)   then relCONTRATO.qrESTADO_CIVIL.Caption    := '' else relCONTRATO.qrESTADO_CIVIL.Caption    := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_estadocivil').AsString;

    //if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').IsNull)  then relCONTRATO.qrPROFISSAO.Caption       := '' else relCONTRATO.qrPROFISSAO.Caption       := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').AsString;
    relCONTRATO.qrPROFISSAO.Caption := mmOCUPACAO_RESP;
    
    if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_nacionalidade').IsNull) then relCONTRATO.qrNACIONALIDADE.Caption   := '' else relCONTRATO.qrNACIONALIDADE.Caption   := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_nacionalidade').AsString;

    relCONTRATO.qrENDERECO.Caption   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ENDERECO').AsString + ', ' + dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_NUMRESIDENCIA').AsString + '  -  ' +
                                        dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_COMPLEMENTO').AsString + ' | ' + dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').AsString;
    relCONTRATO.Preview;
    relCONTRATO.Free;

    Application.CreateForm(TrelFICHA_ADMISSAO, relFICHA_ADMISSAO);
    //relFICHA_ADMISSAO.qrTITULO_FICHA.Caption            := 'FICHA DE ADMISS�O - CADASTRO DE ' + rdgTIPO_ADMISSAO.Caption;
    case rdgTIPO_ADMISSAO.ItemIndex of
      0: relFICHA_ADMISSAO.qrTITULO_FICHA.Caption := 'FICHA DE ADMISS�O - CADASTRO DE CRIAN�A/BOLSISTA';
      1: relFICHA_ADMISSAO.qrTITULO_FICHA.Caption := 'FICHA DE ADMISS�O - CADASTRO DE ADOLESCENTE';
    end;

    //relFICHA_ADMISSAO.qrMATRICULA.Caption               := mmMATRICULA; //Trim(edtMATRICULA.Text);
    if chkOCULTAR_MATRICULA.Checked then relFICHA_ADMISSAO.qrMATRICULA.Caption  := '' else relFICHA_ADMISSAO.qrMATRICULA.Caption  := mmMATRICULA;
    relFICHA_ADMISSAO.qrNOME.Caption                    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').AsString;
    relFICHA_ADMISSAO.qrNASCIMENTO.Caption              := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DATA_NASCIMENTO').AsString;
    relFICHA_ADMISSAO.qrCRACHA.Caption                  := '';
    relFICHA_ADMISSAO.qrENDERECO.Caption                := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ENDERECO').AsString + ', ' + dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_NUMRESIDENCIA').AsString;

    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_COMPLEMENTO').IsNull) then relFICHA_ADMISSAO.qrCOMPLEMENTO.Caption      := '' else relFICHA_ADMISSAO.qrCOMPLEMENTO.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_COMPLEMENTO').AsString;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').IsNull)      then relFICHA_ADMISSAO.qrBAIRRO.Caption           := '' else relFICHA_ADMISSAO.qrBAIRRO.Caption           := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').AsString;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CEP').IsNull)         then relFICHA_ADMISSAO.qrCEP.Caption              := '' else relFICHA_ADMISSAO.qrCEP.Caption              := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CEP').AsString;
    //if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('MAE').IsNull)             then relFICHA_ADMISSAO.qrNOME_MAE.Caption         := '' else relFICHA_ADMISSAO.qrNOME_MAE.Caption         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('MAE').AsString;
    relFICHA_ADMISSAO.qrNOME_MAE.Caption         := mmMAE;
    //if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('PAI').IsNull)             then relFICHA_ADMISSAO.qrNOME_PAI.Caption         := '' else relFICHA_ADMISSAO.qrNOME_PAI.Caption         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('PAI').AsString;
    relFICHA_ADMISSAO.qrNOME_PAI.Caption         := mmPAI;
    //if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('RESPONSAVEL').IsNull)     then relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := '' else relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('RESPONSAVEL').AsString;
    relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := mmRESPONSAVEL;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CIDADE').IsNull)      then relFICHA_ADMISSAO.qrLOCAL_NASCIMENTO.Caption := '' else relFICHA_ADMISSAO.qrLOCAL_NASCIMENTO.Caption := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CIDADE').AsString;

    relFICHA_ADMISSAO.qrMUNICIPIO_RESIDE.Caption        := 'S�O JOS� DOS CAMPOS/SP';

    if ( (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vTelefone').IsNull) or (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vTelefone').AsString = '(99) 9999-9999') ) then
      relFICHA_ADMISSAO.qrTELEFONE.Caption              := ''
    else relFICHA_ADMISSAO.qrTELEFONE.Caption           := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vTelefone').AsString;
    relFICHA_ADMISSAO.qrCELULAR.Caption                 := '';
    relFICHA_ADMISSAO.qrCONJUGE.Caption                 := '';
    relFICHA_ADMISSAO.qrESTADO_CIVIL.Caption            := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ESTADOCIVIL').AsString;
    relFICHA_ADMISSAO.qrMORA_SIM.Caption                := 'SIM';
    relFICHA_ADMISSAO.qrMORA_NAO.Caption                := 'N�O';
    relFICHA_ADMISSAO.qrNACIONALIDADE.Caption           := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_NACIONALIDADE').AsString;
    relFICHA_ADMISSAO.qrGRAU_INSTRUCAO.Caption          := '';
    relFICHA_ADMISSAO.qrRACA.Caption                    := '';
    relFICHA_ADMISSAO.qrCABELOS.Caption                 := '';
    relFICHA_ADMISSAO.qrOLHOS.Caption                   := '';
    relFICHA_ADMISSAO.qrSANGUE.Caption                  := '';
    relFICHA_ADMISSAO.qrPESO.Caption                    := '';
    relFICHA_ADMISSAO.qrALTURA.Caption                  := '';
    relFICHA_ADMISSAO.qrCALCADO.Caption                 := '';
    relFICHA_ADMISSAO.qrFUMA_SIM.Caption                := 'SIM';
    relFICHA_ADMISSAO.qrFUMA_NAO.Caption                := 'N�O';
    relFICHA_ADMISSAO.qrGUARDAREGULAR_SIM.Caption       := 'SIM';
    relFICHA_ADMISSAO.qrGUARDAREGULAR_NAO.Caption       := 'N�O';
    relFICHA_ADMISSAO.qrNECESSIDADES_SIM.Caption        := 'SIM';
    relFICHA_ADMISSAO.qrNECESSIDADES_NAO.Caption        := 'N�O';
    relFICHA_ADMISSAO.qrQUAIS_NECESSIDADES.Caption      := '';
    relFICHA_ADMISSAO.qrCENTRO_CUSTO_ATUAL.Caption      := '2027 - SE��O TRIAGEM';
    relFICHA_ADMISSAO.qrCENTRO_CUSTO_DESTINO.Caption    := Trim(edtCENTRO_CUSTO.Text) + ' - ' + cmbUNIDADE_ADM.Text;
    relFICHA_ADMISSAO.qrRG.Caption                      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_RG').AsString;
    relFICHA_ADMISSAO.qrDATA_EMISSAO_RG.Caption         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DAT_EMISSAO_RG').AsString;
    relFICHA_ADMISSAO.qrEMISSOR.Caption                 := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_EMISSOR').AsString;
    relFICHA_ADMISSAO.qrUF.Caption                      := '';
    relFICHA_ADMISSAO.qrCPF.Caption                     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CPF').AsString;
    relFICHA_ADMISSAO.qrCERTIDAO_NASCIMENTO.Caption     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_NUMCERTNASC').AsString;
    relFICHA_ADMISSAO.qrTITULO_ELEITOR.Caption          := '';
    relFICHA_ADMISSAO.qrZONA_ELEITORAL.Caption          := '';
    relFICHA_ADMISSAO.qrSECAO_ELEITORAL.Caption         := '';
    relFICHA_ADMISSAO.qrCTPS.Caption                    := '';
    relFICHA_ADMISSAO.qrSERIE_CTPS.Caption              := '';
    relFICHA_ADMISSAO.qrEMISSAO_CTPS.Caption            := '';
    relFICHA_ADMISSAO.qrUF_CTPS.Caption                 := '';

    relFICHA_ADMISSAO.qrPIS.Caption                     := '';
    relFICHA_ADMISSAO.qrPIS_EMISSAO.Caption             := '';
    relFICHA_ADMISSAO.qrPIS_BANCO.Caption               := '';

    relFICHA_ADMISSAO.qrSUS.Caption                     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CARTAO_SUS').AsString;
    relFICHA_ADMISSAO.qrSIAS.Caption                    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('COD_SIAS').AsString;
    relFICHA_ADMISSAO.qrRESERVISTA.Caption              := '';
    relFICHA_ADMISSAO.qrCSM.Caption                     := '';
    relFICHA_ADMISSAO.qrNATURALIZADO_SIM.Caption        := 'SIM';
    relFICHA_ADMISSAO.qrNATURALIZADO_NAO.Caption        := 'N�O';
    relFICHA_ADMISSAO.qrDATA_CHEGA_BRASIL.Caption       := '';
    relFICHA_ADMISSAO.qrCART_N19.Caption                := '';
    relFICHA_ADMISSAO.qrQTD_FILHOS_BRASIL.Caption       := '';
    relFICHA_ADMISSAO.qrTOTAL_FILHOS.Caption            := '';
    relFICHA_ADMISSAO.qrNUM_REG_GERAL.Caption           := '';
    relFICHA_ADMISSAO.qrVALIDAE_REGISTRO.Caption        := '';
    relFICHA_ADMISSAO.qrVALIDADE_CTPS.Caption           := '';

    relFICHA_ADMISSAO.qrPERIODO_ATIVIDADE.Caption       := '';
    relFICHA_ADMISSAO.qrPRIMEIRO_EMPREGO.Caption        := '';
    relFICHA_ADMISSAO.qrTERMINO_CONTRATO.Caption        := '';
    relFICHA_ADMISSAO.qrEXAME_MEDICO.Caption            := '';
    relFICHA_ADMISSAO.qrCONTRATADO_SIM.Caption          := '';
    relFICHA_ADMISSAO.qrCONTRATADO_NAO.Caption          := '';
    relFICHA_ADMISSAO.qrNUM_AGENCIA.Caption             := '';
    relFICHA_ADMISSAO.qrNUM_CONTA.Caption               := '';
    relFICHA_ADMISSAO.qrSALARIO_BASE.Caption            := '';
    relFICHA_ADMISSAO.qrFUNCAO.Caption                  := '';
    relFICHA_ADMISSAO.qrHORARIO.Caption                 := '';
    relFICHA_ADMISSAO.qrNECESSITA_PASSE_SIM.Caption     := 'SIM';
    relFICHA_ADMISSAO.qrNECESSITA_PASSE_NAO.Caption     := 'N�O';
    relFICHA_ADMISSAO.qrCONTRIBUI_SINDICATO_SIM.Caption := 'SIM';
    relFICHA_ADMISSAO.qrCONTRIBUI_SINDICATO_NAO.Caption := 'N�O';

    data := trim(mskDATA_ADMISSAO.Text);
    data := (FORMATDATETIME('dd "de" mmmm "de" yyyy', StrToDate(data)));
    relFICHA_ADMISSAO.qrADMISSAO.Caption   := Trim(mskDATA_ADMISSAO.Text);
    relFICHA_ADMISSAO.qrINFO_DATA.Caption  := 'S�O JOS� DOS CAMPOS, ' + data + '.';

    relFICHA_ADMISSAO.Preview;
    relFICHA_ADMISSAO.Free;

    //Executa a procedure de Migra��o dos dados
    try
      with (dmTriagemDeca.cdsMIGRACAO_TRIAGEM_DECA) do
      begin
        Close;
        Params.ParamByName ('@pe_codMatricula').Value   := Trim(mmMATRICULA);
        Params.ParamByName ('@pe_codInscricao').Value   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('COD_INSCRICAO').Value;
        Params.ParamByName ('@pe_Sexo').Value           := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vSEXO').AsString);
        Params.ParamByName ('@pe_Bairro').Value         := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').AsString);
        if (cmbPERIODO_UNIDADE.ItemIndex = 2) then Params.ParamByName ('@pe_PeriodoFundhas').Value := 'INTEGR/ESC' else Params.ParamByName ('@pe_PeriodoFundhas').Value := Trim(cmbPERIODO_UNIDADE.Text);
        Params.ParamByName ('@pe_CodUnidadeDestino').Value   := StrToInt(vListaUnidadeAdm1.Strings[cmbUNIDADE_ADM.ItemIndex]);
        Params.ParamByName ('@pe_NumSecao').Value            := '0000'; //Trim(edtCENTRO_CUSTO.Text);
        Params.ParamByName ('@pe_DatAdmissao').Value         := StrToDate(mskDATA_ADMISSAO.Text);
        Params.ParamByName ('@pe_CodUsuario').Value          := vvCOD_USUARIO;
        if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_EMISSOR').IsNull) then Params.ParamByName ('@pe_EmissorRG').Value := '-' else Params.ParamByName ('@pe_EmissorRG').Value := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_EMISSOR').AsString);
        if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CIDADE').IsNull) then Params.ParamByName ('@pe_Naturalidade').Value := '-' else Params.ParamByName ('@pe_Naturalidade').Value := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CIDADE').AsString);
        if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_NACIONALIDADE').IsNull) then Params.ParamByName ('@pe_Nacionalidade').Value := '-' else Params.ParamByName ('@pe_Nacionalidade').Value := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_NACIONALIDADE').AsString);
        Params.ParamByName ('@pe_DatHistorico').Value := StrToDate(mskDATA_ADMISSAO.Text);
        Execute;
      end;

      Application.MessageBox ('Processo de Admiss�o realizado com sucesso!! Dados do inscrito transferidos para o Sistema Deca.',
                             '[Sistema Deca] - Admiss�o realizada com sucesso!',
                             MB_OK + MB_ICONINFORMATION);

      //Atualiza os dados de demanda - convocados e selecionados
      try
         with dmTriagemDeca.cdsGERA_DEMANDA do
         begin
           Close;
           Params.ParamByName ('@pe_cod_regiao').Value        := Null;
           Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
           Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
           Params.ParamByName ('@pe_cod_unidade').Value       := Null;
           Params.ParamByName ('@pe_flg_situacao').Value      := 1;
           Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
           Params.ParamByName ('@pe_cod_situacao').Value      := Null;
           Params.ParamByName ('@pe_nom_nome').Value          := Null;
           Params.ParamByName ('@pe_flg_selecionado').Value   := 1;
           Open;

           Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de acordo com os crit�rios'),
                                  '[Sistema Deca] - Totlizador de consulta',
                                  MB_OK + MB_ICONINFORMATION);
         end;
      except end;
    except end;

    mskDATA_ADMISSAO.Clear;
    panFAMILIARES.Visible := False;

  end;
end;

procedure TfrmADMISSOES.btnFECHAR_FAMILIARESClick(Sender: TObject);
begin
  panFAMILIARES.Visible := False;
end;

procedure TfrmADMISSOES.dbgADMISSOESDblClick(Sender: TObject);
begin

  //"Limpar" os campos para receber nova admiss�o
  cmbUNIDADE_ADM.ItemIndex     := -1;
  cmbPERIODO_UNIDADE.ItemIndex := -1;
  //mskDATA_ADMISSAO.Text        := DateToStr(Date());
  edtCENTRO_CUSTO.Clear;
  edtMATRICULA.Clear;

  Panel2.Caption               := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').Value;
  cmbUNIDADE_ADM.ItemIndex     := cmbUNIDADE_ADM.Items.IndexOf(Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_UNIDADE').AsString));
  edtCENTRO_CUSTO.Text         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CCUSTO').Value;

  //Atribui automaticamente, quando poss�vel o per�odo escolar para admiss�o, em fun��o do encontrado como per�odo escolar
  if (Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ESCOLAPERIODO').Value) = 'Manh�') then
    cmbPERIODO_UNIDADE.ItemIndex := 1
  else if (Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ESCOLAPERIODO').Value) = 'Tarde') then
    cmbPERIODO_UNIDADE.ItemIndex := 0
  else if (Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ESCOLAPERIODO').Value) = 'Noite') then
    cmbPERIODO_UNIDADE.ItemIndex := 3
  else if (Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ESCOLAPERIODO').Value) = 'Integral') then
    cmbPERIODO_UNIDADE.ItemIndex := 2
  else cmbPERIODO_UNIDADE.ItemIndex := 2;

  try
    with dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar do
    begin
      Close;
      Params.ParamByName ('@Cod_membrofamilia').Value := Null;
      Params.ParamByName ('@Cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('COD_INSCRICAO').Value;
      Open;

      if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.RecordCount > 0) then
      begin
        panFAMILIARES.Visible := True;
        mskDATA_ADMISSAO.SetFocus;
      end
      else
      begin
        Application.MessageBox (PChar('N�o existem familiares cadastrados para o inscrito ' + ' - ' + UpperCase(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').Value)),
                               '[Sistema Deca] - Erro processo de Admiss�o: N�o h� familiares cadastrados!',
                               MB_OK + MB_ICONWARNING);
      end;
    end;
  except end;
end;

procedure TfrmADMISSOES.btnPESQUISA_NOMEDEMANDAClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := 1;  //Apenas os CONVOCADOS
      Params.ParamByName ('@pe_nom_nome').Value          := '%' + Trim(edtNOME_DEMANDA.Text) + '%';
      Params.ParamByName ('@pe_flg_selecionado').Value   := 1;
      Open;
      dbgADMISSOES.Refresh;
    end;
  except end;
end;

procedure TfrmADMISSOES.btnPESQUISA_TODADEMANDAClick(Sender: TObject);
begin
  edtNOME_DEMANDA.Clear;
  btnAPLICAR_FILTRO.Click;
end;

procedure TfrmADMISSOES.btnACESSAR_CADASTROClick(Sender: TObject);
begin
  //Transportar os dados da consulta para a ficha completa....
  with dmTriagemDeca.cdsSel_CadastroFull do
  begin
    Close;
    Params.ParamByName ('@pe_cod_inscricao').Value      := Null;
    Params.ParamByName ('@pe_num_inscricao').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_INSCRICAO').AsInteger;
    Params.ParamByName ('@pe_num_inscricao_digt').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_INSCRICAO_DIGT').AsInteger;
    Open;

    if (dmTriagemDeca.cdsSel_CadastroFull.RecordCount > 0) then
    begin
      //ShowMessage ('Registro encontrado...');
      Application.CreateForm(TfrmCadastroInscricoes, frmCadastroInscricoes);
      vvMODO_PESQUISA := True;
      frmCadastroInscricoes.ModoTela('S');
      frmCadastroInscricoes.ShowModal;
    end
    else
    begin
      Application.MessageBox ('N�o foram encontrados registros.' +#13+#10+
                              'Pedimos que refa�a sua busca verificando os dados digitados',
                              '[Sistema Deca] - Pesquisa Ficha de Inscri��o - Triagem',
                              MB_OK + MB_ICONINFORMATION);
    end;
  end;
end;

procedure TfrmADMISSOES.mskDATA_ADMISSAOExit(Sender: TObject);
begin
  ValidaDataAdmissao;
end;

procedure TfrmADMISSOES.ValidaDataAdmissao;
begin
  try
    StrToDate(mskDATA_ADMISSAO.Text);
  except
    on EConvertError do
    begin
      Application.MessageBox('Aten��o!!!' +#13+#10+
                             'A Data de Admiss�o deve estar corretamente informada!.' +#13+#10+
                             'Favor verificar e tentar novamente.',
                             '[Sistema Deca] - Data de Admiss�o INCORRETA',
                             MB_OK + MB_ICONWARNING);
      mskDATA_ADMISSAO.SetFocus;
    end;
  end;
end;

procedure TfrmADMISSOES.btnLIMPA_REGIAOClick(Sender: TObject);
begin
  cmbREGIAO.ItemIndex := -1;
end;

procedure TfrmADMISSOES.btnLIMPA_FAIXAETARIAClick(Sender: TObject);
begin
  cmbFAIXA_ETARIA.ItemIndex := -1;
end;

procedure TfrmADMISSOES.btnLIMPA_UNIDADEClick(Sender: TObject);
begin
  cmbUNIDADE.ItemIndex := -1;
end;

procedure TfrmADMISSOES.btnLIMPA_SITUACAODEMANDAClick(Sender: TObject);
begin
  cmbSITUACAO_DEMANDA.ItemIndex := -1;
end;

procedure TfrmADMISSOES.btnLIMPA_PERIODOESCOLARClick(Sender: TObject);
begin
  cmbPERIODO_ESCOLA.ItemIndex := -1;
end;

procedure TfrmADMISSOES.btnLIMPA_SITUACAOCADASTROClick(Sender: TObject);
begin
  cmbSITUACAO_CADASTRO.ItemIndex := -1;
end;

procedure TfrmADMISSOES.edtNOME_DEMANDAKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then btnPESQUISA_NOMEDEMANDA.Click;
end;

procedure TfrmADMISSOES.btnADMISSAO_CONTRATOClick(Sender: TObject);
var
   contrato, ficha, data, mmRESPONSAVEL, mmPAI, mmMAE, mmCPF_RESP, mmRG_RESP, mmMATRICULA, mmOCUPACAO_RESP : String;

begin

 if (Application.MessageBox (PChar('Deseja emitir o CONTRATO/FICHA do(a) aluno(a) ' + ' - ' + UpperCase(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').Value) + ' na Unidade ' + cmbUNIDADE_ADM.Text + ' para o Per�odo ' + cmbPERIODO_UNIDADE.Text + ' ?'),
                                    '[Sistema Deca] - EMITIR CONTRATO/FICHA',
                                    MB_YESNO + MB_ICONWARNING) ) = idYes then
  begin
    //Recuperar PAI, M�E e RESPONS�VEL PELO INSCRITO E TRANSPORTAR OS DADOS PARA O DOCUMENTO
    mmRESPONSAVEL   := '';
    mmPAI           := '';
    mmMAE           := '';
    mmCPF_RESP      := '';
    mmRG_RESP       := '';
    mmOCUPACAO_RESP := '';

    //Pega os dados do selecionado no DBGrid/Composi��o Familiar
    mmRESPONSAVEL   := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
    mmCPF_RESP      := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_CPF_fam').AsString;
    mmRG_RESP       := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_RG_fam').AsString;
    mmOCUPACAO_RESP := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').AsString;

    //Roda a procedure novamente para recuperar MAE e PAI
    with dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar do
    begin
      Close;
      Params.ParamByName ('@Cod_membrofamilia').Value := Null;
      Params.ParamByName ('@Cod_inscricao').Value     := Trim(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('COD_INSCRICAO').Value);
      Open;

      if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.RecordCount > 0) then
      begin
        dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.First;
        while not (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.eof) do
        begin
          //if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_responsavel').Value = 1) then
          //begin
          //  mmRESPONSAVEL := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
          //  mmCPF_RESP    := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_CPF_fam').AsString;
          //  mmRG_RESP     := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_RG_fam').AsString;
          //end;
          if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('cod_parentesco').Value = 1) then  //Recuperar o nome do pai
            mmPAI := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
          if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('cod_parentesco').Value = 2) then  //Recuperar o nome da m�e
            mmMAE := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('DSC_NOME_FAM').Value;
          dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.Next;
        end;
      end;
    end;

    Application.CreateForm(TrelCONTRATO, relCONTRATO);

    if chkOCULTAR_MATRICULA.Checked then relCONTRATO.qrMATRICULA.Caption  := '' else relCONTRATO.qrMATRICULA.Caption  := mmMATRICULA;
    relCONTRATO.qrNOME1.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').AsString;
    relCONTRATO.qrNOME2.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').AsString;
    relCONTRATO.qrNASCIMENTO.Caption := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DATA_NASCIMENTO').AsString;

    data := mskDATA_ADMISSAO.Text;
    data := (FORMATDATETIME('dd "de" mmmm "de" yyyy', StrToDate(data)));
    relCONTRATO.qrADMISSAO.Caption   := mskDATA_ADMISSAO.Text;
    relCONTRATO.qrINFO_DATA.Caption  := 'S�O JOS� DOS CAMPOS, ' + data + '.';

    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CPF').IsNull) then relCONTRATO.qrCPF_ALUNO.Caption := '' else relCONTRATO.qrCPF_ALUNO.Caption  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CPF').AsString;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_RG').IsNull) then relCONTRATO.qrRG_ALUNO.Caption := '' else relCONTRATO.qrRG_ALUNO.Caption  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_RG').AsString;
    if mmRESPONSAVEL = '' then relCONTRATO.qrRESPONSAVEL.Caption  := '' else relCONTRATO.qrRESPONSAVEL.Caption  := mmRESPONSAVEL;
    if mmRESPONSAVEL = '' then relCONTRATO.qrRESPONSAVEL2.Caption := '' else relCONTRATO.qrRESPONSAVEL2.Caption := mmRESPONSAVEL;

    relCONTRATO.qrCPF_RESPONSAVEL.Caption := mmCPF_RESP;
    relCONTRATO.qrRG_RESPONSAVEL.Caption  := mmRG_RESP;

    if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_estadocivil').IsNull)   then relCONTRATO.qrESTADO_CIVIL.Caption    := '' else relCONTRATO.qrESTADO_CIVIL.Caption    := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_estadocivil').AsString;

    //if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').IsNull)  then relCONTRATO.qrPROFISSAO.Caption       := '' else relCONTRATO.qrPROFISSAO.Caption       := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').AsString;
    relCONTRATO.qrPROFISSAO.Caption := mmOCUPACAO_RESP;
    
    if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_nacionalidade').IsNull) then relCONTRATO.qrNACIONALIDADE.Caption   := '' else relCONTRATO.qrNACIONALIDADE.Caption   := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_nacionalidade').AsString;

    relCONTRATO.qrENDERECO.Caption   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ENDERECO').AsString + ', ' + dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_NUMRESIDENCIA').AsString + '  -  ' +
                                        dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_COMPLEMENTO').AsString + ' | ' + dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').AsString;
    relCONTRATO.Preview;
    relCONTRATO.Free;

    Application.CreateForm(TrelFICHA_ADMISSAO, relFICHA_ADMISSAO);
    //relFICHA_ADMISSAO.qrTITULO_FICHA.Caption            := 'FICHA DE ADMISS�O - CADASTRO DE ' + rdgTIPO_ADMISSAO.Caption;
    case rdgTIPO_ADMISSAO.ItemIndex of
      0: relFICHA_ADMISSAO.qrTITULO_FICHA.Caption := 'FICHA DE ADMISS�O - CADASTRO DE CRIAN�A/BOLSISTA';
      1: relFICHA_ADMISSAO.qrTITULO_FICHA.Caption := 'FICHA DE ADMISS�O - CADASTRO DE ADOLESCENTE';
    end;

    //relFICHA_ADMISSAO.qrMATRICULA.Caption               := mmMATRICULA; //Trim(edtMATRICULA.Text);
    if chkOCULTAR_MATRICULA.Checked then relFICHA_ADMISSAO.qrMATRICULA.Caption  := '' else relFICHA_ADMISSAO.qrMATRICULA.Caption  := mmMATRICULA;
    relFICHA_ADMISSAO.qrNOME.Caption                    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NOM_NOME').AsString;
    relFICHA_ADMISSAO.qrNASCIMENTO.Caption              := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DATA_NASCIMENTO').AsString;
    relFICHA_ADMISSAO.qrCRACHA.Caption                  := '';
    relFICHA_ADMISSAO.qrENDERECO.Caption                := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ENDERECO').AsString + ', ' + dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_NUMRESIDENCIA').AsString;

    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_COMPLEMENTO').IsNull) then relFICHA_ADMISSAO.qrCOMPLEMENTO.Caption      := '' else relFICHA_ADMISSAO.qrCOMPLEMENTO.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_COMPLEMENTO').AsString;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').IsNull)      then relFICHA_ADMISSAO.qrBAIRRO.Caption           := '' else relFICHA_ADMISSAO.qrBAIRRO.Caption           := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_BAIRRO').AsString;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CEP').IsNull)         then relFICHA_ADMISSAO.qrCEP.Caption              := '' else relFICHA_ADMISSAO.qrCEP.Caption              := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CEP').AsString;
    //if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('MAE').IsNull)             then relFICHA_ADMISSAO.qrNOME_MAE.Caption         := '' else relFICHA_ADMISSAO.qrNOME_MAE.Caption         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('MAE').AsString;
    relFICHA_ADMISSAO.qrNOME_MAE.Caption         := mmMAE;
    //if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('PAI').IsNull)             then relFICHA_ADMISSAO.qrNOME_PAI.Caption         := '' else relFICHA_ADMISSAO.qrNOME_PAI.Caption         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('PAI').AsString;
    relFICHA_ADMISSAO.qrNOME_PAI.Caption         := mmPAI;
    //if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('RESPONSAVEL').IsNull)     then relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := '' else relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('RESPONSAVEL').AsString;
    relFICHA_ADMISSAO.qrRESPONSAVEL.Caption      := mmRESPONSAVEL;
    if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CIDADE').IsNull)      then relFICHA_ADMISSAO.qrLOCAL_NASCIMENTO.Caption := '' else relFICHA_ADMISSAO.qrLOCAL_NASCIMENTO.Caption := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CIDADE').AsString;

    relFICHA_ADMISSAO.qrMUNICIPIO_RESIDE.Caption        := 'S�O JOS� DOS CAMPOS/SP';

    if ( (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vTelefone').IsNull) or (dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vTelefone').AsString = '(99) 9999-9999') ) then
      relFICHA_ADMISSAO.qrTELEFONE.Caption              := ''
    else relFICHA_ADMISSAO.qrTELEFONE.Caption           := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('vTelefone').AsString;
    relFICHA_ADMISSAO.qrCELULAR.Caption                 := '';
    relFICHA_ADMISSAO.qrCONJUGE.Caption                 := '';
    relFICHA_ADMISSAO.qrESTADO_CIVIL.Caption            := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_ESTADOCIVIL').AsString;
    relFICHA_ADMISSAO.qrMORA_SIM.Caption                := 'SIM';
    relFICHA_ADMISSAO.qrMORA_NAO.Caption                := 'N�O';
    relFICHA_ADMISSAO.qrNACIONALIDADE.Caption           := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_NACIONALIDADE').AsString;
    relFICHA_ADMISSAO.qrGRAU_INSTRUCAO.Caption          := '';
    relFICHA_ADMISSAO.qrRACA.Caption                    := '';
    relFICHA_ADMISSAO.qrCABELOS.Caption                 := '';
    relFICHA_ADMISSAO.qrOLHOS.Caption                   := '';
    relFICHA_ADMISSAO.qrSANGUE.Caption                  := '';
    relFICHA_ADMISSAO.qrPESO.Caption                    := '';
    relFICHA_ADMISSAO.qrALTURA.Caption                  := '';
    relFICHA_ADMISSAO.qrCALCADO.Caption                 := '';
    relFICHA_ADMISSAO.qrFUMA_SIM.Caption                := 'SIM';
    relFICHA_ADMISSAO.qrFUMA_NAO.Caption                := 'N�O';
    relFICHA_ADMISSAO.qrGUARDAREGULAR_SIM.Caption       := 'SIM';
    relFICHA_ADMISSAO.qrGUARDAREGULAR_NAO.Caption       := 'N�O';
    relFICHA_ADMISSAO.qrNECESSIDADES_SIM.Caption        := 'SIM';
    relFICHA_ADMISSAO.qrNECESSIDADES_NAO.Caption        := 'N�O';
    relFICHA_ADMISSAO.qrQUAIS_NECESSIDADES.Caption      := '';
    relFICHA_ADMISSAO.qrCENTRO_CUSTO_ATUAL.Caption      := '2027 - SE��O TRIAGEM';
    relFICHA_ADMISSAO.qrCENTRO_CUSTO_DESTINO.Caption    := Trim(edtCENTRO_CUSTO.Text) + ' - ' + cmbUNIDADE_ADM.Text;
    relFICHA_ADMISSAO.qrRG.Caption                      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_RG').AsString;
    relFICHA_ADMISSAO.qrDATA_EMISSAO_RG.Caption         := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DAT_EMISSAO_RG').AsString;
    relFICHA_ADMISSAO.qrEMISSOR.Caption                 := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_EMISSOR').AsString;
    relFICHA_ADMISSAO.qrUF.Caption                      := '';
    relFICHA_ADMISSAO.qrCPF.Caption                     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_CPF').AsString;
    relFICHA_ADMISSAO.qrCERTIDAO_NASCIMENTO.Caption     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('DSC_NUMCERTNASC').AsString;
    relFICHA_ADMISSAO.qrTITULO_ELEITOR.Caption          := '';
    relFICHA_ADMISSAO.qrZONA_ELEITORAL.Caption          := '';
    relFICHA_ADMISSAO.qrSECAO_ELEITORAL.Caption         := '';
    relFICHA_ADMISSAO.qrCTPS.Caption                    := '';
    relFICHA_ADMISSAO.qrSERIE_CTPS.Caption              := '';
    relFICHA_ADMISSAO.qrEMISSAO_CTPS.Caption            := '';
    relFICHA_ADMISSAO.qrUF_CTPS.Caption                 := '';

    relFICHA_ADMISSAO.qrPIS.Caption                     := '';
    relFICHA_ADMISSAO.qrPIS_EMISSAO.Caption             := '';
    relFICHA_ADMISSAO.qrPIS_BANCO.Caption               := '';

    relFICHA_ADMISSAO.qrSUS.Caption                     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_CARTAO_SUS').AsString;
    relFICHA_ADMISSAO.qrSIAS.Caption                    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('COD_SIAS').AsString;
    relFICHA_ADMISSAO.qrRESERVISTA.Caption              := '';
    relFICHA_ADMISSAO.qrCSM.Caption                     := '';
    relFICHA_ADMISSAO.qrNATURALIZADO_SIM.Caption        := 'SIM';
    relFICHA_ADMISSAO.qrNATURALIZADO_NAO.Caption        := 'N�O';
    relFICHA_ADMISSAO.qrDATA_CHEGA_BRASIL.Caption       := '';
    relFICHA_ADMISSAO.qrCART_N19.Caption                := '';
    relFICHA_ADMISSAO.qrQTD_FILHOS_BRASIL.Caption       := '';
    relFICHA_ADMISSAO.qrTOTAL_FILHOS.Caption            := '';
    relFICHA_ADMISSAO.qrNUM_REG_GERAL.Caption           := '';
    relFICHA_ADMISSAO.qrVALIDAE_REGISTRO.Caption        := '';
    relFICHA_ADMISSAO.qrVALIDADE_CTPS.Caption           := '';

    relFICHA_ADMISSAO.qrPERIODO_ATIVIDADE.Caption       := '';
    relFICHA_ADMISSAO.qrPRIMEIRO_EMPREGO.Caption        := '';
    relFICHA_ADMISSAO.qrTERMINO_CONTRATO.Caption        := '';
    relFICHA_ADMISSAO.qrEXAME_MEDICO.Caption            := '';
    relFICHA_ADMISSAO.qrCONTRATADO_SIM.Caption          := '';
    relFICHA_ADMISSAO.qrCONTRATADO_NAO.Caption          := '';
    relFICHA_ADMISSAO.qrNUM_AGENCIA.Caption             := '';
    relFICHA_ADMISSAO.qrNUM_CONTA.Caption               := '';
    relFICHA_ADMISSAO.qrSALARIO_BASE.Caption            := '';
    relFICHA_ADMISSAO.qrFUNCAO.Caption                  := '';
    relFICHA_ADMISSAO.qrHORARIO.Caption                 := '';
    relFICHA_ADMISSAO.qrNECESSITA_PASSE_SIM.Caption     := 'SIM';
    relFICHA_ADMISSAO.qrNECESSITA_PASSE_NAO.Caption     := 'N�O';
    relFICHA_ADMISSAO.qrCONTRIBUI_SINDICATO_SIM.Caption := 'SIM';
    relFICHA_ADMISSAO.qrCONTRIBUI_SINDICATO_NAO.Caption := 'N�O';

    data := trim(mskDATA_ADMISSAO.Text);
    data := (FORMATDATETIME('dd "de" mmmm "de" yyyy', StrToDate(data)));
    relFICHA_ADMISSAO.qrADMISSAO.Caption   := Trim(mskDATA_ADMISSAO.Text);
    relFICHA_ADMISSAO.qrINFO_DATA.Caption  := 'S�O JOS� DOS CAMPOS, ' + data + '.';

    relFICHA_ADMISSAO.Preview;
    relFICHA_ADMISSAO.Free;

  end;

end;

end.
