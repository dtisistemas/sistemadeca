object frmUnidades: TfrmUnidades
  Left = 322
  Top = 194
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Cadastro de Unidades]'
  ClientHeight = 453
  ClientWidth = 709
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 4
    Top = 400
    Width = 701
    Height = 49
    TabOrder = 0
    object btnNovoU: TBitBtn
      Left = 355
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para NOVA advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoUClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvarU: TBitBtn
      Left = 429
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR a advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarUClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelarU: TBitBtn
      Left = 562
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarUClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSairU: TBitBtn
      Left = 632
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSairUClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnAlterarU: TBitBtn
      Left = 487
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnAlterarUClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF7F3F3FBF7F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBF7F3FBFBF9FFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
        3F3FBF9F5FBFBF7F9F3F3F7F3F3FBF7F3FBF9F5FFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBFDF9FBFBF7FBFBF7FBFBF7FBF9F
        5F9F3F3FBF9F5FFFFFFFFFFFFF7FBFBF3F7F9F7FBFBFFFFFFFFFFFFFFFFFFF9F
        3F3FBF9F5FBFDF9FBF7F3F9F3F3FBF7F3FBFBF7F9F3F3FBF9F5F7FBFBF3F7F9F
        7FBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3F3FBF9F5FBF7F3FBFBF9FBF9F
        5F9F3F3FBFBF7FBF7F3F5F9F9F3F7F9FBFDFDFFFFFFFFFFFFF3F5F7FFFFFFFFF
        FFFFFFFFFF9F3F3FBF7F3FFFFFFFFFFFFFBF9F5FBF9F5F9F3F3F3F7F7F3FBFFF
        3F9FDF9FDFDFFFFFFF3F5F7F3F5F7FFFFFFFFFFFFFFFFFFF9F3F3FFFFFFFFFFF
        FFFFFFFF9F3F3FBF7F3F5F9F9F3FDFFF3F5F7F7FBFBF9FDFDF3F5F7F3F9FDF3F
        5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBF9F9F3F3FBFBF9F7FBFBF3F7F7F
        3FFFFF3F7F9F3F5F7F3F7F9F3FDFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFBFBF
        9F9F3F3FBFBF9FFFFFFFFFFFFF5F9F9F3F7F7F3FFFFF3FFFFF3FFFFF3FFFFF3F
        FFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        7FBFBF5F9F9F3F7F7F5F9F9F9FFFFFFFFFFF3F7F7FFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFF3F
        7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF3F7F7F3F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnSecoes: TBitBtn
      Left = 211
      Top = 10
      Width = 118
      Height = 30
      Hint = 'Clique para NOVA advert�ncia'
      Caption = 'Lan�ar Se��es'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnSecoesClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
  end
  object Panel1: TPanel
    Left = 4
    Top = 3
    Width = 701
    Height = 202
    TabOrder = 1
    object Label1: TLabel
      Left = 207
      Top = 16
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Unidade:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 25
      Top = 17
      Width = 107
      Height = 13
      Alignment = taRightJustify
      Caption = 'Centro Custo reduzido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 95
      Top = 39
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'Regi�o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 235
      Top = 38
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nome Gestor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 83
      Top = 61
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Endere�o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 433
      Top = 61
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Complemento:'
    end
    object Label7: TLabel
      Left = 102
      Top = 83
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Bairro:'
    end
    object Label8: TLabel
      Left = 447
      Top = 82
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'CEP:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 75
      Top = 128
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Telefone(1):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 247
      Top = 128
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Telefone(2):'
    end
    object Label11: TLabel
      Left = 99
      Top = 106
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = 'e-Mail :'
    end
    object Label12: TLabel
      Left = 73
      Top = 149
      Width = 60
      Height = 13
      Alignment = taRightJustify
      Caption = 'Capacidade:'
    end
    object Label13: TLabel
      Left = 204
      Top = 150
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 392
      Top = 150
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Situa��o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 89
      Top = 172
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inclus�o:'
    end
    object Label16: TLabel
      Left = 228
      Top = 172
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Altera��o:'
    end
    object edNomeUnidade: TEdit
      Left = 255
      Top = 13
      Width = 417
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      TabOrder = 0
    end
    object edCcusto1: TEdit
      Left = 137
      Top = 13
      Width = 58
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      MaxLength = 4
      TabOrder = 1
    end
    object cbRegiao: TComboBox
      Left = 137
      Top = 35
      Width = 92
      Height = 22
      Style = csOwnerDrawFixed
      Color = clInfoBk
      ItemHeight = 16
      TabOrder = 2
      Items.Strings = (
        'CENTRO'
        'NORTE'
        'SUL'
        'LESTE'
        'OESTE')
    end
    object edNomeGestor: TEdit
      Left = 305
      Top = 35
      Width = 367
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      TabOrder = 3
    end
    object edEndereco: TEdit
      Left = 137
      Top = 58
      Width = 290
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      TabOrder = 4
    end
    object edComplemento: TEdit
      Left = 505
      Top = 57
      Width = 168
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 5
    end
    object edBairro: TEdit
      Left = 137
      Top = 80
      Width = 305
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
    end
    object edCep: TMaskEdit
      Left = 476
      Top = 79
      Width = 81
      Height = 21
      Color = clInfoBk
      EditMask = '99.999-999'
      MaxLength = 10
      TabOrder = 7
      Text = '  .   -   '
    end
    object edTelefone1: TMaskEdit
      Left = 137
      Top = 124
      Width = 101
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      EditMask = '(99) 9999-9999'
      MaxLength = 14
      TabOrder = 8
      Text = '(  )     -    '
    end
    object edTelefone2: TMaskEdit
      Left = 309
      Top = 124
      Width = 101
      Height = 21
      CharCase = ecUpperCase
      EditMask = '(99) 9999-9999'
      MaxLength = 14
      TabOrder = 9
      Text = '(  )     -    '
    end
    object edEmail: TEdit
      Left = 137
      Top = 102
      Width = 293
      Height = 21
      CharCase = ecLowerCase
      TabOrder = 10
    end
    object edCapacidade: TEdit
      Left = 137
      Top = 146
      Width = 64
      Height = 21
      Color = clInfoBk
      TabOrder = 11
    end
    object cbTipo: TComboBox
      Left = 232
      Top = 147
      Width = 158
      Height = 19
      Style = csOwnerDrawFixed
      Color = clInfoBk
      ItemHeight = 13
      TabOrder = 12
      Items.Strings = (
        'CRIAN�A'
        'ADOLESCENTE'
        'MISTA')
    end
    object cbSituacao: TComboBox
      Left = 441
      Top = 147
      Width = 130
      Height = 19
      Style = csOwnerDrawFixed
      Color = clInfoBk
      ItemHeight = 13
      TabOrder = 13
      Items.Strings = (
        'Ativa'
        'Inativa')
    end
    object edDataUltAtualizacao1: TMaskEdit
      Left = 137
      Top = 168
      Width = 85
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 14
      Text = '  /  /    '
    end
    object MaskEdit1: TMaskEdit
      Left = 279
      Top = 168
      Width = 85
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 15
      Text = '  /  /    '
    end
  end
  object Panel3: TPanel
    Left = 4
    Top = 207
    Width = 701
    Height = 191
    Caption = 'Panel3'
    TabOrder = 2
    object dbgUnidade: TDBGrid
      Left = 6
      Top = 7
      Width = 684
      Height = 178
      DataSource = dsUnidade
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dbgUnidadeCellClick
      OnDblClick = dbgUnidadeDblClick
      OnKeyDown = dbgUnidadeKeyDown
      OnKeyUp = dbgUnidadeKeyUp
      Columns = <
        item
          Expanded = False
          FieldName = 'nom_unidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Caption = 'Unidade'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 232
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'vTipo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Tipo'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 65
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'vRegiao'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Regi�o'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 50
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_ccusto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Centro Custo'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_gestor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Caption = 'Gestor(a)'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = []
          Width = 200
          Visible = True
        end>
    end
  end
  object dsUnidade: TDataSource
    DataSet = dmDeca.cdsSel_Unidade
    Left = 600
    Top = 336
  end
end
