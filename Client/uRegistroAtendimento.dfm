object frmRegistroAtendimento: TfrmRegistroAtendimento
  Left = 315
  Top = 233
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Registro de Atendimento]'
  ClientHeight = 344
  ClientWidth = 721
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 705
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR crian�a/adolescente'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnChange = txtMatriculaChange
      OnExit = txtMatriculaExit
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 64
    Width = 705
    Height = 217
    TabOrder = 1
    object Label2: TLabel
      Left = 16
      Top = 65
      Width = 125
      Height = 13
      Caption = 'Descri��o do Atendimento'
    end
    object Label3: TLabel
      Left = 35
      Top = 20
      Width = 83
      Height = 13
      Caption = 'Data do Registro:'
    end
    object Label5: TLabel
      Left = 51
      Top = 44
      Width = 65
      Height = 13
      Caption = 'Classifica��o:'
    end
    object Label4: TLabel
      Left = 202
      Top = 19
      Width = 101
      Height = 13
      Caption = 'Tipo do Atendimento:'
    end
    object txtOcorrencia: TMemo
      Left = 16
      Top = 83
      Width = 673
      Height = 120
      Hint = 'Descri��o do Registro de Atendimento'
      Lines.Strings = (
        '')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object txtData: TMaskEdit
      Left = 122
      Top = 17
      Width = 75
      Height = 22
      Hint = 'DATA do Registro de Atendimento'
      EditMask = '99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      OnExit = txtDataExit
    end
    object cbClassificacao: TComboBox
      Left = 121
      Top = 42
      Width = 315
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'Visita Domiciliar'
        'Projeto de Vida'
        'Atendimento Individual'
        'Entrevista Inicial'
        'Abordagem Grupal'
        'Reuniao Informativa'
        'Integracao Familia/Instituicao'
        'Treinamento: Convenio/Estagio'
        'Atendimento a Comunidade'
        'Supervisao de Estagio'
        'Relatorios Diversos'
        'Reunioes Tecnicas'
        'Curso de Capacitacao'
        'Visita T�cnica'
        'Visita Institucional'
        'Visita na Rede'
        'Discussao de Casos - Equipe Interna'
        'Discussao de Casos - Equipe Multidisciplinar/Plant�o'
        'Discussao de Casos - Rede de Servi�os'
        'Atividades Extra-Projeto/Bloco/Unidade'
        'Registros Diversos')
    end
    object cbTipo: TComboBox
      Left = 305
      Top = 17
      Width = 131
      Height = 19
      Style = csOwnerDrawFixed
      Enabled = False
      ItemHeight = 13
      TabOrder = 3
      Items.Strings = (
        'Crian�a'
        'Adolescente')
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 288
    Width = 705
    Height = 49
    TabOrder = 2
    object btnNovo: TBitBtn
      Left = 343
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para lan�ar NOVO Registro de Atendimento para a crian�a/a' +
        'dolescente'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 418
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 478
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR a opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnImprimir: TBitBtn
      Left = 553
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para EXIBIR/IMPRIMIR os dados do Registro de Atendimento'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Visible = False
      OnClick = btnImprimirClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777776B00777777777000777777776E007777777007880077777771007777
        7007780088007777740077770778878800880077770077778887778888008077
        7A00777887777788888800777D007778F7777F888888880780007778F77FF777
        8888880783007778FFF779977788880786007778F77AA7778807880789007777
        88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
        920077777777778FFFFFF0079500777777777778FFF887779800777777777777
        888777779B00777777777777777777779E0077777777777777777777A1007777
        7777777777777777A400}
    end
    object btnSair: TBitBtn
      Left = 628
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de Registro de Atendimento'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
end
