unit rAtestadoConvenio;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelAtestadoConvenio = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel13: TQRLabel;
    lbNome: TQRLabel;
    QRLabel3: TQRLabel;
    lbNumRA: TQRLabel;
    QRLabel5: TQRLabel;
    lbUnidade: TQRLabel;
    QRLabel7: TQRLabel;
    lbEmpresa: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRBand1: TQRBand;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
  private

  public

  end;

var
  relAtestadoConvenio: TrelAtestadoConvenio;

implementation

{$R *.DFM}

end.
