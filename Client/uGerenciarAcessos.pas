unit uGerenciarAcessos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Grids, DBGrids, ExtCtrls, Buttons, Mask, Db, jpeg,
  ImgList;

type
  TfrmGerenciarAcessos = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Panel1: TPanel;
    dbgAcessos: TDBGrid;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnPesquisarUsuario: TSpeedButton;
    edNomeFuncionario: TEdit;
    cbUnidade: TComboBox;
    cbPerfil: TComboBox;
    Label4: TLabel;
    edSenha1: TEdit;
    Label5: TLabel;
    edSenha2: TEdit;
    PageControl3: TPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    PageControl4: TPageControl;
    TabSheet9: TTabSheet;
    Panel4: TPanel;
    dbgUnidades: TDBGrid;
    PageControl5: TPageControl;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    Panel8: TPanel;
    Panel10: TPanel;
    Label7: TLabel;
    edUnidade: TEdit;
    Label6: TLabel;
    Label8: TLabel;
    edGestor: TEdit;
    Label9: TLabel;
    edAssistenteSocial: TEdit;
    Label10: TLabel;
    edEndereco: TEdit;
    Label11: TLabel;
    edComplemento: TEdit;
    Label12: TLabel;
    edBairro: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    edEmail: TEdit;
    Label17: TLabel;
    edCapacidade: TEdit;
    Label18: TLabel;
    cbRegiao: TComboBox;
    Label19: TLabel;
    cbTipoUnidade: TComboBox;
    Label20: TLabel;
    cbDivisao: TComboBox;
    cbTipoGestao: TComboBox;
    Label21: TLabel;
    mskCcusto: TMaskEdit;
    mskCep: TMaskEdit;
    mskFone1: TMaskEdit;
    mskFone2: TMaskEdit;
    Panel6: TPanel;
    dbgPerfis: TDBGrid;
    GroupBox1: TGroupBox;
    edPerfil: TEdit;
    Panel11: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    edNomeCompleto: TEdit;
    edEnderecoUsuario: TEdit;
    edComplementoEndUsuario: TEdit;
    edBairroUsuario: TEdit;
    edEmailUsuario: TEdit;
    mskMatricula: TMaskEdit;
    mskCepUsuario: TMaskEdit;
    mskFone1Usuario: TMaskEdit;
    mskFone2Usuario: TMaskEdit;
    Label24: TLabel;
    edMunicipioUsuario: TEdit;
    Label25: TLabel;
    edUfUsuario: TEdit;
    Label33: TLabel;
    cbSituacaoUsuario: TComboBox;
    dsAcessos: TDataSource;
    dsUnidades: TDataSource;
    dsPerfis: TDataSource;
    dsUsuarios: TDataSource;
    edMatricula: TEdit;
    TabSheet10: TTabSheet;
    Label36: TLabel;
    edDescricaoPerfil: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    chkTreinado: TCheckBox;
    Label35: TLabel;
    Label37: TLabel;
    datNascimento: TDateTimePicker;
    datCadastro: TDateTimePicker;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Panel7: TPanel;
    btnSairUsuario: TSpeedButton;
    btnNovoUsuario: TSpeedButton;
    btnIncluirUsuario: TSpeedButton;
    btnAlterarUsuario: TSpeedButton;
    btnCancelarOpUsuario: TSpeedButton;
    btnVerDadosUsuario: TSpeedButton;
    btnLiberarAcesso: TSpeedButton;
    Panel9: TPanel;
    btnSairPerfil: TSpeedButton;
    btnNovoPerfil: TSpeedButton;
    btnIncluirPerfil: TSpeedButton;
    btnAlterarPerfil: TSpeedButton;
    btnCancelarOpPerfil: TSpeedButton;
    btnVerDadosPerfil: TSpeedButton;
    Label34: TLabel;
    meAtividadesPerfil: TMemo;
    Panel5: TPanel;
    btnSairUnidade: TSpeedButton;
    btnNovaUnidade: TSpeedButton;
    btnIncluirUnidade: TSpeedButton;
    btnAlterarUnidade: TSpeedButton;
    btnCancelarOpUnidade: TSpeedButton;
    btnVerDadosUnidade: TSpeedButton;
    Panel3: TPanel;
    btnSairAcesso: TSpeedButton;
    btnNovoAcesso: TSpeedButton;
    btnIncluirAcesso: TSpeedButton;
    btnAlterarAcesso: TSpeedButton;
    btnCancelarOpAcesso: TSpeedButton;
    btnVerDadosAcesso: TSpeedButton;
    TabSheet13: TTabSheet;
    btnDesativarUnidade: TSpeedButton;
    GroupBox3: TGroupBox;
    meDescricaoFuncoes: TMemo;
    PageControl6: TPageControl;
    TabSheet14: TTabSheet;
    TabSheet15: TTabSheet;
    dbgUsuarios: TDBGrid;
    dbgAcessosUsuario: TDBGrid;
    imgTreinamentos: TImage;
    Panel12: TPanel;
    Image3: TImage;
    btnHistoricoAcesso: TSpeedButton;
    Label38: TLabel;
    cbSituacaoAcesso: TComboBox;
    ImageList1: TImageList;
    dbgAcessosCadastradosUsuario: TDBGrid;
    lbTituloAcessos: TLabel;
    GroupBox4: TGroupBox;
    btnVerResultados: TSpeedButton;
    edCodMatricula: TEdit;
    btnLocalizarUsuario: TSpeedButton;
    edNomeUsuario: TEdit;
    procedure FormShow(Sender: TObject);
    procedure LimpaCamposUnidade (Modo: String);
    procedure btnNovoUClick(Sender: TObject);
    procedure btnCancelarUClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure dbgUnidadesDblClick(Sender: TObject);
    procedure mskCcustoExit(Sender: TObject);
    procedure btnSalvarUClick(Sender: TObject);
    procedure btnSairAcessoClick(Sender: TObject);
    procedure ModoTelaAcessos (Modo: String);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNovoAcessoClick(Sender: TObject);
    procedure btnCancelarOpAcessoClick(Sender: TObject);
    procedure btnGravarAcessoClick(Sender: TObject);
    procedure btnDesativarUnidadeClick(Sender: TObject);
    procedure btnCancelarOpUnidadeClick(Sender: TObject);
    procedure PageControl3Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNovaUnidadeClick(Sender: TObject);
    procedure btnIncluirUnidadeClick(Sender: TObject);
    procedure btnAlterarUnidadeClick(Sender: TObject);
    procedure dbgUnidadesCellClick(Column: TColumn);
    procedure ModoTelaPerfis(Modo:String);
    procedure btnNovoPerfilClick(Sender: TObject);
    procedure PageControl4Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnCancelarOpPerfilClick(Sender: TObject);
    procedure btnIncluirPerfilClick(Sender: TObject);
    procedure btnAlterarPerfilClick(Sender: TObject);
    procedure dbgPerfisDblClick(Sender: TObject);
    procedure ModoTelaUsuarios(Modo:String);
    procedure PageControl5Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure PageControl6Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNovoUsuarioClick(Sender: TObject);
    procedure btnCancelarOpUsuarioClick(Sender: TObject);
    procedure btnSairUnidadeClick(Sender: TObject);
    procedure btnSairPerfilClick(Sender: TObject);
    procedure btnSairUsuarioClick(Sender: TObject);
    procedure btnIncluirUsuarioClick(Sender: TObject);
    procedure dbgUsuariosDblClick(Sender: TObject);
    procedure btnAlterarUsuarioClick(Sender: TObject);
    procedure dbgUsuariosCellClick(Column: TColumn);
    procedure PageControl2Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnHistoricoAcessoClick(Sender: TObject);
    procedure dbgAcessosDblClick(Sender: TObject);
    procedure btnIncluirAcessoClick(Sender: TObject);
    procedure btnAlterarAcessoClick(Sender: TObject);
    procedure dbgAcessosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnVerResultadosClick(Sender: TObject);
    procedure btnVerDadosAcessoClick(Sender: TObject);
    procedure dbgAcessosCadastradosUsuarioDblClick(Sender: TObject);
    procedure btnPesquisarUsuarioClick(Sender: TObject);
    procedure btnLocalizarUsuarioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerenciarAcessos: TfrmGerenciarAcessos;
  vvMUDA_ABA_UNIDADE,
  vvMUDA_ABA_PERFIL,
  vvMUDA_ABA_USUARIO,
  vvMUDA_ABA_ACESSO : Boolean;
  vvMUDA_ABA : Boolean;
  vUnidadeAcesso1, vUnidadeAcesso2 : TStringList;
  vPerfilAcesso1, vPerfilAcesso2 : TStringList;
  mmCOD_USUARIO : String;

implementation

uses uDM, uUtil, uUsuarios, uPrincipal, uPesquisaUsuarios;

{$R *.DFM}

procedure TfrmGerenciarAcessos.FormShow(Sender: TObject);
begin

  //****************************************************************************
  //UNIDADES...
  LimpaCamposUnidade('P');
  vvMUDA_ABA := True;
  ModoTelaAcessos('P');

  PageControl1.ActivePageIndex := 0; //Inciciar pela Aba Acessos...
  PageControl2.ActivePageIndex := 0; //Acessos Cadastrados...

  vUnidadeAcesso1 := TStringList.Create();
  vUnidadeAcesso2 := TStringList.Create();

  //LimpaCamposUnidade('P');
  //Atualiza o grid com os dados das Unidades
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
      dbgUnidades.Refresh;

      //Carrega a lista de unidades no combo cbUnidade/Aba Acessos
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);  //Campo Unidade na tela de Cadastro de Acessos...
        vUnidadeAcesso1.Add (IntToStr(FieldByName ('cod_unidade').Value));
        vUnidadeAcesso2.Add (FieldByName ('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end;

    end;
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar os dados das unidades...',
                            '[Erro]-Cadastro de Unidades',
                            MB_OK + MB_ICONWARNING);

  end;
  //****************************************************************************


  //****************************************************************************
  //PERFIS...
  ModoTelaPerfis('P');
  PageControl4.ActivePageIndex := 0;
  vvMUDA_ABA_PERFIL := False;
  vPerfilAcesso1 := TStringList.Create();
  vPerfilAcesso2 := TStringList.Create();
  //Atualiza o grid com os dados dos perfis...
  try
    with dmDeca.cdsSel_Perfil do
    begin
      Close;
      Params.ParamByName ('@pe_cod_perfil').Value := Null;
      Open;
      dbgPerfis.Refresh;

      while not dmDeca.cdsSel_Perfil.eof do
      begin
        vPerfilAcesso1.Add (IntToStr(FieldByName ('cod_perfil').Value));
        vPerfilAcesso2.Add (FieldByName ('dsc_perfil').Value);
        cbPerfil.Items.Add (FieldByName ('dsc_perfil').Value);
        dmDeca.cdsSel_Perfil.Next;
      end;
    end;
  except end;
  //****************************************************************************

  //****************************************************************************
  //USU�RIOS...
  ModoTelaUsuarios('P');
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  vvMUDA_ABA_USUARIO := False;

  //Atualizar os dados do grid de usu�rios...
  with dmDeca.cdsSel_Usuario do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value   := Null;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_flg_situacao').Value  := Null;
    Open;
  end;

  //****************************************************************************
  //ACESSOS...
  ModoTelaAcessos('P');
  PageControl2.ActivePageIndex := 0;
  vvMUDA_ABA_ACESSO := False;
  vvMUDA_ABA := True;

  //Atualizar os dados dos acessos cadastrados...
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value   := Null;
      Params.ParamByName ('@pe_cod_perfil').Value    := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := Null;
      Params.ParamByName ('@pe_flg_ativo').Value     := Null;
      Params.ParamByName ('@pe_flg_online').Value    := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Open;
      dbgAcessos.Refresh;
    end;
  except end;

end;

procedure TfrmGerenciarAcessos.LimpaCamposUnidade(Modo: String);
begin
  if Modo = 'P' then
  begin

    PageControl1.ActivePageIndex := 1; //Aba Unidades
    PageControl3.ActivePageIndex := 0; //Dentro da aba Unidades, posiciona-se na aba Unidades Cadastradas
    vvMUDA_ABA_UNIDADE := False;

    //Campos da Aba Nova Unidade
    mskCcusto.Clear;
    edUnidade.Clear;
    edGestor.Clear;
    edAssistenteSocial.Clear;
    edEndereco.Clear;
    edComplemento.Clear;
    mskCep.Clear;
    mskFone1.Clear;
    mskFone2.Clear;
    edEmail.Clear;
    edCapacidade.Clear;
    cbRegiao.ItemIndex := -1;
    cbTipoUnidade.ItemIndex := -1;
    cbDivisao.ItemIndex := -1;
    cbTipoGestao.ItemIndex := -1;
    Panel10.Enabled := False;

    //Bot�es
    Panel5.Enabled := True;
    btnDesativarUnidade.Enabled  := False;
    btnNovaUnidade.Enabled       := True;
    btnIncluirUnidade.Enabled    := False;
    btnAlterarUnidade.Enabled    := False;
    btnCancelarOpUnidade.Enabled := False;
    btnVerDadosUnidade.Enabled   := True;
    btnSairUnidade.Enabled       := True;

  end
  else if Modo = 'N' then
  begin

    vvMUDA_ABA_UNIDADE := True;
    PageControl3.ActivePageIndex := 1; //Dentro da aba Unidades, posiciona-se na aba Unidades Cadastradas
    vvMUDA_ABA_UNIDADE := False;
    Panel10.Enabled := True;

    //Bot�es
    Panel5.Enabled := True;
    btnDesativarUnidade.Enabled  := False;
    btnNovaUnidade.Enabled       := False;
    btnIncluirUnidade.Enabled    := True;
    btnAlterarUnidade.Enabled    := False;
    btnCancelarOpUnidade.Enabled := True;
    btnVerDadosUnidade.Enabled   := False;
    btnSairUnidade.Enabled       := False;

  end
  else if Modo = 'A' then
  begin

    vvMUDA_ABA_UNIDADE := True;
    PageControl3.ActivePageIndex := 1; //Dentro da aba Unidades, posiciona-se na aba Unidades Cadastradas
    vvMUDA_ABA_UNIDADE := False;
    Panel10.Enabled := True;

    //Bot�es
    Panel5.Enabled := True;
    btnDesativarUnidade.Enabled  := False;
    btnNovaUnidade.Enabled       := False;
    btnIncluirUnidade.Enabled    := False;
    btnAlterarUnidade.Enabled    := True;
    btnCancelarOpUnidade.Enabled := True;
    btnVerDadosUnidade.Enabled   := False;
    btnSairUnidade.Enabled       := False;


  end;
end;

procedure TfrmGerenciarAcessos.btnNovoUClick(Sender: TObject);
begin
  LimpaCamposUnidade('N');
end;

procedure TfrmGerenciarAcessos.btnCancelarUClick(Sender: TObject);
begin
  LimpaCamposUnidade('P');
end;

procedure TfrmGerenciarAcessos.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    if not (ActiveControl is TDBGrid) then
    begin
      Key := #0;
      Perform(WM_NEXTDLGCTL, 0, 0);
    end
    else if (ActiveControl is TDBGrid) then
    with TDBGrid(ActiveControl) do
      if selectedindex < (fieldcount -1) then
        selectedindex := selectedindex +1
      else
        selectedindex := 0;
    end;

procedure TfrmGerenciarAcessos.dbgUnidadesDblClick(Sender: TObject);
begin

  //Prepara a tela para altera��o dos dados...
  LimpaCamposUnidade('A');

  //Repassa os valores da tabela para os campos do formul�rio
  mskCcusto.Text          := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').Value;
  edUnidade.Text          := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value;
  edGestor.Text           := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').Value;
  edAssistenteSocial.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_asocial').Value;
  edEndereco.Text         := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').Value;
  edComplemento.Text      := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').Value;
  edBairro.Text           := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').Value;
  mskCep.Text             := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').Value;
  mskFone1.Text           := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').Value;
  mskFone2.Text           := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').Value;
  edEmail.Text            := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').Value;
  edCapacidade.Text       := dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').Value;
  cbRegiao.ItemIndex      := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').Value;
  cbTipoUnidade.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').Value;
  cbDivisao.ItemIndex     := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo_unidade').Value;
  cbTipoGestao.ItemIndex  := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo_gestao').Value;
  mskCcusto.SetFocus;
  vvMUDA_ABA_UNIDADE      := False;
end;

procedure TfrmGerenciarAcessos.mskCcustoExit(Sender: TObject);
begin
  //Validar o n.� do CCusto


end;

procedure TfrmGerenciarAcessos.btnSalvarUClick(Sender: TObject);
begin
  //Perguntar ao usu�rio se deseja incluir o C. Custo...
  try

    if Application.MessageBox ('Deseja incluir o Centro de Custo informado?',
                               '[Inclus�o C. Custo]',
                               MB_YESNO + MB_ICONQUESTION) = id_yes then
    begin

        LimpaCamposUnidade('P');

    end;

  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar incluir os dados da unidades...' + #13+#10 +
                            'Favor entrar em contato com o Administrador.',
                            '[Erro]-Cadastro de Unidades',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmGerenciarAcessos.btnSairAcessoClick(Sender: TObject);
begin
  vUnidadeAcesso1.Free;
  vUnidadeAcesso2.Free;
  vPerfilAcesso1.Free;
  vPerfilAcesso2.Free;

  frmGerenciarAcessos.Close;

end;

procedure TfrmGerenciarAcessos.ModoTelaAcessos(Modo: String);
begin
  if Modo = 'P' then
  begin

    //Aba Acessos Cadastrados
    Panel1.Enabled := True;

    btnHistoricoAcesso.Enabled  := True;
    btnNovoAcesso.Enabled       := True;
    btnIncluirAcesso.Enabled    := False;
    btnAlterarAcesso.Enabled    := False;
    btnCancelarOpAcesso.Enabled := False;
    btnVerDadosAcesso.Enabled   := True;
    btnSairAcesso.Enabled       := True;

  end
  else if Modo = 'N' then
  begin

    //P�gina de Cadastro de Acessos...
    Panel2.Enabled := True;

    //Campos
    edMatricula.Clear;
    edNomeFuncionario.Clear;
    cbUnidade.ItemIndex := -1;
    cbPerfil.ItemIndex  := -1;
    edSenha1.Clear;
    edSenha2.Clear;
    cbSituacaoAcesso.ItemIndex := -1;

    btnHistoricoAcesso.Enabled  := False;
    btnNovoAcesso.Enabled       := False;
    btnIncluirAcesso.Enabled    := True;
    btnAlterarAcesso.Enabled    := False;
    btnCancelarOpAcesso.Enabled := True;
    btnVerDadosAcesso.Enabled   := False;
    btnSairAcesso.Enabled       := False;
  end
  else
  if Modo = 'A' then
  begin
    btnHistoricoAcesso.Enabled  := False;
    btnNovoAcesso.Enabled       := False;
    btnIncluirAcesso.Enabled    := False;
    btnAlterarAcesso.Enabled    := True;
    btnCancelarOpAcesso.Enabled := True;
    btnVerDadosAcesso.Enabled   := False;
    btnSairAcesso.Enabled       := False;
  end;
end;

procedure TfrmGerenciarAcessos.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA = False then AllowChange := False;
end;

procedure TfrmGerenciarAcessos.btnNovoAcessoClick(Sender: TObject);
begin
  ModoTelaAcessos('N');
  PageControl2.ActivePageIndex := 1;
  vvMUDA_ABA_ACESSO := False;
end;

procedure TfrmGerenciarAcessos.btnCancelarOpAcessoClick(Sender: TObject);
begin
  ModoTelaAcessos('P');
  PageControl2.ActivePageIndex := 0;
  vvMUDA_ABA_ACESSO := False;
  vvMUDA_ABA := True;

  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value   := Null;
      Params.ParamByName ('@pe_cod_perfil').Value    := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := Null;
      Params.ParamByName ('@pe_flg_ativo').Value     := Null;
      Params.ParamByName ('@pe_flg_online').Value    := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Open;
      dbgAcessos.Refresh;
    end;
  except end;
end;

procedure TfrmGerenciarAcessos.btnGravarAcessoClick(Sender: TObject);
begin


  try
    //Pede confirma��o do usu�rio...
    if Application.MessageBox ('Deseja incluir os dados do acesso?',
                               '[Sistema Deca] - Incluir Novo Acesso',
                               MB_YESNO + MB_ICONQUESTION) = id_yes then
    begin



    end
    else
    begin
      Application.MessageBox ('Aten��o!!!' +#13+#10+
                              'Usu�rio n�o confirmou a inclus�o dos dados.',
                              '[Sistema Deca] - Cancelar Inclus�o de Acesso.',
                              MB_OK + MB_ICONWARNING);
      ModoTelaAcessos('P');
      PageControl2.ActivePageIndex := 0;
    end;

  except
    Application.MessageBox ('Aten��o!!!' +#13+#10+
                            'Um erro ocorreu ao tentar carregar os dados de acessos.' +#13+#10+
                            'Verifique a conex�o com a Internet ou entre em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro acesso aos dados.',
                            MB_OK + MB_ICONINFORMATION);
    ModoTelaAcessos('P');
  end;
  
end;

procedure TfrmGerenciarAcessos.btnDesativarUnidadeClick(Sender: TObject);
begin
  //Alterar o flg_status da unidade para 1, pois Ativa=0
end;

procedure TfrmGerenciarAcessos.btnCancelarOpUnidadeClick(Sender: TObject);
begin
  LimpaCamposUnidade('P');
  PageControl3.ActivePageIndex := 0;
  vvMUDA_ABA_UNIDADE := False;
  vvMUDA_ABA := True;
end;

procedure TfrmGerenciarAcessos.PageControl3Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if not vvMUDA_ABA_UNIDADE then AllowChange := False;
end;

procedure TfrmGerenciarAcessos.btnNovaUnidadeClick(Sender: TObject);
begin
  LimpaCamposUnidade ('N');
  vvMUDA_ABA := False;
end;

procedure TfrmGerenciarAcessos.btnIncluirUnidadeClick(Sender: TObject);
begin
  //Inserir no banco de dados os dados aqui informados...
  try
    with dmDeca.cdsInc_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_nom_unidade').Value     := GetValue(edUnidade.Text);
      Params.ParamByName ('@pe_ind_regiao').Value      := cbRegiao.ItemIndex;
      Params.ParamByName ('@pe_num_ccusto').Value      := GetValue(mskCCusto.Text);
      Params.ParamByName ('@pe_nom_gestor').Value      := GetValue(edGestor.Text);
      Params.PAramByName ('@pe_nom_asocial').Value     := GetValue(edAssistenteSocial.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value    := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_dsc_complemento').Value := GetValue(edComplemento.Text);
      Params.ParamByName ('@pe_dsc_bairro').Value      := GetValue(edBairro.Text);
      Params.ParamByName ('@pe_num_cep').Value         := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_num_telefone1').Value   := GetValue(mskFone1.Text);
      Params.ParamByName ('@pe_num_telefone2').Value   := GetValue(mskFone2.Text);
      Params.ParamByName ('@pe_dsc_email').Value       := GetValue(edEmail.Text);
      Params.ParamByName ('@pe_num_capacidade').Value  := GetValue(edCapacidade, vtInteger);
      Params.ParamByName ('@pe_ind_tipo').Value        := cbTipoUnidade.ItemIndex;
      Params.ParamByName ('@pe_ind_divisao').Value     := cbDivisao.ItemIndex;
      Params.ParamByName ('@pe_ind_gestao').Value      := cbTipoGestao.ItemIndex;
      Params.ParamByName ('@pe_flg_status').Value      := 0; //Unidade ativa
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Execute;

      //Atualiza o grid de Unidades...
      with dmDeca.cdsSel_Unidade do
      begin
        Open;
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
        Params.ParamByName ('@pe_nom_unidade').Value := Null;
        LimpaCamposUnidade ('P');
        vvMUDA_ABA_UNIDADE := False;
        dbgUnidades.Refresh;
      end;


    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu ao tentar inserir a nova unidade. Verifique conex�o com a internet e tente novamente.',
                            '[Sistema Deca] - Erro inclus�o de unidade',
                            MB_OK + MB_ICONERROR);
    LimpaCamposUnidade ('P');
    vvMUDA_ABA_UNIDADE := False;
  end;
end;

procedure TfrmGerenciarAcessos.btnAlterarUnidadeClick(Sender: TObject);
begin
  //Enviar as altera��es ao banco de dados...
  try
    with dmDeca.cdsAlt_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value     := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_nom_unidade').Value     := GetValue(edUnidade.Text);
      Params.ParamByName ('@pe_ind_regiao').Value      := cbRegiao.ItemIndex;
      Params.ParamByName ('@pe_num_ccusto').Value      := GetValue(mskCcusto.Text);
      Params.ParamByName ('@pe_nom_gestor').Value      := GetValue(edGestor.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value    := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_dsc_complemento').Value := GetValue(edComplemento.Text);
      Params.ParamByName ('@pe_dsc_bairro').Value      := GetValue(edBairro.Text);
      Params.ParamByName ('@pe_num_cep').Value         := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_num_telefone1').Value   := GetValue(mskFone1.Text);
      Params.ParamByName ('@pe_num_telefone2').Value   := GetValue(mskFone2.Text);
      Params.ParamByName ('@pe_dsc_email').Value       := GetValue(edEmail.Text);
      Params.ParamByName ('@pe_num_capacidade').Value  := GetValue(edCapacidade, vtInteger);
      Params.ParamByName ('@pe_ind_tipo').Value        := cbTipoUnidade.ItemIndex;
      Params.ParamByName ('@pe_flg_status').Value      := 0; //Continuar Ativa...
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Params.ParamByName ('@pe_nom_asocial').Value     := GetValue(edAssistenteSocial.Text);
      Params.ParamByName ('@pe_ind_divisao').Value     := cbDivisao.ItemIndex;
      Params.ParamByName ('@pe_ind_gestao').Value      := cbTipoGestao.ItemIndex;
      Execute;

      //Atualiza os dados do grid apos a altera��o
      with dmDeca.cdsSel_Unidade do
      begin
        Close;
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
        Params.ParamByName ('@pe_nom_unidade').Value := Null;
        Open;
        LimpaCamposUnidade ('P');
        vvMUDA_ABA_UNIDADE := False;
        dbgUnidades.Refresh;
      end;
    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu ao tentar alterar dados da nova unidade. Verifique conex�o com a internet e tente novamente.',
                            '[Sistema Deca] - Erro inclus�o de unidade',
                            MB_OK + MB_ICONERROR);
    LimpaCamposUnidade ('P');
    vvMUDA_ABA_UNIDADE := False;
  end;


end;

procedure TfrmGerenciarAcessos.dbgUnidadesCellClick(Column: TColumn);
begin
  if dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0 then
    btnDesativarUnidade.Enabled := True
  else
    btnDesativarUnidade.Enabled := False;
end;

procedure TfrmGerenciarAcessos.ModoTelaPerfis(Modo: String);
begin
  if Modo = 'P' then
  begin
    Panel6.Enabled    := True;
    edPerfil.Clear;
    meDescricaoFuncoes.Clear;
    GroupBox1.Enabled := False;
    GroupBox3.Enabled := False;

    //Bot�es
    btnNovoPerfil.Enabled    := True;
    btnIncluirPerfil.Enabled := False;
    btnAlterarPerfil.Enabled := False;
    btnCancelarOpPerfil.Enabled := False;
    btnVerDadosPerfil.Enabled := True;
    btnSairPerfil.Enabled := True;
  end
  else if Modo = 'N' then
  begin
    Panel6.Enabled    := False;
    edPerfil.Clear;
    meDescricaoFuncoes.Clear;
    GroupBox1.Enabled := True;
    GroupBox3.Enabled := True;

    //Bot�es
    btnNovoPerfil.Enabled    := False;
    btnIncluirPerfil.Enabled := True;
    btnAlterarPerfil.Enabled := False;
    btnCancelarOpPerfil.Enabled := True;
    btnVerDadosPerfil.Enabled := False;
    btnSairPerfil.Enabled := False;
  end
  else if Modo = 'A' then
  begin
    Panel6.Enabled    := True;
    edPerfil.Clear;
    meDescricaoFuncoes.Clear;
    GroupBox1.Enabled := False;
    GroupBox3.Enabled := False;

    //Bot�es
    btnNovoPerfil.Enabled    := False;
    btnIncluirPerfil.Enabled := False;
    btnAlterarPerfil.Enabled := True;
    btnCancelarOpPerfil.Enabled := True;
    btnVerDadosPerfil.Enabled := False;
    btnSairPerfil.Enabled := False;

  end;
end;

procedure TfrmGerenciarAcessos.btnNovoPerfilClick(Sender: TObject);
begin
  ModoTelaPerfis('N');
  vvMUDA_ABA_PERFIL := True;
  vvMUDA_ABA := False;
  PageControl4.ActivePageIndex := 1;
  edDescricaoPerfil.Clear;
  edDescricaoPerfil.SetFocus;
  meAtividadesPerfil.Clear;
end;

procedure TfrmGerenciarAcessos.PageControl4Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA_PERFIL = False then AllowChange := False;
end;

procedure TfrmGerenciarAcessos.btnCancelarOpPerfilClick(Sender: TObject);
begin
  ModoTelaPerfis('P');
  PageControl4.ActivePageIndex := 0;
  vvMUDA_ABA_PERFIL := False;
  vvMUDA_ABA := True;
end;

procedure TfrmGerenciarAcessos.btnIncluirPerfilClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_Perfil do
    begin
      Close;
      Params.ParamByName ('@pe_dsc_perfil').Value      := GetValue(edPerfil.Text);
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Params.ParamByName ('@pe_dsc_funcoes').Value     := GetValue(meDescricaoFuncoes.Text);
      Execute;

      //Atualiza os dados do grid...
      with dmDeca.cdsSel_Perfil do
      begin
        Close;
        Params.ParamByName ('@pe_cod_perfil').Value := Null;
        Open;
        dbgPerfis.Refresh;
      end;
    end;
  except end;
end;

procedure TfrmGerenciarAcessos.btnAlterarPerfilClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Perfil do
    begin
      Close;
      Params.ParamByName ('@pe_cod_perfil').Value      := dmDeca.cdsSel_Perfil.FieldByName ('cod_perfil').Value;
      Params.ParamByName ('@pe_dsc_perfil').Value      := GetValue(edPerfil.Text);
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Params.ParamByName ('@pe_dsc_funcoes').Value     := GetValue(meDescricaoFuncoes.Text);
      Params.ParamByName ('@pe_flg_status').Value      := 0; //Continua como ATIVO...
      Execute;

      //Atualiza os dados do grid...
      with dmDeca.cdsSel_Perfil do
      begin
        Close;
        Params.ParamByName ('@pe_cod_perfil').Value := Null;
        Open;
        dbgPerfis.Refresh;
      end;
    end;
  except end;

  ModoTelaPerfis('P');
  PageControl4.ActivePageIndex := 0;

end;

procedure TfrmGerenciarAcessos.dbgPerfisDblClick(Sender: TObject);
begin
  ModoTelaPerfis('A');
  PageControl4.ActivePageIndex := 1;
  edDescricaoPerfil.Text  := dmDeca.cdsSel_Perfil.FieldByName('dsc_perfil').Value;
  meAtividadesPerfil.Text := dmDeca.cdsSel_Perfil.FieldByName('dsc_funcoes').Value;
end;

procedure TfrmGerenciarAcessos.ModoTelaUsuarios(Modo: String);
begin
  if Modo = 'P' then
  begin
    mskMatricula.Clear;
    datNascimento.Date := Now();
    datCadastro.Date   := Now();
    edNomeCompleto.Clear;
    edEnderecoUsuario.Clear;
    edComplementoEndUsuario.Clear;
    edBairroUsuario.Clear;
    mskCepUsuario.Clear;
    edMunicipioUsuario.Clear;
    edUfUsuario.Clear;
    edEmailUsuario.Clear;
    mskFone1Usuario.Clear;
    mskFone2Usuario.Clear;
    cbSituacaoUsuario.ItemIndex;
    chkTreinado.Checked := False;

    //Bot�es
    btnLiberarAcesso.Enabled     := False;
    btnNovoUsuario.Enabled       := True;
    btnIncluirUsuario.Enabled    := False;
    btnAlterarUsuario.Enabled    := False;
    btnCancelarOpUsuario.Enabled := False;
    btnVerDadosUsuario.Enabled   := False;
    btnSairUsuario.Enabled       := True;
  end
  else if Modo = 'N' then
  begin
    mskMatricula.Clear;
    datNascimento.Date := Now();
    datCadastro.Date   := Now();
    edNomeCompleto.Clear;
    edEnderecoUsuario.Clear;
    edComplementoEndUsuario.Clear;
    edBairroUsuario.Clear;
    mskCepUsuario.Clear;
    edMunicipioUsuario.Clear;
    edUfUsuario.Clear;
    edEmailUsuario.Clear;
    mskFone1Usuario.Clear;
    mskFone2Usuario.Clear;
    cbSituacaoUsuario.ItemIndex;
    chkTreinado.Checked := False;

    //Bot�es
    btnLiberarAcesso.Enabled     := False;
    btnNovoUsuario.Enabled       := False;
    btnIncluirUsuario.Enabled    := True;
    btnAlterarUsuario.Enabled    := False;
    btnCancelarOpUsuario.Enabled := True;
    btnVerDadosUsuario.Enabled   := False;
    btnSairUsuario.Enabled       := False;
  end
  else if Modo = 'A' then
  begin
    mskMatricula.Clear;
    datNascimento.Date := Now();
    datCadastro.Date   := Now();
    edNomeCompleto.Clear;
    edEnderecoUsuario.Clear;
    edComplementoEndUsuario.Clear;
    edBairroUsuario.Clear;
    mskCepUsuario.Clear;
    edMunicipioUsuario.Clear;
    edUfUsuario.Clear;
    edEmailUsuario.Clear;
    mskFone1Usuario.Clear;
    mskFone2Usuario.Clear;
    cbSituacaoUsuario.ItemIndex;
    chkTreinado.Checked := False;

    //Bot�es
    btnLiberarAcesso.Enabled     := False;
    btnNovoUsuario.Enabled       := False;
    btnIncluirUsuario.Enabled    := False;
    btnAlterarUsuario.Enabled    := True;
    btnCancelarOpUsuario.Enabled := True;
    btnVerDadosUsuario.Enabled   := False;
    btnSairUsuario.Enabled       := True;

  end;
end;

procedure TfrmGerenciarAcessos.PageControl5Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA_USUARIO = False then AllowChange := False;
end;

procedure TfrmGerenciarAcessos.PageControl6Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  //if vvMUDA_ABA_USUARIO = False then AllowChange := False;
end;

procedure TfrmGerenciarAcessos.btnNovoUsuarioClick(Sender: TObject);
begin
  PageControl5.ActivePageIndex := 1;
  ModoTelaUsuarios('N');
  mskMatricula.SetFocus;
  vvMUDA_ABA_USUARIO := False;
  vvMUDA_ABA := False;
  cbSituacaoUsuario.ItemIndex := 2; //Aguardando Libera��o..
end;

procedure TfrmGerenciarAcessos.btnCancelarOpUsuarioClick(Sender: TObject);
begin
  vvMUDA_ABA_USUARIO := True;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  ModoTelaUsuarios('P');
  vvMUDA_ABA_USUARIO := False;
  vvMUDA_ABA := True;

  //Atualizar os dados do grid de usu�rios...
  with dmDeca.cdsSel_Usuario do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value   := Null;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_flg_situacao').Value  := Null;
    Open;
  end;




end;

procedure TfrmGerenciarAcessos.btnSairUnidadeClick(Sender: TObject);
begin
  frmGerenciarAcessos.Close;
end;

procedure TfrmGerenciarAcessos.btnSairPerfilClick(Sender: TObject);
begin
  frmGerenciarAcessos.Close;
end;

procedure TfrmGerenciarAcessos.btnSairUsuarioClick(Sender: TObject);
begin
  frmGerenciarAcessos.Close;
end;

procedure TfrmGerenciarAcessos.btnIncluirUsuarioClick(Sender: TObject);
begin
  try
    if Application.MessageBox ('Confirma a inclus�o dos dados do usu�rio?',
                               '[Sistema Deca] - Cadastro de Usu�rios',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      with dmDeca.cdsInc_Usuario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value    := GetValue(mskMatricula.Text);
        Params.ParamByName ('@pe_nom_usuario').Value      := GetValue(edNomeCompleto.Text);
        Params.ParamByName ('@pe_dsc_endereco').Value     := GetValue(edEnderecoUsuario.Text);
        Params.ParamByName ('@pe_dsc_complemento').Value  := GetValue(edComplementoEndUsuario.Text);
        Params.ParamByName ('@pe_dsc_bairro').Value       := GetValue(edBairroUsuario.Text);
        Params.ParamByName ('@pe_num_cep').Value          := GetValue(mskCepUsuario.Text);
        Params.ParamByName ('@pe_dsc_municipio').Value    := GetValue(edMunicipioUsuario.Text);
        Params.ParamByName ('@pe_dsc_uf').Value           := GetValue(edUfUsuario.Text);
        Params.ParamByName ('@pe_dsc_email').Value        := GetValue(edEmailUsuario.Text);
        Params.ParamByName ('@pe_num_fone_celular').Value := GetValue(mskFone1Usuario.Text);
        Params.ParamByName ('@pe_num_fone_recado').Value  := GetValue(mskFone2Usuario.Text);
        Params.ParamByName ('@pe_flg_situacao').Value     := cbSituacaoUsuario.ItemIndex;;
        Params.ParamByName ('@pe_dat_nascimento').Value   := datNascimento.Date;
        Params.ParamByName ('@pe_flg_treinado').Value     := 0; //Padr�o para usu�rio NOVOS-> ainda n�o foram treinados
        Execute;

        //Atualiza a tela e os dados no grid...
        with dmDeca.cdsSel_Usuario do
        begin
          Close;
          Params.ParamByName ('@pe_cod_usuario').Value   := Null;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_flg_situacao').Value  := Null;
          Params.ParamByName ('@pe_nom_usuario').Value   := Null;
          Open;
          dbgUsuarios.Refresh;

          ModoTelaUsuarios('P');
          vvMUDA_ABA_USUARIO := False;
          vvMUDA_ABA := True;

        end;
      end;

      


    end;

  except
    begin
      Application.MessageBox ('Um erro ocorreu e os dados n�o foram inseridos. Favor verifica a conxe�o e tentar novamente.',
                              '[Sistema Deca] - Erro incluindo novo usu�rio',
                              MB_OK + MB_ICONERROR);
      ModoTelaUsuarios('P');
      vvMUDA_ABA_USUARIO := False;

    end;
  end;
end;

procedure TfrmGerenciarAcessos.dbgUsuariosDblClick(Sender: TObject);
begin

  //Repassa os valores para os campos...
  vvMUDA_ABA_USUARIO := True;
  PageControl5.ActivePageIndex := 1;
  ModoTelaUsuarios('A');

  mskMatricula.Text            := dmDeca.cdsSel_Usuario.FieldByName ('cod_matricula').Value;
  datNascimento.Date           := dmDeca.cdsSel_Usuario.FieldByName ('dat_nascimento').AsDateTime;
  datCadastro.Date             := dmDeca.cdsSel_Usuario.FieldByName ('dat_cadastro').AsDateTime;

  edNomeCompleto.Text          := dmDeca.cdsSel_Usuario.FieldByName ('nom_usuario').Value;
  edEnderecoUsuario.Text       := dmDeca.cdsSel_Usuario.FieldByName ('dsc_endereco').Value;

  if dmDeca.cdsSel_Usuario.FieldByName ('dsc_complemento').IsNull then
    edComplementoEndUsuario.Text := ''
  else
    dmDeca.cdsSel_Usuario.FieldByName ('dsc_complemento').Value;

  edBairroUsuario.Text         := dmDeca.cdsSel_Usuario.FieldByName ('dsc_bairro').Value;

  mskCepUsuario.Text           := dmDeca.cdsSel_Usuario.FieldByName ('num_cep').Value;
  edMunicipioUsuario.Text      := dmDeca.cdsSel_Usuario.FieldByName ('dsc_municipio').Value;
  edUfUsuario.Text             := dmDeca.cdsSel_Usuario.FieldByName ('dsc_uf').Value;
  edEmailUsuario.Text          := dmDeca.cdsSel_Usuario.FieldByName ('dsc_email').Value;

  mskFone1Usuario.Text         := dmDeca.cdsSel_Usuario.FieldByName ('num_fone_celular').Value;
  mskFone2Usuario.Text         := dmDeca.cdsSel_Usuario.FieldByName ('num_fone_recado').Value;

  cbSituacaoUsuario.ItemIndex  := dmDeca.cdsSel_Usuario.FieldByName ('flg_situacao').Value;

  if (dmDeca.cdsSel_Usuario.FieldByName ('flg_treinado').Value = 0) then
  begin
    imgTreinamentos.Visible := False;
    chkTreinado.Checked := False
  end
  else
  begin
    imgTreinamentos.Visible := True;
    chkTreinado.Checked := True
  end;
end;

procedure TfrmGerenciarAcessos.btnAlterarUsuarioClick(Sender: TObject);
begin

  //Pede confirma��o ao usu�rio antes de incluir a altera��o no banco de dados
  try
    with dmDeca.cdsAlt_Usuario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value         := dmDeca.cdsSel_Usuario.FieldByName ('cod_usuario').Value;
      Params.ParamByName ('@pe_nom_usuario').Value         := GetValue(edNomeCompleto.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value        := GetValue(edEnderecoUsuario.Text);
      Params.ParamByName ('@pe_dsc_complemento').Value     := GetValue(edComplementoEndUsuario.text);
      Params.ParamByName ('@pe_dsc_bairro').Value          := GetValue(edBairroUsuario.Text);
      Params.ParamByName ('@pe_num_cep').Value             := GetValue(mskCepUsuario.Text);
      Params.ParamByName ('@pe_dsc_municipio').Value       := GetValue(edMunicipioUsuario.Text);
      Params.ParamByName ('@pe_dsc_uf').Value              := GetValue(edUfUsuario.Text);
      Params.ParamByName ('@pe_dsc_email').Value           := GetValue(edEmailUsuario.Text);
      Params.ParamByName ('@pe_num_fone_celular').Value    := GetValue(mskFone1Usuario.Text);
      Params.ParamByName ('@pe_num_fone_recado').Value     := GetValue(mskFone2Usuario.Text);
      Params.ParamByName ('@pe_flg_situacao').Value        := cbSituacaoUsuario.ItemIndex;
      Params.ParamByName ('@pe_dat_nascimento').AsDateTime := datNascimento.Date;
      Execute;

      //Atualiza o grid de usu�rios ap�s as altera��es...
      with dmDeca.cdsSel_Usuario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value   := Null;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_flg_situacao').Value  := Null;
        Params.ParamByName ('@pe_nom_usuario').Value   := Null;
        Open;
        dbgUsuarios.Refresh;
      end;

    end;
  except
    Application.MessageBox ('Aten��o !!! Um erro ocorreu ao tentar alterar os dados do usu�rio. Tente novamente.',
                            '[Sistema Deca] - Erro alterando dados de usu�rio.',
                            MB_OK + MB_ICONERROR);
    ModoTelaUsuarios('P');
    PageControl5.ActivePageIndex := 0;
    vvMUDA_ABA_USUARIO := False;
  end;


end;

procedure TfrmGerenciarAcessos.dbgUsuariosCellClick(Column: TColumn);
begin
  //Exibe os acessos cadastrados para o usu�rio...
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value   := dmDeca.cdsSel_Usuario.FieldByName ('cod_usuario').Value;
      Params.ParamByName ('@pe_cod_perfil').Value    := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := Null;
      Params.ParamByName ('@pe_flg_ativo').Value     := Null;
      Params.ParamByName ('@pe_flg_online').Value    := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Open;

      dbgAcessos.Refresh;

      Panel12.Caption := 'Acessos cadastrados de ' + dmDeca.cdsSel_Usuario.FieldByName ('nom_usuario').Value;

    end;
  except end;
end;

procedure TfrmGerenciarAcessos.PageControl2Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA_ACESSO = False then AllowChange := False;

  //Atualiza os dados do grid de acessos...
  with dmDeca.cdsSel_Acesso do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
    Params.ParamByName ('@pe_cod_usuario').Value   := Null;
    Params.ParamByName ('@pe_cod_perfil').Value    := Null;
    Params.ParamByName ('@pe_cod_unidade').Value   := Null;
    Params.ParamByName ('@pe_flg_ativo').Value     := Null;
    Params.ParamByName ('@pe_flg_online').Value    := Null;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Open;
    dbgAcessos.Refresh;
  end;
end;

procedure TfrmGerenciarAcessos.btnHistoricoAcessoClick(Sender: TObject);
begin
  vvMUDA_ABA_ACESSO := True;
  PageControl2.ActivePageIndex := 2;
  vvMUDA_ABA_ACESSO := False;
  btnCancelarOpAcesso.Enabled := True;
end;

procedure TfrmGerenciarAcessos.dbgAcessosDblClick(Sender: TObject);
var vIndUnidade, vIndPerfil : Integer;
begin
  //Repassa valores para o formul�rio...
  ModoTelaAcessos('A');
  PageControl2.ActivePageIndex := 1;
  vvMUDA_ABA_ACESSO            := False;
  edMatricula.Text             := dmDeca.cdsSel_Acesso.FieldByName ('cod_matricula').Value;
  edNomeFuncionario.Text       := dmDeca.cdsSel_Acesso.FieldByName ('nom_usuario').Value;

  vUnidadeAcesso2.Find (dmDeca.cdsSel_Acesso.FieldByName('nom_unidade').AsString, vIndUnidade);
  cbUnidade.ItemIndex          := vIndUnidade;

  vPerfilAcesso2.Find (dmDeca.cdsSel_Acesso.FieldByName('dsc_perfil').AsString, vIndPerfil);
  cbPerfil.ItemIndex := vIndPerfil;

  edSenha1.Text := dmDeca.cdsSel_Acesso.FieldByName ('dsc_senha').Value;
  edSenha2.Text := dmDeca.cdsSel_Acesso.FieldByName ('dsc_senha').Value;

  cbSituacaoAcesso.ItemIndex := dmDeca.cdsSel_Acesso.FieldByName ('flg_ativo').Value;

end;

procedure TfrmGerenciarAcessos.btnIncluirAcessoClick(Sender: TObject);
begin

  //Pede a confirma��o do usu�rio antes de inserir no banco de dados
  if Application.MessageBox ('Deseja incluir os dados do acesso informado?',
                             '[Sistema Deca] - Inclus�o de novo acesso.',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    try

      //Incluir os dados do acesso no banco...
      with dmDeca.cdsInc_Acesso do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value     := dmDeca.cdsSel_Usuario.FieldByName ('cod_usuario').Value;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vUnidadeAcesso1.Strings[cbUnidade.ItemIndex]);
        Params.ParamByName ('@pe_cod_perfil').AsInteger  := StrToInt(vPerfilAcesso1.Strings[cbPerfil.ItemIndex]);
        Params.ParamByName ('@pe_flg_ativo').Value       := 0; //N�o estar� ativo, ser� liberado posteriormente...
        Params.ParamByName ('@pe_flg_online').Value      := 1; //L�gico que n�o estar� on-line
        Params.ParamByName ('@pe_dsc_senha').Value       := GetValue(edSenha1.Text); 
        Execute;
      end;

      //Atualiza os dados do grid de acessos...
      with dmDeca.cdsSel_Acesso do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
        Params.ParamByName ('@pe_cod_usuario').Value   := Null;
        Params.ParamByName ('@pe_cod_perfil').Value    := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   := Null;
        Params.ParamByName ('@pe_flg_ativo').Value     := Null;
        Params.ParamByName ('@pe_flg_online').Value    := Null;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Open;
        dbgAcessos.Refresh;
      end;

    except
      begin

        Application.MessageBox ('Ocorreu um erro e os dados n�o foram inseridos. Tente novamente.',
                                '[Sistema Deca] - Erro de inclus�o no Acesso...',
                                MB_OK + MB_ICONINFORMATION);
        ModoTelaAcessos('P');
        PageControl2.ActivePageIndex := 0;
        vvMUDA_ABA_ACESSO := False;
        vvMUDA_ABA := True;

      end;
    end;

    ModoTelaAcessos('P');
    PageControl2.ActivePageIndex := 0;
    vvMUDA_ABA_ACESSO := False;
    vvMUDA_ABA := True;
  end;

end;

procedure TfrmGerenciarAcessos.btnAlterarAcessoClick(Sender: TObject);
begin
  //Pede a confirma��o do usu�rio antes de gravar as altera��es no banco de dados
  if Application.MessageBox ('Deseja gravar as altera��es do acesso selecionado?',
                             '[Sistema Deca] - Altera��o de acesso.',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    try
      with dmDeca.cdsAlt_Acesso do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_acesso').Value := dmDeca.cdsSel_Acesso.FieldByName ('cod_id_acesso').Value;
        Params.ParamByName ('@pe_cod_perfil').Value    := StrToInt(vPerfilAcesso1.Strings[cbPerfil.ItemIndex]);
        Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vUnidadeAcesso1.Strings[cbUnidade.ItemIndex]);
        Params.ParamByName ('@pe_flg_ativo').Value     := cbSituacaoAcesso.ItemIndex;
        Params.ParamByName ('@pe_dsc_senha').Value     := GetValue(edSenha1.Text);
        Execute;
      end;

      //Atualiza os dados do grid...
      with dmDeca.cdsSel_Acesso do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
        Params.ParamByName ('@pe_cod_usuario').Value   := Null;
        Params.ParamByName ('@pe_cod_perfil').Value    := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   := Null;
        Params.ParamByName ('@pe_flg_ativo').Value     := Null;
        Params.ParamByName ('@pe_flg_online').Value    := Null;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Open;
        dbgAcessos.Refresh;
      end;

      ModoTelaAcessos('P');
      PageControl2.ActivePageIndex := 0;
      vvMUDA_ABA_ACESSO := True;
      vvMUDA_ABA := True;

    except
        Application.MessageBox ('Ocorreu um erro e os dados n�o foram alterados. Tente novamente.',
                                '[Sistema Deca] - Erro de altera��o dos dados de Acesso...',
                                MB_OK + MB_ICONINFORMATION);
        ModoTelaAcessos('P');
        PageControl2.ActivePageIndex := 0;
        vvMUDA_ABA_ACESSO := False;
        vvMUDA_ABA := True;
    end;

  end;
end;

procedure TfrmGerenciarAcessos.dbgAcessosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
//var Iconx : TBitMap;
begin
{
  Iconx := TBitMap.Create();

  if (Column.FieldName = 'vStatus') then
  begin
    with dbgAcessos.Canvas do
    begin
      Brush.Color := clWhite;
      FillRect(Rect);
      if (dmDeca.cdsSel_Acesso.FieldByName ('flg_ativo').Value = 0) then
        ImageList1.GetBitmap(3, Iconx)
      else if (dmDeca.cdsSel_Acesso.FieldByName ('flg_ativo').Value = 1) then
        ImageList1.GetBitmap(2, Iconx);
      Draw(round((Rect.Left + Rect.Right - Iconx.Width) / 2), Rect.Top, Iconx);
    end;
  end;


  if (Column.FieldName = 'vOnLine') then
  begin
    with dbgAcessos.Canvas do
    begin
      Brush.Color := clWhite;
      FillRect(Rect);
      if (dmDeca.cdsSel_Acesso.FieldByName ('flg_online').Value = 0) then
        ImageList1.GetBitmap(0, Iconx)
      else if (dmDeca.cdsSel_Acesso.FieldByName ('flg_online').Value = 1) then
        ImageList1.GetBitmap(1, Iconx);
      Draw(round((Rect.Left + Rect.Right - Iconx.Width) / 2), Rect.Top, Iconx);
    end;
  end;
}
end;

procedure TfrmGerenciarAcessos.btnVerResultadosClick(Sender: TObject);
begin
  //Pesquisar acessos...
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value   := Null;
      Params.ParamByName ('@pe_cod_perfil').Value    := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := Null;
      Params.ParamByName ('@pe_flg_ativo').Value     := Null;
      Params.ParamByName ('@pe_flg_online').Value    := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(edCodMatricula.Text);
      Open;
      dbgAcessos.Refresh;
    end
  except end;
end;

procedure TfrmGerenciarAcessos.btnVerDadosAcessoClick(Sender: TObject);
begin
  lbTituloAcessos.Caption := 'Acessos cadastrados de ' + dmDeca.cdsSel_Acesso.FieldByname ('nom_usuario').Value;
  mmCOD_USUARIO := dmDeca.cdsSel_Acesso.FieldByname ('cod_matricula').Value;
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value   := Null;
      Params.ParamByName ('@pe_cod_perfil').Value    := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := Null;
      Params.ParamByName ('@pe_flg_ativo').Value     := Null;
      Params.ParamByName ('@pe_flg_online').Value    := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := mmCOD_USUARIO;
      Open;
      dbgAcessosCadastradosUsuario.Refresh;
      PageControl2.ActivePageIndex := 2;
      btnCancelarOpAcesso.Enabled := True;
      btnVerDadosAcesso.Enabled := False;
    end;
  except end;
end;

procedure TfrmGerenciarAcessos.dbgAcessosCadastradosUsuarioDblClick(
  Sender: TObject);
var vIndUnid, vIndPerf : Integer;
begin
  //Repassa valores para o formul�rio...
  ModoTelaAcessos('A');
  PageControl2.ActivePageIndex := 1;
  vvMUDA_ABA_ACESSO            := False;
  edMatricula.Text             := dmDeca.cdsSel_Acesso.FieldByName ('cod_matricula').Value;
  edNomeFuncionario.Text       := dmDeca.cdsSel_Acesso.FieldByName ('nom_usuario').Value;

  vUnidadeAcesso2.Find (dmDeca.cdsSel_Acesso.FieldByName('nom_unidade').AsString, vIndUnid);
  cbUnidade.ItemIndex          := vIndUnid;

  vPerfilAcesso2.Find (dmDeca.cdsSel_Acesso.FieldByName('dsc_perfil').AsString, vIndPerf);
  cbPerfil.ItemIndex := vIndPerf;

  edSenha1.Text := dmDeca.cdsSel_Acesso.FieldByName ('dsc_senha').Value;
  edSenha2.Text := dmDeca.cdsSel_Acesso.FieldByName ('dsc_senha').Value;

  cbSituacaoAcesso.ItemIndex := dmDeca.cdsSel_Acesso.FieldByName ('flg_ativo').Value;
end;

procedure TfrmGerenciarAcessos.btnPesquisarUsuarioClick(Sender: TObject);
begin
  //Acessa o cadastro de usu�rios...
  Application.CreateForm (TfrmPesquisaUsuarios, frmPesquisaUsuarios);
  frmPesquisaUsuarios.ShowModal;

  //Repassa valores do usu�rio localizado
  edMatricula.Text       := dmDeca.cdsSel_Usuario.FieldByName ('cod_matricula').Value;
  edNomeFuncionario.Text := dmDeca.cdsSel_Usuario.FieldByName ('nom_usuario').Value;
end;

procedure TfrmGerenciarAcessos.btnLocalizarUsuarioClick(Sender: TObject);
begin
  //Acessa o cadastro de usu�rios...
  Application.CreateForm (TfrmPesquisaUsuarios, frmPesquisaUsuarios);
  frmPesquisaUsuarios.ShowModal;

  //Repassa valores do usu�rio localizado
  edCodMatricula.Text       := dmDeca.cdsSel_Usuario.FieldByName ('cod_matricula').Value;
  edNomeUsuario.Text        := dmDeca.cdsSel_Usuario.FieldByName ('nom_usuario').Value;
end;

end.
