object frmAvaliacaoDesempenhoAprendiz: TfrmAvaliacaoDesempenhoAprendiz
  Left = 725
  Top = 178
  BorderStyle = bsDialog
  Caption = '[Sistema Deca] - Avalia��o de Desempenho Aprendiz'
  ClientHeight = 583
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 42
    Top = 16
    Width = 123
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data �ltima Avalia��o.....:'
  end
  object Label7: TLabel
    Left = 170
    Top = 14
    Width = 48
    Height = 16
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 0
    Width = 569
    Height = 48
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe o N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnKeyPress = txtMatriculaKeyPress
    end
  end
  object PageControl1: TPageControl
    Left = 1
    Top = 51
    Width = 769
    Height = 478
    ActivePage = TabSheet3
    MultiLine = True
    TabOrder = 1
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = 'Dados do Adolescente'
      object Panel1: TPanel
        Left = 8
        Top = 8
        Width = 741
        Height = 193
        BevelInner = bvSpace
        BevelOuter = bvLowered
        TabOrder = 0
        object Label15: TLabel
          Left = 94
          Top = 24
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Matr�cula.....:'
        end
        object Label16: TLabel
          Left = 93
          Top = 40
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Endere�o.....:'
        end
        object Label17: TLabel
          Left = 249
          Top = 24
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nome.....:'
        end
        object lbMatricula: TLabel
          Left = 162
          Top = 22
          Width = 5
          Height = 16
          Hint = 'N�mero de matr�cula atual'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object lbNomeCompleto: TLabel
          Left = 301
          Top = 22
          Width = 5
          Height = 16
          Hint = 'Nome completo do adolescente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object lbEndereco: TLabel
          Left = 162
          Top = 38
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label21: TLabel
          Left = 112
          Top = 56
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bairro.....:'
        end
        object lbBairro: TLabel
          Left = 162
          Top = 54
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label23: TLabel
          Left = 118
          Top = 72
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'CEP.....:'
        end
        object lbCep: TLabel
          Left = 162
          Top = 70
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label25: TLabel
          Left = 72
          Top = 88
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Unidade Atual.....:'
        end
        object lbUnidadeAtual: TLabel
          Left = 162
          Top = 86
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label27: TLabel
          Left = 27
          Top = 104
          Width = 130
          Height = 13
          Alignment = taRightJustify
          Caption = 'Centro de Custo/Se��o.....:'
        end
        object lbSecaoAtual: TLabel
          Left = 162
          Top = 102
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label29: TLabel
          Left = 62
          Top = 120
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nome do Gestor.....:'
        end
        object lbNomeGestor: TLabel
          Left = 162
          Top = 118
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label31: TLabel
          Left = 41
          Top = 136
          Width = 116
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nome da Ass. Social.....:'
        end
        object lbNomeASocial: TLabel
          Left = 162
          Top = 134
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label1: TLabel
          Left = 97
          Top = 153
          Width = 60
          Height = 13
          Alignment = taRightJustify
          Caption = 'Situa��o.....:'
        end
        object lbSituacao: TLabel
          Left = 162
          Top = 151
          Width = 5
          Height = 16
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
      end
      object Panel2: TPanel
        Left = 8
        Top = 206
        Width = 741
        Height = 232
        BevelInner = bvSpace
        BevelOuter = bvLowered
        TabOrder = 1
        object dbgAvaliacoes: TDBGrid
          Left = 8
          Top = 9
          Width = 723
          Height = 214
          DataSource = dsSel_AvaliacoesApr
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDatAvaliacao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'Avalia��o'
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Arial'
              Title.Font.Style = [fsBold]
              Width = 60
              Visible = True
            end
            item
              Color = clAqua
              Expanded = False
              FieldName = 'nom_unidade'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = 'Unidade'
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Arial'
              Title.Font.Style = [fsBold]
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dsc_nom_secao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = 'Se��o/Empresa'
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Arial'
              Title.Font.Style = [fsBold]
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDatInicio'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'In�cio Pr�tica'
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Arial'
              Title.Font.Style = [fsBold]
              Width = 75
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDatInsercao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'Lan�ado'
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Arial'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_usuario'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              Title.Caption = 'Digitador'
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Arial'
              Title.Font.Style = [fsBold]
              Width = 200
              Visible = True
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Nova Avalia��o'
      ImageIndex = 1
      object PageControl2: TPageControl
        Left = 9
        Top = 52
        Width = 741
        Height = 388
        ActivePage = TabSheet4
        TabOrder = 5
        object TabSheet4: TTabSheet
          Caption = 'Itens de 1 a 6'
          object rgItem01: TRadioGroup
            Left = 8
            Top = 9
            Width = 720
            Height = 56
            Caption = '1. Frequ�ncia'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 0
          end
          object rgItem02: TRadioGroup
            Left = 7
            Top = 67
            Width = 720
            Height = 56
            Caption = '2. Pontualidade'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 1
          end
          object rgItem03: TRadioGroup
            Left = 7
            Top = 124
            Width = 720
            Height = 56
            Caption = '3. Responsalidade e cuidado com materiais e documentos'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 2
          end
          object rgItem04: TRadioGroup
            Left = 8
            Top = 182
            Width = 720
            Height = 56
            Caption = '4. Conhecimento das normas e regras da Institui��o / Empresa'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 3
          end
          object rgItem05: TRadioGroup
            Left = 8
            Top = 240
            Width = 720
            Height = 56
            Caption = '5. Cumprimento das normas e regras da Institui��o / Empresa'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 4
          end
          object rgItem06: TRadioGroup
            Left = 8
            Top = 298
            Width = 720
            Height = 56
            Caption = '6. Participa��o no desenvolvimento das atividades'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 5
          end
          object meJustificativa1: TMemo
            Left = 200
            Top = 20
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 6
            OnChange = meJustificativa1Change
          end
          object Edit1: TEdit
            Left = 689
            Top = 29
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 7
            Text = '255'
          end
          object meJustificativa2: TMemo
            Left = 200
            Top = 76
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 8
            OnChange = meJustificativa2Change
          end
          object Edit2: TEdit
            Left = 689
            Top = 85
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 9
            Text = '255'
          end
          object meJustificativa3: TMemo
            Left = 200
            Top = 139
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 10
            OnChange = meJustificativa3Change
          end
          object Edit3: TEdit
            Left = 689
            Top = 148
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 11
            Text = '255'
          end
          object meJustificativa4: TMemo
            Left = 200
            Top = 196
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 12
            OnChange = meJustificativa4Change
          end
          object Edit4: TEdit
            Left = 689
            Top = 205
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 13
            Text = '255'
          end
          object meJustificativa5: TMemo
            Left = 200
            Top = 253
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 14
            OnChange = meJustificativa5Change
          end
          object Edit5: TEdit
            Left = 689
            Top = 262
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 15
            Text = '255'
          end
          object meJustificativa6: TMemo
            Left = 200
            Top = 312
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 16
            OnChange = meJustificativa6Change
          end
          object Edit6: TEdit
            Left = 689
            Top = 321
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 17
            Text = '255'
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Itens de 7 a 12'
          ImageIndex = 1
          object rgItem07: TRadioGroup
            Left = 8
            Top = 9
            Width = 720
            Height = 56
            Caption = 
              '7. Cumprimento das atividades propostas pela empresa, de acordo ' +
              'com habilidades e conhecimentos'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 0
          end
          object rgItem08: TRadioGroup
            Left = 7
            Top = 67
            Width = 720
            Height = 56
            Caption = '8. Administra��o do tempo:  organizado e controle das atividades'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 1
          end
          object rgItem09: TRadioGroup
            Left = 7
            Top = 124
            Width = 720
            Height = 56
            Caption = '9. Orienta��o e apoio ao aprendiz'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 2
          end
          object rgItem10: TRadioGroup
            Left = 8
            Top = 182
            Width = 720
            Height = 56
            Caption = '10. Trabalho em equipe'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 3
          end
          object rgItem11: TRadioGroup
            Left = 8
            Top = 240
            Width = 720
            Height = 56
            Caption = '11. Oportunidade para o aprendiz demonstrar iniciativa'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 4
          end
          object rgItem12: TRadioGroup
            Left = 8
            Top = 298
            Width = 720
            Height = 56
            Caption = 
              '12. Continuidade no desenvolvimento de seus conhecimentos e habi' +
              'lidades na aprendizagem'
            Items.Strings = (
              'Adequado'
              'Precisa ser aprimorado')
            TabOrder = 5
          end
          object meJustificativa7: TMemo
            Left = 200
            Top = 22
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 6
            OnChange = meJustificativa7Change
          end
          object Edit7: TEdit
            Left = 689
            Top = 31
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 7
            Text = '255'
          end
          object meJustificativa8: TMemo
            Left = 200
            Top = 80
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 8
            OnChange = meJustificativa8Change
          end
          object Edit8: TEdit
            Left = 689
            Top = 89
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 9
            Text = '255'
          end
          object meJustificativa9: TMemo
            Left = 200
            Top = 136
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 10
            OnChange = meJustificativa9Change
          end
          object Edit9: TEdit
            Left = 689
            Top = 145
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 11
            Text = '255'
          end
          object meJustificativa10: TMemo
            Left = 200
            Top = 195
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 12
            OnChange = meJustificativa10Change
          end
          object Edit10: TEdit
            Left = 689
            Top = 204
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 13
            Text = '255'
          end
          object meJustificativa11: TMemo
            Left = 200
            Top = 253
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 14
            OnChange = meJustificativa11Change
          end
          object Edit11: TEdit
            Left = 689
            Top = 262
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 15
            Text = '255'
          end
          object meJustificativa12: TMemo
            Left = 200
            Top = 311
            Width = 486
            Height = 38
            MaxLength = 255
            ScrollBars = ssVertical
            TabOrder = 16
            OnChange = meJustificativa12Change
          end
          object Edit12: TEdit
            Left = 689
            Top = 320
            Width = 31
            Height = 21
            MaxLength = 3
            ReadOnly = True
            TabOrder = 17
            Text = '255'
          end
        end
      end
      object GroupBox29: TGroupBox
        Left = 569
        Top = 7
        Width = 90
        Height = 41
        Caption = 'Data Avalia��o'
        TabOrder = 3
        object mskDataAvaliacao: TMaskEdit
          Left = 8
          Top = 14
          Width = 70
          Height = 21
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
          OnExit = mskDataAvaliacaoExit
        end
      end
      object GroupBox13: TGroupBox
        Left = 479
        Top = 7
        Width = 89
        Height = 41
        Caption = 'Data Digita��o'
        Enabled = False
        TabOrder = 2
        object mskDataDigitacao: TMaskEdit
          Left = 8
          Top = 14
          Width = 73
          Height = 21
          EditMask = '99/99/9999'
          MaxLength = 10
          ReadOnly = True
          TabOrder = 0
          Text = '  /  /    '
          OnExit = mskDataDigitacaoExit
        end
      end
      object GroupBox14: TGroupBox
        Left = 660
        Top = 7
        Width = 96
        Height = 41
        Caption = 'In�c. Apr. Pr�tica'
        TabOrder = 4
        object mskDataInicioPratica: TMaskEdit
          Left = 8
          Top = 14
          Width = 73
          Height = 21
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
          OnExit = mskDataInicioPraticaExit
        end
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 7
        Width = 235
        Height = 41
        Caption = 'Centro de Custo/Unidade'
        TabOrder = 0
        object cbUnidade: TComboBox
          Left = 8
          Top = 14
          Width = 220
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 0
          OnClick = cbUnidadeClick
        end
      end
      object GroupBox2: TGroupBox
        Left = 245
        Top = 7
        Width = 232
        Height = 41
        Caption = 'Se��o/Empresa'
        TabOrder = 1
        object cbSecao: TComboBox
          Left = 8
          Top = 14
          Width = 218
          Height = 19
          Style = csOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 0
          OnClick = cbSecaoClick
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Consulta dados Lan�ados'
      ImageIndex = 2
      OnShow = TabSheet3Show
      object rgCriterio: TRadioGroup
        Left = 8
        Top = 8
        Width = 177
        Height = 81
        Caption = 'Crit�rio de pesquisa...'
        Items.Strings = (
          'Per�odo do Lan�amento'
          'Por Adolescente'
          'Por Unidade')
        TabOrder = 0
        OnClick = rgCriterioClick
      end
      object GroupBox3: TGroupBox
        Left = 187
        Top = 8
        Width = 567
        Height = 81
        TabOrder = 1
        object Label5: TLabel
          Left = 31
          Top = 36
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label8: TLabel
          Left = 12
          Top = 60
          Width = 62
          Height = 13
          Caption = 'Adolescente:'
        end
        object btnPesquisaAdol: TSpeedButton
          Left = 364
          Top = 56
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            36050000424D360500000000000036040000280000000E000000100000000100
            08000000000000010000CA0E0000C30E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000ACACACACACAC
            ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
            00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
            02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
            5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
            00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
            ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
            D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
            D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
          OnClick = btnPesquisaAdolClick
        end
        object btnResultado: TSpeedButton
          Left = 448
          Top = 27
          Width = 97
          Height = 33
          Caption = 'Resultado'
          OnClick = btnResultadoClick
        end
        object Label2: TLabel
          Left = 17
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Per�odo de:'
        end
        object Label4: TLabel
          Left = 149
          Top = 16
          Width = 6
          Height = 13
          Caption = 'a'
        end
        object cbUnidadePesquisa: TComboBox
          Left = 77
          Top = 35
          Width = 254
          Height = 19
          Style = csOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 0
        end
        object edAdolescente: TEdit
          Left = 77
          Top = 56
          Width = 285
          Height = 21
          TabOrder = 1
        end
        object mskData1: TMaskEdit
          Left = 78
          Top = 12
          Width = 66
          Height = 21
          EditMask = 'CC/CC/CCCC'
          MaxLength = 10
          TabOrder = 2
          Text = '  /  /    '
          OnExit = mskData1Exit
        end
        object mskData2: TMaskEdit
          Left = 162
          Top = 12
          Width = 65
          Height = 21
          EditMask = 'CC/CC/CCCC'
          MaxLength = 10
          TabOrder = 3
          Text = '  /  /    '
          OnExit = mskData2Exit
        end
      end
      object PageControl3: TPageControl
        Left = 8
        Top = 94
        Width = 745
        Height = 347
        ActivePage = TabSheet7
        TabOrder = 2
        object TabSheet6: TTabSheet
          Caption = 'Resultados obtidos'
          object Label9: TLabel
            Left = 8
            Top = 297
            Width = 502
            Height = 14
            Caption = 
              'D� um duplo clique na linha do registro ao qual deseja alterar i' +
              'nforma��es do lan�amento...'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Panel4: TPanel
            Left = 6
            Top = 4
            Width = 724
            Height = 287
            TabOrder = 0
            object dbgResultado: TDBGrid
              Left = 8
              Top = 8
              Width = 706
              Height = 268
              DataSource = dsResultado
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = dbgResultadoDblClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'cod_matricula'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'Matr�cula'
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'Tahoma'
                  Title.Font.Style = []
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nom_nome'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = 'Adolescente Aprendiz'
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'Tahoma'
                  Title.Font.Style = []
                  Width = 200
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vDatAvaliacao'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'Avalia��o'
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'Tahoma'
                  Title.Font.Style = []
                  Width = 65
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nom_unidade'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = 'Unidade'
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'Tahoma'
                  Title.Font.Style = []
                  Width = 175
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'num_secao'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Se��o'
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'Tahoma'
                  Title.Font.Style = []
                  Width = 35
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'dsc_nom_secao'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'Tahoma'
                  Title.Font.Style = []
                  Width = 150
                  Visible = True
                end>
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Dados para Altera��o'
          ImageIndex = 1
          object btnCancelarAlteracao: TSpeedButton
            Left = 472
            Top = 279
            Width = 129
            Height = 33
            Caption = 'Cancelar'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btnAlterarLancamento: TSpeedButton
            Left = 605
            Top = 279
            Width = 129
            Height = 33
            Caption = 'Alterar Lan�amento'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btnAlterarLancamentoClick
          end
          object GroupBox5: TGroupBox
            Left = 4
            Top = 7
            Width = 232
            Height = 41
            Caption = 'Centro de Custo/Unidade'
            TabOrder = 0
            object cbUnidadeAltera: TComboBox
              Left = 8
              Top = 14
              Width = 220
              Height = 22
              Style = csOwnerDrawFixed
              ItemHeight = 16
              TabOrder = 0
              OnClick = cbUnidadeAlteraClick
            end
          end
          object GroupBox6: TGroupBox
            Left = 238
            Top = 7
            Width = 229
            Height = 41
            Caption = 'Se��o/Empresa'
            TabOrder = 1
            object cbSecaoAltera: TComboBox
              Left = 6
              Top = 14
              Width = 217
              Height = 21
              ItemHeight = 13
              TabOrder = 0
            end
          end
          object GroupBox7: TGroupBox
            Left = 468
            Top = 7
            Width = 86
            Height = 41
            Caption = 'Data Digita��o'
            Enabled = False
            TabOrder = 2
            object mskDataDigitacaoAltera: TMaskEdit
              Left = 8
              Top = 14
              Width = 73
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              ReadOnly = True
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox8: TGroupBox
            Left = 555
            Top = 7
            Width = 85
            Height = 41
            Caption = 'Data Avalia��o'
            TabOrder = 3
            object mskDataAvaliacaoAltera: TMaskEdit
              Left = 8
              Top = 14
              Width = 70
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox9: TGroupBox
            Left = 641
            Top = 7
            Width = 93
            Height = 41
            Caption = 'In�c. Apr. Pr�tica'
            TabOrder = 4
            object mskDataInicioAprendAltera: TMaskEdit
              Left = 8
              Top = 14
              Width = 73
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object PageControl4: TPageControl
            Left = 6
            Top = 51
            Width = 729
            Height = 222
            ActivePage = TabSheet8
            TabOrder = 5
            object TabSheet8: TTabSheet
              Caption = 'Itens 01 - 03'
              object rgItem01_Altera: TRadioGroup
                Left = 1
                Top = 9
                Width = 720
                Height = 56
                Caption = '1. Frequ�ncia'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 0
              end
              object meJustificativa01_Altera: TMemo
                Left = 176
                Top = 22
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 1
                OnChange = meJustificativa01_AlteraChange
              end
              object Edit15: TEdit
                Left = 682
                Top = 30
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 2
                Text = '255'
              end
              object rgItem02_Altera: TRadioGroup
                Left = 1
                Top = 67
                Width = 720
                Height = 56
                Caption = '2. Pontualidade'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 3
              end
              object meJustificativa02_Altera: TMemo
                Left = 176
                Top = 80
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 4
                OnChange = meJustificativa02_AlteraChange
              end
              object Edit16: TEdit
                Left = 682
                Top = 88
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 5
                Text = '255'
              end
              object rgItem03_Altera: TRadioGroup
                Left = 1
                Top = 124
                Width = 720
                Height = 56
                Caption = '3. Responsalidade e cuidado com materiais e documentos'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 6
              end
              object meJustificativa03_Altera: TMemo
                Left = 176
                Top = 138
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 7
                OnChange = meJustificativa03_AlteraChange
              end
              object Edit17: TEdit
                Left = 682
                Top = 146
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 8
                Text = '255'
              end
            end
            object TabSheet9: TTabSheet
              Caption = 'Itens 04 - 06'
              ImageIndex = 1
              object rgItem04_Altera: TRadioGroup
                Left = 1
                Top = 9
                Width = 720
                Height = 56
                Caption = '4. Conhecimento das normas e regras da Institui��o / Empresa'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 0
              end
              object rgItem05_Altera: TRadioGroup
                Left = 1
                Top = 67
                Width = 720
                Height = 56
                Caption = '5. Cumprimento das normas e regras da Institui��o / Empresa'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 1
              end
              object rgItem06_Altera: TRadioGroup
                Left = 1
                Top = 124
                Width = 720
                Height = 56
                Caption = '6. Participa��o no desenvolvimento das atividades'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 2
              end
              object meJustificativa04_Altera: TMemo
                Left = 176
                Top = 22
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 3
                OnChange = meJustificativa04_AlteraChange
              end
              object meJustificativa05_Altera: TMemo
                Left = 176
                Top = 80
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 4
                OnChange = meJustificativa05_AlteraChange
              end
              object meJustificativa06_Altera: TMemo
                Left = 176
                Top = 138
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 5
                OnChange = meJustificativa06_AlteraChange
              end
              object Edit18: TEdit
                Left = 682
                Top = 30
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 6
                Text = '255'
              end
              object Edit19: TEdit
                Left = 682
                Top = 88
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 7
                Text = '255'
              end
              object Edit20: TEdit
                Left = 682
                Top = 146
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 8
                Text = '255'
              end
            end
            object TabSheet10: TTabSheet
              Caption = 'Itens 07 - 09'
              ImageIndex = 2
              object rgItem07_Altera: TRadioGroup
                Left = 1
                Top = 9
                Width = 720
                Height = 56
                Caption = 
                  '7. Cumprimento das atividades propostas pela empresa, de acordo ' +
                  'com habilidades e conhecimentos'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 0
              end
              object meJustificativa07_Altera: TMemo
                Left = 176
                Top = 22
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 1
                OnChange = meJustificativa07_AlteraChange
              end
              object Edit21: TEdit
                Left = 682
                Top = 30
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 2
                Text = '255'
              end
              object rgItem08_Altera: TRadioGroup
                Left = 1
                Top = 67
                Width = 720
                Height = 56
                Caption = '8. Administra��o do tempo:  organizado e controle das atividades'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 3
              end
              object meJustificativa08_Altera: TMemo
                Left = 176
                Top = 80
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 4
                OnChange = meJustificativa08_AlteraChange
              end
              object Edit22: TEdit
                Left = 682
                Top = 88
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 5
                Text = '255'
              end
              object rgItem09_Altera: TRadioGroup
                Left = 1
                Top = 124
                Width = 720
                Height = 56
                Caption = '9. Orienta��o e apoio ao aprendiz'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 6
              end
              object meJustificativa09_Altera: TMemo
                Left = 176
                Top = 138
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 7
                OnChange = meJustificativa09_AlteraChange
              end
              object Edit23: TEdit
                Left = 682
                Top = 146
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 8
                Text = '255'
              end
            end
            object TabSheet11: TTabSheet
              Caption = 'Itens 10 - 12'
              ImageIndex = 3
              object rgItem10_Altera: TRadioGroup
                Left = 1
                Top = 9
                Width = 720
                Height = 56
                Caption = '10. Trabalho em equipe'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 0
              end
              object Edit24: TEdit
                Left = 682
                Top = 30
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 1
                Text = '255'
              end
              object rgItem11_Altera: TRadioGroup
                Left = 1
                Top = 67
                Width = 720
                Height = 56
                Caption = '11. Oportunidade para o aprendiz demonstrar iniciativa'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 2
              end
              object meJustificativa11_Altera: TMemo
                Left = 176
                Top = 80
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 3
                OnChange = meJustificativa11_AlteraChange
              end
              object Edit25: TEdit
                Left = 682
                Top = 88
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 4
                Text = '255'
              end
              object rgItem12_Altera: TRadioGroup
                Left = 1
                Top = 124
                Width = 720
                Height = 56
                Caption = 
                  '12. Continuidade no desenvolvimento de seus conhecimentos e habi' +
                  'lidades na aprendizagem'
                Items.Strings = (
                  'Adequado'
                  'Precisa ser aprimorado')
                TabOrder = 5
              end
              object meJustificativa10_Altera: TMemo
                Left = 176
                Top = 22
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 6
                OnChange = meJustificativa10_AlteraChange
              end
              object Edit26: TEdit
                Left = 682
                Top = 146
                Width = 31
                Height = 21
                MaxLength = 3
                ReadOnly = True
                TabOrder = 7
                Text = '255'
              end
              object meJustificativa12_Altera: TMemo
                Left = 176
                Top = 138
                Width = 500
                Height = 38
                MaxLength = 255
                ScrollBars = ssVertical
                TabOrder = 8
                OnChange = meJustificativa12_AlteraChange
              end
            end
          end
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 2
    Top = 533
    Width = 767
    Height = 49
    TabOrder = 2
    object imgMensagem: TImage
      Left = 8
      Top = 8
      Width = 32
      Height = 32
      AutoSize = True
      Picture.Data = {
        055449636F6E0000010009003030100001000400680600009600000020201000
        01000400E8020000FE060000101010000100040028010000E609000030300000
        01000800A80E00000E0B00002020000001000800A8080000B619000010100000
        01000800680500005E2200003030000001002000A8250000C627000020200000
        01002000A81000006E4D0000101000000100200068040000165E000028000000
        3000000060000000010004000000000080040000000000000000000000000000
        0000000000000000000080000080000000808000800000008000800080800000
        80808000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
        FFFFFF0000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000888888
        888870000000000000000000000000000000000888888888888FFF8800000000
        000000000000000000008888887C6C7C6C7678FFF80000000000000000000000
        00088887C6C76C7C7C6CC7C78FF700000000000000000000088886C6C76C7C66
        C7C76C7C688F800000000000000000008887C747C6C7C7C7C7C7C7C7C6C88800
        0000000000000008886C6C7C7C7C647C6C7C6C6C7C7C88880000000000000088
        847656C665667877876C767C6C76C7F8000000000000088846C7C67C7CC788F8
        FF76CC7C7C6C7C788000000000000886C7C6C7C6C67C88FF8F7C7C7C67C7C6C8
        880000000000887C76C76C747C6C8F8F8F7C76C7C6C7C76C88700000000887C6
        474656C6C747888FF88C6C7C7C7C6C76C88000000008867C7C6C7C7656C68888
        F886C7C6C76C76C76888000000887C67C67C66C7C67C8888887C7C67C6C7C7C6
        C4880000008867C647C7C746C7C678888F7C67C67C7C6C7C7C8870000088C7C7
        C6646C7C647C8888887C6C7C6C67C646C76880000887C67647C7C7647C648788
        8876C7C656C656C76C48800008867C6C7C6746C7C67C7878787C746C7C7C6C7C
        74778700088C67C767C6C7467C648788887C6C76C647C746C6C788000877C76C
        7C7C76C7C6568877877C76C747C66C7C746C8800087C7C86C766C74647C67887
        8876C65C6C7C7466C7C7870088767EC77C7C7C6C7C6C8787877C6566C746C7C7
        46648800887C7C7C7E7C767C67477887877C6C656C65646C7C7C880088867C7E
        C7C76C76C6C68878887656C656C6C7C646468800F87C7E7C77C77C7C765C8888
        787C6C7C67C764765C7C8800087EC7C7C7EC8676C7668887886C7647C656C7C6
        C766870008857C868C77CEC87C7C88888876C7C656C656476C658800088CE7C7
        C8CE77C7C8C78F88887C746C6C76C6C656C7870008877C8E767C7C86867E8F8F
        887C64765647C76C656880000888C86C7C868C8C67C7C8C87C77C7C7C7C67C77
        C7C880000088E7C87EC8C868C8C8676C867C867C767C76C67C7880000088777C
        8C867CE7C867C878C7CE7C7EC7C7C76C76880000000F8C8EC86777C867C8E88F
        F867C8C77C866C86C7880000000888C87C8CE77C8E7C8FF8FF7C867CE7C77C7C
        788000000000888CE777C868C7778F8FF887CE7C7C7EC767888000000000F888
        7C8E7C8C8CECFFFFFF8E7C7768C767C78800000000000FF8E8C8C8E7E78788F8
        FF7C77C8C67C7C8888000000000000FF8777E7C8C8C7CFFF88C8C8EC8C867E88
        800000000000000FF877C88C87E86768C8E7EC7767CE58880000000000000000
        F8888C8E7C8C8C8C86C777C8C868888000000000000000000FF88E77877E78E7
        687C8C867C888800000000000000000000888887C8C8C8C8C8C8E7C888880000
        0000000000000000000088888E8868C8E7E7C888888000000000000000000000
        0000008F888887878788888880000000000000000000000000000000F8888888
        8888880000000000000000000000000000000000000008888000000000000000
        00000000FFFFFFFFFFFF0000FFFFFFFFFFFF0000FFFFC007FFFF0000FFFE0000
        FFFF0000FFF000003FFF0000FFE000000FFF0000FF80000007FF0000FF000000
        03FF0000FE00000000FF0000FC00000000FF0000F8000000007F0000F8000000
        003F0000F0000000001F0000E0000000001F0000E0000000000F0000C0000000
        000F0000C000000000070000C000000000070000800000000007000080000000
        0003000080000000000300008000000000030000800000000003000000000000
        0003000000000000000300000000000000030000000000000003000080000000
        0003000080000000000300008000000000030000800000000007000080000000
        00070000C000000000070000C0000000000F0000E0000000000F0000E0000000
        001F0000F0000000001F0000F0000000003F0000F8000000003F0000FC000000
        007F0000FE00000000FF0000FF00000001FF0000FF80000003FF0000FFC00000
        0FFF0000FFF000001FFF0000FFFC00007FFF0000FFFF0003FFFF0000FFFFF87F
        FFFF000028000000200000004000000001000400000000000002000000000000
        0000000000000000000000000000000000008000008000000080800080000000
        800080008080000080808000C0C0C0000000FF0000FF000000FFFF00FF000000
        FF00FF00FFFF0000FFFFFF000000000000000000000000000000000000000000
        0000878878700000000000000000000008888877888F88000000000000000008
        8876C6CC6C6C88F700000000000000887C6C7C76C7C7C67F8000000000000886
        C7C7C7C7C7C67CC788000000000087C7466C678887C7C76C688000000008866C
        7C74778F886C6C7CC788000000084C746C7CC88FF8C7C7C76C687000008767C7
        C766678888C6C66C7C7C8000088C7C666C5C7788886C7C7C6C7688000876C74C
        766C678888C76C7C74C77800087C767C6C74C78887C6C746C76C68000867C6C7
        47C76787876C74C76C65C88088C67C76C66C568877C7466C74C67770877C76C7
        65C7678787C6C7C7C67C687088C7E5E5C76C67887847C6466C65C780886C7C77
        EC76C88787C6656C7566687088C868C7C7C7C788876C7C7C6C6C5780087C7CE7
        7E7C788887C746C74747C800087E77C7C7C8688888C6C7646C7C6800088C7C8E
        7C86C7C7C6776C7C766788000088C86C87C87E788C7C867C7C7C88000088EC87
        C8EC8CFFF867C7C867C88000000887C8EC87CFF8F8C867EC7C780000000F88EC
        87C8E7FFF8EC8C77C78800000000F887C8E7C8888C77C7CE7880000000000F88
        E7C88C7E78C8E77C8800000000000088888C8EC8C7EC7C888000000000000000
        8888C88C8C878880000000000000000000888888888880000000000000000000
        000008888800000000000000FFFFFFFFFFF01FFFFF8003FFFE0000FFFC00007F
        F800003FF000001FE000000FE0000007C0000007800000038000000380000003
        8000000100000001000000010000000100000001000000018000000380000003
        80000003C0000003C0000007E000000FE000000FF000001FF800003FFC00007F
        FF0001FFFFC007FFFFF83FFF2800000010000000200000000100040000000000
        8000000000000000000000000000000000000000000000000000800000800000
        0080800080000000800080008080000080808000C0C0C0000000FF0000FF0000
        00FFFF00FF000000FF00FF00FFFF0000FFFFFF00000000000000000000008777
        7880000000086CC6CC678000007C767886CC78000864C7CF8C76C70007C76C78
        86C7C680867C67688C7C7C708C76C5687464647087C76C787C7C7C70868C8678
        86476670FC7EC8C87C7C6C80087C8C888C767C800F87E77F8E7C78000088C8C8
        7C868000000F88E7E78800000000088888000000FFFF0000F01F0000E0070000
        C003000080030000800100000001000000010000000100000001000000010000
        8001000080030000C0070000E00F0000F83F0000280000003000000060000000
        0100080000000000000900000000000000000000000100000001000000000000
        9D4626009D492C009C4B3700A4452400AB452500A5482600A8482700AB462900
        AE462900A64A2800A8492800AF482A00A84C2A00AB4F2D00B0442700B2452800
        B5452800B9462A00BC462B00BA472C00BD472C00B7492D00BE482C00AD512F00
        A54F3400AE4B3100A94F3000A3523300AD533100AF583500AF523800AE553A00
        AB593B00B1563300B3583500B2513800B5533A00B55B3900B85E3B00B7603C00
        BA613E00A75F4700AD5D4200AE604600B1634400BD644100B4614B00BB6A4800
        AA695100B16B5000B06E5300B76F5300B26C5600B9695500AA735E00BE715200
        B3735A00A3796B00AA7C6E00B6766300B9756300B17F6A00AC7E7000C0654300
        C2694500C56C4900C86E4B00C7704D00C9714D00C4735200CD755100CF795500
        C27A5D00D0775300D2795500D57D5900D87E5A00CA7F6200B3816C00B8806D00
        AF807400B1827600B9867300B9897600B7867900B8857800B9887A00BE887A00
        BA887C00BC8B7D00BA8E7D00BE8C7E00D5815E00DA815D00C2816700CD846800
        D5846400DD846000D98A6A00CA8B7200C08C7E00D3937900E0866200E4896500
        E68D6900E3916F00E9916E00E5937200EA927000EC987700E2977800E09A7E00
        E89E7F0098969600999798009C9B9B00A58C8400A88F8700AB938B00BB958700
        BA978A00BC9B8F00A29F9F00B39C9500B7A09900BCA59E00A1A1A100A5A1A100
        A6A3A400A5A5A500A8A4A400ACA7A800A9A9A900AEABAB00ACABAC00ADADAD00
        BCAAA400B3AEAD00B9ACA800B8B0AF00B1B1B100B4B2B200B3B2B400B5B3B400
        B6B5B500BAB5B600BAB7B800B9B9B900BDB9B900BFBBBC00BEBDBD00C3958200
        C9968200CD9D8900DD9D8200C19E9100D2A08C00C1A29600CDA29100CCA79A00
        CBAB9F00DCAA9400D5AB9A00D9B09D00EDA48500E7AA8F00EFA88A00E2AB9400
        C1A9A000C4ABA300C2ABA400C6ADA500C8ADA600D0ADA000C4B3AE00CCB1AA00
        C8B3AD00CAB4AD00CEB5AE00D3B0A400D7B7AB00D2B8AD00DBB9AA00C1B4B100
        CCB7B000C0B9B700CDB9B300C1BBBA00C5BAB800C0BBBC00C1BDBD00C4BEBE00
        CCBFBB00D0BAB300D5BDB300D0BCB700D7BCB500DCBFB700E8BBA800CBC0BE00
        CFC1BF00D8C1B600D0C2BF00E3C6BB00C4BFC000C2C1C200C5C0C100C6C2C400
        C6C5C500C8C1C000CDC3C200CDC4C200C8C3C400C9C5C600CCC4C500CFC8C700
        CAC7C800CCC7C800CAC8C900CDC9CA00CDCBCC00CECCCC00D0C6C400D3C8C300
        D2CBCB00D0CDCD00D2CFD000D5D3D400D8D5D500D9D7D800D9D8D900DCD9DA00
        DDDCDC00E3CCC300EBD2C800E3DBD900E1DDDE00E0DFE000E2E1E100E4E3E300
        E6E5E500EAE9E900ECEAE900EEEDED00F2EEEC00F5F0EF00F0EFF000F1F1F100
        F4F3F300F4F4F400F8F7F600F9F8F700F7F7F800FEFEFE00FFFFFF0000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000828EBFDEE2E3E3DE95858200000000000000
        0000000000000000000000000000000000000000000000000000008EDED5D6DE
        DEBFBABABFD6E5EDF1FAED820000000000000000000000000000000000000000
        000000000000000085D5D5D9C89C3C200911111212121A2F5AC2F7FEFA8E0000
        0000000000000000000000000000000000000000000000CED5DEAA350C111111
        12121212121217171217245AECFAF18200000000000000000000000000000000
        000000000085D9D5B12C09110C110C17111712121712171717171717175AECF1
        950000000000000000000000000000000000000085BFD6500909090911111111
        0C1212121217121712171712121224B0EDD50000000000000000000000000000
        0000008E95CB2B090909090909110C1111111111121212121717171717171212
        5AEDDE7400000000000000000000000000008E92B91A0709090407110911110C
        36515252525103121712171717121712123DE4D6000000000000000000000000
        008592C21B0C07090407090909091111C6F7F1F7F1F751171217121712171217
        12123DE5BF00000000000000000000000095D6210C0707040709090905091107
        C5F1F2F2F7F15A1212121217121712121212125AE38500000000000000000000
        95D6320C070A070C0904090505090511B6EDF1F1F1F756121212121212121212
        12121111ADD67300000000000000008E95780C070A0A07070407090505090511
        AAE3EDEDF1F25A1112121212121212121211111120E2920000000000000000BF
        C81C0A0A0A0C070707090409050509097ED5DBE6EDF155111111121212121112
        111111111153D6740000000000009592530E0C0A0A0707070407090405050505
        7C97CEDBE3E755111111111111111111111111090909D69200000000000095D5
        1B220D0C070A070707040409040905097C919497D1DE51111111111111111111
        11091109090953CE7200000000008E9C22221B0D070A07070707070405040505
        778D8E9497CE520909090909090909090909090505051ADE840000000095D535
        2222220E070A070707040709040904057785858E91943F090909091109050F09
        05050F0505050989920000000095D51E222722220E0707070707040404040904
        758585858D913A09050505050F0505050F05050505050451BF74000000BF7E27
        2227272722070A070707070709040405757F8585858D3A050505050505050505
        050505050405052CD5730000009279272727272227220A0A0707070407040704
        7582828285853A0405050505050505050504050404040401D980000000925027
        272727272727220A0707070704070404777F828282823A040504050405040504
        040504040404040492860000E4954F27272727272E2727220A07010707040404
        778582827F823A04040504040404040504040404040404048B8A0000E4954F27
        412E2E4027402E27270A0A07070707047C85858582823A040404040504040404
        04040404040404048A8A0000E4954F27412E2E4040402E40272E0E0701070107
        7E8E858585823A040404040404040404040404040404040486860000E6954F41
        2E412E4040404041404040220A070701A9958E8E85853A040404040404040404
        04040404040404048B8C000000BF5A41412E414140404141414041412E220707
        AA95958E8E853A0404040404040404040404040404040404BE81000000D57941
        41414241424242414141414141412E22B1D59795918E3A040404040404040404
        0404040404040402CE80000000D68B4542424242424242414342424241424142
        EAEDDECE97943B01040404040404040404040404040404379273000000DECD49
        45424342434243434245424343424242CCF7F2F1E9E354070104040404040404
        04010101040401798A00000000E4D69847454545454545454545454343434342
        47606060604E414127271E0E0E0D0A0A0C0A0C0E0E0E1BC88A0000000000DE89
        6145454545454545454545454545454342424242424141412E2E2E2E27272728
        2722271E221E39BF810000000000DED6644B4B4B454B48454B48454A45474547
        4742394F312D414141402E2E2E2E272727272222221EAA8E00000000000000E2
        AA634B4B4B4A4A4B454B4B4B4A4A474746B5F7F7F79E2D42414140402E2E2E27
        282727272732D58A00000000000000DEDE9A4D4B4B4B4B4C4B4C4B4B484A4A47
        A2FAF7F7F7F75442424241412E2E2E2E2827272721B192000000000000000000
        E4C8704D4D4C4C4B4C4B4B4B4B4B4B4BEBF7F4F4F4F4B64242424241412E2E2E
        272D272853BF8E000000000000000000E4EFC36F4D4D4D4D4C4D4C4D4D4B4C4B
        EBFAF8FAFAF4B645454242424141412E2E272734D6BF00000000000000000000
        00ECFAB56B4D674D4D4D4D4D4D4D4D4CA8FAFAF8FAFA66454545424242414141
        2E4130C99292000000000000000000000000FAFCB76E67676767674D674D4D4D
        4DC7F5FAF5A24B4747454542423041414130C28E8E0000000000000000000000
        000000FEFAB77167676767674D614D4D4D4D61634D4B4B484745454245424241
        34C2949400000000000000000000000000000000FAF8C6A66968676367676367
        4D4D4D4D4D4B4B48474847474242425FD8929500000000000000000000000000
        0000000000F1F2E0A4A569686767676763674D4D4D4D4B4B4B4747474546A0D8
        92970000000000000000000000000000000000000000E6EDE2B2A2A76D696967
        676767674D4D4D4B4B4B47479AD8959500000000000000000000000000000000
        0000000000000000E2E4D8B2A4706E6B696767674D4D4D4D4864B4DE95BFD900
        0000000000000000000000000000000000000000000000000000DEE4D8D8B8A0
        9F99999A999DB4B2DED6BFBFDE00000000000000000000000000000000000000
        000000000000000000000000E6DEDEDED8D1CDBCCDCDCDD895D5000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00E6E6E4E60000000000000000000000000000000000000000000000FFFFFFFF
        FFFF0000FFFFFFFFFFFF0000FFFFC007FFFF0000FFFE0000FFFF0000FFF00000
        3FFF0000FFE000000FFF0000FF80000007FF0000FF00000003FF0000FE000000
        00FF0000FC00000000FF0000F8000000007F0000F8000000003F0000F0000000
        001F0000E0000000001F0000E0000000000F0000C0000000000F0000C0000000
        00070000C0000000000700008000000000070000800000000003000080000000
        0003000080000000000300008000000000030000000000000003000000000000
        0003000000000000000300000000000000030000800000000003000080000000
        00030000800000000003000080000000000700008000000000070000C0000000
        00070000C0000000000F0000E0000000000F0000E0000000001F0000F0000000
        001F0000F0000000003F0000F8000000003F0000FC000000007F0000FE000000
        00FF0000FF00000001FF0000FF80000003FF0000FFC000000FFF0000FFF00000
        1FFF0000FFFC00007FFF0000FFFF0003FFFF0000FFFFF87FFFFF000028000000
        2000000040000000010008000000000000040000000000000000000000010000
        0001000000000000A5432300A3442300A4442300A6432400A2452400A5452400
        A8432400A9452500AD442600A3482600A6482600A8482700AF462800A6492800
        A84A2800AD482900A84C2A00AA4E2C00B1442700B2452800B5452800B9462A00
        BC472B00BC472C00B6482C00BA482C00BD482C00AC512F00AD523000AF553300
        A8523600AD563A00AB593B00A95A3C00B1553200B1563400B2583600B55B3700
        B5533A00B0543900BA543B00B55A3800B65D3A00B85D3A00B95E3C00B9603D00
        BC623F00B0624200BD624000B9654200BE654200B9664600BD684500B76A4B00
        BF705C00A6746400A8766700AF756600AA7A6900AD7B6B00BB7E6D00C1664400
        C1684500C4694700C36A4800C56B4800C56D4900C86E4B00C96F4C00C9714D00
        CC734F00C3755600CD755100CF775400C57B5E00D0765200D1795500D47B5700
        D07C5A00D57D5900D87E5A00B6816D00BB826F00A3877D00A4887F00B1817200
        BA857500B9887500BD8A7600B9877800B7897800BB8B7A00BE887800BC8C7A00
        BB8C7C00D9805C00DC825E00CE806100C6826800D4816000DE846000C3827200
        C88D7E00D38C7000C2917F00DF927200D4967F00E0866200E2886300E4896500
        E68C6800E2957500E9937000E1997C00E99D7E009A9797009C9A9A009E9C9C00
        A6898000A78C8200AB8C8300B08C8000AB908600B7948700BC938700B8948700
        B0918800BC938800BF958A00B6988E00B7999000BA9D9300BB9E9400A2A1A100
        A5A4A400A9A6A600A9A8A800ACABAC00ADADAD00B0AEAE00B9ABA800BCAEAA00
        BDAFAC00B2B1B100B5B2B200B6B3B400B5B5B500B9B5B600BAB7B800B9B9B900
        BCB9B900BCBCBC00C38F8100CB978400CC958700C39B8B00C79D8C00DB9B8000
        D0A18F00C0A09700CEA49200C8A39500C1A19800C3A49C00C4A59C00C9A69900
        C9A89F00D3A59400E0A38A00E3A58A00E5B29D00C7ABA200CAA8A000CCAAA200
        CAADA300CBAEA400C5AFA900C6B0AA00C0B2AF00C6B2AC00C9B5AF00D7B3A400
        D9B3A300DFBBAC00C3B6B200C5B7B300CCB5B000C6B8B500C8BBB700C2BAB900
        C0BCBD00C7BEBD00CBBFBC00D0B8B300E2B5A300C9C0BF00D8C6BF00E7C1B100
        C1C1C100C5C1C200C6C2C400C6C5C500CDC4C300CAC6C700CBC9C900CCC9C900
        CDCBCC00CFCCCD00D2C5C100DDCCC500D4CDCB00D0CECE00D2D0D100D6D5D500
        DDD6D500D9D9D900DDDCDD00E2CFC800E5D1CA00EDD6CD00E6D9D400ECDAD200
        E1E0E100E5E4E500EAE9E900EEECEB00EEEEEE00F0EFEF00F1F1F10000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        FFFFFF0000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000008A878A878A8787000000000000000000
        000000000000000000000000008A95D4B9A07D7D81A5D1E5D88A000000000000
        000000000000000000000090C9BC5D2815161616161B162967DDE08700000000
        000000000000000000008CC45310151515191616161B1B1B1B1B66D790000000
        00000000000000000090B220101010141515151616161B161B161629C2BF0000
        000000000000000090A71010101010141437A5A5A53A161B1B1B1B161BAE9700
        000000000000008CB210110B10101010149BE1E1E1AE1616161B16161619BB89
        00000000000000BF21110C0C061010101467D8E0E1A7161616161616161528CB
        760000000000955210110B0C100610071058CDD6E0A71516161616151516143D
        9700000000C9B91C110B100B06100107103C97C9D6A015151515151515141410
        B976000000905D241011110B06061010073C9097988310141414141414101010
        538A000000C9302525110B0B0B0B060610388A8C907F10101410101010070709
        1FC80000008E25252523110B0B060B06063887898A7910071007070707090709
        068E8700D4A02B2E2C2C23110611060606388787895510070707070707070701
        067F7600CD7C2E2E2E2E2C2311060B06063C8987875401010707070707010101
        067A7600CD7C2E2E2E332E332A100B0B0B568A89875406010601010101010106
        067A7400CD81333F3F333333332E1D0B065B908B895506060606060101010106
        067A7600D6A0333F3F3F3F3F3F3F332E1C5DC7968B7806060606060202020202
        02827600008E433F434343434343433F3FA8D5C7977B02020202020202020202
        028C000000CB4B464646444444444444426BDBDAD1691D110B02050505110A12
        22BF000000CD9D46464646474646464444464242423F3E3E2B3E2A2B2A242424
        5B8800000000BC644C4C4C4C4C4C4C49494663A6583E353E3E302E2E2B2A2125
        959100000000CDA84C4C4C4C4C4C4C4C4968E1E5E19C4242333F3E2E2B2B2B58
        95000000000000D3A96060605060505050C3E5E5E5D24342423333333E2B36C0
        00000000000000D8DD6A60606050605050A9E5E5E5B8494343433F333332B290
        0000000000000000E4DD716D656560606050ABDCC65049494643433333B19400
        000000000000000000E4DDAA6D6D6C6565606050504D49494943434BB5940000
        00000000000000000000E0D7B7736F6D6D6565605050504949499AC097000000
        000000000000000000000000D6C1B672716F656560515062A6BCBF0000000000
        0000000000000000000000000000CDD4B9AEA7A6A7AEBCCEBF00000000000000
        0000000000000000000000000000000000D6D4CDD4D600000000000000000000
        00000000FFFFFFFFFFF01FFFFF8003FFFE0000FFFC00007FF800003FF000001F
        E000000FE0000007C00000078000000380000003800000038000000100000001
        00000001000000010000000100000001800000038000000380000003C0000003
        C0000007E000000FE000000FF000001FF800003FFC00007FFF0001FFFFC007FF
        FFF83FFF28000000100000002000000001000800000000000001000000000000
        00000000000100000001000000000000A4442300A6432400A2452400A5452400
        A9452500AD442600A6482700A8482700A7492800A94A2800A94E2D00B1442700
        B2452800B6452900B9462A00BC472B00BD472C00AC512F00AD523000B0543200
        B0563A00B65A3800B35C3900B75D3B00B95E3B00BA623F00A75E4500AD614A00
        AC664D00B5664700BE654100BD604A00BC644D00A9685200AC6C5600B6685200
        BB6D5900BF725F00AD746000BC766400B6796300BF7A6100BC796700C1664400
        C36A4600C36E4B00C76F4C00C86F4C00C8704C00CE765200CB785800CE7A5800
        D0775300D2785400D17B5800D67D5900D97F5B00C87F6100C67D6C00B4827200
        DA815D00DD835F00DD846000D78D6E00C88A7200CC917900DA947700E0866200
        E38A6700E0987B00B3928700B9958B00BE998E00A5969200A6989300AB9B9700
        BA9E9500AFA09B00AFA29D00BBA29800A5A5A500A9A4A300ACA7A600AAAAAA00
        AEAEAE00B7A9A400B1ADAD00B5B2B200BBBBBB00CE948600C99E8D00D59E8900
        C19F9400DEA18800CDAB9D00E0A28800E3A68D00CAABA300C2B1AD00C2B4AF00
        D7B8AC00C5B5B200C6B9B400CBBAB500CCBDB900CAC1BF00DBC0B600C2C2C200
        CAC7C700D6C6C300D0C7C500D8C7C200D2CECE00D9D8D800DDDBDC00DFDDDD00
        E8DAD500E2E0E000EBE7E600F2F1F10000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000FFFFFF000000000000000000000000000000000000000000
        57492B25265A6600000000000000004D150D0D0F0F0F0F3B530000000000480A
        080D216E620F110F2153000000560A0A08082472700F0F0F0E28000000290A0A
        08081C59630D0D0D0D084C006D17140908081B544E08080D08063C0063191914
        07071B514A08060202022200631F1F1F170A1D544C070101010222006A2D2D2D
        2D2C2A6C4E07010201032700723A31313030335C4114141212124700005F3635
        3535336B5B2D1F1F171E58000072603938384378752D2D1F1F50000000007760
        453E3E5E4035312D5D000000000000766546453E383842670000000000000000
        00726F686971000000000000FFFF0000F01F0000E0070000C003000080030000
        8001000000010000000100000001000000010000000100008001000080030000
        C0070000E00F0000F83F00002800000030000000600000000100200000000000
        8025000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000B0B0B010B0B0B010A0A0A010000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000001A1A1A041616160C
        0F0F0F170B0B0B240C0C0C350A0A0A3F0B0B0B420A0A0A3E0707083306060624
        090909190B0B0B0E0E0E0E060C0C0C0100000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000002525250316161711161516384B4B4B797F7E7FAF
        A4A2A3D6BDBABBEECDCACBF8D0CDCEFCD2CFD0FCD2CFD0FACCC9CAF4B4B3B3E5
        939292C76767679D2A292A660404043009090916101010060000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000313031051D1D1D204C4C4D76A1A0A2CDCCC9CAFBC8C4C4FFC9C4C5FF
        CFCCCDFFCEC9C8FFC2BBB9FFC0B9B7FFC0B9B7FFC2BBBAFFCCC4C3FFD8D6D6FF
        E0DEDFFFE9E8E8FFF5F4F4FED7D6D7EC7F7F7FAE2020205B0707072114141408
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000037373702
        2120211A5D5D5E82BDBBBDE7C8C4C5FECBC7C8FFCBC0BEFEC29D91FEB67663FF
        AE553AFEAF472BFEB64629FFB74629FEB7472AFEB8472AFFB94729FEAE4B31FE
        B4614BFFBE897BFED0BAB3FEF2F0F1FFFDFDFEFEF4F4F4FA9A9A9AC32222225E
        0808081D13131304000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000353435073A393A54
        B5B3B5DAC6C1C2FECDC9CBFEC7ADA5FEB26C56FFAF482AFEB44629FEB5472AFF
        B7462AFEB8462AFEB9472BFFBA472BFEBB472BFEBC472CFFBD472BFEBD472BFE
        BE482CFFBE472CFEBD472CFEB25138FFC08C7EFEE3DBD9FFF5F4F5FEE5E4E4F8
        767575AC0707083A0F0F0F0C0000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000002E2D2D11706F7094C9C6C7FB
        C7C3C4FFCBB4AEFFAE6046FFAE4727FFB04728FFB24728FFB34729FFB54729FF
        B6472AFFB7472AFFB9472AFFBA472BFFBB472BFFBC472BFFBC472CFFBD472CFF
        BE482CFFBE482CFFBE482CFFBE482CFFBE482CFFB7492DFFBF8879FFE2DDDDFF
        E3E1E1FFB3B1B1E0232223610A0A0A1613131301000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000003433341C918F90BBBFB9BAFECFC8C7FE
        B8806DFFAB4727FEAD4727FEAE4628FEB04628FFB14628FEB34629FEB44629FF
        B54629FEB7462AFEB8472AFFB9462AFEBA472AFEBB472BFFBC472BFEBC472BFE
        BD472CFFBD472CFEBE472CFEBE472CFFBD472BFEBE472CFFBD472BFEB5533AFE
        CCB1AAFFDCD9DAFEC7C4C4F43D3C3D7D0808081B161516010000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000003837371DA19FA0CDBCB6B7FED0C2BFFFAD5D42FE
        AB4727FFAB4727FEAC4727FEAE4627FEAF4628FFB04628FEB24628FEB34629FF
        B54629FEB54629FEB34528FFB34429FEB44429FEB54429FFB6442AFEB9462BFE
        BC472CFFBD472BFEBD472BFEBD472CFFBD472BFEBD472CFFBD472BFEBD462BFE
        BA472CFFBE8C7EFEDBD9DAFECCC9C9FA474646880908081A1414140100000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000403F3F119F9C9DC7BCB6B7FFCCB7B0FFA94F30FFAA4827FF
        AB4727FFAB4727FFAC4727FFAD4727FFAE4627FFB04628FFB14628FFB34628FF
        B44629FFB96955FFAF8075FFAF8276FFB08277FFB08277FFB08176FF9C4B37FF
        BB462BFFBC472BFFBC472CFFBD472CFFBD472CFFBD472BFFBC472BFFBC472BFF
        BC472BFFBB462BFFBA7663FFD8D4D5FFC9C6C6FA3C3B3B7C0C0C0C1400000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000504F4F06858484A6BAB5B5FFCDBAB4FEAA5132FEAA4928FFAA4827FE
        AA4727FFAA4727FEAB4727FEAC4627FEAD4627FFAF4627FEB04628FEB24628FF
        B34628FEDCBFB7FEEAE9E9FFEAE9E9FEEAE9E9FEEBEAEAFFEAE9E9FEB88578FE
        B9462AFFBB462BFEBB462BFEBC472BFFBC462BFEBC472BFFBC462BFEBB462BFE
        BB472BFFBB462AFEBA462AFEB97563FFD7D5D6FEBEBABAF32121215F1212120A
        0000000000000000000000000000000000000000000000000000000000000000
        646263016362636BBBB6B6FDCEC4C2FFAB593BFFA94929FFA94927FFA94827FF
        AA4727FFAA4727FFAB4727FFAB4627FFAD4627FFAE4627FFAF4627FFB14628FF
        B24628FFD7BCB5FFE7E6E6FFEAE9E9FFEBEAEAFFEBEAEAFFEBEAEAFFBA887BFF
        B8462AFFBA472BFFBA472BFFBB472BFFBB472BFFBB472BFFBB472BFFBA462BFF
        BA462AFFB9462AFFB9462AFFB84529FFBC8B7DFFD3CFD0FFA3A0A0E008080836
        1817170300000000000000000000000000000000000000000000000000000000
        58575825B8B4B4EECAC6C6FEAD6950FFA94B29FEA94928FEA94928FFA94827FE
        A94827FFA94727FEAA4726FEAB4626FEAC4627FFAD4627FEAE4627FEB04627FF
        B14528FECFB6AFFEDEDDDDFFE4E3E3FEE9E8E8FEEAE9E9FFEAE9E9FEBB887CFE
        B74629FFB9462AFEB9462AFEBA462AFFBA462AFEBA462AFFB9462AFEB9462AFE
        B9462AFFB84529FEB84529FEB74629FFB44629FEC8ADA6FEC9C4C5FF666465AA
        0B0A0A1800000000000000000000000000000000000000000000000075737401
        918F90A9BCB6B7FEBB9587FEA94C2AFFA84A28FEA84928FEA84928FFA94827FE
        A94827FFA94727FEA94626FEAA4626FEAB4626FFAC4626FEAD4527FEAE4627FF
        B04527FEC5ADA6FED2D2D2FFDADADAFEE1E0E0FEE7E6E6FFEAE9E9FEBA897CFE
        B64529FFB74629FEB84629FEB8462AFFB84629FEB8462AFFB84529FEB84529FE
        B74629FFB74529FEB64529FEB64529FFB54528FEAF5238FED2CBCBFFBBB7B7F8
        1E1E1E581A1A1A0600000000000000000000000000000000000000006F6E6E35
        BEB9B9FBCBC0BEFEA65131FEA84B29FFA84A29FEA84928FEA84928FFA84827FE
        A84827FFA84727FEA94626FEA94626FEAA4626FFAB4626FEAC4526FEAD4527FF
        AE4527FEBCA59FFEC6C5C5FFCECDCDFED6D6D6FEDEDDDDFFE4E3E3FEB9887BFE
        B44528FFB64529FEB64529FEB74629FFB74529FEB74629FFB74529FEB64529FE
        B64529FFB54529FEB54528FEB44528FFB34528FEB24527FEBA8677FFCAC5C6FE
        7C7979C00C0C0C1B0000000000000000000000000000000000000000959394A9
        BDB8B8FFB98774FFAB4F2DFFA84B29FFA84B29FFA84A28FFA84928FFA84827FF
        A84827FFA84727FFA84726FFA94626FFA94626FFAA4626FFAB4526FFAC4526FF
        AD4527FFB7A099FFBDBCBCFFC2C2C2FFC9C9C9FFD2D1D1FFDAD9D9FFB78679FF
        B34528FFB44528FFB54529FFB54529FFB54528FFB54528FFB54528FFB54528FF
        B44528FFB44528FFB34528FFB34528FFB24527FFB04427FFAB4629FFCDC3C2FF
        BAB5B5FA1C1C1C541A1919030000000000000000000000007674751DBBB7B7F4
        C8C1C0FFAC5736FEB05431FEA94C29FFA84B28FEA84A28FEA84928FFA84827FE
        A84827FFA84827FEA84726FEA84626FEA84626FFA94626FEAA4526FEAB4526FF
        AC4526FEB19B94FEB6B6B6FFBABABAFEBFBFBFFEC5C5C5FFCDCDCDFEB38376FE
        B14427FFB34528FEB34528FEB34528FFB34528FEB34528FFB34528FEB34428FE
        B34528FFB24427FEB14427FEB14427FFB04427FEAF4426FEAE4426FFB9897AFE
        C4BFC0FE646262A91111110F0000000000000000000000009694956DB8B2B3FE
        C19F92FFB25634FEB25634FEAD512FFFA84B28FEA74A28FEA74928FFA74827FE
        A74827FFA74826FEA74726FEA84726FEA84626FFA84626FEA94525FEA94526FF
        AA4526FEAD948CFEB0AFAFFFB4B3B3FEB8B8B8FEBDBDBDFFC2C1C2FEAF8073FE
        AF4426FFB14527FEB14427FEB14527FFB14427FEB14527FFB14427FEB14427FE
        B14427FFB04427FEAF4427FEAF4426FFAE4426FEAD4426FEAC4426FFA54F34FE
        CECACBFEA5A0A1EA09090927000000000000000000000000A5A2A3BAC5C1C2FF
        B06E53FFB35835FFB35835FFB35734FFAA4E2BFFA74A28FFA74A28FFA74927FF
        A74827FFA74827FFA74726FFA74726FFA74626FFA84626FFA84625FFA84525FF
        A94525FFA99088FFABAAABFFAEADAEFFB2B1B2FFB6B5B5FFBABABAFFAC7E70FF
        AD4426FFAF4426FFAF4426FFAF4426FFB04427FFAF4427FFAF4426FFAF4426FF
        AF4426FFAE4426FFAE4426FFAD4426FFAC4425FFAB4425FFAB4425FFAA4526FF
        C2ABA4FFBBB6B6FE2524245D1D1C1C03000000009895960DBBB7B8EFC4BFC0FE
        AF5835FFB45A36FEB45937FEB45936FFB35734FEA84C29FEA74A28FFA74927FE
        A74827FFA74827FEA74826FEA74726FEA74626FFA74626FEA74625FEA74525FF
        A84525FEA68D85FEA7A7A7FFA9A9A9FEACACACFEB0B0B0FFB3B3B3FEAA7C6EFE
        AB4425FFAD4426FEAD4426FEAE4426FFAE4426FEAE4426FFAD4426FEAD4426FE
        AD4426FFAC4425FEAC4425FEAB4425FFAA4425FEAA4325FEA94424FFA94526FE
        B58676FEBEB9B9FF565454961B1A1B08000000009B999934C0BCBCFEC0A8A0FE
        B55C38FFB55B37FEB65B38FEB65B38FFB65B38FEB25633FEA74B28FFA74928FE
        A64927FFA64827FEA64827FEA64726FEA64726FFA74626FEA74625FEA74625FF
        A74525FEA48C84FEA4A4A4FFA6A6A6FEA8A8A8FEAAAAABFFAEADADFEA77A6DFE
        A94325FFAB4425FEAB4425FEAB4425FFAB4425FEAB4425FFAB4425FEAB4425FE
        AB4425FFAA4425FEAA4325FEA94424FFA94324FEA84324FEA74324FFA74525FE
        A75F47FEC7C3C4FF7D7A7AC21918181000000000AAA8A95DBAB4B4FFBA978AFF
        B75D39FFB75D39FFB75D39FFB75D3AFFB75D3AFFB85C39FFB15533FFA74A28FF
        A64928FFA64827FFA64826FFA64826FFA64726FFA64726FFA64625FFA64625FF
        A64625FFA58D85FFA4A4A4FFA4A4A4FFA5A5A5FFA7A7A7FFA9A9A9FFA5796BFF
        A84324FFA94425FFAA4425FFAA4425FFAA4425FFA94425FFA94425FFA94424FF
        A94424FFA84424FFA84324FFA84324FFA74324FFA64324FFA64424FFA54424FF
        9D492CFFC8C4C6FF9A9696E31515151900000000B1AEAF7CBAB5B5FEB98672FE
        B85F3BFFB95F3BFEB85F3BFEB95F3BFFB95E3CFEB95E3BFEB95E3BFFB25633FE
        A74A28FFA64927FEA64827FEA64826FEA64726FFA64726FEA64625FEA64625FF
        A64625FEA88F87FEA5A5A5FFA4A4A4FEA4A4A4FEA4A4A4FFA6A6A6FEA4786AFE
        A64324FFA84424FEA84424FEA84424FFA84424FEA84424FFA84424FEA74324FE
        A74324FFA74324FEA64324FEA64324FFA54323FEA54324FEA54423FFA54424FE
        A34423FEBFB7B6FFACA7A7F11010102600000000B5B3B38EBDB7B8FEB17F6BFE
        BA613DFFBA613DFEBA613DFEBA603DFFBA603DFEBB603DFEBB603DFFBB5F3DFE
        B45835FFA74A28FEA54927FEA54826FEA54826FFA54726FEA54725FEA54625FF
        A54625FEAD958DFEA8A8A8FFA6A6A6FEA5A5A5FEA4A4A4FFA4A4A4FEA2786AFE
        A54424FFA64424FEA64424FEA64424FFA64324FEA64424FFA64324FEA64324FE
        A64324FFA54323FEA54323FEA54423FFA54423FEA44424FEA44424FFA44423FE
        A34423FEB8B0AFFFB3ADADF91413143100000000B6B4B596BEB9B9FFB17F6AFF
        BC633FFFBC633FFFBC633FFFBC623FFFBC623FFFBC623FFFBC623FFFBC623FFF
        BC613FFFB85C39FFA84C2AFFA54827FFA54826FFA54826FFA54726FFA54725FF
        A54625FFB69E96FFACACACFFA9A9A9FFA7A7A7FFA5A5A5FFA4A4A4FFA27869FF
        A44424FFA54424FFA54424FFA54424FFA54424FFA54424FFA54424FFA54423FF
        A54423FFA44423FFA44424FFA44423FFA44423FFA44423FFA44424FFA34424FF
        A24523FFB7AFAFFFB2ADADFC1515153400000000B7B5B593BEB9BAFEB17F6AFE
        BD6440FFBE6440FEBE6441FEBE6541FFBE6440FEBE6440FEBE6441FFBE6441FE
        BE6341FFBE6340FEBC603EFEAD512FFEA54827FFA54826FEA54726FEA54726FF
        A54725FEBDA59DFEB1B1B1FFAEAEAEFEABABABFEA8A8A8FFA6A6A6FEA2786AFE
        A34424FFA44424FEA44424FEA44424FFA44423FEA44424FFA44423FEA44423FE
        A44423FFA44423FEA44423FEA44424FFA34423FEA34423FEA34523FFA34524FE
        A24423FEB7AFAEFFB1ACACFB1514153100000000B7B4B586BFBABAFEB3816CFE
        BF6642FFBF6642FEC06643FEBF6642FFC06643FEC06642FEC06643FFC06643FE
        C06543FFC06542FEC06542FEC06442FEB65A38FFA74B29FEA44826FEA44826FF
        A44725FEC2AAA2FEB7B6B6FFB3B2B3FEAFAFAFFEACACACFFA9A9A9FEA3796BFE
        A34524FFA44524FEA44524FEA44524FFA44424FEA44424FFA34424FEA44424FE
        A34424FFA34523FEA34523FEA34523FFA34524FEA34524FEA34524FFA34524FE
        A24523FEB8B0AFFFAFAAAAF61D1D1D2700000000AFAEAE6CC0BBBBFFBA8E7DFF
        C16844FFC16844FFC16844FFC16944FFC16844FFC26844FFC16844FFC26845FF
        C26845FFC26845FFC26744FFC16744FFC26644FFC06442FFB35735FFA64A28FF
        A44826FFC6AEA6FFBDBCBDFFB9B8B8FFB5B4B4FFB1B1B1FFAEAEAEFFA37A6CFF
        A24524FFA34524FFA34524FFA34524FFA34524FFA34524FFA34524FFA34524FF
        A34524FFA34524FFA34524FFA34524FFA24524FFA24524FFA24524FFA24624FF
        9D4626FFC4BEBEFFA19D9DEB2322221600000000ACAAAB48C6C1C2FEBB9B8FFE
        C46C48FFC36A46FEC36A46FEC36A46FFC36A46FEC36A47FEC36A47FFC36A46FE
        C36A46FFC36A47FEC36947FEC36946FEC36946FFC36845FEC36745FEC06442FF
        B35736FECCB4ACFEC3C2C2FFBFBEBEFEBBBABAFEB6B6B6FFB3B3B3FEA47B6DFE
        A24524FFA34524FEA34524FEA34524FFA34524FEA34524FFA34524FEA24524FE
        A34524FFA24524FEA24524FEA24524FFA24624FEA24624FEA24624FFA24624FE
        A05336FEC4C0C1FF8D8A8AD02C2B2B0B00000000AAA8A91CC9C6C6FAB9ACA8FE
        C8714EFFC56C48FEC56C48FEC56C48FFC56C49FEC56C49FEC56C48FFC56C48FE
        C56C48FFC56C48FEC56C49FEC56B48FEC56B48FFC56A47FEC46A47FEC46947FF
        C36846FEE3CCC3FEDDDDDDFFCFCECEFEC2C2C2FEBDBCBCFFB9B8B8FEA67D6EFE
        A24625FFA34625FEA34625FEA34624FFA34624FEA34624FFA24624FEA24624FE
        A24624FFA24624FEA24624FEA24624FFA24624FEA24724FEA24724FFA24725FE
        AA735EFEBCB6B7FF6C6A6AA53C3A3A0500000000B2B0B102BFBDBED6C5C1C1FF
        C27A5DFFC86F4BFFC76E4AFFC76E4BFFC76E4BFFC76F4BFFC76F4BFFC76F4BFF
        C76E4BFFC76E4AFFC76E4AFFC76E4AFFC76D4AFFC66D4AFFC66C49FFC66B49FF
        C56B48FFE3C6BBFFECECECFFEAEAEAFFE7E6E6FFDCDCDCFFD2D2D2FFB88572FF
        A54927FFA34725FFA24725FFA24724FFA24724FFA24725FFA24624FFA24724FF
        A24724FFA24624FFA24724FFA24724FFA24725FFA34826FFA64B29FFA84D2BFF
        BD9B8FFFB5AFAFFF4543446B414040010000000000000000B8B7B892C8C3C4FE
        C39582FFCD7652FECA714DFEC9704CFFC9714DFEC9714DFEC9714DFFC9714DFE
        C9714DFFC9714DFEC9704CFEC9704CFEC96F4CFFC86F4CFEC86F4CFEC86E4BFF
        C76D4AFEC87251FECF8568FFCE8468FECD8468FECD8468FFCA7F62FEC26947FE
        BF6442FFBC603FFEB75C3AFEB45836FFB15534FEAF5332FFAD5230FEAD5230FE
        AD5230FFAD5230FEAE5331FEAF5432FFB05533FEAF5533FEAE5431FFAB5331FE
        C5BAB8FEB3AEAEF92E2D2E29000000000000000000000000ADABAC3ECFCCCCFD
        BCAAA4FFD58464FFCD744FFFCC734FFFCB734FFFCB734FFFCB734FFFCB734FFF
        CB734FFFCC734FFFCB724FFFCB724EFFCB724EFFCA714EFFCA714DFFC9704DFF
        C96F4DFFC86F4CFFC86E4BFFC66D4AFFC56B49FFC46A48FFC46947FFC36845FF
        C16744FFC06543FFBF6442FFBE6341FFBC6240FFBB603EFFBA5F3DFFB85E3CFF
        B75C3AFFB65B39FFB45A38FFB35936FFB25735FFB05634FFAF5533FFB0725AFF
        C3BEBFFF8D898AC53D3C3C07000000000000000000000000B3B1B204C1BFC0D4
        CAC6C6FFC98D75FED17753FECF7652FFCE7551FECE7551FECE7551FFCD7551FE
        CD7551FFCD7551FECD7551FECD7451FECD7450FFCC7450FECC734FFECB724FFF
        CA724EFECA714EFEC46E4CFFB7745AFEB37F6AFEA86A52FFB26344FEC36A47FE
        C36946FFC26845FEC06644FEBF6542FFBE6441FEBC6240FFBB613EFEBA603DFE
        B85E3CFFB75D3AFEB55C39FEB45B38FFB35937FEB15835FEB05734FFC4ABA3FE
        B7B2B2FE4E4D4D670000000000000000000000000000000000000000B3B1B268
        D1CDCDFFC3AAA0FEDA8968FED37854FFD27854FED07753FED07753FFD07753FE
        D07753FFCF7753FECF7753FECF7753FECF7653FFCE7652FECE7552FECD7551FF
        CD7450FECA7451FED7B7ABFFEEECECFEEFEEEEFEECEAE9FFC1A296FEB06345FE
        C46B48FFC36A47FEC26846FEC06744FFBF6643FEBE6442FFBC6340FEBB623FFE
        B9603EFFB85F3CFEB75D3BFEB55C39FFB45B38FEB35A38FEB16B50FFC6C2C4FE
        A9A5A5E1474646130000000000000000000000000000000000000000B2B0B009
        C8C6C6DECFCBCBFFCE9E8AFFD77E5BFFD57B57FFD47A56FFD37A55FFD27955FF
        D27955FFD27955FFD27955FFD17955FFD17955FFD07854FFD07854FFD07753FF
        CE7653FFDCA994FFF0F0F0FFF0F0F0FFF0EFF0FFEFEFEFFFF0EFEFFFB88A77FF
        C46C4AFFC56C49FFC36A48FFC26946FFC06845FFBF6644FFBE6542FFBC6341FF
        BB623FFFB9603EFFB85F3CFFB65E3BFFB55D39FFB45D3AFFC8B3ADFFBAB5B5FE
        6261617462606101000000000000000000000000000000000000000000000000
        AFAEAE58D7D4D4FDCCBFBBFEDD9D82FFD87D59FED77D59FED67C58FFD57C58FE
        D47C58FFD47C57FED47B57FED37B57FED37B57FFD27A56FED27A56FED17955FF
        D17955FEEAD2C8FEF0F0F0FFF1F0F0FEF1F0F1FEF0F0F0FFF0F0F0FED2B8ADFE
        C36D4AFFC66E4AFEC56C49FEC36B47FFC26946FEC06844FFBF6643FEBD6541FE
        BC6440FFBA623EFEB9613DFEB85F3CFFB7603CFEBB8875FEC1BDBEFFA7A4A5D4
        5250510F00000000000000000000000000000000000000000000000000000000
        B7B6B603C3C1C2B1E3E1E1FED5BDB3FFE29778FEDB805CFEDA7F5BFFD97F5BFE
        D87E5AFFD77E5AFED67E5AFED67D59FED67D59FFD57D59FED47C58FED37B58FF
        D27B57FEECD2C8FEF1F0F1FFF2F1F1FEF2F2F2FEF1F1F1FFF1F1F1FED9BAAEFE
        C7704DFFC8704CFEC66E4AFEC56D49FFC36B47FEC16A46FFC06844FEBE6743FE
        BD6541FFBC6440FEBA623FFEB9613EFFB76F53FECCC4C5FEBEBABAFA54525345
        0000000000000000000000000000000000000000000000000000000000000000
        00000000BBBABB1BDDDCDDE3F4F3F3FFDBB9AAFFE3916FFFDD825EFFDC825EFF
        DB815DFFDA805CFFD9805CFFD8805CFFD87F5BFFD77F5BFFD77E5AFFD67E5AFF
        D57D59FFE2AB94FFF4F3F3FFF3F2F2FFF3F3F3FFF2F2F2FFF3F2F2FFD39379FF
        CB734FFFC9724EFFC8704CFFC76E4BFFC56D49FFC36B48FFC26A46FFC06845FF
        BF6743FFBD6542FFBC6440FFB86745FFCFC1BFFFBCB6B7FE7A7979896D6B6C01
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000C0C0C03CF3F2F3F5F9F8F7FEDFB9A8FEE59372FFE08560FE
        DF8460FFDE8460FEDD835FFEDB825EFEDA815DFFDA815DFED9805CFED8805CFF
        D77F5BFED67E5AFEE8BBA8FFF5F0EFFEF5F5F5FEF4EDEAFFDFAA94FECE7652FE
        CC7551FFCB734FFEC9724EFEC8704DFFC66F4BFEC46D49FFC36B48FEC16A46FE
        C06845FFBE6743FEBB6A48FECDB9B3FFBBB5B6FE959293B36C6A6B0800000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000C4C4C458F7F7F8F8F8F7F6FEDCBBABFFE89E7FFE
        E28763FFE18662FEE08662FEDF8561FEDE8460FFDC835FFEDB825EFEDA825EFF
        DA815DFED9805CFED77F5BFFD88563FED98C6CFED5815EFFD27A56FED07854FE
        CE7753FFCD7551FECB734FFECA724EFFC8704CFEC76F4BFFC46D49FEC36B48FE
        C26A46FFBE7152FED0BCB7FEBBB6B7FFA5A3A3C7777576130000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000C4C4C458F2F2F2F4F3F2F2FFD8C1B6FF
        E7AA8FFFE68D69FFE48965FFE38864FFE28764FFE08662FFDF8561FFDD8460FF
        DC835FFFDA825EFFD9815DFFD87F5CFFD77E5AFFD57D59FFD47B57FFD27A56FF
        D07854FFCF7653FFCD7551FFCB734FFFCA724EFFC8704CFFC66F4BFFC56D49FF
        C28167FFD0C6C5FFBCB6B7FFA3A0A1C17F7C7D14000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000C2C2C23CE0DFE0E3EAE8E8FE
        D3C8C3FFD9B09DFEEDA485FEE78D69FEE58A66FFE48965FEE38864FEE18763FF
        DF8661FEDD8460FEDB825EFFDA815DFED8805CFED77E5AFFD57D59FED47B57FE
        D27956FFD07854FECF7652FECD7551FFCB734FFECA714DFFC47352FECCA79AFE
        CAC6C7FFBCB7B7FD9997979E8C89890A00000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000C0BFC01BC8C7C7B0
        DFDCDCFDD2CECEFECAB5ACFEDBAB96FEEFA88AFFEA9270FEE68B67FEE58A66FF
        E38865FEE18763FEDF8561FFDD835FFEDB825EFED9805CFFD77E5AFED57C59FE
        D47B57FFD27955FED07854FECF7652FFC97654FECC9C89FFD0C6C4FEBFBABBFE
        BBB6B7EB93919161928F90020000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000000BEBCBD03
        B1B0B057CAC8C9DDD6D2D3FECECACAFFC4B3AEFFD5AB9AFFE09A7EFFEC9877FF
        E9916EFFE58B66FFE28864FFE08662FFDE8460FFDC825EFFDA815CFFD87F5BFF
        D67D59FFCF7955FFCC8A70FFD3B0A4FFCFC8C8FFC1BCBDFFC1BCBCFAA8A5A6A2
        9D9B9B1E00000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000B5B3B409B3B2B267C3C1C2D2D5D2D2FDCECACAFECAC6C7FEC1B4B1FF
        CBAB9FFECDA291FECA9783FFCA9783FECA9783FEC99682FFD2A08CFED0ADA0FE
        CFB7AFFFCCC7C8FEC8C4C5FEC0BBBCFEBEBABAF2ACA9AAA29F9C9D2E00000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000B7B5B504ADABAC3DB9B7B990C2C0C1D4CFCCCCF9
        CFCCCCFECBC7C7FEC7C3C3FFC4C0C0FEC3BEBEFEC3BFBFFFC4C0C0FEC5C1C1FE
        C8C3C4FDBCB8B9ECB1AEAEB6A6A3A367A29FA019000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000B7B5B602ADABAC1B
        ABA9AA45ADABAC6AB7B5B583BAB7B892B9B7B795B7B5B58CAEABAC77A8A5A659
        A3A0A130A9A6A70B000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000FFFFFC7FFFFF0000FFFF8001
        FFFF0000FFFC00007FFF0000FFF000001FFF0000FFC0000007FF0000FF800000
        03FF0000FF00000000FF0000FE000000007F0000FC000000003F0000F8000000
        003F0000F0000000001F0000E0000000000F0000E0000000000F0000C0000000
        00070000C000000000070000C000000000030000800000000003000080000000
        0003000080000000000100000000000000010000000000000001000000000000
        0001000000000000000100000000000000010000000000000001000000000000
        0001000000000000000100000000000000010000000000000001000000000000
        0001000000000000000100008000000000030000800000000003000080000000
        00070000C000000000070000C000000000070000E0000000000F0000E0000000
        001F0000F0000000001F0000F8000000003F0000FC000000007F0000FE000000
        00FF0000FF00000001FF0000FF80000003FF0000FFC000000FFF0000FFF00000
        3FFF0000FFFC0000FFFF0000FFFF8007FFFF0000280000002000000040000000
        0100200000000000801000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000D0D0D04080808070707070906060609
        0707070608080802000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        1919190116161610333334485C5B5C847C7A7BAD8F8E8EC4939191CB929090C5
        7E7D7DB15B5A5B8B2A2A2A530A0A0A1C0A0A0A06000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000252525012E2E2E25
        6F6E6F92B3B0B2E7D1CECFFEC3B6B2FFC0A097FFBC9388FFBC9387FFBF958AFF
        C5A59CFFD2C5C1FFF1F1F1FED6D6D6E97575759E171717380E0E0E0800000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000302F300D6C6B6C87C4C0C2F4
        C6B8B5FFBE8878FFB05439FFB54629FFB8472AFFBA472BFFBB472BFFBC472CFF
        BD482CFFBD472CFFB7533BFFC88D7EFFE5D9D6FFE4E3E4F5676666970C0C0C20
        0C0B0B0100000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000003D3D3D269F9C9DCEC9C0BFFFBB826FFF
        AD4829FFB04628FFB34729FFB54629FFB7472AFFB9472AFFBA472BFFBC472BFF
        BD472CFFBD482CFFBE482CFFBE482CFFBB482CFFC38272FFDED7D6FFA5A3A3D5
        191919400F0F0F03000000000000000000000000000000000000000000000000
        0000000000000000000000004C4B4B33B0ACACE7C6B0AAFFAD563AFFAC4727FF
        AD4627FFAF4628FFB24628FFB44629FFB64629FFB54529FFB6452AFFB7452AFF
        BA462BFFBD472CFFBD472CFFBD472CFFBD472CFFBD472CFFBA543BFFD0B8B3FF
        BBB8B9EB2323234F101010030000000000000000000000000000000000000000
        0000000000000000504F4F22AFABACE6C4A69DFFAA4C2BFFAA4727FFAB4727FF
        AC4627FFAE4627FFB04628FFB34628FFBF705CFFC3A49CFFC3A59DFFC4A59DFF
        AF7566FFBB462BFFBC472BFFBC472BFFBC472BFFBC472BFFBC472BFFB9492DFF
        CCAAA2FFB7B4B4EB1A19193F0D0D0D0100000000000000000000000000000000
        00000000545253089C9999CAC5AFA9FFA94D2DFFA94827FFAA4827FFAA4727FF
        AB4627FFAD4627FFAF4627FFB14628FFCC9587FFE9E8E8FFEAE9E9FFEBEAEAFF
        CAA8A0FFB9462AFFBB472BFFBB472BFFBB472BFFBB472BFFBA462AFFBA462AFF
        B6482CFFCCB5B0FF999696D50F0E0E1D00000000000000000000000000000000
        00000000807E7F7CC2BAB8FFAB593BFFA94928FFA94827FFA94827FFA94727FF
        AA4626FFAC4627FFAE4627FFB04627FFC38F81FFDDDCDCFFE6E5E5FFEAE9E9FF
        CBA9A0FFB74629FFB9462AFFB9462AFFB9462AFFB9462AFFB8462AFFB84629FF
        B74629FFB45339FFCDC4C3FF5856569412121206000000000000000000000000
        716F7018B8B4B5F2B6816DFFA84B29FFA84A28FFA84928FFA84827FFA94727FF
        A94626FFAA4626FFAC4626FFAE4527FFB98778FFCBCACAFFD7D6D6FFE2E1E1FF
        C9A89FFFB54529FFB74629FFB74629FFB74629FFB74529FFB64529FFB64529FF
        B54528FFB34528FFBB7E6DFFB9B5B5F419181832000000000000000000000000
        93909182C0B2AFFFAC5230FFA84B29FFA84A28FFA84928FFA84827FFA84726FF
        A94626FFA94626FFAA4526FFAC4526FFB28172FFBCBCBCFFC5C4C4FFD1D0D0FF
        C1A198FFB34528FFB44528FFB44528FFB54528FFB44528FFB44528FFB34528FF
        B24527FFB14427FFAD4527FFC6B8B5FF5C5A5A9911111104000000007B797A03
        AEAAAAE1BC8C7AFFB15633FFAB4E2CFFA74A28FFA74928FFA74827FFA74826FF
        A84726FFA84626FFA94626FFAA4526FFAD7B6BFFB2B2B2FFB9B8B8FFC0C0C0FF
        B7988FFFB04427FFB24527FFB24527FFB24527FFB24527FFB14427FFB04427FF
        AF4426FFAE4426FFAD4426FFBA8575FFA39F9FE7121212160000000097959633
        C5C1C2FEB06242FFB35835FFB25633FFA84B29FFA74A28FFA74827FFA74827FF
        A74726FFA74626FFA84625FFA84525FFA87667FFAAAAAAFFAFAFAFFFB5B5B5FF
        B09188FFAD4426FFAF4426FFAF4426FFAF4426FFAF4426FFAE4426FFAE4426FF
        AD4425FFAC4425FFAA4425FFA85236FFC6C2C3FE2C2B2B4B00000000A9A6A76E
        BCAEAAFFB55B37FFB55B37FFB55A38FFB15533FFA74A28FFA74927FFA64827FF
        A64726FFA74726FFA74625FFA74525FFA67464FFA5A5A5FFA8A8A8FFACACACFF
        AB8C83FFAA4425FFAC4425FFAC4425FFAC4425FFAC4425FFAB4425FFAB4425FF
        AA4425FFA94424FFA84324FFA74525FFBDAFACFF4D4B4B8400000000B1ADAE98
        BA9D93FFB75E3AFFB75D3AFFB75D3AFFB85D3AFFB15532FFA64928FFA64827FF
        A64826FFA64726FFA64625FFA64625FFA67565FFA4A4A4FFA4A4A4FFA6A6A6FF
        A68980FFA84324FFA94425FFA94425FFA94424FFA94424FFA94324FFA84324FF
        A74324FFA64324FFA64324FFA54424FFB79990FF6B6969AA17161602B6B2B2B1
        B79487FFBA603CFFBA603CFFBA603CFFBA603DFFBA5F3DFFB25634FFA64928FF
        A64826FFA54826FFA54726FFA54625FFAA7A69FFA7A7A7FFA5A4A5FFA4A4A4FF
        A3877DFFA64324FFA74424FFA74424FFA64424FFA64424FFA64324FFA64323FF
        A54323FFA54423FFA44423FFA44424FFB18C81FF7D797AC017171704B7B4B4B8
        B79487FFBC633FFFBC633FFFBD633FFFBD633FFFBD623FFFBD623FFFB75B38FF
        A74B29FFA54826FFA54726FFA54725FFB18272FFADADADFFA8A8A8FFA5A5A5FF
        A3877DFFA44424FFA54424FFA54424FFA54424FFA54423FFA44423FFA44423FF
        A44423FFA44423FFA44424FFA34524FFB08C80FF7E7A7AC61A191904B8B5B6B1
        B89487FFBF6642FFBF6642FFBF6642FFBF6642FFBF6642FFBF6542FFBF6442FF
        BD613FFFAE5230FFA54826FFA44726FFB78978FFB4B4B4FFAFAFAFFFAAAAAAFF
        A4887FFFA34524FFA44524FFA44424FFA44424FFA44424FFA44423FFA34423FF
        A34524FFA34524FFA34524FFA34524FFB08C80FF7C7878BD1E1D1D03B6B3B497
        BB9E94FFC26945FFC16945FFC26945FFC26945FFC26945FFC26845FFC26845FF
        C26745FFC26744FFBA5F3CFFAB4F2DFFBB8C7CFFBDBDBDFFB7B7B7FFB1B1B1FF
        A78C82FFA34524FFA34524FFA34524FFA34524FFA34524FFA34524FFA34524FF
        A34524FFA24524FFA24524FFA24624FFB5988EFF6B6868A426252501B7B4B56C
        BDAEAAFFC66E4BFFC46C48FFC46C48FFC56C48FFC56C48FFC56C48FFC56B48FF
        C56B48FFC46A47FFC46947FFC36846FFD3A594FFD2D2D2FFC3C2C2FFBABABAFF
        AB9086FFA24624FFA34624FFA34624FFA34524FFA24624FFA24624FFA24624FF
        A24624FFA24624FFA24624FFA24624FFB9ABA8FF5A58587A00000000AEACAD30
        CAC6C7FEC57B5EFFC86F4BFFC86F4BFFC76F4BFFC86F4BFFC86F4BFFC86F4BFF
        C86E4BFFC76E4BFFC76D4AFFC66C49FFD4967FFFE5D1CAFFE2CFC8FFD8C6BFFF
        C2917FFFAC512FFFA84C2AFFA54927FFA34725FFA24724FFA24725FFA34826FF
        A54A27FFA64B2AFFA84D2BFFA95A3CFFC1BCBDFE4443433D00000000A6A5A602
        C3C0C0DFC79D8CFFCD7450FFCB724EFFCB724EFFCB724FFFCB724EFFCB724EFF
        CA724EFFCA714DFFC9704DFFC96F4CFFC86E4BFFC76D4AFFC56B48FFC46947FF
        C26845FFC16644FFBF6442FFBD6240FFBB603EFFB95E3CFFB75C3AFFB55A38FF
        B35936FFB15734FFAF5533FFBB8B7AFFA19C9CE3383737080000000000000000
        BBB9BA7EC5B8B4FFD48160FFD07652FFCE7652FFCE7652FFCE7652FFCE7551FF
        CE7551FFCD7551FFCC7450FFCC734FFFCA724EFFC68268FFC8A395FFB98875FF
        B96646FFC36947FFC16745FFBF6542FFBD6340FFBB613FFFB95F3DFFB75D3AFF
        B55B39FFB35937FFB05836FFC0B2AFFF706D6E87000000000000000000000000
        AFADAE14CDCACBF0CEA492FFD57C58FFD37955FFD27955FFD17955FFD17955FF
        D17955FFD07854FFD07753FFCF7653FFD38C70FFEEECEBFFF0EFEFFFEFEFEFFF
        C39B8BFFC46C49FFC36A48FFC16845FFBF6643FFBD6441FFBB623FFFB8603DFF
        B65E3BFFB55C39FFBD8A76FFB7B2B3F25251511B000000000000000000000000
        00000000BBB9BA74D4CDCBFFDB9B80FFD97E5AFFD77D59FFD57C58FFD57C58FF
        D47C58FFD47B57FFD27B57FFD27A56FFE2B5A3FFF1F0F0FFF1F1F1FFF0F0F0FF
        DDCCC5FFC56E4BFFC56D49FFC36B47FFC16945FFBF6743FFBD6441FFBA623EFF
        B8603CFFB76A4BFFC3BBBAFF7877777D00000000000000000000000000000000
        00000000B4B3B304D5D3D4C3E7DAD4FFDF9272FFDC815DFFDA815CFFD97F5BFF
        D87F5BFFD77F5BFFD67E5AFFD57D59FFE0A38AFFF3F2F2FFF3F2F2FFF2F2F2FF
        DFBBACFFCA724EFFC8704CFFC66E4AFFC36B48FFC16945FFBF6743FFBC6541FF
        B96542FFC6B2ACFFA09D9DC8615F600700000000000000000000000000000000
        0000000000000000C5C4C51AECECECE1ECDAD2FFE29575FFE08561FFDE8460FF
        DD835FFFDB825EFFDA815DFFD8805CFFD77F5BFFE5B29DFFEDD6CDFFE7C1B1FF
        D07C5AFFCC7551FFCA724FFFC8704CFFC56E4AFFC36B48FFC16945FFBD6845FF
        C7ABA2FFB1ADAEE4716F701F0000000000000000000000000000000000000000
        000000000000000000000000CCCCCC28ECECECE1E7DAD4FFE3A58AFFE48A66FF
        E28863FFE08662FFDE8561FFDC835FFFDA815EFFD8805CFFD67E5AFFD47C58FF
        D27A56FFCF7754FFCD7551FFCB734FFFC8704CFFC66E4AFFC37556FFC9B5AFFF
        B2AEAEE38482832C000000000000000000000000000000000000000000000000
        00000000000000000000000000000000C7C7C71AD8D7D8C3DCD6D4FFD9B3A3FF
        E99D7EFFE68C68FFE48965FFE18763FFDF8561FFDC835FFFD9815DFFD77E5AFF
        D57C58FFD27A56FFD07753FFCD7551FFC9724FFFCB9784FFC7BEBDFFAAA7A7C5
        8D8A8B1D00000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000BBBABA04BFBDBD74D1CECEF0
        CBBFBCFFD7B3A4FFE1997CFFE99370FFE48A66FFE18763FFDE8460FFDB815DFF
        D87E5BFFD47B57FFCE8061FFD0A18FFFC8BBB7FFBDB9B9F0A8A5A57696939305
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000000B2B0B014
        BEBCBC7DC6C3C3DED1CECEFEC5B7B3FFCAADA3FFC9A699FFC9A699FFC9A699FF
        CBAEA4FFC7B9B6FFCCC8C9FEB5B1B1DEAEABAC7EA3A0A1150000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000ACABAC02B0AEAF2FBAB8B96BBAB7B895BCB9BAAFBBB8B8B8BAB6B7AF
        B5B2B296B3B0B16BA5A2A330A19F9F0200000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000000FFF81FFF
        FF8003FFFE0000FFFC00003FF800001FF000000FE0000007C0000007C0000003
        8000000380000001000000010000000100000001000000000000000000000000
        00000000000000000000000100000001000000018000000380000003C0000007
        C0000007E000000FF000001FF800003FFC00007FFF0001FFFFC007FF28000000
        1000000020000000010020000000000040040000000000000000000000000000
        000000000000000000000000000000000000000000000000141414163C3C3C4D
        4C4C4C6847474761252424380505050800000000000000000000000000000000
        00000000000000000000000000000000323232258A8585AABD978CF9BC7967FF
        BB6D59FFBF725FFFCE9486FEBEACA8E15C5C5C73070707080000000000000000
        00000000000000000000000059575750B79990F2B0563AFFB14628FFB54629FF
        B7462AFFBA462BFFBD472CFFBD472CFFC67D6CFF928B8AC01111111500000000
        00000000000000005857573DB89388F8AA4928FFAB4727FFAF4627FFBC644DFF
        D6C6C3FFCAABA3FFBB462BFFBC472BFFBB462BFFBD604AFF8D8685BF08080807
        0000000036363606AC9B96DBA94E2DFFA94827FFA94726FFAC4627FFB66852FF
        D9D8D8FFD8C7C2FFB74629FFB8462AFFB84629FFB64529FFBC7664FF4F4D4D70
        0000000082808059B67963FFA84B29FFA84927FFA84726FFA94626FFAD614AFF
        BBBBBBFFC2B2AEFFB24528FFB34528FFB34528FFB14427FFAE4427FFA08D89DF
        09090906B0ABAAA8B35C39FFB05432FFA74928FFA74726FFA74626FFA75D45FF
        AAAAAAFFAFA09BFFAD4426FFAD4426FFAD4426FFAC4425FFAA4425FFB48272FE
        26252534B6A49FD2B95F3BFFB95E3BFFB15533FFA64827FFA64726FFA75F46FF
        A5A5A5FFA59692FFA74424FFA84424FFA74324FFA64324FFA54424FFAC6C56FF
        4644445CB8A49EDABE6440FFBE6441FFBE6441FFB75B39FFA74A28FFAC664DFF
        AEAEAEFFA69893FFA44424FFA44424FFA44423FFA44423FFA34424FFA96852FF
        4C4A4A63B9ADAAC0C36B47FFC36A46FFC36A46FFC36946FFC16644FFBF7A61FF
        C2C2C2FFAFA29DFFA34624FFA34524FFA34524FFA24524FFA24624FFAD7460FF
        42414148B8B6B784C87F61FFC9714DFFC9714DFFC9704CFFC86F4CFFCB7858FF
        D59E89FFC88A72FFB55A38FFB05533FFAD5230FFAC512FFFAC512FFFB18F84F8
        2423231189888924CDAA9CFBD27854FFD07753FFCF7753FFCE7652FFCE7A58FF
        DBC0B6FFC99E8DFFC36A47FFBF6643FFBB613FFFB75D3BFFB56647FF8E8988A5
        0000000000000000C6C3C38FDEA188FFD97F5BFFD67E5AFFD57D59FFDA9477FF
        F2F1F1FFE8DAD5FFC76F4CFFC36B47FFBF6743FFBA633FFFB89D93F1504F5021
        000000000000000076767606E4DFDEBAE3A68DFFE08662FFDD8460FFDA815DFF
        E0A288FFD78D6EFFCD7551FFC8704CFFC36E4BFFC09D91F8817F7F4B00000000
        00000000000000000000000079797906CBC9C98ED7B7ABFBE0987BFFE38A67FF
        DD835FFFD77E5AFFD17B58FFCC9179FFBDADA8D98A8888390000000000000000
        0000000000000000000000000000000000000000918F9024BDBABB83C1B5B2BF
        C2AFA9D9C1AFAAD0BBB5B5A6A3A0A05762616105000000000000000000000000
        00000000F81F0000E0070000C003000080010000000100000000000000000000
        00000000000000000000000000000000000100008001000080030000C0070000
        F00F0000}
      Stretch = True
    end
    object lbMensagem: TLabel
      Left = 46
      Top = 16
      Width = 72
      Height = 14
      Caption = 'lbMensagem'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnNovo: TBitBtn
      Left = 412
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 547
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnPesquisa: TBitBtn
      Left = 622
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnPesquisaClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADAADADADAD00CDADADDADADAD00CDADADAAD000070CDADADADD0DADA0BDADA
        DADA0DAFFFF0000000000AF7F770FFFFFFF00DFFFFF0FFFFFFF00AF77F70F7FF
        FFF0A0FFFF0FFFFFFFF0DA00007F77FFFFF0AD0FFFFFFFFFFFF0DA0F7777F77F
        FFF0AD0FFFFFFFFFFFF0DA00000000000000ADADADADADADADAD}
    end
    object btnSair: TBitBtn
      Left = 697
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnSalvar: TBitBtn
      Left = 481
      Top = 10
      Width = 55
      Height = 30
      TabOrder = 4
      OnClick = btnSalvarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C30E0000C30E00000000000000000000BFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00000000000000000000000000000000
        0000000000000000000000000000000000000000000000BFBFBFBFBFBF000000
        007F7F007F7F000000000000000000000000000000000000BFBFBFBFBFBF0000
        00007F7F000000BFBFBFBFBFBF000000007F7F007F7F00000000000000000000
        0000000000000000BFBFBFBFBFBF000000007F7F000000BFBFBFBFBFBF000000
        007F7F007F7F000000000000000000000000000000000000BFBFBFBFBFBF0000
        00007F7F000000BFBFBFBFBFBF000000007F7F007F7F00000000000000000000
        0000000000000000000000000000000000007F7F000000BFBFBFBFBFBF000000
        007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
        7F007F7F000000BFBFBFBFBFBF000000007F7F007F7F00000000000000000000
        0000000000000000000000000000007F7F007F7F000000BFBFBFBFBFBF000000
        007F7F000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000
        00007F7F000000BFBFBFBFBFBF000000007F7F000000BFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBF000000007F7F000000BFBFBFBFBFBF000000
        007F7F000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000
        00007F7F000000BFBFBFBFBFBF000000007F7F000000BFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBF000000007F7F000000BFBFBFBFBFBF000000
        007F7F000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000
        00000000000000BFBFBFBFBFBF000000007F7F000000BFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBF000000BFBFBF000000BFBFBFBFBFBF000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF}
    end
  end
  object GroupBox15: TGroupBox
    Left = 570
    Top = 0
    Width = 198
    Height = 48
    Caption = 'Digitador'
    TabOrder = 3
    object lbUsuario: TLabel
      Left = 8
      Top = 19
      Width = 48
      Height = 16
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object dsSel_AvaliacoesApr: TDataSource
    DataSet = dmDeca.cdsSel_AvaliacoesApr
    Left = 733
    Top = 9
  end
  object dsResultado: TDataSource
    DataSet = dmDeca.cdsSel_AvaliacoesApr
    Left = 687
    Top = 213
  end
end
