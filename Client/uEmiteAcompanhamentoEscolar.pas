unit uEmiteAcompanhamentoEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, ExtCtrls, Buttons, Db, Menus, ComCtrls;

type
  TfrmEmiteAcompanhamentoEscolar = class(TForm)
    Panel2: TPanel;
    dgResultados: TDBGrid;
    Label1: TLabel;
    dsSel_Acompanhamento: TDataSource;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    GroupBox3: TGroupBox;
    cbEscola: TComboBox;
    GroupBox4: TGroupBox;
    cbBimestre: TComboBox;
    Panel4: TPanel;
    btnVer: TSpeedButton;
    btnCancelar: TSpeedButton;
    edAno: TEdit;
    btnSair: TSpeedButton;
    btnPesquisar: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure cbEscolaClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure dgResultadosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteAcompanhamentoEscolar: TfrmEmiteAcompanhamentoEscolar;
  vUnidade1, vUnidade2, vEscola1, vEscola2 : TStringList;
  vIndUnidade, vIndEscola : Integer;

implementation

uses uDM, uInicializarBimestre, uPrincipal, uFichaPesquisa,
  rAcompanhamentoFrente, rAcompanhamentoVerso, uUtil;

{$R *.DFM}

procedure TfrmEmiteAcompanhamentoEscolar.FormShow(Sender: TObject);
//var vIndex : Integer;
begin

  vUnidade1 := TStringList.Create();
  vUnidade2 := TStringList.Create();
  vEscola1  := TStringList.Create();
  vEscola2  := TStringList.Create();


  //Carrega a lista de Unidades
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByname ('@pe_cod_unidade').Value := Null;
      Params.ParamByname ('@pe_nom_unidade').Value := Null;
      Open;

      cbUnidade.Items.Clear;
      while not eof do
      begin
        cbUnidade.Items.Add (FieldByName ('nom_unidade').AsString);
        vUnidade1.Add (IntToStr(FieldByName ('cod_unidade').AsInteger));
        vUnidade2.Add (FieldByName ('nom_unidade').AsString);
        Next;
      end;
    end;

  except
    Application.MessageBox('Aten��o!!! Ocorreu um ERRO e a lista de Unidade n�o foi carregada...'+#13+#10+#13+#10+
                           'Favor entrar em contato com o Administrador do Sistema.',
                           'Sistema DECA - Unidades',
                            MB_OK + MB_ICONERROR);
    frmEmiteAcompanhamentoEscolar.Close;
  end;

  //Carrega a lista de Escolas
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByname ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      cbEscola.Items.Clear;
      while not eof do
      begin
        cbEscola.Items.Add (FieldByName ('nom_escola').AsString);
        vEscola1.Add (IntToStr(FieldByName ('cod_escola').AsInteger));
        vEscola2.Add (FieldByName ('nom_escola').AsString);
        Next;
      end;
    end;
  except
    Application.MessageBox('Aten��o!!! Ocorreu um ERRO e a lista de Escolas n�o foi carregada...'+#13+#10+#13+#10+
                           'Favor entrar em contato com o Administrador do Sistema.',
                           'Sistema DECA - Escolas',
                           MB_OK + MB_ICONERROR);
    frmEmiteAcompanhamentoEscolar.Close;
  end;

end;

procedure TfrmEmiteAcompanhamentoEscolar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  vUnidade1.Free;
  vUnidade2.Free;
  vEscola1.Free;
  vEscola2.Free;
end;

procedure TfrmEmiteAcompanhamentoEscolar.btnPesquisarClick(
  Sender: TObject);
begin
  //Carrega os dados de Acompanhamento para a unidade/escola/bimestre/ano selecionada
  //e atualiza os grid
  try
    with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);;
      Params.ParamByName('@pe_num_bimestre').Value := cbBimestre.ItemIndex + 1;  //Posicao "0" + 1
      Params.ParamByName('@pe_num_ano').Value := StrToINt(edAno.Text);
      Params.ParamByName('@pe_flg_aproveitamento').Value := Null;
      Params.ParamByName('@pe_cod_escola').Value := StrToInt(vEscola1.Strings[cbEscola.ItemIndex]);
      Open;

      dgResultados.Refresh;

    end;
  except end;
end;

procedure TfrmEmiteAcompanhamentoEscolar.btnCancelarClick(Sender: TObject);
begin
  cbUnidade.ItemIndex := -1;
  cbEscola.Clear;
  cbBimestre.ItemIndex := -1;
  edAno.Text := '2000';
  cbUnidade.SetFocus;
  dmDeca.cdsSel_Acompanhamento.Close;
end;

procedure TfrmEmiteAcompanhamentoEscolar.cbUnidadeClick(Sender: TObject);
begin
  //Carrega a lista de Escolas da Unidade selecionada
  vEscola1.Free;
  vEscola2.Free;
  vEscola1 := TStringList.Create();
  vEscola2 := TStringList.Create();

  try
    with dmDeca.cdsSel_EscolaAcompanhamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;

      cbEscola.Enabled := True;
      cbEscola.Items.Clear;

      while not eof do
      begin
        cbEscola.Items.Add (FieldByName ('nom_escola').AsString);
        vEscola1.Add(IntToStr(FieldByname ('cod_escola').AsInteger));
        vEscola2.Add (FieldByName ('nom_escola').AsString);
        Next;
      end

    end
  except end;
end;

procedure TfrmEmiteAcompanhamentoEscolar.cbEscolaClick(Sender: TObject);
begin
  //Carrega os dados de Acompanhamento para a unidade/escola selecionada
  //e carrega os bimestres
  try
    with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);;
      Params.ParamByName('@pe_num_bimestre').Value := Null;
      Params.ParamByName('@pe_num_ano').Value := Null;
      Params.ParamByName('@pe_flg_aproveitamento').Value := Null;
      Params.ParamByName('@pe_cod_escola').Value := StrToInt(vEscola1.Strings[cbEscola.ItemIndex]);
      Open;

      dgResultados.Refresh;

    end;
  except end;
end;

procedure TfrmEmiteAcompanhamentoEscolar.btnVerClick(Sender: TObject);
begin

  //Validar os crit�rios e campos obrigatorios
  if (cbUnidade.ItemIndex <> -1) and (cbBimestre.ItemIndex <> -1) and (edAno.Text <> '') then
  begin

    //Seleciona os dados do acompanhamento escolar
    try
      with dmDeca.cdsSel_Acompanhamento do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
        Params.ParamByName ('@pe_cod_escola').AsInteger := StrToInt(vEscola1.Strings[cbEscola.ItemIndex]);
        //Verifica o Bimestre - Campo Obrigat�rio
        Params.ParamByName ('@pe_num_bimestre').AsInteger := cbBimestre.ItemIndex + 1;
        //Verifica o Ano - Campo Obrigatorio
        Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
        Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
        Open;

        dgResultados.Refresh;
        relAcompanhamentoFrente.Free;
        Application.CreateForm(TrelAcompanhamentoFrente, relAcompanhamentoFrente);
        relAcompanhamentoFrente.Prepare;
        relAcompanhamentoFrente.lbImpressao.Caption := 'de ' + IntToStr(relAcompanhamentoFrente.QRPrinter.PageCount);
        //relAcompanhamentoFrente.lbImpressao.Caption := IntToStr(relAcompanhamentoFrente.QRPrinter.PageCount);
        relAcompanhamentoFrente.lbTitulo.Caption := cbBimestre.Text + '/' + edAno.Text;

        //Repassar os dados da Unidade selecionada
        with dmDeca.cdsSel_Unidade do
        begin
          Close;
          Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
          Params.ParamByName('@pe_nom_unidade').Value := Null;
          Open;

          relAcompanhamentoFrente.lbUnidade.Caption := GetValue(FieldByName('nom_unidade').AsString);
          relAcompanhamentoFrente.lbGestor.Caption := GetValue(FieldByName('nom_gestor').AsString);
          relAcompanhamentoFrente.lbASocial.Caption := GetValue(FieldByName('nom_asocial').AsString);
          relAcompanhamentoFrente.lbFoneUnidade.Caption := GetValue(FieldByName('num_telefone1').AsString);
          relAcompanhamentoFrente.lbEmailUnidade.Caption := GetValue(FieldByName('dsc_email').AsString);

        end;
        //Repassa os dados da Escola selecionada
        with dmDeca.cdsSel_Escola do
        begin
          Close;
          Params.ParamByName('@pe_cod_escola').Value := StrToInt(vEscola1.Strings[cbEscola.ItemIndex]);
          Params.ParamByName ('@pe_nom_escola').Value := Null;
          Open;

          relAcompanhamentoFrente.lbEscola.Caption := FieldByName('nom_escola').Value;
          relAcompanhamentoFrente.lbDiretor.Caption := GetValue(FieldByName('nom_diretor').AsString);
          relAcompanhamentoFrente.lbEnderecoEscola.Caption := GetValue(FieldByName('nom_endereco').AsString);
          relAcompanhamentoFrente.lbBairroEscola.Caption := GetValue(FieldByName('nom_bairro').AsString);
          relAcompanhamentoFrente.lbFoneEscola.Caption := GetValue(FieldByName('num_telefone').AsString);
        end;
      end;

      //Application.CreateForm(TrelAcompanhamentoFrente, relAcompanhamentoFrente);
      relAcompanhamentoFrente.Preview;

      if Application.MessageBox('Pronto para emiss�o do Verso? Ajuste a(s) folha(s) na impressora' +#13+#10+#13+#10+
                                    'e clique em YES para continuar...',
                                    'Emiss�o [Acompanhamento Escolar - Verso]',
                                    MB_ICONQUESTION + MB_YESNO ) = idYes then
      begin
        //frmEmiteAcompanhamentoEscolar.Free;
        Application.CreateForm (TfrmrelAcompanhamentoVerso, frmrelAcompanhamentoVerso);
        frmrelAcompanhamentoVerso.Preview;
        frmrelAcompanhamentoVerso.Free;
      end;

    except end;
  end;
end;

procedure TfrmEmiteAcompanhamentoEscolar.btnSairClick(Sender: TObject);
begin
  frmEmiteAcompanhamentoEscolar.Close;
end;

procedure TfrmEmiteAcompanhamentoEscolar.dgResultadosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

  if dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').Value = 0 then
  begin
    //Abaixo da m�dia
    dgResultados.Canvas.Font.Color := clWhite;
    dgResultados.Canvas.Brush.Color := clRed;
  end
  else if dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').Value = 1 then
  begin
    //Dentro da m�dia
    dgResultados.Canvas.Font.Color := clWhite;
    dgResultados.Canvas.Brush.Color := clGreen;
  end
  else if dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').Value = 2 then
  begin
    //Acima da M�dia
    dgResultados.Canvas.Font.Color := clWhite;
    dgResultados.Canvas.Brush.Color := clBlue;
  end
  else
  begin
    dgResultados.Canvas.Font.Color := clWhite;
    dgResultados.Canvas.Brush.Color := clBlack;
  end;

  dgResultados.Canvas.FillRect(Rect);
  dgResultados.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

end.
