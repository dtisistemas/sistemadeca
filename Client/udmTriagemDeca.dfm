object dmTriagemDeca: TdmTriagemDeca
  OldCreateOrder = False
  Left = 164
  Top = 148
  Height = 841
  Width = 1638
  object sckConnTriagemDeca: TSocketConnection
    ServerGUID = '{A6ED21FD-41FF-49C8-BEA5-B677F1ACF8A4}'
    ServerName = 'DecaServer.dbTriagemDeca'
    ObjectBroker = SimpleObjectBroker1
    Left = 48
    Top = 19
  end
  object SimpleObjectBroker1: TSimpleObjectBroker
    Servers = <
      item
      end>
    Left = 160
    Top = 19
  end
  object cdsSel_Par_EstadoCivil: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_estadocivil'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_EstadoCivil'
    RemoteServer = sckConnTriagemDeca
    Left = 89
    Top = 80
    object cdsSel_Par_EstadoCivilcod_estadocivil: TAutoIncField
      FieldName = 'cod_estadocivil'
      ReadOnly = True
    end
    object cdsSel_Par_EstadoCivildsc_estadocivil: TStringField
      FieldName = 'dsc_estadocivil'
    end
  end
  object cdsSel_Par_Cidades: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_cidade'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Cidades'
    RemoteServer = sckConnTriagemDeca
    Left = 88
    Top = 135
  end
  object cdsSel_Par_Nacionalidade: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_nacionalidade'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Nacionalidade'
    RemoteServer = sckConnTriagemDeca
    Left = 88
    Top = 192
  end
  object cdsSel_Cadastro_Bairro: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftString
        Name = '@cod_bairro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_unidade_referencia'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Cadastro_Bairro'
    RemoteServer = sckConnTriagemDeca
    Left = 88
    Top = 248
  end
  object cdsSel_Cadastro_Emissor: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@PE_COD_EMISSOR'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@PE_IND_STATUS'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Cadastro_Emissor'
    RemoteServer = sckConnTriagemDeca
    Left = 86
    Top = 304
  end
  object cdsSel_Par_Posicao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_posicao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_ativo'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Posicao'
    RemoteServer = sckConnTriagemDeca
    Left = 86
    Top = 362
  end
  object cdsSel_Pesquisa_Inscritos_Triagem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_nom_nome'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_nom_familiar'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao_digt'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_tipo_consulta'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Pesquisa_Inscritos_Triagem'
    RemoteServer = sckConnTriagemDeca
    Left = 86
    Top = 421
  end
  object cdsSel_Par_RelResponsabilidade: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_relResponsabilidade'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_RelResponsabilidade'
    RemoteServer = sckConnTriagemDeca
    Left = 86
    Top = 477
  end
  object cdsSel_Par_Natureza: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_natureza_hab'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Natureza'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 80
  end
  object cdsSel_Par_TipoHabitacao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_tipo_hab'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Tipo_Habitacao'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 135
  end
  object cdsSel_Par_Infraestrutura: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_infra_estrutura'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Infrestrutura'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 192
  end
  object cdsSel_Par_InstalacoesSanitarias: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_inst_sanitaria'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_InstalacoesSanitarias'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 248
  end
  object cdsSel_Par_CondicaoHabitabilidade: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_cond_hab'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_CondicaoHabitabilidade'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 304
  end
  object cdsSel_Par_Escolaridade: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_escolaridade'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Escolaridade'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 360
  end
  object cdsIns_Cadastro_TriagemDeca: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftGuid
        Name = '@pe_Cod_inscricao'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao_digt'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_nom_nome'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_moto'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprovadespmedic'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_carro'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_outroimovel'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_drogadicto'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_alcoolatra'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_paimaeadolescente'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_gestante'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_medicamentopsico'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_prostituicao'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_traficodrogas'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_convivedrogadiccao'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_convivealcool'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conviveportdoencamental'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conviveportdoencainfecto'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_membroautorinfracao'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_aquarela'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_criancasozinha'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprovoualuguel'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dta_renovacao'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dta_inscricao'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_qtdehabitantes'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_TempoResidencia'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_NumResidencia'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_RelResp'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_motoano'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_usoveic'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_qtdecomodos'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_tipo_hab'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_inst_sanitaria'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_infra_estrutura'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_cond_hab'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_natureza_hab'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_situacao'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_carroano'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_portaria'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_motivo_nestuda'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_encaminhamento'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_estadocivil'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_motivo'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_bairro'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_religiao'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_regiao'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_NumCertNasc'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_nascimento'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_RG'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_endereco'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_sexo'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_NomeEscola'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_cidade'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_num_cep'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_Complemento'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_num_fone_contato'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_pontuacao'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_nom_recado'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_parentesco'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Name = '@pe_num_despmedicas'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Name = '@pe_dsc_valoraluguel'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_grau_parentesc'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_SeMatric'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_escolaSerie'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_carromodel'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_motomodel'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_escolaPeriodo'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_TELPROPRIO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_TempoResid'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_idadeXserie'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_razaoincomp'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_ResideFam'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_MotivoProcuraCompl'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_NResideFam'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_PontoRef'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_telRecado'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_CPF'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_cod_sias'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_cod_UsuarioCadastro'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_observacao'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_DiagSaude'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_IncapazTrab'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_DoencaInfecto'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_InvalTempDef'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_log_dtacadastro'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dta_atualizaEscolar'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_inscritopor'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_matricula'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_readmissao'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_unidade_referencia'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_num_cartao_sus'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_cra'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_num_nis'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_num_cartao_passe'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_emissao_rg'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@pe_dsc_ra'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_invalidez_temporaria'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_invalidez_permanente'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_trabalho'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_cursos'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_nao_ficar_sozinho'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_tirar_rua'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_creas'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_cras'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_vij'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_as'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_ct'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_outros'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_veiculo_outras_priori'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_veiculo_trabalho'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_veiculo_unico_transp'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_medidas_socioeducativas'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_fisica'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_psicologica'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_sexual'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_negligencia'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_abandono'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conv_port_doencas_incap'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_nacionalidade'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_emissor'
        ParamType = ptInput
      end>
    ProviderName = 'dspIns_Cadastro_TriagemDeca'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 77
  end
  object cdsSel_Retorna_NIF_Valido: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_digito'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Retorna_NIF_Valido'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 421
  end
  object cdsIns_Cad_Inscricao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_digito'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspIns_Cad_Inscricao'
    RemoteServer = sckConnTriagemDeca
    Left = 262
    Top = 477
  end
  object cdsDel_Cad_Inscricao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_digito'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspDel_Cad_Inscricao'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 131
  end
  object cdsSel_Par_Cadastro_Regioes: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_regiao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Cadastro_Regioes'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 187
  end
  object cdsSel_CadastroFull: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao_digt'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Cadastro_Full'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 243
  end
  object cdsIns_Par_Cad_Bairro: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_bairro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_regiao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_unidade_referencia'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspIns_Par_Cad_Bairro'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 299
  end
  object cdsUpd_Par_Cad_Bairro: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_bairro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_bairro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_regiao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_unidade_referencia'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_Par_Cad_Bairro'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 355
  end
  object cdsUpd_Cadastro_TriagemDeca: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao_digt'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_nom_nome'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_moto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprovadespmedic'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_carro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_outroimovel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_drogadicto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_alcoolatra'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_paimaeadolescente'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_gestante'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_medicamentopsico'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_prostituicao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_traficodrogas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_convivedrogadiccao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_convivealcool'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conviveportdoencamental'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conviveportdoencainfecto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_membroautorinfracao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_aquarela'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_criancasozinha'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprovoualuguel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dta_renovacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dta_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_qtdehabitantes'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_TempoResidencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_NumResidencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_RelResp'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_motoano'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_usoveic'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_qtdecomodos'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_tipo_hab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_inst_sanitaria'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_infra_estrutura'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_cond_hab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_natureza_hab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_carroano'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_portaria'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_motivo_nestuda'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_encaminhamento'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_estadocivil'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_motivo'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_bairro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_religiao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_regiao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_NumCertNasc'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_nascimento'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_RG'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_endereco'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_sexo'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_NomeEscola'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_cidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_num_cep'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_Complemento'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_num_fone_contato'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_pontuacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_nom_recado'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_parentesco'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@pe_num_despmedicas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@pe_dsc_valoraluguel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_grau_parentesc'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_SeMatric'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_escolaSerie'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_carromodel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_motomodel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_escolaPeriodo'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_TELPROPRIO'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_TempoResid'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_idadeXserie'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_razaoincomp'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_ResideFam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_MotivoProcuraCompl'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_NResideFam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_PontoRef'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_telRecado'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_CPF'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_cod_sias'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_cod_UsuarioCadastro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_observacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_DiagSaude'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_IncapazTrab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_DoencaInfecto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_InvalTempDef'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_log_dtacadastro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dta_atualizaEscolar'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_inscritopor'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_matricula'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_readmissao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_unidade_referencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_num_cartao_sus'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_cra'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_num_nis'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_cartao_passe'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_emissao_rg'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_ra'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_invalidez_temporaria'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_invalidez_permanente'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_trabalho'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_cursos'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_nao_ficar_sozinho'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_tirar_rua'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_creas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_cras'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_vij'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_as'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_enc_ct'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_proc_outros'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_veiculo_outras_priori'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_veiculo_trabalho'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_veiculo_unico_transp'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_medidas_socioeducativas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_fisica'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_psicologica'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_sexual'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_negligencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia_abandono'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conv_port_doencas_incap'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_nacionalidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_emissor'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_Cadastro_TriagemDeca'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 417
  end
  object cdsSel_PontosFicha: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_alcoolatra'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_aquarela'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_carro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprovoualuguel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_convivealcool'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_convivedrogadiccao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conviveportdoencainfecto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conviveportdoencamental'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_conv_port_doencas_incap'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_criancasozinha'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprovadespmedic'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_drogadicto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_gestante'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_outroimovel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_medicamentopsico'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_membroautorinfracao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_moto'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_paimaeadolescente'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_prostituicao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_traficodrogas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_violencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_Cod_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_TempoResid'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_cond_hab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_natureza_hab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_estadocivil'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_infra_estrutura'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_inst_sanitaria'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_RelResp'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_tipo_hab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_qtdecomodos'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_qtdehabitantes'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_TempoResidencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@pe_num_despmedicas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@pe_dsc_valoraluguel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@Valor_pontos'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@qtde_InvalidosFamilia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftFloat
        Name = '@renda_Familiar'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@renda_beneficios'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftFloat
        Name = '@temp'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pnt_situacaosocial'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pnt_situacaomoradia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pnt_condSaude'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pnt_tempResidencia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pnt_bens'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_motivodesc'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@pe_num_renda'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@num_rendaTotal'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@num_despesas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_IncapazTrab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_NumPortaria'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftFloat
        Name = '@pe_habtcomodo'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftFloat
        Name = '@temp1'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftFloat
        Name = '@temp2'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftFloat
        Name = '@temp3'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_PontosFicha'
    RemoteServer = sckConnTriagemDeca
    Left = 450
    Top = 473
  end
  object cdsSel_BeneficiosSociais: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_idBeneficio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_BeneficiosSociais'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 77
    object cdsSel_BeneficiosSociaiscod_idBeneficio: TAutoIncField
      FieldName = 'cod_idBeneficio'
      ReadOnly = True
    end
    object cdsSel_BeneficiosSociaisdsc_beneficio: TStringField
      FieldName = 'dsc_beneficio'
      Size = 30
    end
    object cdsSel_BeneficiosSociaisnum_valorbeneficio: TBCDField
      FieldName = 'num_valorbeneficio'
      EditFormat = '0.00'
      Precision = 19
    end
  end
  object cdsIns_BeneficioSocial: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_beneficio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@num_valorbeneficio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspIns_BeneficioSocial'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 130
  end
  object cdsUpd_BeneficioSocial: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_idBeneficio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_beneficio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@num_valorbeneficio'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_BeneficioSocial'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 187
  end
  object cdsDel_BeneficioSocial: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_idBeneficio'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspDel_BeneficioSocial'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 243
  end
  object cdsFamilia_InsUpdDel: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_membrofamilia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_nome_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@ind_responsavel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_parentesco'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_escolaridade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@ind_incapaztrab'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@ind_portdoencacontagia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@ind_cond_recluso'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@ind_cond_falecido'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@ind_cond_nregcrianca'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@ind_cond_morafamilia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@ind_sexo_par'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@ind_estadocivil_par'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@ind_temguarda'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBCD
        Name = '@num_rendamensal_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@dat_nasc_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_diagnostico_par'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_cpf_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_rg_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_emissao_rg'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_emissor'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_cidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_nacionalidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_num_cartao_sus'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_cra'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_num_nis'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_cartao_passe'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_numcertnasc_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_inv_temp'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_ind_inv_perm'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_localtrab_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_tel_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_ocupacao_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_outrarenda'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_relacionamento_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_sitsaude_fam'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@acao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspFamilia_InsUpdDel'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 299
  end
  object cdsSel_Par_Parentesco: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_parentesco'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Par_Parentesco'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 355
  end
  object cdsdSel_Cadastro_CompFamiliar: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_membrofamilia'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Cadastro_CompFamiliar'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 417
  end
  object cdsSel_Cadastro_RegAtendimento: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Cadastro_RegAtendimento'
    RemoteServer = sckConnTriagemDeca
    Left = 625
    Top = 473
  end
  object cdsSel_Cadastro_Relatorio: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario_deca'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_tipo_relatorio'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_Cadastro_Relatorios'
    RemoteServer = sckConnTriagemDeca
    Left = 791
    Top = 76
  end
  object cdsIns_Cadastro_Relatorio: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@id_relatorio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@dta_digitacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@tipo_relatorio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_responsavel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@txt_relatorio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_entrevistador'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario_deca'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspIns_Cadastro_Relatorio'
    RemoteServer = sckConnTriagemDeca
    Left = 791
    Top = 128
  end
  object cdsUpd_Cadastro_Relatorio: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@id_relatorio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@dta_digitacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@tipo_relatorio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@cod_responsavel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@txt_relatorio'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@dsc_entrevistador'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario_deca'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_Cadastro_Relatorio'
    RemoteServer = sckConnTriagemDeca
    Left = 791
    Top = 184
  end
  object cdsSel_AtualizaUsuariosDecaRelatorio: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario_deca'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_AtualizaUsuariosDecaRelatorio'
    RemoteServer = sckConnTriagemDeca
    Left = 799
    Top = 296
  end
  object cdsUpd_AtualizaUsuariosDecaRelatorio: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario_deca'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_responsavel'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_AtualizaUsuariosDecaRelatorio'
    RemoteServer = sckConnTriagemDeca
    Left = 799
    Top = 241
  end
  object cdsSel_PendenciaDocumentos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_PendenciaDocumentos'
    RemoteServer = sckConnTriagemDeca
    Left = 799
    Top = 354
  end
  object cdsIns_PendenciaDocumentos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_cartprof'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprenda'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_compaluguel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comptemporesid'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_nis'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_nis'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_declaraescola'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_declescolar'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_sus'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_sus'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_cpf'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_cpf'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_outros'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_outros'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspIns_PendenciaDocumentos'
    RemoteServer = sckConnTriagemDeca
    Left = 799
    Top = 416
  end
  object cdsUpd_PendenciaDocumentos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_cartprof'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comprenda'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_compaluguel'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_comptemporesid'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_nis'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_nis'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_declaraescola'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_declescolar'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_sus'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_sus'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_cpf'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_cpf'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftBoolean
        Name = '@pe_ind_outros'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_detalhes_outros'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_PendenciaDocumentos'
    RemoteServer = sckConnTriagemDeca
    Left = 799
    Top = 472
  end
  object cdsUpd_AlteraStatusInscricao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_num_inscricao_digt'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUpd_AlteraStatusInscricao'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 76
  end
  object cdsGERA_DEMANDA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_regiao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_faixaetaria1'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_faixaetaria2'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_unidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_flg_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_escolaperiodo'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_nom_nome'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_flg_selecionado'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspGERA_DEMANDA'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 128
  end
  object cdsINS_DEMANDA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_ligacao1'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_quem_ligacao1'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_ligacao2'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_quem_ligacao2'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_ligacao3'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_quem_ligacao3'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_observacoes'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_flg_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_flg_selecionado'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspINS_DEMANDA'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 184
  end
  object cdsUPD_DEMANDA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_ligacao1'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_quem_ligacao1'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_ligacao2'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_quem_ligacao2'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_dat_ligacao3'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_quem_ligacao3'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_dsc_observacoes'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_flg_situacao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_cod_usuario'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_flg_selecionado'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspUPD_DEMANDA'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 240
  end
  object cdsDEL_DEMANDA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end>
    ProviderName = 'dspDEL_DEMANDA'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 296
  end
  object cdsSel_DEMANDA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_cod_inscricao'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspSel_DEMANDA'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 354
  end
  object cdsMIGRACAO_TRIAGEM_DECA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_codMatricula'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@pe_codInscricao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_Sexo'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_Bairro'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_PeriodoFundhas'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_CodUnidadeDestino'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_NumSecao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_DatAdmissao'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@pe_CodUsuario'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_EmissorRG'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_Naturalidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftString
        Name = '@pe_Nacionalidade'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftDateTime
        Name = '@pe_DatHistorico'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspMIGRACAO_TRIAGEM_DECA'
    RemoteServer = sckConnTriagemDeca
    Left = 978
    Top = 416
  end
  object cdsDel_ComposicaoFamiliar: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = Null
      end
      item
        DataType = ftGuid
        Name = '@Cod_membrofamilia'
        ParamType = ptInput
        Value = Null
      end>
    ProviderName = 'dspDel_ComposicaoFamiliar'
    RemoteServer = sckConnTriagemDeca
    Left = 825
    Top = 472
  end
end
