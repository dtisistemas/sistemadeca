unit uRelacaoParaTransferir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, StdCtrls, Buttons;

type
  TfrmRelacaoParaTransferir = class(TForm)
    DBGrid1: TDBGrid;
    dsSel_Mostra_a_Transferir: TDataSource;
    btnConfirmaTransferencia: TSpeedButton;
    btnSair: TSpeedButton;
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure btnConfirmaTransferenciaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelacaoParaTransferir: TfrmRelacaoParaTransferir;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmRelacaoParaTransferir.DBGrid1CellClick(Column: TColumn);
begin
  dmDeca.cdsSel_Mostra_a_Transferir.FieldByName('cod_matricula').AsString;
end;

procedure TfrmRelacaoParaTransferir.FormShow(Sender: TObject);
begin
  with dmDeca.cdsSel_Mostra_a_Transferir do
      begin
        Close;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
        Open;
      end;
end;

procedure TfrmRelacaoParaTransferir.btnConfirmaTransferenciaClick(
  Sender: TObject);
begin

if Application.Messagebox ('Deseja Confirmar Transferência?',
        'Confirmar Transferência',
        MB_ICONQUESTION + MB_YESNO ) = idYes then
begin
  with dmDeca.cdsInc_Cadastro_Historico do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Mostra_a_Transferir.FieldByName ('COD_MATRICULA').AsString;
      Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Params.ParamByName('@pe_dat_historico').AsDateTime := Date();
      Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 7;  //Código para Confirmação de Transf. Unidade
      Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Transferência de Unidade - Confirmação';
      Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Confirmada a transferência da criança/adolescente nesta data';
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;

      //Atualizar STATUS para "1" Ativo pois sai do status de afastado
      with dmDeca.cdsAlt_Cadastro_Status do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').Value := GetValue(dmDeca.cdsSel_Mostra_a_Transferir.FieldByName ('COD_MATRICULA').AsString);
        Params.ParamByName('@pe_ind_status').Value := 1;
        Execute;
      end;

      with dmDeca.cdsSel_Mostra_a_Transferir do
      begin
        Close;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
        Open;
        DBGrid1.Refresh;

      end;

      frmPrincipal.AtualizaStatusBarUnidade;

    end;

end;
end;
procedure TfrmRelacaoParaTransferir.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmRelacaoParaTransferir.btnSairClick(Sender: TObject);
begin
  frmRelacaoParaTransferir.Close;
end;

procedure TfrmRelacaoParaTransferir.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmRelacaoParaTransferir.Close;
  end
end;

end.
