unit uDadosTransferencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask;

type
  TfrmDadosTransferencias = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    txtDataInicial: TMaskEdit;
    txtDataFinal: TMaskEdit;
    btnVer: TBitBtn;
    btnCancelar: TBitBtn;
    procedure btnVerClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtDataInicialExit(Sender: TObject);
    procedure txtDataFinalExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDadosTransferencias: TfrmDadosTransferencias;

implementation

uses uDM, rTransferencia, uPrincipal, uUtil, rListaTransferencias;

{$R *.DFM}

procedure TfrmDadosTransferencias.btnVerClick(Sender: TObject);
begin
  //with dmDeca.cdsSel_Transferencias do
  //begin
  //  Close;
  //  Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
  //  Params.ParamByName('@pe_data_inicial').Value := StrToDate(txtDataInicial.Text);
  //  Params.ParamByName('@pe_data_final').Value := StrToDate(txtDataFinal.Text);
  //  Open;
  with dmDeca.cdsSel_Transferencias_Encaminhadas do
  begin
    Close;

    // Administrador, Consulta, Psicologia/Psicopedagogia, Instrutor/Professor,
    // Diretor, DRH, Triagem e Educa��o F�sica n�o podem alterar dados
    if vvIND_PERFIL = 8 then
      Params.ParamByName ('@pe_cod_unidade').Value := Null
    else
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;

    if (vvIND_PERFIL = 18) or (vvIND_PERFIL = 20) or (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then  //Perfil Servi�o Socail/Acompanhamento Escolar
      Params.ParamByName('@pe_cod_unidade').Value := Null
    else
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;

    Params.ParamByName ('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName ('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Open;

    Application.CreateForm(TrelListaTransferencias, relListaTransferencias);

    relListaTransferencias.lbPeriodo.Caption := 'DE ' + txtDataInicial.Text + ' AT� ' + txtDataFinal.Text;
    relListaTransferencias.lbUnidade.Caption := vvNOM_UNIDADE;
    relListaTransferencias.Preview;
    relListaTransferencias.Free;

  end;
end;

procedure TfrmDadosTransferencias.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmDadosTransferencias.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmDadosTransferencias.Close;
  end;
end;

procedure TfrmDadosTransferencias.txtDataInicialExit(Sender: TObject);
begin
  try
    StrToDate(txtDataInicial.Text);
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataInicial.Clear;
      txtDataInicial.SetFocus;
    end;
  end;
end;

procedure TfrmDadosTransferencias.txtDataFinalExit(Sender: TObject);
begin
  try
    StrToDate(txtDataFinal.Text);

    if StrToDate(txtDataFinal.Text) < StrToDate(txtDataInicial.Text) then
        begin
          ShowMessage('Data final do periodo � menor do que a data inicial.' + #13#10#13 +
                      'Favor informe data v�lida para realizar a consulta...');
          txtDataFinal.SetFocus;
        end
    else
      begin
        btnVer.Enabled := True;
        btnVer.SetFocus;
      end;
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataFinal.Clear;
      txtDataFinal.SetFocus;
    end;
  end;
end;

procedure TfrmDadosTransferencias.btnCancelarClick(Sender: TObject);
begin
  btnVer.Enabled := False;
  txtDataInicial.Clear;
  txtDataFinal.Clear;
  txtDataInicial.SetFocus;
end;

end.
