unit uConfirmaTransferenciaUnidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmConfirmaTransferenciaUnidade = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    txtData: TMaskEdit;
    txtOrigem: TEdit;
    txtDestino: TEdit;
    Panel1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    procedure btnPesquisarClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfirmaTransferenciaUnidade: TfrmConfirmaTransferenciaUnidade;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmConfirmaTransferenciaUnidade.AtualizaCamposBotoes(
  Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := False;
    txtOrigem.Enabled := False;
    txtDestino.Enabled := False;
    txtMatricula.Setfocus;

    btnConfirmar.Enabled := False;
    btnCancelar.Enabled := True;
  end
else if Modo = 'Confirmar' then
  begin
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtOrigem.Enabled := True;
    txtDestino.Enabled := True;
    txtMatricula.Setfocus;

    btnConfirmar.Enabled := True;
    btnCancelar.Enabled := False;
  end;
end;

procedure TfrmConfirmaTransferenciaUnidade.btnPesquisarClick(
  Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        with dmDeca.cdsSel_Ultima_Transferencia_Unidade do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Open;

          if dmDeca.cdsSel_Ultima_Transferencia_Unidade.Eof then
          begin
            ShowMessage('Aten��o' + #13+#10+#13+#10 +
                        'N�o existe Transfer�ncia de Unidade pendente para a Crian�a/Adolescente' + #13+#10 +
                        'Favor verificar... ');

            AtualizaCamposBotoes('Padrao');
          end
          else
          begin
            AtualizaCamposBotoes('Confirmar');
            txtDestino.Text := FieldByName('PARA').Value;
            txtOrigem.Text := FieldByName('DE').Value;
            txtData.Text := FieldByName('dat_historico').Value;
          end;
        end;

      end;
    except end;
  end;
end;

procedure TfrmConfirmaTransferenciaUnidade.txtMatriculaExit(
  Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;
            with dmDeca.cdsSel_Ultima_Transferencia_Unidade do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Open;

              if dmDeca.cdsSel_Ultima_Transferencia_Unidade.Eof then
              begin
                ShowMessage('Aten��o' + #13+#10+#13+#10 +
                            'N�o existe Transfer�ncia de Unidade pendente para a Crian�a/Adolescente' + #13+#10 +
                            'Favor verificar... ');

                AtualizaCamposBotoes('Padr�o');
              end
            
            else
            begin
              AtualizaCamposBotoes('Confirmar');
              txtDestino.Text := FieldByName('PARA').Value;
              txtOrigem.Text := FieldByName('DE').Value;
              txtData.Text := FieldByName('dat_historico').Value;
              btnConfirmar.SetFocus;
            end
          end
        end
      end
    except end;
  end
end;

procedure TfrmConfirmaTransferenciaUnidade.btnConfirmarClick(
  Sender: TObject);
begin
  if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
      if StatusCadastro(txtMatricula.Text) = 'Em Transfer�ncia' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 7;  //C�digo para Confirma��o de Transf. Unidade
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Transfer�ncia de Unidade - Confirma��o';
          Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Confirmada a transfer�ncia da crian�a/adolescente nesta data';
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "1" Ativo pois sai do status de afastado
          with dmDeca.cdsAlt_Cadastro_Status do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_ind_status').Value := 1;
              Execute;
          end;
        end;
        AtualizaCamposBotoes('Padrao');
        frmPrincipal.AtualizaStatusBarUnidade;
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                 'Esta(e) crian�a/adolescente n�o encontra-se em'+#13+#10+#13+#10+
                 'Transfer�ncia...');
        AtualizaCamposBotoes('Padrao');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'TRFc_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                             Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtData.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Transfer�ncia de Unidade - Confirma��o');//Descricao do Tipo de Historico
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);


      //ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10+#13+#10 +
      //         'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmConfirmaTransferenciaUnidade.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmConfirmaTransferenciaUnidade.btnSairClick(Sender: TObject);
begin
  frmConfirmaTransferenciaUnidade.Close;
end;

procedure TfrmConfirmaTransferenciaUnidade.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action :=  caFree;
end;

procedure TfrmConfirmaTransferenciaUnidade.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmConfirmaTransferenciaUnidade.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmConfirmaTransferenciaUnidade.Close;
  end
end;

end.
