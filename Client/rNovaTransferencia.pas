unit rNovaTransferencia;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelNovaTransferencia = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    DetailBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRShape2: TQRShape;
    QRLabel3: TQRLabel;
    QRShape3: TQRShape;
    QRLabel4: TQRLabel;
    QRShape4: TQRShape;
    QRLabel5: TQRLabel;
    QRShape5: TQRShape;
    QRLabel6: TQRLabel;
    QRShape6: TQRShape;
    QRLabel7: TQRLabel;
    QRShape7: TQRShape;
    QRLabel8: TQRLabel;
    QRShape8: TQRShape;
    QRLabel9: TQRLabel;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape12: TQRShape;
    QRLabel10: TQRLabel;
    QRShape13: TQRShape;
    QRLabel14: TQRLabel;
    lbCotas: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRShape14: TQRShape;
    QRLabel21: TQRLabel;
    lbUnidadeOrigem: TQRLabel;
    lbCcustoSecaoOrigem: TQRLabel;
    lbBancoOrigem: TQRLabel;
    lbUnidadeDestino: TQRLabel;
    lbCcustoSecaoDestino: TQRLabel;
    lbBancoDestino: TQRLabel;
    lbEntregaPasse: TQRLabel;
    lbPeriodo: TQRLabel;
    lbMatricula: TQRLabel;
    lbNome: TQRLabel;
    lbTipo: TQRLabel;
    lbMotivo: TQRLabel;
    QRShape15: TQRShape;
    lbDataTransferencia: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape16: TQRShape;
    QRLabel15: TQRLabel;
    QRShape17: TQRShape;
    QRLabel23: TQRLabel;
    lbEndereco: TQRLabel;
    lbBairro: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape18: TQRShape;
    QRLabel25: TQRLabel;
    QRShape19: TQRShape;
    QRLabel26: TQRLabel;
    lbMatricula1: TQRLabel;
    QRShape20: TQRShape;
    QRLabel28: TQRLabel;
    lbNome1: TQRLabel;
    QRShape21: TQRShape;
    QRLabel30: TQRLabel;
    lbEndereco1: TQRLabel;
    QRLabel32: TQRLabel;
    lbBairro1: TQRLabel;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRLabel34: TQRLabel;
    lbDataTransferencia1: TQRLabel;
    QRShape24: TQRShape;
    QRLabel36: TQRLabel;
    lbMotivo1: TQRLabel;
    lbTipo1: TQRLabel;
    QRShape25: TQRShape;
    QRLabel39: TQRLabel;
    QRShape26: TQRShape;
    QRLabel40: TQRLabel;
    lbUnidadeOrigem1: TQRLabel;
    QRShape27: TQRShape;
    QRLabel42: TQRLabel;
    lbCcustoSecaoOrigem1: TQRLabel;
    QRShape28: TQRShape;
    QRLabel44: TQRLabel;
    lbBancoOrigem1: TQRLabel;
    QRShape29: TQRShape;
    QRLabel46: TQRLabel;
    lbUnidadeDestino1: TQRLabel;
    QRShape30: TQRShape;
    QRLabel48: TQRLabel;
    lbCcustoSecaoDestino1: TQRLabel;
    QRShape31: TQRShape;
    QRLabel50: TQRLabel;
    lbBancoDestino1: TQRLabel;
    QRShape32: TQRShape;
    QRLabel52: TQRLabel;
    lbEntregaPasse1: TQRLabel;
    QRShape33: TQRShape;
    QRLabel54: TQRLabel;
    lbPeriodo1: TQRLabel;
    QRShape34: TQRShape;
    QRLabel56: TQRLabel;
    lbCotas1: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRShape35: TQRShape;
    QRLabel62: TQRLabel;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRLabel27: TQRLabel;
    lbCep1: TQRLabel;
    QRShape38: TQRShape;
    QRLabel29: TQRLabel;
    lbCep: TQRLabel;
    QRShape39: TQRShape;
    QRLabel31: TQRLabel;
    lbIdade1: TQRLabel;
    QRShape40: TQRShape;
    QRLabel33: TQRLabel;
    lbIdade2: TQRLabel;
    QRImage2: TQRImage;
  private

  public

  end;

var
  relNovaTransferencia: TrelNovaTransferencia;

implementation

uses uDM, uTransferenciaEncaminhamentoNova, uHistorico;

{$R *.DFM}

end.
