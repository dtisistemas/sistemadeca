unit rCONTRATO;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelCONTRATO = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    DetailBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel7: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel8: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel9: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel10: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel25: TQRLabel;
    QRMemo1: TQRMemo;
    QRMemo2: TQRMemo;
    QRLabel18: TQRLabel;
    PageFooterBand1: TQRBand;
    QRMemo3: TQRMemo;
    QRLabel19: TQRLabel;
    QRMemo4: TQRMemo;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRLabel23: TQRLabel;
    QRMemo5: TQRMemo;
    QRShape15: TQRShape;
    QRLabel11: TQRLabel;
    QRShape16: TQRShape;
    QRLabel24: TQRLabel;
    QRMemo6: TQRMemo;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    qrINFO_DATA: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRLabel26: TQRLabel;
    QRLabel32: TQRLabel;
    qrMATRICULA: TQRLabel;
    qrNOME1: TQRLabel;
    qrNASCIMENTO: TQRLabel;
    qrADMISSAO: TQRLabel;
    qrCPF_ALUNO: TQRLabel;
    qrRG_ALUNO: TQRLabel;
    qrRESPONSAVEL: TQRLabel;
    qrCPF_RESPONSAVEL: TQRLabel;
    qrRG_RESPONSAVEL: TQRLabel;
    qrESTADO_CIVIL: TQRLabel;
    qrPROFISSAO: TQRLabel;
    qrNACIONALIDADE: TQRLabel;
    qrENDERECO: TQRLabel;
    qrNOME2: TQRLabel;
    qrRESPONSAVEL2: TQRLabel;
    QRLabel30: TQRLabel;
  private

  public

  end;

var
  relCONTRATO: TrelCONTRATO;

implementation

{$R *.DFM}

end.
