unit rSolicitacaoTransferenciaPeriodoEscolar;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelSolicitacaoTransferenciaPeriodoEscolar = class(TQuickRep)
    DetailBand1: TQRBand;
    QRLabel2: TQRLabel;
    lbNomeAdolescente: TQRLabel;
    QRLabel3: TQRLabel;
    lbDataNascimento: TQRLabel;
    QRLabel5: TQRLabel;
    lbNumRA: TQRLabel;
    QRLabel7: TQRLabel;
    lbEndereco: TQRLabel;
    QRLabel9: TQRLabel;
    lbBairro: TQRLabel;
    QRLabel11: TQRLabel;
    lbUnidade: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    lbNomeAdolescente2: TQRLabel;
    QRLabel10: TQRLabel;
    lbNumRA2: TQRLabel;
    QRLabel14: TQRLabel;
    lbUnidade2: TQRLabel;
    QRLabel18: TQRLabel;
    lbEmpresa: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRImage1: TQRImage;
    QRLabel26: TQRLabel;
    QRImage2: TQRImage;
    QRShape8: TQRShape;
    QRLabel27: TQRLabel;
  private

  public

  end;

var
  relSolicitacaoTransferenciaPeriodoEscolar: TrelSolicitacaoTransferenciaPeriodoEscolar;

implementation

uses uEmissaoDeclaracoesSolicitacoesEscolares;

{$R *.DFM}

end.
