unit uComparaBimestres;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ComCtrls, ExtCtrls, Excel97, ComObj;

type
  TfrmComparaBimestres = class(TForm)
    GroupBox1: TGroupBox;
    cbBimestre1: TComboBox;
    cbBimestre2: TComboBox;
    btnComparar: TSpeedButton;
    edAno1: TEdit;
    edAno2: TEdit;
    SaveDialog1: TSaveDialog;
    procedure btnCompararClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComparaBimestres: TfrmComparaBimestres;
  objExcel, Sheet : Variant;
  lin : Integer;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmComparaBimestres.btnCompararClick(Sender: TObject);
begin
    //Criar a planilha, solicitando o local a ser salvo...
    objExcel         := CreateOleObject('Excel.Application');
    //objExcel2.Caption := 'Teste';
    objExcel.Workbooks.Add;
    SaveDialog1.Execute;
    //objExcel2.WorkBooks[1].SaveAs (ExtractFilePAth(Application.ExeName) + 'teste.xlsx');
    objExcel.WorkBooks[1].SaveAs (SaveDialog1.FileName);
    objExcel.Visible := True;
    objExcel.WorkBooks[1].Sheets.Add;

    Sheet := objExcel.WorkBooks[1].WorkSheets[1];

    Sheet.Cells[1,1].Font.Bold := True;
    Sheet.Cells[1,1].Font.Size := 8;
    Sheet.Cells[1,1].Value  := '[RA]';

    Sheet.Cells[1,2].Font.Bold := True;
    Sheet.Cells[1,2].Font.Size := 8;
    Sheet.Cells[1,2].Value  := '[MATR�CULA]';

    Sheet.Cells[1,3].Font.Bold := True;
    Sheet.Cells[1,3].Font.Size := 8;
    Sheet.Cells[1,3].Value  := '[NOME DO(A) ALUNO(A)]';

    Sheet.Cells[1,4].Font.Bold := True;
    Sheet.Cells[1,4].Font.Size := 8;
    Sheet.Cells[1,4].Value  := '[UNIDADE]';

    Sheet.Cells[1,5].Font.Bold := True;
    Sheet.Cells[1,5].Font.Size := 8;
    Sheet.Cells[1,5].Value  := '[SITUA��O] - ' + cbBimestre1.Text + '/' + edAno1.Text;

    Sheet.Cells[1,6].Font.Bold := True;
    Sheet.Cells[1,6].Font.Size := 8;
    Sheet.Cells[1,6].Value  := '[S�RIE] - ' + cbBimestre1.Text + '/' + edAno1.Text;


    //Selecionar todos do cadastro
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   :=  0;
        Open;
        ShowMessage('Total de Registros : '+ IntToStr(dmDeca.cdsSel_Cadastro.RecordCount));
      end;

      lin := 2;
      while not dmDeca.cdsSel_Cadastro.eof do
      begin

        if dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger in [47,48] then
          //dmDeca.cdsSel_Cadastro.Next
        else
        begin

          //Consulta os dados do Acompanhamento Escolar referente a matricula atual e dados do bimestre/ano inicial
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno1.Text);
            Params.ParamByName ('@pe_num_bimestre').Value          := cbBimestre1.ItemIndex + 1;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
            Open;

            //Se encontrou, repassa dos dados para a planilha...
            if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
            begin
              //RA
              Sheet.Cells[lin,1].Font.Size := 8;
              Sheet.Cells[lin,1].Value     := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_escolar').AsString;

              //Matricula
              Sheet.Cells[lin,2].Font.Size := 8;
              Sheet.Cells[lin,2].Value     := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;

              //Nome do Aluno
              Sheet.Cells[lin,3].Font.Size := 8;
              Sheet.Cells[lin,3].Value     := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString;

              //Unidade
              Sheet.Cells[lin,4].Font.Size := 8;
              Sheet.Cells[lin,4].Value     := dmDeca.cdsSel_Cadastro.FieldByName ('nom_unidade').AsString;

              //Situa��o no bimestre1/ano1
              Sheet.Cells[lin,5].Font.Size := 8;
              Sheet.Cells[lin,5].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').AsString;

              //Situa��o no bimestre1/ano1
              Sheet.Cells[lin,6].Font.Size := 8;
              Sheet.Cells[lin,6].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').AsString;


              //Montar o cabe�alho para os dados da compara��o
              Sheet.Cells[1,7].Font.Bold := True;
              Sheet.Cells[1,7].Font.Size := 8;
              Sheet.Cells[1,7].Value  := '[ESCOLA] - ' + cbBimestre2.Text + '/' + edAno2.Text;

              Sheet.Cells[1,8].Font.Bold := True;
              Sheet.Cells[1,8].Font.Size := 8;
              Sheet.Cells[1,8].Value  := '[S�RIE] - ' + cbBimestre2.Text + '/' + edAno2.Text;

              Sheet.Cells[1,9].Font.Bold := True;
              Sheet.Cells[1,9].Font.Size := 8;
              Sheet.Cells[1,9].Value  := '[PER�ODO] - ' + cbBimestre2.Text + '/' + edAno2.Text;

              Sheet.Cells[1,10].Font.Bold := True;
              Sheet.Cells[1,10].Font.Size := 8;
              Sheet.Cells[1,10].Value  := '[TURMA] - ' + cbBimestre2.Text + '/' + edAno2.Text;

              Sheet.Cells[1,11].Font.Bold := True;
              Sheet.Cells[1,11].Font.Size := 8;
              Sheet.Cells[1,11].Value  := '[SITUA��O] - ' + cbBimestre2.Text + '/' + edAno2.Text;

              Sheet.Cells[1,12].Font.Bold := True;
              Sheet.Cells[1,12].Font.Size := 8;
              Sheet.Cells[1,12].Value  := '[OBSERVA��ES] - ' + cbBimestre2.Text + '/' + edAno2.Text;

              //Consulta os dados do Acompanhamento Escolar referente a matricula atual e dados do bimestre/ano final
              with dmDeca.cdsSel_AcompEscolar do
              begin
                Close;
                Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno2.Text);
                Params.ParamByName ('@pe_num_bimestre').Value          := cbBimestre2.ItemIndex + 1;
                Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                Open;

                //Se encontrou, repassa dos dados para a planilha...
                if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
                begin

                  Sheet.Cells[lin,7].Font.Size := 8;
                  Sheet.Cells[lin,7].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').AsString;

                  Sheet.Cells[lin,8].Font.Size := 8;
                  Sheet.Cells[lin,8].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_Serie').AsString;

                  Sheet.Cells[lin,9].Font.Size := 8;
                  Sheet.Cells[lin,9].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').AsString;

                  Sheet.Cells[lin,10].Font.Size := 8;
                  Sheet.Cells[lin,10].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').AsString;

                  Sheet.Cells[lin,11].Font.Size := 8;
                  Sheet.Cells[lin,11].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').AsString;

                  Sheet.Cells[lin,12].Font.Size := 8;
                  Sheet.Cells[lin,12].Value     := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString;

                end;
              end;
              
              lin := lin + 1;

            end
            else
            begin

            end;

          end;
        end;

        dmDeca.cdsSel_Cadastro.Next;

      end;

      Application.MessageBox ('Processo finalizado!!!' + #10+#13 +
                              'Os dados referentes ao Acompanhamento Escolar foram gerados com sucesso.',
                              '[Sistema Deca] - Processo terminado.',
                              MB_OK + MB_ICONINFORMATION);



    except
      Application.MessageBox ('Aten��o!!!' + #10+#13 +
                              'Um erro ocorreu na tentativa de consultar os dados do Cadastro.' +#10+#13 +
                              'Verifique as conex�es de rede/internet e tente novamente.',
                              '[Sistema Deca] - ERRO Conex�o BD.',
                              MB_OK + MB_ICONWARNING);

    end;





end;

end.
