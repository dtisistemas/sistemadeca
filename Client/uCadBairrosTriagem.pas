unit uCadBairrosTriagem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, Db;

type
  TfrmCadBairrosTriagem = class(TForm)
    GroupBox1: TGroupBox;
    Bevel1: TBevel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    btnNovoBairro: TSpeedButton;
    btnAlterarBairro: TSpeedButton;
    btnSalvarBairro: TSpeedButton;
    btnCancelar: TSpeedButton;
    dbgBairrosTriagem: TDBGrid;
    edNomeBairro: TEdit;
    cbRegiao: TComboBox;
    cbUnidade: TComboBox;
    dsCadBairrosTriagem: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure dbgBairrosTriagemDblClick(Sender: TObject);
    procedure ModoTela(Modo: String);
    procedure btnNovoBairroClick(Sender: TObject);
    procedure btnAlterarBairroClick(Sender: TObject);
    procedure btnSalvarBairroClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadBairrosTriagem: TfrmCadBairrosTriagem;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses uDM, udmTriagemDeca;

{$R *.DFM}

procedure TfrmCadBairrosTriagem.FormShow(Sender: TObject);
begin
  ModoTela('P');
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  with dmTriagemDeca.cdsSel_Cadastro_Bairro do
  begin
    Close;
    Params.ParamByName ('@cod_bairro').Value                := Null;
    Params.ParamByName ('@pe_cod_unidade_referencia').Value := Null;
    Open;
    dbgBairrosTriagem.Refresh;
  end;

  cbUnidade.Clear;
  //Carregar a lista de Unidades do Sistema Deca...
  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := Null;
    Params.ParamByName ('@pe_nom_unidade').Value := Null;
    Open;

    while not (dmDeca.cdsSel_Unidade.eof) do
    begin

      //Se unidade desativada, n�o carrega...
      if ( dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0 ) then //or ( dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo_unidade').Value <> 3 ) then
      begin
        cbUnidade.Items.Add ( dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value );
        vListaUnidade1.Add ( IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value) );
        vListaUnidade2.Add ( dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value );
      end;
      Next;
    end;
  end;
end;

procedure TfrmCadBairrosTriagem.dbgBairrosTriagemDblClick(Sender: TObject);
begin
  ModoTela('A');
  //Transfere os dados do grid para os combos
  edNomeBairro.Text   := dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('dsc_bairro').Value;
  cbRegiao.ItemIndex  := cbRegiao.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('dsc_regiao').Value));
  if (dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('nom_unidade').IsNull) then cbUnidade.ItemIndex := -1
  else cbUnidade.ItemIndex := cbUnidade.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('nom_unidade').Value));
end;

procedure TfrmCadBairrosTriagem.ModoTela(Modo: String);
begin
  if Modo = 'P' then
  begin
    btnNovoBairro.Enabled    := True;
    btnAlterarBairro.Enabled := False;
    btnSalvarBairro.Enabled  := False;
    btnCancelar.Enabled      := False;

    edNomeBairro.Clear;
    cbRegiao.ItemIndex  := -1;
    cbUnidade.ItemIndex := -1;

    GroupBox3.Enabled := False;
    GroupBox4.Enabled := False;
    GroupBox5.Enabled := False;
  end
  else if Modo = 'N' then
  begin
    GroupBox3.Enabled := True;
    GroupBox4.Enabled := True;
    GroupBox5.Enabled := True;

    btnNovoBairro.Enabled    := False;
    btnAlterarBairro.Enabled := False;
    btnSalvarBairro.Enabled  := True;
    btnCancelar.Enabled      := True;
  end
  else if Modo = 'A' then
  begin
    GroupBox3.Enabled := True;
    GroupBox4.Enabled := True;
    GroupBox5.Enabled := True;

    btnNovoBairro.Enabled    := False;
    btnAlterarBairro.Enabled := True;
    btnSalvarBairro.Enabled  := False;
    btnCancelar.Enabled      := True;
  end;
end;

procedure TfrmCadBairrosTriagem.btnNovoBairroClick(Sender: TObject);
begin
  ModoTela('N');
end;

procedure TfrmCadBairrosTriagem.btnAlterarBairroClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsUpd_Par_Cad_Bairro do
    begin
      Close;
      Params.ParamByName ('@cod_bairro').Value                := dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('cod_bairro').Value;
      Params.ParamByName ('@pe_dsc_bairro').Value             := Trim(edNomeBairro.Text);
      Params.ParamByName ('@pe_dsc_regiao').Value             := cbRegiao.Text;
      Params.ParamByName ('@pe_cod_unidade_referencia').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Execute;
      dbgBairrosTriagem.Refresh;
      ModoTela('P');
      frmCadBairrosTriagem.FormShow(frmCadBairrosTriagem);
    end;
  except
    begin
      Application.MessageBox ('Aten��o!!!' +#13+#10+
                              'Um erro ocorreu na tentativa de alterar os dados do bairro atual.' +#13+#10+
                              'Favor tentar novamente ou contatar o Administrador do Sistema',
                              '[Sistema Deca] - Erro alterando dados de Bairro',
                              MB_OK + MB_ICONINFORMATION);
      ModoTela('P');
    end;
  end;
end;

procedure TfrmCadBairrosTriagem.btnSalvarBairroClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsIns_Par_Cad_Bairro do
    begin
      Close;
      Params.ParamByName ('@pe_dsc_bairro').Value             := Trim(edNomeBairro.Text);
      Params.ParamByName ('@pe_dsc_regiao').Value             := cbRegiao.Text;
      Params.ParamByName ('@pe_cod_unidade_referencia').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Execute;
      dbgBairrosTriagem.Refresh;
      ModoTela('P');
      frmCadBairrosTriagem.FormShow(frmCadBairrosTriagem);
    end;
  except
    begin
      Application.MessageBox ('Aten��o!!!' +#13+#10+
                              'Um erro ocorreu na tentativa de alterar os dados do bairro atual.' +#13+#10+
                              'Favor tentar novamente ou contatar o Administrador do Sistema',
                              '[Sistema Deca] - Erro alterando dados de Bairro',
                              MB_OK + MB_ICONINFORMATION);
      ModoTela('P');
    end;
  end;
end;

procedure TfrmCadBairrosTriagem.btnCancelarClick(Sender: TObject);
begin
  ModoTela('P');
end;

end.
