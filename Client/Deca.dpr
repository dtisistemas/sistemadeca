
program Deca;

uses
  Forms,
  Windows,
  uDM in 'uDM.pas' {dmDeca: TDataModule},
  uUsuarios in 'uUsuarios.pas' {frmUsuarios},
  uUtil in 'uUtil.pas',
  uFichaPesquisa in 'uFichaPesquisa.pas' {frmFichaPesquisa},
  uAdvertencia in 'uAdvertencia.pas' {frmAdvertencia},
  rAdvertencias in 'rAdvertencias.pas' {relAdvertencias: TQuickRep},
  uSuspensao in 'uSuspensao.pas' {frmSuspensao},
  rSuspensao in 'rSuspensao.pas' {relSuspensao: TQuickRep},
  uDesligamento in 'uDesligamento.pas' {frmDesligamento},
  rDesligamento in 'rDesligamento.pas' {relDesligamento: TQuickRep},
  uAfastamentoEntrada in 'uAfastamentoEntrada.pas' {frmAfastamentoEntrada},
  uAfastamentoRetorno in 'uAfastamentoRetorno.pas' {frmAfastamentoRetorno},
  uTransferenciaEncaminhamento in 'uTransferenciaEncaminhamento.pas' {frmTransferenciaEncaminhamento},
  uRegistroAtendimento in 'uRegistroAtendimento.pas' {frmRegistroAtendimento},
  uEvolucaoServicoSocial in 'uEvolucaoServicoSocial.pas' {frmEvolucaoServicoSocial},
  rAfastamento in 'rAfastamento.pas' {relAfastamento: TQuickRep},
  rListaReuniaoPais in 'rListaReuniaoPais.pas' {relListaReuniaoPais: TQuickRep},
  uComplementacaoEncaminhamento in 'uComplementacaoEncaminhamento.pas' {frmComplementacaoEncaminhamento},
  uComplementacaoRetorno in 'uComplementacaoRetorno.pas' {frmComplementacaoRetorno},
  uHistorico in 'uHistorico.pas' {frmHistorico},
  rGeral in 'rGeral.pas' {relGeral: TQuickRep},
  uSelecionaUnidade in 'uSelecionaUnidade.pas' {frmSelecionaUnidade},
  rDadosEscolares in 'rDadosEscolares.pas' {relDadosEscolares: TQuickRep},
  rCepOrdenadoAdm in 'rCepOrdenadoAdm.pas' {relCepOrdenadoAdm: TQuickRep},
  rCepOrdenado in 'rCepOrdenado.pas' {relCepOrdenado: TQuickRep},
  uDadosAfastamento in 'uDadosAfastamento.pas' {frmDadosAfastamento},
  rAfastamentos in 'rAfastamentos.pas' {relAfastamentos: TQuickRep},
  uDadosAdvertencias in 'uDadosAdvertencias.pas' {frmDadosAdvertencias},
  rRelacaoAdvertencias in 'rRelacaoAdvertencias.pas' {relRelacaoAdvertencias: TQuickRep},
  uDadosDesligamentos in 'uDadosDesligamentos.pas' {frmDadosDesligamento},
  rDesligados in 'rDesligados.pas' {relDesligados: TQuickRep},
  uDadosSuspensoes in 'uDadosSuspensoes.pas' {frmDadosSuspensao},
  uAniversariantes in 'uAniversariantes.pas' {frmAniversariantes},
  rAniversariantes in 'rAniversariantes.pas' {relAniversariantes: TQuickRep},
  rTransferencia in 'rTransferencia.pas' {relTransferencias: TQuickRep},
  uConfirmaTransferenciaUnidade in 'uConfirmaTransferenciaUnidade.pas' {frmConfirmaTransferenciaUnidade},
  uTransferenciaConvenio in 'uTransferenciaConvenio.pas' {frmTransferenciaConvenio},
  uTransferenciaConvenioConf in 'uTransferenciaConvenioConf.pas' {frmTransferenciaConvenioConf},
  uDadosTransferencias in 'uDadosTransferencias.pas' {frmDadosTransferencias},
  rListaTransferencias in 'rListaTransferencias.pas' {relListaTransferencias: TQuickRep},
  uMudaMatricula in 'uMudaMatricula.pas' {frmMudaMatricula},
  uEmiteRegAtendimento in 'uEmiteRegAtendimento.pas' {frmEmiteRegAtendimento},
  rRegistroAtendimento in 'rRegistroAtendimento.pas' {relRegistroAtendimento: TQuickRep},
  uEmiteEvSocial in 'uEmiteEvSocial.pas' {frmEmiteEvSocial},
  rEvolucaoSocial in 'rEvolucaoSocial.pas' {relEvolucaoSocial: TQuickRep},
  uInformaPeriodoIndicador in 'uInformaPeriodoIndicador.pas' {frmInformaPeriodoIndicador},
  rIndicadores in 'rIndicadores.pas' {relIndicadores: TQuickRep},
  rIndicadores2 in 'rIndicadores2.pas' {relIndicadores2: TQuickRep},
  uEvolucaoConsAltera in 'uEvolucaoConsAltera.pas' {frmEvolucaoConsAltera},
  rTotaisUnidade in 'rTotaisUnidade.pas' {relTotaisUnidade: TQuickRep},
  uRelacaoParaTransferir in 'uRelacaoParaTransferir.pas' {frmRelacaoParaTransferir},
  uSelecionaIdadeDataEspecificao in 'uSelecionaIdadeDataEspecificao.pas' {frmSelecionaIdadeDataEspecifica},
  rIdades in 'rIdades.pas' {relIdades: TQuickRep},
  uRegistroAtendimentoAltera in 'uRegistroAtendimentoAltera.pas' {frmRegistroAtendimentoAltera},
  uControleFrequencias in 'uControleFrequencias.pas' {frmControleFrequencia},
  uInicializarPresenca in 'uInicializarPresenca.pas' {frmInicializarPresenca},
  rFolhaFrequencia in 'rFolhaFrequencia.pas' {relFolhaFrequencia: TQuickRep},
  uVisitaTecnica in 'uVisitaTecnica.pas' {frmVisitaTecnica},
  uTurmas in 'uTurmas.pas' {frmTurmas},
  uLancamentoBiometrico in 'uLancamentoBiometrico.pas' {frmLancamentoBiometrico},
  uSelecionaFrequencia in 'uSelecionaFrequencia.pas' {frmSelecionaFrequencia},
  uControleFrequenciasMatricula in 'uControleFrequenciasMatricula.pas' {frmControleFrequenciasMatricula},
  rFichaCadastro in 'rFichaCadastro.pas' {relFichaCadastro: TQuickRep},
  uSelecionaFrequenciaPorMatricula in 'uSelecionaFrequenciaPorMatricula.pas' {frmSelecionaFrequenciaPorMatricula},
  uConsultaVisitaTecnica in 'uConsultaVisitaTecnica.pas' {frmConsultaVisitaTecnica},
  uInicializarBimestre in 'uInicializarBimestre.pas' {frmInicializarBimestre},
  rAcompanhamentoFrente in 'rAcompanhamentoFrente.pas' {relAcompanhamentoFrente: TQuickRep},
  uEmiteAcompanhamentoEscolar in 'uEmiteAcompanhamentoEscolar.pas' {frmEmiteAcompanhamentoEscolar},
  rAcompanhamentoVerso in 'rAcompanhamentoVerso.pas' {frmrelAcompanhamentoVerso: TQuickRep},
  uConsultaAcompanhamentoEscolar in 'uConsultaAcompanhamentoEscolar.pas' {frmConsultaAcompanhamentoEscolar},
  uEmpresas in 'uEmpresas.pas' {frmEmpresas},
  rGraficoTotalConvenio in 'rGraficoTotalConvenio.pas' {relGraficoTotalConvenio: TQuickRep},
  rSuspensos in 'rSuspensos.pas' {relSuspensos: TQuickRep},
  uNovidades in 'uNovidades.pas' {frmNovidadesVersao},
  uTransferenciasDRH in 'uTransferenciasDRH.pas' {frmTransferenciasDRH},
  rRelacaoTransferenciasDRH in 'rRelacaoTransferenciasDRH.pas' {relRelacaoTransferenciasDRH: TQuickRep},
  uFichaPesquisaGenerica in 'uFichaPesquisaGenerica.pas' {frmFichaPesquisaGenerica},
  uUsuariosConectados in 'uUsuariosConectados.pas' {frmUsuariosConectados},
  uUnidades in 'uUnidades.pas' {frmUnidades},
  uEmiteRelatorioEspecial in 'uEmiteRelatorioEspecial.pas' {frmEmiteRelatorioEspecial},
  uRegistroAcompanhamentoPsicopedagogicoPsicologico in 'uRegistroAcompanhamentoPsicopedagogicoPsicologico.pas' {frmRegistroAcompanhamentoPsicopedagogicoPsicologico},
  uObservacaoAprendizagem in 'uObservacaoAprendizagem.pas' {frmObservacaoAprendizagem},
  rObservacaoAprendizagem in 'rObservacaoAprendizagem.pas' {relObservacaoAprendizagem: TQuickRep},
  rObservacaoAprendizagem2 in 'rObservacaoAprendizagem2.pas' {relObservacaoAprendizagem2: TQuickRep},
  uPerfis in 'uPerfis.pas' {frmPerfil},
  rTotalPorUnidade in 'rTotalPorUnidade.pas' {relTotalPorUnidade: TQuickRep},
  uFatoresIntervTecnica in 'uFatoresIntervTecnica.pas' {frmFatoresIntervTecnica},
  uLancamentosFatoresIntervencao in 'uLancamentosFatoresIntervencao.pas' {frmLancamentoFatoresIntervencao},
  uLancamentoItensFatores in 'uLancamentoItensFatores.pas' {frmLancamentoItensFatores},
  uEscolas in 'uEscolas.pas' {frmEscolas},
  uProgramasSociais in 'uProgramasSociais.pas' {frmProgramasSociais},
  uCalendarios in 'uCalendarios.pas' {frmCalendarios},
  uItensCalendario in 'uItensCalendario.pas' {frmItensCalendario},
  uLoginNovo in 'uLoginNovo.pas' {frmLoginNovo},
  uTransferenciaEncaminhamentoNova in 'uTransferenciaEncaminhamentoNova.pas' {frmNovaTransferencia},
  uSecoes in 'uSecoes.pas' {frmSecoes},
  rNovaTransferencia in 'rNovaTransferencia.pas' {relNovaTransferencia: TQuickRep},
  uControleUnidade in 'uControleUnidade.pas' {frmControleUnidade},
  uDesligamentoProgSocial in 'uDesligamentoProgSocial.pas' {frmDesligamentoProgSocial},
  rCadastro_ProgramasSociais in 'rCadastro_ProgramasSociais.pas' {relCadastro_ProgramasSociais: TQuickRep},
  uRegistroAtendimentoFamilia in 'uRegistroAtendimentoFamilia.pas' {frmRegistroAtendimentoFamilia},
  uRegistroAtendimentoProfissional in 'uRegistroAtendimentoProfissional.pas' {frmRegistroAtendimentoProfissional},
  rEstatisticasAtendimento in 'rEstatisticasAtendimento.pas' {relEstatisticasAtendimento: TQuickRep},
  uSelecionaDataEstatisticaAtendimento in 'uSelecionaDataEstatisticaAtendimento.pas' {frmSelecionaDataEstatisticaAtendimento},
  uFichaClinica in 'uFichaClinica.pas' {frmFichaClinica},
  uRegistroAbordagemGrupal in 'uRegistroAbordagemGrupal.pas' {frmRegistroAbordagemGrupal},
  uAvaliacaoDesempenho in 'uAvaliacaoDesempenho.pas' {frmAvaliacaoDesempenho},
  uProcessoAdmissional in 'uProcessoAdmissional.pas' {frmProcessoAdmissional},
  rProjetoVida in 'rProjetoVida.pas' {relProjetoVida: TQuickRep},
  uNovoLancamentoBiometrico in 'uNovoLancamentoBiometrico.pas' {frmNovoLancamentoBiometrico},
  rAvaliacaoBiometrica in 'rAvaliacaoBiometrica.pas' {relAvaliacaoBiometrica: TQuickRep},
  uOpcoesImpressaoBiometricos in 'uOpcoesImpressaoBiometricos.pas' {frmOpcoesImpressaoBiometricos},
  uReclassificarIMC in 'uReclassificarIMC.pas' {frmReclassificarIMC},
  uAcompanhamentoBiometrico in 'uAcompanhamentoBiometrico.pas' {frmAcompanhamentoBiometrico},
  rGraficoBiometricoUnidade in 'rGraficoBiometricoUnidade.pas' {relGraficoBiometricoUnidade: TQuickRep},
  rClassificacao in 'rClassificacao.pas' {relClassificacao: TQuickRep},
  rAgendaDiaOdontologia in 'rAgendaDiaOdontologia.pas' {relAgendaDiaOdontologia: TQuickRep},
  uAvaliacaoDesempenhoAprendiz in 'uAvaliacaoDesempenhoAprendiz.pas' {frmAvaliacaoDesempenhoAprendiz},
  uPesquisaAvaliacaoDesempApr in 'uPesquisaAvaliacaoDesempApr.pas' {frmPesquisaAvaliacaoDesempAprendiz},
  rResultadoAvaliacaoDesempAprendiz in 'rResultadoAvaliacaoDesempAprendiz.pas' {relResultadoAvaliacaoDesempAprendiz: TQuickRep},
  uReceituario in 'uReceituario.pas' {frmReceituario},
  rMedicamentos in 'rMedicamentos.pas' {relMedicamentos: TQuickRep},
  rReceituario in 'rReceituario.pas' {relReceituario: TQuickRep},
  uLancarFaltas_Teste in 'uLancarFaltas_Teste.pas' {frmLancarFaltas_Teste},
  rAdolescentesEmpresasConvenio in 'rAdolescentesEmpresasConvenio.pas' {relAdolescentesEmpresasConvenio: TQuickRep},
  uCadastroSecoes in 'uCadastroSecoes.pas' {frmNovoCadastroSecoes},
  rCaroscopio in 'rCaroscopio.pas' {relCaroscopio: TQuickRep},
  uCadastroSeriesPorEscola in 'uCadastroSeriesPorEscola.pas' {frmCadastroSeriesPorEscola},
  uCadastroSeriesEscolares in 'uCadastroSeriesEscolares.pas' {frmCadastroSerieEscolar},
  uTotaisUsuarioUnidade in 'uTotaisUsuarioUnidade.pas' {frmTotaisUsuarioUnidade},
  uTotalRegistrosPorUnidade in 'uTotalRegistrosPorUnidade.pas' {relTotalRegistrosPorUnidade: TQuickRep},
  rTotalRegistrosPorUsuarioUnidade in 'rTotalRegistrosPorUsuarioUnidade.pas' {relTotalRegistrosPorUsuarioUnidade: TQuickRep},
  uTransferenciasDAPA in 'uTransferenciasDAPA.pas' {frmTransferenciasDAPA},
  rTransferenciasDAPA in 'rTransferenciasDAPA.pas' {relTransferenciasDAPA: TQuickRep},
  uAcesso in 'uAcesso.pas' {frmAcesso},
  uGerenciarAcessos in 'uGerenciarAcessos.pas' {frmGerenciarAcessos},
  uSlpash in 'uSlpash.pas' {frmSplash},
  rModeloBoletim in 'rModeloBoletim.pas' {relModeloBoletim: TQuickRep},
  uDefasagemEscolar in 'uDefasagemEscolar.pas' {frmDefasagemEscolar},
  uSelecionaDadosAcompEscolarMatriz in 'uSelecionaDadosAcompEscolarMatriz.pas' {frmSelecionaDadosAcompEscolarMatriz},
  uEncaminhamentoSolicitacaoChefia in 'uEncaminhamentoSolicitacaoChefia.pas' {frmEncaminhamentoSolicitacaoChefia},
  uAlteracoesVersao in 'uAlteracoesVersao.pas' {frmAlteracoesVersao},
  uImportaEscolaCadParaAcompEscolar in 'uImportaEscolaCadParaAcompEscolar.pas' {frmImportaEscolaCadParaAcompEscolar},
  rSatisfacaoConveniados in 'rSatisfacaoConveniados.pas' {relSatisfacaoConveniados: TQuickRep},
  rAproveitamentoMatriz in 'rAproveitamentoMatriz.pas' {relAproveitamentoMatriz: TQuickRep},
  uFormAproveitamento in 'uFormAproveitamento.pas' {frmFormularioAproveitamento},
  uComparaBimAnoAcompEscolar in 'uComparaBimAnoAcompEscolar.pas' {frmComparaBimestreAno},
  uConsultaAcompEscolar in 'uConsultaAcompEscolar.pas' {frmConsultaAcompEscolar},
  uVerDadosEscolaSelecionada in 'uVerDadosEscolaSelecionada.pas' {frmVerDadosEscolaSelecionada},
  uEnviarEmail in 'uEnviarEmail.pas' {frmEnviarEmail},
  uSobreNavegador in 'uSobreNavegador.pas' {frmSobreNavegador},
  uNavegador in 'uNavegador.pas' {frmNavegador},
  rAlunosComDefasagem in 'rAlunosComDefasagem.pas' {relAlunosComDefasagem: TQuickRep},
  uSelecionaDadosDefasagemEscolar in 'uSelecionaDadosDefasagemEscolar.pas' {frmSelecionaDadosDefasagemEscolar},
  rResumoMensal in 'rResumoMensal.pas' {relResumoMensal: TQuickRep},
  rResumoMensal2 in 'rResumoMensal2.pas' {relResumoMensal2: TQuickRep},
  uLegendaAcompEscolar in 'uLegendaAcompEscolar.pas' {frmLegendaAcompEscolar},
  rAlunosSemAcompEscolar in 'rAlunosSemAcompEscolar.pas' {relAlunosSemAcompEscolar: TQuickRep},
  rControleAcompanhamentoEscolar in 'rControleAcompanhamentoEscolar.pas' {relControleAcompanhamentoEscolar: TQuickRep},
  uConsultaDadosAcompEscolar in 'uConsultaDadosAcompEscolar.pas' {frmConsultaDadosAcompEscolar},
  uDiasUteis in 'uDiasUteis.pas' {frmCalendarioDiasUteis},
  uGeraTXT in 'uGeraTXT.pas' {frmGeraTXT},
  uGeradorArquivosFaltasDRH in 'uGeradorArquivosFaltasDRH.pas' {frmGeradorArquivosFaltasDRH},
  uResumoMensal in 'uResumoMensal.pas' {frmResumoMensal},
  uSatisfacaoConveniados in 'uSatisfacaoConveniados.pas' {frmSatisfacaoConveniados},
  rObsSatisfConv in 'rObsSatisfConv.pas' {relObsSatisfConv: TQuickRep},
  rControleAcompanhamentoEscolar_Teste in 'rControleAcompanhamentoEscolar_Teste.pas' {relControleAcompanhamentoEscolar_Teste: TQuickRep},
  uExportaDadosAcompEscolarParaEscolas in 'uExportaDadosAcompEscolarParaEscolas.pas' {frmExportaDadosAcompEscolarParaEscolas},
  uFichaCrianca in 'uFichaCrianca.pas' {frmFichaCrianca},
  rRelacaoFaltasMesDRH in 'rRelacaoFaltasMesDRH.pas' {relRelacaoFaltasMesDRH: TQuickRep},
  uNovoControleFrequencia in 'uNovoControleFrequencia.pas' {frmNovoControleFrequencia},
  uConsultaDadosDoBoletim in 'uConsultaDadosDoBoletim.pas' {frmConsultaDadosDoBoletim},
  uConsultaHistoricoSocial in 'uConsultaHistoricoSocial.pas' {frmHistoricoSocial},
  rSolicitacaoTransferenciaPeriodoEscolar in 'rSolicitacaoTransferenciaPeriodoEscolar.pas' {relSolicitacaoTransferenciaPeriodoEscolar: TQuickRep},
  rAtestadoConvenio in 'rAtestadoConvenio.pas' {relAtestadoConvenio: TQuickRep},
  rSolicitacaoVagaEscolar in 'rSolicitacaoVagaEscolar.pas' {relSolicitacaoVagaEscolar: TQuickRep},
  uEstatisticasAcompEscolar in 'uEstatisticasAcompEscolar.pas' {frmEstatisticasAcompEscolar},
  rEstatisticasAcompEscolar in 'rEstatisticasAcompEscolar.pas' {relEstatisticasAcompEscolar: TQuickRep},
  rDadosBoletim in 'rDadosBoletim.pas' {relDadosBoletim: TQuickRep},
  uResumoMensal_Teste in 'uResumoMensal_Teste.pas' {frmResumoMensal_Teste},
  uExibeSemEscola in 'uExibeSemEscola.pas' {frmExibeSemEscola},
  rSemEscolaUnidade in 'rSemEscolaUnidade.pas' {relSemEscolaUnidade: TQuickRep},
  uPrincipal in 'uPrincipal.pas' {frmPrincipal},
  uImportaDadosBoletimAcompanhamentoEscolar in 'uImportaDadosBoletimAcompanhamentoEscolar.pas' {frmImportaDadosBoletimAcompanhamentoEscolar},
  uSelecionarDadosFrequencia in 'uSelecionarDadosFrequencia.pas' {frmSelecionarDadosFrequencia},
  uInicializarFrequenciaMensal in 'uInicializarFrequenciaMensal.pas' {frmInicializarFrequenciaMensal},
  rRelacaoValeTransporte in 'rRelacaoValeTransporte.pas' {relRelacaoValeTransporte: TQuickRep},
  uAlteracaoEndereco in 'uAlteracaoEndereco.pas' {frmAlteracaoEndereco},
  uImportaCotasPasse in 'uImportaCotasPasse.pas' {frmImportaCotasPasse},
  uConfereDadosDECADRH in 'uConfereDadosDECADRH.pas' {frmConfereDadosDECADRH},
  uConfereDadosDRHDECA in 'uConfereDadosDRHDECA.pas' {frmConfereDadosDRHDECA},
  uAproveitamentoIndividual in 'uAproveitamentoIndividual.pas' {frmAproveitamentoIndividual},
  rGraficoFaltasUnidadesMesPeriodo in 'rGraficoFaltasUnidadesMesPeriodo.pas' {relGraficoFaltasUnidadesMesPeriodo: TQuickRep},
  uPesquisaUsuarios in 'uPesquisaUsuarios.pas' {frmPesquisaUsuarios},
  rNovoRegistroAtendimento in 'rNovoRegistroAtendimento.pas' {relNovoRegistroAtendimento: TQuickRep},
  rResumoMatriz in 'rResumoMatriz.pas' {relResumoMatriz: TQuickRep},
  uRegistroTreinamentoUsuarios in 'uRegistroTreinamentoUsuarios.pas' {frmRegistroTreinamentoUsuarios},
  uLogoff in 'uLogoff.pas' {frmLogoff},
  uGerarDadosMatriz in 'uGerarDadosMatriz.pas' {frmGerarDadosMatriz},
  uAlteraSenhaAcesso in 'uAlteraSenhaAcesso.pas' {frmAlteraSenhaAcesso},
  uGeraEstatisticaMatriz in 'uGeraEstatisticaMatriz.pas' {frmGeraEstatisticaMatriz},
  uConferirAguardandoInformacao in 'uConferirAguardandoInformacao.pas' {frmConfereAguardandoInformacao},
  uControleCalendarios in 'uControleCalendarios.pas' {frmControleCalendarios},
  uImportaAcompEscolar20102011 in 'uImportaAcompEscolar20102011.pas' {frmImportaAcompEscolar20102011},
  rTotalizacaoPorUnidade_DS in 'rTotalizacaoPorUnidade_DS.pas' {relTotalizacaoPorUnidade_DS: TQuickRep},
  uGeral_AcopanhamentoEscolar in 'uGeral_AcopanhamentoEscolar.pas' {frmGeral_AcompanhamentoEscolar},
  uExportarDadosAcompEscolar in 'uExportarDadosAcompEscolar.pas' {frmExportarDadosAcompEscolar},
  RAguardandoInformacao in 'RAguardandoInformacao.pas' {RelAguardandoInformacao: TQuickRep},
  rFechamentoAcompEscolar in 'rFechamentoAcompEscolar.pas' {relFechamentoAcompEscolar: TQuickRep},
  uFechamento in 'uFechamento.pas' {frmFechamento},
  uGeraTotaisDS in 'uGeraTotaisDS.pas' {frmGeraTotaisDS},
  uNovaAvaliacaoAprendiz in 'uNovaAvaliacaoAprendiz.pas' {frmNovaAvaliacaoAprendiz},
  rDesempenhoAprendiz in 'rDesempenhoAprendiz.pas' {relDesempenhoAprendiz: TQuickRep},
  rAcompanhamentoAprendizGeralPorSemestre in 'rAcompanhamentoAprendizGeralPorSemestre.pas' {relAcompanhamentoAprendizGeralPorSemestre: TQuickRep},
  uSelecionaSemAnoDesempAprendiz in 'uSelecionaSemAnoDesempAprendiz.pas' {frmSelecionaSemAnoDesemAprendiz},
  rAICA2 in 'rAICA2.pas' {relAICA2: TQuickRep},
  rAICA in 'rAICA.pas' {relAICA: TQuickRep},
  uEmissaoDeclaracoesSolicitacoesEscolares in 'uEmissaoDeclaracoesSolicitacoesEscolares.pas' {frmEmissaoDeclaracoesSolicitacoesEscolares},
  uSelecionaAnoRAICA in 'uSelecionaAnoRAICA.pas' {frmSelecionaAnoRAICA},
  rNovoDesligamento in 'rNovoDesligamento.pas' {relNovoDesligamento: TQuickRep},
  uSelecionaEmpresaAvDesemp in 'uSelecionaEmpresaAvDesemp.pas' {frmSelecionaEmpresaAvDesemp},
  rEstatisticoEmpresa in 'rEstatisticoEmpresa.pas' {relEstatisticoEmpresa: TQuickRep},
  rAlunosEjaTelessala in 'rAlunosEjaTelessala.pas' {relAlunosEJATelessala: TQuickRep},
  uAlunosEJATelessala in 'uAlunosEJATelessala.pas' {frmAlunosEjaTelessala},
  uEncaminhaNAVE in 'uEncaminhaNAVE.pas' {frmEncaminhaNAVE},
  uRetornoNAVE in 'uRetornoNAVE.pas' {frmRetornoNAVE},
  rEstatisticasFrequencia in 'rEstatisticasFrequencia.pas' {relEstatisticasFrequencia: TQuickRep},
  uDadosEstatisticosFrequencia in 'uDadosEstatisticosFrequencia.pas' {frmDadosEstatisticosFrequencia},
  rNovaFichaCadastro in 'rNovaFichaCadastro.pas' {relNovaFichaCadastro: TQuickRep},
  uTransferenciaDados in 'uTransferenciaDados.pas' {frmTransferenciaDados},
  uImporta in 'uImporta.pas' {frmImporta},
  uAditamentoContratoAprendiz in 'uAditamentoContratoAprendiz.pas' {frmAditamentoContratoAprendiz},
  rAditamentoELEB in 'rAditamentoELEB.pas' {relAditamentoEleb: TQuickRep},
  uComparaBimestres in 'uComparaBimestres.pas' {frmComparaBimestres},
  uFichaCadastroModelo in 'uFichaCadastroModelo.pas' {frmFichaCadastroModelo},
  uMapeamentoXRiscos in 'uMapeamentoXRiscos.pas' {frmMapeamentoXRiscos},
  uMFU in 'uMFU.pas' {frmMfu},
  uNovoCadastroFull in 'uNovoCadastroFull.pas' {frmNovoCadastroFull},
  uEstoqueOdonto in 'uEstoqueOdonto.pas' {frmEstoqueOdonto},
  uTransferenciaColetiva in 'uTransferenciaColetiva.pas' {frmTransferenciaColetiva},
  uREMARCACAO in 'uREMARCACAO.pas' {frmREMARCACAO},
  uSolicitacaoUniformes in 'uSolicitacaoUniformes.pas' {frmSolicitacaoUniformes},
  uDadosRelatorio in 'uDadosRelatorio.pas' {frmDadosRelatorio},
  rDadosAdmitidos_EducaCenso in 'rDadosAdmitidos_EducaCenso.pas' {relDadosAdmitidos_EducaCenso: TQuickRep},
  rDadosTransferidos_educaCenso in 'rDadosTransferidos_educaCenso.pas' {relDadosTransferidos_EducaCenso: TQuickRep},
  rDadosDesligados_EducaCenso in 'rDadosDesligados_EducaCenso.pas' {relDadosDeligados_EducaCenso: TQuickRep},
  rDadosAfastados_EducaCenso in 'rDadosAfastados_EducaCenso.pas' {relDadosAfastados_EducaCenso: TQuickRep},
  uProcessoAdminissionalAprendizes in 'uProcessoAdminissionalAprendizes.pas' {frmProcessoAdminissionalAprendizes},
  rFichaAdmissaoCadastroAprendiz in 'rFichaAdmissaoCadastroAprendiz.pas' {relFichaAdmissaoCadastroAprendiz: TQuickRep},
  uAcesso2 in 'uAcesso2.pas' {frmACESSO2},
  uGerarPedidoUniformes in 'uGerarPedidoUniformes.pas' {frmGerarPedidoUniformes},
  uTermoBolsistas in 'uTermoBolsistas.pas' {frmTermoBolsistas},
  rUniformes in 'rUniformes.pas' {relUniformes: TQuickRep},
  rPedidos in 'rPedidos.pas' {relPedidos: TQuickRep},
  uConsultarPedidosUniformes in 'uConsultarPedidosUniformes.pas' {frmConsultarPedidosUniformes},
  uEmiteComprovanteRecebimentoUniforme in 'uEmiteComprovanteRecebimentoUniforme.pas' {frmEmiteComprovanteRecebimentoUniforme},
  rComprovanteRecebimentoUniformes in 'rComprovanteRecebimentoUniformes.pas' {relComprovanteRecebimentoUniformes: TQuickRep},
  rFichaAdmissaoBolsista in 'rFichaAdmissaoBolsista.pas' {relFichaAdmissaoBolsista: TQuickRep},
  uCadastroUsuarios in 'uCadastroUsuarios.pas' {frmCadastroUsuarios},
  uCadastroValoresPasses in 'uCadastroValoresPasses.pas' {frmCadastroValoresPasses},
  rSeguro in 'rSeguro.pas' {relSeguro: TQuickRep},
  rTotalizaSeguro in 'rTotalizaSeguro.pas' {relTotalizaSeguro: TQuickRep},
  uImportaEducacensoAcompEscolar in 'uImportaEducacensoAcompEscolar.pas' {frmImportaEducacensoAcompEscolar},
  uReativacaoProntuario in 'uReativacaoProntuario.pas' {frmReativacaoProntuario},
  rEstatisticasAbsenteismo in 'rEstatisticasAbsenteismo.pas' {relEstatisticaAbsenteismo: TQuickRep},
  uImportaDocumentos in '..\Imagens\uImportaDocumentos.pas' {frmImportaDocumentos},
  uCadastroFoto in 'uCadastroFoto.pas' {frmCadastroFoto},
  uEstoqueUniformes in 'uEstoqueUniformes.pas' {frmEstoqueUniformes},
  rSolicitacoesUniformes in 'rSolicitacoesUniformes.pas' {relSolicitacoesUniformes: TQuickRep},
  rGerencial_TotalizaUniformes in 'rGerencial_TotalizaUniformes.pas' {relGerencial_TotalizaUniformes: TQuickRep},
  uCursosCertificados in 'uCursosCertificados.pas' {frmCursosCertificados},
  rCertificado_Modelo01 in 'rCertificado_Modelo01.pas' {relCertificado_Modelo01: TQuickRep},
  uEmitirFichaCadastro in 'uEmitirFichaCadastro.pas' {frmEmitirFichaCadastro},
  uFichaCadastroNova in 'uFichaCadastroNova.pas' {relFichaCadastroNova: TQuickRep},
  uConsultaAgendaOdontologica in 'uConsultaAgendaOdontologica.pas' {frmConsultaAgendaOdontologica},
  uMigrarRGeCPF_ComposicaoFamiliar in 'uMigrarRGeCPF_ComposicaoFamiliar.pas' {frmMigrarRGeCPF_ComposicaoFamiliar},
  uControleUnidadesEscolaresTurmasClasses in 'uControleUnidadesEscolaresTurmasClasses.pas' {frmControleUnidadesEscolaresTurmasClasses},
  rEducacenso in 'rEducacenso.pas' {relEducacenso: TQuickRep},
  uEmitirRelatorioDeClasses in 'uEmitirRelatorioDeClasses.pas' {frmEmitirRelatorioDeClasses},
  uConsultaMudancasVinculo in 'uConsultaMudancasVinculo.pas' {frmConsultaMudancasVinculo},
  uNavegador2 in 'uNavegador2.pas' {frmNavegador2},
  uConfereDadosEmissaoContrato in 'uConfereDadosEmissaoContrato.pas' {frmConfereDadosEmissaoContrato},
  uFichaCadastroNovo in 'uFichaCadastroNovo.pas' {frmFichaCadastroNovo},
  uEmiteContratoAdmissao in 'uEmiteContratoAdmissao.pas' {frmEmiteContratoAdmissao},
  uConsultaReativacoesProntuarios in 'uConsultaReativacoesProntuarios.pas' {frmConsultaReativacoesProntuarios},
  uCadastroInscricoes in 'uCadastroInscricoes.pas' {frmCadastroInscricoes},
  uConsultaDemandaTriagemTST in 'uConsultaDemandaTriagemTST.pas' {frmConsultaDemandaTriagemTST},
  rLevantametnoAtendidosPorPeriodo in 'rLevantametnoAtendidosPorPeriodo.pas' {relLevantamentoAtendidosPorPeriodo: TQuickRep},
  uOpcaoRelatorioEstatisticasAcEscolar in 'uOpcaoRelatorioEstatisticasAcEscolar.pas' {frmOpcaoRelatorioEstatisticasAcEscolar},
  uCadBairrosTriagem in 'uCadBairrosTriagem.pas' {frmCadBairrosTriagem},
  udmTriagemDeca in 'udmTriagemDeca.pas' {dmTriagemDeca: TDataModule},
  uPESQUISA_INSCRITOS_TRIAGEM in 'uPESQUISA_INSCRITOS_TRIAGEM.pas' {frmPESQUISA_INSCRITOS_TRIAGEM},
  rINFORMACOES_INSCRICAO_RENOVACAO in 'rINFORMACOES_INSCRICAO_RENOVACAO.pas' {relINFOrMACOES_INSCRICAO_RENOVACAO: TQuickRep},
  uMigrarAtendimentosParaRelatorios in 'uMigrarAtendimentosParaRelatorios.pas' {frmMigrarAtendimentosParaRelatorios},
  ufrmCorrecaoUsuariosRelatorios in 'ufrmCorrecaoUsuariosRelatorios.pas' {frmCorrecaoUsuariosRelatorios},
  uCORRIGE_CADASTROTRIAGEM in 'uCORRIGE_CADASTROTRIAGEM.pas' {frmCORRIGE_CADASTROTRIAGEM},
  uSELECIONA_DEMANDA in 'uSELECIONA_DEMANDA.pas' {frmSELECIONA_DEMANDA},
  uADMISSOES in 'uADMISSOES.pas' {frmADMISSOES},
  rDemanda in 'rDemanda.pas' {relDemanda: TQuickRep},
  uGERA_DEMANDA in 'uGERA_DEMANDA.pas' {frmGERA_DEMANDA},
  rCONTRATO in 'rCONTRATO.pas' {relCONTRATO: TQuickRep},
  rFICHA_ADMISSAO in 'rFICHA_ADMISSAO.pas' {relFICHA_ADMISSAO: TQuickRep},
  uSEGUNDA_VIA_CONTRATO in 'uSEGUNDA_VIA_CONTRATO.pas' {frmSEGUNDA_VIA_CONTRATO};

//uEncaminhamentosEspecificos in 'uEncaminhamentosEspecificos.pas' {frmEncaminhamentosEspecificos};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Sistema Deca';
  Application.CreateForm(TdmDeca, dmDeca);
  Application.CreateForm(TfrmACESSO2, frmACESSO2);
  Application.CreateForm(TdmTriagemDeca, dmTriagemDeca);
  frmSplash.Free;
  Application.Run;
end.
