unit uNovoControleFrequencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, Buttons, StdCtrls, Mask, Db, ADODB, DBGrids, ComObj;

type
  TfrmNovoControleFrequencia = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    btnLocalizar: TSpeedButton;
    GroupBox3: TGroupBox;
    Panel1: TPanel;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    dbgFrequencias: TDBGrid;
    dsCadastroFrequencia: TDataSource;
    btnSair: TSpeedButton;
    btnGravar: TSpeedButton;
    btnCancelar: TSpeedButton;
    GroupBox4: TGroupBox;
    cbUnidade: TComboBox;
    chkTodas: TCheckBox;
    Panel2: TPanel;
    Bevel1: TBevel;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox5: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    edDia01: TEdit;
    edDia02: TEdit;
    edDia03: TEdit;
    edDia04: TEdit;
    edDia05: TEdit;
    edDia06: TEdit;
    edDia07: TEdit;
    edDia08: TEdit;
    edDia09: TEdit;
    edDia10: TEdit;
    edDia11: TEdit;
    edDia12: TEdit;
    edDia13: TEdit;
    edDia14: TEdit;
    edDia15: TEdit;
    edDia16: TEdit;
    edDia17: TEdit;
    edDia18: TEdit;
    edDia19: TEdit;
    edDia20: TEdit;
    edDia21: TEdit;
    edDia22: TEdit;
    edDia23: TEdit;
    edDia24: TEdit;
    edDia25: TEdit;
    edDia26: TEdit;
    edDia27: TEdit;
    edDia28: TEdit;
    edDia29: TEdit;
    edDia30: TEdit;
    edDia31: TEdit;
    lbIdentificacao: TLabel;
    btnLancarFrequencia: TSpeedButton;
    btnSair2: TSpeedButton;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    GroupBox12: TGroupBox;
    lbDiasUteis: TLabel;
    lbFeriados: TLabel;
    lbCompensados: TLabel;
    lbSabados: TLabel;
    lbDomingos: TLabel;
    GroupBox13: TGroupBox;
    lbPresencas: TLabel;
    btnVerImpressao: TSpeedButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    GroupBox10: TGroupBox;
    edFaltas: TEdit;
    GroupBox11: TGroupBox;
    edDSR: TEdit;
    GroupBox14: TGroupBox;
    edJustificados: TEdit;
    GroupBox15: TGroupBox;
    GroupBox16: TGroupBox;
    edComVenc: TEdit;
    edSemVenc: TEdit;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    btnGerarPlanilha: TSpeedButton;
    SaveDialog1: TSaveDialog;
    btnImportarFrequencia: TSpeedButton;
    OpenDialog1: TOpenDialog;
    btnEstatisticas: TSpeedButton;
    btnGeraCSV: TSpeedButton;
    procedure btnLocalizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkTodasClick(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure dbgFrequenciasDblClick(Sender: TObject);
    procedure btnLancarFrequenciaClick(Sender: TObject);
    procedure btnSair2Click(Sender: TObject);
    function CalculaDiasUteis (DataInicial, DataFinal : String): Integer;
    procedure AtualizaFaltas;
    procedure AtualizaFaltasCLT;
    function SemanaDoMes(Date:TDateTime):Integer;
    procedure btnVerImpressaoClick(Sender: TObject);
    procedure AtualizaFaltasCr;
    procedure edDia01Exit(Sender: TObject);
    procedure edDia02Exit(Sender: TObject);
    procedure edDia03Exit(Sender: TObject);
    procedure edDia04Exit(Sender: TObject);
    procedure edDia05Exit(Sender: TObject);
    procedure edDia06Exit(Sender: TObject);
    procedure edDia07Exit(Sender: TObject);
    procedure edDia08Exit(Sender: TObject);
    procedure edDia09Exit(Sender: TObject);
    procedure edDia10Exit(Sender: TObject);
    procedure edDia11Exit(Sender: TObject);
    procedure edDia12Exit(Sender: TObject);
    procedure edDia13Exit(Sender: TObject);
    procedure edDia14Exit(Sender: TObject);
    procedure edDia15Exit(Sender: TObject);
    procedure edDia16Exit(Sender: TObject);
    procedure edDia17Exit(Sender: TObject);
    procedure edDia18Exit(Sender: TObject);
    procedure edDia19Exit(Sender: TObject);
    procedure edDia20Exit(Sender: TObject);
    procedure edDia21Exit(Sender: TObject);
    procedure edDia22Exit(Sender: TObject);
    procedure edDia23Exit(Sender: TObject);
    procedure edDia24Exit(Sender: TObject);
    procedure edDia25Exit(Sender: TObject);
    procedure edDia26Exit(Sender: TObject);
    procedure edDia27Exit(Sender: TObject);
    procedure edDia28Exit(Sender: TObject);
    procedure edDia29Exit(Sender: TObject);
    procedure edDia30Exit(Sender: TObject);
    procedure edDia31Exit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGerarPlanilhaClick(Sender: TObject);
    procedure btnImportarFrequenciaClick(Sender: TObject);
    procedure btnEstatisticasClick(Sender: TObject);
    procedure btnGeraCSVClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovoControleFrequencia: TfrmNovoControleFrequencia;
  vListaUnidade1, vListaUnidade2 : TStringList;

  strMES, strMES1, mmDATA_INICIO_MES, mmDATA_FINAL_MES, mmDATA_ATUAL,
  mmNOME,mmMATRICULA, mmPERIODO : String;     //Essa linha foi adicionada para gerar a importa��o dos dados da planilha....

  mmTOTAL_DIAS_UTEIS, mmTOTAL_FALTAS, sl, mmSEMANA,mmDIAS_UTEIS : Integer;

  total_faltas_mes,

  total_faltas_semana1, total_feriados_semana1,
  total_faltas_semana2, total_feriados_semana2,
  total_faltas_semana3, total_feriados_semana3,
  total_faltas_semana4, total_feriados_semana4,
  total_faltas_semana5, total_feriados_semana5,
  total_faltas_semana6, total_feriados_semana6, col : Integer;


  total_feriados_mes,
  total_domingos_mes,
  total_sabados_mes,
  total_compensados_mes,
  total_presencas_mes,
  total_justif_mes : Integer;

  total_feriados_sem1,
  total_feriados_sem2,
  total_feriados_sem3,
  total_feriados_sem4,
  total_feriados_sem5,
  total_feriados_sem6 : Integer;

  total_dsr_mes,
  total_dsr_proximo_mes,
  total_dsr_semana1,
  total_dsr_semana2,
  total_dsr_semana3,
  total_dsr_semana4,
  total_dsr_semana5,
  total_dsr_semana6,
  total_dsr_acumulado,
  total_comvenc,
  total_semvenc : Integer;

  mmMES_SEGUINTE, mmANO_SEGUINTE, mmSEMANA_PRIMEIRO_DIA_MES_SEGUINTE : Integer;

  mmPRIMEIRO_DIA_MES_SEGUINTE : String;

  objExcel, planilhaFreq : Variant;

  //planilhaFreq, sheetFreq : OleVariant;
  sheetFreq : OleVariant;
  linhas, linha, coluna, mmMES, num_semanas01 : Integer;
  vv_STRDATA, str_data_inicial0, mmFORMULA : String;
  vv_DATA, data_inicial0, data_final0 : TDate;


implementation

uses uPrincipal, uDM, uSelecionarDadosFrequencia, rFolhaFrequencia, uUtil,
  rEstatisticasAbsenteismo;

{$R *.DFM}

procedure TfrmNovoControleFrequencia.btnLocalizarClick(Sender: TObject);
var x, col, vIndex : Integer;
begin
  try

    //Executa a Query -> qryConsultaAlunosUnidade passando como par�metros a unidade, o ano e o m�s para sele��o...
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').Value   := Null;
      Params.ParamByname ('@pe_cod_matricula').Value   := Null;

      if (vvIND_PERFIL in [1,8]) and (chkTodas.Checked) then  //Administrador e DRH
        Params.ParamByName ('@pe_cod_unidade').Value   := Null
      else if (vvIND_PERFIL in [1,8]) and (not (chkTodas.Checked)) then  //Administrador e DRH
        Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex])
      else
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;

      Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_num_mes').Value         := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Params.ParamByName ('@pe_cod_turma').Value       := Null;
      Params.ParamByName ('@pe_num_cotas').Value       := Null;

      Open;

      if (dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0) then
      begin

        //Carrega os dados para o dbgrid
        dbgFrequencias.Refresh;

        with dbgFrequencias do
        begin
          for col := 3 to 35 do
          begin
            if (Columns[col].Field.Text = 'S') or (Columns[col].Field.Text = 'D') then
            begin
              Columns[col].ReadOnly := True;
              Columns[col].Color := clRed;
            end
            else if (Columns[col].Field.Text = 'R') or (Columns[col].Field.Text = 'C') then
            begin
              Columns[col].ReadOnly := True;
              Columns[col].Color := clYellow;
            end;
            //Se for unidade PAE liberar os s�bados para preenchimento
            //else if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) then
            //begin
            //  Columns[col].ReadOnly := False;
            //  Columns[col].Color := clYellow;
            //end
          end;
        end;
        
      end
      else  //Caso n�o exista nenhum aluno com frequencia no per�odo informado...
      begin
        Application.Messagebox('Aten��o!!!' + #13+#10 +
                               'N�o h� informa��es de frequ�ncia lan�ada no per�odo para a unidade.',
                               '[Sistema Deca] - Novo Controle de Frequ�ncia',
                               MB_OK + MB_ICONINFORMATION);
      end;

    end;

  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar os dados da consulta. ' + #13+#10 +
                            'Favor verificar conex�o, cabos de rede e se o erro persistir ' + #1+#10 +
                            'entre em contato com o Administrador do Sistema.',
                            '[Sistema Deca] - Erro Novo Controle de Frequ�ncia',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmNovoControleFrequencia.FormShow(Sender: TObject);
var c, l, x, y: Integer;
begin

  mskAno.Text := Copy(DateToStr(Date()),7,4);


  Panel2.Visible := False;
  //Deixar somente leitura as colunas de 0 a 4...
  with dbgFrequencias do
  begin

    for c := 0 to 4 do
    begin
      Columns[c].ReadOnly := True;
      Columns[c].Color := clAqua;
    end;

    //Desabilitar as celulas aonde est�o preenchidos os valores:
    //  S -> S�bado
    //  D -> Domingo
    //  R -> Feriado
    //  C -> Compensado
    //Ler a partir da 6� coluna
  end;

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de Unidades para uma poss�vel consulta
  //aos dados da frequencia de determinada unidade...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').AsInteger in [1,3,7,9,12,13,40,42,43,47,48,50,71]) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;



  //Se o perfil for 1-Administrador ou 8-DRH, exibe a rela��o de Unidades para consulta...
  if vvIND_PERFIL in [1,7,8,20,23,24,25] then
  begin
    GroupBox4.Visible := True;
    cbUnidade.ItemIndex := -1;
  end
  else
  begin
    GroupBox4.Visible := False;
  end;
end;

procedure TfrmNovoControleFrequencia.chkTodasClick(Sender: TObject);
begin
  if chkTodas.Checked then
  begin
    cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := False
  end
  else
  begin
    cbUnidade.Enabled := True;
  end;
end;

procedure TfrmNovoControleFrequencia.cbUnidadeClick(Sender: TObject);
begin
  vvCOD_UNIDADE := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
end;

procedure TfrmNovoControleFrequencia.btnSairClick(Sender: TObject);
begin
  frmNovoControleFrequencia.Close;
end;

procedure TfrmNovoControleFrequencia.dbgFrequenciasDblClick(
  Sender: TObject);
begin

  GroupBox10.Visible := False;
  GroupBox11.Visible := False;

  //****************************************************************************
  //Conte�do transferido para uma procedure chamada "AtualizaFaltas"...
  //****************************************************************************

  //****************************************************************************
  //Calcula a quantidade de dias �teis no m�s do lan�amento da frequ�ncia...
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value < 10) then
    strMES := '0' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value)
  else
    strMES := IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value);

  mmDATA_INICIO_MES := '01/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  if StrToInt(strMES) in [1,3,5,7,8,10,12] then
  begin
    mmDATA_FINAL_MES  := '31/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  end
  else if StrToInt(strMES) in [4,6,9,11] then
  begin
    mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);
    mmDIAS_UTEIS := -1
  end
  else
    //strMES = '02' then
    mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_EXPORTOU').Value = 0) then
  begin

    Panel2.Visible := True;
    //Carregar os dados de Identifica��o para o "Cabe�alho" do Panel2
    lbIdentificacao.Caption := 'ALUNO: ' + dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').Value +
                               ' - ' + UpperCase(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('Nome').Value) +
                               '    ' + dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('nom_unidade').Value;

    GroupBox5.Caption := 'Refer�ncia :' + cbMes.Text + ' de ' + mskAno.Text;

    //**************************************************************************
    //Dia 01
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
        edDia01.Enabled := False;
        edDia01.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'S') then
      begin
        edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
        edDia01.Enabled := True;
        edDia01.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'R') then
      begin
        edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
        edDia01.Enabled := False;
        edDia01.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'S') then
      begin
        edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
        edDia01.Enabled := True;
        edDia01.Font.Color := clRed;
      end;

    end
    else 
    begin
      edDia01.Enabled := True;
      edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
      edDia01.Font.Color := clBlack;
    end;

    //**************************************************************************
    //Dia 02
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
        edDia02.Enabled := False;
        edDia02.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'S') then
      begin
        edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
        edDia02.Enabled := True;
        edDia02.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'R') then
      begin
        edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
        edDia02.Enabled := False;
        edDia02.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'S') then
      begin
        edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
        edDia02.Enabled := True;
        edDia02.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia02.Enabled := True;
      edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
      edDia02.Font.Color := clBlack;
    end;

    //**************************************************************************
    //Dia 03
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
        edDia03.Enabled := False;
        edDia03.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'S') then
      begin
        edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
        edDia03.Enabled := True;
        edDia03.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'R') then
      begin
        edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
        edDia03.Enabled := False;
        edDia03.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'S') then
      begin
        edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
        edDia03.Enabled := True;
        edDia03.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia03.Enabled := True;
      edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
      edDia03.Font.Color := clBlack;
    end;

    //**************************************************************************
    //Dia 04
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
        edDia04.Enabled := False;
        edDia04.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'S') then
      begin
        edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
        edDia04.Enabled := True;
        edDia04.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'R') then
      begin
        edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
        edDia04.Enabled := False;
        edDia04.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'S') then
      begin
        edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
        edDia04.Enabled := True;
        edDia04.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia04.Enabled := True;
      edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
      edDia04.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 05
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
        edDia05.Enabled := False;
        edDia05.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'S') then
      begin
        edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
        edDia05.Enabled := True;
        edDia05.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'R') then
      begin
        edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
        edDia05.Enabled := False;
        edDia05.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'S') then
      begin
        edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
        edDia05.Enabled := True;
        edDia05.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia05.Enabled := True;
      edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
      edDia05.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 06
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
        edDia06.Enabled := False;
        edDia06.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'S') then
      begin
        edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
        edDia06.Enabled := True;
        edDia06.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'R') then
      begin
        edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
        edDia06.Enabled := False;
        edDia06.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'S') then
      begin
        edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
        edDia06.Enabled := True;
        edDia06.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia06.Enabled := True;
      edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
      edDia06.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 07
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
        edDia07.Enabled := False;
        edDia07.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'S') then
      begin
        edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
        edDia07.Enabled := True;
        edDia07.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'R') then
      begin
        edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
        edDia07.Enabled := False;
        edDia07.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'S') then
      begin
        edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
        edDia07.Enabled := True;
        edDia07.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia07.Enabled := True;
      edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
      edDia07.Font.Color := clBlack;
    end;

    //**************************************************************************
    //Dia 08
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
        edDia08.Enabled := False;
        edDia08.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'S') then
      begin
        edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
        edDia08.Enabled := True;
        edDia08.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'R') then
      begin
        edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
        edDia08.Enabled := False;
        edDia08.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'S') then
      begin
        edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
        edDia08.Enabled := True;
        edDia08.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia08.Enabled := True;
      edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
      edDia08.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 09
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
        edDia09.Enabled := False;
        edDia09.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'S') then
      begin
        edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
        edDia09.Enabled := True;
        edDia09.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'R') then
      begin
        edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
        edDia09.Enabled := False;
        edDia09.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'S') then
      begin
        edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
        edDia09.Enabled := True;
        edDia09.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia09.Enabled := True;
      edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
      edDia09.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 10
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
        edDia10.Enabled := False;
        edDia10.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'S') then
      begin
        edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
        edDia10.Enabled := True;
        edDia10.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'R') then
      begin
        edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
        edDia10.Enabled := False;
        edDia10.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'S') then
      begin
        edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
        edDia10.Enabled := True;
        edDia10.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia10.Enabled := True;
      edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
      edDia10.Font.Color := clBlack;
    end;

    //**************************************************************************
    //Dia 11
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
        edDia11.Enabled := False;
        edDia11.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'S') then
      begin
        edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
        edDia11.Enabled := True;
        edDia11.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'R') then
      begin
        edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
        edDia11.Enabled := False;
        edDia11.Font.Color := clRed;
      end;

     //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'S') then
      begin
        edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
        edDia11.Enabled := True;
        edDia11.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia11.Enabled := True;
      edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
      edDia11.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 12
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
        edDia12.Enabled := False;
        edDia12.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'S') then
      begin
        edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
        edDia12.Enabled := True;
        edDia12.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'R') then
      begin
        edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
        edDia12.Enabled := False;
        edDia12.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'S') then
      begin
        edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
        edDia12.Enabled := True;
        edDia12.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia12.Enabled := True;
      edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
      edDia12.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 13
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
        edDia13.Enabled := False;
        edDia13.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'S') then
      begin
        edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
        edDia13.Enabled := True;
        edDia13.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'R') then
      begin
        edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
        edDia13.Enabled := False;
        edDia13.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'S') then
      begin
        edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
        edDia13.Enabled := True;
        edDia13.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia13.Enabled := True;
      edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
      edDia13.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 14
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
        edDia14.Enabled := False;
        edDia14.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'S') then
      begin
        edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
        edDia14.Enabled := True;
        edDia14.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'R') then
      begin
        edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
        edDia14.Enabled := True;
        edDia14.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'S') then
      begin
        edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
        edDia14.Enabled := False;
        edDia14.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia14.Enabled := True;
      edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
      edDia14.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 15
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
        edDia15.Enabled := False;
        edDia15.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'S') then
      begin
        edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
        edDia15.Enabled := True;
        edDia15.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'R') then
      begin
        edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
        edDia15.Enabled := False;
        edDia15.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'S') then
      begin
        edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
        edDia15.Enabled := True;
        edDia15.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia15.Enabled := True;
      edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
      edDia15.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 16
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
        edDia16.Enabled := False;
        edDia16.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'S') then
      begin
        edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
        edDia16.Enabled := True;
        edDia16.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'R') then
      begin
        edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
        edDia16.Enabled := False;
        edDia16.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'S') then
      begin
        edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
        edDia16.Enabled := True;
        edDia16.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia16.Enabled := True;
      edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
      edDia16.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 17
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
        edDia17.Enabled := False;
        edDia17.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'S') then
      begin
        edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
        edDia17.Enabled := True;
        edDia17.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'R') then
      begin
        edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
        edDia17.Enabled := False;
        edDia17.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'S') then
      begin
        edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
        edDia17.Enabled := True;
        edDia17.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia17.Enabled := True;
      edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
      edDia17.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 18
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
        edDia18.Enabled := False;
        edDia18.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'S') then
      begin
        edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
        edDia18.Enabled := True;
        edDia18.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'R') then
      begin
        edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
        edDia18.Enabled := False;
        edDia18.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'S') then
      begin
        edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
        edDia18.Enabled := True;
        edDia18.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia18.Enabled := True;
      edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
      edDia18.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 19
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
        edDia19.Enabled := False;
        edDia19.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'S') then
      begin
        edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
        edDia19.Enabled := True;
        edDia19.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'R') then
      begin
        edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
        edDia19.Enabled := False;
        edDia19.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'S') then
      begin
        edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
        edDia19.Enabled := True;
        edDia19.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia19.Enabled := True;
      edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
      edDia19.Font.Color := clBlack;
    end;

    //**************************************************************************
    //Dia 20
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
        edDia20.Enabled := False;
        edDia20.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'S') then
      begin
        edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
        edDia20.Enabled := True;
        edDia20.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'R') then
      begin
        edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
        edDia20.Enabled := False;
        edDia20.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'S') then
      begin
        edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
        edDia20.Enabled := True;
        edDia20.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia20.Enabled := True;
      edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
      edDia20.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 21
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
        edDia21.Enabled := False;
        edDia21.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'S') then
      begin
        edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
        edDia21.Enabled := True;
        edDia21.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'R') then
      begin
        edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
        edDia21.Enabled := False;
        edDia21.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'S') then
      begin
        edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
        edDia21.Enabled := True;
        edDia21.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia21.Enabled := True;
      edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
      edDia21.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 22
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
        edDia22.Enabled := False;
        edDia22.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'S') then
      begin
        edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
        edDia22.Enabled := True;
        edDia22.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'R') then
      begin
        edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
        edDia22.Enabled := False;
        edDia22.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'S') then
      begin
        edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
        edDia22.Enabled := True;
        edDia22.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia22.Enabled := True;
      edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
      edDia22.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 23
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
        edDia23.Enabled := False;
        edDia23.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'S') then
      begin
        edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
        edDia23.Enabled := True;
        edDia23.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'R') then
      begin
        edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
        edDia23.Enabled := False;
        edDia23.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'S') then
      begin
        edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
        edDia23.Enabled := True;
        edDia23.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia23.Enabled := True;
      edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
      edDia23.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 24
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
        edDia24.Enabled := False;
        edDia24.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'S') then
      begin
        edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
        edDia24.Enabled := True;
        edDia24.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'R') then
      begin
        edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
        edDia24.Enabled := False;
        edDia24.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'S') then
      begin
        edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
        edDia24.Enabled := True;
        edDia24.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia24.Enabled := True;
      edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
      edDia24.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 25
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
        edDia25.Enabled := False;
        edDia25.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'S') then
      begin
        edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
        edDia25.Enabled := True;
        edDia25.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'R') then
      begin
        edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
        edDia25.Enabled := False;
        edDia25.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'S') then
      begin
        edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
        edDia25.Enabled := True;
        edDia25.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia25.Enabled := True;
      edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
      edDia25.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 26
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
        edDia26.Enabled := False;
        edDia26.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'S') then
      begin
        edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
        edDia26.Enabled := True;
        edDia26.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'R') then
      begin
        edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
        edDia26.Enabled := False;
        edDia26.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'S') then
      begin
        edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
        edDia26.Enabled := True;
        edDia26.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia26.Enabled := True;
      edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
      edDia26.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 27
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
        edDia27.Enabled := False;
        edDia27.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'S') then
      begin
        edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
        edDia27.Enabled := True;
        edDia27.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'R') then
      begin
        edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
        edDia27.Enabled := False;
        edDia27.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'S') then
      begin
        edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
        edDia27.Enabled := True;
        edDia27.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia27.Enabled := True;
      edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
      edDia27.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 28
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
        edDia28.Enabled := False;
        edDia28.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'S') then
      begin
        edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
        edDia28.Enabled := True;
        edDia28.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'R') then
      begin
        edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
        edDia28.Enabled := False;
        edDia28.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'S') then
      begin
        edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
        edDia28.Enabled := True;
        edDia28.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia28.Enabled := True;
      edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
      edDia28.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 29
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
        edDia29.Enabled := False;
        edDia29.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'S') then
      begin
        edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
        edDia29.Enabled := True;
        edDia29.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'R') then
      begin
        edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
        edDia29.Enabled := False;
        edDia29.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'S') then
      begin
        edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
        edDia29.Enabled := True;
        edDia29.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia29.Enabled := True;
      edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
      edDia29.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 30
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
        edDia30.Enabled := False;
        edDia30.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'S') then
      begin
        edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
        edDia30.Enabled := True;
        edDia30.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'R') then
      begin
        edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
        edDia30.Enabled := False;
        edDia30.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'S') then
      begin
        edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
        edDia30.Enabled := True;
        edDia30.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia30.Enabled := True;
      edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
      edDia30.Font.Color := clBlack;
    end;


    //**************************************************************************
    //Dia 31
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').Value = Null) then
    begin
      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) <> '006') then
      begin
        edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
        edDia31.Enabled := False;
        edDia31.Font.Color := clRed;
      end
      else if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3) = '006') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'S') then
      begin
        edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
        edDia31.Enabled := True;
        edDia31.Font.Color := clRed;
      end;

      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'D') or (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'R') then
      begin
        edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
        edDia31.Enabled := False;
        edDia31.Font.Color := clRed;
      end;

      //Se for unidade PAE liberar os s�bados para preenchimento
      if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_unidade').Value = 85) and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'S') then
      begin
        edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
        edDia31.Enabled := True;
        edDia31.Font.Color := clRed;
      end;

    end
    else
    begin
      edDia31.Enabled := True;
      edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
      edDia31.Font.Color := clBlack;
    end;


    //**************************************************************************
    //N�mero de Faltas
    //edFaltas.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').Value;

    //**************************************************************************
    //DSR Acumulado
    //edDSR.Text    := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').Value;
    

    //*****************************************************************************
    //Criar procedimento/fun��o na aplica��o...
    //Criar rotina para calcular total_faltas_m, igual ao exemplo acima
    total_faltas_mes      := 0;
    total_feriados_mes    := 0;
    total_domingos_mes    := 0;
    total_sabados_mes     := 0;
    total_compensados_mes := 0;
    total_presencas_mes   := 0;

    //DIA 01********************************************************************
    if (edDia01.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia01.Text = 'P') or (edDia01.Text = 'J') or (edDia01.Text = 'X') or (edDia01.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia01.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia01.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia01.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia01.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 02********************************************************************
    if (edDia02.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia02.Text = 'P') or (edDia02.Text = 'J') or (edDia02.Text = 'X') or (edDia02.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia02.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia02.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia02.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia02.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 03********************************************************************
    if (edDia03.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia03.Text = 'P') or (edDia03.Text = 'J') or (edDia03.Text = 'X') or (edDia03.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia03.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia03.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia03.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia03.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 04********************************************************************
    if (edDia04.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia04.Text = 'P') or (edDia04.Text = 'J') or (edDia04.Text = 'X') or (edDia04.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia04.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia04.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia04.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia04.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 05********************************************************************
    if (edDia05.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia05.Text = 'P') or (edDia05.Text = 'J') or (edDia05.Text = 'X') or (edDia05.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia05.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia05.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia05.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia05.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 06********************************************************************
    if (edDia06.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia06.Text = 'P') or (edDia06.Text = 'J') or (edDia06.Text = 'X') or (edDia06.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia06.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia06.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia06.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia06.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 07********************************************************************
    if (edDia07.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia07.Text = 'P') or (edDia07.Text = 'J') or (edDia07.Text = 'X') or (edDia07.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia07.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia07.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia07.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia07.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 08********************************************************************
    if (edDia08.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia08.Text = 'P') or (edDia08.Text = 'J') or (edDia08.Text = 'X') or (edDia08.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia08.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia08.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia08.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia08.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 09********************************************************************
    if (edDia09.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia09.Text = 'P') or (edDia09.Text = 'J') or (edDia09.Text = 'X') or (edDia09.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia09.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia09.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia09.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia09.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 10********************************************************************
    if (edDia10.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia10.Text = 'P') or (edDia10.Text = 'J') or (edDia10.Text = 'X') or (edDia10.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia10.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia10.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia10.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia10.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 11********************************************************************
    if (edDia11.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia11.Text = 'P') or (edDia11.Text = 'J') or (edDia11.Text = 'X') or (edDia11.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia11.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia11.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia11.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia11.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 12********************************************************************
    if (edDia12.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia12.Text = 'P') or (edDia12.Text = 'J') or (edDia12.Text = 'X') or (edDia12.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia12.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia12.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia12.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia12.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 13********************************************************************
    if (edDia13.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia13.Text = 'P') or (edDia13.Text = 'J') or (edDia13.Text = 'X') or (edDia13.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia13.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia13.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia13.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia13.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 14********************************************************************
    if (edDia14.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia14.Text = 'P') or (edDia14.Text = 'J') or (edDia14.Text = 'X') or (edDia14.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia14.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia14.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia14.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia14.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 15********************************************************************
    if (edDia15.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia15.Text = 'P') or (edDia15.Text = 'J') or (edDia15.Text = 'X') or (edDia15.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia15.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia15.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia15.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia15.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 16********************************************************************
    if (edDia16.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia16.Text = 'P') or (edDia16.Text = 'J') or (edDia16.Text = 'X') or (edDia16.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia16.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia16.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia16.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia16.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 17********************************************************************
    if (edDia17.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia17.Text = 'P') or (edDia17.Text = 'J') or (edDia17.Text = 'X') or (edDia17.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia17.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia17.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia17.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia17.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 18********************************************************************
    if (edDia18.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia18.Text = 'P') or (edDia18.Text = 'J') or (edDia18.Text = 'X') or (edDia18.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia18.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia18.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia18.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia18.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 19********************************************************************
    if (edDia19.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia19.Text = 'P') or (edDia19.Text = 'J') or (edDia19.Text = 'X') or (edDia19.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia19.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia19.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia19.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia19.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 20********************************************************************
    if (edDia20.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia20.Text = 'P') or (edDia20.Text = 'J') or (edDia20.Text = 'X') or (edDia20.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia20.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia20.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia20.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia20.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 21********************************************************************
    if (edDia21.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia21.Text = 'P') or (edDia21.Text = 'J') or (edDia21.Text = 'X') or (edDia21.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia21.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia21.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia21.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia21.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 22********************************************************************
    if (edDia22.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia22.Text = 'P') or (edDia22.Text = 'J') or (edDia22.Text = 'X') or (edDia22.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia22.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia22.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia22.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia22.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 23********************************************************************
    if (edDia23.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia23.Text = 'P') or (edDia23.Text = 'J') or (edDia23.Text = 'X') or (edDia23.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia23.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia23.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia23.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia23.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 24********************************************************************
    if (edDia24.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia24.Text = 'P') or (edDia24.Text = 'J') or (edDia24.Text = 'X') or (edDia24.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia24.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia24.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia24.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia24.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;
    
    //DIA 25********************************************************************
    if (edDia25.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia25.Text = 'P') or (edDia25.Text = 'J') or (edDia25.Text = 'X') or (edDia25.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia25.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia25.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia25.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia25.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 26********************************************************************
    if (edDia26.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia26.Text = 'P') or (edDia26.Text = 'J') or (edDia26.Text = 'X') or (edDia26.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia26.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia26.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia26.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia26.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 27********************************************************************
    if (edDia27.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia27.Text = 'P') or (edDia27.Text = 'J') or (edDia27.Text = 'X') or (edDia27.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia27.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia27.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia27.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia27.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 28********************************************************************
    if (edDia28.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia28.Text = 'P') or (edDia28.Text = 'J') or (edDia28.Text = 'X') or (edDia28.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia28.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia28.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia28.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia28.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 29********************************************************************
    if (edDia29.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia29.Text = 'P') or (edDia29.Text = 'J') or (edDia29.Text = 'X') or (edDia29.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia29.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia29.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia29.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia29.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 30********************************************************************
    if (edDia30.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia30.Text = 'P') or (edDia30.Text = 'J') or (edDia30.Text = 'X') or (edDia30.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia30.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia30.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia30.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia30.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    //DIA 31********************************************************************
    if (edDia31.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia31.Text = 'P') or (edDia31.Text = 'J') or (edDia31.Text = 'X') or (edDia31.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia31.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia31.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia31.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia31.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1;

    lbFeriados.Caption    := IntToStr(total_feriados_mes);
    lbCompensados.Caption := IntToStr(total_compensados_mes);
    lbSabados.Caption     := IntToStr(total_sabados_mes);
    lbDomingos.Caption    := IntToStr(total_domingos_mes);
    lbPresencas.Caption   := IntToStr(total_presencas_mes);
    edFaltas.Text         := IntToStr(total_faltas_mes);

    GroupBox10.Visible := False;
    GroupBox11.Visible := False;


    with dbgFrequencias do
    begin
      for col := 5 to 35 do
      begin
        if (Columns[col].Field.Text = 'S') or (Columns[col].Field.Text = 'D') then
        begin
          Columns[col].ReadOnly := True;
          Columns[col].Color := clRed;
        end
        else if (Columns[col].Field.Text = 'R') or (Columns[col].Field.Text = 'C') then
        begin
          Columns[col].ReadOnly := True;
          Columns[col].Color := clYellow;
        end;
      end;
    end;
    

  end
  else
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Esses dados j� foram exportados e n�o podem ser alterados.',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    Panel2.Visible := False;
  end;


end;

procedure TfrmNovoControleFrequencia.btnLancarFrequenciaClick(
  Sender: TObject);
begin

  //Pede confirma��o do usu�rio

  if Application.MessageBox ('Deseja lan�ar as altera��es da frequ�ncia do(a) adolescente?' ,
                             '[Sistema Deca] - Inclus�o de Lan�amento de Frequ�ncia',
                             MB_YESNO + MB_ICONQUESTION) = id_yes then
  begin

    try

      //Procedimento para gravar as altera��es de frequ�ncias
      //do(a) adolescente atual...

       case StrToInt(Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)) of
        //2: AtualizaFaltasCr;    //Folha 002  //Descontar apenas faltas sobre os dias �teis
        002,003,999,777: AtualizaFaltas;    //Folha 003
        //999: AtualizaFaltasCr;  //Peti  999
        006: AtualizaFaltasCLT; //Folha 006
      end;

    except end;

    //Ap�s gravar altera��es de frequ�ncias, oculta o Panel2
    try
      with dmDeca.cdsAlt_Cadastro_Frequencia do
      begin
        Close;
        Params.ParamByName ('@pe_id_frequencia').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('id_frequencia').AsInteger;
        Params.ParamByName ('@pe_dia01').Value         := GetValue(edDia01.Text);
        Params.ParamByName ('@pe_dia02').Value         := GetValue(edDia02.Text);
        Params.ParamByName ('@pe_dia03').Value         := GetValue(edDia03.Text);
        Params.ParamByName ('@pe_dia04').Value         := GetValue(edDia04.Text);
        Params.ParamByName ('@pe_dia05').Value         := GetValue(edDia05.Text);
        Params.ParamByName ('@pe_dia06').Value         := GetValue(edDia06.Text);
        Params.ParamByName ('@pe_dia07').Value         := GetValue(edDia07.Text);
        Params.ParamByName ('@pe_dia08').Value         := GetValue(edDia08.Text);
        Params.ParamByName ('@pe_dia09').Value         := GetValue(edDia09.Text);
        Params.ParamByName ('@pe_dia10').Value         := GetValue(edDia10.Text);
        Params.ParamByName ('@pe_dia11').Value         := GetValue(edDia11.Text);
        Params.ParamByName ('@pe_dia12').Value         := GetValue(edDia12.Text);
        Params.ParamByName ('@pe_dia13').Value         := GetValue(edDia13.Text);
        Params.ParamByName ('@pe_dia14').Value         := GetValue(edDia14.Text);
        Params.ParamByName ('@pe_dia15').Value         := GetValue(edDia15.Text);
        Params.ParamByName ('@pe_dia16').Value         := GetValue(edDia16.Text);
        Params.ParamByName ('@pe_dia17').Value         := GetValue(edDia17.Text);
        Params.ParamByName ('@pe_dia18').Value         := GetValue(edDia18.Text);
        Params.ParamByName ('@pe_dia19').Value         := GetValue(edDia19.Text);
        Params.ParamByName ('@pe_dia20').Value         := GetValue(edDia20.Text);
        Params.ParamByName ('@pe_dia21').Value         := GetValue(edDia21.Text);
        Params.ParamByName ('@pe_dia22').Value         := GetValue(edDia22.Text);
        Params.ParamByName ('@pe_dia23').Value         := GetValue(edDia23.Text);
        Params.ParamByName ('@pe_dia24').Value         := GetValue(edDia24.Text);
        Params.ParamByName ('@pe_dia25').Value         := GetValue(edDia25.Text);
        Params.ParamByName ('@pe_dia26').Value         := GetValue(edDia26.Text);
        Params.ParamByName ('@pe_dia27').Value         := GetValue(edDia27.Text);
        Params.ParamByName ('@pe_dia28').Value         := GetValue(edDia28.Text);
        Params.ParamByName ('@pe_dia29').Value         := GetValue(edDia29.Text);
        Params.ParamByName ('@pe_dia30').Value         := GetValue(edDia30.Text);
        Params.ParamByName ('@pe_dia31').Value         := GetValue(edDia31.Text);
        Params.ParamByName ('@pe_num_faltas').Value    := GetValue(edFaltas.Text);
        Params.ParamByName ('@pe_dsr_acumulado').Value := GetValue(edDSR.Text);
        Params.ParamByName ('@pe_num_justificadas').Value := GetValue(edJustificados.Text);  //Separar o campo de justificados para fins de desconto de passe...
        Params.ParamByName ('@pe_num_dias_uteis').Value := mmDIAS_UTEIS;
        Params.ParamByName ('@pe_num_comvenc').Value   := GetValue(edComVenc.Text);
        Params.ParamByName ('@pe_num_semvenc').Value   := GetValue(edSemVenc.Text);
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;

        Execute;

        Application.MessageBox ('Os dados foram lan�ados com sucesso no banco de dados.',
                                '[Sistema Deca] - Frequ�ncia',
                                MB_OK + MB_ICONINFORMATION);
        Panel2.Visible := False;
        GroupBox10.Visible := False;
        GroupBox11.Visible := False;

        with dbgFrequencias do
        begin
          for col := 5 to 35 do
          begin
            if (Columns[col].Field.Text = 'S') or (Columns[col].Field.Text = 'D') then
            begin
              Columns[col].ReadOnly := True;
              Columns[col].Color := clRed;
            end
            else if (Columns[col].Field.Text = 'R') or (Columns[col].Field.Text = 'C') then
            begin
              Columns[col].ReadOnly := True;
              Columns[col].Color := clYellow;
            end;
          end;
          dbgFrequencias.Refresh;
        end;


      end;
    except

      Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                              'Um erro ocorreu ao tentar alterar os dados da frequ�ncia.' + #13+#10 +
                              'Tente novamente mais tarde ou no caso de reincid�ncia, entre em contato com a Divis�o de Inform�tica.',
                              '[Sistema Deca] - Erro',
                              MB_OK + MB_ICONERROR);

    end;


  end;

end;

procedure TfrmNovoControleFrequencia.btnSair2Click(Sender: TObject);
begin
  Panel2.Visible := False;
end;

function TfrmNovoControleFrequencia.CalculaDiasUteis(DataInicial,
  DataFinal: String): Integer;
var a,b,c : TDateTime;
    ct, s : Integer;
begin

  //****************************************************************************
  //Fun��o para retornar a quantidade de dias �teis entre dois per�odos...
  //Utilizado para calcular a quantidade de dias �teis no m�s
  //no lan�amento da frequ�ncia dos adolescentes...
  //****************************************************************************

  if StrToDate(DataFinal) < StrToDate(DataInicial) then
  begin
    Result := 0;
    Exit;
  end;

  ct := 0;
  s  := 1;
  a  := StrToDate(DataFinal);
  b  := StrToDate(DataInicial);
  if a > b then
  begin
    c := a;
    a := b;
    b := c;
    s := 1;
  end;

  a := a + 1;
  while (DayOfWeek(a)<>2) and (a <= b) do
  begin
    if (DayOfWeek(a) in [2..6]) then
    begin
      inc(ct);
    end;
    a := a + 1;
  end;

  ct:= ct + round((5*int((b-a)/7)));
  a := a + (7*int((b-a)/7));

  while a <= b do
  begin
    if DayOfWeek(a) in [2..6] then
    begin
      inc(ct);
    end;
    a := a + 1;
  end;

  if ct < 0 then
  begin
    ct := 0;
  end;
  result := (s * ct);
end;

procedure TfrmNovoControleFrequencia.AtualizaFaltas;
begin

  //****************************************************************************
  // Procedimento para calcular as faltas de crian�as e bolsistas
  //****************************************************************************

  //Calcula a quantidade de dias �teis no m�s do lan�amento da frequ�ncia...
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value < 10) then
    strMES := '0' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value)
  else
    strMES := IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value);

  mmDATA_INICIO_MES := '01/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  //if StrToInt(strMES) in [1,3,5,7,8,10,12] then
  //  mmDATA_FINAL_MES  := '31/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  //else if StrToInt(strMES) in [4,6,9,11] then
  //  mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  //else
  //  //strMES = '02' then
  //  mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  if StrToInt(strMES) <> 2 then
    mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  else
    mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value); 

  //lbDiasUteis.Caption := IntToStr(CalculaDiasUteis(mmDATA_INICIO_MES, mmDATA_FINAL_MES));

  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_EXPORTOU').Value = 0) then
  begin

    Panel2.Visible := True;

    total_faltas_mes      := 0;
    total_feriados_mes    := 0;
    total_domingos_mes    := 0;
    total_sabados_mes     := 0;
    total_compensados_mes := 0;
    total_presencas_mes   := 0;
    total_justif_mes      := 0;
    total_comvenc         := 0;
    total_semvenc         := 0;

    //DIA 01********************************************************************
    if (edDia01.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia01.Text = 'P') or (edDia01.Text = 'X') or (edDia01.Text = 'E') and (edDia01.Text = 'V') and (edDia01.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia01.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia01.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia01.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia01.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia01.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia01.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia01.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 02********************************************************************
    if (edDia02.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia02.Text = 'P') or (edDia02.Text = 'X') or (edDia02.Text = 'E') and (edDia02.Text = 'V') and (edDia02.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia02.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia02.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia02.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia02.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia02.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia02.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia02.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 03********************************************************************
    if (edDia03.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia03.Text = 'P') or (edDia03.Text = 'X') or (edDia03.Text = 'E') and (edDia03.Text = 'V') and (edDia03.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia03.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia03.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia03.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia03.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia03.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia03.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia03.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 04********************************************************************
    if (edDia04.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia04.Text = 'P') or (edDia04.Text = 'X') or (edDia04.Text = 'E') and (edDia04.Text = 'V') and (edDia04.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia04.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia04.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia04.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia04.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia04.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia04.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia04.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 05********************************************************************
    if (edDia05.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia05.Text = 'P') or (edDia05.Text = 'X') or (edDia05.Text = 'E') and (edDia05.Text = 'V') and (edDia05.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia05.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia05.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia05.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia05.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia05.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia05.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia05.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 06********************************************************************
    if (edDia06.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia06.Text = 'P') or (edDia06.Text = 'X') or (edDia06.Text = 'E') and (edDia06.Text = 'V') and (edDia06.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia06.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia06.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia06.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia06.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia06.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia06.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia06.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 07********************************************************************
    if (edDia07.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia07.Text = 'P') or (edDia07.Text = 'X') or (edDia07.Text = 'E') and (edDia07.Text = 'V') and (edDia07.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia07.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia07.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia07.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia07.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia07.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia07.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia07.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 08********************************************************************
    if (edDia08.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia08.Text = 'P') or (edDia08.Text = 'X') or (edDia08.Text = 'E') and (edDia08.Text = 'V') and (edDia08.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia08.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia08.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia08.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia08.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia08.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia08.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia08.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 09********************************************************************
    if (edDia09.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia09.Text = 'P') or (edDia09.Text = 'X') or (edDia09.Text = 'E') and (edDia09.Text = 'V') and (edDia09.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia09.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia09.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia09.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia09.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia09.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia09.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia09.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 10********************************************************************
    if (edDia10.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia10.Text = 'P') or (edDia10.Text = 'X') or (edDia10.Text = 'E') and (edDia10.Text = 'V') and (edDia10.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia10.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia10.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia10.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia10.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia10.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia10.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia10.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 11********************************************************************
    if (edDia11.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia11.Text = 'P') or (edDia11.Text = 'X') or (edDia11.Text = 'E') and (edDia11.Text = 'V') and (edDia11.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia11.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia11.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia11.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia11.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia11.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia11.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia11.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 12********************************************************************
    if (edDia12.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia12.Text = 'P') or (edDia12.Text = 'X') or (edDia12.Text = 'E') and (edDia12.Text = 'V') and (edDia12.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia12.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia12.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia12.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia12.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia12.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia12.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia12.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 13********************************************************************
    if (edDia13.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia13.Text = 'P') or (edDia13.Text = 'X') or (edDia13.Text = 'E') and (edDia13.Text = 'V') and (edDia13.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia13.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia13.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia13.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia13.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia13.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia13.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia13.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 14********************************************************************
    if (edDia14.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia14.Text = 'P') or (edDia14.Text = 'X') or (edDia14.Text = 'E') and (edDia14.Text = 'V') and (edDia14.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia14.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia14.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia14.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia14.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia14.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia14.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia14.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 15********************************************************************
    if (edDia15.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia15.Text = 'P') or (edDia15.Text = 'X') or (edDia15.Text = 'E') and (edDia15.Text = 'V') and (edDia15.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia15.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia15.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia15.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia15.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia15.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia15.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia15.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 16********************************************************************
    if (edDia16.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia16.Text = 'P') or (edDia16.Text = 'X') or (edDia16.Text = 'E') and (edDia16.Text = 'V') and (edDia16.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia16.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia16.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia16.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia16.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia16.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia16.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia16.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 17********************************************************************
    if (edDia17.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia17.Text = 'P') or (edDia17.Text = 'X') or (edDia17.Text = 'E') and (edDia17.Text = 'V') and (edDia17.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia17.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia17.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia17.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia17.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia17.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia17.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia17.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 18********************************************************************
    if (edDia18.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia18.Text = 'P') or (edDia18.Text = 'X') or (edDia18.Text = 'E') and (edDia18.Text = 'V') and (edDia18.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia18.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia18.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia18.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia18.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia18.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia18.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia18.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 19********************************************************************
    if (edDia19.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia19.Text = 'P') or (edDia19.Text = 'X') or (edDia19.Text = 'E') and (edDia19.Text = 'V') and (edDia19.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia19.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia19.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia19.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia19.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia19.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia19.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia19.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 20********************************************************************
    if (edDia20.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia20.Text = 'P') or (edDia20.Text = 'X') or (edDia20.Text = 'E') and (edDia20.Text = 'V') and (edDia20.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia20.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia20.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia20.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia20.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia20.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia20.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia20.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 21********************************************************************
    if (edDia21.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia21.Text = 'P') or (edDia21.Text = 'X') or (edDia21.Text = 'E') and (edDia21.Text = 'V') and (edDia21.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia21.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia21.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia21.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia21.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia21.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia21.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia21.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 22********************************************************************
    if (edDia22.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia22.Text = 'P') or (edDia22.Text = 'X') or (edDia22.Text = 'E') and (edDia22.Text = 'V') and (edDia22.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia22.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia22.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia22.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia22.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia22.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia22.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia22.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 23********************************************************************
    if (edDia23.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia23.Text = 'P') or (edDia23.Text = 'X') or (edDia23.Text = 'E') and (edDia23.Text = 'V') and (edDia23.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia23.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia23.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia23.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia23.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia23.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia23.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia23.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 24********************************************************************
    if (edDia24.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia24.Text = 'P') or (edDia24.Text = 'X') or (edDia24.Text = 'E') and (edDia24.Text = 'V') and (edDia24.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia24.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia24.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia24.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia24.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia24.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia24.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia24.Text = 'L') then
      total_semvenc := total_semvenc + 1;
    
    //DIA 25********************************************************************
    if (edDia25.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia25.Text = 'P') or (edDia25.Text = 'X') or (edDia25.Text = 'E') and (edDia25.Text = 'V') and (edDia25.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia25.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia25.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia25.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia25.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia25.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia25.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia25.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 26********************************************************************
    if (edDia26.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia26.Text = 'P') or (edDia26.Text = 'X') or (edDia26.Text = 'E') and (edDia26.Text = 'V') and (edDia26.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia26.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia26.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia26.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia26.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia26.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia26.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia26.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 27********************************************************************
    if (edDia27.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia27.Text = 'P') or (edDia27.Text = 'X') or (edDia27.Text = 'E') and (edDia27.Text = 'V') and (edDia27.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia27.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia27.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia27.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia27.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia27.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia27.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia27.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 28********************************************************************
    if (edDia28.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia28.Text = 'P') or (edDia28.Text = 'X') or (edDia28.Text = 'E') and (edDia28.Text = 'V') and (edDia28.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia28.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia28.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia28.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia28.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia28.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia28.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia28.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 29********************************************************************
    if (edDia29.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia29.Text = 'P') or (edDia29.Text = 'X') or (edDia29.Text = 'E') and (edDia29.Text = 'V') and (edDia29.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia29.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia29.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia29.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia29.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia29.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia29.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia29.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 30********************************************************************
    if (edDia30.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia30.Text = 'P') or (edDia30.Text = 'X') or (edDia30.Text = 'E') and (edDia30.Text = 'V') and (edDia30.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia30.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia30.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia30.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia30.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia30.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia30.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia30.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 31********************************************************************
    if (edDia31.Text = 'F') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia31.Text = 'P') or (edDia31.Text = 'X') or (edDia31.Text = 'E') and (edDia31.Text = 'V') and (edDia31.Text = 'L') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia31.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia31.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia31.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia31.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia31.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia31.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia31.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    lbFeriados.Caption    := IntToStr(total_feriados_mes);
    lbCompensados.Caption := IntToStr(total_compensados_mes);
    lbSabados.Caption     := IntToStr(total_sabados_mes);
    lbDomingos.Caption    := IntToStr(total_domingos_mes);
    lbPresencas.Caption   := IntToStr(total_presencas_mes);
    edFaltas.Text         := IntToStr(total_faltas_mes);
    edDSR.Text            := '';
    total_dsr_mes         := 0;
    edJustificados.Text   := IntToStr(total_justif_mes);
    edComVenc.Text        := IntToStr(total_comvenc);
    edSemVenc.Text        := IntToStr(total_semvenc);


    //***************************************************************
    // Calcular os dias �teis no "bra�o"
    if StrToInt(strMES) in [1,3,5,7,8,10,12] then
      mmDIAS_UTEIS := (31 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes))
    else if StrToInt(strMES) in [4,6,9,11] then
      mmDIAS_UTEIS := (30 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes))
    else
      mmDIAS_UTEIS := (28 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes));

    //if StrToInt(strMES) in [1,3,5,7,8,10,12] then
    //  mmTOTAL_FALTAS := (31 - total_presencas_mes)
    //else if StrToInt(strMES) in [4,6,9,11] then
    //  mmTOTAL_FALTAS := (30 - total_presencas_mes)
    //else
    //  mmTOTAL_FALTAS := (28 - total_presencas_mes);
 

    //Tratar Justificadas
    //if  (total_justif_mes < ((mmDIAS_UTEIS/2)+1))  then
    //  edJustificados.Text := IntToStr(total_justif_mes)
    //else if (total_justif_mes > ((mmDIAS_UTEIS/2)+1)) then
    //begin
    //  edJustificados.Text := IntToStr(30 - total_presencas_mes);
    //end
    //else if (total_justif_mes = ((mmDIAS_UTEIS/2)+1))  then
    //  edJustificados.Text := '15';


    lbDiasUteis.Caption := IntToStr(mmDIAS_UTEIS);

    GroupBox10.Visible := False;
    GroupBox11.Visible := False;

  end
  else
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Esses dados j� foram exportados e n�o podem ser alterados.',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
  end;

end;

procedure TfrmNovoControleFrequencia.AtualizaFaltasCLT;
begin
  //***********************************************************************************
  //Procedure para calcular as faltas e os respectivos descontos de DSR para celetistas
  //***********************************************************************************

  //Calcula a quantidade de dias �teis no m�s do lan�amento da frequ�ncia...
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value < 10) then
    strMES := '0' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value)
  else
    strMES := IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value);

  mmDATA_INICIO_MES := '01/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  //if StrToInt(strMES) in [1,3,5,7,8,10,12] then
  //  mmDATA_FINAL_MES  := '31/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  //else if StrToInt(strMES) in [4,6,9,11] then
  //  mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  //else
  //  //strMES = '02' then
  //  mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  if StrToInt(strMES) <> 2 then
    mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  else
    mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);


  lbDiasUteis.Caption := IntToStr(CalculaDiasUteis(mmDATA_INICIO_MES, mmDATA_FINAL_MES));

  //Verificar se o registro j� foi exportado...
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('flg_exportou').Value = 0) then
  begin

    //Zerar as vari�veis para c�lculo das frequ�ncias...
    total_faltas_mes := 0;

    total_faltas_semana1 := 0;
    total_faltas_semana2 := 0;
    total_faltas_semana3 := 0;
    total_faltas_semana4 := 0;
    total_faltas_semana5 := 0;
    total_faltas_semana6 := 0;

    total_feriados_mes    := 0;
    total_feriados_semana1   := 0; //Totalizar os feriados na semana1
    total_feriados_semana2   := 0; //Totalizar os feriados na semana2
    total_feriados_semana3   := 0; //Totalizar os feriados na semana3
    total_feriados_semana4   := 0; //Totalizar os feriados na semana4
    total_feriados_semana5   := 0; //Totalizar os feriados na semana5
    total_feriados_semana6   := 0; //Totalizar os feriados na semana5

    total_domingos_mes    := 0;
    total_sabados_mes     := 0;

    total_compensados_mes := 0;
    total_presencas_mes   := 0;

    total_dsr_mes := 0;
    total_dsr_proximo_mes := 0; //DSR acumulado da ultima semana para o proximo mes...

    total_dsr_semana1 := 0;
    total_dsr_semana2 := 0;
    total_dsr_semana3 := 0;
    total_dsr_semana4 := 0;
    total_dsr_semana5 := 0;
    total_dsr_semana6 := 0;

    total_comvenc     := 0;
    total_semvenc     := 0;

    //**************************************************************************
    //Calcular os totais de Faltas, Presen�as, Compensados, S�bados, Domingos e Feriados...
    //**************************************************************************
    //Dia 01
    mmDATA_ATUAL := '01/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia01.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia01.Text = 'P') or (edDia01.Text = 'J') or (edDia01.Text = 'X') or (edDia01.Text = 'E') or (edDia01.Text = 'V') or (edDia01.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia01.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia01.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia01.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia01.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 02
    mmDATA_ATUAL := '02/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia02.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia02.Text = 'P') or (edDia02.Text = 'J') or (edDia02.Text = 'X') or (edDia02.Text = 'E') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia02.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia02.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia02.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia02.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 03
    mmDATA_ATUAL := '03/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia03.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia03.Text = 'P') or (edDia03.Text = 'J') or (edDia03.Text = 'X') or (edDia03.Text = 'E') or (edDia03.Text = 'V') or (edDia03.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia03.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia03.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia03.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia03.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 04
    mmDATA_ATUAL := '04/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia04.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia04.Text = 'P') or (edDia04.Text = 'J') or (edDia04.Text = 'X') or (edDia04.Text = 'E') or (edDia04.Text = 'V') or (edDia04.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia04.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia04.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia04.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia04.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 05
    mmDATA_ATUAL := '05/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia05.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia05.Text = 'P') or (edDia05.Text = 'J') or (edDia05.Text = 'X') or (edDia05.Text = 'E') or (edDia05.Text = 'V') or (edDia05.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia05.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia05.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia05.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia05.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 06
    mmDATA_ATUAL := '06/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia06.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia06.Text = 'P') or (edDia06.Text = 'J') or (edDia06.Text = 'X') or (edDia06.Text = 'E') or (edDia06.Text = 'V') or (edDia06.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia06.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia06.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia06.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia06.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 07
    mmDATA_ATUAL := '07/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia07.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia07.Text = 'P') or (edDia07.Text = 'J') or (edDia07.Text = 'X') or (edDia07.Text = 'E') or (edDia07.Text = 'V') or (edDia07.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia07.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia07.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia07.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia07.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 08
    mmDATA_ATUAL := '08/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia08.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia08.Text = 'P') or (edDia08.Text = 'J') or (edDia08.Text = 'X') or (edDia08.Text = 'E') or (edDia08.Text = 'V') or (edDia08.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia08.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia08.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia08.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia08.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 09
    mmDATA_ATUAL := '09/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia09.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia09.Text = 'P') or (edDia09.Text = 'J') or (edDia09.Text = 'X') or (edDia09.Text = 'E') or (edDia09.Text = 'V') or (edDia09.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia09.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia09.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia09.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia09.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 10
    mmDATA_ATUAL := '10/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia10.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia10.Text = 'P') or (edDia10.Text = 'J') or (edDia10.Text = 'X') or (edDia10.Text = 'E') or (edDia10.Text = 'V') or (edDia10.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia10.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia10.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia10.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia10.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 11
    mmDATA_ATUAL := '11/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia11.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia11.Text = 'P') or (edDia11.Text = 'J') or (edDia11.Text = 'X') or (edDia11.Text = 'E') or (edDia11.Text = 'V') or (edDia11.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia11.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia11.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia11.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia11.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 12
    mmDATA_ATUAL := '12/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia12.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia12.Text = 'P') or (edDia12.Text = 'J') or (edDia12.Text = 'X') or (edDia12.Text = 'E') or (edDia12.Text = 'V') or (edDia12.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia12.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia12.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia12.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia12.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 13
    mmDATA_ATUAL := '13/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia13.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia13.Text = 'P') or (edDia13.Text = 'J') or (edDia13.Text = 'X') or (edDia13.Text = 'E') or (edDia13.Text = 'V') or (edDia13.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia13.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia13.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia13.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia13.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 14
    mmDATA_ATUAL := '14/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia14.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia14.Text = 'P') or (edDia14.Text = 'J') or (edDia14.Text = 'X') or (edDia14.Text = 'E') or (edDia14.Text = 'V') or (edDia14.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia14.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia14.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia14.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia14.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 15
    mmDATA_ATUAL := '15/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia15.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia15.Text = 'P') or (edDia15.Text = 'J') or (edDia15.Text = 'X') or (edDia15.Text = 'E') or (edDia15.Text = 'V') or (edDia15.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia15.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia15.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia15.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia15.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 16
    mmDATA_ATUAL := '16/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia16.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia16.Text = 'P') or (edDia16.Text = 'J') or (edDia16.Text = 'X') or (edDia16.Text = 'E') or (edDia16.Text = 'V') or (edDia16.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia16.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia16.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia16.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia16.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 17
    mmDATA_ATUAL := '17/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia17.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia17.Text = 'P') or (edDia17.Text = 'J') or (edDia17.Text = 'X') or (edDia17.Text = 'E') or (edDia17.Text = 'V') or (edDia17.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia17.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia17.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia17.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia17.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 18
    mmDATA_ATUAL := '18/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia18.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia18.Text = 'P') or (edDia18.Text = 'J') or (edDia18.Text = 'X') or (edDia18.Text = 'E') or (edDia18.Text = 'V') or (edDia18.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia18.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia18.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia18.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia18.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 19
    mmDATA_ATUAL := '19/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia19.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia19.Text = 'P') or (edDia19.Text = 'J') or (edDia19.Text = 'X') or (edDia19.Text = 'E') or (edDia19.Text = 'V') or (edDia19.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia19.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia19.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia19.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia19.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 20
    mmDATA_ATUAL := '20/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia20.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia20.Text = 'P') or (edDia20.Text = 'J') or (edDia20.Text = 'X') or (edDia20.Text = 'E') or (edDia20.Text = 'V') or (edDia20.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia20.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia20.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia20.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia20.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 21
    mmDATA_ATUAL := '21/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia21.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia21.Text = 'P') or (edDia21.Text = 'J') or (edDia21.Text = 'X') or (edDia21.Text = 'E') or (edDia21.Text = 'V') or (edDia21.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia21.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia21.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia21.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia21.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 22
    mmDATA_ATUAL := '22/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));
    if (edDia22.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia22.Text = 'P') or (edDia22.Text = 'J') or (edDia22.Text = 'X') or (edDia22.Text = 'E') or (edDia22.Text = 'V') or (edDia22.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia22.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia22.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia22.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia22.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 23
    mmDATA_ATUAL := '23/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia23.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia23.Text = 'P') or (edDia23.Text = 'J') or (edDia23.Text = 'X') or (edDia23.Text = 'E') or (edDia23.Text = 'V') or (edDia23.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia23.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia23.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia23.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia23.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 24
    mmDATA_ATUAL := '24/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia24.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia24.Text = 'P') or (edDia24.Text = 'J') or (edDia24.Text = 'X') or (edDia24.Text = 'E') or (edDia24.Text = 'V') or (edDia24.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia24.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia24.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia24.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia24.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 25
    mmDATA_ATUAL := '25/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia25.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia25.Text = 'P') or (edDia25.Text = 'J') or (edDia25.Text = 'X') or (edDia25.Text = 'E') or (edDia25.Text = 'V') or (edDia25.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia25.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia25.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia25.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia25.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 26
    mmDATA_ATUAL := '26/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia26.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia26.Text = 'P') or (edDia26.Text = 'J') or (edDia26.Text = 'X') or (edDia26.Text = 'E') or (edDia26.Text = 'V') or (edDia26.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia26.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia26.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia26.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia26.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 27
    mmDATA_ATUAL := '27/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia27.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia27.Text = 'P') or (edDia27.Text = 'J') or (edDia27.Text = 'X') or (edDia27.Text = 'E') or (edDia27.Text = 'V') or (edDia27.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia27.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia27.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia27.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia27.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;

    //**************************************************************************
    //Dia 28
    mmDATA_ATUAL := '28/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
    mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

    if (edDia28.Text = 'F') then
    begin
      total_faltas_mes := total_faltas_mes + 1;
      if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
      else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
      else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
      else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
      else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
      else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
    end
    else if (edDia28.Text = 'P') or (edDia28.Text = 'J') or (edDia28.Text = 'X') or (edDia28.Text = 'E') or (edDia28.Text = 'V') or (edDia28.Text = 'L') then
    begin
      total_presencas_mes := total_presencas_mes + 1
    end
    else if (edDia28.Text = 'C') then
    begin
      total_compensados_mes := total_compensados_mes + 1
    end
    else if (edDia28.Text = 'S') then
    begin
      total_sabados_mes := total_sabados_mes + 1
    end
    else if (edDia28.Text = 'D') then
    begin
      total_domingos_mes := total_domingos_mes + 1
    end
    else if (edDia28.Text = 'R') then
    begin
      total_feriados_mes := total_feriados_mes + 1;
      if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
      else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
      else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
      else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
      else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
      else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
    end;


    //**************************************************************************
    //Dia 29
    if strMes = '02' then
    begin

    end
    else
    begin
      mmDATA_ATUAL := '29/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
      mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

      if (edDia29.Text = 'F') then
      begin
        total_faltas_mes := total_faltas_mes + 1;
        if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
        else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
        else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
        else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
        else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
        else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
      end
      else if (edDia29.Text = 'P') or (edDia29.Text = 'J') or (edDia29.Text = 'X') or (edDia29.Text = 'E') or (edDia29.Text = 'V') or (edDia29.Text = 'L') then
      begin
        total_presencas_mes := total_presencas_mes + 1
      end
      else if (edDia29.Text = 'C') then
      begin
        total_compensados_mes := total_compensados_mes + 1
      end
      else if (edDia29.Text = 'S') then
      begin
        total_sabados_mes := total_sabados_mes + 1
      end
      else if (edDia29.Text = 'D') then
      begin
        total_domingos_mes := total_domingos_mes + 1
      end
      else if (edDia29.Text = 'R') then
      begin
        total_feriados_mes := total_feriados_mes + 1;
        if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
        else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
        else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
        else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
        else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
        else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
      end;

    //**************************************************************************
    //Dia 30
    if strMes = '02' then
    begin

    end
    else
    begin
      mmDATA_ATUAL := '30/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
      mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

      if (edDia30.Text = 'F') then
      begin
        total_faltas_mes := total_faltas_mes + 1;
        if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
        else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
        else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
        else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
        else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
        else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
      end
      else if (edDia30.Text = 'P') or (edDia30.Text = 'J') or (edDia30.Text = 'X') or (edDia30.Text = 'E') or (edDia30.Text = 'V') or (edDia30.Text = 'L') then
      begin
        total_presencas_mes := total_presencas_mes + 1
      end
      else if (edDia30.Text = 'C') then
      begin
        total_compensados_mes := total_compensados_mes + 1
      end
      else if (edDia30.Text = 'S') then
      begin
        total_sabados_mes := total_sabados_mes + 1
      end
      else if (edDia30.Text = 'D') then
      begin
        total_domingos_mes := total_domingos_mes + 1
      end
      else if (edDia30.Text = 'R') then
      begin
        total_feriados_mes := total_feriados_mes + 1;
        if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
        else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
        else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
        else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
        else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
        else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
      end;


    //***************************************************************
    //Dia 31
    if strMes = '02' then
    begin

    end
    else
    begin
      mmDATA_ATUAL := '30/' + strMes + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value);
      mmSEMANA := SemanaDoMes(StrToDate(mmDATA_ATUAL));

      if (edDia31.Text = 'F') then
      begin
        total_faltas_mes := total_faltas_mes + 1;
        if mmSEMANA = 1 then total_faltas_semana1 := total_faltas_semana1 + 1
        else if mmSEMANA = 2 then total_faltas_semana2 := total_faltas_semana2 + 1
        else if mmSEMANA = 3 then total_faltas_semana3 := total_faltas_semana3 + 1
        else if mmSEMANA = 4 then total_faltas_semana4 := total_faltas_semana4 + 1
        else if mmSEMANA = 5 then total_faltas_semana5 := total_faltas_semana5 + 1
        else if mmSEMANA = 6 then total_faltas_semana6 := total_faltas_semana6 + 1;
      end
      else if (edDia31.Text = 'P') or (edDia31.Text = 'J') or (edDia31.Text = 'X') or (edDia31.Text = 'E') or (edDia31.Text = 'V') or (edDia31.Text = 'L') then
      begin
        total_presencas_mes := total_presencas_mes + 1
      end
      else if (edDia31.Text = 'C') then
      begin
        total_compensados_mes := total_compensados_mes + 1
      end
      else if (edDia31.Text = 'S') then
      begin
        total_sabados_mes := total_sabados_mes + 1
      end
      else if (edDia31.Text = 'D') then
      begin
        total_domingos_mes := total_domingos_mes + 1
      end
      else if (edDia31.Text = 'R') then
      begin
        total_feriados_mes := total_feriados_mes + 1;
        if mmSEMANA = 1 then total_feriados_semana1 := total_feriados_semana1 + 1
        else if mmSEMANA = 2 then total_feriados_semana2 := total_feriados_semana2 + 1
        else if mmSEMANA = 3 then total_feriados_semana3 := total_feriados_semana3 + 1
        else if mmSEMANA = 4 then total_feriados_semana4 := total_feriados_semana4 + 1
        else if mmSEMANA = 5 then total_feriados_semana5 := total_feriados_semana5 + 1
        else if mmSEMANA = 6 then total_feriados_semana6 := total_feriados_semana6 + 1;
      end;

      //**************************************************************************
      //Atualiza o c�lculo do DSR
      //Semana 1
      if (total_faltas_semana1 > 0) then
        total_dsr_semana1 := total_feriados_semana1 + 1  //Domingo
      else
        total_dsr_semana1 := 0;

      //Semana 2
      if (total_faltas_semana2 > 0) then
        total_dsr_semana2 := total_feriados_semana2 + 1  //Domingo
      else
        total_dsr_semana2 := 0;

      //Semana 3
      if (total_faltas_semana3 > 0) then
        total_dsr_semana3 := total_feriados_semana3 + 1  //Domingo
      else
        total_dsr_semana3 := 0;

      //Semana 4
      if (total_faltas_semana4 = 0) and (mmSEMANA <> mmSEMANA_PRIMEIRO_DIA_MES_SEGUINTE) then
        total_dsr_semana4 := total_feriados_semana4 + 1  //Domingo
      else
        total_dsr_semana4 := 0;

      //Semana 5
      if (total_faltas_semana5 = 0) and (mmSEMANA <> mmSEMANA_PRIMEIRO_DIA_MES_SEGUINTE) then
        total_dsr_semana5 := total_feriados_semana5 + 1  //Domingo
      else
        total_dsr_semana5 := 0;

      //Semana 6
      if (total_faltas_semana6 = 0) and (mmSEMANA <> mmSEMANA_PRIMEIRO_DIA_MES_SEGUINTE) then
        total_dsr_semana6 := total_feriados_semana6 + 1  //Domingo
      else
        total_dsr_semana6 := 0;


      lbFeriados.Caption    := IntToStr(total_feriados_mes);
      lbCompensados.Caption := IntToStr(total_compensados_mes);
      lbSabados.Caption     := IntToStr(total_sabados_mes);
      lbDomingos.Caption    := IntToStr(total_domingos_mes);
      lbPresencas.Caption   := IntToStr(total_presencas_mes);
      edFaltas.Text         := IntToStr(total_faltas_mes);
      edDSR.Text            := IntToStr(total_dsr_semana1 + total_dsr_semana2 + total_dsr_semana3 + total_dsr_semana4 + total_dsr_semana5 + total_dsr_semana6);

      mmTOTAL_DIAS_UTEIS    := 30 - total_domingos_mes - total_feriados_mes;
      mmTOTAL_FALTAS        := total_faltas_mes;
      edFaltas.Text         := IntToStr(mmTOTAL_FALTAS);

    end;

  end
  end
  end
  else
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10+
                            'Esses dados j� foram exportados e n�o podem ser alterados.',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
  end;

end;

function TfrmNovoControleFrequencia.SemanaDoMes(Date: TDateTime): Integer;
var
  y, m, d : word;
  FirstOfMonth, FirstThursday, FirstWeekStart : TDateTime;
  h : Integer;
begin

  try
    DecodeDate (Date, y, m, d);
    FirstOfMonth   := EncodeDate(y,m,1);
    h              := DayOfWeek(FirstOfMonth);
    FirstThursday  := FirstOfMonth + ((12 - h) mod 7);
    FirstWeekStart := FirstThursday - 3;
    if (Trunc(Date) < FirstWeekStart) then
      result := SemanaDoMes(FirstOfMonth-1)
    else
      result := (Round(Trunc(Date)-FirstWeekStart) div 7) + 1;
    if m = 12 then
    begin
      inc(y);
      m := 0;
    end;
  except
  on EConvertError do
    result := 0;  
  end;
end;


procedure TfrmNovoControleFrequencia.btnVerImpressaoClick(Sender: TObject);
begin
  //Application.CreateForm(TfrmSelecionarDadosFrequencia, frmSelecionarDadosFrequencia);
  //frmSelecionarDadosFrequencia.mskAno.Text := mskAno.Text;
  //frmSelecionarDadosFrequencia.cbMes.ItemIndex := cbMes.ItemIndex;
  //frmSelecionarDadosFrequencia.cbUnidade.ItemIndex := cbUnidade.ItemIndex;
  //frmSelecionarDadosFrequencia.ShowModal;

  Application.CreateForm (TrelFolhaFrequencia, relFolhaFrequencia);
  //relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA'; // ->  ' + vvNOM_UNIDADE;
  if (chkTodas.Checked) then
    relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA -> <Todas as Unidades> '
  else
    relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA -> ' + cbUnidade.Text;

  relFolhaFrequencia.lbMesAno.Caption := cbMes.Text + '/' + mskAno.Text;
  relFolhaFrequencia.QRGroup1.Enabled := True;
  relFolhaFrequencia.Preview;
  relFolhaFrequencia.Free;

end;

procedure TfrmNovoControleFrequencia.AtualizaFaltasCr;
begin
  //Procedure para calcular as faltas para as crian�as...
  //****************************************************************************
  // Procedimento para calcular as faltas de crian�as e bolsistas
  //****************************************************************************
  
  //Calcula a quantidade de dias �teis no m�s do lan�amento da frequ�ncia...
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value < 10) then
    strMES := '0' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value)
  else
    strMES := IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_mes').Value);

  mmDATA_INICIO_MES := '01/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  //if StrToInt(strMES) in [1,3,5,7,8,10,12] then
  //  mmDATA_FINAL_MES  := '31/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  //else if StrToInt(strMES) in [4,6,9,11] then
  //  mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  //else
  //  //strMES = '02' then
  //  mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);

  if StrToInt(strMES) <> 2 then
    mmDATA_FINAL_MES  := '30/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value)
  else
    mmDATA_FINAL_MES  := '28/' + strMES + '/' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_ano').Value);


  //lbDiasUteis.Caption := IntToStr(CalculaDiasUteis(mmDATA_INICIO_MES, mmDATA_FINAL_MES));

  //lbDiasUteis.Caption := IntToStr(CalculaDiasUteis(mmDATA_INICIO_MES, mmDATA_FINAL_MES));

  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_EXPORTOU').Value = 0) then
  begin

    Panel2.Visible := True;
    //**************************************************************************
    //N�mero de Faltas
    //edFaltas.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').Value;
    //**************************************************************************
    //DSR Acumulado
    //edDSR.Text    := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').Value;

    //*****************************************************************************
    //Criar procedimento/fun��o na aplica��o...
    //Criar rotina para calcular total_faltas_m, igual ao exemplo acima
    total_faltas_mes      := 0;
    total_feriados_mes    := 0;
    total_domingos_mes    := 0;
    total_sabados_mes     := 0;
    total_compensados_mes := 0;
    total_presencas_mes   := 0;
    total_justif_mes      := 0;
    total_semvenc         := 0;
    total_comvenc         := 0;

    //DIA 01********************************************************************
    if (edDia01.Text = 'F') then //or (edDia01.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia01.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia01.Text = 'P') or (edDia01.Text = 'X') or (edDia01.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia01.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia01.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia01.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia01.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia01.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia01.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 02********************************************************************
    if (edDia02.Text = 'F') then //or (edDia02.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia02.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia02.Text = 'P') or (edDia02.Text = 'X') or (edDia02.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia02.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia02.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia02.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia02.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia02.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia02.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 03********************************************************************
    if (edDia03.Text = 'F') then //or (edDia03.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia03.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia03.Text = 'P') or (edDia03.Text = 'X') or (edDia03.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia03.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia03.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia03.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia03.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia03.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia03.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 04********************************************************************
    if (edDia04.Text = 'F') then //or (edDia04.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia04.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia04.Text = 'P') or (edDia04.Text = 'X') or (edDia04.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia04.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia04.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia04.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia04.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia04.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia04.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 05********************************************************************
    if (edDia05.Text = 'F') then //or (edDia05.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia05.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia05.Text = 'P') or (edDia05.Text = 'X') or (edDia05.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia05.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia05.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia05.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia05.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia05.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia05.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 06********************************************************************
    if (edDia06.Text = 'F') then //or (edDia06.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia06.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia06.Text = 'P') or (edDia06.Text = 'X') or (edDia06.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia06.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia06.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia06.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia06.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia06.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia06.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 07********************************************************************
    if (edDia07.Text = 'F') then //or (edDia07.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia07.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia07.Text = 'P') or (edDia07.Text = 'X') or (edDia07.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia07.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia07.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia07.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia07.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia07.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia07.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 08********************************************************************
    if (edDia08.Text = 'F') then //or (edDia08.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia08.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia08.Text = 'P') or (edDia08.Text = 'X') or (edDia08.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia08.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia08.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia08.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia08.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia08.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia08.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 09********************************************************************
    if (edDia09.Text = 'F') then //or (edDia09.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia09.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia09.Text = 'P') or (edDia09.Text = 'X') or (edDia09.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia09.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia09.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia09.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia09.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia09.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia09.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 10********************************************************************
    if (edDia10.Text = 'F') then //or (edDia10.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia10.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia10.Text = 'P') or (edDia10.Text = 'X') or (edDia10.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia10.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia10.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia10.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia10.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia10.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia10.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 11********************************************************************
    if (edDia11.Text = 'F') then //or (edDia11.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia11.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia11.Text = 'P') or (edDia11.Text = 'X') or (edDia11.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia11.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia11.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia11.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia11.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia11.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia11.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 12********************************************************************
    if (edDia12.Text = 'F') then //or (edDia12.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia12.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia12.Text = 'P') or (edDia12.Text = 'X') or (edDia12.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia12.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia12.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia12.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia12.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia12.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia12.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 13********************************************************************
    if (edDia13.Text = 'F') then //or (edDia13.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia13.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia13.Text = 'P') or (edDia13.Text = 'X') or (edDia13.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia13.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia13.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia13.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia13.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia13.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia13.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 14********************************************************************
    if (edDia14.Text = 'F') then //or (edDia14.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia14.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia14.Text = 'P') or (edDia14.Text = 'X') or (edDia14.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia14.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia14.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia14.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia14.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia14.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia14.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 15********************************************************************
    if (edDia15.Text = 'F') then //or (edDia15.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia15.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia15.Text = 'P') or (edDia15.Text = 'X') or (edDia15.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia15.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia15.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia15.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia15.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia15.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia15.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 16********************************************************************
    if (edDia16.Text = 'F') then //or (edDia16.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia16.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia16.Text = 'P') or (edDia16.Text = 'X') or (edDia16.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia16.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia16.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia16.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia16.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia16.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia16.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 17********************************************************************
    if (edDia17.Text = 'F') then //or (edDia17.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia17.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia17.Text = 'P') or (edDia17.Text = 'X') or (edDia17.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia17.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia17.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia17.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia17.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia17.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia17.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 18********************************************************************
    if (edDia18.Text = 'F') then //or (edDia18.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia18.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia18.Text = 'P') or (edDia18.Text = 'X') or (edDia18.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia18.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia18.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia18.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia18.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia18.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia18.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 19********************************************************************
    if (edDia19.Text = 'F') then //or (edDia19.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia19.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia19.Text = 'P') or (edDia19.Text = 'X') or (edDia19.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia19.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia19.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia19.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia19.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia19.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia19.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 20********************************************************************
    if (edDia20.Text = 'F') then //or (edDia20.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia20.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia20.Text = 'P') or (edDia20.Text = 'X') or (edDia20.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia20.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia20.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia20.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia20.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia20.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia20.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 21********************************************************************
    if (edDia21.Text = 'F') then //or (edDia21.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia21.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia21.Text = 'P') or (edDia21.Text = 'X') or (edDia21.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia21.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia21.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia21.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia21.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia21.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia21.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 22********************************************************************
    if (edDia22.Text = 'F') then //or (edDia22.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia22.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia22.Text = 'P') or (edDia22.Text = 'X') or (edDia22.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia22.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia22.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia22.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia22.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia22.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia22.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 23********************************************************************
    if (edDia23.Text = 'F') then //or (edDia23.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia23.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia23.Text = 'P') or (edDia23.Text = 'X') or (edDia23.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia23.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia23.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia23.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia23.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia23.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia23.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 24********************************************************************
    if (edDia24.Text = 'F') then //or (edDia24.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia24.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia24.Text = 'P') or (edDia24.Text = 'X') or (edDia24.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia24.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia24.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia24.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia24.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia24.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia24.Text = 'L') then
      total_semvenc := total_semvenc + 1;
    
    //DIA 25********************************************************************
    if (edDia25.Text = 'F') then //or (edDia25.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia25.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia25.Text = 'P') or (edDia25.Text = 'X') or (edDia25.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia25.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia25.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia25.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia25.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia25.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia25.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 26********************************************************************
    if (edDia26.Text = 'F') then //or (edDia26.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia26.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia26.Text = 'P') or (edDia26.Text = 'X') or (edDia26.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia26.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia26.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia26.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia26.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia26.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia26.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 27********************************************************************
    if (edDia27.Text = 'F') then //or (edDia27.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia27.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia27.Text = 'P') or (edDia27.Text = 'X') or (edDia27.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia27.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia27.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia27.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia27.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia27.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia27.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 28********************************************************************
    if (edDia28.Text = 'F') then //or (edDia28.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia28.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia28.Text = 'P') or (edDia28.Text = 'X') or (edDia28.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia28.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia28.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia28.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia28.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia28.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia28.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 29********************************************************************
    if (edDia29.Text = 'F') then //or (edDia29.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia29.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia29.Text = 'P') or (edDia29.Text = 'X') or (edDia29.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia29.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia29.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia29.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia29.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia29.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia29.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 30********************************************************************
    if (edDia30.Text = 'F') then //or (edDia30.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia30.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia30.Text = 'P') or (edDia30.Text = 'X') or (edDia30.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia30.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia30.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia30.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia30.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia30.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia30.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    //DIA 31********************************************************************
    if (edDia31.Text = 'F') then //or (edDia31.Text = 'J') then
      total_faltas_mes := total_faltas_mes + 1
    else if (edDia31.Text = 'J') then
      total_justif_mes := total_justif_mes + 1
    else if (edDia31.Text = 'P') or (edDia31.Text = 'X') or (edDia31.Text = 'E') then
      total_presencas_mes := total_presencas_mes + 1
    else if (edDia31.Text = 'C') then
      total_compensados_mes := total_compensados_mes + 1
    else if (edDia31.Text = 'S') then
      total_sabados_mes := total_sabados_mes + 1
    else if (edDia31.Text = 'D') then
      total_domingos_mes := total_domingos_mes + 1
    else if (edDia31.Text = 'R') then
      total_feriados_mes := total_feriados_mes + 1
    else if (edDia31.Text = 'V') then
      total_comvenc := total_comvenc + 1
    else if (edDia31.Text = 'L') then
      total_semvenc := total_semvenc + 1;

    lbFeriados.Caption    := IntToStr(total_feriados_mes);
    lbCompensados.Caption := IntToStr(total_compensados_mes);
    lbSabados.Caption     := IntToStr(total_sabados_mes);
    lbDomingos.Caption    := IntToStr(total_domingos_mes);
    lbPresencas.Caption   := IntToStr(total_presencas_mes);
    edFaltas.Text         := IntToStr(total_faltas_mes);
    edJustificados.Text   := IntToStr(total_justif_mes);
    edComVenc.Text        := IntToStr(total_comvenc);
    edSemVenc.Text        := IntToStr(total_semvenc);

    //mmTOTAL_DIAS_UTEIS := StrToInt(lbDiasUteis.Caption);
    mmTOTAL_FALTAS     := total_faltas_mes + total_justif_mes;

    //edFaltas.Text := IntToStr(mmTOTAL_DIAS_UTEIS - mmTOTAL_FALTAS);
    //edDSR.Text    := '0';  //N�o existe c�lculo de DSR para crian�as
    //total_dsr_mes := 0;

    //***************************************************************
    // Calcular os dias �teis no "bra�o"
    if StrToInt(strMES) in [1,3,5,7,8,10,12] then
      mmDIAS_UTEIS := (31 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes))
    else if StrToInt(strMES) in [4,6,9,11] then
      mmDIAS_UTEIS := (30 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes))
    else
      mmDIAS_UTEIS := (28 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes));

    //mmDIAS_UTEIS := (30 - (total_sabados_mes + total_domingos_mes + total_feriados_mes + total_compensados_mes));

    //if  ( mmTOTAL_FALTAS < ( (mmDIAS_UTEIS/2)+1) ) then
    if (  mmTOTAL_FALTAS < ((mmDIAS_UTEIS/2)+1) ) then

      edFaltas.Text := IntToStr(mmTOTAL_FALTAS)
    else if ( (mmTOTAL_FALTAS >= (mmDIAS_UTEIS/2)+1) ) then
      edFaltas.Text := IntToStr(30 - (mmDIAS_UTEIS-mmTOTAL_FALTAS));

    lbDiasUteis.Caption := IntToStr(mmDIAS_UTEIS);

    GroupBox10.Visible := True;
    GroupBox11.Visible := True;

    end
  else  
  begin                                     
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Esses dados j� foram exportados e n�o podem ser alterados.',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
  end;
end;

procedure TfrmNovoControleFrequencia.edDia01Exit(Sender: TObject);
begin
  if (Length(edDia01.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia01.SetFocus;
  end
  else if (edDia01.Text <> 'P') and (edDia01.Text <> 'J') and (edDia01.Text <> 'F') and (edDia01.Text <> 'E') and (edDia01.Text <> 'X') and (edDia01.Text <> 'V') and (edDia01.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia01.SetFocus;
  end
  else if ( (edDia01.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'PARA AS UNIDADES DAB O VALOR "V" N�O PODE SER LAN�ADO.' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia01.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia02Exit(Sender: TObject);
begin
  if (Length(edDia02.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia02.SetFocus;
  end
  else if (edDia02.Text <> 'P') and (edDia02.Text <> 'J') and (edDia02.Text <> 'F') and (edDia02.Text <> 'E') and (edDia02.Text <> 'X') and (edDia02.Text <> 'V') and (edDia02.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia02.SetFocus;
  end
  else if ( (edDia02.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia02.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia03Exit(Sender: TObject);
begin
  if (Length(edDia03.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia03.SetFocus;
  end
  else if (edDia03.Text <> 'P') and (edDia03.Text <> 'J') and (edDia03.Text <> 'F') and (edDia03.Text <> 'E') and (edDia03.Text <> 'X') and (edDia03.Text <> 'V') and (edDia03.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia03.SetFocus;
  end
  else if ( (edDia03.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia03.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia04Exit(Sender: TObject);
begin
  if (Length(edDia04.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia04.SetFocus;
  end
  else if (edDia04.Text <> 'P') and (edDia04.Text <> 'J') and (edDia04.Text <> 'F') and (edDia04.Text <> 'E') and (edDia04.Text <> 'X') and (edDia04.Text <> 'V') and (edDia04.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia04.SetFocus;
  end
  else if ( (edDia04.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia04.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia05Exit(Sender: TObject);
begin
  if (Length(edDia05.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia05.SetFocus;
  end
  else if (edDia05.Text <> 'P') and (edDia05.Text <> 'J') and (edDia05.Text <> 'F') and (edDia05.Text <> 'E') and (edDia05.Text <> 'X') and (edDia05.Text <> 'V') and (edDia05.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia05.SetFocus;
  end
  else if ( (edDia05.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia05.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia06Exit(Sender: TObject);
begin
  if (Length(edDia06.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia06.SetFocus;
  end
  else if (edDia06.Text <> 'P') and (edDia06.Text <> 'J') and (edDia06.Text <> 'F') and (edDia06.Text <> 'E') and (edDia06.Text <> 'X') and (edDia06.Text <> 'V') and (edDia06.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia06.SetFocus;
  end
  else if ( (edDia06.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia06.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia07Exit(Sender: TObject);
begin
  if (Length(edDia07.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia07.SetFocus;
  end
  else if (edDia07.Text <> 'P') and (edDia07.Text <> 'J') and (edDia07.Text <> 'F') and (edDia07.Text <> 'E') and (edDia07.Text <> 'X') and (edDia07.Text <> 'V') and (edDia07.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia07.SetFocus;
  end
  else if ( (edDia07.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia07.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia08Exit(Sender: TObject);
begin
  if (Length(edDia08.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia08.SetFocus;
  end
  else if (edDia08.Text <> 'P') and (edDia08.Text <> 'J') and (edDia08.Text <> 'F') and (edDia08.Text <> 'E') and (edDia08.Text <> 'X') and (edDia08.Text <> 'V') and (edDia08.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia08.SetFocus;
  end
  else if ( (edDia08.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia08.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia09Exit(Sender: TObject);
begin
  if (Length(edDia09.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia09.SetFocus;
  end
  else if (edDia09.Text <> 'P') and (edDia09.Text <> 'J') and (edDia09.Text <> 'F') and (edDia09.Text <> 'E') and (edDia09.Text <> 'X') and (edDia09.Text <> 'V') and (edDia09.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia09.SetFocus;
  end
  else if ( (edDia09.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia09.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia10Exit(Sender: TObject);
begin
  if (Length(edDia10.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia10.SetFocus;
  end
  else if (edDia10.Text <> 'P') and (edDia10.Text <> 'J') and (edDia10.Text <> 'F') and (edDia10.Text <> 'E') and (edDia10.Text <> 'X') and (edDia10.Text <> 'V') and (edDia10.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia10.SetFocus;
  end
  else if ( (edDia10.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia10.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia11Exit(Sender: TObject);
begin
  if (Length(edDia11.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia11.SetFocus;
  end
  else if (edDia11.Text <> 'P') and (edDia11.Text <> 'J') and (edDia11.Text <> 'F') and (edDia11.Text <> 'E') and (edDia11.Text <> 'X') and (edDia11.Text <> 'V') and (edDia11.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia11.SetFocus;
  end
  else if ( (edDia11.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia11.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia12Exit(Sender: TObject);
begin
  if (Length(edDia12.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia12.SetFocus;
  end
  else if (edDia12.Text <> 'P') and (edDia12.Text <> 'J') and (edDia12.Text <> 'F') and (edDia12.Text <> 'E') and (edDia12.Text <> 'X') and (edDia12.Text <> 'V') and (edDia12.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia12.SetFocus;
  end
  else if ( (edDia12.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia12.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia13Exit(Sender: TObject);
begin
  if (Length(edDia13.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia13.SetFocus;
  end
  else if (edDia13.Text <> 'P') and (edDia13.Text <> 'J') and (edDia13.Text <> 'F') and (edDia13.Text <> 'E') and (edDia13.Text <> 'X') and (edDia13.Text <> 'V') and (edDia13.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia13.SetFocus;
  end
  else if ( (edDia13.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia13.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia14Exit(Sender: TObject);
begin
  if (Length(edDia14.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia14.SetFocus;
  end
  else if (edDia14.Text <> 'P') and (edDia14.Text <> 'J') and (edDia14.Text <> 'F') and (edDia14.Text <> 'E') and (edDia14.Text <> 'X') and (edDia14.Text <> 'V') and (edDia14.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia14.SetFocus;
  end
  else if ( (edDia14.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia14.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia15Exit(Sender: TObject);
begin
  if (Length(edDia15.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia15.SetFocus;
  end
  else if (edDia15.Text <> 'P') and (edDia15.Text <> 'J') and (edDia15.Text <> 'F') and (edDia15.Text <> 'E') and (edDia15.Text <> 'X') and (edDia15.Text <> 'V') and (edDia15.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia15.SetFocus;
  end
  else if ( (edDia15.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia15.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia16Exit(Sender: TObject);
begin
  if (Length(edDia16.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia16.SetFocus;
  end
  else if (edDia16.Text <> 'P') and (edDia16.Text <> 'J') and (edDia16.Text <> 'F') and (edDia16.Text <> 'E') and (edDia16.Text <> 'X') and (edDia16.Text <> 'V') and (edDia16.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia16.SetFocus;
  end
  else if ( (edDia16.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia16.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia17Exit(Sender: TObject);
begin
  if (Length(edDia17.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia17.SetFocus;
  end
  else if (edDia17.Text <> 'P') and (edDia17.Text <> 'J') and (edDia17.Text <> 'F') and (edDia17.Text <> 'E') and (edDia17.Text <> 'X') and (edDia17.Text <> 'V') and (edDia17.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia17.SetFocus;
  end
  else if ( (edDia17.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia17.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia18Exit(Sender: TObject);
begin
  if (Length(edDia18.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia18.SetFocus;
  end
  else if (edDia18.Text <> 'P') and (edDia18.Text <> 'J') and (edDia18.Text <> 'F') and (edDia18.Text <> 'E') and (edDia18.Text <> 'X') and (edDia18.Text <> 'V') and (edDia18.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia18.SetFocus;
  end
  else if ( (edDia18.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia18.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia19Exit(Sender: TObject);
begin
  if (Length(edDia19.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia19.SetFocus;
  end
  else if (edDia19.Text <> 'P') and (edDia19.Text <> 'J') and (edDia19.Text <> 'F') and (edDia19.Text <> 'E') and (edDia19.Text <> 'X') and (edDia19.Text <> 'V') and (edDia19.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia19.SetFocus;
  end
  else if ( (edDia19.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia19.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia20Exit(Sender: TObject);
begin
  if (Length(edDia20.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia20.SetFocus;
  end
  else if (edDia20.Text <> 'P') and (edDia20.Text <> 'J') and (edDia20.Text <> 'F') and (edDia20.Text <> 'E') and (edDia20.Text <> 'X') and (edDia20.Text <> 'V') and (edDia20.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia20.SetFocus;
  end
  else if ( (edDia20.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia20.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia21Exit(Sender: TObject);
begin
  if (Length(edDia21.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia21.SetFocus;
  end
  else if (edDia21.Text <> 'P') and (edDia21.Text <> 'J') and (edDia21.Text <> 'F') and (edDia21.Text <> 'E') and (edDia21.Text <> 'X') and (edDia21.Text <> 'V') and (edDia21.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia21.SetFocus;
  end
  else if ( (edDia21.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia21.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia22Exit(Sender: TObject);
begin
  if (Length(edDia22.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia22.SetFocus;
  end
  else if (edDia22.Text <> 'P') and (edDia22.Text <> 'J') and (edDia22.Text <> 'F') and (edDia22.Text <> 'E') and (edDia22.Text <> 'X') and (edDia22.Text <> 'V') and (edDia22.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia22.SetFocus;
  end
  else if ( (edDia22.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia22.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia23Exit(Sender: TObject);
begin
  if (Length(edDia23.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia23.SetFocus;
  end
  else if (edDia23.Text <> 'P') and (edDia23.Text <> 'J') and (edDia23.Text <> 'F') and (edDia23.Text <> 'E') and (edDia23.Text <> 'X') and (edDia23.Text <> 'V') and (edDia23.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia23.SetFocus;
  end
  else if ( (edDia23.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia23.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia24Exit(Sender: TObject);
begin
  if (Length(edDia24.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia24.SetFocus;
  end
  else if (edDia24.Text <> 'P') and (edDia24.Text <> 'J') and (edDia24.Text <> 'F') and (edDia24.Text <> 'E') and (edDia24.Text <> 'X') and (edDia24.Text <> 'V') and (edDia24.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia24.SetFocus;
  end
  else if ( (edDia24.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia24.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia25Exit(Sender: TObject);
begin
  if (Length(edDia25.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia25.SetFocus;
  end
  else if (edDia25.Text <> 'P') and (edDia25.Text <> 'J') and (edDia25.Text <> 'F') and (edDia25.Text <> 'E') and (edDia25.Text <> 'X') and (edDia25.Text <> 'V') and (edDia25.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia25.SetFocus;
  end
  else if ( (edDia25.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia25.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia26Exit(Sender: TObject);
begin
  if (Length(edDia26.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia26.SetFocus;
  end
  else if (edDia26.Text <> 'P') and (edDia26.Text <> 'J') and (edDia26.Text <> 'F') and (edDia26.Text <> 'E') and (edDia26.Text <> 'X') and (edDia26.Text <> 'V') and (edDia26.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia26.SetFocus;
  end
  else if ( (edDia26.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia26.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia27Exit(Sender: TObject);
begin
  if (Length(edDia27.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia27.SetFocus;
  end
  else if (edDia27.Text <> 'P') and (edDia27.Text <> 'J') and (edDia27.Text <> 'F') and (edDia27.Text <> 'E') and (edDia27.Text <> 'X') and (edDia27.Text <> 'V') and (edDia27.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia27.SetFocus;
  end
  else if ( (edDia27.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia27.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia28Exit(Sender: TObject);
begin
  if (Length(edDia28.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia28.SetFocus;
  end
  else if (edDia28.Text <> 'P') and (edDia28.Text <> 'J') and (edDia28.Text <> 'F') and (edDia28.Text <> 'E') and (edDia28.Text <> 'X') and (edDia28.Text <> 'V') and (edDia28.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia28.SetFocus;
  end
  else if ( (edDia28.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia28.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia29Exit(Sender: TObject);
begin
  if (Length(edDia29.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia29.SetFocus;
  end
  else if (edDia29.Text <> 'P') and (edDia29.Text <> 'J') and (edDia29.Text <> 'F') and (edDia29.Text <> 'E') and (edDia29.Text <> 'X') and (edDia29.Text <> 'V') and (edDia29.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia29.SetFocus;
  end
  else if ( (edDia29.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia29.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia30Exit(Sender: TObject);
begin
  if (Length(edDia30.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia30.SetFocus;
  end
  else if (edDia30.Text <> 'P') and (edDia30.Text <> 'J') and (edDia30.Text <> 'F') and (edDia30.Text <> 'E') and (edDia30.Text <> 'X') and (edDia30.Text <> 'V') and (edDia30.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia30.SetFocus;
  end
  else if ( (edDia30.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia30.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.edDia31Exit(Sender: TObject);
begin
  if (Length(edDia31.Text)=0) then
  begin
    Application.MessageBox ('Sr.(a) Usu�rio(a). O campo N�O pode ficar em branco.',
                            '[Sistema Deca] - ERRO',
                            MB_OK + MB_ICONERROR);
    edDia31.SetFocus;
  end
  else if (edDia31.Text <> 'P') and (edDia31.Text <> 'J') and (edDia31.Text <> 'F') and (edDia31.Text <> 'E') and (edDia31.Text <> 'X') and (edDia31.Text <> 'V') and (edDia31.Text <> 'L') then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia31.SetFocus;
  end
  else if ( (edDia31.Text = 'V') and (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').Value,1,3)='002') ) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Siga o padr�o de preenchimento a seguir: ' + #13+#10 +
                            'P - Presen�a' + #13+#10 +
                            'F - Falta' + #13+#10 +
                            'J - Justificada' + #13+#10 +
                            'X - Dispensa de Atividades, F�rias, Capacita��es, Planejamentos, ...' + #13+#10 +
                            'E - Atividades Externas (Polo, Educom, CIJ, ...)' + #13+#10 +
                            'V - Afastados com Vencimento' + #13+#10 +
                            'L - Afastados sem Vencimento',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
    edDia31.SetFocus;
  end;
end;

procedure TfrmNovoControleFrequencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if vvIND_PERFIL = 1 then vvCOD_UNIDADE := 0;
end;

procedure TfrmNovoControleFrequencia.btnGerarPlanilhaClick(
  Sender: TObject);
//var
  //planilhaFreq, sheetFreq : OleVariant;
  //linhas, linha, coluna, mmMES, num_semanas01 : Integer;
  //vv_STRDATA, str_data_inicial0, mmFORMULA : String;
  //vv_DATA, data_inicial0, data_final0 : TDate;
begin

  //Verificar se os dados de ano, m�s e unidade foram informados...
  if (Length(mskAno.Text) = 0) or (cbMes.ItemIndex = -1) or (vvCOD_UNIDADE < 0) then//(cbUnidade.ItemIndex = -1) then
  begin
    Application.MessageBox ('Aten��o!!' +#13+#10 +
                            'Favor informar ANO, M�S e UNIDADE',
                            '[Sistema Deca] - Informar dados',
                            MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    //Criar a planilha, solicitando o local a ser salvo...
    objExcel := CreateOleObject('Excel.Application');
    objExcel.Workbooks.Add(1);
    SaveDialog1.Execute;
    //SaveDialog1.FileName := cbUnidade.Text + ' - ' + cbMes.Text + '_' + mskAno.Text;
    //objExcel.WorkBooks[1].SaveAs (SaveDialog1.FileName);
    objExcel.Visible := True;
    //objExcel.WorkBooks[1].Sheets.Add();  //Imaginar que a planilha j� exista na pasta

    planilhaFreq := objExcel.WorkBooks[1].WorkSheets[1];
    planilhaFreq.Range['A1','XFD1048576'].Font.Name := 'Tahoma'; // Fonte
    planilhaFreq.Range['A1','XFD1048576'].Font.Size := 8; // Tamanho da Fonte

    //Montar o cabe�alho
    planilhaFreq.Cells[1,1].Value := 'CONTROLE DE FREQU�NCIA DA UNIDADE ' + cbUnidade.Text + ' -  PER�ODO DE REFER�NCIA: ' + cbMes.Text + '/' + mskAno.Text + '  -   Gerada pelo Sistema Deca.';
    planilhaFreq.Range['A1:AL1'].MergeCells := True;
    planilhaFreq.Range['A1:AL1'].Interior.Color := $00ffcf9c;

    //Todo o cabe�alho ter� a mesma cor interna
    planilhaFreq.Range['A2:AL2'].Interior.Color := $F9C8D8;//$00ffcf9c;
    planilhaFreq.Range['A2:AL2'].HorizontalAlignment := 3;
    planilhaFreq.Range['A2:AL2'].VerticalAlignment := 3;

    planilhaFreq.Cells[2,1].Value := '[MATR�CULA]';
    planilhaFreq.Range['A2:A2'].ColumnWidth := 10;

    planilhaFreq.Cells[2,2].Value := '[NOME]';
    planilhaFreq.Range['B2:B2'].ColumnWidth := 55;

    planilhaFreq.Cells[2,3].Value := '[PER�ODO FUNDHAS]';
    planilhaFreq.Range['C2:C2'].ColumnWidth := 20;

    planilhaFreq.Range['D2:AH2'].ColumnWidth := 5;
    planilhaFreq.Cells[2,4].Value  := '[01]';
    planilhaFreq.Cells[2,5].Value  := '[02]';
    planilhaFreq.Cells[2,6].Value  := '[03]';
    planilhaFreq.Cells[2,7].Value  := '[04]';
    planilhaFreq.Cells[2,8].Value  := '[05]';
    planilhaFreq.Cells[2,9].Value  := '[06]';
    planilhaFreq.Cells[2,10].Value := '[07]';
    planilhaFreq.Cells[2,11].Value := '[08]';
    planilhaFreq.Cells[2,12].Value := '[09]';
    planilhaFreq.Cells[2,13].Value := '[10]';
    planilhaFreq.Cells[2,14].Value := '[11]';
    planilhaFreq.Cells[2,15].Value := '[12]';
    planilhaFreq.Cells[2,16].Value := '[13]';
    planilhaFreq.Cells[2,17].Value := '[14]';
    planilhaFreq.Cells[2,18].Value := '[15]';
    planilhaFreq.Cells[2,19].Value := '[16]';
    planilhaFreq.Cells[2,20].Value := '[17]';
    planilhaFreq.Cells[2,21].Value := '[18]';
    planilhaFreq.Cells[2,22].Value := '[19]';
    planilhaFreq.Cells[2,23].Value := '[20]';
    planilhaFreq.Cells[2,24].Value := '[21]';
    planilhaFreq.Cells[2,25].Value := '[22]';
    planilhaFreq.Cells[2,26].Value := '[23]';
    planilhaFreq.Cells[2,27].Value := '[24]';
    planilhaFreq.Cells[2,28].Value := '[25]';
    planilhaFreq.Cells[2,29].Value := '[26]';
    planilhaFreq.Cells[2,30].Value := '[27]';
    planilhaFreq.Cells[2,31].Value := '[28]';
    planilhaFreq.Cells[2,32].Value := '[29]';
    planilhaFreq.Cells[2,33].Value := '[30]';
    planilhaFreq.Cells[2,34].Value := '[31]';

    planilhaFreq.Range['AI2:AL2'].ColumnWidth := 9;
    planilhaFreq.Cells[2,35].Value := '[Faltas]';
    planilhaFreq.Cells[2,36].Value := '[Justificadas]';
    planilhaFreq.Cells[2,37].Value := '[Com Venc.]';
    planilhaFreq.Cells[2,38].Value := '[Sem Venc.]';

    //Atribuir os valores ao da inicializa��o da frequ�ncia...
    mmMES := cbMes.ItemIndex + 1;
    linha := 3;
    //Valida ANO, M�S e UNIDADE (que devem ser informados/selecionados)
    if (mskAno.Text <> '') and (cbMes.ItemIndex > -1) and (vvCOD_UNIDADE >= 0) then
    begin
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value   := Null;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
        Open;

        mmMATRICULA := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
        mmNOME      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
        mmPERIODO   := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;

        dmDeca.cdsSel_Cadastro.First;
        while not (dmDeca.cdsSel_Cadastro.eof) do
        begin

          planilhaFreq.Cells[linha,1].Value := mmMATRICULA;
          planilhaFreq.Cells[linha,2].Value := mmNOME;
          planilhaFreq.Cells[linha,3].Value := mmPERIODO;

          //**************************************************************************************************************************************************************************
          //Dia 01
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '01' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '01' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 1;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,4].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,4].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,4].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,4].Value := 'S'
              else
                planilhaFreq.Cells[linha,4].Value := '-';
            end;
          end; //Fim DIA 01
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 02
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '02' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '02' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 2;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,5].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,5].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,5].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,5].Value := 'S'
              else
                planilhaFreq.Cells[linha,5].Value := '-';
            end;
          end;  //Fim DIA 02
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 03
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '03' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '03' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 3;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,6].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,6].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,6].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,6].Value := 'S'
              else
                planilhaFreq.Cells[linha,6].Value := '-';
            end;
          end;  //Fim DIA 03
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 04
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '04' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '04' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 4;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,7].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,7].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,7].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,7].Value := 'S'
              else
                planilhaFreq.Cells[linha,7].Value := '-';
            end;
          end;  //Fim DIA 04
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 05
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '05' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '05' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 5;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,8].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,8].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,8].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,8].Value := 'S'
              else
                planilhaFreq.Cells[linha,8].Value := '-';
            end;
          end;  //Fim DIA 05
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 06
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '06' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '06' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 6;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,9].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,9].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,9].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,9].Value := 'S'
              else
                planilhaFreq.Cells[linha,9].Value := '-';
            end;
          end;  //Fim DIA 06
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 07
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '07' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '07' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 7;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,10].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,10].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,10].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,10].Value := 'S'
              else
                planilhaFreq.Cells[linha,10].Value := '-';
            end;
          end;  //Fim DIA 07
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 08
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '08' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '08' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 8;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,11].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,11].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,11].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,11].Value := 'S'
              else
                planilhaFreq.Cells[linha,11].Value := '-';
            end;
          end;  //Fim DIA 08
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 09
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '09' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '09' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 9;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,12].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,12].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,12].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,12].Value := 'S'
              else
                planilhaFreq.Cells[linha,12].Value := '-';
            end;
          end;  //Fim DIA 09
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 10
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '10' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '10' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 10;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,13].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,13].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,13].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,13].Value := 'S'
              else
                planilhaFreq.Cells[linha,13].Value := '-';
            end;
          end;  //Fim DIA 10
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 11
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '11' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '11' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 11;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,14].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,14].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,14].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,14].Value := 'S'
              else
                planilhaFreq.Cells[linha,14].Value := '-';
            end;
          end;  //Fim DIA 11
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 12
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '12' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '12' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 12;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,15].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,15].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,15].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,15].Value := 'S'
              else
                planilhaFreq.Cells[linha,15].Value := '-';
            end;
          end;  //Fim DIA 12
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 13
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '13' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '13' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 13;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,16].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,16].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,16].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,16].Value := 'S'
              else
                planilhaFreq.Cells[linha,16].Value := '-';
            end;
          end;  //Fim DIA 13
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 14
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '14' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '14' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 14;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,17].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,17].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,17].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,17].Value := 'S'
              else
                planilhaFreq.Cells[linha,17].Value := '-';
            end;
          end;  //Fim DIA 14
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 15
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '15' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '15' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 15;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,18].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,18].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,18].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,18].Value := 'S'
              else
                planilhaFreq.Cells[linha,18].Value := '-';
            end;
          end;  //Fim DIA 15
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 16
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '16' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '16' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 16;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,19].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,19].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,19].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,19].Value := 'S'
              else
                planilhaFreq.Cells[linha,19].Value := '-';
            end;
          end;  //Fim DIA 16
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 17
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '17' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '17' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 17;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,20].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,20].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,20].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,20].Value := 'S'
              else
                planilhaFreq.Cells[linha,20].Value := '-';
            end;
          end;  //Fim DIA 17
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 18
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '18' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '18' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 18;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,21].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,21].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,21].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,21].Value := 'S'
              else
                planilhaFreq.Cells[linha,21].Value := '-';
            end;
          end;  //Fim DIA 18
          //**************************************************************************************************************************************************************************
          
          //**************************************************************************************************************************************************************************
          //Dia 19
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '19' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '19' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 19;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,22].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,22].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,22].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,22].Value := 'S'
              else
                planilhaFreq.Cells[linha,22].Value := '-';
            end;
          end;  //Fim DIA 19
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 20
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '20' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '20' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 20;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,23].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,23].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,23].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,23].Value := 'S'
              else
                planilhaFreq.Cells[linha,23].Value := '-';
            end;
          end;  //Fim DIA 20
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 21
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '21' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '21' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 21;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,24].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,24].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,24].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,24].Value := 'S'
              else
                planilhaFreq.Cells[linha,24].Value := '-';
            end;
          end;  //Fim DIA 21
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 22
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '22' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '22' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 22;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,25].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,25].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,25].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,25].Value := 'S'
              else
                planilhaFreq.Cells[linha,25].Value := '-';
            end;
          end;  //Fim DIA 22
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 23
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '23' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '23' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 23;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,26].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,26].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,26].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,26].Value := 'S'
              else
                planilhaFreq.Cells[linha,26].Value := '-';
            end;
          end;  //Fim DIA 23
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 24
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '24' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '24' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 24;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,27].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,27].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,27].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,27].Value := 'S'
              else
                planilhaFreq.Cells[linha,27].Value := '-';
            end;
          end;  //Fim DIA 24
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 25
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '25' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '25' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 25;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,28].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,28].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,28].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,28].Value := 'S'
              else
                planilhaFreq.Cells[linha,28].Value := '-';
            end;
          end;  //Fim DIA 25
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 26
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '26' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '26' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 26;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,29].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,29].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,29].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,29].Value := 'S'
              else
                planilhaFreq.Cells[linha,29].Value := '-';
            end;
          end;  //Fim DIA 26
          //**************************************************************************************************************************************************************************
                
          //**************************************************************************************************************************************************************************
          //Dia 27
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '27' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '27' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 27;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,30].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,30].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,30].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,30].Value := 'S'
              else
                planilhaFreq.Cells[linha,30].Value := '-';
            end;
          end;  //Fim DIA 27
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 28
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '28' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '28' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 28;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                planilhaFreq.Cells[linha,31].Value := 'R'
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                planilhaFreq.Cells[linha,31].Value := 'C';
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
                planilhaFreq.Cells[linha,31].Value := 'D'
              else if (DayOfWeek(vv_DATA)= 7) then
                planilhaFreq.Cells[linha,31].Value := 'S'
              else
                planilhaFreq.Cells[linha,31].Value := '-';
            end;
          end;  //Fim DIA 28
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 29
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '29' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '29' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;

          case mmMES of
            1,3,5,7,8,10,12: begin

               vv_DATA := StrToDate(vv_STRDATA);
               //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
               with dmDeca.cdsSel_ItensCalendario do
               begin
                 Close;
                 Params.ParamByName('@pe_cod_calendario').Value   := Null;
                 Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                 Params.ParamByName('@pe_num_dia').AsInteger      := 29;
                 Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
                 Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
                 Params.ParamByName('@pe_num_semana').Value       := Null;
                 Open;

                 //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                 if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
                 begin
                   if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                     planilhaFreq.Cells[linha,32].Value := 'R'
                   else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                     planilhaFreq.Cells[linha,32].Value := 'C';
                 end
                 else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
                 begin
                   if (DayOfWeek(vv_DATA)= 1) then
                     planilhaFreq.Cells[linha,32].Value := 'D'
                   else if (DayOfWeek(vv_DATA)= 7) then
                     planilhaFreq.Cells[linha,32].Value := 'S'
                   else
                     planilhaFreq.Cells[linha,32].Value := '-';
                 end;
               end;
            end;

            2,4,6,9,11: begin
              planilhaFreq.Cells[linha,32].Value := '-';
            end;
          end; //case
          //end;  //Fim DIA 29
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 30
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '30' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '30' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;

          case mmMES of
            1,3,5,7,8,10,12: begin

               vv_DATA := StrToDate(vv_STRDATA);
               //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
               with dmDeca.cdsSel_ItensCalendario do
               begin
                 Close;
                 Params.ParamByName('@pe_cod_calendario').Value   := Null;
                 Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                 Params.ParamByName('@pe_num_dia').AsInteger      := 30;
                 Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
                 Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
                 Params.ParamByName('@pe_num_semana').Value       := Null;
                 Open;

                 //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                 if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
                 begin
                   if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                     planilhaFreq.Cells[linha,33].Value := 'R'
                   else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                     planilhaFreq.Cells[linha,33].Value := 'C';
                 end
                 else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
                 begin
                   if (DayOfWeek(vv_DATA)= 1) then
                     planilhaFreq.Cells[linha,33].Value := 'D'
                   else if (DayOfWeek(vv_DATA)= 7) then
                     planilhaFreq.Cells[linha,33].Value := 'S'
                   else
                     planilhaFreq.Cells[linha,33].Value := '-';
                 end;
               end;
            end;

            2,4,6,9,11: begin
              planilhaFreq.Cells[linha,33].Value := '-';
            end;
          end; //case
          //end;  //Fim DIA 30
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 31
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '31' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '31' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;

          case mmMES of
            1,3,5,7,8,10,12: begin

               vv_DATA := StrToDate(vv_STRDATA);
               //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
               with dmDeca.cdsSel_ItensCalendario do
               begin
                 Close;
                 Params.ParamByName('@pe_cod_calendario').Value   := Null;
                 Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                 Params.ParamByName('@pe_num_dia').AsInteger      := 31;
                 Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
                 Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
                 Params.ParamByName('@pe_num_semana').Value       := Null;
                 Open;

                 //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                 if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
                 begin
                   if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
                     planilhaFreq.Cells[linha,34].Value := 'R'
                   else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                     planilhaFreq.Cells[linha,34].Value := 'C';
                 end
                 else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
                 begin
                   if (DayOfWeek(vv_DATA)= 1) then
                     planilhaFreq.Cells[linha,34].Value := 'D'
                   else if (DayOfWeek(vv_DATA)= 7) then
                     planilhaFreq.Cells[linha,34].Value := 'S'
                   else
                     planilhaFreq.Cells[linha,34].Value := '-';
                 end;
               end;
            end;

            2,4,6,9,11: begin
              planilhaFreq.Cells[linha,34].Value := '-';
            end;
          end; //case
          //end;  //Fim DIA 30
          //**************************************************************************************************************************************************************************

          //Monta a string que vai "funcionar" como a f�rmula
          //mmFORMULA := '=CONTA.SE($D' + IntToStr(linha) + ':$AH' + IntToStr(linha) + ';"F")';
          //planilhaFreq.Cells[linha,35].NumberFormat := '0';
          //planilhaFreq.Cells[linha,35].Formula := mmFORMULA; //Buscar outras solu��es ***** N�O FUNCIONA
          planilhaFreq.Cells[linha,36].Value := '0';
          planilhaFreq.Cells[linha,37].Value := '0';
          planilhaFreq.Cells[linha,38].Value := '0';

          dmDeca.cdsSel_Cadastro.Next;
          mmMATRICULA := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
          mmNOME      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
          mmPERIODO   := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;
          linha := linha + 1;

        end;  //Fim while not (dmDeca.cdsSel_Cadastro......)

        planilhaFreq.columns.Autofit;

      end; //Fim with dmDeca.cdsSel_Cadastro...
      //planilhaFreq.Close;
    end;
  end;
end;

procedure TfrmNovoControleFrequencia.btnImportarFrequenciaClick(
  Sender: TObject);
var
  total_de_linhas, linha, coluna : Integer;

  mmDIA01, mmDIA02, mmDIA03, mmDIA04, mmDIA05, mmDIA06, mmDIA07, mmDIA08, mmDIA09, mmDIA10,
  mmDIA11, mmDIA12, mmDIA13, mmDIA14, mmDIA15, mmDIA16, mmDIA17, mmDIA18, mmDIA19, mmDIA20,
  mmDIA21, mmDIA22, mmDIA23, mmDIA24, mmDIA25, mmDIA26, mmDIA27, mmDIA28, mmDIA29, mmDIA30,
  mmDIA31, mmFALTAS, mmJUSTIF, mmCOMVENC, mmSEMVENC: String;

begin

  //Verificar se os dados de ano, m�s e unidade foram informados...
  if (Length(mskAno.Text) = 0) or (cbMes.ItemIndex = -1) or (vvCOD_UNIDADE < 0) then
  begin
    Application.MessageBox ('Aten��o!!' +#13+#10 +
                            'Favor informar ANO, M�S e UNIDADE',
                            '[Sistema Deca] - Informar dados',
                            MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    //Carregar a planilha a ser importada - Modelo gerado pelo Sistema
    //No modelo gerado no Sistema, existem 36 colunas
    //Criar o objeto que instanciar� o excel e abre o arquivo
    if OpenDialog1.Execute then
    begin
      objExcel := CreateOleObject('Excel.Application');
      objExcel.WorkBooks.Open(OpenDialog1.FileName);
      planilhaFreq := objExcel.WorkSheets[1];

      //total_de_linhas := planilhaFreq.Cells.SpecialCells(11).Row - 2;
      total_de_linhas := planilhaFreq.Cells.SpecialCells(11).Row;
      linha := 3;
      coluna := 1;

      while (linha <= total_de_linhas) do
      begin
        mmMATRICULA := planilhaFreq.Cells[linha,coluna].Value;
        mmMATRICULA := '00'+mmMATRICULA;
        mmNOME      := planilhaFreq.Cells[linha,coluna+1].Value;
        mmDIA01     := planilhaFreq.Cells[linha,coluna+3].Value;
        mmDIA02     := planilhaFreq.Cells[linha,coluna+4].Value;
        mmDIA03     := planilhaFreq.Cells[linha,coluna+5].Value;
        mmDIA04     := planilhaFreq.Cells[linha,coluna+6].Value;
        mmDIA05     := planilhaFreq.Cells[linha,coluna+7].Value;
        mmDIA06     := planilhaFreq.Cells[linha,coluna+8].Value;
        mmDIA07     := planilhaFreq.Cells[linha,coluna+9].Value;
        mmDIA08     := planilhaFreq.Cells[linha,coluna+10].Value;
        mmDIA09     := planilhaFreq.Cells[linha,coluna+11].Value;
        mmDIA10     := planilhaFreq.Cells[linha,coluna+12].Value;
        mmDIA11     := planilhaFreq.Cells[linha,coluna+13].Value;
        mmDIA12     := planilhaFreq.Cells[linha,coluna+14].Value;
        mmDIA13     := planilhaFreq.Cells[linha,coluna+15].Value;
        mmDIA14     := planilhaFreq.Cells[linha,coluna+16].Value;
        mmDIA15     := planilhaFreq.Cells[linha,coluna+17].Value;
        mmDIA16     := planilhaFreq.Cells[linha,coluna+18].Value;
        mmDIA17     := planilhaFreq.Cells[linha,coluna+19].Value;
        mmDIA18     := planilhaFreq.Cells[linha,coluna+20].Value;
        mmDIA19     := planilhaFreq.Cells[linha,coluna+21].Value;
        mmDIA20     := planilhaFreq.Cells[linha,coluna+22].Value;
        mmDIA21     := planilhaFreq.Cells[linha,coluna+23].Value;
        mmDIA22     := planilhaFreq.Cells[linha,coluna+24].Value;
        mmDIA23     := planilhaFreq.Cells[linha,coluna+25].Value;
        mmDIA24     := planilhaFreq.Cells[linha,coluna+26].Value;
        mmDIA25     := planilhaFreq.Cells[linha,coluna+27].Value;
        mmDIA26     := planilhaFreq.Cells[linha,coluna+28].Value;
        mmDIA27     := planilhaFreq.Cells[linha,coluna+29].Value;
        mmDIA28     := planilhaFreq.Cells[linha,coluna+30].Value;
        mmDIA29     := planilhaFreq.Cells[linha,coluna+31].Value;
        mmDIA30     := planilhaFreq.Cells[linha,coluna+32].Value;
        mmDIA31     := planilhaFreq.Cells[linha,coluna+33].Value;
        mmFALTAS    := planilhaFreq.Cells[linha,coluna+34].Value;
        mmJUSTIF    := planilhaFreq.Cells[linha,coluna+35].Value;
        mmCOMVENC   := planilhaFreq.Cells[linha,coluna+36].Value;
        mmSEMVENC   := planilhaFreq.Cells[linha,coluna+37].Value;

        //Verificar a exist�ncia ou n�o do registro a ser inserido/alterado
        try
          with dmDeca.cdsSel_Cadastro_Frequencia do
          begin
            Close;
            Params.ParamByName ('@pe_id_frequencia').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value   := mmMATRICULA;
            Params.ParamByName ('@pe_cod_unidade').Value     := Null;
            Params.ParamByName ('@pe_num_ano').Value         := mskAno.Text;
            Params.ParamByName ('@pe_num_mes').Value         := cbMes.ItemIndex + 1;
            Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
            Params.ParamByName ('@pe_cod_turma').Value       := Null;
            Params.ParamByName ('@pe_num_cotas').Value       := Null;
            Open;

            //Se n�o encontrou, fa�a a inclus�o dos dados
            if (dmDeca.cdsSel_Cadastro_Frequencia.RecordCount < 1) then
            begin
              with (dmDeca.cdsInc_Cadastro_Frequencia) do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value   := mmMATRICULA;
                Params.ParamByName ('@pe_num_ano').Value         := StrToInt(mskAno.Text);
                Params.ParamByName ('@pe_num_mes').Value         := cbMes.ItemIndex + 1;
                Params.ParamByName ('@pe_flg_dia01').Value       := mmDIA01;
                Params.ParamByName ('@pe_flg_dia02').Value       := mmDIA02;
                Params.ParamByName ('@pe_flg_dia03').Value       := mmDIA03;
                Params.ParamByName ('@pe_flg_dia04').Value       := mmDIA04;
                Params.ParamByName ('@pe_flg_dia05').Value       := mmDIA05;
                Params.ParamByName ('@pe_flg_dia06').Value       := mmDIA06;
                Params.ParamByName ('@pe_flg_dia07').Value       := mmDIA07;
                Params.ParamByName ('@pe_flg_dia08').Value       := mmDIA08;
                Params.ParamByName ('@pe_flg_dia09').Value       := mmDIA09;
                Params.ParamByName ('@pe_flg_dia10').Value       := mmDIA10;
                Params.ParamByName ('@pe_flg_dia11').Value       := mmDIA11;
                Params.ParamByName ('@pe_flg_dia12').Value       := mmDIA12;
                Params.ParamByName ('@pe_flg_dia13').Value       := mmDIA13;
                Params.ParamByName ('@pe_flg_dia14').Value       := mmDIA14;
                Params.ParamByName ('@pe_flg_dia15').Value       := mmDIA15;
                Params.ParamByName ('@pe_flg_dia16').Value       := mmDIA16;
                Params.ParamByName ('@pe_flg_dia17').Value       := mmDIA17;
                Params.ParamByName ('@pe_flg_dia18').Value       := mmDIA18;
                Params.ParamByName ('@pe_flg_dia19').Value       := mmDIA19;
                Params.ParamByName ('@pe_flg_dia20').Value       := mmDIA20;
                Params.ParamByName ('@pe_flg_dia21').Value       := mmDIA21;
                Params.ParamByName ('@pe_flg_dia22').Value       := mmDIA22;
                Params.ParamByName ('@pe_flg_dia23').Value       := mmDIA23;
                Params.ParamByName ('@pe_flg_dia24').Value       := mmDIA24;
                Params.ParamByName ('@pe_flg_dia25').Value       := mmDIA25;
                Params.ParamByName ('@pe_flg_dia26').Value       := mmDIA26;
                Params.ParamByName ('@pe_flg_dia27').Value       := mmDIA27;
                Params.ParamByName ('@pe_flg_dia28').Value       := mmDIA28;
                Params.ParamByName ('@pe_flg_dia29').Value       := mmDIA29;
                Params.ParamByName ('@pe_flg_dia30').Value       := mmDIA30;
                Params.ParamByName ('@pe_flg_dia31').Value       := mmDIA31;
                Params.ParamByName ('@pe_num_faltas').Value      := mmFALTAS;
                Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                Params.ParamByName ('@pe_semana_dia01').Value    := 0;
                Params.ParamByName ('@pe_semana_dia02').Value    := 0;
                Params.ParamByName ('@pe_semana_dia03').Value    := 0;
                Params.ParamByName ('@pe_semana_dia04').Value    := 0;
                Params.ParamByName ('@pe_semana_dia05').Value    := 0;
                Params.ParamByName ('@pe_semana_dia06').Value    := 0;
                Params.ParamByName ('@pe_semana_dia07').Value    := 0;
                Params.ParamByName ('@pe_semana_dia08').Value    := 0;
                Params.ParamByName ('@pe_semana_dia09').Value    := 0;
                Params.ParamByName ('@pe_semana_dia10').Value    := 0;
                Params.ParamByName ('@pe_semana_dia11').Value    := 0;
                Params.ParamByName ('@pe_semana_dia12').Value    := 0;
                Params.ParamByName ('@pe_semana_dia13').Value    := 0;
                Params.ParamByName ('@pe_semana_dia14').Value    := 0;
                Params.ParamByName ('@pe_semana_dia15').Value    := 0;
                Params.ParamByName ('@pe_semana_dia16').Value    := 0;
                Params.ParamByName ('@pe_semana_dia17').Value    := 0;
                Params.ParamByName ('@pe_semana_dia18').Value    := 0;
                Params.ParamByName ('@pe_semana_dia19').Value    := 0;
                Params.ParamByName ('@pe_semana_dia20').Value    := 0;
                Params.ParamByName ('@pe_semana_dia21').Value    := 0;
                Params.ParamByName ('@pe_semana_dia22').Value    := 0;
                Params.ParamByName ('@pe_semana_dia23').Value    := 0;
                Params.ParamByName ('@pe_semana_dia24').Value    := 0;
                Params.ParamByName ('@pe_semana_dia25').Value    := 0;
                Params.ParamByName ('@pe_semana_dia26').Value    := 0;
                Params.ParamByName ('@pe_semana_dia27').Value    := 0;
                Params.ParamByName ('@pe_semana_dia28').Value    := 0;
                Params.ParamByName ('@pe_semana_dia29').Value    := 0;
                Params.ParamByName ('@pe_semana_dia30').Value    := 0;
                Params.ParamByName ('@pe_semana_dia31').Value    := 0;
                Params.ParamByName ('@pe_dsr_acumulado').Value   := 0;
                Params.ParamByName ('@pe_flg_exportou').Value    := 0;
                Params.ParamByName ('@pe_dsc_observacoes').Value := Null;
                Params.ParamByName ('@pe_num_justificadas').Value:= mmJUSTIF;
                Params.ParamByName ('@pe_num_dias_uteis').Value  := 0;
                Params.ParamByName ('@pe_num_comvenc').Value     := mmCOMVENC;
                Params.ParamByName ('@pe_num_semvenc').Value     := mmSEMVENC;

                Execute;
              end;
            end
            else if (dmDeca.cdsSel_Cadastro_Frequencia.RecordCount = 1) then //and (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_exportou').Value = 0) then
            //Sen�o fa�a a altera��o dos dados
            begin
              with dmDeca.cdsAlt_Cadastro_Frequencia do
              begin
                Close;
                Params.ParamByName ('@pe_id_frequencia').Value   := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('id_frequencia').Value;
                Params.ParamByName ('@pe_dia01').Value           := mmDIA01;
                Params.ParamByName ('@pe_dia02').Value           := mmDIA02;
                Params.ParamByName ('@pe_dia03').Value           := mmDIA03;
                Params.ParamByName ('@pe_dia04').Value           := mmDIA04;
                Params.ParamByName ('@pe_dia05').Value           := mmDIA05;
                Params.ParamByName ('@pe_dia06').Value           := mmDIA06;
                Params.ParamByName ('@pe_dia07').Value           := mmDIA07;
                Params.ParamByName ('@pe_dia08').Value           := mmDIA08;
                Params.ParamByName ('@pe_dia09').Value           := mmDIA09;
                Params.ParamByName ('@pe_dia10').Value           := mmDIA10;
                Params.ParamByName ('@pe_dia11').Value           := mmDIA11;
                Params.ParamByName ('@pe_dia12').Value           := mmDIA12;
                Params.ParamByName ('@pe_dia13').Value           := mmDIA13;
                Params.ParamByName ('@pe_dia14').Value           := mmDIA14;
                Params.ParamByName ('@pe_dia15').Value           := mmDIA15;
                Params.ParamByName ('@pe_dia16').Value           := mmDIA16;
                Params.ParamByName ('@pe_dia17').Value           := mmDIA17;
                Params.ParamByName ('@pe_dia18').Value           := mmDIA18;
                Params.ParamByName ('@pe_dia19').Value           := mmDIA19;
                Params.ParamByName ('@pe_dia20').Value           := mmDIA20;
                Params.ParamByName ('@pe_dia21').Value           := mmDIA21;
                Params.ParamByName ('@pe_dia22').Value           := mmDIA22;
                Params.ParamByName ('@pe_dia23').Value           := mmDIA23;
                Params.ParamByName ('@pe_dia24').Value           := mmDIA24;
                Params.ParamByName ('@pe_dia25').Value           := mmDIA25;
                Params.ParamByName ('@pe_dia26').Value           := mmDIA26;
                Params.ParamByName ('@pe_dia27').Value           := mmDIA27;
                Params.ParamByName ('@pe_dia28').Value           := mmDIA28;
                Params.ParamByName ('@pe_dia29').Value           := mmDIA29;
                Params.ParamByName ('@pe_dia30').Value           := mmDIA30;
                Params.ParamByName ('@pe_dia31').Value           := mmDIA31;
                Params.ParamByName ('@pe_num_faltas').Value      := mmFALTAS;
                Params.ParamByName ('@pe_dsr_acumulado').Value   := Null;
                Params.ParamByName ('@pe_num_justificadas').Value:= mmJUSTIF;
                Params.ParamByName ('@pe_num_dias_uteis').Value  := 0;
                Params.ParamByName ('@pe_num_comvenc').Value     := mmCOMVENC;
                Params.ParamByName ('@pe_num_semvenc').Value     := mmSEMVENC;
                Execute;
              end;
            end;
          end;
        except end;

        linha := linha + 1;

      end;

      ShowMessage ('Importa��o finalizada');

    end;
  end;

end;

procedure TfrmNovoControleFrequencia.btnEstatisticasClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_EstatisticasAbsenteismo do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Params.ParamByName ('@pe_num_ano').Value     := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_num_mes').Value     := cbMes.ItemIndex + 1;
      Open;

      Application.CreateForm (TrelEstatisticaAbsenteismo, relEstatisticaAbsenteismo);
      relEstatisticaAbsenteismo.lbTitulo.Caption := 'REFER�NCIA: ' + cbMes.Text + ' de '  + mskAno.Text  + ' - ' + cbUnidade.Text;
      relEstatisticaAbsenteismo.Preview;
      relEstatisticaAbsenteismo.Free;

    end;
  except end;
end;

procedure TfrmNovoControleFrequencia.btnGeraCSVClick(Sender: TObject);
var fileGERALCSV : TextFile;
    mmMes, linha : Integer;
    mmNOME_ARQUIVO : String;
begin
  //AssignFile (fileGERALCSV, ExtractFilePath(Application.ExeName) + 'GERAL-TESTE.CSV');
  //Monta o nome do arquivo
  if (vvIND_PERFIL = 1 ) then //Administrador -> recebe os valores do arquivo setados em tela
  begin
    mmNOME_ARQUIVO := 'Frequencia ' + cbUnidade.Text + ' - Ref ' + cbMes.Text + ' de ' + mskAno.Text + '.CSV';
    AssignFile (fileGERALCSV, ExtractFilePath(Application.ExeName) + mmNOME_ARQUIVO);
  end
  else if (vvIND_PERFIL = 2) then
  begin
    mmNOME_ARQUIVO := 'Frequencia ' + vvNOM_UNIDADE + ' - Ref ' + cbMes.Text + ' de ' + mskAno.Text + '.CSV';
    AssignFile (fileGERALCSV, ExtractFilePath(Application.ExeName) + mmNOME_ARQUIVO);
  end;

  ReWrite (fileGERALCSV);

  Write (fileGERALCSV, 'MATRICULA');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, 'NOME');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, 'PERIODO');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '01');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '02');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '03');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '04');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '05');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '06');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '07');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '08');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '09');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '10');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '11');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '12');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '13');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '14');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '15');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '16');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '17');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '18');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '19');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '20');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '21');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '22');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '23');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '24');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '25');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '26');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '27');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '28');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '29');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '30');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, '31');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, 'FALTAS');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, 'JUSTIFICADAS');
  Write (fileGERALCSV, ';');
  Write (fileGERALCSV, 'COM VENCIMENTO');
  Write (fileGERALCSV, ';');
  Writeln (fileGERALCSV, 'SEM VENCIMENTO');

  try
    mmMES := cbMes.ItemIndex + 1;
    linha := 3;
    //Valida ANO, M�S e UNIDADE (que devem ser informados/selecionados)
    if (mskAno.Text <> '') and (cbMes.ItemIndex > -1) and (vvCOD_UNIDADE >= 0) then
    begin
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value   := Null;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
        Open;

        mmMATRICULA := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
        mmNOME      := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
        mmPERIODO   := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;

        dmDeca.cdsSel_Cadastro.First;
        while not (dmDeca.cdsSel_Cadastro.eof) do
        begin

          //Repassa para o arquivo os dados de cadastro MATRICULA, NOME e PER�ODO FUNDHAS
          Write (fileGERALCSV, dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value);
          Write (fileGERALCSV, ';');
          Write (fileGERALCSV, dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value);
          Write (fileGERALCSV, ';');
          Write (fileGERALCSV, dmDeca.cdsSel_Cadastro.FieldByName('dsc_periodo').Value);

          //**************************************************************************************************************************************************************************
          //Dia 01
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '01' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '01' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 1;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 01
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 02
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '02' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '02' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 2;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 02
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 03
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '03' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '03' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 3;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 03
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 04
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '04' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '04' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 4;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 04
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 05
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '05' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '05' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 5;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 05
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 06
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '06' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '06' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 6;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 06
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 07
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '07' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '07' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 7;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 07
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 08
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '08' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '08' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 8;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 08
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 09
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '09' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '09' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 9;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 09
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 10
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '10' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '10' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 10;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 10
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 11
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '11' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '11' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 11;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 11
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 12
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '12' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '12' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 12;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 12
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 13
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '13' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '13' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 13;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 13
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 14
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '14' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '14' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 14;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 14
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 15
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '15' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '15' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 15;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 15
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 16
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '16' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '16' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 16;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 16
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 17
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '17' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '17' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 17;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 17
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 18
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '18' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '18' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 18;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 18
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 19
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '19' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '19' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 19;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 19
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 20
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '20' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '20' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 20;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 20
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 21
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '21' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '21' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 21;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 21
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 22
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '22' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '22' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 22;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 22
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 23
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '23' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '23' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 23;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 23
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 24
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '24' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '24' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 24;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 24
          //**************************************************************************************************************************************************************************
          //**************************************************************************************************************************************************************************
          //Dia 25
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '25' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '25' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 25;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 25
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 26
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '26' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '26' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 26;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 26
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 27
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '27' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '27' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 27;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 27
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 28
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '28' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '28' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 28;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 28
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 29
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '29' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '29' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 29;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 29
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 30
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '30' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '30' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 30;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 30
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          //Dia 31
          if Length(IntToStr(mmMES)) = 1 then vv_STRDATA := '31' + '/0' + IntToStr(mmMES) + '/' + mskAno.Text
          else vv_STRDATA := '31' + '/' + IntToStr(mmMES) + '/' + mskAno.Text;
          vv_DATA := StrToDate(vv_STRDATA);

          //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
          with dmDeca.cdsSel_ItensCalendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value   := Null;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
            Params.ParamByName('@pe_num_dia').AsInteger      := 31;
            Params.ParamByName('@pe_num_mes').AsInteger      := mmMES;
            Params.ParamByName('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
            Params.ParamByName('@pe_num_semana').Value       := Null;
            Open;

            //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
            if (dmDeca.cdsSel_ItensCalendario.RecordCount = 1) then
            begin
              if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then
              begin
                  Write (fileGERALCSV, ';');
                  Write (fileGERALCSV, 'R');
              end
              else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'C');
              end
            end
            else if (dmDeca.cdsSel_ItensCalendario.RecordCount = 0) then
            begin
              if (DayOfWeek(vv_DATA)= 1) then
              begin
                 Write (fileGERALCSV, ';');
                 Write (fileGERALCSV, 'D');
              end
              else if (DayOfWeek(vv_DATA)= 7) then
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, 'S');
              end
              else
              begin
                Write (fileGERALCSV, ';');
                Write (fileGERALCSV, '-');
              end;
            end;
          end; //Fim DIA 31
          //**************************************************************************************************************************************************************************

          //**************************************************************************************************************************************************************************
          // Repassa a f�rmula  de TOTAL DE FALTAS para o arquivo
          //Write (fileGERALCSV, ';');
          //Write (fileGERALCSV, '=CONT.SE($D3:$AF31;"F"');
          //Write (fileGERALCSV, ';');
          //Writeln (fileGERALCSV, '=CONT.SE($D3:$AF31;"J")');
          Writeln (fileGERALCSV, ';');
          dmDeca.cdsSel_Cadastro.Next;

        end;
      end;
    end
    except

    end;
end;

end.
