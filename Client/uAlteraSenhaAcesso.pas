unit uAlteraSenhaAcesso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, Buttons;

type
  TfrmAlteraSenhaAcesso = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    lbNomeUsuario: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnAlterarSenha: TSpeedButton;
    Label1: TLabel;
    edSenhaNova: TEdit;
    edConfirmaSenhaNova: TEdit;
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure btnAlterarSenhaClick(Sender: TObject);
    procedure mskConfirmaSenhaNovaExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteraSenhaAcesso: TfrmAlteraSenhaAcesso;
  vUnidade1, vUnidade2 : TStringList;
  vvSENHA_OK : Boolean;

implementation

uses uPrincipal, uAcesso, uUtil, uDM;

{$R *.DFM}

procedure TfrmAlteraSenhaAcesso.FormShow(Sender: TObject);
begin
  lbNomeUsuario.Caption := vvNOM_USUARIO;
  vvSENHA_OK := False;
end;

procedure TfrmAlteraSenhaAcesso.cbUnidadeClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_cod_perfil').Value := Null;
      Params.ParamByName ('@pe_flg_ativo').Value := Null;  //Validar somente usu�rio ATIVO
      Params.ParamByName ('@pe_flg_online').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Open;

      if RecordCount > 0 then
      begin
        //Recupera as vari�veis a serem usadas no sistema
        vvIND_PERFIL := FieldByName ('cod_perfil').Value;
        vvCOD_UNIDADE := FieldByName('cod_unidade').Value;
        vvNOM_UNIDADE := FieldByName('nom_unidade').Value;
        vvCOD_ID_ACESSO := FieldByName('cod_id_acesso').Value;
      end;
    end;
  except
    //Mensagem de erro de conex�o...
  end;
end;

procedure TfrmAlteraSenhaAcesso.btnAlterarSenhaClick(Sender: TObject);
begin
  try

    if (Application.MessageBox ('Deseja alterar sua senha de acesso no Sistema Deca?',
                               '[Sistema Deca] - Altera��o Senha de Acesso',
                               MB_YESNO + MB_ICONQUESTION) = idYes) then
    begin

      if (edSenhaNova.Text = edConfirmaSenhaNova.Text) then
      begin
        with dmDeca.cdsAlt_AlteraSenha do
        begin
          Close;
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Params.ParamByName ('@pe_dsc_senha').Value   := GetValue(edSenhaNova.Text);
          Execute;

          Application.MessageBox ('Sua senha foi alterada com sucesso. Apartir do pr�ximo LOGIN utilize essa senha.',
                                  '[Sistema Deca] - Altera��o de Senha.',
                                  MB_OK + MB_ICONINFORMATION);
          frmAlteraSenhaAcesso.Close;
        end
      end
      else
      begin
        Application.MessageBox ('A senha e a confirma��o da senha n�o conferem. Tente novamente...',
                                '[Sistema Deca] - Senha n�o confere.',
                                MB_OK + MB_ICONINFORMATION);

        edSenhaNova.Clear;
        edSenhaNova.SetFocus;
        edConfirmaSenhaNova.Clear;
      end
    end
    else
    begin
      Application.MessageBox ('Opera��o cancelada pelo usu�rio. Sua senha N�O foi alterada...',
                              '[Sistema Deca] - Opera��o cancelada pelo usu�rio.',
                              MB_OK + MB_ICONINFORMATION);
      edSenhaNova.Clear;
      edConfirmaSenhaNova.Clear;
    end;
  except end;
end;

procedure TfrmAlteraSenhaAcesso.mskConfirmaSenhaNovaExit(Sender: TObject);
begin
  if (edSenhaNova.Text <> edConfirmaSenhaNova.Text) then
  begin
    Application.MessageBox ('Aten��o !!! As senhas n�o conferem...',
                            '[Sistema Deca] - Valida��o de senha.',
                            MB_OK + MB_ICONWARNING);
  end
  else
  begin

  end;
end;

end.
