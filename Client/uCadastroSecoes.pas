unit uCadastroSecoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Mask, Db, Grids, DBGrids, Buttons;

type
  TfrmNovoCadastroSecoes = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edNomeUnidade: TEdit;
    cbRegiao: TComboBox;
    cbTipo: TComboBox;
    edNomeGestor: TEdit;
    edEndereco: TEdit;
    edComplemento: TEdit;
    edBairro: TEdit;
    edEmail: TEdit;
    edCapacidade: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    edNomeSecao: TEdit;
    edASocialResponsavel: TEdit;
    Label17: TLabel;
    mskCCusto: TMaskEdit;
    mskCep: TMaskEdit;
    mskFone1: TMaskEdit;
    mskFone2: TMaskEdit;
    mskNumSecao: TMaskEdit;
    Panel3: TPanel;
    dbgSecoes: TDBGrid;
    dsSecoes: TDataSource;
    Panel4: TPanel;
    Panel6: TPanel;
    dbgUnidades: TDBGrid;
    dsUnidades: TDataSource;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    lbCCusto: TLabel;
    Panel5: TPanel;
    btnNovaSecao: TBitBtn;
    btnSalvarSecao: TBitBtn;
    btnAlterarSecao: TBitBtn;
    btnCancelarSecao: TBitBtn;
    btnSairSecao: TBitBtn;
    btnAtivarDesativar: TBitBtn;
    Label18: TLabel;
    Label19: TLabel;
    btnImprimir: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure dbgUnidadesCellClick(Column: TColumn);
    procedure dbgUnidadesKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgUnidadesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ModoTela (Modo: String);
    procedure LimpaCampos (Form: TForm);
    procedure btnNovoClick(Sender: TObject);
    procedure dbgUnidadesDblClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure mskCCustoExit(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure ModoTelaSecao (Modo: String);
    procedure btnSairSecaoClick(Sender: TObject);
    procedure dbgSecoesCellClick(Column: TColumn);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnNovaSecaoClick(Sender: TObject);
    procedure btnCancelarSecaoClick(Sender: TObject);
    procedure dbgSecoesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgSecoesKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarSecaoClick(Sender: TObject);
    procedure mskNumSecaoExit(Sender: TObject);
    procedure dbgSecoesDblClick(Sender: TObject);
    procedure btnAlterarSecaoClick(Sender: TObject);
    procedure btnAtivarDesativarClick(Sender: TObject);
    procedure dbgSecoesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovoCadastroSecoes: TfrmNovoCadastroSecoes;
    vvMUDA_ABA : Boolean;

implementation

uses uDM, uPrincipal, uUtil, rAdolescentesEmpresasConvenio;

{$R *.DFM}

procedure TfrmNovoCadastroSecoes.FormShow(Sender: TObject);
begin
  //No caso de perfil <> de Administrador carrega somente a unidade do usu�rio logado

  PageControl1.ActivePageIndex := 0;
  ModoTela('P');
  vvMUDA_ABA := True;

  if vvCOD_UNIDADE = 44 then
    btnImprimir.Visible := True
  else
    btnImprimir.Visible := False;

  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;

      if vvIND_PERFIL <> 1 then
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE
      else
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
      dbgUnidades.Refresh;
    end;

    //Carrega as se��es cadastradas para a unidade
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      if (vvIND_PERFIL <> 1) or (vvIND_PERFIL <> 20) or (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE
      else
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := Null; //Carregar inclusive as inativas, para poder ativ�-las se necess�rio
      Open;
      dbgSecoes.Refresh;

    end;

  except

  end;
end;

procedure TfrmNovoCadastroSecoes.dbgUnidadesCellClick(Column: TColumn);
begin
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  mskCcusto.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  mskCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  mskFone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  mskFone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;

  //Seleciona/atualiza os dados das se��es referente a unidade selecionada
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := Null;
      Open;
      dbgSecoes.Refresh;
      lbCCusto.Caption := dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value;
    end;
  except

  end;


end;

procedure TfrmNovoCadastroSecoes.dbgUnidadesKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  mskCcusto.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  mskCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  mskFone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  mskFone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;

  //Seleciona/atualiza os dados das se��es referente a unidade selecionada
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := Null;
      Open;
      dbgSecoes.Refresh;
      lbCCusto.Caption := dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value;
    end;
  except

  end;
end;

procedure TfrmNovoCadastroSecoes.dbgUnidadesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  mskCcusto.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  mskCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  mskFone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  mskFone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;

  //Seleciona/atualiza os dados das se��es referente a unidade selecionada
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := Null;
      Open;
      dbgSecoes.Refresh;
      lbCCusto.Caption := dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value;
    end;
  except

  end;

end;

procedure TfrmNovoCadastroSecoes.ModoTela(Modo: String);
begin
  if Modo = 'P' then
  begin
    vvMUDA_ABA := True;
    //Limpar todos os campos
    LimpaCampos(frmNovoCadastroSecoes);
    Panel1.Enabled := False;
    Panel6.Enabled := True;

    //Bot�es
    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
  end
  {else if Modo = 'N' then
  begin
    LimpaCampos(frmNovoCadastroSecoes);
    Panel1.Enabled := True;
    Panel6.Enabled := False;

    //Bot�es
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end}
  else if Modo = 'A' then
  begin
    vvMUDA_ABA := False;
    Panel1.Enabled := True;
    Panel6.Enabled := False;

    //Bot�es
    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmNovoCadastroSecoes.LimpaCampos;
var i : Integer;
begin
  for i := 0 to Form.ComponentCount - 1 do
    if Form.Components[i] is TCustomEdit then
      (Form.Components[i] as TCustomEdit).Clear;
end;

procedure TfrmNovoCadastroSecoes.btnNovoClick(Sender: TObject);
begin
  ModoTela('N');
  edNomeUnidade.SetFocus;
end;

procedure TfrmNovoCadastroSecoes.dbgUnidadesDblClick(Sender: TObject);
begin
  ModoTela('A');
  vvMUDA_ABA := False;
end;

procedure TfrmNovoCadastroSecoes.btnSairClick(Sender: TObject);
begin
  frmNovoCadastroSecoes.Close;
end;

procedure TfrmNovoCadastroSecoes.mskCCustoExit(Sender: TObject);
begin
  //Validar se o n.� do ccusto j� est� cadastrado
end;

procedure TfrmNovoCadastroSecoes.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
      Params.ParamByName ('@pe_nom_unidade').Value := GetValue(edNomeUnidade.Text);
      Params.ParamByName ('@pe_ind_regiao').Value := cbRegiao.ItemIndex;
      Params.ParamByName ('@pe_num_ccusto').Value := GetValue(mskCcusto.Text);
      Params.ParamByName ('@pe_nom_gestor').Value := GetValue(edNomeGestor.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_dsc_complemento').Value := GetValue(edComplemento.Text);
      Params.ParamByName ('@pe_dsc_bairro').Value := GetValue(edBairro.Text);
      Params.ParamByName ('@pe_num_cep').Value := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_num_telefone1').Value := GetValue(mskFone1.Text);
      Params.ParamByName ('@pe_num_telefone2').Value := GetValue(mskFone2.Text);
      Params.ParamByName ('@pe_dsc_email').Value := GetValue(edEmail.Text);
      Params.ParamByName ('@pe_num_capacidade').Value := GetValue(edCapacidade.Text);
      Params.ParamByName ('@pe_ind_tipo').Value := cbTipo.ItemIndex;
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;

      Execute;

      //Atualiza os dados do grid dbgUnidades
      with dmDeca.cdsSel_Unidade do
      begin
        Close;

        if vvIND_PERFIL <> 1 then
          Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE
        else
          Params.ParamByName ('@pe_cod_unidade').Value := Null;
        Params.ParamByName ('@pe_nom_unidade').Value := Null;
        Open;
        dbgUnidades.Refresh;
      end;
    end;
  except
  end;
  vvMUDA_ABA := True;
  cbRegiao.ItemIndex := -1;
  cbTipo.ItemIndex := -1;
  ModoTela('P');
end;

procedure TfrmNovoCadastroSecoes.PageControl1Change(Sender: TObject);
begin
  if vvMUDA_ABA = False then
    PageControl1.ActivePageIndex := 0;
end;

procedure TfrmNovoCadastroSecoes.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA = False then
    AllowChange := False;
end;

procedure TfrmNovoCadastroSecoes.ModoTelaSecao(Modo: String);
begin
  if Modo = 'P' then
  begin
    mskNumSecao.Clear;
    edNomeSecao.Clear;
    edASocialResponsavel.Clear;
    Panel2.Enabled := False;
    Panel3.Enabled := True;

    //Bot�es
    btnAtivarDesativar.Enabled := True;
    btnImprimir.Enabled := True;
    btnNovaSecao.Enabled := True;
    btnSalvarSecao.Enabled := False;
    btnAlterarSecao.Enabled := False;
    btnCancelarSecao.Enabled := False;
    btnSairSecao.Enabled := True;
  end
  else if Modo = 'N' then
  begin
    Panel2.Enabled := True;
    Panel3.Enabled := False;

    //Bot�es
    btnAtivarDesativar.Enabled := False;
    btnImprimir.Enabled := False;
    btnNovaSecao.Enabled := False;
    btnSalvarSecao.Enabled := True;
    btnAlterarSecao.Enabled := False;
    btnCancelarSecao.Enabled := True;
    btnSairSecao.Enabled := False;
  end
  else if Modo = 'A' then
  begin
    Panel2.Enabled := True;
    Panel3.Enabled := False;

    //Bot�es
    btnAtivarDesativar.Enabled := False;
    btnImprimir.Enabled := False;
    btnNovaSecao.Enabled := False;
    btnSalvarSecao.Enabled := False;
    btnAlterarSecao.Enabled := True;
    btnCancelarSecao.Enabled := True;
    btnSairSecao.Enabled := False;
  end;
end;

procedure TfrmNovoCadastroSecoes.btnSairSecaoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmNovoCadastroSecoes.dbgSecoesCellClick(Column: TColumn);
begin
  mskNumSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString;
  edNomeSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString;
  edASocialResponsavel.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').AsString;
end;

procedure TfrmNovoCadastroSecoes.TabSheet2Show(Sender: TObject);
begin
  ModoTelaSecao('P');
end;

procedure TfrmNovoCadastroSecoes.btnNovaSecaoClick(Sender: TObject);
begin
  ModoTelaSecao('N');
  mskNumSecao.SetFocus;
end;

procedure TfrmNovoCadastroSecoes.btnCancelarSecaoClick(Sender: TObject);
begin
  ModoTelaSecao('P');
end;

procedure TfrmNovoCadastroSecoes.dbgSecoesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  mskNumSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString;
  edNomeSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString;
  edASocialResponsavel.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').AsString;
end;

procedure TfrmNovoCadastroSecoes.dbgSecoesKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  mskNumSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString;
  edNomeSecao.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString;
  edASocialResponsavel.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').AsString;
end;

procedure TfrmNovoCadastroSecoes.btnCancelarClick(Sender: TObject);
begin
  ModoTela('P');
end;

procedure TfrmNovoCadastroSecoes.btnSalvarSecaoClick(Sender: TObject);
begin
  try
    if (mskNumSecao.Text <> '0000') then
    begin
      with dmDeca.cdsInc_Secao do
      begin
        Close;
        Params.ParamByName('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
        Params.ParamByName('@pe_num_secao').Value := GetValue(mskNumSecao.Text);
        Params.ParamByName('@pe_nom_asocial').Value := GetValue(edAsocialResponsavel.Text);
        Params.ParamByName('@pe_dsc_nom_secao').Value := GetValue(edNomeSecao.Text);
        Params.ParamByName('@pe_flg_status').Value := 0; //Se��o NOVA, pressup�em-se que esteja ativa
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;

      //Atualiza os dados da se��o no grid
      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;
        dbgSecoes.Refresh;
      end;

      ModoTelaSecao('P');

    end
  else
  begin
    Application.MessageBox('N�mero da Se��o/Empresa n�o pode ser 0000 !!'+#13+#10+#13+#10+
                           'Favor informar um n�mero v�lido!!!!',
                           'Cadastro de Se��es',
                           MB_OK + MB_ICONERROR);
    mskNumSecao.Text := '0000';
    mskNumSecao.SetFocus;
  end;
  except end;
end;

procedure TfrmNovoCadastroSecoes.mskNumSecaoExit(Sender: TObject);
begin
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_secao').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
    Params.ParamByName ('@pe_num_secao').Value := GetValue(mskNumSecao.Text);
    Params.ParamByName ('@pe_flg_status').Value := Null;
    Open;

    if RecordCount > 0 then
    begin
      Application.MessageBox('N�mero da Se��o/Empresa j� cadastrado!'+#13+#10+#13+#10+
                             'para a unidade selecionada. Verifique o n�mero digitado...',
                             'Cadastro de Se��es',
                             MB_OK + MB_ICONERROR);

      mskNumSecao.Text := '0000';
      mskNumSecao.SetFocus;

      //Atualiza o grid
      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;
        dbgSecoes.Refresh;
      end;

    end
    else
    begin
      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;
        dbgSecoes.Refresh;
      end;
    end;
  end;
end;

procedure TfrmNovoCadastroSecoes.dbgSecoesDblClick(Sender: TObject);
begin
  ModoTelaSecao('A');
  mskNumSecao.Text := dmDeca.cdsSel_Secao.FieldByName('num_secao').Value;
  edNomeSecao.Text := dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').Value;
end;

procedure TfrmNovoCadastroSecoes.btnAlterarSecaoClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := dmDeca.cdsSel_Secao.FieldByName ('cod_id_secao').Value;
      Params.ParamByName ('@pe_num_secao').Value := GetValue(mskNuMSecao.Text);
      Params.ParamByName ('@pe_nom_asocial').Value := GetValue(edASocialResponsavel.Text);
      Params.ParamByName ('@pe_dsc_nom_secao').Value := GetValue(edNomeSecao.Text);
      Params.ParamByName ('@pe_flg_status').Value := 0; //Ativa
      Execute;

      //Atualiza os dados no grid
      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;
        dbgSecoes.Refresh;
      end;
    end;

    ModoTelaSecao('P');

  except end;
end;

procedure TfrmNovoCadastroSecoes.btnAtivarDesativarClick(Sender: TObject);
begin
  if (dmDeca.cdsSel_Secao.FieldByName('flg_status').Value = 1) then  //Est� ativa
  begin
    if Application.MessageBox('Deseja "ATIVAR" a se��o ?',
                              'Cadastro de Se��es - [ATIVA��O]',
                              MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      with dmDeca.cdsAlt_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := dmDeca.cdsSel_Secao.FieldByName ('cod_id_secao').Value;
        Params.ParamByName ('@pe_num_secao').Value := GetValue(mskNuMSecao.Text);
        Params.ParamByName ('@pe_nom_asocial').Value := GetValue(edASocialResponsavel.Text);
        Params.ParamByName ('@pe_dsc_nom_secao').Value := GetValue(edNomeSecao.Text);
        Params.ParamByName ('@pe_flg_status').Value := 0; //Ativa
        Execute;
      end;

    end

  end
  else if (dmDeca.cdsSel_Secao.FieldByName('flg_status').Value = 0) then  //Est� ativa
  begin
    if Application.MessageBox('Deseja "DESATIVAR" a se��o ?',
                              'Cadastro de Se��es - [DESATIVA��O]',
                              MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      with dmDeca.cdsAlt_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := dmDeca.cdsSel_Secao.FieldByName ('cod_id_secao').Value;
        Params.ParamByName ('@pe_num_secao').Value := GetValue(mskNuMSecao.Text);
        Params.ParamByName ('@pe_nom_asocial').Value := GetValue(edASocialResponsavel.Text);
        Params.ParamByName ('@pe_dsc_nom_secao').Value := GetValue(edNomeSecao.Text);
        Params.ParamByName ('@pe_flg_status').Value := 1; //Inativa
        Execute;
      end;
    end
  end;

  //Atualizar os dados do grid
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_secao').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
    Params.ParamByName ('@pe_num_secao').Value := Null;
    Params.ParamByName ('@pe_flg_status').Value := Null;
    Open;
    dbgSecoes.Refresh;
  end;
end;

procedure TfrmNovoCadastroSecoes.dbgSecoesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //Ativa = 0
  if (dmDeca.cdsSel_Secao.FieldByName('flg_status').AsInteger = 0) then
  begin
    dbgSecoes.Canvas.Font.Color:= clGreen;
    dbgSecoes.Canvas.Brush.Color:= clWhite;
  end
  else if (dmDeca.cdsSel_Secao.FieldByName('flg_status').AsInteger = 1) then
  begin
    dbgSecoes.Canvas.Font.Color:= clRed;
    dbgSecoes.Canvas.Brush.Color:= clWhite;
  end;

  dbgSecoes.Canvas.FillRect(Rect);
  dbgSecoes.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

procedure TfrmNovoCadastroSecoes.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm (TrelAdolescentesEmpresasConvenio, relAdolescentesEmpresasConvenio);

  with dmDeca.cdsSEL_AsSocial_EmpresasConvenio do
  begin
    Close;
    Open;
    relAdolescentesEmpresasConvenio.Preview;
    relAdolescentesEmpresasConvenio.Free;
  end;
end;

end.
