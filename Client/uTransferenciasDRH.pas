unit uTransferenciasDRH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls;

type
  TfrmTransferenciasDRH = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    meData1: TMaskEdit;
    GroupBox2: TGroupBox;
    meData2: TMaskEdit;
    btnVer: TSpeedButton;
    rgTipoFolha: TRadioGroup;
    procedure btnVerClick(Sender: TObject);
    procedure meData1Exit(Sender: TObject);
    procedure meData2Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciasDRH: TfrmTransferenciasDRH;

implementation

uses uDM, uPrincipal, uUtil, rRelacaoTransferenciasDRH;

{$R *.DFM}

procedure TfrmTransferenciasDRH.btnVerClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Transferencias_para_DRH do
    begin
      Close;
      Params.ParamByName ('@pe_data1').Value      := GetValue(meData1, vtDate);
      Params.ParamByName ('@pe_data2').Value      := GetValue(meData2, vtDate);
      case rgTipoFolha.ItemIndex of
        0: Params.ParamByName ('@pe_tipo_folha').Value := '002';
        1: Params.ParamByName ('@pe_tipo_folha').Value := '003';
        2: Params.ParamByName ('@pe_tipo_folha').Value := '006';
        3: Params.ParamByName ('@pe_tipo_folha').Value := Null;
      end;

      Open;

      if (dmDeca.cdsSel_Transferencias_para_DRH.RecordCount < 1) then
      begin
        ShowMessage ('N�o foram encontrados registros de Transfer�ncias neste per�odo...');
      end
      else
      begin
        Application.CreateForm (TrelRelacaoTransferenciasDRH, relRelacaoTransferenciasDRH);
        relRelacaoTransferenciasDRH.lbTitulo.Caption := 'Transferidos de ' + meData1.Text + ' at� ' + meData2.Text;
        relRelacaoTransferenciasDRH.Preview;
        relRelacaoTransferenciasDRH.Free;
      end;
    end;
  except end;
end;

procedure TfrmTransferenciasDRH.meData1Exit(Sender: TObject);
begin
  try
    StrToDate(meData1.Text)
  except
    meData1.SetFocus;
  end;
end;

procedure TfrmTransferenciasDRH.meData2Exit(Sender: TObject);
begin
  try
    StrToDate(meData2.Text)
  except
    meData2.SetFocus;
  end;
end;

procedure TfrmTransferenciasDRH.FormShow(Sender: TObject);
begin
  meData1.Clear;
  meData2.Clear;
end;

procedure TfrmTransferenciasDRH.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmTransferenciasDRH.Close;
  end;
end;

end.
