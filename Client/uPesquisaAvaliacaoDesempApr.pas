unit uPesquisaAvaliacaoDesempApr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeProcs, TeEngine, Chart, DBChart, Grids, DBGrids, StdCtrls, Mask,
  Buttons, ExtCtrls, Db, ComCtrls, OleServer, Excel97;

type
  TfrmPesquisaAvaliacaoDesempAprendiz = class(TForm)
    Panel4: TPanel;
    btnPesquisaP: TSpeedButton;
    Label4: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    chkMatricula: TCheckBox;
    chkUnidade: TCheckBox;
    cbUnidadeP: TComboBox;
    chkEmpresa: TCheckBox;
    cbSecaoP: TComboBox;
    chkPeriodoAvaliacao: TCheckBox;
    mskDataAv1: TMaskEdit;
    mskDataAv2: TMaskEdit;
    chkPeriodoLancamento: TCheckBox;
    mskDataLanc1: TMaskEdit;
    mskDataLanc2: TMaskEdit;
    mskMatricula: TMaskEdit;
    btnVisualizar: TSpeedButton;
    btnExportar: TSpeedButton;
    btnVerResultado: TBitBtn;
    btnLimpar: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    dbgResultado: TDBGrid;
    DBChart1: TDBChart;
    dsResultados: TDataSource;
    Excel: TExcelApplication;
    chkASocial: TCheckBox;
    cbASocial: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure chkMatriculaClick(Sender: TObject);
    procedure chkUnidadeClick(Sender: TObject);
    procedure chkPeriodoAvaliacaoClick(Sender: TObject);
    procedure btnVerResultadoClick(Sender: TObject);
    procedure btnPesquisaPClick(Sender: TObject);
    procedure chkEmpresaClick(Sender: TObject);
    procedure cbUnidadePClick(Sender: TObject);
    procedure chkPeriodoLancamentoClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure chkASocialClick(Sender: TObject);
  private
    { Private declarations }
    procedure ExportaExcel (DataSet: TDataSet; ArqNome: OleVariant);
    function PegaLetraColuna(IntNumber: Integer): String;
  public
    { Public declarations }
  end;

var
  frmPesquisaAvaliacaoDesempAprendiz: TfrmPesquisaAvaliacaoDesempAprendiz;
  vUnidade1, vUnidade2 : TStringList;
  vUnidade : String;

implementation

uses uDM, uFichaPesquisa, uPrincipal, rResultadoAvaliacaoDesempAprendiz;

{$R *.DFM}

procedure TfrmPesquisaAvaliacaoDesempAprendiz.FormShow(Sender: TObject);
begin
  vUnidade1 := TStringList.Create();
  vUnidade2 := TStringList.Create();

  //Carrega a lista de unidades e a de Assistentes Sociais
  try
  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName('@pe_cod_unidade').Value := Null;
    Params.ParamByName('@pe_nom_unidade').Value := Null;
    Open;

    while not eof do
    begin
      cbUnidadeP.Items.Add (Trim(FieldByName('num_ccusto').Value) + '-' + FieldByName('nom_unidade').Value);
      vUnidade1.Add (IntToStr(FieldByName('cod_unidade').Value));
      vUnidade2.Add (FieldByName('nom_unidade').Value);
      Next;
    end;
  end;

  with dmDeca.cdsSel_Distinto_ASocial_em_Secao do
  begin
    Close;
    Open;

    cbASocial.Clear;
    while not eof do
    begin
      cbASocial.Items.Add (dmDeca.cdsSel_Distinto_ASocial_em_Secao.FieldByName('nom_asocial').AsString);
      dmDeca.cdsSel_Distinto_ASocial_em_Secao.Next;
    end;
  end;


  except end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.chkMatriculaClick(
  Sender: TObject);
begin
  if (chkMatricula.Checked = True) then
  begin
    mskMatricula.Enabled := True;
    chkMatricula.Font.Color := clBlue;
    btnPesquisaP.Enabled := True;
    Label4.Caption := '';
    chkPeriodoAvaliacao.Font.Color := clBlue;
    mskDataAv1.Enabled := True;
    mskDataAv2.Enabled := True;
    chkPeriodoLancamento.Font.Color := clBlue;

    //Desabilita as outars op��es n�o combinadas
    chkUnidade.Enabled := False;
    cbUnidadeP.ItemIndex := -1;
    cbUnidadeP.Enabled := False;
    chkEmpresa.Enabled := False;
    cbSecaoP.ItemIndex := -1
  end
  else
  begin
    chkMatricula.Font.Color := clBlack;
    mskMatricula.Enabled := False;
    btnPesquisaP.Enabled := False;
    chkPeriodoAvaliacao.Font.Color := clBlack;
    chkPeriodoLancamento.Font.Color := clBlack;
    mskDataAv1.Enabled := False;
    mskDataAv2.Enabled := False;

    //Habilita as op��es n�o combinadas
    chkUnidade.Enabled := True;
    chkEmpresa.Enabled := False
  end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.chkUnidadeClick(
  Sender: TObject);
begin
  //Habilita as op��es combinadas
  if (chkUnidade.Checked = True) then
  begin
    chkUnidade.Font.Color := clBlue;
    cbUnidadeP.Enabled := True;
    chkEmpresa.Enabled := True;
    chkEmpresa.Font.Color := clBlue;
    cbSecaoP.Enabled := False;
    chkPeriodoAvaliacao.Enabled := True;
    chkPeriodoAvaliacao.Font.Color := clBlue;
    chkPeriodoLancamento.Enabled := True;
    chkPeriodoLancamento.Font.Color := clBlue;

    //Desabilita as op��es n�o combinadas
    chkMatricula.Enabled := False;
    mskMatricula.Clear;
    chkMatricula.Font.Color := clBlack;
    mskMatricula.Enabled := False;
    btnPesquisaP.Enabled := False;
    Label4.Caption := ''
  end
  else
  begin
    chkUnidade.Font.Color := clBlack;
    cbUnidadeP.ItemIndex := -1;
    cbUnidadeP.Enabled := False;
    chkEmpresa.Font.Color := clBlack;
    chkEmpresa.Enabled := False;
    cbSecaoP.ItemIndex := -1;
    cbSecaoP.Enabled := False;
    chkPeriodoAvaliacao.Enabled := True;
    mskDataAv1.Clear;
    mskDataAv1.Enabled := False;
    mskDataAv2.Clear;
    mskDataAv2.Enabled := False;
    chkPeriodoAvaliacao.Font.Color := clBlack;
    chkPeriodoLancamento.Font.Color := clBlack;

    //Desabilita as op��es n�o combinadas
    chkMatricula.Enabled := True;
    mskMatricula.Clear;
    mskMatricula.Enabled := False;
    btnPesquisaP.Enabled := False;
    Label4.Caption := ''
  end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.chkPeriodoAvaliacaoClick(
  Sender: TObject);
begin
  if (chkPeriodoAvaliacao.Checked = True) then
  begin
    chkPeriodoAvaliacao.Font.Color := clBlue;
    mskDataAv1.Enabled := True;
    //mskDataAv1.Clear;
    mskDataAv2.Enabled := True;
    //mskDataAv2.Clear;

    //Desabilita as op��es n�o combinadas
    //chkMatricula.Font.Color := clBlack;
    //mskMatricula.Clear;
    //mskMatricula.Enabled := False;
    btnPesquisaP.Enabled := False;
    //Label4.Caption :='';
    //cbUnidadeP.ItemIndex := -1;
    //cbUnidadeP.Enabled := False;
    //chkEmpresa.Enabled := False;
    //cbSecaoP.ItemIndex := -1;
    //cbSecaoP.Enabled := False;
    mskDataLanc1.Enabled := False;
  end
  else
  begin
    //Desabilita os campos e limpa os checks
    chkPeriodoAvaliacao.Font.Color := clBlack;
    chkPeriodoAvaliacao.Checked := False;
    mskDataAv1.Clear;
    mskDataAv1.Enabled := False;
    mskDataAv2.Clear;
    mskDataAv2.Enabled := False;
  end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.btnVerResultadoClick(
  Sender: TObject);
begin
  try
  with dmDeca.cdsSel_AvaliacoesApr do
  begin

    Close;
    Params.ParamByName('@pe_cod_id_avaliacao').Value := Null;

    //Matricula
    if (chkMatricula.Checked = True) then
      Params.ParamByName('@pe_cod_matricula').Value := mskMatricula.Text
    else
      Params.ParamByName('@pe_cod_matricula').Value := Null;

    //Unidade
    if (chkUnidade.Checked = true) then
      Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadeP.ItemIndex])
    else
      Params.ParamByName('@pe_cod_unidade').Value := Null;

    //Empresa/Se��o
    if (chkEmpresa.Checked = True) then
      Params.ParamByName('@pe_num_secao').Value := Copy(cbSecaoP.Text,1,4)
    else
      Params.ParamByName('@pe_num_secao').Value := Null;

    //Assistente Social
    if (chkASocial.Checked = True) then
      Params.ParamByName('@pe_nom_asocial').Value := Trim(cbASocial.Text)
    else
      Params.ParamByName('@pe_nom_asocial').Value := Null;

    //Per�odo da data de avalia��o
    if (chkPeriodoAvaliacao.Checked = True) then
    begin
      Params.ParamByName('@pe_dat_av1').Value := StrToDate(mskDataAv1.Text);
      Params.ParamByName('@pe_dat_av2').Value := StrToDate(mskDataAv2.Text)
    end
    else
    begin
      Params.ParamByName('@pe_dat_av1').Value := Null;
      Params.ParamByName('@pe_dat_av1').Value := Null
    end;


    //Per�odo de lan�amentos das avalia��es
    if (chkPeriodoLancamento.Checked = True) then
    begin
      Params.ParamByName('@pe_dat_lanc1').Value := StrToDate(mskDataLanc1.Text);
      Params.ParamByName('@pe_dat_lanc2').Value := StrToDate(mskDataLanc2.Text)
    end
    else
    begin
      Params.ParamByName('@pe_dat_lanc1').Value := Null;
      Params.ParamByName('@pe_dat_lanc2').Value := Null
    end;

    Open;

    dbgResultado.DataSource := dsResultados;
    dbgResultado.Refresh;


  end;
  except end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.btnPesquisaPClick(
  Sender: TObject);
begin
  //Chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    //frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);
    mskMatricula.Text := vvCOD_MATRICULA_PESQUISA;

    PageControl1.ActivePageIndex := 0;

    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= Null;//vvCOD_UNIDADE;
        Open;
        Label4.Caption := FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.chkEmpresaClick(
  Sender: TObject);
begin
  if (chkEmpresa.Checked = True) then
  begin
    cbSecaoP.Enabled := True
  end
  else
  begin
    cbSecaoP.ItemIndex := -1;
    cbSecaoP.Enabled := False;
  end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.cbUnidadePClick(
  Sender: TObject);
begin
  //Carrega os dados das empresas cadastradas de acordo com a unidade selecionada
  vUnidade := cbUnidadeP.Text;
  try
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_secao').Value := Null;
    Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadeP.ItemIndex]);
    Params.ParamByName('@pe_num_secao').Value := Null;
    Params.ParamByName('@pe_flg_status').Value := 0; //Somente os Ativos
    Open;

    if (dmDeca.cdsSel_Secao.RecordCount > 0) then
    begin
      cbSecaoP.Clear;
      while not dmDeca.cdsSel_Secao.eof do
      begin
        cbSecaoP.Items.Add(dmDeca.cdsSel_Secao.FieldByName('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').Value);
        dmDeca.cdsSel_Secao.Next;
      end;
    end
    else
    begin
      Application.MessageBox('Aten��o !!!' + #13+#10 +
                             'N�o foram encontradas se��es/empresas para a unidade selecionada ' +  #13+#10 +
                             'Selecione outra unidade ou entre em contato com o Administrador do Sistema para solucionar o problema...',
                             '[Sistema Deca] - Avalia��o Desempenho - Aprendiz',
                             MB_ICONEXCLAMATION + MB_OK );
    end;
  end;
  except end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.chkPeriodoLancamentoClick(
  Sender: TObject);
begin
  if (chkPeriodoLancamento.Checked = True) then
  begin
    chkPeriodoLancamento.Font.Color := clBlue;
    mskDataLanc1.Enabled := True;
    mskDataLanc2.Enabled := True;

    //Desabilita as op��es n�o combinadas
    btnPesquisaP.Enabled := False;
    //cbUnidadeP.ItemIndex := -1;
    //cbUnidadeP.Enabled := False;
    //chkEmpresa.Enabled := False;
    //cbSecaoP.ItemIndex := -1;
    //cbSecaoP.Enabled := False;
    mskDataAv1.Enabled := False;
    mskDataAv2.Enabled := False;
  end
  else
  begin
    //Desabilita os campos e limpa os checks
    chkPeriodoLancamento.Font.Color := clBlack;
    chkPeriodoLancamento.Checked := False;
    mskDataLanc1.Clear;
    mskDataLanc1.Enabled := False;
    mskDataLanc2.Clear;
    mskDataLanc2.Enabled := False;
  end;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.btnLimparClick(
  Sender: TObject);
begin
  //Habilita os checks para estarem pronto a uma nova consulta
  //atualiza o grid - exibir nada
  chkMatricula.Font.Color := clBlack;
  chkMatricula.Checked := False;
  btnPesquisaP.Enabled := False;
  Label4.Caption := '';
  chkUnidade.Font.Color := clBlack;
  chkUnidade.Checked := False;
  cbUnidadeP.ItemIndex := -1;
  cbUnidadeP.Enabled := False;
  chkEmpresa.Font.Color := clBlack;
  chkEmpresa.Checked := False;
  cbSecaoP.Enabled := False;
  chkPeriodoAvaliacao.Font.Color := clBlack;
  chkPeriodoAvaliacao.Checked := False;
  mskDataAv1.Clear;
  mskDataAv1.Enabled := False;
  mskDataAv2.Clear;
  mskDataAv2.Enabled := False;
  chkPeriodoLancamento.Font.Color := clBlack;
  chkPeriodolancamento.Checked := False;
  mskDataLanc1.Clear;
  mskDataLanc1.Enabled := False;
  mskDataLanc2.Clear;
  mskDataLanc2.Enabled := False;

  //Retira temporariamente o "link" do dbgrid para n�o exibir nada
  dbgResultado.DataSource := nil;

end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.btnVisualizarClick(
  Sender: TObject);
begin
  Application.CreateForm (TrelResultadoAvaliacaoDesempAprendiz,relResultadoAvaliacaoDesempAprendiz);

  relResultadoAvaliacaoDesempAprendiz.qrlTitulo.Caption := Copy(cbUnidadeP.Text,1,4) + '-' + Copy(cbUnidadeP.Text,6,50);
  if chkEmpresa.Checked = True then
    relResultadoAvaliacaoDesempAprendiz.qrlTitulo.Caption := Copy(cbUnidadeP.Text,1,4) + '.' + Copy(cbSecaoP.Text,1,4) +
    ' - ' + Copy(cbUnidadeP.Text,6,50) + '/' + Copy(cbSecaoP.Text,6,50)
  else
    relResultadoAvaliacaoDesempAprendiz.qrlTitulo.Caption := relResultadoAvaliacaoDesempAprendiz.qrlTitulo.Caption;

  if (chkPeriodoAvaliacao.Checked = True) then
    relResultadoAvaliacaoDesempAprendiz.lbPeriodo.Caption := 'Per�odo de avalia��o:  ' + mskDataAv1.Text  + ' a ' + mskDataAv2.Text
  else if (chkPeriodoLancamento.Checked = True) then
    relResultadoAvaliacaoDesempAprendiz.lbPeriodo.Caption := 'Per�odo de Lan�amento:  ' + mskDataLanc1.Text  + ' a ' + mskDataLanc2.Text
  else
    relResultadoAvaliacaoDesempAprendiz.lbPeriodo.Caption := '';

  relResultadoAvaliacaoDesempAprendiz.Preview;
  relResultadoAvaliacaoDesempAprendiz.Free;
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.ExportaExcel(
  DataSet: TDataSet; ArqNome: OleVariant);
var
  NumLinha, NumColuna, LCID : Integer;
  StrCell                   : String;
  FileName                  : OleVariant;
begin
  LCID := GetUserDefaultLCID;
  with Excel do
  begin
    Connect;
    try
      Visible[LCID] := True;
      WorkBooks.Add(EmptyParam, LCID);
      NumLinha := 1;

      (* Aqui pega-se o nome dos CAMPOS do DATASET *)
      with dmDeca.cdsSel_AvaliacoesApr do
      begin
        for NumColuna := 1 to dmDeca.cdsSel_AvaliacoesApr.Fields.Count do
        begin
          StrCell := PegaLetraColuna(NumColuna) + IntToStr(NumLinha);
          (* Usa "DisplayLabel"  das colunas para preencher as c�lulas ou "FieldName"*)
          if dmDeca.cdsSel_AvaliacoesApr.Fields[NumColuna - 1].DisplayLabel <> '' then
            Range[StrCell, StrCell].Value := dmDeca.cdsSel_AvaliacoesApr.Fields[NumColuna - 1].DisplayLabel
          else
            Range[StrCell, StrCell].Value := dmDeca.cdsSel_AvaliacoesApr.Fields[NumColuna - 1].FieldName;
        end;
      end;

      NumLinha := 2;

      with dmDeca.cdsSel_AvaliacoesApr do
      begin
        First;
        while not dmDeca.cdsSel_AvaliacoesApr.eof do
        begin
          for NumColuna := 1 to dmDeca.cdsSel_AvaliacoesApr.Fields.Count do
          begin
            StrCell := PegaLetraColuna(NumColuna) + IntToStr(NumLinha);
            Range[StrCell, StrCell].Value := dmDeca.cdsSel_AvaliacoesApr.Fields.Fields[NumColuna - 1].Value;
          end;
          dmDeca.cdsSel_AvaliacoesApr.Next;
          Inc(NumLinha);
        end;
      end;

      Range['A1', StrCell].EntireColumn.AutoFit;
      ActiveWorkBook.SaveAs(ArqNome, xlNOrmal, '', '', False, False, xlNoChange, xlUserResolution, False, EmptyParam, EmptyParam, LCID);
      Quit;
    finally
      Disconnect;
    end;
  end;
end;

function TfrmPesquisaAvaliacaoDesempAprendiz.PegaLetraColuna(
  IntNumber: Integer): String;
begin
  if IntNumber > 1 then
    Result := 'A'
  else
  begin
    if IntNumber > 256 then
    begin
      Result := Chr(64 + (IntNumber div 26));
      Result := Result + Chr(IntNumber div 26)
    end
    else
      Result := Chr(64 + IntNumber);
  end;
end;
procedure TfrmPesquisaAvaliacaoDesempAprendiz.btnExportarClick(
  Sender: TObject);
begin
  ExportaExcel(dmDeca.cdsSel_AvaliacoesApr, ExtractFilePath(Application.ExeName) + 'EXPORTA');
end;

procedure TfrmPesquisaAvaliacaoDesempAprendiz.chkASocialClick(
  Sender: TObject);
begin
  if (chkASocial.Checked = True) then
  begin
    chkASocial.Font.Color := clBlue;
    cbASocial.Enabled := True;
    cbASocial.ItemIndex := -1;
    //Habilita Per�odo de Avalia��o
    chkPeriodoAvaliacao.Font.Color := clBlue;
    mskDataAv1.Enabled := True;
    mskDataAv2.Enabled := True
  end
  else
  begin
    chkASocial.Font.Color := clBlack;
    cbASocial.ItemIndex := -1;
    cbASocial.Enabled := False;
    //Desabilita Per�odo de Avalia��o
    chkPeriodoAvaliacao.Font.Color := clBlack;
    mskDataAv1.Enabled := False;
    mskDataAv2.Enabled := False
  end;
end;

end.

