unit uControleUnidadesEscolaresTurmasClasses;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons, Db, Mask;

type
  TfrmControleUnidadesEscolaresTurmasClasses = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    dbgUnidadesEscolares: TDBGrid;
    dbgSeriesEscolares: TDBGrid;
    dbgTurmas: TDBGrid;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edUnidadeEscolar: TEdit;
    Label2: TLabel;
    edNumCIE: TEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    edSerieEscolar: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    edTurma: TEdit;
    Label6: TLabel;
    cbUnidadeEscolar: TComboBox;
    Label7: TLabel;
    cbSerieEscolar: TComboBox;
    Panel4: TPanel;
    btnNovoU: TBitBtn;
    btnSalvarU: TBitBtn;
    btnCancelarU: TBitBtn;
    btnImprimirU: TBitBtn;
    btnSairU: TBitBtn;
    btnAlterarU: TBitBtn;
    Panel5: TPanel;
    btnNovoS: TBitBtn;
    btnSalvarS: TBitBtn;
    btnCancelarS: TBitBtn;
    btnImprimirS: TBitBtn;
    btnSairS: TBitBtn;
    btnAlterarS: TBitBtn;
    Panel6: TPanel;
    btnNovoCT: TBitBtn;
    btnSalvarCT: TBitBtn;
    btnCancelarCT: TBitBtn;
    btnImprimirCT: TBitBtn;
    btnSairCT: TBitBtn;
    btnAlterarCT: TBitBtn;
    dsSel_UnidadeEscolarCIE: TDataSource;
    Label8: TLabel;
    cbPeriodo: TComboBox;
    dsSel_SerieCIE: TDataSource;
    dsSel_Tumas: TDataSource;
    mskNumTurma: TMaskEdit;
    procedure btnSairUClick(Sender: TObject);
    procedure btnSairSClick(Sender: TObject);
    procedure btnSairCClick(Sender: TObject);
    procedure ModoTelaUE (ModoU: String);
    procedure btnNovoUClick(Sender: TObject);
    procedure btnCancelarUClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnSalvarUClick(Sender: TObject);
    procedure ModoTelaSCIE (ModoS: String);
    procedure btnNovoSClick(Sender: TObject);
    procedure btnCancelarSClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnSalvarSClick(Sender: TObject);
    procedure dbgUnidadesEscolaresDblClick(Sender: TObject);
    procedure btnAlterarUClick(Sender: TObject);
    procedure btnAlterarSClick(Sender: TObject);
    procedure dbgSeriesEscolaresDblClick(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure ModoTelaClasseTurma(ModoCT: String);
    procedure btnNovoCTClick(Sender: TObject);
    procedure btnCancelarCTClick(Sender: TObject);
    procedure btnSalvarCTClick(Sender: TObject);
    procedure btnAlterarCTClick(Sender: TObject);
    procedure dbgTurmasDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmControleUnidadesEscolaresTurmasClasses: TfrmControleUnidadesEscolaresTurmasClasses;
  MudaAba : Boolean;
  vListaUE1, vListaUE2, vListaSeriesCIE1, vListaSeriesCIE2 : TStringList;
  vIndex, vIndex2 : Integer;
implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnSairUClick(
  Sender: TObject);
begin
  frmControleUnidadesEscolaresTurmasClasses.Close;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnSairSClick(
  Sender: TObject);
begin
  frmControleUnidadesEscolaresTurmasClasses.Close;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnSairCClick(
  Sender: TObject);
begin
  frmControleUnidadesEscolaresTurmasClasses.Close;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.ModoTelaUE(
  ModoU: String);
begin
  if ModoU = 'P' then
  begin
    dbgUnidadesEscolares.Enabled := True;
    GroupBox1.Enabled            := False;
    edUnidadeEscolar.Clear;
    edNumCIE.Clear;

    btnNovoU.Enabled     := True;
    btnSalvarU.Enabled   := False;
    btnAlterarU.Enabled  := False;
    btnCancelarU.Enabled := False;
    btnImprimirU.Enabled := False;
    btnSairU.Enabled     := True;
  end
  else if ModoU = 'N' then
  begin
    dbgUnidadesEscolares.Enabled := False;
    GroupBox1.Enabled            := True;

    btnNovoU.Enabled     := False;
    btnSalvarU.Enabled   := True;
    btnAlterarU.Enabled  := False;
    btnCancelarU.Enabled := True;
    btnImprimirU.Enabled := False;
    btnSairU.Enabled     := False;
  end
  else if ModoU = 'A' then
  begin
    dbgUnidadesEscolares.Enabled := False;
    GroupBox1.Enabled            := True;

    btnNovoU.Enabled     := False;
    btnSalvarU.Enabled   := False;
    btnAlterarU.Enabled  := True;
    btnCancelarU.Enabled := True;
    btnImprimirU.Enabled := False;
    btnSairU.Enabled     := False;
  end;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnNovoUClick(
  Sender: TObject);
begin
  ModoTelaUE('N');
  MudaAba := False;
  edUnidadeEscolar.SetFocus;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnCancelarUClick(
  Sender: TObject);
begin
  ModoTelaUE('P');
  MudaAba := True;  
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.FormShow(
  Sender: TObject);
begin
  ModoTelaUE('P');
  ModoTelaSCIE('P');
  ModoTelaClasseTurma('P');
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
 if MudaAba = True then AllowChange := True else AllowChange := False;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.TabSheet1Show(
  Sender: TObject);
begin
  //Carregar a lista de Unidade Escolares - CIE
  try
    with dmDeca.cdsSel_UnidadeEscolarCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uecie').Value := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value    := Null;
      Params.ParamByName ('@pe_num_uecie').Value    := Null;
      Open;
      dbgUnidadesEscolares.Refresh;
    end;
  except end;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnSalvarUClick(
  Sender: TObject);
begin
  try
    //Verifica se o valor digitado j� encontra-se no banco de dados
    with dmDeca.cdsSel_UnidadeEscolarCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uecie').Value := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value    := Trim(edUnidadeEscolar.Text);
      Params.ParamByName ('@pe_num_uecie').Value    := Null;
      Open;

      if (dmDeca.cdsSel_UnidadeEscolarCIE.RecordCount > 0) then
      begin
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'A UNIDADE ESCOLAR informada j� encontra-se cadastrada no banco de dados.' + #13+#10 +
                                'Favor informar um outro nome de Unidade para prosseguir.',
                                '[Sistema Deca] - Unidade Escolar CIE',
                                MB_OK + MB_ICONINFORMATION);
        edUnidadeEscolar.SetFocus;
      end
      else
      begin
        //N�o encontrou nome da Unidade faz a inser��o da atual no banco de dados
        with dmDeca.cdsIns_UnidadeEscolarCIE do
        begin
          Close;
          Params.ParamByName ('@pe_dsc_uecie').Value := Trim(edUnidadeEscolar.Text);
          Params.ParamByName ('@pe_num_uecie').Value := StrToInt(edNumCIE.Text);
          Execute;
        end;

        //Atualiza o grid
        with dmDeca.cdsSel_UnidadeEscolarCIE do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_uecie').Value := Null;
          Params.ParamByName ('@pe_dsc_uecie').Value    := Null;
          Params.ParamByName ('@pe_num_uecie').Value    := Null;
          Open;
          dbgUnidadesEscolares.Refresh;
        end;

        ModoTelaUE('P');
      end;
    end;
  except end;
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.ModoTelaSCIE(
  ModoS: String);
begin
  if ModoS = 'P' then
  begin
    dbgSeriesEscolares.Enabled := True;
    GroupBox2.Enabled          := False;
    edSerieEscolar.Clear;

    btnNovoS.Enabled     := True;
    btnSalvarS.Enabled   := False;
    btnAlterarS.Enabled  := False;
    btnCancelarS.Enabled := False;
    btnImprimirS.Enabled := False;
    btnSairS.Enabled     := True;
  end
  else if ModoS = 'N' then
  begin
    dbgSeriesEscolares.Enabled := False;
    GroupBox2.Enabled          := True;

    btnNovoS.Enabled     := False;
    btnSalvarS.Enabled   := True;
    btnAlterarS.Enabled  := False;
    btnCancelarS.Enabled := True;
    btnImprimirS.Enabled := False;
    btnSairS.Enabled     := False;
  end
  else if ModoS = 'A' then
  begin
    dbgSeriesEscolares.Enabled := False;
    GroupBox2.Enabled          := True;

    btnNovoS.Enabled     := False;
    btnSalvarS.Enabled   := False;
    btnAlterarS.Enabled  := True;
    btnCancelarS.Enabled := True;
    btnImprimirS.Enabled := False;
    btnSairS.Enabled     := False;
  end;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnNovoSClick(
  Sender: TObject);
begin
  ModoTelaSCIE('N');
  MudaAba := False;
  edSerieEscolar.SetFocus;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnCancelarSClick(
  Sender: TObject);
begin
  ModoTelaSCIE('P');
  MudaAba := True; 
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.TabSheet2Show(
  Sender: TObject);
begin
  //Carregar a lista de S�ries Escolares - CIE
  try
    with dmDeca.cdsSel_SerieCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_serie').Value  := Null;
      Params.ParamByName ('@pe_dsc_serie_cie').Value := Null;
      Open;
      dbgSeriesEscolares.Refresh;
    end;
  except end;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnSalvarSClick(
  Sender: TObject);
begin
  try
    //Verifica se o valor digitado j� encontra-se no banco de dados
    with dmDeca.cdsSel_SerieCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_serie').Value  := Null;
      Params.ParamByName ('@pe_dsc_serie_cie').Value := Trim(edSerieEscolar.Text);
      Open;

      if (dmDeca.cdsSel_SerieCIE.RecordCount > 0) then
      begin
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'A S�RIE ESCOLAR informada j� encontra-se cadastrada no banco de dados.' + #13+#10 +
                                'Favor informar um outro nome de s�rie para prosseguir.',
                                '[Sistema Deca] - S�rie Escolar CIE',
                                MB_OK + MB_ICONINFORMATION);
        edSerieEscolar.SetFocus;
      end
      else
      begin
        //N�o encontrou nome da Unidade faz a inser��o da atual no banco de dados
        with dmDeca.cdsIns_SerieCIE do
        begin
          Close;
          Params.ParamByName ('@pe_dsc_seriecie').Value := Trim(edSerieEscolar.Text);
          Execute;
        end;

        //Atualiza o grid
        with dmDeca.cdsSel_SerieCIE do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_serie').Value  := Null;
          Params.ParamByName ('@pe_dsc_serie_cie').Value := Null;
          Open;
          dbgSeriesEscolares.Refresh;
        end;

        ModoTelaSCIE('P');
      end;
    end;
  except end;
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.dbgUnidadesEscolaresDblClick(
  Sender: TObject);
begin
  ModoTelaUE('A');
  //Repassa os valores do banco para a tela
  edUnidadeEscolar.Text := dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName ('dsc_uecie').Value;
  edNumCIE.Text         := dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName ('num_uecie').Value;
  edUnidadeEscolar.SetFocus;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnAlterarUClick(
  Sender: TObject);
begin
  //Atualizar os dados para o banco de dados
  try
    if Application.MessageBox ('Gravar a altera��o dos dados no banco de dados?',
                               '[Sistema Deca] - Alterar dados Unidade Escolar',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      with dmDeca.cdsUpd_UnidadeEscolarCIE do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_uecie').Value := dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName ('cod_id_uecie').Value;
        Params.ParamByName ('@pe_dsc_uecie').Value    := GetValue(edUnidadeEscolar.Text);
        Params.ParamByName ('@pe_num_uecie').Value    := GetValue(edNumCIE.Text);
        Execute;

        with dmDeca.cdsSel_UnidadeEscolarCIE do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_uecie').Value := Null;
          Params.ParamByName ('@pe_dsc_uecie').Value    := Null;
          Params.ParamByName ('@pe_num_uecie').Value    := Null;
          Open;
          dbgUnidadesEscolares.Refresh;    
        end;
      end;
    end;
    ModoTelaUE('P');
  except
    Application.MessageBox('Um erro ocorreu na tentativa de gravar as altera��es!' + #13+#10 +
                           'Verifique sua conex�o e tente novamente.',
                           '[Sistema Deca] - Erro alterando Unidade Escolar',
                           MB_OK + MB_ICONERROR);
    ModoTelaUE('P');
  end;
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnAlterarSClick(
  Sender: TObject);
begin
  //Atualizar os dados para o banco de dados
  try
    if Application.MessageBox ('Gravar a altera��o dos dados no banco de dados?',
                               '[Sistema Deca] - Alterar dados S�rie Escolar',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      with dmDeca.cdsUpd_SerieCIE do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_seriecie').Value := dmDeca.cdsSel_SerieCIE.FieldByName ('cod_id_seriecie').Value;
        Params.ParamByName ('@pe_dsc_seriecie').Value    := GetValue(edSerieEscolar.Text);
        Execute;

        with dmDeca.cdsSel_SerieCIE do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_serie').Value  := Null;
          Params.ParamByName ('@pe_dsc_serie_cie').Value := Null;
          Open;
          dbgUnidadesEscolares.Refresh;    
        end;
      end;
    end;
    ModoTelaSCIE('P');
  except
    Application.MessageBox('Um erro ocorreu na tentativa de gravar as altera��es!' + #13+#10 +
                           'Verifique sua conex�o e tente novamente.',
                           '[Sistema Deca] - Erro alterando S�rie Escolar',
                           MB_OK + MB_ICONERROR);
    ModoTelaSCIE('P');
  end;
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.dbgSeriesEscolaresDblClick(
  Sender: TObject);
begin
  ModoTelaSCIE('A');
  //Repassa os valores do banco para a tela
  edSerieEscolar.Text := dmDeca.cdsSel_SerieCIE.FieldByName ('dsc_seriecie').Value;
  edSerieEscolar.SetFocus;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.TabSheet3Show(
  Sender: TObject);
begin
  //Carregar os combos "Unidades Escolares" e "S�ries Escolares"
  vListaUE1        := TStringList.Create();
  vListaUE2        := TStringList.Create();
  vListaSeriesCIE1 := TStringList.Create();
  vListaSeriesCIE2 := TStringList.Create();

  try
    with dmDeca.cdsSel_UnidadeEscolarCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uecie').Value := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value    := Null;
      Params.ParamByName ('@pe_num_uecie').Value    := Null;
      Open;

      cbUnidadeEscolar.Clear;
      while not (dmDeca.cdsSel_UnidadeEscolarCIE.eof) do
      begin
        //Carrega as listas
        vListaUE1.Add(IntToStr(dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName('cod_id_uecie').Value));
        vListaUE2.Add(dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName('dsc_uecie').Value);
        //Carrega o combo cbUnidadeEscolar
        //cbUnidadeEscolar.Items.Add(IntToStr(dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName('num_uecie').Value) + '-' + dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName('dsc_uecie').Value);
        cbUnidadeEscolar.Items.Add(dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName('dsc_uecie').Value);
        dmDeca.cdsSel_UnidadeEscolarCIE.Next;
      end;
    end;

    with dmDeca.cdsSel_SerieCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_serie').Value  := Null;
      Params.ParamByName ('@pe_dsc_serie_cie').Value := Null;
      Open;

      while not (dmDeca.cdsSel_SerieCIE.eof) do
      begin
        //Carrega as listas
        vListaSeriesCIE1.Add(IntToStr(dmDeca.cdsSel_SerieCIE.FieldByName('cod_id_seriecie').Value));
        vListaSeriesCIE2.Add(dmDeca.cdsSel_SerieCIE.FieldByName('dsc_seriecie').Value);
        //Carrega o combo cbUnidadeEscolar
        cbSerieEscolar.Items.Add(dmDeca.cdsSel_SerieCIE.FieldByName('dsc_seriecie').Value);
        dmDeca.cdsSel_SerieCIE.Next;
      end;
    end;

    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByName ('@pe_dsc_turmacie').Value    := Null;
      Params.ParamByName ('@pe_cod_id_uecie').Value    := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value       := Null;
      Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByName ('@pe_dsc_seriecie').Value    := Null;
      Params.ParamByName ('@pe_ind_periodo').Value     := Null;
      Params.ParamByName ('@pe_num_turma').Value       := Null;
      Open;
      dbgTurmas.Refresh;
    end;

  except end;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.ModoTelaClasseTurma(
  ModoCT: String);
begin
  if ModoCT = 'P' then
  begin
    dbgTurmas.Enabled          := True;
    edTurma.Clear;
    mskNumTurma.Clear;
    cbUnidadeEscolar.ItemIndex := -1;
    cbSerieEscolar.ItemIndex   := -1;
    cbPeriodo.ItemIndex        := -1;
    GroupBox3.Enabled          := False;

    btnNovoCT.Enabled     := True;
    btnSalvarCT.Enabled   := False;
    btnAlterarCT.Enabled  := False;
    btnCancelarCT.Enabled := False;
    btnImprimirCT.Enabled := False;
    btnSairCT.Enabled     := True;
  end
  else if ModoCT = 'N' then
  begin
    GroupBox3.Enabled          := True;
    dbgTurmas.Enabled          := False;

    btnNovoCT.Enabled     := False;
    btnSalvarCT.Enabled   := True;
    btnAlterarCT.Enabled  := False;
    btnCancelarCT.Enabled := True;
    btnImprimirCT.Enabled := False;
    btnSairCT.Enabled     := False;
  end
  else if ModoCT = 'A' then
  begin
    GroupBox3.Enabled     := True;
    dbgTurmas.Enabled     := False;

    btnNovoCT.Enabled     := False;
    btnSalvarCT.Enabled   := False;
    btnAlterarCT.Enabled  := True;
    btnCancelarCT.Enabled := True;
    btnImprimirCT.Enabled := False;
    btnSairCT.Enabled     := False;
  end;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnNovoCTClick(
  Sender: TObject);
begin
  ModoTelaClasseTurma('N');
  MudaAba := False;
  edTurma.SetFocus;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnCancelarCTClick(
  Sender: TObject);
begin
  ModoTelaClasseTurma('P');
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnSalvarCTClick(
  Sender: TObject);
begin
  try
    //Verifica se o valor digitado j� encontra-se no banco de dados
    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByName ('@pe_dsc_turmacie').Value    := GetValue(edTurma.Text);
      Params.ParamByName ('@pe_cod_id_uecie').Value    := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value       := cbUnidadeEscolar.Text; //Copy(cbUnidadeEscolar.Text,8,50);
      Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByName ('@pe_dsc_seriecie').Value    := cbSerieEscolar.Text;
      Params.ParamByName ('@pe_ind_periodo').Value     := cbPeriodo.ItemIndex;
      Params.ParamByName ('@pe_num_turma').Value       := Null; //GetValue(edNumTurma.Text);
      Open;

      if (dmDeca.cdsSel_TurmaCIE.RecordCount > 0) then
      begin
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'A TURMA ESCOLAR informada j� encontra-se cadastrada no banco de dados.' + #13+#10 +
                                'Favor informar um outro nome de turma para prosseguir.',
                                '[Sistema Deca] - Turma Escolar CIE',
                                MB_OK + MB_ICONINFORMATION);
        edTurma.SetFocus;
      end
      else
      begin
        //N�o encontrou nome da Unidade faz a inser��o da atual no banco de dados
        with dmDeca.cdsIns_TurmaCIE do
        begin
          Close;
          Params.ParamByName ('@pe_dsc_turmacie').Value    := GetValue(edTurma.Text);
          Params.ParamByName ('@pe_cod_id_uecie').Value    := StrToInt(vListaUE1.Strings[cbUnidadeEscolar.ItemIndex]);
          Params.ParamByName ('@pe_cod_id_seriecie').Value := StrToInt(vListaSeriesCIE1.Strings[cbSerieEscolar.ItemIndex]);
          Params.ParamByName ('@pe_ind_periodo').Value     := cbPeriodo.ItemIndex;
          Params.ParamByName ('@pe_num_turma').Value       := GetValue(mskNumTurma.Text);
          Execute;
        end;

        //Atualiza o grid
        with dmDeca.cdsSel_TurmaCIE do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
          Params.ParamByName ('@pe_dsc_turmacie').Value    := Null;
          Params.ParamByName ('@pe_cod_id_uecie').Value    := Null;
          Params.ParamByName ('@pe_dsc_uecie').Value       := Null;
          Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
          Params.ParamByName ('@pe_dsc_seriecie').Value    := Null;
          Params.ParamByName ('@pe_ind_periodo').Value     := Null;
          Params.ParamByName ('@pe_num_turma').Value       := Null; 
          Open;
          dbgTurmas.Refresh;
        end;

        ModoTelaClasseTurma('P');
      end;
    end;
  except end;
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.btnAlterarCTClick(
  Sender: TObject);
begin
  //Atualizar os dados para o banco de dados
  try
    if Application.MessageBox ('Gravar a altera��o dos dados no banco de dados?',
                               '[Sistema Deca] - Alterar dados Turma',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      with dmDeca.cdsUpd_TurmaCIE do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_turmacie').Value := dmDeca.cdsSel_TurmaCIE.FieldByName ('cod_id_turmacie').Value;
        Params.ParamByName ('@pe_dsc_turmacie').Value    := GetValue(edTurma.Text);
        Params.ParamByName ('@pe_cod_id_uecie').Value    := StrToInt(vListaUE1.Strings[cbUnidadeEscolar.ItemIndex]);
        Params.ParamByName ('@pe_cod_id_seriecie').Value := StrToInt(vListaSeriesCIE1.Strings[cbSerieEscolar.ItemIndex]);
        Params.ParamByName ('@pe_ind_periodo').Value     := cbPeriodo.ItemIndex;
        Params.ParamByName ('@pe_num_turma').Value       := GetValue(mskNumTurma.Text);
        Execute;

        with dmDeca.cdsSel_TurmaCIE do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
          Params.ParamByName ('@pe_dsc_turmacie').Value    := Null;
          Params.ParamByName ('@pe_cod_id_uecie').Value    := Null;
          Params.ParamByName ('@pe_dsc_uecie').Value       := Null;
          Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
          Params.ParamByName ('@pe_dsc_seriecie').Value    := Null;
          Params.ParamByName ('@pe_ind_periodo').Value     := Null;
          Params.ParamByName ('@pe_num_turma').Value       := Null;
          Open;
          dbgTurmas.Refresh;
        end;
      end;
    end;
    ModoTelaClasseTurma('P');
  except
    Application.MessageBox('Um erro ocorreu na tentativa de gravar as altera��es!' + #13+#10 +
                           'Verifique sua conex�o e tente novamente.',
                           '[Sistema Deca] - Erro alterando Turma',
                           MB_OK + MB_ICONERROR);
    ModoTelaClasseTurma('P');
  end;
  MudaAba := True;
end;

procedure TfrmControleUnidadesEscolaresTurmasClasses.dbgTurmasDblClick(
  Sender: TObject);
begin
  {
  ModoTelaClasseTurma('A');
  //Repassa os valores de tela para os campos
  edTurma.Text     := dmDeca.cdsSel_TurmaCIE.FieldByName('dsc_turmacie').AsString;
  mskNumTurma.Text := dmDeca.cdsSel_TurmaCIE.FieldByName('num_turma').AsString;
  vListaUE2.Find(dmDeca.cdsSel_TurmaCIE.FieldByName('dsc_uecie').AsString, vIndex);
  cbUnidadeEscolar.ItemIndex := vIndex;
  vListaSeriesCIE2.Find(dmDeca.cdsSel_TurmaCIE.FieldByName('dsc_seriecie').AsString, vIndex2);
  cbSerieEscolar.ItemIndex := vIndex2;
  cbPeriodo.ItemIndex := dmDeca.cdsSel_TurmaCIE.FieldByName('ind_periodo').AsInteger;
  }
end;

end.
