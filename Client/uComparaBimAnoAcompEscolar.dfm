object frmComparaBimestreAno: TfrmComparaBimestreAno
  Left = 601
  Top = 315
  Width = 346
  Height = 298
  Caption = ','
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object TPanel
    Left = 2
    Top = 2
    Width = 321
    Height = 253
    TabOrder = 0
    object Gauge1: TGauge
      Left = 201
      Top = 260
      Width = 303
      Height = 35
      Progress = 0
    end
    object GroupBox1: TGroupBox
      Left = 9
      Top = 12
      Width = 301
      Height = 231
      Caption = '[Bimestre/Ano]'
      TabOrder = 0
      object Label2: TLabel
        Left = 159
        Top = 47
        Width = 63
        Height = 13
        Caption = 'ANO INICIAL'
        Visible = False
      end
      object Label4: TLabel
        Left = 34
        Top = 22
        Width = 242
        Height = 13
        Caption = 'INFORME ANO PARA AGERAÇÃO DA PLANILHA'
      end
      object SpeedButton1: TSpeedButton
        Left = 22
        Top = 95
        Width = 261
        Height = 39
        Caption = '&Gerar Planilha'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        Glyph.Data = {
          4E010000424D4E01000000000000760000002800000012000000120000000100
          040000000000D800000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777770000007777777777777777770000007777777777770007770000007444
          4400000006007700000074FFFF08880600080700000074F008000060EE070700
          000074FFFFF8060EE0047700000074F0088060EE00F47700000074FFFF060EE0
          00747700000074F00800EE0EE0047700000074FFFF0EE0F0EE047700000074F0
          080000F000047700000074FFFFFFFFFFFFF47700000074444444444444447700
          000074F444F444F444F477000000744444444444444477000000777777777777
          777777000000777777777777777777000000}
        ParentFont = False
        OnClick = SpeedButton1Click
      end
      object Label5: TLabel
        Left = 20
        Top = 136
        Width = 264
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'AGUARDE !!! GERANDO PLANILHA!!!'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object SpeedButton2: TSpeedButton
        Left = 21
        Top = 168
        Width = 261
        Height = 39
        Caption = 'Conferir Inconsistências'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton2Click
      end
      object mskAno1: TMaskEdit
        Left = 35
        Top = 48
        Width = 100
        Height = 37
        EditMask = '9999'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 4
        ParentFont = False
        TabOrder = 0
        Text = '    '
      end
      object mskAno2: TMaskEdit
        Left = 143
        Top = 47
        Width = 100
        Height = 37
        EditMask = '9999'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 4
        ParentFont = False
        TabOrder = 1
        Text = '    '
      end
    end
    object Panel1: TPanel
      Left = 304
      Top = 296
      Width = 185
      Height = 41
      Caption = 'Panel1'
      TabOrder = 1
      Visible = False
    end
    object ProgressBar1: TProgressBar
      Left = 241
      Top = 280
      Width = 304
      Height = 53
      Min = 0
      Max = 100
      TabOrder = 2
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 226
    Top = 27
  end
end
