object frmFatoresIntervTecnica: TfrmFatoresIntervTecnica
  Left = 260
  Top = 387
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Cadastro de Fatores para Interven��o T�nica'
  ClientHeight = 341
  ClientWidth = 517
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 504
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 19
      Width = 204
      Height = 13
      Caption = 'Descri��o do Fator de Interven��o T�cica:'
    end
    object edFator: TEdit
      Left = 232
      Top = 17
      Width = 256
      Height = 21
      TabOrder = 0
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 288
    Width = 502
    Height = 49
    TabOrder = 1
    object btnNovo: TBitBtn
      Left = 223
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para NOVA advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 298
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR a advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 358
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 436
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 64
    Width = 504
    Height = 217
    TabOrder = 2
    object dbgFatores: TDBGrid
      Left = 12
      Top = 18
      Width = 477
      Height = 185
      DataSource = dsSel_Fatores_Intervencao
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_id_fator'
          Title.Alignment = taCenter
          Title.Caption = 'C�d. Fator'
          Width = 60
          Visible = True
        end
        item
          Color = clAqua
          Expanded = False
          FieldName = 'dsc_fator'
          Title.Caption = 'Fator de Interven��o'
          Width = 175
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'log_data'
          Title.Alignment = taCenter
          Title.Caption = 'Inclus�o'
          Width = 85
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'log_data_altera'
          Title.Alignment = taCenter
          Title.Caption = 'Altera��o'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_usuario'
          Title.Caption = 'Usu�rio'
          Width = 150
          Visible = True
        end>
    end
  end
  object dsSel_Fatores_Intervencao: TDataSource
    DataSet = dmDeca.cdsSel_Fatores_Intervencao
    Left = 456
    Top = 88
  end
end
