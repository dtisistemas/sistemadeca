unit uReceituario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Buttons, StdCtrls, Mask, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmReceituario = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    edMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome1: TLabel;
    Label1: TLabel;
    mskDataReceituario: TMaskEdit;
    GroupBox2: TGroupBox;
    lbNumReceituario: TLabel;
    Panel2: TPanel;
    Label3: TLabel;
    cbMedicamento: TComboBox;
    Label4: TLabel;
    cbTipoUsoMedicamento: TComboBox;
    dbgItensReceituarios: TDBGrid;
    btnIncluirMedicamentoReceituario: TSpeedButton;
    brnExcluirMedicamentoReceituario: TSpeedButton;
    btnCadastrarMedicamento: TSpeedButton;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    btnNovoMedicamento: TBitBtn;
    btnSalvarMedicamento: TBitBtn;
    btnCancelarM: TBitBtn;
    btnImprimirMedicamentos: TBitBtn;
    btnSairM: TBitBtn;
    btnEncerraReceituario: TSpeedButton;
    Panel1: TPanel;
    btnNovoReceituario: TBitBtn;
    btnGravarReceituario: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimirReceituario: TBitBtn;
    btnSair: TBitBtn;
    Panel4: TPanel;
    dbgMedicamentos: TDBGrid;
    dsMedicamentos: TDataSource;
    btnAlterarMedicamento: TBitBtn;
    Panel5: TPanel;
    Label7: TLabel;
    edNomeMedicamento: TEdit;
    Label5: TLabel;
    cbTipoUso: TComboBox;
    Label8: TLabel;
    meObservacoesMedicamento: TMemo;
    btnDesativarMedicamento: TSpeedButton;
    dsItens_Receituario: TDataSource;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    mskMatriculaC: TMaskEdit;
    btnPesquisarConsulta: TSpeedButton;
    lbNomeConsulta: TLabel;
    Panel7: TPanel;
    dbgConsultaReceituario: TDBGrid;
    dbgResultadoConsulta: TDBGrid;
    Panel8: TPanel;
    btnImprimirConsulta: TBitBtn;
    BitBtn5: TBitBtn;
    dsConsultaReceituario: TDataSource;
    dsResultadoConsulta: TDataSource;
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ModoTelaR (Modo: String);
    procedure btnNovoReceituarioClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure ModoTelaM (Modo:String);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnSairMClick(Sender: TObject);
    procedure btnNovoMedicamentoClick(Sender: TObject);
    procedure btnCancelarMClick(Sender: TObject);
    procedure btnSalvarMedicamentoClick(Sender: TObject);
    procedure dbgMedicamentosCellClick(Column: TColumn);
    procedure dbgMedicamentosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgMedicamentosKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgMedicamentosDblClick(Sender: TObject);
    procedure btnAlterarMedicamentoClick(Sender: TObject);
    procedure dbgMedicamentosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnDesativarMedicamentoClick(Sender: TObject);
    procedure btnImprimirMedicamentosClick(Sender: TObject);
    procedure btnGravarReceituarioClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure cbTipoUsoMedicamentoClick(Sender: TObject);
    procedure btnIncluirMedicamentoReceituarioClick(Sender: TObject);
    procedure brnExcluirMedicamentoReceituarioClick(Sender: TObject);
    procedure btnEncerraReceituarioClick(Sender: TObject);
    procedure btnImprimirReceituarioClick(Sender: TObject);
    procedure btnCadastrarMedicamentoClick(Sender: TObject);
    procedure btnPesquisarConsultaClick(Sender: TObject);
    procedure dbgConsultaReceituarioCellClick(Column: TColumn);
    procedure btnImprimirConsultaClick(Sender: TObject);
  
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReceituario: TfrmReceituario;
  vMedicamentos1, vMedicamentos2 : TStringList;
  vvNUM_RECEITUARIO : Integer;
   
implementation

uses uDM, uUtil, rMedicamentos, uFichaPesquisa, uPrincipal, rReceituario;

{$R *.DFM}

procedure TfrmReceituario.btnSairClick(Sender: TObject);
begin
  frmReceituario.Close;
end;

procedure TfrmReceituario.FormShow(Sender: TObject);
begin
  ModoTelaR ('P');
  PageControl1.ActivePageIndex := 0;
  //dbgItensReceituarios.DataSource := nil;

  //Cria os vetores
  vMedicamentos1 := TStringList.Create();
  vMedicamentos2 := TStringList.Create();


  //Carrega os medicamentos
  try
    with dmDeca.cdsSEL_Medicamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_medicamento').Value := Null;
      Params.ParamByName ('@pe_ind_tipo_uso').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := 0;  //Somente os medicamentos "ATIVOS"
      Open;
    end;

    cbMedicamento.Clear;
    while not dmDeca.cdsSEL_Medicamento.eof do
    begin
      cbMedicamento.Items.Add (dmDeca.cdsSEL_Medicamento.FieldByName ('dsc_medicamento').Value);
      vMedicamentos1.Add (IntToStr(dmDeca.cdsSEL_Medicamento.FieldByName ('cod_id_medicamento').Value));
      vMedicamentos2.Add (dmDeca.cdsSEL_Medicamento.FieldByName ('dsc_medicamento').Value);
      dmDeca.cdsSEL_Medicamento.Next;
    end;
  except
    //Mensagem de erro caso aconte�a
  end;
end;

procedure TfrmReceituario.ModoTelaR(Modo: String);
begin
  if Modo = 'P' then  //Padr�o
  begin
    edMatricula.Clear;
    mskDataReceituario.Clear;
    lbNome1.Caption := '';
    GroupBox1.Enabled := False;
    lbNumReceituario.Caption := '';

    cbTipoUsoMedicamento.ItemIndex := -1;
    cbMedicamento.ItemIndex := -1;
    Panel2.Enabled := False;

    btnNovoReceituario.Enabled := True;
    btnGravarReceituario.Enabled := False;
    //btnAlterarMedicamento.Enabled:=False;
    btnCancelar.Enabled := False;
    btnImprimirReceituario.Enabled := False;
    btnSair.Enabled := True;

  end

  else if Modo = 'N' then  //Novo Receitu�rio
  begin
    GroupBox1.Enabled := True;
    mskDataReceituario.Text := DateToStr(Date());

    Panel2.Enabled := False;

    btnNovoReceituario.Enabled := False;
    btnGravarReceituario.Enabled := True;
    //btnAlterarMedicamento.Enabled:=False;
    btnCancelar.Enabled := True;
    btnImprimirReceituario.Enabled := False;
    btnSair.Enabled := False;
    //dbgItensReceituarios.DataSource := dsItens_Receituario;
  end


  else if Modo = 'A' then  //Novo Receitu�rio
  begin
    GroupBox1.Enabled := True;

    Panel2.Enabled := True;

    btnNovoReceituario.Enabled := False;
    btnGravarReceituario.Enabled := False;
    btnAlterarMedicamento.Enabled:=True;
    btnCancelar.Enabled := True;
    btnImprimirReceituario.Enabled := False;
    btnSair.Enabled := False;

  end;
end;

procedure TfrmReceituario.btnNovoReceituarioClick(Sender: TObject);
begin
  ModoTelaR ('N');
  edMatricula.SetFocus;
  mskDataReceituario.Text := DateToStr(Date());

  with dmDeca.cdsUltimoReceituario do
  begin
    close;
    open;
    lbNumReceituario.Caption:=IntToStr(dmDeca.cdsUltimoReceituario.fieldbyname('UltimoReceituario').value);
    vvNUM_RECEITUARIO := dmDeca.cdsUltimoReceituario.fieldbyname('UltimoReceituario').value + 1;
  end;
end;

procedure TfrmReceituario.btnCancelarClick(Sender: TObject);
begin
  ModoTelaR ('P');
end;

procedure TfrmReceituario.ModoTelaM(Modo: String);
begin
if Modo='P' then
begin
edNomeMedicamento.Clear;
cbTipoUso.Enabled:=True;
cbTipoUsoMedicamento.ItemIndex:=-1;
meObservacoesMedicamento.Clear;
Panel5.Enabled:=False;

btnDesativarMedicamento.Enabled := True;
btnNovoMedicamento.Enabled:=True;
btnSairM.Enabled:=True;
btnSalvarMedicamento.Enabled:=False;
btnAlterarMedicamento.Enabled:=False;
btnCancelarM.Enabled:=False;
btnImprimirMedicamentos.Enabled:=True;

end
else if Modo='N' then
begin

Panel5.Enabled:=True;

btnDesativarMedicamento.Enabled := False;
btnNovoMedicamento.Enabled:=False;
btnSairM.Enabled:=False;
btnSalvarMedicamento.Enabled:=True;
btnAlterarMedicamento.Enabled:=False;
btnCancelarM.Enabled:=True;
btnImprimirMedicamentos.Enabled:=False;

end
else if Modo='A' then
begin

Panel5.Enabled:=True;
cbTipoUso.Enabled:=False;

btnDesativarMedicamento.Enabled := False;
btnNovoMedicamento.Enabled:=False;
btnSairM.Enabled:=False;
btnSalvarMedicamento.Enabled:=False;
btnAlterarMedicamento.Enabled:=True;
btnCancelarM.Enabled:=True;
btnImprimirMedicamentos.Enabled:=False;

end;
end;

procedure TfrmReceituario.TabSheet2Show(Sender: TObject);
begin
ModoTelaM('P');

  with dmDeca.cdsSEL_Medicamento do
  begin
  close;
  params.ParamByName('@pe_cod_id_medicamento').Value:=Null ;
  params.ParamByName('@pe_ind_tipo_uso').Value:=Null ;
  params.ParamByName('@pe_flg_status').Value:=Null ;
  open;

  dbgMedicamentos.Refresh;
  end;
end;

procedure TfrmReceituario.btnSairMClick(Sender: TObject);
begin
frmReceituario.Close;
end;

procedure TfrmReceituario.btnNovoMedicamentoClick(Sender: TObject);
begin
ModoTelaM('N');
end;

procedure TfrmReceituario.btnCancelarMClick(Sender: TObject);
begin
ModoTelaM('P');
end;

procedure TfrmReceituario.btnSalvarMedicamentoClick(Sender: TObject);
begin
try
with dmDeca.cdsINC_Medicamento do
begin
  close;

  params.ParamByName('@pe_dsc_medicamento').Value:=GetValue(edNomeMedicamento.Text);
  params.ParamByName('@pe_dsc_observacao').Value:=GetValue(meObservacoesMedicamento.text);
  params.ParamByName('@pe_ind_tipo_uso').Value:=cbTipoUso.ItemIndex ;
  params.ParamByName('@pe_flg_status').Value:= 0; //ativo
  execute;

  with dmDeca.cdsSEL_Medicamento do
  begin
  close;
  params.ParamByName('@pe_cod_id_medicamento').Value:=Null ;
  params.ParamByName('@pe_ind_tipo_uso').Value:=Null ;
  params.ParamByName('@pe_flg_status').Value:=Null ;
  open;

  dbgMedicamentos.Refresh;
  end;

  ModoTelaM('P');

end
except
    Application.MessageBox('Aten��o!Ocorreu um erro!'+#13+#10+#13+#10+
          'Os dados n�o foram cadastrados corretamente.',
          'Cadastro de Medicamentos',
          MB_OK + MB_ICONERROR);

end;
end;

procedure TfrmReceituario.dbgMedicamentosCellClick(Column: TColumn);
begin
edNomeMedicamento.Text:=dmDeca.cdsSEL_Medicamento.fieldbyName('dsc_medicamento').value;
cbTipoUso.ItemIndex:=dmDeca.cdsSEL_Medicamento.fieldbyName('ind_tipo_uso').value;
meObservacoesMedicamento.Text:=dmDeca.cdsSEL_Medicamento.fieldbyName('dsc_observacao').value;
end;

procedure TfrmReceituario.dbgMedicamentosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
edNomeMedicamento.Text:=dmDeca.cdsSEL_Medicamento.fieldbyName('dsc_medicamento').value;
cbTipoUso.ItemIndex:=dmDeca.cdsSEL_Medicamento.fieldbyName('ind_tipo_uso').value;
meObservacoesMedicamento.Text:=dmDeca.cdsSEL_Medicamento.fieldbyName('dsc_observacao').value;
end;

procedure TfrmReceituario.dbgMedicamentosKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
edNomeMedicamento.Text:=dmDeca.cdsSEL_Medicamento.fieldbyName('dsc_medicamento').value;
cbTipoUso.ItemIndex:=dmDeca.cdsSEL_Medicamento.fieldbyName('ind_tipo_uso').value;
meObservacoesMedicamento.Text:=dmDeca.cdsSEL_Medicamento.fieldbyName('dsc_observacao').value;
end;

procedure TfrmReceituario.dbgMedicamentosDblClick(Sender: TObject);
begin

if dmDeca.cdsSEL_Medicamento.FieldByName ('flg_status').Value = 0 then
  ModoTelaM('A')
else
  begin
    Application.MessageBox('Aten��o! Esse medicamento encontra-se INATIVO.!'+#13+#10+#13+#10+
                           'Favor ATIV�-LO e efetuar a altera��o necess�ria.',
                           'Cadastro de Medicamentos',
                           MB_OK + MB_ICONERROR);
    ModoTelaM('P');

  end;

end;

procedure TfrmReceituario.btnAlterarMedicamentoClick(Sender: TObject);
begin
 try

with dmDeca.cdsALT_Medicamento do
  begin
  close;
  params.ParamByName('@pe_dsc_medicamento').Value:=GetValue(edNomeMedicamento.text) ;
  params.ParamByName('@pe_dsc_observacao').Value:=GetValue(meObservacoesMedicamento.Text) ;
  params.parambyname('@pe_cod_id_medicamento').value:=dmDeca.cdsSEL_Medicamento.fieldbyname('cod_id_medicamento').value;
  execute;

  with dmDeca.cdsSEL_Medicamento do
  begin
  close;
  params.ParamByName('@pe_cod_id_medicamento').Value:=Null ;
  params.ParamByName('@pe_ind_tipo_uso').Value:=Null ;
  params.ParamByName('@pe_flg_status').Value:=Null ;
  open;

  dbgMedicamentos.Refresh;
  end;

  ModoTelaM('P');

  end

except
Application.MessageBox('Aten��o!Ocorreu um erro!'+#13+#10+#13+#10+
          'Os dados n�o foram cadastrados corretamente.',
          'Cadastro de Medicamentos',
          MB_OK + MB_ICONERROR);
end;

end;

procedure TfrmReceituario.dbgMedicamentosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (dmDeca.cdsSel_Medicamento.FieldByName('flg_status').value = 0) then
  begin
    dbgMedicamentos.Canvas.Font.Color:= clWhite;
    dbgMedicamentos.Canvas.Brush.Color:= clGreen;
  end
  else
  begin
    dbgMedicamentos.Canvas.Font.Color:= clWhite;
    dbgMedicamentos.Canvas.Brush.Color:= clRed;
  end;
  dbgMedicamentos.Canvas.FillRect(Rect);
  dbgMedicamentos.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);
end;

procedure TfrmReceituario.btnDesativarMedicamentoClick(Sender: TObject);
begin
  with dmDeca.cdsALT_Desativa_Medicamento do
  begin
    close;
    params.ParamByName('@pe_cod_id_medicamento').value:=dmDeca.cdsSEL_Medicamento.fieldbyname('cod_id_medicamento').value;
    if dmDeca.cdsSEL_Medicamento.FieldByName('flg_status').value=0 then
      params.ParamByName('@pe_flg_status').value:=1
    else
      params.ParamByName('@pe_flg_status').value:=0;

    execute;

    with dmDeca.cdsSEL_Medicamento do
    begin
      close;
      params.ParamByName('@pe_cod_id_medicamento').Value:=Null ;
      params.ParamByName('@pe_ind_tipo_uso').Value:=Null ;
      params.ParamByName('@pe_flg_status').Value:=Null ;
      open;

      dbgMedicamentos.Refresh;
    end;

  ModoTelaM('P');

  end;

end;

procedure TfrmReceituario.btnImprimirMedicamentosClick(Sender: TObject);
begin
  Application.CreateForm (TrelMedicamentos, relMedicamentos);

  with dmDeca.cdsSEL_Medicamento do
  begin
    close;
    params.ParamByName('@pe_cod_id_medicamento').Value:=Null ;
    params.ParamByName('@pe_ind_tipo_uso').Value:=Null ;
    params.ParamByName('@pe_flg_status').Value:=Null ;
    open;

    relMedicamentos.Preview;
    relMedicamentos.Free;

  end;

end;

procedure TfrmReceituario.btnGravarReceituarioClick(Sender: TObject);
begin

  //Validar Matricula para a Data informada - N�o pode haver mais de um receituario na mesma data para a matricula
  try
    with dmDeca.cdsSel_Receituario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_receituario').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(edMatricula.Text);
      Params.ParamByName ('@pe_dat_emissao').Value := StrToDate(mskDataReceituario.Text);
      Open;

      //Se n�o achou...
      if dmDeca.cdsSel_Receituario.RecordCount < 1 then
      begin
        with dmDeca.cdsInc_Receituario do
        begin
          close;
          Params.ParamByName('@pe_cod_matricula').Value:= GetValue(edmatricula.text);
          Params.ParamByName('@pe_dat_emissao').Value:= GetValue(mskdatareceituario,vtdate);
          execute;
        end;

        GroupBox1.Enabled:=False;
        panel2.Enabled:=True;
        btnNovoMedicamento.Enabled:=False;
        btnGravarReceituario.Enabled:=False;
        btnCancelar.Enabled:=False;
        btnImprimirReceituario.Enabled:=False;
        btnSair.Enabled:=False;

      end
      else
      //Se achou...
      begin
        Application.MessageBox('Aten��o!J� existe um receitu�rio lan�ado nesta data para o paciente!'+#13+#10+#13+#10+
          'Favor verificar ou informar outra data.',
          'Cadastro de Receitu�rio',
          MB_OK + MB_ICONERROR);
      end;
    end;
  except end;

end;

procedure TfrmReceituario.btnPesquisarClick(Sender: TObject);
begin
 // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    edMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := edMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        edMatricula.Text := FieldByName('cod_matricula').AsString;
        lbNome1.Caption := FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmReceituario.cbTipoUsoMedicamentoClick(Sender: TObject);
begin
  vMedicamentos1.free;
  vMedicamentos2.free;

  //Cria os vetores
  vMedicamentos1 := TStringList.Create();
  vMedicamentos2 := TStringList.Create();

  //Carrega os medicamentos
  try
    with dmDeca.cdsSEL_Medicamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_medicamento').Value := Null;
      Params.ParamByName ('@pe_ind_tipo_uso').Value := cbTipoUsoMedicamento.ItemIndex;
      Params.ParamByName ('@pe_flg_status').Value := 0;  //Somente os medicamentos "ATIVOS"
      Open;
    end;

    cbMedicamento.Clear;
    while not dmDeca.cdsSEL_Medicamento.eof do
    begin
      cbMedicamento.Items.Add (dmDeca.cdsSEL_Medicamento.FieldByName ('dsc_medicamento').Value);
      vMedicamentos1.Add (IntToStr(dmDeca.cdsSEL_Medicamento.FieldByName ('cod_id_medicamento').Value));
      vMedicamentos2.Add (dmDeca.cdsSEL_Medicamento.FieldByName ('dsc_medicamento').Value);
      dmDeca.cdsSEL_Medicamento.Next;
    end;
  except
    //Mensagem de erro caso aconte�a
  end;
end;

procedure TfrmReceituario.btnIncluirMedicamentoReceituarioClick(
  Sender: TObject);
begin
  //Verificar se j� existe o medicamento lan�ado para matr�cula, e data 

  try
  with dmDeca.cdsSel_Item_Receituario do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_item_receituario').Value := Null;
    Params.ParamByName ('@pe_cod_id_receituario').Value := vvNUM_RECEITUARIO;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;//GetValue(edMatricula.Text);
    Params.ParamByName ('@pe_dat_emissao').Value := Null;//GetValue(mskDataReceituario, vtDate);
    Params.ParamByName ('@pe_cod_id_medicamento').Value := StrToInt(vMedicamentos1.Strings[cbMedicamento.ItemIndex]);
    Open;

    //Se n�o encontrou ....
    //Inclua
    if dmDeca.cdsSel_Item_Receituario.RecordCount < 1 then
    begin
      with dmDeca.cdsInc_Item_Receituario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_receituario').Value := vvNUM_RECEITUARIO; //StrToInt(lbNumReceituario.Caption);
        Params.ParamByName ('@pe_cod_matricula').Value := GetValue(edMatricula.Text);
        Params.ParamByName ('@pe_dat_emissao').Value := GetValue(mskDataReceituario, vtDate);
        Params.ParamByName ('@pe_cod_id_medicamento').Value := StrToInt(vMedicamentos1.Strings[cbMedicamento.ItemIndex]);
        Execute;

        //Atualiza o grid
        with dmDeca.cdsSel_Item_Receituario do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_item_receituario').Value := Null;
          Params.ParamByName ('@pe_cod_id_receituario').Value := vvNUM_RECEITUARIO; //StrToInt(lbNumReceituario.Caption);
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(edMatricula.Text);
          Params.ParamByName ('@pe_dat_emissao').Value := Null;//GetValue(mskDataReceituario, vtDate);
          Params.ParamByName ('@pe_cod_id_medicamento').Value := Null;//StrToInt(vMedicamentos1.Strings[cbMedicamento.ItemIndex]);
          Open;
          dbgItensReceituarios.Refresh;
        end;
      end;
    end
    //Se encontrou
    else
    begin
      Application.MessageBox('Aten��o!!! O medicamento j� est� lan�ado para o receitu�rio atual !'+#13+#10+#13+#10+
                             'Favor selecionar outro medicamento.',
                             'Cadastro de Medicamento/Receitu�rio',
                             MB_OK + MB_ICONERROR);
    end;
  end;
  except end;


end;

procedure TfrmReceituario.brnExcluirMedicamentoReceituarioClick(
  Sender: TObject);
begin
  //Pede confirma��o para exclus�o do medicamento do receitu�rio
  if Application.MessageBox('Deseja Excluir o medicamento do receitu�rio ?',
                            'Receitu�rio - Exclus�o de Medicamento',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    try
      with dmDeca.cdsExc_Item_Receituario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_item_receituario').Value := dmDeca.cdsSel_Item_Receituario.FieldByName ('cod_id_item_receituario').Value;
        Execute;

        with dmDeca.cdsSel_Item_Receituario do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_item_receituario').Value := Null;
          Params.ParamByName ('@pe_cod_id_receituario').Value := StrToInt(lbNumReceituario.Caption);
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_dat_emissao').Value := Null;
          Params.ParamByName ('@pe_cod_id_medicamento').Value := Null;
          Open;
          dbgItensReceituarios.Refresh;
        end;
      end;
    except
      //Mensagem de erro
    end;
  end;
end;

procedure TfrmReceituario.btnEncerraReceituarioClick(Sender: TObject);
begin
  if Application.MessageBox('Deseja encerrar lan�amento para o Receitu�rio ?',
                            'Receitu�rio - Encerramento',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin

    try
      with dmDeca.cdsAlt_EncerraReceituario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_receituario').Value := StrToInt(lbNumReceituario.Caption);
        Execute;
      end;
    except
      //Mensagem de erro
    end;

    //Oculta dados do grid

    btnImprimirReceituario.Enabled:=True;
    //dbgItensReceituarios.DataSource := nil;
  end;
end;

procedure TfrmReceituario.btnImprimirReceituarioClick(Sender: TObject);
begin
try
  with dmDeca.cdsSel_MontaReceituario do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_receituario').value := vvNUM_RECEITUARIO;
    Params.ParamByName('@pe_cod_matricula').value:=GetValue(edMatricula.Text);
    Open;

    Application.CreateForm(TRelreceituario,Relreceituario);
    relreceituario.preview;
    relreceituario.free;

  end;
except
  Application.MessageBox('Aten��o!Um erro ocorreu!'+#13+#10+#13+#10+
          'O relat�rio � imposs�vel de ser gerado.',
          'Impress�o de Receitu�rio',
          MB_OK + MB_ICONERROR);
end;
ModoTelaR('P');

end;

procedure TfrmReceituario.btnCadastrarMedicamentoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex:=1;
end;

procedure TfrmReceituario.btnPesquisarConsultaClick(Sender: TObject);
begin
 // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    mskMatriculaC.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
        begin
          Close;
          Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
          Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatriculaC.Text;
          Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
          Open;
          mskMatriculaC.Text := FieldByName('cod_matricula').AsString;
          lbNomeConsulta.Caption := FieldByName('nom_nome').AsString;

          with dmDeca.cdsSel_Receituario do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_receituario').Value := Null;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(mskMatriculaC.Text);
            Params.ParamByName ('@pe_dat_emissao').Value := null;
            Open;

            dbgConsultaReceituario.Refresh;
          end;
        end;
    except end;
  end;
end;


procedure TfrmReceituario.dbgConsultaReceituarioCellClick(Column: TColumn);
begin
 with dmDeca.cdsSel_Item_Receituario do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_item_receituario').Value := Null;
          Params.ParamByName ('@pe_cod_id_receituario').Value := dmDeca.cdsSel_Receituario.fieldbyname('cod_id_receituario').value;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_dat_emissao').Value := Null;
          Params.ParamByName ('@pe_cod_id_medicamento').Value := Null;
          Open;
          dbgResultadoConsulta.Refresh;
        end;
end;

procedure TfrmReceituario.btnImprimirConsultaClick(Sender: TObject);
begin
 try
  with dmDeca.cdsSel_MontaReceituario do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_receituario').value:=dmDeca.cdsSel_Receituario.fieldbyname('cod_id_receituario').value;
    Params.ParamByName('@pe_cod_matricula').value:=GetValue(mskMatriculaC.Text);
    Open;

    Application.CreateForm(TRelreceituario,Relreceituario);
    relreceituario.preview;
    relreceituario.free;

  end;
except
  Application.MessageBox('Aten��o!Um erro ocorreu!'+#13+#10+#13+#10+
                         'O relat�rio � imposs�vel de ser gerado.',
                         'Impress�o de Receitu�rio',
                         MB_OK + MB_ICONERROR);
end;

ModoTelaR('P');

end;


end.
