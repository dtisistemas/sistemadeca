object frmFichaClinica: TfrmFichaClinica
  Left = 627
  Top = 264
  BorderStyle = bsDialog
  Caption = 'Sistema Deca  -  [Ficha Cl�nica do Paciente]'
  ClientHeight = 547
  ClientWidth = 571
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 661
    Top = 731
    Width = 852
    Height = 49
    TabOrder = 0
    object btnNovo: TBitBtn
      Left = 500
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = False
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 575
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 635
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = False
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnImprimir: TBitBtn
      Left = 30
      Top = 10
      Width = 200
      Height = 30
      Hint = 'Clique para IMPRIMIR advert�ncia'
      Caption = 'Emitir Receitu�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777776B00777777777000777777776E007777777007880077777771007777
        7007780088007777740077770778878800880077770077778887778888008077
        7A00777887777788888800777D007778F7777F888888880780007778F77FF777
        8888880783007778FFF779977788880786007778F77AA7778807880789007777
        88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
        920077777777778FFFFFF0079500777777777778FFF887779800777777777777
        888777779B00777777777777777777779E0077777777777777777777A1007777
        7777777777777777A400}
    end
    object btnSair: TBitBtn
      Left = 785
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object BitBtn1: TBitBtn
      Left = 235
      Top = 10
      Width = 200
      Height = 30
      Hint = 'Clique para IMPRIMIR advert�ncia'
      Caption = 'Marcar Consulta'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FFFFFFFFFFFFFFF000000000000000077777777777777770FF7FF7FF7FF
        7FF07FF7FF7FF7F37F3709F79F79F7FF7FF077F77F77F7FF7FF7077777777777
        777077777777777777770FF7FF7FF7FF7FF07FF7FF7FF7FF7FF709F79F79F79F
        79F077F77F77F77F77F7077777777777777077777777777777770FF7FF7FF7FF
        7FF07FF7FF7FF7FF7FF709F79F79F79F79F077F77F77F77F77F7077777777777
        777077777777777777770FFFFF7FF7FF7FF07F33337FF7FF7FF70FFFFF79F79F
        79F07FFFFF77F77F77F700000000000000007777777777777777CCCCCC8888CC
        CCCC777777FFFF777777CCCCCCCCCCCCCCCC7777777777777777}
      NumGlyphs = 2
    end
  end
  object PageControl2: TPageControl
    Left = 1
    Top = 4
    Width = 571
    Height = 544
    ActivePage = TabSheet5
    TabOrder = 1
    object TabSheet5: TTabSheet
      Caption = 'Agenda'
      object Panel4: TPanel
        Left = 4
        Top = 316
        Width = 556
        Height = 49
        TabOrder = 0
        object btnREMARCAR: TSpeedButton
          Left = 11
          Top = 6
          Width = 175
          Height = 34
          Caption = 'Remarcar Consulta'
          Flat = True
          OnClick = btnREMARCARClick
        end
        object btnPacienteAusente: TSpeedButton
          Left = 190
          Top = 6
          Width = 175
          Height = 34
          Caption = 'Paciente Ausente'
          Flat = True
          OnClick = btnPacienteAusenteClick
        end
        object btnImprimirAgenda: TSpeedButton
          Left = 370
          Top = 6
          Width = 175
          Height = 34
          Caption = 'Imprimir Agenda'
          Flat = True
          OnClick = btnImprimirAgendaClick
        end
      end
      object GroupBox11: TGroupBox
        Left = 4
        Top = 4
        Width = 557
        Height = 308
        TabOrder = 1
        object Label5: TLabel
          Left = 16
          Top = 24
          Width = 75
          Height = 13
          Caption = 'Filtrar pela data:'
        end
        object btnVerConsultasData: TSpeedButton
          Left = 217
          Top = 20
          Width = 26
          Height = 24
          Flat = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            77777000000000000007707778FF7FF7FF077077788F78F78F07708888877877
            87077077780078F78F077077780E0FF78F0770888870E0777707700000FF0E07
            FF077077770F70E0FF07077777707F0E0F070F7555707FF0E0070F7577704444
            0E070F757770000000E070FFF707777777007700007777777777}
          OnClick = btnVerConsultasDataClick
        end
        object dataFiltro: TDateTimePicker
          Left = 95
          Top = 22
          Width = 120
          Height = 21
          CalAlignment = dtaLeft
          Date = 39232.6155042593
          Time = 39232.6155042593
          DateFormat = dfShort
          DateMode = dmComboBox
          Kind = dtkDate
          ParseInput = False
          TabOrder = 0
        end
        object dbAgenda: TDBGrid
          Left = 8
          Top = 49
          Width = 539
          Height = 248
          DataSource = dsAgenda
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dbAgendaDblClick
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'cod_matricula'
              Title.Alignment = taCenter
              Title.Caption = 'Matricula'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_nome'
              Title.Caption = 'Paciente'
              Width = 190
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDataConsulta'
              Title.Alignment = taCenter
              Title.Caption = 'Data'
              Width = 65
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vHoraConsulta'
              Title.Alignment = taCenter
              Title.Caption = 'Hora'
              Width = 45
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vStatusConsulta'
              Title.Alignment = taCenter
              Title.Caption = 'Situa��o'
              Width = 75
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vData'
              Title.Alignment = taCenter
              Title.Caption = 'Inclu�do...'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'cod_id_consulta'
              Visible = False
            end>
        end
      end
      object GroupBox12: TGroupBox
        Left = 3
        Top = 365
        Width = 557
        Height = 144
        TabOrder = 2
        object Label6: TLabel
          Left = 26
          Top = 20
          Width = 59
          Height = 13
          Caption = 'Matr�cula:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNome1: TLabel
          Left = 219
          Top = 17
          Width = 262
          Height = 18
          Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SpeedButton1: TSpeedButton
          Left = 179
          Top = 14
          Width = 28
          Height = 24
          Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777770000777777777777777700770000777777777777777C440700007777
            7777777777C4C40700007777777777777C4C4077000077777777777784C40777
            0000777777788888F740777700007777770000007807777700007777707EFEE6
            007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
            8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
            0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
            7777777700A077777777777777777777FFA077777777777777777777FFFF7777
            7777777777777777FF81}
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton1Click
        end
        object Label10: TLabel
          Left = 20
          Top = 43
          Width = 67
          Height = 13
          Caption = 'Data Consulta'
        end
        object Label11: TLabel
          Left = 182
          Top = 41
          Width = 82
          Height = 13
          Caption = 'Hora da Consulta'
        end
        object btnAgendarConsulta: TSpeedButton
          Left = 382
          Top = 61
          Width = 168
          Height = 44
          Caption = 'Agendar Consulta/Retorno'
          Flat = True
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777770000777777777777777700770000777777777777777C440700007777
            7777777777C4C40700007777777777777C4C4077000077777777777784C40777
            0000777777788888F740777700007777770000007807777700007777707EFEE6
            007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
            8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
            0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
            7777777700A077777777777777777777FFA077777777777777777777FFFF7777
            7777777777777777FF81}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnAgendarConsultaClick
        end
        object Label12: TLabel
          Left = 48
          Top = 65
          Width = 40
          Height = 13
          Caption = 'Unidade'
        end
        object Label13: TLabel
          Left = 368
          Top = 40
          Width = 56
          Height = 13
          Caption = 'Nascimento'
        end
        object Label7: TLabel
          Left = 24
          Top = 88
          Width = 64
          Height = 13
          Caption = 'Fone Contato'
        end
        object Label14: TLabel
          Left = 21
          Top = 111
          Width = 68
          Height = 13
          Caption = 'Nome Contato'
        end
        object Label15: TLabel
          Left = 192
          Top = 87
          Width = 54
          Height = 13
          Caption = 'Parentesco'
        end
        object txtMatricula1: TMaskEdit
          Left = 92
          Top = 16
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '!99999999;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '        '
        end
        object txtData1: TMaskEdit
          Left = 92
          Top = 39
          Width = 83
          Height = 21
          Color = clAqua
          EditMask = '99/99/9999'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '  /  /    '
        end
        object txtHora1: TMaskEdit
          Left = 270
          Top = 38
          Width = 83
          Height = 21
          Color = clAqua
          EditMask = '99:99'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 5
          ParentFont = False
          TabOrder = 2
          Text = '  :  '
        end
        object edUnidade1: TEdit
          Left = 92
          Top = 62
          Width = 286
          Height = 21
          Color = clInfoBk
          Enabled = False
          TabOrder = 3
        end
        object edNascimento1: TMaskEdit
          Left = 431
          Top = 37
          Width = 84
          Height = 21
          Color = clInfoBk
          Enabled = False
          EditMask = '99/99/9999'
          MaxLength = 10
          ReadOnly = True
          TabOrder = 4
          Text = '  /  /    '
        end
        object edFoneContato: TMaskEdit
          Left = 92
          Top = 85
          Width = 91
          Height = 21
          Color = clScrollBar
          EditMask = '(99)9999-9999'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 13
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          Text = '(  )    -    '
        end
        object edNomeContato: TEdit
          Left = 92
          Top = 108
          Width = 224
          Height = 21
          Color = clScrollBar
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object cbParentesco: TComboBox
          Left = 249
          Top = 84
          Width = 130
          Height = 22
          Style = csOwnerDrawFixed
          Color = clScrollBar
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 16
          ParentFont = False
          TabOrder = 7
          Items.Strings = (
            'PAI'
            'M�E'
            'PADRASTO'
            'MADRASTA'
            'AV�/AV�'
            'TIO/TIA'
            'IRM�O/IRM�'
            'PRIMO/PRIMA'
            'ENTIDADE SOCIAL'
            'ASSISTENTE SOCIAL'
            'FILHO/FILHA'
            'OUTROS')
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Consultas'
      ImageIndex = 1
      OnShow = TabSheet6Show
      object GroupBox1: TGroupBox
        Left = 5
        Top = 8
        Width = 557
        Height = 49
        TabOrder = 0
        Visible = False
        object Label1: TLabel
          Left = 16
          Top = 20
          Width = 59
          Height = 13
          Caption = 'Matr�cula:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNome: TLabel
          Left = 216
          Top = 19
          Width = 262
          Height = 18
          Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnPesquisar: TSpeedButton
          Left = 169
          Top = 14
          Width = 28
          Height = 24
          Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777770000777777777777777700770000777777777777777C440700007777
            7777777777C4C40700007777777777777C4C4077000077777777777784C40777
            0000777777788888F740777700007777770000007807777700007777707EFEE6
            007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
            8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
            0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
            7777777700A077777777777777777777FFA077777777777777777777FFFF7777
            7777777777777777FF81}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnPesquisarClick
        end
        object txtMatricula: TMaskEdit
          Left = 82
          Top = 16
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '!99999999;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '        '
        end
      end
      object PageControl1: TPageControl
        Left = 5
        Top = 5
        Width = 557
        Height = 496
        ActivePage = TabSheet1
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = 'Dados da Consulta'
          object btnGravarDadosConsulta: TSpeedButton
            Left = 6
            Top = 430
            Width = 160
            Height = 30
            Caption = 'Gravar Dados da Consulta'
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333FFFFF3333333333000003333333333F77777FFF333333009999900
              3333333777777777FF33330998FFF899033333777333F3777FF33099FFFCFFF9
              903337773337333777F3309FFFFFFFCF9033377333F3337377FF098FF0FFFFFF
              890377F3373F3333377F09FFFF0FFFFFF90377F3F373FFFFF77F09FCFFF90000
              F90377F733377777377F09FFFFFFFFFFF90377F333333333377F098FFFFFFFFF
              890377FF3F33333F3773309FCFFFFFCF9033377F7333F37377F33099FFFCFFF9
              90333777FF37F3377733330998FCF899033333777FF7FF777333333009999900
              3333333777777777333333333000003333333333377777333333}
            NumGlyphs = 2
            OnClick = btnGravarDadosConsultaClick
          end
          object btnEncerrarConsulta: TSpeedButton
            Left = 194
            Top = 430
            Width = 160
            Height = 30
            Caption = 'Encerrar Consulta'
            Flat = True
            Glyph.Data = {
              02030000424D0203000000000000360100002800000013000000170000000100
              080000000000CC010000C30E0000C30E000040000000000000001C3404002434
              1C00242424001C3C0400243C0C00244404002C5C04003C5C240044543C005C5C
              54005C5C5C00646464006C6C6C0054743C007474740044840400747C74007C7C
              7C0084848400449404006C8C540054AC0400000000008C8C8C008C948C009494
              94009C9C9C00A4A4A400ACACAC00B4B4B4006CD404006CDC040074F404007CFC
              040084FC0C0084FC14007CDC24008CFC1C008CFC240094FC240094EC3C0094FC
              2C009CFC3C0094D45C009CF44C009CFC4400A4FC4C00A4FC5400ACFC6400B4FC
              6C00B4F47400BCF48400BCFC7C00B4C4A400ACCC9400BCCCAC00BCC4B400BCCC
              B400B4E48C00BCE49400BCDCA400C4F49400C4FC8C00C0C0C0003F3F3F3F3F3F
              191717193F3F3F3F3F3F3F3F3F003F3F3F3F3F1712111112193F3F3F3F3F3F3F
              3F003F3F3F3F19120E0C0C0E123F3F3F3F3F3F3F3F003F3F3F3F120E0C0B0B0C
              11173F3F3F3F3F3F3F003F3F3F17110C0B0A0A0B0E123F3F3F3F3F3F3F003F3F
              3F12140702010B0B0C11173F3F3F3F3F3F003F3F3F181E1E0F03100C0C0E1219
              3F3F3F3F3F003F3F3F2422231F06080C0C0C11173F3F3F3F3F003F3F2B212223
              221305170C0C0E11173F3F3F3F003F3521222323231E06090E0C0C0E12193F3F
              3F003F2B2223272726221304180E0C0C0E123F3F3F003F2926252A2F2F261F06
              08110E0C0E11173F3F0038302D232C39332E23150311110E0C0E11173F003F39
              2E28383F37312A220F0117110E0E0E1219003F3F373F3F3F3F3A30261E060917
              110E0E1117003F3F3F3F3F3F3F3F322E2315030C1712111217003F3F3F3F3F3F
              3F3F37342D2313001819171719003F3F3F3F3F3F3F3F3F3B342E231300193F3F
              3F003F3F3F3F3F3F3F3F3F3F3C3330230F011D3F3F003F3F3F3F3F3F3F3F3F3F
              3F393E31250F0D3F3F003F3F3F3F3F3F3F3F3F3F3F3F383D312320353F003F3F
              3F3F3F3F3F3F3F3F3F3F3F3F3C2A23363F003F3F3F3F3F3F3F3F3F3F3F3F3F3F
              3F3F373F3F00}
            OnClick = btnEncerrarConsultaClick
          end
          object btnEmitirReceituario: TSpeedButton
            Left = 378
            Top = 430
            Width = 160
            Height = 30
            Caption = 'Receitu�rio'
            Flat = True
            Glyph.Data = {
              66010000424D6601000000000000760000002800000014000000140000000100
              040000000000F000000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
              7777777700007777777777777777777700007777777777777777777700007700
              00000000007700770000770FFFFFFFFFF07000770000770FFFFFFF0000800777
              0000770FFFFFF087780877770000770FFFFF0877E88077770000770FFFFF0777
              787077770000770FFFFF07E7787077770000770FFFFF08EE788077770000770F
              FFFFF087780777770000770FFFFFFF00007777770000770FFFFFFFFFF0777777
              0000770FFFFFFF00007777770000770FFFFFFF07077777770000770FFFFFFF00
              7777777700007700000000077777777700007777777777777777777700007777
              77777777777777770000}
            OnClick = btnEmitirReceituarioClick
          end
          object GroupBox3: TGroupBox
            Left = 5
            Top = 2
            Width = 508
            Height = 71
            Caption = 'Dados do Paciente'
            TabOrder = 0
            object GroupBox6: TGroupBox
              Left = 7
              Top = 14
              Width = 294
              Height = 47
              Caption = 'Unidade atual'
              TabOrder = 0
              object edUnidade: TEdit
                Left = 8
                Top = 16
                Width = 278
                Height = 21
                Color = clInfoBk
                Enabled = False
                ReadOnly = True
                TabOrder = 0
              end
            end
            object GroupBox7: TGroupBox
              Left = 304
              Top = 14
              Width = 100
              Height = 47
              Caption = 'Nascimento'
              TabOrder = 1
              object edNascimento: TMaskEdit
                Left = 9
                Top = 15
                Width = 84
                Height = 21
                Color = clInfoBk
                Enabled = False
                EditMask = '99/99/9999'
                MaxLength = 10
                TabOrder = 0
                Text = '  /  /    '
              end
            end
          end
          object GroupBox8: TGroupBox
            Left = 5
            Top = 73
            Width = 543
            Height = 339
            TabOrder = 1
            object PageControl3: TPageControl
              Left = 8
              Top = 43
              Width = 529
              Height = 287
              ActivePage = TabSheet3
              TabOrder = 0
              object TabSheet4: TTabSheet
                Caption = 'Dados Consulta'
                object Label4: TLabel
                  Left = 68
                  Top = 28
                  Width = 82
                  Height = 13
                  Caption = 'Data da Consulta'
                end
                object Label2: TLabel
                  Left = 68
                  Top = 52
                  Width = 82
                  Height = 13
                  Caption = 'Hora da Consulta'
                end
                object Label3: TLabel
                  Left = 20
                  Top = 76
                  Width = 140
                  Height = 13
                  Caption = 'Anota��es gerais da consulta'
                end
                object txtData: TMaskEdit
                  Left = 156
                  Top = 26
                  Width = 88
                  Height = 21
                  Color = clAqua
                  EditMask = '99/99/9999'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  MaxLength = 10
                  ParentFont = False
                  TabOrder = 0
                  Text = '  /  /    '
                end
                object txtHora: TMaskEdit
                  Left = 156
                  Top = 49
                  Width = 88
                  Height = 21
                  Color = clAqua
                  EditMask = '99:99'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  MaxLength = 5
                  ParentFont = False
                  TabOrder = 1
                  Text = '  :  '
                end
                object meAnotacoes: TMemo
                  Left = 18
                  Top = 92
                  Width = 487
                  Height = 149
                  ScrollBars = ssVertical
                  TabOrder = 2
                end
              end
              object TabSheet7: TTabSheet
                Caption = 'Procedimentos/Tratamentos'
                ImageIndex = 1
                OnShow = TabSheet7Show
                object btnExcluirTratamento: TSpeedButton
                  Left = 369
                  Top = 187
                  Width = 65
                  Height = 30
                  Hint = 'Excluir Tratamento'
                  Flat = True
                  Glyph.Data = {
                    5E050000424D5E05000000000000360000002800000013000000160000000100
                    18000000000028050000C30E0000C30E00000000000000000000BEBEBEBEBEBE
                    BEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBE8D8D8D5858582727
                    274F4F4F878787BEBEBEBEBEBEBEBEBEBEBEBE000000BEBEBEBEBEBEBEBEBEBE
                    BEBEBEBEBEBEBEBEBEBEBEBEBEBEA8A8A84848484E4E4E7878788787876B6B6B
                    4F4F4F4848489C9C9CBEBEBEBEBEBE000000BEBEBEBEBEBEBEBEBEBEBEBEBEBE
                    BEBEBEBE9494945A5A5A3C3C3C878787B1B1B1BCBCBC8C8C8C7F7F7F7E7E7E77
                    7777313131BEBEBEBEBEBE000000BEBEBEBEBEBEBEBEBEBEBEBEA3A3A3545454
                    494949787878A7A7A7BFBFBFBFBFBFBFBFBF8C8C8C7F7F7F7F7F7F7F7F7F2C2C
                    2CBEBEBEBEBEBE000000BEBEBEBEBEBEBABABA6363634343437B7B7BADADAD8A
                    978AB2B6B2BFBFBFBFBFBFBFBFBF8C8C8C7F7F7F7F7F7F7F7F7F3A3A3AA7A7A7
                    BEBEBE000000BEBEBEBEBEBEACACAC939393A7A7A7BFBFBF8D9B8D1D7F1D519A
                    515F9F5FADB9ADBFBFBF8C8C8C7F7F7F7F7F7F7F7F7F7A7A7A454545BEBEBE00
                    0000BEBEBEBEBEBEACACACA6A6A68BAC8B83A883348A34007F00148514138313
                    359035BFBFBF8C8C8C7F7F7F7F7F7F7F7F7F7A7A7A454545BEBEBE000000BEBE
                    BEBEBEBEA1A1A17D9F7D2D882DAFAFAFA7B7A7268B26A3B5A3ADADAD368C36BF
                    BFBF8C8C8C7F7F7F7F7F7F7F7F7F7C7C7C4B4B4BA3A3A3000000BEBEBEBEBEBE
                    9393934A974A2F8F2FB8BCB8BDBEBDA5B6A5B8BCB8BFBFBF459545BFBFBF8C8C
                    8C7F7F7F7F7F7F7F7F7F7F7F7F525252818181000000BEBEBEBEBEBE9494944B
                    984B1D891D328F32AAB8AABFBFBF81AA8177A77781AA81BFBFBF8C8C8C7F7F7F
                    7F7F7F7F7F7F7F7F7F525252818181000000BEBEBEA2A2A2AFAFAF9AB29A0881
                    08007F00AAB8AABFBFBF73A573007F001B881BBFBFBF8C8C8C7F7F7F7F7F7F7F
                    7F7F7F7F7F707070454545000000BEBEBE969696BCBCBC6FA46F3B933B3B933B
                    7BA87B83AB831285121D891D258B25BFBFBF8C8C8C7F7F7F7F7F7F7F7F7F7F7F
                    7F7F7F7F282828000000BEBEBE969696BCBCBCBFBFBFBFBFBFBFBFBF76A07624
                    7F241585159BB39BAECFAEE7E7E7D3D3D3ACACAC7F7F7F7F7F7F7F7F7F7F7F7F
                    282828000000ABABABA6A6A6BEBEBEBFBFBFBFBFBFBFBFBFBEBEBEBFC3BFC7CD
                    C7DBDBDBA0A0A0A9A9A9D1D1D1D2D2D2E7E7E7CACACA9F9F9F93939379797900
                    0000AAAAAAA8A8A8BFBFBFBFBFBFBFBFBFBFBFBFDFDFDFCBCBCBACACAC919191
                    B0B0B0BABABAFBFBFBE3E3E3E0E0E0C9C9C9DBDBDBD3D3D3C7C7C7000000A5A5
                    A5ACACACBFBFBFC6C6C6D0D0D0DFDFDFACACACCDCDCDDADADA9F9F9FACACACE9
                    E9E9FFFFFFFFFFFFFEFEFEE5E5E5BABABACDCDCDE7E7E7000000939393BEBEBE
                    DDDDDDCBCBCBBFBFBF9A9A9AF1F1F1FFFFFFC2C2C2BFBFBFE6E6E69C9C9CF1F1
                    F1FFFFFFFFFFFFE6E6E6939393A2A2A29F9F9F000000A4A4A4E8E8E8999999C6
                    C6C6EDEDEDB5B5B5EDEDEDC3C3C3C0C0C0F9F9F9ECECEC9A9A9AA9A9A9FFFFFF
                    EDEDED979797A3A3A3B6B6B6BEBEBE000000A0A0A0C8C8C8808080AAAAAAFAFA
                    FAB5B5B5D5D5D5AFAFAFF9F9F9FFFFFFCECECED8D8D8C7C7C7C7C7C7C0C0C0CD
                    CDCDACACACBEBEBEBEBEBE000000A6A6A69E9E9EBCBCBCD7D7D7A7A7A7949494
                    A7A7A7E8E8E8FDFDFDFFFFFFA6A6A6F7F7F7F7F7F7DDDDDDEDEDEDB3B3B3ADAD
                    ADBEBEBEBEBEBE000000BEBEBEBEBEBE989898979797B4B4B4CFCFCFA4A4A4AF
                    AFAFC9C9C9E7E7E7999999979797ACACACC1C1C19C9C9CA6A6A6BEBEBEBEBEBE
                    BEBEBE000000BEBEBEBEBEBEBEBEBEBEBEBEABABABAAAAAA9C9C9CA7A7A7C6CF
                    CFB9BFBF8F8F8FBEBEBEAEAEAEAAAAAAB9B9B9BEBEBEBEBEBEBEBEBEBEBEBE00
                    0000}
                  ParentShowHint = False
                  ShowHint = True
                  Visible = False
                  OnClick = btnExcluirTratamentoClick
                end
                object Label16: TLabel
                  Left = 18
                  Top = 192
                  Width = 53
                  Height = 13
                  Caption = 'Tipo Dente'
                  Visible = False
                end
                object Label17: TLabel
                  Left = 40
                  Top = 210
                  Width = 34
                  Height = 13
                  Caption = 'Arcada'
                  Visible = False
                end
                object Label18: TLabel
                  Left = 15
                  Top = 230
                  Width = 57
                  Height = 13
                  Caption = 'Hemiarcada'
                  Visible = False
                end
                object Label19: TLabel
                  Left = 6
                  Top = 175
                  Width = 34
                  Height = 13
                  Caption = 'Dentes'
                end
                object btnVerTodosDentes: TSpeedButton
                  Left = 441
                  Top = 187
                  Width = 65
                  Height = 30
                  Hint = 'Ver todos os dentes'
                  Caption = 'Ver Todos'
                  Flat = True
                  ParentShowHint = False
                  ShowHint = True
                  Visible = False
                  OnClick = btnVerTodosDentesClick
                end
                object GroupBox9: TGroupBox
                  Left = 3
                  Top = 6
                  Width = 355
                  Height = 45
                  Caption = 'Procedimentos/Tratamentos'
                  TabOrder = 0
                  object btnIncluirTratamento: TSpeedButton
                    Left = 319
                    Top = 12
                    Width = 27
                    Height = 26
                    Flat = True
                    Glyph.Data = {
                      42010000424D4201000000000000760000002800000011000000110000000100
                      040000000000CC00000000000000000000001000000010000000000000000000
                      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                      77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
                      CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
                      000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
                      07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
                      00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
                      0000777777770000007000000000777777777777777770000000777777777777
                      777770000000}
                    OnClick = btnIncluirTratamentoClick
                  end
                  object cbTratamentos: TComboBox
                    Left = 7
                    Top = 15
                    Width = 308
                    Height = 22
                    Style = csOwnerDrawFixed
                    Color = clAqua
                    ItemHeight = 16
                    TabOrder = 0
                  end
                end
                object dbgTratamentosConsultas: TDBGrid
                  Left = 4
                  Top = 57
                  Width = 511
                  Height = 112
                  DataSource = dsTratamentosConsultas
                  Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'cod_id_consulta'
                      Title.Alignment = taCenter
                      Title.Caption = 'N.� Consulta'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'dsc_tratamento'
                      Title.Caption = 'Tratamento'
                      Width = 345
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'num_dente'
                      Title.Alignment = taCenter
                      Title.Caption = 'Dente N.�'
                      Visible = True
                    end>
                end
                object cbTipoDente: TComboBox
                  Left = 77
                  Top = 190
                  Width = 145
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 2
                  Visible = False
                  OnClick = cbTipoDenteClick
                  Items.Strings = (
                    'DEC�DUO'
                    'PERMANENTE')
                end
                object cbTipoArcada: TComboBox
                  Left = 77
                  Top = 209
                  Width = 145
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 3
                  Visible = False
                  OnClick = cbTipoArcadaClick
                  Items.Strings = (
                    'SUPERIOR'
                    'INFERIOR')
                end
                object cbTipoHemiarcada: TComboBox
                  Left = 77
                  Top = 229
                  Width = 145
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 4
                  Visible = False
                  OnClick = cbTipoHemiarcadaClick
                  Items.Strings = (
                    'ESQUERDA'
                    'DIREITA')
                end
                object lstDentes: TCheckListBox
                  Left = 6
                  Top = 190
                  Width = 355
                  Height = 58
                  ItemHeight = 13
                  TabOrder = 5
                  OnClick = lstDentesClick
                end
                object chkMarcaTodosLista: TCheckBox
                  Left = 369
                  Top = 231
                  Width = 131
                  Height = 15
                  Hint = 'Marcar todos da lista atual'
                  Caption = 'Marcar todos da lista'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 6
                  OnClick = chkMarcaTodosListaClick
                end
              end
              object TabSheet8: TTabSheet
                Caption = 'Encaminhamentos'
                ImageIndex = 2
                object btnExcluirEncaminhamento: TSpeedButton
                  Left = 175
                  Top = 226
                  Width = 180
                  Height = 27
                  Caption = 'Excluir Encaminhamento'
                  Flat = True
                  Glyph.Data = {
                    5E050000424D5E05000000000000360000002800000013000000160000000100
                    18000000000028050000C30E0000C30E00000000000000000000BEBEBEBEBEBE
                    BEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBE8D8D8D5858582727
                    274F4F4F878787BEBEBEBEBEBEBEBEBEBEBEBE000000BEBEBEBEBEBEBEBEBEBE
                    BEBEBEBEBEBEBEBEBEBEBEBEBEBEA8A8A84848484E4E4E7878788787876B6B6B
                    4F4F4F4848489C9C9CBEBEBEBEBEBE000000BEBEBEBEBEBEBEBEBEBEBEBEBEBE
                    BEBEBEBE9494945A5A5A3C3C3C878787B1B1B1BCBCBC8C8C8C7F7F7F7E7E7E77
                    7777313131BEBEBEBEBEBE000000BEBEBEBEBEBEBEBEBEBEBEBEA3A3A3545454
                    494949787878A7A7A7BFBFBFBFBFBFBFBFBF8C8C8C7F7F7F7F7F7F7F7F7F2C2C
                    2CBEBEBEBEBEBE000000BEBEBEBEBEBEBABABA6363634343437B7B7BADADAD8A
                    978AB2B6B2BFBFBFBFBFBFBFBFBF8C8C8C7F7F7F7F7F7F7F7F7F3A3A3AA7A7A7
                    BEBEBE000000BEBEBEBEBEBEACACAC939393A7A7A7BFBFBF8D9B8D1D7F1D519A
                    515F9F5FADB9ADBFBFBF8C8C8C7F7F7F7F7F7F7F7F7F7A7A7A454545BEBEBE00
                    0000BEBEBEBEBEBEACACACA6A6A68BAC8B83A883348A34007F00148514138313
                    359035BFBFBF8C8C8C7F7F7F7F7F7F7F7F7F7A7A7A454545BEBEBE000000BEBE
                    BEBEBEBEA1A1A17D9F7D2D882DAFAFAFA7B7A7268B26A3B5A3ADADAD368C36BF
                    BFBF8C8C8C7F7F7F7F7F7F7F7F7F7C7C7C4B4B4BA3A3A3000000BEBEBEBEBEBE
                    9393934A974A2F8F2FB8BCB8BDBEBDA5B6A5B8BCB8BFBFBF459545BFBFBF8C8C
                    8C7F7F7F7F7F7F7F7F7F7F7F7F525252818181000000BEBEBEBEBEBE9494944B
                    984B1D891D328F32AAB8AABFBFBF81AA8177A77781AA81BFBFBF8C8C8C7F7F7F
                    7F7F7F7F7F7F7F7F7F525252818181000000BEBEBEA2A2A2AFAFAF9AB29A0881
                    08007F00AAB8AABFBFBF73A573007F001B881BBFBFBF8C8C8C7F7F7F7F7F7F7F
                    7F7F7F7F7F707070454545000000BEBEBE969696BCBCBC6FA46F3B933B3B933B
                    7BA87B83AB831285121D891D258B25BFBFBF8C8C8C7F7F7F7F7F7F7F7F7F7F7F
                    7F7F7F7F282828000000BEBEBE969696BCBCBCBFBFBFBFBFBFBFBFBF76A07624
                    7F241585159BB39BAECFAEE7E7E7D3D3D3ACACAC7F7F7F7F7F7F7F7F7F7F7F7F
                    282828000000ABABABA6A6A6BEBEBEBFBFBFBFBFBFBFBFBFBEBEBEBFC3BFC7CD
                    C7DBDBDBA0A0A0A9A9A9D1D1D1D2D2D2E7E7E7CACACA9F9F9F93939379797900
                    0000AAAAAAA8A8A8BFBFBFBFBFBFBFBFBFBFBFBFDFDFDFCBCBCBACACAC919191
                    B0B0B0BABABAFBFBFBE3E3E3E0E0E0C9C9C9DBDBDBD3D3D3C7C7C7000000A5A5
                    A5ACACACBFBFBFC6C6C6D0D0D0DFDFDFACACACCDCDCDDADADA9F9F9FACACACE9
                    E9E9FFFFFFFFFFFFFEFEFEE5E5E5BABABACDCDCDE7E7E7000000939393BEBEBE
                    DDDDDDCBCBCBBFBFBF9A9A9AF1F1F1FFFFFFC2C2C2BFBFBFE6E6E69C9C9CF1F1
                    F1FFFFFFFFFFFFE6E6E6939393A2A2A29F9F9F000000A4A4A4E8E8E8999999C6
                    C6C6EDEDEDB5B5B5EDEDEDC3C3C3C0C0C0F9F9F9ECECEC9A9A9AA9A9A9FFFFFF
                    EDEDED979797A3A3A3B6B6B6BEBEBE000000A0A0A0C8C8C8808080AAAAAAFAFA
                    FAB5B5B5D5D5D5AFAFAFF9F9F9FFFFFFCECECED8D8D8C7C7C7C7C7C7C0C0C0CD
                    CDCDACACACBEBEBEBEBEBE000000A6A6A69E9E9EBCBCBCD7D7D7A7A7A7949494
                    A7A7A7E8E8E8FDFDFDFFFFFFA6A6A6F7F7F7F7F7F7DDDDDDEDEDEDB3B3B3ADAD
                    ADBEBEBEBEBEBE000000BEBEBEBEBEBE989898979797B4B4B4CFCFCFA4A4A4AF
                    AFAFC9C9C9E7E7E7999999979797ACACACC1C1C19C9C9CA6A6A6BEBEBEBEBEBE
                    BEBEBE000000BEBEBEBEBEBEBEBEBEBEBEBEABABABAAAAAA9C9C9CA7A7A7C6CF
                    CFB9BFBF8F8F8FBEBEBEAEAEAEAAAAAAB9B9B9BEBEBEBEBEBEBEBEBEBEBEBE00
                    0000}
                  Visible = False
                  OnClick = btnExcluirEncaminhamentoClick
                end
                object GroupBox10: TGroupBox
                  Left = 3
                  Top = 6
                  Width = 355
                  Height = 45
                  Caption = 'Encaminhamentos'
                  TabOrder = 0
                  object btnIncluirEncaminhamento: TSpeedButton
                    Left = 319
                    Top = 12
                    Width = 27
                    Height = 26
                    Flat = True
                    Glyph.Data = {
                      42010000424D4201000000000000760000002800000011000000110000000100
                      040000000000CC00000000000000000000001000000010000000000000000000
                      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                      77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
                      CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
                      000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
                      07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
                      00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
                      0000777777770000007000000000777777777777777770000000777777777777
                      777770000000}
                    OnClick = btnIncluirEncaminhamentoClick
                  end
                  object cbEncaminhamentos: TComboBox
                    Left = 7
                    Top = 15
                    Width = 308
                    Height = 22
                    Style = csOwnerDrawFixed
                    Color = clAqua
                    ItemHeight = 16
                    TabOrder = 0
                  end
                end
                object dbgEncaminhamentosConsultas: TDBGrid
                  Left = 4
                  Top = 57
                  Width = 354
                  Height = 163
                  DataSource = dsEncaminhamentos
                  Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'cod_id_consulta'
                      Title.Alignment = taCenter
                      Title.Caption = 'N.� Consulta'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'dsc_encaminhamento'
                      Title.Caption = 'Encaminhamento'
                      Width = 268
                      Visible = True
                    end>
                end
              end
              object TabSheet3: TTabSheet
                Caption = 'Odontograma'
                ImageIndex = 3
                object Image2: TImage
                  Left = 128
                  Top = 32
                  Width = 216
                  Height = 172
                  AutoSize = True
                  Picture.Data = {
                    0A544A504547496D6167658A1B0000FFD8FFE000104A46494600010101004800
                    480000FFDB0043000B07070808080B09090B100B090B10130E0B0B0E13151111
                    11111115171213131313121715181A1B1A18152020222220202C2C2C2C2C2E2E
                    2E2E2E2E2E2E2E2EFFDB0043010C0B0B0F0F0F1711111718141314181E1C1C1C
                    1C1E241E1E1F1E1E242822202020202228252724242427252A2A28282A2A2E2E
                    2E2E2E2E2E2E2E2E2E2E2E2E2EFFC000110800AC00D803011100021101031101
                    FFC4001C0001000203010101000000000000000000000506030407020108FFC4
                    0043100001040102030407030907040300000001000203040511120621311322
                    4151071423326171914281A1152433526272B1C1F01617437382A2E1345354D1
                    92B2D2FFC4001B01010002030101000000000000000000000003040102050607
                    FFC4003811000201020404040307030403000000000001020311041221310541
                    5161137181912232A1142352B1C1D1F0064262153372E182B2F1FFDA000C0301
                    0002110311003F00EB8802008020080200802008020080200802008020080200
                    8020080200802008020080200802008020080200802008020080200802008020
                    0802008020080200802008020080200802008020080200802008020080200802
                    0080200802008020080200802008020080200802008020080200802008020080
                    2008020080200802008020080200802008020080200802008020080200802008
                    02008020080200802008020080200802008020080200802008020080200808DB
                    3C4F80AB376163275229BFEDBE68DAEE5E60B901B95AE55B6DDF5A78E767EB46
                    E0F1F569280CC802008020080200802008020080200802008020080200802008
                    02008086CBE7678A738EC5422DE4F4064DC76C159AEF75F3B873E7CF6B40D4E9
                    E1D5415F110A2AF2F632A373422E138EC343F396E6CAD83FA4123DD1D7D7AF72
                    B465B181F304FC571EB711AB2D9E45FCE64AA28DF870B87823ECA2A35D91FEAB
                    62669F7F2549E2BACFEA6C69DBE11C4CD20B14DA71B79BC99728910BFCF47068
                    D8F1F07B4AB1431D521F2CB32E8F5357143099CC8D3BCDC2710B9AEB52EBF93B
                    20C6ED8EDB4732D701C992B4751D0F82EDE1B150ACB4F996E88E51B165564D42
                    008020080200802031FAC41DA765DA37B53CC47B86EFA7540644010040100401
                    00401004010040101AB95C9438BA162F4FFA3AEC2F23C5C7ECB1BF171D00F8A0
                    22386EA5BAF43D6721A7E53BCE366E69F65CFF007621E3A46CD183E4BCCE3F14
                    A5372E5B2278A241D22F3F5F16D92289E371557C691B58FBBCADE38992316223
                    8AF16ECBE226863E56E2D2C5293C59622EF4641F0E7CBE455AC1710951C4C2A7
                    2BDA5E4F73128DD1E71BE943856C63ABD9B5918AB589230E9ABBF5DEC7FDA1A0
                    D7C7A7985F432A1E24F4B5C1C1E2382DC96E43F62082571FC5A1625251576ECB
                    B834B29E98B178F89B33B1592ECDE76B2596010C6E3D740E7BBAFDCA3862294D
                    E584E326B926999B323E8FA63C8E6A674183E1E7DA958373B7CED6E83A6A7B9A
                    7E2B5C462A8E1E39AAC94530A2DEC69F12FA4EE38C2431BEDE328D2331D2363A
                    4EDA43A0E676B25E8A2C2F10A1896D516E5977795A5EECCB8B5B91984F485C73
                    C4761F0B723063626B7776DEAE1C09D400C682D909298DC6AC346F95CDB7B26B
                    DF5118DCFBC44CF484C1136965F21927CBAF6DEAF03EB323F01DEEEEBAFC954C
                    371AA53CDE2E4A296D79A6DFA23674DF2D4C183E10E26C876AFE218F2936BA76
                    2C16E360F1DC65333DCEF2D346A831BC6E9C6CA855A7DEEA52F2B5BF7331A7D5
                    1BB87A182C3F14E1EB51AD255CD7AEBA2B2C74E65DB088B53D341DF0F1E1E07E
                    6A6E195F155A79E7352A392E9A8E5BB6FF004B189A4BCCEC8BB0461004010040
                    1004010040100401015AE3A95BD9E1EA3BDDB794AAC703D088C99F4FAC614389
                    76A33FF8B331DC9594AF078EABA96A28C4B986E1004044F133EFBE8494B1AD2E
                    BD681635C081D9C6794926AEE5A81C9BF156F02A9AA8AA55F921AF9BE4BF7EC6
                    B2EC55335C037B338EAD8FAB4A9E22083697CBB8493CA58DD8CDE6389BE1D7BC
                    BAF86E2D4E8559559CEA5794B656B455DDDDAEFF0043470BAB6C65E15F467778
                    76C3ED41938C58919D9977ABF685ADD753B4B9E34D74F25A63F8DD3C54542549
                    E54EFF003DBDEC8469D897CA70256CE765F96AF59BA21D7B38FD9C2C05DA6A76
                    C4C07C3C4AA7438ACB0F7FB3D3853CDBBD64FEACD9C2FB95DB183C4623277E86
                    369568DB4E9FADCA6CCD3BDF63B9216C6D8C480722CD49F2F0E6BA50C557AF4A
                    9D4AB39BF12A64596314A3AAD6F6EFA1A6549D91B9C236B1D91C9D3ECA956AE2
                    4C699ECD76C1182DB4D99AC71DC7590723A8D4F304150F11855A546779CE56AF
                    9632CCFE4CADF979998EBEC5E8003901A05C0253EA0080E5368B60F4C95DCEE8
                    F9E003E6FAED68FC4AF7FC05DF014FFF002FFD9956AFCC76B5D6230802008020
                    080200802008020080ACFA476B070E496BB411CF46682DD4275EF4D1480B636E
                    9CF73FDD1F35A54578B4F9A32B7242BDA8AED586DC2758A76091BF270D745E13
                    8950CB265989F7A2E4121A76F358DA5FF5166363BC19AEE79F931BAB8FDC14D4
                    F0D56A7CB16FBF2F7D8C5D1037B313484E4EF49362F07576BA180FB3B3765075
                    1AB46AF6C7D0067273BC741C974296192FB9A6A35B113DDEF0A6BF26FBECB91A
                    37CF645722CFE77FB471641F2F61EBB0485B464E71B216380858475DC4EA491F
                    25DFA7C1F0F3C3780F78BBE75BE6E7E9D847337E85CB19C594AD3DB5EDFE6570
                    F48A470D8FFF002A4E41DF2E47E0BCF63B83E230DADBC4A7F897EAB91B5F93D0
                    959EE56AD13E69E6647146373DEE7001AD3D092B9D1A7393518A6DBD9193565E
                    21C3C3626AD2DD8639ABB3B59D8E781B187400B89E43A852C7075E518C9424E3
                    2765A6EC6645233995C31CAD9CC8BB31AF6E9BAAF630D42E716B7746E93B4940
                    D23D641EE91B8E8BBB85A15FC18D0C91CD0A8A77954B2EB6B2FEED39EC44DABD
                    CCF8DB984C4DA665DAD9DD2D68E4C2DB908646D79A3009DD3399DE76A4461A39
                    FCD475A9E22BC1D0F82D36ABC56AEDE24B2E5BEDCEE6745AFA1211F1AE5AD558
                    DB0E2C419096CB6AEB349BA084CAC6CB13DE5A039FAB5DD1A3A8EA392AEF8650
                    84DB955CD4E30CFA2F8A56769257D16BD4CE77D0D59789F3B6269F1AD9E2AD6D
                    96ADB04F1C7A81154ACC9B4DB26BAEE7C835F1D14B1C0E1A318D5CB29C1D3A6E
                    CDF39CDC79744BDCC666687F6DF256DD24761EE861B51E364AAE8FD9EC9A4EC9
                    F345BC73EFB4B88D7C068AC7FA5D2859C529383ACA57D6F159945DBB697319DF
                    E469F1760ADDFE36C85CAAE746EC6D16DE12375D7B5899AC4D1A7892DFC159E1
                    78E587C0D14F79D6C9E8DEACD671BC9F91D8E85B8EF53AF6E23AC762364AC23C
                    9ED0E1FC57A62133A0080200802008020080200808CE20E23C6F0F54F5ABF268
                    1CED90C4CE724AF3D1AC6FF43CD0142CC71A457F2D83B9938A1AF87A93C92BDA
                    D9C4F2B662C2D8649A36B5BB4309EADDC3E8A9E2652A94A51A69E6FCFAD9EC6E
                    958BC33274AD303EBD88A5611A8731ED70D3CF50578BC6D497CB964BD09E2455
                    DC1F0F4B23EC5F8D9339CEDEEF5891CF683A69C9AF71681F00156A58AC524A34
                    DB8E96F8559FBA36B2226F715F0DE036D4C3D7867BF20D23AD54358DD3CE491A
                    346B7EA55FA3C2F198A79ABCA5186F9A6EEFD11AB925B1AB4F1F632960E73885
                    ED115604C4CE6D8626F53B03BC7CDC7995D9A54A9E1A1E1505ACB77CDBFE7232
                    A3CE4739CB66E5CBF10CB9083511B9DB2B37A6D899DD672FC7EF5D7A70F0E9A8
                    F3E7E64B4239A598BA4753D768761798246C8DD1EC23FAD0FC54A8CCD27A7221
                    AFD7CA63A2C806B8DAA3EA51D56B74F68211292C772F7BB2710D3FB2E1E4B9D5
                    E8515529FF006B75653BF2CD6D576CCB5F3455945C7BAB137332275FCD411579
                    246D886F4D24734044F5DEF859ED6391BB9AF8E62D6ED1A6E042E2C5BF0E849C
                    92CB2A514E32F864949E8D72946EEFC8CF366EDCC5656EFF0066EF53AA2734A8
                    365960B25D1B247B442591B9DE1203AB9BB869A8E6A1A788A34FED34E72CBE25
                    5B271D5A5F15DF97276EA65A7A791A72F0FDBB96AF4373B1A146E7ACDE619278
                    CBA3B36EB883B37B01FB07576A3929A38B84214E54F354A90C94F48BB3853966
                    BA7DF63163DFE5CC754AB11CA672A9B91DCAF62486091F6238E2AC047D9C4180
                    F79DA6A7E256BF65AB39BF06854C8E9CE29C928B729EB795F92175CD99E7C28C
                    9FAE3EBD7C8EFB9624B51CEC8E2AA5AC9E1104B16B65FA90F68EBB75F15A4713
                    E16452951B420A0D3729DDC65993F85727DCCDAFD4AC6778CA8E3AEDEC78C46F
                    3F9B432C6FB3BA261A3FA211F66C69D47DAEF2EA61786D4AB4E9D4F1ADF3C935
                    0D5F89F35EEFDB42394EDA58B86138928C151D97CF5CAECBB968E3269400BB63
                    181C18DD8D323C9707F3FA2E362B055253F030D09B8506FE396976F7D745C896
                    2F9BE6587D19DE65BE19898C276D49A7AAC0E696B832390F640B4F304465ABDB
                    516DD38B96F955FCCACF72D0A430100401004010040100406A6532B4F1351D6E
                    E3F644D21A3405CE739C746B18D6EA5CE71E80202A388A5C4333E5C94F5EB55B
                    D65F21366DEF9E66C5B8F631B21696888359A6A03FAF33CD51AF5237D5B6BB13
                    45336ACDCA71C7D9F115BA0F7B1C1F1EA04406DD083B259243A83E45509394E5
                    F76A56DBF8C99592D4E756A1C053C8BE08D83290587C92C33D26B6C4A03CEFD9
                    2B0B474DDC8827557D2AB287E1B25A3D17A117C29F5B9AB5F21C2D2DA7566558
                    A9C8CFF1F28D7ED0E1D4763087731FB4E08E9D54AEDDFB47F7662F1F2F32CB8E
                    CBF02606096C1BD1E42EBB4ED1EC6733A746451801AC6FC3EA5569C31151DAD9
                    57F376491C8BBB2A3C61C6F7389E41046D35B191F36D7D79BDC3ED49A72F9056
                    6861E3455F7975248D39547DBA7EE6AF0A5096C647635DB591692C926809DBAF
                    B9A1F33E3E4AC6FA9BBCD07E1ADB7BFE85F9EF6C6C2E772685930552F717494F
                    2B5ED523B6481DB4123782D906C76AD046BD75D3E0ABE270F0C4D295292BA7E9
                    AAD4D6B45C55FA17C7B73B928E56D193273CBB0F632BA28B1D5B7F8126563663
                    CFC97030FC1EADD3950A508DF5BB7395BDF2903A8BA9035BD14F1C5EB11CB99C
                    AB2480383A481F6269378079B7DDDA35E8BD12C2528C5AA7085376D1A8AD3B91
                    6665E22E12BCD85D145352A01CD2CFCD2A68F6EBCBBAF749A7FB573A3C069E65
                    2AB56AD5B3BEAF436F17B2203FB9BE11C3D59B217A7B761B558E9A473A46B068
                    C05C48D8D69F0F35DBB72232855EE5ECA76F2DDB366463DE5B1C72CCF7063074
                    1A6BA1235FC171AB4695269528422BB4516A94732F88C5C22D8E3C8EC731A5CF
                    84F320121F13F4D75F88FAACF116DD2BA6F497D1A36C37CDE9F9170000E834F1
                    5C52F161F4672EDB59BABD3DAC161A3CC49176648FBE25E9386CAF878F6BAFA9
                    CCC52B54F32F0AF1004010040100401004010115C43811998EB1658755B54A61
                    62AD86B5AFDAF00B7BCC7F27021C561AB839C65721936676D6333376699F0BBB
                    95E377AB45340EE6C7B591105DAFDA05C552AD4F2EC979EE4D095CB5616D619D
                    13055822848FB01A011F7AE6CFC44EEDB658562745B6F82DBED0FA1AE43E4B1D
                    5B236CF132507968F68772FF0052DE3885E461C0AA657D14F0DE437BEB09284C
                    EE63B23ECC1FF2DDA8FA68AD4312FCCD6CD146E25F47B94C0832F666DD300B8D
                    AACC27681D7B58F996FCC6A14D1929F6659862B2AB497B1BBC1F5190E299383B
                    A4B5ED1EEF2F00DFF4E8A6238BBACDCDEA68715E5CEEF5589DC86BBF45A4996A
                    9439B23382710739C518FA9A7B3ED44F37F970FB477D74D1490454C5CF9753F4
                    7A90A410040543D2C5D15B85658B5D0DA9628BE25A1DDABF4FF4B0AD2A3B4599
                    8ABB3956359B29C3E6E1BCEBE6FEF7F35C7ACEF51FB7B17A9FCA61C37B3E226B
                    3F6E7F8727B03C7F05BE275C2DFB47E8EC474FFDEF565C9714BE48F064DD8716
                    B184E82DD29583E2E8A48DE3FDAE72EDF0797C338F75FCFA14718B54CE8EBAE5
                    30802008020080A4F1DFA481C3F63F2663228AD65768925133836289A7A07779
                    A4B9DE035F8A02B58EE3DE2BBCCF58872555CE07492B3EB7758EF16F27EFFBF5
                    405CF8578E62CBCC31B908C53CBED2E6C60EB1586B7DE7C0E3F8B4F31F1404A7
                    13E7E2E1FC54B79EDED25E51D683C659DFCA38C7CCF5F82CC62E4ECB760E41C4
                    317E62EB178B67C9CAE32CB63A1ED1FD761EA00E83E002F4F85C1432AA4E319A
                    E775EEFAFF003A11B915BA7C4B91A6E00BFB768F0935DC3FD639FD755CDC6701
                    A2E5F752C9D9EABDFF00E8DE15E4B7D49867A44B31B4691C9BBFCDE5FF00D572
                    E5FD3D5BFC1FEA4AB128B2F0DF1B64B28C2D6D9AB0E874EFB2495E3F786E8972
                    3118354A56946575E84D0A99BA1678F1B7ECC67B4CED83BBFF001990C2D00F83
                    75648EFF0072ABE2A8ED04BCDB66F96FCCD8ADC358B6C2E86C7AC5ADFEFBEC58
                    9A42EFABF4FB80524316EFAD97923574CE737AF56C05ACBE2A11B2382CBBD523
                    D7DC8A56B64006BE00B974A33CD04FAA24C3C1BBAE8CA65CB0E91EE738EA5C75
                    2B3145AAB2CAAC750F4118A6F6191CBB8B5CE7B9B5621F6D819DF7EBE5BB737E
                    8A78A395567999D51648C203E17068D4A038D7A52CC9CC711331AC3F9B50D217
                    791965D2498FDCC686FD556C4CF2AF257FD8929C6FEA452E4178D28FD9711547
                    7EBEDF9F30E8CA9DEB859F6FFA643B565DFF00F85C1718BC64C6C9D8710E167F
                    2B4623F2961919CFE1AE8BA5C25FDF35FE255C5AF81799D4C381E8BD01CF3EA0
                    0802008020381D8BB059BF7EE4974B65B56A69481A721BC860E6D27930202233
                    72BEB385EA564BDC7464FD353CBB84801BF2406D61335264B482690B2CC677D7
                    B2D3A491483DD7B0F50414059867325C47332EE5C063714CF576460F75D61A3D
                    BD92340013D1BE4175785D0BDEA6EEF646B2654F3D917DCB4FE67634E8D1E0BD
                    9E128A843BBDCAD26434CDE6A8F12A7152525BB368185CC91DC981C4805C401A
                    F747371F900B898BABE1A8DA596EFE9CFD89228CB0CB356904D13CC520E8F074
                    59C561A8D656A96BF27CCCC64D6C5AF07C7D2C1B63B323A3935D03A3EFB1DE1E
                    E7320FC82F2F8AE1D91FC2E338F54CB50AC593FBC782366A6DC2E3F1275FA722
                    A87D8DF4649E29CFB8832EDC9E62EE41A7BB3BC6D3F06B5ACD79FC95D853CB08
                    C7A226A3572A93EAC8B277F78FBBFC54BB1AB79BE27F2AFA9DB7D12F638BE1D8
                    D8F3A4D69EEB120FDEE4D1FF00C5A14A526EEEE5F62B0C90722860CA80D6BC5C
                    2276D3A723CFC901C76FF04C55B20EB06ED89771739DBCB77B9EFF0079DBC0F1
                    F92D254E32DD1952680C252D343DABB5F13349CFE842D7C0A7F857B19CF2EA44
                    F1253AF8EF54BB562117672FB4737993D241BB5275E6D4AD4D4A0E3D74F7D042
                    4D3B9680438023A1E617923B060BCFEC84138F7A0B1048D3E5ED1A0FE0485778
                    6CAD888F7BAFA106255E9B2D9478A9E34123BEF2BD29CC2C3533704FA7790120
                    C998F1A8280F6802020B8938B30D8886C579EFC30E40C123E181CF01DB83096F
                    CB53D35EBE080E4144DF6D48006C3FA36F573B5E83F65018730DB13E3AC45242
                    0F7770746FD742DE7D1DB4A02AD85B1D85E8DDAE835405DA9D1B9785C7456FB0
                    AD258712033738E8D6B4E8EDDA69CBC976B86F88A9DE324BE2FC37FE7B1A48F4
                    DE12A0C1ACD66479F13AB5A3E8D0345D555ABF3A937ECBF248D2C84982E1F60D
                    24EF1F8C8EFF00F4B59D39555F16797AB1B183D5B86203FA16BC9E44B8927CBC
                    4E88B865F5F0EFE7AF7E6339E1D2F0F47CD9523D7C390F2FBD4D1E16BF0417A1
                    8CFDCC5264B13D83E16D48FB3775681A7F21CFEF52BE179A395DACF756319CAF
                    4EEED642D7127B33ECDE7DFD3C353E6178EE2982960B10E1C9ABAF2675F0928D
                    7A6E33D5AF7307AB307BC496F92A39D93FD96296ADB8F436F118F764EF47001E
                    C5BDE974FD41E1F7ADE08AB89A9FDAB44744AF23EB86B633A06F4D1485327317
                    C4734276BDDAB4A02D943330D86FBDCD01B72CD13DBD7EA8081CA632A58D4EA3
                    5F9A02BF6F10D8F52D704056B89F1762D50315767692B5E1C1BA86F2D08775F8
                    1586AE818689CF0A704262820746C6B0BE57BA4777469BB6B341E1FACB9AF854
                    25372727ABBD916962DA4925B23DBF176EC3765BC849247A83B2364718E44387
                    3D1C7A856696068539668C755DD914ABCE4ACDE8497356888C91DA9A3E6D711A
                    202568711DA89C039E48F1405BF19966DA60E68087E3CE299F1A21C5639FB325
                    71A5EE9B91F56AEDE4E9743E24F75BF1F92038E67F2F52364F5218C39F36A659
                    5FDE7B9C7ED39EED5C4A02569C903AB43EDFAB19FE27C072407B9E073E19364E
                    E1AB1DE4F1D3E480E7C0969D47221013F532B76BD4640D95C1ADE7A6BE2799FC
                    57D030180853C3D3525796557F37A95653773CBAF593D64E7F4FE1A2BEA94172
                    35BB31BA695FD5E4ADB2A460F3B89F13CD641F35D7FAFEBC500D52E818643B64
                    6BFC0F74FF0022BC9FF5551528D3AAB7578BFCD7EA5EE1F57254D7667C25F2B9
                    B1C6D2E7B8E8D601A927E0BC946274EBD645DF8770DF932AFB5FFAA9B9CBE3A7
                    9301F829D1CD94B33B92A86A1019E0BB3C27B8E2101B0735708D3794061764AD
                    3BAC879FC50188DA95DD5C80F1BDC7A9407C4010040101F5A09E880B5F0C4330
                    D09D40FE480E7DC499D6372B9FB16641EBDEB72576C6E20384307B389A07978F
                    C501CEE590C8F73DDCC93AA02D781BB1CD49919A8649611B5EE0C69FDDEA4782
                    0366E9AACAB3C86ABE12D8DDDED3673D3CD87C501484049F6C0720BE84F88535
                    A2572AE43C9B1E4A29F127C95BCCCE43DC2CB53FE8627487A775A5DF8854AA71
                    7C9BD48FA6A6DE1F63761C06666FF03B31FB640FC39954E7FD41D333FA1B7846
                    E45C219077E9258D9F2D5DFF00A55A7C7EBBD97BB33E1A36A3E0BFFBB6DC7F75
                    A07F125579719C53D9A5E86D91191DC114DCC20D8975F03DDE5F768A0AFC42BD
                    68E59CBE17CAC14522471982A58DD5D13774A7ACAFE6EFF8FB953376DBDC9043
                    010040100401004034407D0C71F040668E8D893DD61FA2036A2C15C7FD9D3F92
                    037A0E12B2E3DFE48096A5C250B3473C6E3F5FF8404FD6A31C0000101E2E61B1
                    B779D9A704CEEA1D246D79D7EF0501136F83B1726A5B52167EEC6C1A7CB40808
                    1CA7A3865997D62ACC6ADA6B766E00398E6EBC9AF61D35D3E61010593F45BC49
                    7A0301BF57617730D63DBA81F125C80D5A7E839EDE772E993F6616867E2F24FE
                    0809367A26C6B3FC17BBE2E7BCFF0035278F53F1CBDD8B1B117A38A95F9C7523
                    1A7DA2DDC7EAE5A36DEFA8375BC23600D07268F00345807C7F0A5968D7CBE080
                    8EB38AB3013AB49406B3A178EA101E763BC900D101F34401004010040101B742
                    97AD481880B253E118480E793E7A20256BF0CD38FC101BF1E32B47D183FAF9A0
                    33B61637C3A203D6D68F0080FA80200802008068101F3637C8203E766CF20807
                    66CF2403B36790400C6C3E080C12E3E093AB420355F80AAEFB3AA035A4E15AAE
                    F0FE1FF080D697846BFDDFD79202032F8714FA0FB9010FA73407D0D080F85A80
                    7F5FD7D5006824A02C5C3741C656B88E480BAD766D6A032A0080200802008020
                    0802008020080200802008020080202272D8B165BA78F87C50151BBC3F3C6EEE
                    8D420345D8DB2DFB2501E7D427FD5280C91E26CBFEC91F3404BE3B865E5C0C88
                    0B5D1C7B206F7501BC8020080200802008020080200802008020080200802008
                    020040234280C12578DE1DA8E9FF00A406ABAA40EFB0354079F51AFF00A880F6
                    DAF0B7A3020375B1359D101ED00401004010040100407FFFD9}
                end
                object GroupBox2: TGroupBox
                  Left = 6
                  Top = 233
                  Width = 507
                  Height = 244
                  Caption = 'Odontograma'
                  TabOrder = 0
                  Visible = False
                  object Image1: TImage
                    Left = 10
                    Top = 21
                    Width = 328
                    Height = 212
                    Picture.Data = {
                      07544269746D617006330100424D063301000000000036040000280000005201
                      0000E40000000100080000000000D02E01000000000000000000000100000001
                      0000B3B3B3008F8F8F0011111100EBEBEB00676767003F3F3F00D2D2D200FFFF
                      FF00000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070706
                      0400070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070703040207070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707040202040706050700020400020505
                      0103050204050701020200030502050407000502040703020307070707070707
                      0707070707070707070707070707070707030506070707070707070505020505
                      0305070004070101070707060505010407030502040507010201040204050705
                      0603050300040707060502010704000701040704020000050201070707070707
                      0707070707070707070707070707070707070505050506070707070707070700
                      0407070005070102050003050204040705030004000505050400050504010704
                      0504050303050204030707070707070707070707070707070707070707070707
                      0707070002050007070707070707070701050505050004010704010101070405
                      0500070102010605020107010107000107040206010505060707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070701020000020106020704050304050605040102060504030200010500
                      0206040507020106050003010707070707070707070707070707070707070707
                      0707070707060206070303030707070201000000060207000507040407070701
                      0206040407000200050507050400020004020702000602030002070702040004
                      0304040704040702010302010002060707070707070707070707070707070707
                      0707070707070402040403070707030307070701050707020406020001040602
                      0004050702000105030205000004050002010002000402030405060504070707
                      0707070707070707070707070707070707070707070703020101020707070703
                      0307070704020101010606020502030404000500040207050403020101040304
                      0407010503020100020101040707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070606070005010002
                      0704040705010704040305020204000207070707040202050602070704040707
                      0707070707070707070707070707070707070707070707070703020607050204
                      0707070206070707060207010507040407070701050701050707040202050705
                      0107040202050702000302030102070302050405060401070404070206000207
                      0705040707070707070707070707070707070707070707070707070505070707
                      0700020203070701050305050300020405040305020505070206010507060200
                      0706020502000105070602030206070102070707070707070707070707070707
                      0707070707070707070707030706020707070002020707070405070707070704
                      0200070404070105020407050103020504040604040704050302060102040404
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070704020205030002070405030504070404060103
                      0504030200010103040304050702010602000700070707070707070707070707
                      0707070707070707070707070703020607070307070707020202020106020004
                      0203050107070704050704020006040304050702010704030402070201000200
                      0102070702010002060405070504030200030200060200070707070707070707
                      0707070707070707070707070707070705050707070703030707070402020205
                      0606020001020301030405070206010507030102000600060200000203010203
                      0504030505070707070707070707070707070707070707070707070707070707
                      0002010707070703030707070402020502070302020507040400020100010705
                      0403020100020605040705040302060002060402070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0102010306030605030202040401070104070402050007010202000704020200
                      0700050204070302030707070707070707070707070707070707070707070707
                      0405020607070707070707020006060306020502040502060707070202050105
                      0201040202000102020004020201070205020505020107070102020407040502
                      0200040202060102020407070707070707070707070707070707070707070707
                      0707000306020607070707070707070105060304040704020200070402020007
                      0206010506020202050602020503030502020203060502020307070707070707
                      0707070707070707070707070707070707070703030405070707070707070707
                      0405060006070405030201040403050202000002020100020204070402020200
                      0402020304020200070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707000201040206030107040407
                      0707070707070707070707070707070700000103070707070707070707070707
                      0707070707070707070707070707070707070707030502060707070707070702
                      0101010007070707070707070707070105070707070707070707070501070707
                      0707070707070707070707070707030707070707030703020007070703070707
                      0707070707070707070707070707070707070707070705050402060707070707
                      0707070102000102040707030707070707070707020606000707070707070707
                      0707070703060203070703070707070707070707070707070707070707070707
                      0707070707070705050505070707070707070707040200010003070707070306
                      0007070307070705040707030307070707030307060206070703030707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070105050007060407070607070707070707070707070707
                      0707070706010106070707070707070707070707070707070707070707070707
                      0707070707070707070704030707070707070705050505040707070707070707
                      0707070700070707070707070707070606070707070707070707070707070707
                      0707070707070707070707060307070707070707070707070707070707070707
                      0707070707070707070703040500070707070707070707000505050403070707
                      0707070707070707050600040707070707070707070707070703050307070707
                      0707070707070707070707070707070707070707070707070707070604040607
                      0707070707070707010505050506070707070701010707070707070300070707
                      0707070707070707070003070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070307070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070706050500070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070602020407070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070704050200070707070707070707070707030105050707070707030200
                      0002030707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707000003
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070706000006070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707020106050107
                      0707070707070707070707010400070707070707070707070707070402040707
                      0707070707070707070707070706000707070707070707060206010207070707
                      0707070707070707040504050007070707000407070105070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707030000030707070707070302020202060707070707070707070707
                      0701020205070707070707070707070703000707070707070707070707070307
                      0707070707070707070707070700050506070707070707070707070707040205
                      0202000707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070703040500
                      0707070707070707070707070701050707000507070707070707070707070105
                      0105070707070707070707070707010506050007070707070707070707070707
                      0402020107070707070707040007070506070707070707070707070005030701
                      0407070707040007070702030707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070705020202030707
                      0707070002030304050707070707070707070707060201000501070707070707
                      0707070002020207070707070707070706020205000707070707070707070707
                      0702010402070707070707070707070700020607070404070707070707070707
                      0707070707070707070707070707070707070707030707070707070707070707
                      0707070707070707070707070707070705050402030707070707070707070707
                      0705000707030203070707070707070707070506070503070707070707070707
                      0707050607060207070707070707070707070701050706020707070707070705
                      0607070401070707070707070707070206070700040707070705000707070401
                      0707070707070707070707070402020607070707070301040007070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070102000605040707070707010507070302060707
                      0707070707070707040507070105070707070707070700020003050107070707
                      0707070302010300020007070707070707070707070207070501070707070707
                      0707070704010707070005070707070707070707070707000505030307070707
                      0707070707070602020007070707070707070707070701040007070707070707
                      0707070005070704000707070707070707070707030207070707020307070707
                      0707070707070207070500070707070707070707070605070707050007070707
                      0707070707070305060707050007070707070705070707060407070707070707
                      0707000507070700040707070702070707070005070707070707030707070704
                      0500040107070707030505010201070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0204070700050707070707010407070704040707070707070707070302000707
                      0602070707070707070602010707000507070707070707000507070700050707
                      0707070707070707000507070005070707070707070707070500070707030207
                      0707070707070707070707050402000007070707070707070707040501020707
                      0707070707070707070402040201070707070707070707010407070104070707
                      0707070707070707010407070707040007070707070707070700040707000407
                      0707070707070707070005070707000407070707070707070707000507070701
                      0407070707070705070707060207070707070707070701040707070004070707
                      0605070707070702070707070707070707070102030704000707070704040707
                      0004070707070707070707070000070707070707070707070707070707070307
                      0707070707070402050607070707070707070700020607070602060707070701
                      0407070700020707070707070707070102070707070203070707070707040507
                      0707030207070707070707010407070707020607070707070707070701040707
                      0302060707070707070707070200070707070200070707070707070707070005
                      0700050407070707070707070707020603050307070707070707070706020307
                      0605070707070707070707040107070605070707070707070707070705000707
                      0707010107070707070707070704010707030207070707070707070707040107
                      0707030507070707070707070707040107070706020707070707060407070707
                      0507070707070707070704000707070004070707010407070707070207070707
                      0707070707070500070705060707070602030707060207070707070707070707
                      0000070707070707070707070707070703050205030707070706020104050307
                      0707070707070705050707070702060707070702010707070705000707070707
                      0707070404070707070501070707070703020107070707020607070707070704
                      0107070707040107070707070707070704040707070404070707070707070706
                      0203070707070401070707070707070707070401070302020307070707070707
                      0700050707000507070707070707070700040707070203070707070707070702
                      0607070702070707070707070707070602070707070701040707070707070707
                      0705030707070503070707070707070703020307070707020607070707070707
                      0707050307070707020307070707010107070707050607070707070707070207
                      0707070005070707040107070707070207070707070707070707020707070503
                      0707070401070707070207070707070707070707000007070707070707070707
                      0707070704020102000707070701050703020007070707070707030206070707
                      0705010707070302000707070704010707070707070707020007070707040407
                      0707070701020707070707050007070707070702000707070700050707070707
                      0707070705010707070105070707070707070706050707070707060507070707
                      0707070707070203070704040607070707070707070400070707020607070707
                      0707070700040707070203070707070707070302030707070206070707070707
                      0707070104070707070700040707070707070707030507070707050607070707
                      0707070706050707070707010007070707070707070705070707070705000707
                      0707050607070707040007070707070707070207070707030507070702060707
                      0707070206070707070707070700040707070500070707020307070707020707
                      0707070707070707000007070707070707070707070707030201070405070707
                      0701040707010407070707070707000207070707070104070707030206070707
                      0704050707070707070707020307070707010207070707070501070707070704
                      0007070707070002030707070703020707070707070707070200070707060507
                      0707070707070700050707070707030507070707070707070701010707070101
                      0007070707070707070503070707040107070707070707070101070707050007
                      0707070707070302070707070104070707070707070707040107070707070605
                      0707070707070707000407070707010007070707070707070001070707070700
                      0507070707070707070705070707070701040707070702060707070704010707
                      0707070707000407070707070507070605070707070707020607070707070707
                      0704000707070506070706050707070707020707070707070707070700000707
                      0707070707070707070707040507070602070707070501070706020307070707
                      0707010207070707070602070707010207070707070605070707070707070005
                      0707070707000207070707060203070707070701010707070707050107070707
                      0707040007070707070707070506070707070503070707070707070104070707
                      0707070506070707070707070704000707070000040707070707070703020707
                      0707000407070707070707070101070707040007070707070707060507070707
                      0104070707070707070707020607070707070702070707070707070704010707
                      0707010107070707070707070400070707070703020707070707070707070507
                      0707070706050707070305070707070704000707070707070704000707070707
                      0507070401070707070703020707070707070707070506070707050607070101
                      0707070707020707070707070707070700000707070707070707070707070705
                      0007070302030707070500070707020007070707070701050707070707070200
                      0707050507070707070605070707070707070105070707070703020607070705
                      0507070707070704010707070707020307070707070701040707070707070707
                      0203070707070506070707070707070104070707070707010107070707070707
                      0702030707070707050707070707070706050707070703020707070707070707
                      0400070707000107070707070707010407070707060507070707070707070602
                      0707070707070705060707070707070704030707070706050707070707070707
                      0506070707070707020707070707070707070207070707070702030707000407
                      0707070705000707070707070705030707070707050603020307070707070702
                      0707070707070707070203070707040007070506070707070602070707070707
                      0707070700000707070707070707070707070702060707070206070707020007
                      0707050007070707070700020707070707070504070602060707070707000507
                      0707070707070405070707070707050107070602060707070707070101070707
                      0703050707070707070700050707070707070706020707070707010107070707
                      0707070100070707070707030207070707070707060507070707070705070707
                      0707070700050707070707050307070707070707040607070706050707070707
                      0707040107070707070203070707070707070005070707070707070401070707
                      0707070702070707070707020707070707070707020307070707070702060707
                      0707070707030207070707070704060707040007070707070507070707070707
                      0705030707070707000405010707070707070702070707070707070703020707
                      0707040007030207070707070604070707070707070707070000070707070707
                      0707070707070602070707070200070703020607070705010707070707070102
                      0707070707070405070402070707070707060507070707070707010407070707
                      0707040507070504070707070707070401070707070005070707070707070302
                      0307070707070701020307070707060407070707070707050607070707070707
                      0506070707070707060507070707070705030707070707070104070707070704
                      0607070707070707050707070703020707070707070704000707070707050007
                      0707070707070401070707070707070104070707070707030207070707070705
                      0607070707070703050707070707070701060707070707070706050707070707
                      0701010703020307070707030507070707070707070507070707070703020503
                      0707070707070302070707070707070700050707070701010700040707070707
                      0604070707070707070707070000070707070707070707070707060507070707
                      0500070701050707070704040707070707070402070707070707010201020407
                      0707070707000207070707070707010507070707070706020300020607070707
                      0707070501070707070504070707070707070705060707070707070102070707
                      0707070407070707070707050307070707070707010107070707070701040707
                      0707070704010707070707070001070707070704060707070707070702030707
                      0707020307070707070705060707070707010407070707070707050607070707
                      0707070605070707070707000507070707070704000707070707070304070707
                      0707070704000707070707070700040707070707070004070005070707070706
                      0507070707070707070507070707070707050607070707070707030207070707
                      0707070700040707070700040705000707070707060407070707070707070707
                      0000070707070707070707070707010507070707050007070504070707070102
                      0707070707070102070707070707060202050707070707070701050707070707
                      0707040507070707070707050202040707070707070707020007070707050007
                      0707070707070704010707070707070105070707070707050707070707070702
                      0707070707070707000407070707070701010707070707070101070707070707
                      0005070707070704060707070707070705070707070705060707070707070203
                      0707070707000507070707070707020307070707070707070206070707070700
                      0407070707070701040707070707070305070707070707070100070707070707
                      0706040707070707070302060206070707070706050707070707070707050707
                      0707070707050707070707070707070203070707070707070004070707070302
                      0702060707070707010507070707070707070707000007070707070707070707
                      0707010407070707050607060206070707070405070707070707010507070707
                      0707070202060707070707070704050707070707070701020707070707070706
                      0202030707070707070703020707070707050607070707070707070401070707
                      0707070404070707070707040607070707070702070707070707070706050707
                      0707070700040707070707070104070707070707000407070707070401070707
                      0707070305070707070705000707070707030207070707070703020707070707
                      0706050707070707070707070401070707070700040707070707070005070707
                      0707070305070707060105050201070707070707070004070703070707070502
                      0107070707070706040707070707070707050707070707070302070707070707
                      0707070203070707070707070401070707070702040507070707070704040707
                      0707070707070707000007070707070707070707070701040707070702060700
                      0207070707070002070707070707010207070707070707010507070707070707
                      0700050707070707070706050707070707070703020107070707070707070305
                      0707070703020307070707070707070004070707070707050407070707070701
                      0107070707070002070707070707070703020707070707070104070707070707
                      0602070707070707000407070707070101070707070707000407070707070401
                      0707070707060207070707070707020607070707070105070707030300000000
                      0105070707070701040703060603070002070707070707000500040502040600
                      0405070707070707070004070707070707070405070707070707070101070707
                      0707070707020707070707070301070707070707070707050007070707070707
                      0501070707070704020107070707070704010707070707070707070700000707
                      0707070707070707070701010707070705060705010707070707000207070707
                      0707010507070707070707060207070707070707070101070707070707070305
                      0707070707070700020707070707070707070605070707070604070707070607
                      0707070605070707070707020107070707070700050707070707000507070707
                      0707070703020707070707070101070707070707030203070707070700040707
                      0707070101070707070707010506000000070504070707070703020707070707
                      0707050007070707070402010402020202020505020207070707070102020202
                      0202050202030707070707010204040103070707070206070707070707010407
                      0707070707070500070707070707070401070707070707070702030606060707
                      0707070600010404040101020407070707070707050007070707070102060707
                      0707070702000707070707070707070700000707070707070707070707070400
                      0707070705000702060707070707030207070707070701040707070707070707
                      0500070707070707070400070707070707070302070707070707070602070707
                      0707070707070401070707070002010502020202020504010203070707070702
                      0607070707070703050707070707000207030707070707070602070707070707
                      0104070703070707070206070707070700050707070707010407070707070705
                      0202050502020204070707070706020504040501000002040707070707050204
                      0101060303070707070501070707070404000307070306000206070707070701
                      0107070707070707070401070707070707010407030306070303050401040404
                      0401010200070707070707070602020202020204010402020205050404040502
                      0207070707070707050107070707070005070707070707070206070707070707
                      0707070700000707070707070707070707070400070707070401030207070707
                      0707060207070707070704040707070707070707010507070707070707040007
                      0707070707070702030707070707070105070707070707070707040007070707
                      0402040006060006010104050203070707070002020502040504040402070707
                      0707040202020202020202050202070707070707000205020202050204020007
                      0707070701020606060606050407070707070302010607070303010407070707
                      0700020404040404050502010707070706020007070707070707070707010507
                      0707060203070707070707070400070707070705000707070707070707010507
                      0707070707040501020202020202020205050505050505020407070707070707
                      0504060607060002020501060707070707070703020107070707070705000707
                      0707070002070707070707070206070707070707070707070000070707070707
                      0707070707070500070707070105050407070707070706020707070707070501
                      0707070707070707030206070703060000020007070707070707070500070707
                      0001050202050404040001000006020307070707050607070707070707070707
                      0506070707070504060600060006000002000707070702010307070303060000
                      0005010707070707010506070303060601020107070707070402020202020202
                      0407070707070105070707070707060207070707070200070707070707070104
                      0707070704050707070707070707070707060203070701050707070707070707
                      0104070707070702000707070707070707030203070707070705020506030102
                      0607070707070707070707000201070707070700020307070707070002070707
                      0707070707070707010203070707070702020504010106060407070707070703
                      0206070707070707070707070000070707070707070707070707020307070707
                      0302020607070707070703020307070707070202020202020202040202020202
                      0202020202020407070707070707070504040502020204060001000001040404
                      0502020707070707020607070707070707070707000007070703050307070707
                      0707070704010707070602030707070707070707070105070707070704040707
                      0707070707040407070707030201070707070700020707070707050107070707
                      0707030206070707000207070707070707070002070707070200070707070707
                      0707070707030203070705000707070707070707060207070707030206070707
                      0707070707070200070707070705020307070501070707070707070707070707
                      0002000707070705010707070707070702030707070707070707070707050107
                      0707070602040105020202050400000603060005020707070707070707070707
                      0000070707070707070707070707020307070707070405070707070707070705
                      0107070707030204010000000100000101010101010006030706020607070707
                      0707000205040502030707070707070707070707070602070707070602070707
                      0707070707070707010507070704040707070707070707070005070707000507
                      0707070707070707070702000707070704010707070707070704040707070700
                      0203070707070703020307070703020607070707070707020007070704040707
                      0707070707070302070707030206070707070707070707070707020307030203
                      0707070707070707030203070707030203070707070707070707010407070707
                      0302060707030203070707070707070707070707070405030707000203070707
                      0707070702030707070707070707070707000207070707010507070707030004
                      0202020202020202020707070707070707070707000007070707070707070707
                      0703020607070707070104070607070707070705010707070706020707070707
                      0707070707070707070707070707010207070707070702040007050507070707
                      0707070707070707070702030707070005070707070707070707070700050707
                      0302060707070707070707070002070707040107070707070707070707070404
                      0707070705010707070707070701040707070705040707070707070702010707
                      0700020707070707070707050007070702000707070707070707070200070700
                      0507070707070707070707070602020707000207070707070707070707020607
                      0707000207070707070707070707060207070707050107070706020707070707
                      0707070707070707070302000707040407070707070707070200070707070707
                      0707070707070203070707050107070707070707000200030303070002070707
                      0707070707070707000007070707070707070707070302050405050401020503
                      0307030606010502050707070701050707070707070707070707070707070707
                      0707030200070707070105000007010007070707070707030007070707070200
                      0707070104070707070707070707070706020707060507070707070707070707
                      0702030707020600000707070707070707070005070707070200070707070707
                      0700050707070302060707070707070705010707070105070707070707070705
                      0107070302030707070707070707070501070701050707070707070707070707
                      0602020307010407070707070707070707050107070701040707070707070707
                      0707070500070707020607070700050707070707070707070706030707070105
                      0707050107070707070707070401070707070707070707070707020007070702
                      0007070707070707070500070707070102030707070707070707070700000707
                      0707070707070707070002040404040404050202020202020204010002060707
                      0704040707070707070707070707070707070707070707040407070707050607
                      0707070707070707070707030207070707070501070707040407070707070707
                      0703000703020707040107070707070707070707070500070005070401070707
                      0707070707070302070707070203070707070707070602070707060207070707
                      0707070701050707070401070707070707070704010707000507070707070707
                      0707070404070704040707070707070707070707070202070705000707070707
                      0707070707040107070704010707070707070707070707040107070105070707
                      0701040707070707070707070705010707070602070705060707070707070707
                      0101070707070707070707070707040107070602030707070707070707050007
                      0707070602030707070707070707070700000707070707070707070707010507
                      0707070707030203030003070707070704050707070504070707070707070706
                      0607070707070707070707060203070703020707070707070707070707070707
                      0206070707070501070707040007070707070707070605070702000702060707
                      0707070707070707070401070104070707070707070707070707070203070707
                      0207070707070707070302030707040407070707070707070005070707020007
                      0707070707070704010707040407070707070707070707010407070404070707
                      0707070707070707070502070702060707070707070707040300050707070506
                      0707070707070707070707010507070500070707070105070707070707070707
                      0700050707070702060702030707070707070707010107070707070707070707
                      0707010407070005070707070707070707050007070707070206070707070707
                      0707070700000707070707070707070707020007070707070707040107070707
                      0707070703020607070200070707070707070701040707070707070707070707
                      0200070704040707070707070707070707070707010007070707040407070705
                      0607070707070707050105070704010302070707070707070707070707010407
                      0501070707070707070707070003070500070704050707070707070707070206
                      0707050107070707070707070602030706020607070707070707070404070704
                      0007070707070707070707010407070404070707070707070707070707050207
                      0302030707070707070707050006050707060207070707070707070707070306
                      0203030207070707070002070707070707070707070105070707070500070503
                      0707070707070707010107070707070707070707070705010707010507070707
                      0707070703020607070707070500070707070707070707070000070707070707
                      0707070707020607070707070707000507070707070707070704040707020307
                      0707070707070701040707070707070707070707020007070506070707070707
                      0707070707070707010407070707010507070705030700010707070705010207
                      0701040302070707070707070707070707000507020307070707070707070707
                      0401070500070705010707070707070707070203070702060707070707070707
                      0702000700020707070707070707070105070705000707070707070707070700
                      0407070501070707070707070707070703020507060507070707070707070703
                      0707020307000507070707070707070707070602050001050707070707030203
                      0707070707070707070104070707070400070200070707070707070701010707
                      0707070707070707070702060707040507070707070707070105070707070707
                      0504070707070707070707070000070707070707070707070602030707070707
                      0707030203070707070707070704010707020607070707070707070404070707
                      0707070707070707040007070206070707070707070707070707070700040707
                      0707000507070702070700000707070704010201030604070207070707070707
                      0707070707000507020607070707070707070707040107050007030206070707
                      0707070707070500070602070707070707070707070501070005070707070707
                      0707070305070302030707070707070707070706050707020007070707070707
                      0707030706020407000405030707070707070707070705000704010103070707
                      0707070707070702040104010707070707070500070707070707070707050007
                      0707070401070500070707070707070701010707070707070707070707030203
                      0707050007070707070707070405070707070707050507070707070707070707
                      0000070707070707070707070105070707070707070707020607070707070707
                      0704040707020307070707070707070501070707070707070707070704010707
                      0203070706030707070707070707070701040707070700050707070207070707
                      0707070704000202030302070203070707070707070707070701050705060707
                      0707070707070707050107040107000207070707070707070707040107000507
                      0707070707070707070401070104070707070707070707030507060203070707
                      0707070707070706020707020607070707070707070602070701010701020203
                      0707070707070707070705000705000200070707070707070707070300050104
                      0707070707070105070707070707070703020607070707040107040407070707
                      0707070701010707070707070707070707010507070702000707070707070707
                      0200070707070707000203070707070707070707000007070707070707070707
                      0404070707070707070707050607070707070707070405070702060707070707
                      0707070500070707070707070707070701050700020707070503070707070707
                      0707070701010707070700050707070207070707070707070700020307030206
                      0500070707070707070707070704010704010707070707070707070702000701
                      0107040407070707070707070707050007060207070707070707070707010407
                      0404070707070707070707030206030207070707070707070707070302070702
                      0307070707070707070602070705000704020207070707070707070707070206
                      0702070103070707070707070707070706050002070707070707060203070707
                      0707070700020307070707040107010507070707070707070101070707070707
                      0707070707020107070705000707070707070706020007070707070703020007
                      0707070707070707000007070707070707070707040107070707070707070705
                      0007070707070707070402070702030707070707070707050107070707070707
                      0707070700050701040707070206070707070707070707070500070707070605
                      0707030201070707000107070706020307070200010407070707070707070707
                      0702060700050607070707070707070701070701010705000707070707070707
                      0707050107000507070707070707070707000407040407070707070707070707
                      0500070207070707070707070707070302070702030707070707070707070407
                      0702000701020206070707070707070707060207030207070707070707070707
                      0707070704040702000707070707070501070707070707070504070707070704
                      0007030206070707070707070101070707070707070707070402030707070405
                      0707070707070703040707070707070707050407070707070707070700000707
                      0707070707070707040407070707070707070702060707070707070707040107
                      0705010707070707070707040107070707070707070707070404070104070707
                      0500070707070707070707070200070707070005070703020507070703000707
                      0703040707000203060207070707070707070707000203070302020706070707
                      0707070707070705010702060707070707070707070704000706050707070707
                      0707070707060207050007070707070707070707050007020307070707070707
                      0707070302070702070707070707070707070707070500070704050707070707
                      0707070703050107030207070707070707070707070707010203070405070707
                      0707070105070707070707010203070707070302000707050407070707070707
                      0105060307070707070707040200070707070105070707070707070704060707
                      0707070707020507070707070707070700000707070707070707070704040707
                      0707070707070705000707070707070707050107070404070707070707070704
                      0107070707070707070707070401070404070707010503030303000101040405
                      0202050607070102070707050107070707070707070707070602010707050107
                      0707070707070707040507070704050302070707070707070707060203070206
                      0707070707070707070704010706050707070707070707070707050605060707
                      0707070707070707050107020707070707070707070707070203070501070707
                      0707070707070707030203070703050503070707070707060505030707020607
                      0707070707070707070706020107070302000707070707040201060703000402
                      0201000707070602070707010507060101050504020202020501060303000502
                      0007070707070302030707070707070302000707070707070302000707070707
                      0707070700000707070707070707070705040707070707070707070105070707
                      0707070707020007070602030707070707070102050307070707070707070707
                      0500070404070707070202020202020505040404000305050707050107070701
                      0507070707070707070707070504070707000203070707070707070002030707
                      0703050202070707070707070707050407070203070707070707070707070404
                      0707020007070707070707070707050305010707070707070707070701040702
                      0307070707070707070707070206070002010707070707070707070704050707
                      0707030502010307070301020503070707000200070707070707070703040204
                      0707070704020307070700020405020202020504010402020107040507070703
                      0202020505040404020602050405020202020400070707070707070502040505
                      0400070302060707070707030402070707070707070707070000070707070707
                      0707070005020707070707070707070302000707070707070602030707070200
                      0707030005020202020202040101000100000004020607010507070707060006
                      0003070707070707070703050707050107070706020407070707070707070705
                      0507070707070405070707070707070501070707070703020507070707070707
                      0701020307070500070707070707070707070404070701020500070707070707
                      0701020700020707070707070707070704010705010707070707070707070707
                      0206070704020403070707070707030402060707070707070102020202020200
                      0707070707070102020106060600010502020007070707070305020307030200
                      0707000204020307070707010202020007070707040204010104040402010407
                      0600040202010707070707070707070602020104020202040205040404020202
                      0201070707070707070707070000070707070707070707070602000707070707
                      0707070700020607070707070405070707070405030102020501010606060104
                      0505050505050502020307070201070707070707070707070707070707070707
                      0703020007070707010205060707070707070102060707070707030505060707
                      0707040507070707070707060201070707070703040206070707040503070707
                      0707070707000201070707000402020400060601040200070705050401000006
                      0606060002000700020204040000060306060102050707070700050204010606
                      0601020500070707070707070707060000030707070707070707070301050202
                      0202050106070707070707070703050204020507070304050705050006070707
                      0402010707070707030402050401050202020202020202010607070707070707
                      0707070704020401040202020202020201050202040707070707070707070707
                      0000070707070707070707070704050307070707070707070704040707070700
                      0200070707070702040607070707070707070707070707070707070105070707
                      0002010707070707070707070707070707070707070402070707070707030102
                      0201060600020206070707070707070304020400000205060707070707070707
                      0602040707070102020307070707030502050505040101010402010707070707
                      0707060102020205010607070707000405050402020202020107070707000105
                      0202020202050501030707070707070601020202020500070707070707070707
                      0707070707070707070707070707070707070303030707070707070707070707
                      0707070601050202050202020502020202020502020407070707070707070707
                      0707070303030606030707070707070707070707070707070700050202020202
                      0402020205020200070707070707070707070707000007070707070707070707
                      0706020407070707070707070701020307070602050707070707070602020202
                      0401060607030600010502020505050200070707070002050104050404000000
                      0307070707070707010200070707070707070703040202020201030707070707
                      0707070707000502020103070707070707070707070602020502020107070707
                      0707070706000004040405050501070707070707070707070707070707070707
                      0707070707070707030303070707070707070707070707070707070707070707
                      0707070707070703070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707030104040001
                      0000000006010000030707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707000001050504010006070707070707
                      0707070707070707000007070707070707070707070703020501070707070707
                      0701020202020204070707070707070703060000040502020202020204040000
                      0001010607070707070703040405040405050202020505040404040502040707
                      0707070707070707070703030707070707070707070707070707070707070707
                      0707070707070707070703010106070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070102050106060105020504000000060707070707
                      0707070707070707070707070606030707070707070707070707070707070707
                      0707070707070703060104040404040400070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0706040202020205010707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707030006030707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0103060505060707070707070707070707070707030407010607070707070707
                      0707070707070707070403060505030707070707070707070707070707000107
                      0505010707070707070707070707070707000107070000070707070707070707
                      0707070707070000070402010707070707070707070707070707010301020201
                      0707070707070707070707070707070407070601070707070707070707070707
                      0707070707040501070701070707070707070707070707070605040304050501
                      0707070707070707070707070105010701050107070707070707070707070703
                      0505000707010307070707070707070707070305050606050506070707070707
                      0707070707070704050007040500070707070707070707070707070105010700
                      0607070707070707070707070707000504070604040707070707070707070707
                      0000070707070707070707070707070707070105020104060604070707070707
                      0707070707070605050203000107070707070707070707070707070404020005
                      0701010707070707070707070707070604050503000305070707070707070707
                      0707070605050500040204070707070707070707070707070004050506010705
                      0707070707070707070707070104020103050607070707070707070707070707
                      0704040200070001070707070707070707070707070707070304070507070507
                      0707070707070707070707070106000100050307070707070707070707070707
                      0403040304070503070707070707070707070706010305010402010707070707
                      0707070707070000060400060304070707070707070707070707060103050000
                      0305070707070707070707070707070407050301000707070707070707070707
                      0707010601000503010007070707070707070707000007070707070707070707
                      0707070707070004040600050500070707070707070707070707030506040707
                      0507070707070707070707070707070101040305010400070707070707070707
                      0707070305000107010005070707070707070707070707030500010604010007
                      0707070707070707070707070605000107070104070707070707070707070707
                      0004040307030506070707070707070707070707070101050307060107070707
                      0707070100070707070707070707000507070407070707070707070707070707
                      0706040007030503070707070707070707070707070005030700050707070707
                      0707070707070707070104010404060707070707070707070707070304010600
                      0004070707070707070707070707070700040404000407070707070707070707
                      0707070706050703040707070707070707070707070707060506040105060707
                      0707070707070707000007070707070707070707070707070707070402030004
                      0401070707070707070707070707070605040707000107070707070707070707
                      0707070705020705000003070707070707070707070707070005010702010607
                      0707070707070707070707070005010701050007070707070707070707070707
                      0701050003030104070707070707070707070707070402070607030407030707
                      0707070707070707070705050706040107070707070707040107070707070707
                      0703000507010507070707070707070707070707030304060607000107070707
                      0707070707070707030605070306050707070707070707070707070303040107
                      0405060707070707070707070707030304010005040307070707070707070707
                      0707070301040104010307070707070707070707070707030005070701060707
                      0707070707070707070707060507040105060707070707070707070700000707
                      0707070707070707070707070707070302030005050007070707070707070707
                      0707070701040301040207070707070707070707070707070302070105050307
                      0707070707070707070707070704010705010007070707070707070707070707
                      0704010707050007070707070707070707070707070705000305050407070707
                      0707070707070707070302030005050107070707070707070707070707070602
                      0703050107070707070707040107070707070707030504050706020307070707
                      0707070707070707000505060105050007070707070707070707070705040507
                      0504050707070707070707070707070605050107030203070707070707070707
                      0707000505000605000607070707070707070707070703050501030504010707
                      0707070707070707070707050405030000050707070707070707070707070404
                      0207040405030707070707070707070700000707070707070707070707070707
                      0707070703070703030707070707070707070707070707070703030000000707
                      0707070707070707070707070703070703030707070707070707070707070707
                      0707030706000607070707070707070707070707070703070703030707070707
                      0707070707070707070703030703030707070707070707070707070707070607
                      0703060707070707070707070707070707070706070707030707070707070704
                      0107070707070707070306070707060707070707070707070707070707060607
                      0706060707070707070707070707070707060307030603070707070707070707
                      0707070703060707070607070707070707070707070707030607070001060707
                      0707070707070707070707030607070306070707070707070707070707070703
                      0003030100010707070707070707070707070700030707060607070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070704010707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070606000307070707070707070707070707070707
                      0306060603070707070707070707070707070707030606060307070707070707
                      0707070707070707070606060307070707070707070707070707070707030606
                      0607070707070707070707070707070707070306030307070707070707070707
                      0707070707070306060307070707070707070707070707070707070306030707
                      0707070707070704010707070707070707070603030307070707070707070707
                      0707070707030303030707070707070707070707070707070703030303070707
                      0707070707070707070707070703030607070707070707070707070707070707
                      0303030707070707070707070707070707070707030303070707070707070707
                      0707070707070707030307070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707060505
                      0505040505010307070707070707070707030105050505050504060707070707
                      0707070707070005050405040505000707070707070707070707060405050505
                      0505010307070707070707070707070105050505050504060707070707070707
                      0707070604050505050505040307070707070707070707030405050505050501
                      0307070707070707070707070005050505050205060707070707070401070707
                      0707070605050505050505000707070707070707070707010505050505050403
                      0707070707070707070703040505050505050103070707070707070707070004
                      0205020502040607070707070707070707030402050205020501070707070707
                      0707070707030405050205050501030707070707070707070700040505020505
                      0406070707070707070707070701040202020505010307070707070707070707
                      0000070707070707070707070707070704050607000506070301050007070707
                      0707070700050103060104070700050407070707070707070305040603010507
                      0703040506070707070707070701050007000506070700020007070707070707
                      0706020103060104070700050107070707070707070701050007070707070301
                      0500070707070707070701050003070707070301050007070707070707070305
                      0406070707070706050403070707070401070707070304050607070707070604
                      0503070707070707070605010307070707070005010707070707070707010501
                      0707070707030105000707070707070703050406030005070700050407070707
                      0707070701050007000503070301050607070707070707070105000706040607
                      0301050607070707070707030504060300050707000404070707070707070706
                      0501030601000703010500070707070707070707000007070707070707070707
                      0707070504070707050503070707060206070707070707000203070700020407
                      0707070504070707070707030501070703050507070707000206070707070707
                      0105030707050506070707030200070707070707060206070701020107070703
                      0504070707070707070402030707070707070707060200070707070707010203
                      0707070707070707060200000707070707030504070707070707070707040507
                      0707070401070707030505070707070707070707040503070707070706020007
                      0707070707070707020107070707070701020307070707070707070002000707
                      0707070305010707060505070707070405070707070707040407070702050307
                      0707060206070707070707040407070705020607070706020007070707070305
                      0007070005040707070704050707070707070005060707040501070707030200
                      0707070707070707000007070707070707070707070701050503070706020707
                      0707040405030707070706050501070707040107070706050401070707070704
                      0405030707010407070703050105070707070701050506070703020707070701
                      0405060707070706050500070707050007070706050400070707070701040400
                      0707030101070707010405030707070700040506070703000107070701040505
                      0607070707050405070707060503070707050404070707040107070704010503
                      0707060403070707050105070707070305010407070706050707070305050007
                      0707070005050007070301010707070104050607070707050501070707040407
                      0707030504040707070701020503070700020707070701050503070707070105
                      0507070706020707070700050506070707070505010707070404070707070504
                      0407070707060205030707030206070707060505000707070707070700000707
                      0707070707070707070605070605030703010603070401070604070707070506
                      0304000707060103070005030705030707070004070005030703010307030500
                      0701010707070305070605060707010607070104070605070707070400070501
                      0707000007070005030305030707070305030305070700020407070305070605
                      0707070305030305070701020407070305030606040707070101070400070702
                      0503070701010704000707040107070604070101070705020007070004070100
                      0707070400070506070305020307070506070503070703050306050707010204
                      0707030507060507070700040704040707030107070305000704060707060507
                      0005030707010307070404070004070707030507000503070701030707000507
                      0604070707010007040107070300070703050007040607070305030605030707
                      0006070706050303050707070707070700000707070707070707070707040607
                      0706050405040505040107070705060707060507070705040505040204050307
                      0700040707070503070700050405040504050007070302070707010007070605
                      0402040505040407070704060707060507070704040505040201050307070001
                      0707070101070701010707050107070400070704060707010107070401070705
                      0107070401070707050607070503070302030700020707030507070605070704
                      0107070503070705030706020307030203070302070703050707060507070105
                      0707000407070001070701000707040007070501070704010707050607070503
                      0707040404050502040500070706050707040007070005010505050504040707
                      0705060707040007070005010205050504040307070506070705070707040404
                      0505020105000707030507070001070706050105050505010503070701000707
                      0707070700000707070707070707070707020703070701040306070602030707
                      0700010707010007030707020303060705010707070705070703050703070704
                      0103060700050707070705060707050307030700040306030305030707070001
                      0707010107030707020606030705000707070305070707050303070305070706
                      0607000507070700010707050303070305030706060706050707070700040700
                      0407030701010707010707040007070705030704010703050707070004070701
                      0707010107070704060700010703070400070701070705060707030507070503
                      0707030203070606070605070707000407060407030707050107060704040707
                      0707050307050707070701050303070602070707070101070705030707070105
                      0303070602070707070001070001070707070200070307040407070707050307
                      0406070707060506070703020607070706050707070707070000070707070707
                      0707070706050700000705070101040701000105000304070704060704070101
                      0304040303050605040705070700040704070305070101010705060405060100
                      0707050706000705060001040701010005010305070704060704070101030404
                      0303050305050705070707050701060701040707070705060105060604070705
                      0704060701010707070705060105000006050700010704070705030707070605
                      0305040704060704010706040704070705030707070305070404070100070400
                      0301070302070707070104060404070507070507000607010107070707040001
                      0500030507000107040703050704040607050304050304000705070106070503
                      0004010704000104000604070305070003070503000404070101000500060407
                      0100070407060407040406070503040406010007050703010704000304040700
                      0406040407020707070707070000070707070707070707070604070106070507
                      0601040700010700010304070704030704070406070105030705070305070503
                      0700010704070604070004000701000701000001070705070601070507060104
                      0706040706040305070705030704070106070105030705030304070503070702
                      0701060707020401040405070701010305070705070103070705040401040507
                      0300010107050701000704070700050101010500070304070406070401070001
                      0704070700020101010501070304030001070406060107070405010101020307
                      0604070503070507010607030504010104050707000103050701010704070001
                      0700040007040607040001000604070106070507030004070001030001060407
                      0604070006070507030004070604030604070507040007040701010700040007
                      0406070106010007050703010705030700040707050303040705070707070707
                      0000070707070707070707070604030506070507000103070100000106060407
                      0704060105070101070506070305030005070507070004060207030507010007
                      0704060004030101070705070500070503060403070001000001030507070406
                      0005070101070506070305030604070507070705030206070602000000000507
                      0004060604070705070203070305000000000503000400060605070101010507
                      0704040000000401030605070400070401070604000507070004000000010407
                      0005070101070406040407070501000000050606060407050707050305060703
                      0500000000050300040003050700010605070604070400070705030005070100
                      0305030506070507000407070101060100060407030507050307050300040307
                      0001060001030407010000050700010705060707050700050301000705070401
                      0704060305030706040306040702070707070707000007070707070707070707
                      0702070603070104030100060207060403000107070100070607030203000107
                      0500030100070507070305070607070401030403000507000407050607070503
                      0306070004030400030203030406000107070100070607030503000107040003
                      0400030507070705030603070401070707070401060403000107070503060307
                      0400070707070101000403030004070604070007030507070707060503040007
                      0503070401070305070007030503070707070506010107040607000103060700
                      0507070707060503040007050707050306030704000707070701010004030004
                      0706040700070705000604030004070104070503070507060307010407040603
                      0503060403010107070207060307010407040003050306040600010700010700
                      0707050600040300050701010705030704060306070605070101070400030400
                      0305070707070707000007070707070707070707070400070707050405040505
                      0501070707050307070605070707000505050405040503070700010707070503
                      0707030505050405050506070707020707070100070707040505040405050107
                      0707040607070605070707060505050405050507070700010707070101070706
                      0507060401070305030707040607070101070706050706040407070503070707
                      0400070705070707040007010406070001070706050707040107070503070701
                      0107010400070004070707050707030507070705060304040307040007070604
                      0707010007070605070604010707050307070400070705030707030505050405
                      0505060707030507070406070707040505040402050007070705030707040007
                      0707040505040405050107070704060703050707070605050404040505060707
                      0705070701010707070005050401050504070707000107070707070700000707
                      0707070707070707070604070705000700050407070401070004070707070506
                      0701050307050200070005030705030707070004070605060701050107030506
                      0704010707070305070704010706050507070404070605070707070506070005
                      0307040506070005030705030707070302070705060703050607070104070605
                      0707070305070705060703050607070104070606050707070401070605070701
                      0407070705030704000707040107070004070605070700040307070500070101
                      0707070506070004070704010707030507070506070703050707050607030500
                      0707010407060507070701010703050007010201070605060704000707060507
                      0704010706050507070500070005070707060507070401070602050703040407
                      0605070707040607060506070102010706050307040007070305070700050703
                      0505060701040703050307070707070700000707070707070707070707070401
                      0401070703050607070704040503070707070605010507070701040707070005
                      0501070707070705010506070706050307070305040507070707070104040107
                      0703050007070704040506070707070605010503070704040707070005040107
                      0707070701040004070707030407070705060506070707070104000407070703
                      0407070705060505060707070705060206070707010007070004000507070704
                      0107070705060400070707000107070605060503070707060503050307070704
                      0307070101010407070707010400050707070304070707050605000707070305
                      0005000707000503070706050105070707070404040107070302000707030504
                      0506070707070401040407070305000707070404050607070703050005060707
                      0104030707060501050707070701040005070707050107070701040401070707
                      0707070700000707070707070707070707070705050707070703040707070602
                      0607070707070700020607070707010007070705040707070707070305040707
                      0707060407070700020607070707070704020707070703050707070302000707
                      0707070700020607070707040607070705040707070707070705020307070605
                      0407070701020007070707070704020307070605040707070102010107070707
                      0706020407070704050007070705020307070704010707070302050707070104
                      0107070705020607070707070002010707070405060707070205070707070707
                      0402060707060405070707010201070707070706020107070707000107070700
                      0203070707070705050707070703040707070302000707070707030505070707
                      0703050707070302010707070707000200070707070100070707010203070707
                      0707040207070707070403070707050407070707070707070000070707070707
                      0707070707070707040506070304040703010500070707070707070700050103
                      0704050607000501070707070707070703050406070604040706040506070707
                      0707070707010500030304050707010500070707070707070700050107070105
                      0607000501070707070707070707010500070707070703010500070707070707
                      0707010500070703070707010500070707070707070706050403070707070703
                      0405030707070704010707070703050403070703070703040506070707070707
                      0700050107070703070706050407070707070707070105000707030707070105
                      0107070707070707060504030700010107030405030707070707070704050607
                      0304050707010500070707070707070304050607030404070701050107070707
                      0707070605010307010101070301050307070707070707010506070701040307
                      0005040707070707070707070000070707070707070707070707070707060502
                      0404040502010707070707070707070707030102050404040205060707070707
                      0707070707070005020404040505010707070707070707070707060402040404
                      0502040307070707070707070707030402050404040205060707070707070707
                      0707070605050401040402040307070707070707070707060502040101040204
                      0307070707070707070707030102050401040502000707070707070401070707
                      0707070102050401040502010307070707070707070703040205010104020500
                      0707070707070707070706050204010104020406070707070707070707070102
                      0504040405020107070707070707070707000505040404040204030707070707
                      0707070707000505040404040204030707070707070707070304020401040105
                      0201070707070707070707070605050401010405050607070707070707070707
                      0000070707070707070707070707070707070703060000060707070707070707
                      0707070707070707060000060307070707070707070707070707070703000100
                      0307070707070707070707070707070703060100060707070707070707070707
                      0707070707060001060307070707070707070707070707070703000101060307
                      0707070707070707070707070703000101060307070707070707070707070707
                      0707060001010607070707070707070401070707070707070706000100060707
                      0707070707070707070707070706010100060707070707070707070707070707
                      0300010100030707070707070707070707070707060101000607070707070707
                      0707070707070706000101060307070707070707070707070707070300010100
                      0307070707070707070707070707070601010006070707070707070707070707
                      0707030001010003070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070401070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707000405020505040607
                      0707070707070707070707060105020205040007070707070707070707070707
                      0004050505050106070707070707070707070707060405050505010607070707
                      0707070707070707030105050505040007070707070707040107070707070703
                      0004050505040003070707070707070707070703010505050504000707070707
                      0707070707070706040505050501060707070707070707070707070005050505
                      0400030707070707070707070707060405050505010307070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070605040007040703000504030707070707070707030405
                      0007000007000405060707070707070707070605040007070703010504030707
                      0707070707070605050007070703000504070707070707070707070105010607
                      0707060405000707070707040107070707070005040607070706040501070707
                      0707070707030105010307070700040506070707070707070703050500070707
                      0301050407070707070707070701050406070407060405000707070707070707
                      0605050007040703010501070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700050607
                      0705020607070700020307070707070706050107070402040707070602000707
                      0707070707000206070707070707070701050307070707070700020607070707
                      0707070701020303070707070707040403070707070707070305010707070705
                      0007070707040503070707070707070305040707070707070305040707070707
                      0707070602000707070707070602000707070707070707010206070707070707
                      0404030706020207070703050107070707070700050607070202060707070105
                      0307070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070602050707070002070707070005050307
                      0707070305050307070305000707070305050607070707070005050607070700
                      0007070701040507070707070605050307070306000707070005050507070707
                      0704050407070703010707070705050107070704010707070104050707070701
                      0307070704050107070707070504010707070301070707030505000707070706
                      0505060707070600070707000505030707070704020007070704040707070701
                      0201070707070602040707070105070707070602050707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070503010507070701070707060503010107070707010106020307070000
                      0707070506030507070707030503060407070102040707030507010107070707
                      0503000407070102040707070203000101070707000407050607030202060707
                      0004070503070704010707030507040007070502000707060507050607070701
                      0107050307030202030707010103050707070705060605070701020407070705
                      0301040707070605030500070703010707070104070506070707050604040707
                      0704070707060203010107070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070700010707040506040205
                      0100050307070503070703020707000506010505040605000707040007070701
                      0107070506070302000707010107070503070700010707040607030200070701
                      0107070702030707050307060507070105070707050307000407070400070704
                      0607030507070002070707050607060507070702070700040707010507070605
                      0707010107070604070704000703050107070104070705030707040607030500
                      0605050506010407070004070701010707040403040505000005030707020307
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070700040707010506040505010005030707050307070305
                      0707060500010502040605000707040007070701010707050607030500070701
                      0107070503070700040707050607030200070701010707070503070705030706
                      0507070105070707050307010407070401070704060703050707000207070705
                      0607060507070305070700040707040507070305070701000707000407070400
                      0703050107070101070702030707050607030501060405040601040707000407
                      0701010707040406040505000005060703020707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070403
                      0707070102000707010203070707010007070101070707060201070700020007
                      0707060407070705030707060507070000070605070707010007070503070706
                      0507070000070305070707070100070604070707040007070407070101070707
                      0507070401070305070707010107070407070004070707050307000407070705
                      0607070407070400070703050707040607070605070700000703050307070101
                      0703050707070305050607060505070707070503070503070707010200070704
                      0203070707040007070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070507060607040600040107040006
                      0406060407070403070107000103040407060403040007050707070507000307
                      0400070707070400000403000107070507010707040007070707040000040303
                      0004070001070007030507070707030503040007050307050007060407000703
                      0507070707070503010107010007010003000700050707070706050304000705
                      0707050706060704000707070701010604030604070001070007070507010406
                      0705070004070406070507060607040006040107040006040300040707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070507000007050703030407000106060403050707050307040704
                      0607060407070503060407050707030507010307060506060600050706000106
                      0407070507010307030506060600050700000101030507010007040707010106
                      0606040003060407040607040107000107040707000406060601010700040301
                      0007040606010707050006060605060606050705030705070106070305060606
                      0005070000010305070101070407060407060601070406060100000107050701
                      0607050703030407000100060103040707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707040704000705
                      0300050607000107010006040707050300040701000702000703050706040705
                      0707070503050307030204040404040707050606040707050705030707050404
                      0404050707040606030507010000040707000504040405060706040704060704
                      0007000106040707000504040405000703050701000704060101070704050404
                      0402030700040705070705070400070705040404040507070406070507000103
                      0507060407040403070406070507010003050704060705070605060700010701
                      0006040707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707050700060701010304000705060005060001
                      0707040606010706050704010701010604010705070707050704030704010707
                      0707050601040300010707050704070701010707070704000105030306040700
                      0106010707050707070706040604010705030704010703050301070705030707
                      0703050304040704000701000001070605070707070004060401070507070503
                      0106070101070707070400010506000407060403040703050300040303050704
                      0507040607050701060701010704060705060005060001070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707040607070700020400000502060707070406070700040707070302050000
                      0502010707070004070707040007070605070300000706050707070406070704
                      0007070305070300000703050707070704000703050707070401070000030704
                      0007070305070704010707050707070001070600030701010707070503070604
                      0707070400070000030705060707060407070400070703050303000007030507
                      0707010007030507070707040200060102050707070705030704030707070002
                      0406000502030707070400070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070706040707010503010202
                      0606050007030507070703050307060506060202000704010707050307070700
                      0407070400070005060707040007030507070706040707040007000500070704
                      0007030305070707040607060507070404030703050707010107070400070701
                      0007070503070104060707020307000407070705030706050703050103070605
                      0707040007070604070701010706020007070101070702030707040007070404
                      0305020503000507070101070700040707000503010202060605060706050707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707040001040307030200070703050005000707070700
                      0406050607070501030707040401010707070707050000040707070604070703
                      0507050007070707040000040707070604070703050304040007070703050305
                      0607070704060707010106050307070401070703050304000707070100070700
                      0403020307070700040705030707070403070704000004070707070401030507
                      0707060407070305030401070707030503050107070105060707000506050307
                      0707050001050307030200070703050604000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0505070707070304070707030201070707070707040203070707070403070707
                      0505070707070707030502030707000104070707010201070707070707050203
                      0707000104070707010204040707070707000204070707010100070703050506
                      0707070500070707060505070707010001070707050506070707070704050107
                      0707040406070703020503070707070705020607070601050707070102040707
                      0707070002010707070701000707070002060707070703020507070707030407
                      0707030201070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707030404030706010507070605
                      0107070707070707070105060707010406070304040307070707070707030501
                      0707070603070706050107070707070707030501030707060307070305010707
                      0707070707070005060707030007070700050607070707040107070707000500
                      0707030007070706050007070707070707040406070706060707030105030707
                      0707070703050103070706030707030404070707070707070005060707000004
                      0707000500070707070707030501030703010407070304040707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070700050501040404050406070707070707070707070004
                      0501040501050500070707070707070707070701050401010101050406070707
                      0707070707070701050501010104040506070707070707070707070604050101
                      0001040501030707070707050007070707070304050101000101050406070707
                      0707070707070004050101000104050107070707070707070707000504010001
                      0104050007070707070707070703040501040504010504030707070707070707
                      0701050401040401040506070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0600010106030707070707070707070707070707030001010006070707070707
                      0707070707070707070600010100060707070707070707070707070707060001
                      0100060707070707070707070707070707030001010106030707070707070704
                      0107070707070707030001010100030707070707070707070707070706000101
                      0106070707070707070707070707070706000101000607070707070707070707
                      0707070300010101000307070707070707070707070707060101010006070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707030007070600070707070707
                      0707070707070707030007070703070707070707070707070707070707030007
                      0703000707070707070707070707070707060607060000060707070707070707
                      0707070707070603070707030707070707070705000707070707070707030707
                      0707060707070707070707070707070707030707060000060707070707070707
                      0707070703070707030603070707070707070707070707070307070707030707
                      0707070707070707070707030707070303070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070703050104060501040707070707070707070707070305010407
                      0304000707070707070707070707070706050104060501040707070707070707
                      0707070700040400010501060707070707070707070707070701040506070001
                      0707070707070704010707070707070707010007070704030707070707070707
                      0707070707050707010501000707070707070707070707070601070704040507
                      0707070707070707070707070403070707040607070707070707070707070705
                      0707060504010707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070304070407
                      0707050707070707070707070707070601070501050501070707070707070707
                      0707070700010304030703040707070707070707070707070406000107010107
                      0707070707070707070707070704030000070001070707070707070500070707
                      0707070707060407070704070707070707070707070707070704030707010107
                      0707070707070707070707070304070703070406070707070707070707070707
                      0100070405020107070707070707070707070701060703070704070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070705020403020401070707070707070707
                      0707070702050103050406070707070707070707070707070702050107070501
                      0707070707070707070707070002020607070400070707070707070707070707
                      0700020203070000070707070707070401070707070707070707040707070407
                      0707070707070707070707070706010707070100070707070707070707070707
                      0701030707010507070707070707070707070707030407030504060707070707
                      0707070707070703040700050401070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070305030407050307070707070707070707070707030506050700020607
                      0707070707070707070707070605030406010004070707070707070707070707
                      0100010001060001070707070707070707070707070406040000020107070707
                      0707070500070707070707070707060407040207070707070707070707070707
                      0707010301060604070707070707070707070707070304070107020707070707
                      0707070707070707070001070002060707070707070707070707070701000604
                      0607070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070004060704050007
                      0707070707070707070707070104060707010307070707070707070707070707
                      0701040607010406070707070707070707070707030404030304040607070707
                      0707070707070707070305040707000607070707070707040107070707070707
                      0605050207070407070707070707070707070707010502010304050307070707
                      0707070707070707050502030005010707070707070707070707070005020407
                      0701030707070707070707070707000505040705050007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707050007070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707060205050505
                      0502050205020505050505050205020502050502050202020202020502020502
                      0205020502020505020502050205050505050505020505050505050505050505
                      0405040505050405050505050505050505050405040505050405050405040504
                      0505050505040504050405040505040505040505050505040505050505050504
                      0505040505040504050405040504040505040504040504050405040504050502
                      0505040504050405040504050405040504050405040504050405040504040504
                      0504050405040504050405040504050405050405050404050405040504050405
                      0405040504050405040504050405040504050405040504050404040504040404
                      0404050405040404050405040504050404050404040504040404040404040404
                      0504040404040404040404040404040404040404040404040404040404040707
                      0707070700000707070707070707070606000606060606060606060006060606
                      0606000606000606060006060606060606060006000606000606060006060006
                      0006000600060006000600060006000600060006000600060006000600060006
                      0006000600060006000600060006000600000000000600060000060000000000
                      0000000000000000000000010000000000000001000001010001010101010101
                      0101010000010001010100010001010101000005040101000101010101010101
                      0101010101010101010101010101010101010101000100010101010101010101
                      0101010101000101000101010101010101010101010101010101010101010101
                      0101010101010101010101010101010101010101010101010101010101010101
                      0101010101010101010101010101010101010101010101010101010101010101
                      0101010101010101010101010101010101000707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070704000707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070705050003
                      0505000707070707070707070707070305050007070106070707070707070707
                      0707070703050500030505000707070707070707070707070005050304020500
                      0707070707070707070707070700050503070000070707070707070500070707
                      0707070707040501070701070707070707070707070707070604050604050400
                      0707070707070707070707070005040701050407070707070707070707070703
                      0405000707010307070707070707070707070304050003050506070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070306070403060705070707070707070707
                      0707070306070501040201070707070707070707070707070606070406060705
                      0707070707070707070707070003060106050607070707070707070707070707
                      0700070100070001070707070707070400070707070707070601070507070507
                      0707070707070707070707070403060106050307070707070707070707070707
                      0407010600070406070707070707070707070700000704010502010707070707
                      0707070707070106070400030304070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070704010403040104070707070707070707070707030401040604040607
                      0707070707070707070707070304010407030401070707070707070707070707
                      0001040007030406070707070707070707070707070001040607000007070707
                      0707070500070707070707070005010407070407070707070707070707070707
                      0504040007030406070707070707070707070703020105030700050707070707
                      0707070707070701050104000404060707070707070707070707040404010601
                      0104070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070500070705060707
                      0707070707070707070707070500030701020307070707070707070707070707
                      0305000706060004070707070707070707070707000400070003000107070707
                      0707070707070707070104000700050007070707070707040007070707070707
                      0604000007010207070707070707070707070707040106030003060407070707
                      0707070707070707050600070003050707070707070707070707070004000607
                      0102030707070707070707070707010100060604000707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070405010705050007070707070707070707070707
                      0404000707040607070707070707070707070707070505000305050607070707
                      0707070707070707030505030605050607070707070707070707070707060504
                      0307010007070707070707050007070707070707070405010703050707070707
                      0707070707070707060505030605050607070707070707070707070700050407
                      0105040707070707070707070707070304050007070503070707070707070707
                      0707070505060702050007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070704
                      0007070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070705000707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070606000607070707070707070707070707070707060600
                      0607070707070707070707070707070707070600000607070707070707070707
                      0707070707070600060307070707070707070707070707070707030606060707
                      0707070707070704000707070707070707030606060707070707070707070707
                      0707070707030606060707070707070707070707070707070706060606070707
                      0707070707070707070707070706060607070707070707070707070707070707
                      0606060707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707000405040401
                      0405040307070707070707070707030405040401040504060707070707070707
                      0707070004050401010405040307070707070707070707000405040101050501
                      0307070707070707070707070105040401040505000707070707070500070707
                      0707070005050401040504000707070707070707070703010504040104050406
                      0707070707070707070706040504040105050403070707070707070707070005
                      0504040405050007070707070707070707060405040404050501070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070704040607010203070700050007070707070707
                      0700050607000401070706050107070707070707070704040307070707070700
                      0500070707070707070704040307070707070700050007070707070707070605
                      0107070707070703040403070707070400070707070305040307070707070304
                      0503070707070707070005000707070707070605010707070707070707010506
                      0707070707070005000707070707070703050103030105070703040403070707
                      0707070704050607010503070700050007070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0501070707040507070707030200070707070707010507070700020007070707
                      0504070707070707030505070707070703070707060200070707070707050507
                      0707070307070707060200000707070707060200070707070307070707040503
                      0707070500070707030201070707070307070707010203070707070700020607
                      0707070707070707050407070707070704020707070707070707070602010707
                      0707070302060707030505070707070105030707070707050107070705050307
                      0707030200070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070704040506070703050707070704
                      0105060707070700050401070707040607070701040401070707070704010400
                      0707000401070707040005060707070704010406070706040107070705000505
                      0607070703050005070707000503070703050004070707040007070705000503
                      0707000503070703050005070707070605000407070300050707070604040107
                      0707070104040007070601040707070401050007070703050404070707010407
                      0707030501050707070701040506070703050707070704040506070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070604070305060706010003070500070605070707070503070401
                      0703000103070004070705030707070605070305070701050107070004070604
                      0707070305070305070701050007070004070606040707070400070101070305
                      0507070704000704000707050007070101070101070705050307070101070101
                      0707070506070406070305050707070503070503070703050703050307000201
                      0707060507060507070701010701040707060006070305060701010707060507
                      0605060703000607070401070605070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707040607070305
                      0504010105050007070704060707000407070704050501010505040707070001
                      0707070406070701010707040007070506070704060707040607070400070705
                      0007070503070707050607030407070305030706050707060407070605070705
                      0007070507070705030703020707060507070305070706040707030507070005
                      0707010107070001070704000707010107070400070705060707040607030507
                      0707000505040105050506070703050707040607070305040504040505010707
                      0705060707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707050703030701010300030305030700070001
                      0707040607030706050700060704000706030304070707050703070705030703
                      0307010107060701000707050706070305030703030701010303070700010700
                      0107030700010707030707050303070705070704000703040703070604070703
                      0707040603030704030701000703070401070706030302030307070507070507
                      0307070503070703070004070307000407060407030707050603000700050703
                      0307040307050703030701040706030305030703070001070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707040700000705070000040701010001040304070705070704070106030104
                      0703050301040702070707050704070700040707070305030104000604070705
                      0704070700040707070305030104000006040701000704070705030707070101
                      0604040704060705000700010704070705060707070004030404070100070406
                      0601070305030707070400060104070507070507010607000407070707050301
                      0401060507010107040706040701010007050301040001000705070006070503
                      0601040701010001010304070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070705070000070507000401
                      0700010701010304070705070704070403030404070705070604070507070705
                      0701030703020505050504070704060604070305070407070702050505050407
                      0705060606040704060704070700020505050203070604070406070500070101
                      0704070706020205050206070305070100070406060007070402020505020707
                      0004070507070507000607070505050502040707040003050701000704070001
                      0701050607040607040301010304070006070507060401070604070101060407
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070704070506070503060406070406000400000407070406
                      0104070101070506070604060104070507070705030207070004070707030207
                      0104030001070704030207070004070707030507010403030004070100010407
                      0705060707070101060104070503070400070001000407070400070707000403
                      0105070406070406040107070503070707040006010107050707050302060706
                      0407070703050301040606040700010602070305070400070705070104030106
                      0705070506070503030403070100000100060407070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070503
                      0307070005060603000207030007010007070100070307070206060306020607
                      0003060407070705030307070503070707070101060007010007070507030703
                      0503070707070101060007070100070604070307000407070707070503000303
                      0507070500070305070307060407070707070506000607050307010107030701
                      0107070707060503000306040707050303070705060707070700040600070101
                      0706050703070704010300070404070600070503070503030307000503060300
                      0207030003010107070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070101070703050004020201010507
                      0707050707070605070707040104050204010506070704000707070101070701
                      0107000501070705030707050307070101070701010701020107070503070707
                      0207070705070703050307050406070004070700040707050007070503070705
                      0607040406070605070706050707030507070305070305040307010107070100
                      0707010107070104070005010707050607070506070705030707000504050502
                      0105000707060507070100070703050105050504010407070705030707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070705030605060700050407070005030101070707070400070401
                      0707020503070305060305070707070305030305070707040007070004070101
                      0707070305030305070707040007070004070101010707070104070101070706
                      0407070704060705030707050007070004070001070703040307070400070506
                      0707070400070400070700040707030503030507070703050303050707070100
                      0707060507000407070700040700050307040200070304010704060707030503
                      0305060706020407070104030104070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070005050607
                      0707010107070700020507070707070305050107070706040707070605020307
                      0707070700050400070706030407070704010507070707070005040007070303
                      0407070705010505070707070705010507070706000007070305040107070704
                      0007070704010503070706060107070305040107070707030500050707070604
                      0307070004050007070707000504000707030304070707040105030707070704
                      0405030707030503070707040504070707070005050607070701000707070005
                      0503070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700050607070303040707070105030707
                      0707070706050007070706010607070605000707070707070701020307070604
                      0007070704050307070707070700020307070604000707070405030307070707
                      0703050407070701040307070305040707070705000707070305050707070104
                      0607070705040707070707070602010707070404070707060200070707070707
                      0002060707030401070707000206070707070703050107070703070407070704
                      0407070707070701050307070303040707070005060707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707060504000304010301050407070707070707070703050500030404
                      0300050503070707070707070707060504060707070301050403070707070707
                      0707000204060707070300050403070707070707070703040500030707070301
                      0200070707070705000707070707040201030707070301020107070707070707
                      0706050500070707070604050007070707070707070005040607070707000405
                      0307070707070707070405010300050003010201070707070707070700020103
                      0704040700050503070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070004050202
                      0501030707070707070707070707070604050202050400070707070707070707
                      0707070701040205020504060707070707070707070707070105020502050406
                      0707070707070707070707070604050205020501030707070707070500070707
                      0707070304050205020501030707070707070707070707000402020502050007
                      0707070707070707070703000502050502040007070707070707070707070304
                      0502020205010307070707070707070707070105020505020400070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070500070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070703070707070707070707
                      0707070707070707070307070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070300040104010607070707070707070707070707070601040401
                      0007070707070707070707070707070600010401000307070707070707070707
                      0707070300010101000307070707070707070707070707070601010101060707
                      0707070707070707070707070306010101000307070707070707070707070707
                      0706010101000307070707070707070707070707070300010101060707070707
                      0707070400070707070707070700010101000707070707070707070707070707
                      0301010101060707070707070707070707070707060101010003070707070707
                      0707070707070707000101010607070707070707070707070707070600010100
                      0307070707070707070707070707070600010100030707070707070707070707
                      0707070601010106070707070707070707070707070703000000000307070707
                      0707070707070707000007070707070707070707070707070304040100010600
                      0405060707070707070707070700050400000000010501070707070707070707
                      0706050401000100010404030707070707070707070701050101010001040506
                      0707070707070707070700050401010001040500070707070707070707070701
                      0501010000010504060707070707070707070701050401000001050406070707
                      0707070707070706050501000001040501070707070707050007070707070304
                      0501010001010504030707070707070707070005040100000104050007070707
                      0707070707070105040100000105050607070707070707070703040501010501
                      0405040707070707070707070701050401040001050406070707070707070707
                      0701050401040001050406070707070707070707030405010101010405010707
                      0707070707070707000504010101010405060707070707070707070700000707
                      0707070707070707070707060500070704020307070305010707070707070707
                      0404070701050107070701050307070707070707000506070605050707070005
                      0607070707070707030501070704020607070305010707070707070703040403
                      0701020107070304040707070707070707030501070707070707070605000707
                      0707070707030501070707070707070605000707070707070707000506070707
                      0707070701050307070707050007070707000500070707070707070105030707
                      0707070707040503070707070707030404070707070707070304040707070707
                      0707030501070707070707070005000706040507070701050607070707070703
                      0504070704020607070605010707070707070703050107070402060707060500
                      0707070707070700050607060405070707010503070707070707070404030700
                      0500070703050107070707070707070700000707070707070707070707070602
                      0607070704050707070703020107070707070704050707070602000707070705
                      0503070707070700020307070305040707070701020607070707070302010707
                      0704050707070703020107070707070704040707070002060707070705050707
                      0707070703050507070707030307070700020107070707070305050707070703
                      0307070700020100070707070700020007070707030707070704050307070704
                      0007070700020107070707030707070704020307070707070402030707070703
                      0707070702050707070707070502070707070303070707060204070707070700
                      0203070703020407070707010203070707070305010707070505070707070302
                      0107070707070305000707070405070707070302000707070707000207070706
                      0501070707070105030707070707040107070701020607070707050407070707
                      0707070700000707070707070707070707070500050607070304070707030506
                      0406070707070104010407070701000707070401010407070707030500050607
                      0706010707070004060503070707070500050007070305070707030506050007
                      0707070104040107070704060707070401010107070707070506040007070005
                      0407070305030506070707070500040007070105010707070506050506070707
                      0605060507070304020307070605000507070705000707030503050307070102
                      0607070605060507070707010406050707060402070707010101040707070705
                      0004010707000404070707050005000707070605010507070700040707070005
                      0005070707070501050607070305070707030501050007070707050105060707
                      0705070707070401050007070706050404070707010107070700050005070707
                      0704040506070707050607070701040401070707070707070000070707070707
                      0707070707010107030500070104010603050307030507070703050707010407
                      0001040007040107070406070707040607060503030104010300050707010107
                      0707000107030500070004010603050607030507070703050707040407060104
                      0007040107070503070707000407030503070005000707010107060507070700
                      0407030507070105000707010107060605070707050607010107070505070707
                      0503070400070705060707040007010107070505070707050607040107070302
                      0307040007030505070703050307050607070004070305030700020007070004
                      0703020707070400070005030300010003000503070101070700040703050007
                      0001010303050007060507070700040703050007000101030305000703050707
                      0705030700040303000106070005070701000707060507030500070301000307
                      0101070705030707070707070000070707070707070707070705070707030505
                      0106000502030707070406070700010707070102040000040201070707000107
                      0707050707070602050100010505070707030507070704060707030505040000
                      0502060707070406070701010707070105050000050501070707000107070705
                      0607070101070704000703050307070503070705030707010107070506070705
                      0307070705060706040707030503070605070700010707060507070500070305
                      0707070503070305070700040707030507070001070703050707000407070400
                      0707000107070406070701040707040007070506070704000703050707070002
                      0504010405050307070305070704030707030505040001050500070707050607
                      0705060707030505040101050500070707040607060407070701050501010405
                      0503070703050707040007070705040501010505040707070101070707070707
                      0000070707070707070707070305070007070506060406070503060003000107
                      0704060700070004070401070101070106070507070004070007070503000403
                      0605070000070506070705070603070400060406070503060103000107070406
                      0706070004070400070400030006030507070705070607070506070707070400
                      0600070100070705070007030503070707070401060007070101070101070607
                      0004070703070305030003070503070400070604070607060407070307070503
                      0006070503070406030607040107070307060503000303050707050706070705
                      0607070307010403000700040700040706070705030001070605070600070503
                      0705070603070401030106070503030003000107070507030707040003010303
                      0503030003000107010007060703050300010700040706060705070705070306
                      0701040701060705060700030604070707070707000007070707070707070707
                      0604070103030407060704070100000601060107070507070407040603060403
                      0305060005070507070100070107000107060000070503000406010607030407
                      0006070507060304070100000001060407070507070407040303060407030406
                      0004070207070304070407070004070707060507010100000107030407040707
                      0004070707060507010400000004070406070407070506070707010106010407
                      0403070500070101070407070406070707010103010407040607050300000703
                      0503070707050600000407050707050701060706050707070305070101010305
                      0701010704070604070001000705030104060100030407010607050700060407
                      0001000101030407060407010307050704060407000100010106040704060704
                      0701000700010607050701040304060705070600070503060604070604060104
                      0705070707070707000007070707070707070707060107010303040700050007
                      0100070406060107070507030107040603050407070507000407050707010003
                      0407000107040403070406070507040007030403010607050701050007000107
                      0400060407070507030107040306050407070507000407050707060507040707
                      0702050202020107070503000107030407040707070202020202010707050303
                      0604070406070107070102020502020707000407050307050007010007040707
                      0002050205020307060507010607050700000707050205020205070701010705
                      0703050701060707050205020204070705060605070100070407000107040506
                      0704030705030400060407010607050706050107000107010006040706040701
                      0303050700050107000107010006040704060704070100070405030705070305
                      0704060705070601070507030404070305070001070507070707070700000707
                      0707070707070707060403050307050700010307040301040600010707040301
                      0407010107040007000106040407050707000106050703050301010703050704
                      0507040607070407050607050306040607040601040000010707050701010701
                      0007050007000100010103050707030506020707010407070706050704040701
                      0107070506050707010107070703050301050303000107010001040707050307
                      0707010106010407050307050607000101010707040607070701010604040705
                      0307050305000706050307070705000001010705070705030503070004070707
                      0305030104060604070100060507030507040007070507040507040607050305
                      0307050306040307040001010000040703050305030705030004030704060004
                      0000010701000004070001070406070305070105070403070507040007050606
                      0403070001060101030507070707070700000707070707070707070707020707
                      0707010500030304020707060704060707010007070703020103030102000706
                      0700040707030507070707050406030605040703030705070707050307070701
                      0500070301020707060704000707010007070703020003030002060706070001
                      0707070503070703050307070707010103060704060707050303070305070707
                      0707010106060707040607000107030701010707070707050306030305070705
                      0007060407030700040707070707050300030705070701000703070400070707
                      0706050700070604070705030307070503070707070004030007010107060507
                      0307070501030606040407060607050707050703070701050606030102070300
                      0701000707050307070701050306030105070300070400070001070307070200
                      0306030501070606070507070503070307060503030306020307000300040707
                      0707070700000707070707070707070707010007070605060405050000050707
                      0305070707060507070305000105020106050007070406070707050607070104
                      0004020406040107070104070707010107070605000405050101050307030507
                      0707060507070705010105020400050607070506070707010107070400070102
                      0107030503070305070707010007070400070102010707050307030302070707
                      0207070605070702050607000407070101070705060707050307070503070505
                      0607000407070104070706050707060507060204070704010707040607070101
                      0707010107000204070705060703050307070503070700050105020501050007
                      0700040707040007070605010502050401040707030203070704010707060501
                      0502020104040707070507070305070707010404050205000500070700040707
                      0101070707050105020204010503070704060707070707070000070707070707
                      0707070707030503060503070002040707000503040007070707040003050007
                      0302050607030500000407070707060507010407070102000707040103050307
                      0707070503060503070002010707000503040007070707040003050007030205
                      0307030506000407070707030503030507070704000707000407040007070703
                      0507060407070704000707000407040400070707010107040007070604070707
                      0406030503070705000707000407010107070305030707050007050607070705
                      0607050607070004070707050306050707070705030305070707010007070605
                      0701040707070004070005070704020007070501070503070703050306050607
                      0602050707010403010107070703050306050607000204070701050701010707
                      0701000701040707040206070305000705030707030503030500070305050707
                      0605030604070707070707070000070707070707070707070707000505030707
                      0700010707070602010707070707030502000707070304030707030502030707
                      0707070105040707070704060707070102000707070707060505030707070101
                      0707070602040707070707030505000707070604070707030505030707070707
                      0005050607070603040707070404040707070707000505030707000304070707
                      0504040407070707070504050707070001060707060505000707070506070707
                      0404050707070000000707060505000707070703050104070703060403070700
                      0405060707070706050400070706060407070704010503070707070404050307
                      0703040607070704050107070707000505030707070101070707000205070707
                      0707000505060707070100070707000205070707070705040407070706040307
                      0703050500070707070005050007070700010707070005020307070707070707
                      0000070707070707070707070707070005030707060304070703040407070707
                      0707070305000707070000060707000506070707070707070404030707060304
                      0707030501070707070707070005060707030304070703010507070707070707
                      0305000707070601060707000506070707070707070002030707060406070703
                      0404030707070707070102030707060400070707040503030707070707030501
                      0707070104030707030201070707070506070707070504070707000403070703
                      0501070707070707060200070707040107070706020607070707070700020307
                      0703040007070701020607070707070705010707070603010707030501070707
                      0707070105030707030304070707000503070707070707010503070703030507
                      0707000503070707070703020007070703000107070304010707070707070005
                      0307070703040307070605060707070707070707000007070707070707070707
                      0707070706050400060400000405000707070707070707070304050106010406
                      0105040307070707070707070701050400000400000405000707070707070707
                      0706050400060401060105010707070707070707070304050003010403000504
                      0307070707070707070700050406030707060105010707070707070707070002
                      0406070707030105040707070707070707070304050103070707000405000707
                      0707070206070707070301020103070707060102000707070707070707060504
                      0003070707000405060707070707070707000504060707070300050403070707
                      0707070707040501030005000601020007070707070707070002010603040107
                      0005040307070707070707070005010603040107000504030707070707070703
                      0405000301040606010500070707070707070700050103030404070004050307
                      0707070707070707000007070707070707070707070707070707000105050504
                      0003070707070707070707070707030104050505010607070707070707070707
                      0707070004050505040007070707070707070707070707000405050504010307
                      0707070707070707070707060105050505010607070707070707070707070707
                      0004050505050103070707070707070707070707000505020505010307070707
                      0707070707070707060105050205040007070707070707050607070707070703
                      0105050205050103070707070707070707070700040505020504000707070707
                      0707070707070700040502050504060707070707070707070707030105020205
                      0501030707070707070707070703010505050205040607070707070707070707
                      0703010505020505040607070707070707070707070604050505050500030707
                      0707070707070707070105050505050400070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707020607070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070703070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070702
                      0607070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707030407010505060707
                      0707070707070707070707070403070403070707070707070707070707070707
                      0601070104050707070707070707070707070707070407030505000707070707
                      0707070707070707070407070704030707070707070707070707070707070407
                      0605050007070707070707070707070707060107050205000707070707070707
                      0707070707070000070704070707070707070705060707070707070700020504
                      0707040707070707070707070707070704020500040505000707070707070707
                      0707070705050503040504070707070707070707070707000205010707040707
                      0707070707070707070701020501060505060707070707070707070707070002
                      0501030405060707070707070707070707070602050407040707070707070707
                      0707070707030505040701050407070707070707070707070000070707070707
                      0707070707070707070707030407040700000707070707070707070707070707
                      0407070100070707070707070707070707070707060107040701060707070707
                      0707070707070707070507060307050707070707070707070707070707040701
                      0502010707070707070707070707070707070407000303040707070707070707
                      0707070707000107060403070707070707070707070707070707000107070507
                      0707070707070705060707070707070707040107070704070707070707070707
                      0707070703050307060503070707070707070707070707070004070700070403
                      0707070707070707070707070500070105020107070707070707070707070304
                      0007000303040707070707070707070707070705000704030304070707070707
                      0707070707070704000707010607070707070707070707070707000407070507
                      0406070707070707070707070000070707070707070707070707070707070703
                      0407000202030707070707070707070707070707040707030407070707070707
                      0707070707070707060107020405030707070707070707070707070707040706
                      0401040707070707070707070707070707040700010407070707070707070707
                      0707070707070507070305000707070707070707070707070706010707030503
                      0707070707070707070707070707000007070407070707070707070206070707
                      0707070707070101070704070707070707070707070707070703050307030503
                      0707070707070707070707070706040707000507070707070707070707070707
                      0704000101040707070707070707070707070703040606000004070707070707
                      0707070707070707040605010401070707070707070707070707070701000706
                      0107070707070707070707070707070004070404050707070707070707070707
                      0000070707070707070707070707070707070605040704060406070707070707
                      0707070707070704050707070106070707070707070707070707070005010705
                      0600070707070707070707070707070704050703040107070707070707070707
                      0707070704050707010207070707070707070707070707070304040706030100
                      0707070707070707070707070005000700070100070707070707070707070707
                      0700050003040507070707070707070506070707070707070606070407010507
                      0707070707070707070707070007000000070000070707070707070707070707
                      0007040306060507070707070707070707070706060304070405070707070707
                      0707070707070603030400040107070707070707070707070707060303040401
                      0003070707070707070707070707030607050707010307070707070707070707
                      0707000704030400050707070707070707070707000007070707070707070707
                      0707070707070706010706050403070707070707070707070707070304070605
                      0505070707070707070707070707070700000700050407070707070707070707
                      0707070703040707050400070707070707070707070707070604070707050707
                      0707070707070707070707070706040706050506070707070707070707070707
                      0701010700050503070707070707070707070707070704000700040707070707
                      0707070206070707070707070305050107060507070707070707070707070707
                      0005050600050506070707070707070707070707040505070405040707070707
                      0707070707070706050501070305070707070707070707070707060505000605
                      0406070707070707070707070707060505000605050607070707070707070707
                      0707030505010004040407070707070707070707070704050507040504070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707020607070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070703030307070707070707070707
                      0707070707070707070707070707070707070707070707070707070603030707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707020607070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070701
                      0307070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707030303030707070707070707070707070707070707070707
                      0707070707070707070707070306060606030707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707030601040202020202020202020202050502
                      0507070707070707070707070707070707070707070303070707070707070305
                      0204040505020205000603030603070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0703050202020202020202020205000307070706020407070707070706010401
                      0101040101000004020202050103070707070205030707070707000502020202
                      0202040007070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707030101050505050502050505040106
                      0707070707070707070707070707070703030600000000010101030707070707
                      0707070703000406070707070707070707070707070306010607070707070707
                      0707070703010502020202050007070707070707070707070707070707070707
                      0707070707070703070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070001000707070707070707070707070707070707070707070707
                      0707070707070707030001060307070707070707070204060703060303030701
                      0507070707070707060206070707070002050101050202020202020401060606
                      0502070707040507070707070707070705040003060004020503070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707030105020205010003030303030001040502050307070707070605050202
                      0202020202020202020202020402050307070707070700050202020205000707
                      0707070707070707010202020202030707070707070707010202040000050205
                      0204070707070707070706000404050100030707070707070001050202020205
                      0400060707070707070707070707070700010405020202050404040100030707
                      0707070707070004050202020400060707070707070707070102020202020107
                      0707070707070707070707000104010007070707070707070707070605020502
                      0205000707070707010203070707070707070702000707070707070707040407
                      0707060200070707070706010505030707070707070501070302030707070707
                      0707070302070707070707000200070707070707000007070707070707070301
                      0402050400030707070707070707070707070707070305050106070707070707
                      0707070707070703040503070707060204010100000603000200070707070000
                      0707050407070707030402040007070601020501070707070707070402020003
                      0004020107070707070701020103070707010107020200070707070604020202
                      0401040402020007070704040202040000020504020202050405050401030707
                      0707000502050402020401010404020202020501030707070704020202010104
                      0502020406070707070703050202000703020205070707070707070707040202
                      0505050202040707070707070707010202050707060402050607070705010707
                      0707070707070602030707070707070707000207070705040707070707070707
                      0404070707070707070104070404070707070707070707010507070707070707
                      0501070707070707000007070707070707000202010001050202050404000000
                      0000000303070707070102030707070707070707070707070707070707020107
                      0707050507070707070707010507070707070707070700020707070305020607
                      0707070707030102010707070707010201040007070701020507070707030205
                      0707070707070706020502070707070502020503070707070502020007000202
                      0402060703020000020100020502050402020607070302020407010200070707
                      0707060200000402050307070702040002030707070602020205030707060205
                      0305010707020402050307070707070102020503070707070102020007070707
                      0701020100060707070701020200070702060707070707070707060207070707
                      0707070707060207070302060707070707070707020107070707070707000407
                      0500070707070707070707050007070707070707040107070707070700000707
                      0707070706020403070707070700000104050202050205020205070707020007
                      0707070707070707070707070707070707010507070102070707070707070705
                      0107070707070707070707020007070505030707070707070707070602000707
                      0707020107070707070707060205070707040204070707070707070601030201
                      0707060204050307070707070103040207000202030206070002030600070705
                      0605010703050507070405030707000007070707070703020607070302050707
                      0002000704070707070302030602040703020407070303070705000305020406
                      0707000200000307070707070707020206070707060201070707070707070504
                      0102060302070707070707070707000507070707070707070706020707010507
                      0707070707070706020307070707070707010407050607070707070707070702
                      0307070707070707050107070707070700000707070707070404070707070707
                      0707070707070707070707070602010700020707070707070707070707070707
                      0707070707000207070504070707070707070705010707070707070707070705
                      0107000203070707070707070707070704050307070102030707070707070707
                      0002000707050202070707070707070707070105070700020707070707070707
                      0707030206010404070307070405070707070707070501070700020707050107
                      0707070707070707070707050107070700020607010200070707070707070307
                      0704020704020307070707070707070707020202030701040707070707070707
                      0707020202070707000203070707070707070607070404000207070707070707
                      0707000207070707070707070706020707010107070707070707070102070707
                      0707070707000207050007070707070707070702070707070707070705010707
                      0707070700000707070707070200070707070707070707070707070707070707
                      0704050704050707070707070707000107070707070707070700020703020607
                      0707070707070705010707070707070707070701050704010707070707070707
                      0707070703020007070204070707070707070707070205070705000507070707
                      0707070707070002060704040707070707070707070707020101040507070707
                      0405070707070707070504070703020703020607070707070707070707070704
                      0407070706020307060200070707070707070707070602030501070707070707
                      0707070706020602060701040707070707070707070700070206070700020007
                      0707070707070707070005060207070707070707070700020707070707070707
                      0700050707050107070707070707070404070707070707070706050704040707
                      0707070707070302070707070707070702000707070707070000070707070703
                      0200070707070707070707070707070707070707070002070501070707070707
                      0707040507070707070707070703020606020607070707070707070201070707
                      0707070707070701020702000707070707070707070707070705010700020204
                      0707070707070707000205070702000707070707070707070707070504070401
                      0707070707070707070703020601020207070707050407070707070707040107
                      0700020703020007070707070707070707070704040707070102070707020007
                      0707070707070707070602000200070707070707070707070005070200070004
                      0707070707070707070707070501070701020007070707070707070707010403
                      0206070707070707070706050707070707070707070405070701040707070707
                      0707070201070707070707070704040700020707070707070707000507070707
                      0707070002030707070707070000070707070707050107070707070707070707
                      0707070707070707070302070200070707070707070702010707070707070707
                      0703020603020607070707070707070006070707070707070707070105070203
                      0707070707070707070707070702010705050104030707070707070706020407
                      0702000707070707070707070707070405070501070707070707070707070102
                      0301020207070707060307070707070707070707070102070702060707070707
                      0707070707030402010707070102070707020007070707070707070707070200
                      0400040607070707070707070301070203070005070707070707070707070707
                      0404070704020007070707070707070707040403020607070707070707070002
                      0707070707070707070500070700050707070707070707050007070707070707
                      0704010703020307070707070707040407070707070707040507070707070707
                      0000070707070707010207070707070707070707070707070707070707060207
                      0500070707070707070002070707070707070707070702000702000707070707
                      0707070707070707070707070707030200070207070707070707070707070707
                      0702000002060707070707070707070707020007070500070707070707070707
                      0707070602060401070707070707070707070404070102020707070707070707
                      0707070707070707070105070702060707070707070707070703020203070707
                      0405070707020007070707070707070707070200010202010707070707070707
                      0707070206070602070707070707070707070707010207070102010707070707
                      0707070707040003020607070707070707070602030707070707070707020307
                      0706020707070707070706020307070707070707070200070705010707070707
                      0707040007070707070707020007070707070707000007070707070706020007
                      0707070707070707070707070707070707010207050107070707070707010507
                      0707070707070707070302060705040707070707070707070707070707070707
                      0707000203070206070707070707070707070707060203010507070707070707
                      0707070706020607070200070707070707070707070707060206050107070707
                      0707070707070404070105020707070707070707070707070707070707010507
                      0702000707070707070707070707030307070707050107070702010707070707
                      0707070707070206000202010707070707070707070706020707060203070707
                      0707070707070707040407070502050707070707070707070702000702000707
                      0707070707070302030707070707070706050707070702060707070707070002
                      0707070707070707000203070701050707070707070704010707070707070002
                      0607070707070707000007070707070707040407070707070707070707070707
                      0707070707020407050507070707070707040507070707070707070707070200
                      0701050707070707070707070707070707070707070705050707020007070707
                      0707070707070707010507000507070707070707070707070102070707020007
                      0707070707070707070707010207050107070707070707070707020107000202
                      0707070707070707070707070707070707040507070401070707070707070707
                      0707070707070707020407070705040707070707070707070703020307050204
                      0707070707070707070700020707070206070707070707070707070704010707
                      0504020707070707070707070302030705000707070707070707070206070707
                      0707070704010707070702060707070707070002070707070707070702010707
                      0706020001010006070704010707070707070504070707070707070700000707
                      0707070707010206070707070707070707070707070707070602060700020707
                      0707070707040407070707070707070707070501070002030707070707070707
                      0707070707070707070302060707040407070707070707070707070705010700
                      0207070707070707070707070405070707020007070707070707070707070705
                      0107050107070707070707070706020007000202030707070707070707070707
                      0707070707020407070504070707070707070707070707070707070705010707
                      0704050707070707070707070706020307040202070707070707070707070005
                      0707070501070707070707070707070702060707050402070707070707070707
                      0002070701040707070707070707070206070707070707070206070707070501
                      0707070707070105070707070707070002070707070302020202020205040204
                      0306060104020203070707070707070700000707070707070707020407070707
                      0707070707070707070707070505070703020007070707070704040707070707
                      0707070707070501070702000707070707070707070707070707070707060203
                      0707010207070707070707070707070002030703020607070707070707070707
                      0501070707020007070707070707070707070002060702010707070707070707
                      0700020307070202010707070707070707070707070707070702010707010207
                      0707070707070707070707070707070302000707070404070707070707070707
                      0703020607040202070707070707070707070404070707010507070707070707
                      0707070302030707020204070707070707070707010507070002070707070707
                      0707070203070707070707010207070707070502050505050401050200060707
                      0707070505070707070702000707070301040202020202050402050707070707
                      0707070700000707070707070707040207070707070707070707070707070700
                      0200070707040203070707070701020707070707070707070707020107070105
                      0707070707070707070707070707070703040207070707020007070707070707
                      0707070504070707050107070707070707070703020007070702000707070707
                      0707070707070405070702010707070707070707070405070707020201070707
                      0707070707070707070707070702010707000207070707070707070707070707
                      0707070602060707070405070707070707070707070302030701020203070707
                      0707070707070401070707000207070707070707070707010507070302000707
                      0707070707070707040407070302000707070707070706020707070707070702
                      0107070707070402010001010404050202020205040405020007070707030207
                      0707070707070203070307070705010707070707070707070000070707070707
                      0707000204060606070306060000010000000602050707070703050507070707
                      0703020007070707070707070703020007070602060707000600010006070707
                      0300010502020407070707040407070707070707070703020007070705050707
                      0707070707070700020607070705010707070707070707070703020407070201
                      0707070707070707070501070707050501070707070707070707070707070707
                      0702010707060206070707070707070707070707070707060206070707040201
                      0707070707070707070602060707020207070707070707070707050607070703
                      0206070707070707070707050107070302070707070707070707070702060707
                      0705020204000003070701020001010000000402030707070707010407070707
                      0707000207030101010104020607070707060207070707070707020707070707
                      0705000707070707070707070000070707070707070707020202020202020202
                      0202050202020202060707070707000201030707070002050707070703000707
                      0704020607070705050402020202050202040706050202010005010707070700
                      0203070707070707070701050707070700020307070707070707070002070707
                      0705010707070707070707070701020607070201070707070707070703020107
                      0707040404070707070707070707070707070707030206070707020407070707
                      0707070707070707070707000203070707040206070707070707070707000203
                      0707020007070707070707070707020607070707050107070707070707070302
                      0607070005070707070707070707070602070707070405000402020205020202
                      0505050505020201070707070707020007070707070703020307070707070602
                      0307070707060507070707070703020707070707070206070707070707070707
                      0000070707070707070707020403060606060102000707070703000203070707
                      0707060202020404050202020201040202020202020202060707070102050603
                      0707070700020402050607070704010707070707050007070707070707070204
                      0707070703020107070707070707070405070707070501070707070707070707
                      0705050707070201070707070707070706020007070701010507070707070707
                      0707070707070707010207070707040507070707070707070707070707070700
                      0507070707000207070707070707070707000207070702000707070707070707
                      0706020707070707010407070707070707070005070707000407070707070707
                      0707070104070707070104070707070306060206070707070707050107070707
                      0703020607070707070707050107070707070602060707070701050707070707
                      0703020307070707070200070707070707070707000007070707070707070702
                      0107070707070302000707070707030206070707070700020601040405000703
                      0202040400000104040102060707070002030707070707070704020507070707
                      0705040707070707010206070707070707000206070707070704040707070707
                      0707070504070707070501070707070707070707060200070707020107070707
                      0707070700020707070700010203070707070707070707070707070704050707
                      0707000203070707070707070707070707070704050707070700020707070707
                      0707070707010507070705000707070707070707070002070707070706020400
                      0603070707070501070707000200000000000101010006020007070707000407
                      0707070707070500070707070707040007070707070002070707070707070704
                      0407070707070302030707070704040707070707070105070707070707020607
                      0707070707070707000007070707070707070702000707070707070204070707
                      0707070200070707070705050707070707070707020207070707070707000206
                      0707070702000707070707070701020607070707070404070707070706020202
                      0505050202020207070707070701020607070707030700020007070707050107
                      0707070707070707040207070707020107070707070707070105070707070303
                      0200070707070707070707070707070702010707070707020107070707070707
                      0707070707070705010707070706020307070707070707070700050707070501
                      0707070707070707070105070707070706020402020202020502020607070706
                      0205020202040404050402020607070707000407070707070707050007070707
                      0707010007070707070504070707070707070302050707070707030203070707
                      0705010707070707070104070707070707020007070707070707070700000707
                      0707070707070702060707070707070504070707070707020007070707070505
                      0707070707070703020203070707070707060206070707030200070707070707
                      0701020707070707070105070707070707050501040501010005050707070707
                      0703020204010105020202020307070707020107070707070707070302040707
                      0703020107070707070707070405070707070707050107070707070707070707
                      0707070602030707070707040407070707070707070707070707070501070707
                      0707020007070707070707070704050707070405070707070707070707040407
                      0707070703020707060001010104020707070700050703060307070707070102
                      0307070707040407070707070707050107070707070704000707070707020007
                      0707070707030502020007070707030207070707070506070707070707040107
                      0707070707020007070707070707070700000707070707070707030200070707
                      0707070505070707070707020107070707070204070707070707070002020007
                      0707070707010203070707070200070707070707070402060707070707000507
                      0707070707040407070707070704040707070707070705020502050500000402
                      0707070707050407070707070707070602000707070302000707070707070703
                      0201070707070707040507070707070707070707070707010207070707070701
                      0507070707070707070707070707070200070707070705010607070707070707
                      0705040707070402020501010101040202020407070707070702070707070707
                      0701040707070700050707070707070707070404070707070705000707070707
                      0707050107070707070701040707070703020307070707070705050704040707
                      0707030203070707070203070707070700020107070707070702060707070707
                      0707070700000707070707070707030201070707070707010207070707070702
                      0407070707030201070707070707070105020407070707070704050707070703
                      0201070707070707060202000707070707000507070707070701020707070707
                      0705010707070707070705040707070707070405070707070702020007070707
                      0707070502070707070602020106070707070605020607070707070701020307
                      0707070707070707070707050407070707070700020007070707070707070707
                      0707060206070707070705020202050505050502020200070707040106040502
                      0205040006020007070707070702030707070707070401070707070605070707
                      0707070707070200070707070705010707070707070705010707070707070404
                      0707070701040707070707030202030700020707070703020007070703020307
                      0707070602020203070707070705000707070707070707070000070707070707
                      0707030205070707070707000200070707070701020707070707020007070707
                      0707070504010207070707070704040707070703020107070707070705020505
                      0707070707010203070707070701020307070707030200070707070707070402
                      0707070707070205070707070702020202040603030004020107070707060205
                      0202020405020202020607070707070703020403030707030307070707070602
                      0007070707070707040407070703000707070707070701020707070707070201
                      0600000401000006000200070707040107070707070707070702060707070707
                      0702070707070707070206070707070005070707070707070706020307070707
                      0705010707070707070002020707070707070405070707070504070707070704
                      0202030707020607070707050107070706020707070707040507050007070707
                      0702000707070707070707070000070707070707070707020407070707070700
                      0204070707070701050707070703020007070707070703020600020707070707
                      0704040707070703020007070707070002030102070707070700020707070707
                      0700020707070707060206070707070707070102070707070703020107070707
                      0702010004020202020202020607070707010207060105050401060405070707
                      0707070707050202020202020202020501060102070707070707070706020307
                      0703030707070707070701050707070707070501070707070707070706020607
                      0707010507070707070707070302070707070707070203070707070703020307
                      0707070605070707070707070704040707070707070200070707070707020402
                      0707070707070105070707070401070707070602030500070705000707070702
                      0007070700020707070706020607040407070707070206070707070707070707
                      0000070707070707070707020507070707070704020207070707070102070707
                      0703020007070707070700020303020107070707070404070707070702000707
                      0707070505070402000707070706020307070707070002070707070700020707
                      0707070707070105070707070706020007070707070201070707060303030602
                      0607070707050507070707070707070404070707070707070701020101050101
                      0101010105020201070707070707070707050206070707070707070707070504
                      0707070707070501070707070707070700020707070701050707070707070707
                      0005070707070707070207070707070703050707070707000407070707070707
                      0705010707070707070206070707070701020705000707070707040407070707
                      0506070707070404070401070705010707070302060707070305070707070102
                      0007060507070707070200070707070707070707000007070707070707070702
                      0407070707070602050201070707070102070707070702000707070707070105
                      0707040507070707070204070707070302000707070700020003020201070707
                      0706020307070707070002070707070705020707070707070707010507070707
                      0700020307070707070200070707070707070102030707070704050707070707
                      0707070201070707070707070701020707070707070707070701020607070707
                      0707070707010202020205050202020202020204070707070707050407070707
                      0707070700020707070700050707070707070707010107070707070707020307
                      0707070700050707070707000507070707070707030203070707070703020307
                      0707070702000701040707070707050107070707050007070700020607040407
                      0705040707070302030707070602070707070202040703020707070703020607
                      0707070707070707000007070707070707070702040707070707010203040407
                      0707070002070707070702040707070707070404070701020307070707050107
                      0707070702000707070704050700020205070707070602030707070707010203
                      0707070702040707070707070707010507070707070102070707070707020607
                      0707070707070405070707070704050707070707070707020007070707070707
                      0704010707070707070707070700050707070707070707070706020706000101
                      0101010106000206070707070707010507070707070707070404070707070005
                      0707070707070707050107070707070707050607070707070004070707070706
                      0507070707070707000507070707070706020707070707010207070005070707
                      0707020007070707050107070705040707010507070504070707000207070707
                      0002070707000201050707020007070706020707070707070707070700000707
                      0707070707070702040707070707050407000207070707060207070707070204
                      0707070707030201070702020607070707020107070707030200070707070201
                      0701050402070707070602070707070707040207070707030200070707070707
                      0707050507070707070405070707070707020007070707070707010507070707
                      0704040707070707070703020607070707070707070401070707070707070707
                      0705010707070707070707070706020707070707070707070706020707070707
                      0707000507070707070707070501070707070002070707070707070705060707
                      0707070707050607070707070005070707070706020707070707070704040707
                      0707070706020707070700020203070302070707070702070707070705010707
                      0002030707010507070201070707050507070707030203070704040002070702
                      0607070700020707070707070707070700000707070707070707070201070707
                      0703020107010206070707000207070707070204070707070701020607010202
                      0107070703020107070707070200070707060206070404000203070707060203
                      0707070707050407070707060206070707070707070702040707070707050107
                      0707070707020007070707070707040407070707070404070707070707070602
                      0707070707070707070101070707070707070707060203070707070707070707
                      0707020607070707070707070701050707070707070700050707070707070707
                      0200030307070602070707070707070302030707070707070705060707070707
                      0401070707070703020707070707070702060707070707070002070707060200
                      0504070702000707070005070707070701050707010507070700020307050407
                      0707020607070707070200070702000702070702000707070501070707070707
                      0707070700000707070707070707070504070707070602030702020107070701
                      0207070707070205070707070705050707050502010707070602060707070703
                      0200070707040203070504060206070707010507070707070704040707070704
                      0207070707070707070702010707070707020007070707070702000707070707
                      0707050107070707070404070707070707070404070707070707070707040107
                      0707070707070707010507070707070707070707070702000707070707070707
                      0704010707070707070701050707070707070700020307070707060207070707
                      0707070104070707070707070705000707070707050607070707070302060707
                      0707070605070707070707070602070707040407000507070101070707010507
                      0707070706020307040407070707020007020107070102030707070707040407
                      0002070702070302010707070200070707070707070707070000070707070707
                      0707070102070707070102070002020407070701020707070707050507070707
                      0302040706020601050707070002070707070707020107070705040707020107
                      0200070707040507070707070704050707070705040707070707070707070200
                      0707070706020607070707070702060707070707070705010707070707040407
                      0707070707070401070707070707070707040407070707070707070705000707
                      0707070707070707070704010707070707070707070506070707070707070005
                      0707070707070700020707070707060207070707070707040407070707070707
                      0704010707070707050607070707070705000707070707000507070707070707
                      0702060707020607030203070102070707050107070707070702000705010707
                      0707040504020107030201070707070707000207010407070506040204070700
                      0203070707070707070707070000070707070707070707000206070707040407
                      0105040207070705050707070707010507070707000206070102030002030707
                      0002070707070707050407070002040703020106020007070302010707070707
                      0704050707070602060707070707070707070200070707070002070707070707
                      0702000707070707070302030707070707040407070707070707040007070707
                      0707070707040107070707070707070602070707070707070707070707070404
                      0707070707070707030203070707070707070004070707070707070104070707
                      0707060207070707070707050007070707070707070104070707070302030707
                      0707070704040707070707040107070707070707070501070302030707050107
                      0602060703020607070707070704050602060707070700020204020004020307
                      0707070707070204020007070505020205070705040707070707070707070707
                      0000070707070707070707070201070707040407040100020707070201070707
                      0707000206070707010207070201070702010707040507070707070704020707
                      0102020003020103020107070002060707070707070002070707010207070707
                      0707070707070501070707070105070707070707070200070707070707060207
                      0707070707040407070707070707020707070707070707070704010707070707
                      0707070405070707070707070707070707070005070707070707070700050707
                      0707070707070004070707070707070500070707070703020707070707070702
                      0607070707070707070005070707070602070707070707070602070707070702
                      0307070707070707070105070405070707050507000205070405070707070707
                      0703020202070707070707010607010202000707070707070707060202060707
                      0402040602070102030707070707070707070707000007070707070707070707
                      0505070707020207050103020307000206070707070706020007070302050706
                      0200070705050707020107070707070701020307050502050402030702040707
                      0505070707070707070602030707050407070707070707070707010407070707
                      0401070707070707070200070707070707010507070707070701040707070707
                      0703020707070707070707070704010707070707070707020007070707070707
                      0707070707070605070707070707070704010707070707070707000507070707
                      0707060203070707070707020707070707070602070707070707070707000507
                      0707070404070707070707070702000707070602070707070707070707000507
                      0404070707010203020202040201070707070707070704020007070707070707
                      0707070606070707070707070707070105070707060107030202020107070707
                      0707070707070707000007070707070707070707000206070702020102000702
                      0107050507070707070707020407070602020701020707070602000102000707
                      0707070703020106020103020202070705010707020107070707070707070501
                      0706020607070707070707070707010507070707020007070707070703020007
                      0707070707040107070707070704040707070707070005070707070707070707
                      0704010707070707070700020707070707070707070707070707030207070707
                      0707070705000707070707070707040107070707070701050707070707070702
                      0707070707070404070707070707070707030203070707050007070707070707
                      0701020707070104070707070707070707060200050107070703020202070005
                      0503070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070405010707070707070707070707070700000707
                      0707070707070707070505070602040202000704020102060707070707070701
                      0507070402020105040707070704020205070707070707070701020502030706
                      0501070705010701020607070707070707070102030504070707070707070707
                      0707000203070707020607070707070707020007070707070702000707070707
                      0700050707070707070401070707070707070707070401070707070707070501
                      0707070707070707070707070707030203070707070707030203070707070707
                      0707050007070707070705010707070707070702060707070707050107070707
                      0707070707070206070701020707070707070707070305020101020607070707
                      0707070707070402020007070707050200070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707060201
                      0105030405030706020204070707070707070706020101020004020206070707
                      0707000003070707070707070707050201070707070707070405060204070707
                      0707070707070302020203070707070707070707070707020007070302030707
                      0707070707020607070707070602030707070707070602070707070707050107
                      0707070707070707070405070707070707030203070707070707070707070707
                      0707070206070707070707060207070707070707070705000707070707030206
                      0707070707070705000707070703020307070707070707070707020007060200
                      0707070707070707070703040505040307070707070707070707030504030707
                      0707030607070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070102020407070707070700000707
                      0707070707070707010202040703000607070707070707070707070707070707
                      0707030307070707070707070602020503070707070707070707070605060707
                      0707070707070707070707010200010505070707070707070702000707070707
                      0105070707070707070702030707070707050007070707070707070707030207
                      0707070707010507070707070707070707070707070707040407070707070704
                      0107070707070707070705000707070707040507070707070707070501070707
                      0700050707070707070707070707010204020407070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070706000307070707070707070707070707070707070707060607
                      0707070707070707070707070707070707070707070707070707070707070707
                      0704010707070707070707070707070707070707070707070707070707070707
                      0202050406070707070707070705010707070707050407070707070707070504
                      0707070707020307070707070707070707070207070707070702000707070707
                      0707070707070707070707010407070707070705000707070707070707070500
                      0707070706020107070707070707070105070707070401070707070707070707
                      0707030405000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070307070707070707070707
                      0704040707070703020607070707070707070602060707070602070707070707
                      0707070707070500070707070102070707070707070707070707070707070700
                      0207070707070602070707070707070707070500070707070505070707070707
                      0707070602070707030203070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070005070707070105070707
                      0707070707070704040707070005070707070707070707070707000507070707
                      0504070707070707070707070707070707070703020307070707010407070707
                      0707070707070400070707040203070707070707070707030203070704040707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070602070707070201070707070707070707070602000707
                      0404070707070707070707070707060207070706020607070707070707070707
                      0707070707070707050107070707050107070707070707070707000203030402
                      0607070707070707070707070404070102060707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070500
                      0707060203070707070707070707070706020101020607070707070707070707
                      0707030201070704040707070707070707070707070707070707070700020707
                      0706020307070707070707070707070402020503070707070707070707070707
                      0002020206070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070105070704040707070707070707
                      0707070707000202040707070707070707070707070707010507060206070707
                      0707070707070707070707070707070707020107070105070707070707070707
                      0707070707070707070707070707070707070707070001070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070700000707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070302050502000707070707070707070707070707070307070707
                      0707070707070707070707030205020507070707070707070707070707070707
                      0707070707010203060201070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070700000707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070300060307
                      0707070707070707070707070707070707070707070707070707070707070707
                      0002010307070707070707070707070707070707070707070707050202050307
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070000070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707060406070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070000070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0000070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707000007070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707000007070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070700000707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      0707070707070707070707070707070707070707070707070707070707070707
                      070707070707070707070707070707070000}
                    Stretch = True
                  end
                  object btnEditarImagem: TSpeedButton
                    Left = 378
                    Top = 95
                    Width = 111
                    Height = 34
                    Caption = 'Editar IMagem'
                    OnClick = btnEditarImagemClick
                  end
                end
              end
            end
            object panNumeroConsulta: TPanel
              Left = 8
              Top = 11
              Width = 194
              Height = 27
              Alignment = taLeftJustify
              Caption = 'Consulta n.� 00000'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
            object Panel3: TPanel
              Left = 206
              Top = 11
              Width = 172
              Height = 27
              Alignment = taLeftJustify
              Caption = 'Matr�cula: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              object lbMatricula: TLabel
                Left = 71
                Top = 8
                Width = 36
                Height = 13
                Caption = '00000'
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Pesquisa Consultas Odontol�gicas'
          ImageIndex = 1
          OnShow = TabSheet2Show
          object Panel5: TPanel
            Left = 5
            Top = 51
            Width = 540
            Height = 154
            TabOrder = 0
            object dbgConsultasPaciente: TDBGrid
              Left = 8
              Top = 11
              Width = 522
              Height = 135
              DataSource = dsAgenda
              Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = dbgConsultasPacienteCellClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'cod_id_consulta'
                  Title.Alignment = taCenter
                  Title.Caption = 'N.�'
                  Width = 25
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'cod_matricula'
                  Title.Alignment = taCenter
                  Title.Caption = 'Matr�cula'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nom_nome'
                  Title.Caption = 'Paciente'
                  Width = 200
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vDataConsulta'
                  Title.Alignment = taCenter
                  Title.Caption = 'Data'
                  Width = 65
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vHoraConsulta'
                  Title.Alignment = taCenter
                  Title.Caption = 'Hor�rio'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vStatusConsulta'
                  Title.Caption = 'Situa��o'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object Panel2: TPanel
            Left = 5
            Top = 5
            Width = 540
            Height = 45
            TabOrder = 1
            object Label8: TLabel
              Left = 14
              Top = 18
              Width = 59
              Height = 13
              Caption = 'Matr�cula:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object SpeedButton6: TSpeedButton
              Left = 160
              Top = 12
              Width = 28
              Height = 24
              Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
              Glyph.Data = {
                66010000424D6601000000000000760000002800000014000000140000000100
                040000000000F0000000120B0000120B00001000000010000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                777777770000777777777777777700770000777777777777777C440700007777
                7777777777C4C40700007777777777777C4C4077000077777777777784C40777
                0000777777788888F740777700007777770000007807777700007777707EFEE6
                007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
                8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
                0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
                7777777700A077777777777777777777FFA077777777777777777777FFFF7777
                7777777777777777FF81}
              ParentShowHint = False
              ShowHint = True
              OnClick = SpeedButton6Click
            end
            object lbNomePaciente: TLabel
              Left = 192
              Top = 15
              Width = 151
              Height = 18
              Caption = 'NOME DO PACIENTE'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object mskMatricula: TMaskEdit
              Left = 74
              Top = 14
              Width = 83
              Height = 21
              EditMask = '!99999999;1;_'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              MaxLength = 8
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Text = '        '
            end
          end
          object Panel6: TPanel
            Left = 6
            Top = 207
            Width = 540
            Height = 126
            TabOrder = 2
            object dbgTratamentosConsultasP: TDBGrid
              Left = 8
              Top = 11
              Width = 522
              Height = 108
              DataSource = dsEncaminhamentosConsultas
              Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'cod_id_consulta'
                  Title.Alignment = taCenter
                  Title.Caption = 'N.�'
                  Width = 25
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'dsc_encaminhamento'
                  Title.Caption = 'Encaminhamento'
                  Width = 350
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'log_data'
                  Title.Caption = 'Data/Hora'
                  Width = 115
                  Visible = True
                end>
            end
          end
          object Panel7: TPanel
            Left = 6
            Top = 336
            Width = 540
            Height = 126
            TabOrder = 3
            object dbgEncaminhamentosConsultasP: TDBGrid
              Left = 8
              Top = 11
              Width = 522
              Height = 108
              DataSource = dsTratamentosConsultas
              Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'cod_id_consulta'
                  Title.Alignment = taCenter
                  Title.Caption = 'N.�'
                  Width = 25
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'dsc_tratamento'
                  Title.Caption = 'Tratamento'
                  Width = 300
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'num_dente'
                  Title.Alignment = taCenter
                  Title.Caption = 'Dente'
                  Width = 45
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object dsAgenda: TDataSource
    DataSet = dmDeca.cdsSel_Consulta
    Left = 529
    Top = 64
  end
  object dsTratamentosConsultas: TDataSource
    DataSet = dmDeca.cdsSel_TratamentosConsultas
    Left = 530
    Top = 96
  end
  object dsEncaminhamentosConsultas: TDataSource
    DataSet = dmDeca.cdsSel_EncaminhamentosConsultas
    Left = 530
    Top = 130
  end
  object dsEncaminhamentos: TDataSource
    DataSet = dmDeca.cdsSel_EncaminhamentosConsultas
    Left = 532
    Top = 165
  end
end
