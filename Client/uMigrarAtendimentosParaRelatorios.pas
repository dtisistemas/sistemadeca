unit uMigrarAtendimentosParaRelatorios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, Db, Buttons, ComCtrls;

type
  TfrmMigrarAtendimentosParaRelatorios = class(TForm)
    GroupBox1: TGroupBox;
    ProgressBar1: TProgressBar;
    btnINICIAR_MIGRACAO: TSpeedButton;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    dsSel_Cadastro_Relatorio: TDataSource;
    procedure btnINICIAR_MIGRACAOClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMigrarAtendimentosParaRelatorios: TfrmMigrarAtendimentosParaRelatorios;

implementation

uses udmTriagemDeca, uDM;

{$R *.DFM}

procedure TfrmMigrarAtendimentosParaRelatorios.btnINICIAR_MIGRACAOClick(
  Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsSel_Cadastro_RegAtendimento do
    begin
      Close;
      Params.ParamByName ('@pe_Cod_inscricao').Value := Null;
      Open;

      ProgressBar1.Min := 0;
      ProgressBar1.Max := dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.RecordCount;

      while not (dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.eof) do
      begin
        //Insere os dados em Cadastro_Relatorios
        with dmTriagemDeca.cdsIns_Cadastro_Relatorio do
        begin
          Close;
          Params.ParamByName ('@dta_digitacao').Value       := dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.FieldByName ('dta_regAtendimento').Value;
          Params.ParamByName ('@Cod_inscricao').Value       := dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.FieldByName ('cod_inscricao').Value;
          Params.ParamByName ('@tipo_relatorio').Value      := 4;
          Params.ParamByName ('@cod_responsavel').Value     := dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.FieldByName ('cod_respAtendimento').Value;
          Params.ParamByName ('@txt_relatorio').Value       := dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.FieldByName ('dsc_regAtendimento').Value;
          Params.ParamByName ('@dsc_entrevistador').Value   := dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.FieldByName ('nom_usuario').Value;
          Params.ParamByName ('@pe_cod_usuario_deca').Value := Null;
          Execute;
        end;

        with dmTriagemDeca.cdsSel_Cadastro_Relatorio do
        begin
          Close;
          Params.ParamByName ('@Cod_inscricao').Value        := Null;
          Params.ParamByName ('@pe_cod_usuario_deca').Value  := Null;
          Params.ParamByName ('@pe_tipo_relatorio').Value    := Null;
          Open;
          DBGrid1.Refresh;
        end;
        dmTriagemDeca.cdsSel_Cadastro_RegAtendimento.Next;
        ProgressBar1.Position := ProgressBar1.Position + 1;
      end;
    end;
  except end;
end;

end.
