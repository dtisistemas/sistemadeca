unit uAditamentoContratoAprendiz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, ComObj;

type
  TfrmAditamentoContratoAprendiz = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Label1: TLabel;
    edNome: TEdit;
    Label4: TLabel;
    edEndereco: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    edComplemento: TEdit;
    edbairro: TEdit;
    Label3: TLabel;
    Label5: TLabel;
    edCidade: TEdit;
    edUF: TEdit;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    cbAreaEmpresa: TComboBox;
    edCtpsNum: TEdit;
    Label11: TLabel;
    Label10: TLabel;
    edCtpsSerie: TEdit;
    btnGerarContrato: TSpeedButton;
    edSeqAditamento: TEdit;
    UpDown1: TUpDown;
    Label12: TLabel;
    edEnderecoEmpresa: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    edBairroEmpresa: TEdit;
    edCidadeEmpresa: TEdit;
    edUfEmpresa: TEdit;
    Label16: TLabel;
    OpenDialog1: TOpenDialog;
    Label17: TLabel;
    edNomeEmpresa: TEdit;
    Label18: TLabel;
    edNumCnpj: TEdit;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnGerarContratoClick(Sender: TObject);
    procedure cbAreaEmpresaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAditamentoContratoAprendiz: TfrmAditamentoContratoAprendiz;
  vOK : Boolean;

implementation

uses uFichaPesquisa, uDM, uPrincipal;

{$R *.DFM}

procedure TfrmAditamentoContratoAprendiz.btnPesquisarClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        edNome.Text          := FieldByName('nom_nome').AsString;
        edEndereco.Text      := FieldByName('nom_endereco').AsString;
        edComplemento.Text   := FieldByName('nom_complemento').AsString;
        edBairro.Text        := FieldByName('nom_bairro').AsString;
        edCtpsNum.Text       := FieldByName('ctps_num').AsString;
        edCtpsSerie.Text     := FieldByName('ctps_serie').AsString;


        edCidade.Text        := 'S�O JOS� DOS CAMPOS';
        edUF.Text            := 'Estado de S�o Paulo';
        edCidadeEmpresa.Text := 'S�O JOS� DOS CAMPOS';
        edUfEmpresa.Text     := 'Estado de S�o Paulo';
      end;

    except end;
  end;
end;

procedure TfrmAditamentoContratoAprendiz.btnGerarContratoClick(
  Sender: TObject);
var
  winword, docs, doc : Variant;
  arquivo : String;
begin

    case cbAreaEmpresa.ItemIndex of
    0: begin   //EMBRAER
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\embraer.docx');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      //Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      //Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      //Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      //Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;
      end;

    1: begin  //ELEB
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\eleb.docx');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      //Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      //Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      //Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      //Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;

    2: begin  //AREAS ADMINISTRATIVAS COM 6 HORAS
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\areadm6h.docx');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA2%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;

    3: begin  //AREAS ADMINISTRATIVAS COM 4 HORAS
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\areadm4h.docx');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA2%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;

    4: begin  //APRENDIZAGEM INDUSTRIAL COM 6 HORAS
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\apind6h.docx');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA2%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;

    5: begin  //APRENDIZAGEM INDUSTRIAL COM 4 HORAS
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\apind4h.docx');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA2%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;


    6,8,10,12: begin  //APRENDIZAGEM JOHNSON&JOHNSON - 3 HORAS
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\johnson4hs.doc');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA2%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;

    7,9,11,13: begin  //APRENDIZAGEM JOHNSON&JOHNSON - 8 HORAS
      arquivo := ( ExtractFilePath(Application.Exename) + '\Contratos\johnson8hs.doc');
      //Cria o objetivo principal do controle (Word)
      winword := CreateOleObject('Word.Application');
      //Mostra o Word
      winword.Visible := True;
      //Pega uma interface para o objeto que manipula os documentos
      Docs := Winword.Documents;
      //Abre um documento
      Doc := Docs.Open(arquivo);
      //Substitui os textos via par�metros
      Doc.Content.Find.Execute( FindText := '%NUMSEQADIT%', replacewith := Trim(edSeqAditamento.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := Trim(edNome.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOA%', replacewith := Trim(edEndereco.Text) + ' - ' + Trim(edComplemento.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := Trim(edBairro.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSNUM%', replacewith := Trim(edCtpsNum.Text) );
      Doc.Content.Find.Execute( FindText := '%CTPSSERIE%', replacewith := Trim(edCtpsSerie.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%EMPRESA2%', replacewith := Trim(edNomeEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%CNPJ%', replacewith := Trim(edNumCnpj.Text) );
      Doc.Content.Find.Execute( FindText := '%ENDERECOEMPRESA%', replacewith := Trim(edEnderecoEmpresa.Text) );
      Doc.Content.Find.Execute( FindText := '%BAIRROEMPRESA%', replacewith := Trim(edBairroEmpresa.Text) );

      //Imprime
      //Doc.PrintOut(false);

      //Fecha o Word
      //winword.ActiveDocument.Close (SaveChanges := 0);

      //winword.Quit;

      end;


    end; //case...

end;

procedure TfrmAditamentoContratoAprendiz.cbAreaEmpresaClick(
  Sender: TObject);
begin

  case cbAreaEmpresa.ItemIndex of

    0: begin //EMBRAER
      edNomeEmpresa.Text     := 'EMBRAER S.A.';
      edEnderecoEmpresa.Text := 'AVENIDA BRIGADEIRO FARIA LIMA N.� 2170';
      edBairroEmpresa.Text   := 'BAIRRO PUTIM';
      edNumCnpj.Text         := '07.689.002/0001-89';
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';

    end;

    1: begin //ELEB
      edNomeEmpresa.Text     := 'ELEB EQUIPAMENTOS LTDA.';
      edEnderecoEmpresa.Text := 'AVENIDA ITABAIANA N.� 40';
      edBairroEmpresa.Text   := 'BAIRRO 31 DE MAR�O';
      edNumCnpj.Text         := '55.763.775/0001-00';
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';

    end;

    2,3,4,5: begin
      edNomeEmpresa.Clear;
      edEnderecoEmpresa.Clear;
      edBairroEmpresa.Clear;
      edNumCnpj.Clear;
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';
    end;

    6,7: begin
      edNomeEmpresa.Text     := 'JANSSEN CILAG FARMAC�UTICA LTDA.';
      edEnderecoEmpresa.Text := 'RODOVIA PRESIDENTE DUTRA, KM 154';
      edBairroEmpresa.Text   := 'JARDIM DAS IND�STRIAS';
      edNumCnpj.Text         := '51.780.468/0002-68';
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';
    end;

    8,9: begin
      edNomeEmpresa.Text     := 'JOHNSON & JOHNSON INDUSTRIAL LTDA.';
      edEnderecoEmpresa.Text := 'RODOVIA PRESIDENTE DUTRA, KM 154';
      edBairroEmpresa.Text   := 'JARDIM DAS IND�STRIAS';
      edNumCnpj.Text         := '59.748.988/0001-14';
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';
    end;

    10,11: begin
      edNomeEmpresa.Text     := 'JOHNSON & JOHNSON DO BRASIL INDUSTRIA E COMERCIO DE PRODUTOS PARA SAUDE LTDA.';
      edEnderecoEmpresa.Text := 'RODOVIA PRESIDENTE DUTRA, KM 154';
      edBairroEmpresa.Text   := 'JARDIM DAS IND�STRIAS';
      edNumCnpj.Text         := '54.516.661/0037-04';
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';
    end;

    12,13: begin
      edNomeEmpresa.Text     := 'JOHNSON & JOHNSON DO BRASIL INDUSTRIA E COMERCIO DE PRODUTOS PARA SAUDE LTDA.';
      edEnderecoEmpresa.Text := 'RODOVIA PRESIDENTE DUTRA, KM 154';
      edBairroEmpresa.Text   := 'JARDIM DAS IND�STRIAS';
      edNumCnpj.Text         := '54.516.661/0002-84';
      edCidadeEmpresa.Text   := 'S�o Jos� dos Campos';
      edUfEmpresa.Text       := 'Estado de S�o Paulo';
    end;


  end;

end;

procedure TfrmAditamentoContratoAprendiz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
