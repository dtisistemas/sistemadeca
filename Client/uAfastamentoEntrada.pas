unit uAfastamentoEntrada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls, Grids, DBGrids;

type
  TfrmAfastamentoEntrada = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtData: TMaskEdit;
    txtNomeResponsavel: TEdit;
    txtTipoDocumento: TEdit;
    txtNumDocumento: TEdit;
    Label6: TLabel;
    meMotivoAfastamento: TMemo;
    Label7: TLabel;
    edPeriodoEscolar: TEdit;
    Label8: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure AtualizaCamposBotoes (Modo: String);
    procedure meMotivoAfastamentoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAfastamentoEntrada: TfrmAfastamentoEntrada;
  
implementation

uses uFichaPesquisa, uDM, uUtil, uPrincipal, uDesligamento, rAfastamento;

{$R *.DFM}

procedure TfrmAfastamentoEntrada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmAfastamentoEntrada.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmAfastamentoEntrada.Close;
  end;

end;

procedure TfrmAfastamentoEntrada.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;
          with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_ind_responsavel').Value := 1;
            Open;

            if dmDeca.cdsSel_Cadastro_Familia_Responsavel.Eof then
            begin
              ShowMessage('Aten��o' + #13+#10+#13+#10 +
                          'N�o existe Respons�vel cadastrado para a Crian�a/Adolescente' + #13+#10 +
                          'Favor verificar e tentar novamente... ');

              AtualizaCamposBotoes('Padr�o');
            end
            else
            begin
              txtNomeResponsavel.Text := FieldByName('nom_familiar').Value;
              txtTipoDocumento.Text := FieldByName('ind_tipo_documento').Value;
              txtNumDocumento.Text := FieldByName('num_documento').Value;
            end;
          end;
      end;
    except end;
  end;

end;

procedure TfrmAfastamentoEntrada.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmAfastamentoEntrada.btnSairClick(Sender: TObject);
begin
  mmINTEGRACAO := False;
  frmAfastamentoEntrada.Close;
end;

procedure TfrmAfastamentoEntrada.AtualizaCamposBotoes(Modo : String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtNomeResponsavel.Clear;
    txtTipoDocumento.Clear;
    txtNumDocumento.Clear;
    edPeriodoEscolar.Clear;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtNomeResponsavel.Enabled := False;
    txtTipoDocumento.Enabled := False;
    txtNumDocumento.Enabled := False;
    meMotivoAfastamento.Lines.Clear;
    meMotivoAfastamento.Enabled := False;
    edPeriodoEscolar.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtData.Text := DateToSTr(Date());
    txtNomeResponsavel.Clear;
    txtTipoDocumento.Clear;
    txtNumDocumento.Clear;
    meMotivoAfastamento.Enabled := True;
    
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    edPeriodoEscolar.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;

    txtMatricula.SetFocus;
  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;

    btnImprimir.SetFocus;
  end
end;

procedure TfrmAfastamentoEntrada.btnSalvarClick(Sender: TObject);
begin

  if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      if StatusCadastro(txtMatricula.Text) = 'Ativo' then
      begin
        if (Length(meMotivoAfastamento.Text)>0) then
        begin
          with dmDeca.cdsInc_Cadastro_Historico do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
            Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
            Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
            Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 2;  //C�digo para AFASTAMENTO - Encaminhamento
            Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Afastamento - Encaminhamento';
            Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Crian�a/Adolescente colocada(o) em Afastamento nesta data';
            Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
            Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(meMotivoAfastamento.Text);
            Execute;

            //Atualizar STATUS para "4" Afastado
            with dmDeca.cdsAlt_Cadastro_Status do
            begin
                Close;
                Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Params.ParamByName('@pe_ind_status').Value := 4;
                Execute;
            end;

            {
            //Atualiza o Per�odo no Cadastro de acordo com regras do Acomp Escola de Mar�o 2014
            try
              with dmDeca.cdsAlt_PeriodoCadastro do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Params.ParamByName ('@pe_dsc_periodo').Value   := '<ND>';
                Execute;
              end;
            except
              Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                      'Um erro ocorreu ao tentar alterar o PER�ODO NA FICHA DE CADASTRO.' + #13+#10 +
                                      'Favor tentar novamente ou reportar ao Administrador do Sistema.',
                                      '[Sistema Deca] - Altera��o de PER�ODO CADASTRAL',
                                      MB_OK + MB_ICONINFORMATION);
            end;
            }

            //Bloqueia o cart�o de passe escolar do aluno atual
            try
              with dmDeca.cdsAlt_CartaoPasse do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_num_cartao_passe').Value := dmDeca.cdsSel_Cadastro.FieldByName ('num_cartao_passe').Value;
                Params.ParamByName ('@pe_passe_bloqueado').Value  := 1;
                Execute;
              end;
            except
              Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                      'Um erro ocorreu ao tentar BLOQUEAR O CART�O DE PASSE.' + #13+#10 +
                                      'Favor tentar novamente ou reportar ao Administrador do Sistema.',
                                      '[Sistema Deca] - Bloqueio de Passe Escolar',
                                      MB_OK + MB_ICONINFORMATION);
            end;
            
          end;
          AtualizaCamposBotoes('Salvar');
        end
      else
      begin
        ShowMessage ('O campo MOTIVO � de preenchimento OBRIGAT�RIO...!');
      end;

      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'AFe_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                        Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtData.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Afastamento - Encaminhamento');//Descricao do Tipo de Historico
      Writeln (log_file, ' ');
      Writeln (log_file, 'Cran�a/Adolescente colocada(o) em Afastamento nesta data');
      Writeln (log_file, ' ');
      Writeln (log_file, Trim(meMotivoAfastamento.Text));
      Writeln (log_file, ' ');
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmAfastamentoEntrada.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;

        with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName ('@pe_ind_responsavel').Value := 1;
          Open;

          if dmDeca.cdsSel_Cadastro_Familia_Responsavel.Eof then
          begin
            ShowMessage('Aten��o' + #13+#10+#13+#10 +
                        'N�o existe Respons�vel cadastrado para a Crian�a/Adolescente' + #13+#10 +
                        'Favor verificar e tentar novamente... ');

            AtualizaCamposBotoes('Padr�o');
          end
          else
          begin
            txtNomeResponsavel.Text := FieldByName('nom_familiar').Value;
            //txtTipoDocumento.Text := FieldByName('ind_tipo_documento').Value;
            //txtNumDocumento.Text := FieldByName('num_documento').Value;
            txtNumDocumento.Text := FieldByName('num_cpf_fam').Value;
          end;
        end;

      end;

      //Recuperar o �ltimo bimestre do aluno
      try
        with dmDeca.cdsSel_UltimoBimestreAluno do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
          Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(Copy(DateToStr(Date),7,4));
          Open;

          edPeriodoEscolar.Text := dmDeca.cdsSel_UltimoBimestreAluno.FieldByName ('vPeriodoEscolar').Value;

        end;
      except
      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar consultar dados '+#13+#10+#13+#10+
                             'do �ltimo bimestre letivo do aluno. Favor consultar novamente...',
                             '[Sistema DECA] - Erro consulta bimestre.',
                             MB_OK + MB_ICONERROR);
      end;





    except end;
  end;
end;

procedure TfrmAfastamentoEntrada.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmAfastamentoEntrada.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmAfastamentoEntrada.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
  if (mmINTEGRACAO = False) then
    AtualizaCamposBotoes('Padrao')
  else if (mmINTEGRACAO = True) then
  begin
    AtualizaCamposBotoes('Novo');
    txtMatricula.Text := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
    lbNome.Caption    := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
    txtData.Text := DateToStr(Date());
    meMotivoAfastamento.SetFocus;

    //Pesquisar o nome do respons�vel pelo aluno
    with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
      Params.ParamByName ('@pe_ind_responsavel').Value := 1;
      Open;

      if dmDeca.cdsSel_Cadastro_Familia_Responsavel.Eof then
      begin
        ShowMessage('Aten��o' + #13+#10+#13+#10 +
                    'N�o existe Respons�vel cadastrado para a Crian�a/Adolescente' + #13+#10 +
                    'Favor verificar e tentar novamente... ');
        AtualizaCamposBotoes('Padr�o');
      end
      else
      begin
        txtNomeResponsavel.Text := FieldByName('nom_familiar').Value;
        txtTipoDocumento.Text := FieldByName('ind_tipo_documento').Value;
        txtNumDocumento.Text := FieldByName('num_documento').Value;
      end;
    end;

    //Recuperar o �ltimo bimestre do aluno
    try
      with dmDeca.cdsSel_UltimoBimestreAluno do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
        Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(Copy(DateToStr(Date),7,4));
        Open;

        edPeriodoEscolar.Text := dmDeca.cdsSel_UltimoBimestreAluno.FieldByName ('vPeriodoEscolar').Value;

      end;
    except
    Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar consultar dados '+#13+#10+#13+#10+
                           'do �ltimo bimestre letivo do aluno. Favor consultar novamente...',
                           '[Sistema DECA] - Erro consulta bimestre.',
                           MB_OK + MB_ICONERROR);
    end;
    
  end;
end;

procedure TfrmAfastamentoEntrada.txtDataExit(Sender: TObject);
begin
  // testa se a data � v�lida
  try
    StrToDate(txtData.Text);

    if StrToDate(txtData.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtData.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtData.SetFocus;
    end;
  end;

end;

procedure TfrmAfastamentoEntrada.btnImprimirClick(Sender: TObject);
var vvMes : Integer;
begin
  try
    Application.CreateForm(TrelAfastamento, relAfastamento);
    relAfastamento.lblResponsavel.CAption := txtNomeResponsavel.Text;
    relAfastamento.lblDocResp.Caption := txtTipoDocumento.Text + ': ' + txtNumDocumento.Text;
    relAfastamento.lblNome.Caption := lbNome.Caption;
    relAfastamento.lblMatricula.CAption := txtMatricula.Text;

    relAfastamento.lblDia.Caption := Copy(txtData.Text,1,2);
    relAfastamento.lblMes.Caption := cMeses[StrToInt(Copy(txtData.Text,4,2))];
    relAfastamento.lblAno.Caption := Copy(txtData.Text,7,4);

    relAfastamento.lbMotivoAfastamento.Lines.Add(meMotivoAfastamento.Text);

    relAfastamento.Preview;
    relAfastamento.Free;
    AtualizaCamposBotoes('Padrao');
  except end;
end;

procedure TfrmAfastamentoEntrada.meMotivoAfastamentoExit(Sender: TObject);
begin
  if Length(meMotivoAfastamento.Text) < 0 then
  begin
    ShowMessage ('O campo MOTIVO DO AFASTAMENTO � de preenchimento OBRIGAT�RIO...');
    meMotivoAfastamento.SetFocus;
  end;

end;

end.
