object frmSelecionaIdadeDataEspecifica: TfrmSelecionaIdadeDataEspecifica
  Left = 400
  Top = 325
  BorderStyle = bsDialog
  Caption = 'Rela��o de IDADES'
  ClientHeight = 249
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 522
    Height = 249
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 1
    object Label3: TLabel
      Left = 16
      Top = 18
      Width = 67
      Height = 13
      Caption = 'Informe DATA'
      OnClick = btnImprimirClick
    end
    object Label1: TLabel
      Left = 120
      Top = 17
      Width = 86
      Height = 13
      Caption = 'Selecione a Idade'
    end
    object lbMensagem: TLabel
      Left = 7
      Top = 72
      Width = 61
      Height = 13
      Caption = 'Mensagem...'
    end
    object cbIdades: TComboBox
      Left = 120
      Top = 35
      Width = 97
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 2
      OnClick = cbIdadesClick
      OnExit = cbIdadesExit
    end
    object optTodasIdades: TCheckBox
      Left = 232
      Top = 38
      Width = 113
      Height = 17
      Caption = 'Todas as IDADES'
      TabOrder = 1
      OnClick = optTodasIdadesClick
    end
    object edData: TMaskEdit
      Left = 16
      Top = 36
      Width = 72
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
      OnExit = edDataExit
    end
    object meRelacao: TMemo
      Left = 8
      Top = 96
      Width = 497
      Height = 137
      TabOrder = 3
    end
  end
  object btnImprimir: TBitBtn
    Left = 374
    Top = 27
    Width = 123
    Height = 30
    Caption = 'Exibir Rela��o'
    Default = True
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = btnImprimirClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F0000000120B0000120B00001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      777777776B00777777777000777777776E007777777007880077777771007777
      7007780088007777740077770778878800880077770077778887778888008077
      7A00777887777788888800777D007778F7777F888888880780007778F77FF777
      8888880783007778FFF779977788880786007778F77AA7778807880789007777
      88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
      920077777777778FFFFFF0079500777777777778FFF887779800777777777777
      888777779B00777777777777777777779E0077777777777777777777A1007777
      7777777777777777A400}
  end
end
