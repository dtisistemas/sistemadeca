unit uSkype;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ImgList, StdCtrls, ToolWin, OleCtrls, SKYPE4COMLib_TLB,
  ExtCtrls;

type
  TfrmSkype = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    ListViewListaContato: TListView;
    ToolBar1: TToolBar;
    Edit1: TEdit;
    Label1: TLabel;
    ImageList1: TImageList;
    tlbLimparHistorico: TToolButton;
    tlbLocalizarUsuario: TToolButton;
    ListViewListaJanelasAbertas: TListView;
    Skype1: TSkype;
    ListView1: TListView;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CarregarListaContatoSkype;
    procedure JanelasAbertas;


  public
    { Public declarations }
  end;

var
  frmSkype: TfrmSkype;

implementation

{$R *.DFM}

{ TfrmSkype }

procedure TfrmSkype.CarregarListaContatoSkype;
var
  U       : IUserCollection;
  Usuario : IUser;
  I       : Integer;
  Item    : TListItem;
begin

  ListViewListaContato.Items.Clear;
  U := Skype1.Friends;

  for I := 1 to U.Count do
  begin
    Usuario      := U.Item[I];
    Item         := ListViewListaContato.Items.Add;
    Item.Caption := Usuario.Handle;
  end;

end;

procedure TfrmSkype.JanelasAbertas;
var
  I    : Integer;
  Chat : IChatCollection;
  Item : TListItem;
begin
  Chat := Skype1.ActiveChats;
  for I := 1 to Chat.Count do
  begin
    Item         := ListViewListaJanelasAbertas.Items.Add;
    Item.Caption := Chat.Item[I].FriendlyName;
  end;
end;

procedure TfrmSkype.FormCreate(Sender: TObject);
begin
  StatusBar1.Panels[0].Text := Skype1.CurrentUser.Handle;
end;

end.
