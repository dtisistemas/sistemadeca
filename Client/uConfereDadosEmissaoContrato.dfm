object frmConfereDadosEmissaoContrato: TfrmConfereDadosEmissaoContrato
  Left = 281
  Top = 173
  Width = 1047
  Height = 679
  Caption = '[Sistema Deca] - Confere Dados para Emiss�o de Contrato'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel17: TPanel
    Left = 7
    Top = 8
    Width = 1018
    Height = 33
    TabOrder = 0
    object Label39: TLabel
      Left = 16
      Top = 6
      Width = 463
      Height = 22
      Caption = 'MATR�CULA  -  NOME DA CRIAN�A/ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
  end
  object PageControl1: TPageControl
    Left = 5
    Top = 45
    Width = 1020
    Height = 595
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = '[Cadastros Lan�ados]'
      object btnEmitirContrato: TSpeedButton
        Left = 356
        Top = 512
        Width = 300
        Height = 52
        Caption = 'Emitir 2.� Via Contrato e Ficha Admissional'
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777700000007777777777777777700000007777777774F77777700000007777
          7777444F77777000000077777774444F777770000000700000444F44F7777000
          000070FFF444F0744F777000000070F8884FF0774F777000000070FFFFFFF077
          74F77000000070F88888F077774F7000000070FFFFFFF0777774F000000070F8
          8777F07777774000000070FFFF00007777777000000070F88707077777777000
          000070FFFF007777777770000000700000077777777770000000777777777777
          777770000000}
      end
      object btnNovaAdmissao: TSpeedButton
        Left = 692
        Top = 512
        Width = 300
        Height = 52
        Caption = 'Nova Admiss�o'
        Enabled = False
        Visible = False
      end
      object Label7: TLabel
        Left = 21
        Top = 7
        Width = 966
        Height = 16
        Alignment = taCenter
        Caption = 
          'FAVOR PREENCHER TODOS OS CAMPOS DAS TELAS PARA A EMISS�O CORRETA' +
          ' DO CONTRATO DE ADMISS�O DE APRENDIZAGEM.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbgAdmissoes: TDBGrid
        Left = 4
        Top = 33
        Width = 1003
        Height = 473
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'COD_MATRICULA'
            Title.Alignment = taCenter
            Title.Caption = '[Matr�cula]'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOM_NOME'
            Title.Caption = '[Nome]'
            Width = 400
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ADMISSAO'
            Title.Alignment = taCenter
            Title.Caption = '[Data Admiss�o]'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TERMINO_CONTRATO'
            Title.Alignment = taCenter
            Title.Caption = '[T�rmino Contrato]'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CBO'
            Title.Alignment = taCenter
            Title.Caption = '[CBO]'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRAU_INSTRUCAO'
            Title.Caption = '{Grau Instru��o]'
            Width = 220
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = '[Dados para Admiss�o]'
      ImageIndex = 1
      object PageControl2: TPageControl
        Left = 8
        Top = 8
        Width = 997
        Height = 505
        ActivePage = TabSheet3
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = 'Ficha de Admiss�o e Contrato de Admiss�o'
          object Label1: TLabel
            Left = 254
            Top = 18
            Width = 78
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nome Completo:'
          end
          object Label2: TLabel
            Left = 286
            Top = 40
            Width = 48
            Height = 13
            Alignment = taRightJustify
            Caption = 'Matr�cula:'
          end
          object Label3: TLabel
            Left = 264
            Top = 64
            Width = 70
            Height = 13
            Alignment = taRightJustify
            Caption = 'Unidade Atual:'
          end
          object Label4: TLabel
            Left = 249
            Top = 85
            Width = 82
            Height = 13
            Alignment = taRightJustify
            Caption = 'Unidade Destino:'
          end
          object Label5: TLabel
            Left = 282
            Top = 108
            Width = 49
            Height = 13
            Alignment = taRightJustify
            Caption = 'Endere�o:'
          end
          object Label8: TLabel
            Left = 264
            Top = 132
            Width = 67
            Height = 13
            Alignment = taRightJustify
            Caption = 'Complemento:'
          end
          object Label9: TLabel
            Left = 301
            Top = 152
            Width = 30
            Height = 13
            Alignment = taRightJustify
            Caption = 'Bairro:'
          end
          object Label10: TLabel
            Left = 307
            Top = 174
            Width = 24
            Height = 13
            Alignment = taRightJustify
            Caption = 'CEP:'
          end
          object Label11: TLabel
            Left = 295
            Top = 197
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Cidade:'
          end
          object Label12: TLabel
            Left = 687
            Top = 197
            Width = 17
            Height = 13
            Alignment = taRightJustify
            Caption = 'UF:'
          end
          object Label13: TLabel
            Left = 228
            Top = 219
            Width = 103
            Height = 13
            Alignment = taRightJustify
            Caption = 'Telefone Residencial:'
          end
          object Label14: TLabel
            Left = 413
            Top = 219
            Width = 80
            Height = 13
            Alignment = taRightJustify
            Caption = 'Telefone Celular:'
          end
          object Label15: TLabel
            Left = 258
            Top = 240
            Width = 73
            Height = 13
            Alignment = taRightJustify
            Caption = 'Grau Instru��o:'
          end
          object Edit1: TEdit
            Left = 334
            Top = 16
            Width = 375
            Height = 21
            TabOrder = 0
            Text = 'Edit1'
          end
          object Edit2: TEdit
            Left = 335
            Top = 38
            Width = 67
            Height = 21
            TabOrder = 1
            Text = 'Edit1'
          end
          object Edit3: TEdit
            Left = 335
            Top = 60
            Width = 325
            Height = 21
            TabOrder = 2
            Text = 'Edit1'
          end
          object ComboBox1: TComboBox
            Left = 335
            Top = 82
            Width = 429
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 3
          end
          object Edit4: TEdit
            Left = 335
            Top = 105
            Width = 394
            Height = 21
            TabOrder = 4
            Text = 'Edit1'
          end
          object Edit5: TEdit
            Left = 335
            Top = 127
            Width = 201
            Height = 21
            TabOrder = 5
            Text = 'Edit1'
          end
          object Edit6: TEdit
            Left = 335
            Top = 149
            Width = 298
            Height = 21
            TabOrder = 6
            Text = 'Edit1'
          end
          object MaskEdit1: TMaskEdit
            Left = 335
            Top = 171
            Width = 121
            Height = 21
            TabOrder = 7
            Text = 'MaskEdit1'
          end
          object Edit7: TEdit
            Left = 335
            Top = 193
            Width = 347
            Height = 21
            TabOrder = 8
            Text = 'Edit1'
          end
          object ComboBox2: TComboBox
            Left = 707
            Top = 193
            Width = 56
            Height = 22
            Style = csOwnerDrawFixed
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ItemHeight = 16
            ParentFont = False
            TabOrder = 9
            Items.Strings = (
              'AC'
              'AL'
              'AP'
              'AM'
              'BA'
              'CE'
              'DF'
              'ES'
              'GO'
              'MA'
              'MT'
              'MS'
              'MG'
              'PA'
              'PB'
              'PR'
              'PE'
              'PI'
              'RJ'
              'RN'
              'RS'
              'RO'
              'RR'
              'SC'
              'SP'
              'SE'
              'TO')
          end
          object MaskEdit2: TMaskEdit
            Left = 335
            Top = 215
            Width = 75
            Height = 21
            TabOrder = 10
            Text = 'MaskEdit1'
          end
          object MaskEdit3: TMaskEdit
            Left = 495
            Top = 215
            Width = 75
            Height = 21
            TabOrder = 11
            Text = 'MaskEdit1'
          end
          object ComboBox3: TComboBox
            Left = 335
            Top = 237
            Width = 428
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 12
            Items.Strings = (
              '8� Ano do Ensino Fundamental'
              '9� Ano do Ensino Fundamental'
              '1.� Ano do Ensino M�dio'
              '2.� Ano do Ensino M�dio'
              '3.� Ano do Ensino M�dio'
              'Concluinte do Ensino Fundamental'
              'Concluinte do Ensino M�dio')
          end
        end
      end
      object Panel8: TPanel
        Left = 8
        Top = 520
        Width = 995
        Height = 46
        TabOrder = 1
        object btnAlterar: TSpeedButton
          Left = 613
          Top = 5
          Width = 119
          Height = 37
          Caption = '&Alterar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          Visible = False
        end
        object btnInserir: TSpeedButton
          Left = 743
          Top = 5
          Width = 119
          Height = 37
          Caption = '&Inserir'
          Flat = True
          Glyph.Data = {
            02030000424D0203000000000000360100002800000013000000170000000100
            080000000000CC010000C30E0000C30E000040000000000000001C3404002434
            1C00242424001C3C0400243C0C00244404002C5C04003C5C240044543C005C5C
            54005C5C5C00646464006C6C6C0054743C007474740044840400747C74007C7C
            7C0084848400449404006C8C540054AC0400000000008C8C8C008C948C009494
            94009C9C9C00A4A4A400ACACAC00B4B4B4006CD404006CDC040074F404007CFC
            040084FC0C0084FC14007CDC24008CFC1C008CFC240094FC240094EC3C0094FC
            2C009CFC3C0094D45C009CF44C009CFC4400A4FC4C00A4FC5400ACFC6400B4FC
            6C00B4F47400BCF48400BCFC7C00B4C4A400ACCC9400BCCCAC00BCC4B400BCCC
            B400B4E48C00BCE49400BCDCA400C4F49400C4FC8C00C0C0C0003F3F3F3F3F3F
            191717193F3F3F3F3F3F3F3F3F003F3F3F3F3F1712111112193F3F3F3F3F3F3F
            3F003F3F3F3F19120E0C0C0E123F3F3F3F3F3F3F3F003F3F3F3F120E0C0B0B0C
            11173F3F3F3F3F3F3F003F3F3F17110C0B0A0A0B0E123F3F3F3F3F3F3F003F3F
            3F12140702010B0B0C11173F3F3F3F3F3F003F3F3F181E1E0F03100C0C0E1219
            3F3F3F3F3F003F3F3F2422231F06080C0C0C11173F3F3F3F3F003F3F2B212223
            221305170C0C0E11173F3F3F3F003F3521222323231E06090E0C0C0E12193F3F
            3F003F2B2223272726221304180E0C0C0E123F3F3F003F2926252A2F2F261F06
            08110E0C0E11173F3F0038302D232C39332E23150311110E0C0E11173F003F39
            2E28383F37312A220F0117110E0E0E1219003F3F373F3F3F3F3A30261E060917
            110E0E1117003F3F3F3F3F3F3F3F322E2315030C1712111217003F3F3F3F3F3F
            3F3F37342D2313001819171719003F3F3F3F3F3F3F3F3F3B342E231300193F3F
            3F003F3F3F3F3F3F3F3F3F3F3C3330230F011D3F3F003F3F3F3F3F3F3F3F3F3F
            3F393E31250F0D3F3F003F3F3F3F3F3F3F3F3F3F3F3F383D312320353F003F3F
            3F3F3F3F3F3F3F3F3F3F3F3F3C2A23363F003F3F3F3F3F3F3F3F3F3F3F3F3F3F
            3F3F373F3F00}
        end
        object btnCancelar: TSpeedButton
          Left = 871
          Top = 5
          Width = 119
          Height = 37
          Caption = '&Cancelar'
          Flat = True
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FF033B8A033D90013D95023B91033A89033A89FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0357D30147B20051D0035CE007
            63E3035CE0004ED30042B7023A8F023A8FFF00FFFF00FFFF00FFFF00FFFF00FF
            0650BA0357D32781F278B4F7CAE2FCE9F4FFDCEDFF9CC7FA3F8FF20155DD0140
            A404367DFF00FFFF00FFFF00FF075DD70762E155A0F7F3F8FEFFFFFFE9F3FCC6
            DEFAD9E9FCFFFFFFFFFFFF99C5F8055DE70040A302398BFF00FFFF00FF075DD7
            529EF7FEFEFFE2EFFC0F65EB0558E70959E50250E20454E16FA6F0DEEBFC9CC9
            F80355DE02398BFF00FF0455C9207DF0E1EFFEFFFFFF6FA7F076AFF7176CED06
            5AE9075AE60F5EE66AA1F06FA7F0FFFFFF3E8FF20043B7033E96085FDA56A1FA
            FFFFFF9ECBFB1573F779B4FACFE3FC1C72EF2274EECBE1FB6DA5F20556E3DEEB
            FC9FCBFA0050D4033E960F6BE68BC1FCFFFFFF2987FC1F7DFA1674F779B5FADE
            EDFEDDEDFC6EAAF4065AE90455E5A0C5F6DEEFFF0560E202409C1B76EDA4CFFC
            FFFFFF2988FF1C7EFE1C7BFB2D87FBEDF6FEEDF6FE2279F20B63ED085DEA88BA
            F4EBF6FF0C68E60141A1207AEBA5CFFEFFFFFF3F97FF1F81FF3B93FEE1EFFF6B
            ADFC69ABFBE0EEFE2C80F30C65EEC6DEFBCEE5FE0763E203419E146FE79ACAFC
            FFFFFFB2D8FF318EFFE7F3FF67AFFF1D7EFE1A7AFB60A7FCE5F2FE3F8FF6E2EF
            FE81BAF80258D8033E96FF00FF237BEBEDF6FFFAFCFF5DA9FF469AFF1F81FF1F
            81FF1E80FF1C7DFC4D9CFBF0F8FFF2F8FE3089F4024FC0FF00FFFF00FF237BEB
            BFDEFFF3F8FFFAFCFFB0D5FF3E96FF2B89FF308CFF6AB0FEFAFCFFFFFFFF5DA6
            F70860DE024FC0FF00FFFF00FFFF00FF4997F3C7E3FFF7FBFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFE0EFFE5CA5F80E6BE70552C2FF00FFFF00FFFF00FFFF00FF
            FF00FF2D82EB91C5FBCCE6FFD9EDFFDCEDFEC4E0FE86BFFC348BF40A65E10A65
            E1FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF247BEB4696F34A
            98F42F87F0116CE6075FDCFF00FFFF00FFFF00FFFF00FFFF00FF}
        end
        object Label6: TLabel
          Left = 8
          Top = 11
          Width = 500
          Height = 26
          Alignment = taCenter
          Caption = 
            'PARA QUE O PROCESSO ADMISSIONAL OCORRA SEM ERROS, PEDE-SE FAVOR ' +
            'DE CADASTRAR/INFORMAR TODOS OS CAMPOS.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
      end
    end
  end
end
