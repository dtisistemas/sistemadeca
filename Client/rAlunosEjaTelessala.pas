unit rAlunosEjaTelessala;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelAlunosEJATelessala = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    lbPeriodo: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel12: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText10: TQRDBText;
    SummaryBand1: TQRBand;
    QRLabel14: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRExpr1: TQRExpr;
  private

  public

  end;

var
  relAlunosEJATelessala: TrelAlunosEJATelessala;

implementation

uses uDM;

{$R *.DFM}

end.
