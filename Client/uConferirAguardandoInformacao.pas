unit uConferirAguardandoInformacao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, Buttons, StdCtrls, Mask, Db;

type
  TfrmConfereAguardandoInformacao = class(TForm)
    GroupBox1: TGroupBox;
    mskAnoLetivo: TMaskEdit;
    cbBimestre: TComboBox;
    btnPesquisar: TSpeedButton;
    Panel1: TPanel;
    btnVisualizar: TSpeedButton;
    Memo1: TMemo;
    dsResultado: TDataSource;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfereAguardandoInformacao: TfrmConfereAguardandoInformacao;

implementation

uses uPrincipal, uDM, uUtil, RAguardandoInformacao;

{$R *.DFM}

procedure TfrmConfereAguardandoInformacao.btnPesquisarClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_AcompEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
      Params.ParamByName ('@pe_cod_matricula').Value         := Null;
      Params.ParamByName ('@pe_num_ano_letivo').Value        := GetValue(mskAnoLetivo, vtInteger);
      Params.ParamByName ('@pe_num_bimestre').Value          := cbBimestre.ItemIndex + 1;
      Params.ParamByName ('@pe_cod_unidade').Value           := Null;
      Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
      Open;

      Application.CreateForm (TRelAguardandoInformacao, RelAguardandoInformacao);
      RelAguardandoInformacao.Prepare;
      Memo1.Lines.Clear;
      while not dmDeca.cdsSel_AcompEscolar.eof do
      begin

        if (dmDeca.cdsSel_AcompEscolar.FieldByName('ind_situacao').Value = 8) and (not(dmDeca.cdsSel_AcompEscolar.FieldByName('cod_unidade').AsInteger in [1,3,7,12,40,42,43,47,48])) then
        begin
          Memo1.Lines.Add (dmDeca.cdsSel_AcompEscolar.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_AcompEscolar.FieldByName('nom_nome').Value + ' - ' + dmDeca.cdsSel_AcompEscolar.FieldByName('nom_unidade').Value); // + ' - ' + IntToStr(FieldByName('num_bimestre').Value) + ' - ' + IntToStr(FieldByName('num_ano_letivo').Value));
          RelAguardandoInformacao.QRRichText1.Lines.Add(dmDeca.cdsSel_AcompEscolar.FieldByName('cod_matricula').Value);
          RelAguardandoInformacao.QRRichText2.Lines.Add(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_nome').Value);
          RelAguardandoInformacao.QRRichText3.Lines.Add(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_unidade').Value);
        end;
        
        dmDeca.cdsSel_AcompEscolar.Next;

      end;
      RelAguardandoInformacao.Preview;
    end;
  except
    Application.MessageBox ('Um erro ocorreu tentando executar a consulta.',
                            '[Sistema Deca] - Verificar situa��es "Aguardando Informa��o..."',
                            MB_OK + MB_ICONERROR);


  end;
end;

procedure TfrmConfereAguardandoInformacao.btnVisualizarClick(
  Sender: TObject);
begin
{Application.CreateForm (TRelAguardandoInformacao, RelAguardandoInformacao);
RelAguardandoInformacao.Prepare;
RelAguardandoInformacao.lbTotalPaginas.Caption := IntToStr(RelAguardandoInformacao.PageNumber);
RelAguardandoInformacao.Preview; }
end;

end.
