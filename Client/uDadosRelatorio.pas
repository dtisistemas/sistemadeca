unit uDadosRelatorio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, Buttons, StdCtrls, Mask, Db, ComObj;

type
  TfrmDadosRelatorio = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cbMes: TComboBox;
    GroupBox3: TGroupBox;
    mskAno: TMaskEdit;
    btnConsultar: TSpeedButton;
    btnImprimir: TSpeedButton;
    Panel1: TPanel;
    dbgDadosRelatorio: TDBGrid;
    GroupBox4: TGroupBox;
    cbTipoRelatorio: TComboBox;
    dsSel_DadosEducaSenso: TDataSource;
    lbTotalizador: TLabel;
    btnSair: TSpeedButton;
    btnExportar: TSpeedButton;
    SaveDialog1: TSaveDialog;
    procedure btnConsultarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDadosRelatorio: TfrmDadosRelatorio;

implementation

uses uDM, rRelatorioEducaSenso, rDadosAdmitidos_EducaCenso,
  rDadosTransferidos_educaCenso, rDadosDesligados_EducaCenso,
  rDadosAfastados_EducaCenso, uUtil;

{$R *.DFM}

procedure TfrmDadosRelatorio.btnConsultarClick(Sender: TObject);
begin

  lbTotalizador.Caption := '';

  try
    with dmDeca.cdsSel_DadosEducaSenso do
    begin
      Close;
      Params.ParamByName ('@pe_tipoDado').Value := cbTipoRelatorio.ItemIndex;
      Params.ParamByName ('@pe_num_mes').Value  := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value  := GetValue(mskAno.Text);
      Open;
      dbgDadosRelatorio.Refresh;

      lbTotalizador.Caption := 'Foram encontrados 0 registros no m�s ';
      lbTotalizador.Caption := 'Foram encontrados ' +  IntToStr(dmDeca.cdsSel_DadosEducaSenso.RecordCount) + ' registros no m�s ' + cbMes.Text + '/' + mskAno.Text;

    end;
  except end;

end;

procedure TfrmDadosRelatorio.FormShow(Sender: TObject);
begin
  lbTotalizador.Caption := '';
end;

procedure TfrmDadosRelatorio.btnImprimirClick(Sender: TObject);
begin

  //Carrega um determinado relat�rio de acordo com a op��o selecionada... imprescind�vel devido a quantidade de campos distintos em cada sele��o...
  //ADMITIDOS
  case cbTipoRelatorio.ItemIndex of
   0: begin
        with dmDeca.cdsSel_DadosEducaSenso do
        begin
          //Chama o relat�rio de Admitidos...
          Application.CreateForm (TrelDadosAdmitidos_EducaCenso, relDadosAdmitidos_EducaCenso);
          relDadosAdmitidos_EducaCenso.qlbTitulo.Caption := 'RELA��O DE ADMITIDOS NO PER�ODO ' + cbMes.Text + '/' + mskAno.Text;
          relDadosAdmitidos_EducaCenso.Preview;
          relDadosAdmitidos_EducaCenso.Free;
        end;
      end;

   1: begin
        with dmDeca.cdsSel_DadosEducaSenso do
        begin
          //Close;
          //Params.ParamByName ('@pe_tipoDado').Value := 1;
          //Params.ParamByName ('@pe_num_mes').Value  := cbMes.ItemIndex + 1;
          //Params.ParamByName ('@pe_num_ano').Value  := mskAno.Text;
          //Open;

          //Chama o relat�rio de Desligados...
          Application.CreateForm (TrelDadosDeligados_EducaCenso, relDadosDeligados_EducaCenso);
          relDadosDeligados_EducaCenso.qlbTitulo.Caption := 'RELA��O DE DESLIGADOS NO PER�ODO ' + cbMes.Text + '/' + mskAno.Text;
          relDadosDeligados_EducaCenso.Preview;
          relDadosDeligados_EducaCenso.Free;
        end;
      end;

   2: begin
        with dmDeca.cdsSel_DadosEducaSenso do
        begin
          //Close;
          //Params.ParamByName ('@pe_tipoDado').Value := 2;
          //Params.ParamByName ('@pe_num_mes').Value  := cbMes.ItemIndex + 1;
          //Params.ParamByName ('@pe_num_ano').Value  := mskAno.Text;
          //Open;

          //Chama o relat�rio de Afastados...
          Application.CreateForm (TrelDadosAfastados_EducaCenso, relDadosAfastados_EducaCenso);
          relDadosAfastados_EducaCenso.qlbTitulo.Caption := 'RELA��O DE AFASTAMENTOS NO PER�ODO ' + cbMes.Text + '/' + mskAno.Text;
          relDadosAfastados_EducaCenso.Preview;
          relDadosAfastados_EducaCenso.Free;
        end;
      end;

   3: begin
        with dmDeca.cdsSel_DadosEducaSenso do
        begin
          //Close;
          //Params.ParamByName ('@pe_tipoDado').Value := 3;
          //Params.ParamByName ('@pe_num_mes').Value  := cbMes.ItemIndex + 1;
          //Params.ParamByName ('@pe_num_ano').Value  := mskAno.Text;
          //Open;

          //Chama o relat�rio de Admitidos...
          Application.CreateForm (TrelDadosTransferidos_EducaCenso, relDadosTransferidos_EducaCenso);
          relDadosTransferidos_EducaCenso.qlbTitulo.Caption := 'RELA��O DE TRANSFERIDOS NO PER�ODO ' + cbMes.Text + '/' + mskAno.Text;
          relDadosTransferidos_EducaCenso.Preview;
          relDadosAfastados_EducaCenso.Free;
        end;
      end;

   4: begin
        with dmDeca.cdsSel_DadosEducaSenso do
        begin
          //Chama o relat�rio de Readmitidos...
          Application.CreateForm (TrelDadosTransferidos_EducaCenso, relDadosTransferidos_EducaCenso);
          relDadosTransferidos_EducaCenso.qlbTitulo.Caption := 'RELA��O DE READMITIDOS NO PER�ODO ' + cbMes.Text + '/' + mskAno.Text;
          relDadosTransferidos_EducaCenso.Preview;
          relDadosAfastados_EducaCenso.Free;
        end;
      end;

  end;





end;

procedure TfrmDadosRelatorio.btnSairClick(Sender: TObject);
begin
  frmDadosRelatorio.Close;
end;

procedure TfrmDadosRelatorio.btnExportarClick(Sender: TObject);
var
  Excel : variant;
  ContCampos, ContCampos1, Linha : Integer;
  mmDIA, mmMES, mmANO, mmDATA : String;
begin
  try
    SaveDialog1.FileName := cbTipoRelatorio.Text + ' - ' + cbMes.Text + ' ' + mskAno.Text;
    if SaveDialog1.Execute then
    begin

      Excel := CreateOleObject('Excel.Application');
      Excel.WorkBooks.add(1);
      Excel.WorkBooks[1].SaveAs(SaveDialog1.FileName);;

      for ContCampos := 1 to dmDeca.cdsSel_DadosEducaSenso.FieldCount -1 do
      begin
        Excel.Cells[1, ContCampos] := dmDeca.cdsSel_DadosEducaSenso.Fields[ContCampos].DisplayName;
      end;

      Linha := 2;

      dmDeca.cdsSel_DadosEducaSenso.First;
      while not (dmDeca.cdsSel_DadosEducaSenso.eof) do
      begin
        mmDIA  := '';
        mmMES  := '';
        mmANO  := '';
        mmDATA := '';

        for ContCampos1 := 1 to dmDeca.cdsSel_DadosEducaSenso.FieldCount - 1 do
        begin
          //if (dmDeca.cdsSel_DadosEducaSenso.IndexName ('NASCIMENTO').Value) or (dmDeca.cdsSel_DadosEducaSenso.FieldByName ('ADMISSAO').Value) or (dmDeca.cdsSel_DadosEducaSenso.FieldByName ('LOG_DATA').Value)then
          if (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'NASCIMENTO') OR (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'ADMISSAO') OR
             (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'INSERIDO EM...') OR (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'DESLIGADO') OR
             (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'AFASTADO') OR (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'TRANSFERIDO') OR
             (dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].FieldName = 'REATIVADO EM') then
          begin
            Excel.Cells[Linha, ContCampos].NumberFormat := 'Geral'; //'dd/mm/aaaa';
            //Montar a data na m�o e repassar para a planilha em forma de vari�vel
            mmDIA  := Copy(dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].AsString, 1, 2);
            mmMES  := Copy(dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].AsString, 4, 2);
            mmANO  := Copy(dmDeca.cdsSel_DadosEducaSenso.Fields.Fields[ContCampos1].AsString, 7, 4);
            //Usar CHR(39) para for�ar o excel a entender o campo como String
            mmDATA := chr(39) + mmDIA + '/' + mmMES + '/' + mmANO;
            Excel.Cells[Linha, ContCampos1] := mmDATA;
          end
          else
            Excel.Cells[Linha, ContCampos1] := dmDeca.cdsSel_DadosEducaSenso.Fields[ContCampos1].AsString;
        end;
        Inc(Linha);
        dmDeca.cdsSel_DadosEducaSenso.Next;
      end;
      Excel.Columns.AutoFit;
      Excel.WorkBooks[1].Save;
      Excel.WorkBooks[1].Close;

      Application.MessageBox ('Aten��o !!!' +#13+#10+
                              'A planilha com seus dados foi gerada na pasta selecionada.' +#13+#10+
                              'Acesse sua pasta para ter acesso a planilha e consultar os dados.',
                              '[Sistema Deca] - Exporta��o de dados para planilha',
                              MB_OK + MB_ICONINFORMATION);

    end;
  except end;
end;

end.
