object frmHistorico: TfrmHistorico
  Left = 362
  Top = 247
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - Hist�rico de Movimenta��es de Crian�a/Adolescente'
  ClientHeight = 344
  ClientWidth = 721
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 705
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR crian�a/adolescente'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Clique na LUPA para pesquisar nome/matr�cula'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnExit = txtMatriculaExit
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 59
    Width = 705
    Height = 223
    TabOrder = 1
    object dbgHistorico: TDBGrid
      Left = 6
      Top = 11
      Width = 692
      Height = 205
      Hint = 'Exibe HIST�RICO da crian�a/adolescente'
      DataSource = dsSel_Historico
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection]
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = [fsBold]
      OnCellClick = dbgHistoricoCellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Dat_Historico'
          Title.Caption = 'Data'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dsc_tipo_historico'
          Title.Caption = 'Hist�rico de Movimenta��o'
          Width = 315
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 259
          Visible = True
        end>
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 288
    Width = 705
    Height = 49
    TabOrder = 2
    object btnReemissao: TBitBtn
      Left = 176
      Top = 10
      Width = 209
      Height = 30
      Hint = 
        'Clique para REEMITIR formul�rio relativo � Movimenta��o Selecion' +
        'ada'
      Caption = 'Reemiss�o de Formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnReemissaoClick
    end
    object btnCancelar: TBitBtn
      Left = 390
      Top = 10
      Width = 219
      Height = 30
      Hint = 'Clique para CANCELAR a opera��o'
      Caption = 'Cancelar Opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object btnSair: TBitBtn
      Left = 628
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de Hist�rico de Movimenta��es'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object dsSel_Historico: TDataSource
    DataSet = dmDeca.cdsSel_Historico
    Left = 640
    Top = 32
  end
end
