object frmLogoff: TfrmLogoff
  Left = 442
  Top = 277
  BorderStyle = bsDialog
  Caption = 'Log-Off'
  ClientHeight = 107
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 441
    Height = 107
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 40
      Height = 13
      Caption = 'Unidade'
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 84
      Height = 13
      Caption = 'Senha de Acesso'
    end
    object cbUnidadeLogoff: TComboBox
      Left = 8
      Top = 32
      Width = 301
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnClick = cbUnidadeLogoffClick
    end
    object btnLogin: TBitBtn
      Left = 317
      Top = 23
      Width = 102
      Height = 66
      Caption = 'login'
      TabOrder = 2
      OnClick = btnLoginClick
      Kind = bkOK
    end
    object mskSenha: TEdit
      Left = 10
      Top = 70
      Width = 109
      Height = 24
      Hint = 'DIGITE A SUA SENHA DE ACESSO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      PasswordChar = '*'
      ShowHint = True
      TabOrder = 1
    end
  end
end
