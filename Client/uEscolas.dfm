object frmEscolas: TfrmEscolas
  Left = 503
  Top = 209
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Cadastro de Escolas]'
  ClientHeight = 478
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 616
    Height = 465
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Cadastro de Escolas'
      object Panel2: TPanel
        Left = 7
        Top = 357
        Width = 594
        Height = 43
        TabOrder = 0
        object btnNovo: TBitBtn
          Left = 286
          Top = 7
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnNovoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvar: TBitBtn
          Left = 349
          Top = 7
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnSalvarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnCancelar: TBitBtn
          Left = 469
          Top = 7
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnCancelarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnSair: TBitBtn
          Left = 531
          Top = 7
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnSairClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
        object btnAlterar: TBitBtn
          Left = 409
          Top = 7
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnAlterarClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FF00004300004300003C000037000036000036FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000930000930002841518892B
            2D8C2A2A830F0F6200004000003A00003AFF00FFFF00FFFF00FFFF00FFFF00FF
            0004B30104A73D45C3B3B7EAFFFFFFFFFFFFFFFFFFFFFFFFA0A2CC2D2D740000
            3A00004EFF00FFFF00FFFF00FF0005CC0107BA7F89E2FFFFFFFFFFFF9B9CDB5E
            5EB75F5FB6ADADDDFFFFFFFFFFFF5E5E9B00003A000043FF00FFFF00FF0005CC
            6472E5FFFFFFD7DAF52528B30102880A0DA206078C00006A31319CE6E6F7FFFF
            FF36367D000043FF00FF0007E80B1BD8F8F8FFF2F3FC1721C30405A41214A303
            048905068B01017409097630309CFFFFFFC3C3DB01015000004B0007E84358F0
            FFFFFF6476ED0104C00203950507910304870E109807088C03036E0909798686
            C9FFFFFF27278800004B0009F37F94FAFFFFFF1932F05867EAFCFCFDEBEDF8EA
            EAF7EAEBF7FFFFFFFFFFFF03047E3233A2FFFFFF5558BE00004A0218FDA6B4FD
            FFFFFF112CFD0C20F37E89F2FFFFFFFFFFFFFFFFFFFFFFFF0B0EBD0308AE5257
            BEFFFFFF6267C30000781735FFA4B6FFFFFFFF2C4AFF000FFF0518FCA3ACF3FF
            FFFFFFFFFF070EC70107C31017BE5E65CAFDFDFF4A4EBD00007F0318FF91A6FF
            FFFFFFA9B9FF000EFF0008FF0E1FFDA4AEF71420D8060ECA070CC3090FB7BABE
            EDFCFCFD1017AA00007FFF00FF5F7AFFFFFFFFFFFFFF5C75FF000BFF000EFF00
            0AFF0618F80007DB0006CC7380E8FFFFFF8F97E2000198FF00FFFF00FF425FFF
            C4CFFFFFFFFFFFFFFF8B9FFF162FFF0414FF0515FD2037F39FADF7FFFFFFE2E5
            FA101ABA000198FF00FFFF00FFFF00FF5975FFD7DFFFFFFFFFFFFFFFFCFCFFD1
            DBFFD4DFFFFFFFFFFFFFFFC6CDF71825CD0001B0FF00FFFF00FFFF00FFFF00FF
            FF00FF5C76FF97A9FDDAE0FFFFFFFFFFFFFFFFFFFFD4DBFD596FF00514D70001
            B0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF5C75FF5C79FF62
            7DFF4A66FD203CF50619E5FF00FFFF00FFFF00FFFF00FFFF00FF}
        end
      end
      object Panel8: TPanel
        Left = 7
        Top = 6
        Width = 594
        Height = 175
        TabOrder = 1
        object Label1: TLabel
          Left = 71
          Top = 13
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nome da Escola:'
        end
        object Label2: TLabel
          Left = 48
          Top = 37
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nome do(a) Diretor(a):'
        end
        object Label3: TLabel
          Left = 53
          Top = 60
          Width = 99
          Height = 13
          Alignment = taRightJustify
          Caption = 'Endere�o da Escola:'
        end
        object Label4: TLabel
          Left = 128
          Top = 83
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'CEP:'
        end
        object Label5: TLabel
          Left = 246
          Top = 83
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bairro:'
        end
        object Label6: TLabel
          Left = 107
          Top = 104
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Telefone:'
        end
        object Label7: TLabel
          Left = 120
          Top = 128
          Width = 32
          Height = 13
          Alignment = taRightJustify
          Caption = 'E-Mail:'
        end
        object Label8: TLabel
          Left = 323
          Top = 104
          Width = 37
          Height = 13
          Alignment = taRightJustify
          Caption = 'Regi�o:'
          Visible = False
        end
        object Label9: TLabel
          Left = 78
          Top = 148
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Escola:'
        end
        object edNomeEscola: TEdit
          Left = 157
          Top = 11
          Width = 289
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
        end
        object edNomeDiretor: TEdit
          Left = 157
          Top = 34
          Width = 254
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 1
        end
        object edEnderecoEscola: TEdit
          Left = 157
          Top = 57
          Width = 356
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 2
        end
        object edCepEscola: TMaskEdit
          Left = 157
          Top = 79
          Width = 79
          Height = 21
          EditMask = '99999-999'
          MaxLength = 9
          TabOrder = 3
          Text = '     -   '
        end
        object edBairroEscola: TEdit
          Left = 279
          Top = 79
          Width = 234
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 4
        end
        object edFoneEscola: TMaskEdit
          Left = 157
          Top = 101
          Width = 88
          Height = 21
          EditMask = '(99)9999-9999'
          MaxLength = 13
          TabOrder = 5
          Text = '(  )    -    '
        end
        object edEMailEscola: TEdit
          Left = 157
          Top = 124
          Width = 209
          Height = 21
          CharCase = ecLowerCase
          TabOrder = 6
        end
        object cbRegiao: TComboBox
          Left = 365
          Top = 100
          Width = 113
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 7
          Visible = False
          Items.Strings = (
            'Centro'
            'Leste'
            'Norte'
            'Sul'
            'Oeste'
            'Sudeste')
        end
        object cbTipoEscola: TComboBox
          Left = 157
          Top = 146
          Width = 163
          Height = 19
          Style = csOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 8
          Items.Strings = (
            'Municipal'
            'Estadual'
            'Particular'
            'Sem Escola')
        end
      end
      object Panel1: TPanel
        Left = 7
        Top = 183
        Width = 594
        Height = 171
        BevelOuter = bvLowered
        TabOrder = 2
        object dbgEscolas: TDBGrid
          Left = 9
          Top = 8
          Width = 577
          Height = 154
          DataSource = dsSel_Escola
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dbgEscolasCellClick
          OnDblClick = dbgEscolasDblClick
          OnKeyDown = dbgEscolasKeyDown
          OnKeyUp = dbgEscolasKeyUp
          Columns = <
            item
              Expanded = False
              FieldName = 'nom_escola'
              Title.Caption = 'Nome da Escola'
              Width = 340
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vTipoEscola'
              Title.Alignment = taCenter
              Title.Caption = 'Tipo da Escola'
              Width = 90
              Visible = True
            end>
        end
      end
      object Panel3: TPanel
        Left = 8
        Top = 401
        Width = 593
        Height = 32
        TabOrder = 3
        object Label10: TLabel
          Left = 8
          Top = 9
          Width = 202
          Height = 13
          Caption = 'Informe Nome da Escola ou parte do nome'
        end
        object btnPesquisar: TSpeedButton
          Left = 421
          Top = 4
          Width = 26
          Height = 24
          Flat = True
          Glyph.Data = {
            36050000424D360500000000000036040000280000000E000000100000000100
            08000000000000010000CA0E0000C30E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000ACACACACACAC
            ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
            00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
            02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
            5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
            00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
            ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
            D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
            D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
          OnClick = btnPesquisarClick
        end
        object SpeedButton2: TSpeedButton
          Left = 450
          Top = 3
          Width = 140
          Height = 26
          Caption = 'Ver TODAS as Escolas'
          Flat = True
          OnClick = SpeedButton2Click
        end
        object edValorPesquisa: TEdit
          Left = 215
          Top = 5
          Width = 202
          Height = 24
          Hint = 'INFORME NOME DA ESCOLA (OU PARTE DO NOME) PARA CONSULTA'
          CharCase = ecUpperCase
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnKeyPress = edValorPesquisaKeyPress
        end
      end
    end
  end
  object dsSel_Escola: TDataSource
    DataSet = dmDeca.cdsSel_Escola
    Left = 710
    Top = 7
  end
end
