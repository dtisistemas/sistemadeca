unit uConsultarPedidosUniformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, Buttons, Grids, DBGrids, ExtCtrls, Db;

type
  TfrmConsultarPedidosUniformes = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cbUnidade: TComboBox;
    cbMes1: TComboBox;
    mskAno1: TMaskEdit;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    txtMatricula: TMaskEdit;
    cbMes2: TComboBox;
    mskAno2: TMaskEdit;
    TabSheet3: TTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    cbMes3: TComboBox;
    mskAno3: TMaskEdit;
    TabSheet4: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    cbTipoUniforme: TComboBox;
    cbMes4: TComboBox;
    mskAno4: TMaskEdit;
    btnVerRelatorio1: TSpeedButton;
    btnVerRelatorio2: TSpeedButton;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    dsUniformesSolicitacoes: TDataSource;
    btnVerRelatorio3: TSpeedButton;
    btnVerRelatorio4: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnVerRelatorio1Click(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnVerRelatorio2Click(Sender: TObject);
    procedure btnVerRelatorio3Click(Sender: TObject);
    procedure btnVerRelatorio4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultarPedidosUniformes: TfrmConsultarPedidosUniformes;
  vListaTipoUniforme1, vListaTipoUniforme2, vListaUnidade1, vListaUnidade2 : TStringList;
implementation

uses uDM, uFichaPesquisa, uPrincipal, rPedidos;

{$R *.DFM}

procedure TfrmConsultarPedidosUniformes.FormShow(Sender: TObject);
begin

  PageControl1.ActivePageIndex := 0;

  vListaTipoUniforme1 := TStringList.Create();
  vListaTipoUniforme2 := TStringList.Create();
  cbTipoUniforme.Clear;
  //Carrega a lista de tipos de uniformes...
  try
    with dmDeca.cdsSel_TipoUniforme do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
      Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
      Open;

      while not (dmDeca.cdsSel_TipoUniforme.eof) do
      begin
        cbTipoUniforme.Items.Add (FieldByName('dsc_tipouniforme').Value);
        vListaTipoUniforme1.Add(IntToStr(FieldByName('cod_id_tipouniforme').Value));
        vListaTipoUniforme2.Add(FieldByName('dsc_tipouniforme').Value);
        dmDeca.cdsSel_TipoUniforme.Next;
      end;

    end;
  except end;

  vListaUnidade1 := TStringList.Create();                                                                                                                  
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de Unidades para uma possível consulta
  //aos dados da frequencia de determinada unidade...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('flg_status').AsInteger = 1) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;

end;

procedure TfrmConsultarPedidosUniformes.btnVerRelatorio1Click(
  Sender: TObject);
begin

  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes1.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAno1.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);

      Open;
      DBGrid1.Refresh;


      Application.CreateForm (TrelPedidos, relPedidos);
      relPedidos.QRLabel7.Caption := 'SOLICITAÇÕES EFETUADAS PELA UNIDADE EM ' + cbMes1.Text + '/' + mskAno1.Text;
      relPedidos.Preview;
      relPedidos.Free;
      
    end;
  except end;

end;

procedure TfrmConsultarPedidosUniformes.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (genérica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmConsultarPedidosUniformes.btnVerRelatorio2Click(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Trim(txtMatricula.Text);
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes2.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAno2.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value        := Null;

      Open;
      DBGrid1.Refresh;

      Application.CreateForm (TrelPedidos, relPedidos);
      relPedidos.QRLabel7.Caption := 'SOLICITAÇÕES EFETUADAS PELO ALUNO  ' + txtMatricula.Text + ' - ' + lbNome.Caption + ' EM ' +  cbMes2.Text + '/' + mskAno2.Text;
      relPedidos.Preview;
      relPedidos.Free;

    end;
  except end;
end;

procedure TfrmConsultarPedidosUniformes.btnVerRelatorio3Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes3.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAno3.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value        := Null;

      Open;
      DBGrid1.Refresh;

      Application.CreateForm (TrelPedidos, relPedidos);
      relPedidos.QRLabel7.Caption := 'SOLICITAÇÕES EFETUADAS EM ' +  cbMes3.Text + '/' + mskAno3.Text;
      relPedidos.Preview;
      relPedidos.Free;

    end;
  except end;
end;

procedure TfrmConsultarPedidosUniformes.btnVerRelatorio4Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes4.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAno4.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme.ItemIndex]);
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value        := Null;
      Open;
      DBGrid1.Refresh;

      Application.CreateForm (TrelPedidos, relPedidos);
      relPedidos.QRLabel7.Caption := 'SOLICITAÇÕES DE ' + cbTipoUniforme.Text + ' ' + cbMes4.Text + '/' + mskAno4.Text;
      relPedidos.Preview;
      relPedidos.Free;

    end;
  except end;
end;

end.
