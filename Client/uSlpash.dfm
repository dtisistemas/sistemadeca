object frmSplash: TfrmSplash
  Left = 701
  Top = 339
  BorderStyle = bsNone
  Caption = 'Splash'
  ClientHeight = 202
  ClientWidth = 491
  Color = clWindowText
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 26
    Top = 14
    Width = 229
    Height = 41
    Caption = 'Sistema DECA'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Arial Black'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 29
    Top = 49
    Width = 110
    Height = 23
    Caption = 'Vers�o 5.3.1'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial Black'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 32
    Top = 88
    Width = 434
    Height = 60
    Alignment = taCenter
    Caption = 
      'O Sistema DECA � de uso exclusivo de funcion�rios da Funda��o H�' +
      'lio Augusto de Souza. Contempla os dados do prontu�rio da crian�' +
      'a/adolescente,, incluindo dados cadastrais, programas sociais, a' +
      'tendimentos diversos e relat�rios gerenciais.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label4: TLabel
    Left = 96
    Top = 168
    Width = 287
    Height = 18
    Alignment = taCenter
    Caption = 'AGUARDE... CARREGANDO APLICA��O.'
    Color = clNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    WordWrap = True
  end
end
