unit uGeradorArquivosFaltasDRH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg, Buttons, StdCtrls, Mask, ComCtrls, Grids, DBGrids, Db;

type
  TfrmGeradorArquivosFaltasDRH = class(TForm)
    Image1: TImage;
    Bevel1: TBevel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    mskAno: TMaskEdit;
    GroupBox2: TGroupBox;
    cbMes: TComboBox;
    rgOpcoes: TRadioGroup;
    Panel1: TPanel;
    btnPesquisar: TSpeedButton;
    btnGerarArquivoFaltas: TSpeedButton;
    Panel2: TPanel;
    dbgFrequencia: TDBGrid;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    lbMatricula: TLabel;
    GroupBox5: TGroupBox;
    lbNome: TLabel;
    GroupBox6: TGroupBox;
    lbPeriodo: TLabel;
    GroupBox8: TGroupBox;
    meObservacoes: TMemo;
    dsFrequencia: TDataSource;
    Panel4: TPanel;
    Label4: TLabel;
    Shape1: TShape;
    Label5: TLabel;
    Label8: TLabel;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    btnVer: TSpeedButton;
    GroupBox13: TGroupBox;
    btnExportar: TSpeedButton;
    DBGrid2: TDBGrid;
    SpeedButton2: TSpeedButton;
    cbMesImportacao: TComboBox;
    mskAnoImportacao: TMaskEdit;
    Panel3: TPanel;
    ProgressBar1: TProgressBar;
    GroupBox7: TGroupBox;
    btnSair: TSpeedButton;
    btnImprimir: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnAlterar: TSpeedButton;
    GroupBox12: TGroupBox;
    mskMatricula: TMaskEdit;
    btnPesquisaCadastro: TSpeedButton;
    edDescontoEm: TEdit;
    edReferencia: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox14: TGroupBox;
    mskDataPagamento: TMaskEdit;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure dbgFrequenciaCellClick(Column: TColumn);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGerarArquivoFaltasClick(Sender: TObject);
    procedure dbgFrequenciaDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnPesquisaCadastroClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure cbMesClick(Sender: TObject);
    procedure cbMesImportacaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeradorArquivosFaltasDRH: TfrmGeradorArquivosFaltasDRH;
  mmTOTAL_FALTAS : Integer;

implementation

uses uDM, uUtil, uFichaPesquisa, uPrincipal, rRelacaoFaltasMesDRH;

{$R *.DFM}

procedure TfrmGeradorArquivosFaltasDRH.btnPesquisarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin

      Close;
      Params.ParamByName ('@pe_id_frequencia').Value := Null;

      //Verifica se existe uma matr�cula para consultar...
      if (Length(mskMatricula.Text) = 0) then
        Params.ParamByName ('@pe_cod_matricula').Value := Null
      else
        Params.ParamByName ('@pe_cod_matricula').Value := GetValue(mskMatricula.Text);

      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_num_ano').AsInteger := GetValue(mskAno.Text);
      Params.ParamByName ('@pe_num_mes').AsInteger := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;

      dsFrequencia.DataSet := dmDeca.cdsSel_Cadastro_Frequencia;
      dbgFrequencia.DataSource := dsFrequencia;

    end
  except
    begin
      ShowMessage('Aten��o! Ocorreu um ERRO e os dados n�o foram selecionados...'+#13+#10+#13+#10+
                  'Contate o Administrador do Sistema e informe o erro ocorrido...');
      cbMes.ItemIndex := -1;
      mskAno.Text := '';
      dbgFrequencia.DataSource := nil;
    end;
  end;
end;

procedure TfrmGeradorArquivosFaltasDRH.btnExportarClick(Sender: TObject);
var fileCRIANCAS, fileBOLSISTAS, fileCELETISTAS : TextFile;
begin
  //N�o esquecer de que ao gerar o(s) arquivo(s), os registros identificados devem ser "flagados"
  //de maneira que n�o se possa mais realizar qualquer altera��o dos dados.
  ProgressBar1.Min := 0;
  ProgressBar1.Max := dmDeca.cdsSel_Cadastro_Frequencia.RecordCount;

  AssignFile (fileBOLSISTAS, ExtractFilePath(Application.ExeName) + 'BOLSISTAS.TXT');
  ReWrite (fileBOLSISTAS);

  AssignFile (fileCELETISTAS, ExtractFilePath(Application.ExeName) + 'CELETISTAS.TXT');
  ReWrite (fileCELETISTAS);

  AssignFile (fileCRIANCAS, ExtractFilePath(Application.ExeName) + 'CRIANCAS.TXT');
  ReWrite (fileCRIANCAS);

  if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount < 1 then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Nenhum registro selecionado para ser gerado o arquivo.',
                            '[Sistema Deca] - Exportando dados...',
                            MB_OK + MB_ICONQUESTION);
    cbMes.ItemIndex := -1;
    mskAno.Clear;
    dbgFrequencia.DataSource := nil;
  end
  else
  begin

    dmDeca.cdsSel_Cadastro_Frequencia.First;
    while not (dmDeca.cdsSel_Cadastro_Frequencia.eof) do
    begin

      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').AsString,1,3) = '002') and
         ( (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger > 0) or
         ( (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_justificadas').AsInteger > 0)) ) then
      begin

        {*******************************************************************************************************}
        {Layout de Exporta��o dos dados a partir de 2011 - Sistema TOTVS  CRIAN�AS}

        Write (fileCRIANCAS, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + ';');
        Write (fileCRIANCAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,7,4))) + ';');
        Write (fileCRIANCAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,4,2))) + ';');
        Write (fileCRIANCAS, '30;');
        Write (fileCRIANCAS, '1118;');
        Write (fileCRIANCAS, Copy(mskDataPagamento.Text,1,2) + Copy(mskDataPagamento.Text,4,2) + Copy(mskDataPagamento.Text,7,4) + ';');
        Write (fileCRIANCAS, '000:00;');

        mmTOTAL_FALTAS := 0;
        mmTOTAL_FALTAS := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger +
                          dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_justificadas').AsInteger;

        if (Length(IntToStr(mmTOTAL_FALTAS))=1) then
          Write (fileCRIANCAS,'00000000000' + IntToStr(mmTOTAL_FALTAS) + ',00;')
        else if (Length(IntToStr(mmTOTAL_FALTAS))=2) then
          Write (fileCRIANCAS,'0000000000' + IntToStr(mmTOTAL_FALTAS) + ',00;');

        Write (fileCRIANCAS, '000000000000,00;');
        Write (fileCRIANCAS, '000000000000,00;');
        Writeln (fileCRIANCAS);

      end;


        {*******************************************************************************************************}
        {Layout de Exporta��o dos dados a partir de 2011 - Sistema TOTVS  BOLSISTAS}


      if ( (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').AsString,1,3) = '003') and
         (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger > 0) ) then
      begin

        Write (fileBOLSISTAS, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + ';');
        Write (fileBOLSISTAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,7,4))) + ';');
        Write (fileBOLSISTAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,4,2))) + ';');
        Write (fileBOLSISTAS, '30;');
        Write (fileBOLSISTAS, '0439;');
        Write (fileBOLSISTAS, Copy(mskDataPagamento.Text,1,2) + Copy(mskDataPagamento.Text,4,2) + Copy(mskDataPagamento.Text,7,4) + ';');
        Write (fileBOLSISTAS, '000:00;');

        mmTOTAL_FALTAS := 0;
        mmTOTAL_FALTAS := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger;

        if (Length(IntToStr(mmTOTAL_FALTAS))=1) then
          Write (fileBOLSISTAS,'00000000000' + IntToStr(mmTOTAL_FALTAS) + ',00;')
        else if (Length(IntToStr(mmTOTAL_FALTAS))=2) then
          Write (fileBOLSISTAS,'0000000000' + IntToStr(mmTOTAL_FALTAS) + ',00;');

        Write (fileBOLSISTAS, '000000000000,00;');
        Write (fileBOLSISTAS, '000000000000,00;');
        Writeln (fileBOLSISTAS);

      end;

        {*******************************************************************************************************}
        {Layout de Exporta��o dos dados a partir de 2011 - Sistema TOTVS  CELETISTAS}

      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').AsString,1,3) = '006') and
         (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger > 0) then
      begin

        //Faltas
        Write (fileCELETISTAS, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + ';');
        //Write (fileCELETISTAS, GetValue(mskAnoImportacao.Text) + ';');
        Write (fileCELETISTAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,7,4))) + ';');
        Write (fileCELETISTAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,4,2))) + ';');
        //Write (fileCELETISTAS, IntToStr(cbMesImportacao.ItemIndex + 1) + ';');
        Write (fileCELETISTAS, '30;');
        Write (fileCELETISTAS, '0405;');
        Write (fileCELETISTAS, Copy(mskDataPagamento.Text,1,2) + Copy(mskDataPagamento.Text,4,2) + Copy(mskDataPagamento.Text,7,4) + ';');
        Write (fileCELETISTAS, '000:00;');

        if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 1) then
          Write (fileCELETISTAS,'00000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  ',00;')
        else if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 2) then
          Write (fileCELETISTAS,'0000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  ',00;');
        Write (fileCELETISTAS, ';');
        Write (fileCELETISTAS, '000000000000,00;');
        Writeln (fileCELETISTAS);

        //DSR
        Write (fileCELETISTAS, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + ';');
        Write (fileCELETISTAS, GetValue(mskAnoImportacao.Text) + ';');
        Write (fileCELETISTAS, IntToStr(StrToInt(Copy(mskDataPagamento.Text,4,2))) + ';');
        Write (fileCELETISTAS, '30;');
        Write (fileCELETISTAS, '0424;');
        Write (fileCELETISTAS, Copy(mskDataPagamento.Text,1,2) + Copy(mskDataPagamento.Text,4,2) + Copy(mskDataPagamento.Text,7,4) + ';');
        Write (fileCELETISTAS, '000:00;');
        if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger)) = 1) then
          Write (fileCELETISTAS,'00000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger) +  ',00;')
        else if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger)) = 2) then
          Write (fileCELETISTAS,'0000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger) +  ',00;');
        Write (fileCELETISTAS, '000000000000,00;');
        Write (fileCELETISTAS, '000000000000,00;');
        Writeln (fileCELETISTAS);

      end;


      //Depois de exportado, altera o "flag" do registro pata "1"
      with dmDeca.cdsAlt_MudaFlgExportouFrequencia do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').Value;
        Params.ParamByName ('@pe_num_ano').Value       := GetValue(mskAnoImportacao.Text);
        Params.ParamByName ('@pe_num_mes').Value       := cbMesImportacao.ItemIndex + 1;
        Execute;
      end;

      dmDeca.cdsSel_Cadastro_Frequencia.Next;
      ProgressBar1.Position := ProgressBar1.Position + 1;
      
    end;

    CloseFile (fileCRIANCAS);
    CloseFile (fileBOLSISTAS);
    CloseFile (fileCELETISTAS);

    ShowMessage ('Dados exportados com sucesso...');
    ProgressBar1.Position := 0;
    cbMes.ItemIndex := -1;
    mskAno.Text := '2005';
    dbgFrequencia.DataSource := nil;
  end;
end;

procedure TfrmGeradorArquivosFaltasDRH.btnVerClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin

      Close;
      Params.ParamByName ('@pe_id_frequencia').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_num_ano').AsInteger := GetValue(mskAnoImportacao.Text);
      Params.ParamByName ('@pe_num_mes').AsInteger := cbMesImportacao.ItemIndex + 1;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;

      //Se o registro encontra-se flg=1, j� exportado...
      if dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_exportou').Value = 1 then
      begin
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Os dados pesquisados j� encontram-se exportados.' + #13+#10 +
                                'Essa opera��o n�o pode ser refeita.',
                                '[Sistema Deca] - Exporta��o de Faltas',
                                MB_OK + MB_ICONERROR);
        btnExportar.Enabled := True;
      end
      else
      begin
        dsFrequencia.DataSet := dmDeca.cdsSel_Cadastro_Frequencia;
        dbgFrequencia.DataSource := dsFrequencia;
        //Aqui criar uma rotina para c�lculo do DSR para folha 006
      end;
    end
  except
    begin
      ShowMessage('Aten��o! Ocorreu um ERRO e os dados n�o foram selecionados...'+#13+#10+#13+#10+
                  'Contate o Administrador do Sistema e informe o erro ocorrido...');
      cbMes.ItemIndex := -1;
      mskAno.Text := '';
      dbgFrequencia.DataSource := nil;
    end;
  end;
end;

procedure TfrmGeradorArquivosFaltasDRH.dbgFrequenciaCellClick(
  Column: TColumn);
begin
  with dmDeca.cdsSel_Cadastro_Frequencia do
  begin
    lbMatricula.Caption := FieldByName ('cod_matricula').Value;
    lbNome.Caption      := FieldByName ('Nome').Value;
    lbPeriodo.Caption   := Copy(FieldByName ('Mes').Value,1,3) + '/' +IntToStr(FieldByName ('num_ano').AsInteger);
    if FieldByName ('dsc_observacoes').Value = Null then
      meObservacoes.Text := ''
    else
      meObservacoes.Text := FieldByName ('dsc_observacoes').Value;

    //Habilita o bot�o alterar caso ainda n�o tenha exportado para
    //a base de dados do DRH
    if FieldByName ('flg_exportou').Value = 0 then
      btnAlterar.Enabled := True
    else
      btnAlterar.Enabled := False;

  end;
end;

procedure TfrmGeradorArquivosFaltasDRH.btnAlterarClick(Sender: TObject);
begin
  try

    with dmDeca.cdsAlt_Cadastro_Frequencia_Observacoes do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').AsInteger := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('id_frequencia').AsInteger;
      Params.ParamByName ('@pe_dsc_observacoes').AsString := GetValue(meObservacoes.Text);
      Execute;

      with dmDeca.cdsSel_Cadastro_Frequencia do
      begin
        Close;
        Params.ParamByName ('@pe_id_frequencia').Value := Null;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
        Params.ParamByName ('@pe_num_ano').AsInteger := GetValue(mskAno.Text);
        Params.ParamByName ('@pe_num_mes').AsInteger := cbMes.ItemIndex + 1;
        Params.ParamByName ('@pe_cod_turma').Value := Null;
        Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
        Open;

        dsFrequencia.DataSet := dmDeca.cdsSel_Cadastro_Frequencia;
        dbgFrequencia.DataSource := dsFrequencia;
      end ;

      //Habilita os bot�es e campos padr�es
      lbMatricula.Caption          := '';
      lbNome.Caption               := '';
      lbPeriodo.Caption            := '';
      meObservacoes.Clear;

      btnAlterar.Enabled := False;
      btnCancelar.Enabled := False;
      btnSair.Enabled := True;

    end;
  except end;
end;

procedure TfrmGeradorArquivosFaltasDRH.FormShow(Sender: TObject);
begin

  //Habilita os bot�es e campos padr�es
  PageControl1.ActivePageIndex := 0;
  mskAno.Clear;
  cbMes.ItemIndex              := -1;
  dbgFrequencia.DataSource     := nil;
  lbMatricula.Caption          := '';
  lbNome.Caption               := '';
  lbPeriodo.Caption            := '';
  meObservacoes.Clear;
  edReferencia.Clear;
  edDescontoEm.Clear;

  btnAlterar.Enabled := False;
  btnCancelar.Enabled := False;
  btnImprimir.Enabled := True;  
  btnSair.Enabled := True;

end;

procedure TfrmGeradorArquivosFaltasDRH.btnSairClick(Sender: TObject);
begin
  frmGeradorArquivosFaltasDRH.Close;
end;

procedure TfrmGeradorArquivosFaltasDRH.btnCancelarClick(Sender: TObject);
begin
  //Habilita os bot�es e campos padr�es
  PageControl1.ActivePageIndex := 0;
  mskAno.Clear;
  cbMes.ItemIndex              := -1;
  dbgFrequencia.DataSource     := nil;
  lbMatricula.Caption          := '';
  lbNome.Caption               := '';
  lbPeriodo.Caption            := '';
  meObservacoes.Clear;
  edReferencia.Clear;
  edDescontoEm.Clear;

  btnAlterar.Enabled := False;
  btnCancelar.Enabled := False;
  btnImprimir.Enabled := True;
  btnSair.Enabled := True;
end;

procedure TfrmGeradorArquivosFaltasDRH.btnGerarArquivoFaltasClick(
  Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
end;

procedure TfrmGeradorArquivosFaltasDRH.dbgFrequenciaDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('flg_exportou').AsInteger = 0 then
  begin
    dbgFrequencia.Canvas.Font.Color:= clWhite;
    dbgFrequencia.Canvas.Brush.Color:= clGreen;
  end
  else
  begin
    dbgFrequencia.Canvas.Font.Color:= clYellow;
    dbgFrequencia.Canvas.Brush.Color:= clRed;
  end;

  dbgFrequencia.Canvas.FillRect(Rect);
  dbgFrequencia.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

procedure TfrmGeradorArquivosFaltasDRH.btnPesquisaCadastroClick(
  Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    mskMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
      end;
    except
    end;
end;
end;
procedure TfrmGeradorArquivosFaltasDRH.btnImprimirClick(Sender: TObject);
begin
  //Imprimir o relat�rio de Controle de Frequ�ncia...  
  Application.CreateForm (TrelRelacaoFaltasMesDRH, relRelacaoFaltasMesDRH);
  relRelacaoFaltasMesDRH.Prepare;
  relRelacaoFaltasMesDRH.qrlbPaginas.Caption := '/' + IntToStr(relRelacaoFaltasMesDRH.QRPrinter.PageCount);
  relRelacaoFaltasMesDRH.qrlbTitulo1.Caption := 'RELA��O DE FALTAS DO M�S DE ' + edReferencia.Text;
  relRelacaoFaltasMesDRH.qrlbTitulo2.Caption := 'DESCONTO EM FOLHA EM - ' + edDescontoEm.Text;
  relRelacaoFaltasMesDRH.Preview;
  relRelacaoFaltasMesDRH.Free;
end;

procedure TfrmGeradorArquivosFaltasDRH.cbMesClick(Sender: TObject);
begin
  edReferencia.Text := cbMes.Text + '/' + mskAno.Text;
  edDescontoEm.Text := cbMes.Text + '/' + mskAno.Text;
  Application.MessageBox ('Aten��o !!!' + #13+#10 +
                          'Verifique o m�s para o DESCONTO EM FOLHA...',
                          '[Sistema Deca] - Rela��o de Faltas no m�s',
                          MB_OK + MB_ICONINFORMATION);
end;

procedure TfrmGeradorArquivosFaltasDRH.cbMesImportacaoClick(
  Sender: TObject);
begin
  if (cbMesImportacao.ItemIndex <= 9) then
    mskDataPagamento.Text := '29/' + '0' + IntToStr((cbMesImportacao.ItemIndex+1)) + '/' + mskAnoImportacao.Text
  else
    mskDataPagamento.Text := '29/' + IntToStr((cbMesImportacao.ItemIndex+1)) + '/' +mskAnoImportacao.Text ;
end;

end.
