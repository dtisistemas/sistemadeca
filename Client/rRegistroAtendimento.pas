unit rRegistroAtendimento;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelRegistroAtendimento = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRShape1: TQRShape;
    QRShape6: TQRShape;
    QRShape8: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel4: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText3: TQRDBText;
    lbIdentificacao: TQRLabel;
  private

  public

  end;

var
  relRegistroAtendimento: TrelRegistroAtendimento;

implementation

uses uDM, uEmiteRegAtendimento;

{$R *.DFM}

end.
