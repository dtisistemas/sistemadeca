unit uConsultaHistoricoSocial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, ComCtrls, Db, StdCtrls;

type
  TfrmHistoricoSocial = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dbgHistorico: TDBGrid;
    dbgEvolucao: TDBGrid;
    Panel1: TPanel;
    dsHistorico: TDataSource;
    dsEvolucao: TDataSource;
    lbIdentificacao: TLabel;
    GroupBox1: TGroupBox;
    meDescricaoOcorrencia: TMemo;
    GroupBox2: TGroupBox;
    meDescricaoEvolucao: TMemo;
    procedure dbgHistoricoCellClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure dbgEvolucaoCellClick(Column: TColumn);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistoricoSocial: TfrmHistoricoSocial;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmHistoricoSocial.dbgHistoricoCellClick(Column: TColumn);
begin
  meDescricaoOcorrencia.Lines.Clear;
  meDescricaoOcorrencia.Lines.Add (dmDeca.cdsSel_Cadastro_Historico.FieldByName('dsc_ocorrencia').Value);
end;

procedure TfrmHistoricoSocial.FormShow(Sender: TObject);
begin

  //Se o perfil do usu�rio for diferente do Servi�o Social, n�o habilita o p�gina Evolu��o do Servi�o Social
  //if vvIND_PERFIL <> 3 then
  //  PageControl1.Pages[1].Visible := False;
end;

procedure TfrmHistoricoSocial.dbgEvolucaoCellClick(Column: TColumn);
begin
  meDescricaoEvolucao.Lines.Clear;
  meDescricaoEvolucao.Lines.Add (dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByName('dsc_comentario').Value);
end;

procedure TfrmHistoricoSocial.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  //Se o perfil do usu�rio for diferente do Servi�o Social, n�o habilita o p�gina Evolu��o do Servi�o Social
  if vvIND_PERFIL <> 3 then AllowChange := False;
  //  PageControl1.Pages[1].Visible := False;
end;

procedure TfrmHistoricoSocial.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) then
  begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if Key = #27 then
  begin
    Key := #0;
    frmHistoricoSocial.Close;
  end;
end;

end.
