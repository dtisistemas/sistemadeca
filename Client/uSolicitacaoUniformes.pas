unit uSolicitacaoUniformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, Mask, Buttons, Db, CheckLst;

type
  TfrmSolicitacaoUniformes = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    dbgUniformes: TDBGrid;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    btnNovoUniforme: TSpeedButton;
    btnGravarUniforme: TSpeedButton;
    btnAlterarUniforme: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnRelacaoUniformes: TSpeedButton;
    btnSair: TSpeedButton;
    btnAtivarDesativar: TSpeedButton;
    GroupBox1: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    btnTiposUniformes: TSpeedButton;
    cbTipoUniforme: TComboBox;
    cbCor: TComboBox;
    cbSexo: TComboBox;
    cbNumeracao: TComboBox;
    edQtdParaAdmissao: TEdit;
    edQtdParaReposicao: TEdit;
    cbPeriodoReposicao: TComboBox;
    dsSel_Uniformes: TDataSource;
    GroupBox8: TGroupBox;
    GroupBox10: TGroupBox;
    cbUnidade: TComboBox;
    GroupBox11: TGroupBox;
    cbMes: TComboBox;
    mskAno: TMaskEdit;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    GroupBox12: TGroupBox;
    cbTipoUniforme2: TComboBox;
    btnGerarSolicitacao: TSpeedButton;
    btnVerAlunosDaUnidade: TSpeedButton;
    dbgAlunos: TDBGrid;
    dsSel_Cadastro: TDataSource;
    Panel3: TPanel;
    Label1: TLabel;
    cbNumeracao2: TComboBox;
    Label2: TLabel;
    GroupBox9: TGroupBox;
    dbgUniformesParaSolicitacao: TDBGrid;
    TabSheet5: TTabSheet;
    dsSolicitacoes: TDataSource;
    GroupBox15: TGroupBox;
    GroupBox14: TGroupBox;
    Label6: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    dbgSOLICITACOES: TDBGrid;
    btnALTERAR_SOLICITACAO: TSpeedButton;
    GroupBox16: TGroupBox;
    GroupBox17: TGroupBox;
    GroupBox18: TGroupBox;
    GroupBox19: TGroupBox;
    btnCONSULTA_SOLICITACOES: TSpeedButton;
    edNumeracao: TEdit;
    edQtd: TEdit;
    btnExcluirSolicitacao: TSpeedButton;
    edTipoUniforme: TEdit;
    edCor: TEdit;
    btnVerSolicitacoes: TSpeedButton;
    btnGerarPedidos: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label5: TLabel;
    edTamanho: TEdit;
    Label4: TLabel;
    edQtdSolicitada: TEdit;
    Label3: TLabel;
    mskDataSolicitacao: TMaskEdit;
    Label7: TLabel;
    procedure btnSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ModoTela(Modo: String);
    procedure btnNovoUniformeClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGravarUniformeClick(Sender: TObject);
    procedure dbgUniformesDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ValidaCampos;
    procedure btnAlterarUniformeClick(Sender: TObject);
    procedure btnGerarSolicitacaoClick(Sender: TObject);
    procedure cbTipoUniforme2Click(Sender: TObject);
    procedure cbNumeracao2Click(Sender: TObject);
    procedure edQtdSolicitadaEnter(Sender: TObject);
    procedure edQtdSolicitadaExit(Sender: TObject);
    procedure edTamanhoEnter(Sender: TObject);
    procedure edTamanhoExit(Sender: TObject);
    procedure dbgUniformesParaSolicitacaoCellClick(Column: TColumn);
    procedure btnVerAlunosDaUnidadeClick(Sender: TObject);
    procedure cbMesClick(Sender: TObject);
    procedure btnCONSULTA_SOLICITACOESClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure dbgSOLICITACOESDblClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnALTERAR_SOLICITACAOClick(Sender: TObject);
    procedure btnExcluirSolicitacaoClick(Sender: TObject);
    procedure btnGerarPedidosClick(Sender: TObject);
    procedure btnRelacaoUniformesClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSolicitacaoUniformes: TfrmSolicitacaoUniformes;
  vListaTipoUniforme1, vListaTipoUniforme2, vListaUnidade1, vListaUnidade2, vListaNumeracao1, vListaNumeracao2 : TStringList;
  vIndex : Integer;
  vvMUDA_ABA_UNIFORME : Boolean;

implementation

uses uPrincipal, uDM, uFichaPesquisa, uUtil, uGerarPedidoUniformes,
  rUniformes, uConsultarPedidosUniformes;

{$R *.DFM}

procedure TfrmSolicitacaoUniformes.btnSairClick(Sender: TObject);
begin
  frmSolicitacaoUniformes.Close;
end;

procedure TfrmSolicitacaoUniformes.FormCreate(Sender: TObject);
begin
  ModoTela('P');
end;

procedure TfrmSolicitacaoUniformes.ModoTela(Modo: String);
begin
  if (Modo = 'P') then
  begin
    dbgUniformes.Enabled         := True;
    //Campos
    cbTipoUniforme.ItemIndex     := -1;
    cbCor.ItemIndex              := -1;
    cbSexo.ItemIndex             := -1;
    cbNumeracao.ItemIndex        := -1;
    edQtdParaAdmissao.Clear;
    edQtdParaReposicao.Clear;
    cbPeriodoReposicao.ItemIndex := -1;
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;
    GroupBox3.Enabled := False;
    GroupBox4.Enabled := False;
    GroupBox5.Enabled := False;
    GroupBox6.Enabled := False;
    GroupBox7.Enabled := False;
    //Bot�es
    btnNovoUniforme.Enabled      := True;
    btnGravarUniforme.Enabled    := False;
    btnAlterarUniforme.Enabled   := False;
    btnCancelar.Enabled          := False;
    btnRelacaoUniformes.Enabled  := True;
    btnAtivarDesativar.Enabled   := True;
    btnTiposUniformes.Enabled    := True;
    btnSair.Enabled              := True;
    vvMUDA_ABA_UNIFORME          := True;
  end
  else if (Modo = 'N') then
  begin
    dbgUniformes.Enabled         := False;
    //Campos
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    GroupBox4.Enabled := True;
    GroupBox5.Enabled := True;
    GroupBox6.Enabled := True;
    GroupBox7.Enabled := True;
    //Bot�es
    btnNovoUniforme.Enabled      := False;
    btnGravarUniforme.Enabled    := True;
    btnAlterarUniforme.Enabled   := False;
    btnCancelar.Enabled          := True;
    btnRelacaoUniformes.Enabled  := False;
    btnAtivarDesativar.Enabled   := False;
    btnTiposUniformes.Enabled    := False;
    btnSair.Enabled              := False;
    vvMUDA_ABA_UNIFORME          := False;
  end
  else if (Modo = 'A') then
  begin
    dbgUniformes.Enabled         := False;
    //Campos
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    GroupBox4.Enabled := True;
    GroupBox5.Enabled := True;
    GroupBox6.Enabled := True;
    GroupBox7.Enabled := True;
    //Bot�es
    btnNovoUniforme.Enabled      := False;
    btnGravarUniforme.Enabled    := False;
    btnAlterarUniforme.Enabled   := True;
    btnCancelar.Enabled          := True;
    btnRelacaoUniformes.Enabled  := False;
    btnAtivarDesativar.Enabled   := False;
    btnTiposUniformes.Enabled    := False;
    btnSair.Enabled              := False;
    vvMUDA_ABA_UNIFORME          := False;
  end;
end;

procedure TfrmSolicitacaoUniformes.btnNovoUniformeClick(Sender: TObject);
begin
  ModoTela('N');
  vvMUDA_ABA_UNIFORME := False;
end;

procedure TfrmSolicitacaoUniformes.btnCancelarClick(Sender: TObject);
begin
  ModoTela('P');
  vvMUDA_ABA_UNIFORME := True;
end;

procedure TfrmSolicitacaoUniformes.btnGravarUniformeClick(Sender: TObject);
begin

  ValidaCampos;

  try
    with dmDeca.cdsSel_ConsultaUniforme do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme.ItemIndex]);
      Params.ParamByName ('@pe_ind_cor').Value             := cbCor.ItemIndex;
      Params.ParamByName ('@pe_ind_sexo').Value            := cbSexo.ItemIndex;
      Params.ParamByName ('@pe_ind_numeracao').Value       := cbNumeracao.ItemIndex;
      Open;

      //Se encontrar um registro avisa e n�o grava
      if RecordCount > 0 then
      begin
        Application.MessageBox('Aten��o!!!'+#13+#10+
                               'O UNIFORME que est� tentando gravar j� existe no banco de dados.'+#13+#10+
                               'Verifique os dados e tente novamente',
                               '[Sistema DECA] - Inclus�o de Uniforme',
                               MB_OK + MB_ICONWARNING);
        cbTipoUniforme.SetFocus;
      end
      else
      //Se n�o encontrou registros, grava os dados no banco
      begin
        if Application.MessageBox('Deseja INCLUIR O UNIFORME?',
                                  '[Sistema Deca] - Inclus�o de Uniforme',
                                  MB_ICONQUESTION + MB_YESNO ) = idYes then
        begin
          try
            with dmDeca.cdsInc_Uniforme do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_tipouniforme').Value   := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme.ItemIndex]);
              Params.ParamByName ('@pe_ind_cor').Value               := cbCor.ItemIndex;
              Params.ParamByName ('@pe_ind_sexo').Value              := cbSexo.ItemIndex;
              Params.ParamByName ('@pe_qtd_admissao').Value          := StrToInt(edQtdParaAdmissao.Text);
              Params.ParamByName ('@pe_qtd_reposicao').Value         := StrToInt(edQtdParaReposicao.Text);
              Params.ParamByName ('@pe_ind_periodo_reposicao').Value := cbPeriodoReposicao.ItemIndex;
              Params.ParamByName ('@pe_ind_numeracao').Value         := cbNumeracao.Text; //cbNumeracao.ItemIndex;
              Execute;

              Application.MessageBox('Aten��o!!!'+#13+#10+
                                     'Os dados foram inseridos com sucesso.',
                                     '[Sistema DECA] - Sucesso',
                                     MB_OK + MB_ICONINFORMATION);
              ModoTela('P');
              with dmDeca.cdsSel_Uniformes do
              begin
                Close;
                Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
                Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
                Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
                Params.ParamByName ('@pe_numeracao').Value           := Null;
                Open;
                dbgUniformes.Refresh;
              end;

            end;
          except
              Application.MessageBox('Aten��o!!!'+#13+#10+
                                     'Um ERRO ocorreu na tentativa de inserir os dados do uniforme.'+#13+#10+
                                     'Verifique internet e/ou conex�o de rede e tente novamente.',
                                     '[Sistema DECA] - Erro de Inser��o',
                                     MB_OK + MB_ICONWARNING);
              ModoTela('P');
          end;

        end;
      end;

    end;

  except
    Application.MessageBox('Aten��o!!!'+#13+#10+
                           'Um ERRO ocorreu na tentativa de validar os dados do uniforme.'+#13+#10+
                           'Verifique internet e/ou conex�o de rede e tente novamente.',
                           '[Sistema DECA] - Erro de Consulta',
                           MB_OK + MB_ICONWARNING);
  end;

end;

procedure TfrmSolicitacaoUniformes.dbgUniformesDblClick(Sender: TObject);
begin
  ModoTela('A');
  vListaTipoUniforme2.Find(dmDeca.cdsSel_Uniformes.FieldByName('dsc_tipouniforme').Value, vIndex);
  cbTipoUniforme.ItemIndex := vIndex;
  cbCor.ItemIndex               := dmDeca.cdsSel_Uniformes.FieldByName('ind_cor').Value;
  cbSexo.ItemIndex              := dmDeca.cdsSel_Uniformes.FieldByName('ind_sexo').Value;
  cbNumeracao.ItemIndex         := dmDeca.cdsSel_Uniformes.FieldByName('ind_numeracao').Value;
  edQtdParaAdmissao.Text        := dmDeca.cdsSel_Uniformes.FieldByName('qtd_admissao').Value;
  edQtdParaReposicao.Text       := dmDeca.cdsSel_Uniformes.FieldByName('qtd_reposicao').Value;
  cbPeriodoReposicao.ItemIndex  := dmDeca.cdsSel_Uniformes.FieldByName('ind_periodo_reposicao').Value;
  cbTipoUniforme.SetFocus;
end;

procedure TfrmSolicitacaoUniformes.FormShow(Sender: TObject);
begin

  //Verifica se o perfil logado � permitido fazer o cadastro (12)
  if (vvIND_PERFIL in [1, 12])  then
  begin
    vvMUDA_ABA_UNIFORME := True;
    PageControl1.Pages[0].Enabled := True;
  end
  else
  begin
    vvMUDA_ABA_UNIFORME := False;
    PageControl1.ActivePageIndex := 1;
    PageControl1.Pages[0].Enabled := False;
  end;

  mskDataSolicitacao.Text := DateToStr(Date());
  btnVerAlunosDaUnidade.Enabled := False;
  mskAno.Text := Copy(mskDataSolicitacao.Text,7,4);
  PageControl2.ActivePageIndex := 0;

  //Carrega os dados dos uniformes cadastrados
  with dmDeca.cdsSel_Uniformes do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
    Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
    Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
    Params.ParamByName ('@pe_numeracao').Value           := Null;
    Open;
    dbgUniformes.Refresh;
  end;

  vListaTipoUniforme1 := TStringList.Create();
  vListaTipoUniforme2 := TStringList.Create();
  cbTipoUniforme.Clear;
  cbTipoUniforme2.Clear;
  //Carrega a lista de tipos de uniformes...
  try
    with dmDeca.cdsSel_TipoUniforme do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
      Params.ParamByName ('@pe_dsc_tipouniforme').Value   := Null;
      Open;

      while not (dmDeca.cdsSel_TipoUniforme.eof) do
      begin
        cbTipoUniforme.Items.Add (FieldByName('dsc_tipouniforme').Value);
        cbTipoUniforme2.Items.Add (FieldByName('dsc_tipouniforme').Value);
        vListaTipoUniforme1.Add(IntToStr(FieldByName('cod_id_tipouniforme').Value));
        vListaTipoUniforme2.Add(FieldByName('dsc_tipouniforme').Value);
        dmDeca.cdsSel_TipoUniforme.Next;
      end;

    end;
  except end;

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de Unidades para uma poss�vel consulta
  //aos dados da frequencia de determinada unidade...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('flg_status').AsInteger = 1) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;

  //cbUnidade.ItemIndex := StrToInt(vListaUnidade1.Strings[vvCOD_UNIDADE]);


end;

procedure TfrmSolicitacaoUniformes.ValidaCampos;
var vOK: Boolean;
begin

  vOK := True;


  if (cbTipoUniforme.ItemIndex = -1) and (vOK) then
  begin
    vOK := False;
    Application.MessageBox('Aten��o!!!'+#13+#10+
                           'O Campo TIPO DE UNIFORME deve ser selecionado.',
                           '[Sistema DECA] - Valida��o',
                           MB_OK + MB_ICONWARNING);
    cbTipoUniforme.SetFocus;
  end else
  begin
    if (cbCor.ItemIndex = -1) and (vOK) then
    begin
      vOK := False;
      Application.MessageBox('Aten��o!!!'+#13+#10+
                             'O Campo COR DO UNIFORME deve ser selecionado.',
                             '[Sistema DECA] - Valida��o',
                             MB_OK + MB_ICONWARNING);
      cbCor.SetFocus;
    end else
    begin
      if (cbSexo.ItemIndex = -1) and (vOK) then
      begin
        vOK := False;
        Application.MessageBox('Aten��o!!!'+#13+#10+
                               'O Campo SEXO DO UNIFORME deve ser selecionado.',
                               '[Sistema DECA] - Valida��o',
                               MB_OK + MB_ICONWARNING);
        cbSexo.SetFocus;
      end else
      begin
        if (cbNumeracao.ItemIndex = -1) and (vOK) then
        begin
          vOK := False;
          Application.MessageBox('Aten��o!!!'+#13+#10+
                                 'O Campo NUMERA��O DO UNIFORME deve ser selecionado.',
                                 '[Sistema DECA] - Valida��o',
                                 MB_OK + MB_ICONWARNING);
          cbNumeracao.SetFocus;
        end else
        begin
          if (Length(edQtdParaAdmissao.Text)=0) and (vOK) then
          begin
            vOK := False;
            Application.MessageBox('Aten��o!!!'+#13+#10+
                                   'O Campo QUANTIDADE M�XIMA PARA ADMISS�O DO UNIFORME deve ser selecionado.',
                                   '[Sistema DECA] - Valida��o',
                                   MB_OK + MB_ICONWARNING);
            edQtdParaAdmissao.SetFocus;
          end else
          begin
            if (Length(edQtdParaReposicao.Text)=0) and (vOK) then
            begin
              vOK := False;
              Application.MessageBox('Aten��o!!!'+#13+#10+
                                     'O Campo QUANTIDADE M�XIMA PARA REPOSI��O DO UNIFORME deve ser selecionado.',
                                     '[Sistema DECA] - Valida��o',
                                     MB_OK + MB_ICONWARNING);
              edQtdParaReposicao.SetFocus;
            end else
            begin
              if (cbPeriodoReposicao.ItemIndex = -1) and (vOK) then
              begin
                vOK := False;
                Application.MessageBox('Aten��o!!!'+#13+#10+
                                       'O Campo PER�ODO M�NIMO DE REPOSI��O DO UNIFORME deve ser selecionado.',
                                       '[Sistema DECA] - Valida��o',
                                       MB_OK + MB_ICONWARNING);
                cbPeriodoReposicao.SetFocus;
              end else
              begin
                vOK := True;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;  

procedure TfrmSolicitacaoUniformes.btnAlterarUniformeClick(
  Sender: TObject);
begin

  if Application.MessageBox('Deseja ALTERAR DADOS DO UNIFORME?',
                            '[Sistema Deca] - Altera��o de Uniforme',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    try
      with dmDeca.cdsUpd_Uniforme do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_uniforme').Value       := dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
        Params.ParamByName ('@pe_cod_id_tipouniforme').Value   := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme.ItemIndex]);
        Params.ParamByName ('@pe_ind_cor').Value               := cbCor.ItemIndex;
        Params.ParamByName ('@pe_ind_sexo').Value              := cbSexo.ItemIndex;
        Params.ParamByName ('@pe_qtd_admissao').Value          := StrToInt(edQtdParaAdmissao.Text);
        Params.ParamByName ('@pe_qtd_reposicao').Value         := StrToInt(edQtdParaReposicao.Text);
        Params.ParamByName ('@pe_ind_periodo_reposicao').Value := cbPeriodoReposicao.ItemIndex;
        Params.ParamByName ('@pe_ind_numeracao').Value         := cbNumeracao.ItemIndex;
        Execute;

        Application.MessageBox('Aten��o!!!'+#13+#10+
                               'Os dados foram alterados com sucesso.',
                               '[Sistema DECA] - Sucesso',
                               MB_OK + MB_ICONINFORMATION);
        ModoTela('P');
        with dmDeca.cdsSel_Uniformes do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
          Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
          Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
          Params.ParamByName ('@pe_numeracao').Value           := Null;
          Open;
          dbgUniformes.Refresh;
        end;

      end;
    except
        Application.MessageBox('Aten��o!!!'+#13+#10+
                               'Um ERRO ocorreu na tentativa de inserir os dados do uniforme.'+#13+#10+
                               'Verifique internet e/ou conex�o de rede e tente novamente.',
                               '[Sistema DECA] - Erro de Inser��o',
                               MB_OK + MB_ICONWARNING);
        ModoTela('P');
    end;
  end;
end;

procedure TfrmSolicitacaoUniformes.btnGerarSolicitacaoClick(Sender: TObject);
var contador : Integer;
begin

  if Application.MessageBox ('Deseja GERAR as solicita��es de acordo com as informa��es?',
                             '[Sistema Deca] - Confirma��o de Solicita��o',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    with dbgAlunos do
    begin
      for contador := 0 to Pred(SelectedRows.Count) do
      begin
        DataSource.DataSet.BookMark := SelectedRows[contador];
        //Validar os dados da solcita��o...
        //...
        try

          //Para cada solicita��o, verificar se a mesma j� se encontra gravada na tabela de solicita��es
          with dmDeca.cdsSel_ConsultaSolicitacao do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').value   := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_mes_solicitacao').value := cbMes.ItemIndex;
            Params.ParamByName ('@pe_ano_solicitacao').value := StrToInt(mskAno.Text);
            Params.ParamByName ('@pe_cod_id_uniforme').value := dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
            Open;

            if (dmDeca.cdsSel_ConsultaSolicitacao.RecordCount > 0) then
            begin

            end
            else
            begin
              //N�o encontrou solicta��o anterior ent�o grava no banco
              with dmDeca.cdsInc_UniformeSolicitacao do
              begin
                Close;
                Params.ParamByName ('@pe_dat_solicitacao').Value := StrToDate(mskDataSolicitacao.Text);
                Params.ParamByName ('@pe_cod_matricula').Value   := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_mes_solicitacao').Value := cbMes.ItemIndex;
                Params.ParamByName ('@pe_ano_solicitacao').Value := StrToInt(mskAno.Text);
                Params.ParamByName ('@pe_cod_id_uniforme').Value := dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
                Params.ParamByName ('@pe_qtd_uniforme').Value    := StrToInt(edQtdSolicitada.Text);
                Params.ParamByName ('@pe_tam_uniforme').Value    := cbNumeracao2.Text; //dmDeca.cdsSel_Uniformes.FieldByName ('vNumeracao').Value;
                Execute;
              end;
            end;                                                                                  

            //Criar procedimento para incluir o registro de solicita��o de uniformes no Hist�rico

          end;



        except
          Application.MessageBox('Aten��o!!!'+#13+#10+
                                 'Um ERRO ocorreu na tentativa de inserir os dados da solicita��o.'+#13+#10+
                                 'Verifique internet e/ou conex�o de rede e tente novamente.',
                                 '[Sistema DECA] - Erro de Inser��o',
                                 MB_OK + MB_ICONWARNING);
        end;
      end; //for contador ...


    end; //fim with dbgAlunos...
  end
  else
  begin
    //Cancelado pelo usu�rio...
  end;

end;

procedure TfrmSolicitacaoUniformes.cbTipoUniforme2Click(Sender: TObject);
begin

  //vListaNumeracao1 := TStringList.Create();
  //vListaNumeracao2 := TStringList.Create();

  try
    with dmDeca.cdsSel_Uniformes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme2.ItemIndex]);
      Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
      Params.ParamByName ('@pe_numeracao').Value           := Null;
      Open;

      cbNumeracao2.Clear;
      while not eof do
      begin
        //Carrega a lista de numeracao
        //vListaNumeracao1.Add (IntToStr(FieldByName('COD_ID_UNIFORME').Value));
        //vListaNumeracao2.Add (FieldByName('vNumeracao').Value);
        cbNumeracao2.Items.Add (FieldByName('vNumeracao').Value);
        Next;
      end;

    end;
  except end;
end;

procedure TfrmSolicitacaoUniformes.cbNumeracao2Click(Sender: TObject);
begin
{  try
    with dmDeca.cdsSel_Uniformes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme2.ItemIndex]);
      Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
      Params.ParamByName ('@pe_numeracao').Value           := cbNumeracao2.Text;
      Open;
      dbgUniformesParaSolicitacao.Refresh;
    end;
  except end;
  }
end;

procedure TfrmSolicitacaoUniformes.edQtdSolicitadaEnter(Sender: TObject);
begin
  edQtdSolicitada.Color := $0078FE96;
end;

procedure TfrmSolicitacaoUniformes.edQtdSolicitadaExit(Sender: TObject);
begin
  edQtdSolicitada.Color := clWindow;
  //edTamanho.SetFocus;
end;

procedure TfrmSolicitacaoUniformes.edTamanhoEnter(Sender: TObject);
begin
  //edTamanho.Color := $0078FE96;
end;

procedure TfrmSolicitacaoUniformes.edTamanhoExit(Sender: TObject);
begin
  //edTamanho.Color := clWindow;
end;

procedure TfrmSolicitacaoUniformes.dbgUniformesParaSolicitacaoCellClick(
  Column: TColumn);
begin
  edTamanho.Text := dmDeca.cdsSel_Uniformes.FieldByName('vNumeracao').Value;
end;

procedure TfrmSolicitacaoUniformes.btnVerAlunosDaUnidadeClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;
      dbgAlunos.Refresh;
    end;

    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes.ItemIndex;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := Trim(mskAno.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;  //Apenas os que ainda est�o liberados
      Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;
      dbgSOLICITACOES.Refresh;
    end;
  except end;
end;

procedure TfrmSolicitacaoUniformes.cbMesClick(Sender: TObject);
begin
  btnVerAlunosDaUnidade.Enabled := True;
end;

procedure TfrmSolicitacaoUniformes.btnCONSULTA_SOLICITACOESClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes.ItemIndex;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := Trim(mskAno.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;  //Apenas os que ainda est�o liberados
      Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;
      dbgSOLICITACOES.Refresh;
    end;
  except end;
end;

procedure TfrmSolicitacaoUniformes.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        try
          with dmDeca.cdsSel_UniformesSolicitacoes do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
            Params.ParamByName ('@pe_cod_matricula').Value      := txtMatricula.Text;
            Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes.ItemIndex;
            Params.ParamByName ('@pe_ano_solicitacao').Value    := Trim(mskAno.Text);
            Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
            Params.ParamByName ('@pe_flg_bloqueado').Value      := 0;  //Apenas os que ainda est�o liberados
            Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
            Open;
            dbgSOLICITACOES.Refresh;
          end;
        except end;

      end;
    except end;
  end;
end;

procedure TfrmSolicitacaoUniformes.dbgSOLICITACOESDblClick(
  Sender: TObject);
begin
  with dmDeca.cdsSel_UniformesSolicitacoes do
  begin

    if FieldByName('flg_bloqueado').Value = 0 then
    begin
      edTipoUniforme.Text := FieldByName ('dsc_tipouniforme').Value;
      edCor.Text          := FieldByName ('vCorUniforme').Value;
      edNumeracao.Text    := FieldByName ('tam_uniforme').Value;
      edQtd.Text          := IntToStr(FieldByName ('qtd_uniforme').Value);
      edNumeracao.SetFocus;
      btnAlterarUniforme.Enabled := False;
    end
    else
    begin
      Application.MessageBox ('Aten��o!!!' +#13+#10+
                              'Pedido gerado para a solicita��o atual.' +#13+#10+
                              'Imposs�vel fazer altera��es. Em caso de d�vidas entre em contato ' +#13+#10+
                              'com a equipe da Divis�o de Suprimentos/Almoxarifado.',
                              '[Sistema Deca] - Altera��o de Solicita��o',
                              MB_OK + MB_ICONWARNING);
      btnAlterarUniforme.Enabled := False;
    end;

  end;
  btnAlterarUniforme.Enabled := True;
end;

procedure TfrmSolicitacaoUniformes.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA_UNIFORME = False then
    AllowChange := False
  else AllowChange := True;
end;

procedure TfrmSolicitacaoUniformes.btnALTERAR_SOLICITACAOClick(
  Sender: TObject);
begin
  //Fazer a altera��o dos dados e desabilita o bot�o de altera��o...
  try
    with dmDeca.cdsUpd_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_qtd_uniforme').AsInteger       := GetValue(edQtd.Text);
      Params.ParamByName ('@pe_tam_uniforme').AsString        := GetValue(edNumeracao.Text);
      Params.ParamByName ('@pe_cod_id_solicitacao').AsInteger := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_id_solicitacao').Value;
      Execute;
    end;
  except end;
  btnAlterarUniforme.Enabled := False;
end;

procedure TfrmSolicitacaoUniformes.btnExcluirSolicitacaoClick(
  Sender: TObject);
begin
  try

    if dmDeca.cdsSel_UniformesSolicitacoes.FieldByName('flg_bloqueado').Value = 0 then
    begin

      with dmDeca.cdsDel_UniformesSolicitacao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_solicitacao').AsInteger := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName('cod_id_solicitacao').AsInteger;
        Execute;
      end;

      with dmDeca.cdsSel_UniformesSolicitacoes do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
        Params.ParamByName ('@pe_cod_matricula').Value      := Null;
        Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes.ItemIndex;
        Params.ParamByName ('@pe_ano_solicitacao').Value    := Trim(mskAno.Text);
        Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
        Params.ParamByName ('@pe_flg_bloqueado').Value      := 0;  //Apenas os que ainda est�o liberados
        Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
        Open;
        dbgSOLICITACOES.Refresh;
      end;
    end
    else
    begin
      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'A solicita��o j� foi PEDIDA e N�O pode ser exclu�da.',
                              '[Sistema Deca] - Exclus�o de Item',
                              MB_OK + MB_ICONWARNING);
    end;
  except end;
end;

procedure TfrmSolicitacaoUniformes.btnGerarPedidosClick(Sender: TObject);
begin
  Application.CreateForm(TfrmGerarPedidoUniformes, frmGerarPedidoUniformes);
  frmGerarPedidoUniformes.ShowModal;
end;

procedure TfrmSolicitacaoUniformes.btnRelacaoUniformesClick(
  Sender: TObject);
begin
  Application.CreateForm (TrelUniformes, relUniformes);
  relUniformes.Preview;
  relUniformes.Free;
end;

procedure TfrmSolicitacaoUniformes.SpeedButton2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConsultarPedidosUniformes, frmConsultarPedidosUniformes);
  frmConsultarPedidosUniformes.ShowModal;
end;

end.
