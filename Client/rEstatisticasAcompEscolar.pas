unit rEstatisticasAcompEscolar;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg, TeEngine, Series,
  TeeProcs, Chart, DBChart, QrTee;

type
  TrelEstatisticasAcompEscolar = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRSysData1: TQRSysData;
    lbTitulo: TQRLabel;
    QRLabel6: TQRLabel;
    QRExpr1: TQRExpr;
  private

  public

  end;

var
  relEstatisticasAcompEscolar: TrelEstatisticasAcompEscolar;

implementation

uses uDM;

{$R *.DFM}

end.
