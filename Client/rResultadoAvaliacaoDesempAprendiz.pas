unit rResultadoAvaliacaoDesempAprendiz;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelResultadoAvaliacaoDesempAprendiz = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel18: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRLabel31: TQRLabel;
    qrlTitulo: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    lbPeriodo: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRExpr1: TQRExpr;
    procedure QRDBText4Print(sender: TObject; var Value: String);
    procedure QRDBText5Print(sender: TObject; var Value: String);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText7Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRDBText10Print(sender: TObject; var Value: String);
    procedure QRDBText11Print(sender: TObject; var Value: String);
    procedure QRDBText12Print(sender: TObject; var Value: String);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure QRDBText15Print(sender: TObject; var Value: String);
  private

  public

  end;

var
  relResultadoAvaliacaoDesempAprendiz: TrelResultadoAvaliacaoDesempAprendiz;

implementation

uses uDM;

{$R *.DFM}

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText4Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem01').Value = 'O' then
  begin
    QRDBText4.Font.Color := clGreen;
    QRDBText4.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem01').Value = 'X' then
  begin
    QRDBText4.Font.Color := clRed;
    QRDBText4.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText4.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText5Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem02').Value = 'O' then
  begin
    QRDBText5.Font.Color := clGreen;
    QRDBText5.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem02').Value = 'X' then
  begin
    QRDBText5.Font.Color := clRed;
    QRDBText5.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText5.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText6Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem03').Value = 'O' then
  begin
    QRDBText6.Font.Color := clGreen;
    QRDBText6.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem03').Value = 'X' then
  begin
    QRDBText6.Font.Color := clRed;
    QRDBText6.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText6.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText7Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem04').Value = 'O' then
  begin
    QRDBText7.Font.Color := clGreen;
    QRDBText7.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem04').Value = 'X' then
  begin
    QRDBText7.Font.Color := clRed;
    QRDBText7.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText7.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText8Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem05').Value = 'O' then
  begin
    QRDBText8.Font.Color := clGreen;
    QRDBText8.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem05').Value = 'X' then
  begin
    QRDBText8.Font.Color := clRed;
    QRDBText8.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText8.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText9Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem06').Value = 'O' then
  begin
    QRDBText9.Font.Color := clGreen;
    QRDBText9.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem06').Value = 'X' then
  begin
    QRDBText9.Font.Color := clRed;
    QRDBText9.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText9.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText10Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem07').Value = 'O' then
  begin
    QRDBText10.Font.Color := clGreen;
    QRDBText10.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem07').Value = 'X' then
  begin
    QRDBText10.Font.Color := clRed;
    QRDBText10.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText10.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText11Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem08').Value = 'O' then
  begin
    QRDBText11.Font.Color := clGreen;
    QRDBText11.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem08').Value = 'X' then
  begin
    QRDBText11.Font.Color := clRed;
    QRDBText11.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText11.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText12Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem09').Value = 'O' then
  begin
    QRDBText12.Font.Color := clGreen;
    QRDBText12.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem09').Value = 'X' then
  begin
    QRDBText12.Font.Color := clRed;
    QRDBText12.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText12.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText13Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem10').Value = 'O' then
  begin
    QRDBText13.Font.Color := clGreen;
    QRDBText13.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem10').Value = 'X' then
  begin
    QRDBText13.Font.Color := clRed;
    QRDBText13.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText13.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText14Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem11').Value = 'O' then
  begin
    QRDBText14.Font.Color := clGreen;
    QRDBText14.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem11').Value = 'X' then
  begin
    QRDBText14.Font.Color := clRed;
    QRDBText14.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText14.Font.Color := clBlack;
  end;
end;

procedure TrelResultadoAvaliacaoDesempAprendiz.QRDBText15Print(
  sender: TObject; var Value: String);
begin
  if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem12').Value = 'O' then
  begin
    QRDBText15.Font.Color := clGreen;
    QRDBText15.Font.Style := [fsBold];
  end
  else if dmDeca.cdsSel_AvaliacoesApr.FieldByName('vItem12').Value = 'X' then
  begin
    QRDBText15.Font.Color := clRed;
    QRDBText15.Font.Style := [fsBold];
  end
  else
  begin
    QRDBText15.Font.Color := clBlack;
  end;
end;

end.
