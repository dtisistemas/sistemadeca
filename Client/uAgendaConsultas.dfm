object frmAgendaConsultas: TfrmAgendaConsultas
  Left = 264
  Top = 107
  BorderStyle = bsDialog
  Caption = 'Sistema Deca - [Agenda de Consultas]'
  ClientHeight = 551
  ClientWidth = 718
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 2
    Top = 3
    Width = 713
    Height = 545
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Consultas'
      object Label1: TLabel
        Left = 11
        Top = 20
        Width = 81
        Height = 13
        Caption = 'Data da consulta'
      end
      object DateTimePicker1: TDateTimePicker
        Left = 97
        Top = 17
        Width = 144
        Height = 21
        CalAlignment = dtaLeft
        Date = 39232.3587972685
        Time = 39232.3587972685
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 4
        Top = 48
        Width = 699
        Height = 289
        BevelInner = bvLowered
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 8
          Top = 8
          Width = 684
          Height = 273
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object Panel2: TPanel
        Left = 4
        Top = 339
        Width = 589
        Height = 123
        BevelInner = bvLowered
        TabOrder = 2
        object Label2: TLabel
          Left = 42
          Top = 20
          Width = 59
          Height = 13
          Caption = 'Matr�cula:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnPesquisar: TSpeedButton
          Left = 195
          Top = 14
          Width = 28
          Height = 24
          Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777770000777777777777777700770000777777777777777C440700007777
            7777777777C4C40700007777777777777C4C4077000077777777777784C40777
            0000777777788888F740777700007777770000007807777700007777707EFEE6
            007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
            8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
            0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
            7777777700A077777777777777777777FFA077777777777777777777FFFF7777
            7777777777777777FF81}
          ParentShowHint = False
          ShowHint = True
        end
        object lbNome: TLabel
          Left = 242
          Top = 19
          Width = 265
          Height = 18
          Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 18
          Top = 91
          Width = 85
          Height = 13
          Caption = 'Data Consulta:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 198
          Top = 91
          Width = 85
          Height = 13
          Caption = 'Hora Consulta:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 49
          Top = 43
          Width = 52
          Height = 13
          Caption = 'Unidade:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 30
          Top = 68
          Width = 71
          Height = 13
          Caption = 'Nascimento:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txtMatricula: TMaskEdit
          Left = 108
          Top = 16
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '!99999999;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '        '
        end
        object MaskEdit1: TMaskEdit
          Left = 108
          Top = 88
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '99/99/9999'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = '  /  /    '
        end
        object MaskEdit2: TMaskEdit
          Left = 288
          Top = 88
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '99:99'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 5
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '  :  '
        end
        object edUnidade: TEdit
          Left = 108
          Top = 40
          Width = 413
          Height = 21
          Hint = 'UNIDADE ATUAL'
          Color = clInfoBk
          ReadOnly = True
          TabOrder = 3
        end
        object edNascimento: TMaskEdit
          Left = 108
          Top = 64
          Width = 84
          Height = 21
          Hint = 'DATA DE NASCIMENTO'
          Color = clInfoBk
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 4
          Text = '  /  /    '
        end
      end
      object Panel3: TPanel
        Left = 4
        Top = 466
        Width = 698
        Height = 49
        TabOrder = 3
        object btnNovo: TBitBtn
          Left = 348
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para marcar nova consulta'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvar: TBitBtn
          Left = 423
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para gravar a consulta'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnCancelar: TBitBtn
          Left = 483
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para cancelar a opera��o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnSair: TBitBtn
          Left = 633
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para SAIR'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
      end
      object btnImprimir: TBitBtn
        Left = 630
        Top = 378
        Width = 46
        Height = 42
        Hint = 'Clique para IMPRIMIR agenda do dia'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Glyph.Data = {
          FE0A0000424DFE0A00000000000036000000280000001E0000001E0000000100
          180000000000C80A0000C30E0000C30E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000000000FFFFFFFFFFFFFF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFF0000000000
          00C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000000000FFFFFFFFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFF000000C0C0C0000000
          C0C0C0C0C0C0C0C0C0C0C0C000000000000000FF000080FFFFFFFFFFFFFF0000
          000000808080FFFFFFFF0000000000808080FFFFFFFF0000000000808080FFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFC0C0C0000000C0
          C0C0C0C0C0C0C0C00000000000FF0000FF0000FF0000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
          FF0000FF0000FF0000FF0000FF0000000000C0C0C0FFFFFFC0C0C0000000C0C0
          C0C0C0C00000000000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
          FFFFFFFFFFFF0000FFFFFF000000C0C0C0C0C0C0FFFFFFC0C0C0000000C0C0C0
          0000000000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
          FFFF0000FFFFFF00000000000000000000000000000000000000000000000000
          000000FF000080FFFFFFFFFFFFFF0000000000808080FFFFFFFF000000000080
          8080FFFFFFFF0000000000808080FFFFFFFF0000000000808080FFFFFFFF0000
          000000808080FFFFFFFF0000000000808080FFFFFF0000000000000000FF0000
          FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
          00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF00000000000000000000FFFFFFFFFFFFFF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFFFFFFFF00000000000000000000FF000080FFFFFFFFFFFFFF0000
          000000808080FFFFFFFF0000000000808080FFFFFFFF00000000800000800000
          80FF0000000000808080FFFFFFFF0000000000808080FFFFFFFF000000000080
          8080FFFFFF0000000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF00000000800000FF0000FF0000FF000080
          FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
          000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFF0000800000FFFFFFFFFFFFFFFFFFFF0000FF000080FF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000
          0000000000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFF0000800000FFFFFFFF808080FFFFFF0000FF000080FFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF00000000000000
          000000FF000080FFFFFFFFFFFFFF0000000000808080FFFFFFFF000000000080
          80800000800000FF808080808080FFFFFF0000FF000080FFFFFFFFFFFFFF0000
          000000808080FFFFFFFF0000000000808080FFFFFF0000000000000000FF0000
          FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
          000000800000FF0000FF0000FF000080FF0000FF0000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF00000000000000000000FFFFFFFFFFFFFF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          000080000080000080FF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
          FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF0000
          FFFFFFFFFFFFFFFFFFFF0000000000808080FFFFFFFF0000000000808080FFFF
          FFFF0000000000808080FFFFFFFF0000000000808080FFFFFFFF000000000080
          8080FFFFFF0000000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
          FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
          000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
          0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
          00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0000800000FF0000FF0000FFFFFFFFFFFFFF0000000000000000FFFFFF
          FFFFFFFFFFFFFFFFFF000000000000000000FFFFFF000000000000FFFFFF0000
          00000000FFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF0000800000FFFFFFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          800000FFFFFFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFF
          FF808080000000000000000000000000000000000000FFFFFF00000000000000
          0000000000000000808080FFFFFFFFFFFFFFFFFFFFFFFF0000800000FF0000FF
          FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF808080
          000000000000000000000000000000000000FFFFFF0000000000000000000000
          00000000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000800000FFFFFFFFFF
          FFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
    end
  end
end
