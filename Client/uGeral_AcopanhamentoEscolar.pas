unit uGeral_AcopanhamentoEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, ExtCtrls, Buttons, StdCtrls, Grids, DBGrids, ComCtrls, Db, QRExport, Excel97, ComObj;

type
  TfrmGeral_AcompanhamentoEscolar = class(TForm)
    Image1: TImage;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cbEscola: TComboBox;
    chkTodas: TCheckBox;
    edAnoLetivo: TEdit;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    btnExportar: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ProgressBar1: TProgressBar;
    dbgDadosBoletim: TDBGrid;
    dsSel_Boletim: TDataSource;
    btnPesquisar: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Panel1: TPanel;
    SpeedButton3: TSpeedButton;
    SaveDialog1: TSaveDialog;
    SaveDialog2: TSaveDialog;
    SpeedButton4: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure chkTodasClick(Sender: TObject);
    procedure cbEscolaEnter(Sender: TObject);
    procedure cbEscolaClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeral_AcompanhamentoEscolar: TfrmGeral_AcompanhamentoEscolar;
  vListaEscola1, vListaEscola2 : TStringList;
  mmTURMA1B, mmTURMA2B, mmTURMA3B, mmTURMA_ATUAL : String[5];
  objExcel3, Sheet3 : Variant;
  lin : Integer;

  //objExcel2, Sheet2 : Variant;

implementation

uses rControleAcompanhamentoEscolar, uPrincipal, uDM,
  uExportarDadosAcompEscolar, uUtil;

{$R *.DFM}

procedure TfrmGeral_AcompanhamentoEscolar.FormShow(Sender: TObject);
begin

  vListaEscola1 := TStringList.Create();
  vListaEscola2 := TStringList.Create();

  Panel3.Caption := 'Total de registros encontrados: ';

  try

    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      while not (dmDeca.cdsSel_Escola.eof) do
      begin
        cbEscola.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
        vListaEscola1.Add (IntToStr(dmDeca.cdsSel_Escola.FieldByName ('cod_escola').Value));
        vListaEscola2.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
        dmDeca.cdsSel_Escola.Next;
      end;
    end;

    {
    Application.MessageBox ('Bem-vindo ao m�dulo de Monitoramento do ' +#13+#10+
                            'Acompanhamento Escolar. Primeiramente selecione o ANO LETIVO' + #13+#10+
                            'e clique em Pesquisar. Depois, basta selecionar a escola e ' +#13+#10+
                            'clicar em FILTRAR ou deixe a op��o TODAS selecionada e clique em FILTRAR.' +#13+#10+
                            'Bom trabalho!!!',
                            '[Sistema Deca] - Monitoramento Acompanhamento Escolar',
                            MB_OK + MB_ICONINFORMATION);
    }
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10+
                            'Um erro ocorreu ao tentar carregar os dados das Escolas.' +#13+#10 +
                            'Verifique a conex�o com a INTERNET e caso o erro persista, informe ao Administrador do Sistema.',
                            '[Sistema Deca] - Erro carregando Escolas...',
                            MB_OK + MB_ICONERROR);
    //Limpa e "trava" os campos...
    cbEscola.ItemIndex := -1;
    chkTodas.Checked    := False; 
  end;
end;

procedure TfrmGeral_AcompanhamentoEscolar.SpeedButton2Click(
  Sender: TObject);
begin
  vListaEscola1.Free;
  vListaEscola2.Free;
  frmGeral_AcompanhamentoEscolar.Close;
end;

procedure TfrmGeral_AcompanhamentoEscolar.btnPesquisarClick(
  Sender: TObject);
begin

    //Excluir os dados referentes ao usu�rio logado referentes ao Boletim
    with dmDeca.cdsExc_Boletim do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Execute;
    end;

    //Seleciona os dados do cadastro para a unidade selecionada...
    with dmDeca.cdsSel_CadastroACOMPESCOLAR do
    begin
      Close;
      Open;

      ProgressBar1.Max := dmDeca.cdsSel_CadastroACOMPESCOLAR.RecordCount;
      ProgressBar1.Position := 0;

      while not dmDeca.cdsSel_CadastroACOMPESCOLAR.eof do
      begin

        //Incluir os dados na tabela BOLETIM...
        with dmDeca.cdsInc_Boletim do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value  := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
          Params.ParamByName ('@pe_nom_nome').Value       := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('nom_nome').Value;
          Params.ParamByName ('@pe_num_rg_escolar').Value := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('dsc_rg_escolar').Value;
          Params.ParamByName ('@pe_dat_nascimento').Value := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('dat_nascimento').Value;
          Params.ParamByName ('@pe_cod_unidade').Value    := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('cod_unidade').Value;

          //Seleciona a escola e s�rie atuais
          with dmDeca.cdsSel_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_cod_escola').Value := Null;
            Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAnoLetivo.Text);
            Open;

            if dmDeca.cdsSel_HistoricoEscola.RecordCount > 0 then
            begin
              while not (dmDeca.cdsSel_HistoricoEscola.eof) do
              begin
                //Verifica se � escola/s�rie atuais...
                if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 0) then
                begin
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value;
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := dmDeca.cdsSel_HistoricoEscola.FieldByName ('dsc_serie').Value;
                  dmDeca.cdsSel_HistoricoEscola.Last;
                end
                //Ou n�o � escola/s�rie atuais...
                else if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 1) then
                begin
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := '--';
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := '--';
                  dmDeca.cdsSel_HistoricoEscola.Last;
                end;
              end;
            end
            else
            begin
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := ' ';
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := ' ';
            end;
          end; //with dmDeca.cdsSel_HistoricoEscola do


          {*************** Consultar dados do Acompanhamento Escolar *******************************}
          //1.� Bimestre do ano informado na tela
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
            Params.ParamByName ('@pe_num_bimestre').Value          := 1;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
            Open;

            //Se encontrou registros do Acomp. Escolar referentes ao 1.� Bimestre
            if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
            begin
              //Avalia o ind_frequencia
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '+75%';
                1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '-75%';
                2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '--';
                3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'FLX';
                4 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'DE';
              end;

              //Avalia o ind_aproveitamento
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AB';
                1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'DE';
                2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AC';
                3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := '--';
              end;

              //Repassa o valor de vSituacaoBimestre para o campo DSC_SIT_1B na tabela BOLETIM
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_sit_1b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoBimestre').Value;

            end
            else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
            begin
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := ' ';
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := ' ';
            end;
          end; //with dmDeca.cdsSel_AcompEscolar do... 1.� bimestre

          //Recupera a Turma - 1.� Bimestre
          if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
            mmTURMA1B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
          else
            mmTURMA1B := '-';


          {*************** Consultar dados do Acompanhamento Escolar ***************}
          //2.� Bimestre do ano informado na tela
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnOLetivo.Text);
            Params.ParamByName ('@pe_num_bimestre').Value          := 2;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
            Open;

            //Se encontrou registros do Acomp. Escolar referentes ao 2.� Bimestre
            if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
            begin
              //Avalia o ind_frequencia
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '+75%';
                1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '-75%';
                2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '--';
                3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'FLX';
                4 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'DE';
              end;

              //Avalia o ind_aproveitamento
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AB';
                1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'DE';
                2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AC';
                3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := '--';
              end;

              //Repassa o valor de vSituacaoBimestre para o campo DSC_SIT_2B na tabela BOLETIM
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_sit_2b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoBimestre').Value;

            end
            else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
            begin
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value  := ' ';
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := ' ';
            end;
          end; //with dmDeca.cdsSel_AcompEscolar do... 2.� bimestre

          //Recupera a Turma - 2.� Bimestre
          if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
            mmTURMA2B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
          else
            mmTURMA2B := '-';


          {*************** Consultar dados do Acompanhamento Escolar ***************}
          //3.� Bimestre do ano informado na tela
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_CadastroACOMPESCOLAR.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
            Params.ParamByName ('@pe_num_bimestre').Value          := 3;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
            Open;

            //Se encontrou registros do Acomp. Escolar referentes ao 3.� Bimestre
            if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
            begin
              //Avalia o ind_frequencia
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '+75%';
                1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '-75%';
                2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '--';
                3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'FLX';
                4 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'DE';
              end;

              //Avalia o ind_aproveitamento
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AB';
                1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'DE';
                2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AC';
                3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := '--';
              end;

              //Repassa o valor de vSituacaoBimestre para o campo DSC_SIT_3B na tabela BOLETIM
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_sit_3b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoBimestre').Value;

            end
            else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
            begin
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value  := ' ';
              dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := ' ';
            end;
          end; //with dmDeca.cdsSel_AcompEscolar do... 3.� bimestre

          //Recupera a Turma - 3.� Bimestre
          if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
            mmTURMA3B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
          else
            mmTURMA3B := '-';

          dmDeca.cdsInc_Boletim.Execute;

        end; //dmDeca.Inc_Boletim...
        ProgressBar1.Position := ProgressBar1.Position + 1;
        dmDeca.cdsSel_CadastroACOMPESCOLAR.Next;
      end; //while not dmDeca.cdsSel_CadastroACOMPESCOLAR.eof do


      //Seleciona os dados do Boletim para o usu�rio atual...
      with dmDeca.cdsSel_Boletim do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value  := Null;
        Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
        Params.ParamByName ('@pe_nom_escola').Value     := Null;
        Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
        Params.ParamByName ('@pe_ind_status').Value     := Null;
        Params.ParamByName ('@pe_flg_defasagem').Value  := Null;
        Open;
        dbgDadosBoletim.Refresh;
      end;

    end; //with dmDeca.cdsSel_CadastroACOMPESCOLAR do

end;
procedure TfrmGeral_AcompanhamentoEscolar.SpeedButton1Click(
  Sender: TObject);
begin

  //Seleciona os dados do Boletim para o usu�rio atual...
  with dmDeca.cdsSel_Boletim do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value  := Null;
    Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
    //Verifica qual escola est� selecionada...
    if (chkTodas.Checked) then
      Params.ParamByName ('@pe_nom_escola').Value   := Null
    else
      Params.ParamByName ('@pe_nom_escola').Value   := cbEscola.Text;

    Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
    Params.ParamByName ('@pe_ind_status').Value     := Null;
    Params.ParamByName ('@pe_flg_defasagem').Value  := Null;
    Open;
    dbgDadosBoletim.Refresh;

    Panel3.Caption := 'Total de registros encontrados: ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount);
  end;

end;

procedure TfrmGeral_AcompanhamentoEscolar.SpeedButton3Click(
  Sender: TObject);
begin
  Application.CreateForm (TrelControleAcompanhamentoEscolar, relControleAcompanhamentoEscolar);

  relControleAcompanhamentoEscolar.Prepare;
  relControleAcompanhamentoEscolar.qrlTotalPaginas.Caption := IntToStr(relControleAcompanhamentoEscolar.PageNumber);
  relControleAcompanhamentoEscolar.qrlUsuario.Caption := vvNOM_USUARIO;


  relControleAcompanhamentoEscolar.Preview;
  relControleAcompanhamentoEscolar.Free;
end;

procedure TfrmGeral_AcompanhamentoEscolar.btnExportarClick(
  Sender: TObject);
begin

  //Se estiver marcado TODAS, apenas exporta os dados para uma planilha GERAL...
  if (chkTodas.Checked) then
  begin

    //Criar a planilha, solicitando o local a ser salvo...
    objExcel3         := CreateOleObject('Excel.Application');
    //objExcel2.Caption := 'Teste';
    objExcel3.Workbooks.Add;
    SaveDialog1.Execute;
    //objExcel2.WorkBooks[1].SaveAs (ExtractFilePAth(Application.ExeName) + 'teste.xlsx');
    objExcel3.WorkBooks[1].SaveAs (SaveDialog1.FileName);
    objExcel3.Visible := True;
    objExcel3.WorkBooks[1].Sheets.Add;

    Sheet3 := objExcel3.WorkBooks[1].WorkSheets[1];
    //Sheet2.Name := 'Escola : ' + edNomeEscola.Text;

    Sheet3.Cells[1,1].Font.Bold := True;
    Sheet3.Cells[1,1].Font.Size := 8;
    Sheet3.Cells[1,1].Value  := 'RA';

    Sheet3.Cells[1,2].Font.Bold := True;
    Sheet3.Cells[1,2].Font.Size := 8;
    Sheet3.Cells[1,2].Value  := 'Nome do Aluno';

    Sheet3.Cells[1,3].Font.Bold := True;
    Sheet3.Cells[1,3].Font.Size := 8;
    Sheet3.Cells[1,3].Value  := 'Nascimento';

    Sheet3.Cells[1,4].Font.Bold := True;
    Sheet3.Cells[1,4].Font.Size := 8;
    Sheet3.Cells[1,4].Value  := 'Escola';

    Sheet3.Cells[1,5].Font.Bold := True;
    Sheet3.Cells[1,5].Font.Size := 8;
    Sheet3.Cells[1,5].Value  := 'S�rie Escolar';

    Sheet3.Cells[1,6].Font.Bold := True;
    Sheet3.Cells[1,6].Font.Size := 8;
    Sheet3.Cells[1,6].Value  := 'Unidade FUNDHAS';

    Sheet3.Cells[1,7].Font.Bold := True;
    Sheet3.Cells[1,7].Font.Size := 8;
    Sheet3.Cells[1,7].Value  := 'Apr. 1B';

    Sheet3.Cells[1,8].Font.Bold := True;
    Sheet3.Cells[1,8].Font.Size := 8;
    Sheet3.Cells[1,8].Value  := 'Freq. 1B';

    Sheet3.Cells[1,9].Font.Bold := True;
    Sheet3.Cells[1,9].Font.Size := 8;
    Sheet3.Cells[1,9].Value  := 'Sit. 1B';

    Sheet3.Cells[1,10].Font.Bold := True;
    Sheet3.Cells[1,10].Font.Size := 8;
    Sheet3.Cells[1,10].Value  := 'Apr. 2B';

    Sheet3.Cells[1,11].Font.Bold := True;
    Sheet3.Cells[1,11].Font.Size := 8;
    Sheet3.Cells[1,11].Value := 'Freq. 2B';

    Sheet3.Cells[1,12].Font.Bold := True;
    Sheet3.Cells[1,12].Font.Size := 8;
    Sheet3.Cells[1,12].Value := 'Sit. 2B';

    Sheet3.Cells[1,13].Font.Bold := True;
    Sheet3.Cells[1,13].Font.Size := 8;
    Sheet3.Cells[1,13].Value := 'Apr. 3B';

    Sheet3.Cells[1,14].Font.Bold := True;
    Sheet3.Cells[1,14].Font.Size := 8;
    Sheet3.Cells[1,14].Value := 'Freq. 3B';

    Sheet3.Cells[1,15].Font.Bold := True;
    Sheet3.Cells[1,15].Font.Size := 8;
    Sheet3.Cells[1,15].Value := 'Sit. 3B';

    Sheet3.Cells[1,16].Font.Bold := True;
    Sheet3.Cells[1,16].Font.Size := 8;
    Sheet3.Cells[1,16].Value := 'Observa��es'; 

    //Seleciona os dados do Boletim para o usu�rio atual...
    with dmDeca.cdsSel_Boletim do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value  := Null;
      Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value     := Null;
      Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
      Params.ParamByName ('@pe_ind_status').Value     := Null;
      Params.ParamByName ('@pe_flg_defasagem').Value  := Null;
      Open;

      lin := 2;
      while not dmDeca.cdsSel_Boletim.eof do
      begin
        Sheet3.Cells[lin,1].Font.Size := 8;
        Sheet3.Cells[lin,1].Value   := dmDeca.cdsSel_Boletim.FieldByName ('num_rg_escolar').AsString;

        Sheet3.Cells[lin,2].Font.Size := 8;
        Sheet3.Cells[lin,2].Value   := dmDeca.cdsSel_Boletim.FieldByName ('nom_nome').AsString;

        Sheet3.Cells[lin,3].Font.Size := 8;
        Sheet3.Cells[lin,3].Value := dmDeca.cdsSel_Boletim.FieldByName ('DatNascimento').AsString;

        //Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('DatNascimento').Value +';');

        Sheet3.Cells[lin,4].Font.Size := 8;
        Sheet3.Cells[lin,4].Value   := dmDeca.cdsSel_Boletim.FieldByName ('nom_escola').AsString;

        Sheet3.Cells[lin,5].Font.Size := 8;
        Sheet3.Cells[lin,5].Value   := dmDeca.cdsSel_Boletim.FieldByName ('dsc_serie').AsString;

        Sheet3.Cells[lin,6].Font.Size := 8;
        Sheet3.Cells[lin,6].Value   := dmDeca.cdsSel_Boletim.FieldByName ('nom_unidade').AsString;

        Sheet3.Cells[lin,7].Font.Size := 8;
        Sheet3.Cells[lin,7].Value   := dmDeca.cdsSel_Boletim.FieldByName ('dsc_aprov_1b').AsString;

        Sheet3.Cells[lin,8].Font.Size := 8;
        Sheet3.Cells[lin,8].Value   := dmDeca.cdsSel_Boletim.FieldByName ('dsc_freq_1b').AsString;

        Sheet3.Cells[lin,9].Font.Size := 8;
        Sheet3.Cells[lin,9].Value   := dmDeca.cdsSel_Boletim.FieldByName ('dsc_sit_1b').AsString;

        Sheet3.Cells[lin,10].Font.Size := 8;
        Sheet3.Cells[lin,10].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_aprov_2b').AsString;

        Sheet3.Cells[lin,11].Font.Size := 8;
        Sheet3.Cells[lin,11].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_freq_2b').AsString;

        Sheet3.Cells[lin,12].Font.Size := 8;
        Sheet3.Cells[lin,12].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_sit_2b').AsString;

        Sheet3.Cells[lin,13].Font.Size := 8;
        Sheet3.Cells[lin,13].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_aprov_3b').AsString;

        Sheet3.Cells[lin,14].Font.Size := 8;
        Sheet3.Cells[lin,14].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_freq_3b').AsString;

        Sheet3.Cells[lin,15].Font.Size := 8;
        Sheet3.Cells[lin,15].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_sit_3b').AsString;

        lin := lin + 1;
        dmDeca.cdsSel_Boletim.Next;
      end;

      objExcel3.WorkBooks[1].Close;
    end;
  end
  else
  //Singinfica que selecionou uma das escolas da Lista...
  begin
    Application.CreateForm (TfrmExportarDadosAcompEscolar, frmExportarDadosAcompEscolar);

    frmExportarDadosAcompEscolar.edNomeEscola.Text  := cbEscola.Text;
    frmExportarDadosAcompEscolar.edNomeDiretor.Text := dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').Value;
    frmExportarDadosAcompEscolar.edEmailPara.Text   := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').Value;

    frmExportarDadosAcompEscolar.ShowModal;
    frmExportarDadosAcompEscolar.Close;
  end;

end;

procedure TfrmGeral_AcompanhamentoEscolar.chkTodasClick(Sender: TObject);
begin
  if chkTodas.Checked then cbEscola.ItemIndex := -1;
end;

procedure TfrmGeral_AcompanhamentoEscolar.cbEscolaEnter(Sender: TObject);
begin
  chkTodas.Checked := False;
end;

procedure TfrmGeral_AcompanhamentoEscolar.cbEscolaClick(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := StrToInt(vListaEscola1.Strings[cbEscola.ItemIndex]);
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;
    end;
  except end;

end;

procedure TfrmGeral_AcompanhamentoEscolar.SpeedButton4Click(
  Sender: TObject);
var
  CSVFile : TextFile;
begin

  SaveDialog2.Execute;

  AssignFile(CSVFile, SaveDialog2.FileName);
  Rewrite(CSVFile);

  Writeln (CSVFile, 'Matricula;Nome;Situacao;Unidade;RGEscolar;Nascimento;Escola;Serie;Freq1b;Aprov1b;Sit1b;Freq2b;Aprov2b;Sit2b;Freq3b;Aprov3b;Sit3b');

  with dmDeca.cdsSel_Boletim do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value  := Null;
    Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
    Params.ParamByName ('@pe_nom_escola').Value     := Null;
    Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
    Params.ParamByName ('@pe_ind_status').Value     := Null;
    Params.ParamByName ('@pe_flg_defasagem').Value  := Null;
    Open;

    dmDeca.cdsSel_Boletim.First;
    while not dmDeca.cdsSel_Boletim.eof do
    begin
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('cod_matricula').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('nom_nome').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('vStatus').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('nom_unidade').Value +';');

      if dmDeca.cdsSel_Boletim.FieldByName('num_rg_escolar').IsNull then
        Write  (CSVFile, '<N�O INFORMADO>;')
      else
        Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('num_rg_escolar').Value +';');

      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('DatNascimento').Value +';');

      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('nom_escola').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_serie').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_freq_1b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_aprov_1b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_sit_1b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_freq_2b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_aprov_2b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_sit_2b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_freq_3b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_aprov_3b').Value +';');
      Write  (CSVFile, dmDeca.cdsSel_Boletim.FieldByName('dsc_sit_3b').Value);
      Writeln(CSVFile, '');

      if dmDeca.cdsSel_Boletim.eof then
      begin
        Writeln(CSVFile, #13);
        Write  (CSVFile, 'Fim de Arquivo');
      end;
      
      dmDeca.cdsSel_Boletim.Next;

    end;
  end;
end;

end.
