object frmRegistroAtendimentoAltera: TfrmRegistroAtendimentoAltera
  Left = 493
  Top = 254
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Altera��o de Registro de Atendimento]'
  ClientHeight = 474
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 2
    Width = 705
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR crian�a/adolescente'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnExit = txtMatriculaExit
    end
  end
  object Panel1: TPanel
    Left = 4
    Top = 421
    Width = 705
    Height = 49
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 16
      Width = 397
      Height = 13
      Caption = 
        'ATEN��O !!!  As altera��es de Registro s� podem ser feitas por q' +
        'uem os registrou...'
    end
    object btnAlterar: TBitBtn
      Left = 474
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAlterarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF7F3F3FBF7F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBF7F3FBFBF9FFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
        3F3FBF9F5FBFBF7F9F3F3F7F3F3FBF7F3FBF9F5FFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBFDF9FBFBF7FBFBF7FBFBF7FBF9F
        5F9F3F3FBF9F5FFFFFFFFFFFFF7FBFBF3F7F9F7FBFBFFFFFFFFFFFFFFFFFFF9F
        3F3FBF9F5FBFDF9FBF7F3F9F3F3FBF7F3FBFBF7F9F3F3FBF9F5F7FBFBF3F7F9F
        7FBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3F3FBF9F5FBF7F3FBFBF9FBF9F
        5F9F3F3FBFBF7FBF7F3F5F9F9F3F7F9FBFDFDFFFFFFFFFFFFF3F5F7FFFFFFFFF
        FFFFFFFFFF9F3F3FBF7F3FFFFFFFFFFFFFBF9F5FBF9F5F9F3F3F3F7F7F3FBFFF
        3F9FDF9FDFDFFFFFFF3F5F7F3F5F7FFFFFFFFFFFFFFFFFFF9F3F3FFFFFFFFFFF
        FFFFFFFF9F3F3FBF7F3F5F9F9F3FDFFF3F5F7F7FBFBF9FDFDF3F5F7F3F9FDF3F
        5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBF9F9F3F3FBFBF9F7FBFBF3F7F7F
        3FFFFF3F7F9F3F5F7F3F7F9F3FDFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFBFBF
        9F9F3F3FBFBF9FFFFFFFFFFFFF5F9F9F3F7F7F3FFFFF3FFFFF3FFFFF3FFFFF3F
        FFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        7FBFBF5F9F9F3F7F7F5F9F9F9FFFFFFFFFFF3F7F7FFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFF3F
        7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF3F7F7F3F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnCancelar: TBitBtn
      Left = 534
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR a opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 628
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de Registro de Atendimento'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 4
    Top = 52
    Width = 705
    Height = 365
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 168
      Width = 125
      Height = 13
      Caption = 'Descri��o do Atendimento'
    end
    object txtOcorrencia: TMemo
      Left = 16
      Top = 185
      Width = 673
      Height = 166
      Hint = 'Descri��o do Registro de Atendimento'
      Lines.Strings = (
        '')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 15
      Width = 673
      Height = 150
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      OnKeyDown = DBGrid1KeyDown
      OnKeyUp = DBGrid1KeyUp
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Dat_Historico'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = [fsBold]
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dsc_tipo_historico'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Caption = 'Tipo Hist�rico'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = [fsBold]
          Width = 231
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = [fsBold]
          Width = 222
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome Usuario'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Caption = 'Usuario'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Arial'
          Title.Font.Style = [fsBold]
          Width = 97
          Visible = True
        end>
    end
  end
  object DataSource1: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Historico
    Left = 660
    Top = 76
  end
end
