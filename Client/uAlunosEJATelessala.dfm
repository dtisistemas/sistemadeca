object frmAlunosEjaTelessala: TfrmAlunosEjaTelessala
  Left = 492
  Top = 402
  Width = 381
  Height = 92
  Caption = '[Sistema Deca] - Alunos EJA/Telessala'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 361
    Height = 49
    Caption = '[Selecione Bimestre/Ano]'
    TabOrder = 0
    object Label1: TLabel
      Left = 132
      Top = 19
      Width = 5
      Height = 13
      Caption = '/'
    end
    object cbBimestre: TComboBox
      Left = 8
      Top = 16
      Width = 121
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      Items.Strings = (
        '1.� Bimestre'
        '2.� Bimestre'
        '3.� Bimestre'
        '4.� Bimestre')
    end
    object mskAnoLetivo: TMaskEdit
      Left = 144
      Top = 16
      Width = 48
      Height = 21
      EditMask = '9999'
      MaxLength = 4
      TabOrder = 1
      Text = '    '
    end
    object btnVer: TBitBtn
      Left = 200
      Top = 14
      Width = 152
      Height = 25
      Caption = 'Gerar e Visualizar'
      TabOrder = 2
      OnClick = btnVerClick
      Kind = bkOK
    end
  end
end
