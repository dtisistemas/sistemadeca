unit uItensCalendario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, Grids, DBGrids, Db, ComCtrls;

type
  TfrmItensCalendario = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    btnCalendarios: TSpeedButton;
    rgTipoFeriado: TRadioGroup;
    Panel2: TPanel;
    dbDatas: TDBGrid;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnAlterar: TBitBtn;
    btnSair: TBitBtn;
    btnCopiar: TBitBtn;
    dsDatas: TDataSource;
    edCalendarioAtual: TEdit;
    meData: TMaskEdit;
    edNumSemana: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure AtualizaTela_Itens(Modo:String);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure meDataExit(Sender: TObject);
    function NumSemana(const vData:TDateTime):Word;
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmItensCalendario: TfrmItensCalendario;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmItensCalendario.FormShow(Sender: TObject);
begin
(*
  //Carrega o calend�rio ativo/atual
  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Null;
      Params.ParamByName('@pe_flg_atual').AsInteger := 0;
      Open;

      if RecordCount > 0 then
      begin
        edCalendarioAtual.Text := dmDeca.cdsSel_Calendario.FieldByName('Calend�rio').AsString


      end
      else
      begin
        ShowMessage('N�o h� nenhum calend�rio definido como Atual...');
      end;
    end;
  except end;
  AtualizaTela_Itens('Padr�o');*)
end;

procedure TfrmItensCalendario.btnNovoClick(Sender: TObject);
begin
  (*
  //Carrega o calend�rio ativo/atual
  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Null;
      Params.ParamByName('@pe_flg_atual').AsInteger := 0;
      Open;

      if RecordCount > 0 then
        edCalendarioAtual.Text := dmDeca.cdsSel_Calendario.FieldByName('Calend�rio').AsString
      else
      begin
        ShowMessage('N�o h� nenhum calend�rio definido como Atual...');
      end;
    end;
  except end;
  *)

  AtualizaTela_Itens('Novo');

end;

procedure TfrmItensCalendario.AtualizaTela_Itens(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    Panel1.Enabled := False;
    Panel2.Enabled := True;

    btnCopiar.Enabled := False;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end

  else if Modo = 'Novo' then
  begin

    Panel1.Enabled := True;
    Panel2.Enabled := False;

    GroupBox1.Enabled := False;
    GroupBox2.Enabled := True;
    rgTipoFeriado.Enabled := True;

    btnCopiar.Enabled := False;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;

  end;
end;

procedure TfrmItensCalendario.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela_Itens('Padr�o');
end;

procedure TfrmItensCalendario.btnSalvarClick(Sender: TObject);
begin

  if (rgTipoFeriado.ItemIndex > -1) then
  begin

    //Validar a data
    try
      with dmDeca.cdsSel_ItensCalendario do
      begin

        Close;
        Params.ParamByName ('@pe_cod_calendario').Value := Null;
        Params.ParamByName ('@pe_ind_tipo_feriado').Value := Null;
        Params.ParamByName ('@pe_num_dia').AsInteger := StrToInt(Copy(meData.Text,1,2));
        Params.ParamByName ('@pe_num_mes').AsInteger := StrToInt(Copy(meData.Text,4,2));
        Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(Copy(meData.Text,4,2));
        Params.ParamByName ('@pe_num_semana').Value := Null;
        Open;

        //Se encontrou � porque a data j� foi lan�ada
        if (RecordCount > 0) then
        begin
          ShowMessage ('Aten��o !!! Data j� lan�ada...');
          meData.SetFocus;
        end
        else
        begin

          //Insere a data
          with dmDeca.cdsInc_ItensCalendario do
          begin

            Close;
            Params.ParamByName('@pe_cod_calendario').AsInteger := dmDeca.cdsSel_Calendario.FieldByName('cod_calendario').AsInteger;
            Params.ParamByName('@pe_ind_tipo_feriado').Value := rgTipoFeriado.ItemIndex + 1;
            Params.ParamByName('@pe_num_dia').AsInteger := StrToInt(Copy(meData.Text,1,2));
            Params.ParamByName('@pe_num_mes').Value := StrToInt(Copy(meData.Text,4,2));
            Params.ParamByName('@pe_num_ano').Value := StrToInt(Copy(meData.Text,7,4));
            Params.ParamByName('@pe_num_semana').Value := StrToInt(edNumSemana.Text);
            Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Execute;

            //Atualiza o grid
            with dmDeca.cdsSel_ItensCalendario do
            begin
              Close;
              Params.ParamByName ('@pe_cod_calendario').AsInteger := dmDeca.cdsSel_Calendario.FieldByName('cod_calendario').AsInteger;;
              Params.ParamByName ('@pe_ind_tipo_feriado').Value := Null;
              Params.ParamByName ('@pe_num_dia').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null; //
              Params.ParamByName ('@pe_num_ano').Value := Null;//StrToInt(Copy(meData.Text,4,2));
              Params.ParamByName ('@pe_num_semana').Value := Null;
              Open;
              dbDatas.Refresh;
            end;
          end;

        end;
      end;
    except end;
    AtualizaTela_Itens('Padr�o');
  end;

end;

procedure TfrmItensCalendario.meDataExit(Sender: TObject);
var
  str_data_inicial : String;
  data_inicial, data_final : TDateTime;
  num_semanas : Integer;
begin
  edNumSemana.Clear;
  edNumSemana.Text := IntToStr(NumSemana(StrToDate(meData.Text)));

  str_data_inicial := '01/01/' + Copy(meData.Text,7,4);
  data_inicial := StrToDate(str_data_inicial);
  data_final := StrToDate(meData.Text);
  num_semanas := trunc(data_final-data_inicial) div 7;
  edNumSemana.Text := IntToStr(num_semanas+1);

end;

function TfrmItensCalendario.NumSemana(const vData: TDateTime): Word;
var
  Ano, Mes, Dia : Word;
  DataTEMP : TDateTime;
begin

  DecodeDate (vData, Ano, Mes, Dia);
  DataTEMP := EncodeDate(Ano, 1, 1);
  Result := (Trunc(vData-DataTEMP))DIV 7;//+DayOfWeek(DataTEMP)-1) DIV 7;

  if Result = 0 then
  begin
    Result := 51
  end
  else
  begin
    Result := Result-1;
  end;
end;

procedure TfrmItensCalendario.btnSairClick(Sender: TObject);
begin
  frmItensCalendario.Close;
end;

end.
