unit uImportaEducacensoAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons;

type
  TfrmImportaEducacensoAcompEscolar = class(TForm)
    SpeedButton1: TSpeedButton;
    OpenDialog1: TOpenDialog;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaEducacensoAcompEscolar: TfrmImportaEducacensoAcompEscolar;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmImportaEducacensoAcompEscolar.SpeedButton1Click(
  Sender: TObject);
var
  arqImporta : TextFile;
  linha : String;
  vvCOD_MATRICULA, vvDAT_CADASTRO, vvOBS : String;
  vvNUM_CEI : Integer;
begin

  if OpenDialog1.Execute then
  begin

    AssignFile (arqImporta, OpenDialog1.FileName);
    Reset (arqImporta);

    while not Eoln(arqImporta) do
    begin

      Readln (arqImporta, linha);

      while not Eoln(arqImporta) do
      begin

        vvCOD_MATRICULA  := '00' + Copy(linha,1,6);
        vvDAT_CADASTRO   := Copy(linha,8,10);
        vvNUM_CEI        := StrToInt(Copy(linha,19,6));
        vvOBS            := Trim(Copy(linha,26,10));

        //Promover a altera��o dos dados no cadastro....
        with dmDeca.cdsUpd_DadosEducacenso do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value              := vvCOD_MATRICULA;
          Params.ParamByName ('@pe_dat_cadastro_educacenso').Value    := StrToDate(vvDAT_CADASTRO);
          Params.ParamByName ('@pe_num_situacao_cei').Value           := vvNUM_CEI;
          Params.ParamByName ('@pe_dsc_observacoes_educacenso').Value := vvOBS;
          Execute;
        end;

        Readln (arqImporta, linha);

      end;

    end;

    Application.MessageBox ('Aten��o !!!' + #13#10 +
                            'Os dados do arquivo foram importados.',
                            '[Sistema Deca] - Importa��o Dados Educacenso',
                            MB_OK + MB_ICONINFORMATION);


  end;

end;

end.
