unit rResumoMatriz;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelResumoMatriz = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    DetailBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    qrlUnidade: TQRLabel;
    qrlPeriodo_Bimestre_Ano: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape3: TQRShape;
    QRLabel6: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape4: TQRShape;
    QRLabel10: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel11: TQRLabel;
    QRShape9: TQRShape;
    QRLabel12: TQRLabel;
    QRShape10: TQRShape;
    QRLabel13: TQRLabel;
    QRShape11: TQRShape;
    QRLabel14: TQRLabel;
    QRShape12: TQRShape;
    QRLabel15: TQRLabel;
    QRShape13: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape14: TQRShape;
    QRLabel23: TQRLabel;
    QRShape15: TQRShape;
    QRLabel24: TQRLabel;
    QRShape16: TQRShape;
    QRLabel25: TQRLabel;
    QRShape17: TQRShape;
    QRLabel26: TQRLabel;
    QRShape18: TQRShape;
    QRLabel27: TQRLabel;
    QRShape19: TQRShape;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRShape20: TQRShape;
    QRLabel42: TQRLabel;
    QRShape21: TQRShape;
    QRLabel43: TQRLabel;
    QRShape22: TQRShape;
    QRLabel44: TQRLabel;
    QRShape23: TQRShape;
    QRLabel45: TQRLabel;
    QRShape24: TQRShape;
    QRLabel46: TQRLabel;
    QRShape25: TQRShape;
    QRLabel47: TQRLabel;
    QRShape26: TQRShape;
    QRLabel48: TQRLabel;
    QRShape27: TQRShape;
    QRLabel49: TQRLabel;
    QRShape28: TQRShape;
    QRLabel50: TQRLabel;
    QRShape29: TQRShape;
    QRLabel51: TQRLabel;
    QRShape30: TQRShape;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRShape31: TQRShape;
    QRLabel56: TQRLabel;
    QRShape32: TQRShape;
    QRLabel57: TQRLabel;
    QRShape33: TQRShape;
    QRLabel58: TQRLabel;
    QRShape34: TQRShape;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRShape35: TQRShape;
    QRLabel61: TQRLabel;
    QRShape36: TQRShape;
    QRLabel62: TQRLabel;
    QRShape37: TQRShape;
    QRLabel63: TQRLabel;
    QRShape38: TQRShape;
    QRLabel64: TQRLabel;
    QRShape39: TQRShape;
    PageFooterBand1: TQRBand;
    QRLabel65: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    QRDBText44: TQRDBText;
    QRDBText45: TQRDBText;
    QRDBText46: TQRDBText;
    QRDBText49: TQRDBText;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRDBText47: TQRDBText;
    QRDBText48: TQRDBText;
    QRDBText50: TQRDBText;
  private

  public

  end;

var
  relResumoMatriz: TrelResumoMatriz;

implementation

uses uDM;

{$R *.DFM}

end.
