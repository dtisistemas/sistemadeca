unit uFormAproveitamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, ComCtrls, ExtCtrls, CheckLst, Buttons, Mask,
  jpeg;

type
  TfrmFormularioAproveitamento = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    GroupBox2: TGroupBox;
    lstAlunos: TCheckListBox;
    GroupBox8: TGroupBox;
    SpeedButton2: TSpeedButton;
    ComboBox4: TComboBox;
    GroupBox9: TGroupBox;
    ComboBox5: TComboBox;
    MaskEdit2: TMaskEdit;
    GroupBox10: TGroupBox;
    GroupBox12: TGroupBox;
    meObservacoes: TMemo;
    ComboBox6: TComboBox;
    GroupBox11: TGroupBox;
    ComboBox7: TComboBox;
    TabSheet5: TTabSheet;
    CheckBox1: TCheckBox;
    Image1: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFormularioAproveitamento: TfrmFormularioAproveitamento;

implementation

{$R *.DFM}

end.
