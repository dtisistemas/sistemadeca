unit uUsuariosConectados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, Db, Buttons;

type
  TfrmUsuariosConectados = class(TForm)
    Panel1: TPanel;
    dbgUsuarios: TDBGrid;
    dsUsuarios: TDataSource;
    Panel2: TPanel;
    btnConectar: TSpeedButton;
    btnSair: TSpeedButton;
    procedure dbgUsuariosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnConectarClick(Sender: TObject);
    procedure btnDesconectarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUsuariosConectados: TfrmUsuariosConectados;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmUsuariosConectados.dbgUsuariosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //if odd(cdsSel_Usuario.RecNo) then
  if (dmDeca.cdsSel_Acesso.FieldByName ('flg_ativo').AsInteger=0) then
  begin
    dbgUsuarios.Canvas.Font.Color:= clWhite;
    dbgUsuarios.Canvas.Brush.Color:= clGreen;
  end
  else
  begin
    dbgUsuarios.Canvas.Font.Color:= clBlack;
    dbgUsuarios.Canvas.Brush.Color:= clWhite;
  end;

  dbgUsuarios.Canvas.FillRect(Rect);
  dbgUsuarios.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

procedure TfrmUsuariosConectados.btnConectarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value  := Null;
      Params.ParamByName ('@pe_cod_usuario').Value    := Null;
      Params.ParamByName ('@pe_cod_perfil').Value     := Null;
      Params.ParamByName ('@pe_cod_unidade').Value    := Null;
      Params.ParamByName ('@pe_flg_ativo').Value      := Null;
      Params.ParamByName ('@pe_flg_online').Value     := Null;
      Params.ParamByName ('@pe_cod_matricula').Value  := Null;
      Open;
      dbgUsuarios.Refresh;
    end;
  except end;
end;

procedure TfrmUsuariosConectados.btnDesconectarClick(Sender: TObject);
begin
  btnConectar.Enabled := True;
  btnSair.Enabled := True;
end;

procedure TfrmUsuariosConectados.btnSairClick(Sender: TObject);
begin
  frmUsuariosConectados.Close;
end;

procedure TfrmUsuariosConectados.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
