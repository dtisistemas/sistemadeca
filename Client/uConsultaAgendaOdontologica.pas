unit uConsultaAgendaOdontologica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ComCtrls, Buttons, StdCtrls, Db;

type
  TfrmConsultaAgendaOdontologica = class(TForm)
    GroupBox11: TGroupBox;
    Label5: TLabel;
    btnVerConsultasData: TSpeedButton;
    dataFiltro: TDateTimePicker;
    dbAgenda: TDBGrid;
    btnImprimirAgenda: TSpeedButton;
    dsAgenda: TDataSource;
    procedure btnVerConsultasDataClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnImprimirAgendaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaAgendaOdontologica: TfrmConsultaAgendaOdontologica;
  vData : String;

implementation

uses uDM, rAgendaDiaOdontologia;

{$R *.DFM}

procedure TfrmConsultaAgendaOdontologica.btnVerConsultasDataClick(
  Sender: TObject);
begin
  vData := DateToStr(dataFiltro.Date);
  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_dat_consulta').AsDateTime := StrToDate(vData);
      Params.ParamByName('@pe_hor_consulta').Value := Null;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;
      dbAgenda.Refresh;
    end;
  except end;
end;

procedure TfrmConsultaAgendaOdontologica.FormShow(Sender: TObject);
begin
  dataFiltro.Date := Now;
end;

procedure TfrmConsultaAgendaOdontologica.btnImprimirAgendaClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value     := Null;
      Params.ParamByName('@pe_cod_matricula').Value       := Null;
      Params.ParamByName('@pe_nom_nome').Value            := Null;
      Params.ParamByName('@pe_dat_consulta').AsDateTime   := StrToDate(vData);
      Params.ParamByName('@pe_hor_consulta').Value        := Null;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;

      if (dmDeca.cdsSel_Consulta.RecordCount < 1) then
        ShowMessage ('N�o existem pacientes agendados para este dia...')
      else
      begin
        Application.CreateForm (TrelAgendaDiaOdontologia, relAgendaDiaOdontologia);
        relAgendaDiaOdontologia.lbTitulo.Caption := 'Rela��o de Hor�rios das consultas da data de ' + DateToStr(dataFiltro.Date);
        relAgendaDiaOdontologia.Preview;
        relAgendaDiaOdontologia.Free;
      end;
    end;
  except end;
end;

end.
