unit uImportaEscolaCadParaAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, Gauges;

type
  TfrmImportaEscolaCadParaAcompEscolar = class(TForm)
    GroupBox1: TGroupBox;
    meOcorrencias: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbTotalFichas: TLabel;
    lbTotalSemRGEscolar: TLabel;
    Gauge1: TGauge;
    btnImportar: TSpeedButton;
    btnVerOcorrencias: TSpeedButton;
    meSemEscola: TMemo;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnVerOcorrenciasClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaEscolaCadParaAcompEscolar: TfrmImportaEscolaCadParaAcompEscolar;
  vvTOTAL_SEM_RG, vvTOTAL_SEM_ESCOLA : Integer;

implementation

uses uPrincipal, uDM, uUtil;

{$R *.DFM}

procedure TfrmImportaEscolaCadParaAcompEscolar.FormShow(Sender: TObject);
begin
  lbTotalFichas.Caption := IntToStr(vvTOTAL_FICHAS);
  lbTotalSemRGEscolar.Caption := '00000';

end;

procedure TfrmImportaEscolaCadParaAcompEscolar.btnVerOcorrenciasClick(
  Sender: TObject);
begin
  meOcorrencias.Lines.SaveToFile (ExtractFilePath(Application.ExeName) + 'SEMRGESCOLAR.TXT');
  meSemEscola.Lines.SaveToFile (ExtractFilePath(Application.ExeName) + 'SEMESCOLARIDADE.TXT');
  Application.MessageBox('Arquivos "SEMRGESCOLA" e "SEMESCOLARIDADE" gravado na pasta do Sistema',
                         '[Sistema Deca] - Arquivo gerado com sucesso...',
                         MB_OK + MB_ICONQUESTION);  
end;

procedure TfrmImportaEscolaCadParaAcompEscolar.btnImportarClick(
  Sender: TObject);
begin

  try

    //Seleciona as fichas do cadastro...
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := 0;
      Open;

      lbTotalFichas.Caption := IntToStr(dmDeca.cdsSel_Cadastro.RecordCount);

      //Preparar a barra de progress�o
      Gauge1.MinValue := 0;
      Gauge1.Progress := Gauge1.MinValue;
      Gauge1.MaxValue := dmDeca.cdsSel_Cadastro.RecordCount;

      vvTOTAL_SEM_RG := 0;
      while not dmDeca.cdsSel_Cadastro.eof do
      begin
        //Gerar rela��o com os alunos sem RG Escolar...
        Gauge1.Progress := Gauge1.Progress + 1;

        if (dmDeca.cdsSel_Cadastro.FieldByName('dsc_rg_escolar').Value = Null) then
        begin
         meOcorrencias.Lines.Add (dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + '-' +
                                  dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value + '-> ' +
                                  'N�O TEM N.� RG ESCOLAR PREENCHIDO');
         vvTOTAL_SEM_RG := vvTOTAL_SEM_RG + 1;
        end
        else
        begin

        end; //Gerar rela��o com os alunos sem rg escolar...

        if (dmDeca.cdsSel_Cadastro.FieldByName('cod_escola').Value = 209) or
           (dmDeca.cdsSel_Cadastro.FieldByName('cod_escola').Value = Null) or
           ((dmDeca.cdsSel_Cadastro.FieldByName('dsc_escola_serie').Value = Null)) or
           ((dmDeca.cdsSel_Cadastro.FieldByName('dsc_escola_serie').Value = '<ND>')) then
        begin
         meSemEscola.Lines.Add (dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + '-' +
                                  dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value + '-> ' +
                                  'SEM INFORMA��O DE ESCOLA E S�RIE');
         vvTOTAL_SEM_ESCOLA := vvTOTAL_SEM_ESCOLA + 1;
        end
        else
        begin

        end; //Gerar rela��o com os alunos sem rg escolar...


        //Validar campos referentes a escola, j� inseridos antes no cadastro e inser�-los no Acompanhamento Escolar,
        //de acordo com os crit�rios citados no come�o da procedure, a saber...
        {Na importa��o:
           - Incluir Frequencia Escolar como SEM INFORMACAO = 2
           - Incluir Aproveitamento Escolar como SEM INFORMACAO = 3
           - Incluir ANO LETIVO = 2009
           - Incluir BIMESTRE = 1.�
           - Situa��o Escolar SEM INFROMACAO = 7
           - Per�odo Escolar invertido ao da FUNDHAS
           - Escola: inserir o c�digo da escola contido no cadastro
           - S�rie Escolar: localizar o c�digo da s�rie escolar e repassar a ACOMPESCOLAR
        }

        with dmDeca.cdsInc_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_num_ano_letivo').Value := 2009;
          Params.ParamByName ('@pe_num_bimestre').Value := 1;
          Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_escola').Value;
          if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '1.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName('dsc_escola_serie').Value = '1.� S�rie - Ensino Fundamental')then
            Params.ParamByName ('@pe_cod_id_serie').Value := 1
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '2.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '2.� S�rie - Ensino Fundamental')then
            Params.ParamByName ('@pe_cod_id_serie').Value := 2
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '3.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '3.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 3
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '4.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '4.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 4
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '5.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '5.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 13
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '6.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '6.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 14
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '7.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '7.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 15
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '8.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '8.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 16
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '9.� S�rie/Ano - EF') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '9.� S�rie - Ensino Fundamental') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 17
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '1.� Ano - EM') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '1.� S�rie - Ensino M�dio') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 18
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '2.� Ano - EM') or (dmDeca.cdsSel_Cadastro.FieldByName('dsc_escola_serie').Value = '2.� S�rie - Ensino M�dio') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 19
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = '3.� Ano - EM') or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_Serie').Value = '3.� Ano - EM') then
            Params.ParamByName ('@pe_cod_id_serie').Value := 20
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo I (1.� S�rie Ens. Fund.) -  Fase 1' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 33
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo I (2.� S�rie Ens. Fund.) -  Fase 2' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 34
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo I (3.� S�rie Ens. Fund.) -  Fase 3' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 35
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo I (4.� S�rie Ens. Fund.) -  Fase 4' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 36
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo II (5.� S�rie Ens. Fund.) -  Fase 1' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 38
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo II (6.� S�rie Ens. Fund.) -  Fase 2' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 39
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo II (7.� S�rie Ens. Fund.) -  Fase 3' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 40
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'EJA-Ciclo II (8.� S�rie Ens. Fund.) -  Fase 4' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 41
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Recupera��o de Ciclo I' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 37
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Sala Especial' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 29
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia - 1.� Serie Ensino M�dio' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 21
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia - 2.� Serie Ensino M�dio' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 22
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia - 3.� Serie Ensino M�dio' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 23
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia/Ciclo I - 4.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 24
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia/Ciclo II - 5.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 25
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia/Ciclo II - 6.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 26
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia/Ciclo II - 7.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 27
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supl�ncia/Ciclo II - 8.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 28
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo - 1.� Serie Ensino M�dio' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 21
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo I - 1.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 44
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo I - 2.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 44
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo I - 3.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 44
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo I - 4.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 24
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo II - 5.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 25
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo II - 6.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 26
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo II - 7.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 27
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Supletivo/Ciclo II - 8.� Serie Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 28
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Tele Sala' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 32
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Tele Sala - Ensino Fundamental' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 31
          else if dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Tele Sala - Ensino M�dio' then
            Params.ParamByName ('@pe_cod_id_serie').Value := 32
          else Params.ParamByName ('@pe_cod_id_serie').Value := 44;

          Params.ParamByName ('@pe_ind_frequencia').Value := 2;
          Params.ParamByName ('@pe_ind_aproveitamento').Value := 3;

          //Se concluinte do Ensino Fundamental, situa��o=3, escola=209 e serie=9aserie
          if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Concluinte 8.� S�rie Ensino Fundamental') then
          begin
            Params.ParamByName ('ind_situacao').Value := 3;
            Params.ParamByName ('cod_id_serie').Value := 44;
          end
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value = 'Concluinte Ensino M�dio') then
          begin
            Params.ParamByName ('ind_situacao').Value := 4;
            Params.ParamByName ('cod_id_serie').Value := 44;
          end
          else Params.ParamByName ('@pe_ind_situacao').Value := 7;

          //Verifica per�odo da FUNDHAS e inverte o da escola...
          if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_periodo').Value = 'MANH�') then
            Params.ParamByName ('@pe_ind_periodo_escolar').Value := 0
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_periodo').Value = 'TARDE') then
            Params.ParamByName ('@pe_ind_periodo_escolar').Value := 1
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_periodo').Value = 'NOITE') then
            Params.ParamByName ('@pe_ind_periodo_escolar').Value := 2
          else if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_periodo').Value = 'INTEGRAL') then
            Params.ParamByName ('@pe_ind_periodo_escolar').Value := 3
          else Params.ParamByName ('@pe_ind_periodo_escolar').Value := 4;

          if Length(dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_obs').Value)= 0 then
            Params.ParamByName ('@pe_dsc_observacoes').Value := Null
          else Params.ParamByName ('@pe_dsc_observacoes').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_obs').Value;
          
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Execute;
        end; //dmDeca.cdsInc_AcompEscolar...

        dmDeca.cdsSel_Cadastro.Next;  //Ap�s fazer todas as tarefas com o aluno,move para o pr�ximo...
      end;//while not eof



      if dmDeca.cdsSel_Cadastro.eof then
      begin
        ShowMessage ('Registros importados com sucesso');
        //ProgressBar1.Position := 0;
        Gauge1.Progress := 0;
        lbTotalSemRGEscolar.Caption := '00000';
      end;

    end; //with dmDeca.cdsSel_Cadastro...

    meOcorrencias.Lines.Add('Total de alunos SEM RG ESCOLAR:  ' + IntToStr(vvTOTAL_SEM_RG));
    meSemEscola.Lines.Add('Total de alunos SEM INFORMA��ES DE ESCOLA/S�RIE ESCOLAR:  ' + IntToStr(vvTOTAL_SEM_ESCOLA));
    
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ao carregar os dados ocorreu. ' + #13+#10 +
                            'Essa aplica��o ser� finalizada e por favor, entre ' + #13+#10 +
                            'em contato com o Administrador do Sistema!',
                            '[Sistema Deca] - ERRO DE IMPORTA��O DOS DADOS',
                            MB_OK + MB_ICONERROR);
  end;


end;

end.
