object frmMigrarAtendimentosParaRelatorios: TfrmMigrarAtendimentosParaRelatorios
  Left = 384
  Top = 198
  Width = 1126
  Height = 609
  Caption = 
    '[Sistema Deca] - Migra��o dos dados dos Registros de Atendimento' +
    ' para o Cadastro de Relat�rios - Triagem'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 1105
    Height = 169
    Caption = 
      '[Processo de Migra��o dos dados dos Registros de Atendimento par' +
      'a os Relat�rios]'
    TabOrder = 0
    object btnINICIAR_MIGRACAO: TSpeedButton
      Left = 400
      Top = 102
      Width = 208
      Height = 51
      Caption = 'Iniciar Migra��o'
      Flat = True
      OnClick = btnINICIAR_MIGRACAOClick
    end
    object ProgressBar1: TProgressBar
      Left = 16
      Top = 24
      Width = 1073
      Height = 57
      Min = 0
      Max = 100
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 184
    Width = 1105
    Height = 385
    Caption = '[Importados]'
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 8
      Top = 16
      Width = 1089
      Height = 361
      DataSource = dsSel_Cadastro_Relatorio
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object dsSel_Cadastro_Relatorio: TDataSource
    DataSet = dmTriagemDeca.cdsSel_Cadastro_Relatorio
    Left = 1048
    Top = 216
  end
end
