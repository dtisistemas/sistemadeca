object frmSolicitacaoUniformes: TfrmSolicitacaoUniformes
  Left = 577
  Top = 276
  Width = 1098
  Height = 577
  Caption = '[Sistema DECA] - Solicita��es de Uniformes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1090
    Height = 546
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = '[Cadastro de Uniformes]'
      object btnNovoUniforme: TSpeedButton
        Left = 920
        Top = 48
        Width = 148
        Height = 41
        Caption = 'Novo'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
        OnClick = btnNovoUniformeClick
      end
      object btnGravarUniforme: TSpeedButton
        Left = 920
        Top = 96
        Width = 148
        Height = 41
        Caption = '&Gravar'
        Flat = True
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C30E0000C30E00000000000000000000BFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBF7F7F7F00000000000000000000000000000000
          00000000000000000000000000000000000000007F7F7FBFBFBFBFBFBF000000
          FF0000FF00000000007F7F7FFF0000FF0000FFFFFFBFBFBFBFBFBF000000FF00
          00FF0000000000BFBFBFBFBFBF000000FF0000FF00000000007F7F7FFF0000FF
          0000FFFFFFBFBFBFBFBFBF000000FF0000FF0000000000BFBFBFBFBFBF000000
          FF0000FF0000000000BFBFBF7F7F7F7F7F7FBFBFBFBFBFBFBFBFBF000000FF00
          00FF0000000000BFBFBFBFBFBF000000FF0000FF00007F7F0000000000000000
          00000000000000000000007F7F00FF0000FF0000000000BFBFBFBFBFBF000000
          FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
          00FF0000000000BFBFBFBFBFBF000000FF00007F7F0000000000000000000000
          00000000000000000000000000007F7F00FF0000000000BFBFBFBFBFBF000000
          FF0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00FF0000000000BFBFBFBFBFBF000000FF0000000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF0000000000BFBFBFBFBFBF000000
          FF0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00FF0000000000BFBFBFBFBFBF000000FF0000000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF0000000000BFBFBFBFBFBF000000
          000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00000000000000BFBFBFBFBFBF000000FF0000000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF0000000000BFBFBFBFBFBF7F7F7F
          0000000000000000000000000000000000000000000000000000000000000000
          000000007F7F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF}
        OnClick = btnGravarUniformeClick
      end
      object btnAlterarUniforme: TSpeedButton
        Left = 920
        Top = 144
        Width = 148
        Height = 41
        Caption = '&Alterar'
        Flat = True
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000484848767676
          79797A79797A7B7B7B7C7C7C7C7C7C7D7D7D7E7E7E7E7E7E7F7F7F8080808080
          808181817373735757574D4D4DEBEBEBEAEAEAE6E6EBEEEEEEF0F0F0F2F2F2F4
          F4F4F6F6F6F8F8F8F9F9F9FBFBFBFCFCFCFEFEFEDFDFDF676767545454EDEDED
          ECECECE8E8EDF0F0F0F2F2F2F4F4F4F6F6F6F8F8F8F9F9F9FBFBFBFCFCFCFDFD
          FDFEFEFEDEDEDE707070555555E6E6E6E9E9E9E4E4E9ECECECEEEEEEF0F0F0EF
          EFEFF3F3F3F4F4F4F6F6F6F7F7F7F8F8F8F7F7F7D8D8D86E6E6E555555EDEDED
          EEEEEEE8E8EEF1F1F1F3F3F3F5F5F5BFCFD9DADFE3F8F8F8FAFAFAFBFBFBFAFA
          FAF9F9F9D9D9D96D6D6D565656E6E6E6ECECECE8E8EDF0F0F0F1F1F1F3F3F3A2
          C5DB78C6E8DCE5E8F8F8F8F7F7F7F6F6F6F5F5F5D6D6D66D6D6D565656F3F3F3
          F4F4F4E9E9EFF1F1F1F3F3F3F4F4F4D5E3EF49ACE98ED4EAF3F3F3F6F6F6F5F5
          F5F3F3F3D4D4D46D6D6D575757E9E9E9EFEFEFEDEDF3F6F6F6F8F8F8F9F9F9FA
          FAFA8DC0E855C3F4CBE1E7F8F8F8F7F7F7F5F5F5D6D6D66C6C6C575757F7F7F7
          F8F8F8ECECF2F4F4F4F6F6F6F7F7F7F8F8F8EAEFF450A9E678CFEDEBEBEBF2F2
          F2F0F0F0D2D2D26C6C6C575757ECECECF3F3F3F0F0F6F9F9F9FAFAFAFBFBFBFA
          FAFAF9F9F9A8CDEA4FBBF1B2D9E4F2F2F2F1F1F1D3D3D36B6B6B585858FAFAFA
          FBFBFBEFEFF4F7F7F7F8F8F8F7F7F7F6F6F6F5F5F5F1F2F35EACE467CBF0DDE1
          E2ECECECCFCFCF6A6A6A585858EFEFEFF6F6F6F0F0F5F8F8F8F7F7F7F6F6F6F5
          F5F5F3F3F3F2F2F2BFD5E78ABBD6D8DCE0E9E9E9CDCDCD6A6A6A585858FDFDFD
          FDFDFDF3F3F9FAFAFAF9F9F9F8F8F8F7F7F7F5F5F5F3F3F3F1F1F18A9AC77B98
          E4EAEAEACFCFCF696969595959F1F1F1F7F7F7F0F0F5F6F6F6F5F5F5F3F3F3F2
          F2F2F0F0F0EEEEEEECECECE7E7E9E3E4E7E6E6E6CACACA696969595959FEFEFE
          FEFEFEF4F4FAFBFBFBFAFAFAF8F8F8F6F6F6F5F5F5F3F3F3F0F0F0EEEEEEECEC
          ECEAEAEACECECE696969646464D7D7D7D9D9D9D3D3D7D7D7D7D6D6D6D5D5D5D4
          D4D4D2D2D2D1D1D1CFCFCFCECECECCCCCCCBCBCBB5B5B56E6E6E}
        OnClick = btnAlterarUniformeClick
      end
      object btnCancelar: TSpeedButton
        Left = 920
        Top = 192
        Width = 148
        Height = 41
        Caption = 'Cancelar'
        Flat = True
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FF033B8A033D90013D95023B91033A89033A89FF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0357D30147B20051D0035CE007
          63E3035CE0004ED30042B7023A8F023A8FFF00FFFF00FFFF00FFFF00FFFF00FF
          0650BA0357D32781F278B4F7CAE2FCE9F4FFDCEDFF9CC7FA3F8FF20155DD0140
          A404367DFF00FFFF00FFFF00FF075DD70762E155A0F7F3F8FEFFFFFFE9F3FCC6
          DEFAD9E9FCFFFFFFFFFFFF99C5F8055DE70040A302398BFF00FFFF00FF075DD7
          529EF7FEFEFFE2EFFC0F65EB0558E70959E50250E20454E16FA6F0DEEBFC9CC9
          F80355DE02398BFF00FF0455C9207DF0E1EFFEFFFFFF6FA7F076AFF7176CED06
          5AE9075AE60F5EE66AA1F06FA7F0FFFFFF3E8FF20043B7033E96085FDA56A1FA
          FFFFFF9ECBFB1573F779B4FACFE3FC1C72EF2274EECBE1FB6DA5F20556E3DEEB
          FC9FCBFA0050D4033E960F6BE68BC1FCFFFFFF2987FC1F7DFA1674F779B5FADE
          EDFEDDEDFC6EAAF4065AE90455E5A0C5F6DEEFFF0560E202409C1B76EDA4CFFC
          FFFFFF2988FF1C7EFE1C7BFB2D87FBEDF6FEEDF6FE2279F20B63ED085DEA88BA
          F4EBF6FF0C68E60141A1207AEBA5CFFEFFFFFF3F97FF1F81FF3B93FEE1EFFF6B
          ADFC69ABFBE0EEFE2C80F30C65EEC6DEFBCEE5FE0763E203419E146FE79ACAFC
          FFFFFFB2D8FF318EFFE7F3FF67AFFF1D7EFE1A7AFB60A7FCE5F2FE3F8FF6E2EF
          FE81BAF80258D8033E96FF00FF237BEBEDF6FFFAFCFF5DA9FF469AFF1F81FF1F
          81FF1E80FF1C7DFC4D9CFBF0F8FFF2F8FE3089F4024FC0FF00FFFF00FF237BEB
          BFDEFFF3F8FFFAFCFFB0D5FF3E96FF2B89FF308CFF6AB0FEFAFCFFFFFFFF5DA6
          F70860DE024FC0FF00FFFF00FFFF00FF4997F3C7E3FFF7FBFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFE0EFFE5CA5F80E6BE70552C2FF00FFFF00FFFF00FFFF00FF
          FF00FF2D82EB91C5FBCCE6FFD9EDFFDCEDFEC4E0FE86BFFC348BF40A65E10A65
          E1FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF247BEB4696F34A
          98F42F87F0116CE6075FDCFF00FFFF00FFFF00FFFF00FFFF00FF}
        OnClick = btnCancelarClick
      end
      object btnRelacaoUniformes: TSpeedButton
        Left = 920
        Top = 264
        Width = 148
        Height = 41
        Caption = 'Rela��o'
        Flat = True
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777700000007777777770000000000000007777747770FFFFFF000000007777
          744770F8888F000000007444444470FFFFFF000000007777744770F8888F0000
          00007777747770FFFFFF000000007777777770F8888F000000007770000070FF
          FFFF000000007770FFF07000000000000000700000F0777777777000000070FF
          F0F0777777777000000070F8F000777777777000000070F8F007777777777000
          000070FF00777777777770000000700007777777777770000000777777777777
          777770000000}
        OnClick = btnRelacaoUniformesClick
      end
      object btnSair: TSpeedButton
        Left = 920
        Top = 456
        Width = 148
        Height = 41
        Caption = '&Sair'
        Flat = True
        Glyph.Data = {
          D6060000424DD606000000000000360400002800000019000000180000000100
          080000000000A0020000C30E0000C30E00000001000000000000000000007B00
          0000FF000000007B00007B7B000000FF0000FFFF0000007B7B007B7B7B00BDBD
          BD0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00090909090909
          0000000809090909090809090909090909090900000009090909090909090909
          0909090908080809090909090909090000000909090909090909090909090909
          0804040809090909090909000000090909090909090909090909090908060604
          0809090909090900000009090909090909090909090909090806060404080909
          0909090000000909090909090909090909090909080606040404080909090900
          0000000000000000000000000909090908060604040400000000080000000909
          0909090909090900080808080006060404040008090909000000090909090909
          090808000A0A0A0A070606040404000809090900000009090909090908000400
          0A0A0A0A0706060404040008090909000000090909090909080004000A0A0A0A
          070606040404000809090900000009090908080808040404070A0A0A07060404
          000400080909090000000909080808080804060404070A0A0706040800040008
          090909000000090400000000000406060404070A070606040404000809090900
          0000080406060606060606060604040707060604040400080909090000000804
          060B0B0B0B0B0B0B060604070706060404040008090909000000090404040404
          040406060604070A070606040404000809090900000009090808080808040606
          04070A0A070606040404000809090900000009090909090908040604070A0A0A
          0706060404040008090909000000090909090909080404000A0A0A0A07040604
          04040008090909000000090909090909080000000A0A0A0A0A07040604040008
          0909090000000909090909090909080A0A0A0A0A0A0A0A040604000809090900
          0000090909090909090908070707070707070700040400080909090000000909
          0909090909090908080808080808080808080809090909000000}
        OnClick = btnSairClick
      end
      object btnAtivarDesativar: TSpeedButton
        Left = 920
        Top = 312
        Width = 148
        Height = 41
        Caption = 'Ativar/Desativar'
        Flat = True
        Glyph.Data = {
          1E060000424D1E06000000000000360000002800000018000000150000000100
          180000000000E8050000CA0E0000C30E00000000000000000000BFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF7F7F7F60302F60302F7F7F
          7FCF6760BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF7F7F7F60302FFF9790
          FF979000FFFF60302F60302FCF67607F7F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF7F7F7F60302FFF
          9790FFC8CFFFC8CFFFC8CF00FFFFFFC8CF60302FCF6760CF6760CF67607F7F7F
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF7F7F7F6030
          2FFF9790FFC8CF00FFFFCFFFFFFFC8CFFFC8CFCFFFFFFFC8CFCF676060302FCF
          6760CF6760CF67607F7F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          7F7F7FCF6760FFC8CF00FFFFFFC8CFFFC8CF7F7F7F7F7F7FCFFFFFFFC8CFCFFF
          FFFFC8CF60302FFF0000CF6760CF6760CF67607F7F7F7F7F7FBFBFBFBFBFBFBF
          BFBFBFBFBF7F7F7FFF9790FFC8CF00FFFFCFFFFF7F7F7F7F7F7FFFC8CF00FFFF
          FFC8CF00FFFFFFC8CFCFFFFFCF67600000FFFF0000FF00007F7F7F7F7F7F7F7F
          7F7F7F7FBFBFBFBFBFBFBFBFBFBFBFBF00FFFFFFC8CFFFC8CF7F7F7FFFC8CFCF
          FFFFFFC8CF7F7F7F7F7F7FCFFFFFCFFFFFFFC8CFCFFFFF60302F0000FFFF0000
          CF67607F7F7F7F7F7F7F7F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFFFFFFFFFFF
          FFFFFFFF00FFFF7F7F7F7F7F7FCFFFFFCFFFFFCFFFFFFFC8CFCFFFFFFFC8CFCF
          6760FF97900000FFFF0000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFFFC8CFFFFFFFFFFFFF1F201F7F7F7FCFFFFFCFFFFF7F7F7F7F7F7FCFFF
          FFFFC8CFCFFFFFFFC8CF60302FFFC8CF0000FFFF0000BFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFFFC8CFCF6760CF67601F201F7F7F7F7F7F7F
          CFFFFFCFFFFFFFC8CFCFFFFFFFC8CFCFFFFFCF6760CFFFFFFF97900000FFFF00
          00BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCF6760FFC8CFFF9790CF
          67601F201F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFC8CFFFFFFF60302F
          CFFFFFFF97900000FFCF6760BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCF6760FFC8
          CFFFC8CFFF9790FF9790CF67601F201F7F7F7FCFFFFFFFFFFFCFFFFFFFFFFFCF
          FFFFFFC8CFCF6760FFFFFFFFFFFFFF00000000FFCF6760BFBFBFBFBFBFBFBFBF
          CF6760FFC8CFFFC8CFFFC8CFFF9790FF9790FF9790CF67601F201F7F7F7FFFFF
          FFFFFFFFFFFFFFFFC8CFCFFFFFFFC8CFFFFFFFFFFFFFFFFFFFCF6760CF6760BF
          BFBFBFBFBFCF6760FFC8CFFFFFFFFFC8CFFFC8CFFF9790CF6760CF6760CF6760
          CF67601F201F7F7F7FFFFFFF3F3700CFFFFFCF6760FFFFFF0000FF0000FF0000
          FFCF6760BFBFBFBFBFBFBFBFBF60302F60302F60302FFFC8CFFFC8CFFF9790CF
          67601F201F1F201F1F201F1F201F1F201FFFFFFFFFFFFFFFC8CFFFFFFFFFFFFF
          FFFFFFFFC8CFCF6760BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFFFC8
          CFFFC8CFFF9790CF67601F201F60302F60302F60302F60302FFFC8CFFFC8CFFF
          FFFFFF0000FF0000CF6760BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          FF9790FFC8CFFFC8CFFFFFFFFF9790CF67601F201F60302F0000FF0000FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFCF6760FFFFFFFFFFFFFFC8CFFF9790CF67601F201F60302F
          BFBFBFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCF6760FF9790FF9790FF9790FF9790CF
          67601F201F60302FBFBFBFBFBFBFFF0000FF0000BFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCF6760CF6760CF67
          60CF6760CF67601F201F60302F7F7F7FFFC8CFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBF}
        Visible = False
      end
      object btnTiposUniformes: TSpeedButton
        Left = 920
        Top = 360
        Width = 148
        Height = 41
        Caption = 'Tipos de Uniformes'
        Flat = True
        Visible = False
      end
      object btnGerarPedidos: TSpeedButton
        Left = 919
        Top = 407
        Width = 148
        Height = 43
        Caption = 'Gerar Pedidos'
        Flat = True
        Glyph.Data = {
          02030000424D0203000000000000360100002800000013000000170000000100
          080000000000CC010000C30E0000C30E000040000000000000001C3404002434
          1C00242424001C3C0400243C0C00244404002C5C04003C5C240044543C005C5C
          54005C5C5C00646464006C6C6C0054743C007474740044840400747C74007C7C
          7C0084848400449404006C8C540054AC0400000000008C8C8C008C948C009494
          94009C9C9C00A4A4A400ACACAC00B4B4B4006CD404006CDC040074F404007CFC
          040084FC0C0084FC14007CDC24008CFC1C008CFC240094FC240094EC3C0094FC
          2C009CFC3C0094D45C009CF44C009CFC4400A4FC4C00A4FC5400ACFC6400B4FC
          6C00B4F47400BCF48400BCFC7C00B4C4A400ACCC9400BCCCAC00BCC4B400BCCC
          B400B4E48C00BCE49400BCDCA400C4F49400C4FC8C00C0C0C0003F3F3F3F3F3F
          191717193F3F3F3F3F3F3F3F3F003F3F3F3F3F1712111112193F3F3F3F3F3F3F
          3F003F3F3F3F19120E0C0C0E123F3F3F3F3F3F3F3F003F3F3F3F120E0C0B0B0C
          11173F3F3F3F3F3F3F003F3F3F17110C0B0A0A0B0E123F3F3F3F3F3F3F003F3F
          3F12140702010B0B0C11173F3F3F3F3F3F003F3F3F181E1E0F03100C0C0E1219
          3F3F3F3F3F003F3F3F2422231F06080C0C0C11173F3F3F3F3F003F3F2B212223
          221305170C0C0E11173F3F3F3F003F3521222323231E06090E0C0C0E12193F3F
          3F003F2B2223272726221304180E0C0C0E123F3F3F003F2926252A2F2F261F06
          08110E0C0E11173F3F0038302D232C39332E23150311110E0C0E11173F003F39
          2E28383F37312A220F0117110E0E0E1219003F3F373F3F3F3F3A30261E060917
          110E0E1117003F3F3F3F3F3F3F3F322E2315030C1712111217003F3F3F3F3F3F
          3F3F37342D2313001819171719003F3F3F3F3F3F3F3F3F3B342E231300193F3F
          3F003F3F3F3F3F3F3F3F3F3F3C3330230F011D3F3F003F3F3F3F3F3F3F3F3F3F
          3F393E31250F0D3F3F003F3F3F3F3F3F3F3F3F3F3F3F383D312320353F003F3F
          3F3F3F3F3F3F3F3F3F3F3F3F3C2A23363F003F3F3F3F3F3F3F3F3F3F3F3F3F3F
          3F3F373F3F00}
        OnClick = btnGerarPedidosClick
        OnDblClick = btnALTERAR_SOLICITACAOClick
      end
      object Panel1: TPanel
        Left = 8
        Top = 8
        Width = 889
        Height = 430
        TabOrder = 0
        object dbgUniformes: TDBGrid
          Left = 12
          Top = 12
          Width = 865
          Height = 408
          DataSource = dsSel_Uniformes
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dbgUniformesDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'DSC_TIPOUNIFORME'
              Title.Caption = '[Tipo de Uniforme]'
              Width = 220
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vNumeracao'
              Title.Alignment = taCenter
              Title.Caption = '[Numera��o]'
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vCor'
              Title.Alignment = taCenter
              Title.Caption = '[Cor]'
              Width = 110
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vSexo'
              Title.Alignment = taCenter
              Title.Caption = '[Sexo]'
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTD_ADMISSAO'
              Title.Alignment = taCenter
              Title.Caption = '[Qtd p/ Admiss�o]'
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTD_REPOSICAO'
              Title.Alignment = taCenter
              Title.Caption = '[Qtd. p/ Reposi��o]'
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vPeriodoReposicao'
              Title.Alignment = taCenter
              Title.Caption = '[Per�odo p/ Reposi��o]'
              Width = 115
              Visible = True
            end>
        end
      end
      object Panel2: TPanel
        Left = 8
        Top = 444
        Width = 889
        Height = 65
        TabOrder = 1
        object GroupBox2: TGroupBox
          Left = 11
          Top = 6
          Width = 229
          Height = 48
          Caption = '[Tipo de Uniforme]'
          TabOrder = 0
          object cbTipoUniforme: TComboBox
            Left = 8
            Top = 18
            Width = 212
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 0
          end
        end
        object GroupBox3: TGroupBox
          Left = 464
          Top = 6
          Width = 113
          Height = 48
          Caption = '[Numera��o]'
          TabOrder = 1
          object cbNumeracao: TComboBox
            Left = 9
            Top = 16
            Width = 98
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 0
            Items.Strings = (
              '06'
              '08'
              '10'
              '12'
              'PP'
              'P'
              'M'
              'G'
              'GG'
              'XGG'
              '�NICO'
              '26'
              '27'
              '28'
              '29'
              '30'
              '31'
              '32'
              '33'
              '34'
              '35'
              '36'
              '37'
              '38'
              '39'
              '40'
              '41'
              '42'
              '43'
              '44')
          end
        end
        object GroupBox1: TGroupBox
          Left = 242
          Top = 6
          Width = 108
          Height = 48
          Caption = '[Cor]'
          TabOrder = 2
          object cbCor: TComboBox
            Left = 9
            Top = 16
            Width = 94
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 0
            Items.Strings = (
              'AZUL MARINHO'
              'BRANCA'
              'PRETA'
              'AZUL'
              'CINZA')
          end
        end
        object GroupBox4: TGroupBox
          Left = 351
          Top = 6
          Width = 113
          Height = 48
          Caption = '[Sexo]'
          TabOrder = 3
          object cbSexo: TComboBox
            Left = 9
            Top = 16
            Width = 97
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 0
            Items.Strings = (
              'MASCULINO'
              'FEMININO'
              'UNISSEX')
          end
        end
        object GroupBox5: TGroupBox
          Left = 577
          Top = 6
          Width = 80
          Height = 48
          Caption = '[Qtd p/ Adm.]'
          TabOrder = 4
          object edQtdParaAdmissao: TEdit
            Left = 9
            Top = 16
            Width = 60
            Height = 21
            TabOrder = 0
          end
        end
        object GroupBox6: TGroupBox
          Left = 658
          Top = 6
          Width = 80
          Height = 48
          Caption = '[Qtd p/ Rep.]'
          TabOrder = 5
          object edQtdParaReposicao: TEdit
            Left = 9
            Top = 16
            Width = 60
            Height = 21
            TabOrder = 0
          end
        end
        object GroupBox7: TGroupBox
          Left = 739
          Top = 6
          Width = 143
          Height = 48
          Caption = '[Per�odo Reposi��o]'
          TabOrder = 6
          object cbPeriodoReposicao: TComboBox
            Left = 9
            Top = 16
            Width = 125
            Height = 22
            Style = csOwnerDrawFixed
            ItemHeight = 16
            TabOrder = 0
            Items.Strings = (
              '06 MESES'
              '01 ANO')
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = '[Solicita��es de Uniformes]'
      ImageIndex = 1
      object GroupBox8: TGroupBox
        Left = 8
        Top = 8
        Width = 961
        Height = 86
        Caption = '[Crit�rios para Lan�amento da Solicita��o de Uniformes]'
        TabOrder = 0
        object btnVerAlunosDaUnidade: TSpeedButton
          Left = 585
          Top = 28
          Width = 110
          Height = 35
          Caption = 'Visualizar Alunos'
          OnClick = btnVerAlunosDaUnidadeClick
        end
        object SpeedButton2: TSpeedButton
          Left = 706
          Top = 28
          Width = 235
          Height = 35
          Caption = 'Consultar Pedidos'
          Flat = True
          Glyph.Data = {
            36050000424D360500000000000036040000280000000E000000100000000100
            08000000000000010000CA0E0000C30E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000ACACACACACAC
            ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
            00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
            02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
            5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
            00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
            ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
            D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
            D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
          OnClick = SpeedButton2Click
        end
        object GroupBox10: TGroupBox
          Left = 10
          Top = 18
          Width = 343
          Height = 53
          Caption = '[Unidade]'
          TabOrder = 0
          object cbUnidade: TComboBox
            Left = 10
            Top = 18
            Width = 325
            Height = 19
            Style = csOwnerDrawFixed
            ItemHeight = 13
            TabOrder = 0
          end
        end
        object GroupBox11: TGroupBox
          Left = 355
          Top = 18
          Width = 220
          Height = 53
          Caption = '[Per�odo da Solicita��o]'
          TabOrder = 1
          object cbMes: TComboBox
            Left = 10
            Top = 19
            Width = 145
            Height = 19
            Style = csOwnerDrawFixed
            ItemHeight = 13
            TabOrder = 0
            OnClick = cbMesClick
            Items.Strings = (
              '<SELECIONE>'
              'JANEIRO'
              'FEVEREIRO'
              'MAR�O'
              'ABRIL'
              'MAIO'
              'JUNHO'
              'JULHO'
              'AGOSTO'
              'SETEMBRO'
              'OUTUBRO'
              'NOVEMBRO'
              'DEZEMBRO')
          end
          object mskAno: TMaskEdit
            Left = 158
            Top = 18
            Width = 56
            Height = 21
            EditMask = '9999'
            MaxLength = 4
            TabOrder = 1
            Text = '    '
          end
        end
      end
      object PageControl2: TPageControl
        Left = 8
        Top = 100
        Width = 1061
        Height = 405
        ActivePage = TabSheet4
        TabOrder = 1
        object TabSheet4: TTabSheet
          Caption = '[Solicita��es]'
          OnShow = btnALTERAR_SOLICITACAOClick
          object btnGerarSolicitacao: TSpeedButton
            Left = 578
            Top = 250
            Width = 469
            Height = 117
            Caption = 'Gerar Solicita��o'
            OnClick = btnGerarSolicitacaoClick
          end
          object GroupBox12: TGroupBox
            Left = 574
            Top = 4
            Width = 473
            Height = 221
            Caption = '[Informe as op��es para a sele��o do uniforme]'
            TabOrder = 0
            object Label1: TLabel
              Left = 84
              Top = 79
              Width = 109
              Height = 13
              Caption = 'Numera��o Dispon�vel'
            end
            object Label2: TLabel
              Left = 83
              Top = 40
              Width = 81
              Height = 13
              Caption = 'Tipo de Uniforme'
            end
            object Label5: TLabel
              Left = 403
              Top = 35
              Width = 45
              Height = 13
              Caption = 'Tamanho'
              Visible = False
            end
            object Label4: TLabel
              Left = 235
              Top = 78
              Width = 55
              Height = 13
              Caption = 'Quantidade'
            end
            object Label3: TLabel
              Left = 299
              Top = 79
              Width = 23
              Height = 13
              Caption = 'Data'
            end
            object Label7: TLabel
              Left = 83
              Top = 137
              Width = 283
              Height = 40
              Alignment = taCenter
              Caption = 'ATEN��O!!! Quantidade solicitada para cada aluno e n�o o TOTAL.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object cbTipoUniforme2: TComboBox
              Left = 84
              Top = 56
              Width = 293
              Height = 19
              Style = csOwnerDrawFixed
              ItemHeight = 13
              TabOrder = 0
              OnClick = cbTipoUniforme2Click
            end
            object cbNumeracao2: TComboBox
              Left = 83
              Top = 94
              Width = 147
              Height = 19
              Style = csOwnerDrawFixed
              ItemHeight = 13
              TabOrder = 1
              OnClick = cbNumeracao2Click
            end
            object edTamanho: TEdit
              Left = 404
              Top = 49
              Width = 62
              Height = 21
              TabOrder = 2
              Visible = False
              OnEnter = edTamanhoEnter
              OnExit = edTamanhoExit
            end
            object edQtdSolicitada: TEdit
              Left = 234
              Top = 93
              Width = 58
              Height = 21
              TabOrder = 3
              OnEnter = edQtdSolicitadaEnter
              OnExit = edQtdSolicitadaExit
            end
            object mskDataSolicitacao: TMaskEdit
              Left = 299
              Top = 94
              Width = 78
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 4
              Text = '  /  /    '
            end
          end
          object dbgAlunos: TDBGrid
            Left = 8
            Top = 8
            Width = 563
            Height = 331
            DataSource = dsSel_Cadastro
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'cod_matricula'
                Title.Alignment = taCenter
                Title.Caption = '[Matr�cula]'
                Width = 85
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'nom_nome'
                Title.Caption = '[Nome do Aluno]'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ind_sexo'
                Title.Alignment = taCenter
                Title.Caption = '[Sexo]'
                Width = 90
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'vStatus'
                Title.Alignment = taCenter
                Title.Caption = '[Situa��o]'
                Visible = True
              end>
          end
          object Panel3: TPanel
            Left = 8
            Top = 344
            Width = 564
            Height = 27
            Caption = 
              'PARA SELECIONAR MAIS DE UM ALUNO, SEGURA A TECLA <CTRL>E V� CLIC' +
              'ANDO NOS NOMES.'
            Color = clRed
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = 'Arial Narrow'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object GroupBox9: TGroupBox
            Left = 1023
            Top = 75
            Width = 473
            Height = 152
            Caption = '[Op��es de Uniformes Dispon�veis de acordo com a sele��o acima]'
            TabOrder = 3
            Visible = False
            object dbgUniformesParaSolicitacao: TDBGrid
              Left = 9
              Top = 18
              Width = 453
              Height = 122
              DataSource = dsSel_Uniformes
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = dbgUniformesParaSolicitacaoCellClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DSC_TIPOUNIFORME'
                  Title.Caption = 'Tipo de Uniforme'
                  Width = 165
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vCor'
                  Title.Caption = 'Cor'
                  Width = 85
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vNumeracao'
                  Title.Caption = 'Numera��o'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vSexo'
                  Title.Caption = 'G�nero'
                  Width = 90
                  Visible = True
                end>
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = '[Consulta Solicita��es Realizadas]'
          ImageIndex = 2
          object btnALTERAR_SOLICITACAO: TSpeedButton
            Left = 897
            Top = 324
            Width = 148
            Height = 41
            Caption = '&Alterar Solicita��o'
            Flat = True
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000484848767676
              79797A79797A7B7B7B7C7C7C7C7C7C7D7D7D7E7E7E7E7E7E7F7F7F8080808080
              808181817373735757574D4D4DEBEBEBEAEAEAE6E6EBEEEEEEF0F0F0F2F2F2F4
              F4F4F6F6F6F8F8F8F9F9F9FBFBFBFCFCFCFEFEFEDFDFDF676767545454EDEDED
              ECECECE8E8EDF0F0F0F2F2F2F4F4F4F6F6F6F8F8F8F9F9F9FBFBFBFCFCFCFDFD
              FDFEFEFEDEDEDE707070555555E6E6E6E9E9E9E4E4E9ECECECEEEEEEF0F0F0EF
              EFEFF3F3F3F4F4F4F6F6F6F7F7F7F8F8F8F7F7F7D8D8D86E6E6E555555EDEDED
              EEEEEEE8E8EEF1F1F1F3F3F3F5F5F5BFCFD9DADFE3F8F8F8FAFAFAFBFBFBFAFA
              FAF9F9F9D9D9D96D6D6D565656E6E6E6ECECECE8E8EDF0F0F0F1F1F1F3F3F3A2
              C5DB78C6E8DCE5E8F8F8F8F7F7F7F6F6F6F5F5F5D6D6D66D6D6D565656F3F3F3
              F4F4F4E9E9EFF1F1F1F3F3F3F4F4F4D5E3EF49ACE98ED4EAF3F3F3F6F6F6F5F5
              F5F3F3F3D4D4D46D6D6D575757E9E9E9EFEFEFEDEDF3F6F6F6F8F8F8F9F9F9FA
              FAFA8DC0E855C3F4CBE1E7F8F8F8F7F7F7F5F5F5D6D6D66C6C6C575757F7F7F7
              F8F8F8ECECF2F4F4F4F6F6F6F7F7F7F8F8F8EAEFF450A9E678CFEDEBEBEBF2F2
              F2F0F0F0D2D2D26C6C6C575757ECECECF3F3F3F0F0F6F9F9F9FAFAFAFBFBFBFA
              FAFAF9F9F9A8CDEA4FBBF1B2D9E4F2F2F2F1F1F1D3D3D36B6B6B585858FAFAFA
              FBFBFBEFEFF4F7F7F7F8F8F8F7F7F7F6F6F6F5F5F5F1F2F35EACE467CBF0DDE1
              E2ECECECCFCFCF6A6A6A585858EFEFEFF6F6F6F0F0F5F8F8F8F7F7F7F6F6F6F5
              F5F5F3F3F3F2F2F2BFD5E78ABBD6D8DCE0E9E9E9CDCDCD6A6A6A585858FDFDFD
              FDFDFDF3F3F9FAFAFAF9F9F9F8F8F8F7F7F7F5F5F5F3F3F3F1F1F18A9AC77B98
              E4EAEAEACFCFCF696969595959F1F1F1F7F7F7F0F0F5F6F6F6F5F5F5F3F3F3F2
              F2F2F0F0F0EEEEEEECECECE7E7E9E3E4E7E6E6E6CACACA696969595959FEFEFE
              FEFEFEF4F4FAFBFBFBFAFAFAF8F8F8F6F6F6F5F5F5F3F3F3F0F0F0EEEEEEECEC
              ECEAEAEACECECE696969646464D7D7D7D9D9D9D3D3D7D7D7D7D6D6D6D5D5D5D4
              D4D4D2D2D2D1D1D1CFCFCFCECECECCCCCCCBCBCBB5B5B56E6E6E}
            OnClick = btnALTERAR_SOLICITACAOClick
          end
          object btnCONSULTA_SOLICITACOES: TSpeedButton
            Left = 897
            Top = 236
            Width = 148
            Height = 85
            Caption = 'Todas Solicita��es'
            Flat = True
            Glyph.Data = {
              36050000424D360500000000000036040000280000000E000000100000000100
              08000000000000010000CA0E0000C30E00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000ACACACACACAC
              ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
              00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
              02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
              5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
              00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
              ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
              D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
              D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
            OnClick = btnCONSULTA_SOLICITACOESClick
          end
          object btnExcluirSolicitacao: TSpeedButton
            Left = 897
            Top = 192
            Width = 148
            Height = 41
            Caption = 'Excluir Solicita��o'
            Flat = True
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
              FF00FFFF00FFFF00FF033B8A033D90013D95023B91033A89033A89FF00FFFF00
              FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0357D30147B20051D0035CE007
              63E3035CE0004ED30042B7023A8F023A8FFF00FFFF00FFFF00FFFF00FFFF00FF
              0650BA0357D32781F278B4F7CAE2FCE9F4FFDCEDFF9CC7FA3F8FF20155DD0140
              A404367DFF00FFFF00FFFF00FF075DD70762E155A0F7F3F8FEFFFFFFE9F3FCC6
              DEFAD9E9FCFFFFFFFFFFFF99C5F8055DE70040A302398BFF00FFFF00FF075DD7
              529EF7FEFEFFE2EFFC0F65EB0558E70959E50250E20454E16FA6F0DEEBFC9CC9
              F80355DE02398BFF00FF0455C9207DF0E1EFFEFFFFFF6FA7F076AFF7176CED06
              5AE9075AE60F5EE66AA1F06FA7F0FFFFFF3E8FF20043B7033E96085FDA56A1FA
              FFFFFF9ECBFB1573F779B4FACFE3FC1C72EF2274EECBE1FB6DA5F20556E3DEEB
              FC9FCBFA0050D4033E960F6BE68BC1FCFFFFFF2987FC1F7DFA1674F779B5FADE
              EDFEDDEDFC6EAAF4065AE90455E5A0C5F6DEEFFF0560E202409C1B76EDA4CFFC
              FFFFFF2988FF1C7EFE1C7BFB2D87FBEDF6FEEDF6FE2279F20B63ED085DEA88BA
              F4EBF6FF0C68E60141A1207AEBA5CFFEFFFFFF3F97FF1F81FF3B93FEE1EFFF6B
              ADFC69ABFBE0EEFE2C80F30C65EEC6DEFBCEE5FE0763E203419E146FE79ACAFC
              FFFFFFB2D8FF318EFFE7F3FF67AFFF1D7EFE1A7AFB60A7FCE5F2FE3F8FF6E2EF
              FE81BAF80258D8033E96FF00FF237BEBEDF6FFFAFCFF5DA9FF469AFF1F81FF1F
              81FF1E80FF1C7DFC4D9CFBF0F8FFF2F8FE3089F4024FC0FF00FFFF00FF237BEB
              BFDEFFF3F8FFFAFCFFB0D5FF3E96FF2B89FF308CFF6AB0FEFAFCFFFFFFFF5DA6
              F70860DE024FC0FF00FFFF00FFFF00FF4997F3C7E3FFF7FBFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFE0EFFE5CA5F80E6BE70552C2FF00FFFF00FFFF00FFFF00FF
              FF00FF2D82EB91C5FBCCE6FFD9EDFFDCEDFEC4E0FE86BFFC348BF40A65E10A65
              E1FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF247BEB4696F34A
              98F42F87F0116CE6075FDCFF00FFFF00FFFF00FFFF00FFFF00FF}
            OnClick = btnExcluirSolicitacaoClick
          end
          object btnVerSolicitacoes: TSpeedButton
            Left = 897
            Top = 10
            Width = 148
            Height = 73
            Caption = 'Visualizar Solicita��es'
            Enabled = False
            Flat = True
            Visible = False
          end
          object GroupBox15: TGroupBox
            Left = 11
            Top = 54
            Width = 878
            Height = 263
            TabOrder = 0
            object dbgSOLICITACOES: TDBGrid
              Left = 8
              Top = 15
              Width = 860
              Height = 235
              DataSource = dsSolicitacoes
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = dbgSOLICITACOESDblClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'cod_matricula'
                  Title.Alignment = taCenter
                  Title.Caption = '[Matr�cula]'
                  Width = 85
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nom_nome'
                  Title.Alignment = taCenter
                  Title.Caption = '[Nome Aluno]'
                  Width = 285
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'dsc_tipouniforme'
                  Title.Alignment = taCenter
                  Title.Caption = '[Tipo Uniforme]'
                  Width = 125
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vCorUniforme'
                  Title.Alignment = taCenter
                  Title.Caption = '[Cor]'
                  Width = 100
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'tam_uniforme'
                  Title.Alignment = taCenter
                  Title.Caption = '[Tam.]'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'qtd_uniforme'
                  Title.Alignment = taCenter
                  Title.Caption = '[Qtd.]'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vStatusSolicitacao'
                  Title.Alignment = taCenter
                  Title.Caption = '[Situa��o]'
                  Width = 115
                  Visible = True
                end>
            end
          end
          object GroupBox14: TGroupBox
            Left = 11
            Top = 6
            Width = 878
            Height = 49
            TabOrder = 1
            object Label6: TLabel
              Left = 16
              Top = 20
              Width = 59
              Height = 13
              Caption = 'Matr�cula:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lbNome: TLabel
              Left = 216
              Top = 19
              Width = 262
              Height = 18
              Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object btnPesquisar: TSpeedButton
              Left = 169
              Top = 14
              Width = 28
              Height = 24
              Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
              Glyph.Data = {
                66010000424D6601000000000000760000002800000014000000140000000100
                040000000000F0000000120B0000120B00001000000010000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                777777770000777777777777777700770000777777777777777C440700007777
                7777777777C4C40700007777777777777C4C4077000077777777777784C40777
                0000777777788888F740777700007777770000007807777700007777707EFEE6
                007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
                8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
                0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
                7777777700A077777777777777777777FFA077777777777777777777FFFF7777
                7777777777777777FF81}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnPesquisarClick
            end
            object txtMatricula: TMaskEdit
              Left = 82
              Top = 16
              Width = 83
              Height = 21
              Hint = 'Informe o N�MERO DE MATR�CULA'
              EditMask = '!99999999;1;_'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              MaxLength = 8
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Text = '        '
            end
          end
          object GroupBox16: TGroupBox
            Left = 12
            Top = 320
            Width = 434
            Height = 46
            Caption = '[Tipo de Uniforme]'
            TabOrder = 2
            object edTipoUniforme: TEdit
              Left = 8
              Top = 16
              Width = 417
              Height = 21
              ReadOnly = True
              TabOrder = 0
            end
          end
          object GroupBox17: TGroupBox
            Left = 447
            Top = 320
            Width = 164
            Height = 46
            Caption = '[Cor]'
            TabOrder = 3
            object edCor: TEdit
              Left = 8
              Top = 16
              Width = 146
              Height = 21
              ReadOnly = True
              TabOrder = 0
            end
          end
          object GroupBox18: TGroupBox
            Left = 612
            Top = 320
            Width = 160
            Height = 46
            Caption = '[Numera��o]'
            TabOrder = 4
            object edNumeracao: TEdit
              Left = 8
              Top = 16
              Width = 141
              Height = 21
              TabOrder = 0
            end
          end
          object GroupBox19: TGroupBox
            Left = 773
            Top = 320
            Width = 117
            Height = 46
            Caption = '[Quantidade]'
            TabOrder = 5
            object edQtd: TEdit
              Left = 9
              Top = 16
              Width = 100
              Height = 21
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object dsSel_Uniformes: TDataSource
    DataSet = dmDeca.cdsSel_Uniformes
    Left = 972
    Top = 24
  end
  object dsSel_Cadastro: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro
    Left = 972
    Top = 64
  end
  object dsSolicitacoes: TDataSource
    DataSet = dmDeca.cdsSel_UniformesSolicitacoes
    Left = 972
    Top = 104
  end
end
