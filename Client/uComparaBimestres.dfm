object frmComparaBimestres: TfrmComparaBimestres
  Left = 537
  Top = 331
  Width = 346
  Height = 180
  Caption = '[Sistema Deca] - Comparativo de Bimestres'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnComparar: TSpeedButton
    Left = 4
    Top = 106
    Width = 331
    Height = 42
    Caption = 'Comparar e Gerar Planilha'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = btnCompararClick
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 2
    Width = 333
    Height = 100
    Caption = '[Selecione os bimestres para comparação]'
    TabOrder = 0
    object cbBimestre1: TComboBox
      Left = 8
      Top = 26
      Width = 243
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      Items.Strings = (
        '1.º Bimestre'
        '2.º Bimestre'
        '3.º Bimestre')
    end
    object cbBimestre2: TComboBox
      Left = 8
      Top = 67
      Width = 243
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        '1.º Bimestre'
        '2.º Bimestre'
        '3.º Bimestre')
    end
    object edAno1: TEdit
      Left = 256
      Top = 26
      Width = 70
      Height = 21
      TabOrder = 2
    end
    object edAno2: TEdit
      Left = 256
      Top = 66
      Width = 70
      Height = 21
      TabOrder = 3
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = 'Planilha do MS-Excel|*.xls'
    Left = 224
    Top = 8
  end
end
