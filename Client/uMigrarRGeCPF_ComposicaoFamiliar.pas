unit uMigrarRGeCPF_ComposicaoFamiliar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, Buttons, Db;

type
  TfrmMigrarRGeCPF_ComposicaoFamiliar = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    dbgComposicaoFamiliar: TDBGrid;
    dsComposicaoFamiliar: TDataSource;
    btnIniciarMigracao: TSpeedButton;
    ProgressBar1: TProgressBar;
    procedure btnIniciarMigracaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMigrarRGeCPF_ComposicaoFamiliar: TfrmMigrarRGeCPF_ComposicaoFamiliar;

implementation

uses uPrincipal, uDM, uUtil;

{$R *.DFM}

procedure TfrmMigrarRGeCPF_ComposicaoFamiliar.btnIniciarMigracaoClick(
  Sender: TObject);
var mmRG, mmCPF : String;
begin
  ProgressBar1.Min := 0;
  //Pede confirma��o para o usu�rio
  if Application.MessageBox ('Deseja realizar a migra��o dos documentos RG e CPF do ' + #13#10 +
                             'tipo de documento para os campos RG e CPF da Ficha de Composi��o Familiar?',
                             '[Sistema Deca] - Migrar Dados',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   := 0;
        Open;

        ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;
        
        while not (dmDeca.cdsSel_Cadastro.eof) do
        begin

          //Consultar dados de composi��o familiar
          with dmDeca.cdsSel_Cadastro_Familia do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_cod_familiar').Value  := Null;
            Open;

            ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

            if dmDeca.cdsSel_Cadastro_Familia.RecordCount > 0 then
            begin
              dmDeca.cdsSel_Cadastro_Familia.First;
              while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
              begin

                mmRG  := '00.000.000-0';
                mmCPF := '000.000.000-00';

                //No caso de n�o haver nenhum tipo de documento cadastrado...
                if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_tipo_documento').IsNull) then
                begin
                  mmRG  := '00.000.000-0';
                  mmCPF := '000.000.000-00';
                end
                else
                if (Trim(dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_tipo_documento').Value) = 'RG') then
                begin
                  if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_documento').IsNull) then
                    mmRG := '00.000.000-0'
                  else
                    mmRG := Trim(dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_documento').Value);
                end
                else
                if (Trim(dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_tipo_documento').Value) = 'CPF') then
                begin
                  if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_documento').IsNull) then
                    mmCPF := '000.000.000-00'
                  else
                    mmCPF := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_documento').Value;
                end;


                with dmDeca.cdsDesabilitaTriggers_Responsavel do
                begin
                  Close;
                  Execute;
                end;

                //Atualiza os "novos" campos RG e CPF
                with dmDeca.cdsUPD_RGCPF_Fam do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_familiar').Value  := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('cod_familiar').Value;
                  Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_rg_fam').Value    := mmRG;
                  Params.ParamByName ('@pe_num_cpf_fam').Value   := mmCPF;
                  Execute;
                end;

                with dmDeca.cdsHabilitaTriggers_Responsavel do
                begin
                  Close;
                  Execute;
                end;



                dmDeca.cdsSel_Cadastro_Familia.Next;
              end;
            end
            else
            begin
              //N�o foram encontrados familiares
            end;
          end;
          dmDeca.cdsSel_Cadastro.Next;
          ProgressBar1.Position := ProgressBar1.Position + 1;
        end;
        dbgComposicaoFamiliar.Refresh;
      end;
    except end;
  end;
end;

end.
