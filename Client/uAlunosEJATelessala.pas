unit uAlunosEJATelessala;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, Mask;

type
  TfrmAlunosEjaTelessala = class(TForm)
    GroupBox1: TGroupBox;
    cbBimestre: TComboBox;
    Label1: TLabel;
    mskAnoLetivo: TMaskEdit;
    btnVer: TBitBtn;
    procedure btnVerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlunosEjaTelessala: TfrmAlunosEjaTelessala;

implementation

uses uDM, rAlunosEjaTelessala;

{$R *.DFM}

procedure TfrmAlunosEjaTelessala.btnVerClick(Sender: TObject);
begin

  try
    if (cbBimestre.ItemIndex <> -1 ) and (Length(mskAnoLetivo.Text)<4) then
    begin
      ShowMessage ('Campos devem ser preenchidos/selecionados...');
      cbBimestre.SetFocus;
    end
    else
    begin
      with dmDeca.cdsSel_EjaTelessala do
      begin
        Close;
        Params.ParamByName ('@pe_num_bimestre').Value   := cbBimestre.ItemIndex + 1;
        Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(mskAnoLetivo.Text);
        Open;

        Application.CreateForm (TrelAlunosEJATelessala, relAlunosEJATelessala);
        relAlunosEJATelessala.lbPeriodo.Caption := 'PER�ODO: ' + cbBimestre.Text + ' / '+mskAnoLetivo.Text;
        relAlunosEJATelessala.Preview;
        relAlunosEJATelessala.Free;

      end;
    end;
  except end;

end;

end.
