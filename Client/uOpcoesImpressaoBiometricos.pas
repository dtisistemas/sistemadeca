unit uOpcoesImpressaoBiometricos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TfrmOpcoesImpressaoBiometricos = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cbUnidade: TComboBox;
    chkTodasUnidades: TCheckBox;
    chkApenasFolhaRosto: TCheckBox;
    btnVisualizar: TBitBtn;
    procedure chkTodasUnidadesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOpcoesImpressaoBiometricos: TfrmOpcoesImpressaoBiometricos;
  vUnidadeOp1, vUnidadeOp2 : TStringList;

implementation

uses uDM, uPrincipal, rAvaliacaoBiometrica, uUtil;

{$R *.DFM}

procedure TfrmOpcoesImpressaoBiometricos.chkTodasUnidadesClick(
  Sender: TObject);
begin
  if chkTodasUnidades.Checked then
  begin
    cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := False
  end
  else
    cbUnidade.Enabled := True;
end;

procedure TfrmOpcoesImpressaoBiometricos.FormShow(Sender: TObject);
begin

  vUnidadeOp1 := TStringLIst.Create();
  vUnidadeOp2 := TStringLIst.Create();

  //Carrega os dados das unidades no combo cbUnidade
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        vUnidadeOp1.Add(IntToStr(FieldByName('cod_unidade').AsInteger));
        vUnidadeOp2.Add(FieldByName('nom_unidade').AsString);
        cbUnidade.Items.Add(FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;

    end;
  except end;
end;

procedure TfrmOpcoesImpressaoBiometricos.btnVisualizarClick(
  Sender: TObject);
begin

  with dmDeca.cdsExc_TempBiometrica do
  begin
    Close;
    Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
    Execute;
  end;

  try
  with dmDeca.cdsSel_CadastroParaBiometrico do
  begin
    Close;
    Params.ParamByName('@pe_cod_matricula').Value := Null;
    if (chkTodasUnidades.Checked) then Params.ParamByName('@pe_cod_unidade').Value := Null else Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidadeOp1.Strings[cbUnidade.ItemIndex]);
    Open;

    if (RecordCount < 1) then
      ShowMessage ('Nenhum registro encontrado...')
    else
    begin

      while not dmDeca.cdsSel_CadastroParaBiometrico.eof do
      begin

        with dmDeca.cdsSel_UltimoLancamentoBiometrico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('cod_matricula').Value;
          Open;


            with dmDeca.cdsInc_TempBiometrica do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('cod_matricula').Value;
              Params.ParamByName('@pe_nom_nome').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('nom_nome').Value;
              Params.ParamByName('@pe_nom_unidade').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('nom_unidade').Value;
              Params.ParamByName('@pe_idade').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('idade').Value;
              Params.ParamByName('@pe_ind_sexo').Value := Copy(dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('ind_sexo').Value,1,1);
              Params.ParamByName('@pe_val_peso').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('val_peso').Value;
              Params.ParamByName('@pe_val_altura').Value := dmDeca.cdsSel_CadastroParaBiometrico.FieldByName('val_altura').Value;
              Params.ParamByName('@pe_val_imc').Value := dmDeca.cdsSel_UltimoLancamentoBiometrico.FieldByName('val_imc').Value;
              Params.ParamByName('@pe_dsc_classificacao').Value := dmDeca.cdsSel_UltimoLancamentoBiometrico.FieldByName('dsc_classificacao_imc').Value;
              Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
              Execute;
            end;

        end;

        dmDeca.cdsSel_CadastroParaBiometrico.Next;

      end;
    end;

  end;
  except end;
  
  //Apresenta o relatorio
  Application.CreateForm (TrelAvaliacaoBiometrica, relAvaliacaoBiometrica);

  try
    with dmDeca.cdsSel_TMPBiometricos do
    begin
      Close;

      if (chkTodasUnidades.Checked = True) then
        Params.ParamByName('@pe_nom_unidade').Value := Null
      else
        Params.ParamByName('@pe_nom_unidade').Value := cbUnidade.Text;
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;


      //Inibe a exibi��o das colunas Peso, Altura, IMC e Classificacao
      if (chkApenasFolhaRosto.Checked = True) then
      begin
        relAvaliacaoBiometrica.QRDBText6.Enabled := False;
        relAvaliacaoBiometrica.QRDBText7.Enabled := False;
        relAvaliacaoBiometrica.QRDBText8.Enabled := False;
        relAvaliacaoBiometrica.QRDBText9.Enabled := False;
      end
      else
      begin
        relAvaliacaoBiometrica.QRDBText6.Enabled := True;
        relAvaliacaoBiometrica.QRDBText7.Enabled := True;
        relAvaliacaoBiometrica.QRDBText8.Enabled := True;
        relAvaliacaoBiometrica.QRDBText9.Enabled := True;
      end;

      relAvaliacaoBiometrica.lbUnidade.Caption := cbUnidade.Text;
      relAvaliacaoBiometrica.Prepare;
      relAvaliacaoBiometrica.lbTotalPaginas.Caption := ' de ' + IntToStr(relAvaliacaoBiometrica.QRPrinter.PageCount);
      relAvaliacaoBiometrica.Preview;
      relAvaliacaoBiometrica.Free;

    end;
  except end;

end;

end.
