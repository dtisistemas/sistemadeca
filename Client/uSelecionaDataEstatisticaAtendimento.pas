unit uSelecionaDataEstatisticaAtendimento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, Buttons;

type
  TfrmSelecionaDataEstatisticaAtendimento = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    meData1: TMaskEdit;
    meData2: TMaskEdit;
    btnEstatisticas: TSpeedButton;
    procedure btnEstatisticasClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaDataEstatisticaAtendimento: TfrmSelecionaDataEstatisticaAtendimento;

implementation

uses uDM, rEstatisticasAtendimento, uFichaCrianca, uPrincipal, uLoginNovo;

{$R *.DFM}

procedure TfrmSelecionaDataEstatisticaAtendimento.btnEstatisticasClick(
  Sender: TObject);
begin
  //Valida as datas do formulário
  if (Length(meData1.Text) = 10) AND (Length(meData2.Text) = 10) then  //preenchida a data no formato 99/99/9999
  begin

    Application.CreateForm(TrelEstatisticasAtendimento, relEstatisticasAtendimento);

    //***********************************************************************************************************************************
    //Calcula o total de Atendimentos de Criança
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lblTotalCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lblTotalCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcula o total de Atendimentos de Adolescente
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTotalAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTotalAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcula o total de Atendimentos de Família
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTotalFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTotalFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcula o total de Atendimentos de Profissional
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTotalProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTotalProfissional.Caption := '0';
    end;
    except end;


    //***********************************************************************************************************************************
    //***********************************************************************************************************************************
    //Posteriormente, criar uma tabela para armazenar os dados gerados através das procedures
    //contendo os campos TIPO(C,A,F,P) , CLASSIFICACAO, QTD.
    //***********************************************************************************************************************************
    //***********************************************************************************************************************************



    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 1-ABORDAGEM GRUPAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 1;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAbordagemCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAbordagemCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 2-ENTREVISTA INICIAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 2;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbEntrevistaInicialCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbEntrevistaInicialCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 3-ATENDIMENTO A COMUNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 3;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoComunidadeCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoComunidadeCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 4-ATENDIMENTO INDIVIDUAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 4;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoIndividualCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoIndividualCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 5-ATIVIDADES EXTRA-PROJETO/UNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 5;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtividadesExtraCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtividadesExtraCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 6-CURSO DE CAPACITAÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 6;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbCursoCapacitacaoCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbCursoCapacitacaoCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 7-DISCUSSÃO DE CASOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 7;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 8-DISCUSSÃO DE CASOS(REDE DE SERVIÇOS)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 8;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosRedeCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosRedeCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 9-GRUPO MULTIFAMÍLIA
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 9;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMultifamiliaCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMultifamiliaCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 10-GRUPO TEMÁTICO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 10;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTematicoCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTematicoCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 11-INTEGRAÇÃO FAMÍLIA/INSTITUIÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 11;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbIntegracaoCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbIntegracaoCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 12-MAPEAMENTO POR VULNERABILIDADES
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 12;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMapeamentoCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMapeamentoCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 13-REGISTROS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 13;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRegistrosDiversosCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRegistrosDiversosCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 14-RELATÓRIOS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 14;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRelatoriosDiversosCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRelatoriosDiversosCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 15-REUNIÃO INFORMATIVA (Reunião de Pais)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 15;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReuniaoPaisCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReuniaoPaisCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 16-REUNIÕES TÉCNICAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 16;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReunioesTecnicasCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReunioesTecnicasCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 17-SUPERVISÃO DE ESTÁGIO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 17;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbSupervisaoCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbSupervisaoCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 18-TREINAMENTO APRENDIZ
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 18;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTreinamentoCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTreinamentoCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 19-VISITA DOMICILIAR
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 19;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaDomiciliarCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaDomiciliarCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 20-VISITA INSTITUCIONAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 20;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaInstitucionalCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaInstitucionalCrianca.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: CRIANÇA
    //Classificação: 21-VISITA TÉCNICA A EMPRESAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 0;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 21;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaTecnicaCrianca.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaTecnicaCrianca.Caption := '0';
    end;
    except end;


    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 1-ABORDAGEM GRUPAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 1;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAbordagemAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAbordagemAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 2-ENTREVISTA INICIAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 2;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbEntrevistaInicialAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbEntrevistaInicialAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 3-ATENDIMENTO A COMUNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 3;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoComunidadeAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoComunidadeAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 4-ATENDIMENTO INDIVIDUAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 4;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoIndividualAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoIndividualAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 5-ATIVIDADES EXTRA-PROJETO/UNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 5;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtividadesExtraAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtividadesExtraAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 6-CURSO DE CAPACITAÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 6;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbCursoCapacitacaoAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbCursoCapacitacaoAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 7-DISCUSSÃO DE CASOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 7;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 8-DISCUSSÃO DE CASOS(REDE DE SERVIÇOS)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 8;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosRedeAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosRedeAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 9-GRUPO MULTIFAMÍLIA
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 9;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMultifamiliaAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMultifamiliaAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 10-GRUPO TEMÁTICO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 10;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTematicoAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTematicoAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 11-INTEGRAÇÃO FAMÍLIA/INSTITUIÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 11;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbIntegracaoAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbIntegracaoAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 12-MAPEAMENTO POR VULNERABILIDADES
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 12;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMapeamentoAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMapeamentoAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 13-REGISTROS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 13;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRegistrosDiversosAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRegistrosDiversosAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 14-RELATÓRIOS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 14;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRelatoriosDiversosAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRelatoriosDiversosAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 15-REUNIÃO INFORMATIVA (Reunião de Pais)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 15;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReuniaoPaisAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReuniaoPaisAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 16-REUNIÕES TÉCNICAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 16;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReunioesTecnicasAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReunioesTecnicasAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 17-SUPERVISÃO DE ESTÁGIO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 17;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbSupervisaoAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbSupervisaoAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 18-TREINAMENTO APRENDIZ
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 18;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTreinamentoAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTreinamentoAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 19-VISITA DOMICILIAR
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 19;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaDomiciliarAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaDomiciliarAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 20-VISITA INSTITUCIONAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 20;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaInstitucionalAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaInstitucionalAdolescente.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: ADOLESCENTE
    //Classificação: 21-VISITA TÉCNICA AS EMPRESAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 1;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 21;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaTecnicaAdolescente.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaTecnicaAdolescente.Caption := '0';
    end;
    except end;


    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 1-ABORDAGEM GRUPAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 1;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAbordagemFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAbordagemFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 2-ENTREVISTA INICIAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 2;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbEntrevistaInicialFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbEntrevistaInicialFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 3-ATENDIMENTO A COMUNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 3;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoComunidadeFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoComunidadeFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 4-ATENDIMENTO INDIVIDUAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 4;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoIndividualFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoIndividualFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 5-ATIVIDADES EXTRA-PROJETO/UNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 5;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtividadesExtraFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtividadesExtraFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 6-CURSO DE CAPACITAÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 6;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbCursoCapacitacaoFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbCursoCapacitacaoFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 7-DISCUSSÃO DE CASOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 7;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 8-DISCUSSÃO DE CASOS(REDE DE SERVIÇOS)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 8;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosRedeFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosRedeFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 9-GRUPO MULTIFAMÍLIA
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 9;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMultifamiliaFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMultifamiliaFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 10-GRUPO TEMÁTICO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 10;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTematicoFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTematicoFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 11-INTEGRAÇÃO FAMÍLIA/INSTITUIÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 11;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbIntegracaoFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbIntegracaoFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 12-MAPEAMENTO POR VULNERABILIDADES
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 12;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMapeamentoFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMapeamentoFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 13-REGISTROS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 13;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRegistrosDiversosFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRegistrosDiversosFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 14-RELATÓRIOS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 14;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRelatoriosDiversosFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRelatoriosDiversosFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 15-REUNIÃO INFORMATIVA (Reunião de Pais)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 15;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReuniaoPaisFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReuniaoPaisFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 16-REUNIÕES TÉCNICAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 16;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReunioesTecnicasFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReunioesTecnicasFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 17-SUPERVISÃO DE ESTÁGIO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 17;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbSupervisaoFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbSupervisaoFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 18-TREINAMENTO APRENDIZ
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 18;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTreinamentoFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTreinamentoFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 19-VISITA DOMICILIAR
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 19;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaDomiciliarFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaDomiciliarFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 20-VISITA INSTITUCIONAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 20;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaInstitucionalFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaInstitucionalFamilia.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 21-VISITA TÉCNICA AS EMPRESAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 2;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 21;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaTecnicaFamilia.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaTecnicaFamilia.Caption := '0';
    end;
    except end;


    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 1-ABORDAGEM GRUPAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 1;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAbordagemProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAbordagemProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 2-ENTREVISTA INICIAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 2;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbEntrevistaInicialProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbEntrevistaInicialProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 3-ATENDIMENTO A COMUNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 3;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoComunidadeProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoComunidadeProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 4-ATENDIMENTO INDIVIDUAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 4;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtendimentoIndividualProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtendimentoIndividualProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 5-ATIVIDADES EXTRA-PROJETO/UNIDADE
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 5;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbAtividadesExtraProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbAtividadesExtraProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 6-CURSO DE CAPACITAÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 6;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbCursoCapacitacaoProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbCursoCapacitacaoProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 7-DISCUSSÃO DE CASOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 7;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 8-DISCUSSÃO DE CASOS(REDE DE SERVIÇOS)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 8;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbDiscussaoCasosRedeProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbDiscussaoCasosRedeProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 9-GRUPO MULTIFAMÍLIA
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 9;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMultifamiliaProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMultifamiliaProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 10-GRUPO TEMÁTICO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 10;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTematicoProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTematicoProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 11-INTEGRAÇÃO FAMÍLIA/INSTITUIÇÃO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 11;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbIntegracaoProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbIntegracaoProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 12-MAPEAMENTO POR VULNERABILIDADES
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 12;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbMapeamentoProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbMapeamentoProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 13-REGISTROS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 13;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRegistrosDiversosProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRegistrosDiversosProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 14-RELATÓRIOS DIVERSOS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 14;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbRelatoriosDiversosProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbRelatoriosDiversosProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: FAMÍLIA
    //Classificação: 15-REUNIÃO INFORMATIVA (Reunião de Pais)
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 15;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReuniaoPaisProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReuniaoPaisProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 16-REUNIÕES TÉCNICAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 16;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbReunioesTecnicasProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbReunioesTecnicasProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 17-SUPERVISÃO DE ESTÁGIO
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 17;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbSupervisaoProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbSupervisaoProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 18-TREINAMENTO APRENDIZ
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 18;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbTreinamentoProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbTreinamentoProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 19-VISITA DOMICILIAR
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 19;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaDomiciliarProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaDomiciliarProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 20-VISITA INSTITUCIONAL
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 20;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaInstitucionalProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaInstitucionalProfissional.Caption := '0';
    end;
    except end;

    //***********************************************************************************************************************************
    //Calcular dados por tipo e classificação
    //Tipo: PROFISSIONAL
    //Classificação: 21-VISITA TÉCNICA AS EMPRESAS
    //***********************************************************************************************************************************
    try
    with dmDeca.cdsTotaliza_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_ind_tipo_indicador').Value := 3;
      Params.ParamByName('@pe_cod_id_classificacao').Value := 21;
      Params.ParamByName('@pe_dat_registro1').Value := StrToDate(meData1.Text);
      Params.ParamByName('@pe_dat_registro2').Value := StrToDate(meData2.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Open;
      if RecordCount > 0 then relEstatisticasAtendimento.lbVisitaTecnicaProfissional.Caption := FieldByName('nTotal').Value
      else relEstatisticasAtendimento.lbVisitaTecnicaProfissional.Caption := '0';
    end;
    except end;   


    relEstatisticasAtendimento.lbReferencia.Caption := 'REFERÊNCIA: ' + meData1.Text + ' a ' + meData2.Text;
    relEstatisticasAtendimento.lbDadosUsuario.Caption := 'ATENDIMENTOS DO USUÁRIO: [' + vvNOM_USUARIO + ']';
    relEstatisticasAtendimento.Preview;
    relEstatisticasAtendimento.Free;

  end;
end;

procedure TfrmSelecionaDataEstatisticaAtendimento.FormKeyPress(
  Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmSelecionaDataEstatisticaAtendimento.Close;
  end;
end;

end.
