unit uGerarPedidoUniformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, Grids, DBGrids, ExtCtrls, StdCtrls, Mask, Db;

type
  TfrmGerarPedidoUniformes = class(TForm)
    GroupBox11: TGroupBox;
    cbMes: TComboBox;
    mskAno: TMaskEdit;
    Panel1: TPanel;
    dbgSOLICITACOES: TDBGrid;
    btnGerarPedidos: TSpeedButton;
    btnPesquisar: TSpeedButton;
    dsSolicitacoes: TDataSource;
    SpeedButton2: TSpeedButton;
    btnComprovanteRecebimentoUniforme: TSpeedButton;
    Memo1: TMemo;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnGerarPedidosClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnComprovanteRecebimentoUniformeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerarPedidoUniformes: TfrmGerarPedidoUniformes;
  mmNUM_PEDIDO, mmCOD_UNIDADE : Integer;

implementation

uses uDM, rConsultaPedidosUniformes, uConsultarPedidosUniformes,
  uEmiteComprovanteRecebimentoUniforme;

{$R *.DFM}

procedure TfrmGerarPedidoUniformes.btnPesquisarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes.ItemIndex;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := Trim(mskAno.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := 0;  //Apenas os que ainda est�o liberados
      Params.ParamByName ('@pe_cod_unidade').Value        := Null;  //Todas as unidades que fizeram solicita��es de uniformes
      Open;
      dbgSOLICITACOES.Refresh;
    end;
  except end;
end;

procedure TfrmGerarPedidoUniformes.btnGerarPedidosClick(Sender: TObject);
begin

  try
    with dmDeca.cdsUPD_GeraPedidosUniforme do
    begin
      Close;
      Params.ParamByName ('@pe_mes_solicitacao').AsInteger := cbMes.ItemIndex;
      Params.ParamByName ('@pe_ano_solicitacao').AsInteger := StrToInt(mskAno.Text);
      Execute;

      Application.MessageBox ('As solicita��es dos uniformes foram geradas PEDIDOS.' + #13+#10 +
                              'Qualquer d�vida entre em contato com a Equipe da Divis�o de Suprimentos/Almoxarifado.',
                              '[Sistema Deca] - Gera Pedido de Uniformes',
                              MB_OK + MB_ICONINFORMATION);
      //Atualizar o grid
      //btnPesquisar.Click;

      //Ao gerar os pedidos, para cada unidade gerar um n�mero de pedido diferente
      try
        with dmDeca.cdsSel_UniformesSolicitacoes do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Null;
          Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMes.ItemIndex;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAno.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
          Params.ParamByName ('@pe_flg_bloqueado').Value      := 1;  //Pedidos j� realizados - rotina acima
          Params.ParamByName ('@pe_cod_unidade').Value        := Null;
          Open;

          if dmDeca.cdsSel_UniformesSolicitacoes.RecordCount > 0 then
          begin
            //Registros encontrados...
            dmDeca.cdsSel_UniformesSolicitacoes.First;

            mmNUM_PEDIDO := 1;
            //Gerar e Gravar dados na tabela de Pedido - utilizar o "�ltimo + 1"
            mmCOD_UNIDADE := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_unidade').Value;

            while not dmDeca.cdsSel_UniformesSolicitacoes.eof do
            begin

              while (mmCOD_UNIDADE = dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_unidade').Value) do
              begin
                //Grava o pedido atual para a mesma unidade... na tabela de UniformesSolicitacoes - coluna = NUM_PEDIDO
                Memo1.Lines.Add ('Pedido N.� ' + IntToStr(mmNUM_PEDIDO) + ' - Per�odo: ' + cbMes.Text + '/' + mskAno.Text + ' - Unidade: ' + dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('nom_unidade').Value);
                dmDeca.cdsSel_UniformesSolicitacoes.Next;
                if dmDeca.cdsSel_UniformesSolicitacoes.eof then mmCOD_UNIDADE := 0;
              end;

              mmNUM_PEDIDO  := mmNUM_PEDIDO + 1;
              //Gerar e Gravar dados na tabela de Pedido - utilizar o "�ltimo + 1"
              mmCOD_UNIDADE := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_unidade').Value;
              
            end;

          end
          else
          begin
            Application.MessageBox ('Aten��o !!! ' +#13+#10 +
                                    'N�o h� dados encontrados para o per�odo informado.',
                                    '[Sistema Deca] - Pedidos de Uniformes',
                                    MB_OK + MB_ICONINFORMATION);
          end;

        end;
      except end;

    end;
  except end;

end;

procedure TfrmGerarPedidoUniformes.SpeedButton2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConsultarPedidosUniformes, frmConsultarPedidosUniformes);
  frmConsultarPedidosUniformes.ShowModal;
end;

procedure TfrmGerarPedidoUniformes.btnComprovanteRecebimentoUniformeClick(
  Sender: TObject);
begin
  Application.CreateForm (TfrmEmiteComprovanteRecebimentoUniforme, frmEmiteComprovanteRecebimentoUniforme);
  frmEmiteComprovanteRecebimentoUniforme.ShowModal;
end;

end.
