object frmImportaEducacensoAcompEscolar: TfrmImportaEducacensoAcompEscolar
  Left = 756
  Top = 369
  Width = 526
  Height = 150
  Caption = 
    '[Sstema Deca] - Importação dados Educacenso/Acompanhamento Escol' +
    'ar'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 32
    Top = 24
    Width = 441
    Height = 73
    Caption = 'Selecionar arquivo e importar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton1Click
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.csv'
    Filter = 'Arquivos CSV|*.csv|Arquivos TXT|*.txt'
    Left = 408
    Top = 56
  end
end
