unit uTrocaUsuario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Mask;

type
  TfrmTrocaUsuario = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Image1: TImage;
    Label1: TLabel;
    cbListaUsuario: TComboBox;
    edSenha: TMaskEdit;
    btnEntrar2: TBitBtn;
    btnSair2: TBitBtn;
    procedure btnSair2Click(Sender: TObject);
    procedure btnEntrar2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTrocaUsuario: TfrmTrocaUsuario;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmTrocaUsuario.btnSair2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmTrocaUsuario.btnEntrar2Click(Sender: TObject);
var
  vOk : boolean;
  i : Integer;

begin

  // autentica��o do usu�rio
  vOk := true;
  try
    with uDM.dmDeca.cdsSel_Usuario do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').value:= NULL;
      Params.ParamByName('@pe_cod_unidade').value:= NULL;
      Params.ParamByName('@pe_nom_usuario').value:= cbListaUsuario.Items[cbListaUsuario.ItemIndex];
      vvNOM_USUARIO := cbListaUsuario.Items[cbListaUsuario.ItemIndex];
      Open;

      if (Trim(edSenha.Text) = '') or
         (Trim(FieldByName('dsc_senha').AsString) <> Trim(edSenha.Text)) then
      begin
        Application.MessageBox('Senha Inv�lida!'+#13+#10+#13+#10+
             'Em caso de perda da senha, favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Autentica��o',
             MB_OK + MB_ICONERROR);
        edSenha.Clear;
        edSenha.SetFocus;
        vOk := false
      end
      else begin

        //Atualiza o campo flg_status do usu�rio para ON-LINE
        try
          with dmDeca.cdsAlt_Status_Usuario do
          begin
            Close;
            Params.ParamByName ('@pe_cod_usuario').Value := dmDeca.cdsSel_Usuario.FieldByname ('cod_usuario').Value;
            Params.ParamByName ('@pe_flg_status').Value := 0; // 0 para On-Line e 1 para Off-Line
            Execute;

            Application.MessageBox ('Bem-Vindo ao Sistema Deca' + #13+#10+#13+#10 +
                                    'A partir de agora voc� est� ON-LINE. Clique OK e Bom Trabalho!',
                                    'Sistema DECA - Autenticado',
                                    MB_OK + MB_ICONQUESTION);

          end;
        except end;

        // Atualiza o StatusBar com o nome do usuario
        //frmPrincipal.StatusBar1.Panels[0].Text := 'Usu�rio(a): ' + FieldByName('nom_usuario').AsString +
                                                  //'  -  ON-LINE';

        frmPrincipal.StatusBar1.Panels[1].Text := 'Usu�rio(a): ' + FieldByName('nom_usuario').AsString +
                                                  '  -  ON-LINE';

        // carrega as vari�veis publicas
        vvCOD_USUARIO := FieldByName('cod_usuario').AsInteger;
        vvIND_PERFIL  := FieldByName('ind_perfil').AsInteger;

        //Permitir ao Administrador e Psicologia/Psicopedagogia
        //Diretor, DRH, Triagem e Educa��o F�sica visualizar todas as c�as/adolescentes
        if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or
           (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 9) or (vvIND_PERFIL = 10) then
        begin
          vvCOD_UNIDADE := 0;
          vvNOM_UNIDADE := 'FUNDHAS';
          //frmPrincipal.StatusBar1.Panels[1].Text := 'FUNDHAS [';
          frmPrincipal.StatusBar1.Panels[3].Text := 'FUNDHAS [';
        end
        else
        begin
          try
            with dmDeca.cdsSel_Unidade do
            begin
              Close;
              Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Usuario.FieldByName ('cod_unidade').Value;
              Params.ParamByName ('@pe_nom_unidade').Value := Null;
              Open;

              vvCOD_UNIDADE := FieldByName('cod_unidade').AsInteger;
              vvNOM_UNIDADE := FieldByName('nom_unidade').AsString;
              vvNUM_CCUSTO  := FieldByName('num_ccusto').AsString;
              vvNOM_GESTOR  := FieldByName('nom_gestor').AsString;
              vvNOM_ASOCIAL := FieldByName('nom_asocial').AsString;
              vvDSC_EMAIL   := FieldByName('dsc_email').AsString;
              vvNUM_TELEFONE:= FieldByName('num_telefone').AsString;
            end;
          except end;

          //frmPrincipal.StatusBar1.Panels[1].Text := 'Unidade: ' + FieldByName('nom_unidade').AsString + ' [';
          frmPrincipal.StatusBar1.Panels[3].Text := 'Unidade: ' + FieldByName('nom_unidade').AsString + ' [';
        end;

        frmPrincipal.AtualizaStatusBarUnidade;

        // *****************************************************************************
        // faz a atualiza��o automatica do STATUS da crian�a qu est� suspensa,
        // com base na data de retorno da suspensao e data do dia
        // gravado no hist�rico
        try
          with dmDeca.cdsAlt_Cadastro_Tira_Suspensao do
          begin
            Close;
            Execute;
          end;
        except
          ShowMessage('Sr. Usu�rio'+#13+#10+#13+#10+
                      'Ocorreu um problema no sistema, favor informar o Administrador'+#13+#10+
                      'do Sistema informando esta mensagem: ERRO COM RETORNO DE SUSPENS�ES');
        end;


              Close;
      end
    end
  except end;

  if vOk then
  begin
    // preparar o menu conforme o perfil
    with frmPrincipal.mmPrincipal do
    begin
      case vvIND_PERFIL of

        1: // Administrador
        begin

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := True; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := True; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair


          //Menu Movimenta��es
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Administrador do Sistema';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Administrador do Sistema';

          frmPrincipal.mmPrincipal.Items[1].Visible := True;

          for i := 0 to 25 do
            Items[1].Items[i].Visible := True;

          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica

          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False;  //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas


          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := True;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := True;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := True;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := True;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := True;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := True;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := True;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := True;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := True;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := True; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := True; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := True; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := True; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := True; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := True; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := True; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := True; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := True; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := True; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := True; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := True; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := True; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := True; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := True; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := True; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := True; //Emite Acomp. Escolar

        end;

        2: // Gestor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Gestor';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Gestor';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := True; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := True; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := True; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := True; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := True; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := True; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar


          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;

          if vvCOD_UNIDADE = 44 then  //Somente o Gestor do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          //frmPrincipal.ToolBar1.Buttons[5].Visible := True;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := True;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := True;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := True;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := True;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := True; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := True; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := True; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := True; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := True; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := True; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := True; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := True; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := True; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;

        3: // Assistente Social
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Assistente Social';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assistente Social';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := True; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := True; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := True; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := True; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := True; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := True; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;

          if vvCOD_UNIDADE = 44 then  //Somente a Assistente Social do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          //frmPrincipal.ToolBar1.Buttons[5].Visible := True;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := True;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := True;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := True;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := True;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := True; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := True; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := True; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := True; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := True; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := True; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := True; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := True; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := True; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := True; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;

        4: // Outros - Operacional
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Consulta';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Consulta';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := False; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar


          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := True; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;

        5: //Psicologia-Psicopedagogia
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Psicologia/Psicopedagogia';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Psicologia/Psicopedagogia';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := True; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenadas por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenadas por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := True;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := False; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := True; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := True; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := True; //Emite Acomp. Escolar

        end;

        6: //Professor-Instrutor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Professor/Instrutor';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Professor/Instrutor';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := True; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := False; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;

        7:  //Diretor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Diretor(a)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Diretor(a)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu Movimenta��es

          frmPrincipal.mmPrincipal.Items[1].Visible := False;

          for i := 0 to 25 do
            Items[1].Items[i].Visible := False;

          //frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True;

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := False; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := False; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := False; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;


        8:  //DRH
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Divis�o de Recursos Humanos';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Divis�o de Recursos Humanos';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresa
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair


          //Menu Movimenta��es

          frmPrincipal.mmPrincipal.Items[1].Visible := False;
          for i := 0 to 25 do
            Items[1].Items[i].Visible := False;

          //frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True;

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := False; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := False; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := False; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;

        9:  //Triagem
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Triagem';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Triagem';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair


          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := True; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := False; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := False; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := False; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := True; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := True; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := False; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;


        10: //Educa��o F�sica
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Educa��o F�sica';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Educa��o F�sica';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Empresas
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Sair

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := True; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := True; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;

          //Monta a Barra de Ferramentas
          frmPrincipal.ToolBar1.Visible := True;
          frmPrincipal.ToolBar1.Buttons[0].Visible := True;  //Ficha de Cadastro
          frmPrincipal.ToolBar1.Buttons[1].Visible := False;  //Escolas
          frmPrincipal.ToolBar1.Buttons[2].Visible := False;  //Turmas
          frmPrincipal.ToolBar1.Buttons[3].Visible := False;  //Usu�rios
          frmPrincipal.ToolBar1.Buttons[4].Visible := False;  //Unidades
          frmPrincipal.ToolBar1.Buttons[5].Visible := False;  //Empresas
          frmPrincipal.ToolBar1.Buttons[6].Visible := False;  //1� Separador
          frmPrincipal.ToolBar1.Buttons[7].Visible := False;  //Advert�ncias
          frmPrincipal.ToolBar1.Buttons[8].Visible := False;  //Encaminhamento p/ Afastamento
          frmPrincipal.ToolBar1.Buttons[9].Visible := False;  //Retorno de Afastamento
          frmPrincipal.ToolBar1.Buttons[10].Visible := False; //Suspens�o
          frmPrincipal.ToolBar1.Buttons[11].Visible := False; //Desligamento
          frmPrincipal.ToolBar1.Buttons[12].Visible := False; //Transfer�ncia p/ Unidade
          frmPrincipal.ToolBar1.Buttons[13].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.ToolBar1.Buttons[14].Visible := True; //Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[15].Visible := True; //Altera��o de Registro de Atendimento
          frmPrincipal.ToolBar1.Buttons[16].Visible := False; //Evolu��o do Servi�o Social
          frmPrincipal.ToolBar1.Buttons[17].Visible := False; //Altera dados Evolu��o
          frmPrincipal.ToolBar1.Buttons[18].Visible := False; //2� Separador
          frmPrincipal.ToolBar1.Buttons[19].Visible := False; //Hist�rico
          frmPrincipal.ToolBar1.Buttons[20].Visible := False; //Confirma��o de Recebimento de Transfer�ncia
          frmPrincipal.ToolBar1.Buttons[21].Visible := False; //Controle de Frequencia por Turma
          frmPrincipal.ToolBar1.Buttons[22].Visible := False; //Controle de Frequencia por Matricula
          frmPrincipal.ToolBar1.Buttons[23].Visible := False; //Inicializar Presen�a
          frmPrincipal.ToolBar1.Buttons[24].Visible := False; //Lan�amento de Visita T�cnica
          frmPrincipal.ToolBar1.Buttons[25].Visible := False; //Consulta dados de Vis. Tecnica
          frmPrincipal.ToolBar1.Buttons[26].Visible := True; //Lan�amento de Av. Biom�trica
          frmPrincipal.ToolBar1.Buttons[27].Visible := False; //Inicializar Bimestre
          frmPrincipal.ToolBar1.Buttons[28].Visible := False; //Consulta Dados de Acomp. Escolar
          frmPrincipal.ToolBar1.Buttons[29].Visible := False; //Emite Acomp. Escolar

        end;

      end;
    end;

    // esconder a tela de autentica��o
    frmTrocaUsuario.Hide;

    // abrir a tela principal do sistema
    frmPrincipal.ShowModal;

    // fechar a tela de autentica��o e o sistema
    frmTrocaUsuario.Close;

  end;

end;

procedure TfrmTrocaUsuario.FormActivate(Sender: TObject);
begin

  // formatar o padr�o de data
  ShortDateFormat := 'dd/mm/yyyy';

  // carregar lista de usu�rios
  try
    with dmDeca.cdsSel_Usuario do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_usuario').Value := Null;
      Params.ParamByName('@pe_flg_situacao').Value := 0; //Carregar a lista somente com os usu�rios ATIVOS
      //Params.ParamByName('@pe_flg_status').Value := 1; //Todos os que estiverem Off-Line
      Open;
      cbListaUsuario.Items.Clear;
      while not Eof do
      begin
        cbListaUsuario.Items.Add(FieldByName('nom_usuario').AsString);
        Next;
      end;
      Close;
    end;
  except end;
end;

end.
