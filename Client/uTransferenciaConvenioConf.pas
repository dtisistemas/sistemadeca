unit uTransferenciaConvenioConf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmTransferenciaConvenioConf = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    txtData: TMaskEdit;
    txtOrigem: TEdit;
    txtDestino: TEdit;
    Panel1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciaConvenioConf: TfrmTransferenciaConvenioConf;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil, uTransferenciaConvenio;

{$R *.DFM}

{ TfrmTransferenciaConvenioConf }

procedure TfrmTransferenciaConvenioConf.AtualizaCamposBotoes(Modo: String);
begin
if Modo = 'Padrao' then
  begin
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := False;
    txtOrigem.Enabled := False;
    txtDestino.Enabled := False;
    txtMatricula.Setfocus;

    btnConfirmar.Enabled := False;
    btnCancelar.Enabled := True;

  end
else if Modo = 'Confirmar' then
  begin
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtOrigem.Enabled := True;
    txtDestino.Enabled := True;
    txtMatricula.Setfocus;

    btnConfirmar.Enabled := True;
    btnCancelar.Enabled := False;
  end;
end;

procedure TfrmTransferenciaConvenioConf.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        with dmDeca.cdsSel_Ultima_Transferencia_Convenio do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Open;

          if dmDeca.cdsSel_Ultima_Transferencia_Unidade.Eof then
          begin
            ShowMessage('Aten��o' + #13+#10+#13+#10 +
                        'N�o existe Transfer�ncia de Unidade pendente para a Crian�a/Adolescente' + #13+#10 +
                        'Favor verificar... ');

            AtualizaCamposBotoes('Padr�o');
          end
          else
          begin
            txtDestino.Text := FieldByName('PARA').Value;
            txtOrigem.Text := FieldByName('DE').Value;
            txtData.Text := FieldByName('dat_historico').Value;
            AtualizaCamposBotoes('Confirmar');
          end;
        end;

      end;
    except end;
  end;
end;

procedure TfrmTransferenciaConvenioConf.txtMatriculaExit(Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;
            with dmDeca.cdsSel_Ultima_Transferencia_Unidade do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Open;

              if dmDeca.cdsSel_Ultima_Transferencia_Convenio.Eof then
              begin
                ShowMessage('Aten��o' + #13+#10+#13+#10 +
                            'N�o existe Transfer�ncia de Unidade pendente para a Crian�a/Adolescente' + #13+#10 +
                            'Favor verificar... ');

                AtualizaCamposBotoes('Padr�o');
              end
            
            else
            begin
              txtDestino.Text := FieldByName('PARA').Value;
              txtOrigem.Text := FieldByName('DE').Value;
              txtData.Text := FieldByName('dat_historico').Value;
              AtualizaCamposBotoes('Salvar');
            end
          end
        end
      end
    except end;
  end
end;

procedure TfrmTransferenciaConvenioConf.btnConfirmarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      if StatusCadastro(txtMatricula.Text) = 'Em Conv�nio' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 9;  //C�digo para Confirma��o de Transf. Conv�nio
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Transfer�ncia de Conv�nio - Confirma��o';
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "1" Ativo pois sai do status de afastado
          with dmDeca.cdsAlt_Cadastro_Status do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_ind_status').Value := 1;
              Execute;
          end;
        end;
        AtualizaCamposBotoes('Padrao');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                 'Esta(e) crian�a/adolescente n�o encontra-se em'+#13+#10+#13+#10+
                 'Conv�nio...');
        AtualizaCamposBotoes('Padrao');
      end;
    except
      ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10+#13+#10 +
               'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmTransferenciaConvenioConf.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmTransferenciaConvenioConf.btnSairClick(Sender: TObject);
begin
  frmTransferenciaConvenioConf.Close;
end;

procedure TfrmTransferenciaConvenioConf.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmTransferenciaConvenioConf.Close;
  end
end;

procedure TfrmTransferenciaConvenioConf.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action :=  caFree;
end;

procedure TfrmTransferenciaConvenioConf.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

end.
