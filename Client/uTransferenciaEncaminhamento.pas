unit uTransferenciaEncaminhamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmTransferenciaEncaminhamento = class(TForm)
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    txtDataTransferencia: TMaskEdit;
    rgOpcao: TRadioGroup;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    lbUnidade: TLabel;
    cbUnidades: TComboBox;
    txtCcustoOrigem: TMaskEdit;
    txtCcustoDestino: TMaskEdit;
    txtLocalOrigem: TEdit;
    txtBancoOrigem: TEdit;
    txtBancoDestino: TEdit;
    GroupBox6: TGroupBox;
    Label14: TLabel;
    txtFuncaoDe: TEdit;
    Label16: TLabel;
    txtFuncaoPara: TEdit;
    GroupBox7: TGroupBox;
    Label18: TLabel;
    txtHorario1: TMaskEdit;
    Label19: TLabel;
    txtHorario2: TMaskEdit;
    rgEntregaPasse: TRadioGroup;
    Label20: TLabel;
    GroupBox8: TGroupBox;
    Label15: TLabel;
    Label23: TLabel;
    txtInicioCurso: TMaskEdit;
    GroupBox9: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    txtInicioEstagio: TMaskEdit;
    Label26: TLabel;
    Label27: TLabel;
    txtCotas: TEdit;
    txtDuracaoCurso: TEdit;
    txtDuracaoEstagio: TEdit;
    Label13: TLabel;
    edSecaoOrigem: TEdit;
    edNumSecaoOrigem: TMaskEdit;
    edNumSecaoDestino: TMaskEdit;
    cbSecao: TComboBox;
    cbBancoOrigem: TComboBox;
    cbBancoDestino: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure cbUnidadesExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure cbUnidadesClick(Sender: TObject);
    procedure cbSecaoClick(Sender: TObject);
    procedure txtDataTransferenciaExit(Sender: TObject);
    procedure VerificaSituacaoBimestre(cdMatricula:String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciaEncaminhamento: TfrmTransferenciaEncaminhamento;
  vvPODE_TRANSFERIR : Boolean;

implementation

uses uDM, uPrincipal, uFichaPesquisa, uUtil, rTransferencia;

{$R *.DFM}

procedure TfrmTransferenciaEncaminhamento.FormShow(Sender: TObject);
begin
  cbUnidades.Enabled := True;
  cbUnidades.Clear;

  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName('@pe_cod_unidade').Value := Null;
    Params.ParamByName('@pe_nom_unidade').Value := Null;
    Open;
    while not eof do
    begin
      //if FieldByName('nom_unidade').AsString <> vvNOM_UNIDADE then
        cbUnidades.Items.Add(FieldByName('nom_unidade').AsString);
      Next;
    end;
  AtualizaCamposBotoes('Padrao');  
  end;
end;

procedure TfrmTransferenciaEncaminhamento.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmTransferenciaEncaminhamento.Close;
  end;
end;

procedure TfrmTransferenciaEncaminhamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmTransferenciaEncaminhamento.AtualizaCamposBotoes(
  Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    lbNome.Caption := '';
    txtDataTransferencia.Enabled := False;
    rgOpcao.ItemIndex := -1;
    rgOpcao.Enabled := False;

    txtCcustoOrigem.Clear;
    txtCcustoOrigem.Enabled := False;
    edNumSecaoOrigem.Clear;
    edNumSecaoOrigem.Enabled := False;
    txtLocalOrigem.Clear;
    txtLocalOrigem.Enabled := False;
    edSecaoOrigem.Clear;
    edSecaoOrigem.Enabled := False;
    //txtBancoOrigem.Enabled := False;
    cbBancoOrigem.ItemIndex := -1;
    cbBancoOrigem.Enabled := False;

    lbUnidade.Caption := '';
    cbUnidades.ItemIndex := -1;
    cbUnidades.Enabled := False;

    txtCcustoDestino.Clear;
    txtCcustoDestino.Enabled := False;
    edNumSecaoDestino.Clear;
    edNumSecaoDestino.Enabled := False;
    cbSecao.ItemIndex := -1;
    cbSecao.Enabled := False;
    cbBancoDestino.ItemIndex := -1;
    cbBancoDestino.Enabled := False;

    txtFuncaoDe.Clear;
    txtFuncaoDe.Enabled := False;
    txtFuncaoPara.Clear;
    txtFuncaoPara.Enabled := False;
    txtHorario1.Clear;
    txtHorario1.Enabled := False;
    txtHorario2.Clear;
    txtHorario2.Enabled := False;

    rgEntregaPasse.ItemIndex := -1;
    rgEntregaPasse.Enabled := False;

    txtCotas.Clear;
    txtCotas.Enabled := False;

    txtInicioCurso.Clear;
    txtInicioCurso.Enabled := False;
    txtDuracaoCurso.Clear;
    txtDuracaoCurso.Enabled := False;

    txtInicioEstagio.Clear;
    txtInicioEstagio.Enabled := False;
    txtDuracaoEstagio.Clear;
    txtDuracaoEstagio.Enabled := False;

    btnNovo.SetFocus;

  end
else if Modo = 'Novo' then
  begin
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

    txtMatricula.Enabled := True;
    txtMatricula.Clear;
    btnPesquisar.Enabled := True;
    lbNome.Caption := '';
    txtDataTransferencia.Enabled := True;
    txtDataTransferencia.Text := DateToStr(Date());
    rgOpcao.ItemIndex := -1;
    rgOpcao.Enabled := True;

    txtCcustoOrigem.Enabled := True;
    txtCcustoOrigem.Clear;
    txtLocalOrigem.Enabled := True;
    txtLocalOrigem.Clear;
    //txtBancoOrigem.Enabled := True;
    cbBancoOrigem.Enabled := True;
    //txtBancoOrigem.Clear;

    lbUnidade.Caption := vvNOM_UNIDADE;

    txtCcustoOrigem.Text := vvNUM_CCUSTO;
    txtLocalOrigem.Text := vvNOM_UNIDADE;

    //end;
    //except end;


    cbUnidades.ItemIndex := -1;
    cbUnidades.Enabled := True;

    txtCcustoDestino.Enabled := True;
    txtCcustoDestino.Clear;
    //txtLocalDestino.Enabled := True;
    //txtLocalDestino.Clear;
    //txtBancoDestino.Enabled := True;
    cbBancoDestino.Enabled := True;
    //txtBancoDestino.Clear;
    cbSecao.Enabled := True;

    txtFuncaoDe.Enabled := True;
    txtFuncaoDe.Clear;
    txtFuncaoPara.Enabled := True;
    txtFuncaoPara.Clear;
    txtHorario1.Enabled := True;
    txtHorario1.Clear;
    txtHorario2.Enabled := True;
    txtHorario2.Clear;

    rgEntregaPasse.ItemIndex := -1;
    rgEntregaPasse.Enabled := True;

    txtCotas.Enabled := True;
    txtCotas.Text := '00';

    txtInicioCurso.Enabled := True;
    txtInicioCurso.Clear;
    txtDuracaoCurso.Enabled := True;
    txtDuracaoCurso.Text := '00';

    txtInicioEstagio.Enabled := True;
    txtInicioEstagio.Clear;
    txtDuracaoEstagio.Enabled := True;
    txtDuracaoEstagio.Text := '00';

    txtMatricula.SetFocus;

  end
else if Modo = 'Salvar' then
  begin
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;
    btnSair.Enabled := True;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtDataTransferencia.Enabled := False;
    rgOpcao.Enabled := False;

    txtCcustoOrigem.Enabled := False;
    txtLocalOrigem.Enabled := False;
    //txtBancoOrigem.Enabled := False;
    cbBancoOrigem.Enabled := False;

    cbUnidades.Enabled := False;

    txtCcustoDestino.Enabled := False;
    //txtLocalDestino.Enabled := False;
    //txtBancoDestino.Enabled := False;
    cbBancoDestino.Enabled := False;

    txtFuncaoDe.Enabled := False;
    txtFuncaoPara.Enabled := False;
    txtHorario1.Enabled := False;
    txtHorario2.Enabled := False;

    rgEntregaPasse.Enabled := False;

    txtCotas.Enabled := False;

    txtInicioCurso.Enabled := False;
    txtDuracaoCurso.Enabled := False;

    txtInicioEstagio.Enabled := False;
    txtDuracaoEstagio.Enabled := False;

  end;
end;

procedure TfrmTransferenciaEncaminhamento.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmTransferenciaEncaminhamento.btnPesquisarClick(
  Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  //VerificaSituacaoBimestre(dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value);

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').AsInteger:= vvCOD_UNIDADE;
        Open;

        VerificaSituacaoBimestre(dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value);

        if (StatusCadastro(txtMatricula.Text) = 'Ativo') and (vvPODE_TRANSFERIR = True) then
        begin
          lbNome.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_nome').AsString;
          txtDataTransferencia.SetFocus;
          txtCcustoOrigem.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_ccusto').AsString;

          //Carrega os dados da Se��o
          with dmDeca.cdsSel_Secao do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_secao').Value := Null;
            Params.ParamByName ('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').AsInteger;
            Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;

            if (Length(edNumSecaoOrigem.Text)<1) or (edNumSecaoOrigem.Text = '') then
              Params.ParamByName ('@pe_num_secao').Value := Null
            else
              Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;

            Params.ParamByName ('@pe_flg_status').Value := Null;
            Open;

            if (RecordCount > 0) then
            begin
              edNumSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value;
              edSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
            end
            else
            begin
              edNumSecaoOrigem.Text := '';
              edSecaoOrigem.Text := '';
            end;
          end;

        end
        else
        begin
          ShowMessage('Aten��o!!! Essa crian�a/adolescente encontra-se' +#13+#10+#13+#10 +
               'em situa��o ' + StatusCadastro(txtMatricula.Text)+ '. Favor regularizar...');
          AtualizaCamposBotoes ('Padrao');
        end;
      end;
    except end;
  end;
end;

procedure TfrmTransferenciaEncaminhamento.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmTransferenciaEncaminhamento.btnSalvarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try
      // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
      if StatusCadastro(txtMatricula.Text) = 'Ativo' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtDataTransferencia, vtDate); //StrToDate(txtDataTransferencia.Text);
          Params.ParamByName('@pe_ind_tipo_historico').Value := 6;
          Params.ParamByName('@pe_dsc_tipo_historico').Value := 'Transfer�ncia p/ Unidade - Encaminhamento';
          Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Crian�a/Adolescente encaminhado da unidade/bloco/projeto ' +
                             txtCcustoOrigem.Text + '.' + ednumSecaoOrigem.Text + '-' + txtLocalOrigem.Text + '/' + edSecaoOrigem.Text + ' para a unidade/bloco/projeto ' +
                             txtCcustoDestino.Text + '.' + edNumSecaoDestino.Text +  '-' + cbUnidades.Text + '/'+ cbSecao.Text;
          Params.ParamByName ('@pe_orig_unidade').Value := lbUnidade.Caption;
          Params.ParamByName ('@pe_orig_ccusto').Value := GetValue(txtCcustoOrigem.Text);
          Params.ParamByName ('@pe_orig_secao').Value := GetValue(edSecaoOrigem.Text);
          Params.ParamByName ('@pe_orig_num_secao').Value := GetValue(edNumSecaoOrigem.Text);
          Params.ParamByName ('@pe_orig_banco').Value := GetValue(cbBancoOrigem.Text);
          Params.ParamByName ('@pe_dest_unidade').Value := GetValue(cbUnidades.Text);
          Params.ParamByName ('@pe_dest_ccusto').Value := GetValue(txtCcustoDestino.Text);
          Params.ParamByName ('@pe_dest_secao').Value := GetValue(cbSecao.Text);
          Params.ParamByName ('@pe_dest_num_secao').Value := GetValue(edNumSecaoDestino.Text);
          Params.ParamByName ('@pe_dest_banco').Value := GetValue(cbBancoDestino.Text);

          Params.ParamByName('@pe_orig_funcao').Value := GetValue(txtFuncaoDe.Text);
          Params.ParamByName('@pe_dest_funcao').Value := GetValue(txtFuncaoPara.Text);
          Params.ParamByName('@pe_hora_inicio').Value := GetValue(txtHorario1.Text);
          Params.ParamByName('@pe_hora_final').Value := GetValue(txtHorario2.Text);;
          Params.ParamByName('@pe_flg_passe').Value :=  rgEntregaPasse.ItemIndex;
          Params.ParamByName('@pe_num_cotas_passe').Value := StrToFloat(Trim(txtCotas.Text));

          if (txtInicioCurso.Text = '  /  /    ')  or (txtInicioCurso.Text = '//') then
            Params.ParamByName('@pe_dat_curso_inicio').Value := Null
          else
            Params.ParamByName('@pe_dat_curso_inicio').AsDateTime := StrToDate(txtInicioCurso.TExt);

          if (txtInicioEstagio.Text = '  /  /    ')  or (txtInicioEstagio.Text = '//') then
            Params.ParamByName('@pe_dat_estagio_inicio').Value := Null
          else
            Params.ParamByName('@pe_dat_estagio_inicio').AsDateTime := StrToDate(txtInicioEstagio.Text);

          Params.ParamByName('@pe_num_curso_duracao').Value := StrToInt(txtDuracaoCurso.Text);
          Params.ParamByName('@pe_num_estagio_duracao').Value := StrToInt(txtDuracaoEstagio.Text);
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "5" - Transferencia
          with dmDeca.cdsAlt_Cadastro_Status_Transf do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_cod_unidade').Value := vvINDEX_UNIDADE;
              Params.ParamByName('@pe_num_secao').Value := edNumSecaoDestino.Text;

              if (TRIM(txtLocalOrigem.Text)=cbUnidades.Text) then
                Params.ParamByName('@pe_ind_status').Value := 1
              else
                Params.ParamByName('@pe_ind_status').Value := 5;
              Execute;
          end;

          //"Zera" a turma na transfer�ncia
          with dmDeca.cdsAlt_Turma_PosTransferencia do
          begin
            Close;
            if (Trim(txtLocalOrigem.Text)=cbUnidades.Text) then
              Params.ParamByName ('@pe_cod_turma').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_turma').Value
            else
              Params.ParamByName ('@pe_cod_turma').Value := Null;
            Params.ParamByName ('@pr_cod_matricula').Value := GetValue(txtMatricula.Text);
            Execute;
          end;
        end;

        ShowMessage ('Transfer�ncia executada com sucesso... ! ! !');
        //AtualizaCamposBotoes('Padrao');

        btnNovo.Enabled := True;
        btnSalvar.Enabled := False;
        btnCancelar.Enabled := False;
        btnImprimir.Enabled := True;
        btnSair.Enabled := True;
        //AtualizaCamposBotoes('Padrao');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Novo');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'TRFe_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                          Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtDataTransferencia.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Transfer�ncia de Unidade - Encaminhamento');//Descricao do Tipo de Historico
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);



      //ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10+#13+#10 +
      //            'Favor entrar em contato com o Administrador do Sistema');
      AtualizaCamposBotoes('Padrao');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmTransferenciaEncaminhamento.txtMatriculaExit(
  Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
        begin
          lbNome.Caption := 'Ficha n�o cadastrada';
          AtualizaCamposBotoes ('Novo');
        end
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

          //Carrega os dados da Se��o
          with dmDeca.cdsSel_Secao do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_secao').Value := Null;
            Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').AsInteger;
            if (Length(edNumSecaoOrigem.Text)<1) or (edNumSecaoOrigem.Text <> '') then
              Params.ParamByName ('@pe_num_secao').Value := Null
            else
              Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;
            Params.ParamByName ('@pe_flg_status').Value := Null;
            Open;

            if (RecordCount > 0) then
            begin
              edNumSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value;
              edSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
            end
            else
            begin
              edNumSecaoOrigem.Text := '';
              edSecaoOrigem.Text := '';
            end;
          end;

      end
    except end;
  end;
end;

procedure TfrmTransferenciaEncaminhamento.cbUnidadesExit(Sender: TObject);
begin
with DmDeca.cdsSel_Unidade do
  begin
    vvINDEX_UNIDADE := 0;
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= cbUnidades.Items[cbUnidades.ItemIndex];
    Open;
    vvINDEX_UNIDADE := FieldByName('cod_unidade').Value;

    //Carrega a lista de Se��es da Unidade selecionada
    cbSecao.Items.Clear;
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := 0;
      Open;

      if (RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_Secao.Eof do
        begin
          cbSecao.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString);
          Next;
        end;
      end;
    end;
  end;
end;

procedure TfrmTransferenciaEncaminhamento.btnImprimirClick(
  Sender: TObject);
begin
  Application.CreateForm(TRelTransferencias,RelTransferencias);
  RelTransferencias.QRLabel123.Enabled := False; //Conv�nio
  RelTransferencias.QRLabel126.Enabled := True;  //Unidade
  RelTransferencias.lbNome1.caption := lbnome.caption;
  RelTransferencias.lbMatricula1.caption := txtMatricula.text;

  RelTransferencias.lbProjeto1Do.caption := vvNom_Unidade;
  RelTransferencias.lbCcusto1Do.caption := txtCCustoorigem.text+'.'+edNumSecaoOrigem.Text;
  RelTransferencias.lbLocal1Do.caption := edSecaoOrigem.Text; //txtLocalOrigem.text;
  RelTransferencias.lbBanco1Do.caption := cbBancoOrigem.Text;//txtBancoOrigem.text;

  RelTransferencias.lbProjeto1para.caption := cbUnidades.text;
  RelTransferencias.lbCCusto1Para.caption := txtCCustoDestino.text + '.'+edNumSecaoDestino.Text;
  RelTransferencias.lbLocal1Para.caption := cbSecao.Text;//txtLocalDestino.text;
  RelTransferencias.lbBanco1Para.caption := cbBancoDestino.Text;//txtBancoDestino.text;

  RelTransferencias.lbFuncao1De.caption:=txtFuncaoDe.text;
  RelTransferencias.lbFuncao1Para.caption:=txtFuncaoPara.text;

  RelTransferencias.lbHorario1.caption:=txtHorario1.text;
  RelTransferencias.lbHorario2.caption:=txtHorario2.text;
  RelTransferencias.lbCotas1.caption:=txtCotas.text;
  if rgEntregapasse.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape30.Brush.color:=clBlack;
       RelTransferencias.QrShape31.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape30.Brush.color:=clwhite;
       RelTransferencias.QrShape31.Brush.color:=clBlack;
      end;
  RelTransferencias.lbInicioCurso1.caption:=txtInicioCurso.text;
  RelTransferencias.lbDuracaoCurso1.caption:=txtDuracaoCurso.text;
  RelTransferencias.lbInicioEstagio1.caption:=txtInicioEstagio.text;
  RelTransferencias.lbDuracaoEstagio1.caption:=txtDuracaoEstagio.text;

  if rgopcao.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape109.Brush.color:=clBlack;
       RelTransferencias.QrShape110.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape109.Brush.color:=clwhite;
       RelTransferencias.QrShape110.Brush.color:=clBlack;
      end;

  RelTransferencias.lbDia1.Caption := Copy(txtDataTransferencia.Text,1,2);
  RelTransferencias.lbMes1.Caption := cMeses[StrToInt(Copy(txtDataTransferencia.Text,4,2))];
  RelTransferencias.lbAno1.Caption := Copy(txtDataTransferencia.Text,7,4);

{2� Via}
  RelTransferencias.QRLabel124.Enabled := False; //Conv�nio
  RelTransferencias.QRLabel122.Enabled := True;  //Unidade
  RelTransferencias.lbNome2.caption:= lbnome.caption;
  RelTransferencias.lbMatricula2.caption := txtMatricula.text;

  RelTransferencias.lbProjeto2Do.caption := vvNom_Unidade;
  RelTransferencias.lbCcusto2Do.caption := txtCCustoorigem.text+'.'+edNumSecaoOrigem.Text;
  RelTransferencias.lbLocal2Do.caption := edSecaoOrigem.Text; //txtLocalOrigem.text;
  RelTransferencias.lbBanco2Do.caption := cbBancoOrigem.Text; //txtBancoOrigem.text;

  RelTransferencias.lbProjeto2para.caption := cbUnidades.text;
  RelTransferencias.lbCCusto2Para.caption := txtCCustoDestino.text + '.'+edNumSecaoDestino.Text;
  RelTransferencias.lbLocal2Para.caption := cbSecao.Text; //txtLocalDestino.text;
  RelTransferencias.lbBanco2Para.caption := cbBancoDestino.Text; //txtBancoDestino.text;

  RelTransferencias.lbFuncao2De.caption:=txtFuncaoDe.text;
  RelTransferencias.lbFuncao2Para.caption:=txtFuncaoPara.text;

  RelTransferencias.lbHorario3.caption:=txtHorario1.text;
  RelTransferencias.lbHorario4.caption:=txtHorario2.text;
  RelTransferencias.lbCotas2.caption:=txtCotas.text;
  if rgEntregapasse.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape111.Brush.color:=clBlack;
       RelTransferencias.QrShape112.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape111.Brush.color:=clwhite;
       RelTransferencias.QrShape112.Brush.color:=clBlack;
      end;
  RelTransferencias.lbInicioCurso2.caption:=txtInicioCurso.text;
  RelTransferencias.lbDuracaoCurso2.caption:=txtDuracaoCurso.text;
  RelTransferencias.lbInicioEstagio2.caption:=txtInicioEstagio.text;
  RelTransferencias.lbDuracaoEstagio2.caption:=txtDuracaoEstagio.text;

  if rgopcao.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape111.Brush.color:=clBlack;
       RelTransferencias.QrShape112.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape111.Brush.color:=clwhite;
       RelTransferencias.QrShape112.Brush.color:=clBlack;
      end;

  RelTransferencias.lbDia2.Caption := Copy(txtDataTransferencia.Text,1,2);
  RelTransferencias.lbMes2.Caption := cMeses[StrToInt(Copy(txtDataTransferencia.Text,4,2))];
  RelTransferencias.lbAno2.Caption := Copy(txtDataTransferencia.Text,7,4);

  RelTransferencias.Preview;
  RelTransferencias.Free;
  AtualizaCamposBotoes('Padrao');

end;

procedure TfrmTransferenciaEncaminhamento.btnSairClick(Sender: TObject);
begin
  frmTransferenciaEncaminhamento.Close;
end;

procedure TfrmTransferenciaEncaminhamento.cbUnidadesClick(Sender: TObject);
begin
  txtCcustoDestino.Clear;
  edNumSecaoDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').AsString := cbUnidades.Items[cbUnidades.ItemIndex];
      Open;

      txtCcustoDestino.Text := dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString;
      //edSecaoDestino.Text := FieldByName('nom_unidade').AsString;

    end;
  except end;
end;

procedure TfrmTransferenciaEncaminhamento.cbSecaoClick(Sender: TObject);
begin
try
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_secao').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := vvINDEX_UNIDADE;
    Params.ParamByName ('@pe_num_secao').Value := Null;
    Params.ParamByName ('@pe_flg_status').Value := 0;
    Open;

    if (RecordCount > 0) then
      edNumSecaoDestino.Text := Copy(cbSecao.Text,1,4 )  //dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value
    else
      edNumSecaoDestino.Text := '';

  end;
except end;
end;

procedure TfrmTransferenciaEncaminhamento.txtDataTransferenciaExit(
  Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(txtDataTransferencia.Text);

    if StrToDate(txtDataTransferencia.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtDataTransferencia.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtDataTransferencia.SetFocus;
    end;
  end;
end;

procedure TfrmTransferenciaEncaminhamento.VerificaSituacaoBimestre(
  cdMatricula: String);
begin

  vvPODE_TRANSFERIR := False;

  try
    //Verifica se h� algum bimestre com situa��o "8"
    with dmDeca.cds_Sel_Verifica_AguardandoInformacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value  := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_matricula').Value;
      Params.ParamByName ('@pe_num_ano_letivo').Value := Copy(DateToStr(Date()),7,4);
      Params.ParamByName ('@pe_num_bimestre').Value   := Null;
      Params.ParamByName ('@pe_ind_situacao').Value   := 8; //Aguardando Informa��o...
      Open;

      if (dmDeca.cds_Sel_Verifica_AguardandoInformacoes.RecordCount > 0) then
      begin

        Application.MessageBox ('Aten��o !!!' +#13+#10 +
                                'Esta opera��o n�o ser� executada.' + #13+#10 +
                                'Consulte a Equipe do Acompanhamento Escolar para outras d�vidas.',
                                '[Sistema Deca] - Erro Transfer�ncia...',
                                MB_OK + MB_ICONWARNING);
        AtualizaCamposBotoes('Padrao');
        vvPODE_TRANSFERIR := False;
      end
      else if (dmDeca.cds_Sel_Verifica_AguardandoInformacoes.RecordCount < 1) then
      begin
        vvPODE_TRANSFERIR := True;
      end;
    end;

  except end;

end;

end.
