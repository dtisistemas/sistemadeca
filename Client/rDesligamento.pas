unit rDesligamento;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelDesligamento = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    PageFooterBand1: TQRBand;
    QRShape28: TQRShape;
    DetailBand1: TQRBand;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape11: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    lblMaioridade1: TQRLabel;
    lblInaptidao1: TQRLabel;
    lblAbandono1: TQRLabel;
    lblAPedido1: TQRLabel;
    lblMaioridade2: TQRLabel;
    lblInaptidao2: TQRLabel;
    lblAbandono2: TQRLabel;
    lblNome1: TQRLabel;
    lblNome2: TQRLabel;
    lblProjeto1: TQRLabel;
    lblLocal1: TQRLabel;
    lblProntuario1: TQRLabel;
    lblProjeto2: TQRLabel;
    lblLocal2: TQRLabel;
    QRShape55: TQRShape;
    QRLabel63: TQRLabel;
    lbMatricula1: TQRLabel;
    QRLabel64: TQRLabel;
    lbMatricula2: TQRLabel;
    QRLabel20: TQRLabel;
    lbMotivoDesligamento1: TQRRichText;
    QRLabel3: TQRLabel;
    lblProntuario2: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape17: TQRShape;
    QRLabel21: TQRLabel;
    QRShape2: TQRShape;
    QRLabel2: TQRLabel;
    QRShape4: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRLabel22: TQRLabel;
    lbMotivoDesligamento2: TQRRichText;
    QRShape29: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    lbJustaCausa1: TQRLabel;
    lblAPedido2: TQRLabel;
    lbJustaCausa2: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    lbMorte2: TQRLabel;
    QRLabel26: TQRLabel;
    lbMorte1: TQRLabel;
    QRShape8: TQRShape;
    QRLabel30: TQRLabel;
    QRShape9: TQRShape;
    QRLabel31: TQRLabel;
    QRShape10: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape16: TQRShape;
    QRLabel34: TQRLabel;
    QRShape23: TQRShape;
    lbDataDesligamento1: TQRLabel;
    lbDataDesligamento2: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRImage3: TQRImage;
  private

  public

  end;

var
  relDesligamento: TrelDesligamento;

implementation

uses uDM, uDesligamento, uUtil;

{$R *.DFM}

end.
