unit uConsultaDadosAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, QRExport, ComObj, DBCtrls, Db;

type
  TfrmConsultaDadosAcompEscolar = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    edAno: TEdit;
    Panel2: TPanel;
    btnConsultarDadosAcompEscolar: TSpeedButton;
    btnSair: TSpeedButton;
    ProgressBar1: TProgressBar;
    btnExportar: TSpeedButton;
    SaveDialog1: TSaveDialog;
    chkOcultarIntervencoes: TCheckBox;
    procedure btnConsultarDadosAcompEscolarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaDadosAcompEscolar: TfrmConsultaDadosAcompEscolar;
  vListaUnidade1, vListaUnidade2 : TStringList;

  mmTURMA1B, mmTURMA2B, mmTURMA3B, mmTURMA_ATUAL : String[5]; 

implementation

uses uDM, uPrincipal, uUtil, rControleAcompanhamentoEscolar,
  rControleAcompanhamentoEscolar_Teste, rObsSatisfConv,
  uExportaDadosAcompEscolarParaEscolas;

{$R *.DFM}

procedure TfrmConsultaDadosAcompEscolar.btnConsultarDadosAcompEscolarClick(
  Sender: TObject);
begin

  ProgressBar1.Position := 0;

  //Excluir os dados referentes ao boletim, inseridos pelo usu�rio atual
  with dmDeca.cdsExc_Boletim do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Execute;
  end;

  try
    //Seleciona os dados do cadastro para a unidade selecionada...
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;

      //Se n�o houver alunos cadastrados
      if dmDeca.cdsSel_Cadastro.RecordCount < 1 then
      begin
        //Mensagem caso n�o encontre nenhum aluno na unidade selecionada...

      end
      else if dmDeca.cdsSel_Cadastro.RecordCount > 0 then
      begin
        //Encontrou...
        //Enquanto n�o fim-do-cadastro

        ProgressBar1.Min := 0;
        ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

        dmDeca.cdsSel_Cadastro.First;
        while not (dmDeca.cdsSel_Cadastro.eof) do
        begin
            //Incluir dados cadastrais na tabela BOLETIM
            with dmDeca.cdsInc_Boletim do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
              Params.ParamByName ('@pe_nom_nome').Value := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
              Params.ParamByName ('@pe_num_rg_escolar').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_escolar').Value;
              Params.ParamByName ('@pe_dat_nascimento').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value;
              Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);

              //Seleciona a escola e s�rie atuais
              with dmDeca.cdsSel_HistoricoEscola do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_cod_escola').Value := Null;
                Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAno.Text);
                Open;

                if dmDeca.cdsSel_HistoricoEscola.RecordCount > 0 then
                begin
                  while not (dmDeca.cdsSel_HistoricoEscola.eof) do
                  begin
                    //Verifica se � escola/s�rie atuais...
                    if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 0) then
                    begin
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value;
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := dmDeca.cdsSel_HistoricoEscola.FieldByName ('dsc_serie').Value;
                      dmDeca.cdsSel_HistoricoEscola.Last;
                    end
                    //Ou n�o � escola/s�rie atuais...
                    else if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 1) then
                    begin
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := '--';
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := '--';
                      dmDeca.cdsSel_HistoricoEscola.Last;
                    end;
                  end;
                end
                else
                begin
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := ' ';
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := ' ';
                end;
            end; //with dmDeca.cdsSel_HistoricoEscola do


            {*************** Consultar dados do Acompanhamento Escolar *******************************}
            //1.� Bimestre do ano informado na tela
            with dmDeca.cdsSel_AcompEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
              Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_bimestre').Value          := 1;
              Params.ParamByName ('@pe_cod_unidade').Value           := Null;
              Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
              Open;

              //Se encontrou registros do Acomp. Escolar referentes ao 1.� Bimestre
              if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
              begin
                //Avalia o ind_frequencia
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '+75%';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '-75%';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '--';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'FLX';
                  4 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'DE';
                end;

                //Avalia o ind_aproveitamento
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AB';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'DE';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AC';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := '--';
                end;
              end
              else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
              begin
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := ' ';
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := ' ';
              end;
            end; //with dmDeca.cdsSel_AcompEscolar do... 1.� bimestre

            //Recupera a Turma - 1.� Bimestre
            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
              mmTURMA1B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
            else
              //mmTURMA1B := '-';
              mmTURMA1B := '';


            //Grava a SITUA��O DO 1.� BIMESTRE
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_sit_1b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoBimestre').Value;

            //Grava o per�odo do 1.� Bimestre
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_per_1b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value;

            {*************************************************************************************************}

            //2.� Bimestre do ano informado na tela
            with dmDeca.cdsSel_AcompEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
              Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_bimestre').Value          := 2;
              Params.ParamByName ('@pe_cod_unidade').Value           := Null;
              Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
              Open;

              //Se encontrou registros do Acomp. Escolar referentes ao 2.� Bimestre
              if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
              begin
                //Avalia o ind_frequencia
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '+75%';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '-75%';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '--';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'FLX';
                  4 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'DE';

                end;

                //Avalia o ind_aproveitamento
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AB';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'DE';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AC';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := '--';
                end;
              end
              else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
              begin
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := ' ';
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := ' ';
              end;
            end; //with dmDeca.cdsSel_AcompEscolar do... 2.� bimestre

            //Recupera a Turma - 2.� Bimestre
            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
              mmTURMA2B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
            else
              //mmTURMA2B := '-';
              mmTURMA2B := '';

            //Grava a SITUA��O DO 2.� BIMESTRE
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_sit_2b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoBimestre').Value;

            //Grava o per�odo do 2.� Bimestre
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_per_2b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value;

            {*************************************************************************************************}

            //3.� Bimestre do ano informado na tela
            with dmDeca.cdsSel_AcompEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
              Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_bimestre').Value          := 3;
              Params.ParamByName ('@pe_cod_unidade').Value           := Null;
              Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
              Open;

              //Se encontrou registros do Acomp. Escolar referentes ao 3.� Bimestre
              if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
              begin
                //Avalia o ind_frequencia
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '+75%';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '-75%';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '--';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'FLX';
                  4 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'DE';
                end;

              //Avalia o ind_aproveitamento
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AB';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'DE';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AC';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := '--';
                end;
              end
              else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
              begin
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := ' ';
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := ' ';
              end;
            end; //with dmDeca.cdsSel_AcompEscolar do... 3.� bimestre

            //Recupera a Turma - 3.� Bimestre
            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
              mmTURMA3B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
            else
              //mmTURMA3B := '-';
              mmTURMA3B := '';

            //Grava a SITUA��O DO 3.� BIMESTRE
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_sit_3b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoBimestre').Value;

            //Grava o per�odo do 3.� Bimestre
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_per_3b').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value;

            {*************************************************************************************************}

            //Compara as turmas lan�adas nos bimestres
            if (mmTURMA1B = mmTURMA2B) then
              mmTURMA_ATUAL := mmTURMA2B
            else if (mmTURMA1B <> mmTURMA2B) and (mmTURMA2B <> '') then
              mmTURMA_ATUAL := mmTURMA2B
            else if (mmTURMA1B <> mmTURMA2B) and (mmTURMA2B = '') then
            //mmTURMA_ATUAL := mmTURMA1B;
              mmTURMA_ATUAL := '';



            if (mmTURMA2B = mmTURMA3B) then
              mmTURMA_ATUAL := mmTURMA3B
            else if (mmTURMA2B <> mmTURMA3B) and (mmTURMA3B <> '') then
              mmTURMA_ATUAL := mmTURMA3B
            else if (mmTURMA2B <> mmTURMA3B) and (mmTURMA3B = '') then
            //mmTURMA_ATUAL := mmTURMA2B;
              mmTURMA_ATUAL := '';

            //if (mmTURMA2B = mmTURMA3B) then //or (mmTURMA2B <> mmTURMA3B) then
            //  mmTURMA_ATUAL := mmTURMA3B
            //else mmTURMA_ATUAL := mmTURMA3B; //mmTURMA3B;

            if (Length(mmTURMA_ATUAL)=0) then mmTURMA_ATUAL := '-' else mmTURMA_ATUAL := mmTURMA_ATUAL;
            
            //Repassa a turma atual para o boletim...
            dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_turma').Value := mmTURMA_ATUAL;
            //dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_turma').Value := '-';



            dmDeca.cdsInc_Boletim.Execute;

          end; //Fim with dmDeca.cdsInc_Boletim do...

          dmDeca.cdsSel_Cadastro.Next;
          ProgressBar1.Position := ProgressBar1.Position + 1;
        end; //while not (dmDeca.cdsSel_Cadastro.eof) do
      end
      else
      begin
        //Inverso caso a unidade esteja na lista "negra"
      end;

    end;

    //Exibe a rela��o com os dados coletados...
    with dmDeca.cdsSel_Boletim do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Open;

      //Carrega o relat�rio baseado nos dados do Boletim, que acabamos de gerar
      Application.CreateForm (TrelControleAcompanhamentoEscolar_Teste, relControleAcompanhamentoEscolar_Teste);
      relControleAcompanhamentoEscolar_Teste.Prepare;
      relControleAcompanhamentoEscolar_Teste.QRLabel3.Caption := ' de  ' + IntToStr(relControleAcompanhamentoEscolar_Teste.QRPrinter.PageCount);
      relControleAcompanhamentoEscolar_Teste.QRLabel2.Caption := 'Unidade: ' + cbUnidade.Text + '  -  ANO : ' + edAno.Text;

      if chkOcultarIntervencoes.Checked then
      begin
        relControleAcompanhamentoEscolar_Teste.QRDBText7.Enabled  := False;
        relControleAcompanhamentoEscolar_Teste.QRDBText8.Enabled  := False;
        relControleAcompanhamentoEscolar_Teste.QRDBText9.Enabled  := False;
        relControleAcompanhamentoEscolar_Teste.QRDBText10.Enabled := False;
        relControleAcompanhamentoEscolar_Teste.QRDBText11.Enabled := False;
        relControleAcompanhamentoEscolar_Teste.QRDBText12.Enabled := False;
        relControleAcompanhamentoEscolar_Teste.QRLabel26.Caption  := ' >> [Rela��o para confer�ncia Escola/S�rie - os dados de interven��es est�o ocultos...]';

      end
      else
      if not chkOcultarIntervencoes.Checked then
      begin
        relControleAcompanhamentoEscolar_Teste.QRDBText7.Enabled  := True;
        relControleAcompanhamentoEscolar_Teste.QRDBText8.Enabled  := True;
        relControleAcompanhamentoEscolar_Teste.QRDBText9.Enabled  := True;
        relControleAcompanhamentoEscolar_Teste.QRDBText10.Enabled := True;
        relControleAcompanhamentoEscolar_Teste.QRDBText11.Enabled := True;
        relControleAcompanhamentoEscolar_Teste.QRDBText12.Enabled := True;
        relControleAcompanhamentoEscolar_Teste.QRLabel26.Caption  := ' ';
      end;

      with dmDeca.cdsTOTALIZA_APROVFREQ_ACOMPESCOLAR do
      begin
        Close;
        Params.ParamByName ('@pe_num_ano_letivo').Value := GetValue(edAno.Text);
        Params.ParamByName ('@pe_cod_unidade').Value    := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
        Open;
      end;

      relControleAcompanhamentoEscolar_Teste.Preview;
      relControleAcompanhamentoEscolar_Teste.Free;
    end;

  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu. Favor entrar em contato com o Administrador',
                            '[Sistema Deca] - Acomp. Escolar',
                            MB_OK + MB_ICONEXCLAMATION);
  end;
end;

procedure TfrmConsultaDadosAcompEscolar.FormShow(Sender: TObject);
begin

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  edAno.Text := Copy(DateToStr(Date()),7,4);

  //Carregar as unidades...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;

      if (vvIND_PERFIL in [1,5,7,16,17,18,19,20,23,24,25]) then
        Params.ParamByName ('@pe_cod_unidade').Value := Null
      else
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        cbUnidade.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
        vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end;

    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu e a consulta n�o foi realizada. ',
                            '[Sistema Deca] - Consulta Unidades...',
                            MB_OK + MB_ICONWARNING );

  end;

end;

procedure TfrmConsultaDadosAcompEscolar.btnExportarClick(Sender: TObject);
var
  arqCSV : TextFile;
  lin    : Integer;
begin

  //Carrega os dados para exporta��o...
  //Excluir os dados referentes ao boletim, inseridos pelo usu�rio atual
  with dmDeca.cdsExc_Boletim do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
    Execute;
  end;

  try
    //Seleciona os dados do cadastro para a unidade selecionada...
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      //Params.ParamByName ('@pe_cod_unidade').Value   := 0;//2; //Todas as Unidades, exceto as relacionadas abaixo
      Params.ParamByName ('@pe_cod_unidade').Value   := 27; //Bolsa - Teste
      Open;

      //Se n�o houver alunos cadastrados
      if (dmDeca.cdsSel_Cadastro.RecordCount < 1) then
      begin
        //Mensagem caso n�o encontre nenhum aluno na unidade selecionada...

      end
      else if (dmDeca.cdsSel_Cadastro.RecordCount > 0) then
      begin
        //Encontrou...
        //Enquanto n�o fim-do-cadastro

        ProgressBar1.Min := 0;
        ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

        while not (dmDeca.cdsSel_Cadastro.eof) do
        begin

          if ( ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 1 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 3 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 9 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 12 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 40 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 42 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 43 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 47 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 48 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 50 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 74 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 75 ) or
               ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 76 ) ) then               
          begin

            //Incluir dados cadastrais na tabela BOLETIM
            with dmDeca.cdsInc_Boletim do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
              Params.ParamByName ('@pe_nom_nome').Value := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
              Params.ParamByName ('@pe_num_rg_escolar').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_escolar').Value;
              Params.ParamByName ('@pe_dat_nascimento').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value;
              Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]); 

              //Seleciona a escola e s�rie atuais
              with dmDeca.cdsSel_HistoricoEscola do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_cod_escola').Value := Null;
                Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAno.Text);
                Open;

                if dmDeca.cdsSel_HistoricoEscola.RecordCount > 0 then
                begin
                  while not (dmDeca.cdsSel_HistoricoEscola.eof) do
                  begin
                    //Verifica se � escola/s�rie atuais...
                    if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 0) then
                    begin
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value;
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := dmDeca.cdsSel_HistoricoEscola.FieldByName ('dsc_serie').Value;
                      dmDeca.cdsSel_HistoricoEscola.Last;
                    end
                    //Ou n�o � escola/s�rie atuais...
                    else if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 1) then
                    begin
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := '--';
                      dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := '--';
                      dmDeca.cdsSel_HistoricoEscola.Last;
                    end;
                  end;
                end
                else
                begin
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := ' ';
                  dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := ' ';
                end;
            end; //with dmDeca.cdsSel_HistoricoEscola do


            {*************** Consultar dados do Acompanhamento Escolar *******************************}
            //1.� Bimestre do ano informado na tela
            with dmDeca.cdsSel_AcompEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
              Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_bimestre').Value          := 1;
              Params.ParamByName ('@pe_cod_unidade').Value           := Null;
              Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
              Open;

              //Se encontrou registros do Acomp. Escolar referentes ao 1.� Bimestre
              if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
              begin
                //Avalia o ind_frequencia
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '+75%';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '-75%';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '--';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'FLX';
                end;

                //Avalia o ind_aproveitamento
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AB';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'DE';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AC';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := '--';
                end;
              end
              else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
              begin
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := ' ';
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := ' ';
              end;
            end; //with dmDeca.cdsSel_AcompEscolar do... 1.� bimestre
            {*************************************************************************************************}

            //2.� Bimestre do ano informado na tela
            with dmDeca.cdsSel_AcompEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
              Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_bimestre').Value          := 2;
              Params.ParamByName ('@pe_cod_unidade').Value           := Null;
              Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
              Open;

              //Se encontrou registros do Acomp. Escolar referentes ao 2.� Bimestre
              if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
              begin
                //Avalia o ind_frequencia
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '+75%';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '-75%';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '--';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'FLX';
                end;

                //Avalia o ind_aproveitamento
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AB';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'DE';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AC';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := '--';
                end;
              end
              else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
              begin
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := ' ';
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := ' ';
              end;
            end; //with dmDeca.cdsSel_AcompEscolar do... 2.� bimestre
            {*************************************************************************************************}

            //3.� Bimestre do ano informado na tela
            with dmDeca.cdsSel_AcompEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
              Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_bimestre').Value          := 3;
              Params.ParamByName ('@pe_cod_unidade').Value           := Null;
              Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
              Open;

              //Se encontrou registros do Acomp. Escolar referentes ao 3.� Bimestre
              if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
              begin
                //Avalia o ind_frequencia
                case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '+75%';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '-75%';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '--';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'FLX';
                end;

              //Avalia o ind_aproveitamento
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                  0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AB';
                  1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'DE';
                  2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AC';
                  3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := '--';
                end;
              end
              else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
              begin
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := ' ';
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := ' ';
              end;
            end; //with dmDeca.cdsSel_AcompEscolar do... 3.� bimestre
            {*************************************************************************************************}

            dmDeca.cdsInc_Boletim.Execute;

          end; //Fim with dmDeca.cdsInc_Boletim do...

          dmDeca.cdsSel_Cadastro.Next;
          ProgressBar1.Position := ProgressBar1.Position + 1;

        end; //while not (dmDeca.cdsSel_Cadastro.eof) do
      end
      end
      else
      begin
        //Inverso caso a unidade esteja na lista "negra"
      end;

    end;

    //Exibe a rela��o com os dados coletados...
    with dmDeca.cdsSel_Boletim do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Open;

      //Procedimento para gerar um arquivo CSV, pedindo ao usu�rio o local a ser salvo
      SaveDialog1.Execute;

      AssignFile(arqCSV, SaveDialog1.FileName);
      Rewrite(arqCSV);

      with dmDeca.cdsSel_Boletim do
      begin

        Writeln (arqCSV, '[FUNDHAS] - Funda��o H�lio Augusto de Souza');
        Writeln (arqCSV, '[Sistema Deca] - Rela��o de Dados do Acompanhamento Escolar');
        Writeln (arqCSV, 'Ano Letivo: ' + edAno.Text);
        Writeln (arqCSV, 'Data/Hora Impress�o:' + DateToStr(Date()) + ' / ' + TimeToStr(Time));
        Writeln (arqCSV, 'Unidade: ');
        Writeln (arqCSV, '');
        Writeln (arqCSV, '');

        Writeln (arqCSV, 'Matricula;Nome;RGEscolar;Nascimento;Escola;Serie;Freq1b;Freq2b;Freq3b;Aprov1b;Aprov2b;Aprov3b');
        dmDeca.cdsSel_Boletim.First;

        for lin := 0 to RecordCount - 1 do
        begin

          Write  (arqCSV, FieldByName('cod_matricula').Value +';');
          Write  (arqCSV, FieldByName('nom_nome').Value +';');
          Write  (arqCSV, FieldByName('num_rg_escolar').Value +';');
          Write  (arqCSV, FieldByName('DatNascimento').Value +';');
          Write  (arqCSV, FieldByName('nom_escola').Value +';');
          Write  (arqCSV, FieldByName('dsc_serie').Value +';');
          Write  (arqCSV, FieldByName('dsc_freq_1b').Value +';');
          Write  (arqCSV, FieldByName('dsc_freq_2b').Value +';');
          Write  (arqCSV, FieldByName('dsc_freq_3b').Value +';');
          Write  (arqCSV, FieldByName('dsc_aprov_1b').Value +';');
          Write  (arqCSV, FieldByName('dsc_aprov_2b').Value +';');
          Writeln(arqCSV, FieldByName('dsc_aprov_3b').Value +';');
          dmDeca.cdsSel_Boletim.Next;

        end;
        Writeln (arqCSV, '');
        Writeln (arqCSV, '');
        Writeln (arqCSV, 'Total de Registros encontrados.........>>>' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount));

        CloseFile (arqCSV);
        Application.MessageBox ('Um arquivo com os dados selecionados foi gerado. ' + #13+#10 +
                                'Verifique o local que voc� pediu para salv�-lo.',
                                '[Sistema Deca] - Exporta��o de Dados',
                                MB_OK + MB_ICONINFORMATION);
      end;

      //Carrega o formul�rio para selecionar escola e enviar por e-mail para a mesma...
      //Application.CreateForm (TfrmExportaDadosAcompEscolarParaEscolas, frmExportaDadosAcompEscolarParaEscolas);

      {------- >>> Rotina para exportar para planilha do Excel <<< -------}
      //Carregar a lista de escolas a partir do boletim gerado (em tabela), onde haver� op��o de exporta��o...
      {
      with dmDeca.cdsSel_EscolasBoletim do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Open;

        frmExportaDadosAcompEscolarParaEscolas.clbEscolas.Clear;
        while not dmDeca.cdsSel_EscolasBoletim.eof do
        begin
          frmExportaDadosAcompEscolarParaEscolas.clbEscolas.Items.Add (dmDeca.cdsSel_EscolasBoletim.FieldByName ('nom_escola').Value);
          dmDeca.cdsSel_EscolasBoletim.Next;
        end
      end;


      if Application.MessageBox ('A gera��o dos dados referentes ao ano letivo selecionado foi finalizada!' + #13+#10 +
                                 'Deseja ir para a tela de exporta��o dos dados agora?',
                                 '[Sistema Deca] - Prepara��o e Exporta��o dos Dados Acomp. Escolar',
                                 MB_YESNO + MB_ICONQUESTION) = id_yes then
      begin
              frmExportaDadosAcompEscolarParaEscolas.ShowModal;
      end
      else
      begin
        Application.MessageBox ('Sr(a) Usu�rio(a)' + #13+#10+
                                'Voc� cancelou a opera��o. Para ir a tela de ' +#13+#10+
                                'exporta��o, voc� deve PRIMEIRAMENTE gerar os dados.' + #13+#10+
                                'Ou seja. Voc� dever� acessar esta tela novamente mais tarde ' + #13+#10+
                                'para que possa gerar e exportar os dados para planilhas de c�lculos.',
                                '[Sistema Deca] - Aviso ao Usu�rio',
                                MB_OK + MB_ICONINFORMATION)
      end;

      }

    end;

  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu. Favor entrar em contato com o Administrador',
                            '[Sistema Deca] - Acomp. Escolar',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

end;

procedure TfrmConsultaDadosAcompEscolar.btnSairClick(Sender: TObject);
begin
  frmConsultaDadosAcompEscolar.Close;
end;

end.
