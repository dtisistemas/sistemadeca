unit uEmpresas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, DBGrids, Buttons, Db, Mask;

type
  TfrmEmpresas = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    dgEmpresas: TDBGrid;
    dsEmpresas: TDataSource;
    btnNovo: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnSair: TSpeedButton;
    GroupBox1: TGroupBox;
    edAssSocial: TEdit;
    edEmpresa: TEdit;
    GroupBox2: TGroupBox;
    edContato: TEdit;
    GroupBox3: TGroupBox;
    edEndereco: TEdit;
    GroupBox4: TGroupBox;
    mskCep: TMaskEdit;
    rgTipo: TRadioGroup;
    rgCargaHoraria: TRadioGroup;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    mskFone1: TMaskEdit;
    Label2: TLabel;
    mskFone2: TMaskEdit;
    Label3: TLabel;
    mskFone3: TMaskEdit;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    edEmail: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtualizaCamposBotoes (Modo : String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edEmpresaKeyPress(Sender: TObject; var Key: Char);
    procedure btnSalvarClick(Sender: TObject);
    procedure dgEmpresasDblClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmpresas: TfrmEmpresas;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmEmpresas.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin

    edEmpresa.Clear;
    edEmpresa.Enabled := False;
    edContato.Clear;
    edContato.Enabled := False;
    edEndereco.Clear;
    edEndereco.Enabled := False;
    mskCep.Clear;
    mskCep.Enabled := False;
    rgTipo.ItemIndex := -1;
    rgTipo.Enabled := False;
    rgCargaHoraria.ItemIndex := -1;
    rgCargaHoraria.Enabled := False;
    mskFone1.Clear;
    mskFone1.Enabled := False;
    mskFone2.Clear;
    mskFone2.Enabled := False;
    mskFone3.Clear;
    mskFone3.Enabled := False;
    edAssSocial.Clear;
    edAssSocial.Enabled := False;
    edEmail.Clear;
    edEmail.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    edEmpresa.Enabled := True;
    edContato.Enabled := True;
    edEndereco.Enabled := True;
    mskCep.Enabled := True;
    rgTipo.Enabled := True;
    rgCargaHoraria.Enabled := True;
    mskFone1.Enabled := True;
    mskFone2.Enabled := True;
    mskFone3.Enabled := True;
    edAssSocial.Enabled := True;
    edEmail.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end
  else if Modo = 'Alterar' then
  begin

    edEmpresa.Enabled := True;
    edContato.Enabled := True;
    edEndereco.Enabled := True;
    mskCep.Enabled := True;
    rgTipo.Enabled := True;
    rgCargaHoraria.Enabled := True;
    mskFone1.Enabled := True;
    mskFone2.Enabled := True;
    mskFone3.Enabled := True;
    edAssSocial.Enabled := True;
    edEmail.Enabled := True;
    
    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmEmpresas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEmpresas.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Novo');
end;

procedure TfrmEmpresas.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
end;

procedure TfrmEmpresas.btnSairClick(Sender: TObject);
begin
  frmEmpresas.Close;
end;

procedure TfrmEmpresas.FormShow(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Empresa do
    begin
      Close;
      Params.ParamByName ('@pe_cod_empresa').Value := Null;
      Open;
      dgEmpresas.Refresh;
    end;
  except end;
  
  AtualizaCamposBotoes ('Padrao');

end;

procedure TfrmEmpresas.edEmpresaKeyPress(Sender: TObject; var Key: Char);
begin
  if key > #127 then key := #0; 
end;

procedure TfrmEmpresas.btnSalvarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_Empresa do
    begin
      Close;
      Params.ParamByName ('@pe_nom_empresa').Value := GetValue(edEmpresa.Text);
      Params.ParamByName ('@pe_nom_contato').Value := GetValue(edContato.Text);
      Params.ParamByName ('@pe_num_fone1').Value := GetValue(mskFone1.Text);
      Params.ParamByName ('@pe_num_fone2').Value := GetValue(mskFone2.Text);
      Params.ParamByName ('@pe_num_fone3').Value := GetValue(mskFone3.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_num_cep').Value := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_ind_tipo').Value := rgTipo.ItemIndex;
      Params.ParamByName ('@pe_ind_periodo_empresa').Value := rgCargaHoraria.ItemIndex;
      Params.ParamByName ('@pe_nom_as_social').Value := GetValue(edAssSocial.Text);
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Execute;

      with dmDeca.cdsSel_Empresa do
      begin
        Close;
        Params.ParamByName ('@pe_cod_empresa').Value := Null;
        Open;
        dgEmpresas.Refresh;
      end;

      AtualizaCamposBotoes ('Padrao');

    end;
  except
    Application.MessageBox('Aten��o!!! Ocorreu um ERRO ao tentar inserir'+#13+#10+#13+#10+
             'os dados de Empresa. Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Empresas',
             MB_OK + MB_ICONERROR);
    AtualizaCamposBotoes ('Padrao');
  end;
end;

procedure TfrmEmpresas.dgEmpresasDblClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Alterar');
  edEmpresa.Text := dmDeca.cdsSel_Empresa.FieldByName ('nom_empresa').AsString;
  edContato.Text := dmDeca.cdsSel_Empresa.FieldByName ('nom_contato').AsString;
  edEndereco.Text := dmDeca.cdsSel_Empresa.FieldByName ('dsc_endereco').AsString;
  mskCep.Text := dmDeca.cdsSel_Empresa.FieldByName ('num_cep').AsString;
  rgTipo.ItemIndex := dmDeca.cdsSel_Empresa.FieldByName ('ind_tipo').AsInteger;
  rgCargaHoraria.ItemIndex := dmDeca.cdsSel_Empresa.FieldByName ('ind_periodo_empresa').AsInteger;
  mskFone1.Text := dmDeca.cdsSel_Empresa.FieldByName ('num_fone1').AsString;
  mskFone2.Text := dmDeca.cdsSel_Empresa.FieldByName ('num_fone2').AsString;
  mskFone3.Text := dmDeca.cdsSel_Empresa.FieldByName ('num_fone3').AsString;
  edAssSocial.Text := dmDeca.cdsSel_Empresa.FieldByName ('nom_as_social').AsString;
  edEmail.Text := dmDeca.cdsSel_Empresa.FieldByName ('dsc_email').AsString;
end;

procedure TfrmEmpresas.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Empresa do
    begin
      Close;
      Params.ParamByName ('@pe_cod_empresa').AsInteger := dmDeca.cdsSel_Empresa.FieldByName ('cod_empresa').AsInteger;
      Params.ParamByName ('@pe_nom_empresa').Value := GetValue(edEmpresa.Text);
      Params.ParamByName ('@pe_nom_contato').Value := GetValue(edContato.Text);
      Params.ParamByName ('@pe_num_fone1').Value := GetValue(mskFone1.Text);
      Params.ParamByName ('@pe_num_fone2').Value := GetValue(mskFone2.Text);
      Params.ParamByName ('@pe_num_fone3').Value := GetValue(mskFone3.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_num_cep').Value := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_ind_tipo').Value := rgTipo.ItemIndex;
      Params.ParamByName ('@pe_ind_periodo_empresa').Value := rgCargaHoraria.ItemIndex;
      Params.ParamByName ('@pe_nom_as_social').Value := GetValue(edAssSocial.Text);
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Execute;

      with dmDeca.cdsSel_Empresa do
      begin
        Close;
        Params.ParamByName ('@pe_cod_empresa').Value := Null;
        Open;
        dgEmpresas.Refresh;
      end;

      AtualizaCamposBotoes ('Padrao');

    end;
  except
    Application.MessageBox('Aten��o!!! Ocorreu um ERRO ao tentar alterar'+#13+#10+#13+#10+
             'os dados de Empresa. Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Empresas',
             MB_OK + MB_ICONERROR);
    AtualizaCamposBotoes ('Padrao');
  end;
end;

end.
