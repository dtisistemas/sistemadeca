object frmCadastroInscricoes: TfrmCadastroInscricoes
  Left = 333
  Top = 199
  Width = 955
  Height = 676
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Anchors = [akLeft, akTop, akRight, akBottom]
  BorderStyle = bsSizeToolWin
  Caption = '[FUNDHAS] - Cadastro de Inscri��es >> Setor Triagem'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 918
    Top = 636
    Width = 21
    Height = 13
    Caption = 'FHS'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object PageControl1: TPageControl
    Left = 4
    Top = 4
    Width = 938
    Height = 633
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '[Inscri��o]'
      object Bevel1: TBevel
        Left = 5
        Top = 123
        Width = 920
        Height = 5
      end
      object PageControl2: TPageControl
        Left = 5
        Top = 135
        Width = 920
        Height = 465
        ActivePage = TabSheet15
        MultiLine = True
        TabOrder = 0
        OnChanging = PageControl2Changing
        object TabSheet2: TTabSheet
          Caption = '[Dados do Inscrito]'
          object PageControl4: TPageControl
            Left = 8
            Top = 4
            Width = 898
            Height = 362
            ActivePage = TabSheet9
            TabOrder = 0
            OnChanging = PageControl4Changing
            object TabSheet9: TTabSheet
              Caption = '[Dados Cadastrais]'
              object gpbNOME_INSCRITO: TGroupBox
                Left = 6
                Top = 5
                Width = 512
                Height = 50
                Caption = '[Nome completo]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                object edtNOME_COMPLETO_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 494
                  Height = 21
                  Hint = 
                    'INFORME O NOME COMPLETO DO INSCRITO, SEM ABREVIA��ES E/OU ACENTO' +
                    'S'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtNOME_COMPLETO_INSCRITOExit
                end
              end
              object gpbDATA_NASCIMENTO: TGroupBox
                Left = 520
                Top = 5
                Width = 365
                Height = 50
                Caption = '[Data de Nascimento]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                object lblIDADE: TLabel
                  Left = 117
                  Top = 19
                  Width = 212
                  Height = 13
                  AutoSize = False
                  Caption = 'Idade: 00 anos e 00 meses'
                end
                object mskDATA_NASCIMENTO_INSCRITO: TMaskEdit
                  Left = 9
                  Top = 16
                  Width = 102
                  Height = 21
                  Hint = 'INFORME A DATA DE NASCIMENTO DO INSCRITO'
                  EditMask = '99/99/9999'
                  MaxLength = 10
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Text = '  /  /    '
                  OnExit = mskDATA_NASCIMENTO_INSCRITOExit
                end
              end
              object gpbESTADO_CIVIL_INSCRITO: TGroupBox
                Left = 6
                Top = 61
                Width = 207
                Height = 50
                Caption = '[Estado Civil]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
                object cmbESTADO_CIVIL_INSCRITO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 187
                  Height = 22
                  Hint = 'SELECIONE O ESTADO CIVIL DO INSCRITO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 16
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbSEXO_INSCRITO: TGroupBox
                Left = 215
                Top = 61
                Width = 140
                Height = 50
                Caption = '[Sexo]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                object cmbSEXO_INSCRITO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 125
                  Height = 19
                  Hint = 'SELECIONE O SEXO DO INSCRITO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = cmbSEXO_INSCRITOClick
                  OnExit = cmbSEXO_INSCRITOExit
                  Items.Strings = (
                    'Masculino'
                    'Feminino')
                end
              end
              object gpbNATURALIDADE_INSCRITO: TGroupBox
                Left = 357
                Top = 61
                Width = 528
                Height = 50
                Caption = '[Naturalidade]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                object SpeedButton2: TSpeedButton
                  Left = 492
                  Top = 16
                  Width = 23
                  Height = 22
                end
                object cmbNATURALIDADE_INSCRITO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 478
                  Height = 19
                  Hint = 'SELECIONE A CIDADE NATAL DO INSCRITO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbNACIONALIDADE_INSCRITO: TGroupBox
                Left = 6
                Top = 115
                Width = 316
                Height = 50
                Caption = '[Nacionalidade]'
                TabOrder = 5
                object cmbNACIONALIDADE_INSCRITO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 299
                  Height = 19
                  Hint = 'SELECIONE O PA�S DO INSCRITO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbENDERECO_INSCRITO: TGroupBox
                Left = 324
                Top = 115
                Width = 493
                Height = 50
                Caption = '[Endere�o]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                object edtENDERECO_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 475
                  Height = 21
                  Hint = 'INFORME O ENDERE�O COMPLETO DO INSCRITO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtENDERECO_INSCRITOExit
                end
              end
              object gpbNUMERO_RESIDENCIA_INSCRITO: TGroupBox
                Left = 819
                Top = 115
                Width = 66
                Height = 50
                Caption = '[N.�]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                object edtNUMERO_RESIDENCIA_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 45
                  Height = 21
                  Hint = 
                    'INFORME O N�MERO DA RESID�NCIA DE ACORDO COM O ENDERE�O INFORMAD' +
                    'O'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbREGIAO_INSCRITO: TGroupBox
                Left = 411
                Top = 170
                Width = 114
                Height = 50
                Caption = '[Regi�o]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 10
                object cmbREGIAO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 96
                  Height = 19
                  Hint = 'SELECIONE A REGI�O DO ENDERE�O RESIDENCIAL'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbCOMPLEMENTO_ENDERECO_INSCRITO: TGroupBox
                Left = 7
                Top = 170
                Width = 292
                Height = 50
                Caption = '[Complemento de endere�o]'
                TabOrder = 8
                object edtCOMPLEMENTO_ENDERECO_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 273
                  Height = 21
                  Hint = 'INFORME O COMPLEMENTO DE ENDERE�O '
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtCOMPLEMENTO_ENDERECO_INSCRITOExit
                end
              end
              object gpbCEP_ENDERECO_INSCRITO: TGroupBox
                Left = 300
                Top = 170
                Width = 109
                Height = 50
                Caption = '[CEP]'
                TabOrder = 9
                object SpeedButton4: TSpeedButton
                  Left = 79
                  Top = 15
                  Width = 23
                  Height = 22
                  Hint = 'CLIQUE PARA ACESSAR O SITE DOS CORREIOS'
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECFAFEE3F0F8E4EEF6E3F0F7E2
                    F1F8ECF7FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    BCF2FC41C1E73787C03987BF3692C73FA1D1B9E0F0FFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFD0F7FD31C3E909A0D50E87C41271B4137EBD80
                    BFDFF7FBFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCF9FE47CEEE
                    0AA0D50E9FD40CA7DA108BC768AAD2EFF6F9FFFEFEFEFEFEFEFEFEFFFEFEFFFF
                    FFFFFFFFFFFFFFFFFFFFA4E7F714A8D90D9ED30CA6D90AADDD13B5E3AFE0EAC9
                    B185AF925DB99B5EC1A35EC9AA5FE1CD98FDFCF9FFFFFFFFFFFFF0F9FC70C5E5
                    0CA4D70BACDC09B3E106BAE73DD1F3BECCAE9C6F13956400A37200B07D00C18F
                    0BEAD59AFFFEFEFFFFFFFFFFFFEAF7FB52C3E605B0E004B7E502BFEA00C5EF4F
                    DEF9C9C898AD7E0DAF7D00BD8900C99300DAA817F7E6B5FFFFFEFFFFFFFFFFFF
                    DBF4FA74D6F062D5F162DAF460DEF761E3FBBCF3F8CBAA57BA8600C79200D49E
                    00E0A700E6B73BFBF6EBFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFE
                    FFFFD7CFC17F5A21A77700D49D00DFA700E0A508ECCE8CFEFCFAFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6DFD58261305B3200663E00BB8900E1A6
                    01E7C26DFDFAF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F7F4AA
                    93707855267352276B4C279F7828E8C163FBF6EBFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFCFBFAEEE9E2ECE7E1EBE7E1EBE6E1F4EFE2FEFB
                    F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                  ParentShowHint = False
                  ShowHint = True
                  OnClick = SpeedButton4Click
                end
                object mskCEP_RESIDENCIAL_INSCRITO: TMaskEdit
                  Left = 9
                  Top = 16
                  Width = 66
                  Height = 21
                  Hint = 'INFORME O N�MERO DO CEP DO ENDERE�O RESIDENCIAL'
                  EditMask = '99999-999'
                  MaxLength = 9
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Text = '     -   '
                end
              end
              object gpbBAIRRO_RESIDENCIAL_INSCRITO: TGroupBox
                Left = 7
                Top = 278
                Width = 355
                Height = 50
                Caption = '[Bairro]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 15
                object SpeedButton5: TSpeedButton
                  Left = 492
                  Top = 16
                  Width = 23
                  Height = 22
                end
                object SpeedButton6: TSpeedButton
                  Left = 322
                  Top = 16
                  Width = 23
                  Height = 22
                end
                object cmbBAIRRO_INSCRITO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 312
                  Height = 19
                  Hint = 'SELECOINE O BAIRRO REFERENTE AO ENDERE�O INFORMADO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbPONTO_REFERENCIA: TGroupBox
                Left = 641
                Top = 170
                Width = 245
                Height = 50
                Caption = '[Ponto de Refer�ncia]'
                TabOrder = 12
                object edtPONTO_REFERENCIA_RESIDENCIA: TEdit
                  Left = 9
                  Top = 18
                  Width = 225
                  Height = 21
                  Hint = 'INFORME UM PONTO DE REFER�NCIA PR�XIMO AO ENDERE�O'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtPONTO_REFERENCIA_RESIDENCIAExit
                end
              end
              object gpbRESIDE_FAMILIA: TGroupBox
                Left = 7
                Top = 224
                Width = 114
                Height = 50
                Caption = '[Reside c/ Fam.]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 13
                object cmbRESIDE_FAMILIA: TComboBox
                  Left = 8
                  Top = 17
                  Width = 96
                  Height = 22
                  Hint = 'SELECIONE SE RESIDE OU N�O COM A FAM�LIA'
                  Style = csOwnerDrawFixed
                  ItemHeight = 16
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Items.Strings = (
                    'Sim'
                    'N�o')
                end
              end
              object gpbTEMPO_RESIDENCIA: TGroupBox
                Left = 526
                Top = 170
                Width = 114
                Height = 50
                Caption = '[Tempo Resid.]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 11
                object edtTEMPO_RESIDENCIA: TEdit
                  Left = 9
                  Top = 18
                  Width = 95
                  Height = 21
                  Hint = 'INFORME O TEMPO DE RESID�NCIA'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbGRAU_PARENTESCO: TGroupBox
                Left = 123
                Top = 224
                Width = 763
                Height = 50
                Caption = '[Outros - Especificar se existe rela��o de parentesco]'
                TabOrder = 14
                object edtN_RESIDE_FAM: TEdit
                  Left = 9
                  Top = 18
                  Width = 508
                  Height = 21
                  Hint = 'INFORME A RELA��O DE PARENTESCO COM O INSCRITO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtN_RESIDE_FAMExit
                end
              end
              object gpbUNIDADE_REFERENCIA_PARA_INSCRITO: TGroupBox
                Left = 363
                Top = 278
                Width = 523
                Height = 50
                Caption = '[Unidade de Refer�ncia]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 16
                object cmbUNIDADE_REFERENCIA_PARA_INSCRITO: TComboBox
                  Left = 8
                  Top = 17
                  Width = 505
                  Height = 19
                  Hint = 
                    'SELECOINE A UNIDADE DE REFER�NCIA DE ACORDO COM O ENDERE�O INFOR' +
                    'MADO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
            end
            object TabSheet10: TTabSheet
              Caption = '[Documenta��o e Telefones]'
              ImageIndex = 1
              object gpbNUMERO_CARTAO_SUS_INSCRITO: TGroupBox
                Left = 5
                Top = 5
                Width = 175
                Height = 50
                Caption = '[N�mero Cart�o SUS]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                object btnACESSO_CONSULTASUS: TSpeedButton
                  Left = 146
                  Top = 17
                  Width = 23
                  Height = 22
                  Hint = 'CLIQUE PARA TER ACESSO � CONSULT DO N�MERO DO CART�O DO SUS'
                  Flat = True
                  Glyph.Data = {
                    36050000424D360500000000000036040000280000000E000000100000000100
                    08000000000000010000CA0E0000C30E00000001000000000000000000003300
                    00006600000099000000CC000000FF0000000033000033330000663300009933
                    0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                    000000990000339900006699000099990000CC990000FF99000000CC000033CC
                    000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                    0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                    330000333300333333006633330099333300CC333300FF333300006633003366
                    33006666330099663300CC663300FF6633000099330033993300669933009999
                    3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                    330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                    66006600660099006600CC006600FF0066000033660033336600663366009933
                    6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                    660000996600339966006699660099996600CC996600FF99660000CC660033CC
                    660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                    6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                    990000339900333399006633990099339900CC339900FF339900006699003366
                    99006666990099669900CC669900FF6699000099990033999900669999009999
                    9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                    990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                    CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                    CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                    CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                    CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                    CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                    FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                    FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                    FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                    FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000ACACACACACAC
                    ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
                    00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
                    02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
                    5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
                    00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
                    ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
                    D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
                    D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
                  ParentShowHint = False
                  ShowHint = True
                  Visible = False
                  OnClick = btnACESSO_CONSULTASUSClick
                end
                object edtNUMERO_CARTAO_SUS_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 135
                  Height = 21
                  Hint = 'INFORME O N�MERO DO CART�O SUS'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbNUMERO_CRA_INSCRITO: TGroupBox
                Left = 182
                Top = 5
                Width = 175
                Height = 50
                Caption = '[N�mero CRA]'
                TabOrder = 1
                object edtNUMERO_CRA_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 157
                  Height = 21
                  Hint = 'INFORME O N�MERO DO CRA'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbNUMERO_NIS_INSCRITO: TGroupBox
                Left = 359
                Top = 5
                Width = 175
                Height = 50
                Caption = '[N�mero NIS]'
                TabOrder = 2
                object edtNUMERO_NIS_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 157
                  Height = 21
                  Hint = 'INFORME O N�MERO DO NIS'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbNUMERO_CARTAO_PASSE_INSCRITO: TGroupBox
                Left = 537
                Top = 5
                Width = 178
                Height = 50
                Caption = '[N�mero Cart�o de Passe Escolar]'
                TabOrder = 3
                object edtNUMERO_CARTAO_PASSE_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 158
                  Height = 21
                  Hint = 'INFORME O N�MERO DO CART�O DE PASSE'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbCERTIDAO_NASCIMENTO_INSCRITO: TGroupBox
                Left = 5
                Top = 58
                Width = 255
                Height = 50
                Caption = '[Certid�o de Nascimento] [Livro] [Folha]'
                TabOrder = 5
                object edtCERTIDAO_NASCIMENTO_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 237
                  Height = 21
                  Hint = 'INFORME TODOS OS DADOS DA CERTID�O DE NASCIMENTO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtCERTIDAO_NASCIMENTO_INSCRITOExit
                end
              end
              object gpbNUMERO_CPF_INSCRITO: TGroupBox
                Left = 262
                Top = 58
                Width = 110
                Height = 50
                Caption = '[CPF]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                object mskNUMERO_CPF_INSCRITO: TMaskEdit
                  Left = 8
                  Top = 16
                  Width = 96
                  Height = 21
                  Hint = 'INFORME O N�MERO DO CPF DO ISNCRITO'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbDADOS_RG_INSCRITO: TGroupBox
                Left = 374
                Top = 58
                Width = 510
                Height = 50
                Caption = '[RG]                    [Data Emiss�o]   [�rg�o Emissor]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                object SpeedButton7: TSpeedButton
                  Left = 476
                  Top = 16
                  Width = 23
                  Height = 22
                end
                object edtNUMERO_RG_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 103
                  Height = 21
                  Hint = 'INFORME O N�MERO DO RG DO INSCRITO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object mskDATA_EMISSAO_RG_INSCRITO: TMaskEdit
                  Left = 114
                  Top = 17
                  Width = 96
                  Height = 21
                  Hint = 'INFORME A DATA DE EMISS�O DO RG DO INSCRITO'
                  EditMask = '99/99/9999'
                  MaxLength = 10
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  Text = '  /  /    '
                end
                object cmbORGAO_EMISSOR_RG_INSCRITO: TComboBox
                  Left = 211
                  Top = 17
                  Width = 260
                  Height = 19
                  Hint = 'SELECIONE O �RG�O EMISSOR DO RG DO INSCRITO'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
              end
              object gpbTELEFONES_E_CONTATOS_INSCRITO: TGroupBox
                Left = 6
                Top = 109
                Width = 388
                Height = 218
                Caption = '[Telefones de Contato]'
                TabOrder = 8
                object lblTOTAL_CARACTERES: TLabel
                  Left = 248
                  Top = 193
                  Width = 103
                  Height = 13
                  Caption = 'Caracteres restantes: '
                  Visible = False
                end
                object Label7: TLabel
                  Left = 11
                  Top = 161
                  Width = 191
                  Height = 13
                  Caption = 'Sugest�o para o cadastro dos telefones:'
                end
                object Label8: TLabel
                  Left = 11
                  Top = 177
                  Width = 189
                  Height = 13
                  Caption = '(P) - Pr�prio. Exemplo (11) 1111-1111(P)'
                end
                object Label9: TLabel
                  Left = 11
                  Top = 193
                  Width = 196
                  Height = 13
                  Caption = '(R) - Recado. Exemplo (11) 1111-1111(R)'
                end
                object medTELEFONES_CONTATOS_INSCRITO: TMemo
                  Left = 9
                  Top = 20
                  Width = 368
                  Height = 140
                  Hint = 
                    'INFORME OS TELEFONES DE CONTATO, NOMES PARA RECADO, TIPO DE TELE' +
                    'FONE, ETC...'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = medTELEFONES_CONTATOS_INSCRITOExit
                  OnKeyPress = medTELEFONES_CONTATOS_INSCRITOKeyPress
                end
              end
              object gpbNUMERO_CARTAO_SIAS_INSCRITO: TGroupBox
                Left = 717
                Top = 5
                Width = 166
                Height = 50
                Caption = '[SIAS]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                object edtNUMERO_CARTAO_SIAS_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 147
                  Height = 21
                  Hint = 'INFORME O N�MERO DO SIAS'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
            end
            object TabSheet17: TTabSheet
              Caption = '[Dados de Escola]'
              ImageIndex = 3
              object rdgESTA_MATRICULADO: TRadioGroup
                Left = 5
                Top = 5
                Width = 126
                Height = 50
                Hint = 'SELECIONE A OP��O SE EST� MATRICULADO OU N�O'
                Caption = '[Est� matriculado?]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Items.Strings = (
                  'SIM'
                  'N�O')
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = rdgESTA_MATRICULADOClick
              end
              object gpbMOTIVO_NAO_ESTUDA: TGroupBox
                Left = 133
                Top = 5
                Width = 322
                Height = 50
                Caption = '[Motivo de n�o estar estudando]'
                TabOrder = 1
                object edtMOTIVO_NAO_ESTUDA: TEdit
                  Left = 9
                  Top = 18
                  Width = 307
                  Height = 21
                  Hint = 'INFORME O MOTIVO DE N�O ESTAR ESTUDANDO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtMOTIVO_NAO_ESTUDAExit
                end
              end
              object gpbESCOLA_ATUAL_INSCRITO: TGroupBox
                Left = 458
                Top = 5
                Width = 427
                Height = 50
                Caption = '[Nome da Escola]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
                object cmbESCOLA_ATUAL_INSCRITO: TComboBox
                  Left = 9
                  Top = 17
                  Width = 414
                  Height = 19
                  Hint = 'SELECIONE O NOME DA ESCOLA'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = cmbESCOLA_ATUAL_INSCRITOClick
                end
              end
              object gpbPERIODO_ESCOLAR_INSCRITO: TGroupBox
                Left = 441
                Top = 56
                Width = 167
                Height = 50
                Caption = '[Per�odo Escolar]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                object cmbPERIODO_ESCOLAR_INSCRITO: TComboBox
                  Left = 9
                  Top = 17
                  Width = 147
                  Height = 19
                  Hint = 'SELECIONE O PER�ODO ESCOLAR'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Items.Strings = (
                    'Manh�'
                    'Tarde'
                    'Noite'
                    'Integral'
                    'Sem Informa��o')
                end
              end
              object gpbNUMERO_RA_INSCRITO: TGroupBox
                Left = 611
                Top = 56
                Width = 274
                Height = 50
                Caption = '[N�mero do RA]'
                TabOrder = 5
                object edtNUMERO_RA_INSCRITO: TEdit
                  Left = 9
                  Top = 18
                  Width = 260
                  Height = 21
                  Hint = 'INFORME O N�MERO DO RA'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbSERIE_ATUAL_INSCRITO: TGroupBox
                Left = 5
                Top = 56
                Width = 433
                Height = 50
                Caption = '[S�rie Escolar]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                object cmbSERIE_ATUAL_INSCRITO: TComboBox
                  Left = 9
                  Top = 17
                  Width = 414
                  Height = 19
                  Hint = 'SELECIONE A S�RIE ESCOLAR'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbRELACAO_IDADE_SERIE_INSCRITO: TGroupBox
                Left = 5
                Top = 108
                Width = 199
                Height = 50
                Caption = '[Rela��o Idade x S�rie]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                object cmbRELACAO_IDADE_SERIE_INSCRITO: TComboBox
                  Left = 8
                  Top = 18
                  Width = 181
                  Height = 21
                  Hint = 'SELECIONE A RELA��O IDADE x S�RIE'
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Items.Strings = (
                    'Compat�vel Padr�o'
                    'Incompat�vel Padr�o')
                end
              end
              object gpbRAZAO_INCOMPATIBILIDADE_DADE_SERIE: TGroupBox
                Left = 206
                Top = 107
                Width = 680
                Height = 102
                Caption = '[Raz�o da Incompatibilidade]'
                TabOrder = 8
                object medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE: TMemo
                  Left = 9
                  Top = 18
                  Width = 663
                  Height = 74
                  Hint = 'INFORME A RAZ�O DA INCOMPATIBILIDADE ESCOLA x S�RIE'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = medRAZAO_INCOMPATIBILIDADE_IDADE_SERIEExit
                end
              end
              object gpbDATA_ATUALIZACAO_ESCOLA: TGroupBox
                Left = 5
                Top = 159
                Width = 199
                Height = 50
                Caption = '[Data Atualiza��o Escola]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                object mskDATA_ATUALIZACAO_ESCOLA: TMaskEdit
                  Left = 10
                  Top = 17
                  Width = 179
                  Height = 21
                  Hint = 'INFORME A DATA DA �LTIMA ATUALIZA��O DOS DADOS ESCOLARES'
                  EditMask = '99/99/9999'
                  MaxLength = 10
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Text = '  /  /    '
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = '[Situa��o de Sa�de]'
              ImageIndex = 2
              object gpbINVALLIDEZ_INSCRITO: TGroupBox
                Left = 559
                Top = 5
                Width = 100
                Height = 50
                Caption = '[Invalidez]'
                TabOrder = 1
                object chkINVALIDEZ_TEMPORARIA_INSCRITO: TCheckBox
                  Left = 9
                  Top = 13
                  Width = 76
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR INVALIDEZ TEMPOR�RIA'
                  Caption = 'Tempor�ria'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkINVALIDEZ_PERMANENTE_INSCRITO: TCheckBox
                  Left = 9
                  Top = 28
                  Width = 76
                  Height = 16
                  Hint = 'CLIQUE SE ELE INFORMAR INVALIDEZ PERMANENTE'
                  Caption = 'Permanente'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
              end
              object gpbDIAGNOSTICO_SAUDE_INSCRITO: TGroupBox
                Left = 5
                Top = 5
                Width = 552
                Height = 50
                Caption = '[SA�DE  - Diagn�stica]'
                TabOrder = 0
                object edtDIAGNOSTICO_SAUDE_INSCRITO: TEdit
                  Left = 10
                  Top = 18
                  Width = 534
                  Height = 21
                  Hint = 'INFORME A SITUA��O DE SA�DE/DIGN�STICO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnExit = edtDIAGNOSTICO_SAUDE_INSCRITOExit
                end
              end
              object gpbPORTADOR_DOENCA_INFECTO_INSCRITO: TGroupBox
                Left = 661
                Top = 5
                Width = 225
                Height = 50
                TabOrder = 2
                object chkPORTADOR_DOENCA_INFECTO_INSCRITO: TCheckBox
                  Left = 9
                  Top = 19
                  Width = 210
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR PORTADOR DE DOEN�A INFECTO CONTAGIOSA'
                  Caption = 'Portador de doen�a infecto contagiosa'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbINCAPACIDADE_TRABALHO_INSCRITO: TGroupBox
                Left = 5
                Top = 57
                Width = 174
                Height = 50
                TabOrder = 3
                object chkINCAPACIDADE_TRABALHO_INSCRITO: TCheckBox
                  Left = 9
                  Top = 19
                  Width = 163
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR INCAPACIDADE PARA O TRABALHO'
                  Caption = 'Incapacidade para o trabalho'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbDESPESAS_MEDICAS_INSCRITO: TGroupBox
                Left = 182
                Top = 57
                Width = 202
                Height = 50
                Caption = '[Gastos com Sa�de - R$]'
                TabOrder = 4
                object edtVALOR_DESPESAS_MEDICAS_INSCRITO: TEdit
                  Left = 9
                  Top = 17
                  Width = 88
                  Height = 21
                  Hint = 'INFORME O TOTAL DE GASTOS COM SA�DE'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkCOMPROVOU_DESPESAS_MEDICAS: TCheckBox
                  Left = 103
                  Top = 17
                  Width = 85
                  Height = 17
                  Hint = 'CLIQUE SE ELE COMPROVAR DESPESAS COM SA�DE'
                  Caption = 'Comprovada'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
              end
              object gpbMOTIVO_PROCURA_INSCRITO: TGroupBox
                Left = 5
                Top = 109
                Width = 540
                Height = 129
                Caption = '[Motivo da Procura]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 5
                object Label2: TLabel
                  Left = 172
                  Top = 16
                  Width = 103
                  Height = 13
                  Caption = 'Encaminhamentos'
                end
                object chkMOTIVO_PROCURA_TRABALHO: TCheckBox
                  Left = 8
                  Top = 19
                  Width = 85
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR PROCURA POR MOTIVO - TRABALHO'
                  Caption = 'Trabalho'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkMOTIVO_PROCURA_CURSOS: TCheckBox
                  Left = 8
                  Top = 40
                  Width = 85
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR PROCURA POR MOTIVO - CURSOS'
                  Caption = 'Cursos'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO: TCheckBox
                  Left = 8
                  Top = 62
                  Width = 127
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR PROCURA POR MOTIVO - N�O FICAR SOZINHO'
                  Caption = 'N�o ficar sozinho'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                object chkMOTIVO_PROCURA_TIRAR_RUA: TCheckBox
                  Left = 8
                  Top = 83
                  Width = 107
                  Height = 17
                  Hint = 'CLIQUE SE ELE INFORMAR PROCURA POR MOTIVO - TIRAR DA RUA'
                  Caption = 'Tirar da rua'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                end
                object chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS: TCheckBox
                  Left = 184
                  Top = 35
                  Width = 112
                  Height = 17
                  Hint = 'CLIQUE SE ELE VEIO COMO ENCAMINHAMENTO - CREAS'
                  Caption = 'CREAS'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                end
                object chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS: TCheckBox
                  Left = 184
                  Top = 51
                  Width = 112
                  Height = 17
                  Hint = 'CLIQUE SE ELE VEIO COMO ENCAMINHAMENTO - CRAS'
                  Caption = 'CRAS'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                end
                object chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ: TCheckBox
                  Left = 184
                  Top = 67
                  Width = 112
                  Height = 17
                  Hint = 'CLIQUE SE ELE VEIO COMO ENCAMINHAMENTO - VIJ'
                  Caption = 'VIJ'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 6
                end
                object chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS: TCheckBox
                  Left = 184
                  Top = 83
                  Width = 112
                  Height = 17
                  Hint = 'CLIQUE SE ELE VEIO COMO ENCAMINHAMENTO - ASSISTENTE SOCIAL'
                  Caption = 'A. Social'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 7
                end
                object chkMOTIVO_PROCURA_OUTROS: TCheckBox
                  Left = 312
                  Top = 19
                  Width = 112
                  Height = 17
                  Hint = 'CLIQUE SE ELE VEIO COMO ENCAMINHAMENTO - OUTROS'
                  Caption = 'Outros'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 8
                end
                object medMOTIVO_PROCURA_OUTROS: TMemo
                  Left = 312
                  Top = 38
                  Width = 218
                  Height = 82
                  Hint = 'INFORMAR MOTIVO DE OUTRO ENCAMINHAMENTO'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 9
                  OnExit = medMOTIVO_PROCURA_OUTROSExit
                end
                object chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR: TCheckBox
                  Left = 184
                  Top = 101
                  Width = 123
                  Height = 17
                  Hint = 'CLIQUE SE ELE VEIO COMO ENCAMINHAMENTO - CONSELHO TUTELAR'
                  Caption = 'Conselho Tutelar'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 10
                end
              end
            end
            object TabSheet18: TTabSheet
              Caption = '[Benef. Sociais e Bens Complem.]'
              ImageIndex = 4
              OnShow = TabSheet18Show
              object gpbBENEFICIOS: TGroupBox
                Left = 5
                Top = 5
                Width = 614
                Height = 195
                Caption = '[Informa��es referentes a Fam�lia]'
                TabOrder = 0
                object btnNOVO_BENEFICIO: TSpeedButton
                  Left = 481
                  Top = 76
                  Width = 42
                  Height = 42
                  Hint = 'CLIQUE PARA PREENCHER UM NOVO BENEF�CIO'
                  Flat = True
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFFEFEFDF4FAF3EBF7E9E3F4E1E3F4E1ECF7EAF4FAF3FEFEFDFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEF7EDB6DFB0A5DA9DB8E4B0D1
                    EECBD1EECCBAE4B2A6DB9EB6DFB1EEF7EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    D9EED67DC47396D58EBEEBB8D4F5D0E2FAE0E3FAE1D5F6D1C0ECBA98D68F7DC4
                    73D9EED6FFFFFFFFFFFFFFFFFFE2F2E07BC47197DA8F9BE393ABEBA5C0EDBBCC
                    F0C9CDF0C9C1EDBCACEBA69CE49497DA8F7BC471E2F2E0FFFFFFFBFDFB8FCD87
                    90D68789D87F90DE879BE493FBFDFBFFFFFFFFFFFFFEFEFE9EE49791DF8889D9
                    8091D6888FCD87FBFDFBDBEFD884CC7B7DCE7385D57A8ADA828FDE87F9FCF9FF
                    FFFFFFFFFFFEFEFE92DD898BDA8285D57B7ECF7384CC7CDBEFD8B7DFB177C66C
                    78CA6D7FD07485D57B8CD983F8FBF7FFFFFFFFFFFFFEFEFE8DD98486D57B7FD0
                    7579CA6E78C66CB7DFB1A9D8A16CBE5F9BD493DEF0DCE0F1DEE1F1DFFDFEFDFF
                    FFFFFFFFFFFEFEFEE1F1DFE0F1DEDEF0DCA2D69A6CBF5FA9D8A1A2D39873C065
                    ABDAA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFB4DDAD73C065A2D499A3D49976BF68A5D59CFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADD9A577BF69A4D49AB9DDB172BC62
                    98CE8EF3F9F1F3F9F2F3F9F2FDFEFDFFFFFFFFFFFFFEFEFEF3F9F2F3F9F2F3F9
                    F1A0D29672BC63BADDB1EBF3E96AB75873BC6477BE687AC16C7CC26FF0F7EEFF
                    FFFFFFFFFFF8FBF87CC26F7AC16C77BF6873BC646AB758EBF4E9FFFFFFA0D194
                    6EB85C73BB6275BD6677BE68EEF6ECFFFFFFFFFFFFF6FAF677BE6875BD6673BB
                    626EB85CA1D194FFFFFFFFFFFFFAFCFA81C1706DB75A71BA5F73BB62EAF4E8FF
                    FFFFFFFFFFF3F8F273BB6271BA5F6EB75A82C171FAFCFAFFFFFFFFFFFFFFFFFF
                    FAFCFA9DCE8F66B3506FB85B70B85D71B85D71B85D70B85D6FB85B66B3509ECE
                    90FAFCFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E2A2D1947EBF6A6E
                    B7596EB7597EBF6AA3D194E6F0E2FFFFFFFFFFFFFFFFFFFFFFFF}
                  ParentShowHint = False
                  ShowHint = True
                  OnClick = btnNOVO_BENEFICIOClick
                end
                object btnSALVA_BENEFICIO: TSpeedButton
                  Left = 523
                  Top = 76
                  Width = 42
                  Height = 42
                  Hint = 'CLIQUE PARA OS DADOS DO BENEF�CIO INFORMADO'
                  Flat = True
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFFEFEFDFCF7F2FAF1E7F8ECDEF8ECDEFAF1E7FCF7F2FEFEFDFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF2EBECCAA7EBC18CF0D0A1F6
                    E1BCF6E1BDF0D1A3EBC28FEDCBA8FAF2EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    F5E3D2E0A160EAB775F6D8A3FBE8BEFDF1D3FDF2D4FBE9BFF6D9A5EAB877E0A1
                    61F5E3D2FFFFFFFFFFFFFFFFFFF7EADDE0A05CEBB973F0C371F5D186F9DFA2FC
                    E7B6FCE7B7FADFA4F5D288F0C373EBBA74E0A05CF7EADDFFFFFFFDFCFBE4AE77
                    E6B26CE7B15DECBA64F0C77FF6DFB8F7D794F8D895F6D288F2C674EDBA64E8B1
                    5DE6B26CE4AE77FEFCFBF5E4D4E1A561DFA252E4AB59E9B970FBF3E8FFFFFFF6
                    E3C3F1C574F0C16DEDBA64E9B45FE4AC59DFA253E1A661F5E4D4ECCAA7D8964D
                    DB9B4DE3AE6AFAF3E9FFFFFFFFFFFFFFFFFFF5E0C0EAB867E8B462E5AC5AE0A4
                    54DC9C4DD8974DECCAA7E4BB93D28B3EDDA464FAF4ECFFFFFFFDFBF7F6E6CFFF
                    FFFFFFFFFFF4E1C7E6B26BE3AD66DFA45BD8974BD38B3EE5BC93E0B285D28D47
                    E0AE78FCF8F4FCF9F5E7BD88E3AD68F2DABBFFFFFFFFFFFFF4E1CADFA764DCA0
                    5DD89A57D28D46E0B286DFB187D28D4BD59352DFAC76E1B07BDCA260DEA562DE
                    A663EED3B3FFFFFFFFFFFFF3E1CED99C5CD59452D28D4BE0B287E6C3A2CE8744
                    D18D4CD49251D69656D89A59D99C5CDA9E5DDA9E5DEBCBA9FEFEFEFFFFFFF3E2
                    D0D39253CE8745E6C3A2F5EDE5C97D39CE8746D08B4BD28F4FD49253D59455D5
                    9556D59556D59455E7C29EFEFEFEFFFFFFE4BE9ACA7D39F6EDE6FFFFFFDCAC81
                    CB803ECD8545CF8948D08B4BD18D4DD18E4ED18E4ED18D4ED08B4BE2B892EACC
                    B1CB8140DCAC81FFFFFFFFFFFFFCFAF9D18F58CA7E3CCC8242CD8544CE8646CE
                    8747CE8747CE8646CD8544CC8342CA7E3DD19058FCFBF9FFFFFFFFFFFFFFFFFF
                    FCFAF9DAA77CC77632CA7F3ECB8040CB8040CB8040CB8040CA7F3EC77632DBA8
                    7DFCFAF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E7DEDCAB83CF8B52C9
                    7D3DC97D3DCF8B52DCAB83F3E8DEFFFFFFFFFFFFFFFFFFFFFFFF}
                  ParentShowHint = False
                  ShowHint = True
                  OnClick = btnSALVA_BENEFICIOClick
                end
                object btnALTERA_BENEFICIO: TSpeedButton
                  Left = 565
                  Top = 76
                  Width = 42
                  Height = 42
                  Hint = 'CLIQUE PARA ALTERAR OS DADOS DO BENEF�CIO ATUAL'
                  Flat = True
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFFEFDFDFBF8F1FAF4E8F7F1E0F7F1E0FAF5E8FCF9F2FEFEFDFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4EAEAD7ABE9D096EDDCA9F5
                    E9C5F5EAC6EEDDABE9D297EBD8ACF9F4EAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                    F3E9D3DBB76AE7C982F5E2ACFBEDC5FEF4D9FEF4DAFBEEC7F5E2AEE7CA84DBB7
                    6BF3E9D3FFFEFEFFFFFFFFFFFFF6EEDDDBB566E9CA7EF0D07CF5DB91FAE5ACFC
                    ECBEFCECBFFAE6AEF6DB93F1D17DE9CA7EDBB566F6EEDDFFFFFFFDFCFAE0C17F
                    E6C474E8C266EDCA6FF2D27EF6DB92F8E09FF8E1A0F6DC94F2D380EDCA6FE8C3
                    67E6C474E0C17FFDFCFAF4EBD7DFBB6AE0BA60E9CD8BECD190EED594F0D89BF1
                    DBA0F1DBA0F0D89CEED595ECD291E9CE8CE1BB63DFBB6AF4EBD7EBD7ADD9B052
                    E2C072FEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                    FEE3C47AD9B053EBD7ADE4CD97D4A743D9B051EFDDB5FFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFF1E1BDD9B051D4A844E5CD98E1C689D4AA4C
                    D9B35CDDB966F8F2E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4E8DEBA
                    69DAB35CD4AA4BE1C689E1C68AD3AB4FD6AF56D9B35CE1C27DFEFDFAFFFFFFFF
                    FFFFFFFFFFFFFFFFFEFEFCE3C583D9B35CD6AF57D3AB50E1C689E7D3A5D0A748
                    D3AB50D5AE56D7B15AEAD6A9FFFFFFFFFFFFFFFFFFFFFFFFECDAB1D7B15BD5AE
                    56D3AB50D0A749E7D4A5F5F1E7CBA13CD0A849D2AA4FD4AD53D6AF58F5ECD8FF
                    FFFFFFFFFFF7EFDFD6B05BD4AD54D2AA4FD0A84ACCA13CF6F2E7FFFFFFDEC382
                    CDA440CFA747D0A94CD2AA4FD8B769FCFBF7FDFCF9DABA6FD2AA4FD1A94CCFA7
                    48CDA441DEC483FFFFFFFFFFFFFCFBF9D3B059CCA33ECEA644CFA747CFA849E0
                    C688E1C98ECFA849CFA747CEA644CCA33FD3B05AFCFBF9FFFFFFFFFFFFFFFFFF
                    FCFBF9DCC17DC99E33CDA440CDA541CDA542CDA542CDA541CDA440C99F33DCC1
                    7EFCFBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDDEDEC483D1AD52CB
                    A43ECBA43ED1AE52DEC584F3EDDFFFFFFFFFFFFFFFFFFFFFFFFF}
                  ParentShowHint = False
                  ShowHint = True
                  OnClick = btnALTERA_BENEFICIOClick
                end
                object btnCANCELA: TSpeedButton
                  Left = 544
                  Top = 128
                  Width = 42
                  Height = 42
                  Hint = 'CLIQUE PARA CANCELAR A OPERA��O ATUAL'
                  Flat = True
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFFDFDFEF0F0F8E3E3F4D8D8F0D9D9F1E4E4F5F0F0F9FDFDFEFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F69B9BDD7777DB8585E4A2
                    A2EFA3A3EF8888E57878DC9C9CDEE8E8F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    CDCDEC4A4AC65757D68585E9A6A6F2C3C3F8C4C4F8A9A9F38787E95959D64B4B
                    C7CCCCECFFFFFFFFFFFFFFFFFFD8D8F04242C65050D44343D95D5DE38282EC9C
                    9CF29D9DF28484ED5F5FE34545DA5050D44242C6D8D8F0FFFFFFF9F9FC6363CD
                    4646CC2D2DCB8383E0B9B9EE6262E37070E87171E86363E3BABAEE8282E02D2D
                    CB4646CC6363CDF9F9FCCDCDEC3F3FC52424BD8686DCFEFEFEFFFFFFCACAF24E
                    4EDC4F4FDCCACAF2FFFFFFFEFEFE8686DD2424BE4040C5CDCDED9898DB2525B5
                    4141C2FCFCFEFFFFFFFFFFFFFFFFFFD0D0F2D0D0F3FFFFFFFFFFFFFFFFFFFCFC
                    FE4141C22525B59898DB7F7FCE1717AC2323B57777D3F9F9FDFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFF9F9FD7676D32323B51818AC8080CF7474C72727AD
                    3636B63939BB6C6CCFF3F3FBFFFFFFFFFFFFFFFFFFFFFFFFF3F3FB6C6CCF3939
                    BC3636B72727AD7575C77979C73232AD3535B23838B65858C5EAEAF8FFFFFFFF
                    FFFFFFFFFFFFFFFFE9E9F85858C53838B63535B23232AE7979C79A9AD43131A9
                    3434AE5D5DC2EEEEF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEF95D5D
                    C23535AE3131AA9A9AD4E4E4F12A2AA33C3CAEEFEFF9FFFFFFFFFFFFFFFFFFD6
                    D6EFD6D6EFFFFFFFFFFFFFFFFFFFEFEFF93C3CAE2A2AA3E4E4F2FFFFFF7B7BC4
                    3232A66E6EC1F4F4FAFFFFFFC8C8E84444B24444B2C8C8E8FFFFFFF4F4FA6E6E
                    C13232A67C7CC5FFFFFFFFFFFFF9F9FB5353B13434A55D5DB8A5A5D83E3EAC39
                    39AA3939AA3E3EACA5A5D85D5DB83434A55353B1F9F9FBFFFFFFFFFFFFFFFFFF
                    F9F9FB7B7BC22E2EA03939A63939A63A3AA73A3AA73939A63939A62E2EA07B7B
                    C2F9F9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEED8383C55151AE3C
                    3CA53C3CA55151AE8383C5DEDEEDFFFFFFFFFFFFFFFFFFFFFFFF}
                  ParentShowHint = False
                  ShowHint = True
                  OnClick = btnCANCELAClick
                end
                object btnEXCLUI_BENEFICIO: TSpeedButton
                  Left = 502
                  Top = 128
                  Width = 42
                  Height = 42
                  Hint = 'CLIQUE PARA EXCLUIR O BENEF�CIO SELECIONADO'
                  Flat = True
                  Glyph.Data = {
                    42010000424D4201000000000000760000002800000011000000110000000100
                    040000000000CC00000000000000000000001000000010000000000000000000
                    BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                    77777000000077777000000077777000000077770F0F0F0F0777700000007777
                    0F0F0F0F07777000000077770F0F0F0F07777000000077770F0F0F0F07777000
                    000077770F0F0F0F07777000000077770F0F0F0F07777000000077770F0F0F0F
                    07777000000077770F0F0F0F07777000000077770F0F0F0F0777700000007777
                    0F0F0F0F0777700000007770000000000077700000007770FFFFFFFFF0777000
                    0000777700000000077770000000777777700077777770000000777777777777
                    777770000000}
                  ParentShowHint = False
                  ShowHint = True
                  OnClick = btnEXCLUI_BENEFICIOClick
                end
                object gpbBENEFICIO: TGroupBox
                  Left = 15
                  Top = 20
                  Width = 467
                  Height = 46
                  Caption = '[Descri��o do benef�cio]'
                  TabOrder = 0
                  object edtDESCRICAO_BENEFICIO: TEdit
                    Left = 9
                    Top = 17
                    Width = 452
                    Height = 21
                    Hint = 'INFORME O NOME DO BENEF�CIO'
                    CharCase = ecUpperCase
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnExit = edtDESCRICAO_BENEFICIOExit
                  end
                end
                object dbgBENEFICIOS_SOCIAIS: TDBGrid
                  Left = 16
                  Top = 72
                  Width = 456
                  Height = 109
                  Hint = 'BENEF�CIOS SOCIAIS CADASTRADOS'
                  DataSource = dsSel_BeneficiosSociais
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = dbgBENEFICIOS_SOCIAISDblClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'dsc_beneficio'
                      Title.Caption = '[Benef�cio]'
                      Width = 315
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'num_valorbeneficio'
                      Title.Alignment = taCenter
                      Title.Caption = '[Valor do Benef�cio]'
                      Width = 125
                      Visible = True
                    end>
                end
                object gpbDATA_INICIO_BENEFICIO: TGroupBox
                  Left = 516
                  Top = 20
                  Width = 93
                  Height = 46
                  Caption = '[Data In�cio]'
                  TabOrder = 3
                  Visible = False
                  object mskDATA_INICIO_BENEFICIO: TMaskEdit
                    Left = 9
                    Top = 16
                    Width = 75
                    Height = 21
                    Hint = 'INFORME A DATA DE IN�CIO DO RECEBIMENTO DO BENEF�CIO'
                    EditMask = '99/99/9999'
                    MaxLength = 10
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    Text = '  /  /    '
                  end
                end
                object gpbVALOR_BENEFICIO: TGroupBox
                  Left = 483
                  Top = 20
                  Width = 109
                  Height = 46
                  Caption = '[Valor]'
                  TabOrder = 1
                  object edtVALOR_BENEFICIO: TEdit
                    Left = 9
                    Top = 17
                    Width = 93
                    Height = 21
                    Hint = 'INFORME O VALOR DO BENEF�CIO RECEBIDO'
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                  end
                end
              end
              object gpbBENS_COMPLEMENTARES: TGroupBox
                Left = 622
                Top = 5
                Width = 259
                Height = 195
                Caption = '[Bens Complementares]'
                TabOrder = 1
                object chkIMOVEL_OUTRO: TCheckBox
                  Left = 16
                  Top = 24
                  Width = 143
                  Height = 17
                  Hint = 'CLIQUE SE POSSUI IM�VEL (AL�M DA MORADIA)'
                  Caption = 'Im�vel (al�m da moradia)'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkPOSSUI_CARRO: TCheckBox
                  Left = 16
                  Top = 40
                  Width = 143
                  Height = 17
                  Hint = 'CLIQUE SE POSSUI CARRO'
                  Caption = 'Carro'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object GroupBox84: TGroupBox
                  Left = 17
                  Top = 56
                  Width = 234
                  Height = 50
                  Caption = '[Modelo do Carro]                       [Ano do Carro]'
                  TabOrder = 2
                  object edtCARRO_MODELO: TEdit
                    Left = 9
                    Top = 17
                    Width = 149
                    Height = 21
                    Hint = 'INFORME O MODELO DO CARRO'
                    CharCase = ecUpperCase
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnExit = edtCARRO_MODELOExit
                  end
                  object edtCARRO_ANO: TEdit
                    Left = 161
                    Top = 17
                    Width = 56
                    Height = 21
                    Hint = 'INFORME O ANO DO CARRO'
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                  end
                end
                object chkPOSSUI_MOTO: TCheckBox
                  Left = 16
                  Top = 112
                  Width = 145
                  Height = 17
                  Hint = 'CLIQUE SE POSSUI MOTO'
                  Caption = 'Moto'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                end
                object GroupBox85: TGroupBox
                  Left = 17
                  Top = 129
                  Width = 235
                  Height = 50
                  Caption = '[Modelo da Moto]                       [Ano da Moto]'
                  TabOrder = 4
                  object edtMOTO_MODELO: TEdit
                    Left = 9
                    Top = 17
                    Width = 149
                    Height = 21
                    Hint = 'INFORME O MODELO DA MOTO'
                    CharCase = ecUpperCase
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnExit = edtMOTO_MODELOExit
                  end
                  object edtMOTO_ANO: TEdit
                    Left = 161
                    Top = 17
                    Width = 56
                    Height = 21
                    Hint = 'INFORME O ANO DA MOTO'
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                  end
                end
              end
              object gpbUTILIZACAO_VEICULO: TGroupBox
                Left = 5
                Top = 201
                Width = 877
                Height = 50
                Caption = '[Utiliza��o do Ve�culo]'
                TabOrder = 2
                object chkUSO_VEICULO_OUTRAS_PRIORIDADES: TCheckBox
                  Left = 16
                  Top = 21
                  Width = 143
                  Height = 17
                  Hint = 'CLIQUE SE UTILIZA O VE�CULO PARA OUTRAS PRIORIDADES'
                  Caption = 'Outras prioridades'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkUSO_VEICULO_TRABALHO: TCheckBox
                  Left = 396
                  Top = 21
                  Width = 74
                  Height = 17
                  Hint = 'CLIQUE SE UTILIZA O VE�CULO PARA O TRABALHO'
                  Caption = 'Trabalho'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object chkUSO_VEICULO_UNICO_MEIO_TRANSP: TCheckBox
                  Left = 721
                  Top = 21
                  Width = 138
                  Height = 17
                  Hint = 'CLIQUE PARA INFORMAR QUE O VE�CULO � O �NICO MEIO DE TRANSPORTE'
                  Caption = '�nico meio de transporte'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
              end
            end
            object TabSheet19: TTabSheet
              Caption = '[Situa��o Social]'
              ImageIndex = 5
              object gpbRELACAO_RESPONSABILIDADE: TGroupBox
                Left = 5
                Top = 5
                Width = 363
                Height = 50
                Caption = '[Rela��o de Responsabilidade]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                object cmbRELACAO_RESPONSABILIDADE: TComboBox
                  Left = 9
                  Top = 17
                  Width = 345
                  Height = 19
                  Hint = 'SELECIONE A RELA��O DE RESPONSABILIDADE'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbAGRAVANTES_CONDUTA: TGroupBox
                Left = 5
                Top = 58
                Width = 363
                Height = 252
                Caption = '[Agravantes em Raz�o da Conduta do Inscrito]'
                TabOrder = 1
                object chkAGRAVANTE_DROGADICTO: TCheckBox
                  Left = 16
                  Top = 24
                  Width = 97
                  Height = 17
                  Hint = 'CONDUTA - � DROGADICTO'
                  Caption = 'Drogadicto'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkAGRAVANTE_ALCOOLATRA: TCheckBox
                  Left = 16
                  Top = 59
                  Width = 97
                  Height = 17
                  Hint = 'CONDUTA - � ALCO�LATRA'
                  Caption = 'Alco�latra'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object chkAGRAVANTE_PAIMAE_ADOLESCENTE: TCheckBox
                  Left = 16
                  Top = 95
                  Width = 136
                  Height = 17
                  Hint = 'CONDUTA - � PAI OU M�E ADOLESCENTE'
                  Caption = '� pai/m�e adolescente'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                object chkAGRAVANTE_GESTANTE: TCheckBox
                  Left = 16
                  Top = 135
                  Width = 136
                  Height = 17
                  Hint = 'CONDUTA - � GESTANTE'
                  Caption = 'Gestante'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                end
                object chkAGRAVANTE_USO_PSICOTROPICOS: TCheckBox
                  Left = 16
                  Top = 176
                  Width = 186
                  Height = 17
                  Hint = 'CONDUTA - USA MEDICAMENTOS PSICOTR�PICOS'
                  Caption = 'Uso de medicamento psicotr�pico'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                end
                object chkAGRAVANTE_MEDIDAS_SOCIOEDUCATIVAS: TCheckBox
                  Left = 16
                  Top = 216
                  Width = 186
                  Height = 17
                  Hint = 'CONDUTA - POSSUI MEDIDAS SOCIO EDUCATIVAS'
                  Caption = 'Medida s�cio-educativa'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                end
              end
              object gpbSITUACOES_RISCO: TGroupBox
                Left = 372
                Top = 5
                Width = 498
                Height = 303
                Caption = '[Situa��es de Risco]'
                TabOrder = 2
                object chkSIT_RISCO_CREAS: TCheckBox
                  Left = 16
                  Top = 24
                  Width = 97
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - CREAS'
                  Caption = 'CREAS'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkSIT_RISCO_VIOLENCIA: TCheckBox
                  Left = 16
                  Top = 40
                  Width = 143
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � VIOL�NCIA'
                  Caption = 'Submetido � viol�ncia'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object chkSIT_RISCO_VIOLENCIA_FISICA: TCheckBox
                  Left = 32
                  Top = 56
                  Width = 136
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � VIOL�NCIA F�SICA'
                  Caption = 'F�sica'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                object chkSIT_RISCO_VIOLENCIA_PSICOLOGICA: TCheckBox
                  Left = 32
                  Top = 72
                  Width = 136
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � VIOL�NCIA PSICOL�GICA'
                  Caption = 'Psicol�gica'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                end
                object chkSIT_RISCO_VIOLENCIA_SEXUAL: TCheckBox
                  Left = 32
                  Top = 88
                  Width = 186
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � VIOL�NCIA SEXUAL'
                  Caption = 'Sexual'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                end
                object chkSIT_RISCO_VIOLENCIA_NEGLIGENCIA: TCheckBox
                  Left = 32
                  Top = 104
                  Width = 186
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � NEGLIG�NCIA'
                  Caption = 'Neglig�ncia'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                end
                object chkSIT_RISCO_VIOLENCIA_ABANDONO: TCheckBox
                  Left = 32
                  Top = 120
                  Width = 186
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � ABANDONO'
                  Caption = 'Abandono'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 6
                end
                object chkSIT_RISCO_PROSTITUICAO: TCheckBox
                  Left = 16
                  Top = 136
                  Width = 186
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � PROSTITUI��O'
                  Caption = 'Submetido � prostitui��o'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 7
                end
                object chkSIT_RISCO_TRAFICO_DROGAS: TCheckBox
                  Left = 16
                  Top = 152
                  Width = 186
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO AO TR�FICO DE DROGAS'
                  Caption = 'Submetido ao tr�fico de drogas'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 8
                end
                object chkSIT_RISCO_CONVIVE_DROGADICCAO: TCheckBox
                  Left = 16
                  Top = 168
                  Width = 236
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � CONVIV�NCIA COM DROGADIC��O'
                  Caption = 'Submetido � conviv�ncia com drogadic��o'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 9
                end
                object chkSIT_RISCO_CONVIVE_ALCOOLATRA: TCheckBox
                  Left = 16
                  Top = 184
                  Width = 236
                  Height = 17
                  Hint = 'SITUA��O DE RISCO - SUBMETIDO � CONVIV�NCIA COM ALCO�LATRAS'
                  Caption = 'Submetido � conviv�ncia com alco�latras'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 10
                end
                object chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_MENTAIS: TCheckBox
                  Left = 16
                  Top = 200
                  Width = 393
                  Height = 17
                  Hint = 
                    'SITUA��O DE RISCO - SUBMETIDO � CONVIV�NCIA COM PORTADORES DE DO' +
                    'EN�AS MENTAIS'
                  Caption = 
                    'Submetido � conviv�ncia com portadores de doen�as mentais/psiqui' +
                    '�tricas'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 11
                end
                object chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INFECTO: TCheckBox
                  Left = 16
                  Top = 216
                  Width = 393
                  Height = 17
                  Hint = 
                    'SITUA��O DE RISCO - SUBMETIDO � CONVIV�NCIA COM PORTADORES DE DO' +
                    'EN�AS INFECTO CONTAGIOSAS'
                  Caption = 
                    'Submetido � conviv�ncia com portadores de doen�as infecto contag' +
                    'iosas'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 12
                end
                object chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INCAPACITANTES: TCheckBox
                  Left = 16
                  Top = 232
                  Width = 393
                  Height = 17
                  Hint = 
                    'SITUA��O DE RISCO - SUBMETIDO � CONVIV�NCIA COM PORTADORES DE DO' +
                    'EN�AS INCAPACITANTES'
                  Caption = 'Submetido � conviv�ncia com portadores de doen�as incapacitantes'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 13
                end
                object chkSIT_RISCO_MEMBRO_ATOS_INFRACIONAIS: TCheckBox
                  Left = 16
                  Top = 248
                  Width = 393
                  Height = 17
                  Hint = '� MEMBRO FAMILIAR AUTOR DE ATOS INFRACIONAIS'
                  Caption = 'Membro familiar autor de atos infracionais'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 14
                end
                object chkSIT_RISCO_FICAM_SOZINHAS: TCheckBox
                  Left = 16
                  Top = 264
                  Width = 393
                  Height = 17
                  Hint = 'CRIAN�A DE 06 A 12 ANSO QUE FICAM SOZINHAS'
                  Caption = 'Crian�as de 06 a 12 anos que ficam sozinhas'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 15
                end
              end
            end
            object TabSheet20: TTabSheet
              Caption = '[Situa��o de Moradia]'
              ImageIndex = 6
              object gpbNATUREZA_HABITACAO: TGroupBox
                Left = 5
                Top = 5
                Width = 312
                Height = 50
                Caption = '[Natureza da Habita��o]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                object cmbNATUREZA_HABITACAO: TComboBox
                  Left = 10
                  Top = 17
                  Width = 295
                  Height = 19
                  Hint = 'SELECIONE A NATUREZA DA HABITA��O'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbCOMPROVA_ALUGUEL: TGroupBox
                Left = 319
                Top = 5
                Width = 213
                Height = 50
                Caption = '[Se alugada/financiada informar valor R$ ]'
                TabOrder = 1
                object edtVALOR_ALUGUEL_INFORMADO: TEdit
                  Left = 11
                  Top = 17
                  Width = 77
                  Height = 21
                  Hint = 'INFORME O VALOR DO ALUGUEL OU FINANCIAMENTO'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object chkCOMPROVOU_ALUGUEL: TCheckBox
                  Left = 95
                  Top = 19
                  Width = 89
                  Height = 17
                  Hint = 'CLIQUE SE COMPROVOU VALOR DO ALUGUEL/FINANCIAMENTO'
                  Caption = 'Comprovado'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
              end
              object gpbOBSERVACAO: TGroupBox
                Left = 535
                Top = 5
                Width = 351
                Height = 50
                Caption = '[Observa��o]'
                TabOrder = 2
                object edtOBSERVACOES: TEdit
                  Left = 9
                  Top = 17
                  Width = 333
                  Height = 21
                  Hint = 'OBSERVA��ES'
                  CharCase = ecUpperCase
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbTIPO_HABITACAO: TGroupBox
                Left = 5
                Top = 57
                Width = 449
                Height = 52
                Caption = '[Tipo da Habita��o]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                object cmbTIPO_HABITACAO: TComboBox
                  Left = 10
                  Top = 17
                  Width = 432
                  Height = 19
                  Hint = 'SELECIONE O TIPO DA HABITA��O'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbINFRAESTRUTURA: TGroupBox
                Left = 457
                Top = 57
                Width = 429
                Height = 52
                Caption = '[Infraestrutura]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                object cmbINFRAESTRUTURA: TComboBox
                  Left = 10
                  Top = 17
                  Width = 411
                  Height = 19
                  Hint = 'SELECIONE O TIPO DE INFRAESTRUTURA'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbCONDICOES_HABITABILIDADE: TGroupBox
                Left = 466
                Top = 111
                Width = 421
                Height = 52
                Caption = '[Condi��es de Habitabilidade]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                object cmbCONDICOES_HABITABILIDADE: TComboBox
                  Left = 10
                  Top = 17
                  Width = 406
                  Height = 19
                  Hint = 'SELECIONE AS CONDI��ES DE HABITABILIDADE'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbINSTALACOES_SANITARIAS: TGroupBox
                Left = 149
                Top = 111
                Width = 315
                Height = 52
                Caption = '[Instala��es Sanit�rias]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                object cmbINSTALACOES_SANITARIAS: TComboBox
                  Left = 10
                  Top = 17
                  Width = 298
                  Height = 19
                  Hint = 'SELECIONE O TIPO DE INSTALA��ES SANIT�RIAS'
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
              end
              object gpbHABITANTES_COMODOS: TGroupBox
                Left = 4
                Top = 111
                Width = 143
                Height = 52
                Caption = '[Habitantes X C�modo]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 5
                object Label3: TLabel
                  Left = 55
                  Top = 19
                  Width = 10
                  Height = 18
                  Caption = 'X'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -16
                  Font.Name = 'Arial Rounded MT Bold'
                  Font.Style = []
                  ParentFont = False
                end
                object edtQTD_HABITANTES: TEdit
                  Left = 15
                  Top = 17
                  Width = 36
                  Height = 21
                  Hint = 'INFORME A QUANTIDADE DE HABITANTES NA RESID�NCIA'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object edtQTD_COMODOS: TEdit
                  Left = 72
                  Top = 17
                  Width = 36
                  Height = 21
                  Hint = 'INFORME A QUANTIDADE DE C�MODOS NA RESID�NCIA'
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
              end
            end
          end
          object PanelX: TPanel
            Left = 7
            Top = 379
            Width = 896
            Height = 51
            BevelInner = bvLowered
            TabOrder = 1
            object btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 472
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA PREENCHER NOVA INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDF4FAF3EBF7E9E3F4E1E3F4E1ECF7EAF4FAF3FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEF7EDB6DFB0A5DA9DB8E4B0D1
                EECBD1EECCBAE4B2A6DB9EB6DFB1EEF7EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D9EED67DC47396D58EBEEBB8D4F5D0E2FAE0E3FAE1D5F6D1C0ECBA98D68F7DC4
                73D9EED6FFFFFFFFFFFFFFFFFFE2F2E07BC47197DA8F9BE393ABEBA5C0EDBBCC
                F0C9CDF0C9C1EDBCACEBA69CE49497DA8F7BC471E2F2E0FFFFFFFBFDFB8FCD87
                90D68789D87F90DE879BE493FBFDFBFFFFFFFFFFFFFEFEFE9EE49791DF8889D9
                8091D6888FCD87FBFDFBDBEFD884CC7B7DCE7385D57A8ADA828FDE87F9FCF9FF
                FFFFFFFFFFFEFEFE92DD898BDA8285D57B7ECF7384CC7CDBEFD8B7DFB177C66C
                78CA6D7FD07485D57B8CD983F8FBF7FFFFFFFFFFFFFEFEFE8DD98486D57B7FD0
                7579CA6E78C66CB7DFB1A9D8A16CBE5F9BD493DEF0DCE0F1DEE1F1DFFDFEFDFF
                FFFFFFFFFFFEFEFEE1F1DFE0F1DEDEF0DCA2D69A6CBF5FA9D8A1A2D39873C065
                ABDAA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFB4DDAD73C065A2D499A3D49976BF68A5D59CFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADD9A577BF69A4D49AB9DDB172BC62
                98CE8EF3F9F1F3F9F2F3F9F2FDFEFDFFFFFFFFFFFFFEFEFEF3F9F2F3F9F2F3F9
                F1A0D29672BC63BADDB1EBF3E96AB75873BC6477BE687AC16C7CC26FF0F7EEFF
                FFFFFFFFFFF8FBF87CC26F7AC16C77BF6873BC646AB758EBF4E9FFFFFFA0D194
                6EB85C73BB6275BD6677BE68EEF6ECFFFFFFFFFFFFF6FAF677BE6875BD6673BB
                626EB85CA1D194FFFFFFFFFFFFFAFCFA81C1706DB75A71BA5F73BB62EAF4E8FF
                FFFFFFFFFFF3F8F273BB6271BA5F6EB75A82C171FAFCFAFFFFFFFFFFFFFFFFFF
                FAFCFA9DCE8F66B3506FB85B70B85D71B85D71B85D70B85D6FB85B66B3509ECE
                90FAFCFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E2A2D1947EBF6A6E
                B7596EB7597EBF6AA3D194E6F0E2FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick
            end
            object btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 536
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA GRAVAR OS DADOS DA FICHA EM TELA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDFCF7F2FAF1E7F8ECDEF8ECDEFAF1E7FCF7F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF2EBECCAA7EBC18CF0D0A1F6
                E1BCF6E1BDF0D1A3EBC28FEDCBA8FAF2EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5E3D2E0A160EAB775F6D8A3FBE8BEFDF1D3FDF2D4FBE9BFF6D9A5EAB877E0A1
                61F5E3D2FFFFFFFFFFFFFFFFFFF7EADDE0A05CEBB973F0C371F5D186F9DFA2FC
                E7B6FCE7B7FADFA4F5D288F0C373EBBA74E0A05CF7EADDFFFFFFFDFCFBE4AE77
                E6B26CE7B15DECBA64F0C77FF6DFB8F7D794F8D895F6D288F2C674EDBA64E8B1
                5DE6B26CE4AE77FEFCFBF5E4D4E1A561DFA252E4AB59E9B970FBF3E8FFFFFFF6
                E3C3F1C574F0C16DEDBA64E9B45FE4AC59DFA253E1A661F5E4D4ECCAA7D8964D
                DB9B4DE3AE6AFAF3E9FFFFFFFFFFFFFFFFFFF5E0C0EAB867E8B462E5AC5AE0A4
                54DC9C4DD8974DECCAA7E4BB93D28B3EDDA464FAF4ECFFFFFFFDFBF7F6E6CFFF
                FFFFFFFFFFF4E1C7E6B26BE3AD66DFA45BD8974BD38B3EE5BC93E0B285D28D47
                E0AE78FCF8F4FCF9F5E7BD88E3AD68F2DABBFFFFFFFFFFFFF4E1CADFA764DCA0
                5DD89A57D28D46E0B286DFB187D28D4BD59352DFAC76E1B07BDCA260DEA562DE
                A663EED3B3FFFFFFFFFFFFF3E1CED99C5CD59452D28D4BE0B287E6C3A2CE8744
                D18D4CD49251D69656D89A59D99C5CDA9E5DDA9E5DEBCBA9FEFEFEFFFFFFF3E2
                D0D39253CE8745E6C3A2F5EDE5C97D39CE8746D08B4BD28F4FD49253D59455D5
                9556D59556D59455E7C29EFEFEFEFFFFFFE4BE9ACA7D39F6EDE6FFFFFFDCAC81
                CB803ECD8545CF8948D08B4BD18D4DD18E4ED18E4ED18D4ED08B4BE2B892EACC
                B1CB8140DCAC81FFFFFFFFFFFFFCFAF9D18F58CA7E3CCC8242CD8544CE8646CE
                8747CE8747CE8646CD8544CC8342CA7E3DD19058FCFBF9FFFFFFFFFFFFFFFFFF
                FCFAF9DAA77CC77632CA7F3ECB8040CB8040CB8040CB8040CA7F3EC77632DBA8
                7DFCFAF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E7DEDCAB83CF8B52C9
                7D3DC97D3DCF8B52DCAB83F3E8DEFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick
            end
            object btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 577
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA HABILITAR A EDI��O DOS DADOS EM TELA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFDFDFBF8F1FAF4E8F7F1E0F7F1E0FAF5E8FCF9F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4EAEAD7ABE9D096EDDCA9F5
                E9C5F5EAC6EEDDABE9D297EBD8ACF9F4EAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                F3E9D3DBB76AE7C982F5E2ACFBEDC5FEF4D9FEF4DAFBEEC7F5E2AEE7CA84DBB7
                6BF3E9D3FFFEFEFFFFFFFFFFFFF6EEDDDBB566E9CA7EF0D07CF5DB91FAE5ACFC
                ECBEFCECBFFAE6AEF6DB93F1D17DE9CA7EDBB566F6EEDDFFFFFFFDFCFAE0C17F
                E6C474E8C266EDCA6FF2D27EF6DB92F8E09FF8E1A0F6DC94F2D380EDCA6FE8C3
                67E6C474E0C17FFDFCFAF4EBD7DFBB6AE0BA60E9CD8BECD190EED594F0D89BF1
                DBA0F1DBA0F0D89CEED595ECD291E9CE8CE1BB63DFBB6AF4EBD7EBD7ADD9B052
                E2C072FEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEE3C47AD9B053EBD7ADE4CD97D4A743D9B051EFDDB5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF1E1BDD9B051D4A844E5CD98E1C689D4AA4C
                D9B35CDDB966F8F2E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4E8DEBA
                69DAB35CD4AA4BE1C689E1C68AD3AB4FD6AF56D9B35CE1C27DFEFDFAFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFCE3C583D9B35CD6AF57D3AB50E1C689E7D3A5D0A748
                D3AB50D5AE56D7B15AEAD6A9FFFFFFFFFFFFFFFFFFFFFFFFECDAB1D7B15BD5AE
                56D3AB50D0A749E7D4A5F5F1E7CBA13CD0A849D2AA4FD4AD53D6AF58F5ECD8FF
                FFFFFFFFFFF7EFDFD6B05BD4AD54D2AA4FD0A84ACCA13CF6F2E7FFFFFFDEC382
                CDA440CFA747D0A94CD2AA4FD8B769FCFBF7FDFCF9DABA6FD2AA4FD1A94CCFA7
                48CDA441DEC483FFFFFFFFFFFFFCFBF9D3B059CCA33ECEA644CFA747CFA849E0
                C688E1C98ECFA849CFA747CEA644CCA33FD3B05AFCFBF9FFFFFFFFFFFFFFFFFF
                FCFBF9DCC17DC99E33CDA440CDA541CDA542CDA542CDA541CDA440C99F33DCC1
                7EFCFBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDDEDEC483D1AD52CB
                A43ECBA43ED1AE52DEC584F3EDDFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick
            end
            object btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 650
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CANCELAR A OPERA��O ATUAL'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFDFEF0F0F8E3E3F4D8D8F0D9D9F1E4E4F5F0F0F9FDFDFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F69B9BDD7777DB8585E4A2
                A2EFA3A3EF8888E57878DC9C9CDEE8E8F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                CDCDEC4A4AC65757D68585E9A6A6F2C3C3F8C4C4F8A9A9F38787E95959D64B4B
                C7CCCCECFFFFFFFFFFFFFFFFFFD8D8F04242C65050D44343D95D5DE38282EC9C
                9CF29D9DF28484ED5F5FE34545DA5050D44242C6D8D8F0FFFFFFF9F9FC6363CD
                4646CC2D2DCB8383E0B9B9EE6262E37070E87171E86363E3BABAEE8282E02D2D
                CB4646CC6363CDF9F9FCCDCDEC3F3FC52424BD8686DCFEFEFEFFFFFFCACAF24E
                4EDC4F4FDCCACAF2FFFFFFFEFEFE8686DD2424BE4040C5CDCDED9898DB2525B5
                4141C2FCFCFEFFFFFFFFFFFFFFFFFFD0D0F2D0D0F3FFFFFFFFFFFFFFFFFFFCFC
                FE4141C22525B59898DB7F7FCE1717AC2323B57777D3F9F9FDFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF9F9FD7676D32323B51818AC8080CF7474C72727AD
                3636B63939BB6C6CCFF3F3FBFFFFFFFFFFFFFFFFFFFFFFFFF3F3FB6C6CCF3939
                BC3636B72727AD7575C77979C73232AD3535B23838B65858C5EAEAF8FFFFFFFF
                FFFFFFFFFFFFFFFFE9E9F85858C53838B63535B23232AE7979C79A9AD43131A9
                3434AE5D5DC2EEEEF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEF95D5D
                C23535AE3131AA9A9AD4E4E4F12A2AA33C3CAEEFEFF9FFFFFFFFFFFFFFFFFFD6
                D6EFD6D6EFFFFFFFFFFFFFFFFFFFEFEFF93C3CAE2A2AA3E4E4F2FFFFFF7B7BC4
                3232A66E6EC1F4F4FAFFFFFFC8C8E84444B24444B2C8C8E8FFFFFFF4F4FA6E6E
                C13232A67C7CC5FFFFFFFFFFFFF9F9FB5353B13434A55D5DB8A5A5D83E3EAC39
                39AA3939AA3E3EACA5A5D85D5DB83434A55353B1F9F9FBFFFFFFFFFFFFFFFFFF
                F9F9FB7B7BC22E2EA03939A63939A63A3AA73A3AA73939A63939A62E2EA07B7B
                C2F9F9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEED8383C55151AE3C
                3CA53C3CA55151AE8383C5DEDEEDFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick
            end
            object btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 722
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE PESQUISA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFCFBFAFAF8F5F8F4EFF8F4EFFAF8F5FCFBFAFFFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F7ECE3D6E9DECDEEE7D9F5
                F0E8F6F0E8EFE7DAEADFCDECE3D6FBF9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5F0EADDCBB3E6D9C3F1E7D9F5EEE3F9F4ECF9F4ECF6EFE4F1E8DAE7DAC5DDCB
                B3F5F1EAFFFFFFFFFFFFFFFFFFF8F4EFDDCBB2E7D8C1E8D9C0ECDFCAF1E7D7F4
                ECDFF4ECE0F1E7D7ECDFCBE8D9C0E7D8C2DDCBB2F8F4EFFFFFFFFEFDFDE1D3BE
                E5D6BEE3D3B9EBE1D1EDE4D5EDE3D4EFE3D1EFE3D1EDE4D5EDE4D6EBE1D1E3D3
                B9E5D6BEE1D3BEFEFDFDF5F0EAE1D1B8E0CFB2E5D7C0FFFFFFFFFFFFF5F0E8E9
                DAC1E9DAC1F5F0E8FFFFFFFFFFFFE5D7C0E0CFB3E1D1B8F5F0EAECE2D5DECDB1
                DECDB0E4D5BEFFFFFFFFFFFFF5F0E7E6D7BDE6D7BDF5F0E7FFFFFFFFFFFFE4D6
                BEDFCDB1DECDB2ECE2D5E9DFCFDBC9ACDECDB1E4D7C1FFFFFFFFFFFFFBF9F7F3
                EEE7F3EEE7FBF9F7FFFFFFFFFFFFE4D7C1DECDB1DBC9ACE9DFCFE9DFCFDDCCB1
                E0D0B7E5D8C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5D8
                C3E0D0B7DDCCB1E9DFCFEAE1D1DECEB4E2D5C1F2EDE5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDE5E2D5C1DECEB4EAE1D1EFE9DDDDCDB3
                DECEB5ECE3D5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEECE3
                D5DECEB5DDCDB3EFE8DDF8F6F3DBCAB1DDCEB4DECFB5E7DDCCFDFCFBFFFFFFFF
                FFFFFFFFFFFFFFFFFDFCFBE7DDCCDECFB5DDCEB4DBCAB1F9F7F4FFFFFFE8DECE
                DCCCB3DDCEB5DDCEB6E4D8C4FBF9F7FFFFFFFFFFFFFBF9F7E4D8C4DDCEB6DDCE
                B5DCCCB3E8DECEFFFFFFFFFFFFFCFCFCE1D3BFDCCDB4DDCEB5DDCEB6E1D3BEF8
                F5F1F8F5F1E1D3BEDDCEB6DDCEB5DCCDB4E1D3BFFDFCFCFFFFFFFFFFFFFFFFFF
                FCFCFCE7DDCDDBCAB1DCCDB5DDCEB5DED0B8DED0B8DDCEB5DCCDB5DBCAB1E7DD
                CDFDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F3EFE9DFD0E0D3BEDD
                CEB6DDCEB6E0D3BEE9DFD1F6F3F0FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick
            end
            object btnCHAMAIMPRESSAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 788
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE OP��ES DE IMPRESS�O'
              Flat = True
              ParentShowHint = False
              ShowHint = True
            end
            object btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton
              Left = 850
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA SAIR DA TELA DE INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFEFEF1F9F7E5F6F2DBF2ECDBF2ECE6F6F2F2F9F7FDFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F3A3DDCD8AD9C59FE3D3BB
                EFE4BCEFE4A1E4D48BDAC6A4DDCDEAF6F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D0ECE45DC4A774D8BEA2EFDFBDF9EED3FDF6D4FDF6BFFAEFA4F0E076D8C05EC4
                A7D0ECE4FFFFFFFFFFFFFFFFFFDBF1EA59C5A673DEC571EBD286F2DDA2F8E8B6
                FBEFB7FBEFA4F9E988F3DE73ECD373DEC559C5A6DBF1EAFFFFFFFBFDFC74CCB3
                6BDBBF5DE0C264E6CB73EDD587F3DE95F6E39FF3E2CFF4EB8EEBD864E7CC5DE0
                C36BDBC074CCB3FBFDFCD2EDE660D0B252D4B559DBBD5FE2C564E7CB6DEBD174
                EED6A6F2E2FFFFFFF7FCFB81E4CD59DCBD53D5B560D0B2D2EDE6A4DECE4CCBAA
                4DCFAF54D6B65ADCBD62E1C567E5CA6AE7CC6CE7CDCBF5ECFFFFFFF7FCFB7ADC
                C44DD0AF4DCBAAA4DECE91D7C43EC29F5DCFB182DCC58BE0CB8DE2CE8FE4D08F
                E5D18FE5D190E4D1E7F8F3FFFFFFF6FCFA72D5BB3EC3A091D8C485D4BE46C2A1
                ECF8F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF6FCFA4DC4A485D5BF86D4BF4BC1A19FDECDC7EDE3C9EEE4CAEFE5CBEFE6CB
                F0E6CBF0E6CBEFE6EAF8F4FFFFFFFFFFFFB7E6DA4BC1A187D4BFA2DDCD44BD9B
                4CC1A151C5A556C8A959CBAC5CCCAE5DCDAF5DCDAF87D9C3FAFDFCFFFFFFB8E6
                D94DC1A145BD9CA2DDCDE5F3EF39B79346BC9B4BBF9F4FC2A253C4A555C6A756
                C7A879D2BAFAFDFCFFFFFFB7E5D94CC0A046BD9C39B794E6F4F0FFFFFF81D0BA
                3EB89545BB9A48BD9D4BBF9F4DC0A14EC1A183D3BDFEFEFEB5E4D749BE9D45BB
                9A3EB89681D0BAFFFFFFFFFFFFF9FCFB58C0A23CB79442B99744BA9946BB9A47
                BC9B47BC9B52C0A145BA9942B9983DB79458C1A3F9FCFBFFFFFFFFFFFFFFFFFF
                F9FCFB7CCDB732B28D3EB79540B79640B89640B89640B8963EB79532B28E7DCE
                B7F9FCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEEFEB83D0BA52BD9F3D
                B5943DB59452BD9F83D0BADEF0EBFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick
            end
            object Label6: TLabel
              Left = 12
              Top = 20
              Width = 262
              Height = 13
              Caption = 'Os campos marcados em azul s�o obrigat�rios'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = '[Composi��o Familiar]'
          ImageIndex = 2
          object pgcAbas_ComposicaoFamiliar: TPageControl
            Left = 5
            Top = 47
            Width = 902
            Height = 321
            ActivePage = TabSheet11
            TabOrder = 0
            object TabSheet11: TTabSheet
              Caption = '[Dados Pessoais do Familiar]'
              object GroupBox42: TGroupBox
                Left = 5
                Top = 1
                Width = 535
                Height = 50
                Caption = '[Nome completo]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                object edtNOME_COMPLETO_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 419
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  OnExit = edtNOME_COMPLETO_FAMILIARExit
                end
                object chkFAMILIAR_RESPONSAVEL: TCheckBox
                  Left = 436
                  Top = 19
                  Width = 90
                  Height = 17
                  Caption = 'Respons�vel'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                end
              end
              object GroupBox43: TGroupBox
                Left = 541
                Top = 1
                Width = 140
                Height = 50
                Caption = '[Sexo]'
                TabOrder = 1
                object cmbSEXO_FAMILIAR: TComboBox
                  Left = 8
                  Top = 17
                  Width = 125
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 0
                  Items.Strings = (
                    'Masculino'
                    'Feminino')
                end
              end
              object GroupBox44: TGroupBox
                Left = 683
                Top = 1
                Width = 204
                Height = 50
                Caption = '[Estado Civil]'
                TabOrder = 2
                object cmbESTADO_CIVIL_FAMILIAR: TComboBox
                  Left = 8
                  Top = 17
                  Width = 187
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 0
                end
              end
              object GroupBox46: TGroupBox
                Left = 5
                Top = 52
                Width = 199
                Height = 50
                Caption = '[Grau de Parentesco com o inscrito]'
                TabOrder = 3
                object cmbGRAU_PARENTESCO_FAMILIAR: TComboBox
                  Left = 8
                  Top = 17
                  Width = 185
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 0
                end
              end
              object GroupBox45: TGroupBox
                Left = 206
                Top = 52
                Width = 256
                Height = 50
                Caption = '[Data de Nascimento]'
                TabOrder = 4
                object lblIDADE_Familiar: TLabel
                  Left = 117
                  Top = 19
                  Width = 133
                  Height = 13
                  AutoSize = False
                  Caption = 'Idade: 00 anos e 00 meses'
                end
                object mskDATA_NASCIMENTO_FAMILIAR: TMaskEdit
                  Left = 9
                  Top = 16
                  Width = 102
                  Height = 21
                  EditMask = '99/99/9999'
                  MaxLength = 10
                  TabOrder = 0
                  Text = '  /  /    '
                  OnExit = mskDATA_NASCIMENTO_FAMILIARExit
                end
              end
              object GroupBox47: TGroupBox
                Left = 465
                Top = 52
                Width = 231
                Height = 50
                Caption = '[Local de Trabalho]'
                TabOrder = 5
                object edtLOCAL_TRABALHO_FAMILIAR: TEdit
                  Left = 10
                  Top = 16
                  Width = 217
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  OnExit = edtLOCAL_TRABALHO_FAMILIARExit
                end
              end
              object GroupBox48: TGroupBox
                Left = 698
                Top = 52
                Width = 191
                Height = 50
                Caption = '[Ocupa��o]'
                TabOrder = 6
                object edtOCUPACAO_FAMILIAR: TEdit
                  Left = 10
                  Top = 16
                  Width = 173
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  OnExit = edtOCUPACAO_FAMILIARExit
                end
              end
              object GroupBox49: TGroupBox
                Left = 6
                Top = 104
                Width = 113
                Height = 50
                Caption = '[Telefone]'
                TabOrder = 7
                object mskTELEFONE_FAMILIAR: TMaskEdit
                  Left = 9
                  Top = 16
                  Width = 95
                  Height = 21
                  EditMask = '(99)99999-9999'
                  MaxLength = 14
                  TabOrder = 0
                  Text = '(  )     -    '
                end
              end
              object GroupBox50: TGroupBox
                Left = 213
                Top = 104
                Width = 151
                Height = 50
                Caption = '[Outra fonte de renda]'
                TabOrder = 9
                object edtOUTRAS_RENDAS_FAMILIAR: TEdit
                  Left = 10
                  Top = 16
                  Width = 135
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  OnExit = edtOUTRAS_RENDAS_FAMILIARExit
                end
              end
              object GroupBox51: TGroupBox
                Left = 120
                Top = 104
                Width = 90
                Height = 50
                Caption = '[Renda - R$]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 8
                object edtRENDA_MENSAL_FAMILIAR: TEdit
                  Left = 7
                  Top = 16
                  Width = 73
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                end
              end
              object GroupBox53: TGroupBox
                Left = 371
                Top = 104
                Width = 280
                Height = 50
                Caption = '[N�vel de Escolaridade]'
                TabOrder = 10
                object cmbNIVEL_ESCOLARIDADE_FAMILIAR: TComboBox
                  Left = 8
                  Top = 17
                  Width = 265
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 0
                end
              end
              object GroupBox54: TGroupBox
                Left = 658
                Top = 104
                Width = 93
                Height = 50
                Caption = '[Possui guarda?]'
                TabOrder = 11
                object cmbFAMILIAR_POSSUI_GUARDA: TComboBox
                  Left = 8
                  Top = 17
                  Width = 81
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 0
                  Items.Strings = (
                    'SIM'
                    'N�O')
                end
              end
              object GroupBox55: TGroupBox
                Left = 754
                Top = 104
                Width = 135
                Height = 50
                Caption = '[Mora com a fam�lia?]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 12
                object cmbFAMILIAR_MORA_FAMILIA: TComboBox
                  Left = 12
                  Top = 17
                  Width = 98
                  Height = 19
                  Style = csOwnerDrawFixed
                  ItemHeight = 13
                  TabOrder = 0
                  Items.Strings = (
                    'SIM'
                    'N�O')
                end
              end
              object Panel5: TPanel
                Left = 5
                Top = 156
                Width = 884
                Height = 132
                BevelInner = bvLowered
                TabOrder = 13
                object dbgCOMPOSICAO_FAMILIAR: TDBGrid
                  Left = 7
                  Top = 6
                  Width = 871
                  Height = 120
                  DataSource = dsCOMPOSICAO_FAMILIAR
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnCellClick = dbgCOMPOSICAO_FAMILIARCellClick
                  OnKeyDown = dbgCOMPOSICAO_FAMILIARKeyDown
                  OnKeyUp = dbgCOMPOSICAO_FAMILIARKeyUp
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'vRESPONSAVEL'
                      Title.Alignment = taCenter
                      Title.Caption = '[� o Respons�vel?]'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'dsc_Nome_fam'
                      Title.Caption = '[Nome do Familiar]'
                      Width = 335
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'dsc_parentesco'
                      Title.Caption = '[Parentesco]'
                      Width = 135
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'dsc_escolaridade'
                      Title.Caption = '[Escolaridade]'
                      Width = 180
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'num_rendamensal_fam'
                      Title.Alignment = taCenter
                      Title.Caption = '[Renda Mensal R$]'
                      Width = 100
                      Visible = True
                    end>
                end
              end
            end
            object TabSheet12: TTabSheet
              Caption = '[Documenta��o do Familiar]'
              ImageIndex = 1
              object GroupBox65: TGroupBox
                Left = 6
                Top = 10
                Width = 110
                Height = 50
                Caption = '[CPF]'
                TabOrder = 0
                object mskNUMERO_CPF_FAMILIAR: TMaskEdit
                  Left = 8
                  Top = 16
                  Width = 96
                  Height = 21
                  TabOrder = 0
                end
              end
              object GroupBox66: TGroupBox
                Left = 537
                Top = 133
                Width = 353
                Height = 50
                Caption = '[N�mero Cart�o de Passe Escolar]'
                TabOrder = 1
                object edtNUMERO_CARTAO_PASSE_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 312
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                end
              end
              object GroupBox67: TGroupBox
                Left = 118
                Top = 10
                Width = 510
                Height = 50
                Caption = 
                  '[RG]                            [Data Emiss�o]         [�rg�o Em' +
                  'issor]'
                TabOrder = 2
                object edtNUMERO_RG_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 103
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                end
                object mskDATA_EMISSAO_RG_FAMILIAR: TMaskEdit
                  Left = 114
                  Top = 17
                  Width = 96
                  Height = 21
                  EditMask = '99/99/9999'
                  MaxLength = 10
                  TabOrder = 1
                  Text = '  /  /    '
                end
                object cmbORGAO_EMISSOR_RG_FAMILIAR: TComboBox
                  Left = 211
                  Top = 17
                  Width = 286
                  Height = 21
                  ItemHeight = 13
                  TabOrder = 2
                end
              end
              object GroupBox68: TGroupBox
                Left = 5
                Top = 133
                Width = 175
                Height = 50
                Caption = '[N�mero Cart�o SUS]'
                TabOrder = 3
                object btnACESSO_CONSULTASUS_FAMILIAR: TSpeedButton
                  Left = 146
                  Top = 17
                  Width = 23
                  Height = 22
                  Hint = 'CLIQUE PARA TER ACESSO � CONSULT DO N�MERO DO CART�O DO SUS'
                  Flat = True
                  Glyph.Data = {
                    36050000424D360500000000000036040000280000000E000000100000000100
                    08000000000000010000CA0E0000C30E00000001000000000000000000003300
                    00006600000099000000CC000000FF0000000033000033330000663300009933
                    0000CC330000FF33000000660000336600006666000099660000CC660000FF66
                    000000990000339900006699000099990000CC990000FF99000000CC000033CC
                    000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
                    0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
                    330000333300333333006633330099333300CC333300FF333300006633003366
                    33006666330099663300CC663300FF6633000099330033993300669933009999
                    3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
                    330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
                    66006600660099006600CC006600FF0066000033660033336600663366009933
                    6600CC336600FF33660000666600336666006666660099666600CC666600FF66
                    660000996600339966006699660099996600CC996600FF99660000CC660033CC
                    660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
                    6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
                    990000339900333399006633990099339900CC339900FF339900006699003366
                    99006666990099669900CC669900FF6699000099990033999900669999009999
                    9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
                    990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
                    CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
                    CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
                    CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
                    CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
                    CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
                    FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
                    FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
                    FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
                    FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000ACACACACACAC
                    ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
                    00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
                    02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
                    5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
                    00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
                    ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
                    D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
                    D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
                  ParentShowHint = False
                  ShowHint = True
                  Visible = False
                  OnClick = btnACESSO_CONSULTASUS_FAMILIARClick
                end
                object edtNUMERO_CARTAO_SUS_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 134
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                end
              end
              object GroupBox69: TGroupBox
                Left = 182
                Top = 133
                Width = 175
                Height = 50
                Caption = '[N�mero CRA]'
                TabOrder = 4
                object edtNUMERO_CRA_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 157
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                end
              end
              object GroupBox70: TGroupBox
                Left = 359
                Top = 133
                Width = 175
                Height = 50
                Caption = '[N�mero NIS]'
                TabOrder = 5
                object edtNUMERO_NIS_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 157
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                end
              end
              object GroupBox71: TGroupBox
                Left = 5
                Top = 69
                Width = 525
                Height = 50
                Caption = '[Naturalidade]'
                TabOrder = 6
                object cmbNATURALIDADE_FAMILIAR: TComboBox
                  Left = 8
                  Top = 17
                  Width = 509
                  Height = 22
                  Style = csOwnerDrawFixed
                  ItemHeight = 16
                  TabOrder = 0
                end
              end
              object GroupBox72: TGroupBox
                Left = 532
                Top = 68
                Width = 357
                Height = 50
                Caption = '[Nacionalidade]'
                TabOrder = 7
                object cmbNACIONALIDADE_FAMILIAR: TComboBox
                  Left = 8
                  Top = 17
                  Width = 340
                  Height = 22
                  Style = csOwnerDrawVariable
                  ItemHeight = 16
                  TabOrder = 0
                end
              end
              object GroupBox116: TGroupBox
                Left = 630
                Top = 10
                Width = 259
                Height = 50
                Caption = '[Certid�o de Nascimento]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                object edtCERTIDAO_NASCIMENTO_FAMILIAR: TEdit
                  Left = 9
                  Top = 18
                  Width = 242
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  OnExit = edtCERTIDAO_NASCIMENTO_FAMILIARExit
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = '[Situa��o de Sa�de do Familiar]'
              ImageIndex = 2
              object GroupBox56: TGroupBox
                Left = 5
                Top = 6
                Width = 100
                Height = 50
                Caption = '[Invalidez]'
                TabOrder = 0
                object chkINVALIDEZ_TEMPORARIA_FAMILIAR: TCheckBox
                  Left = 9
                  Top = 13
                  Width = 76
                  Height = 17
                  Caption = 'Tempor�ria'
                  TabOrder = 0
                end
                object chkINVALIDEZ_PERMANENTE_FAMILIAR: TCheckBox
                  Left = 9
                  Top = 28
                  Width = 76
                  Height = 16
                  Caption = 'Permanente'
                  TabOrder = 1
                end
              end
              object GroupBox57: TGroupBox
                Left = 109
                Top = 5
                Width = 174
                Height = 50
                TabOrder = 1
                object chkINCAPACIDADE_TRABALHO_FAMILIAR: TCheckBox
                  Left = 9
                  Top = 19
                  Width = 163
                  Height = 17
                  Caption = 'Incapacidade para o trabalho'
                  TabOrder = 0
                end
              end
              object GroupBox58: TGroupBox
                Left = 287
                Top = 5
                Width = 225
                Height = 50
                TabOrder = 2
                object chkPORTADOR_DOENCA_INFECTO_FAMILIAR: TCheckBox
                  Left = 9
                  Top = 19
                  Width = 210
                  Height = 17
                  Caption = 'Portador de doen�a infecto contagiosa'
                  TabOrder = 0
                end
              end
              object GroupBox59: TGroupBox
                Left = 515
                Top = 5
                Width = 212
                Height = 50
                TabOrder = 3
                object chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE: TCheckBox
                  Left = 9
                  Top = 19
                  Width = 199
                  Height = 17
                  Caption = 'N�o registrou a crian�a/adolescente'
                  TabOrder = 0
                end
              end
              object GroupBox60: TGroupBox
                Left = 730
                Top = 5
                Width = 74
                Height = 50
                TabOrder = 4
                object chkFAMILIAR_FALECIDO: TCheckBox
                  Left = 6
                  Top = 19
                  Width = 62
                  Height = 17
                  Caption = 'Falecido'
                  TabOrder = 0
                end
              end
              object GroupBox61: TGroupBox
                Left = 807
                Top = 5
                Width = 74
                Height = 50
                TabOrder = 5
                object chkFAMILIAR_RECLUSO: TCheckBox
                  Left = 6
                  Top = 19
                  Width = 62
                  Height = 17
                  Caption = 'Recluso'
                  TabOrder = 0
                end
              end
              object GroupBox64: TGroupBox
                Left = 5
                Top = 57
                Width = 876
                Height = 50
                Caption = '[SA�DE  - Diagn�stica]'
                TabOrder = 6
                object edtDIAGNOSTICO_SAUDE_FAMILIAR: TEdit
                  Left = 10
                  Top = 18
                  Width = 855
                  Height = 21
                  TabOrder = 0
                  OnExit = edtDIAGNOSTICO_SAUDE_FAMILIARExit
                end
              end
              object GroupBox52: TGroupBox
                Left = 6
                Top = 108
                Width = 288
                Height = 50
                Caption = '[Relacionamento Familiar]'
                TabOrder = 7
                object edtRELACIONAMENTO_FAMILIAR: TEdit
                  Left = 10
                  Top = 16
                  Width = 271
                  Height = 21
                  TabOrder = 0
                  OnExit = edtRELACIONAMENTO_FAMILIARExit
                end
              end
            end
          end
          object Panel6: TPanel
            Left = 5
            Top = 3
            Width = 905
            Height = 41
            BevelInner = bvLowered
            TabOrder = 1
            object Label10: TLabel
              Left = 12
              Top = 11
              Width = 877
              Height = 20
              AutoSize = False
              Caption = 'Nome completo do familiar'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Arial Rounded MT Bold'
              Font.Style = []
              ParentFont = False
            end
          end
          object Panel4: TPanel
            Left = 7
            Top = 379
            Width = 896
            Height = 51
            BevelInner = bvLowered
            TabOrder = 2
            object btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 472
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA PREENCHER NOVA INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDF4FAF3EBF7E9E3F4E1E3F4E1ECF7EAF4FAF3FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEF7EDB6DFB0A5DA9DB8E4B0D1
                EECBD1EECCBAE4B2A6DB9EB6DFB1EEF7EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D9EED67DC47396D58EBEEBB8D4F5D0E2FAE0E3FAE1D5F6D1C0ECBA98D68F7DC4
                73D9EED6FFFFFFFFFFFFFFFFFFE2F2E07BC47197DA8F9BE393ABEBA5C0EDBBCC
                F0C9CDF0C9C1EDBCACEBA69CE49497DA8F7BC471E2F2E0FFFFFFFBFDFB8FCD87
                90D68789D87F90DE879BE493FBFDFBFFFFFFFFFFFFFEFEFE9EE49791DF8889D9
                8091D6888FCD87FBFDFBDBEFD884CC7B7DCE7385D57A8ADA828FDE87F9FCF9FF
                FFFFFFFFFFFEFEFE92DD898BDA8285D57B7ECF7384CC7CDBEFD8B7DFB177C66C
                78CA6D7FD07485D57B8CD983F8FBF7FFFFFFFFFFFFFEFEFE8DD98486D57B7FD0
                7579CA6E78C66CB7DFB1A9D8A16CBE5F9BD493DEF0DCE0F1DEE1F1DFFDFEFDFF
                FFFFFFFFFFFEFEFEE1F1DFE0F1DEDEF0DCA2D69A6CBF5FA9D8A1A2D39873C065
                ABDAA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFB4DDAD73C065A2D499A3D49976BF68A5D59CFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADD9A577BF69A4D49AB9DDB172BC62
                98CE8EF3F9F1F3F9F2F3F9F2FDFEFDFFFFFFFFFFFFFEFEFEF3F9F2F3F9F2F3F9
                F1A0D29672BC63BADDB1EBF3E96AB75873BC6477BE687AC16C7CC26FF0F7EEFF
                FFFFFFFFFFF8FBF87CC26F7AC16C77BF6873BC646AB758EBF4E9FFFFFFA0D194
                6EB85C73BB6275BD6677BE68EEF6ECFFFFFFFFFFFFF6FAF677BE6875BD6673BB
                626EB85CA1D194FFFFFFFFFFFFFAFCFA81C1706DB75A71BA5F73BB62EAF4E8FF
                FFFFFFFFFFF3F8F273BB6271BA5F6EB75A82C171FAFCFAFFFFFFFFFFFFFFFFFF
                FAFCFA9DCE8F66B3506FB85B70B85D71B85D71B85D70B85D6FB85B66B3509ECE
                90FAFCFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E2A2D1947EBF6A6E
                B7596EB7597EBF6AA3D194E6F0E2FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick
            end
            object btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 536
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA GRAVAR OS DADOS DA FICHA EM TELA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDFCF7F2FAF1E7F8ECDEF8ECDEFAF1E7FCF7F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF2EBECCAA7EBC18CF0D0A1F6
                E1BCF6E1BDF0D1A3EBC28FEDCBA8FAF2EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5E3D2E0A160EAB775F6D8A3FBE8BEFDF1D3FDF2D4FBE9BFF6D9A5EAB877E0A1
                61F5E3D2FFFFFFFFFFFFFFFFFFF7EADDE0A05CEBB973F0C371F5D186F9DFA2FC
                E7B6FCE7B7FADFA4F5D288F0C373EBBA74E0A05CF7EADDFFFFFFFDFCFBE4AE77
                E6B26CE7B15DECBA64F0C77FF6DFB8F7D794F8D895F6D288F2C674EDBA64E8B1
                5DE6B26CE4AE77FEFCFBF5E4D4E1A561DFA252E4AB59E9B970FBF3E8FFFFFFF6
                E3C3F1C574F0C16DEDBA64E9B45FE4AC59DFA253E1A661F5E4D4ECCAA7D8964D
                DB9B4DE3AE6AFAF3E9FFFFFFFFFFFFFFFFFFF5E0C0EAB867E8B462E5AC5AE0A4
                54DC9C4DD8974DECCAA7E4BB93D28B3EDDA464FAF4ECFFFFFFFDFBF7F6E6CFFF
                FFFFFFFFFFF4E1C7E6B26BE3AD66DFA45BD8974BD38B3EE5BC93E0B285D28D47
                E0AE78FCF8F4FCF9F5E7BD88E3AD68F2DABBFFFFFFFFFFFFF4E1CADFA764DCA0
                5DD89A57D28D46E0B286DFB187D28D4BD59352DFAC76E1B07BDCA260DEA562DE
                A663EED3B3FFFFFFFFFFFFF3E1CED99C5CD59452D28D4BE0B287E6C3A2CE8744
                D18D4CD49251D69656D89A59D99C5CDA9E5DDA9E5DEBCBA9FEFEFEFFFFFFF3E2
                D0D39253CE8745E6C3A2F5EDE5C97D39CE8746D08B4BD28F4FD49253D59455D5
                9556D59556D59455E7C29EFEFEFEFFFFFFE4BE9ACA7D39F6EDE6FFFFFFDCAC81
                CB803ECD8545CF8948D08B4BD18D4DD18E4ED18E4ED18D4ED08B4BE2B892EACC
                B1CB8140DCAC81FFFFFFFFFFFFFCFAF9D18F58CA7E3CCC8242CD8544CE8646CE
                8747CE8747CE8646CD8544CC8342CA7E3DD19058FCFBF9FFFFFFFFFFFFFFFFFF
                FCFAF9DAA77CC77632CA7F3ECB8040CB8040CB8040CB8040CA7F3EC77632DBA8
                7DFCFAF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E7DEDCAB83CF8B52C9
                7D3DC97D3DCF8B52DCAB83F3E8DEFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick
            end
            object btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 577
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA HABILITAR A EDI��O DOS DADOS EM TELA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFDFDFBF8F1FAF4E8F7F1E0F7F1E0FAF5E8FCF9F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4EAEAD7ABE9D096EDDCA9F5
                E9C5F5EAC6EEDDABE9D297EBD8ACF9F4EAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                F3E9D3DBB76AE7C982F5E2ACFBEDC5FEF4D9FEF4DAFBEEC7F5E2AEE7CA84DBB7
                6BF3E9D3FFFEFEFFFFFFFFFFFFF6EEDDDBB566E9CA7EF0D07CF5DB91FAE5ACFC
                ECBEFCECBFFAE6AEF6DB93F1D17DE9CA7EDBB566F6EEDDFFFFFFFDFCFAE0C17F
                E6C474E8C266EDCA6FF2D27EF6DB92F8E09FF8E1A0F6DC94F2D380EDCA6FE8C3
                67E6C474E0C17FFDFCFAF4EBD7DFBB6AE0BA60E9CD8BECD190EED594F0D89BF1
                DBA0F1DBA0F0D89CEED595ECD291E9CE8CE1BB63DFBB6AF4EBD7EBD7ADD9B052
                E2C072FEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEE3C47AD9B053EBD7ADE4CD97D4A743D9B051EFDDB5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF1E1BDD9B051D4A844E5CD98E1C689D4AA4C
                D9B35CDDB966F8F2E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4E8DEBA
                69DAB35CD4AA4BE1C689E1C68AD3AB4FD6AF56D9B35CE1C27DFEFDFAFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFCE3C583D9B35CD6AF57D3AB50E1C689E7D3A5D0A748
                D3AB50D5AE56D7B15AEAD6A9FFFFFFFFFFFFFFFFFFFFFFFFECDAB1D7B15BD5AE
                56D3AB50D0A749E7D4A5F5F1E7CBA13CD0A849D2AA4FD4AD53D6AF58F5ECD8FF
                FFFFFFFFFFF7EFDFD6B05BD4AD54D2AA4FD0A84ACCA13CF6F2E7FFFFFFDEC382
                CDA440CFA747D0A94CD2AA4FD8B769FCFBF7FDFCF9DABA6FD2AA4FD1A94CCFA7
                48CDA441DEC483FFFFFFFFFFFFFCFBF9D3B059CCA33ECEA644CFA747CFA849E0
                C688E1C98ECFA849CFA747CEA644CCA33FD3B05AFCFBF9FFFFFFFFFFFFFFFFFF
                FCFBF9DCC17DC99E33CDA440CDA541CDA542CDA542CDA541CDA440C99F33DCC1
                7EFCFBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDDEDEC483D1AD52CB
                A43ECBA43ED1AE52DEC584F3EDDFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick
            end
            object btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 650
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CANCELAR A OPERA��O ATUAL'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFDFEF0F0F8E3E3F4D8D8F0D9D9F1E4E4F5F0F0F9FDFDFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F69B9BDD7777DB8585E4A2
                A2EFA3A3EF8888E57878DC9C9CDEE8E8F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                CDCDEC4A4AC65757D68585E9A6A6F2C3C3F8C4C4F8A9A9F38787E95959D64B4B
                C7CCCCECFFFFFFFFFFFFFFFFFFD8D8F04242C65050D44343D95D5DE38282EC9C
                9CF29D9DF28484ED5F5FE34545DA5050D44242C6D8D8F0FFFFFFF9F9FC6363CD
                4646CC2D2DCB8383E0B9B9EE6262E37070E87171E86363E3BABAEE8282E02D2D
                CB4646CC6363CDF9F9FCCDCDEC3F3FC52424BD8686DCFEFEFEFFFFFFCACAF24E
                4EDC4F4FDCCACAF2FFFFFFFEFEFE8686DD2424BE4040C5CDCDED9898DB2525B5
                4141C2FCFCFEFFFFFFFFFFFFFFFFFFD0D0F2D0D0F3FFFFFFFFFFFFFFFFFFFCFC
                FE4141C22525B59898DB7F7FCE1717AC2323B57777D3F9F9FDFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF9F9FD7676D32323B51818AC8080CF7474C72727AD
                3636B63939BB6C6CCFF3F3FBFFFFFFFFFFFFFFFFFFFFFFFFF3F3FB6C6CCF3939
                BC3636B72727AD7575C77979C73232AD3535B23838B65858C5EAEAF8FFFFFFFF
                FFFFFFFFFFFFFFFFE9E9F85858C53838B63535B23232AE7979C79A9AD43131A9
                3434AE5D5DC2EEEEF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEF95D5D
                C23535AE3131AA9A9AD4E4E4F12A2AA33C3CAEEFEFF9FFFFFFFFFFFFFFFFFFD6
                D6EFD6D6EFFFFFFFFFFFFFFFFFFFEFEFF93C3CAE2A2AA3E4E4F2FFFFFF7B7BC4
                3232A66E6EC1F4F4FAFFFFFFC8C8E84444B24444B2C8C8E8FFFFFFF4F4FA6E6E
                C13232A67C7CC5FFFFFFFFFFFFF9F9FB5353B13434A55D5DB8A5A5D83E3EAC39
                39AA3939AA3E3EACA5A5D85D5DB83434A55353B1F9F9FBFFFFFFFFFFFFFFFFFF
                F9F9FB7B7BC22E2EA03939A63939A63A3AA73A3AA73939A63939A62E2EA07B7B
                C2F9F9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEED8383C55151AE3C
                3CA53C3CA55151AE8383C5DEDEEDFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick
            end
            object btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 722
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE PESQUISA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFCFBFAFAF8F5F8F4EFF8F4EFFAF8F5FCFBFAFFFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F7ECE3D6E9DECDEEE7D9F5
                F0E8F6F0E8EFE7DAEADFCDECE3D6FBF9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5F0EADDCBB3E6D9C3F1E7D9F5EEE3F9F4ECF9F4ECF6EFE4F1E8DAE7DAC5DDCB
                B3F5F1EAFFFFFFFFFFFFFFFFFFF8F4EFDDCBB2E7D8C1E8D9C0ECDFCAF1E7D7F4
                ECDFF4ECE0F1E7D7ECDFCBE8D9C0E7D8C2DDCBB2F8F4EFFFFFFFFEFDFDE1D3BE
                E5D6BEE3D3B9EBE1D1EDE4D5EDE3D4EFE3D1EFE3D1EDE4D5EDE4D6EBE1D1E3D3
                B9E5D6BEE1D3BEFEFDFDF5F0EAE1D1B8E0CFB2E5D7C0FFFFFFFFFFFFF5F0E8E9
                DAC1E9DAC1F5F0E8FFFFFFFFFFFFE5D7C0E0CFB3E1D1B8F5F0EAECE2D5DECDB1
                DECDB0E4D5BEFFFFFFFFFFFFF5F0E7E6D7BDE6D7BDF5F0E7FFFFFFFFFFFFE4D6
                BEDFCDB1DECDB2ECE2D5E9DFCFDBC9ACDECDB1E4D7C1FFFFFFFFFFFFFBF9F7F3
                EEE7F3EEE7FBF9F7FFFFFFFFFFFFE4D7C1DECDB1DBC9ACE9DFCFE9DFCFDDCCB1
                E0D0B7E5D8C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5D8
                C3E0D0B7DDCCB1E9DFCFEAE1D1DECEB4E2D5C1F2EDE5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDE5E2D5C1DECEB4EAE1D1EFE9DDDDCDB3
                DECEB5ECE3D5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEECE3
                D5DECEB5DDCDB3EFE8DDF8F6F3DBCAB1DDCEB4DECFB5E7DDCCFDFCFBFFFFFFFF
                FFFFFFFFFFFFFFFFFDFCFBE7DDCCDECFB5DDCEB4DBCAB1F9F7F4FFFFFFE8DECE
                DCCCB3DDCEB5DDCEB6E4D8C4FBF9F7FFFFFFFFFFFFFBF9F7E4D8C4DDCEB6DDCE
                B5DCCCB3E8DECEFFFFFFFFFFFFFCFCFCE1D3BFDCCDB4DDCEB5DDCEB6E1D3BEF8
                F5F1F8F5F1E1D3BEDDCEB6DDCEB5DCCDB4E1D3BFFDFCFCFFFFFFFFFFFFFFFFFF
                FCFCFCE7DDCDDBCAB1DCCDB5DDCEB5DED0B8DED0B8DDCEB5DCCDB5DBCAB1E7DD
                CDFDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F3EFE9DFD0E0D3BEDD
                CEB6DDCEB6E0D3BEE9DFD1F6F3F0FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick
            end
            object btnCHAMAIMPRESSAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 788
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE OP��ES DE IMPRESS�O'
              Flat = True
              ParentShowHint = False
              ShowHint = True
            end
            object btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton
              Left = 850
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA SAIR DA TELA DE INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFEFEF1F9F7E5F6F2DBF2ECDBF2ECE6F6F2F2F9F7FDFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F3A3DDCD8AD9C59FE3D3BB
                EFE4BCEFE4A1E4D48BDAC6A4DDCDEAF6F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D0ECE45DC4A774D8BEA2EFDFBDF9EED3FDF6D4FDF6BFFAEFA4F0E076D8C05EC4
                A7D0ECE4FFFFFFFFFFFFFFFFFFDBF1EA59C5A673DEC571EBD286F2DDA2F8E8B6
                FBEFB7FBEFA4F9E988F3DE73ECD373DEC559C5A6DBF1EAFFFFFFFBFDFC74CCB3
                6BDBBF5DE0C264E6CB73EDD587F3DE95F6E39FF3E2CFF4EB8EEBD864E7CC5DE0
                C36BDBC074CCB3FBFDFCD2EDE660D0B252D4B559DBBD5FE2C564E7CB6DEBD174
                EED6A6F2E2FFFFFFF7FCFB81E4CD59DCBD53D5B560D0B2D2EDE6A4DECE4CCBAA
                4DCFAF54D6B65ADCBD62E1C567E5CA6AE7CC6CE7CDCBF5ECFFFFFFF7FCFB7ADC
                C44DD0AF4DCBAAA4DECE91D7C43EC29F5DCFB182DCC58BE0CB8DE2CE8FE4D08F
                E5D18FE5D190E4D1E7F8F3FFFFFFF6FCFA72D5BB3EC3A091D8C485D4BE46C2A1
                ECF8F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF6FCFA4DC4A485D5BF86D4BF4BC1A19FDECDC7EDE3C9EEE4CAEFE5CBEFE6CB
                F0E6CBF0E6CBEFE6EAF8F4FFFFFFFFFFFFB7E6DA4BC1A187D4BFA2DDCD44BD9B
                4CC1A151C5A556C8A959CBAC5CCCAE5DCDAF5DCDAF87D9C3FAFDFCFFFFFFB8E6
                D94DC1A145BD9CA2DDCDE5F3EF39B79346BC9B4BBF9F4FC2A253C4A555C6A756
                C7A879D2BAFAFDFCFFFFFFB7E5D94CC0A046BD9C39B794E6F4F0FFFFFF81D0BA
                3EB89545BB9A48BD9D4BBF9F4DC0A14EC1A183D3BDFEFEFEB5E4D749BE9D45BB
                9A3EB89681D0BAFFFFFFFFFFFFF9FCFB58C0A23CB79442B99744BA9946BB9A47
                BC9B47BC9B52C0A145BA9942B9983DB79458C1A3F9FCFBFFFFFFFFFFFFFFFFFF
                F9FCFB7CCDB732B28D3EB79540B79640B89640B89640B8963EB79532B28E7DCE
                B7F9FCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEEFEB83D0BA52BD9F3D
                B5943DB59452BD9F83D0BADEF0EBFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick
            end
            object btnCOPIAR_FICHA: TSpeedButton
              Left = 7
              Top = 7
              Width = 169
              Height = 39
              Caption = 'COPIAR FICHA'
              Flat = True
              Glyph.Data = {
                36030000424D360300000000000036000000280000000F000000100000000100
                18000000000000030000CE0E0000C40E00000000000000000000C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C08000008000008000008000008000008000008000
                00800000800000000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0800000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0800000FFFFFF0000000000000000000000000000
                00FFFFFF800000000000000000000000000000000000000000000000800000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFF0000000000000000000000000000
                00FFFFFF800000000000000000FFFFFF000000000000000000000000800000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFF000000000000FFFFFF8000008000
                00800000800000000000000000FFFFFF000000000000000000000000800000FF
                FFFFFFFFFFFFFFFFFFFFFF800000FFFFFF800000C0C0C0000000000000FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000
                00C0C0C0C0C0C0000000000000FFFFFF000000000000FFFFFF00000080000080
                0000800000800000800000800000C0C0C0C0C0C0C0C0C0000000000000FFFFFF
                FFFFFFFFFFFFFFFFFF000000FFFFFF000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000000000000000
                000000000000000000000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000}
              Visible = False
            end
            object btnEXCLUIR_FAMILIAR: TSpeedButton
              Left = 184
              Top = 7
              Width = 163
              Height = 40
              Hint = 'CLIQUE PARA EXCLUIR MEMBRO DA COMPOSI��O FAMILIAR'
              Caption = 'EXCLUIR FAMILIAR'
              Flat = True
              Glyph.Data = {
                96060000424D9606000000000000360000002800000016000000180000000100
                18000000000060060000C30E0000C30E00000000000000000000CCCCCCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBFBF
                BF9595959A9A9ACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC0000CCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC85
                85856161616060606767675E5E5EA8A8A8CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
                0000CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCBBBBBBA2A2A2
                4141417B7B7B8E8E8E9C9C9C8181817F7F7F5858585E5E5EBCBCBCCCCCCCCCCC
                CCCCCCCC0000CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC7B7B7B6868
                68535353ACACACB3B3B3BFBFBF9D9D9D7F7F7F7F7F7F7F7F7F747474787878CC
                CCCCCCCCCCCCCCCC0000CCCCCCCCCCCCCCCCCCCCCCCCC3C3C39999994646467C
                7C7C8E8E8EB2B2B2BFBFBFBFBFBFBFBFBF9D9D9D7F7F7F7F7F7F7F7F7F636363
                787878CCCCCCCCCCCCCCCCCC0000CCCCCCCCCCCCCCCCCCC6C6C6686868616161
                A0A0A0B5B5B57D947DA5B0A5BFBFBFBFBFBFBFBFBF9D9D9D7F7F7F7F7F7F7F7F
                7F7171716C6C6CC1C1C1CCCCCCCCCCCC0000CCCCCCCCCCCCCCCCCCBEBEBEB4B4
                B4AFAFAFBFBFBF849484167F16328F3241954192AF92BFBFBF9D9D9D7F7F7F7F
                7F7F7F7F7F8282824B4B4BAEAEAECCCCCCCCCCCC0000CCCCCCCCCCCCCCCCCCBE
                BEBEB8B9B86DA06D84A784389038007F00248B242A892A118011B2BAB29D9D9D
                7F7F7F7F7F7F7F7F7F7F7F7F444444AEAEAECCCCCCCCCCCC0000CCCCCCCCCCCC
                C9C9C9B9B9B96EA26E338933B3B3B3ADB8AD34903493B093B4B4B4388A38B2BA
                B29D9D9D7F7F7F7F7F7F7F7F7F7F7F7F707070757575CCCCCCCCCCCC0000CCCC
                CCCCCCCCC7C7C7B5B5B5318F313D933DB0BAB0BCBEBCAFB9AFB7BCB7BFBFBF52
                9A52B3BAB39D9D9D7F7F7F7F7F7F7F7F7F7F7F7F8383833E3E3ECCCCCCCCCCCC
                0000CCCCCCCCCCCCC7C7C7B5B5B5318F31238A232A8C2AACB8ACBFBFBF80A980
                6EA46E6EA46EB9BCB99D9D9D7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F3D3D3DCCCC
                CCCCCCCC0000CCCCCCCCCCCCB9B9B9BDBDBD92AF92007F00007F00ACB8ACBFBF
                BF8CAD8C007F00007F00B2BAB29D9D9D7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F79
                79797F7F7FCCCCCC0000CCCCCCCCCCCCB3B3B3BFBFBF5B9D5B3892383892387E
                A87E86AC861E881E138513138513B2BAB29D9D9D7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F717171616161CCCCCC0000CCCCCCCCCCCCB3B3B3BFBFBFBFBFBFBFBFBF
                BFBFBF719F71267F260781078CAD8CA4C6A4DFE2DFD7D7D7C3C3C37F7F7F7F7F
                7F7F7F7F7F7F7F717171616161CCCCCC0000CCCCCCBBBBBBBDBDBDBFBFBFBFBF
                BFBFBFBFBFBFBFBDBDBDB6BDB6B8C5B8E0E0E0B1B1B19D9D9DD2D2D2CBCBCBE5
                E5E5E5E5E59999999797978484846565659B9B9B0000CCCCCCBABABABCBCBCBF
                BFBFBFBFBFBFBFBFBFBFBFDADADACFCFCFBCBCBC898989B3B3B3A0A0A0FFFFFF
                E4E4E4DEDEDEC8C8C8DEDEDEE0E0E0C1C1C17575759696960000CBCBCBB9B9B9
                BCBCBCBFBFBFC1C1C1CECECEE8E8E8AAAAAAC1C1C1DADADA959595A4A4A4DCDC
                DCFFFFFFFFFFFFFFFFFFF4F4F4C2C2C2BDBDBDEBEBEB8888889999990000C3C3
                C3B8B8B8C2C2C2D6D6D6D1D1D1C4C4C4909090F2F2F2FFFFFFD7D7D7ACACACF0
                F0F0999999E8E8E8FFFFFFFFFFFFFFFFFFCCCCCCAAAAAAB9B9B9C0C0C0CCCCCC
                0000C3C3C3CFCFCFDEDEDEABABABC8C8C8E1E1E1B0B0B0F2F2F2D6D6D6ADADAD
                F0F0F0FFFFFF909090A9A9A9EEEEEEFFFFFFC5C5C5B1B1B1C1C1C1CACACACCCC
                CCCCCCCC0000C3C3C3D4D4D4A8A8A87F7F7FC6C6C6FFFFFFB0B0B0DADADAA6A6
                A6F0F0F0FFFFFFEAEAEAB7B7B7CBCBCBD2D2D2C8C8C8DDDDDDB6B6B6CCCCCCCC
                CCCCCCCCCCCCCCCC0000C7C7C7BCBCBCC8C8C8B4B4B4C2C2C2BBBBBB959595B5
                B5B5DFDFDFFFFFFFFFFFFFACACACF6F6F6FFFFFFCECECED9D9D9ECECECB6B6B6
                CCCCCCCCCCCCCCCCCCCCCCCC0000CCCCCCC8C8C8C4C4C4BABABABEBEBEC6C6C6
                DEDEDE959595A1A1A1E1E1E1F6F6F6C2C2C2BDBDBDC8C8C8EAEAEAC9C9C9B8B8
                B8C7C7C7CCCCCCCCCCCCCCCCCCCCCCCC0000CCCCCCCCCCCCCCCCCCCCCCCCCBCB
                CBBABABABABABAC3C3C3CDCDCDBBBBBBBBBBBBB3B3B3CACACAC2C2C2BABABAC1
                C1C1CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC0000CCCCCCCCCCCCCCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCC4C4C4C3C3C3CACACAC8C8C8C3C3C3CBCBCBCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC0000}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnEXCLUIR_FAMILIARClick
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = '[Relat�rios]'
          ImageIndex = 6
          object Bevel2: TBevel
            Left = 6
            Top = 169
            Width = 899
            Height = 9
          end
          object gpbDATA_RELATORIO: TGroupBox
            Left = 5
            Top = 5
            Width = 100
            Height = 50
            Caption = '[Data]'
            TabOrder = 0
            object mskDATA_RELATORIO: TMaskEdit
              Left = 9
              Top = 17
              Width = 75
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object gpbENTREVISTADOR: TGroupBox
            Left = 107
            Top = 5
            Width = 425
            Height = 50
            Caption = '[Entrevistador]'
            TabOrder = 1
            object edtNOME_ENTREVISTADOR: TEdit
              Left = 9
              Top = 17
              Width = 406
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
            end
          end
          object gpbTIPO_RELATORIO: TGroupBox
            Left = 534
            Top = 5
            Width = 373
            Height = 50
            Caption = '[Relat�rio a ser digitado]'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object cmbTIPO_RELATORIO: TComboBox
              Left = 8
              Top = 16
              Width = 356
              Height = 19
              Style = csOwnerDrawFixed
              ItemHeight = 13
              TabOrder = 0
              Items.Strings = (
                'Servi�o Social: Inscri��o'
                'Servi�o Social: Renova��o'
                'Servi�o Social: Visita'
                'Servi�o Social: Atendimento'
                'Registro de Atendimento')
            end
          end
          object gpbDESCRICAO_RELATORIO: TGroupBox
            Left = 6
            Top = 56
            Width = 901
            Height = 108
            Caption = '[Relat�rio]'
            TabOrder = 3
            object medDESCRICAO_RELATORIO: TMemo
              Left = 8
              Top = 16
              Width = 884
              Height = 83
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
          object gpbRELATORIOS_GRAVADOS: TGroupBox
            Left = 9
            Top = 182
            Width = 894
            Height = 194
            Caption = '[Relat�rios Gravados para a inscri��o atual]'
            TabOrder = 4
            object dbgRELATORIOS_GRAVADOS: TDBGrid
              Left = 12
              Top = 19
              Width = 874
              Height = 162
              DataSource = dsSel_Cadastro_Relatorios
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = dbgRELATORIOS_GRAVADOSCellClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'dta_digitacao'
                  Title.Alignment = taCenter
                  Title.Caption = '[Data Digita��o]'
                  Width = 95
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'dsc_entrevistador'
                  Title.Caption = '[Nome Entrevistador]'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TipodeRelatorio'
                  Title.Caption = '[Tipo Relat�rio]'
                  Width = 250
                  Visible = True
                end>
            end
          end
          object Panel7: TPanel
            Left = 7
            Top = 379
            Width = 896
            Height = 51
            BevelInner = bvLowered
            TabOrder = 5
            object btnNOVO_RELATORIO: TSpeedButton
              Left = 472
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA PREENCHER NOVA INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDF4FAF3EBF7E9E3F4E1E3F4E1ECF7EAF4FAF3FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEF7EDB6DFB0A5DA9DB8E4B0D1
                EECBD1EECCBAE4B2A6DB9EB6DFB1EEF7EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D9EED67DC47396D58EBEEBB8D4F5D0E2FAE0E3FAE1D5F6D1C0ECBA98D68F7DC4
                73D9EED6FFFFFFFFFFFFFFFFFFE2F2E07BC47197DA8F9BE393ABEBA5C0EDBBCC
                F0C9CDF0C9C1EDBCACEBA69CE49497DA8F7BC471E2F2E0FFFFFFFBFDFB8FCD87
                90D68789D87F90DE879BE493FBFDFBFFFFFFFFFFFFFEFEFE9EE49791DF8889D9
                8091D6888FCD87FBFDFBDBEFD884CC7B7DCE7385D57A8ADA828FDE87F9FCF9FF
                FFFFFFFFFFFEFEFE92DD898BDA8285D57B7ECF7384CC7CDBEFD8B7DFB177C66C
                78CA6D7FD07485D57B8CD983F8FBF7FFFFFFFFFFFFFEFEFE8DD98486D57B7FD0
                7579CA6E78C66CB7DFB1A9D8A16CBE5F9BD493DEF0DCE0F1DEE1F1DFFDFEFDFF
                FFFFFFFFFFFEFEFEE1F1DFE0F1DEDEF0DCA2D69A6CBF5FA9D8A1A2D39873C065
                ABDAA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFB4DDAD73C065A2D499A3D49976BF68A5D59CFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADD9A577BF69A4D49AB9DDB172BC62
                98CE8EF3F9F1F3F9F2F3F9F2FDFEFDFFFFFFFFFFFFFEFEFEF3F9F2F3F9F2F3F9
                F1A0D29672BC63BADDB1EBF3E96AB75873BC6477BE687AC16C7CC26FF0F7EEFF
                FFFFFFFFFFF8FBF87CC26F7AC16C77BF6873BC646AB758EBF4E9FFFFFFA0D194
                6EB85C73BB6275BD6677BE68EEF6ECFFFFFFFFFFFFF6FAF677BE6875BD6673BB
                626EB85CA1D194FFFFFFFFFFFFFAFCFA81C1706DB75A71BA5F73BB62EAF4E8FF
                FFFFFFFFFFF3F8F273BB6271BA5F6EB75A82C171FAFCFAFFFFFFFFFFFFFFFFFF
                FAFCFA9DCE8F66B3506FB85B70B85D71B85D71B85D70B85D6FB85B66B3509ECE
                90FAFCFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E2A2D1947EBF6A6E
                B7596EB7597EBF6AA3D194E6F0E2FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnNOVO_RELATORIOClick
            end
            object btnGRAVAR_RELATORIO: TSpeedButton
              Left = 536
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA GRAVAR OS DADOS DA FICHA EM TELA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDFCF7F2FAF1E7F8ECDEF8ECDEFAF1E7FCF7F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF2EBECCAA7EBC18CF0D0A1F6
                E1BCF6E1BDF0D1A3EBC28FEDCBA8FAF2EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5E3D2E0A160EAB775F6D8A3FBE8BEFDF1D3FDF2D4FBE9BFF6D9A5EAB877E0A1
                61F5E3D2FFFFFFFFFFFFFFFFFFF7EADDE0A05CEBB973F0C371F5D186F9DFA2FC
                E7B6FCE7B7FADFA4F5D288F0C373EBBA74E0A05CF7EADDFFFFFFFDFCFBE4AE77
                E6B26CE7B15DECBA64F0C77FF6DFB8F7D794F8D895F6D288F2C674EDBA64E8B1
                5DE6B26CE4AE77FEFCFBF5E4D4E1A561DFA252E4AB59E9B970FBF3E8FFFFFFF6
                E3C3F1C574F0C16DEDBA64E9B45FE4AC59DFA253E1A661F5E4D4ECCAA7D8964D
                DB9B4DE3AE6AFAF3E9FFFFFFFFFFFFFFFFFFF5E0C0EAB867E8B462E5AC5AE0A4
                54DC9C4DD8974DECCAA7E4BB93D28B3EDDA464FAF4ECFFFFFFFDFBF7F6E6CFFF
                FFFFFFFFFFF4E1C7E6B26BE3AD66DFA45BD8974BD38B3EE5BC93E0B285D28D47
                E0AE78FCF8F4FCF9F5E7BD88E3AD68F2DABBFFFFFFFFFFFFF4E1CADFA764DCA0
                5DD89A57D28D46E0B286DFB187D28D4BD59352DFAC76E1B07BDCA260DEA562DE
                A663EED3B3FFFFFFFFFFFFF3E1CED99C5CD59452D28D4BE0B287E6C3A2CE8744
                D18D4CD49251D69656D89A59D99C5CDA9E5DDA9E5DEBCBA9FEFEFEFFFFFFF3E2
                D0D39253CE8745E6C3A2F5EDE5C97D39CE8746D08B4BD28F4FD49253D59455D5
                9556D59556D59455E7C29EFEFEFEFFFFFFE4BE9ACA7D39F6EDE6FFFFFFDCAC81
                CB803ECD8545CF8948D08B4BD18D4DD18E4ED18E4ED18D4ED08B4BE2B892EACC
                B1CB8140DCAC81FFFFFFFFFFFFFCFAF9D18F58CA7E3CCC8242CD8544CE8646CE
                8747CE8747CE8646CD8544CC8342CA7E3DD19058FCFBF9FFFFFFFFFFFFFFFFFF
                FCFAF9DAA77CC77632CA7F3ECB8040CB8040CB8040CB8040CA7F3EC77632DBA8
                7DFCFAF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E7DEDCAB83CF8B52C9
                7D3DC97D3DCF8B52DCAB83F3E8DEFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnGRAVAR_RELATORIOClick
            end
            object btnEDITAR_RELATORIO: TSpeedButton
              Left = 577
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA HABILITAR A EDI��O DOS DADOS EM TELA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFDFDFBF8F1FAF4E8F7F1E0F7F1E0FAF5E8FCF9F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4EAEAD7ABE9D096EDDCA9F5
                E9C5F5EAC6EEDDABE9D297EBD8ACF9F4EAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                F3E9D3DBB76AE7C982F5E2ACFBEDC5FEF4D9FEF4DAFBEEC7F5E2AEE7CA84DBB7
                6BF3E9D3FFFEFEFFFFFFFFFFFFF6EEDDDBB566E9CA7EF0D07CF5DB91FAE5ACFC
                ECBEFCECBFFAE6AEF6DB93F1D17DE9CA7EDBB566F6EEDDFFFFFFFDFCFAE0C17F
                E6C474E8C266EDCA6FF2D27EF6DB92F8E09FF8E1A0F6DC94F2D380EDCA6FE8C3
                67E6C474E0C17FFDFCFAF4EBD7DFBB6AE0BA60E9CD8BECD190EED594F0D89BF1
                DBA0F1DBA0F0D89CEED595ECD291E9CE8CE1BB63DFBB6AF4EBD7EBD7ADD9B052
                E2C072FEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEE3C47AD9B053EBD7ADE4CD97D4A743D9B051EFDDB5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF1E1BDD9B051D4A844E5CD98E1C689D4AA4C
                D9B35CDDB966F8F2E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4E8DEBA
                69DAB35CD4AA4BE1C689E1C68AD3AB4FD6AF56D9B35CE1C27DFEFDFAFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFCE3C583D9B35CD6AF57D3AB50E1C689E7D3A5D0A748
                D3AB50D5AE56D7B15AEAD6A9FFFFFFFFFFFFFFFFFFFFFFFFECDAB1D7B15BD5AE
                56D3AB50D0A749E7D4A5F5F1E7CBA13CD0A849D2AA4FD4AD53D6AF58F5ECD8FF
                FFFFFFFFFFF7EFDFD6B05BD4AD54D2AA4FD0A84ACCA13CF6F2E7FFFFFFDEC382
                CDA440CFA747D0A94CD2AA4FD8B769FCFBF7FDFCF9DABA6FD2AA4FD1A94CCFA7
                48CDA441DEC483FFFFFFFFFFFFFCFBF9D3B059CCA33ECEA644CFA747CFA849E0
                C688E1C98ECFA849CFA747CEA644CCA33FD3B05AFCFBF9FFFFFFFFFFFFFFFFFF
                FCFBF9DCC17DC99E33CDA440CDA541CDA542CDA542CDA541CDA440C99F33DCC1
                7EFCFBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDDEDEC483D1AD52CB
                A43ECBA43ED1AE52DEC584F3EDDFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnEDITAR_RELATORIOClick
            end
            object btnCANCELAROPERACAO_RELATORIO: TSpeedButton
              Left = 650
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CANCELAR A OPERA��O ATUAL'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFDFEF0F0F8E3E3F4D8D8F0D9D9F1E4E4F5F0F0F9FDFDFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F69B9BDD7777DB8585E4A2
                A2EFA3A3EF8888E57878DC9C9CDEE8E8F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                CDCDEC4A4AC65757D68585E9A6A6F2C3C3F8C4C4F8A9A9F38787E95959D64B4B
                C7CCCCECFFFFFFFFFFFFFFFFFFD8D8F04242C65050D44343D95D5DE38282EC9C
                9CF29D9DF28484ED5F5FE34545DA5050D44242C6D8D8F0FFFFFFF9F9FC6363CD
                4646CC2D2DCB8383E0B9B9EE6262E37070E87171E86363E3BABAEE8282E02D2D
                CB4646CC6363CDF9F9FCCDCDEC3F3FC52424BD8686DCFEFEFEFFFFFFCACAF24E
                4EDC4F4FDCCACAF2FFFFFFFEFEFE8686DD2424BE4040C5CDCDED9898DB2525B5
                4141C2FCFCFEFFFFFFFFFFFFFFFFFFD0D0F2D0D0F3FFFFFFFFFFFFFFFFFFFCFC
                FE4141C22525B59898DB7F7FCE1717AC2323B57777D3F9F9FDFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF9F9FD7676D32323B51818AC8080CF7474C72727AD
                3636B63939BB6C6CCFF3F3FBFFFFFFFFFFFFFFFFFFFFFFFFF3F3FB6C6CCF3939
                BC3636B72727AD7575C77979C73232AD3535B23838B65858C5EAEAF8FFFFFFFF
                FFFFFFFFFFFFFFFFE9E9F85858C53838B63535B23232AE7979C79A9AD43131A9
                3434AE5D5DC2EEEEF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEF95D5D
                C23535AE3131AA9A9AD4E4E4F12A2AA33C3CAEEFEFF9FFFFFFFFFFFFFFFFFFD6
                D6EFD6D6EFFFFFFFFFFFFFFFFFFFEFEFF93C3CAE2A2AA3E4E4F2FFFFFF7B7BC4
                3232A66E6EC1F4F4FAFFFFFFC8C8E84444B24444B2C8C8E8FFFFFFF4F4FA6E6E
                C13232A67C7CC5FFFFFFFFFFFFF9F9FB5353B13434A55D5DB8A5A5D83E3EAC39
                39AA3939AA3E3EACA5A5D85D5DB83434A55353B1F9F9FBFFFFFFFFFFFFFFFFFF
                F9F9FB7B7BC22E2EA03939A63939A63A3AA73A3AA73939A63939A62E2EA07B7B
                C2F9F9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEED8383C55151AE3C
                3CA53C3CA55151AE8383C5DEDEEDFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnCANCELAROPERACAO_RELATORIOClick
            end
            object SpeedButton10: TSpeedButton
              Left = 722
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE PESQUISA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFCFBFAFAF8F5F8F4EFF8F4EFFAF8F5FCFBFAFFFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F7ECE3D6E9DECDEEE7D9F5
                F0E8F6F0E8EFE7DAEADFCDECE3D6FBF9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5F0EADDCBB3E6D9C3F1E7D9F5EEE3F9F4ECF9F4ECF6EFE4F1E8DAE7DAC5DDCB
                B3F5F1EAFFFFFFFFFFFFFFFFFFF8F4EFDDCBB2E7D8C1E8D9C0ECDFCAF1E7D7F4
                ECDFF4ECE0F1E7D7ECDFCBE8D9C0E7D8C2DDCBB2F8F4EFFFFFFFFEFDFDE1D3BE
                E5D6BEE3D3B9EBE1D1EDE4D5EDE3D4EFE3D1EFE3D1EDE4D5EDE4D6EBE1D1E3D3
                B9E5D6BEE1D3BEFEFDFDF5F0EAE1D1B8E0CFB2E5D7C0FFFFFFFFFFFFF5F0E8E9
                DAC1E9DAC1F5F0E8FFFFFFFFFFFFE5D7C0E0CFB3E1D1B8F5F0EAECE2D5DECDB1
                DECDB0E4D5BEFFFFFFFFFFFFF5F0E7E6D7BDE6D7BDF5F0E7FFFFFFFFFFFFE4D6
                BEDFCDB1DECDB2ECE2D5E9DFCFDBC9ACDECDB1E4D7C1FFFFFFFFFFFFFBF9F7F3
                EEE7F3EEE7FBF9F7FFFFFFFFFFFFE4D7C1DECDB1DBC9ACE9DFCFE9DFCFDDCCB1
                E0D0B7E5D8C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5D8
                C3E0D0B7DDCCB1E9DFCFEAE1D1DECEB4E2D5C1F2EDE5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDE5E2D5C1DECEB4EAE1D1EFE9DDDDCDB3
                DECEB5ECE3D5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEECE3
                D5DECEB5DDCDB3EFE8DDF8F6F3DBCAB1DDCEB4DECFB5E7DDCCFDFCFBFFFFFFFF
                FFFFFFFFFFFFFFFFFDFCFBE7DDCCDECFB5DDCEB4DBCAB1F9F7F4FFFFFFE8DECE
                DCCCB3DDCEB5DDCEB6E4D8C4FBF9F7FFFFFFFFFFFFFBF9F7E4D8C4DDCEB6DDCE
                B5DCCCB3E8DECEFFFFFFFFFFFFFCFCFCE1D3BFDCCDB4DDCEB5DDCEB6E1D3BEF8
                F5F1F8F5F1E1D3BEDDCEB6DDCEB5DCCDB4E1D3BFFDFCFCFFFFFFFFFFFFFFFFFF
                FCFCFCE7DDCDDBCAB1DCCDB5DDCEB5DED0B8DED0B8DDCEB5DCCDB5DBCAB1E7DD
                CDFDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F3EFE9DFD0E0D3BEDD
                CEB6DDCEB6E0D3BEE9DFD1F6F3F0FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = SpeedButton10Click
            end
            object SpeedButton11: TSpeedButton
              Left = 788
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE OP��ES DE IMPRESS�O'
              Flat = True
              ParentShowHint = False
              ShowHint = True
            end
            object btnSAIR_RELATORIO: TSpeedButton
              Left = 850
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA SAIR DA TELA DE INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFEFEF1F9F7E5F6F2DBF2ECDBF2ECE6F6F2F2F9F7FDFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F3A3DDCD8AD9C59FE3D3BB
                EFE4BCEFE4A1E4D48BDAC6A4DDCDEAF6F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D0ECE45DC4A774D8BEA2EFDFBDF9EED3FDF6D4FDF6BFFAEFA4F0E076D8C05EC4
                A7D0ECE4FFFFFFFFFFFFFFFFFFDBF1EA59C5A673DEC571EBD286F2DDA2F8E8B6
                FBEFB7FBEFA4F9E988F3DE73ECD373DEC559C5A6DBF1EAFFFFFFFBFDFC74CCB3
                6BDBBF5DE0C264E6CB73EDD587F3DE95F6E39FF3E2CFF4EB8EEBD864E7CC5DE0
                C36BDBC074CCB3FBFDFCD2EDE660D0B252D4B559DBBD5FE2C564E7CB6DEBD174
                EED6A6F2E2FFFFFFF7FCFB81E4CD59DCBD53D5B560D0B2D2EDE6A4DECE4CCBAA
                4DCFAF54D6B65ADCBD62E1C567E5CA6AE7CC6CE7CDCBF5ECFFFFFFF7FCFB7ADC
                C44DD0AF4DCBAAA4DECE91D7C43EC29F5DCFB182DCC58BE0CB8DE2CE8FE4D08F
                E5D18FE5D190E4D1E7F8F3FFFFFFF6FCFA72D5BB3EC3A091D8C485D4BE46C2A1
                ECF8F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF6FCFA4DC4A485D5BF86D4BF4BC1A19FDECDC7EDE3C9EEE4CAEFE5CBEFE6CB
                F0E6CBF0E6CBEFE6EAF8F4FFFFFFFFFFFFB7E6DA4BC1A187D4BFA2DDCD44BD9B
                4CC1A151C5A556C8A959CBAC5CCCAE5DCDAF5DCDAF87D9C3FAFDFCFFFFFFB8E6
                D94DC1A145BD9CA2DDCDE5F3EF39B79346BC9B4BBF9F4FC2A253C4A555C6A756
                C7A879D2BAFAFDFCFFFFFFB7E5D94CC0A046BD9C39B794E6F4F0FFFFFF81D0BA
                3EB89545BB9A48BD9D4BBF9F4DC0A14EC1A183D3BDFEFEFEB5E4D749BE9D45BB
                9A3EB89681D0BAFFFFFFFFFFFFF9FCFB58C0A23CB79442B99744BA9946BB9A47
                BC9B47BC9B52C0A145BA9942B9983DB79458C1A3F9FCFBFFFFFFFFFFFFFFFFFF
                F9FCFB7CCDB732B28D3EB79540B79640B89640B89640B8963EB79532B28E7DCE
                B7F9FCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEEFEB83D0BA52BD9F3D
                B5943DB59452BD9F83D0BADEF0EBFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnSAIR_RELATORIOClick
            end
          end
        end
        object TabSheet15: TTabSheet
          Caption = '[Pontua��o Final]'
          ImageIndex = 6
          OnShow = TabSheet15Show
          object gpbPONTUACAO: TGroupBox
            Left = 5
            Top = 7
            Width = 902
            Height = 422
            Caption = '[Pontua��o]'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            object gpbRENDA_PERCAPTA: TGroupBox
              Left = 17
              Top = 25
              Width = 864
              Height = 158
              Caption = '[Renda Per Capita]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              object Panel3: TPanel
                Left = 178
                Top = 23
                Width = 120
                Height = 41
                Caption = 'RENDA TOTAL'
                TabOrder = 0
              end
              object Panel10: TPanel
                Left = 448
                Top = 23
                Width = 120
                Height = 41
                Caption = 'N� RESIDENTES'
                TabOrder = 1
              end
              object panRENDA_TOTAL: TPanel
                Left = 299
                Top = 23
                Width = 120
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
              end
              object panNUMERO_RESIDENTES: TPanel
                Left = 569
                Top = 23
                Width = 120
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
              end
              object panRENDA_PORHABITANTE: TPanel
                Left = 297
                Top = 87
                Width = 120
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
              end
              object Panel11: TPanel
                Left = 176
                Top = 87
                Width = 120
                Height = 41
                Caption = 'RENDA P/ CAPTA'
                TabOrder = 5
              end
              object Panel9: TPanel
                Left = 448
                Top = 87
                Width = 120
                Height = 41
                Caption = 'DESPESAS'
                TabOrder = 6
              end
              object panDESPESAS: TPanel
                Left = 569
                Top = 87
                Width = 120
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
              end
            end
            object gpbPONTUACAO_FINAL: TGroupBox
              Left = 18
              Top = 189
              Width = 864
              Height = 223
              Caption = '[Pontua��o Final]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              object btnCALCULA_PONTOS: TSpeedButton
                Left = 121
                Top = 124
                Width = 191
                Height = 90
                Caption = 'CALCULAR PONTOS'
                Flat = True
                Glyph.Data = {
                  76050000424D7605000000000000360000002800000012000000180000000100
                  18000000000040050000C30E0000C30E00000000000000000000BFBFBFBFBFBF
                  BFBFBF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                  7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F0000BFBFBFBFBFBF7F7F7F7F7F7F7F7F
                  7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                  7F7F7F7F7F7F7F7F0000BFBFBF00000000000000000000000000000000000000
                  00000000000000000000000000000000000000000000007F7F7F7F7F7F7F7F7F
                  0000BFBFBF0000007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
                  7F7F007F7F007F7F007F7F007F7F000000007F7F7F7F7F7F0000BFBFBF000000
                  FFFF000000000000007F7F000000000000007F7F000000000000007F7F000000
                  000000007F7F000000007F7F7F7F7F7F0000BFBFBF000000FFFF007F7F7F0000
                  007F7F007F7F7F0000007F7F007F7F7F0000007F7F007F7F7F0000007F7F0000
                  00007F7F7F7F7F7F0000BFBFBF000000FFFF007F7F007F7F007F7F007F7F007F
                  7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F000000007F7F7F7F7F7F
                  0000BFBFBF000000FFFF000000000000007F7F000000000000007F7F00000000
                  0000007F7F000000000000007F7F000000007F7F7F7F7F7F0000BFBFBF000000
                  FFFF007F7F7F0000007F7F007F7F7F0000007F7F007F7F7F0000007F7F007F7F
                  7F0000007F7F000000007F7F7F7F7F7F0000BFBFBF000000FFFF007F7F007F7F
                  007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F0000
                  00007F7F7F7F7F7F0000BFBFBF000000FFFF000000000000007F7F0000000000
                  00007F7F000000000000007F7F000000000000007F7F000000007F7F7F7F7F7F
                  0000BFBFBF000000FFFF007F7F7F0000007F7F007F7F7F0000007F7F007F7F7F
                  0000007F7F007F7F7F0000007F7F000000007F7F7F7F7F7F0000BFBFBF000000
                  FFFF007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
                  007F7F007F7F000000007F7F7F7F7F7F0000BFBFBF000000FFFF000000000000
                  007F7F000000000000007F7F000000000000007F7F000000000000007F7F0000
                  00007F7F7F7F7F7F0000BFBFBF000000FFFF007F7F7F0000007F7F007F7F7F00
                  00007F7F007F7F7F0000007F7F007F7F7F0000007F7F000000007F7F7F7F7F7F
                  0000BFBFBF000000FFFF007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
                  7F7F007F7F007F7F007F7F007F7F000000007F7F7F7F7F7F0000BFBFBF000000
                  FFFF007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
                  007F7F007F7F000000007F7F7F7F7F7F0000BFBFBF000000FFFF00000000FFFF
                  FFFFFFFF000000FFFFFF000000000000000000FFFFFFFFFFFF0000007F7F0000
                  00007F7F7F7F7F7F0000BFBFBF000000FFFF00000000FFFFFFFFFFFF000000FF
                  FFFF000000FFFFFF000000FFFFFFFFFFFF0000007F7F000000007F7F7F7F7F7F
                  0000BFBFBF000000FFFF00000000FFFFFFFFFFFF000000FFFFFF000000000000
                  000000FFFFFFFFFFFF0000007F7F000000007F7F7F7F7F7F0000BFBFBF000000
                  FFFF00000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
                  BF0000007F7F000000007F7F7F7F7F7F0000BFBFBF000000FFFF000000000000
                  000000000000000000000000000000000000000000000000000000007F7F0000
                  00007F7F7F7F7F7F0000BFBFBF000000FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF007F7F000000007F7F7FBFBFBF
                  0000BFBFBF000000000000000000000000000000000000000000000000000000
                  000000000000000000000000000000000000BFBFBFBFBFBF0000}
                Layout = blGlyphTop
                OnClick = btnCALCULA_PONTOSClick
              end
              object Panel14: TPanel
                Left = 8
                Top = 20
                Width = 130
                Height = 41
                Caption = 'RENDA PER CAPTA'
                TabOrder = 0
              end
              object panRENDA_PERCAPTA: TPanel
                Left = 140
                Top = 20
                Width = 130
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
              object Panel13: TPanel
                Left = 299
                Top = 20
                Width = 130
                Height = 41
                Caption = 'SITUA��O SOCIAL'
                TabOrder = 2
              end
              object panSITUACAO_SOCIAL: TPanel
                Left = 431
                Top = 20
                Width = 130
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
              end
              object Panel16: TPanel
                Left = 586
                Top = 20
                Width = 130
                Height = 41
                Caption = 'COND. DE SA�DE'
                TabOrder = 4
              end
              object panCONDICAO_SAUDE: TPanel
                Left = 718
                Top = 20
                Width = 130
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 5
              end
              object Panel18: TPanel
                Left = 8
                Top = 65
                Width = 130
                Height = 41
                Caption = 'TEMPO RESID.'
                TabOrder = 6
              end
              object panTEMPO_RESIDENCIA: TPanel
                Left = 140
                Top = 65
                Width = 130
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
              end
              object Panel20: TPanel
                Left = 299
                Top = 65
                Width = 130
                Height = 41
                Caption = 'SITUA��O MORADIA'
                TabOrder = 8
              end
              object panSITUACAO_MORADIA: TPanel
                Left = 431
                Top = 65
                Width = 130
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 9
              end
              object Panel22: TPanel
                Left = 586
                Top = 65
                Width = 130
                Height = 41
                Caption = 'BENS'
                TabOrder = 10
              end
              object panBENS: TPanel
                Left = 718
                Top = 65
                Width = 130
                Height = 41
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -19
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 11
              end
              object Panel24: TPanel
                Left = 405
                Top = 132
                Width = 202
                Height = 75
                Caption = 'PONTUA��O TOTAL'
                TabOrder = 12
              end
              object panPONTUACAO_TOTAL: TPanel
                Left = 611
                Top = 131
                Width = 202
                Height = 75
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -24
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 13
              end
            end
          end
        end
        object TabSheet14: TTabSheet
          Caption = '[Pend�ncias]'
          ImageIndex = 8
          object gpbPENDENCIAS: TGroupBox
            Left = 5
            Top = 5
            Width = 902
            Height = 270
            Caption = 
              '[Informe as pend�ncias que impedem a inscri��o em definitivo do(' +
              'a) candidato(a)]'
            TabOrder = 0
            object Label1: TLabel
              Left = 584
              Top = 183
              Width = 77
              Height = 13
              Caption = '�ltima Altera��o'
              Visible = False
            end
            object chk_PENDENCIA_CARTPROF: TCheckBox
              Left = 21
              Top = 27
              Width = 176
              Height = 17
              Caption = 'Carteira Profissional'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object chk_PENDENCIA_COMPROVRENDA: TCheckBox
              Left = 21
              Top = 57
              Width = 176
              Height = 17
              Caption = 'Comprovante de Renda'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object chk_PENDENCIA_COMPROVALUGUEL: TCheckBox
              Left = 21
              Top = 85
              Width = 292
              Height = 17
              Caption = 'Comprovante de aluguel/financiamento/IPTU'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
            end
            object chk_PENDENCIA_SUS: TCheckBox
              Left = 21
              Top = 198
              Width = 93
              Height = 17
              Caption = 'Cart�o SUS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
            end
            object chk_PENDENCIA_DECLARAESCOLA: TCheckBox
              Left = 21
              Top = 169
              Width = 144
              Height = 17
              Caption = 'Declara��o Escolar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
            end
            object chk_PENDENCIA_OUTROS: TCheckBox
              Left = 504
              Top = 27
              Width = 65
              Height = 17
              Caption = 'Outros'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
            end
            object med_PENDENCIA_OUTROS: TMemo
              Left = 506
              Top = 47
              Width = 376
              Height = 99
              ScrollBars = ssVertical
              TabOrder = 6
            end
            object mskDATA_ULTIMALATERACAO: TMaskEdit
              Left = 666
              Top = 181
              Width = 83
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 7
              Text = '  /  /    '
              Visible = False
            end
            object edtPENDENCIA_DETALHES_ESCOLA: TEdit
              Left = 165
              Top = 167
              Width = 274
              Height = 21
              TabOrder = 8
            end
            object edtPENDENCIAS_DETALHE_SUS: TEdit
              Left = 115
              Top = 196
              Width = 274
              Height = 21
              TabOrder = 9
            end
            object chk_PENDENCIA_NIS: TCheckBox
              Left = 21
              Top = 142
              Width = 44
              Height = 17
              Caption = 'NIS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 10
            end
            object edtPENDENCIA_DETALHES_NIS: TEdit
              Left = 68
              Top = 139
              Width = 274
              Height = 21
              TabOrder = 11
            end
            object chk_PENDENCIA_CPF: TCheckBox
              Left = 22
              Top = 228
              Width = 49
              Height = 17
              Caption = 'CPF'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 12
            end
            object edtPENDENCIAS_DETALHE_CPF: TEdit
              Left = 73
              Top = 225
              Width = 274
              Height = 21
              TabOrder = 13
            end
            object chk_PENDENCIA_COMPROVTEMPORESID: TCheckBox
              Left = 22
              Top = 113
              Width = 260
              Height = 17
              Caption = 'Comprovante de Tempo de Resid�ncia'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 14
            end
          end
          object gpbDADOS_PENDENCIAS: TGroupBox
            Left = 5
            Top = 275
            Width = 902
            Height = 98
            Caption = '[Pend�ncias Cadastradas]'
            TabOrder = 1
            object dbgPendencias: TDBGrid
              Left = 8
              Top = 17
              Width = 885
              Height = 66
              DataSource = dsPENDENCIAS
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = dbgPendenciasCellClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'DataCadastro'
                  Title.Alignment = taCenter
                  Title.Caption = '[Cadastro]'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'DataAltera'
                  Title.Alignment = taCenter
                  Title.Caption = '[Alterado]'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vCTPS'
                  Title.Alignment = taCenter
                  Title.Caption = '[Cart. Trab.]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vRenda'
                  Title.Alignment = taCenter
                  Title.Caption = '[Comp. Renda]'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vAluguel'
                  Title.Alignment = taCenter
                  Title.Caption = '[Comp. Alug.]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vTempoRes'
                  Title.Alignment = taCenter
                  Title.Caption = '[Tempo res.]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vNIS'
                  Title.Alignment = taCenter
                  Title.Caption = '[NIS]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vEscola'
                  Title.Alignment = taCenter
                  Title.Caption = '[Decl. Esc.]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vSUS'
                  Title.Alignment = taCenter
                  Title.Caption = '[SUS]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vCpf'
                  Title.Alignment = taCenter
                  Title.Caption = '[CPF]'
                  Width = 75
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'vOutros'
                  Title.Alignment = taCenter
                  Title.Caption = '[Outros]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nom_usuario'
                  Title.Caption = '[Quem registrou?]'
                  Visible = True
                end>
            end
          end
          object Panel8: TPanel
            Left = 5
            Top = 379
            Width = 901
            Height = 51
            BevelInner = bvLowered
            TabOrder = 2
            object btnNOVA_PENDENCIA: TSpeedButton
              Left = 472
              Top = 7
              Width = 40
              Height = 40
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDF4FAF3EBF7E9E3F4E1E3F4E1ECF7EAF4FAF3FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEF7EDB6DFB0A5DA9DB8E4B0D1
                EECBD1EECCBAE4B2A6DB9EB6DFB1EEF7EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D9EED67DC47396D58EBEEBB8D4F5D0E2FAE0E3FAE1D5F6D1C0ECBA98D68F7DC4
                73D9EED6FFFFFFFFFFFFFFFFFFE2F2E07BC47197DA8F9BE393ABEBA5C0EDBBCC
                F0C9CDF0C9C1EDBCACEBA69CE49497DA8F7BC471E2F2E0FFFFFFFBFDFB8FCD87
                90D68789D87F90DE879BE493FBFDFBFFFFFFFFFFFFFEFEFE9EE49791DF8889D9
                8091D6888FCD87FBFDFBDBEFD884CC7B7DCE7385D57A8ADA828FDE87F9FCF9FF
                FFFFFFFFFFFEFEFE92DD898BDA8285D57B7ECF7384CC7CDBEFD8B7DFB177C66C
                78CA6D7FD07485D57B8CD983F8FBF7FFFFFFFFFFFFFEFEFE8DD98486D57B7FD0
                7579CA6E78C66CB7DFB1A9D8A16CBE5F9BD493DEF0DCE0F1DEE1F1DFFDFEFDFF
                FFFFFFFFFFFEFEFEE1F1DFE0F1DEDEF0DCA2D69A6CBF5FA9D8A1A2D39873C065
                ABDAA4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFB4DDAD73C065A2D499A3D49976BF68A5D59CFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADD9A577BF69A4D49AB9DDB172BC62
                98CE8EF3F9F1F3F9F2F3F9F2FDFEFDFFFFFFFFFFFFFEFEFEF3F9F2F3F9F2F3F9
                F1A0D29672BC63BADDB1EBF3E96AB75873BC6477BE687AC16C7CC26FF0F7EEFF
                FFFFFFFFFFF8FBF87CC26F7AC16C77BF6873BC646AB758EBF4E9FFFFFFA0D194
                6EB85C73BB6275BD6677BE68EEF6ECFFFFFFFFFFFFF6FAF677BE6875BD6673BB
                626EB85CA1D194FFFFFFFFFFFFFAFCFA81C1706DB75A71BA5F73BB62EAF4E8FF
                FFFFFFFFFFF3F8F273BB6271BA5F6EB75A82C171FAFCFAFFFFFFFFFFFFFFFFFF
                FAFCFA9DCE8F66B3506FB85B70B85D71B85D71B85D70B85D6FB85B66B3509ECE
                90FAFCFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E2A2D1947EBF6A6E
                B7596EB7597EBF6AA3D194E6F0E2FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnNOVA_PENDENCIAClick
            end
            object btnGRAVAR_PENDENCIA: TSpeedButton
              Left = 536
              Top = 7
              Width = 40
              Height = 40
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFEFDFCF7F2FAF1E7F8ECDEF8ECDEFAF1E7FCF7F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF2EBECCAA7EBC18CF0D0A1F6
                E1BCF6E1BDF0D1A3EBC28FEDCBA8FAF2EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5E3D2E0A160EAB775F6D8A3FBE8BEFDF1D3FDF2D4FBE9BFF6D9A5EAB877E0A1
                61F5E3D2FFFFFFFFFFFFFFFFFFF7EADDE0A05CEBB973F0C371F5D186F9DFA2FC
                E7B6FCE7B7FADFA4F5D288F0C373EBBA74E0A05CF7EADDFFFFFFFDFCFBE4AE77
                E6B26CE7B15DECBA64F0C77FF6DFB8F7D794F8D895F6D288F2C674EDBA64E8B1
                5DE6B26CE4AE77FEFCFBF5E4D4E1A561DFA252E4AB59E9B970FBF3E8FFFFFFF6
                E3C3F1C574F0C16DEDBA64E9B45FE4AC59DFA253E1A661F5E4D4ECCAA7D8964D
                DB9B4DE3AE6AFAF3E9FFFFFFFFFFFFFFFFFFF5E0C0EAB867E8B462E5AC5AE0A4
                54DC9C4DD8974DECCAA7E4BB93D28B3EDDA464FAF4ECFFFFFFFDFBF7F6E6CFFF
                FFFFFFFFFFF4E1C7E6B26BE3AD66DFA45BD8974BD38B3EE5BC93E0B285D28D47
                E0AE78FCF8F4FCF9F5E7BD88E3AD68F2DABBFFFFFFFFFFFFF4E1CADFA764DCA0
                5DD89A57D28D46E0B286DFB187D28D4BD59352DFAC76E1B07BDCA260DEA562DE
                A663EED3B3FFFFFFFFFFFFF3E1CED99C5CD59452D28D4BE0B287E6C3A2CE8744
                D18D4CD49251D69656D89A59D99C5CDA9E5DDA9E5DEBCBA9FEFEFEFFFFFFF3E2
                D0D39253CE8745E6C3A2F5EDE5C97D39CE8746D08B4BD28F4FD49253D59455D5
                9556D59556D59455E7C29EFEFEFEFFFFFFE4BE9ACA7D39F6EDE6FFFFFFDCAC81
                CB803ECD8545CF8948D08B4BD18D4DD18E4ED18E4ED18D4ED08B4BE2B892EACC
                B1CB8140DCAC81FFFFFFFFFFFFFCFAF9D18F58CA7E3CCC8242CD8544CE8646CE
                8747CE8747CE8646CD8544CC8342CA7E3DD19058FCFBF9FFFFFFFFFFFFFFFFFF
                FCFAF9DAA77CC77632CA7F3ECB8040CB8040CB8040CB8040CA7F3EC77632DBA8
                7DFCFAF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E7DEDCAB83CF8B52C9
                7D3DC97D3DCF8B52DCAB83F3E8DEFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnGRAVAR_PENDENCIAClick
            end
            object btnEDITAR_PENDENCIA: TSpeedButton
              Left = 577
              Top = 7
              Width = 40
              Height = 40
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFEFDFDFBF8F1FAF4E8F7F1E0F7F1E0FAF5E8FCF9F2FEFEFDFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4EAEAD7ABE9D096EDDCA9F5
                E9C5F5EAC6EEDDABE9D297EBD8ACF9F4EAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                F3E9D3DBB76AE7C982F5E2ACFBEDC5FEF4D9FEF4DAFBEEC7F5E2AEE7CA84DBB7
                6BF3E9D3FFFEFEFFFFFFFFFFFFF6EEDDDBB566E9CA7EF0D07CF5DB91FAE5ACFC
                ECBEFCECBFFAE6AEF6DB93F1D17DE9CA7EDBB566F6EEDDFFFFFFFDFCFAE0C17F
                E6C474E8C266EDCA6FF2D27EF6DB92F8E09FF8E1A0F6DC94F2D380EDCA6FE8C3
                67E6C474E0C17FFDFCFAF4EBD7DFBB6AE0BA60E9CD8BECD190EED594F0D89BF1
                DBA0F1DBA0F0D89CEED595ECD291E9CE8CE1BB63DFBB6AF4EBD7EBD7ADD9B052
                E2C072FEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEE3C47AD9B053EBD7ADE4CD97D4A743D9B051EFDDB5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF1E1BDD9B051D4A844E5CD98E1C689D4AA4C
                D9B35CDDB966F8F2E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F4E8DEBA
                69DAB35CD4AA4BE1C689E1C68AD3AB4FD6AF56D9B35CE1C27DFEFDFAFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFCE3C583D9B35CD6AF57D3AB50E1C689E7D3A5D0A748
                D3AB50D5AE56D7B15AEAD6A9FFFFFFFFFFFFFFFFFFFFFFFFECDAB1D7B15BD5AE
                56D3AB50D0A749E7D4A5F5F1E7CBA13CD0A849D2AA4FD4AD53D6AF58F5ECD8FF
                FFFFFFFFFFF7EFDFD6B05BD4AD54D2AA4FD0A84ACCA13CF6F2E7FFFFFFDEC382
                CDA440CFA747D0A94CD2AA4FD8B769FCFBF7FDFCF9DABA6FD2AA4FD1A94CCFA7
                48CDA441DEC483FFFFFFFFFFFFFCFBF9D3B059CCA33ECEA644CFA747CFA849E0
                C688E1C98ECFA849CFA747CEA644CCA33FD3B05AFCFBF9FFFFFFFFFFFFFFFFFF
                FCFBF9DCC17DC99E33CDA440CDA541CDA542CDA542CDA541CDA440C99F33DCC1
                7EFCFBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDDEDEC483D1AD52CB
                A43ECBA43ED1AE52DEC584F3EDDFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              Visible = False
            end
            object btnCANCELA_PENDENCIA: TSpeedButton
              Left = 650
              Top = 7
              Width = 40
              Height = 40
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFDFEF0F0F8E3E3F4D8D8F0D9D9F1E4E4F5F0F0F9FDFDFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F69B9BDD7777DB8585E4A2
                A2EFA3A3EF8888E57878DC9C9CDEE8E8F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                CDCDEC4A4AC65757D68585E9A6A6F2C3C3F8C4C4F8A9A9F38787E95959D64B4B
                C7CCCCECFFFFFFFFFFFFFFFFFFD8D8F04242C65050D44343D95D5DE38282EC9C
                9CF29D9DF28484ED5F5FE34545DA5050D44242C6D8D8F0FFFFFFF9F9FC6363CD
                4646CC2D2DCB8383E0B9B9EE6262E37070E87171E86363E3BABAEE8282E02D2D
                CB4646CC6363CDF9F9FCCDCDEC3F3FC52424BD8686DCFEFEFEFFFFFFCACAF24E
                4EDC4F4FDCCACAF2FFFFFFFEFEFE8686DD2424BE4040C5CDCDED9898DB2525B5
                4141C2FCFCFEFFFFFFFFFFFFFFFFFFD0D0F2D0D0F3FFFFFFFFFFFFFFFFFFFCFC
                FE4141C22525B59898DB7F7FCE1717AC2323B57777D3F9F9FDFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF9F9FD7676D32323B51818AC8080CF7474C72727AD
                3636B63939BB6C6CCFF3F3FBFFFFFFFFFFFFFFFFFFFFFFFFF3F3FB6C6CCF3939
                BC3636B72727AD7575C77979C73232AD3535B23838B65858C5EAEAF8FFFFFFFF
                FFFFFFFFFFFFFFFFE9E9F85858C53838B63535B23232AE7979C79A9AD43131A9
                3434AE5D5DC2EEEEF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEF95D5D
                C23535AE3131AA9A9AD4E4E4F12A2AA33C3CAEEFEFF9FFFFFFFFFFFFFFFFFFD6
                D6EFD6D6EFFFFFFFFFFFFFFFFFFFEFEFF93C3CAE2A2AA3E4E4F2FFFFFF7B7BC4
                3232A66E6EC1F4F4FAFFFFFFC8C8E84444B24444B2C8C8E8FFFFFFF4F4FA6E6E
                C13232A67C7CC5FFFFFFFFFFFFF9F9FB5353B13434A55D5DB8A5A5D83E3EAC39
                39AA3939AA3E3EACA5A5D85D5DB83434A55353B1F9F9FBFFFFFFFFFFFFFFFFFF
                F9F9FB7B7BC22E2EA03939A63939A63A3AA73A3AA73939A63939A62E2EA07B7B
                C2F9F9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEED8383C55151AE3C
                3CA53C3CA55151AE8383C5DEDEEDFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnCANCELA_PENDENCIAClick
            end
            object btnPENDENCIA_CHAMAPESQUISA: TSpeedButton
              Left = 722
              Top = 7
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE PESQUISA'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFCFBFAFAF8F5F8F4EFF8F4EFFAF8F5FCFBFAFFFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F7ECE3D6E9DECDEEE7D9F5
                F0E8F6F0E8EFE7DAEADFCDECE3D6FBF9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F5F0EADDCBB3E6D9C3F1E7D9F5EEE3F9F4ECF9F4ECF6EFE4F1E8DAE7DAC5DDCB
                B3F5F1EAFFFFFFFFFFFFFFFFFFF8F4EFDDCBB2E7D8C1E8D9C0ECDFCAF1E7D7F4
                ECDFF4ECE0F1E7D7ECDFCBE8D9C0E7D8C2DDCBB2F8F4EFFFFFFFFEFDFDE1D3BE
                E5D6BEE3D3B9EBE1D1EDE4D5EDE3D4EFE3D1EFE3D1EDE4D5EDE4D6EBE1D1E3D3
                B9E5D6BEE1D3BEFEFDFDF5F0EAE1D1B8E0CFB2E5D7C0FFFFFFFFFFFFF5F0E8E9
                DAC1E9DAC1F5F0E8FFFFFFFFFFFFE5D7C0E0CFB3E1D1B8F5F0EAECE2D5DECDB1
                DECDB0E4D5BEFFFFFFFFFFFFF5F0E7E6D7BDE6D7BDF5F0E7FFFFFFFFFFFFE4D6
                BEDFCDB1DECDB2ECE2D5E9DFCFDBC9ACDECDB1E4D7C1FFFFFFFFFFFFFBF9F7F3
                EEE7F3EEE7FBF9F7FFFFFFFFFFFFE4D7C1DECDB1DBC9ACE9DFCFE9DFCFDDCCB1
                E0D0B7E5D8C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5D8
                C3E0D0B7DDCCB1E9DFCFEAE1D1DECEB4E2D5C1F2EDE5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2EDE5E2D5C1DECEB4EAE1D1EFE9DDDDCDB3
                DECEB5ECE3D5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEECE3
                D5DECEB5DDCDB3EFE8DDF8F6F3DBCAB1DDCEB4DECFB5E7DDCCFDFCFBFFFFFFFF
                FFFFFFFFFFFFFFFFFDFCFBE7DDCCDECFB5DDCEB4DBCAB1F9F7F4FFFFFFE8DECE
                DCCCB3DDCEB5DDCEB6E4D8C4FBF9F7FFFFFFFFFFFFFBF9F7E4D8C4DDCEB6DDCE
                B5DCCCB3E8DECEFFFFFFFFFFFFFCFCFCE1D3BFDCCDB4DDCEB5DDCEB6E1D3BEF8
                F5F1F8F5F1E1D3BEDDCEB6DDCEB5DCCDB4E1D3BFFDFCFCFFFFFFFFFFFFFFFFFF
                FCFCFCE7DDCDDBCAB1DCCDB5DDCEB5DED0B8DED0B8DDCEB5DCCDB5DBCAB1E7DD
                CDFDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F3EFE9DFD0E0D3BEDD
                CEB6DDCEB6E0D3BEE9DFD1F6F3F0FFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
            end
            object btnVISUALIZAR_DOCPENDENCIAS: TSpeedButton
              Left = 788
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA CHAMAR A TELA DE OP��ES DE IMPRESS�O'
              Flat = True
              ParentShowHint = False
              ShowHint = True
            end
            object btnSAIR_INSCRICAO_PENDENCIA: TSpeedButton
              Left = 850
              Top = 6
              Width = 40
              Height = 40
              Hint = 'CLIQUE PARA SAIR DA TELA DE INSCRI��O'
              Flat = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFDFEFEF1F9F7E5F6F2DBF2ECDBF2ECE6F6F2F2F9F7FDFEFEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F3A3DDCD8AD9C59FE3D3BB
                EFE4BCEFE4A1E4D48BDAC6A4DDCDEAF6F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                D0ECE45DC4A774D8BEA2EFDFBDF9EED3FDF6D4FDF6BFFAEFA4F0E076D8C05EC4
                A7D0ECE4FFFFFFFFFFFFFFFFFFDBF1EA59C5A673DEC571EBD286F2DDA2F8E8B6
                FBEFB7FBEFA4F9E988F3DE73ECD373DEC559C5A6DBF1EAFFFFFFFBFDFC74CCB3
                6BDBBF5DE0C264E6CB73EDD587F3DE95F6E39FF3E2CFF4EB8EEBD864E7CC5DE0
                C36BDBC074CCB3FBFDFCD2EDE660D0B252D4B559DBBD5FE2C564E7CB6DEBD174
                EED6A6F2E2FFFFFFF7FCFB81E4CD59DCBD53D5B560D0B2D2EDE6A4DECE4CCBAA
                4DCFAF54D6B65ADCBD62E1C567E5CA6AE7CC6CE7CDCBF5ECFFFFFFF7FCFB7ADC
                C44DD0AF4DCBAAA4DECE91D7C43EC29F5DCFB182DCC58BE0CB8DE2CE8FE4D08F
                E5D18FE5D190E4D1E7F8F3FFFFFFF6FCFA72D5BB3EC3A091D8C485D4BE46C2A1
                ECF8F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF6FCFA4DC4A485D5BF86D4BF4BC1A19FDECDC7EDE3C9EEE4CAEFE5CBEFE6CB
                F0E6CBF0E6CBEFE6EAF8F4FFFFFFFFFFFFB7E6DA4BC1A187D4BFA2DDCD44BD9B
                4CC1A151C5A556C8A959CBAC5CCCAE5DCDAF5DCDAF87D9C3FAFDFCFFFFFFB8E6
                D94DC1A145BD9CA2DDCDE5F3EF39B79346BC9B4BBF9F4FC2A253C4A555C6A756
                C7A879D2BAFAFDFCFFFFFFB7E5D94CC0A046BD9C39B794E6F4F0FFFFFF81D0BA
                3EB89545BB9A48BD9D4BBF9F4DC0A14EC1A183D3BDFEFEFEB5E4D749BE9D45BB
                9A3EB89681D0BAFFFFFFFFFFFFF9FCFB58C0A23CB79442B99744BA9946BB9A47
                BC9B47BC9B52C0A145BA9942B9983DB79458C1A3F9FCFBFFFFFFFFFFFFFFFFFF
                F9FCFB7CCDB732B28D3EB79540B79640B89640B89640B8963EB79532B28E7DCE
                B7F9FCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEEFEB83D0BA52BD9F3D
                B5943DB59452BD9F83D0BADEF0EBFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
            end
            object btnFINALIZAR_PENDENCIAS: TSpeedButton
              Left = 322
              Top = 7
              Width = 140
              Height = 40
              Hint = 'CLIQUE AQUI PARA FINALIZAR TODAS AS PEND�NCIAS'
              Caption = 'Finalizar Pend�ncias'
              Flat = True
              Glyph.Data = {
                02030000424D0203000000000000360100002800000013000000170000000100
                080000000000CC010000C30E0000C30E000040000000000000001C3404002434
                1C00242424001C3C0400243C0C00244404002C5C04003C5C240044543C005C5C
                54005C5C5C00646464006C6C6C0054743C007474740044840400747C74007C7C
                7C0084848400449404006C8C540054AC0400000000008C8C8C008C948C009494
                94009C9C9C00A4A4A400ACACAC00B4B4B4006CD404006CDC040074F404007CFC
                040084FC0C0084FC14007CDC24008CFC1C008CFC240094FC240094EC3C0094FC
                2C009CFC3C0094D45C009CF44C009CFC4400A4FC4C00A4FC5400ACFC6400B4FC
                6C00B4F47400BCF48400BCFC7C00B4C4A400ACCC9400BCCCAC00BCC4B400BCCC
                B400B4E48C00BCE49400BCDCA400C4F49400C4FC8C00C0C0C0003F3F3F3F3F3F
                191717193F3F3F3F3F3F3F3F3F003F3F3F3F3F1712111112193F3F3F3F3F3F3F
                3F003F3F3F3F19120E0C0C0E123F3F3F3F3F3F3F3F003F3F3F3F120E0C0B0B0C
                11173F3F3F3F3F3F3F003F3F3F17110C0B0A0A0B0E123F3F3F3F3F3F3F003F3F
                3F12140702010B0B0C11173F3F3F3F3F3F003F3F3F181E1E0F03100C0C0E1219
                3F3F3F3F3F003F3F3F2422231F06080C0C0C11173F3F3F3F3F003F3F2B212223
                221305170C0C0E11173F3F3F3F003F3521222323231E06090E0C0C0E12193F3F
                3F003F2B2223272726221304180E0C0C0E123F3F3F003F2926252A2F2F261F06
                08110E0C0E11173F3F0038302D232C39332E23150311110E0C0E11173F003F39
                2E28383F37312A220F0117110E0E0E1219003F3F373F3F3F3F3A30261E060917
                110E0E1117003F3F3F3F3F3F3F3F322E2315030C1712111217003F3F3F3F3F3F
                3F3F37342D2313001819171719003F3F3F3F3F3F3F3F3F3B342E231300193F3F
                3F003F3F3F3F3F3F3F3F3F3F3C3330230F011D3F3F003F3F3F3F3F3F3F3F3F3F
                3F393E31250F0D3F3F003F3F3F3F3F3F3F3F3F3F3F3F383D312320353F003F3F
                3F3F3F3F3F3F3F3F3F3F3F3F3C2A23363F003F3F3F3F3F3F3F3F3F3F3F3F3F3F
                3F3F373F3F00}
              ParentShowHint = False
              ShowHint = True
              OnClick = btnFINALIZAR_PENDENCIASClick
            end
          end
        end
      end
      object Panel1: TPanel
        Left = 5
        Top = 4
        Width = 921
        Height = 64
        BevelInner = bvLowered
        TabOrder = 1
        object gpbINSCRICAO: TGroupBox
          Left = 7
          Top = 9
          Width = 82
          Height = 46
          Caption = '[N.� Inscri��o]'
          TabOrder = 0
          object mskNUMERO_INSCRICAO: TMaskEdit
            Left = 8
            Top = 17
            Width = 68
            Height = 21
            TabOrder = 0
          end
        end
        object gpbINSCRICAO_DIGITO: TGroupBox
          Left = 90
          Top = 9
          Width = 51
          Height = 46
          Caption = '[D�gito]'
          TabOrder = 1
          object mskINSCRICAO_DIGITO: TMaskEdit
            Left = 7
            Top = 17
            Width = 37
            Height = 21
            TabOrder = 0
          end
        end
        object gpbDATA_INSCRICAO: TGroupBox
          Left = 142
          Top = 9
          Width = 90
          Height = 46
          Caption = '[Inscri��o]'
          TabOrder = 2
          object mskDATA_INSCRICAO: TMaskEdit
            Left = 8
            Top = 17
            Width = 75
            Height = 21
            EditMask = '99/99/9999'
            MaxLength = 10
            TabOrder = 0
            Text = '  /  /    '
          end
        end
        object gpbDATA_RENOVACAO: TGroupBox
          Left = 233
          Top = 9
          Width = 90
          Height = 46
          Caption = '[Renova��o]'
          TabOrder = 3
          object mskDATA_RENOVACAO: TMaskEdit
            Left = 8
            Top = 17
            Width = 75
            Height = 21
            EditMask = '99/99/9999'
            MaxLength = 10
            TabOrder = 0
            Text = '  /  /    '
          end
        end
        object gpbDATA_MATRICULA: TGroupBox
          Left = 324
          Top = 9
          Width = 90
          Height = 46
          Caption = '[Matr�cula]'
          TabOrder = 4
          object mskDATA_MATRICULA: TMaskEdit
            Left = 8
            Top = 17
            Width = 75
            Height = 21
            EditMask = '99/99/9999'
            MaxLength = 10
            TabOrder = 0
            Text = '  /  /    '
          end
        end
        object gpbDATA_READMISSAO: TGroupBox
          Left = 415
          Top = 9
          Width = 90
          Height = 46
          Caption = '[Readmiss�o]'
          TabOrder = 5
          object mskDATA_READMISSAO: TMaskEdit
            Left = 8
            Top = 17
            Width = 75
            Height = 21
            EditMask = '99/99/9999'
            MaxLength = 10
            TabOrder = 0
            Text = '  /  /    '
          end
        end
        object gpbNUMERO_PORTARIA: TGroupBox
          Left = 506
          Top = 9
          Width = 123
          Height = 46
          Caption = '[Portaria]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          object cmbNUMERO_PORTARIA: TComboBox
            Left = 7
            Top = 16
            Width = 108
            Height = 19
            Style = csOwnerDrawFixed
            ItemHeight = 13
            TabOrder = 0
            Items.Strings = (
              '031/2008'
              '081/2015')
          end
        end
        object gpbSITUACAO_INSCRICAO: TGroupBox
          Left = 630
          Top = 9
          Width = 284
          Height = 46
          Caption = '[Situa��o]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          object cmbSITUACAO_INSCRICAO: TComboBox
            Left = 7
            Top = 16
            Width = 271
            Height = 19
            Style = csOwnerDrawFixed
            ItemHeight = 13
            TabOrder = 0
          end
        end
      end
      object GroupBox104: TGroupBox
        Left = 673
        Top = 71
        Width = 256
        Height = 46
        Caption = '[Entrevistador]'
        TabOrder = 3
        Visible = False
        object lblINSCRICAO_NomeEntrevistador: TLabel
          Left = 12
          Top = 20
          Width = 164
          Height = 13
          Caption = 'lblINSCRICAO_NomeEntrevistador'
        end
      end
      object Panel2: TPanel
        Left = 5
        Top = 75
        Width = 921
        Height = 41
        BevelInner = bvLowered
        TabOrder = 2
        object lblINSCRICAO_NomeInscrito: TLabel
          Left = 12
          Top = 11
          Width = 901
          Height = 20
          AutoSize = False
          Caption = 'Nome completo do inscrito....'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Rounded MT Bold'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
  object dsSel_BeneficiosSociais: TDataSource
    DataSet = dmTriagemDeca.cdsSel_BeneficiosSociais
    Left = 807
    Top = 27
  end
  object dsCOMPOSICAO_FAMILIAR: TDataSource
    DataSet = dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar
    Left = 873
    Top = 25
  end
  object dsSel_Cadastro_Relatorios: TDataSource
    DataSet = dmTriagemDeca.cdsSel_Cadastro_Relatorio
    Left = 842
    Top = 25
  end
  object dsPENDENCIAS: TDataSource
    DataSet = dmTriagemDeca.cdsSel_PendenciaDocumentos
    Left = 905
    Top = 28
  end
end
