object Form1: TForm1
  Left = 140
  Top = 190
  Width = 1040
  Height = 461
  Caption = 'Avalia��o de desempenho'
  Color = 13488566
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 68
    Top = 16
    Width = 886
    Height = 79
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 14153215
    TabOrder = 0
    object Label1: TLabel
      Left = 40
      Top = 14
      Width = 53
      Height = 13
      Caption = 'Matricula'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 368
      Top = 7
      Width = 37
      Height = 13
      Caption = 'Nome:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNome: TLabel
      Left = 376
      Top = 23
      Width = 87
      Height = 13
      Caption = '-----------------------------'
    end
    object Label4: TLabel
      Left = 368
      Top = 41
      Width = 52
      Height = 13
      Caption = 'Unidade:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblUnidade: TLabel
      Left = 376
      Top = 57
      Width = 87
      Height = 13
      Caption = '-----------------------------'
    end
    object Label3: TLabel
      Left = 752
      Top = 16
      Width = 89
      Height = 13
      Caption = 'Ano Referencia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtBusca: TEdit
      Left = 48
      Top = 31
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 185
      Top = 27
      Width = 75
      Height = 25
      Caption = 'Localizar'
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object edtAnoRef: TEdit
      Left = 760
      Top = 32
      Width = 89
      Height = 21
      TabOrder = 2
    end
  end
  object PC1: TPageControl
    Left = 7
    Top = 105
    Width = 1003
    Height = 321
    ActivePage = TabSheet1
    BiDiMode = bdLeftToRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBiDiMode = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
    OnDrawTab = PC1DrawTab
    object TabSheet1: TTabSheet
      Caption = 'INTERA��O RELACIONAL '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnShow = TabSheet1Show
      object Panel2: TPanel
        Left = 22
        Top = 5
        Width = 951
        Height = 273
        BevelInner = bvSpace
        BevelOuter = bvLowered
        Color = 14153215
        TabOrder = 0
        object stgInteracaoRelacional: TStringGrid
          Left = 42
          Top = 9
          Width = 868
          Height = 255
          FixedColor = 13488566
          FixedCols = 0
          RowCount = 10
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnDrawCell = stgInteracaoRelacionalDrawCell
          OnSelectCell = stgInteracaoRelacionalSelectCell
          ColWidths = (
            520
            85
            85
            85
            85)
        end
        object ComboBox1: TComboBox
          Left = 346
          Top = 111
          Width = 97
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Visible = False
          OnChange = ComboBox1Change
          OnExit = ComboBox1Exit
          Items.Strings = (
            'Insuficiente'
            'Regular'
            'Bom'
            'Muito Bom'
            '�timo')
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'PARTICIPA��O '#9#9#9#9#9
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object Panel3: TPanel
        Left = 22
        Top = 5
        Width = 950
        Height = 271
        BevelInner = bvSpace
        BevelOuter = bvLowered
        Color = 14153215
        TabOrder = 0
        object stgParticipacao: TStringGrid
          Left = 41
          Top = 8
          Width = 868
          Height = 255
          FixedColor = 13488566
          FixedCols = 0
          RowCount = 10
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = stgParticipacaoClick
          OnDrawCell = stgParticipacaoDrawCell
          OnSelectCell = stgParticipacaoSelectCell
          ColWidths = (
            520
            85
            85
            85
            85)
        end
        object ComboBox2: TComboBox
          Left = 170
          Top = 96
          Width = 97
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Visible = False
          OnChange = ComboBox2Change
          OnExit = ComboBox2Exit
          Items.Strings = (
            'Insuficiente'
            'Regular'
            'Bom'
            'Muito Bom'
            '�timo')
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'APRENDIZAGEM'#9#9#9#9#9
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      object Panel5: TPanel
        Left = 18
        Top = 5
        Width = 958
        Height = 220
        BevelInner = bvSpace
        BevelOuter = bvLowered
        Color = 14153215
        TabOrder = 0
        object stgAprendizagem: TStringGrid
          Left = 45
          Top = 8
          Width = 868
          Height = 204
          FixedColor = 13488566
          FixedCols = 0
          RowCount = 8
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = stgAprendizagemClick
          OnDrawCell = stgAprendizagemDrawCell
          OnSelectCell = stgAprendizagemSelectCell
          ColWidths = (
            520
            85
            85
            85
            85)
        end
        object ComboBox3: TComboBox
          Left = 354
          Top = 47
          Width = 97
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Visible = False
          OnChange = ComboBox3Change
          OnExit = ComboBox3Exit
          Items.Strings = (
            'Insuficiente'
            'Regular'
            'Bom'
            'Muito Bom'
            '�timo')
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'EMPREENDEDORISMO '#9#9#9#9#9
      ImageIndex = 3
      object Panel6: TPanel
        Left = 22
        Top = 5
        Width = 950
        Height = 244
        BevelInner = bvSpace
        BevelOuter = bvLowered
        Color = 14153215
        TabOrder = 0
        object stgEmpreendedorismo: TStringGrid
          Left = 41
          Top = 8
          Width = 868
          Height = 230
          FixedColor = 13488566
          FixedCols = 0
          RowCount = 9
          TabOrder = 0
          OnClick = stgEmpreendedorismoClick
          OnDrawCell = stgEmpreendedorismoDrawCell
          OnSelectCell = stgEmpreendedorismoSelectCell
          ColWidths = (
            520
            85
            85
            85
            85)
        end
        object ComboBox4: TComboBox
          Left = 354
          Top = 96
          Width = 97
          Height = 28
          ItemHeight = 20
          TabOrder = 1
          Visible = False
          OnChange = ComboBox4Change
          OnExit = ComboBox4Exit
          Items.Strings = (
            'Insuficiente'
            'Regular'
            'Bom'
            'Muito Bom'
            '�timo')
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'QUALIDADE DE VIDA'
      ImageIndex = 5
      object Panel4: TPanel
        Left = 20
        Top = 4
        Width = 954
        Height = 242
        BevelInner = bvSpace
        BevelOuter = bvLowered
        Color = 14153215
        TabOrder = 0
        object stgQualidade: TStringGrid
          Left = 43
          Top = 6
          Width = 868
          Height = 229
          FixedColor = 13488566
          FixedCols = 0
          RowCount = 9
          TabOrder = 0
          OnClick = stgQualidadeClick
          OnDrawCell = stgQualidadeDrawCell
          OnSelectCell = stgQualidadeSelectCell
          ColWidths = (
            520
            85
            85
            85
            85)
        end
        object ComboBox5: TComboBox
          Left = 514
          Top = 48
          Width = 97
          Height = 28
          ItemHeight = 20
          TabOrder = 1
          Visible = False
          OnChange = ComboBox5Change
          OnExit = ComboBox5Exit
          Items.Strings = (
            'Insuficiente'
            'Regular'
            'Bom'
            'Muito Bom'
            '�timo')
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'OBSERVA��ES'
      ImageIndex = 5
      object btnSalva: TButton
        Left = 461
        Top = 244
        Width = 72
        Height = 34
        Caption = 'Salvar'
        TabOrder = 0
        Visible = False
        OnClick = btnSalvaClick
      end
      object Memo1: TMemo
        Left = 8
        Top = 8
        Width = 977
        Height = 227
        TabOrder = 1
      end
    end
  end
end
