unit uTransferenciaDados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls;

type
  TfrmTransferenciaDados = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    panACOMP_ESCOLAR1: TPanel;
    panAV_DESEMP1: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    SpeedButton1: TSpeedButton;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    GroupBox2: TGroupBox;
    Panel37: TPanel;
    Panel38: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    Panel41: TPanel;
    panACOMP_ESCOLAR2: TPanel;
    panAV_DESEMP2: TPanel;
    Panel44: TPanel;
    Panel45: TPanel;
    Panel46: TPanel;
    Panel47: TPanel;
    Panel48: TPanel;
    Panel49: TPanel;
    Panel50: TPanel;
    Panel51: TPanel;
    Panel52: TPanel;
    Panel53: TPanel;
    Panel54: TPanel;
    Panel55: TPanel;
    Panel56: TPanel;
    Panel57: TPanel;
    Panel58: TPanel;
    Panel59: TPanel;
    Panel60: TPanel;
    Panel61: TPanel;
    Panel62: TPanel;
    Panel63: TPanel;
    Panel64: TPanel;
    Panel65: TPanel;
    Panel66: TPanel;
    Panel67: TPanel;
    Panel68: TPanel;
    Panel69: TPanel;
    Panel70: TPanel;
    Panel71: TPanel;
    Panel72: TPanel;
    Panel73: TPanel;
    Label1: TLabel;
    Memo1: TMemo;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciaDados: TfrmTransferenciaDados;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmTransferenciaDados.SpeedButton1Click(Sender: TObject);
begin

  //Totalizar os registros de...
  //CADASTRO

  try
    with dmDeca.cdsSel_Cadastro_Desligados_TRF do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Open;

      Panel13.Caption := 'Total de ' + IntToStr(dmDeca.cdsSel_Cadastro_Desligados_TRF.RecordCount) + ' registros.';

      //Inserir os dados selecionados no banco de dados DecaInativos, tabela CADASTRO
      while not dmDeca.cdsSel_Cadastro_Desligados_TRF.eof do
      begin

        {
        with dmDeca.cdsIns_CadastroInativos do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value         := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('cod_matricula').AsString);
          Params.ParamByName ('@pe_nom_nome').Value              := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('nom_nome').AsString);
          Params.ParamByName ('@pe_num_prontuario').Value        := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_prontuario').AsInteger;
          Params.ParamByName ('@pe_num_cartaosias').Value        := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_cartaosias').AsString);
          Params.ParamByName ('@pe_dat_nascimento').Value        := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('dat_nascimento').AsDateTime;
          Params.ParamByName ('@pe_ind_sexo').Value              := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_sexo').AsString);
          Params.ParamByName ('@pe_nom_endereco').Value          := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('nom_endereco').AsString);
          Params.ParamByName ('@pe_nom_complemento').Value       := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('nom_complemento').AsString);
          Params.ParamByName ('@pe_nom_bairro').Value            := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('nom_bairro').AsString);
          Params.ParamByName ('@pe_num_cep').Value               := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_cep').AsString);
          Params.ParamByName ('@pe_num_telefone').Value          := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_telefone').AsString);
          Params.ParamByName ('@pe_val_peso').Value              := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('val_peso').Value;
          Params.ParamByName ('@pe_val_altura').Value            := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('val_altura').Value;
          Params.ParamByName ('@pe_dsc_etnia').Value             := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('dsc_etnia').AsString);
          Params.ParamByName ('@pe_num_calcado').Value           := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_calcado').AsString);
          Params.ParamByName ('@pe_ind_tam_camisa').Value        := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_tam_camisa').AsString);
          Params.ParamByName ('@pe_ind_pontuacao_triagem').Value := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_pontuacao_triagem').AsInteger;
          Params.ParamByName ('@pe_dsc_periodo').Value           := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('dsc_periodo').AsString);
          Params.ParamByName ('@pe_ind_modalidade').Value        := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_modalidade').Value;
          Params.ParamByName ('@pe_flg_projeto_vida').Value      := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('flg_projeto_vida').Value;
          Params.ParamByName ('@pe_cod_unidade').Value           := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('cod_unidade').Value;
          Params.ParamByName ('@pe_num_secao').Value             := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_secao').AsString);
          Params.ParamByName ('@pe_dsc_saude').Value             := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('dsc_saude').AsString);
          Params.ParamByName ('@pe_dat_admissao').Value          := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('dat_admissao').AsDateTime;
          Params.ParamByName ('@pe_ind_tam_calca').Value         := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_tam_calca').AsString);
          Params.ParamByName ('@pe_ind_tipo_sanguineo').Value    := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_tipo_sanguineo').AsString);
          Params.ParamByName ('@pe_ind_status').Value            := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('ind_status').Value;
          Params.ParamByName ('@pe_num_cota_passes').Value       := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_cota_passes').Value;
          Params.ParamByName ('@pe_log_cod_usuario').Value       := 15; //dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('log_cod_usuario').Value;
          Params.ParamByName ('@pe_cod_turma').Value             := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('cod_turma').AsInteger;
          Params.ParamByName ('@pe_num_cpf').Value               := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_cpf').AsString);
          Params.ParamByName ('@pe_num_rg').Value                := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_rg').AsString);
          Params.ParamByName ('@pe_num_certidao').Value          := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('num_certidao').AsString);
          Params.ParamByName ('@pe_dsc_rg_escolar').Value        := GetValue(dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('dsc_rg_escolar').AsString);
          Execute;
        end;



        //********************************************************************************************************************************************************//
        //Seleciona os dados do Acompanhamento Escolar e insere os dados referentes na tabela ACOMPESCOLAR
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
          Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_num_ano_letivo').Value        := Null;
          Params.ParamByName ('@pe_num_bimestre').Value          := Null;
          Params.ParamByName ('@pe_cod_unidade').Value           := Null;
          Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
          Open;

          while not dmDeca.cdsSel_AcompEscolar.eof do
          begin
            with dmDeca.cdsInc_AcompEscolar_TRF do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value       := GetValue(dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_matricula').AsString);
              Params.ParamByName ('@pe_num_ano_letivo').Value      := dmDeca.cdsSel_AcompEscolar.FieldByName ('num_ano_letivo').Value;
              Params.ParamByName ('@pe_num_bimestre').Value        := dmDeca.cdsSel_AcompEscolar.FieldByName ('num_bimestre').Value;
              Params.ParamByName ('@pe_cod_escola').Value          := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
              Params.ParamByName ('@pe_cod_id_serie').Value        := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_serie').Value;
              Params.ParamByName ('@pe_ind_frequencia').Value      := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value;
              Params.ParamByName ('@pe_ind_aproveitamento').Value  := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value;
              Params.ParamByName ('@pe_ind_situacao').Value        := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_situacao').Value;
              Params.ParamByName ('@pe_ind_periodo_escolar').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value;
              Params.ParamByName ('@pe_dsc_observacoes').Value     := GetValue(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString);
              Params.ParamByName ('@pe_cod_usuario').Value         := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_usuario').Value;
              Params.ParamByName ('@pe_nom_turma').Value           := GetValue(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').AsString);
              Execute;
            end;

            dmDeca.cdsSel_AcompEscolar.Next;

          end;


        end;
        //********************************************************************************************************************************************************//
        }

        //********************************************************************************************************************************************************//
        //Seleciona os dados do Histórico de Intervenções e insere os dados referentes na tabela CADASTRO_HISTORICO
        with dmDeca.cdsSel_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_cod_id_historico').Value := Null;
          Params.ParamByName ('@pe_cod_usuario').Value      := Null;
          Open;

          while not dmDeca.cdsSel_Cadastro_Historico.eof do
          begin

            with dmDeca.cdsInc_Cadastro_Historico_TRF do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value       := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_dat_historico').Value       := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_historico').AsDateTime;
              Params.ParamByName ('@pe_ind_tipo_historico').Value  := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_tipo_historico').Value;
              Params.ParamByName ('@pe_dsc_tipo_historico').Value  := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_tipo_historico').Value;
              Params.ParamByName ('@pe_cod_unidade').Value         := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('cod_unidade').Value;
              Params.ParamByName ('@pe_dsc_ocorrencia').Value      := GetValue(dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_ocorrencia').AsString);

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_projeto').IsNull then Params.ParamByName ('@pe_dsc_projeto').Value := Null
              else Params.ParamByName ('@pe_dsc_projeto').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_projeto').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_motivo1').IsNull then Params.ParamByName ('@pe_ind_motivo1').Value := Null
              else Params.ParamByName ('@pe_ind_motivo1').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_motivo1').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_motivo2').IsNull then Params.ParamByName ('@pe_ind_motivo2').Value := Null
              else Params.ParamByName ('@pe_ind_motivo2').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_motivo2').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_funcao').IsNull then Params.ParamByName ('@pe_dsc_funcao').Value := Null
              else Params.ParamByName ('@pe_dsc_funcao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_funcao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_comentario').IsNull then Params.ParamByName ('@pe_dsc_comentario').Value := Null
              else Params.ParamByName ('@pe_dsc_comentario').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_comentario').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_final_suspensao').IsNull then Params.ParamByName ('@pe_dat_final_suspensao').Value := Null
              else Params.ParamByName ('@pe_dat_final_suspensao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_final_suspensao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_materia').IsNull then Params.ParamByName ('@pe_dsc_materia').Value := Null
              else Params.ParamByName ('@pe_dsc_materia').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_materia').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_materia').IsNull then Params.ParamByName ('@pe_dsc_materia').Value := Null
              else Params.ParamByName ('@pe_dsc_materia').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_materia').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_motivo_transf').IsNull then Params.ParamByName ('@pe_ind_motivo_transf').Value := Null
              else Params.ParamByName ('@pe_ind_motivo_transf').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_motivo_transf').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_unidade').IsNull then Params.ParamByName ('@pe_orig_unidade').Value := Null
              else Params.ParamByName ('@pe_orig_unidade').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_unidade').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_ccusto').IsNull then Params.ParamByName ('@pe_orig_unidade').Value := Null
              else Params.ParamByName ('@pe_orig_ccusto').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_ccusto').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_secao').IsNull then Params.ParamByName ('@pe_orig_secao').Value := Null
              else Params.ParamByName ('@pe_orig_secao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_secao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_num_secao').IsNull then Params.ParamByName ('@pe_orig_num_secao').Value := Null
              else Params.ParamByName ('@pe_orig_num_secao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_num_secao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_banco').IsNull then Params.ParamByName ('@pe_orig_banco').Value := Null
              else Params.ParamByName ('@pe_orig_banco').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_banco').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_unidade').IsNull then Params.ParamByName ('@pe_dest_unidade').Value := Null
              else Params.ParamByName ('@pe_dest_unidade').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_unidade').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_ccusto').IsNull then Params.ParamByName ('@pe_dest_ccusto').Value := Null
              else Params.ParamByName ('@pe_dest_ccusto').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_ccusto').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_secao').IsNull then Params.ParamByName ('@pe_dest_secao').Value := Null
              else Params.ParamByName ('@pe_dest_secao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_secao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_num_secao').IsNull then Params.ParamByName ('@pe_dest_num_secao').Value := Null
              else Params.ParamByName ('@pe_dest_num_secao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_num_secao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_banco').IsNull then Params.ParamByName ('@pe_dest_banco').Value := Null
              else Params.ParamByName ('@pe_dest_banco').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_banco').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('hora_inicio').IsNull then Params.ParamByName ('@pe_hora_inicio').Value := Null
              else Params.ParamByName ('@pe_hora_inicio').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('hora_inicio').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_num_secao').IsNull then Params.ParamByName ('@pe_orig_num_secao').Value := Null
              else Params.ParamByName ('@pe_orig_num_secao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_num_secao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('hora_final').IsNull then Params.ParamByName ('@pe_hora_final').Value := Null
              else Params.ParamByName ('@pe_hora_final').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('hora_final').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('flg_passe').IsNull then Params.ParamByName ('@pe_flg_passe').Value := Null
              else Params.ParamByName ('@pe_flg_passe').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('flg_passe').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_cotas_passe').IsNull then Params.ParamByName ('@pe_num_cotas_passe').Value := Null
              else Params.ParamByName ('@pe_num_cotas_passe').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_cotas_passe').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_funcao').IsNull then Params.ParamByName ('@pe_orig_funcao').Value := Null
              else Params.ParamByName ('@pe_orig_funcao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('orig_funcao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_funcao').IsNull then Params.ParamByName ('@pe_dest_funcao').Value := Null
              else Params.ParamByName ('@pe_dest_funcao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dest_funcao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_curso_inicio').IsNull then Params.ParamByName ('@pe_dat_curso_inicio').Value := Null
              else Params.ParamByName ('@pe_dat_curso_inicio').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_curso_inicio').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_curso_duracao').IsNull then Params.ParamByName ('@pe_num_curso_duracao').Value := Null
              else Params.ParamByName ('@pe_num_curso_duracao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_curso_duracao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_estagio_inicio').IsNull then Params.ParamByName ('@pe_dat_estagio_inicio').Value := Null
              else Params.ParamByName ('@pe_dat_estagio_inicio').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dat_estagio_inicio').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_tipo').IsNull then Params.ParamByName ('@pe_ind_tipo').Value := Null
              else Params.ParamByName ('@pe_ind_tipo').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_tipo').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_classificacao').IsNull then Params.ParamByName ('@pe_ind_classificacao').Value := Null
              else Params.ParamByName ('@pe_ind_classificacao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('ind_classificacao').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('qtd_participantes').IsNull then Params.ParamByName ('@pe_qtd_participantes').Value := Null
              else Params.ParamByName ('@pe_qtd_participantes').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('qtd_participantes').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('qtd_participantes').IsNull then Params.ParamByName ('@pe_qtd_participantes').Value := Null
              else Params.ParamByName ('@pe_qtd_participantes').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('qtd_participantes').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_horas').IsNull then Params.ParamByName ('@pe_num_horas').Value := Null
              else Params.ParamByName ('@pe_num_horas').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_horas').Value;

              if dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_estagio_duracao').IsNull then Params.ParamByName ('@pe_num_estagio_duracao').Value := Null
              else Params.ParamByName ('@pe_num_estagio_duracao').Value := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('num_estagio_duracao').Value;
              
              Params.ParamByName ('@pe_log_cod_usuario').Value     := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('log_cod_usuario').Value;
              Execute;
            end;

            dmDeca.cdsSel_Cadastro_Historico.Next;

          end;

        end;

        dmDeca.cdsSel_Cadastro_Desligados_TRF.Next;

      end;

      //Ao inserir, comparar a quantidade de registros...
      //Caso os valores sejam os mesmos, excluir os dados da tabela CADASTRO do DECA(Oficial)




    end;
  except
    begin
      Memo1.Lines.Add (dmDeca.cdsSel_Cadastro_Desligados_TRF.FieldByName('cod_matricula').Value);
      dmDeca.cdsSel_Cadastro_Desligados_TRF.Next;

    end;
  end;



end;

end.
