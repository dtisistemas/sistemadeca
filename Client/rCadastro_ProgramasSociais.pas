unit rCadastro_ProgramasSociais;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelCadastro_ProgramasSociais = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText1: TQRDBText;
    DetailBand1: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRGroup3: TQRGroup;
    QRGroup2: TQRGroup;
    QRDBText2: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText8: TQRDBText;
    QRImage1: TQRImage;
    QRDBText4: TQRDBText;
    QRGroup4: TQRGroup;
    QRLabel4: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText11: TQRDBText;
    procedure QRDBText9Print(sender: TObject; var Value: String);
  private

  public

  end;

var
  relCadastro_ProgramasSociais: TrelCadastro_ProgramasSociais;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TrelCadastro_ProgramasSociais.QRDBText9Print(sender: TObject;
  var Value: String);
begin
if dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName ('vStatus').AsString = 'Prioritário' then QRDBText9.Font.Color := clRed;
if dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName ('vStatus').AsString = 'Ativo' then QRDBText9.Font.Color := clGreen;
if dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName ('vStatus').AsString = 'Desligado' then QRDBText9.Font.Color := clBlack;
if dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName ('vStatus').AsString = 'Cadastrado' then QRDBText9.Font.Color := clBlue;
end;

end.
