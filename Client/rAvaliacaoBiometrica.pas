unit rAvaliacaoBiometrica;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelAvaliacaoBiometrica = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    lbUnidade: TQRLabel;
    QRImage1: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRShape7: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRDBText6: TQRDBText;
    QRLabel1: TQRLabel;
    QRSysData1: TQRSysData;
    lbTotalPaginas: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel12: TQRLabel;
    QRExpr1: TQRExpr;
  private

  public

  end;

var
  relAvaliacaoBiometrica: TrelAvaliacaoBiometrica;

implementation

uses uDM, uOpcoesImpressaoBiometricos;

{$R *.DFM}

end.

