unit uEmiteRelatorioEspecial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ExtCtrls, ComOBJ;

type
  TfrmEmiteRelatorioEspecial = class(TForm)
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    txtCorpoRelatorio: TMemo;
    txtData: TMaskEdit;
    edOrgao: TEdit;
    Label4: TLabel;
    edResponsavel: TEdit;
    SaveDialog1: TSaveDialog;
    Label5: TLabel;
    edNomeArquivo: TEdit;
    Label6: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure AtualizaCamposBotoesRE (Modo : String);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteRelatorioEspecial: TfrmEmiteRelatorioEspecial;
  vvMS_WORD : Variant;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmEmiteRelatorioEspecial.AtualizaCamposBotoesRE(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

    edOrgao.Clear;
    edOrgao.Enabled := False;
    txtData.Clear;
    txtData.Enabled := False;
    edResponsavel.Clear;
    edResponsavel.Enabled := False;
    txtCorpoRelatorio.Clear;
    txtCorpoRelatorio.Enabled := False;

  end
  else if Modo = 'Novo' then
  begin

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;

    edOrgao.Enabled := True;
    txtData.Enabled := True;
    edResponsavel.Enabled := True;
    txtCorpoRelatorio.Enabled := True;

  end;
end;

procedure TfrmEmiteRelatorioEspecial.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoesRE ('Padr�o');
end;

procedure TfrmEmiteRelatorioEspecial.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoesRE ('Padr�o');
end;

procedure TfrmEmiteRelatorioEspecial.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEmiteRelatorioEspecial.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmEmiteRelatorioEspecial.Close;
  end;
end;

procedure TfrmEmiteRelatorioEspecial.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoesRE ('Novo');
  edResponsavel.Text := vvNOM_USUARIO;
  txTData.Text := DateToStr(Date());
end;

procedure TfrmEmiteRelatorioEspecial.btnSalvarClick(Sender: TObject);
begin

  if Application.MessageBox ('Deseja gravar os dados do relat�rio de encaminhamento?',
                             'Sistema Deca - Relat�rio de Encaminhamento',
                             MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin

    //Criar um relat�rio no MS-Word a partir dos dados informados no formul�rio
    vvMS_WORD := CreateOleObject ('Word.Basic');
    vvMS_WORD.AppShow;       //Mostra o MS-Word
    vvMS_WORD.FileNew;       //Inicia um novo documento
    vvMS_WORD.LeftPara;
    vvMS_WORD.InsertPicture (ExtractFilePath(Application.Exename) + '\FUNDHAS2005.JPG');
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.FontSize(10);  //Muda o tamanho da fonte
    vvMS_WORD.CenterPara;    //Alinhar ao centro
    vvMS_WORD.Bold;
    vvMS_WORD.Insert ('RELAT�RIO DE ENCAMINHAMENTO' + #13);     //T�tulo do Documento a ser gerado
    vvMS_WORD.Insert ('Emitido pelo Sistema DECA - FUNDHAS' + #13 + #13);     //Segunda linha do t�tulo
    vvMS_WORD.LeftPara;
    vvMS_WORD.Bold (False);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert('DE : ' + UPPERCASE(edResponsavel.Text));
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert('PARA : '+ UPPERCASE(edOrgao.Text));
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert('Em: ' + txtData.Text);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert(#13+#9);
    vvMS_WORD.Insert(UPPERCASE(txtCorpoRelatorio.Text));
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.Insert(#13);
    vvMS_WORD.FileSaveAs (ExtractFilePath(Application.Exename) + edNomeArquivo.Text+'.doc');

    AtualizaCamposBotoesRE ('Padr�o');

  end;
end;

procedure TfrmEmiteRelatorioEspecial.btnSairClick(Sender: TObject);
begin
  frmEmiteRelatorioEspecial.Close;
end;

end.
