unit uObservacaoAprendizagem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, Grids, DBGrids, ComCtrls, ExtCtrls, Db;

type
  TfrmObservacaoAprendizagem = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Label2: TLabel;
    edAspectosGerais: TMemo;
    Label3: TLabel;
    edExpressaoOral: TMemo;
    Label6: TLabel;
    edMatematica: TMemo;
    Label7: TLabel;
    edObservacoes: TMemo;
    Panel2: TPanel;
    dbgResultado: TDBGrid;
    Panel3: TPanel;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    dsResultado: TDataSource;
    btnImprimir: TBitBtn;
    GroupBox3: TGroupBox;
    lbNascimento: TLabel;
    GroupBox4: TGroupBox;
    lbUnidade: TLabel;
    GroupBox5: TGroupBox;
    lbPeriodoUnidade: TLabel;
    GroupBox6: TGroupBox;
    lbPeriodoEscola: TLabel;
    GroupBox7: TGroupBox;
    txtAsocial: TEdit;
    GroupBox8: TGroupBox;
    lbEscola: TLabel;
    GroupBox9: TGroupBox;
    lbSerie: TLabel;
    Label4: TLabel;
    cbPesquisa: TComboBox;
    edValor: TEdit;
    procedure AtualizaCamposBotoesOA (Modo: String);
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dbgResultadoDblClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure edValorKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmObservacaoAprendizagem: TfrmObservacaoAprendizagem;
  vvCOD_ID_OBS : Integer;

implementation

uses uFichaPesquisa, uDM, uFichaCrianca, uPrincipal, uUtil,
  rObservacaoAprendizagem, rObservacaoAprendizagem2;

{$R *.DFM}

{ TfrmObservacaoAprendizagem }

procedure TfrmObservacaoAprendizagem.AtualizaCamposBotoesOA(Modo: String);
begin

  if Modo = 'Padr�o' then
  begin

    txtMatricula.Clear;
    lbNome.Caption := '';
    lbNascimento.Caption := '';
    lbUnidade.Caption := '';
    lbPeriodoUnidade.Caption := '';
    txtAsocial.Clear;
    lbEscola.Caption := '';
    lbSerie.Caption := '';
    lbPeriodoEscola.Caption := '';
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;
    Panel1.Enabled := False;

    PageControl1.ActivePageIndex := 0;
    edAspectosGerais.Clear;
    edExpressaoOral.Clear;
    edMatematica.Clear;
    edObservacoes.Clear;

    btnNovo.Enabled := True;
    btnAlterar.Enabled := False;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin

    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    lbNascimento.Caption := '';
    lbUnidade.Caption := '';
    lbPeriodoUnidade.Caption := '';
    txtAsocial.Clear;
    lbEscola.Caption := '';
    lbSerie.Caption := '';
    lbPeriodoEscola.Caption := '';

    edAspectosGerais.Clear;
    edExpressaoOral.Clear;
    edMatematica.Clear;
    edObservacoes.Clear;

    Panel1.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;

  end;

end;

procedure TfrmObservacaoAprendizagem.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoesOA ('Padr�o');

  try
    with dmDeca.cdsSel_ObsAprendizagem do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_observacao').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_cod_escola').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Open;
      dbgResultado.Refresh;
    end;
  except end;

end;

procedure TfrmObservacaoAprendizagem.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        lbNascimento.Caption := DateToStr(FieldByName('dat_nascimento').AsDateTime);
        lbUnidade.Caption := FieldByName('vUnidade').AsString;
        lbPeriodoUnidade.Caption := FieldByName('dsc_periodo').AsString;

        //lbEscola.Caption := FieldByName ('nom_escola').AsString;
        //lbSerie.Caption := FieldByName ('dsc_escola_serie').AsString;
        //lbPeriodoEscola.Caption := FieldByName ('dsc_escola_periodo').AsString;

        txtASocial.SetFocus;

        //Carrega as Observa��es de Aprendizagem da Crian�a/Adolescente
        //try
        //  with dmDeca.cdsSel_Psicopedagogico do
        //  begin
        //    Close;
        //    Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
        //    Open;
        //    dbgAcompanhamentos.Refresh;
        //  end;
        //except end;

      end;
    except end;
  end;
end;

procedure TfrmObservacaoAprendizagem.btnSairClick(Sender: TObject);
begin
  frmObservacaoAprendizagem.Close;
end;

procedure TfrmObservacaoAprendizagem.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoesOA ('Novo');
end;

procedure TfrmObservacaoAprendizagem.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoesOA ('Padr�o');
end;

procedure TfrmObservacaoAprendizagem.btnSalvarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_ObsAprendizagem do
    begin

      Close;
      Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
      Params.ParamByName('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').AsInteger;
      Params.ParamByName('@pe_cod_escola').Value := Null; //dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_escola').Value;
      Params.ParamByName('@pe_dsc_aspectosgerais').Value := GetValue(edAspectosGerais.Text);
      Params.ParamByName('@pe_dsc_expressaoral').Value := GetValue(edExpressaoOral.Text);
      Params.ParamByName('@pe_dsc_matematica').Value := GetValue(edMatematica.Text);
      Params.ParamByName('@pe_dsc_especifica').Value := GetValue(edObservacoes.Text);
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;

      //Atualiza o Grid
      with dmDeca.cdsSel_ObsAprendizagem do
      begin

        Close;
        Params.ParamByName('@pe_cod_id_observacao').Value := Null;
        Params.ParamByName('@pe_cod_matricula').Value := Null;
        Params.ParamByName('@pe_cod_escola').Value := Null;
        Params.ParamByName('@pe_cod_unidade').Value := Null;
        Open;

        dbgResultado.Refresh;

      end;

    end;
  except end;

  AtualizaCamposBotoesOA ('Padr�o');
  btnImprimir.Enabled := True;

end;

procedure TfrmObservacaoAprendizagem.btnImprimirClick(Sender: TObject);
begin
  {try
    with dmDeca.cdsSel_ObsAprendizagem do
    begin

      Close;
      Params.ParamByName('@pe_cod_id_observacao').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := txtMatricula.Text;
      Params.ParamByName('@pe_cod_escola').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Open;

      Application.CreateForm (TrelObservacaoAprendizagem, relObservacaoAprendizagem);
      relObservacaoAprendizagem.Preview;
      relObservacaoAprendizagem.Free;
      Application.CreateForm (TrelObservacaoAprendizagem2, relObservacaoAprendizagem2);
      relObservacaoAprendizagem2.Preview;
      relObservacaoAprendizagem2.Free;

      AtualizaCamposBotoesOA ('Padr�o');


      try
        with dmDeca.cdsSel_ObsAprendizagem do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_observacao').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_cod_escola').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value := Null;
          Open;

          dbgResultado.Refresh;
        end;
      except end;

    end;
  except end;}

  Application.CreateForm (TrelObservacaoAprendizagem, relObservacaoAprendizagem);

  relObservacaoAprendizagem.lbNome.Caption := lbNome.Caption;
  relObservacaoAprendizagem.lbNascimento.Caption := lbNascimento.Caption;
  //relObservacaoAprendizagem.lbDivisao.Caption := ;
  relObservacaoAprendizagem.lbUnidade.Caption := lbUnidade.Caption;
  relObservacaoAprendizagem.lbAssistenteSocial.Caption := txtAsocial.Text;
  relObservacaoAprendizagem.lbPeriodo.Caption := lbPeriodoUnidade.Caption;
  relObservacaoAprendizagem.lbEscola.Caption := lbescola.Caption;
  relObservacaoAprendizagem.lbSerie.Caption := lbSerie.Caption;
  relObservacaoAprendizagem.lbPeriodoEscola.Caption := lbPeriodoEscola.Caption;
  relObservacaoAprendizagem.edAspectosGerais.Lines.Text := edAspectosGerais.Text;
  relObservacaoAprendizagem.edExpressaoOral.Lines.Text := edExpressaoOral.Text;

  relObservacaoAprendizagem.Preview;
  relObservacaoAprendizagem.Free;

  //Application.CreateForm (TrelObservacaoAprendizagem2, relObservacaoAprendizagem2);
  //relObservacaoAprendizagem2.Preview;
  //relObservacaoAprendizagem2.Free;

  AtualizaCamposBotoesOA ('Padr�o');

end;
procedure TfrmObservacaoAprendizagem.dbgResultadoDblClick(Sender: TObject);
begin

  with dmDeca.cdsSel_Cadastro_Move do
  begin
    Close;
    Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
    Params.ParamByName('@pe_Matricula_Atual').AsString := dmDeca.cdsSel_ObsAprendizagem.FieldByName('cod_matricula').Value;
    Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
    Open;
    txtMatricula.Text := dmDeca.cdsSel_ObsAprendizagem.FieldByName('cod_matricula').AsString;
    lbNome.Caption := FieldByName('nom_nome').AsString;

    lbNascimento.Caption := DateToStr(FieldByName('dat_nascimento').AsDateTime);
    lbUnidade.Caption := FieldByName('vUnidade').AsString;
    lbPeriodoUnidade.Caption := FieldByName('dsc_periodo').AsString;

    //lbEscola.Caption := FieldByName ('nom_escola').AsString;
    //lbSerie.Caption := FieldByName ('dsc_escola_serie').AsString;
    //lbPeriodoEscola.Caption := FieldByName ('dsc_escola_periodo').AsString;

    PageControl1.ActivePageIndex := 0;

    btnAlterar.Enabled := True;

    Panel1.Enabled := True;
    edAspectosGerais.Enabled := True;
    edExpressaoOral.Enabled := True;
    edMatematica.Enabled := True;
    edObservacoes.Enabled := True;

    //edAspectosGerais.SetFocus;

    edAspectosGerais.Text := dmDeca.cdsSel_ObsAprendizagem.FieldByName('dsc_aspectosgerais').Value;
    edExpressaoOral.Text := dmDeca.cdsSel_ObsAprendizagem.FieldByName('dsc_expressaoral').Value;
    edMatematica.Text := dmDeca.cdsSel_ObsAprendizagem.FieldByName('dsc_matematica').Value;
    edObservacoes.Text := dmDeca.cdsSel_ObsAprendizagem.FieldByName('dsc_especifica').Value;
  end;

end;

procedure TfrmObservacaoAprendizagem.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_ObsAprendizagem do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_observacao').AsInteger := dmDeca.cdsSel_ObsAprendizagem.FieldByName('cod_id_observacao').AsInteger;
      Params.ParamByName('@pe_dsc_aspectos_gerais').Value := GetValue(edAspectosGerais.Text);
      Params.ParamByName('@pe_dsc_expressaoral').Value := GetValue(edExpressaoOral.Text);
      Params.ParamByName('@pe_dsc_matematica').Value := GetValue(edMatematica.Text);
      Params.ParamByName('@pe_dsc_especifica').Value := GetValue(edObservacoes.Text);
      Execute;

      with dmDeca.cdsSel_ObsAprendizagem do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_observacao').Value := Null;
        Params.ParamByName('@pe_cod_matricula').Value := Null;
        Params.ParamByName('@pe_cod_escola').Value := Null;
        Params.ParamByName('@pe_cod_unidade').Value := Null;
        Params.ParamByName('@pe_nom_nome').Value := Null;
        Open;
        dbgResultado.Refresh;
      end;

      AtualizaCamposBotoesOA('Padr�o');

    end;
  except end;
end;

procedure TfrmObservacaoAprendizagem.edValorKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
  begin
    if cbPesquisa.ItemIndex = 0 then    //Pesquisa por Matr�cula
    begin
      //Carrega as Observa��es de Aprendizagem da Crian�a/Adolescente
      try
        with dmDeca.cdsSel_ObsAprendizagem do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_observacao').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := Trim(edValor.Text);
          Params.ParamByName('@pe_cod_escola').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Open;
          dbgResultado.Refresh;
        end;
      except end;
    end
    else if cbPesquisa.ItemIndex = 1 then
    begin
      //Carrega as Observa��es de Aprendizagem da Crian�a/Adolescente
      try
        with dmDeca.cdsSel_ObsAprendizagem do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_observacao').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_cod_escola').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value := Null;
          Params.ParamByName('@pe_nom_nome').AsString := Trim(edValor.Text) + '%';
          Open;
          dbgResultado.Refresh;
        end;
      except end;
    end
    else if cbPesquisa.ItemIndex = 2 then
    begin
      //Carrega as Observa��es de Aprendizagem da Crian�a/Adolescente
      try
        with dmDeca.cdsSel_ObsAprendizagem do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_observacao').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_cod_escola').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Open;
          dbgResultado.Refresh;
        end;
      except end;
    end;
end;
end;

end.
