object frmMapeamentoXRiscos: TfrmMapeamentoXRiscos
  Left = 457
  Top = 199
  Width = 882
  Height = 737
  BorderIcons = [biSystemMenu]
  Caption = '[Sistema Deca] - Mapeamento de Vulnerabilidades X Riscos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 874
    Height = 691
    Align = alClient
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 8
      Top = 79
      Width = 860
      Height = 605
      ActivePage = TabSheet2
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = '[Selecione a crian�a/adolescente]'
        object btnRegistroAtendimento: TSpeedButton
          Left = 641
          Top = 535
          Width = 208
          Height = 39
          Caption = 'LAN�AR INDICADORES >>>'
          Flat = True
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 3
          Width = 847
          Height = 527
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end
            item
              Expanded = False
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = '[Indicadores]'
        ImageIndex = 1
      end
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 7
      Width = 860
      Height = 69
      Caption = '[Identifica��o da Unidade]'
      TabOrder = 1
      object Label1: TLabel
        Left = 21
        Top = 21
        Width = 43
        Height = 13
        Caption = 'DIVIS�O'
      end
      object Label2: TLabel
        Left = 413
        Top = 21
        Width = 49
        Height = 13
        Caption = 'UNIDADE'
      end
      object Edit1: TEdit
        Left = 21
        Top = 36
        Width = 389
        Height = 21
        TabOrder = 0
        Text = 'Edit1'
      end
      object Edit2: TEdit
        Left = 411
        Top = 36
        Width = 440
        Height = 21
        TabOrder = 1
        Text = 'Edit1'
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 1320
    Top = 16
    object MapeamentoxRiscos1: TMenuItem
      Caption = 'Mapeamento x Riscos'
      object LanamentodeDados1: TMenuItem
        Caption = 'Lan�amento de Dados'
      end
      object ConsultaLanamentos1: TMenuItem
        Caption = 'Consulta Lan�amentos'
      end
    end
  end
end
