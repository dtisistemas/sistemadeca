unit rObservacaoAprendizagem;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelObservacaoAprendizagem = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    lbDivisao: TQRLabel;
    QRLabel13: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel14: TQRLabel;
    edAspectosGerais: TQRRichText;
    edExpressaoOral: TQRRichText;
    lbNome: TQRLabel;
    lbNascimento: TQRLabel;
    lbUnidade: TQRLabel;
    lbPeriodo: TQRLabel;
    lbAssistenteSocial: TQRLabel;
    lbEscola: TQRLabel;
    lbSerie: TQRLabel;
    lbPeriodoEscola: TQRLabel;
  private

  public

  end;

var
  relObservacaoAprendizagem: TrelObservacaoAprendizagem;

implementation

uses uDM, uObservacaoAprendizagem;

{$R *.DFM}

end.
