unit rFolhaFrequencia;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelFolhaFrequencia = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRLabel2: TQRLabel;
    lbMesAno: TQRLabel;
    DetailBand1: TQRBand;
    QRShape31: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRShape42: TQRShape;
    QRShape43: TQRShape;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape56: TQRShape;
    QRShape57: TQRShape;
    QRShape58: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    QRShape66: TQRShape;
    QRShape67: TQRShape;
    QRShape68: TQRShape;
    QRShape69: TQRShape;
    QRShape70: TQRShape;
    QRShape71: TQRShape;
    QRShape72: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRLabel1: TQRLabel;
    lbTurma: TQRLabel;
    QRShape74: TQRShape;
    QRDBText35: TQRDBText;
    QRShape77: TQRShape;
    QRDBText36: TQRDBText;
    QRGroup1: TQRGroup;
    SummaryBand1: TQRBand;
    QRShape5: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel7: TQRLabel;
    QRExpr3: TQRExpr;
    QRLabel43: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel44: TQRLabel;
    QRExpr2: TQRExpr;
    QRShape6: TQRShape;
    QRShape75: TQRShape;
    QRLabel45: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel52: TQRLabel;
    QRDBText38: TQRDBText;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape10: TQRShape;
    QRShape9: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape12: TQRShape;
    QRShape11: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape16: TQRShape;
    QRShape15: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape24: TQRShape;
    QRShape23: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape39: TQRShape;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape32: TQRShape;
    QRShape38: TQRShape;
    QRShape76: TQRShape;
    QRShape73: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape78: TQRShape;
    QRShape79: TQRShape;
    QRLabel41: TQRLabel;
    QRLabel53: TQRLabel;
    QRShape80: TQRShape;
    QRShape81: TQRShape;
    QRDBText37: TQRDBText;
    QRDBText39: TQRDBText;
    QRShape84: TQRShape;
    QRShape85: TQRShape;
    QRShape86: TQRShape;
    QRLabel46: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel47: TQRLabel;
    QRDBText41: TQRDBText;
    QRLabel48: TQRLabel;
    QRDBText42: TQRDBText;
    QRShape87: TQRShape;
    QRLabel50: TQRLabel;
    QRShape88: TQRShape;
    QRSysData2: TQRSysData;
    QRSysData3: TQRSysData;
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public

  end;

var
  relFolhaFrequencia: TrelFolhaFrequencia;

implementation

uses uDM;

{$R *.DFM}

procedure TrelFolhaFrequencia.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //Dia 01
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia01').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia01').Value = 'D') then
    QRDBText3.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia01').Value = 'J') then
    QRDBText3.Font.Color := clGreen
  else
    QRDBText3.Font.Color := clBlack;

  //Dia 02
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia02').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia02').Value = 'D') then
    QRDBText4.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia02').Value = 'J') then
    QRDBText4.Font.Color := clGreen
  else
    QRDBText4.Font.Color := clBlack;

  //Dia 03
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia03').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia03').Value = 'D') then
    QRDBText5.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia03').Value = 'J') then
    QRDBText5.Font.Color := clGreen
  else
    QRDBText5.Font.Color := clBlack;

  //Dia 04
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia04').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia04').Value = 'D') then
    QRDBText6.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia04').Value = 'J') then
    QRDBText6.Font.Color := clGreen
  else
    QRDBText6.Font.Color := clBlack;

  //Dia 05
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia05').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia05').Value = 'D') then
    QRDBText7.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia05').Value = 'J') then
    QRDBText7.Font.Color := clGreen
  else
    QRDBText7.Font.Color := clBlack;

  //Dia 06
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia06').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia06').Value = 'D') then
    QRDBText8.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia06').Value = 'J') then
    QRDBText8.Font.Color := clGreen
  else
    QRDBText8.Font.Color := clBlack;

  //Dia 07
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia07').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia07').Value = 'D') then
    QRDBText9.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia07').Value = 'J') then
    QRDBText9.Font.Color := clGreen
  else
    QRDBText9.Font.Color := clBlack;

  //Dia 08
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia08').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia08').Value = 'D') then
    QRDBText10.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia08').Value = 'J') then
    QRDBText10.Font.Color := clGreen
  else
    QRDBText10.Font.Color := clBlack;

  //Dia 09
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia09').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia09').Value = 'D') then
    QRDBText11.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia09').Value = 'J') then
    QRDBText11.Font.Color := clGreen
  else
    QRDBText11.Font.Color := clBlack;

  //Dia 10
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia10').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia10').Value = 'D') then
    QRDBText12.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia10').Value = 'J') then
    QRDBText12.Font.Color := clGreen
  else
    QRDBText12.Font.Color := clBlack;

  //Dia 11
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia11').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia11').Value = 'D') then
    QRDBText13.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia11').Value = 'J') then
    QRDBText13.Font.Color := clGreen
  else
    QRDBText13.Font.Color := clBlack;

  //Dia 12
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia12').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia12').Value = 'D') then
    QRDBText14.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia12').Value = 'J') then
    QRDBText14.Font.Color := clGreen
  else
    QRDBText14.Font.Color := clBlack;

  //Dia 13
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia13').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia13').Value = 'D') then
    QRDBText15.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia13').Value = 'J') then
    QRDBText15.Font.Color := clGreen
  else
    QRDBText15.Font.Color := clBlack;

  //Dia 14
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia14').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia14').Value = 'D') then
    QRDBText16.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia14').Value = 'J') then
    QRDBText16.Font.Color := clGreen
  else
    QRDBText16.Font.Color := clBlack;

  //Dia 15
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia15').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia15').Value = 'D') then
    QRDBText17.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia15').Value = 'J') then
    QRDBText17.Font.Color := clGreen
  else
    QRDBText17.Font.Color := clBlack;

  //Dia 16
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia16').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia16').Value = 'D') then
    QRDBText18.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia16').Value = 'J') then
    QRDBText18.Font.Color := clGreen
  else
    QRDBText18.Font.Color := clBlack;

  //Dia 17
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia17').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia17').Value = 'D') then
    QRDBText19.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia17').Value = 'J') then
    QRDBText19.Font.Color := clGreen
  else
    QRDBText19.Font.Color := clBlack;

  //Dia 18
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia18').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia18').Value = 'D') then
    QRDBText20.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia18').Value = 'J') then
    QRDBText20.Font.Color := clGreen
  else
    QRDBText20.Font.Color := clBlack;

  //Dia 19
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia19').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia19').Value = 'D') then
    QRDBText21.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia19').Value = 'J') then
    QRDBText21.Font.Color := clGreen
  else
    QRDBText21.Font.Color := clBlack;

  //Dia 20
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia20').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia20').Value = 'D') then
    QRDBText22.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia20').Value = 'J') then
    QRDBText22.Font.Color := clGreen
  else
    QRDBText22.Font.Color := clBlack;

  //Dia 21
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia21').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia21').Value = 'D') then
    QRDBText23.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia21').Value = 'J') then
    QRDBText23.Font.Color := clGreen
  else
    QRDBText23.Font.Color := clBlack;

  //Dia 22
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia22').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia22').Value = 'D') then
    QRDBText24.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia22').Value = 'J') then
    QRDBText24.Font.Color := clGreen
  else
    QRDBText24.Font.Color := clBlack;

  //Dia 23
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia23').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia23').Value = 'D') then
    QRDBText25.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia23').Value = 'J') then
    QRDBText25.Font.Color := clGreen
  else
    QRDBText25.Font.Color := clBlack;

  //Dia 24
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia24').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia24').Value = 'D') then
    QRDBText26.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia24').Value = 'J') then
    QRDBText26.Font.Color := clGreen
  else
    QRDBText26.Font.Color := clBlack;

  //Dia 25
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia25').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia25').Value = 'D') then
    QRDBText27.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia25').Value = 'J') then
    QRDBText27.Font.Color := clGreen
  else
    QRDBText27.Font.Color := clBlack;

  //Dia 26
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia26').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia26').Value = 'D') then
    QRDBText28.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia26').Value = 'J') then
    QRDBText28.Font.Color := clGreen
  else
    QRDBText28.Font.Color := clBlack;

  //Dia 27
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia27').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia27').Value = 'D') then
    QRDBText29.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia27').Value = 'J') then
    QRDBText29.Font.Color := clGreen
  else
    QRDBText29.Font.Color := clBlack;

  //Dia 28
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia28').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia28').Value = 'D') then
    QRDBText30.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia28').Value = 'J') then
    QRDBText30.Font.Color := clGreen
  else
    QRDBText30.Font.Color := clBlack;

  //Dia 29
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia29').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia29').Value = 'D') then
    QRDBText31.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia29').Value = 'J') then
    QRDBText31.Font.Color := clGreen
  else
    QRDBText31.Font.Color := clBlack;

  //Dia 30
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia30').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia30').Value = 'D') then
    QRDBText32.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia30').Value = 'J') then
    QRDBText32.Font.Color := clGreen
  else
    QRDBText32.Font.Color := clBlack;

  //Dia 31
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia31').Value = 'S') or
    (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia31').Value = 'D') then
    QRDBText33.Font.Color := clRed else
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('flg_dia31').Value = 'J') then
    QRDBText33.Font.Color := clGreen
  else
    QRDBText33.Font.Color := clBlack;

end;

end.
