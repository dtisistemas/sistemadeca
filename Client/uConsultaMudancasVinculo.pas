unit uConsultaMudancasVinculo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, Mask, Grids, DBGrids, Db;

type
  TfrmConsultaMudancasVinculo = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    btnConsultar: TSpeedButton;
    Bevel1: TBevel;
    dsConsultaMudancasVinculo: TDataSource;
    dbgMudancasVinculo: TDBGrid;
    btnGerarCSV: TSpeedButton;
    mskAno: TMaskEdit;
    cbMeses: TComboBox;
    procedure btnConsultarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaMudancasVinculo: TfrmConsultaMudancasVinculo;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmConsultaMudancasVinculo.btnConsultarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_ConsultaMudancasVinculo do
    begin
      Close;
      Params.ParamByName ('@pe_ano').Value := Trim(mskAno.Text);
      Params.ParamByName ('@pe_mes').Value := cbMeses.ItemIndex;
      Open;
      dbgMudancasVinculo.Refresh;
    end;
  except end;
end;

end.
