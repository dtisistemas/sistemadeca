unit uInformaPeriodoIndicador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask;

type
  TfrmInformaPeriodoIndicador = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    txtDataInicial: TMaskEdit;
    txtDataFinal: TMaskEdit;
    btnEmitirIndicadores: TBitBtn;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    txtMesAno: TEdit;
    txtAssistenteSocial: TEdit;
    lbUnidade: TLabel;
    procedure txtDataInicialExit(Sender: TObject);
    procedure txtDataFinalExit(Sender: TObject);
    procedure txtMesAnoExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEmitirIndicadoresClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInformaPeriodoIndicador: TfrmInformaPeriodoIndicador;

implementation

uses uDM, rIndicadores, uPrincipal, rIndicadores2;

{$R *.DFM}

procedure TfrmInformaPeriodoIndicador.txtDataInicialExit(Sender: TObject);
begin
  try
    StrToDate(txtDataInicial.Text);
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataInicial.Clear;
      txtDataInicial.SetFocus;
    end;
  end;
end;

procedure TfrmInformaPeriodoIndicador.txtDataFinalExit(Sender: TObject);
begin
  try
    StrToDate(txtDataFinal.Text);

    if StrToDate(txtDataFinal.Text) < StrToDate(txtDataInicial.Text) then
        begin
          ShowMessage('Data final do periodo � menor do que a data inicial.' + #13#10#13 +
                      'Favor informe data v�lida para realizar a consulta...');
          txtDataFinal.SetFocus;
        end
    else
      begin
        txtMesAno.SetFocus;
      end;
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataFinal.Clear;
      txtDataFinal.SetFocus;
    end;
  end;
end;

procedure TfrmInformaPeriodoIndicador.txtMesAnoExit(Sender: TObject);
begin
  if Length(txtMesAno.Text) <= 4 then
  begin
    ShowMessage('Per�odo informado inv�lido. Favor tentar novamente!'+ #13#10 +
                'Favor verificar o formato de preenchimento do campo:' + #13#10+
                'MM/AAAA  -  Dois caracteres para o M�S e Quatro caracteres para o ANO');
    txtMesAno.Clear;
    txtMesAno.SetFocus;
  end
  else btnEmitirIndicadores.Enabled := True;
end;

procedure TfrmInformaPeriodoIndicador.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmInformaPeriodoIndicador.Close;
  end;
end;

procedure TfrmInformaPeriodoIndicador.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    btnEmitirIndicadores.Enabled := False;
    txtDataInicial.Clear;
    txtDataFinal.Clear;
    txtMesAno.Clear;
    txtAssistenteSocial.Clear;
    txtDataInicial.SetFocus;
  end;
end;

procedure TfrmInformaPeriodoIndicador.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmInformaPeriodoIndicador.btnCancelarClick(Sender: TObject);
begin
  txtDataInicial.Text := '  /  /    ';
  txtDataFinal.Text := '  /  /    ';
  frmInformaPeriodoIndicador.Close;
end;

procedure TfrmInformaPeriodoIndicador.btnEmitirIndicadoresClick(
  Sender: TObject);
begin
  with dmDeca.cdsSel_Indicadores do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;

    //*******************************************************
    // Emite o Rosto do Indicador
    Application.CreateForm(TrelIndicadores, relIndicadores);

    relIndicadores.lbMesAno.Caption := txtMesAno.Text;
    relIndicadores.lbAsSocial.Caption := 'As. Social: ' + Trim(txtAssistenteSocial.Text);
    relIndicadores.lbUnidade.Caption := vvNOM_UNIDADE;

    relIndicadores.Preview;
    //relIndicadores.Free;

  end;

  // Emite o verso do Indicador

  //Total de Atendimento Fam�lia
  with dmDeca.cdsSel_Total_Familia do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimento Crian�as
  with dmDeca.cdsSel_Total_Criancas do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimento Adolescentes
  with dmDeca.cdsSel_Total_Adolescente do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimento Profissionais
  with dmDeca.cdsSel_Total_Profissionais do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimento Projeto de Vida
  with dmDeca.cdsSel_Total_Projeto_de_Vida do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

 //Total de Atendimento Fam�lia - Visita Domiciliar
  with dmDeca.cdsSel_Fam_Visita_Docimicliar do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimento Fam�lia - Atendimento Individual
  with dmDeca.cdsSel_Fam_Atendimento_Individual do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimento Familia - Anamnese
  with dmDeca.cdsSel_Fam_Anamnese do
  begin
  Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Fam�lia - Abordagem Grupal
  with dmDeca.cdsSel_Fam_Abordagem_Grupal do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Fam�lia - Reuni�o Informativa
  with dmDeca.cdsSel_Fam_Reuniao_Informativa do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Fam�lia - Integra��o Fam�lia/Institui��o
  with dmDeca.cdsSel_Fam_Integracao do
  begin
   Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Crian�a/Adolescente - Atendimento Individual
  with dmDeca.cdsSel_Criadol_Atendimento_Individual do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Crian�a/Adolescente - Abordagem Grupal
  with dmDeca.cdsSel_Criadol_Abordagem_Grupal do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Crian�a/Adolescente - Treinamento Conv�nio/Est�gio
  with dmDeca.cdsSel_Criadol_Treinamento do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Atendimento a Comunidade
  with dmDeca.cdsSel_Profissional_Comunidade do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Relat�rios Diversos
  with dmDeca.cdsSel_RelDiversos do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Cursos de Capacita��o
  with dmDeca.cdsSel_Cursos_Capacitacao do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Atividades Extras
  with dmDeca.cdsSel_Atividades_Extras do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Visitas Diversas
  with dmDeca.cdsSel_Visitas_Diversas do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Supervis�o de Est�gio
  with dmDeca.cdsSel_Supervisao_Estagio do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Reuni�es T�cnicas
  with dmDeca.cdsSel_Reunioes_Tecnicas do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Profissional - Discuss�o de Casos
  with dmDeca.cdsSel_Discussao_Casos do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Projeto de Vida/Fam�lia - Visita Domiciliar
  with dmDeca.cdsSel_Projeto_Vida_Visita do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Projeto de Vida/Fam�lia - Abordagem Grupal
  with dmDeca.cdsSel_Projeto_Vida_Abordagem do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;

  //Total de Atendimentos Projeto de Vida/Fam�lia - Atendimento Individual
  with dmDeca.cdsSel_Projeto_Vida_AtIndividual do
  begin
    Close;
    Params.ParamByName('@pe_dat_historico1').AsDateTime := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_dat_historico2').AsDateTime := StrToDate(txtDataFinal.Text);
    Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;
  end;


  //Repassa os valores calculados para os respectivos campos no formul�rio (Verso do Indicador)
  Application.CreateForm(TrelIndicadores2, relIndicadores2);
  relIndicadores2.lbTtAtFamilia.Caption := IntToStr(dmDeca.cdsSel_Total_Familia.FieldByName('Total Familia').AsInteger);
  relIndicadores2.lbTtAtCrianca.Caption := IntToStr(dmDeca.cdsSel_Total_Criancas.FieldByName('Total Crian�a').AsInteger);
  relIndicadores2.lbTtAtAdol.Caption := IntToStr(dmDeca.cdsSel_Total_Adolescente.FieldByName('Total Adolescente').AsInteger);
  relIndicadores2.lbTtAtProf.Caption := IntToStr(dmDeca.cdsSel_Total_Profissionais.FieldByName('Total Profissional').AsInteger);
  relIndicadores2.lbTtAtProjetoVida.Caption := IntToStr(dmDeca.cdsSel_Total_Projeto_de_Vida.FieldByName('Total Projeto Vida').AsInteger);
  relIndicadores2.lbTtAtFam_AbordagemGrupal.Caption := IntToStr(dmDeca.cdsSel_Fam_Abordagem_Grupal.FieldByName('Total Familia AbGrupal').AsInteger);
  relIndicadores2.lbTtAtFam_ReuniaoInf.Caption := IntToStr(dmDeca.cdsSel_Fam_Reuniao_Informativa.FieldByName('Total Familia ReuniaoInf').AsInteger);
  relIndicadores2.lbTtAtFam_Integracao.Caption := IntToStr(dmDeca.cdsSel_Fam_Integracao.FieldByName('Total Familia Integracao').AsInteger);
  relIndicadores2.lbTtAtFam_VisitaDomiciliar.Caption := IntToStr(dmDeca.cdsSel_Fam_Visita_Docimicliar.FieldByName('Total Familia Visita').AsInteger);
  relIndicadores2.lbTtAtFam_AtIndividual.Caption := IntToStr(dmDeca.cdsSel_Fam_Atendimento_Individual.FieldByName('Total Familia AtIndividual').AsInteger);
  relIndicadores2.lbTtAtFam_Anamnese.Caption := IntToStr(dmDeca.cdsSel_Fam_Anamnese.FieldByName('Total Familia Anamnese').AsInteger);
  relIndicadores2.lbTtAtCriAdol_AtendimentoIndividual.Caption := IntToStr(dmDeca.cdsSel_Criadol_Atendimento_Individual.FieldByName('Total Criadol Individual').AsInteger);
  relIndicadores2.lbTtCriAdol_AbordagemGrupal.Caption := IntToStr(dmDeca.cdsSel_Criadol_Abordagem_Grupal.FieldByName('Total Criadol AbGrupal').AsInteger);
  relIndicadores2.lbTtAtCriAdol_Treinamento.Caption := IntToStr(dmDeca.cdsSel_Criadol_Treinamento.FieldByName('Total Criadol Treinamento').AsInteger);
  relIndicadores2.lbtTAtProf_AtendimentoComunidade.Caption := IntToStr(dmDeca.cdsSel_Profissional_Comunidade.FieldByName('Total ProfComunidade').AsInteger);
  relIndicadores2.lbTtAtProf_RelatoriosDiversos.Caption := IntToStr(dmDeca.cdsSel_RelDiversos.FieldByName('Total RelDiversos').AsInteger);
  relIndicadores2.lbTtAtProf_CursosCapacitacao.Caption := IntToStr(dmDeca.cdsSel_Cursos_Capacitacao.FieldByName('Total Cursos').AsInteger);
  relIndicadores2.lbTtAtProf_ParticipaAtividades.Caption := IntToStr(dmDeca.cdsSel_Atividades_Extras.FieldByname('Total AtivExtra').AsInteger);
  relIndicadores2.lbTtAtProf_VisitasDiversas.Caption := IntToStr(dmDeca.cdsSel_Visitas_Diversas.FieldByName('Total VisDiversas').AsInteger);
  relIndicadores2.lbTtAtProf_SupervisaoEstagio.Caption := IntToStr(dmDeca.cdsSel_Supervisao_Estagio.FieldByName('Total SupEstagio').AsInteger);
  relIndicadores2.lbTtAtProf_ReunioesTecnicas.Caption := IntToStr(dmDeca.cdsSel_Reunioes_Tecnicas.FieldByName('Total ReuTecnica').AsInteger);
  relIndicadores2.lbTtAtProf_DiscussaoCasos.Caption := IntToStr(dmDeca.cdsSel_Discussao_Casos.FieldByName('Total DiscCasos').AsInteger);
  relIndicadores2.lbTtAtProjetoVida_VisitaDomiciliar.Caption := IntToStr(dmDeca.cdsSel_Projeto_Vida_Visita.FieldByName('Total ProjVida').AsInteger);
  relIndicadores2.lbTtAtProjetoVida_AbordagemGrupal.Caption := IntToStr(dmDeca.cdsSel_Projeto_Vida_Abordagem.FieldByName('Total AbGrupal').AsInteger);
  relIndicadores2.lbTtAtProjetoVida_AtendimentoIndividual.Caption := IntToStr(dmDeca.cdsSel_Projeto_Vida_AtIndividual.FieldByName('Total AtIndividual').AsInteger);

  relIndicadores2.Preview;
  relIndicadores.Free;
  relIndicadores2.Free;

  frmInformaPeriodoIndicador.Close;

  //AtualizaCamposBotoes('Padrao');

end;

procedure TfrmInformaPeriodoIndicador.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
