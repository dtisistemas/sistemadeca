unit uCadastroSeriesPorEscola;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, jpeg, Db;

type
  TfrmCadastroSeriesPorEscola = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    cbEscola: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edEnderecoEscola: TEdit;
    edBairroEscola: TEdit;
    Label4: TLabel;
    edCepEscola: TEdit;
    Label5: TLabel;
    edDiretorEscola: TEdit;
    Label7: TLabel;
    edEmailEscola: TEdit;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    cbSerieEscolar: TComboBox;
    Panel2: TPanel;
    dbgSeriesPorEscola: TDBGrid;
    GroupBox3: TGroupBox;
    btnAdicionarSerieEmEscola: TSpeedButton;
    btnExcluirSerieEmEscola: TSpeedButton;
    btnVerSeriesPorEscola: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Image1: TImage;
    dsEscolaSerie: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure cbEscolaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAdicionarSerieEmEscolaClick(Sender: TObject);
    procedure cbSerieEscolarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroSeriesPorEscola: TfrmCadastroSeriesPorEscola;
  vListaEscola1, vListaEscola2, vListaSerie1, vListaSerie2 : TStringList;
  mmCOD_ESCOLA, mmCOD_SERIE : Integer;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmCadastroSeriesPorEscola.FormShow(Sender: TObject);
begin
  //Selecionar TODAS as escolas
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      //Carreg�-las no combo...
      cbEscola.Clear;
      while not dmDeca.cdsSel_Escola.Eof do
      begin
        vListaEscola1.Add (IntToStr(dmDeca.cdsSel_Escola.FieldByName ('cod_escola').Value));
        vListaEscola2.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
        cbEscola.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
        dmDeca.cdsSel_Escola.Next;
      end;

    end;

  except
    Application.MessageBox ('Aten��o!!! ' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar a lista de escolas.' + #13+ #10 +
                            'Verifique conex�o de rede/internet ou entre em contato ' + #13+#10 +
                            'com o Administrador do Sistema.',
                            '[Sistema Deca]-Cadastro de S�ries por Escola',
                            MB_OK + MB_ICONERROR);
  end;


  {
  //Carregar as s�ries escolares...
  try
    with dmDeca.cdsSel_SerieEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_serie').Value := Null;
      Open;

      cbSerieEscolar.Clear;
      while not eof do
      begin
        cbSerieEscolar.Items.Add (FieldByName ('dsc_serie').Value);
        Next;
      end 
    end;
  except end;}

end;

procedure TfrmCadastroSeriesPorEscola.cbEscolaClick(Sender: TObject);
begin

  mmCOD_ESCOLA := StrToInt(vListaEscola1.Strings[cbEscola.ItemIndex]);
  //Exibe os dados
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := mmCOD_ESCOLA;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      if RecordCount > 0 then
      begin
        edCepEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('num_cep').Value;
        edEnderecoEscola.Text := dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').Value;
        edBairroEscola.Text   := dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').Value;
        edDiretorEscola.Text  := dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').Value;
        edBairroEscola.Text   := dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').Value;
        edEmailEscola.Text    := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').Value;
      end;
    end;
  except end;

  //Carrega a lista de s�ries (geral) - cbSerieEscolar
  try
    with dmDeca.cdsSel_SerieEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_serie').Value := Null;
      Open;

      //Carrega a lista de s�ries escolares em cbSerieEscolar
      cbSerieEscolar.Clear;
      while not eof do
      begin
        vListaSerie1.Add (IntToStr(FieldByName ('cod_id_serie').Value));
        vListaSerie2.Add (FieldByName ('dsc_serie').Value);
        cbSerieEscolar.Items.Add (FieldByName ('dsc_serie').Value);
        dmDeca.cdsSel_SerieEscolar.Next;
      end;

      dbgSeriesPorEscola.Refresh;

    end;
  except
    Application.MessageBox ('Aten��o!!! ' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar a lista de s�ries escolares.' + #13+ #10 +
                            'Verifique conex�o de rede/internet ou entre em contato ' + #13+#10 +
                            'com o Administrador do Sistema.',
                            '[Sistema Deca]-Cadastro de S�ries por Escola',
                            MB_OK + MB_ICONERROR);
  end;

  //Carrega as s�ries da escola selecionada
  try
    with dmDeca.cdsSel_EscolaSerie do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value := mmCOD_ESCOLA;
      Params.ParamByName ('@pe_cod_id_serie').Value := Null;
      Open;
      dbgSeriesPorEscola.Refresh;
    end;

  except end;
end;

procedure TfrmCadastroSeriesPorEscola.FormCreate(Sender: TObject);
begin
  vListaEscola1 := TStringList.Create();
  vListaEscola2 := TStringList.Create();
  vListaSerie1 := TStringList.Create();
  vListaSerie2 := TStringList.Create();
end;

procedure TfrmCadastroSeriesPorEscola.btnAdicionarSerieEmEscolaClick(
  Sender: TObject);
begin
  //Verifica se a s�rie j� est� lan�ada para a escola atual
  try
    with dmDeca.cdsSel_EscolaSerie do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_escola_serie').Value := null;
      Params.ParamByName ('@pe_cod_escola').Value := mmCOD_ESCOLA;
      Params.ParamByName ('@pe_cod_id_serie').Value := mmCOD_SERIE;
      Open;
    end;

    //Se n�o existe, INCLUI
    if dmDeca.cdsSel_EscolaSerie.RecordCount < 1 then
    begin
      //Proceder a inclus�o no banco de dados
      with dmDeca.cdsInc_EscolaSerie do
      begin
        Close;
        Params.ParamByName ('@pe_cod_escola').Value := mmCOD_ESCOLA;
        Params.ParamByName ('@pe_cod_id_serie').Value := mmCOD_SERIE;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;

        //Atualiza os dados do grid...
        with dmDeca.cdsSel_EscolaSerie do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_escola_serie').Value := null;
          Params.ParamByName ('@pe_cod_escola').Value := mmCOD_ESCOLA;
          Params.ParamByName ('@pe_cod_id_serie').Value := Null;
          Open;
          dbgSeriesPorEscola.Refresh;
        end;
      end;
    end
    else
    begin
      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'A s�rie escolar informada j� est� cadastrada na ' + #13+#10 +
                              'escola selecionada. Informe outra s�rie!!',
                              '[Sistema Deca]-Cadastro de S�ries por Escola',
                              MB_OK + MB_ICONERROR);
      cbSerieEscolar.SetFocus;
    end;
  except
    Application.MessageBox ('Aten��o!!! ' + #13+#10 +
                            'Um erro ocorreu ao tentar incluir a s�ries na escola.' + #13+ #10 +
                            'Verifique conex�o de rede/internet ou entre em contato ' + #13+#10 +
                            'com o Administrador do Sistema.',
                            '[Sistema Deca]-Cadastro de S�ries por Escola',
                            MB_OK + MB_ICONERROR);
  end;
end;

procedure TfrmCadastroSeriesPorEscola.cbSerieEscolarClick(Sender: TObject);
begin
  mmCOD_SERIE := StrToInt(vListaSerie1.Strings[cbSerieEscolar.ItemIndex]);
end;

end.
