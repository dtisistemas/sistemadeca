unit uAgendaConsultas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, Grids, DBGrids, ExtCtrls, ComCtrls;

type
  TfrmAgendaConsultas = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    Label2: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label4: TLabel;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    edUnidade: TEdit;
    Label7: TLabel;
    edNascimento: TMaskEdit;
    btnImprimir: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAgendaConsultas: TfrmAgendaConsultas;

implementation

{$R *.DFM}

end.
