unit uTransferenciaColetiva;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, ExtCtrls, Mask;

type
  TfrmTransferenciaColetiva = class(TForm)
    GroupBox2: TGroupBox;
    lstALUNOS: TCheckListBox;
    Button2: TButton;
    Button3: TButton;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    rgPeriodo: TRadioGroup;
    btnCONFIRMATRANSF: TButton;
    cbUNIDADE: TComboBox;
    mskDATATRANSF: TMaskEdit;
    cbMOTIVO: TComboBox;
    cbUnidadeDestino: TComboBox;
    GroupBox9: TGroupBox;
    cbSecaoDestino: TComboBox;
    cbBanco: TComboBox;
    cbCotas: TComboBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Memo1: TMemo;
    GroupBox1: TGroupBox;
    cbSecaoOrigem: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure cbUNIDADEClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cbUnidadeDestinoEnter(Sender: TObject);
    procedure CarregaDR1_DR2;
    procedure CarregaBolsistasDE;
    procedure CarregaAprendizesDE;
    procedure cbUnidadeDestinoClick(Sender: TObject);
    procedure cbSecaoDestinoExit(Sender: TObject);
    procedure cbUnidadeDestinoExit(Sender: TObject);
    procedure btnCONFIRMATRANSFClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciaColetiva: TfrmTransferenciaColetiva;
  vListaUnidade1, vListaUnidade2 : TStringList;
  mmIDX_UNIDADE, n,vvINDEX_UNIDADE : Integer;
  mmCCUSTO_ORIGEM : String;

implementation

uses uDM, uTransferenciaEncaminhamentoNova, uPrincipal, uFichaCrianca,
  uUtil;

{$R *.DFM}

procedure TfrmTransferenciaColetiva.FormShow(Sender: TObject);
begin

  Application.CreateForm (TfrmNovaTransferencia, frmNovaTransferencia);

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  cbUnidade.Clear;
  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := Null;
    Params.ParamByName ('@pe_nom_unidade').Value := Null;
    Open;

    while not eof do
    begin
      if (Copy(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value,1,4)<>'0000') and (Copy(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value,1,4)<>'9999') then
      begin
        vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value));
        vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
        cbUNIDADE.Items.Add (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) + '.' + Trim(dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value));
        cbUnidadeDestino.Items.Add (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) + '.' + Trim(dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value));
      end;
      dmDeca.cdsSel_Unidade.Next;
    end;

  end;
end;

procedure TfrmTransferenciaColetiva.cbUNIDADEClick(Sender: TObject);
begin
mskDATATRANSF.Text := DateToStr(Date());
lstALUNOS.Clear;
with dmDeca.cdsSel_Unidade do
  begin
    mmIDX_UNIDADE := 0;
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= Copy(cbUnidade.Items[cbUnidade.ItemIndex],6,100);
    Open;
    mmIDX_UNIDADE   := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').AsInteger;
    mmCCUSTO_ORIGEM := dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value;

    //Seleciona os adolescentes da unidade selecionada...
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value   := Null;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := mmIDX_UNIDADE;
      Open;

      while not dmDeca.cdsSel_Cadastro.eof do
      begin
        lstALUNOS.Items.Add(dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value);
        dmDeca.cdsSel_Cadastro.Next;
      end;
    end;

    //Carrega as se��es da unidade selecionada
    cbSecaoOrigem.Clear;
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value  := mmIDX_UNIDADE; //dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value    := Null;
      Params.ParamByName ('@pe_flg_status').Value   := 0;
      Open;

      if (RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_Secao.Eof do
        begin
          cbSecaoOrigem.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString);
          Next;
        end;
      end;
    end;


  end;
end;
procedure TfrmTransferenciaColetiva.Button3Click(Sender: TObject);
begin
  for n := 0 to lstALUNOS.Items.Count - 1 do
    lstALUNOS.Checked[n] := False;
end;

procedure TfrmTransferenciaColetiva.Button2Click(Sender: TObject);
begin
  for n := 0 to lstALUNOS.Items.Count - 1 do
    lstALUNOS.Checked[n] := True;
end;

procedure TfrmTransferenciaColetiva.cbUnidadeDestinoEnter(Sender: TObject);
begin
  {
   ORIGEM TRANSFER�NCIA: UNIDADES DR2 (CENTRO DE CUSTO 42...)
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DR2
  }
  if (Copy(mmCCUSTO_ORIGEM,1,2) = '42') and (cbMOTIVO.ItemIndex = 0) then //and (cbTipoTransferencia.ItemIndex = 0) then  --> OUTRA UNIDADE
  begin
    CarregaDR1_DR2;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: UNIDADE DR2 (CENTRO DE CUSTO 42...)
   MOTIVO: MUDAN�A DE V�NCULO
   DESTINO: UNIDADES DR1
  }
  else if (Copy(mmCCUSTO_ORIGEM,1,2) = '42') and (cbMOTIVO.ItemIndex = 1) then //and (cbTipoTransferencia.ItemIndex = 1) then --> MUDAN�A DE V�NCULO
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: UNIDADE DR1
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DR1
  }
  else if (Copy(mmCCUSTO_ORIGEM,1,2) = '32') and (cbMOTIVO.ItemIndex = 0) then //and (cbTipoTransferencia.ItemIndex = 2) then --> OUTRA UNIDADE
  begin
    CarregaDR1_DR2;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: UNIDADE DDC (CENTRO DE CUSTO 31...)
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DE
  }
  else if (Copy(mmCCUSTO_ORIGEM,1,2) = '32') and (cbMOTIVO.ItemIndex = 1) then //and (cbTipoTransferencia.ItemIndex = 3) then   --> MUDAN�A DE V�NCULO
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: BOLSISTAS DE
   MOTIVO: OUTRA UNIDADE
   DESTINO: BOLSISTAS DE
  }
  else if ( (Copy(mmCCUSTO_ORIGEM,1,4) = '5107') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5109') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5112') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5113') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5114')) and
            (cbMOTIVO.ItemIndex = 0) then
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := False;
  end
  {
   ORIGEM TRANSFER�NCIA: BOLSISTAS DE
   MOTIVO: MUDAN�A DE V�NCULO
   DESTINO: UNIDADES DE - CELETISTAS
  }
  else if ( (Copy(mmCCUSTO_ORIGEM,1,4) = '5107') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5109') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5112') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5113') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5114')) and
            (cbMOTIVO.ItemIndex = 1) then
  begin
    CarregaAprendizesDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: APRENDIZES CELETISTAS
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DE - CELETISTAS
  }

  else if ( (Copy(mmCCUSTO_ORIGEM,1,4) = '5102') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5103') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5104') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5105') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5106') or
            (Copy(mmCCUSTO_ORIGEM,1,4) = '5108') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5110') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5111') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5115')) and
            (cbMOTIVO.ItemIndex = 0) then
  begin
    CarregaAprendizesDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: APRENDIZES CELETISTAS
   MOTIVO: MUDAN�A DE V�NCULO
   DESTINO: UNIDADES DE - CELETISTAS
  }

  else if ( (Copy(mmCCUSTO_ORIGEM,1,4) = '5102') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5103') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5104') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5105') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5106') or
            (Copy(mmCCUSTO_ORIGEM,1,4) = '5108') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5110') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5111') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5115')) and
            (cbMOTIVO.ItemIndex = 1) then
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end
end;

procedure TfrmTransferenciaColetiva.CarregaDR1_DR2;
begin
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,2) = '42') or (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,2) = '32') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

procedure TfrmTransferenciaColetiva.CarregaBolsistasDE;
begin
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5107') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5109') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5112') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5113') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5114') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

procedure TfrmTransferenciaColetiva.CarregaAprendizesDE;
begin
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5102') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5103') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5104') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5105') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5106') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5108') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5110') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5111') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5115') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

procedure TfrmTransferenciaColetiva.cbUnidadeDestinoClick(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').AsString := Copy(cbUnidadeDestino.Items[cbUnidadeDestino.ItemIndex],6,100);
      Open;
    end;
  except end;
end;

procedure TfrmTransferenciaColetiva.cbSecaoDestinoExit(Sender: TObject);
begin
{with DmDeca.cdsSel_Unidade do
  begin
    vvINDEX_UNIDADE := 0;
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= Copy(cbUnidadeDestino.Items[cbUnidadeDestino.ItemIndex],6,100);
    Open;
    vvINDEX_UNIDADE := FieldByName('cod_unidade').Value;

    //Carrega a lista de Se��es da Unidade selecionada
    cbSecaoDestino.Items.Clear;
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := 0;
      Open;

      if (RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_Secao.Eof do
        begin
          cbSecaoDestino.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString);
          Next;
        end;
      end;
    end;
  end;}
end;

procedure TfrmTransferenciaColetiva.cbUnidadeDestinoExit(Sender: TObject);
begin
with DmDeca.cdsSel_Unidade do
  begin
    vvINDEX_UNIDADE := 0;
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= Copy(cbUnidadeDestino.Items[cbUnidadeDestino.ItemIndex],6,100);
    Open;
    vvINDEX_UNIDADE := FieldByName('cod_unidade').Value;

    //Carrega a lista de Se��es da Unidade selecionada
    cbSecaoDestino.Items.Clear;
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := 0;
      Open;

      if (RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_Secao.Eof do
        begin
          cbSecaoDestino.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString);
          Next;
        end;
      end;
    end;
  end;
end;

procedure TfrmTransferenciaColetiva.btnCONFIRMATRANSFClick(
  Sender: TObject);
var i : Integer;
matricula, nome, strOcorrencia : String;
begin
  Memo1.Lines.Clear;
  Memo1.Lines.Add('======= ALUNOS SELECIONADOS =======');
  for i := 0 to lstALUNOS.Items.Count - 1 do
    if lstALUNOS.Checked[i] = True then
    begin
      matricula := Copy(lstALUNOS.Items[i],1,8);
      nome := Copy(lstALUNOS.Items[i],12,70);

      //Validar os campos obrigat�rios
      //Unidade Origem, Data da Transfer�ncia, Motivo, Unidade e Se��o destino, Cotas de passe 
      if (cbUNIDADE.ItemIndex > -1) and (mskDATATRANSF.Text <> '  /  /    ') and (cbMOTIVO.ItemIndex > -1) and (cbUnidadeDestino.ItemIndex > -1) and (cbSecaoDestino.ItemIndex > -1) and (cbCotas.ItemIndex > -1) then
      begin
        if StatusCadastro(matricula) = 'Ativo' then
        begin
          with dmDeca.cdsInc_Cadastro_Historico do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value       := matricula;
            Params.ParamByName ('@pe_dat_historico').Value       := GetValue(mskDATATRANSF, vtDate);
            Params.ParamByName ('@pe_ind_tipo_historico').Value  := 6;
            Params.ParamByName ('@pe_dsc_tipo_historico').Value  := 'Transfer�ncia p/ Unidade - Encaminhamento';
            Params.ParamByName ('@pe_cod_unidade').Value         := vvCOD_UNIDADE;
            Params.ParamByName ('@pe_ind_motivo1').Value         := Null;
            Params.ParamByName ('@pe_ind_motivo2').Value         := cbMOTIVO.ItemIndex;

            strOcorrencia := '';
            strOcorrencia := 'Crian�a/Adolescente encaminhado(a) de: ' + Copy(cbUNIDADE.Text,1,4) + '-'+ Copy(cbUNIDADE.Text,6,100) + ' para ' + Copy(cbUnidadeDestino.Text,1,4) + '-'+ Copy(cbUnidadeDestino.Text,6,100);
            strOcorrencia := strOcorrencia + 'Conforme delibera��o institucional a Unidade Novo Horizonte foi unificada com a Unidade Leste, � partir de 12/06/2017, portanto faz-se necess�rio a referida transfer�ncia.';

            Params.ParamByName ('@pe_dsc_ocorrencia').Value      := Trim(strOcorrencia);
            Params.ParamByName ('@pe_orig_unidade').Value        := Copy(cbUNIDADE.Text,6,100);
            Params.ParamByName ('@pe_orig_ccusto').Value         := Copy(cbUNIDADE.Text,1,4);
            Params.ParamByName ('@pe_orig_secao').Value          := Copy(cbSecaoOrigem.Text,6,100);
            Params.ParamByName ('@pe_orig_num_secao').Value      := Copy(cbSecaoOrigem.Text,1,4);
            Params.ParamByName ('@pe_orig_banco').Value          := cbBanco.Text;
            Params.ParamByName ('@pe_dest_unidade').Value        := Copy(cbUnidadeDestino.Text,6,100);
            Params.ParamByName ('@pe_dest_ccusto').Value         := Copy(cbUnidadeDestino.Text,1,4);
            Params.ParamByName ('@pe_dest_secao').Value          := Copy(cbSecaoDestino.Text,6,100);
            Params.ParamByName ('@pe_dest_num_secao').Value      := Copy(cbSecaoDestino.Text,1,4);
            Params.ParamByName ('@pe_dest_banco').Value          := cbBanco.Text;
            Params.ParamByName ('@pe_flg_passe').Value           := 0;
            Params.ParamByName ('@pe_num_cotas_passe').AsFloat   := StrToFloat(cbCotas.Text);
            Params.ParamByName ('@pe_log_cod_usuario').Value     := vvCOD_USUARIO;
            Execute;


            //Atualiza o STATUS para "Em Transfer�ncia - 5"
            with dmdeca.cdsAlt_Cadastro_Status_Transf do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := matricula;
              Params.ParamByName ('@pe_cod_unidade').Value   := vvINDEX_UNIDADE;
              Params.ParamByName ('@pe_num_secao').Value     := cbSecaoDestino.Text;

              if (cbUNIDADE.Text = cbUnidadeDestino.Text) then
                Params.ParamByName ('@pe_ind_status').Value    := 1
              else Params.ParamByName ('@pe_ind_status').Value := 5;

              Execute;
            end;

            //Altera a cota de passe no Cadastro
            with dmDeca.cdsAlt_CotasPasse do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value     := matricula;
              Params.ParamByName ('@pe_num_cotas_passe').AsFloat := StrToFloat(cbCotas.Text);
              Execute;
            end;

            //Atualizar o n�mero da se��o no cadastro
            with dmDeca.cdsAlt_Secao_Cadastro do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := matricula;
              Params.ParamByName ('@pe_num_secao').Value     := Copy(cbSecaoDestino.Text,1,4);
              Execute;
            end;

            ShowMessage ('Transfer�ncias realizadas...');

            //cbUNIDADE.ItemIndex := -1;
            //cbSecaoOrigem.ItemIndex := -1;
            //lstAlunos.Clear;
            //mskDATATRANSF.Clear;
            //cbMOTIVO.ItemIndex := -1;
            //cbUnidadeDestino.ItemIndex := -1;
            //cbSecaoDestino.ItemIndex := -1;
            //cbBanco.ItemIndex := -1;
            //cbCotas.ItemIndex := -1;

          end;
        end
        else
        begin
          //N�o foram transferidos por causa do status estar diferente de Ativo
          //Gravar no arquivo de LOG de conex�o os dados registrados na tela
        end;
      end
      else
      begin
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'Existem campos obrigat�rios sem preenchimento/sele��o' + #13+#10 +
                                'Favor verificar!',
                                '[Sistema Deca]-Preencher/selecionar campos obrigat�rios',
                                MB_OK + MB_ICONERROR);
      end;
    end;
end;

end.
