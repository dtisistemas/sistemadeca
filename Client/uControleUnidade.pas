unit uControleUnidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, ComCtrls, Db;

type
  TfrmControleUnidade = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    cbDivisao: TComboBox;
    GroupBox2: TGroupBox;
    cbAlimentacaoPor: TComboBox;
    Panel2: TPanel;
    Panel3: TPanel;
    btnCancelar: TBitBtn;
    btnAlterar: TBitBtn;
    btnSair: TBitBtn;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    Panel5: TPanel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    dbgTotais: TDBGrid;
    DBGrid2: TDBGrid;
    DataSource1: TDataSource;
    Panel6: TPanel;
    GroupBox4: TGroupBox;
    cbDivisaoP: TComboBox;
    cbGestaoP: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    btnPesquisarPorDivisaoGestao: TSpeedButton;
    dsTotaliza: TDataSource;
    BitBtn1: TBitBtn;
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaTela(Modo:String);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnPesquisarPorDivisaoGestaoClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmControleUnidade: TfrmControleUnidade;

implementation

uses uDM, rTotalPorUnidade, uPrincipal;

{$R *.DFM}

procedure TfrmControleUnidade.AtualizaTela(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;
    Panel2.Enabled := True;

    btnCancelar.Enabled := False;
    btnAlterar.Enabled := False;
    btnSair.Enabled := True;
  end
  else if Modo = 'Alterar' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    Panel2.Enabled := False;

    btnCancelar.Enabled := True;
    btnAlterar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmControleUnidade.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmControleUnidade.FormShow(Sender: TObject);
begin

  AtualizaTela('Padr�o');

  PageControl1.ActivePageIndex := 0;

  //Carregar os dados das unidades para o DBGrid1
  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName('@pe_cod_unidade').Value := Null;
    Params.ParamByName('@pe_nom_unidade').Value := Null;
    Open;

    DBGrid2.Refresh;
    
  end;
end;

procedure TfrmControleUnidade.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela('Padr�o');
end;

procedure TfrmControleUnidade.DBGrid2DblClick(Sender: TObject);
begin
  AtualizaTela('Alterar');

  cbDivisao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName('ind_tipo_unidade').Value;
  cbAlimentacaoPor.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName('ind_tipo_gestao').Value;

end;

procedure TfrmControleUnidade.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_UnidadesNutricao do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
      Params.ParamByName('@pe_ind_tipo_unidade').Value := cbDivisao.ItemIndex;
      Params.ParamByName('@pe_ind_tipo_gestao').Value := cbAlimentacaoPor.ItemIndex;
      Execute;

      with dmDeca.cdsSel_Unidade do
      begin
        Close;
        Params.ParamByName('@pe_cod_unidade').Value := Null;
        Params.ParamByName('@pe_nom_unidade').Value := Null;
        Open;
        DBGrid2.Refresh;
      end;
    end;

    AtualizaTela('Padr�o');

  except end;
end;

procedure TfrmControleUnidade.btnLimparClick(Sender: TObject);
begin
  cbDivisaoP.ItemIndex := -1;
  cbGestaoP.ItemIndex := -1;
end;

procedure TfrmControleUnidade.TabSheet2Show(Sender: TObject);
begin

  //N�o exibe o resultado de consultas anteriores
  dbgTotais.DataSource := nil;
  cbDivisaoP.ItemIndex := 3;
  cbGestaoP.ItemIndex := 3;
end;

procedure TfrmControleUnidade.btnPesquisarPorDivisaoGestaoClick(
  Sender: TObject);
begin

  //Verifica se selecionou Divis�o e Gest�o
  if (cbDivisaoP.ItemIndex > -1) and (cbGestaoP.ItemIndex > -1) then
  begin
    try

      with dmDeca.cdsTotalizaPorUnidade do
      begin
        Close;

        //Se seleciou Todas as Divis�es...
        if (cbDivisaoP.ItemIndex = 3) then
          Params.ParamByName('@pe_ind_tipo_unidade').Value := Null
        else
          Params.ParamByName('@pe_ind_tipo_unidade').Value := cbDivisaoP.ItemIndex;

        //Se selecionou Todas as Gest�es
        if (cbGestaoP.ItemIndex = 3) then
          Params.ParamByName('@pe_ind_tipo_gestao').Value := Null
        else
          Params.ParamByName('@pe_ind_tipo_gestao').Value := cbGestaoP.ItemIndex;

        Open;

        dbgTotais.DataSource := dsTotaliza;
        dbgTotais.Refresh;

      end;

    except end;
  end
  else
  begin
    ShowMessage ('Aten��o !!! Selecione as duas op��es de FILTRO dispon�veis...');
    cbDivisaoP.SetFocus;
  end;
end;

procedure TfrmControleUnidade.BitBtn5Click(Sender: TObject);
begin
  frmControleUnidade.Close;
end;

procedure TfrmControleUnidade.BitBtn4Click(Sender: TObject);
begin
  Application.CreateForm(TrelTotalPorUnidade, relTotalPorUnidade);
  relTotalPorUnidade.lbDivisaoR.Caption := 'Divis�o: [' + cbDivisaoP.Text + ']';
  relTotalPorUnidade.lbGestaoR.Caption := 'Gest�o: [' + cbGestaoP.Text + ']';
  relTotalPorUnidade.lbNomeUsuario.Caption := vvNOM_USUARIO;
  relTotalPorUnidade.Preview;
  relTotalPorUnidade.Free;
end;

end.
