unit uTransferenciasDAPA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ExtCtrls, Grids, DBGrids, Db, ComCtrls, QRExport;

type
  TfrmTransferenciasDAPA = class(TForm)
    Panel1: TPanel;
    btnVer: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    meData1: TMaskEdit;
    meData2: TMaskEdit;
    btnLimpar: TSpeedButton;
    btnPesquisar: TSpeedButton;
    dsTransferenciasDAPA: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dbgResultado: TDBGrid;
    btnExportar: TSpeedButton;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciasDAPA: TfrmTransferenciasDAPA;

implementation

uses uDM, rTransferenciasDAPA, uUtil;

{$R *.DFM}

procedure TfrmTransferenciasDAPA.btnPesquisarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_TransferenciasDAPA do
    begin
      Close;
      Params.ParamByName ('@pe_data1').Value := GetValue(meData1, vtDate);
      Params.ParamByName ('@pe_data2').Value := GetValue(meData2, vtDate);
      Params.ParamByName ('@pe_ind_motivo1').Value := 1;
      Open;

      dbgResultado.Refresh;

    end;
  except end;
end;

procedure TfrmTransferenciasDAPA.btnVerClick(Sender: TObject);
begin
  if (dmDeca.cdsSel_TransferenciasDAPA.RecordCount > 0) then
  begin
    Application.CreateForm (TrelTransferenciasDAPA, relTransferenciasDAPA);
    relTransferenciasDAPA.lbTitulo.Caption := 'Transferidos de ' + meData1.Text + ' at� ' + meData2.Text;
    relTransferenciasDAPA.Preview;
    relTransferenciasDAPA.Free;
  end
  else
  begin
     Application.MessageBox ('N�o foram encontrados registros!!! Imposs�vel Imprimir...',
                             'Transfer�ncias DAPA',
                             MB_ICONERROR + MB_OK);
  end;
end;

procedure TfrmTransferenciasDAPA.btnExportarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_TransferenciasDAPA do
    begin
      Close;
      Params.ParamByName ('@pe_data1').Value := GetValue(meData1, vtDate);
      Params.ParamByName ('@pe_data2').Value := GetValue(meData2, vtDate);
      Params.ParamByName ('@pe_ind_motivo1').Value := 1;
      Open;

      if (dmDeca.cdsSel_TransferenciasDAPA.RecordCount > 0) then
      begin
        Application.CreateForm (TrelTransferenciasDAPA, relTransferenciasDAPA);
        relTransferenciasDAPA.lbTitulo.Caption := 'Transferidos de ' + meData1.Text + ' at� ' + meData2.Text;
        //relTransferenciasDAPA.Preview;
        //relTransferenciasDAPA.ExportToFilter(TQRHTMLDocumentFilter.Create(ExtractFilePath(Application.ExeName) + 'TransferidosDAPA.HTML'));
        relTransferenciasDAPA.ExportToFilter(TQRAsciiExportFilter.Create(ExtractFilePath(Application.ExeName) + 'TransferidosDAPA.DOC'));
        relTransferenciasDAPA.Free;
      end
      else
      begin
        Application.MessageBox ('N�o foram encontrados registros!!! Imposs�vel Imprimir...',
                                'Transfer�ncias DAPA',
                                MB_ICONERROR + MB_OK);
      end;
    end;
  except end;

end;

end.
