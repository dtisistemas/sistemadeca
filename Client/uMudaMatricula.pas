unit uMudaMatricula;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmMudaMatricula = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    txtOcorrencia: TMemo;
    txtData: TMaskEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    Label4: TLabel;
    txtMatriculaNova: TMaskEdit;
    btnSalvar: TBitBtn;
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure txtMatriculaNovaExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMudaMatricula: TfrmMudaMatricula;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmMudaMatricula.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmMudaMatricula.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end;
end;

procedure TfrmMudaMatricula.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmMudaMatricula.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmMudaMatricula.btnSairClick(Sender: TObject);
begin
  frmMudaMatricula.Close;
end;

procedure TfrmMudaMatricula.btnSalvarClick(Sender: TObject);
begin

  if (Length(Trim(txtMatricula.Text)) > 0) and (Length(Trim(txtMatriculaNova.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
      if (StatusCadastro(txtMatricula.Text) = 'Ativo') or ((vvIND_PERFIL = 8) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19))  then
        begin

        //Desabilita a trigger para atualiza��o do cadastro_familia

        //Desabilita a trigger de atualizacao/inclus�o do cadastro hist�rico

        try
          with dmDeca.cdsInc_Cadastro_Historico do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);

            if (vvIND_PERFIL = 8) or (vvIND_PERFIL = 19) then
              Params.ParamByName('@pe_cod_unidade').AsInteger := 1
            else
              Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;

            Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
            Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 11;  //Mudan�a de Matr�cula
            Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Mudan�a de Matr�cula';
            Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
            Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
            Execute;

            //Desabilita as triggers para altera��o de matricula
            with dmDeca.cdsDesabilitaTriggers_Responsavel do
            begin
              Close;
              Execute;
            end;

            //Realiza a Mudan�a de Matr�cula
            with dmDeca.cdsAlt_Cadastro_Muda_Matricula do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula_atual').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_Cod_matricula_novo').Value := GetValue(txtMatriculaNova.Text);
              Execute;
            end;

            //Habilita as triggers para altera��o de matricula
            with dmDeca.cdsHabilitaTriggers_Responsavel do
            begin
              Close;
              Execute;
            end;

            //Habilita a trigger de atualizacao/inclus�o do cadastro hist�rico

            //Altera a Modalidade da Crian�a/Adolescente
            with dmDeca.cdsAlt_Cadastro do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              if Copy(txtMatriculaNova.Text,1,3) = '999' then
                Params.ParamByName('@pe_ind_modalidade').AsInteger := 2  //   FUNDHAS/Peti
              else if (Copy(txtMatriculaNova.Text,1,3) = '002') OR (Copy(txtMatricula.Text,1,3) = '003') OR (Copy(txtMatricula.Text,1,3) = '006') then
                Params.ParamByName('@pe_ind_modalidade').AsInteger := 0;  //   FUNDHAS
              Execute;
              AtualizaCamposBotoes('Padrao');
            end
          end
          except
            Application.MessageBox('Aten��o!'+#13+#10+#13+#10+
                                   'Ocorreu um ERRO e os dados n�o foram gravados. Favor entrar em contato com o Administrador do Sistema.',
                                   'Sistema DECA - Altera��o de Matr�cula',
                                   MB_OK + MB_ICONERROR);
          end;
        AtualizaCamposBotoes('Padrao');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'MAT_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                         Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtData.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Mudan�a de Matr�cula');   //Descricao do Tipo de Historico
      Writeln (log_file, ' ');
      Writeln (log_file, Trim(txtOcorrencia.Text));
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);



      //ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10 +#13+#10 +
      //         'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;

end;

procedure TfrmMudaMatricula.txtMatriculaNovaExit(Sender: TObject);
begin
  
  if (Length(Trim(txtMatriculaNova.Text)) > 0) and (Length(Trim(txtMatriculaNova.Text)) = 8) and
     ( (Copy(txtMatriculaNova.Text,1,3) = '002') or (Copy(txtMatriculaNova.Text,1,3) = '003') or (Copy(txtMatriculaNova.Text,1,3) = '006') ) then
  begin
    txtOcorrencia.SetFocus;
    txtOcorrencia.Lines.Add ('Mudan�a de Matr�cula de ' + txtMatricula.Text + ' para ' + txtMatriculaNova.Text);
  end
  else
  begin
    ShowMessage('Aten��o...' + #13+#10#13+
                'O N�mero da Matr�cula deve conter 8 caracteres'+ #13+#10 +
                'Seguindo o padr�o:' + #13+#10+
                '002 + C�digo da Matr�cula (Para CRIAN�A)' + #13+#10 +
                '003 + C�digo da Matr�cula (Para ADOLESCENTE - BOLSISTA)' + #13+#10 +
                '006 + C�digo da Matr�cula (Para ADOLESCENTE - CELETISTA)'+ #13+#10+#13 +
                '---> D�VIDAS ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA <---');
    txtMatriculaNova.Clear;
    txtMatriculaNova.SetFocus;
  end;

end;

procedure TfrmMudaMatricula.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmMudaMatricula.Close;
  end;

end;

procedure TfrmMudaMatricula.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

    txtMatricula.Enabled := False;
    txtMatricula.Clear;
    txtData.Clear;
    txtMatriculaNova.Clear;
    txtOcorrencia.Clear;
    btnPesquisar.Enabled := False;
    lbNome.Caption := '';
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    txtData.Enabled := False;
    txtMatriculaNova.Enabled := False;
    txtOcorrencia.Enabled := False;

  end
  else if Modo = 'Novo' then
  begin
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := True;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    lbNome.Caption := '';
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtData.Enabled := True;
    txtData.Text := DateToStr(Date());
    txtMatriculaNova.Enabled := True;
    txtOcorrencia.Enabled := True;
  end;

end;
procedure TfrmMudaMatricula.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmMudaMatricula.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
