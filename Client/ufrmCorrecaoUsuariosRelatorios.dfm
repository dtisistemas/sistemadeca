object frmCorrecaoUsuariosRelatorios: TfrmCorrecaoUsuariosRelatorios
  Left = 340
  Top = 183
  Width = 1395
  Height = 357
  Caption = '[Sistema Deca]'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnATUALIZAR_USUARIO_RELATORIO: TSpeedButton
    Left = 1185
    Top = 278
    Width = 193
    Height = 41
    Caption = 'Atualizar Usu�rio em Relat�rios'
    Flat = True
    OnClick = btnATUALIZAR_USUARIO_RELATORIOClick
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 8
    Width = 686
    Height = 265
    Caption = '[Relat�rios Cadastrados]'
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 9
      Top = 18
      Width = 667
      Height = 237
      DataSource = dsSel_Relatorios
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Cod_inscricao'
          Title.Alignment = taCenter
          Title.Caption = '[Inscri��o]'
          Width = 120
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'dta_digitacao'
          Title.Alignment = taCenter
          Title.Caption = '[Data Digita��o]'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipodeRelatorio'
          Title.Alignment = taCenter
          Title.Caption = '[Tipo de Relat�rio]'
          Width = 175
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_usuario'
          Title.Caption = '[Usu�rio Triagem]'
          Width = 250
          Visible = True
        end>
    end
  end
  object GroupBox2: TGroupBox
    Left = 694
    Top = 8
    Width = 686
    Height = 265
    Caption = '[Rela��o de Usu�rios - DECA]'
    TabOrder = 1
    object DBGrid2: TDBGrid
      Left = 9
      Top = 18
      Width = 668
      Height = 237
      DataSource = dsSel_UsuariosDeca
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_usuario'
          Title.Alignment = taCenter
          Title.Caption = '[C�digo]'
          Width = 80
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Alignment = taCenter
          Title.Caption = '[Matr�cula]'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_usuario'
          Title.Caption = '[Nome de Usu�rio]'
          Width = 400
          Visible = True
        end>
    end
  end
  object dsSel_Relatorios: TDataSource
    DataSet = dmTriagemDeca.cdsSel_AtualizaUsuariosDecaRelatorio
    Left = 20
    Top = 280
  end
  object dsSel_UsuariosDeca: TDataSource
    DataSet = dmDeca.cdsSel_Usuario
    Left = 702
    Top = 288
  end
end
