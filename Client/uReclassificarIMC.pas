unit uReclassificarIMC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Grids, DBGrids, Db;

type
  TfrmReclassificarIMC = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    rgCriterio: TRadioGroup;
    GroupBox1: TGroupBox;
    Bevel1: TBevel;
    Bevel2: TBevel;
    btnClassificar: TSpeedButton;
    dbgResultado: TDBGrid;
    btnSair: TSpeedButton;
    cbUnidade: TComboBox;
    dsCadastro: TDataSource;
    procedure rgCriterioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnClassificarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReclassificarIMC: TfrmReclassificarIMC;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmReclassificarIMC.rgCriterioClick(Sender: TObject);
begin
  case rgCriterio.ItemIndex of

    0 : begin
      cbUnidade.ItemINdex := -1;
      cbUnidade.Enabled := False;
      dbgResultado.DataSource := nil;
      btnClassificar.Enabled := True;

      try
        with dmDeca.cdsSel_Cadastro do
        begin
          Close;
          Params.ParamByname('@pe_cod_matricula').Value := Null;
          Params.ParamByname('@pe_cod_unidade').Value := 0;
          Open;
          dbgResultado.DataSource := dsCadastro;
          dbgResultado.Refresh;
          btnClassificar.Enabled := True;
        end;
      except end;
    end;

    1: begin
      cbUnidade.Enabled := True;
      cbUnidade.ItemINdex := 0;
      dbgResultado.DataSource := nil;
      btnClassificar.Enabled := True;
    end;
  end;
end;

procedure TfrmReclassificarIMC.FormShow(Sender: TObject);
begin

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();
  rgCriterio.ItemIndex := 0;

  //Carrega a lista de unidades
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin
        vListaUnidade1.Add (IntToStr(FieldByName('cod_unidade').Value));
        vListaUnidade2.Add (FieldByName('nom_unidade').Value);
        cbUnidade.Items.Add(FieldByName('nom_unidade').Value);
        Next;
      end;
    end;
  except end;

  cbUnidade.Enabled := False;

end;

procedure TfrmReclassificarIMC.cbUnidadeClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByname('@pe_cod_matricula').Value := Null;
      Params.ParamByname('@pe_cod_unidade').Value := vListaUnidade1.Strings[cbUnidade.ItemIndex];
      Open;
      dbgResultado.DataSource := dsCadastro;
      dbgResultado.Refresh;
      btnClassificar.Enabled := True;
    end;
  except end;
end;

procedure TfrmReclassificarIMC.btnSairClick(Sender: TObject);
begin
  frmReclassificarIMC.Close;
end;

procedure TfrmReclassificarIMC.btnClassificarClick(Sender: TObject);
var
  mIMC, mPeso, mAltura : Real;
  strIMC : String;
begin
{
  if Application.MessageBox('Deseja realmente reclassificar dados de IMC?', 'Sistema Deca - Reclassifica��o IMC', MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin

    mPeso := 0;
    mAltura := 0;
    mIMC := 0;
    strIMC := '';

    btnClassificar.Enabled := False;
    dmDeca.cdsSel_Cadastro.First;

    while not dmDeca.cdsSel_Cadastro.eof do
    begin

      //mPeso := 0;
      //mAltura := 0;
      //mIMC := 0;
      //strIMC := '';

      //Se os campos estiveren preenchidos...
      if (dmDeca.cdsSel_Cadastro.FieldByName('val_peso').Value > 0) and (dmDeca.cdsSel_Cadastro.FieldByName('val_altura').Value > 0) and
         (Length(dmDeca.cdsSel_Cadastro.FieldByName('ind_sexo').Value) > 1) then
      begin

        mPeso := 0;
        mAltura := 0;
        mIMC := 0;
        strIMC := '';

        mPeso := dmDeca.cdsSel_Cadastro.FieldByName('val_peso').Value;
        mAltura := dmDeca.cdsSel_Cadastro.FieldByName('val_altura').Value;
        mIMC := mPeso / (mAltura * mAltura);
        strIMC := Format ('%2.1f', [mIMC]);

        //Passa os valores a serem encontrados na tabela e repassados os valores de classificacao do IMC correto
        //de acordo com a idade e o sexo
        //Pesquisa o valor do IMC e tras a Classifica��o
        try
          with dmDeca.cdsSel_ProcuraIMC do
          begin
            Close;
            Params.ParamByName('@pe_id_indicebiometrico').Value := Null;
            Params.ParamByName('@pe_ind_sexo').Value := Copy(dmDeca.cdsSel_Cadastro.FieldByName('ind_sexo').Value,1,1);
            Params.ParamByName('@pe_num_idade').Value := dmDeca.cdsSel_Cadastro.FieldByName('Idade').Value;
            Params.ParamByName('@pe_num_valorIMC').Value := StrToFloat(strIMC);
            Open;

            //Atualiza o cadastro biom�trico/reclassifica de acordo com os dados atuais
            with dmDeca.cdsAlt_Biometrico do
            begin
              Close;
              Params.ParamByName('@pe_dsc_classificacao').Value := dmDeca.cdsSel_ProcuraIMC.FieldByName('dsc_classificacao').Value;
              Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
              Execute;
            end;

            //Incluir lan�amento de avalia��o biom�trica no hist�rico e em Biom�tricos
            //Primeiro valida o Status da crian�a - se ela pode sofrer este movimento
            //Grava os dados da Avalia��o Biom�trica na Tabela BIOMETRICOS
            with dmDeca.cdsInc_Biometricos do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
              Params.ParamByName ('@pe_val_peso').AsFloat := mPeso;//GetValue(txtPeso, vtReal);
              Params.ParamByName ('@pe_val_altura').AsFloat := mAltura;//GetValue(txtAltura, vtReal);
              Params.ParamByName ('@pe_val_imc').AsFloat := StrToFloat(lbIMC.Caption);
              Params.ParamByName ('@pe_dsc_classificacao_imc').AsString := lbClassificacao.Caption;

              if chkProblemasSaude.Checked = True then
                Params.ParamByName ('@pe_flg_problema_saude').AsInteger := 1
              else
                Params.ParamByName ('@pe_flg_problema_saude').AsInteger := 0;

              Params.ParamByName ('@pe_dsc_problema_saude').Value := GetValue(txtProblemasSaude.Lines.Text);
              Params.ParamByName ('@pe_dsc_observacoes').Value := GetValue(txtObservacoes.Lines.Text);
              Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
              Params.ParamByName ('@pe_log_data').AsDateTime := StrToDate(txtData.Text);
              Execute;
            end;

            //Lan�a os dados no cadastro_hist�rico
            with dmDeca.cdsInc_Cadastro_Historico do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
              Params.ParamByName('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro.FieldBYName ('cod_unidade').AsInteger;//vvCOD_UNIDADE;
              Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
              Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 18;
              Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Lan�amento de Avalia��o Biom�trica';
              Params.ParamByName('@pe_dsc_ocorrencia').Value := Null;
              Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
              Execute;
            end;

            //Faz a atualiza��o do Cadastro, nos campos PESO e ALTURA, conforme avalia��o Biom�trica
            with dmDeca.cdsAlt_Cadastro_Dados_Biometricos do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;;
              Params.ParamByName('@pe_val_peso').AsFloat := StrToFloat(txtPeso.Text);
              Params.ParamByName('@pe_val_altura').AsFloat := StrToFloat(txtAltura.Text);
              Execute;
            end;

          end;
        except end;
      end;
      dmDeca.cdsSel_Cadastro.Next;

      //se os campos Peso e Altura n�o estiverem preenchidos
      else
      begin


      end;

    end;
  end;

  }
end;

end.

