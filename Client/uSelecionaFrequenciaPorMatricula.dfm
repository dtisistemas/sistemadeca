object frmSelecionaFrequenciaPorMatricula: TfrmSelecionaFrequenciaPorMatricula
  Left = 182
  Top = 174
  BorderStyle = bsDialog
  Caption = 'Selecione a Crian�a/Adolescente para Emiss�o da Frequ�ncia'
  ClientHeight = 343
  ClientWidth = 719
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 705
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 265
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar2: TSpeedButton
      Left = 170
      Top = 17
      Width = 23
      Height = 22
      Glyph.Data = {
        36050000424D360500000000000036040000280000000E000000100000000100
        08000000000000010000CA0E0000C30E00000001000000000000000000003300
        00006600000099000000CC000000FF0000000033000033330000663300009933
        0000CC330000FF33000000660000336600006666000099660000CC660000FF66
        000000990000339900006699000099990000CC990000FF99000000CC000033CC
        000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
        0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
        330000333300333333006633330099333300CC333300FF333300006633003366
        33006666330099663300CC663300FF6633000099330033993300669933009999
        3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
        330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
        66006600660099006600CC006600FF0066000033660033336600663366009933
        6600CC336600FF33660000666600336666006666660099666600CC666600FF66
        660000996600339966006699660099996600CC996600FF99660000CC660033CC
        660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
        6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
        990000339900333399006633990099339900CC339900FF339900006699003366
        99006666990099669900CC669900FF6699000099990033999900669999009999
        9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
        990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
        CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
        CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
        CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
        CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
        CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
        FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
        FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
        FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
        FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000ACACACACACAC
        ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
        00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
        02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
        5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
        00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
        ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
        D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
        D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
      OnClick = btnPesquisar2Click
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe o N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnExit = txtMatriculaExit
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 64
    Width = 705
    Height = 217
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 8
      Top = 14
      Width = 689
      Height = 196
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Mes'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 73
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_ano'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Ano'
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia01'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '01'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia02'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '02'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia03'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '03'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia04'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '04'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia05'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '05'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia06'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '06'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia07'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '07'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia08'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '08'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia09'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '09'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia10'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '10'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia11'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '11'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia12'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '12'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia13'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '13'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia14'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '14'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia15'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '15'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia16'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '16'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia17'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '17'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia18'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '18'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia19'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '19'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia20'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '20'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia21'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '21'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia22'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '22'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia23'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '23'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia24'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '24'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia25'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '25'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia26'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '26'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia27'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '27'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia28'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '28'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '29'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia30'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '30'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia31'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = '31'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_faltas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Faltas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Usu�rio'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 98
          Visible = True
        end>
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 288
    Width = 705
    Height = 49
    TabOrder = 2
    object btnImprimir: TBitBtn
      Left = 553
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para IMPRIMIR advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnImprimirClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777776B00777777777000777777776E007777777007880077777771007777
        7007780088007777740077770778878800880077770077778887778888008077
        7A00777887777788888800777D007778F7777F888888880780007778F77FF777
        8888880783007778FFF779977788880786007778F77AA7778807880789007777
        88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
        920077777777778FFFFFF0079500777777777778FFF887779800777777777777
        888777779B00777777777777777777779E0077777777777777777777A1007777
        7777777777777777A400}
    end
    object btnSair: TBitBtn
      Left = 628
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object DataSource1: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Frequencia
    Left = 672
    Top = 88
  end
end
