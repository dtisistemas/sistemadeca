unit rNovoRegistroAtendimento;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelNovoRegistroAtendimento = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    lbIdentificacao: TQRLabel;
    lbPeriodo: TQRLabel;
    QRShape1: TQRShape;
    PageFooterBand1: TQRBand;
    QRLabel8: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
  private

  public

  end;

var
  relNovoRegistroAtendimento: TrelNovoRegistroAtendimento;

implementation

{$R *.DFM}

end.
