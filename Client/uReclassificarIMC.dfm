object frmReclassificarIMC: TfrmReclassificarIMC
  Left = 318
  Top = 242
  BorderStyle = bsDialog
  Caption = 'Sistema Deca - [Reclassificação de IMC]'
  ClientHeight = 411
  ClientWidth = 608
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 608
    Height = 28
    Align = alTop
    Caption = 'Utilitário para Reclassificar o IMC de acordo com o sexo.'
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 29
    Width = 608
    Height = 382
    Align = alBottom
    TabOrder = 1
    object Bevel1: TBevel
      Left = 8
      Top = 71
      Width = 588
      Height = 5
    end
    object Bevel2: TBevel
      Left = 387
      Top = 14
      Width = 209
      Height = 51
    end
    object btnClassificar: TSpeedButton
      Left = 391
      Top = 18
      Width = 99
      Height = 43
      Caption = 'Classificar'
      Flat = True
      OnClick = btnClassificarClick
    end
    object btnSair: TSpeedButton
      Left = 493
      Top = 18
      Width = 99
      Height = 43
      Caption = 'Sair'
      Flat = True
      OnClick = btnSairClick
    end
    object rgCriterio: TRadioGroup
      Left = 8
      Top = 8
      Width = 110
      Height = 58
      Caption = 'Critério'
      Items.Strings = (
        'Todos'
        'Por Unidade')
      TabOrder = 0
      OnClick = rgCriterioClick
    end
    object GroupBox1: TGroupBox
      Left = 120
      Top = 8
      Width = 265
      Height = 58
      Caption = 'Unidade'
      TabOrder = 1
      object cbUnidade: TComboBox
        Left = 7
        Top = 20
        Width = 248
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 0
        OnClick = cbUnidadeClick
      end
    end
    object dbgResultado: TDBGrid
      Left = 9
      Top = 81
      Width = 587
      Height = 290
      DataSource = dsCadastro
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Alignment = taCenter
          Title.Caption = 'Matrícula'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_nome'
          Title.Caption = 'Nome'
          Width = 215
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'dat_nascimento'
          Title.Alignment = taCenter
          Title.Caption = 'Nascimento'
          Width = 65
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'idade'
          Title.Alignment = taCenter
          Title.Caption = 'Idade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ind_sexo'
          Title.Caption = 'Sexo'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Val_Peso'
          Title.Caption = 'Peso'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Val_Altura'
          Title.Caption = 'Altura'
          Width = 45
          Visible = True
        end>
    end
  end
  object dsCadastro: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro
    Left = 552
    Top = 117
  end
end
