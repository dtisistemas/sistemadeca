unit uFichaCadastroModelo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, Db, Grids, DBGrids, StdCtrls, ComCtrls, Mask, Menus,
  jpeg, ImgList;

type
  TfrmFichaCadastroModelo = class(TForm)
    Panel1: TPanel;
    btnFichaCompleta: TSpeedButton;
    Panel2: TPanel;
    btnSair: TSpeedButton;
    GroupBox1: TGroupBox;
    imgFOTOGRAFIA: TImage;
    StatusBar1: TStatusBar;
    btnFotografia: TSpeedButton;
    btnFrequencia: TSpeedButton;
    Panel3: TPanel;
    dsCadastro: TDataSource;
    butMapaSocial: TSpeedButton;
    btnServicoSocial: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    SpeedButton12: TSpeedButton;
    SpeedButton11: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton9: TSpeedButton;
    ImageList1: TImageList;
    butDesligamento: TSpeedButton;
    butTransferencia: TSpeedButton;
    butSuspensao: TSpeedButton;
    butAfastamento: TSpeedButton;
    Image10: TImage;
    Label5: TLabel;
    Image3: TImage;
    Label2: TLabel;
    Image4: TImage;
    Label6: TLabel;
    Image2: TImage;
    Label4: TLabel;
    Image5: TImage;
    Image6: TImage;
    Label7: TLabel;
    Label8: TLabel;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    btnEstatisticas: TSpeedButton;
    btnRegistroAtendimento: TSpeedButton;
    btnPesquisa: TSpeedButton;
    Bevel1: TBevel;
    SpeedButton2: TSpeedButton;
    dbgResultado: TDBGrid;
    btnExcluirProntuario: TSpeedButton;
    procedure btnFichaCompletaClick(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFrequenciaClick(Sender: TObject);
    procedure butDesligamentoClick(Sender: TObject);
    procedure butTransferenciaClick(Sender: TObject);
    procedure btnComposicaoFamiliarClick(Sender: TObject);
    procedure btnAcompanhamentoEscolarClick(Sender: TObject);
    procedure dbgResultadoCellClick(Column: TColumn);
    procedure Button1Click(Sender: TObject);
    procedure btnRegistroAtendimentoClick(Sender: TObject);
    procedure btnServicoSocialClick(Sender: TObject);
    procedure butSuspensaoClick(Sender: TObject);
    procedure butAfastamentoClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure dbgResultadoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgResultadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSairClick(Sender: TObject);
    procedure btnEstatisticasClick(Sender: TObject);
    procedure btnFotografiaClick(Sender: TObject);
    procedure dbgResultadoDblClick(Sender: TObject);
  private
    { Private declarations }
    //Img: Array[0..2] of TBitmap;
  public
    { Public declarations }
  end;

var
  frmFichaCadastroModelo: TfrmFichaCadastroModelo;
  bmp1, bmp2 : TBitMap;

implementation

uses uDM, uFichaCrianca, uPrincipal, uFichaPesquisa, uNavegador,
  uNovoControleFrequencia, uDesligamento, uTransferenciaEncaminhamentoNova,
  uRegistroAtendimento, uEvolucaoServicoSocial, uSuspensao,
  uAfastamentoEntrada, uDadosRelatorio, uCadastroFoto;

{$R *.DFM}

procedure TfrmFichaCadastroModelo.btnFichaCompletaClick(Sender: TObject);
begin
  Application.CreateForm(TfrmFichaCrianca, frmFichaCrianca);
  frmFichaCrianca.FillForm ('S', dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value);
  mmINTEGRACAO := True;
  mmNUM_ABA := 0;
  //frmFichaCrianca.PageControl1.ActivePageIndex := 0;
  frmFichaCrianca.ShowModal;
end;

procedure TfrmFichaCadastroModelo.Image2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmNavegador, frmNavegador);
  frmNavegador.URLs.Text := 'http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuLogradouro';
  frmNavegador.StatusBar1.Panels[0].Text := 'Busca por CEP  -  Site Correios (EBCT)';
  frmNavegador.Show;
end;

procedure TfrmFichaCadastroModelo.FormShow(Sender: TObject);
begin
    {
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
        Open;
        dbgResultado.Refresh;
      end;
    except

    end;
    }
    
    //Verifica se o perfil de acesso � de Assistente Social e
    //desabilita o bot�o Servi�o Social...
    if vvIND_PERFIL = 3 then
      btnServicoSocial.Visible := True
    else
      btnServicoSocial.Visible := False;

    //Para perfis diferentes de Gestor e Assistente Social
    //if vvIND_PERFIL = 2 then

    if (vvIND_PERFIL in [1,18,19,8] ) then
    begin
      btnEstatisticas.Visible := True;
      btnEstatisticas.Enabled := True;
    end else
    begin
      //Ainda ficar� desabilitado at� a adapta��o para Unidade
      btnEstatisticas.Visible := False;
      btnEstatisticas.Enabled := False;;
    end;

    //Perfil Triagem e Administrador podem excluir prontu�rios...
    if (vvIND_PERFIL in [1,9]) then
      btnExcluirProntuario.Visible := True
    else btnExcluirProntuario.Visible := False;                                   
end;

procedure TfrmFichaCadastroModelo.btnFrequenciaClick(Sender: TObject);
begin
  Application.CreateForm(TfrmNovoControleFrequencia, frmNovoControleFrequencia);
  frmNovoControleFrequencia.ShowModal;
end;

procedure TfrmFichaCadastroModelo.butDesligamentoClick(Sender: TObject);
begin
  Application.CreateForm (TfrmDesligamento, frmDesligamento);
  mmINTEGRACAO := True;

  with dmDeca.cdsSel_Cadastro_Frequencia do
  begin
    Close;
    Params.ParamByName('@pe_id_frequencia').Value   := Null;
    Params.ParamByName('@pe_cod_matricula').Value   := Trim(dmDeca.cdsSel_Cadastro.FieldByname('cod_matricula').Value);
    Params.ParamByName('@pe_cod_unidade').Value     := Null;
    Params.ParamByName('@pe_num_ano').AsInteger     := StrToInt(Copy(DateToStr(Date()),7,4));
    Params.ParamByName('@pe_num_mes').AsInteger     := mmMES_ATUAL; //StrToInt(Copy(txtData.Text,4,2));
    Params.ParamByName('@pe_cod_turma').Value       := Null;
    Params.ParamByName('@pe_log_cod_usuario').Value := Null;
    Params.ParamByName('@pe_num_cotas').Value       := Null;
    Open;

    if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0 then
    begin
      frmDesligamento.meMotivoDesligamento.Lines.Add('*************************************************************');
      frmDesligamento.meMotivoDesligamento.Lines.Add('Aten��o !!!');
      frmDesligamento.meMotivoDesligamento.Lines.Add('O aluno apresenta ' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_faltas').Value) +
                                     ' faltas no per�odo de ' + IntToStr(mmMES_ATUAL) + '/' + Copy(DateToStr(Date()),7,4));
    end;

    end;

  //Verificar a quantidade de faltas no m�s anterior
  with dmDeca.cdsSel_Cadastro_Frequencia do
  begin
    Close;
    Params.ParamByName('@pe_id_frequencia').Value   := Null;
    Params.ParamByName('@pe_cod_matricula').Value   := Trim(dmDeca.cdsSel_Cadastro.FieldByname('cod_matricula').Value);
    Params.ParamByName('@pe_cod_unidade').Value     := Null;
    Params.ParamByName('@pe_num_ano').AsInteger     := StrToInt(Copy(DateToStr(Date()),7,4));
    Params.ParamByName('@pe_num_mes').AsInteger     := mmMES_ANTERIOR;
    Params.ParamByName('@pe_cod_turma').Value       := Null;
    Params.ParamByName('@pe_log_cod_usuario').Value := Null;
    Params.ParamByName('@pe_num_cotas').Value       := Null;
    Open;

    if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount > 0 then
    begin
      frmDesligamento.meMotivoDesligamento.Lines.Add('O aluno apresenta ' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_faltas').Value) +
                                                     ' faltas no per�odo de ' + IntToStr(mmMES_ANTERIOR) + '/' + Copy(DateToStr(Date()),7,4));
      frmDesligamento.meMotivoDesligamento.Lines.Add('*************************************************************');
    end;
  end;

  frmDesligamento.ShowModal;

end;

procedure TfrmFichaCadastroModelo.butTransferenciaClick(Sender: TObject);
begin
  Application.CreateForm (TfrmNovaTransferencia, frmNovaTransferencia);
  frmNovaTransferencia.ShowModal;
end;

procedure TfrmFichaCadastroModelo.btnComposicaoFamiliarClick(
  Sender: TObject);
begin
  Application.CreateForm(TfrmFichaCrianca, frmFichaCrianca);
  //frmFichaCrianca.FillForm ('S', dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value);
  mmINTEGRACAO := True;
  mmNUM_ABA := 1;
  //frmFichaCrianca.PageControl1.ActivePageIndex := 0;
  frmFichaCrianca.ShowModal;
end;

procedure TfrmFichaCadastroModelo.btnAcompanhamentoEscolarClick(
  Sender: TObject);
begin
  Application.CreateForm(TfrmFichaCrianca, frmFichaCrianca);
  //frmFichaCrianca.FillForm ('S', dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value);
  mmINTEGRACAO := True;
  frmFichaCrianca.ShowModal;
end;

procedure TfrmFichaCadastroModelo.dbgResultadoCellClick(Column: TColumn);
begin
  //Exibe a Fotografia da crian�a
  vv_JPG := nil;
  try
    with dmDeca.cdsSel_Cadastro_Foto do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      Open;

      b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

      if (b.Size > 0) then
      begin
        try
          vv_JPG := TJPEGImage.Create;
          vv_JPG.LoadFromStream (b);
          imgFOTOGRAFIA.Picture.Assign (vv_JPG);
          imgFOTOGRAFIA.Picture.Assign (vv_JPG);
        except end
      end
      else
      begin
        imgFOTOGRAFIA.Picture.Assign (nil);
        imgFOTOGRAFIA.Picture.Assign (nil);
      end;

      vv_JPG.Free;
      b.Destroy;

    end

  except end;
end;

procedure TfrmFichaCadastroModelo.Button1Click(Sender: TObject);
begin
  //frmSkype.ShowModal;
end;

procedure TfrmFichaCadastroModelo.btnRegistroAtendimentoClick(
  Sender: TObject);
begin
  Application.CreateForm (TfrmRegistroAtendimento, frmRegistroAtendimento);
  mmINTEGRACAO := True;
  frmRegistroAtendimento.ShowModal;
end;

procedure TfrmFichaCadastroModelo.btnServicoSocialClick(Sender: TObject);
begin
  Application.CreateForm (TfrmEvolucaoServicoSocial, frmEvolucaoServicoSocial);
  mmINTEGRACAO := True;
  frmEvolucaoServicoSocial.ShowModal;
end;

procedure TfrmFichaCadastroModelo.butSuspensaoClick(Sender: TObject);
begin
  Application.CreateForm (TfrmSuspensao, frmSuspensao);
  mmINTEGRACAO := True;
  frmSuspensao.ShowModal;
end;

procedure TfrmFichaCadastroModelo.butAfastamentoClick(Sender: TObject);
begin
  Application.CreateForm (TfrmAfastamentoEntrada, frmAfastamentoEntrada);
  mmINTEGRACAO := True;
  frmAfastamentoEntrada.ShowModal;
end;

procedure TfrmFichaCadastroModelo.SpeedButton2Click(Sender: TObject);
begin
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
        Open;
        dbgResultado.Refresh;
      end;
    except

    end;

    //Verifica se o perfil de acesso � de Assistente Social e
    //desabilita o bot�o Servi�o Social...
    if vvIND_PERFIL = 3 then
      btnServicoSocial.Visible := True
    else
      btnServicoSocial.Visible := False;
end;

procedure TfrmFichaCadastroModelo.btnPesquisaClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    //frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);
      try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := vvCOD_MATRICULA_PESQUISA;
        Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
        Open;
        dbgResultado.Refresh;
      end;
      except end;

    //Exibe a Fotografia da crian�a
    vv_JPG := nil;
    try
      with dmDeca.cdsSel_Cadastro_Foto do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
        Open;

        b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

        if (b.Size > 0) then
        begin
          try
            vv_JPG := TJPEGImage.Create;
            vv_JPG.LoadFromStream (b);
            imgFOTOGRAFIA.Picture.Assign (vv_JPG);
            imgFOTOGRAFIA.Picture.Assign (vv_JPG);
          except end
        end
        else
        begin
          imgFOTOGRAFIA.Picture.Assign (nil);
          imgFOTOGRAFIA.Picture.Assign (nil);
        end;

        vv_JPG.Free;
        b.Destroy;

      end

    except end;
    

  end;
end;

procedure TfrmFichaCadastroModelo.dbgResultadoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {
  //Exibe a Fotografia da crian�a
  vv_JPG := nil;
  try
    with dmDeca.cdsSel_Cadastro_Foto do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      Open;

      b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

      if (b.Size > 0) then
      begin
        try
          vv_JPG := TJPEGImage.Create;
          vv_JPG.LoadFromStream (b);
          imgFOTOGRAFIA.Picture.Assign (vv_JPG);
          imgFOTOGRAFIA.Picture.Assign (vv_JPG);
        except end
      end
      else
      begin
        imgFOTOGRAFIA.Picture.Assign (nil);
        imgFOTOGRAFIA.Picture.Assign (nil);
      end;

      vv_JPG.Free;
      b.Destroy;

    end

  except end;      }
end;

procedure TfrmFichaCadastroModelo.dbgResultadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{
  //Exibe a Fotografia da crian�a
  vv_JPG := nil;
  try
    with dmDeca.cdsSel_Cadastro_Foto do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      Open;

      b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

      if (b.Size > 0) then
      begin
        try
          vv_JPG := TJPEGImage.Create;
          vv_JPG.LoadFromStream (b);
          imgFOTOGRAFIA.Picture.Assign (vv_JPG);
          imgFOTOGRAFIA.Picture.Assign (vv_JPG);
        except end
      end
      else
      begin
        imgFOTOGRAFIA.Picture.Assign (nil);
        imgFOTOGRAFIA.Picture.Assign (nil);
      end;

      vv_JPG.Free;
      b.Destroy;

    end

  except end;
 } 
end;

procedure TfrmFichaCadastroModelo.btnSairClick(Sender: TObject);
begin
  frmFichaCadastroModelo.Close;
end;

procedure TfrmFichaCadastroModelo.btnEstatisticasClick(Sender: TObject);
begin
  Application.CreateForm (TfrmDadosRelatorio, frmDadosRelatorio);
  frmDadosRelatorio.ShowModal;
end;

procedure TfrmFichaCadastroModelo.btnFotografiaClick(Sender: TObject);
begin
  Application.CreateForm(TfrmCadastroFoto, frmCadastroFoto);
  frmCadastroFoto.ShowModal;
end;

procedure TfrmFichaCadastroModelo.dbgResultadoDblClick(Sender: TObject);
begin
  btnFichaCompleta.Click;
end;

end.
