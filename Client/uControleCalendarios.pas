unit uControleCalendarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, ExtCtrls, StdCtrls, Grids, DBGrids, Db, ComCtrls, Buttons, Mask;

type
  TfrmControleCalendarios = class(TForm)
    Image1: TImage;
    Bevel1: TBevel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dsCalendarios: TDataSource;
    dbgCalendarios: TDBGrid;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    btnNovo1: TBitBtn;
    btnSalvar1: TBitBtn;
    btnCancelar1: TBitBtn;
    btnAlterar1: TBitBtn;
    btnLancarDatas: TBitBtn;
    btnSair1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edDescricao: TEdit;
    edDataInclusao: TEdit;
    Label4: TLabel;
    edNomeUsuario: TEdit;
    cbSituacao: TComboBox;
    panTituloCalendario: TPanel;
    GroupBox2: TGroupBox;
    meData: TMaskEdit;
    edNumSemana: TEdit;
    rgTipoFeriado: TRadioGroup;
    Panel2: TPanel;
    dbDatas: TDBGrid;
    Panel4: TPanel;
    btnNovo2: TBitBtn;
    btnSalvar2: TBitBtn;
    btnCancelar2: TBitBtn;
    BitBtn4: TBitBtn;
    btnSair2: TBitBtn;
    btnCalendario: TBitBtn;
    dsItensCalendario: TDataSource;
    procedure AtualizaTela (Modo: String);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNovo1Click(Sender: TObject);
    procedure btnSalvar1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure meDataExit(Sender: TObject);
    function NumSemana (const vData: TDateTime): Word;
    procedure btnCancelar1Click(Sender: TObject);
    procedure btnLancarDatasClick(Sender: TObject);
    procedure btnSalvar2Click(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnNovo2Click(Sender: TObject);
    procedure btnCancelar2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmControleCalendarios: TfrmControleCalendarios;
  vvMUDA_ABA : Boolean;

implementation

uses uDM, uPrincipal, uItensCalendario, uCalendarios;

{$R *.DFM}

{ TfrmControleCalendarios }

procedure TfrmControleCalendarios.AtualizaTela(Modo: String);
begin
  if Modo = 'P' then
  begin

    //Aba Calend�rios Cadastrados
    edDescricao.Clear;
    edDescricao.Enabled    := False;
    cbSituacao.ItemIndex   := -1;
    cbSituacao.Enabled     := False;
    edDataInclusao.Clear;
    edDataInclusao.Enabled := False;
    edNomeUsuario.Clear;
    edNomeUsuario.Enabled  := False;

    btnNovo1.Enabled     := True;
    btnSalvar1.Enabled   := False;
    btnAlterar1.Enabled  := False;
    btnCancelar1.Enabled := False;
    btnSair1.Enabled     := True;

    btnLancarDatas.Enabled  := True;

    vvMUDA_ABA := True;

  end
  else if Modo = 'N' then
  begin

    edDescricao.Enabled  := True;
    cbSituacao.Enabled   := True;
    edDataInclusao.Text  := DateToStr(Date());
    edNomeUsuario.Text   := vvNOM_USUARIO;

    btnNovo1.Enabled     := False;
    btnSalvar1.Enabled   := True;
    btnAlterar1.Enabled  := False;
    btnCancelar1.Enabled := True;
    btnSair1.Enabled     := False;

    btnLancarDatas.Enabled  := False;

    vvMUDA_ABA := False;

  end
  else if Modo = 'A' then
  begin

    edDescricao.Enabled  := True;
    cbSituacao.Enabled   := True;
    edDataInclusao.Text  := DateToStr(Date());
    edNomeUsuario.Text   := vvNOM_USUARIO;

    btnNovo1.Enabled     := False;
    btnSalvar1.Enabled   := False;
    btnAlterar1.Enabled  := True;
    btnCancelar1.Enabled := True;
    btnSair1.Enabled     := False;

    btnLancarDatas.Enabled  := False;    

    vvMUDA_ABA := False;

  end;
end;

procedure TfrmControleCalendarios.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA = False then AllowChange := False;
end;

procedure TfrmControleCalendarios.btnNovo1Click(Sender: TObject);
begin
  AtualizaTela('N');
  edDescricao.SetFocus;
  cbSituacao.ItemIndex := 0;
end;

procedure TfrmControleCalendarios.btnSalvar1Click(Sender: TObject);
begin
  //Validar o nome antes da grava��o
  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Trim(edDescricao.Text);
      Params.ParamByName('@pe_flg_atual').Value      := Null;
      Open;

      if (dmDeca.cdsSel_Calendario.RecordCount > 0) then
      begin
        ShowMessage ('J� existe um calend�rio com esse nome ! Favor informar outro nome...');
        edDescricao.SetFocus;
      end

      else
      begin
        //Atualiza todos os calendarios para "anteriores"
        with dmDeca.cdsAlt_Calendario do
        begin
          Close;
          //Params.ParamByName('@pe_cod_calendario').Value := Null;
          Params.ParamByName('@pe_flg_atual').Value      := 1;
          //Params.ParamByName('@pe_dsc_calendario').Value := Null;
          Execute;
        end;

        with dmDeca.cdsInc_Calendario do
        begin
          Close;
          Params.ParamByName('@pe_dsc_calendario').Value  := Trim(edDescricao.Text);
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Params.ParamByName('@pe_flg_atual').Value       := 0;
          Execute;

          //Atualiza o grid
          with dmDeca.cdsSel_Calendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value := Null;
            Params.ParamByName('@pe_dsc_calendario').Value := Null;
            Params.ParamByName('@pe_flg_atual').Value      := Null;
            Open;
            dbgCalendarios.Refresh;
          end;
        end;
        AtualizaTela('P');
      end;
    end;
  except
  begin
    ShowMessage('Aten��o!!! Os dados n�o foram gravados. Entre em contato com o Administrador do Sistema!');
    AtualizaTela('P');
  end;
end;
end;

procedure TfrmControleCalendarios.FormShow(Sender: TObject);
begin

  PageControl1.ActivePageIndex := 0;

  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Null;
      Params.ParamByName('@pe_flg_atual').Value      := Null;
      Open;

      dbgCalendarios.Refresh;

    end;
  except end;
end;

procedure TfrmControleCalendarios.meDataExit(Sender: TObject);
var
  str_data_inicial : String;
  data_inicial, data_final : TDateTime;
  num_semanas : Integer;
begin
  edNumSemana.Clear;
  edNumSemana.Text := IntToStr(NumSemana(StrToDate(meData.Text)));

  str_data_inicial := '01/01/' + Copy(meData.Text,7,4);
  data_inicial := StrToDate(str_data_inicial);
  data_final := StrToDate(meData.Text);
  num_semanas := trunc(data_final-data_inicial) div 7;
  edNumSemana.Text := IntToStr(num_semanas+1);
end;

function TfrmControleCalendarios.NumSemana(const vData: TDateTime): Word;
var
  Ano, Mes, Dia : Word;
  DataTEMP : TDateTime;
begin

  DecodeDate (vData, Ano, Mes, Dia);
  DataTEMP := EncodeDate(Ano, 1, 1);
  Result := (Trunc(vData-DataTEMP))DIV 7;//+DayOfWeek(DataTEMP)-1) DIV 7;

  if Result = 0 then
  begin
    Result := 51
  end
  else
  begin
    Result := Result-1;
  end;
end;

procedure TfrmControleCalendarios.btnCancelar1Click(Sender: TObject);
begin
  AtualizaTela ('P');
end;

procedure TfrmControleCalendarios.btnLancarDatasClick(Sender: TObject);
begin
  if dmDeca.cdsSel_Calendario.FieldByName ('flg_atual').Value = 0 then
  begin
    PageControl1.ActivePageIndex := 1;
    panTituloCalendario.Caption  := dmDeca.cdsSel_Calendario.FieldByName ('dsc_calendario').Value;

    with dmDeca.cdsSel_ItensCalendario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_calendario').Value   := dmDeca.cdsSel_Calendario.FieldByName ('cod_calendario').Value;
      Params.ParamByName ('@pe_ind_tipo_feriado').Value := Null;
      Params.ParamByName ('@pe_num_dia').Value          := Null;
      Params.ParamByName ('@pe_num_mes').Value          := Null;
      Params.ParamByName ('@pe_num_ano').Value          := Null;
      Params.ParamByName ('@pe_num_semana').Value       := Null;
      Open;
      dbDatas.Refresh;
    end;

  end
  else
  begin
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Esse calend�rio n�o � o atual...',
                            '[Sistema Deca] - Controle de Calend�rios',
                            MB_OK + MB_ICONINFORMATION);

  end;
end;

procedure TfrmControleCalendarios.btnSalvar2Click(Sender: TObject);
begin
  if (rgTipoFeriado.ItemIndex > -1) or (edNumSemana.Text = '00') or (Length(meData.Text) = 0) then
  begin

    //Validar a data
    try
      with dmDeca.cdsSel_ItensCalendario do
      begin

        Close;
        Params.ParamByName ('@pe_cod_calendario').Value := Null;
        Params.ParamByName ('@pe_ind_tipo_feriado').Value := Null;
        Params.ParamByName ('@pe_num_dia').AsInteger := StrToInt(Copy(meData.Text,1,2));
        Params.ParamByName ('@pe_num_mes').AsInteger := StrToInt(Copy(meData.Text,4,2));
        Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(Copy(meData.Text,4,2));
        Params.ParamByName ('@pe_num_semana').Value := Null;
        Open;

        //Se encontrou � porque a data j� foi lan�ada
        if (RecordCount > 0) then
        begin
          ShowMessage ('Aten��o !!! Data j� lan�ada...');
          meData.SetFocus;
        end
        else
        begin

          //Insere a data
          with dmDeca.cdsInc_ItensCalendario do
          begin

            Close;
            Params.ParamByName('@pe_cod_calendario').AsInteger := dmDeca.cdsSel_Calendario.FieldByName('cod_calendario').AsInteger;
            Params.ParamByName('@pe_ind_tipo_feriado').Value   := rgTipoFeriado.ItemIndex + 1;
            Params.ParamByName('@pe_num_dia').AsInteger        := StrToInt(Copy(meData.Text,1,2));
            Params.ParamByName('@pe_num_mes').Value            := StrToInt(Copy(meData.Text,4,2));
            Params.ParamByName('@pe_num_ano').Value            := StrToInt(Copy(meData.Text,7,4));
            Params.ParamByName('@pe_num_semana').Value         := StrToInt(edNumSemana.Text);
            Params.ParamByName('@pe_log_cod_usuario').Value    := vvCOD_USUARIO;
            Execute;

            //Atualiza o grid
            with dmDeca.cdsSel_ItensCalendario do
            begin
              Close;
              Params.ParamByName ('@pe_cod_calendario').AsInteger := dmDeca.cdsSel_Calendario.FieldByName('cod_calendario').AsInteger;;
              Params.ParamByName ('@pe_ind_tipo_feriado').Value   := Null;
              Params.ParamByName ('@pe_num_dia').Value            := Null;
              Params.ParamByName ('@pe_num_mes').Value            := Null; //
              Params.ParamByName ('@pe_num_ano').Value            := Null;//StrToInt(Copy(meData.Text,4,2));
              Params.ParamByName ('@pe_num_semana').Value         := Null;
              Open;
              dbDatas.Refresh;
            end;
          end;

        end;
      end;
    except end;
    
  end;
end;

procedure TfrmControleCalendarios.TabSheet2Show(Sender: TObject);
begin

  meData.Clear;
  GroupBox2.Enabled := False;
  rgTipoFeriado.ItemIndex := -1;
  rgTipoFeriado.Enabled := False;

  btnNovo2.Enabled     := True;
  btnSalvar2.Enabled   := False;
  btnCancelar2.Enabled := False;
  btnSair2.Enabled     := True;

  vvMUDA_ABA := True;
  
end;

procedure TfrmControleCalendarios.btnNovo2Click(Sender: TObject);
begin

  vvMUDA_ABA := False;
  GroupBox2.Enabled := True;
  meData.SetFocus;
  rgTipoFeriado.Enabled := True;

  btnNovo2.Enabled     := False;
  btnSalvar2.Enabled   := True;
  btnCancelar2.Enabled := True;
  btnSair2.Enabled     := False;

end;

procedure TfrmControleCalendarios.btnCancelar2Click(Sender: TObject);
begin
  meData.Clear;
  GroupBox2.Enabled := False;
  rgTipoFeriado.ItemIndex := -1;
  rgTipoFeriado.Enabled := False;

  btnNovo2.Enabled     := True;
  btnSalvar2.Enabled   := False;
  btnCancelar2.Enabled := False;
  btnSair2.Enabled     := True;

  vvMUDA_ABA := True;
end;

end.
