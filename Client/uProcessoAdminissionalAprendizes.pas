unit uProcessoAdminissionalAprendizes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, ComObj, Db;

type
  TfrmProcessoAdminissionalAprendizes = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dbgAdmissoes: TDBGrid;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    GroupBox13: TGroupBox;
    GroupBox14: TGroupBox;
    GroupBox15: TGroupBox;
    GroupBox16: TGroupBox;
    Panel3: TPanel;
    GroupBox17: TGroupBox;
    GroupBox18: TGroupBox;
    GroupBox19: TGroupBox;
    GroupBox20: TGroupBox;
    GroupBox21: TGroupBox;
    GroupBox22: TGroupBox;
    GroupBox23: TGroupBox;
    GroupBox24: TGroupBox;
    GroupBox25: TGroupBox;
    GroupBox26: TGroupBox;
    TabSheet5: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    rgPrimeiroEmprego: TRadioGroup;
    GroupBox27: TGroupBox;
    GroupBox28: TGroupBox;
    GroupBox29: TGroupBox;
    rgContribuiSindicato: TRadioGroup;
    GroupBox30: TGroupBox;
    GroupBox31: TGroupBox;
    Panel4: TPanel;
    rgDesejaSeguroVida: TRadioGroup;
    GroupBox32: TGroupBox;
    meBeneficiariosSeguroVida: TMemo;
    Panel5: TPanel;
    GroupBox33: TGroupBox;
    meDependentes: TMemo;
    GroupBox34: TGroupBox;
    GroupBox35: TGroupBox;
    GroupBox36: TGroupBox;
    rgGuardaRegularizada: TRadioGroup;
    TabSheet6: TTabSheet;
    Panel6: TPanel;
    rgNecessitaVT: TRadioGroup;
    rgNecessitaPasse: TRadioGroup;
    GroupBox37: TGroupBox;
    GroupBox38: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Panel7: TPanel;
    GroupBox39: TGroupBox;
    GroupBox40: TGroupBox;
    GroupBox41: TGroupBox;
    GroupBox42: TGroupBox;
    GroupBox43: TGroupBox;
    GroupBox44: TGroupBox;
    rgPortadorNecessidades: TRadioGroup;
    GroupBox45: TGroupBox;
    Panel8: TPanel;
    edLocalUnidade: TEdit;
    edEndereco: TEdit;
    edComplemento: TEdit;
    edBairro: TEdit;
    cbGrauInstrucao: TComboBox;
    edEMail: TEdit;
    edNacionalidade: TEdit;
    edLocalNascimento: TEdit;
    edNumRG: TEdit;
    edEmissorRG: TEdit;
    edNumCTPS: TEdit;
    edSerieCTPS: TEdit;
    edUFCTPS: TEdit;
    edNumCPF: TEdit;
    mskEmissaoPIS: TMaskEdit;
    edBancoPIS: TEdit;
    mskAdmissao: TMaskEdit;
    mskTerminoContrato: TMaskEdit;
    mskExameMedico: TMaskEdit;
    edNumCCorrente: TEdit;
    cbCargoFuncao: TComboBox;
    edValorContribuicaoSindicato: TEdit;
    edValorSalarioBase: TEdit;
    edHorarioTrabalho: TEdit;
    edNomePai: TEdit;
    edNomeMae: TEdit;
    edConjuge: TEdit;
    edResponsavel: TEdit;
    edItinerarioVT: TEdit;
    edItinerarioPasse: TEdit;
    cbRaca: TComboBox;
    edAltura: TEdit;
    edPeso: TEdit;
    edCorOlhos: TEdit;
    edCorCabelos: TEdit;
    edTipoSanguineo: TEdit;
    edQualDeficiencia: TEdit;
    edEstadoCivil: TEdit;
    btnEmitirContrato: TSpeedButton;
    btnNovaAdmissao: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnInserir: TSpeedButton;
    btnCancelar: TSpeedButton;
    GroupBox49: TGroupBox;
    cbCurso: TComboBox;
    dsAdmissoes: TDataSource;
    GroupBox46: TGroupBox;
    edNumCartaoSUS: TEdit;
    rgFumante: TRadioGroup;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    mskCep: TMaskEdit;
    mskTelefoneRes: TMaskEdit;
    mskTelefoneCel: TMaskEdit;
    mskNascimento: TMaskEdit;
    mskEmissaoRG: TMaskEdit;
    mskEmissaoCTPS: TMaskEdit;
    mskNumAgencia: TMaskEdit;
    mskQtdVT: TMaskEdit;
    mskQtdPasse: TMaskEdit;
    mskTotalVTPasse: TMaskEdit;
    edNumPIS: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovaAdmissaoClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ModoTela(Modo:String);
    procedure LimpaEdits;
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure PageControl2Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure dbgAdmissoesDblClick(Sender: TObject);
    procedure btnEmitirContratoClick(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edNumCTPSKeyPress(Sender: TObject; var Key: Char);
    procedure edNumCartaoSUSKeyPress(Sender: TObject; var Key: Char);
    procedure edNumCCorrenteKeyPress(Sender: TObject; var Key: Char);
    procedure edNumPISKeyPress(Sender: TObject; var Key: Char);
    procedure cbCargoFuncaoClick(Sender: TObject);
    procedure rgContribuiSindicatoClick(Sender: TObject);
    procedure rgDesejaSeguroVidaClick(Sender: TObject);
    procedure rgNecessitaVTClick(Sender: TObject);
    procedure rgNecessitaPasseClick(Sender: TObject);
    procedure mskTotalVTPasseEnter(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProcessoAdminissionalAprendizes: TfrmProcessoAdminissionalAprendizes;
  mmMATRICULA_OK, mmMUDA_ABA2 : Boolean;
  vListaCursoAprendiz1, vListaCursoAprendiz2 : TStringList; 

implementation

uses uFichaPesquisa, uPrincipal, uDM, uUtil, rFichaAdmissaoCadastroAprendiz;

{$R *.DFM}

procedure TfrmProcessoAdminissionalAprendizes.btnPesquisarClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        //Quando disponibilizada para as unidades, atribuir a unidade de login...
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        btnNovaAdmissao.Enabled := True;
        btnNovaAdmissao.Click;
      end;
    except end;
  end
  else
  begin
    btnNovaAdmissao.Enabled := False;
    mmMATRICULA_OK := False;
  end;
end;

procedure TfrmProcessoAdminissionalAprendizes.btnNovaAdmissaoClick(
  Sender: TObject);
begin
  //Pede confirma��o para o usu�rio para inserir nova admiss�o...
  if Application.MessageBox ( 'Deseja preencher dados para uma nova admiss�o do aluno?' + #13+#10+
                              'Favor preencher/selecionar TODOS os campos da tela para a correta emiss�o do Contrato de Aprendizagem',
                              '[Sistema Deca] - Nova Admiss�o de Aprendiz',
                              MB_YESNO + MB_ICONQUESTION ) = idYes then
  begin
    ModoTela('N');

    //Repassa os valores existentes do cadastro principal para os devidos campos na tela atual...
    with dmDeca.cdsSel_Cadastro_Move do
    begin
      edLocalUnidade.Text := Trim(FieldByName ('num_ccusto').Value) + '-' + FieldByName ('vUnidade').Value;
      edEndereco.Text     := GetValue(FieldByName ('nom_endereco').AsString);

      if (Length(FieldByName ('nom_complemento').AsString)=0) then
        edComplemento.Text  := '' else edComplemento.Text  := GetValue(FieldByName ('nom_complemento').AsString);
      edBairro.Text       := GetValue(FieldByName ('nom_bairro').AsString);
      mskCep.Text         := GetValue(FieldByName ('num_cep').AsString);
      mskTelefoneRes.Text := GetValue(FieldByName ('num_telefone').AsString);
      mskNascimento.Text  := GetValue(DateToStr(FieldByName ('dat_nascimento').AsDateTime));

      edEMail.Text        := '<seunome@dominio.com.br>';
      edLocalNascimento.Text := '<Cidade onde nasceu>';

      edNumRG.Text        := GetValue(FieldByName ('num_rg').AsString);
      edEmissorRG.Text    := 'SSP/SP';

      edNumCTPS.Text      := '00000';
      edSerieCTPS.Text    := '000';
      edUFCTPS.Text       := 'SP';
      edNumPIS.Text       := '000.0000.0000';
      edBancoPIS.Text     := 'CAIXA ECON�MICA FEDERAL';
      edNumCartaoSUS.Text := '000000000000000';
      edHorarioTrabalho.Text := '<Informe hor�rio de trabalho>';

      edNumCPF.Text       := GetValue(FieldByName ('num_cpf').AsString);
      edNumCTPS.Text      := GetValue(FieldByName ('ctps_num').AsString);
      edSerieCTPS.Text    := GetValue(FieldByName ('ctps_serie').AsString);

      edConjuge.Text       := '-';
      edEstadoCivil.Text   := 'SOLTEIRO';
      edNacionalidade.Text := 'BRASILEIRO(A)';
      edUFCTPS.Text        := 'SP';
      meBeneficiariosSeguroVida.Text := '-';

      //Repassa valores b�sicos para os campos moeda
      edValorContribuicaoSindicato.Text := '0,00';
      edValorSalarioBase.Text           := '0,00';
      edPeso.Text                       := '00,000';
      edAltura.Text                     := '0,00';

      rgPrimeiroEmprego.ItemIndex      := 0;
      rgContribuiSindicato.ItemIndex   := 0;
      rgDesejaSeguroVida.ItemIndex     := 0;
      rgGuardaRegularizada.ItemIndex   := 0;
      rgNecessitaVT.ItemIndex          := 0;
      rgNecessitaPasse.ItemIndex       := 0;
      rgPortadorNecessidades.ItemIndex := 1;
      edQualDeficiencia.Text := '-';
      rgFumante.ItemIndex              := 0;

      //edLocalUnidade.SetFocus;

    end;

    //Recupera os membros da fam�lia e preenche o campo Dependentes e repassa PAI, M�E e RESPONS�VEL
    meDependentes.Clear;
    with dmDeca.cdsSel_Cadastro_Familia do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
      Params.ParamByName ('@pe_cod_familiar').Value  := Null;
      Open;

      while not dmDeca.cdsSel_Cadastro_Familia.eof do
      begin

        meDependentes.Lines.Add (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value + ' - ' + dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value);

        if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'PAI') then
          edNomePai.Text := GetValue(FieldByName('nom_familiar').AsString)
        else if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'M�E') then
          edNomeMae.Text := GetValue(FieldByName('nom_familiar').AsString);

        if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('Responsavel').AsString = 'Sim') then
          edResponsavel.Text := GetValue(FieldByName('nom_familiar').AsString);

        dmDeca.cdsSel_Cadastro_Familia.Next;

      end;
    end;

  end
  else
  begin
    //Usu�rio n�o confirma a nova admiss�o
    //Mantem os padr�es atuais da tela...
    btnNovaAdmissao.Enabled      := False;
    PageControl1.ActivePageIndex := 0;
  end;
end;

procedure TfrmProcessoAdminissionalAprendizes.PageControl1Change(
  Sender: TObject);
begin
  if (mmMATRICULA_OK = False) then PageControl1.ActivePageIndex := 0;
end;

procedure TfrmProcessoAdminissionalAprendizes.FormShow(Sender: TObject);
begin

  vListaCursoAprendiz1 := TStringList.Create();
  vListaCursoAprendiz2 := TStringList.Create();
  cbCargoFuncao.Clear;

  ModoTela('P');

  //Atualziar os dados do grid e habilitar tela "padr�o"
  with dmDeca.cdsSel_Admissoes do
  begin
    Close;
    Params.ParamByName ('@pe_id_admissao').Value   := Null;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Open;
    dbgAdmissoes.Refresh;
  end;

  
  //Carrega a rela��o de Cargos e Fun��es na aba Contrato...
  try
    with dmDeca.cdsSel_CursosAprendizagem do
    begin
      Close;
      Params.ParamByName ('@pe_id').Value     := Null;
      Params.ParamByName ('@pe_funcao').Value := Null;
      Params.ParamByName ('@pe_cbo').Value    := Null;
      Open;

      while not (dmDeca.cdsSel_CursosAprendizagem.eof) do
      begin
        vListaCursoAprendiz1.Add(IntToStr(dmDeca.cdsSel_CursosAprendizagem.FieldByName('id').Value));
        vListaCursoAprendiz2.Add(dmDeca.cdsSel_CursosAprendizagem.FieldByName('funcao').Value);
        cbCargoFuncao.Items.Add(dmDeca.cdsSel_CursosAprendizagem.FieldByName('funcao').Value);
        dmDeca.cdsSel_CursosAprendizagem.Next;
      end;
    end;
  except end;
end;

procedure TfrmProcessoAdminissionalAprendizes.ModoTela(Modo: String);
begin
  if Modo = 'P' then
  begin
    LimpaEdits;
    PageControl1.ActivePageIndex := 0;
    PageControl2.ActivePageIndex := 0;
    mmMATRICULA_OK               := False;
    mmMUDA_ABA2                  := False;
  end
  else if Modo = 'N' then
  begin
    PageControl1.ActivePageIndex := 1;
    PageControl2.ActivePageIndex := 0;
    mmMATRICULA_OK               := False;
    mmMUDA_ABA2                  := True;
    btnAlterar.Enabled           := False;
    btnInserir.Enabled           := True;
    btnCancelar.Enabled          := True;
  end
  else if Modo = 'A' then
  begin
    PageControl1.ActivePageIndex := 1;
    PageControl2.ActivePageIndex := 0;
    mmMATRICULA_OK               := False;
    mmMUDA_ABA2                  := True;
    btnAlterar.Enabled           := True;
    btnInserir.Enabled           := False;
    btnCancelar.Enabled          := True;
  end; 
end;

procedure TfrmProcessoAdminissionalAprendizes.LimpaEdits;
var
  i : Integer;
begin
  for i := 0 to frmProcessoAdminissionalAprendizes.ComponentCount - 1 do
  if frmProcessoAdminissionalAprendizes.Components[i] is TCustomEdit then
    (frmProcessoAdminissionalAprendizes.Components[i] as TCustomEdit).Clear;
end;

procedure TfrmProcessoAdminissionalAprendizes.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  // n�o permite a troca de TabSheet
  if mmMATRICULA_OK = False then
    AllowChange := false;
end;

procedure TfrmProcessoAdminissionalAprendizes.PageControl2Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  // n�o permite a troca de TabSheet
  if mmMUDA_ABA2 = False then
    AllowChange := false;
end;

procedure TfrmProcessoAdminissionalAprendizes.btnCancelarClick(
  Sender: TObject);
begin
  ModoTela('P');
end;

procedure TfrmProcessoAdminissionalAprendizes.dbgAdmissoesDblClick(
  Sender: TObject);
begin
  ModoTela('A');
  btnAlterar.Visible := True;

  //Repassa os valores do bd aos campos
  edLocalUnidade.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('LOCAL_CCUSTO').Value;
  edEndereco.Text                   := dmDeca.cdsSel_Admissoes.FieldByName ('ENDERECO').Value;
  edComplemento.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('COMPLEMENTO').Value;
  edBairro.Text                     := dmDeca.cdsSel_Admissoes.FieldByName ('BAIRRO').Value;
  mskCep.Text                       := dmDeca.cdsSel_Admissoes.FieldByName ('CEP').Value;
  mskTelefoneRes.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('TELEFONE').Value;
  mskTelefoneCel.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('CELULAR').Value;
  cbGrauInstrucao.ItemIndex         := cbGrauInstrucao.Items.IndexOf(dmDeca.cdsSel_Admissoes.FieldByName ('GRAU_INSTRUCAO').Value);
  edEstadoCivil.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('ESTADO_CIVIL').Value;
  edEmail.Text                      := dmDeca.cdsSel_Admissoes.FieldByName ('EMAIL').Value;
  edNacionalidade.Text              := dmDeca.cdsSel_Admissoes.FieldByName ('NACIONALIDADE').Value;
  mskNascimento.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('NASCIMENTO').Value;
  edLocalNascimento.Text            := dmDeca.cdsSel_Admissoes.FieldByName ('LOCAL_NASCIMENTO').Value;
  edNumRG.Text                      := dmDeca.cdsSel_Admissoes.FieldByName ('RG').Value;
  mskEmissaoRG.Text                 := dmDeca.cdsSel_Admissoes.FieldByName ('RG_EMISSAO').Value;
  edEmissorRG.Text                  := dmDeca.cdsSel_Admissoes.FieldByName ('RG_EMISSOR').Value;
  edNumCTPS.Text                    := dmDeca.cdsSel_Admissoes.FieldByName ('CTPS').Value;
  edSerieCTPS.Text                  := dmDeca.cdsSel_Admissoes.FieldByName ('CTPS_SERIE').Value;
  mskEmissaoCTPS.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('CTPS_EMISSAO').Value;
  edUFCTPS.Text                     := dmDeca.cdsSel_Admissoes.FieldByName ('CTPS_UF').Value;
  edNumCPF.Text                     := dmDeca.cdsSel_Admissoes.FieldByName ('CPF').Value;
  ednumPIS.Text                     := dmDeca.cdsSel_Admissoes.FieldByName ('PIS').Value;
  mskEmissaoPIS.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('PIS_EMISSAO').Value;
  edBancoPIS.Text                   := dmDeca.cdsSel_Admissoes.FieldByName ('PIS_BANCO').Value;
  ednumCartaoSUS.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('CARTAO_SUS').Value;
  mskAdmissao.Text                  := dmDeca.cdsSel_Admissoes.FieldByName ('ADMISSAO').Value;
  mskTerminoContrato.Text           := dmDeca.cdsSel_Admissoes.FieldByName ('TERMINO_CONTRATO').Value;
  mskExameMedico.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('DATAEXAMEMEDICO').Value;
  rgPrimeiroEmprego.ItemIndex       := dmDeca.cdsSel_Admissoes.FieldByName ('PRIMEIRO_EMPREGO').Value;
  mskNumAgencia.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('BANCO_AGENCIA').Value;
  edNumCCorrente.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('BANCO_CONTA').Value;
  rgContribuiSindicato.ItemIndex    := dmDeca.cdsSel_Admissoes.FieldByName ('CONTRIB_SINDICAL').Value;
  edValorContribuicaoSindicato.Text := dmDeca.cdsSel_Admissoes.FieldByName ('VALOR_CONTRIB_SINDICAL').Value;
  edValorSalarioBase.Text           := dmDeca.cdsSel_Admissoes.FieldByName ('SALARIO_BASE').Value;
  cbCurso.ItemIndex                 := cbCurso.Items.IndexOf(dmDeca.cdsSel_Admissoes.FieldByName ('curso').Value);
  cbCargoFuncao.ItemIndex           := cbCargoFuncao.Items.IndexOf(dmDeca.cdsSel_Admissoes.FieldByName ('FUNCAO').Value);
  rgDesejaSeguroVida.ItemIndex      := dmDeca.cdsSel_Admissoes.FieldByName ('SEGURO_VIDA').Value;
  meBeneficiariosSeguroVida.Text    := dmDeca.cdsSel_Admissoes.FieldByName ('BENEFICIARIOS').Value;
  meDependentes.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('DEPENDENTES').Value;
  edNomePai.Text                    := dmDeca.cdsSel_Admissoes.FieldByName ('PAI').Value;
  edNomeMae.Text                    := dmDeca.cdsSel_Admissoes.FieldByName ('MAE').Value;
  edConjuge.Text                    := dmDeca.cdsSel_Admissoes.FieldByName ('CONJUGE').Value;
  edResponsavel.Text                := dmDeca.cdsSel_Admissoes.FieldByName ('RESPONSAVEL').Value;
  rgGuardaRegularizada.ItemIndex    := dmDeca.cdsSel_Admissoes.FieldByName ('GUARDA_REGULAR').Value;
  rgNecessitaVT.ItemIndex           := dmDeca.cdsSel_Admissoes.FieldByName ('VT').Value;
  edItinerarioVT.Text               := dmDeca.cdsSel_Admissoes.FieldByName ('VT_TIPO_ITINERARIO').Value;
  mskQtdVT.Text                     := dmDeca.cdsSel_Admissoes.FieldByName ('VT_QTD_DIA').Value;
  rgNecessitaPasse.ItemIndex        := dmDeca.cdsSel_Admissoes.FieldByName ('PE').Value;
  edItinerarioPasse.Text            := dmDeca.cdsSel_Admissoes.FieldByName ('PE_TIPO_ITINERARIO').Value;
  mskQtdPasse.Text                  := dmDeca.cdsSel_Admissoes.FieldByName ('PE_QTD_DIA').Value;
  mskTotalVTPasse.Text              := dmDeca.cdsSel_Admissoes.FieldByName ('QTD_ADMISSAO').Value;
  cbRaca.ItemIndex                  := dmDeca.cdsSel_Admissoes.FieldByName ('RACA').Value;
  edAltura.Text                     := dmDeca.cdsSel_Admissoes.FieldByName ('ALTURA').Value;
  edPeso.Text                       := dmDeca.cdsSel_Admissoes.FieldByName ('PESO').Value;
  edCorOlhos.Text                   := dmDeca.cdsSel_Admissoes.FieldByName ('OLHOS').Value;
  edCorCabelos.Text                 := dmDeca.cdsSel_Admissoes.FieldByName ('CABELO').Value;
  edTipoSanguineo.Text              := dmDeca.cdsSel_Admissoes.FieldByName ('TIPO_SANGUINEO').Value;
  rgPortadorNecessidades.ItemIndex  := dmDeca.cdsSel_Admissoes.FieldByName ('PORTADOR_NECESSIDADES').Value;
  edQualDeficiencia.Text            := dmDeca.cdsSel_Admissoes.FieldByName ('QUAL_NECESSIDADE').Value;
  rgFumante.ItemIndex               := dmDeca.cdsSel_Admissoes.FieldByName ('FUMANTE').Value;
end;

procedure TfrmProcessoAdminissionalAprendizes.btnEmitirContratoClick(
  Sender: TObject);
var
  winword, doc2, docs2 : Variant;
  arquivo2 : String;
begin

  arquivo2 := (ExtractFilePath(Application.ExeName) + '\Aditamentos\DRH-43 CONTRATO DE APRENDIZAGEM.doc' );
  //Cria o objeto principal do controle (Word)
  winword := CreateOleObject('Word.Application');
  //Exibe o word
  winword.Visible := True;
  //Pega uma interface para o objeto que manipula os documentos
  docs2 := Winword.Documents;
  //Abre o documento
  doc2 := Docs2.Open (arquivo2);

  try
    with dmDeca.cdsSel_Admissoes do
    begin
      doc2.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('NOM_NOME').AsString);
      doc2.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('NOM_NOME').AsString);
      doc2.Content.Find.Execute( FindText := '%ENDERECO%', replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('ENDERECO').AsString);
      doc2.Content.Find.Execute( FindText := '%BAIRRO%', replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('BAIRRO').AsString);

      doc2.Content.Find.Execute( FindText := '%CARGO_FUNCAO_CBO%', replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('FUNCAO').AsString+' - CBO: '+dmDeca.cdsSel_Admissoes.FieldByName ('CBO').AsString);

      doc2.Content.Find.Execute( FindText := '%CTPS_NUMERO%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('CTPS').AsInteger );
      doc2.Content.Find.Execute( FindText := '%CTPS_SERIE%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('CTPS_SERIE').AsInteger );

      doc2.Content.Find.Execute( FindText := '%DATA_ADMISSAO%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('ADMISSAO').AsDateTime );
      doc2.Content.Find.Execute( FindText := '%TERMINO_CONTRATO%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('TERMINO_CONTRATO').Value );

      doc2.Content.Find.Execute( FindText := '%HORAS_JORNADA_PRATICA%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('DIARIA_PRATICA').Value );
      doc2.Content.Find.Execute( FindText := '%HORAS_JORNADA_TEORICA%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('DIARIA_TEORICA').Value );

      doc2.Content.Find.Execute( FindText := '%TOTAL_HORAS_PRATICA%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('PRATICA_TOTAL').Value );
      doc2.Content.Find.Execute( FindText := '%TOTAL_HORAS_TEORICA%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('TEORICA_TOTAL').Value );

      doc2.Content.Find.Execute( FindText := '%TOTAL_HORAS%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('TOTAL_PROGRAMA').Value );

      doc2.Content.Find.Execute( FindText := '%CURSO%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('CURSO').AsString );

      doc2.Content.Find.Execute( FindText := '%GRAU_INSTRUCAO%' , replacewith := dmDeca.cdsSel_Admissoes.FieldByName ('GRAU_INSTRUCAO').AsString );

      //Solicita a impress�o da Ficha de Admiss�o...
      if Application.MessageBox ('Deseja visualizar os dados da Ficha de Admiss�o/Cadastro de Aprendiz?',
                                 '[Sistema Deca] - Ficha de Admiss�o/Cadastro de Aprendiz',
                                 MB_ICONQUESTION + MB_YESNO) = idYes then
      begin
        Application.CreateForm (TrelFichaAdmissaoCadastroAprendiz, relFichaAdmissaoCadastroAprendiz);
        relFichaAdmissaoCadastroAprendiz.Preview;
        relFichaAdmissaoCadastroAprendiz.Free;
      end;
    end;
  except end;

end;

procedure TfrmProcessoAdminissionalAprendizes.btnInserirClick(
  Sender: TObject);
begin

  //Pede confirma��o do usu�rio
  if Application.MessageBox ('Deseja salvar os dados informados na Ficha de Cadastro?',
                             '[Sistema Deca] - Ficha de Cadastro para Admiss�o',
                             MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    with dmDeca.cdsInc_Admissoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value            := GetValue(txtMatricula.Text);
      Params.ParamByName ('@pe_local_ccusto').Value             := GetValue(edLocalUnidade.Text);
      Params.ParamByName ('@pe_endereco').Value                 := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_complemento').Value              := GetValue(edComplemento.Text);
      Params.ParamByName ('@pe_bairro').Value                   := GetValue(edbairro.Text);
      Params.ParamByName ('@pe_cep').Value                      := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_telefone').Value                 := GetValue(mskTelefoneRes.Text);
      Params.ParamByName ('@pe_celular').Value                  := GetValue(mskTelefoneCel.Text);
      Params.ParamByName ('@pe_grau_instrucao').Value           := cbGrauInstrucao.Text;
      Params.ParamByName ('@pe_estado_civil').Value             := GetValue(edEstadoCivil.Text);
      Params.ParamByName ('@pe_email').Value                    := GetValue(edEMail.Text);
      Params.ParamByName ('@pe_nacionalidade').Value            := GetValue(edNacionalidade.Text);
      Params.ParamByName ('@pe_nascimento').Value               := StrToDate(mskNascimento.Text);
      Params.ParamByName ('@pe_local_nascimento').Value         := GetValue(edLocalNascimento.Text);
      Params.ParamByName ('@pe_rg').Value                       := GetValue(edNumRG.Text);
      Params.ParamByName ('@pe_rg_emissao').Value               := StrToDate(mskEmissaoRG.Text);
      Params.ParamByName ('@pe_rg_emissor').Value               := GetValue(edEmissorRG.Text);
      Params.ParamByName ('@pe_ctps').Value                     := GetValue(edNumCTPS.Text);
      Params.ParamByName ('@pe_ctps_Serie').Value               := GetValue(edSerieCTPS.Text);
      Params.ParamByName ('@pe_ctps_emissao').Value             := StrToDate(mskEmissaoCTPS.Text);
      Params.ParamByName ('@pe_ctps_uf').Value                  := GetValue(edUFCTPS.Text);
      Params.ParamByName ('@pe_cpf').Value                      := GetValue(edNumCPF.Text);
      Params.ParamByName ('@pe_pis').Value                      := GetValue(edNumPIS.Text);
      Params.ParamByName ('@pe_pis_emissao').Value              := StrToDate(mskEmissaoPIS.Text);
      Params.ParamByName ('@pe_pis_banco').Value                := Trim(edBancoPIS.Text);
      Params.ParamByName ('@pe_data_admissao').Value            := StrToDate(mskAdmissao.Text);
      Params.ParamByName ('@pe_termino_contrato').Value         := StrToDate(mskTerminoContrato.Text);
      Params.ParamByName ('@pe_data_examemedico').Value         := StrToDate(mskExameMedico.Text);
      Params.ParamByName ('@pe_primeiro_emprego').Value         := rgPrimeiroEmprego.ItemIndex;
      Params.ParamByName ('@pe_banco_agencia').Value            := StrToInt(mskNumAgencia.Text);
      Params.ParamByName ('@pe_banco_conta').Value              := StrToInt(edNumCCorrente.Text);
      Params.ParamByName ('@pe_contrib_sindical').Value         := rgContribuiSindicato.ItemIndex;
      Params.ParamByName ('@pe_valor_contrib_sindical').AsFloat := StrToFloat(edValorContribuicaoSindicato.Text);
      Params.ParamByName ('@pe_salario_base').Value             := StrToFloat(edValorSalarioBase.Text);
      //Params.ParamByName ('@pe_horario_trabalho').Value := GetValue(edHorarioTrabalho.Text);
      Params.ParamByName ('@pe_curso').Value                    := cbCurso.Text;
      Params.ParamByName ('@pe_cargo_funcao').Value             := StrToInt(vListaCursoAprendiz1.Strings[cbCargoFuncao.ItemIndex]);
      Params.ParamByName ('@pe_seguro_vida').Value              := rgDesejaSeguroVida.ItemIndex;
      Params.ParamByName ('@pe_beneficiarios').Value            := GetValue(meBeneficiariosSeguroVida.Text);
      Params.ParamByName ('@pe_dependentes').Value              := GetValue(meDependentes.Text);
      Params.ParamByName ('@pe_pai').Value                      := GetValue(edNomePai.Text);
      Params.ParamByName ('@pe_mae').Value                      := GetValue(edNomeMae.Text);
      Params.ParamByName ('@pe_conjuge').Value                  := GetValue(edConjuge.Text);
      Params.ParamByName ('@pe_responsavel').Value              := GetValue(edResponsavel.Text);
      Params.ParamByName ('@pe_guarda_regular').Value           := rgGuardaRegularizada.ItemIndex;
      Params.ParamByName ('@pe_vt').Value                       := rgNecessitaVT.ItemIndex;
      Params.ParamByName ('@pe_vt_tipo_itinerario').Value       := GetValue(edItinerarioVT.Text);
      Params.ParamByName ('@pe_vt_qtd_dia').Value               := StrToInt(Trim(mskQtdVT.Text));
      Params.ParamByName ('@pe_pe').Value                       := rgNecessitaPasse.ItemIndex;
      Params.ParamByName ('@pe_pe_tipo_itinerario').Value       := GetValue(edItinerarioPasse.Text);
      Params.ParamByName ('@pe_pe_qtd_dia').Value               := StrToInt(Trim(mskQtdPasse.Text));
      Params.ParamByName ('@pe_qtd_admissao').Value             := StrToInt(mskTotalVTPasse.Text);
      Params.ParamByName ('@pe_raca').Value                     := cbRaca.ItemIndex;
      Params.ParamByName ('@pe_altura').Value                   := StrToFloat(edAltura.Text);
      Params.ParamByName ('@pe_peso').Value                     := StrToFloat(edPeso.Text);
      Params.ParamByName ('@pe_olhos').Value                    := GetValue(edCorOlhos.Text);
      Params.ParamByName ('@pe_cabelo').Value                   := GetValue(edCorCabelos.Text);
      Params.ParamByName ('@pe_tipo_sanguineo').Value           := GetValue(edTipoSanguineo.Text);
      Params.ParamByName ('@pe_portador_necessidades').Value    := rgPortadorNecessidades.ItemIndex;
      Params.ParamByName ('@pe_qual_necessidade').Value         := GetValue(edQualDeficiencia.Text);
      Params.ParamByName ('@pe_cod_usuario').Value              := vvCOD_USUARIO;
      Params.ParamByName ('@pe_cartao_sus').Value               := GetValue(edNumCartaoSUS.Text);
      Params.ParamByName ('@pe_fumante').Value                  := rgFumante.ItemIndex;
      Execute;

      //Atualziar os dados do grid e habilitar tela "padr�o"
      with dmDeca.cdsSel_Admissoes do
      begin
        Close;
        Params.ParamByName ('@pe_id_admissao').Value   := Null;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        Open;
        dbgAdmissoes.Refresh;
      end;

      ModoTela('P'); 

    end;
  end;
end;

procedure TfrmProcessoAdminissionalAprendizes.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end;
end;

procedure TfrmProcessoAdminissionalAprendizes.edNumCTPSKeyPress(
  Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) and (Key <> #8) then Key := #0;
end;

procedure TfrmProcessoAdminissionalAprendizes.edNumCartaoSUSKeyPress(
  Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) and (Key <> #8) then Key := #0;
end;

procedure TfrmProcessoAdminissionalAprendizes.edNumCCorrenteKeyPress(
  Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) and (Key <> #8) then Key := #0;
end;

procedure TfrmProcessoAdminissionalAprendizes.edNumPISKeyPress(
  Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) and (Key <> #8) then Key := #0;
end;

procedure TfrmProcessoAdminissionalAprendizes.cbCargoFuncaoClick(
  Sender: TObject);
begin
  ShowMessage (cbCargoFuncao.Text);
end;

procedure TfrmProcessoAdminissionalAprendizes.rgContribuiSindicatoClick(
  Sender: TObject);
begin
  if rgContribuiSindicato.ItemIndex = 1 then edValorContribuicaoSindicato.Text := '0,00';
end;

procedure TfrmProcessoAdminissionalAprendizes.rgDesejaSeguroVidaClick(
  Sender: TObject);
begin
  if rgDesejaSeguroVida.ItemIndex = 0 then meBeneficiariosSeguroVida.SetFocus else meBeneficiariosSeguroVida.Text := '<N�o optante por seguro de vida>';
end;

procedure TfrmProcessoAdminissionalAprendizes.rgNecessitaVTClick(
  Sender: TObject);
begin
  if rgNecessitaVT.ItemIndex = 0 then
  begin
    edItinerarioVT.Text := '<Informe o tipo de VT e o itiner�rio>';
    mskQtdVT.Text := '000';
  end
  else
  begin
    edItinerarioVT.Text := '<N�o h� necessidade de VT>';
    mskQtdVT.Text := '000';
  end;
end;

procedure TfrmProcessoAdminissionalAprendizes.rgNecessitaPasseClick(
  Sender: TObject);
begin
  if rgNecessitaPasse.ItemIndex = 0 then
  begin
    edItinerarioPasse.Text := '<Informe o tipo de Passe Escolar e o itiner�rio>';
    mskQtdPasse.Text := '000';

  end
  else
  begin
    edItinerarioPasse.Text := '<N�o h� necessidade de Passe Escolar>';
    mskQtdPasse.Text := '000';
  end;
end;

procedure TfrmProcessoAdminissionalAprendizes.mskTotalVTPasseEnter(
  Sender: TObject);
begin
  mskTotalVTPasse.Text := '0000';  
end;

procedure TfrmProcessoAdminissionalAprendizes.btnAlterarClick(
  Sender: TObject);
begin
  if Application.MessageBox ('Deseja manter as altera��es efetudas nos dados de Admiss�o?',
                             '[Sistema Deca] - Altera��o de Dados Admissionais',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    with dmDeca.cdsUpd_Admissao do
    begin
      Close;
      Params.ParamByName ('@pe_id_admissao').Value              := dmDeca.cdsSel_Admissoes.FieldByName ('id_admissao').Value;
      Params.ParamByName ('@pe_cod_matricula').Value            := dmDeca.cdsSel_Admissoes.FieldByname ('cod_matricula').Value;
      Params.ParamByName ('@pe_local_ccusto').Value             := GetValue(edLocalUnidade.Text);
      Params.ParamByName ('@pe_endereco').Value                 := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_complemento').Value              := GetValue(edComplemento.Text);
      Params.ParamByName ('@pe_bairro').Value                   := GetValue(edbairro.Text);
      Params.ParamByName ('@pe_cep').Value                      := GetValue(mskCep.Text);
      Params.ParamByName ('@pe_telefone').Value                 := GetValue(mskTelefoneRes.Text);
      Params.ParamByName ('@pe_celular').Value                  := GetValue(mskTelefoneCel.Text);
      Params.ParamByName ('@pe_grau_instrucao').Value           := cbGrauInstrucao.Text;
      Params.ParamByName ('@pe_estado_civil').Value             := GetValue(edEstadoCivil.Text);
      Params.ParamByName ('@pe_email').Value                    := GetValue(edEMail.Text);
      Params.ParamByName ('@pe_nacionalidade').Value            := GetValue(edNacionalidade.Text);
      Params.ParamByName ('@pe_nascimento').Value               := StrToDate(mskNascimento.Text);
      Params.ParamByName ('@pe_local_nascimento').Value         := GetValue(edLocalNascimento.Text);
      Params.ParamByName ('@pe_rg').Value                       := GetValue(edNumRG.Text);
      Params.ParamByName ('@pe_rg_emissao').Value               := StrToDate(mskEmissaoRG.Text);
      Params.ParamByName ('@pe_rg_emissor').Value               := GetValue(edEmissorRG.Text);
      Params.ParamByName ('@pe_ctps').Value                     := GetValue(edNumCTPS.Text);
      Params.ParamByName ('@pe_ctps_Serie').Value               := GetValue(edSerieCTPS.Text);
      Params.ParamByName ('@pe_ctps_emissao').Value             := StrToDate(mskEmissaoCTPS.Text);
      Params.ParamByName ('@pe_ctps_uf').Value                  := GetValue(edUFCTPS.Text);
      Params.ParamByName ('@pe_cpf').Value                      := GetValue(edNumCPF.Text);
      Params.ParamByName ('@pe_pis').Value                      := GetValue(edNumPIS.Text);
      Params.ParamByName ('@pe_pis_emissao').Value              := StrToDate(mskEmissaoPIS.Text);
      Params.ParamByName ('@pe_pis_banco').Value                := Trim(edBancoPIS.Text);
      Params.ParamByName ('@pe_data_admissao').Value            := StrToDate(mskAdmissao.Text);
      Params.ParamByName ('@pe_termino_contrato').Value         := StrToDate(mskTerminoContrato.Text);
      Params.ParamByName ('@pe_data_examemedico').Value         := StrToDate(mskExameMedico.Text);
      Params.ParamByName ('@pe_primeiro_emprego').Value         := rgPrimeiroEmprego.ItemIndex;
      Params.ParamByName ('@pe_banco_agencia').Value            := StrToInt(Trim(mskNumAgencia.Text));
      Params.ParamByName ('@pe_banco_conta').Value              := StrToInt(Trim(edNumCCorrente.Text));
      Params.ParamByName ('@pe_contrib_sindical').Value         := rgContribuiSindicato.ItemIndex;
      Params.ParamByName ('@pe_valor_contrib_sindical').AsFloat := StrToFloat(edValorContribuicaoSindicato.Text);
      Params.ParamByName ('@pe_salario_base').Value             := StrToFloat(edValorSalarioBase.Text);
      Params.ParamByName ('@pe_curso').Value                    := cbCurso.Text;
      Params.ParamByName ('@pe_cargo_funcao').Value             := StrToInt(vListaCursoAprendiz1.Strings[cbCargoFuncao.ItemIndex]);
      Params.ParamByName ('@pe_seguro_vida').Value              := rgDesejaSeguroVida.ItemIndex;
      Params.ParamByName ('@pe_beneficiarios').Value            := GetValue(meBeneficiariosSeguroVida.Text);
      Params.ParamByName ('@pe_dependentes').Value              := GetValue(meDependentes.Text);
      Params.ParamByName ('@pe_pai').Value                      := GetValue(edNomePai.Text);
      Params.ParamByName ('@pe_mae').Value                      := GetValue(edNomeMae.Text);
      Params.ParamByName ('@pe_conjuge').Value                  := GetValue(edConjuge.Text);
      Params.ParamByName ('@pe_responsavel').Value              := GetValue(edResponsavel.Text);
      Params.ParamByName ('@pe_guarda_regular').Value           := rgGuardaRegularizada.ItemIndex;
      Params.ParamByName ('@pe_vt').Value                       := rgNecessitaVT.ItemIndex;
      Params.ParamByName ('@pe_vt_tipo_itinerario').Value       := GetValue(edItinerarioVT.Text);
      Params.ParamByName ('@pe_vt_qtd_dia').Value               := StrToInt(Trim(mskQtdVT.Text));
      Params.ParamByName ('@pe_pe').Value                       := rgNecessitaPasse.ItemIndex;
      Params.ParamByName ('@pe_pe_tipo_itinerario').Value       := GetValue(edItinerarioPasse.Text);
      Params.ParamByName ('@pe_pe_qtd_dia').Value               := StrToInt(Trim(mskQtdPasse.Text));
      Params.ParamByName ('@pe_qtd_admissao').Value             := StrToInt(mskTotalVTPasse.Text);
      Params.ParamByName ('@pe_raca').Value                     := cbRaca.ItemIndex;
      Params.ParamByName ('@pe_altura').Value                   := StrToFloat(edAltura.Text);
      Params.ParamByName ('@pe_peso').Value                     := StrToFloat(edPeso.Text);
      Params.ParamByName ('@pe_olhos').Value                    := GetValue(edCorOlhos.Text);
      Params.ParamByName ('@pe_cabelo').Value                   := GetValue(edCorCabelos.Text);
      Params.ParamByName ('@pe_tipo_sanguineo').Value           := GetValue(edTipoSanguineo.Text);
      Params.ParamByName ('@pe_portador_necessidades').Value    := rgPortadorNecessidades.ItemIndex;
      Params.ParamByName ('@pe_qual_necessidade').Value         := GetValue(edQualDeficiencia.Text);
      Params.ParamByName ('@pe_cod_usuario').Value              := vvCOD_USUARIO;
      Params.ParamByName ('@pe_cartao_sus').Value               := GetValue(edNumCartaoSUS.Text);
      Params.ParamByName ('@pe_fumante').Value                  := rgFumante.ItemIndex;
      Execute;

      ModoTela('P');

    end;
  end
  else
  begin
    ModoTela('P');
  end;
end;

end.
