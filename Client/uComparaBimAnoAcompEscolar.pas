unit uComparaBimAnoAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, Grids, DBGrids, StdCtrls, Db, ComCtrls, Gauges, Excel97, ComObj,
  jpeg, Mask, Clipbrd, Psock, NMsmtp;

type
  TfrmComparaBimestreAno = class(TForm)
    SaveDialog1: TSaveDialog;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    mskAno1: TMaskEdit;
    Label4: TLabel;
    mskAno2: TMaskEdit;
    SpeedButton1: TSpeedButton;
    Gauge1: TGauge;
    Panel1: TPanel;
    Label5: TLabel;
    SpeedButton2: TSpeedButton;
    ProgressBar1: TProgressBar;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComparaBimestreAno: TfrmComparaBimestreAno;
  CSVDecaFile : TextFile;
  objexcel2014, Sheet2014 : Variant;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmComparaBimestreAno.SpeedButton1Click(Sender: TObject);
var
    mmRG_ESCOLAR, mmNOM_NOME, mmCOD_MATRICULA, mmUNIDADE, mmSTATUSCADASTRO, mmPERIODO_FUNDHAS : String;
    lin : Integer;

    //Vari�veis para compara��o - ref: 1.� Bimestre
    mmESCOLA_A1_B1, mmSERIE_A1_B1, mmSITUACAO_A1_B1, mmAPROV_A1_B1, mmFREQ_A1_B1, mmOBS_A1_B1, mmTURMA_A1_B1 : String;
    mmESCOLA_A1_B2, mmSERIE_A1_B2, mmSITUACAO_A1_B2, mmAPROV_A1_B2, mmFREQ_A1_B2, mmOBS_A1_B2, mmTURMA_A1_B2 : String;
    mmESCOLA_A1_B3, mmSERIE_A1_B3, mmSITUACAO_A1_B3, mmAPROV_A1_B3, mmFREQ_A1_B3, mmOBS_A1_B3, mmTURMA_A1_B3 : String;
    mmESCOLA_A2_B1, mmSERIE_A2_B1, mmSITUACAO_A2_B1, mmAPROV_A2_B1, mmFREQ_A2_B1, mmOBS_A2_B1, mmTURMA_A2_B1 : String;
    mmESCOLA_A2_B2, mmSERIE_A2_B2, mmSITUACAO_A2_B2, mmAPROV_A2_B2, mmFREQ_A2_B2, mmOBS_A2_B2, mmTURMA_A2_B2 : String;
    mmESCOLA_A2_B3, mmSERIE_A2_B3, mmSITUACAO_A2_B3, mmAPROV_A2_B3, mmFREQ_A2_B3, mmOBS_A2_B3, mmTURMA_A2_B3, mmPERIODO_ESCOLAR : String;
    mmPERIODO_A1_B1, mmPERIODO_A1_B2, mmPERIODO_A1_B3, mmPERIODO_A2_B1, mmPERIODO_A2_B2, mmPERIODO_A2_B3 : String;
    celula, mmUNIDADE_ATUAL, mmFORMULA : String;

begin

  mmUNIDADE_ATUAL := '';
  Label5.Caption := 'POR FAVOR, AGUARDE !!! GERANDO SEUS DADOS...';
  //Criar a planilha, solicitando o local a ser salvo...
  objExcel2014 := CreateOleObject('Excel.Application');
  objExcel2014.Workbooks.Add(1);
  SaveDialog1.Execute;
  objExcel2014.WorkBooks[1].SaveAs (SaveDialog1.FileName);
  objExcel2014.Visible := True;
  objExcel2014.WorkBooks[1].Sheets.Add;

  Sheet2014 := objExcel2014.WorkBooks[1].WorkSheets[1];
  Sheet2014.Range['A1','XFD1048576'].Font.Name  := 'Tahoma'; // Fonte
  Sheet2014.Range['A1','XFD1048576'].Font.Size  := 8; // Tamanho da Fonte 

  //DADOS DO ALUNO
  Sheet2014.Cells[1,1].Value                     := 'DADOS DO ALUNO';
  Sheet2014.Range['A1:I2'].Interior.Color        := $F9C8D8;
  Sheet2014.Range['A1:I2'].Mergecells            := True;
  Sheet2014.Range['A1:I2'].HorizontalAlignment   := 3;
  Sheet2014.Range['A1:I2'].VerticalAlignment     := 2;

  //ESCOLARIDADE ATUAL
  Sheet2014.Cells[1,10].Value                    := 'ESCOLARIDADE ATUAL';
  Sheet2014.Range['J1:M2'].Interior.Color        := $F9C8D8;
  Sheet2014.Range['J1:M2'].Mergecells            := True;
  Sheet2014.Range['J1:M2'].HorizontalAlignment   := 3;
  Sheet2014.Range['J1:M2'].VerticalAlignment     := 2;

  //ANO 1
  Sheet2014.Cells[1,14].Value                    := 'ANO ' + mskAno1.Text;
  Sheet2014.Range['N1:AI1'].Interior.Color       := $F9C8D8;
  Sheet2014.Range['N1:AI1'].Mergecells           := True;
  Sheet2014.Range['N1:AI1'].HorizontalAlignment  := 3;
  Sheet2014.Range['N1:AI1'].VerticalAlignment    := 2;

  //ANO 1 - 1 BIMESTRE
  Sheet2014.Cells[2,14].Value                    := '1.� BIMESTRE';
  Sheet2014.Range['N2:T2'].Interior.Color        := $BFE2F9;
  Sheet2014.Range['N2:T2'].Mergecells            := True;
  Sheet2014.Range['N2:T2'].HorizontalAlignment   := 3;
  Sheet2014.Range['N2:T2'].VerticalAlignment     := 2;

  //ANO 1 - 2 BIMESTRE
  Sheet2014.Cells[2,21].Value                    := '2.� BIMESTRE';
  Sheet2014.Range['U2:AA2'].Interior.Color       := $BFE2F9;
  Sheet2014.Range['U2:AA2'].Mergecells           := True;
  Sheet2014.Range['U2:AA2'].HorizontalAlignment  := 3;
  Sheet2014.Range['U2:AA2'].VerticalAlignment    := 2;

  //ANO 1 - 3 BIMESTRE
  Sheet2014.Cells[2,28].Value                    := '3.� BIMESTRE';
  Sheet2014.Range['AB2:AH2'].Interior.Color      := $BFE2F9;
  Sheet2014.Range['AB2:AH2'].Mergecells          := True;
  Sheet2014.Range['AB2:AH2'].HorizontalAlignment := 3;
  Sheet2014.Range['AB2:AH2'].VerticalAlignment   := 2;

  //ANO 2
  Sheet2014.Cells[1,36].Value                    := 'ANO ' + mskAno2.Text;
  Sheet2014.Range['AJ1:BE1'].Interior.Color      := $F9C8D8;
  Sheet2014.Range['AJ1:BE1'].Mergecells          := True;
  Sheet2014.Range['AJ1:BE1'].HorizontalAlignment := 3;
  Sheet2014.Range['AJ1:BE1'].VerticalAlignment   := 2;

  //ANO 2 - 1 BIMESTRE
  Sheet2014.Cells[2,36].Value                    := '1.� BIMESTRE';
  Sheet2014.Range['AJ2:AP2'].Interior.Color      := $BFE2F9;
  Sheet2014.Range['AJ2:AP2'].Mergecells          := True;
  Sheet2014.Range['AJ2:AP2'].HorizontalAlignment := 3;
  Sheet2014.Range['AJ2:AP2'].VerticalAlignment   := 2;

  //ANO 2 - 2 BIMESTRE
  Sheet2014.Cells[2,43].Value                    := '2.� BIMESTRE';
  Sheet2014.Range['AQ2:AW2'].Interior.Color      := $BFE2F9;
  Sheet2014.Range['AQ2:AW2'].Mergecells          := True;
  Sheet2014.Range['AQ2:AW2'].HorizontalAlignment := 3;
  Sheet2014.Range['AQ2:AW2'].VerticalAlignment   := 2;

  //ANO 2 - 3 BIMESTRE
  Sheet2014.Cells[2,50].Value                    := '3.� BIMESTRE';
  Sheet2014.Range['AX2:BD2'].Interior.Color      := $BFE2F9;
  Sheet2014.Range['AX2:BD2'].Mergecells          := True;
  Sheet2014.Range['AX2:BD2'].HorizontalAlignment := 3;
  Sheet2014.Range['AX2:BD2'].VerticalAlignment   := 2;

  //OBSERVA��ES ANO 1
  Sheet2014.Cells[3,35].Value                    := 'OBSERVA��ES';
  Sheet2014.Range['AI2'].Interior.Color          := $BFE2F9;
  Sheet2014.Range['AI2'].HorizontalAlignment     := 3;
  Sheet2014.Range['AI2'].VerticalAlignment       := 2;

  //OBSERVA��ES ANO 2
  Sheet2014.Cells[3,57].Value                    := 'OBSERVA��ES';
  Sheet2014.Range['BE2'].Interior.Color          := $BFE2F9;
  Sheet2014.Range['BE2'].HorizontalAlignment     := 3;
  Sheet2014.Range['BE2'].VerticalAlignment       := 2;

  //DADOS DO EDUCACENSO
  Sheet2014.Cells[1,58].Value                    := 'EDUCANCENSO';
  Sheet2014.Range['BF1:BJ2'].Interior.Color      := $F9C8D8;
  Sheet2014.Range['BF1:BJ2'].Mergecells          := True;
  Sheet2014.Range['BF1:BJ2'].HorizontalAlignment := 3;
  Sheet2014.Range['BF1:BJ2'].VerticalAlignment   := 2;

  //Montar o cabe�alho para receber os campos
  Sheet2014.Range['A3:BJ3'].Interior.Color       := $66CDAA;

  Sheet2014.Cells[3,1].Value                     := 'RA';
  Sheet2014.Range['A3:A3'].ColumnWidth           := 15;

  Sheet2014.Range['B3:B3'].ColumnWidth           := 35;
  Sheet2014.Cells[3,2].Value                     := 'Nome';

  Sheet2014.Range['C3:C3'].ColumnWidth           := 10;
  Sheet2014.Cells[3,3].Value                     := 'Matr�cula';

  Sheet2014.Range['D3:D3'].ColumnWidth           := 10;
  Sheet2014.Cells[3,4].Value                     := 'Nascimento';

  Sheet2014.Range['E3:E3'].ColumnWidth           := 10;
  Sheet2014.Cells[3,5].Value                     := 'Idade';

  Sheet2014.Range['F3:F3'].ColumnWidth           := 50;
  Sheet2014.Cells[3,6].Value                     := 'Unidade';

  Sheet2014.Range['G3:G3'].ColumnWidth           := 15;
  Sheet2014.Cells[3,7].Value                     := 'Situa��o';

  Sheet2014.Range['H3:H3'].ColumnWidth           := 15;
  Sheet2014.Cells[3,8].Value                     := 'Per�odo FUNDHAS';

  Sheet2014.Range['I3:I3'].ColumnWidth           := 15;
  Sheet2014.Cells[3,9].Value                     := 'Sexo';

  Sheet2014.Range['J3:J3'].ColumnWidth           := 15;
  Sheet2014.Cells[3,10].Value                    := 'Per�odo Escolar';

  Sheet2014.Range['K3:K3'].ColumnWidth           := 35;
  Sheet2014.Cells[3,11].Value                    := 'Escola';

  Sheet2014.Range['L3:L3'].ColumnWidth           := 15;
  Sheet2014.Cells[3,12].Value                    := 'S�rie Escolar';

  Sheet2014.Range['M3:M3'].ColumnWidth           := 35;
  Sheet2014.Cells[3,13].Value                    := 'Turma Atual';

  //Aproveitamento, Frequ�ncia e Situa��o para o ano INICIAL
  Sheet2014.Cells[3,14].Value                    := 'APROV.';
  Sheet2014.Cells[3,15].Value                    := 'FREQ.';
  Sheet2014.Cells[3,16].Value                    := 'SIT.';
  Sheet2014.Cells[3,17].Value                    := 'PER�ODO';
  Sheet2014.Cells[3,18].Value                    := 'TURMA';
  Sheet2014.Cells[3,19].Value                    := 'ESCOLA';
  Sheet2014.Cells[3,20].Value                    := 'S�RIE';

  Sheet2014.Cells[3,21].Value                    := 'APROV.';
  Sheet2014.Cells[3,22].Value                    := 'FREQ.';
  Sheet2014.Cells[3,23].Value                    := 'SIT.';
  Sheet2014.Cells[3,24].Value                    := 'PER�ODO';
  Sheet2014.Cells[3,25].Value                    := 'TURMA';
  Sheet2014.Cells[3,26].Value                    := 'ESCOLA';
  Sheet2014.Cells[3,27].Value                    := 'S�RIE';

  Sheet2014.Cells[3,28].Value                    := 'APROV.';
  Sheet2014.Cells[3,29].Value                    := 'FREQ.';
  Sheet2014.Cells[3,30].Value                    := 'SIT.';
  Sheet2014.Cells[3,31].Value                    := 'PER�ODO';
  Sheet2014.Cells[3,32].Value                    := 'TURMA';
  Sheet2014.Cells[3,33].Value                    := 'ESCOLA';
  Sheet2014.Cells[3,34].Value                    := 'S�RIE';

  //Aproveitamento, Frequ�ncia e Situa��o para o ano FINAL
  Sheet2014.Cells[3,36].Value                    := 'APROV.';
  Sheet2014.Cells[3,37].Value                    := 'FREQ.';
  Sheet2014.Cells[3,38].Value                    := 'SIT.';
  Sheet2014.Cells[3,39].Value                    := 'PER�ODO';
  Sheet2014.Cells[3,40].Value                    := 'TURMA';
  Sheet2014.Cells[3,41].Value                    := 'ESCOLA';
  Sheet2014.Cells[3,42].Value                    := 'S�RIE';

  Sheet2014.Cells[3,43].Value                    := 'APROV.';
  Sheet2014.Cells[3,44].Value                    := 'FREQ.';
  Sheet2014.Cells[3,45].Value                    := 'SIT.';
  Sheet2014.Cells[3,46].Value                    := 'PER�ODO';
  Sheet2014.Cells[3,47].Value                    := 'TURMA';
  Sheet2014.Cells[3,48].Value                    := 'ESCOLA';
  Sheet2014.Cells[3,49].Value                    := 'S�RIE';

  Sheet2014.Cells[3,50].Value                    := 'APROV.';
  Sheet2014.Cells[3,51].Value                    := 'FREQ.';
  Sheet2014.Cells[3,52].Value                    := 'SIT.';
  Sheet2014.Cells[3,53].Value                    := 'PER�ODO';
  Sheet2014.Cells[3,54].Value                    := 'TURMA';
  Sheet2014.Cells[3,55].Value                    := 'ESCOLA';
  Sheet2014.Cells[3,56].Value                    := 'S�RIE';

  Sheet2014.Cells[3,58].Value                    := 'DATA CADASTRO';
  Sheet2014.Cells[3,59].Value                    := 'SITUA��O CEI';
  Sheet2014.Cells[3,60].Value                    := 'UNIDADE ATUAL';
  Sheet2014.Cells[3,61].Value                    := 'TURMA ATUAL';
  Sheet2014.Cells[3,62].Value                    := 'ADMISS�O';


  //Inserir aqui a coluna COTAS DE PASSE, a pedido da DECA
  Sheet2014.Cells[3,63].Value                    := 'COTA';

  //Carrega TODOS os dados do cadastro
  with dmDeca.cdsSel_Cadastro do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value   := 0;
    Open;

    //Gauge1.MinValue := 0;
    //Gauge1.MaxValue := dmDeca.cdsSel_Cadastro.RecordCount;
    ProgressBar1.Min := 0;
    ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

    //Iniciar a "transfer�ncia" dos dados a partir da linha 4
    lin := 4;
    dmDeca.cdsSel_Cadastro.First;

    //Para cada aluno do cadastro, consultar os dados dos bimestres em cada ano informado na tela inicial
    //repassar para as vari�veis e transferir para a planilha j� inicializada....
    while not dmDeca.cdsSel_Cadastro.eof do
    begin

      //Valida as unidades que n�o ser�o contempladas
      if ( ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 1 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 2 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 7 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 9 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 12 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 13 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 20 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 27 ) or
         //( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 40 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 42 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 43 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 46 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 47 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 48 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 50 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 71 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 73 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 77 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 79 ) or
         ( dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').AsInteger = 80 ) ) then
      begin
         dmDeca.cdsSel_Cadastro.Next;
      end
      else
      begin
        //Se for uma unidade "v�lida" executar os procedimentos a seguir...
        //Zerar as vari�veis iniciais
        mmCOD_MATRICULA   := '';
        mmNOM_NOME        := '';
        mmUNIDADE         := '';
        mmSTATUSCADASTRO  := '--';
        mmRG_ESCOLAR      := '--';
        mmPERIODO_FUNDHAS := '--';

        mmCOD_MATRICULA   := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
        mmNOM_NOME        := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;

        Sheet2014.Cells[lin,4].HorizontalAlignment := 3; //Centralizado
        Sheet2014.Cells[lin,4].NumberFormat        := 'dd/mm/aaaa';
        Sheet2014.Cells[lin,4].Value               := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime;
        Sheet2014.Cells[lin,4].ColumnWidth         := 15;

        mmUNIDADE         := dmDeca.cdsSel_Cadastro.FieldByName ('nom_unidade').Value;
        mmRG_ESCOLAR      := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_escolar').Value;
        mmPERIODO_FUNDHAS := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;
        mmSTATUSCADASTRO  := dmDeca.cdsSel_Cadastro.FieldByName ('vStatus').Value;

        //Repassa o valor das vari�veis para as c�lulas
        Sheet2014.Cells[lin,1].Value     := mmRG_ESCOLAR;
        Sheet2014.Cells[lin,2].Value     := mmNOM_NOME;
        Sheet2014.Cells[lin,3].Value     := mmCOD_MATRICULA;

        //F�rmula
        //Sheet2014.Cells[lin,5].NumberFormat := '@';
        //celula := 'D'+IntToStr(lin);
        //Sheet2014.Cells[lin,5].FormulaR1C1 := '=DATEDIFF(' + celula + ';HOJE();"Y")&" anos e "&DATEDIFF(' + celula + ';HOJE();"YM")&" m�s(es)"';
        //A f�rmula acima est� calculando corretamente, mas quando passa para a c�lula, da erro de OLE800...


        Sheet2014.Cells[lin,6].Value     := mmUNIDADE;
        Sheet2014.Cells[lin,7].Value     := mmSTATUSCADASTRO;
        Sheet2014.Cells[lin,8].Value     := mmPERIODO_FUNDHAS;
        Sheet2014.Cells[lin,9].Value     := dmDeca.cdsSel_Cadastro.FieldByName ('ind_sexo').AsString;

        //Rotina para pegar Escola e S�rie Atuais - HISTORICO DE ESCOLAS

        //1� BIMESTRE DO ANO INICIAL
        //Consultar dados do 1.� Bimestre do Ano INICIAL (Invertido para planilha a pedido da Cristina/Ac. Escolar)
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_num_ano_letivo').Value      := StrToInt(mskAno1.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := 1;
          Params.ParamByName ('@pe_cod_unidade').Value         := Null;
          Open;

          //Se n�o encontrou registros, repassa valores 'defaults'
          if (dmDeca.cdsSel_AcompEscolar.RecordCount < 1) then
          begin
            mmESCOLA_A1_B1   := '--';
            mmSERIE_A1_B1    := '--';
            mmSITUACAO_A1_B1 := '--';
            mmAPROV_A1_B1    := '--';
            mmFREQ_A1_B1     := '--';
            mmOBS_A1_B1      := '--';
            mmTURMA_A1_B1    := '--';
            mmPERIODO_A1_B1  := '--';
          end
          else
          begin
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = NULL then mmESCOLA_A1_B1 := '--' else mmESCOLA_A1_B1 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A1_B1 := '--' else mmSERIE_A1_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value = NULL then mmSITUACAO_A1_B1 := '--' else mmSITUACAO_A1_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value = NULL then mmAPROV_A1_B1 := '--' else mmAPROV_A1_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value = NULL then mmFREQ_A1_B1 := '--' else mmFREQ_A1_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = NULL then mmPERIODO_A1_B1 := '--' else mmPERIODO_A1_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A1_B1 := '--' else mmSERIE_A1_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString) > 0) then mmOBS_A1_B1 := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value else mmOBS_A1_B1 := '--';

            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then mmTURMA_A1_B1 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value else mmTURMA_A1_B1 := '--';

          end;
        end; //Fim da consulta dos dados do Acomp. Escolar - 1B/ano final

        //2� BIMESTRE DO ANO INICIAL
        //Consultar dados do 2.� Bimestre do Ano INICIAL (Invertido para planilha a pedido da Cristina/Ac. Escolar)
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_num_ano_letivo').Value      := StrToInt(mskAno1.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := 2;
          Params.ParamByName ('@pe_cod_unidade').Value         := Null;
          Open;

          //Se n�o encontrou registros, repassa valores 'defaults'
          if (dmDeca.cdsSel_AcompEscolar.RecordCount < 1) then
          begin
            mmESCOLA_A1_B2   := '--';
            mmSERIE_A1_B2    := '--';
            mmSITUACAO_A1_B2 := '--';
            mmAPROV_A1_B2    := '--';
            mmFREQ_A1_B2     := '--';
            mmOBS_A1_B2      := '--';
            mmTURMA_A1_B2    := '--';
            mmPERIODO_A1_B2  := '--';
          end
          else
          begin
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = NULL then mmESCOLA_A1_B2 := '--' else mmESCOLA_A1_B2 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A1_B2 := '--' else mmSERIE_A1_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value = NULL then mmSITUACAO_A1_B2 := '--' else mmSITUACAO_A1_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value = NULL then mmAPROV_A1_B2 := '--' else mmAPROV_A1_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value = NULL then mmFREQ_A1_B2 := '--' else mmFREQ_A1_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = NULL then mmPERIODO_A1_B2 := '--' else mmPERIODO_A1_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A1_B2 := '--' else mmSERIE_A1_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString) > 0) then mmOBS_A1_B2 := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value else mmOBS_A1_B2 := '--';

            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then mmTURMA_A1_B2 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value else mmTURMA_A1_B2 := '--';
          end;
        end; //Fim da consulta dos dados do Acomp. Escolar - 2B/ano inicial

        //3� BIMESTRE DO ANO FINAL
        //Consultar dados do 1.� Bimestre do Ano FINAL (Invertido para planilha a pedido da Cristina/Ac. Escolar)
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_num_ano_letivo').Value      := StrToInt(mskAno1.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := 3;
          Params.ParamByName ('@pe_cod_unidade').Value         := Null;
          Open;

          //Se n�o encontrou registros, repassa valores 'defaults'
          if (dmDeca.cdsSel_AcompEscolar.RecordCount < 1) then
          begin
            mmESCOLA_A1_B3   := '--';
            mmSERIE_A1_B3    := '--';
            mmSITUACAO_A1_B3 := '--';
            mmAPROV_A1_B3    := '--';
            mmFREQ_A1_B3     := '--';
            mmOBS_A1_B3      := '--';
            mmTURMA_A1_B3    := '--';
            mmPERIODO_A1_B3  := '--';
          end
          else
          begin
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = NULL then mmESCOLA_A1_B3 := '--' else mmESCOLA_A1_B3 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A1_B3 := '--' else mmSERIE_A1_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value = NULL then mmSITUACAO_A1_B3 := '--' else mmSITUACAO_A1_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value = NULL then mmAPROV_A1_B3 := '--' else mmAPROV_A1_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value = NULL then mmFREQ_A1_B3 := '--' else mmFREQ_A1_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = NULL then mmPERIODO_A1_B3 := '--' else mmPERIODO_A1_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A1_B3 := '--' else mmSERIE_A1_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString) > 0) then mmOBS_A1_B3 := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value else mmOBS_A1_B3 := '--';

            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then mmTURMA_A1_B3 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value else mmTURMA_A1_B3 := '--';
          end;
        end;  //Fim da consulta dos dados do Acomp. Escolar - 3B/ano inicial

        //Repassa os valores para as c�lulas...
        //Ano1 Bim1
        Sheet2014.Cells[lin,14].Value     := mmAPROV_A1_B1;
        Sheet2014.Cells[lin,15].Value     := mmFREQ_A1_B1;
        Sheet2014.Cells[lin,16].Value     := mmSITUACAO_A1_B1;
        Sheet2014.Cells[lin,17].Value     := mmPERIODO_A1_B1;
        Sheet2014.Cells[lin,18].Value     := mmTURMA_A1_B1;
        Sheet2014.Cells[lin,19].Value     := mmESCOLA_A1_B1;
        Sheet2014.Cells[lin,20].Value     := mmSERIE_A1_B1;
        //Ano2 Bim2
        Sheet2014.Cells[lin,21].Value     := mmAPROV_A1_B2;
        Sheet2014.Cells[lin,22].Value     := mmFREQ_A1_B2;
        Sheet2014.Cells[lin,23].Value     := mmSITUACAO_A1_B2;
        Sheet2014.Cells[lin,24].Value     := mmPERIODO_A1_B2;
        Sheet2014.Cells[lin,25].Value     := mmTURMA_A1_B2;
        Sheet2014.Cells[lin,26].Value     := mmESCOLA_A1_B2;
        Sheet2014.Cells[lin,27].Value     := mmSERIE_A1_B2;
        //Ano2 Bim3
        Sheet2014.Cells[lin,28].Value     := mmAPROV_A1_B3;
        Sheet2014.Cells[lin,29].Value     := mmFREQ_A1_B3;
        Sheet2014.Cells[lin,30].Value     := mmSITUACAO_A1_B3;
        Sheet2014.Cells[lin,31].Value     := mmPERIODO_A1_B3;
        Sheet2014.Cells[lin,32].Value     := mmTURMA_A1_B3;
        Sheet2014.Cells[lin,33].Value     := mmESCOLA_A1_B3;
        Sheet2014.Cells[lin,34].Value     := mmSERIE_A1_B3;
        Sheet2014.Cells[lin,35].Value     := mmOBS_A1_B1 + ' ' + mmOBS_A1_B2 + ' ' + mmOBS_A1_B3;

        //1� BIMESTRE DO ANO FINAL
        //Consultar dados do 1.� Bimestre do Ano FINAL (Invertido para planilha a pedido da Cristina/Ac. Escolar)
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_num_ano_letivo').Value      := StrToInt(mskAno2.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := 1;
          Params.ParamByName ('@pe_cod_unidade').Value         := Null;
          Open;

          //Se n�o encontrou registros, repassa valores 'defaults'
          if (dmDeca.cdsSel_AcompEscolar.RecordCount < 1) then
          begin
            mmESCOLA_A2_B1   := '--';
            mmSERIE_A2_B1    := '--';
            mmSITUACAO_A2_B1 := '--';
            mmAPROV_A2_B1    := '--';
            mmFREQ_A2_B1     := '--';
            mmOBS_A2_B1      := '--';
            mmTURMA_A2_B1    := '--';
            mmPERIODO_A2_B1  := '--';
          end
          else
          begin
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = NULL then mmESCOLA_A2_B1 := '--' else mmESCOLA_A2_B1 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A2_B1 := '--' else mmSERIE_A2_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value = NULL then mmSITUACAO_A2_B1 := '--' else mmSITUACAO_A2_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value = NULL then mmAPROV_A2_B1 := '--' else mmAPROV_A2_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value = NULL then mmFREQ_A2_B1 := '--' else mmFREQ_A2_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = NULL then mmPERIODO_A2_B1 := '--' else mmPERIODO_A2_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A2_B1 := '--' else mmSERIE_A2_B1  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString) > 0) then mmOBS_A2_B1 := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value else mmOBS_A2_B1 := '--';
            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then mmTURMA_A2_B1 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value else mmTURMA_A2_B1 := '--';
          end;
        end; //Fim da consulta dos dados do Acomp. Escolar - 1B/ano final

        //2� BIMESTRE DO ANO FINAL
        //Consultar dados do 2.� Bimestre do Ano FINAL (Invertido para planilha a pedido da Cristina/Ac. Escolar)
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_num_ano_letivo').Value      := StrToInt(mskAno2.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := 2;
          Params.ParamByName ('@pe_cod_unidade').Value         := Null;
          Open;

          //Se n�o encontrou registros, repassa valores 'defaults'
          if (dmDeca.cdsSel_AcompEscolar.RecordCount < 1) then
          begin
            mmESCOLA_A2_B2   := '--';
            mmSERIE_A2_B2    := '--';
            mmSITUACAO_A2_B2 := '--';
            mmAPROV_A2_B2    := '--';
            mmFREQ_A2_B2     := '--';
            mmOBS_A2_B2      := '--';
            mmTURMA_A2_B2    := '--';
            mmPERIODO_A2_B2  := '--';
          end
          else
          begin
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = NULL then mmESCOLA_A2_B2 := '--' else mmESCOLA_A2_B2 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A2_B2 := '--' else mmSERIE_A2_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value = NULL then mmSITUACAO_A2_B2 := '--' else mmSITUACAO_A2_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value = NULL then mmAPROV_A2_B2 := '--' else mmAPROV_A2_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value = NULL then mmFREQ_A2_B2 := '--' else mmFREQ_A2_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = NULL then mmPERIODO_A2_B2 := '--' else mmPERIODO_A2_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A2_B2 := '--' else mmSERIE_A2_B2  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString) > 0) then mmOBS_A2_B2 := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value else mmOBS_A2_B2 := '--';
            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then mmTURMA_A2_B2 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value else mmTURMA_A2_B2 := '--';
          end;
        end; //Fim da consulta dos dados do Acomp. Escolar - 2B/ano final

        //3� BIMESTRE DO ANO FINAL
        //Consultar dados do 2.� Bimestre do Ano FINAL (Invertido para planilha a pedido da Cristina/Ac. Escolar)
        with dmDeca.cdsSel_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_num_ano_letivo').Value      := StrToInt(mskAno2.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := 3;
          Params.ParamByName ('@pe_cod_unidade').Value         := Null;
          Open;

          //Se n�o encontrou registros, repassa valores 'defaults'
          if (dmDeca.cdsSel_AcompEscolar.RecordCount < 1) then
          begin
            mmESCOLA_A2_B3   := '--';
            mmSERIE_A2_B3    := '--';
            mmSITUACAO_A2_B3 := '--';
            mmAPROV_A2_B3    := '--';
            mmFREQ_A2_B3     := '--';
            mmOBS_A2_B3      := '--';
            mmTURMA_A2_B3    := '--';
            mmPERIODO_A2_B3  := '--';
          end
          else
          begin
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = NULL then mmESCOLA_A2_B3 := '--' else mmESCOLA_A2_B3 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A2_B3 := '--' else mmSERIE_A2_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value = NULL then mmSITUACAO_A2_B3 := '--' else mmSITUACAO_A2_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacao').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value = NULL then mmAPROV_A2_B3 := '--' else mmAPROV_A2_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vAproveitamento').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value = NULL then mmFREQ_A2_B3 := '--' else mmFREQ_A2_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vFrequencia').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = NULL then mmPERIODO_A2_B3 := '--' else mmPERIODO_A2_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
            if dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value = NULL then mmSERIE_A2_B3 := '--' else mmSERIE_A2_B3  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').AsString) > 0) then mmOBS_A2_B3 := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value else mmOBS_A2_B3 := '--';
            if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then mmTURMA_A2_B3 := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value else mmTURMA_A2_B3 := '--';
          end;
        end;  //Fim da consulta dos dados do Acomp. Escolar - 3B/ano final

        //Repassa os valores para as c�lulas...
        //Ano2 Bim1
        Sheet2014.Cells[lin,36].Value     := mmAPROV_A2_B1;
        Sheet2014.Cells[lin,37].Value     := mmFREQ_A2_B1;
        Sheet2014.Cells[lin,38].Value     := mmSITUACAO_A2_B1;
        Sheet2014.Cells[lin,39].Value     := mmPERIODO_A2_B1;
        Sheet2014.Cells[lin,40].Value     := mmTURMA_A2_B1;
        Sheet2014.Cells[lin,41].Value     := mmESCOLA_A2_B1;
        Sheet2014.Cells[lin,42].Value     := mmSERIE_A2_B1;
        //Ano2 Bim2
        Sheet2014.Cells[lin,43].Value     := mmAPROV_A2_B2;
        Sheet2014.Cells[lin,44].Value     := mmFREQ_A2_B2;
        Sheet2014.Cells[lin,45].Value     := mmSITUACAO_A2_B2;
        Sheet2014.Cells[lin,46].Value     := mmPERIODO_A2_B2;
        Sheet2014.Cells[lin,47].Value     := mmTURMA_A2_B2;
        Sheet2014.Cells[lin,48].Value     := mmESCOLA_A2_B2;
        Sheet2014.Cells[lin,49].Value     := mmSERIE_A2_B2;
        //Ano2 Bim3
        Sheet2014.Cells[lin,50].Value     := mmAPROV_A2_B3;
        Sheet2014.Cells[lin,51].Value     := mmFREQ_A2_B3;
        Sheet2014.Cells[lin,52].Value     := mmSITUACAO_A2_B3;
        Sheet2014.Cells[lin,53].Value     := mmPERIODO_A2_B3;
        Sheet2014.Cells[lin,54].Value     := mmTURMA_A2_B3;
        Sheet2014.Cells[lin,55].Value     := mmESCOLA_A2_B3;
        Sheet2014.Cells[lin,56].Value     := mmSERIE_A2_B3;
        Sheet2014.Cells[lin,57].Value     := mmOBS_A2_B1 + ' ' + mmOBS_A2_B2 + ' ' + mmOBS_A2_B3;

        Sheet2014.Cells[lin,58].HorizontalAlignment := 3; //Centralizado
        Sheet2014.Cells[lin,58].NumberFormat        := 'dd/mm/aaaa';
        Sheet2014.Cells[lin,58].ColumnWidth         := 15;
        if dmDeca.cdsSel_Cadastro.FieldByName ('dat_cadastro_educacenso').IsNull then Sheet2014.Cells[lin,58].Value := '--/--/----' else Sheet2014.Cells[lin,58].Value := dmDeca.cdsSel_Cadastro.FieldByName ('dat_cadastro_educacenso').AsDateTime;
        Sheet2014.Cells[lin,59].Value := '�';

        try
          with dmDeca.cdsSel_TurmaCIE do
          begin
            Close;
            Params.ParamByname ('@pe_cod_id_turmacie').Value := Null;
            Params.ParamByname ('@pe_dsc_turmacie').Value    := Null;
            Params.ParamByname ('@pe_cod_id_uecie').Value    := Null;
            Params.ParamByname ('@pe_dsc_uecie').Value       := Null;
            Params.ParamByname ('@pe_cod_id_seriecie').Value := Null;
            Params.ParamByname ('@pe_dsc_seriecie').Value    := Null;
            Params.ParamByname ('@pe_ind_periodo').Value     := Null;
            Params.ParamByname ('@pe_num_turma').Value       := dmDeca.cdsSel_Cadastro.FieldByName ('num_turma_cei').Value;
            Open;

            if dmDeca.cdsSel_Cadastro.FieldByName ('num_turma_cei').IsNull then Sheet2014.Cells[lin,60].Value := '<Unidade n�o Cadastrada>'
            else
            begin
              mmUNIDADE_ATUAL               := IntToStr(dmDeca.cdsSel_TurmaCIE.FieldByName ('num_uecie').Value) + '-' + dmDeca.cdsSel_TurmaCIE.FieldByName ('dsc_uecie').Value;
              Sheet2014.Cells[lin,60].Value := mmUNIDADE_ATUAL;
            end;
          end;
        except end;

        if dmDeca.cdsSel_Cadastro.FieldByName ('num_turma_cei').IsNull then Sheet2014.Cells[lin,61].Value := '--' else Sheet2014.Cells[lin,61].Value := dmDeca.cdsSel_Cadastro.FieldByName ('num_turma_cei').AsString;

        Sheet2014.Cells[lin,62].HorizontalAlignment := 3; //Centralizado
        Sheet2014.Cells[lin,62].NumberFormat        := 'dd/mm/aaaa';
        Sheet2014.Cells[lin,62].ColumnWidth         := 15;
        if dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').IsNull then Sheet2014.Cells[lin,61].Value := '--/--/----' else Sheet2014.Cells[lin,62].Value := dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').AsDateTime;

        //Inserir aqui o valor do campo COTAS DE PASSE do cadastro atual...

        if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cota_passes').IsNull) then
          Sheet2014.Cells[lin,63].Value := '-' else Sheet2014.Cells[lin,63].Value := dmDeca.cdsSel_Cadastro.FieldByName ('num_cota_passes').Value;

        lin := lin + 1;
        //Gauge1.Progress := Gauge1.Progress + 1;
        ProgressBar1.Position := ProgressBar1.Position + 1;
        dmDeca.cdsSel_Cadastro.Next;
      end;  //fim do enqto nao fim do cadastro
    end;
  end;
  objExcel2014.WorkBooks[1].Close;
  Label5.Caption := 'PROCESSO FINALIZADO!!! VERIFIQUE NO LOCAL ONDE FOI SALVO...';
end;

procedure TfrmComparaBimestreAno.SpeedButton2Click(Sender: TObject);
var fileINCONSISTENCIAS : TextFile;
begin
  //Gera um arquivo CSV com os dados de inconsist�ncias...
  AssignFile (fileINCONSISTENCIAS, ExtractFilePath(Application.ExeName) + 'INCONSISTENCIAS.CSV');
  ReWrite (fileINCONSISTENCIAS);


  //Verificar se existe DATA DE NASCIMENTO est� em branco
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 1;
    Open;

    Write  (fileINCONSISTENCIAS, 'DATA DE NASCIMENTO "EM BRANCO');
    Writeln(fileINCONSISTENCIAS, ';');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NOME');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NASCIMENTO');
    Writeln(fileINCONSISTENCIAS, ';');
    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('nom_nome').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('dat_nascimento').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o da data de nascimento "em branco"

  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se existe PER�ODO sem preenchimento
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 2;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'PER�ODO FUNDHAS SEM PREENCHIMENTO"');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NOME');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'PERIODO');
    Writeln(fileINCONSISTENCIAS, ';');
    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('nom_nome').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('dsc_periodo').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o do per�odo do cadastro

  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se existe RG ESCOLAR sem preenchimento
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 3;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'RG ESCOLAR SEM PREENCHIMENTO');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NOME');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'RG ESCOLAR');
    Writeln(fileINCONSISTENCIAS, ';');
    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('nom_nome').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('dsc_rg_escolar').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o do rg escolar

  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se existe PER�ODO ESCOLAR sem preenchimento
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 4;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'PER�ODO ESCOLAR SEM PREENCHIMENTO');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'ANO LETIVO');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'BIMESTRE');
    Writeln(fileINCONSISTENCIAS, ';');
    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_ano_letivo').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_bimestre').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o do per�odo escolar

  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se existe APROVEITAMENTO ESCOLAR sem preenchimento
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 5;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'APROVEITAMENTO ESCOLAR SEM PREENCHIMENTO');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'ANO LETIVO');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'BIMESTRE');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'APROVEITAMENTO ESCOLAR');
    Write  (fileINCONSISTENCIAS, ';');

    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_ano_letivo').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_bimestre').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('ind_aproveitamento').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o do aproveitamento escolar


  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se existe FREQUENCIA ESCOLAR sem preenchimento
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 6;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'FREQU�NCIA ESCOLAR SEM PREENCHIMENTO');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'ANO LETIVO');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'BIMESTRE');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'FREQU�NCIA ESCOLAR');
    Writeln(fileINCONSISTENCIAS, ';');

    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_ano_letivo').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_bimestre').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('ind_frequencia').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o da frequencia escolar


  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se o campo DATA CADASTRO EDUCACENSO est� "em branco"
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 7;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'DATA DE CADASTRO EDUCACENSO SEM PREENCHIMENTO');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NOME');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'DATA CADASTRO EDUCACENSO');
    Writeln(fileINCONSISTENCIAS, ';');

    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('nom_nome').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('dat_cadastro_educacenso').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o data cadastro educacenso

  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se o campo SITUA��O CIE EDUCACENSO est� "em branco"
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 8;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'SITUA��O CIE');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NOME');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'SITUA��O CIE');
    Writeln(fileINCONSISTENCIAS, ';');

    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('nom_nome').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_situacao_cei').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o da situa��o cie

  Writeln(fileINCONSISTENCIAS, '');
  Writeln(fileINCONSISTENCIAS, '');

  //Verificar se o campo SITUA��O CIE EDUCACENSO est� "em branco"
  with dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_tipo').Value := 9;
    Open;

    Writeln  (fileINCONSISTENCIAS, 'OBSERVA��ES EDUCACENSO');

    Write  (fileINCONSISTENCIAS, 'MATR�CULA');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'NOME');
    Write  (fileINCONSISTENCIAS, ';');
    Write  (fileINCONSISTENCIAS, 'OBSERVA��ES EDUCACENSO');
    Writeln(fileINCONSISTENCIAS, ';');

    while not (dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.eof) do
    begin
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('cod_matricula').Value);
      Write  (fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('nom_nome').Value);
      Writeln(fileINCONSISTENCIAS, dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.FieldByName ('num_situacao_cei').Value);
      dmDeca.cdsSel_VerificaInconsistencias_AcompEscolar.Next;
    end;
  end;
  //Final verifica��o da observa��o educacenso
  CloseFile(fileINCONSISTENCIAS);

  Application.MessageBox ('Arquivo gerado com sucesso!!!' + #13 + #10 +
                          'Verifique na pasta do Sistema (C:\Deca) o arquivo' + #13 + #10 +
                          'chamado INCONSISTENCIAS.CSV',
                          '[Sistema Deca] - Inconsist�ncias',
                          MB_OK + MB_ICONINFORMATION);

end;

end.
