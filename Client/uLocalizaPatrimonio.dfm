object frmLocalizaPatrimonio: TfrmLocalizaPatrimonio
  Left = 203
  Top = 128
  Width = 1012
  Height = 449
  Caption = 'Sistema DECA - [Localizar Patrim�nio]'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 6
    Top = 2
    Width = 992
    Height = 79
    Caption = 'Crit�rios de Pesquisa'
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 33
      Height = 13
      Caption = 'Campo'
    end
    object Label2: TLabel
      Left = 224
      Top = 24
      Width = 85
      Height = 13
      Caption = 'Valor de Pesquisa'
    end
    object cbCampo: TComboBox
      Left = 25
      Top = 40
      Width = 189
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnChange = cbCampoChange
      OnClick = cbCampoClick
      Items.Strings = (
        'N.� Patrim�nio'
        'Descri��o de Patrim�nio'
        'N.� S�rie')
    end
    object edValor: TEdit
      Left = 225
      Top = 39
      Width = 208
      Height = 21
      TabOrder = 1
    end
    object btnPesquisar: TButton
      Left = 464
      Top = 24
      Width = 99
      Height = 38
      Caption = '&Pesquisar'
      Default = True
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 7
    Top = 86
    Width = 993
    Height = 330
    TabOrder = 1
    object dbgEquipamentos: TDBGrid
      Left = 8
      Top = 8
      Width = 977
      Height = 314
      DataSource = dsEquipamentos
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dbgEquipamentosDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_patrimonio'
          Title.Alignment = taCenter
          Title.Caption = 'N.� Patrim�nio'
          Width = 75
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_serie'
          Title.Alignment = taCenter
          Title.Caption = 'N.� S�rie'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dsc_patrimonio'
          Title.Caption = 'Patrim�nio'
          Width = 350
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_empresa'
          Title.Caption = 'Fornecedor'
          Width = 350
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_ccusto'
          Title.Alignment = taCenter
          Title.Caption = 'Centro Custo'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_unidade'
          Title.Caption = 'Unidade'
          Width = 350
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'dat_atualiz_ccusto'
          Title.Alignment = taCenter
          Title.Caption = 'Data �ltima Atualiza��o'
          Width = 125
          Visible = True
        end>
    end
  end
  object dsEquipamentos: TDataSource
    Left = 615
    Top = 134
  end
end
