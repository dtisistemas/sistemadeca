unit uEstatisticasAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, ExtCtrls, StdCtrls, TeeProcs, TeEngine, Chart, DBChart, ComCtrls,
  Db, Grids, DBGrids, Buttons, Mask, Series;

type
  TfrmEstatisticasAcompEscolar = class(TForm)
    Panel1: TPanel;
    Bevel1: TBevel;
    Image1: TImage;
    dsDadosGrafico: TDataSource;
    Panel2: TPanel;
    rgCriterio: TRadioGroup;
    GroupBox1: TGroupBox;
    cbBimestre: TComboBox;
    mskAno: TMaskEdit;
    Label1: TLabel;
    btnGerar: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    btnRelatorio: TSpeedButton;
    DBGrid1: TDBGrid;
    procedure btnGerarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstatisticasAcompEscolar: TfrmEstatisticasAcompEscolar;
  vvTOTAL : Integer;

implementation

uses uDM, uPrincipal, rEstatisticasAcompEscolar;

{$R *.DFM}

procedure TfrmEstatisticasAcompEscolar.btnGerarClick(Sender: TObject);
begin

  //Validar crit�rio, bimestre e ano...
  if (rgCriterio.ItemIndex < 0) and (cbBimestre.ItemIndex < 0) and (Length(mskAno.Text)<4) then
  begin

  end
  else
  begin

    //Seleciona os dados e grava-os na tabela GRAFICO
    //Antes de gravar, excluir os dados do USUARIO logado...
    try

      //Zerar os registros do usuario logado na tabela GRAFICO
      with dmDeca.cdsExc_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;


      //Seleciona os dados atrav�s do crit�rio selecionado
      case rgCriterio.ItemIndex of

      //Total de alunos por escola
      0: begin

           //Seleciona as unidades para a consulta...
           with dmDeca.cdsSel_Escola do
           begin
             Close;
             Params.ParamByName ('@pe_cod_escola').Value := Null;
             Params.ParamByName ('@pe_nom_escola').Value := Null;
             Open;

             dmDeca.cdsSel_Escola.First;

             while not dmDeca.cdsSel_Escola.eof do
             begin

               with dmDeca.cdsSel_EstatisticasAcompEscolar do
               begin
                 Close;
                 Params.ParamByName ('@pe_tipo').Value           := 1;
                 Params.ParamByName ('@pe_cod_escola').Value     := dmDeca.cdsSel_Escola.FieldByName ('cod_escola').Value;
                 Params.ParamByName ('@pe_num_bimestre').Value   := cbBimestre.ItemIndex + 1;
                 Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(mskAno.Text);
                 Open;

                 //vvTOTAL := dmDeca.cdsSel_EstatisticasAcompEscolar.RecordCount;

                 //Incluir o resultado no grafico...
                 with dmDeca.cdsInc_DadosGrafico do
                 begin
                   Close;
                   Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
                   Params.ParamByName ('@pe_dsc_total').AsString    := dmDeca.cdsSel_Escola.FieldByName ('nom_escola').AsString;
                   Params.ParamByName ('@pe_num_total').AsInteger   := dmDeca.cdsSel_EstatisticasAcompEscolar.FieldByName ('vTotalEscola').AsInteger; //vvTOTAL;
                   Execute;
                 end;

               end;
               dmDeca.cdsSel_Escola.Next;
             end; //while not dmDeca.cdsSel_Escola...
           end;

           //Selecionar os dados para exibi��o no grid e gr�fico na tela...
           with dmDeca.cdsSel_DadosGrafico do
           begin
             Close;
             Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
             Open;

             DBGrid1.Refresh;

           end;

        end; //Case 0...



      //Total de alunos por s�rie escolar...
      1: begin

           //Seleciona as unidades para a consulta...
           with dmDeca.cdsSel_SerieEscolar do
           begin
             Close;
             Params.ParamByName ('@pe_cod_id_serie').Value := Null;
             Open;

             dmDeca.cdsSel_SerieEscolar.First;

             while not dmDeca.cdsSel_SerieEscolar.eof do
             begin

               with dmDeca.cdsSel_EstatisticasAcompEscolar do
               begin
                 Close;
                 Params.ParamByName ('@pe_tipo').AsInteger           := 2;
                 Params.ParamByName ('@pe_cod_id_serie').AsInteger   := dmDeca.cdsSel_SerieEscolar.FieldByName ('cod_id_serie').AsInteger;
                 Params.ParamByName ('@pe_num_bimestre').AsInteger   := cbBimestre.ItemIndex + 1;
                 Params.ParamByName ('@pe_num_ano_letivo').AsInteger := StrToInt(mskAno.Text);
                 Open;

                 //Incluir o resultado no grafico...
                 with dmDeca.cdsInc_DadosGrafico do
                 begin
                   Close;
                   Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
                   Params.ParamByName ('@pe_dsc_total').AsString    := dmDeca.cdsSel_SerieEscolar.FieldByName ('dsc_serie').AsString;
                   Params.ParamByName ('@pe_num_total').AsInteger   := dmDeca.cdsSel_EstatisticasAcompEscolar.FieldByName ('vTotalSerie').AsInteger;
                   Execute;
                 end;

               end;
               dmDeca.cdsSel_SerieEscolar.Next;
             end; //while not dmDeca.cdsSel_SerieEscolar...
           end;

           //Selecionar os dados para exibi��o no grid e gr�fico na tela...
           with dmDeca.cdsSel_DadosGrafico do
           begin
             Close;
             Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
             Open;

             DBGrid1.Refresh;

           end;

        end; //Case 1...



      //Total por situa��o no cadastro do Acomp. Escolar...
      2: begin

           with dmDeca.cdsSel_EstatisticasAcompEscolar do
           begin
             Close;
             Params.ParamByName ('@pe_tipo').AsInteger           := 3;
             Params.ParamByName ('@pe_num_bimestre').AsInteger   := cbBimestre.ItemIndex + 1;
             Params.ParamByName ('@pe_num_ano_letivo').AsInteger := StrToInt(mskAno.Text);
             Open;

             while not dmDeca.cdsSel_EstatisticasAcompEscolar.eof do
             begin

               //Incluir o resultado no grafico...
               with dmDeca.cdsInc_DadosGrafico do
               begin
                 Close;
                 Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
                 Params.ParamByName ('@pe_dsc_total').AsString    := dmDeca.cdsSel_EstatisticasAcompEscolar.FieldByName ('vSituacao').AsString;
                 Params.ParamByName ('@pe_num_total').AsInteger   := dmDeca.cdsSel_EstatisticasAcompEscolar.FieldByName ('vTotalSituacao').AsInteger;
                 Execute;
               end;
               dmDeca.cdsSel_EstatisticasAcompEscolar.Next;
             end;

           end; //while not dmDeca.cdsSel_EstatisticasAcompEscolar...

           //Selecionar os dados para exibi��o no grid e gr�fico na tela...
           with dmDeca.cdsSel_DadosGrafico do
           begin
             Close;
             Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
             Open;

             DBGrid1.Refresh;

           end;



        end; //Case 2...



        3: begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'Esse item de consulta ainda est� em desenvolvimento.' + #13+#10 +
                                  'Caso necessite, entre em contato com o Administrador do Sistema.',
                                  '[Sistema Deca] - Em desenvolvimento...',
                                  MB_OK + MB_ICONINFORMATION);
        end;  //Case 3...



        4: begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'Esse item de consulta ainda est� em desenvolvimento.' + #13+#10 +
                                  'Caso necessite, entre em contato com o Administrador do Sistema.',
                                  '[Sistema Deca] - Em desenvolvimento...',
                                  MB_OK + MB_ICONINFORMATION);
        end;  //Case 4...


        5: begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'Esse item de consulta ainda est� em desenvolvimento.' + #13+#10 +
                                  'Caso necessite, entre em contato com o Administrador do Sistema.',
                                  '[Sistema Deca] - Em desenvolvimento...',
                                  MB_OK + MB_ICONINFORMATION);
        end;  //Case 5...

        

      end; //Case...

    except
      Application.MessageBox ('Aten��o!!!' + #13+#10 +
                              'Ocorreu um erro ao tentar carregar os dados.' + #13+#10 +
                              'Favor tentar novamente ou comunicar o Administrador do Sistema.',
                              '[Sistema Deca] - Erro Consultando Dados.',
                              MB_OK + MB_ICONERROR);
      //Limpa todos os campos da tela...
      rgCriterio.ItemIndex         := 0;
      cbBimestre.ItemIndex         := 0;
      PageControl1.ActivePageIndex := 0;
      mskAno.Text                  := Copy(DateToStr(Date()),7,4);
    end;

  end




end;

procedure TfrmEstatisticasAcompEscolar.FormShow(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  cbBimestre.ItemIndex         := 0;
  mskAno.Text                  := Copy(DateToStr(Date()),7,4);
end;

procedure TfrmEstatisticasAcompEscolar.btnRelatorioClick(Sender: TObject);
begin

  Application.CreateForm (TrelEstatisticasAcompEscolar, relEstatisticasAcompEscolar);
  relEstatisticasAcompEscolar.lbTitulo.Caption := rgCriterio.Items.Strings[rgCriterio.ItemIndex] + '  -  Ref: '  + cbBimestre.Text + '/' + mskAno.Text;
  relEstatisticasAcompEscolar.Preview;
  relEstatisticasAcompEscolar.Free;



end;

end.
