unit uSelecionaAnoRAICA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls;

type
  TfrmSelecionaAnoRAICA = class(TForm)
    GroupBox1: TGroupBox;
    mskAno: TMaskEdit;
    btnGerarRAICA: TSpeedButton;
    Image1: TImage;
    Bevel1: TBevel;
    rgCriterio: TRadioGroup;
    GroupBox2: TGroupBox;
    cbDivisao: TComboBox;
    chkTodasDivisoes: TCheckBox;
    cbUnidade: TComboBox;
    mskMatricula: TMaskEdit;
    btnLocalizar: TSpeedButton;
    edNome: TEdit;
    chkTodasUnidades: TCheckBox;
    procedure btnGerarRAICAClick(Sender: TObject);
    procedure rgCriterioClick(Sender: TObject);
    procedure chkTodasDivisoesClick(Sender: TObject);
    procedure chkTodasUnidadesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLocalizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaAnoRAICA: TfrmSelecionaAnoRAICA;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses uDM, uUtil, uFichaPesquisa, uPrincipal, rAICA, rAICA2;

{$R *.DFM}

procedure TfrmSelecionaAnoRAICA.btnGerarRAICAClick(Sender: TObject);
begin

//Executa a consulta com base no ano informado
try

  case rgCriterio.ItemIndex of
    0: begin
         with dmDeca.cdsGERA_DADOS_RAICA do
         begin
           Close;
           Params.ParamByName ('@pe_nm_ano').Value        := GetValue(mskAno.Text);
           Params.ParamByName ('@pe_cod_matricula').Value := Null;
           Params.ParamByName ('@pe_cod_unidade').Value   := Null;
           if chkTodasDivisoes.Checked then
             Params.ParamByName ('@pe_ind_divisao').Value := Null
           else
             Params.ParamByName ('@pe_ind_divisao').Value := cbDivisao.ItemIndex;
           Open;
         end;
    end;

    1: begin
         with dmDeca.cdsGERA_DADOS_RAICA do
         begin
           Close;
           Params.ParamByName ('@pe_nm_ano').Value         := GetValue(mskAno.Text);
           Params.ParamByName ('@pe_cod_matricula').Value  := Null;
           if chkTodasUnidades.Checked then
             Params.ParamByName ('@pe_cod_unidade').Value  := Null
           else
             Params.ParamByName ('@pe_cod_unidade').Value  := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
           Params.ParamByName ('@pe_ind_divisao').Value    := Null ;
           Open;
         end;
    end;

    2: begin
         with dmDeca.cdsGERA_DADOS_RAICA do
         begin
           Close;
           Params.ParamByName ('@pe_nm_ano').Value        := GetValue(mskAno.Text);
           Params.ParamByName ('@pe_cod_matricula').Value := GetValue(mskMatricula.Text);
           Params.ParamByName ('@pe_cod_unidade').Value   := Null;
           Params.ParamByName ('@pe_ind_divisao').Value   := Null;
           Open;
         end;
    end;
  end;

  //Exibe o relatório de aproveitamento individual em duas fase
  //Frente e Verso...
  Application.CreateForm (TrelAICA, relAICA);
  Application.CreateForm (TrelAICA2, relAICA2);
  relAICA.QRLabel2.Caption := 'Ref.: ' + mskAno.Text;

  relAICA.Preview;
  relAICA2.Preview;
  relAICA.Free;
  relAICA2.Free;

except end;



end;

procedure TfrmSelecionaAnoRAICA.rgCriterioClick(Sender: TObject);
begin
  case rgCriterio.ItemIndex of

   0: begin

        cbDivisao.Visible        := True;
        chkTodasDivisoes.Visible := True;
        cbDivisao.SetFocus;

        cbUnidade.Visible        := False;
        chkTodasUnidades.Visible := False;

        mskMatricula.Visible     := False;
        btnLocalizar.Visible     := False;
        edNome.Clear;
        edNome.Visible           := False;

      end;

   1: begin

        cbDivisao.Visible        := False;
        chkTodasDivisoes.Visible := False;

        cbUnidade.Visible        := True;
        chkTodasUnidades.Visible := True;
        cbUnidade.SetFocus;

        mskMatricula.Visible     := False;
        btnLocalizar.Visible     := False;
        edNome.Clear;
        edNome.Visible           := False;
      end;

   2: begin

        cbDivisao.Visible        := False;
        chkTodasDivisoes.Visible := False;

        cbUnidade.Visible        := False;
        chkTodasUnidades.Visible := False;

        mskMatricula.Visible     := True;
        btnLocalizar.Visible     := True;
        edNome.Clear;
        edNome.Visible           := True;
      end;

  end; //Case...
end;

procedure TfrmSelecionaAnoRAICA.chkTodasDivisoesClick(Sender: TObject);
begin
  if chkTodasDivisoes.Checked then
  begin
    cbDivisao.Enabled   := False;
    cbDivisao.ItemIndex := -1
  end
  else cbDivisao.Enabled := True;
end;

procedure TfrmSelecionaAnoRAICA.chkTodasUnidadesClick(Sender: TObject);
begin
  if chkTodasUnidades.Checked then
  begin
    cbUnidade.Enabled    := False;
    cbUnidade.ItemIndex := -1
  end
  else cbUnidade.Enabled := True;
end;

procedure TfrmSelecionaAnoRAICA.FormShow(Sender: TObject);
begin

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();


  rgCriterio.ItemIndex := 0;

    //Carrega a lista de unidades do Aprendiz
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      cbUnidade.Clear;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        cbUnidade.Items.Add(dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
        vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
end;

procedure TfrmSelecionaAnoRAICA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vListaUnidade1.Free;
  vListaUnidade2.Free;
end;

procedure TfrmSelecionaAnoRAICA.btnLocalizarClick(Sender: TObject);
begin

  // chama a tela de pesquisa (genérica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    mskMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value        := vvCOD_UNIDADE;
        Open;
        edNome.Text := FieldByName('nom_nome').AsString;
      end
    except end;
  end;

end;

end.
