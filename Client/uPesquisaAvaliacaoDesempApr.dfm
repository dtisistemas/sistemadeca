object frmPesquisaAvaliacaoDesempAprendiz: TfrmPesquisaAvaliacaoDesempAprendiz
  Left = 285
  Top = 105
  BorderStyle = bsDialog
  Caption = '[Sistema Deca] - Pesquisa Avalia��o Desempenho'
  ClientHeight = 613
  ClientWidth = 838
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnVisualizar: TSpeedButton
    Left = 713
    Top = 574
    Width = 121
    Height = 32
    Caption = 'Visualizar'
    OnClick = btnVisualizarClick
  end
  object btnExportar: TSpeedButton
    Left = 588
    Top = 575
    Width = 121
    Height = 31
    Caption = 'Exportar'
    Enabled = False
    Flat = True
    Visible = False
    OnClick = btnExportarClick
  end
  object Panel4: TPanel
    Left = 2
    Top = 4
    Width = 834
    Height = 168
    BevelInner = bvLowered
    TabOrder = 0
    object btnPesquisaP: TSpeedButton
      Left = 153
      Top = 14
      Width = 23
      Height = 22
      Enabled = False
      Flat = True
      Glyph.Data = {
        36050000424D360500000000000036040000280000000E000000100000000100
        08000000000000010000CA0E0000C30E00000001000000000000000000003300
        00006600000099000000CC000000FF0000000033000033330000663300009933
        0000CC330000FF33000000660000336600006666000099660000CC660000FF66
        000000990000339900006699000099990000CC990000FF99000000CC000033CC
        000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
        0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
        330000333300333333006633330099333300CC333300FF333300006633003366
        33006666330099663300CC663300FF6633000099330033993300669933009999
        3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
        330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
        66006600660099006600CC006600FF0066000033660033336600663366009933
        6600CC336600FF33660000666600336666006666660099666600CC666600FF66
        660000996600339966006699660099996600CC996600FF99660000CC660033CC
        660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
        6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
        990000339900333399006633990099339900CC339900FF339900006699003366
        99006666990099669900CC669900FF6699000099990033999900669999009999
        9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
        990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
        CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
        CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
        CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
        CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
        CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
        FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
        FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
        FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
        FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000ACACACACACAC
        ACACACAC0000ACAC0000ACACACACACACACACAC02230500AC0000ACAC00000000
        00000223050500000000ACAC56ACACACAC02230505ACAC000000ACAC56D7D7D7
        02230505ACD7AC000000ACAC56000056AC0505ACD7D7AC000000AC56AC23AC00
        5656ACD7D7D7AC00000056D723AC23AC00ACD7D7D7D7AC0000005623D723AC23
        00ACD7D7D7D7AC00000056D723D723AC00ACD7D7D7D7AC000000AC56D723D700
        ACD7D7D7D7D7AC000000ACAC565600ACD7D7D7D7D7D7AC000000ACAC56D7D7D7
        D7D7D7D700000000FFFFACAC56D7D7D7D7D7D7D7ACD756AC0000ACAC56D7D7D7
        D7D7D7D7AC56ACAC0000ACAC565656565656565656ACACAC0000}
      OnClick = btnPesquisaPClick
    end
    object Label4: TLabel
      Left = 182
      Top = 19
      Width = 219
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 214
      Top = 112
      Width = 15
      Height = 13
      Caption = 'at�'
    end
    object Label2: TLabel
      Left = 214
      Top = 134
      Width = 15
      Height = 13
      Caption = 'at�'
    end
    object chkMatricula: TCheckBox
      Left = 15
      Top = 16
      Width = 69
      Height = 17
      Caption = 'Matr�cula'
      TabOrder = 0
      OnClick = chkMatriculaClick
    end
    object chkUnidade: TCheckBox
      Left = 15
      Top = 40
      Width = 67
      Height = 17
      Caption = 'Unidade'
      TabOrder = 1
      OnClick = chkUnidadeClick
    end
    object cbUnidadeP: TComboBox
      Left = 84
      Top = 38
      Width = 304
      Height = 22
      Style = csOwnerDrawFixed
      Enabled = False
      ItemHeight = 16
      TabOrder = 2
      OnClick = cbUnidadePClick
    end
    object chkEmpresa: TCheckBox
      Left = 15
      Top = 64
      Width = 98
      Height = 17
      Caption = 'Empresa/Se��o'
      Enabled = False
      TabOrder = 3
      OnClick = chkEmpresaClick
    end
    object cbSecaoP: TComboBox
      Left = 121
      Top = 62
      Width = 313
      Height = 19
      Style = csOwnerDrawFixed
      Enabled = False
      ItemHeight = 13
      TabOrder = 4
    end
    object chkPeriodoAvaliacao: TCheckBox
      Left = 15
      Top = 110
      Width = 126
      Height = 17
      Caption = 'Per�odo Avalia��o de'
      TabOrder = 5
      OnClick = chkPeriodoAvaliacaoClick
    end
    object mskDataAv1: TMaskEdit
      Left = 143
      Top = 108
      Width = 65
      Height = 21
      Enabled = False
      EditMask = 'CC/CC/CCCC'
      MaxLength = 10
      TabOrder = 6
      Text = '  /  /    '
    end
    object mskDataAv2: TMaskEdit
      Left = 235
      Top = 108
      Width = 65
      Height = 21
      Enabled = False
      EditMask = 'CC/CC/CCCC'
      MaxLength = 10
      TabOrder = 7
      Text = '  /  /    '
    end
    object chkPeriodoLancamento: TCheckBox
      Left = 15
      Top = 134
      Width = 123
      Height = 17
      Caption = 'Per�odo Lan�amento'
      TabOrder = 8
      OnClick = chkPeriodoLancamentoClick
    end
    object mskDataLanc1: TMaskEdit
      Left = 143
      Top = 132
      Width = 65
      Height = 21
      Enabled = False
      EditMask = 'CC/CC/CCCC'
      MaxLength = 10
      TabOrder = 9
      Text = '  /  /    '
    end
    object mskDataLanc2: TMaskEdit
      Left = 235
      Top = 132
      Width = 65
      Height = 21
      Enabled = False
      EditMask = 'CC/CC/CCCC'
      MaxLength = 10
      TabOrder = 10
      Text = '  /  /    '
    end
    object mskMatricula: TMaskEdit
      Left = 84
      Top = 14
      Width = 64
      Height = 21
      Enabled = False
      EditMask = '99999999'
      MaxLength = 8
      TabOrder = 11
      Text = '        '
    end
    object btnVerResultado: TBitBtn
      Left = 637
      Top = 33
      Width = 116
      Height = 46
      Caption = 'Resultados'
      Default = True
      TabOrder = 12
      OnClick = btnVerResultadoClick
      Glyph.Data = {
        F6030000424DF60300000000000076000000280000001F0000001C0000000100
        0800000000008003000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00080800080800
        0808000808000808000808000808000808000808000808000805080808080808
        0808080808080808080808080808080808080808080808080805070000000000
        0000000000000000000000000000000000000000000000000705000F080F080F
        080F080F080F080F080F080F080F080F080F080F080F080F000500080F080F08
        0F080F080F080F080F080F080F080F080F080F080F080F080005000808000000
        0000080800000000000808000000000008080000000000080005000800000E0E
        06000808000E0E06000808000E0E06000808000E0E060008000500080B000E0E
        06000808000E0E06000808000E0E06000808000E0E060008000500080B0B000E
        06000808000E0E06000808000E0E06000808000E0E06000800050008080B0B00
        06000800000E0E06000808000E0E06000808000E0E0600080005000808000B0B
        0000000B000E0E06000808000E0E06000808000E0E0600080005000808000E0B
        0B000B0B0B000E06000808000E0E06000808000E0E0600080005000808000E0E
        0B0B0B080B0B0006000808000E0E06000808000E0E0600080005000808000E0E
        060B0808000B0B00000808000E0E06000800000E0E0600080005000808000E0E
        06000808000E0B0B000808000E0E0600000B000E0E0600080005000808000E0E
        06000808000E0E0B0B0008000E0E06000B0B0B000E0600080005000808000E0E
        06000808000E0E060B0B00000E0E000B0B080B0B000600080005000808000E0E
        06000808000E0E06000B0B000E000B0B0808000B0B0000080005000808000E0E
        06000808000E0E0600080B0B000B0B000808000F0B0B00080005000808000E0E
        06000808000E0E060008080B0B0B060008080800000B0B000005000808000E0E
        06000808000F0F0E000808000B0E06000808080808080B0B0005000808000E0E
        0600080808000000080808000E0E0600080808080808080B0005000808000E0E
        0600080808080808080808000E0E060008080808080808080005000808000E0E
        0600080808080808080808000F0F0E0008080808080808080005000F08000F0F
        0E00080F080F080F080F080F0000000F080F080F080F080F000500080F080000
        00080F080F080F080F080F080F080F080F080F080F080F080005000F080F080F
        080F080F080F080F080F080F080F080F080F080F080F080F0005070000000000
        0000000000000000000000000000000000000000000000000705}
    end
    object btnLimpar: TBitBtn
      Left = 637
      Top = 85
      Width = 116
      Height = 46
      Caption = 'Limpar'
      TabOrder = 13
      OnClick = btnLimparClick
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF004EAB004EAB004EAB004E
        AB004EAB004EABFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0077F1006DE40063D8
        005FCD0060C8005CC10054B7004EAB004794003A76FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF007DFD0077F800
        75F32988ED67A9EC91C1F0A3CAF1A2C9F18BBCEC5C9DE01C73CD0053B3004797
        003D7CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF007C
        FF0078FF3597FDAAD3FCF4F9FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEA
        F3FD93BFED2274CB004CA7003C7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF027FFF0982FF8EC5FFF5FAFFFFFFFFFFFFFFD8E8FBD8E8FBC2DCFACEE2
        FAD8E7FBD8E7FBFFFFFFFFFFFFE7F1FC6CA5E00054B1003D7EFF00FFFF00FFFF
        00FFFF00FFFF00FF0A84FF0A84FFA6D2FFFFFFFFFFFFFFE1EDFDB0D3FA1577EB
        086DE8076CE7056AE70469E61876E8C2DCFAFFFFFFFFFFFFFFFFFF72A8E1004E
        AB003E7EFF00FFFF00FFFF00FFFF00FF0380FF7CBDFFFFFFFFFFFFFFE2EEFD5A
        A4F70F75EC0D72EB0C70EA1375EA086DE8076CE7056AE7056AE66EABF2E0ECFC
        FFFFFFF3F8FC2F7DCC004DA5FF00FFFF00FFFF00FF0F86FF2390FFE6F3FFFFFF
        FFE2EFFD52A0F7167AEF1378EE1478EE0E74EC0C72EA0D71E9096EE9076CE706
        6BE70569E661A4F1E0ECFCFFFFFFB6D2EE025ABA0053A9FF00FFFF00FF0A84FF
        79BBFFFFFFFFFFFFFF8CC2FB1B80F39EC8F9FDFEFF8DBEF71277ED1074EC197A
        EC1A79EB9EC6F6E7F1FD066BE8056AE7A4CBF7FFFFFFFDFDFD3F89D40058BEFF
        00FF1B8CFF188BFFC5E1FFFFFFFFFEFFFF2287F61F84F54A9CF7CDE3FCFFFFFF
        68AAF51478EE1277EDAACEF8FFFFFFA1C8F70A6EE9076DE8247FEBE0ECFCFFFF
        FF91BDE90061CF0053A91B8CFF3097FFF1F8FFFFFFFFC4E1FE268AF7278AF620
        86F53690F5C5DFFCF5FAFE6AACF67BB4F6FFFFFFA7CCF81D7CEC0C71EB0A6FEA
        086EE9CAE1FBFFFFFFC9DFF60470DE0069D71D8DFF52A7FFFCFDFFFFFFFF83BF
        FE298EF92F91F8248AF82287F63893F7C3DEFCF6FAFEEDF5FE7DB5F6197DEE11
        76ED0F75EC0D72EB0B70EA96C3F6FFFFFFEAF3FD0E7CEC0073ED2591FF70B7FF
        FEFFFFFFFFFF7FBDFE2C91FC298FFA3393F92A8DF82388F7B3D6FCFFFFFFFCFE
        FF65A9F6197FF1157AEF1377EE1075EC0E73EC89BBF6FFFFFFF1F8FF1081F400
        77F12E95FF82C0FFFFFFFFFFFFFF94C8FF2F94FD2D92FC2B90FB3092FA8AC1FC
        F4F9FFB6D8FCC8E1FDF4F9FE6BACF71D81F1177CF01479EF1277ED9FCAF8FFFF
        FFE8F4FF0D82F70078F53297FF86C2FFFAFDFFFFFFFFD8EBFF3196FD3095FD43
        9EFDB2D7FEFFFFFF8FC4FC268BF83592F8CBE3FDFFFFFF92C3FA1A7FF2187DF1
        177BF0D3E6FCFFFFFFC7E3FF047DFA0079F757AAFF75BAFFEEF7FFFFFFFFFFFF
        FF45A1FF3297FEA0CFFEFFFFFFB4D8FE2C90FB298FFA288DF93A95F9D1E6FDFF
        FFFF1E83F41B81F24297F6FFFFFFFFFFFF8CC5FF0076FD0079F7FF00FF58ABFF
        D9ECFFFFFFFFFFFFFFC1E0FF3398FF5DADFE9DCEFF439FFD2F93FC2D91FB2A90
        FB288DFA469CFAA7D0FC2287F62185F6C3DEFDFFFFFFFCFEFF389BFF007AFFFF
        00FFFF00FF5DADFFACD5FFFEFFFFFFFFFFFFFFFF8EC6FF3398FF3398FF3297FF
        3195FE3095FD2E92FC2C91FB298FFA288CF9288CF7CAE5FFFFFFFFFFFFFFACD5
        FF017FFF017FFFFF00FFFF00FFFF00FF6FB7FFE8F4FFFFFFFFFFFFFFFFFFFFA5
        D1FF3599FF3398FF3398FF3296FE3095FE2F93FD2C92FB2F92FBB9DAFEFFFFFF
        FFFFFFEDF6FF2A94FF027FFFFF00FFFF00FFFF00FFFF00FF77BAFFA3D0FFFAFD
        FFFFFFFFFFFFFFFFFFFFFAFDFF92C8FF56AAFF46A2FF46A1FE5BACFEA1CFFEFF
        FFFFFFFFFFFFFFFFFCFEFF64B1FF017FFF0A84FFFF00FFFF00FFFF00FFFF00FF
        FF00FF76B9FFAFD7FFF5FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFDCEDFF60AFFF0782FF0F86FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF79BBFF91C8FFDBEDFFFAFDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF6FFA1CFFF3599FF0E85FF1489FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF97CAFF75B9FF9E
        CEFFCAE5FFE3F1FFEDF6FFEDF6FFE6F3FFD4E9FFABD4FF6DB5FF3097FF1D8DFF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FF95C9FF75B9FF75B9FF7BBCFF7BBCFF6DB5FF58ABFF44A1FF2D
        95FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
    object chkASocial: TCheckBox
      Left = 15
      Top = 87
      Width = 108
      Height = 17
      Caption = 'Assistente Social'
      TabOrder = 14
      OnClick = chkASocialClick
    end
    object cbASocial: TComboBox
      Left = 118
      Top = 83
      Width = 215
      Height = 22
      Style = csOwnerDrawFixed
      Enabled = False
      ItemHeight = 16
      TabOrder = 15
    end
  end
  object PageControl1: TPageControl
    Left = 2
    Top = 175
    Width = 832
    Height = 396
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Dados'
      object Panel5: TPanel
        Left = 5
        Top = 4
        Width = 812
        Height = 359
        BevelOuter = bvLowered
        TabOrder = 0
        object dbgResultado: TDBGrid
          Left = 10
          Top = 11
          Width = 792
          Height = 340
          DataSource = dsResultados
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'cod_matricula'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'Matr�cula'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_nome'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Caption = 'Nome do adolescente aprendiz'
              Width = 250
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDatAvaliacao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'Avaliado em...'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_unidade'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Caption = 'Unidade/Projeto'
              Width = 185
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'num_secao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'N.� Se��o'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dsc_nom_secao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Caption = 'Empresa/Se��o'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_asocial'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDatInicio'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'In�cio Apr. Pr�tica'
              Width = 95
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem01'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '01'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem02'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '02'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem03'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '03'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem04'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '04'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem05'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '05'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem06'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '06'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem07'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '07'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem08'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '08'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem09'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '09'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem10'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '10'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem11'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '11'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vItem12'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = '12'
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'vDatInsercao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'Inserido em...'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nom_usuario'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Times New Roman'
              Font.Style = []
              Title.Caption = 'Digitado por...'
              Width = 90
              Visible = True
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Gr�fico'
      ImageIndex = 1
      object DBChart1: TDBChart
        Left = 160
        Top = 14
        Width = 469
        Height = 348
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TDBChart')
        TabOrder = 0
      end
    end
  end
  object dsResultados: TDataSource
    DataSet = dmDeca.cdsSel_AvaliacoesApr
    Left = 410
    Top = 232
  end
  object Excel: TExcelApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 763
    Top = 214
  end
end
