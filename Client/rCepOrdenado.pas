unit rCepOrdenado;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelCepOrdenado = class(TQuickRep)
    SummaryBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRExpr1: TQRExpr;
    DetailBand1: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRLabel1: TQRLabel;
    lbUnidade: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel2: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText6: TQRDBText;
  private

  public

  end;

var
  relCepOrdenado: TrelCepOrdenado;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

end.
