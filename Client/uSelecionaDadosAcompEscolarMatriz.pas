unit uSelecionaDadosAcompEscolarMatriz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, Buttons, TeeProcs, TeEngine, Chart, DBChart;

type
  TfrmSelecionaDadosAcompEscolarMatriz = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cbMes: TComboBox;
    Label2: TLabel;
    cbBimestre: TComboBox;
    edAno: TEdit;
    Label3: TLabel;
    btnVerResumoMensal: TSpeedButton;
    ProgressBar1: TProgressBar;
    Panel1: TPanel;
    SpeedButton2: TSpeedButton;
    DBChart1: TDBChart;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaDadosAcompEscolarMatriz: TfrmSelecionaDadosAcompEscolarMatriz;

implementation

{$R *.DFM}

end.
