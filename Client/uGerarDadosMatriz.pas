unit uGerarDadosMatriz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls, Db, Grids, DBGrids, ADODB, DBCtrls,
  jpeg, qrexport;

type
  TfrmGerarDadosMatriz = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cbUnidade: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cbBimestre: TComboBox;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    btnPesquisar: TSpeedButton;
    DataSource1: TDataSource;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    btnVisualizar: TSpeedButton;
    DBGrid7: TDBGrid;
    Image1: TImage;
    lbUnidade: TLabel;
    CheckBox1: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerarDadosMatriz: TfrmGerarDadosMatriz;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses uDM, uUtil, rResumoMatriz, uPrincipal;

{$R *.DFM}

procedure TfrmGerarDadosMatriz.FormShow(Sender: TObject);
begin

if (vvIND_PERFIL in [1,7,17,18,19,20,23,24,25]) then
begin

  lbUnidade.Visible := False;
  cbUnidade.Visible := True;
  cbUnidade.Enabled := True;

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();
  btnVisualizar.Enabled := False;

  //Carregar as unidades com exce��o de inativos e maioridade...
  try

    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 3) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 7) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 9) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 12) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 13) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 42) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 43) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 47) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 48) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 50) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 71) and
        (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 84) then
        begin
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;

    end;
  except
    Application.Messagebox ('Aten��o...!!!' +#13+#10 +
                            'Um erro ocorreu ao tentar carregar as unidades.',
                            '[Sistema Deca] - Erro carregando unidades',
                            MB_OK + MB_ICONERROR);
    frmGerarDadosMatriz.Close;
  end;
end
else if (vvIND_PERFIL in [2,3]) then
begin
  lbUnidade.Caption := vvNOM_UNIDADE;
  cbUnidade.Visible := False;
  cbUnidade.Enabled := False;
end;
end;

procedure TfrmGerarDadosMatriz.btnPesquisarClick(Sender: TObject);
begin

  try

    with dmDeca.cdsGeraDadosMatriz do
    begin

      Close;
      if vvIND_PERFIL = 1 then
        Params.ParamByName ('@pe_cod_unidade').AsInteger  := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex])
      else if vvIND_PERFIL in [2,3] then
        Params.ParamByName ('@pe_cod_unidade').AsInteger  := vvCOD_UNIDADE;

      Params.ParamByName ('@pe_num_ano').AsInteger      := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_num_bimestre').AsInteger := cbBimestre.ItemIndex + 1;
      Params.ParamByName ('@pe_num_mes').AsInteger      := cbMes.ItemIndex + 1;
      Open;

      btnVisualizar.Enabled := True;

    end;

  except
    Application.MessageBox ('Aten��o!!!' + #13#10 +
                            'Um erro ocorreu ao tentar realizar a consulta dos Dados da Matriz. Favor entrar em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro Dados Matriz...',
                            MB_OK + MB_ICONERROR);
  end;

end;

procedure TfrmGerarDadosMatriz.btnVisualizarClick(Sender: TObject);
var nomArquivo:String;
begin
  Application.CreateForm (TrelResumoMatriz, relResumoMatriz);
  if (vvIND_PERFIL = 1) then
    relResumoMatriz.qrlUnidade.Caption := cbUnidade.Text
  else
    relResumoMatriz.qrlUnidade.Caption := vvNOM_UNIDADE;
  relResumoMatriz.qrlPeriodo_Bimestre_Ano.Caption := 'Ref.: Per�odo: ' + cbMes.Text + '/' + mskAno.Text + '   -   Bimestre:  ' + cbBimestre.Text;
  relResumoMatriz.Preview;
  relResumoMatriz.Free;

  btnVisualizar.Enabled := False;



end;

end.
