unit uSELECIONA_DEMANDA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, ExtCtrls, Mask, Grids, DBGrids, Buttons, ComCtrls, Db;

type
  TfrmSELECIONA_DEMANDA = class(TForm)
    PageControl3: TPageControl;
    TabSheet6: TTabSheet;
    Bevel3: TBevel;
    dbgDEMANDAS: TDBGrid;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cmbREGIAO: TComboBox;
    cmbFAIXA_ETARIA: TComboBox;
    cmbUNIDADE: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    btnAPLICAR_FILTRO: TSpeedButton;
    btnLIMPAR_FILTROS: TSpeedButton;
    Label6: TLabel;
    cmbPERIODO_ESCOLA: TComboBox;
    SpeedButton1: TSpeedButton;
    Label5: TLabel;
    cmbSITUACAO_CADASTRO: TComboBox;
    Label7: TLabel;
    cmbSELECIONADOS_DEMANDA: TComboBox;
    dsGERA_DEMANDA: TDataSource;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    edtNOME: TEdit;
    GroupBox6: TGroupBox;
    rdgSITUACAO: TRadioGroup;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    edtNUMERO_INSCRICAO: TEdit;
    edtNUMERO_DIGITO: TEdit;
    GroupBox9: TGroupBox;
    edtPONTUACAO: TEdit;
    GroupBox10: TGroupBox;
    medFONE_CONTATO: TMemo;
    Panel1: TPanel;
    btnALTERAR_DADOSDEMANDA: TSpeedButton;
    SpeedButton2: TSpeedButton;
    btnREGISTRAR_LIGACAO: TSpeedButton;
    GroupBox5: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    edtDATA_TENTATIVA1: TEdit;
    edtQUEM_TENTATIVA1: TEdit;
    edtQUEM_TENTATIVA2: TEdit;
    edtDATA_TENTATIVA2: TEdit;
    edtQUEM_TENTATIVA3: TEdit;
    edtDATA_TENTATIVA3: TEdit;
    medOBSERVACOES: TMemo;
    GroupBox11: TGroupBox;
    btnPESQUISA_NOMEDEMANDA: TSpeedButton;
    btnPESQUISA_TODADEMANDA: TSpeedButton;
    edtNOME_DEMANDA: TEdit;
    Label8: TLabel;
    cmbSITUACAO_DEMANDA: TComboBox;
    btnLIMPA_REGIAO: TSpeedButton;
    btnLIMPA_FAIXAETARIA: TSpeedButton;
    btnLIMPA_UNIDADE: TSpeedButton;
    btnLIMPA_SITUACAODEMANDA: TSpeedButton;
    btnLIMPA_PERIODOESCOLAR: TSpeedButton;
    btnLIMPA_SITUACAOCADASTRO: TSpeedButton;
    btnLIMPA_SELECIONADOS: TSpeedButton;
    btnCARREGA_DATAHORA1: TSpeedButton;
    btnCARREGA_DATAHORA2: TSpeedButton;
    btnCARREGA_DATAHORA3: TSpeedButton;
    procedure btnGERA_DEMANDAClick(Sender: TObject);
    procedure btnAPLICAR_FILTROClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnLIMPAR_FILTROSClick(Sender: TObject);
    procedure btnALTERAR_DADOSDEMANDAClick(Sender: TObject);
    procedure btnVER_DADOSFILTROClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure dbgDEMANDASTitleClick(Column: TColumn);
    procedure btnPESQUISA_NOMEDEMANDAClick(Sender: TObject);
    procedure edtNOME_DEMANDAEnter(Sender: TObject);
    procedure btnPESQUISA_TODADEMANDAClick(Sender: TObject);
    procedure btnDADOS_COMISSAOTRIAGEMClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnREGISTRAR_LIGACAOClick(Sender: TObject);
    procedure btnLIMPA_REGIAOClick(Sender: TObject);
    procedure btnLIMPA_FAIXAETARIAClick(Sender: TObject);
    procedure btnLIMPA_UNIDADEClick(Sender: TObject);
    procedure btnLIMPA_SITUACAODEMANDAClick(Sender: TObject);
    procedure btnLIMPA_PERIODOESCOLARClick(Sender: TObject);
    procedure btnLIMPA_SITUACAOCADASTROClick(Sender: TObject);
    procedure btnLIMPA_SELECIONADOSClick(Sender: TObject);
    procedure InsereRegistroAtendimento;
    procedure edtNOME_DEMANDAKeyPress(Sender: TObject; var Key: Char);
    procedure btnCARREGA_DATAHORA1Click(Sender: TObject);
    procedure btnCARREGA_DATAHORA2Click(Sender: TObject);
    procedure btnCARREGA_DATAHORA3Click(Sender: TObject);
    procedure edtDATA_TENTATIVA1Change(Sender: TObject);
    procedure edtDATA_TENTATIVA2Change(Sender: TObject);
    procedure edtDATA_TENTATIVA3Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSELECIONA_DEMANDA: TfrmSELECIONA_DEMANDA;
  vListaUnidadeDem1, vListaUnidadeDem2,
  vListaRegiaoDem1, vListaRegiaoDem2 : TStringList;
  vIndex : Integer;

implementation

uses udmTriagemDeca, uDM, uPrincipal, uUtil, rDemanda;

{$R *.DFM}

procedure TfrmSELECIONA_DEMANDA.btnGERA_DEMANDAClick(Sender: TObject);
begin

  //Confirma ao usu�rio se ele realmente deseja gerar uma nova demanda...
  if Application.MessageBox ('Deseja realmente gerar uma nova rela��o de demanda?' +#13+#10+
                             'Os dados atuais da demanda ser�o exclu�dos e ser�o gerados novamente. Tem certeza?',
                             '[Sistema Deca] - Exclus�o de Demanda atual',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    //Caso confirmado, excluir os dados da tabela DEMANDA
    try
      with dmTriagemDeca.cdsDEL_DEMANDA do
      begin
        Close;
        Execute;

        //Verifica se todos os registros foram exclu�dos...
        try
          with dmTriagemDeca.cdsGERA_DEMANDA do
          begin
            Close;
            Params.ParamByName ('@pe_cod_regiao').Value        := Null;
            Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
            Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
            Params.ParamByName ('@pe_cod_unidade').Value       := Null;
            Params.ParamByName ('@pe_flg_situacao').Value      := Null;
            Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
            Params.ParamByName ('@pe_cod_situacao').Value      := Null;
            Params.ParamByName ('@pe_nom_nome').Value          := Null;
            Open;

            if (dmTriagemDeca.cdsGERA_DEMANDA.RecordCount > 0) then
            begin
              //Gerar os dados novamente e inser�-los na tabela DEMANDA
              while not (dmTriagemDeca.cdsGERA_DEMANDA.eof) do
              begin
                with dmTriagemDeca.cdsINS_DEMANDA do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
                  Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao1').Value;
                  Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
                  Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao2').Value;
                  Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
                  Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao3').Value;
                  Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
                  Params.ParamByName ('@pe_dsc_observacoes').Value   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_observacoes').Value;
                  Params.ParamByName ('@pe_flg_situacao').Value      := 0; //Deixar o padr�o como N�O CONVOCADO
                  Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
                  Execute;
                  dmTriagemDeca.cdsGERA_DEMANDA.Next;
                end;
              end;

              //Executar o select em DEMANDA para atualizar os dados do GRID em tela
              try
                with dmTriagemDeca.cdsGERA_DEMANDA do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_regiao').Value        := Null;
                  Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
                  Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
                  Params.ParamByName ('@pe_cod_unidade').Value       := Null;
                  Params.ParamByName ('@pe_flg_situacao').Value      := Null;
                  Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
                  Params.ParamByName ('@pe_cod_situacao').Value      := Null;
                  Params.ParamByName ('@pe_nom_nome').Value          := Null;
                  Open;
                  dbgDEMANDAS.Refresh;
                end;
              except end;
            end;
          end;
        except
          Application.MessageBox ('Os dados da demanda n�o foram exclu�dos com sucesso.' +#13+#10+
                                  'Tente novamente!!!',
                                  '[Sistema Deca] - Excluir dados de DEMANDA',
                                  MB_OK + MB_ICONERROR);
        end;


      end;
    except
      Application.MessageBox ('Um erro ocorreu e os dados n�o foram exclu�dos.' +#13+#10+
                              'Tente novamente!!!',
                              '[Sistema Deca] - Erro excluindo dados de DEMANDA',
                              MB_OK + MB_ICONERROR);

    end;

  end
  else
  begin
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := Null;
      Params.ParamByName ('@pe_nom_nome').Value          := Null;
      Open;
      dbgDEMANDAS.Refresh;
    end;

  end;

end;

procedure TfrmSELECIONA_DEMANDA.btnAPLICAR_FILTROClick(Sender: TObject);
begin
  //Verificar qual(is) filtros est�o selecionados...
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;

      //REGI�O
      if (cmbREGIAO.ItemIndex = -1) then Params.ParamByName ('@pe_cod_regiao').Value := Null else Params.ParamByName ('@pe_cod_regiao').AsInteger := StrToInt(vListaRegiaoDem1.Strings[cmbREGIAO.ItemIndex]);

      //FAIXA ET�RIA
      if (cmbFAIXA_ETARIA.ItemIndex = -1) then
      begin
        Params.ParamByName ('@pe_faixaetaria1').Value := Null;
        Params.ParamByName ('@pe_faixaetaria2').Value := Null;
      end
      else
      begin
        if (cmbFAIXA_ETARIA.ItemIndex = -1) then
        begin
          Params.ParamByName ('@pe_faixaetaria1').Value := Null;
          Params.ParamByName ('@pe_faixaetaria2').Value := Null;
        end
        else
        begin
          case (cmbFAIXA_ETARIA.ItemIndex) of
          0: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 72;
               Params.ParamByName ('@pe_faixaetaria2').Value := 83;
             end;

          1: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 84;
               Params.ParamByName ('@pe_faixaetaria2').Value := 95;
             end;

          2: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 96;
               Params.ParamByName ('@pe_faixaetaria2').Value := 107;
             end;

          3: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 108;
               Params.ParamByName ('@pe_faixaetaria2').Value := 119;
             end;

          4: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 120;
               Params.ParamByName ('@pe_faixaetaria2').Value := 131;
             end;

          5: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 132;
               Params.ParamByName ('@pe_faixaetaria2').Value := 143;
             end;

          6: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 144;
               Params.ParamByName ('@pe_faixaetaria2').Value := 155;
             end;

          7: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 156;
               Params.ParamByName ('@pe_faixaetaria2').Value := 167;
             end;

          8: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 168;
               Params.ParamByName ('@pe_faixaetaria2').Value := 179;
             end;

          9: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 180;
               Params.ParamByName ('@pe_faixaetaria2').Value := 191;
             end;

          10: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 192;
               Params.ParamByName ('@pe_faixaetaria2').Value := 203;
             end;

          11: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 204;
               Params.ParamByName ('@pe_faixaetaria2').Value := 215;
             end;

          12: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 216;
               Params.ParamByName ('@pe_faixaetaria2').Value := 1000;
             end;
          end; //case
        end;
      end;

      //UNIDADE
      if (cmbUNIDADE.ItemIndex = -1) then Params.ParamByName ('@pe_cod_unidade').Value := Null else Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidadeDem1.Strings[cmbUNIDADE.ItemIndex]);

      //SITUA��O DE DEMANDA
      if (cmbSITUACAO_DEMANDA.ItemIndex = -1) then
        Params.ParamByName ('@pe_flg_situacao').Value := Null
      else
      begin
        case (cmbSITUACAO_DEMANDA.ItemIndex) of
          0: Params.ParamByName ('@pe_flg_situacao').Value := 0;
          1: Params.ParamByName ('@pe_flg_situacao').Value := 1;
          2: Params.ParamByName ('@pe_flg_situacao').Value := 2;
          3: Params.ParamByName ('@pe_flg_situacao').Value := 3;
        end;
      end;

      //PER�ODO ESCOLAR
      if (cmbPERIODO_ESCOLA.ItemIndex = -1) then Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null else Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Trim(cmbPERIODO_ESCOLA.Text);

      //SITUA��O CADASTRO
      if (cmbSITUACAO_CADASTRO.ItemIndex = -1) then
        Params.ParamByName ('@pe_cod_situacao').Value     := Null
      else
      begin
        case (cmbSITUACAO_CADASTRO.ItemIndex) of
          0: Params.ParamByName ('@pe_cod_situacao').Value := 12; //INSCRITO = 12
          1: Params.ParamByName ('@pe_cod_situacao').Value := 15; //NECESSIDADE ESPECIAL = 15
          2: Params.ParamByName ('@pe_cod_situacao').Value := 16; //PEND�NCIA DE INSCRI��O = 16
          3: Params.ParamByName ('@pe_cod_situacao').Value := 19; //ESCOLA INTEGRAL = 19
          4: Params.ParamByName ('@pe_cod_situacao').Value := 22; //COMISSAO TRIAGEM = 22
        end;
      end;

      //NOME
      Params.ParamByName ('@pe_nom_nome').Value           := Null;

      //SELECIONADO
      Params.ParamByName ('@pe_flg_selecionado').Value    := 1; //SELECIONADOS

      Open;
      Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de acordo com os crit�rios'),
                                    '[Sistema Deca] - Totlizador de consulta',
                                    MB_OK + MB_ICONINFORMATION);
      dbgDEMANDAS.Refresh;
    end
  except end;
end;

procedure TfrmSELECIONA_DEMANDA.FormShow(Sender: TObject);
begin
  //Carregar as regi�es
  vListaRegiaoDem1 := TStringList.Create();
  vListaRegiaoDem2 := TStringList.Create();

  with dmTriagemDeca.cdsSel_Par_Cadastro_Regioes do
  begin
    Close;
    Params.ParamByName ('@cod_regiao').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.eof) do
      begin
        cmbREGIAO.Items.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        vListaRegiaoDem1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('cod_regiao').Value));
        vListaRegiaoDem2.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.Next;
      end;
    end;
  end;

  //****************************************************************************

  //Carregar a lista de Unidades
  vListaUnidadeDem1 := TStringList.Create();
  vListaUnidadeDem2 := TStringList.Create();

  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := Null;
    Params.ParamByName ('@pe_nom_unidade').Value := Null;
    Open;

    if (dmDeca.cdsSel_Unidade.RecordCount > 0) then
    begin
      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        //Carrega apenas as unidades "ATIVAS"
        if (dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0) then
        begin
          cmbUNIDADE.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidadeDem1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidadeDem2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  end;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPAR_FILTROSClick(Sender: TObject);
begin
  //btnGERA_DEMANDA.OnClick(Self);
  cmbREGIAO.ItemIndex               := -1;
  cmbFAIXA_ETARIA.ItemIndex         := -1;
  cmbUNIDADE.ItemIndex              := -1;
  cmbSITUACAO_DEMANDA.ItemIndex     := -1;
  cmbPERIODO_ESCOLA.ItemIndex       := -1;
  cmbSITUACAO_CADASTRO.ItemIndex    := -1;
  cmbSELECIONADOS_DEMANDA.ItemIndex := -1;
  //btnAPLICAR_FILTRO.OnClick(Self);
end;

procedure TfrmSELECIONA_DEMANDA.btnALTERAR_DADOSDEMANDAClick(
  Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsUPD_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;

      if (Length(edtDATA_TENTATIVA1.Text) = 0) then Params.ParamByName ('@pe_dat_ligacao1').Value := Null else Params.ParamByName ('@pe_dat_ligacao1').AsDateTime := StrToDateTime(edtDATA_TENTATIVA1.Text);
      if (Length(edtDATA_TENTATIVA2.Text) = 0) then Params.ParamByName ('@pe_dat_ligacao2').Value := Null else Params.ParamByName ('@pe_dat_ligacao2').AsDateTime := StrToDateTime(edtDATA_TENTATIVA2.Text);
      if (Length(edtDATA_TENTATIVA3.Text) = 0) then Params.ParamByName ('@pe_dat_ligacao3').Value := Null else Params.ParamByName ('@pe_dat_ligacao3').AsDateTime := StrToDateTime(edtDATA_TENTATIVA3.Text);

      if (Length(edtQUEM_TENTATIVA1.Text) = 0) then Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := Null else Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := Trim(edtQUEM_TENTATIVA1.Text);
      if (Length(edtQUEM_TENTATIVA2.Text) = 0) then Params.ParamByName ('@pe_dsc_quem_ligacao2').Value := Null else Params.ParamByName ('@pe_dsc_quem_ligacao2').Value := Trim(edtQUEM_TENTATIVA2.Text);
      if (Length(edtQUEM_TENTATIVA3.Text) = 0) then Params.ParamByName ('@pe_dsc_quem_ligacao3').Value := Null else Params.ParamByName ('@pe_dsc_quem_ligacao3').Value := Trim(edtQUEM_TENTATIVA3.Text);

      if (Length(medOBSERVACOES.Text) = 0) then Params.ParamByName ('@pe_dsc_observacoes').Value := Null else Params.ParamByName ('@pe_dsc_observacoes').Value := Trim(medOBSERVACOES.Text);

      Params.ParamByName ('@pe_flg_situacao').Value      := rdgSITUACAO.ItemIndex;
      Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
      Params.ParamByName ('@pe_flg_selecionado').Value   := 1;

      Execute;

      InsereRegistroAtendimento;

      //btnAPLICAR_FILTRO.Click;
      Panel2.Visible := False;
    end;
  except end;
end;

procedure TfrmSELECIONA_DEMANDA.btnVER_DADOSFILTROClick(Sender: TObject);
begin
  Application.CreateForm(TrelDemanda, relDemanda);
  relDemanda.Prepare;
  relDemanda.Preview;
  relDemanda.Free;
end;

procedure TfrmSELECIONA_DEMANDA.SpeedButton2Click(Sender: TObject);
begin
  Panel2.Visible := False;
end;

procedure TfrmSELECIONA_DEMANDA.dbgDEMANDASTitleClick(Column: TColumn);
begin
  dmTriagemDeca.cdsGERA_DEMANDA.IndexFieldNames := Column.FieldName;
end;

procedure TfrmSELECIONA_DEMANDA.btnPESQUISA_NOMEDEMANDAClick(
  Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := Null;
      Params.ParamByName ('@pe_nom_nome').Value          := '%' + Trim(edtNOME_DEMANDA.Text) + '%';
      Params.ParamByName ('@pe_flg_selecionado').Value   := 1;
      Open;
      dbgDEMANDAS.Refresh;
    end;
  except end;
end;

procedure TfrmSELECIONA_DEMANDA.edtNOME_DEMANDAEnter(Sender: TObject);
begin
  edtNOME_DEMANDA.Clear;
end;

procedure TfrmSELECIONA_DEMANDA.btnPESQUISA_TODADEMANDAClick(Sender: TObject);
begin
  edtNOME_DEMANDA.Clear;
  btnAPLICAR_FILTRO.Click;
end;

procedure TfrmSELECIONA_DEMANDA.btnDADOS_COMISSAOTRIAGEMClick(
  Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := 22;
      Params.ParamByName ('@pe_nom_nome').Value          := Null;
      Params.ParamByName ('@pe_flg_selecionado').Value   := 1;
      Open;

      Application.CreateForm(TrelDemanda, relDemanda);
      relDemanda.Prepare;
      relDemanda.Preview;
      relDemanda.Free;
    end;
  except end;
end;

procedure TfrmSELECIONA_DEMANDA.SpeedButton1Click(Sender: TObject);
begin
  //Executar o select em DEMANDA para atualizar os dados do GRID em tela
  try
  with dmTriagemDeca.cdsGERA_DEMANDA do
  begin
    Close;
    Params.ParamByName ('@pe_cod_regiao').Value        := Null;
    Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
    Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
    Params.ParamByName ('@pe_cod_unidade').Value       := Null;
    Params.ParamByName ('@pe_flg_situacao').Value      := Null;
    Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
    Params.ParamByName ('@pe_cod_situacao').Value      := Null;
    Params.ParamByName ('@pe_nom_nome').Value          := Null;
    Params.ParamByName ('@pe_flg_selecionado').Value   := 1; //Apenas os Selecionados

    Open;
    dbgDEMANDAS.Refresh;
  end;
except end;
end;

procedure TfrmSELECIONA_DEMANDA.btnREGISTRAR_LIGACAOClick(Sender: TObject);
begin
  Panel2.Visible := True;
  edtNUMERO_INSCRICAO.Text := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NUM_INSCRICAO').Value;
  edtNUMERO_DIGITO.Text    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NUM_INSCRICAO_DIGT').Value;
  edtNOME.Text             := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NOM_NOME').Value;
  edtPONTUACAO.Text        := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('PONTUACAO').Value;


  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DAT_LIGACAO1').IsNull) then edtDATA_TENTATIVA1.Text := '' else edtDATA_TENTATIVA1.Text  := DateTimeToStr(dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DAT_LIGACAO1').Value);
  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DAT_LIGACAO2').IsNull) then edtDATA_TENTATIVA2.Text := '' else edtDATA_TENTATIVA2.Text  := DateTimeToStr(dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DAT_LIGACAO2').Value);
  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DAT_LIGACAO3').IsNull) then edtDATA_TENTATIVA3.Text := '' else edtDATA_TENTATIVA3.Text  := DateTimeToStr(dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DAT_LIGACAO3').Value);

  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_QUEMLIGACAO1').IsNull) then edtQUEM_TENTATIVA1.Text := '' else edtQUEM_TENTATIVA1.Text  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_QUEMLIGACAO1').Value;
  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_QUEMLIGACAO2').IsNull) then edtQUEM_TENTATIVA2.Text := '' else edtQUEM_TENTATIVA2.Text  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_QUEMLIGACAO2').Value;
  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_QUEMLIGACAO3').IsNull) then edtQUEM_TENTATIVA3.Text := '' else edtQUEM_TENTATIVA3.Text  := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_QUEMLIGACAO3').Value;

  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_OBSERVACOES').IsNull) then medOBSERVACOES.Text := '' else medOBSERVACOES.Text := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('DSC_OBSERVACOES').Value;

  rdgSITUACAO.ItemIndex    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('FLG_SITUACAO').Value;

  if ( (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NUM_FONE_CONTATO').IsNull) or (Length(dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('NUM_FONE_CONTATO').Value)=0))then
    medFONE_CONTATO.Text   := 'N�o h� informa��es de telefone e nomes de contatos cadastrados na ficha de inscri��o.'
  else
    medFONE_CONTATO.Text   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NUM_FONE_CONTATO').Value;

end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_REGIAOClick(Sender: TObject);
begin
  cmbREGIAO.iTEMiNDEX := -1;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_FAIXAETARIAClick(Sender: TObject);
begin
  cmbFAIXA_ETARIA.ItemIndex := -1;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_UNIDADEClick(Sender: TObject);
begin
  cmbUNIDADE.ItemIndex := -1;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_SITUACAODEMANDAClick(
  Sender: TObject);
begin
  cmbSITUACAO_DEMANDA.ItemIndex := -1;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_PERIODOESCOLARClick(
  Sender: TObject);
begin
  cmbPERIODO_ESCOLA.ItemIndex := -1;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_SITUACAOCADASTROClick(
  Sender: TObject);
begin
  cmbSITUACAO_CADASTRO.ItemIndex := -1;
end;

procedure TfrmSELECIONA_DEMANDA.btnLIMPA_SELECIONADOSClick(
  Sender: TObject);
begin
  cmbSELECIONADOS_DEMANDA.ItemIndex := -1;
end;

procedure TfrmSELECIONA_DEMANDA.InsereRegistroAtendimento;
begin
  try
    with dmTriagemDeca.cdsIns_Cadastro_Relatorio do
    begin
      Close;
      Params.ParamByName ('@Cod_inscricao').Value       := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
      Params.ParamByName ('@dta_digitacao').Value       := Date();
      Params.ParamByName ('@tipo_relatorio').Value      := 4;  //Vai ser inserido como Registro de Atendimento
      Params.ParamByName ('@cod_responsavel').Value     := Null;  //Trazer os dados de usu�rio pelo Sistema Deca
      Params.ParamByName ('@txt_relatorio').Value       := Trim(medOBSERVACOES.Text) + #13 + edtNOME.Text + ' nessa data teve seu status em Demanda alterado para ' + rdgSITUACAO.Items[rdgSITUACAO.ItemIndex];
      Params.ParamByName ('@dsc_entrevistador').Value   := vvNOM_USUARIO;
      Params.ParamByName ('@pe_cod_usuario_deca').Value := vvCOD_USUARIO; //Estabelece a rela��o do usu�rio com o banco de dados Usuario -> Deca
      Execute;
    end;
  except end;
end;

procedure TfrmSELECIONA_DEMANDA.edtNOME_DEMANDAKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then btnPESQUISA_NOMEDEMANDA.Click;
end;

procedure TfrmSELECIONA_DEMANDA.btnCARREGA_DATAHORA1Click(Sender: TObject);
begin
  edtDATA_TENTATIVA1.Text :=  DateTimeToStr(Now());
end;

procedure TfrmSELECIONA_DEMANDA.btnCARREGA_DATAHORA2Click(Sender: TObject);
begin
  edtDATA_TENTATIVA2.Text :=  DateTimeToStr(Now());
end;

procedure TfrmSELECIONA_DEMANDA.btnCARREGA_DATAHORA3Click(Sender: TObject);
begin
  edtDATA_TENTATIVA3.Text :=  DateTimeToStr(Now());
end;

procedure TfrmSELECIONA_DEMANDA.edtDATA_TENTATIVA1Change(Sender: TObject);
begin
  if (Length(edtDATA_TENTATIVA1.Text)=0) then
  begin
    btnCARREGA_DATAHORA1.Enabled := True;
    edtQUEM_TENTATIVA1.Enabled   := True;
  end
  else
  begin
    btnCARREGA_DATAHORA1.Enabled := False;
    edtQUEM_TENTATIVA1.Enabled        := False;
  end;
end;

procedure TfrmSELECIONA_DEMANDA.edtDATA_TENTATIVA2Change(Sender: TObject);
begin
  if (Length(edtDATA_TENTATIVA2.Text)=0) then
  begin
    btnCARREGA_DATAHORA2.Enabled := True;
    edtQUEM_TENTATIVA2.Enabled   := True
  end
  else
  begin
    btnCARREGA_DATAHORA2.Enabled := False;
    edtQUEM_TENTATIVA2.Enabled        := False;
  end;
end;

procedure TfrmSELECIONA_DEMANDA.edtDATA_TENTATIVA3Change(Sender: TObject);
begin
  if (Length(edtDATA_TENTATIVA1.Text)=0) then
  begin
    btnCARREGA_DATAHORA3.Enabled := True;
    edtQUEM_TENTATIVA3.Enabled   := True
  end
  else
  begin
    btnCARREGA_DATAHORA3.Enabled := False;
    edtQUEM_TENTATIVA3.Enabled        := False;
  end;
end;

end.
