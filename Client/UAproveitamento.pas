unit UAproveitamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, Buttons, StdCtrls, ExtCtrls, Db, ADODB, ComCtrls, DBClient;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edtBusca: TEdit;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    lblNome: TLabel;
    PC1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    TabSheet4: TTabSheet;
    TabSheet6: TTabSheet;
    Panel4: TPanel;
    stgQualidade: TStringGrid;
    Panel5: TPanel;
    Panel6: TPanel;
    stgEmpreendedorismo: TStringGrid;
    stgInteracaoRelacional: TStringGrid;
    stgParticipacao: TStringGrid;
    stgAprendizagem: TStringGrid;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    Label4: TLabel;
    lblUnidade: TLabel;
    edtAnoRef: TEdit;
    Label3: TLabel;
    TabSheet5: TTabSheet;
    btnSalva: TButton;
    Memo1: TMemo;
    procedure TabSheet1Show(Sender: TObject);
    procedure PC1DrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox1Exit(Sender: TObject);
    procedure stgInteracaoRelacionalSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure stgParticipacaoSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure ComboBox2Change(Sender: TObject);
    procedure ComboBox2Exit(Sender: TObject);
    procedure stgParticipacaoClick(Sender: TObject);
    procedure stgAprendizagemClick(Sender: TObject);
    procedure stgEmpreendedorismoClick(Sender: TObject);
    procedure stgQualidadeClick(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure ComboBox5Change(Sender: TObject);
    procedure ComboBox3Exit(Sender: TObject);
    procedure ComboBox4Exit(Sender: TObject);
    procedure ComboBox5Exit(Sender: TObject);
    procedure stgAprendizagemSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure stgEmpreendedorismoSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure stgQualidadeSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure stgInteracaoRelacionalDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure stgParticipacaoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure stgAprendizagemDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure stgEmpreendedorismoDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure stgQualidadeDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btnSalvaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}
uses Udm;

procedure TForm1.TabSheet1Show(Sender: TObject);
begin
with stgInteracaoRelacional do
   begin
        cells[0,0]:='INTERA��O RELACIONAL'  ;
        cells[1,0]:='      1�Bim';
        cells[2,0]:='      2�Bim';
        cells[3,0]:='      3�Bim';
        cells[4,0]:='      4�Bim';
        cells[0,1]:='Respeita as diferen�as no conv�vio com seus pares';
        cells[0,2]:='Estabelece v�nculos com outras crian�as/adolescentes';
        cells[0,3]:='Estabelece v�nculos com os profissionais da Unidade';
        cells[0,4]:='Desenvolve atitudes de coopera��o com os integrantes de seu grupo';
        cells[0,5]:='Identifica e respeita as regras b�sicas de conv�vio social';
        cells[0,6]:='Valoriza e respeita as produ��es dos colegas';
        cells[0,7]:='Apresenta capacidade de negocia��o e di�logo';
        cells[0,8]:='Respeita e valoriza o patrim�nio coletivo';
        cells[0,9]:='Conceito Final';
   end;
with stgParticipacao do
   begin
        cells[0,0]:='PARTICIPA��O';
        cells[1,0]:='      1�Bim';
        cells[2,0]:='      2�Bim';
        cells[3,0]:='      3�Bim';
        cells[4,0]:='      4�Bim';
        cells[0,1]:='Expressa-se corporalmente nas atividades';
        cells[0,2]:='Expressa oralmente suas opini�es';
        cells[0,3]:='Utiliza-se de express�es gr�ficas (escrita e desenho) nas atividades';
        cells[0,4]:='Exp�e suas opini�es com rela��o � proposta da atividade';
        cells[0,5]:='Exp�e suas opini�es com rela��o �s opini�es dos outros';
        cells[0,6]:='Busca informa��es e/ou conhecimentos';
        cells[0,7]:='Participa das atividades contribuindo para o seu desenvolvimento';
        cells[0,8]:='Faz observa��es pertinentes �s atividades';
        cells[0,9]:='Conceito Final';
   end;
with stgAprendizagem do
   begin
        cells[0,0]:='APRENDIZAGEM';
        cells[1,0]:='      1�Bim';
        cells[2,0]:='      2�Bim';
        cells[3,0]:='      3�Bim';
        cells[4,0]:='      4�Bim';
        cells[0,1]:='Compreende as atividades propostas';
        cells[0,2]:='Compara e interpreta dados e informa��es';
        cells[0,3]:='Transfere as aprendizagens para diferentes contextos';
        cells[0,4]:='Apresenta dom�nio das capacidades psicomotoras';
        cells[0,5]:='Testa hip�teses envolvendo criatividade, criticidade e racioc�nio';
        cells[0,6]:='Apresenta habilidade na busca de conhecimentos para resolver situa��es-problema';
        cells[0,7]:='Conceito Final';
   end;
with stgEmpreendedorismo do
   begin
        cells[0,0]:='EMPREENDEDORISMO';
        cells[1,0]:='      1�Bim';
        cells[2,0]:='      2�Bim';
        cells[3,0]:='      3�Bim';
        cells[4,0]:='      4�Bim';
        cells[0,1]:='Tem autonomia no desenvolvimento das atividades';
        cells[0,2]:='Apresenta iniciativa para tomar decis�es';
        cells[0,3]:='Exerce lideran�a positiva sobre o grupo';
        cells[0,4]:='Apresenta potencial criativo';
        cells[0,5]:='Aplica sua criatividade em situa��es construtivas';
        cells[0,6]:='Tem consci�ncia de seus talentos, valores e limites';
        cells[0,7]:='Preocupa-se com a qualidade de suas produ��es nas atividades';
        cells[0,8]:='Conceito Final';
   end;
with stgQualidade do
   begin
        cells[0,0]:='QUALIDADE DE VIDA';
        cells[1,0]:='      1�Bim';
        cells[2,0]:='      2�Bim';
        cells[3,0]:='      3�Bim';
        cells[4,0]:='      4�Bim';
        cells[0,1]:='Possui uma imagem positiva de si';
        cells[0,2]:='Valoriza o pr�prio corpo utilizando h�bitos de autocuidado e higiene pessoal';
        cells[0,3]:='Preocupa-se com sua apar�ncia';
        cells[0,4]:='Tem cuidados com o meio ambiente';
        cells[0,5]:='Demonstra interesse e participa de atividades culturais';
        cells[0,6]:='Demonstra interesse e participa de atividades esportivas';
        cells[0,7]:='Demonstra interesse e participa de atividades recreativas';
        cells[0,8]:='Conceito Final';
   end;

end;

procedure TForm1.PC1DrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
begin

with PC1.Canvas  do
 begin
   //   Brush.Color:= $008B89EF;
      FillRect(Rect);
//      Font.Color:= $003539EC;
      Font.Color:= clred;
 //     Font.Size:= 12;
 //     Font.Name:= 'Arial';
      Refresh;
 end;
    if TabIndex=0 then
        PC1.Canvas.TextRect(Rect,10,5,'INTERA��O RELACIONAL')
      else if TabIndex=1 then
        PC1.Canvas.TextRect(Rect,225,5,'PARTICIPA��O ')
      else if TabIndex=2 then
        PC1.Canvas.TextRect(Rect,355,5,'APRENDIZAGEM')
      else if TabIndex=3 then
        PC1.Canvas.TextRect(Rect,500,5,'EMPREENDEDORISMO ')
      else if TabIndex=4 then
        PC1.Canvas.TextRect(Rect,690,5,'QUALIDADE DE VIDA')
      else if TabIndex=5 then
        PC1.Canvas.TextRect(Rect,790,5,'Observa��es');
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  myYear, myMonth, myDay : Word;
begin
//stgInteracaoRelacional.DefaultRowHeight:=ComboBox1.Height;

DecodeDate(date, myYear, myMonth, myDay);

edtAnoRef.Text:=inttostr( myyear);


end;

procedure TForm1.stgInteracaoRelacionalSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if ((aCol <> 0) AND(aRow <> 9)) then
    begin
    R := stgInteracaoRelacional.CellRect(aCol, aRow);
    R.Left := R.Left + stgInteracaoRelacional.Left;
    R.Right := R.Right + stgInteracaoRelacional.Left;
    R.Top := R.Top + stgInteracaoRelacional.Top;
    R.Bottom := R.Bottom + stgInteracaoRelacional.Top;
    ComboBox1.Left := R.Left+2;
    ComboBox1.Top := R.Top+3;
    ComboBox1.Width := (R.Right +1) - R.Left;
    ComboBox1.Height := (R.Bottom +1) - R.Top;
    ComboBox1.Visible := True;
    ComboBox1.SetFocus;
    end;
CanSelect := True;

end;

procedure TForm1.stgParticipacaoSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if ((aCol <> 0) AND(aRow <> 9)) then
    begin
    R := stgParticipacao.CellRect(aCol, aRow);
    R.Left := R.Left + stgParticipacao.Left;
    R.Right := R.Right + stgParticipacao.Left;
    R.Top := R.Top + stgParticipacao.Top;
    R.Bottom := R.Bottom + stgParticipacao.Top;
    ComboBox2.Left := R.Left +2;
    ComboBox2.Top := R.Top +3;
    ComboBox2.Width := (R.Right + 1) - R.Left;
    ComboBox2.Height := (R.Bottom + 1) - R.Top;
    ComboBox2.Visible := True;
    ComboBox2.SetFocus;
    end;
CanSelect := True;

end;



procedure TForm1.stgParticipacaoClick(Sender: TObject);
begin
with sender as tstringgrid do
if Col =1 then
     Options:= Options +[goEditing]
   else
     Options:= Options -[goEditing];
end;

procedure TForm1.stgAprendizagemClick(Sender: TObject);
begin
with sender as tstringgrid do
if Col =1 then
     Options:= Options +[goEditing]
   else
     Options:= Options -[goEditing];
end;

procedure TForm1.stgEmpreendedorismoClick(Sender: TObject);
begin
with sender as tstringgrid do
if Col =1 then
     Options:= Options +[goEditing]
   else
     Options:= Options -[goEditing];
end;

procedure TForm1.stgQualidadeClick(Sender: TObject);
begin
with sender as tstringgrid do
if Col =1 then
     Options:= Options +[goEditing]
   else
     Options:= Options -[goEditing];
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
with stgInteracaoRelacional do
   begin
        Cells[Col,Row]:= ComboBox1.items[ComboBox1.ItemIndex];
        Combobox1.Visible := False;
        stgInteracaoRelacional.SetFocus;
    end;
end;

procedure TForm1.ComboBox2Change(Sender: TObject);
begin
stgParticipacao.Cells[stgParticipacao.Col,stgParticipacao.Row]:= ComboBox2.Items[ComboBox2.ItemIndex];
ComboBox2.Visible := False;
stgParticipacao.SetFocus;
end;


procedure TForm1.ComboBox3Change(Sender: TObject);
begin
stgAprendizagem.Cells[stgAprendizagem.Col,stgAprendizagem.Row]:= ComboBox3.Items[ComboBox3.ItemIndex];
ComboBox3.Visible := False;
stgAprendizagem.SetFocus;
end;

procedure TForm1.ComboBox4Change(Sender: TObject);
begin
stgEmpreendedorismo.Cells[stgEmpreendedorismo.Col,stgEmpreendedorismo.Row]:= ComboBox4.Items[ComboBox4.ItemIndex];
ComboBox4.Visible := False;
stgEmpreendedorismo.SetFocus;
end;

procedure TForm1.ComboBox5Change(Sender: TObject);
begin
stgQualidade.Cells[stgQualidade.Col,stgQualidade.Row]:= ComboBox5.Items[ComboBox5.ItemIndex];
ComboBox5.Visible := False;
stgQualidade.SetFocus;
end;


procedure TForm1.ComboBox1Exit(Sender: TObject);
var
i,cont:integer;
MEDIA:real;
begin

with stgInteracaoRelacional do
  with sender as tcombobox do
   begin
        Cells[Col,Row] := Items[ItemIndex];
        Visible := False;
        stgInteracaoRelacional.SetFocus;       // mudar nome stringgrid
        ItemIndex:=-1;
        cont:=0;
        for i :=1 to RowCount-1 do
                begin
                   if  Cells[Col,i]='Insuficiente' then  cont:=cont+1 else
                   if  Cells[Col,i]='Regular' then  cont:=cont+2 else
                   if  Cells[Col,i]='Bom' then  cont:=cont+3 else
                   if  Cells[Col,i]='Muito Bom' then  cont:=cont+4 else
                   if  Cells[Col,i]='�timo' then  cont:=cont+5
                end;
        MEDIA:=cont/(rowcount-2);
        if (MEDIA>=1) AND (MEDIA<=1.5) THEN
           Cells[col,9]:='Insuficiente' ELSE
        if (MEDIA>=1.63) AND (MEDIA<=2.5) THEN
           Cells[col,9]:='Regular' ELSE
        if (MEDIA>=2.63) AND (MEDIA<=3.5) THEN
           Cells[col,9]:='Bom' ELSE
        if (MEDIA>=3.63) AND (MEDIA<=4.5) THEN
           Cells[col,9]:='Muito Bom' ELSE
        if (MEDIA>=4.63) AND (MEDIA<=5) THEN
           Cells[col,9]:='�timo' ;
   end;

end;

procedure TForm1.ComboBox2Exit(Sender: TObject);
var
i,cont:integer;
MEDIA:real;
begin
with stgParticipacao do
  with sender as tcombobox do
   begin
        Cells[Col,Row] := Items[ItemIndex];
        Visible := False;
        stgParticipacao.SetFocus;       // mudar nome stringgrid
        ItemIndex:=-1;
        cont:=0;
        for i :=1 to RowCount-1 do
                begin
                   if  Cells[Col,i]='Insuficiente' then  cont:=cont+1 else
                   if  Cells[Col,i]='Regular' then  cont:=cont+2 else
                   if  Cells[Col,i]='Bom' then  cont:=cont+3 else
                   if  Cells[Col,i]='Muito Bom' then  cont:=cont+4 else
                   if  Cells[Col,i]='�timo' then  cont:=cont+5
                end;
        MEDIA:=cont/(rowcount-2);
        if (MEDIA>=1) AND (MEDIA<=1.5) THEN
           Cells[col,9]:='Insuficiente' ELSE
        if (MEDIA>=1.63) AND (MEDIA<=2.5) THEN
           Cells[col,9]:='Regular' ELSE
        if (MEDIA>=2.63) AND (MEDIA<=3.5) THEN
           Cells[col,9]:='Bom' ELSE
        if (MEDIA>=3.63) AND (MEDIA<=4.5) THEN
           Cells[col,9]:='Muito Bom' ELSE
        if (MEDIA>=4.63) AND (MEDIA<=5) THEN
           Cells[col,9]:='�timo' ;
   end;
end;

procedure TForm1.ComboBox3Exit(Sender: TObject);
var
i,cont:integer;
MEDIA:real;
begin
with stgAprendizagem do
  with sender as tcombobox do
   begin
        Cells[Col,Row] := Items[ItemIndex];
        Visible := False;
        stgAprendizagem.SetFocus;       // mudar nome stringgrid
        ItemIndex:=-1;
        cont:=0;
        for i :=1 to RowCount-1 do
                begin
                   if  Cells[Col,i]='Insuficiente' then  cont:=cont+1 else
                   if  Cells[Col,i]='Regular' then  cont:=cont+2 else
                   if  Cells[Col,i]='Bom' then  cont:=cont+3 else
                   if  Cells[Col,i]='Muito Bom' then  cont:=cont+4 else
                   if  Cells[Col,i]='�timo' then  cont:=cont+5
                end;
        MEDIA:=cont/(rowcount-2);
        if (MEDIA>=1) AND (MEDIA<=1.5) THEN
           Cells[col,7]:='Insuficiente' ELSE
        if (MEDIA>=1.63) AND (MEDIA<=2.5) THEN
           Cells[col,7]:='Regular' ELSE
        if (MEDIA>=2.63) AND (MEDIA<=3.5) THEN
           Cells[col,7]:='Bom' ELSE
        if (MEDIA>=3.63) AND (MEDIA<=4.5) THEN
           Cells[col,7]:='Muito Bom' ELSE
        if (MEDIA>=4.63) AND (MEDIA<=5) THEN
           Cells[col,7]:='�timo' ;
   end;

end;

procedure TForm1.ComboBox4Exit(Sender: TObject);
var
i,cont:integer;
MEDIA:real;
begin
with stgEmpreendedorismo do
  with sender as tcombobox do
   begin
        Cells[Col,Row] := Items[ItemIndex];
        Visible := False;
        stgEmpreendedorismo.SetFocus;       // mudar nome stringgrid
        ItemIndex:=-1;
        cont:=0;
        for i :=1 to RowCount-1 do
                begin
                   if  Cells[Col,i]='Insuficiente' then  cont:=cont+1 else
                   if  Cells[Col,i]='Regular' then  cont:=cont+2 else
                   if  Cells[Col,i]='Bom' then  cont:=cont+3 else
                   if  Cells[Col,i]='Muito Bom' then  cont:=cont+4 else
                   if  Cells[Col,i]='�timo' then  cont:=cont+5
                end;
        MEDIA:=cont/(rowcount-2);
        if (MEDIA>=1) AND (MEDIA<=1.5) THEN
           Cells[col,8]:='Insuficiente' ELSE
        if (MEDIA>=1.63) AND (MEDIA<=2.5) THEN
           Cells[col,8]:='Regular' ELSE
        if (MEDIA>=2.63) AND (MEDIA<=3.5) THEN
           Cells[col,8]:='Bom' ELSE
        if (MEDIA>=3.63) AND (MEDIA<=4.5) THEN
           Cells[col,8]:='Muito Bom' ELSE
        if (MEDIA>=4.63) AND (MEDIA<=5) THEN
           Cells[col,8]:='�timo' ;
   end;

end;

procedure TForm1.ComboBox5Exit(Sender: TObject);
var
i,cont:integer;
MEDIA:real;
begin
with stgQualidade do
  with sender as tcombobox do
   begin
        Cells[Col,Row] := Items[ItemIndex];
        Visible := False;
        stgQualidade.SetFocus;       // mudar nome stringgrid
        ItemIndex:=-1;
        cont:=0;
        for i :=1 to RowCount-1 do
                begin
                   if  Cells[Col,i]='Insuficiente' then  cont:=cont+1 else
                   if  Cells[Col,i]='Regular' then  cont:=cont+2 else
                   if  Cells[Col,i]='Bom' then  cont:=cont+3 else
                   if  Cells[Col,i]='Muito Bom' then  cont:=cont+4 else
                   if  Cells[Col,i]='�timo' then  cont:=cont+5
                end;
        MEDIA:=cont/(rowcount-2);
        if (MEDIA>=1) AND (MEDIA<=1.5) THEN
           Cells[col,8]:='Insuficiente' ELSE
        if (MEDIA>=1.63) AND (MEDIA<=2.5) THEN
           Cells[col,8]:='Regular' ELSE
        if (MEDIA>=2.63) AND (MEDIA<=3.5) THEN
           Cells[col,8]:='Bom' ELSE
        if (MEDIA>=3.63) AND (MEDIA<=4.5) THEN
           Cells[col,8]:='Muito Bom' ELSE
        if (MEDIA>=4.63) AND (MEDIA<=5) THEN
           Cells[col,8]:='�timo' ;
   end;

end;

procedure TForm1.stgAprendizagemSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if ((aCol <> 0) AND(aRow <> 8)) then
    begin
    R := stgAprendizagem.CellRect(aCol, aRow);
    R.Left := R.Left + stgAprendizagem.Left;
    R.Right := R.Right + stgAprendizagem.Left;
    R.Top := R.Top + stgAprendizagem.Top;
    R.Bottom := R.Bottom + stgAprendizagem.Top;
    ComboBox3.Left := R.Left+2;
    ComboBox3.Top := R.Top+3;
    ComboBox3.Width := (R.Right +1) - R.Left;
    ComboBox3.Height := (R.Bottom +1) - R.Top;
    ComboBox3.Visible := True;
    ComboBox3.SetFocus;
    end;
CanSelect := True;

end;

procedure TForm1.stgEmpreendedorismoSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if ((aCol <> 0) AND(aRow <> 8)) then
    begin
    R := stgEmpreendedorismo.CellRect(aCol, aRow);
    R.Left := R.Left + stgEmpreendedorismo.Left;
    R.Right := R.Right + stgEmpreendedorismo.Left;
    R.Top := R.Top + stgEmpreendedorismo.Top;
    R.Bottom := R.Bottom + stgEmpreendedorismo.Top;
    ComboBox4.Left := R.Left+2;
    ComboBox4.Top := R.Top+3;
    ComboBox4.Width := (R.Right +1) - R.Left;
    ComboBox4.Height := (R.Bottom +1) - R.Top;
    ComboBox4.Visible := True;
    ComboBox4.SetFocus;
    end;
CanSelect := True;

end;

procedure TForm1.stgQualidadeSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
R: TRect;
begin
if ((aCol <> 0) AND(aRow <> 8)) then
    begin
    R := stgQualidade.CellRect(aCol, aRow);
    R.Left := R.Left + stgQualidade.Left;
    R.Right := R.Right + stgQualidade.Left;
    R.Top := R.Top + stgQualidade.Top;
    R.Bottom := R.Bottom + stgQualidade.Top;
    ComboBox5.Left := R.Left+2;
    ComboBox5.Top := R.Top+3;
    ComboBox5.Width := (R.Right +1) - R.Left;
    ComboBox5.Height := (R.Bottom +1) - R.Top;
    ComboBox5.Visible := True;
    ComboBox5.SetFocus;
    end;
CanSelect := True;


end;

procedure TForm1.stgInteracaoRelacionalDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
with sender as TStringGrid do
   begin
    if arow=9 then Canvas.Brush.Color:= $00B8CCA8;
    Canvas.TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[Acol,Arow]);
   end;
end;

procedure TForm1.stgParticipacaoDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
with sender as TStringGrid do
   begin
    if arow=9 then Canvas.Brush.Color:= $00B8CCA8;
    Canvas.TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[Acol,Arow]);
   end;
end;

procedure TForm1.stgAprendizagemDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
with sender as TStringGrid do
   begin
    if arow=7 then Canvas.Brush.Color:= $00B8CCA8;
    Canvas.TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[Acol,Arow]);
   end;
end;

procedure TForm1.stgEmpreendedorismoDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
with sender as TStringGrid do
   begin
    if arow=8 then Canvas.Brush.Color:= $00B8CCA8;
    Canvas.TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[Acol,Arow]);
   end;
end;

procedure TForm1.stgQualidadeDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
with sender as TStringGrid do
   begin
    if arow=8 then Canvas.Brush.Color:= $00B8CCA8;
    Canvas.TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[Acol,Arow]);
   end;
end;

procedure TForm1.btnSalvaClick(Sender: TObject);
var
 i:integer;
begin

with  dbdecaserver do
begin
try
//*********************************************************************
  with stgInteracaoRelacional do
    for i:=1 to RowCount-2 do
        begin
           with cdsINSUPD_relatorioAICA.Params do
             begin
                ParamValues['@tx_criterio']:='IR' ;// modificar para cada stringgrid
                ParamValues['@cod_matricula']:=edtBusca.Text;
                ParamValues['@cd_unidade']:=cds_sel_RELATORIOAicaDP.FieldValues['cod_unidade'] ;
                ParamValues['@nm_ano']:=edtAnoRef.Text;
                ParamValues['@tx_opcao']:=cells[0,i];
                ParamValues['@bimestre1']:=cells[1,i];
                ParamValues['@bimestre2']:=cells[2,i];
                ParamValues['@bimestre3']:=cells[3,i];
                ParamValues['@bimestre4']:=cells[4,i];
                ParamValues['@bt_travado']:=0;
                ParamValues['@dt_logdatahora']:=(date);
                ParamValues['@tx_logusuario']:='Qualidade'
             end;
           cdsINSUPD_relatorioAICA.Execute;
        end;


//*********************************************************************
  with stgParticipacao do
    for i:=1 to RowCount-2 do
        begin
           with cdsINSUPD_relatorioAICA.Params do
             begin
                ParamValues['@tx_criterio']:='P' ;// modificar para cada stringgrid
                ParamValues['@cod_matricula']:=edtBusca.Text;
                ParamValues['@cd_unidade']:=cds_sel_RELATORIOAicaDP.FieldValues['cod_unidade'] ;
                ParamValues['@nm_ano']:=edtAnoRef.Text;
                ParamValues['@tx_opcao']:=cells[0,i];
                ParamValues['@bimestre1']:=cells[1,i];
                ParamValues['@bimestre2']:=cells[2,i];
                ParamValues['@bimestre3']:=cells[3,i];
                ParamValues['@bimestre4']:=cells[4,i];
                ParamValues['@bt_travado']:=0;
                ParamValues['@dt_logdatahora']:=(date);
                ParamValues['@tx_logusuario']:='Qualidade'
             end;
           cdsINSUPD_relatorioAICA.Execute;
        end;

//*********************************************************************
  with stgAprendizagem do
    for i:=1 to RowCount-2 do
        begin
           with cdsINSUPD_relatorioAICA.Params do
             begin
                ParamValues['@tx_criterio']:='A' ;// modificar para cada stringgrid
                ParamValues['@cod_matricula']:=edtBusca.Text;
                ParamValues['@cd_unidade']:=cds_sel_RELATORIOAicaDP.FieldValues['cod_unidade'] ;
                ParamValues['@nm_ano']:=edtAnoRef.Text;
                ParamValues['@tx_opcao']:=cells[0,i];
                ParamValues['@bimestre1']:=cells[1,i];
                ParamValues['@bimestre2']:=cells[2,i];
                ParamValues['@bimestre3']:=cells[3,i];
                ParamValues['@bimestre4']:=cells[4,i];
                ParamValues['@bt_travado']:=0;
                ParamValues['@dt_logdatahora']:=(date);
                ParamValues['@tx_logusuario']:='Qualidade'
             end;
           cdsINSUPD_relatorioAICA.Execute;
        end;
 //*********************************************************************
  with stgEmpreendedorismo do
    for i:=1 to RowCount-2 do
        begin
           with cdsINSUPD_relatorioAICA.Params do
             begin
                ParamValues['@tx_criterio']:='E' ;// modificar para cada stringgrid
                ParamValues['@cod_matricula']:=edtBusca.Text;
                ParamValues['@cd_unidade']:=cds_sel_RELATORIOAicaDP.FieldValues['cod_unidade'] ;
                ParamValues['@nm_ano']:=edtAnoRef.Text;
                ParamValues['@tx_opcao']:=cells[0,i];
                ParamValues['@bimestre1']:=cells[1,i];
                ParamValues['@bimestre2']:=cells[2,i];
                ParamValues['@bimestre3']:=cells[3,i];
                ParamValues['@bimestre4']:=cells[4,i];
                ParamValues['@bt_travado']:=0;
                ParamValues['@dt_logdatahora']:=(date);
                ParamValues['@tx_logusuario']:='Qualidade'
             end;
           cdsINSUPD_relatorioAICA.Execute;
        end;

//*********************************************************************
  with stgQualidade do
    for i:=1 to RowCount-2 do
        begin
           with cdsINSUPD_relatorioAICA.Params do
             begin
                ParamValues['@tx_criterio']:='Q' ;// modificar para cada stringgrid
                ParamValues['@cod_matricula']:=edtBusca.Text;
                ParamValues['@cd_unidade']:=cds_sel_RELATORIOAicaDP.FieldValues['cod_unidade'] ;
                ParamValues['@nm_ano']:=edtAnoRef.Text;
                ParamValues['@tx_opcao']:=cells[0,i];
                ParamValues['@bimestre1']:=cells[1,i];
                ParamValues['@bimestre2']:=cells[2,i];
                ParamValues['@bimestre3']:=cells[3,i];
                ParamValues['@bimestre4']:=cells[4,i];
                ParamValues['@bt_travado']:=0;
                ParamValues['@dt_logdatahora']:=(date);
                ParamValues['@tx_logusuario']:='Qualidade'
             end;
           cdsINSUPD_relatorioAICA.Execute;
        end;

           with cdsINSUPD_RelatorioAICAObs.Params do
             begin
                ParamValues['@cod_matricula']:=edtBusca.Text;
                ParamValues['@nm_ano']:=edtAnoRef.Text;
                ParamValues['@tx_obs']:=Memo1.Lines.Text;
             end;
             cdsINSUPD_RelatorioAICAObs.Execute;



        showmessage('Os dados foram salvos com sucesso!' );
        btnSalva.Visible:=false;
except
end;
end;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
var
filtro: string;
i:integer;
begin
try

with dbdecaserver do
  begin

    if cds_sel_RELATORIOAicaDP.Active then cds_sel_RELATORIOAicaDP.Active:=false;
    if length(trim(edtBusca.text))>0 then
       begin
        cds_sel_RELATORIOAicaDP.Params.ParamValues['@matricula']:=edtBusca.Text;
        cds_sel_RELATORIOAicaDP.Active:=true;
        if cds_sel_RELATORIOAicaDP.RecordCount > 0 then
           begin
              lblNome.caption:= cds_sel_RELATORIOAicaDP.FieldValues['nom_nome'];
              lblUnidade.caption:= cds_sel_RELATORIOAicaDP.FieldValues['nom_unidade'];
              btnSalva.Visible:=true ;
              if cds_sel_RELATORIOAica.Active then cds_sel_RELATORIOAica.Close;
              cds_sel_RELATORIOAica.Params.ParamValues['@matricula']:=edtBusca.text;
              cds_sel_RELATORIOAica.Params.ParamValues['@ano']:= edtAnoRef.text;
              cds_sel_RELATORIOAica.open;
              if cds_sel_RELATORIOAica.RecordCount > 0 then
                begin
                  with stgInteracaoRelacional do
                  for i:=1 to rowcount-2 do
                     begin
                       if cds_sel_RELATORIOAica.FieldValues['tx_criterio']='IR' then
                         begin
                           Cells[0,i]:= cds_sel_RELATORIOAica.FieldValues['tx_opcao'];
                           Cells[1,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre1'];
                           Cells[2,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre2'];
                           Cells[3,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre3'];
                           Cells[4,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre4'];
                           if not cds_sel_RELATORIOAica.eof then cds_sel_RELATORIOAica.next;
                         end;
                     end;
                  with stgParticipacao do
                  for i:=1 to rowcount-2 do
                     begin
                       if cds_sel_RELATORIOAica.FieldValues['tx_criterio']='P' then
                         begin
                           Cells[0,i]:= cds_sel_RELATORIOAica.FieldValues['tx_opcao'];
                           Cells[1,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre1'];
                           Cells[2,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre2'];
                           Cells[3,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre3'];
                           Cells[4,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre4'];
                           if not cds_sel_RELATORIOAica.eof then cds_sel_RELATORIOAica.next;
                         end;
                     end;
                   with stgAprendizagem do
                  for i:=1 to rowcount-2 do
                     begin
                       if cds_sel_RELATORIOAica.FieldValues['tx_criterio']='A' then
                         begin
                           Cells[0,i]:= cds_sel_RELATORIOAica.FieldValues['tx_opcao'];
                           Cells[1,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre1'];
                           Cells[2,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre2'];
                           Cells[3,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre3'];
                           Cells[4,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre4'];
                           if not cds_sel_RELATORIOAica.eof then cds_sel_RELATORIOAica.next;
                         end;
                     end;
                  with stgEmpreendedorismo do
                  for i:=1 to rowcount-2 do
                     begin
                       if cds_sel_RELATORIOAica.FieldValues['tx_criterio']='E' then
                         begin
                           Cells[0,i]:= cds_sel_RELATORIOAica.FieldValues['tx_opcao'];
                           Cells[1,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre1'];
                           Cells[2,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre2'];
                           Cells[3,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre3'];
                           Cells[4,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre4'];
                           if not cds_sel_RELATORIOAica.eof then cds_sel_RELATORIOAica.next;
                         end;
                     end;
                  with stgQualidade do
                  for i:=1 to rowcount-2 do
                     begin
                       if cds_sel_RELATORIOAica.FieldValues['tx_criterio']='Q' then
                         begin
                           Cells[0,i]:= cds_sel_RELATORIOAica.FieldValues['tx_opcao'];
                           Cells[1,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre1'];
                           Cells[2,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre2'];
                           Cells[3,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre3'];
                           Cells[4,i]:= cds_sel_RELATORIOAica.FieldValues['bimestre4'];
                           if not cds_sel_RELATORIOAica.eof then cds_sel_RELATORIOAica.next;
                         end;
                     end;
              if cds_sel_RELATORIOAicaobs.Active then cds_sel_RELATORIOAicaobs.Close;
              cds_sel_RELATORIOAicaobs.Params.ParamValues['@matricula']:=edtBusca.text;
              cds_sel_RELATORIOAicaobs.Params.ParamValues['@ano']:= edtAnoRef.text;
              cds_sel_RELATORIOAicaobs.open;
              if cds_sel_RELATORIOAicaobs.RecordCount > 0 then
                 memo1.Text:=cds_sel_RELATORIOAicaobs.FieldValues['tx_obs'];
             end
             else  //  caso n�o tenha nada cadastrado para essa matricula
////*****************************************************************************
                begin
                  with stgInteracaoRelacional do
                  for i:=1 to rowcount-2 do
                     begin
                           Cells[1,i]:= '';
                           Cells[2,i]:= '';
                           Cells[3,i]:= '';
                           Cells[4,i]:= '';
                     end;
                  with stgParticipacao do
                  for i:=1 to rowcount-2 do
                     begin
                           Cells[1,i]:= '';
                           Cells[2,i]:= '';
                           Cells[3,i]:= '';
                           Cells[4,i]:= '';
                     end;
                   with stgAprendizagem do
                  for i:=1 to rowcount-2 do
                     begin
                           Cells[1,i]:= '';
                           Cells[2,i]:= '';
                           Cells[3,i]:= '';
                           Cells[4,i]:= '';
                     end;
                  with stgEmpreendedorismo do
                  for i:=1 to rowcount-2 do
                     begin
                           Cells[1,i]:= '';
                           Cells[2,i]:= '';
                           Cells[3,i]:= '';
                           Cells[4,i]:= '';
                     end;
                  with stgQualidade do
                  for i:=1 to rowcount-2 do
                     begin
                           Cells[1,i]:= '';
                           Cells[2,i]:= '';
                           Cells[3,i]:= '';
                           Cells[4,i]:= '';
                     end;
                end;
//********************************************************************************
           end
     else
        showmessage('N�o foi encontrado registro com essa matricula');
       end
    else
        showmessage('Digite a matricula');
  end;
except;
end;
end;

end.




