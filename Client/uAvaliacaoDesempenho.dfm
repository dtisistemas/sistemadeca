object frmAvaliacaoDesempenho: TfrmAvaliacaoDesempenho
  Left = 294
  Top = 186
  BorderStyle = bsDialog
  Caption = 'Sistema Deca - [Question�rio de Avalia��o de Desempenho]'
  ClientHeight = 462
  ClientWidth = 743
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 4
    Top = 4
    Width = 733
    Height = 454
    ActivePage = TabSheet3
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Dados da Pesquisa'
      object PageControl2: TPageControl
        Left = 9
        Top = 60
        Width = 704
        Height = 301
        ActivePage = TabSheet2
        TabOrder = 0
        object TabSheet2: TTabSheet
          Caption = 'Question�rio'
          object GroupBox3: TGroupBox
            Left = 8
            Top = 8
            Width = 679
            Height = 49
            Caption = 'Dados da Digita��o'
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 21
              Width = 60
              Height = 13
              Caption = 'Digitado por:'
            end
            object Label2: TLabel
              Left = 329
              Top = 21
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label5: TLabel
              Left = 457
              Top = 21
              Width = 41
              Height = 13
              Caption = 'Per�odo:'
            end
            object edUsuario: TEdit
              Left = 72
              Top = 21
              Width = 250
              Height = 14
              BorderStyle = bsNone
              Color = clMenu
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object meDataAvaliacao: TMaskEdit
              Left = 360
              Top = 18
              Width = 91
              Height = 21
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 1
              Text = '  /  /    '
            end
            object mePeriodo: TMaskEdit
              Left = 501
              Top = 18
              Width = 68
              Height = 21
              EditMask = '99/9999'
              MaxLength = 7
              TabOrder = 2
              Text = '  /    '
            end
          end
          object Panel5: TPanel
            Left = 8
            Top = 60
            Width = 679
            Height = 205
            BevelInner = bvLowered
            TabOrder = 1
            object Shape1: TShape
              Left = 56
              Top = 19
              Width = 240
              Height = 25
            end
            object Label7: TLabel
              Left = 87
              Top = 25
              Width = 176
              Height = 13
              Caption = 'PONTOS OBTIDOS NA AVALIA��O'
              Transparent = True
            end
            object Shape2: TShape
              Left = 56
              Top = 49
              Width = 196
              Height = 25
            end
            object Label8: TLabel
              Left = 60
              Top = 55
              Width = 79
              Height = 13
              Caption = 'ADOLESCENTE'
              Transparent = True
            end
            object Shape3: TShape
              Left = 256
              Top = 49
              Width = 40
              Height = 25
            end
            object Shape4: TShape
              Left = 56
              Top = 76
              Width = 196
              Height = 25
            end
            object Shape5: TShape
              Left = 256
              Top = 76
              Width = 40
              Height = 25
            end
            object Shape6: TShape
              Left = 56
              Top = 103
              Width = 196
              Height = 25
            end
            object Shape7: TShape
              Left = 256
              Top = 103
              Width = 40
              Height = 25
            end
            object Shape8: TShape
              Left = 56
              Top = 130
              Width = 196
              Height = 25
            end
            object Shape9: TShape
              Left = 256
              Top = 130
              Width = 40
              Height = 25
            end
            object Shape10: TShape
              Left = 56
              Top = 157
              Width = 196
              Height = 25
            end
            object Shape11: TShape
              Left = 256
              Top = 157
              Width = 40
              Height = 25
            end
            object Label9: TLabel
              Left = 62
              Top = 82
              Width = 52
              Height = 13
              Caption = 'EMPRESA'
              Transparent = True
            end
            object Label10: TLabel
              Left = 63
              Top = 109
              Width = 65
              Height = 13
              Caption = 'ASS. SOCIAL'
              Transparent = True
            end
            object Label11: TLabel
              Left = 64
              Top = 136
              Width = 66
              Height = 13
              Caption = 'PROFESSOR'
              Transparent = True
            end
            object Label12: TLabel
              Left = 64
              Top = 163
              Width = 64
              Height = 13
              Caption = 'INSTRUTOR'
              Transparent = True
            end
            object Shape12: TShape
              Left = 320
              Top = 19
              Width = 240
              Height = 25
            end
            object Shape13: TShape
              Left = 320
              Top = 49
              Width = 196
              Height = 25
            end
            object Shape14: TShape
              Left = 520
              Top = 49
              Width = 40
              Height = 25
            end
            object Shape15: TShape
              Left = 320
              Top = 76
              Width = 196
              Height = 25
            end
            object Shape16: TShape
              Left = 520
              Top = 76
              Width = 40
              Height = 25
            end
            object Shape17: TShape
              Left = 520
              Top = 103
              Width = 40
              Height = 25
            end
            object Shape18: TShape
              Left = 520
              Top = 130
              Width = 40
              Height = 25
            end
            object Shape19: TShape
              Left = 520
              Top = 157
              Width = 40
              Height = 25
            end
            object Shape20: TShape
              Left = 320
              Top = 157
              Width = 196
              Height = 25
            end
            object Shape21: TShape
              Left = 320
              Top = 130
              Width = 196
              Height = 25
            end
            object Shape22: TShape
              Left = 320
              Top = 103
              Width = 196
              Height = 25
            end
            object Label6: TLabel
              Left = 332
              Top = 55
              Width = 152
              Height = 13
              Caption = 'TOTAL DE PONTOS OBTIDOS'
              Transparent = True
            end
            object Label13: TLabel
              Left = 332
              Top = 82
              Width = 151
              Height = 13
              Caption = 'TOTAL DE ITENS AVALIADOS'
              Transparent = True
            end
            object Label14: TLabel
              Left = 332
              Top = 109
              Width = 100
              Height = 13
              Caption = 'M�DIA DE PONTOS'
              Transparent = True
            end
            object Label15: TLabel
              Left = 332
              Top = 135
              Width = 121
              Height = 13
              Caption = 'CLASSIFICA��O GERAL'
              Transparent = True
            end
            object Label16: TLabel
              Left = 396
              Top = 25
              Width = 76
              Height = 13
              Caption = 'ESTAT�STICAS'
              Transparent = True
            end
            object btnMedia: TSpeedButton
              Left = 574
              Top = 80
              Width = 90
              Height = 45
              Flat = True
            end
            object edPontosAdolescente: TEdit
              Left = 262
              Top = 51
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clInfoBk
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 0
              Text = '00'
            end
            object edPontosEmpresa: TEdit
              Left = 262
              Top = 78
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clInfoBk
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 1
              Text = '00'
            end
            object edPontosASocial: TEdit
              Left = 262
              Top = 107
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clInfoBk
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 2
              Text = '00'
            end
            object edPontosPofessor: TEdit
              Left = 262
              Top = 133
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clInfoBk
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 3
              Text = '00'
            end
            object edPontosInstrutor: TEdit
              Left = 262
              Top = 159
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clInfoBk
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 4
              Text = '00'
            end
            object Edit1: TEdit
              Left = 526
              Top = 51
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clBtnHighlight
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 5
              Text = '00'
            end
            object Edit2: TEdit
              Left = 526
              Top = 79
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clBtnHighlight
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 6
              Text = '00'
            end
            object Edit3: TEdit
              Left = 526
              Top = 106
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clBtnHighlight
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 7
              Text = '0,00'
            end
            object Edit4: TEdit
              Left = 526
              Top = 133
              Width = 26
              Height = 19
              BorderStyle = bsNone
              Color = clBtnHighlight
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              MaxLength = 2
              ParentFont = False
              TabOrder = 8
              Text = 'XX'
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Observa��es'
          ImageIndex = 1
          object GroupBox2: TGroupBox
            Left = 8
            Top = 8
            Width = 680
            Height = 258
            Caption = 'Observa��es'
            TabOrder = 0
            object Memo1: TMemo
              Left = 10
              Top = 19
              Width = 657
              Height = 229
              TabOrder = 0
            end
          end
        end
      end
      object Panel3: TPanel
        Left = 10
        Top = 365
        Width = 703
        Height = 49
        TabOrder = 1
        object btnNovo: TBitBtn
          Left = 346
          Top = 10
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvar: TBitBtn
          Left = 421
          Top = 10
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnCancelar: TBitBtn
          Left = 481
          Top = 10
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnImprimir: TBitBtn
          Left = 556
          Top = 10
          Width = 55
          Height = 30
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777776B00777777777000777777776E007777777007880077777771007777
            7007780088007777740077770778878800880077770077778887778888008077
            7A00777887777788888800777D007778F7777F888888880780007778F77FF777
            8888880783007778FFF779977788880786007778F77AA7778807880789007777
            88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
            920077777777778FFFFFF0079500777777777778FFF887779800777777777777
            888777779B00777777777777777777779E0077777777777777777777A1007777
            7777777777777777A400}
        end
        object BitBtn1: TBitBtn
          Left = 631
          Top = 10
          Width = 55
          Height = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 8
        Width = 705
        Height = 49
        TabOrder = 2
        object Label3: TLabel
          Left = 16
          Top = 20
          Width = 59
          Height = 13
          Caption = 'Matr�cula:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNome: TLabel
          Left = 216
          Top = 19
          Width = 262
          Height = 18
          Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnPesquisar: TSpeedButton
          Left = 169
          Top = 14
          Width = 28
          Height = 24
          Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777770000777777777777777700770000777777777777777C440700007777
            7777777777C4C40700007777777777777C4C4077000077777777777784C40777
            0000777777788888F740777700007777770000007807777700007777707EFEE6
            007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
            8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
            0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
            7777777700A077777777777777777777FFA077777777777777777777FFFF7777
            7777777777777777FF81}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnPesquisarClick
        end
        object txtMatricula: TMaskEdit
          Left = 82
          Top = 16
          Width = 83
          Height = 21
          Hint = 'Informe o N�MERO DE MATR�CULA'
          EditMask = '!99999999;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '        '
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Pesquisas Anteriores'
      ImageIndex = 2
      object Panel1: TPanel
        Left = 5
        Top = 8
        Width = 713
        Height = 41
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 5
        Top = 53
        Width = 713
        Height = 363
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 13
          Top = 15
          Width = 685
          Height = 335
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 751
    Top = 21
    Width = 297
    Height = 528
    Caption = 'Selecione a unidade'
    TabOrder = 1
    Visible = False
    object clbAlunos: TCheckListBox
      Left = 9
      Top = 42
      Width = 275
      Height = 455
      ItemHeight = 13
      TabOrder = 0
    end
    object cbUnidade: TComboBox
      Left = 9
      Top = 16
      Width = 276
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
    end
    object Panel4: TPanel
      Left = 9
      Top = 499
      Width = 275
      Height = 24
      TabOrder = 2
    end
  end
end
