unit uCalendarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, Db, ImgList;

type
  TfrmCalendarios = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    lbUsuario: TLabel;
    edDescricaoCalendario: TEdit;
    edInclusao: TMaskEdit;
    Panel2: TPanel;
    dbgCalendarios: TDBGrid;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnAlterar: TBitBtn;
    btnVerDatas: TBitBtn;
    rgAtual: TRadioGroup;
    dsCalendarios: TDataSource;
    btnSair: TBitBtn;
    btnCopiar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure AtualizaTela_Calendario (Modo:String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure dbgCalendariosDblClick(Sender: TObject);
    procedure btnVerDatasClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCalendarios: TfrmCalendarios;

implementation

uses uDM, uPrincipal, uItensCalendario;

{$R *.DFM}

procedure TfrmCalendarios.AtualizaTela_Calendario(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    edDescricaoCalendario.Clear;
    GroupBox1.Enabled := False;
    edInclusao.Clear;
    GroupBox2.Enabled := False;
    lbUsuario.Caption := '';
    GroupBox3.Enabled := False;
    rgAtual.ItemIndex := -1;
    rgAtual.Enabled := False;

    btnCopiar.Enabled := True;
    btnVerDatas.Enabled := True;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin

    btnCopiar.Enabled := False;
    btnVerDatas.Enabled := False;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    rgAtual.Enabled := True;

  end;
end;

procedure TfrmCalendarios.FormShow(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Null;
      Params.ParamByName('@pe_flg_atual').Value := Null;
      Open;

      dbgCalendarios.Refresh;

    end;
  except end;

  AtualizaTela_Calendario ('Padr�o');

end;

procedure TfrmCalendarios.btnNovoClick(Sender: TObject);
begin
  AtualizaTela_Calendario('Novo');
  edDescricaoCalendario.SetFocus;
  lbUsuario.Caption := vvNOM_USUARIO;
  edInclusao.Text := DateToStr(Date());
end;

procedure TfrmCalendarios.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela_Calendario('Padr�o');
end;

procedure TfrmCalendarios.btnSalvarClick(Sender: TObject);
begin
  //Validar o nome antes da grava��o
  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Trim(edDescricaoCalendario.Text);
      Params.ParamByName('@pe_flg_atual').Value := Null;
      Open;

      if RecordCount > 0 then
      begin
        ShowMessage ('J� existe um calend�rio com esse nome ! Favor informar outro nome...');
        edDescricaoCalendario.SetFocus;
      end

      else
      begin
        //Atualiza todos os calendarios para "anteriores"
        with dmDeca.cdsAlt_Calendario do
        begin
          Close;
          Params.ParamByName('@pe_cod_calendario').Value := Null;
          Params.ParamByName('@pe_flg_atual').Value := 1;
          Params.ParamByName('@pe_dsc_calendario').Value := Null;
          Execute;
        end;

        with dmDeca.cdsInc_Calendario do
        begin
          Close;
          Params.ParamByName('@pe_dsc_calendario').Value := Trim(edDescricaoCalendario.Text);
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Params.ParamByName('@pe_flg_atual').Value := 0;
          Execute;

          //Atualiza o grid
          with dmDeca.cdsSel_Calendario do
          begin
            Close;
            Params.ParamByName('@pe_cod_calendario').Value := Null;
            Params.ParamByName('@pe_dsc_calendario').Value := Null;
            Params.ParamByName('@pe_flg_atual').Value := Null;
            Open;
            dbgCalendarios.Refresh;
          end;
        end;
        AtualizaTela_Calendario('Padr�o');
      end;
    end;
  except
  begin
    ShowMessage('Aten��o!!! Os dados n�o foram gravados. Entre em contato com o Administrador do Sistema!');
    AtualizaTela_Calendario('Padr�o');
  end;
end;
end;

procedure TfrmCalendarios.btnSairClick(Sender: TObject);
begin
  frmCalendarios.Close;
end;

procedure TfrmCalendarios.dbgCalendariosDblClick(Sender: TObject);
begin
  //edDescricaoCalendario.Text := dmDeca.cdsSel_Calendario.FieldByName('dsc_calendario').Value;
  //edInclusao.Text := DateToStr(dmDeca.cdsSel_Calendario.FieldByName('log_data').AsDateTime);
  //rgAtual.ItemIndex := dmDeca.cdsSel_Calendario.FieldByName('flg_atual').Value;
end;

procedure TfrmCalendarios.btnVerDatasClick(Sender: TObject);
begin

  Application.CreateForm (TfrmItensCalendario, frmItensCalendario);

  //Carrega o calend�rio ativo/atual
  try
    with dmDeca.cdsSel_Calendario do
    begin
      Close;
      Params.ParamByName('@pe_cod_calendario').Value := Null;
      Params.ParamByName('@pe_dsc_calendario').Value := Null;
      Params.ParamByName('@pe_flg_atual').AsInteger := 0;
      Open;

      //CArrega os dados do calend�rio atual assim como as datas j� lan�adas
      if RecordCount > 0 then
      begin
        frmItensCalendario.edCalendarioAtual.Text := dmDeca.cdsSel_Calendario.FieldByName('dsc_calendario').AsString;

        with dmDeca.cdsSel_ItensCalendario do
        begin
          Close;
          Params.ParamByName ('@pe_cod_calendario').Value := dmDeca.cdsSel_Calendario.FieldByName('cod_calendario').Value;
          Params.ParamByName ('@pe_ind_tipo_feriado').Value := Null;
          Params.ParamByName ('@pe_num_dia').Value := Null;
          Params.ParamByName ('@pe_num_mes').Value := Null;
          Params.ParamByName ('@pe_num_ano').Value := Null;
          Params.ParamByName ('@pe_num_semana').Value := Null;
          Open;
          frmItensCalendario.dbDatas.Refresh;
        end
      end
      else
      begin
        ShowMessage('N�o h� nenhum calend�rio definido como Atual...');
      end;
    end;
  except end;

  frmItensCalendario.AtualizaTela_Itens('Padr�o');

  frmItensCalendario.ShowModal;
  frmCalendarios.Close;
end;

end.
