object frmTransferenciasDRH: TfrmTransferenciasDRH
  Left = 797
  Top = 339
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Transfer�ncias]'
  ClientHeight = 175
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 266
    Height = 175
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 0
    object btnVer: TSpeedButton
      Left = 70
      Top = 130
      Width = 121
      Height = 33
      Caption = 'Ver Rela��o'
      Flat = True
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDDD7777777777DDDDD0000DDDD
        000000000007DDDD0000DDD07878787870707DDD0000DD0000000000000707DD
        0000DD0F8F8F8AAA8F0007DD0000DD08F8F8F999F80707DD0000DD0000000000
        0008707D0000DD08F8F8F8F8F080807D0000DDD0000000000F08007D0000DDDD
        0BFFFBFFF0F080DD0000DDDDD0F00000F0000DDD0000DDDDD0FBFFFBFF0DDDDD
        0000DDDDDD0F00000F0DDDDD0000DDDDDD0FFBFFFBF0DDDD0000DDDDDDD00000
        0000DDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDD
        DDDDDDDDDDDDDDDD0000}
      OnClick = btnVerClick
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 8
      Width = 121
      Height = 48
      Caption = 'Data Inicial'
      TabOrder = 0
      object meData1: TMaskEdit
        Left = 9
        Top = 16
        Width = 79
        Height = 21
        EditMask = '99/99/9999'
        MaxLength = 10
        TabOrder = 0
        Text = '  /  /    '
        OnExit = meData1Exit
      end
    end
    object GroupBox2: TGroupBox
      Left = 134
      Top = 8
      Width = 121
      Height = 48
      Caption = 'Data Final'
      TabOrder = 1
      object meData2: TMaskEdit
        Left = 9
        Top = 16
        Width = 79
        Height = 21
        EditMask = '99/99/9999'
        MaxLength = 10
        TabOrder = 0
        Text = '  /  /    '
        OnExit = meData2Exit
      end
    end
    object rgTipoFolha: TRadioGroup
      Left = 8
      Top = 56
      Width = 247
      Height = 68
      Caption = '[Selecionar "tipo de folha"]'
      Items.Strings = (
        '002 - Crian�a'
        '003 - Bolsista'
        '006 - Celetrista'
        'Todos')
      TabOrder = 2
    end
  end
end
