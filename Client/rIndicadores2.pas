unit rIndicadores2;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls;

type
  TrelIndicadores2 = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRShape3: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape4: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape5: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape8: TQRShape;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRShape9: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    lbTtAtFamilia: TQRLabel;
    lbTtAtCrianca: TQRLabel;
    lbTtAtAdol: TQRLabel;
    lbTtAtProjetoVida: TQRLabel;
    lbTtAtProf: TQRLabel;
    lbTtAtFam_VisitaDomiciliar: TQRLabel;
    lbTtAtFam_AtIndividual: TQRLabel;
    lbTtAtFam_Anamnese: TQRLabel;
    lbTtAtFam_AbordagemGrupal: TQRLabel;
    lbTtAtFam_Integracao: TQRLabel;
    lbTtAtCriAdol_AtendimentoIndividual: TQRLabel;
    lbTtCriAdol_AbordagemGrupal: TQRLabel;
    lbTtAtCriAdol_Treinamento: TQRLabel;
    lbtTAtProf_AtendimentoComunidade: TQRLabel;
    lbTtAtProf_RelatoriosDiversos: TQRLabel;
    lbTtAtProf_CursosCapacitacao: TQRLabel;
    lbTtAtProf_SupervisaoEstagio: TQRLabel;
    lbTtAtProf_ReunioesTecnicas: TQRLabel;
    lbTtAtProf_DiscussaoCasos: TQRLabel;
    lbTtAtProf_ParticipaAtividades: TQRLabel;
    lbTtAtProf_VisitasDiversas: TQRLabel;
    lbTtAtProjetoVida_VisitaDomiciliar: TQRLabel;
    lbTtAtProjetoVida_AbordagemGrupal: TQRLabel;
    lbTtAtProjetoVida_AtendimentoIndividual: TQRLabel;
    lbTtAtFam_ReuniaoInf: TQRLabel;
    QRLabel50: TQRLabel;
  private

  public

  end;

var
  relIndicadores2: TrelIndicadores2;

implementation

uses uDM;

{$R *.DFM}

end.
