object frmCadastrarDiasUteis: TfrmCadastrarDiasUteis
  Left = 581
  Top = 294
  Width = 425
  Height = 386
  Caption = '[Sistema Deca] - Dias �teis'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 272
    Top = 175
    Width = 137
    Height = 41
    Caption = 'Novo'
  end
  object SpeedButton2: TSpeedButton
    Left = 272
    Top = 221
    Width = 137
    Height = 41
    Caption = 'Salvar'
  end
  object SpeedButton3: TSpeedButton
    Left = 272
    Top = 312
    Width = 137
    Height = 41
    Caption = 'Sair'
  end
  object SpeedButton4: TSpeedButton
    Left = 272
    Top = 266
    Width = 137
    Height = 41
    Caption = 'Alterar'
  end
  object GroupBox1: TGroupBox
    Left = 2
    Top = 2
    Width = 266
    Height = 51
    Caption = '[Calend�rio Atual]'
    TabOrder = 0
    object edCalendarioAtual: TEdit
      Left = 9
      Top = 16
      Width = 248
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 3
    Top = 55
    Width = 265
    Height = 298
    Caption = '[Dias �teis cadastrados no Calend�rio Atual]'
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 7
      Top = 17
      Width = 249
      Height = 273
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = '[M�s]'
          Width = 100
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = '[Ano]'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = '[Dias �teis]'
          Visible = True
        end>
    end
  end
end
