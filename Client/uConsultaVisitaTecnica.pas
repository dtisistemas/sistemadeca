unit uConsultaVisitaTecnica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, Db, Grids, DBGrids, DBCtrls;

type
  TfrmConsultaVisitaTecnica = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    btnSair: TBitBtn;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    DBMemo1: TDBMemo;
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaVisitaTecnica: TfrmConsultaVisitaTecnica;

implementation

uses uDM, uPrincipal, uFichaPesquisa;

{$R *.DFM}

procedure TfrmConsultaVisitaTecnica.txtMatriculaExit(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carrega os dados das Visitas T�cnicas para a c�a/adol informado
        try
          with dmDeca.cdsSel_Dados_Visita_Tecnica do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
            Open;

            DBGrid1.Refresh;

          end;
        except end;

      end;
    except end;
  end;
end;

procedure TfrmConsultaVisitaTecnica.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carrega os dados das Visitas T�cnicas para a c�a/adol informado
        try
          with dmDeca.cdsSel_Dados_Visita_Tecnica do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
            Open;

            DBGrid1.Refresh;

          end;
        except end;

      end;
    except end;
  end;
end;

procedure TfrmConsultaVisitaTecnica.btnSairClick(Sender: TObject);
begin
  frmConsultaVisitaTecnica.Close;
end;

end.
