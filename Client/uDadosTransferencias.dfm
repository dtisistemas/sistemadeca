object frmDadosTransferencias: TfrmDadosTransferencias
  Left = 352
  Top = 249
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - Consulta Transfer�ncias'
  ClientHeight = 139
  ClientWidth = 332
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 0
    Width = 324
    Height = 137
    Caption = 'Selecione per�odo para consulta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 47
      Top = 28
      Width = 82
      Height = 13
      Caption = 'DATA INICIAL'
    end
    object Label2: TLabel
      Left = 55
      Top = 53
      Width = 73
      Height = 13
      Caption = 'DATA FINAL'
    end
    object txtDataInicial: TMaskEdit
      Left = 135
      Top = 24
      Width = 90
      Height = 21
      Hint = 'INFORME DATA INICIAL'
      EditMask = '!99/99/0000;1;'
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      OnExit = txtDataInicialExit
    end
    object txtDataFinal: TMaskEdit
      Left = 135
      Top = 48
      Width = 90
      Height = 21
      Hint = 'INFORME DATA FINAL'
      EditMask = '!99/99/0000;1;'
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  /  /    '
      OnExit = txtDataFinalExit
    end
    object btnVer: TBitBtn
      Left = 48
      Top = 88
      Width = 108
      Height = 29
      Hint = 'Clique para EXIBIR rela��o de Transfer�ncias por per�odo'
      Caption = '&Ver Rela��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnVerClick
    end
    object btnCancelar: TBitBtn
      Left = 168
      Top = 88
      Width = 108
      Height = 29
      Hint = 'Clique para LIMPAR os campos da tela'
      Caption = '&Limpar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnCancelarClick
    end
  end
end
