unit rSolicitacaoVagaEscolar;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelSolicitacaoVagaEscolar = class(TQuickRep)
    DetailBand1: TQRBand;
    lbUnidade: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    lbNome: TQRLabel;
    lbNascimento: TQRLabel;
    lbRA: TQRLabel;
    lbEndereco: TQRLabel;
    lbBairro: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape1: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel26: TQRLabel;
    QRShape2: TQRShape;
    QRLabel13: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    qrlNomeEscola: TQRLabel;
    qrlNomeSerie: TQRLabel;
    qrlEnderecoEscola: TQRLabel;
    qrlBairroEscola: TQRLabel;
    qrlFoneEscola: TQRLabel;
    qrlCepEscola: TQRLabel;
    qrlDiretoraEscola: TQRLabel;
    qrlIdentificaoSolicitante: TQRLabel;
    QRImage2: TQRImage;
    QRLabel8: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel17: TQRLabel;
    lbUnidade2: TQRLabel;
    QRLabel22: TQRLabel;
    lbNascimento2: TQRLabel;
    QRLabel25: TQRLabel;
    lbNome2: TQRLabel;
    QRLabel28: TQRLabel;
    lbEndereco2: TQRLabel;
    QRLabel30: TQRLabel;
    lbRA2: TQRLabel;
    QRLabel32: TQRLabel;
    qrlNomeEscola2: TQRLabel;
    QRLabel34: TQRLabel;
    qrlNomeSerie2: TQRLabel;
    QRLabel36: TQRLabel;
    qrlEnderecoEscola2: TQRLabel;
    QRLabel38: TQRLabel;
    qrlBairroEscola2: TQRLabel;
    QRLabel40: TQRLabel;
    qrlDiretoraEscola2: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    qrlFoneEscola2: TQRLabel;
    QRLabel45: TQRLabel;
    qrlCepEscola2: TQRLabel;
    QRShape6: TQRShape;
    QRLabel47: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    qrlIdentificaoSolicitante2: TQRLabel;
    lbBairro2: TQRLabel;
    QRShape10: TQRShape;
    QRLabel18: TQRLabel;
    QRImage3: TQRImage;
    QRLabel24: TQRLabel;
    QRLabel27: TQRLabel;
    QRSysData3: TQRSysData;
    QRLabel29: TQRLabel;
    qrlIdentificaoSolicitante3: TQRLabel;
    QRLabel33: TQRLabel;
    lbUnidade3: TQRLabel;
    QRLabel37: TQRLabel;
    lbNascimento3: TQRLabel;
    QRLabel41: TQRLabel;
    lbNome3: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    lbEndereco3: TQRLabel;
    QRLabel50: TQRLabel;
    lbBairro3: TQRLabel;
    QRLabel52: TQRLabel;
    qrlNomeEscola3: TQRLabel;
    QRLabel54: TQRLabel;
    qrlNomeSerie3: TQRLabel;
    QRLabel56: TQRLabel;
    qrlEnderecoEscola3: TQRLabel;
    QRLabel58: TQRLabel;
    qrlBairroEscola3: TQRLabel;
    QRLabel60: TQRLabel;
    qrlDiretoraEscola3: TQRLabel;
    QRLabel62: TQRLabel;
    qrlCepEscola3: TQRLabel;
    QRLabel64: TQRLabel;
    qrlFoneEscola3: TQRLabel;
    lbRA3: TQRLabel;
    QRShape11: TQRShape;
    QRLabel67: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel31: TQRLabel;
    QRImage4: TQRImage;
    QRLabel35: TQRLabel;
    QRLabel39: TQRLabel;
  private

  public

  end;

var
  relSolicitacaoVagaEscolar: TrelSolicitacaoVagaEscolar;

implementation

uses uDM;

{$R *.DFM}

end.
