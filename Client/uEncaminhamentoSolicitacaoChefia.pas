unit uEncaminhamentoSolicitacaoChefia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Grids, DBGrids, ComCtrls, CheckLst, Buttons, ExtCtrls,
  ImgList, Db;

type
  TfrmEncaminhamentoSolicitacaoChefia = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBoxN1: TGroupBox;
    mskDataSolicitacaoN: TMaskEdit;
    GroupBoxN2: TGroupBox;
    cbTipoSolicitacaoN: TComboBox;
    GroupBoxN4: TGroupBox;
    GroupBoxN5: TGroupBox;
    meSolicitacaoN: TMemo;
    lstUnidades: TCheckListBox;
    GroupBox5: TGroupBox;
    dbgSolicitacoes: TDBGrid;
    TabSheet3: TTabSheet;
    GroupBoxA1: TGroupBox;
    mskDataSolicitacaoA: TMaskEdit;
    GroupBoxA4: TGroupBox;
    meSolicitacaoA: TMemo;
    GroupBoxN3: TGroupBox;
    cbPrioridadeN: TComboBox;
    GroupBoxA2: TGroupBox;
    cbTipoSolicitacaoA: TComboBox;
    GroupBoxA3: TGroupBox;
    cbPrioridadeA: TComboBox;
    GroupBoxA5: TGroupBox;
    meDevolutiva: TMemo;
    chkMarcarTodas: TCheckBox;
    Panel3: TPanel;
    btnSair: TSpeedButton;
    btnNovo: TSpeedButton;
    btnGravar: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnConsultar: TSpeedButton;
    lbTotalUnidades: TLabel;
    ImageList1: TImageList;
    dsSolicitacoes: TDataSource;
    procedure chkMarcarTodasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNovoClick(Sender: TObject);
    procedure dbgSolicitacoesDblClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure lstUnidadesClick(Sender: TObject);
    procedure dbgSolicitacoesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnGravarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEncaminhamentoSolicitacaoChefia: TfrmEncaminhamentoSolicitacaoChefia;
  vListaUnidade1, vListaUnidade2 : TStringList;
  vListaTipoSolicitacao1, vListaTipoSolicitacao2 : TStringList;
  vvMUDA_ABA : Boolean;
  i, x, ttUnidades, vIndUnidade : Integer;
  mmNOM_UNIDADE : String;
  vIndSolicitacao : Integer;

implementation

uses uDM, uUtil, uPrincipal, uFichaCrianca;

{$R *.DFM}

procedure TfrmEncaminhamentoSolicitacaoChefia.chkMarcarTodasClick(
  Sender: TObject);
begin
  ttUnidades := 0;
  if chkMarcarTodas.Checked then
  begin

    for i:= 0 to lstUnidades.Items.Count - 1 do
    begin
      lstUnidades.Checked[i] := True;
      ttUnidades := ttUnidades + 1
    end;
    lbTotalUnidades.Caption := 'Voc� selecionou ' + IntToStr(ttUnidades) + ' unidade(s).';
  end
  else
  begin
    for i:= 0 to lstUnidades.Items.Count - 1 do
      lstUnidades.Checked[i] := False;
    lbTotalUnidades.Caption := 'Voc� n�o selecionou unidade(s).';
  end;



end;

procedure TfrmEncaminhamentoSolicitacaoChefia.FormShow(Sender: TObject);
begin
  // habilita os tabsheet conforme o perfil
  case vvIND_PERFIL of
    1: begin //Administrador
        frmEncaminhamentoSolicitacaoChefia.PageControl1.Pages[0].TabVisible := True;  //Solicita��es Encaminhadas
        frmEncaminhamentoSolicitacaoChefia.PageControl1.Pages[1].TabVisible := True;  //Nova Solicita��o
        frmEncaminhamentoSolicitacaoChefia.PageControl1.Pages[2].TabVisible := False;  //Dados da Solicita��o
      end;

    2,3 : begin
        frmEncaminhamentoSolicitacaoChefia.PageControl1.Pages[0].TabVisible := True;  //Solicita��es Encaminhadas
        frmEncaminhamentoSolicitacaoChefia.PageControl1.Pages[1].TabVisible := False;  //Nova Solicita��o
        frmEncaminhamentoSolicitacaoChefia.PageControl1.Pages[2].TabVisible := True;  //Dados da Solicita��o
      end;
    end;                                                                            

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();
  vListaTipoSolicitacao1 := TStringList.Create();
  vListaTipoSolicitacao2 := TStringList.Create();
  AtualizaCamposBotoes('P');
  //PageControl1.ActivePageIndex := 0;
  //Carregar os TIPOS DE SOLICITA��O

  try
  with dmDeca.cdsSel_TipoSolicitacao do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_tipo_solicitacao').Value := Null;
    //Verificar por perfil...
    //Se perfil 2 ou 3, abre somente as ativas...
    //if (vvIND_PERFIL=2) or (vvIND_PERFIL=3) then
    //Params.ParamByName ('@pe_flg_ativa').Value := 0 //Somente as ATIVAS
    //else //Se perfil 7...
    Params.ParamByName ('@pe_flg_ativa').Value := Null;
    Open;

    while not eof do
    begin
      cbTipoSolicitacaoN.Items.Add (FieldByName ('dsc_tipo_solicitacao').Value);
      cbTipoSolicitacaoA.Items.Add (FieldByName ('dsc_tipo_solicitacao').Value);
      vListaTipoSolicitacao1.Add (IntToStr(FieldByName ('cod_id_tipo_solicitacao').Value));
      vListaTipoSolicitacao2.Add (FieldByName ('dsc_tipo_solicitacao').Value);
      Next;
    end;
  end;
  except end;

  //Carregar TODAS as solicita��es gravadas, independente da Situa��o
  //Ordenadas pela data da solicita��o...
  with dmDeca.cdsSel_Solicitacao do
  begin
    Close;

    Open;
    dbgSolicitacoes.Refresh;
  end;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  if vvMUDA_ABA then
    AllowChange := True
  else
    AllowChange := False;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.btnNovoClick(
  Sender: TObject);
begin
  vvMUDA_ABA := True;
  //PageControl1.ActivePageIndex := 1;
  AtualizaCamposBotoes('N');
  mskDataSolicitacaoN.Text := DateToStr(Date());
  vvMUDA_ABA := False;

  //Carregar todas as unidades no CheckListBox lstUnidades
  lstUnidades.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin
        vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
        vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        lstUnidades.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.dbgSolicitacoesDblClick(
  Sender: TObject);
begin
  //Verificar se o perfil pode realizar altera��es...
  //Diretor n�o faz altera��o, apenas GESTOR ou ASSISTENTE SOCIAL...

  //if (dmDeca.cdsSel_Solicitacao.RecordCount > 0) then
  //begin
    if (vvIND_PERFIL = 7) or (vvIND_PERFIL = 19) then
    begin
      Application.MessageBox ('O seu perfil n�o pode fazer altera��es ' + #13+#10 +
                              'nos dados de solicita��es j� realizadas. ',
                              '[Sistema Deca] - Acesso negado!!!',
                              MB_OK + MB_ICONERROR);
    end
    else
    if (vvIND_PERFIL = 2) or (vvIND_PERFIL = 3) then
    begin
      vvMUDA_ABA := True;
      AtualizaCamposBotoes('A');

      //Repassa os valores para os campos para devolutiva da Solicita��o...
      mskDataSolicitacaoA.Text := dmDeca.cdsSel_Solicitacao.FieldByName ('DatSolicitacao').Value;
      vListaTipoSolicitacao2.Find(dmDeca.cdsSel_Solicitacao.FieldByName ('dsc_tipo_solicitacao').Value, vIndSolicitacao);
      cbTipoSolicitacaoA.ItemIndex := vIndSolicitacao;
      cbPrioridadeA.ItemIndex := dmDeca.cdsSel_Solicitacao.FieldByName ('ind_prioridade').Value;
      meSolicitacaoA.Text := dmDeca.cdsSel_Solicitacao.FieldByName ('dsc_solicitacao').Value;
      GroupBoxA1.Enabled := False;
      GroupBoxA2.Enabled := False;
      GroupBoxA3.Enabled := False;
      GroupBoxA4.Enabled := False;
      GroupBoxA5.Enabled := True;
    end;
  //else
  //begin
  //  Application.MessageBox ('Aten��o!!!' + #13+#10 +
  //                          'N�o existem registros para REGISTRAR DEVOLUTIVA.',
  //                          '[Sistema Deca] - Registros n�o encontrados...',
  //                          MB_OK + MB_ICONEXCLAMATION);
  //end;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.FormCanResize(
  Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
begin
  Resize := False;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.AtualizaCamposBotoes(
  Modo: String);
begin
  if Modo = 'P' then //Apresenta��o de bot�es padr�o
  begin
    lbTotalUnidades.Caption := '';
    PageControl1.ActivePageIndex := 0;
    vvMUDA_ABA := False;

    //Habilita/Desabilita bot�es
    //Barra de ferramentas apresenta sempre os mesmos bot�es
    //Independente da Aba...

    if (vvIND_PERFIL = 2) or (vvIND_PERFIL = 3) then
      btnNovo.Enabled := False
    else btnNovo.Enabled := True;
    btnGravar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnConsultar.Enabled := True;
    btnSair.Enabled := True;

    //Limpar TODOS os campos do formul�rio
    //Aba "Nova Solicita��o"
    mskDataSolicitacaoN.Clear;
    cbTipoSolicitacaoN.ItemIndex := -1;
    cbPrioridadeN.ItemIndex := -1;
    lstUnidades.Clear;
    meSolicitacaoN.Clear;
    chkMarcarTodas.Checked := False;

    GroupBoxN1.Enabled := False;
    GroupBoxN2.Enabled := False;
    GroupBoxN3.Enabled := False;
    GroupBoxN4.Enabled := False;
    GroupBoxN5.Enabled := False;

    //Aba "Dados da Solicita��o"
    mskDataSolicitacaoA.Clear;
    cbTipoSolicitacaoA.ItemIndex := -1;
    cbPrioridadeA.ItemIndex := -1;
    meSolicitacaoA.Clear;
    meDevolutiva.Clear;

    GroupBoxA1.Enabled := False;
    GroupBoxA2.Enabled := False;
    GroupBoxA3.Enabled := False;
    GroupBoxA4.Enabled := False;
    GroupBoxA5.Enabled := False;

  end

  else if Modo = 'N' then
  begin
    lbTotalUnidades.Caption := 'Voc� n�o selecionou unidade(s).';
    vvMUDA_ABA := True;
    PageControl1.ActivePageIndex := 1;

    //Habilita ou Desabilita bot�es
    //Barra de ferramentas apresenta sempre os mesmos bot�es
    //Independente da Aba...

    btnNovo.Enabled := False;
    btnGravar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnConsultar.Enabled := False;
    btnSair.Enabled := False;

    //Limpar TODOS os campos do formul�rio
    //Aba "Nova Solicita��o"
    mskDataSolicitacaoN.Clear;
    cbTipoSolicitacaoN.ItemIndex := -1;
    cbPrioridadeN.ItemIndex := -1;
    lstUnidades.Clear;
    meSolicitacaoN.Clear;
    chkMarcarTodas.Checked := False;

    GroupBoxN1.Enabled := True;
    GroupBoxN2.Enabled := True;
    GroupBoxN3.Enabled := True;
    GroupBoxN4.Enabled := True;
    GroupBoxN5.Enabled := True;

  end
  else if Modo = 'A' then
  begin
    lbTotalUnidades.Caption := '';
    vvMUDA_ABA := False;
    PageControl1.ActivePageIndex := 2;

    //Habilita ou Desabilita bot�es
    //Barra de ferramentas apresenta sempre os mesmos bot�es
    //Independente da Aba...
    btnNovo.Enabled := False;
    btnGravar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnConsultar.Enabled := False;
    btnSair.Enabled := False;

    //Aba "Dados da Solicita��o"
    mskDataSolicitacaoA.Clear;
    cbTipoSolicitacaoA.ItemIndex := -1;
    cbPrioridadeA.ItemIndex := -1;
    meSolicitacaoA.Clear;
    meDevolutiva.Clear;

    GroupBoxA1.Enabled := True;
    GroupBoxA2.Enabled := True;
    GroupBoxA3.Enabled := True;
    GroupBoxA4.Enabled := True;
    GroupBoxA5.Enabled := True;

  end;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.btnCancelarClick(
  Sender: TObject);
begin
  vvMUDA_ABA := True;
  AtualizaCamposBotoes('P');
  vvMUDA_ABA := False;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.btnSairClick(
  Sender: TObject);
begin
  frmEncaminhamentoSolicitacaoChefia.Close;
end;

procedure TfrmEncaminhamentoSolicitacaoChefia.lstUnidadesClick(
  Sender: TObject);
begin
  ttUnidades := 0;
  for x:=0 to lstUnidades.Items.Count - 1 do
  begin
    if lstUnidades.Checked[x] = True then
      ttUnidades := ttUnidades + 1;
  end;

  lbTotalUnidades.Caption := 'Voc� selecionou ' + IntToStr(ttUnidades) + ' unidade(s).';

end;

procedure TfrmEncaminhamentoSolicitacaoChefia.dbgSolicitacoesDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var icon : TBitMap;
begin

  //Desenha icones de situacao no grid
  icon := TBitmap.Create();
  if (Column.FieldName = 'vSituacao') then
  begin
    with dbgSolicitacoes.Canvas do
    begin
      Brush.Color := clWhite;
      FillRect(Rect);
      if (dmDeca.cdsSel_Solicitacao.FieldByName('flg_situacao').Value) = 0 then
        ImageList1.GetBitMap(1,Icon)
      else if (dmDeca.cdsSel_Solicitacao.FieldByName('flg_situacao').Value) = 1 then
        ImageList1.GetBitMap(2, Icon)
      else if (dmDeca.cdsSel_Solicitacao.FieldByName('flg_situacao').Value) = 2 then
        ImageList1.GetBitMap(3, Icon);
      Draw(round((Rect.Left + Rect.Right - Icon.Width) / 2), Rect.Top, Icon);
    end;
  end;

  //Muda a cor da linha do grid de acordo com a prioridade...
  //URGENTE = 0
  if (dmDeca.cdsSel_Solicitacao.FieldByName('ind_prioridade').AsInteger = 0) then
  begin
    dbgSolicitacoes.Canvas.Font.Color:= clWhite;
    dbgSolicitacoes.Canvas.Brush.Color:= clRed;
  end
  else
  //ALTA = 1
  if (dmDeca.cdsSel_Solicitacao.FieldByName('ind_prioridade').AsInteger = 1) then
  begin
    dbgSolicitacoes.Canvas.Font.Color:= clWhite;
    dbgSolicitacoes.Canvas.Brush.Color:= clBlue;
  end
  else
  //NORMAL = 2
  if (dmDeca.cdsSel_Solicitacao.FieldByName('ind_prioridade').AsInteger = 2) then
  begin
    dbgSolicitacoes.Canvas.Font.Color:= clWhite;
    dbgSolicitacoes.Canvas.Brush.Color:= clGreen;
  end
  else
  //BAIXA = 0
  if (dmDeca.cdsSel_Solicitacao.FieldByName('ind_prioridade').AsInteger = 0) then
  begin
    dbgSolicitacoes.Canvas.Font.Color:= clWhite;
    dbgSolicitacoes.Canvas.Brush.Color:= clFuchsia;
  end;

end;

procedure TfrmEncaminhamentoSolicitacaoChefia.btnGravarClick(
  Sender: TObject);
begin
  if Application.MessageBox ('Deseja gravar os dados da solicita��o informadas na tela?',
                             '[Sistema Deca] - Gerar Solicita��o...',
                             MB_YESNO + MB_ICONQUESTION) = id_yes then
  begin
    //Fazer a inclus�o para cada unidade selecionada no CheckListBox lstUnidades
    for x:= 0 to lstUnidades.Items.Count - 1 do
    begin
      //Verifica se o item de lstUnidades est� "checado"
      //Se estiver, pesquisar em vListaUnidade2 o c�digo da UNIDADE...
      if lstUnidades.Checked[x] = True then
      begin
        mmNOM_UNIDADE := lstUnidades.Items.Strings[x];
        //Pesquisa o codigo da unidade selecionada
        dmDeca.cdsSel_Unidade.Locate('nom_unidade', mmNOM_UNIDADE, []);
        vIndUnidade := dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value;
        with dmDeca.cdsInc_Solicitacao do
        begin
          Close;
          Params.ParamByName ('@pe_dat_solicitacao').Value := GetValue(mskDataSolicitacaoN, vtDate);
          Params.ParamByName ('@pe_cod_id_tipo_solicitacao').Value := StrToInt(vListaTipoSolicitacao1.Strings[cbTipoSolicitacaoN.ItemIndex]);
          Params.ParamByName ('@pe_ind_prioridade').Value := cbPrioridadeN.ItemIndex;
          Params.ParamByName ('@pe_dsc_solicitacao').Value := GetValue(meSolicitacaoN.Text);
          Params.ParamByName ('@pe_flg_situacao').Value := 0; //ENVIADA
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Params.ParamByName ('@pe_cod_unidade').Value := vIndUnidade;
          Execute;
        end;
      end;
    end;
    Application.MessageBox ('A(s) solicita��o(�es) foram geradas com sucesso ' + #13+#10 +
                            'de acordo com as informa��es apresentadas no formul�rio.',
                            '[Sistema Deca] - Registro de Solicita��o',
                            MB_OK + MB_ICONINFORMATION);

    //Atualizar os dados do grid...
    try
    with dmDeca.cdsSel_Solicitacao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_ind_prioridade').Value := Null;
      Params.ParamByName ('@pe_cod_id_tipo_solicitacao').Value := Null;
      Params.ParamByName ('@pe_flg_situacao').Value := Null;

      //if (vvIND_PERFIL = 2) or (vvIND_PERFIL = 3) then
      //  Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE
      //else
      Params.ParamByName ('@pe_cod_unidade').Value := Null;

      Open;
      dbgSolicitacoes.Refresh;
    end;
    except end;

    AtualizaCamposBotoes('P');

  end;
end;
procedure TfrmEncaminhamentoSolicitacaoChefia.btnAlterarClick(
  Sender: TObject);
begin
  try
  if Application.MessageBox ('Deseja REGISTRAR DEVOLUTIVA para essa Solicita��o ?',
                             '[Sistema Deca] - Registrar Devolutiva',
                             MB_YESNO + MB_ICONQUESTION) = id_yes then
  begin
    with dmDeca.cdsAlt_Solicitacao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := dmDeca.cdsSel_Solicitacao.FieldByName('cod_id_solicitacao').Value;
      Params.ParamByName ('@pe_flg_situacao').Value := 2;
      Params.ParamByName ('@pe_dsc_devolutiva').Value := GetValue(meDevolutiva.Text);
      Execute;
    end;

    //Atualiza os dados do grid
    with dmDeca.cdsSel_Solicitacao do
    begin
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_ind_prioridade').Value := Null;
      Params.ParamByName ('@pe_cod_id_tipo_solicitacao').Value := Null;
      Params.ParamByName ('@pe_flg_situacao').Value := 0;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;
      dbgSolicitacoes.Refresh;
      AtualizaCamposBotoes('P');
    end;
  end;
  except end;
  //Verificar se ainda existem pend�ncias...
  //Caso negativo, fecha a janela e carrega novamente os menus tradicionais de acordo com o perfil...
  {with dmDeca.cdsSel_Solicitacao do
  begin
    Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
    Params.ParamByName ('@pe_ind_prioridade').Value := Null;
    Params.ParamByName ('@pe_cod_id_tipo_solicitacao').Value := Null;
    Params.ParamByName ('@pe_flg_situacao').Value := 0;
    Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
    Open;
    dbgSolicitacoes.Refresh;}

    if (dmDeca.cdsSel_Solicitacao.RecordCount > 0) then
    begin
      frmPrincipal.Menu := frmPrincipal.mmPrincipal;
      frmPrincipal.imgPendencia.Visible := False;
      frmPrincipal.lbMensagemPendencias.Visible := False;
      frmEncaminhamentoSolicitacaoChefia.Close;
    end;
  //end;
end;

end.


