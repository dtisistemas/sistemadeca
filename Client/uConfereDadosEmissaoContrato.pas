unit uConfereDadosEmissaoContrato;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, Buttons, ComCtrls, ExtCtrls, Mask;

type
  TfrmConfereDadosEmissaoContrato = class(TForm)
    Panel17: TPanel;
    Label39: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    btnEmitirContrato: TSpeedButton;
    btnNovaAdmissao: TSpeedButton;
    Label7: TLabel;
    dbgAdmissoes: TDBGrid;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel8: TPanel;
    btnAlterar: TSpeedButton;
    btnInserir: TSpeedButton;
    btnCancelar: TSpeedButton;
    Label6: TLabel;
    Label1: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    ComboBox1: TComboBox;
    Label5: TLabel;
    Edit4: TEdit;
    Label8: TLabel;
    Edit5: TEdit;
    Label9: TLabel;
    Edit6: TEdit;
    Label10: TLabel;
    MaskEdit1: TMaskEdit;
    Label11: TLabel;
    Edit7: TEdit;
    Label12: TLabel;
    ComboBox2: TComboBox;
    Label13: TLabel;
    MaskEdit2: TMaskEdit;
    Label14: TLabel;
    MaskEdit3: TMaskEdit;
    Label15: TLabel;
    ComboBox3: TComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfereDadosEmissaoContrato: TfrmConfereDadosEmissaoContrato;

implementation

{$R *.DFM}

end.
