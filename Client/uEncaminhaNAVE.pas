unit uEncaminhaNAVE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, jpeg, ExtCtrls, Buttons;

type
  TfrmEncaminhaNAVE = class(TForm)
    Image1: TImage;
    Bevel1: TBevel;
    Panel1: TPanel;
    Panel2: TPanel;
    btnEncaminhar: TSpeedButton;
    btnSair: TSpeedButton;
    mskMatricula: TMaskEdit;
    Label1: TLabel;
    btnLocalizar: TSpeedButton;
    edNome: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    edUnidade: TEdit;
    GroupBox1: TGroupBox;
    meJustificativaEncaminhamento: TMemo;
    Shape1: TShape;
    Image2: TImage;
    procedure btnEncaminharClick(Sender: TObject);
    procedure btnLocalizarClick(Sender: TObject);
    procedure meJustificativaEncaminhamentoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEncaminhaNAVE: TfrmEncaminhaNAVE;

implementation

uses uFichaPesquisa, uDM, uPrincipal;

{$R *.DFM}

procedure TfrmEncaminhaNAVE.btnEncaminharClick(Sender: TObject);
begin
  //Ao fazer a "inclus�o" do aluno no projeto Nave, alterar o status do Cadastro -> "flg_defasagem_aprendizagem"
  btnEncaminhar.Enabled := False;
end;

procedure TfrmEncaminhaNAVE.btnLocalizarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;
  edNome.Text       := dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName ('nom_nome').Value;
  mskMatricula.Text := dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName ('cod_matricula').Value;
  edUnidade.Text    := dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName ('nom_unidade').Value;

  if vvCOD_MATRICULA_PESQUISA = '' then
  begin
    //frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);
    edNome.Text := '';
    mskMatricula.Clear;
  end;


  if dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName ('flg_defasagem_aprendizagem').Value = 1 then
  begin
    ShowMessage ('O aluno j� foi encaminhado para o NAVE. Verifique as informa��es !!!');
  end
  else
  begin
    meJustificativaEncaminhamento.Clear;
    meJustificativaEncaminhamento.SetFocus;
  end;

end;

procedure TfrmEncaminhaNAVE.meJustificativaEncaminhamentoExit(
  Sender: TObject);
begin
  if (Length(meJustificativaEncaminhamento.Text) = 0 ) then
  begin
    ShowMessage ('O campo Justificativa deve ser preenchido corretamente...');
    meJustificativaEncaminhamento.SetFocus;
  end
  else
  begin
    btnEncaminhar.Enabled := True;
  end;
end;

end.
