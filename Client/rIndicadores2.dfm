�
 TRELINDICADORES2 0N�  TPF0TrelIndicadores2relIndicadores2Left Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	ReportTitle.Dados Estat�sticos do Servi�o Social - [Verso]
SnapToGrid	UnitsMMZoomd TQRBandPageHeaderBand1Left&Top&Width�Height6Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      �
@������v�	@ BandTyperbDetail TQRShapeQRShape5LeftTopWidth�HeightSFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@       �@       �@������z�	@ ShapeqrsRectangle  TQRLabel	QRLabel17Left TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUUU�@UUUUUUM�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTipos de AtendimentosColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel18Left Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUUU�@TUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionFam�lia: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel19Left Top8Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@������*�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption#Profissionais: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel20Left� Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUu�@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCrian�a: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel21Left�Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@������*�	@UUUUUUU�@      �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption!Adolescente: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel22Left Top8Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@������*�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption.Projeto de Vida/Fam�lias: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape6LeftTop\Width�Height9Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      Ж@       �@������j�@������z�	@ ShapeqrsRectangle  TQRShapeQRShape7LeftTop� Width�Height[Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesTUUUUU��@       �@VUUUUU�@������z�	@ ShapeqrsRectangle  TQRLabel	QRLabel23Left TopaWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@������R�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption"Atendimentos Individuais - Fam�liaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel24Left TopyWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@�������@������J�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption'Visita Domiciliar: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel25Left� TopyWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      ��@�������@      Ț@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption,Atendimento Individual: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel26Left�Top{Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      ��	@      ��@      (�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionAnamnese: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel27Left Top� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@��������@      �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption Atendimentos Coletivos - Fam�liaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel28Left Top� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU��@������6�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption&Abordagem Grupal: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel29Left Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU��@UUUUUUE�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption4Integra��o Fam�lia/Institui��o: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel30Left0Top� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUU�@UUUUUU��@      ؒ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption)Reuni�o Informativa: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape8LeftTop� Width�Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@       �@UUUUUUY�@������z�	@ ShapeqrsRectangle  TQRLabel	QRLabel31Left TopWidth	HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@      ��@UUUUUUI�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption,Atendimentos Individuais Crian�a/AdolescenteColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel32Left Top!Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU)�@      Ț@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption,Atendimento Individual: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape9LeftTopEWidth�Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@       �@UUUUUU��@������z�	@ ShapeqrsRectangle  TQRLabel	QRLabel33Left TopKWidth HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@VUUUUU��@UUUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption*Atendimentos Coletivos Crian�a/AdolescenteColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel34Left TopiWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU��@������6�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption&Abordagem Grupal: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel35LeftHTopiWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUU��@UUUUUU��@UUUUUU9�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption3Treinamento: Conv�nio/Est�gio: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShape	QRShape10LeftTop�Width�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@������L�	@������z�	@ ShapeqrsRectangle  TQRShape	QRShape11LeftTopVWidth�HeightdFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������J�@       �@��������	@������z�	@ ShapeqrsRectangle  TQRShape	QRShape12Left�TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @������D�	@UUUUUU(�
@�������@ ShapeqrsRectangle  TQRLabel	QRLabel36Left�TopWidthlHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUi�	@������N�
@      ��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaptionAssistente SocialColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabel	QRLabel37Left Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU��	@������Z�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption Atendimento T�cnico ProfissionalColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel38Left Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@������4�	@UUUUUUy�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption)Atend. a Comunidade: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel39Left Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@      ʙ	@������2�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption)Relat�rios Diversos: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel40Left Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU_�	@      Ĝ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption+Cursos de Capacita��o: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel41Left TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@��������	@      ��	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionLParticipa��o em Atividades extra projeto/bloco/unidade: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel42Left Top1Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@      ��	@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption&Visitas diversas: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel43LeftxTop�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@TUUUUU��@������4�	@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption2Superv. de est�gio S. Social: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel44Left�Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUU��	@      ʙ	@      ܐ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption'Reuni�es T�cnicas: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel45Left�Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUU��	@UUUUUU_�	@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption(Discuss�o de Casos: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel46Left Top^Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@      l�	@������6�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption$Atendimentos Projeto de Vida/Fam�liaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel47Left TopyWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@      Z�	@������J�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption'Visita Domiciliar: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel48Left Top�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU�@UUUUUU��	@������6�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption&Abordagem Grupal: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel49LeftXTopyWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@��������@      Z�	@      Ț@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption,Atendimento Individual: ____________________ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFamiliaLeftlTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ��@VUUUUU��@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtCriancaLeft6TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@VUUUUU�@VUUUUU��@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lbTtAtAdolLeft&TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@�������	@VUUUUU��@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProjetoVidaLeft�Top.WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      �	@������j�@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lbTtAtProfLeft� Top.WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ��@������j�@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFam_VisitaDomiciliarLeft� TopoWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@UUUUUU�@      ؒ@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFam_AtIndividualLeft�TopoWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ��	@      ؒ@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFam_AnamneseLeftXTopoWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      p�	@      ؒ@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFam_AbordagemGrupalLeft� Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      H�@��������@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFam_ReuniaoInfLeft�Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ؒ	@��������@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtFam_IntegracaoLeft� Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@������.�@      �@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel#lbTtAtCriAdol_AtendimentoIndividualLeft� TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      �@      ��@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtCriAdol_AbordagemGrupalLeft� Top_WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@     @�@      ,�@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtCriAdol_TreinamentoLeftTop_WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@��������	@      ,�@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel lbtTAtProf_AtendimentoComunidadeLeft� Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@UUUUUU��@      �	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_RelatoriosDiversosLeft� Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      8�@UUUUUU{�	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_CursosCapacitacaoLeft� Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@������b�@�������	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_SupervisaoEstagioLeft+Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ��	@      �	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_ReunioesTecnicasLeft+Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ��	@UUUUUU{�	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_DiscussaoCasosLeft+Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      ��	@�������	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_ParticipaAtividadesLeftSTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@������;�@      ��	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellbTtAtProf_VisitasDiversasLeft� Top'WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      x�@UUUUUU;�	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel"lbTtAtProjetoVida_VisitaDomiciliarLeft� TopoWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      x�@UUUUUU�	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel!lbTtAtProjetoVida_AbordagemGrupalLeft� Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@      8�@��������	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel'lbTtAtProjetoVida_AtendimentoIndividualLeft�TopoWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     @�@UUUUUU_�	@UUUUUU�	@������*�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRBandPageFooterBand1Left&Top\Width�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      Ԕ@������v�	@ BandTyperbPageFooter TQRLabelQRLabel1LeftTopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@��������@      ��	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionhTipos de Atendimentos: F(fam�lia)  C(Crian�a)  A(Adolescente P(Profissional)  V(Projeto de Vida/Fam�lia)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape1LeftTopWidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUUU�@     @�@��������	@ ShapeqrsRectangle  TQRLabelQRLabel2LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@       �@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption&Classifica��o de Atendimentos: FAM�LIAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel3LeftTop(Width6HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@��������@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionIndividuais:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel4LeftATop(WidthRHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@��������@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption>1(Visita Domiciliar) - 2(Atendimento Individual) - 3(Anamnese)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel5LeftTop8Width0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@������*�@       �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Coletivos:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel6Left9Top8Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      Ж@������*�@      ޏ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionP4(Abordagem Grupal) - 5(Reuni�o Informativa) - 6(Integra��o Fam�lia/Institui��o)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape2LeftTopIWidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUUU�@UUUUUU%�@��������	@ ShapeqrsRectangle  TQRLabelQRLabel7LeftTopLWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption2Classifica��o de Atendimentos: CRIAN�A/ADOLESCENTEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel8LeftTop\Width6HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@������j�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionIndividuais:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel9Left?Top\Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@������j�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption7(Atendimento Indvidual)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel10Left� Top\Width0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUy�@������j�@       �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Coletivos:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel11LeftTop\Width;HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������޹@������j�@     \�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption68(Abordagem Grupal) - 9(Treinamento: Conv�nio/Est�gio)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape3LeftTopnWidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUUU�@UUUUUU��@��������	@ ShapeqrsRectangle  TQRLabel	QRLabel12LeftTopqWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUU}�@UUUUUUY�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption3Classifica��o de Atendimentos: T�cnico ProfissionalColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel13LeftTop� WidthWHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@      ��@UUUUUU�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionj10(Atendimento a Comunidade) - 11(Supervis�o de Est�gio) - 12(Relat�rios Diversos) - 13(Reuni�es T�cnicas)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel14LeftTop� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@      ��@UUUUUU��	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionv14(Cursos de Capacita��o) - 15(Discuss�o de Casos) - 16(Atividades Extra-Projeto/Bloco/Unidade) - 17(Visitas Diversas)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape4LeftTop� WidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUUU�@      X�@��������	@ ShapeqrsRectangle  TQRLabel	QRLabel15LeftTop� Width0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@      P�@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption9Classifica��o de Atendimentos: (PROJETO DE VIDA/FAM�LIAS)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel16LeftTop� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@������z�@������L�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionH18(Visita Domiciliar) - 19(AtendimentoIndividual) - 20(Abordagem Grupal)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel50LeftTop� WidthXHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������@      �@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSFIT.5047.32        REV.04                TAMANHO:A4        Data da revis�o:02/02/04ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize    