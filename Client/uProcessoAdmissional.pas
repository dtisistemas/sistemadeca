unit uProcessoAdmissional;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Mask, Buttons, ExtCtrls;

type
  TfrmProcessoAdmissional = class(TForm)
    GroupBox1: TGroupBox;
    clbAdolescentes: TCheckListBox;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    cbSecao: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    GroupBox3: TGroupBox;
    Memo1: TMemo;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProcessoAdmissional: TfrmProcessoAdmissional;

implementation

{$R *.DFM}

end.
