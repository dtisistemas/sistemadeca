unit uAfastamentoRetorno;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, Grids, DBGrids, ExtCtrls, Db;

type
  TfrmAfastamentoRetorno = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Panel1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtData: TMaskEdit;
    txtNomeResponsavel: TEdit;
    txtTipoDocumento: TEdit;
    txtNumDocumento: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAfastamentoRetorno: TfrmAfastamentoRetorno;

implementation

uses uDM, uPrincipal, uAfastamentoEntrada, uFichaPesquisa, uUtil;

{$R *.DFM}

procedure TfrmAfastamentoRetorno.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmAfastamentoRetorno.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmAfastamentoEntrada.Close;
  end;
end;

procedure TfrmAfastamentoRetorno.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end;
end;

procedure TfrmAfastamentoRetorno.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmAfastamentoRetorno.btnSairClick(Sender: TObject);
begin
  frmAfastamentoRetorno.Close;
end;

procedure TfrmAfastamentoRetorno.AtualizaCamposBotoes(Modo: String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtNomeResponsavel.Clear;
    txtTipoDocumento.Clear;
    txtNumDocumento.Clear;

    txtMatricula.Enabled := False;
    txtData.Enabled := True;
    btnPesquisar.Enabled := True;
    txtNomeResponsavel.Enabled := False;
    txtTipoDocumento.Enabled := False;
    txtNumDocumento.Enabled := False;

    btnConfirmar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnConfirmar.Enabled := True;
    btnCancelar.Enabled := True;

    txtMatricula.Enabled := False;
    txtData.Enabled := True;
    txtNomeResponsavel.Enabled := False;
    txtTipoDocumento.Enabled := False;
    txtNumDocumento.Enabled := False;

  end
end;

procedure TfrmAfastamentoRetorno.btnConfirmarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try
      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      if StatusCadastro(txtMatricula.Text) = 'Afastado' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := Date();
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 3;  //C�digo para AFASTAMENTO - Retorno
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Afastamento - Retorno';
          Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Crian�a/Adolescente retornando do Afastamento nesta data';
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "1" Ativo pois sai do status de afastado
          with dmDeca.cdsAlt_Cadastro_Status do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_ind_status').Value := 1;
              Execute;
          end;    

          //Recuperar o �ltimo bimestre lan�ado para a matr�cula atual
          with dmDeca.cdsSel_UltimoBimestre do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_num_ano').Value       := StrToInt(Copy(DateToStr(Date()),7,4));//dmDeca.cdsSel_AcompEscolar.FieldByName ('num_ano_letivo').Value;
            Open;
          end;

          //Ap�s identificar o �ltimo bimestre, consultar os dados para compara��o e atribui��o do per�odo no cadastro
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value         := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(Copy(DateToStr(Date()),7,4));
            Params.ParamByName ('@pe_num_bimestre').Value          := dmDeca.cdsSel_UltimoBimestreAluno.FieldByName('UltBim').Value;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := null;
            Open;
          end;

          with dmDeca.cdsAlt_PeriodoCadastro do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
            if (dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value = 209) and (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_status').Value = 4) then
            begin
              Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>'
            end
            else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value = 209) then
              Params.ParamByName ('@pe_dsc_periodo').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_periodo').Value
            else if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_status').Value = 4) and (dmDeca.cdsSel_UltimoBimestre.FieldByName ('ind_periodo_escolar').Value = 3) then //3=Integral
              Params.ParamByName ('@pe_dsc_periodo').Value := 'INTEGR/ESC'
            else if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_status').Value = 4) and (dmDeca.cdsSel_UltimoBimestre.FieldByName ('ind_periodo_escolar').Value <> 3) then //3=Integral
            begin
              Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>'
            end else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value <> 'Noite') then  //Acrescentado a pedido do Acompanhamento Escolar em 27/11/2014
            begin
              case dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value of
                0: Params.ParamByName ('@pe_dsc_periodo').Value := 'TARDE';
                1: Params.ParamByName ('@pe_dsc_periodo').Value := 'MANH�';
                //2: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
                3: Params.ParamByName ('@pe_dsc_periodo').Value := 'INTEGR/ESC';
                4: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
              end;
            end; //else Params.ParamByName ('@pe_dsc_periodo').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_periodo').Value;
            //Caso o per�odo da Escola no bimestre atual for Noite, manter o per�odo do Cadastro   -  28/05/2015
            //else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value = 'Noite') then
            //  Params.ParamByName ('@pe_dsc_periodo').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;
            Execute;
          end;

          //Desbloqueia o cart�o de passe escolar do aluno atual
          try
            with dmDeca.cdsAlt_CartaoPasse do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
              Params.ParamByName ('@pe_num_cartao_passe').Value := dmDeca.cdsSel_Cadastro.FieldByName ('num_cartao_passe').Value;
              Params.ParamByName ('@pe_passe_bloqueado').Value  := 0;
              Execute;
            end;
          except
            Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                    'Um erro ocorreu ao tentar BLOQUEAR O CART�O DE PASSE.' + #13+#10 +
                                    'Favor tentar novamente ou reportar ao Administrador do Sistema.',
                                    '[Sistema Deca] - Bloqueio de Passe Escolar',
                                    MB_OK + MB_ICONINFORMATION);
          end;





        end;
        AtualizaCamposBotoes('Padrao');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                 'Esta(e) crian�a/adolescente n�o encontra-se em'+#13+#10+#13+#10+
                 'Afastamento...');
        AtualizaCamposBotoes('Padrao');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'AFr_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                         Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtData.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Afastamento - Retorno');//Descricao do Tipo de Historico
      Writeln (log_file, ' ');
      Writeln (log_file, 'Crian�a/Adolescente retornando do Afastamento nesta data');
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);

    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmAfastamentoRetorno.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName ('@pe_ind_responsavel').Value := 1;
          Open;

          if dmDeca.cdsSel_Cadastro_Familia_Responsavel.Eof then
          begin
            ShowMessage('Aten��o' + #13+#10+#13+#10 +
                        'N�o existe Respons�vel cadastrado para a Crian�a/Adolescente' + #13+#10 +
                        'Favor verificar e tentar novamente... ');

            AtualizaCamposBotoes('Padr�o');
          end
          else
          begin
            txtNomeResponsavel.Text := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName('nom_familiar').AsString;
            txtTipoDocumento.Text := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName('ind_tipo_documento').AsString;
            txtNumDocumento.Text := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName('num_documento').AsString;
            //txtData.Text := DateToStr(Date());
            AtualizaCamposBotoes('Salvar');
          end;
        end;

      end;
    except end;
  end;
end;

procedure TfrmAfastamentoRetorno.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmAfastamentoRetorno.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

end.
