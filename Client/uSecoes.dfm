object frmSecoes: TfrmSecoes
  Left = 265
  Top = 309
  BorderStyle = bsDialog
  Caption = 'Sistema DECA  -  [Cadastro de Se��es]'
  ClientHeight = 358
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 1
    Top = 3
    Width = 632
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 64
      Top = 19
      Width = 43
      Height = 13
      Caption = 'Unidade:'
    end
    object cbUnidade: TComboBox
      Left = 112
      Top = 15
      Width = 273
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnClick = cbUnidadeClick
    end
  end
  object Panel2: TPanel
    Left = 1
    Top = 54
    Width = 632
    Height = 110
    TabOrder = 1
    object Label2: TLabel
      Left = 169
      Top = 10
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nome da Se��o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 53
      Top = 34
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ass. Social:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 46
      Top = 11
      Width = 62
      Height = 13
      Caption = 'N�m. Se��o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 65
      Top = 59
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inclus�o:'
    end
    object Label6: TLabel
      Left = 212
      Top = 58
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Altera��o:'
    end
    object Label7: TLabel
      Left = 62
      Top = 82
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Situa��o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edNumSecao: TEdit
      Left = 110
      Top = 7
      Width = 49
      Height = 21
      Color = clInfoBk
      TabOrder = 0
    end
    object edSecao: TEdit
      Left = 251
      Top = 6
      Width = 284
      Height = 21
      Color = clInfoBk
      TabOrder = 1
    end
    object edAsSocial: TEdit
      Left = 110
      Top = 31
      Width = 284
      Height = 21
      Color = clInfoBk
      TabOrder = 2
    end
    object edInclusao: TEdit
      Left = 110
      Top = 55
      Width = 91
      Height = 21
      Enabled = False
      TabOrder = 3
    end
    object edAlteracao: TEdit
      Left = 262
      Top = 55
      Width = 91
      Height = 21
      Enabled = False
      TabOrder = 4
    end
    object cbSituacao: TComboBox
      Left = 110
      Top = 78
      Width = 145
      Height = 22
      Style = csOwnerDrawFixed
      Color = clInfoBk
      ItemHeight = 16
      TabOrder = 5
      Items.Strings = (
        'Ativa'
        'Inativa')
    end
  end
  object Panel3: TPanel
    Left = 1
    Top = 166
    Width = 632
    Height = 139
    TabOrder = 2
    object dbgSecoes: TDBGrid
      Left = 8
      Top = 9
      Width = 614
      Height = 120
      DataSource = dsSecoes
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dbgSecoesDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_secao'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'N.� Se��o'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dsc_nom_secao'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Title.Caption = 'Se��o'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_asocial'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Title.Caption = 'Ass. Social'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'vStatus'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Situa��o'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 50
          Visible = True
        end>
    end
  end
  object Panel4: TPanel
    Left = 1
    Top = 306
    Width = 633
    Height = 49
    TabOrder = 3
    object btnNovo: TBitBtn
      Left = 291
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para NOVA advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 365
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR a advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 498
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 568
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnAlterar: TBitBtn
      Left = 423
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnAlterarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF7F3F3FBF7F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBF7F3FBFBF9FFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
        3F3FBF9F5FBFBF7F9F3F3F7F3F3FBF7F3FBF9F5FFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBFDF9FBFBF7FBFBF7FBFBF7FBF9F
        5F9F3F3FBF9F5FFFFFFFFFFFFF7FBFBF3F7F9F7FBFBFFFFFFFFFFFFFFFFFFF9F
        3F3FBF9F5FBFDF9FBF7F3F9F3F3FBF7F3FBFBF7F9F3F3FBF9F5F7FBFBF3F7F9F
        7FBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3F3FBF9F5FBF7F3FBFBF9FBF9F
        5F9F3F3FBFBF7FBF7F3F5F9F9F3F7F9FBFDFDFFFFFFFFFFFFF3F5F7FFFFFFFFF
        FFFFFFFFFF9F3F3FBF7F3FFFFFFFFFFFFFBF9F5FBF9F5F9F3F3F3F7F7F3FBFFF
        3F9FDF9FDFDFFFFFFF3F5F7F3F5F7FFFFFFFFFFFFFFFFFFF9F3F3FFFFFFFFFFF
        FFFFFFFF9F3F3FBF7F3F5F9F9F3FDFFF3F5F7F7FBFBF9FDFDF3F5F7F3F9FDF3F
        5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBF9F9F3F3FBFBF9F7FBFBF3F7F7F
        3FFFFF3F7F9F3F5F7F3F7F9F3FDFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFBFBF
        9F9F3F3FBFBF9FFFFFFFFFFFFF5F9F9F3F7F7F3FFFFF3FFFFF3FFFFF3FFFFF3F
        FFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        7FBFBF5F9F9F3F7F7F5F9F9F9FFFFFFFFFFF3F7F7FFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFF3F
        7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF3F7F7F3F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  object dsSecoes: TDataSource
    DataSet = dmDeca.cdsSel_Secao
    Left = 569
    Top = 254
  end
end
