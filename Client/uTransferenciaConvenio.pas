unit uTransferenciaConvenio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmTransferenciaConvenio = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    txtDataTransferencia: TMaskEdit;
    rgOpcao: TRadioGroup;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lbUnidade: TLabel;
    txtCcustoOrigem: TMaskEdit;
    txtLocalOrigem: TEdit;
    txtBancoOrigem: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cbUnidades: TComboBox;
    txtCcustoDestino: TMaskEdit;
    txtLocalDestino: TEdit;
    txtBancoDestino: TEdit;
    GroupBox6: TGroupBox;
    Label14: TLabel;
    Label16: TLabel;
    txtFuncaoDe: TEdit;
    txtFuncaoPara: TEdit;
    GroupBox7: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    txtHorario1: TMaskEdit;
    txtHorario2: TMaskEdit;
    rgEntregaPasse: TRadioGroup;
    Label20: TLabel;
    txtCotas: TEdit;
    GroupBox8: TGroupBox;
    Label15: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    txtInicioCurso: TMaskEdit;
    txtDuracaoCurso: TEdit;
    GroupBox9: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    txtInicioEstagio: TMaskEdit;
    txtDuracaoEstagio: TEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure cbUnidadesExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransferenciaConvenio: TfrmTransferenciaConvenio;

implementation

uses uDM, uTransferenciaEncaminhamento, uFichaPesquisa, rTransferencia,
  uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmTransferenciaConvenio.FormShow(Sender: TObject);
begin
  cbUnidades.Enabled := True;
  cbUnidades.Clear;

  with dmDeca.cdsSel_Unidade do
  begin
    Params.ParamByName('@pe_cod_unidade').Value := Null;
    Params.ParamByName('@pe_nom_unidade').Value := Null;
    Open;
    while not eof do
    begin
      cbUnidades.Items.Add(FieldByName('nom_unidade').AsString);
      Next;
    end;
  AtualizaCamposBotoes('Padrao');
  end;
end;

procedure TfrmTransferenciaConvenio.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmTransferenciaConvenio.Close;
  end;
end;

procedure TfrmTransferenciaConvenio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmTransferenciaConvenio.AtualizaCamposBotoes(Modo: String);
begin
if Modo = 'Padrao' then
  begin
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    lbNome.Caption := '';
    txtDataTransferencia.Enabled := False;
    rgOpcao.ItemIndex := -1;
    rgOpcao.Enabled := False;

    txtCcustoOrigem.Enabled := False;
    txtLocalOrigem.Enabled := False;
    txtBancoOrigem.Enabled := False;

    lbUnidade.Caption := '';
    cbUnidades.ItemIndex := -1;
    cbUnidades.Enabled := False;

    txtCcustoDestino.Enabled := False;
    txtLocalDestino.Enabled := False;
    txtBancoDestino.Enabled := False;

    txtFuncaoDe.Enabled := False;
    txtFuncaoPara.Enabled := False;
    txtHorario1.Enabled := False;
    txtHorario2.Enabled := False;

    rgEntregaPasse.ItemIndex := -1;
    rgEntregaPasse.Enabled := False;

    txtCotas.Enabled := False;

    txtInicioCurso.Enabled := False;
    txtDuracaoCurso.Enabled := False;

    txtInicioEstagio.Enabled := False;
    txtDuracaoEstagio.Enabled := False;

    btnNovo.SetFocus;

  end
else if Modo = 'Novo' then
  begin
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

    txtMatricula.Enabled := True;
    txtMatricula.Clear;
    btnPesquisar.Enabled := True;
    lbNome.Caption := '';
    txtDataTransferencia.Enabled := True;
    txtDataTransferencia.Text := DateToStr(Date());
    rgOpcao.ItemIndex := -1;
    rgOpcao.Enabled := True;

    txtCcustoOrigem.Enabled := True;
    txtCcustoOrigem.Clear;
    txtLocalOrigem.Enabled := True;
    txtLocalOrigem.Clear;
    txtBancoOrigem.Enabled := True;
    txtBancoOrigem.Clear;

    lbUnidade.Caption := vvNOM_UNIDADE;
    cbUnidades.ItemIndex := -1;
    cbUnidades.Enabled := True;

    txtCcustoDestino.Enabled := True;
    txtCcustoDestino.Clear;
    txtLocalDestino.Enabled := True;
    txtLocalDestino.Clear;
    txtBancoDestino.Enabled := True;
    txtBancoDestino.Clear;

    txtFuncaoDe.Enabled := True;
    txtFuncaoDe.Clear;
    txtFuncaoPara.Enabled := True;
    txtFuncaoPara.Clear;
    txtHorario1.Enabled := True;
    txtHorario1.Clear;
    txtHorario2.Enabled := True;
    txtHorario2.Clear;

    rgEntregaPasse.ItemIndex := -1;
    rgEntregaPasse.Enabled := True;

    txtCotas.Enabled := True;
    txtCotas.Text := '00';

    txtInicioCurso.Enabled := True;
    txtInicioCurso.Clear;
    txtDuracaoCurso.Enabled := True;
    txtDuracaoCurso.Text := '00';

    txtInicioEstagio.Enabled := True;
    txtInicioEstagio.Clear;
    txtDuracaoEstagio.Enabled := True;
    txtDuracaoEstagio.Text := '00';

    txtMatricula.SetFocus;

  end
else if Modo = 'Salvar' then
  begin
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;
    btnSair.Enabled := True;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtDataTransferencia.Enabled := False;
    rgOpcao.Enabled := False;

    txtCcustoOrigem.Enabled := False;
    txtLocalOrigem.Enabled := False;
    txtBancoOrigem.Enabled := False;

    cbUnidades.Enabled := False;

    txtCcustoDestino.Enabled := False;
    txtLocalDestino.Enabled := False;
    txtBancoDestino.Enabled := False;

    txtFuncaoDe.Enabled := False;
    txtFuncaoPara.Enabled := False;
    txtHorario1.Enabled := False;
    txtHorario2.Enabled := False;

    rgEntregaPasse.Enabled := False;

    txtCotas.Enabled := False;

    txtInicioCurso.Enabled := False;
    txtDuracaoCurso.Enabled := False;

    txtInicioEstagio.Enabled := False;
    txtDuracaoEstagio.Enabled := False;

  end;
end;

procedure TfrmTransferenciaConvenio.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmTransferenciaConvenio.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtDataTransferencia.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmTransferenciaConvenio.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmTransferenciaConvenio.btnSalvarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try
      // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
      if StatusCadastro(txtMatricula.Text) = 'Ativo' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtDataTransferencia, vtDate); //StrToDate(txtDataTransferencia.Text);
          Params.ParamByName('@pe_ind_tipo_historico').Value := 8;  //Encaminhamento para Conv�nio
          Params.ParamByName('@pe_dsc_tipo_historico').Value := 'Transfer�ncia p/ Conv�nio - Encaminhamento';
          Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;

          Params.ParamByName('@pe_orig_projeto').Value := lbUnidade.Caption;
          Params.ParamByName('@pe_orig_ccusto').Value := GetValue(txtCcustoOrigem.Text);
          Params.ParamByName('@pe_orig_local').Value := getValue(txtLocalOrigem.Text);
          Params.ParamByName('@pe_orig_banco').Value := GetValue(txtBancoOrigem.Text);
          Params.ParamByName('@pe_dest_projeto').Value := cbUnidades.Text;
          Params.ParamByName('@pe_dest_ccusto').Value := GetValue(txtCcustoDestino.Text);
          Params.ParamByName('@pe_dest_local').Value := GetValue(txtLocalDestino.Text);;
          Params.ParamByName('@pe_dest_banco').Value := GetValue(txtBancoDestino.Text);;
          Params.ParamByName('@pe_orig_funcao').Value := GetValue(txtFuncaoDe.Text);
          Params.ParamByName('@pe_dest_funcao').Value := GetValue(txtFuncaoPara.Text);
          Params.ParamByName('@pe_hora_inicio').Value := GetValue(txtHorario1.Text);
          Params.ParamByName('@pe_hora_final').Value := GetValue(txtHorario2.Text);;
          Params.ParamByName('@pe_flg_passe').Value :=  rgEntregaPasse.ItemIndex;
          Params.ParamByName('@pe_num_cotas_passe').Value := StrToFloat(Trim(txtCotas.Text));

          if (txtInicioCurso.Text = '  /  /    ')  or (txtInicioCurso.Text = '//') then
            Params.ParamByName('@pe_dat_curso_inicio').Value := Null
          else
            Params.ParamByName('@pe_dat_curso_inicio').AsDateTime := StrToDate(txtInicioCurso.TExt);

          if (txtInicioEstagio.Text = '  /  /    ')  or (txtInicioEstagio.Text = '//') then
            Params.ParamByName('@pe_dat_estagio_inicio').Value := Null
          else
            Params.ParamByName('@pe_dat_estagio_inicio').AsDateTime := StrToDate(txtInicioEstagio.Text);

          Params.ParamByName('@pe_num_curso_duracao').Value := StrToInt(txtDuracaoCurso.Text);
          Params.ParamByName('@pe_num_estagio_duracao').Value := StrToInt(txtDuracaoEstagio.Text);
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "6" - Conv�nio
          with dmDeca.cdsAlt_Cadastro_Status_Transf do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_cod_unidade').Value := vvINDEX_UNIDADE; 
              Params.ParamByName('@pe_ind_status').Value := 6;
              Execute;
          end;

        end;

        AtualizaCamposBotoes('Salvar');

      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except
      ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10+#13+#10 +
               'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmTransferenciaConvenio.txtMatriculaExit(Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end;
end;

procedure TfrmTransferenciaConvenio.cbUnidadesExit(Sender: TObject);
begin
with DmDeca.cdsSel_Unidade do
  begin
    vvINDEX_UNIDADE := 0;
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= cbUnidades.Items[cbUnidades.ItemIndex];
    Open;
    vvINDEX_UNIDADE := FieldByName('cod_unidade').Value;
  end;
end;

procedure TfrmTransferenciaConvenio.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm(TRelTransferencias,RelTransferencias);
  RelTransferencias.QRLabel123.Enabled := True; //Conv�nio
  RelTransferencias.QRLabel126.Enabled := False;  //Unidade
  RelTransferencias.lbNome1.caption:=lbnome.caption;
  RelTransferencias.lbMatricula1.caption:=txtMatricula.text;
  RelTransferencias.lbProjeto1Do.caption:=vvNom_Unidade;
  RelTransferencias.lbCcusto1Do.caption:=txtCCustoorigem.text;
  RelTransferencias.lbLocal1Do.caption:=txtLocalOrigem.text;
  RelTransferencias.lbBanco1Do.caption:=txtBancoOrigem.text;
  RelTransferencias.lbProjeto1Para.caption:=cbunidades.text;
  RelTransferencias.lbCCusto1Para.caption:=txtCCustoDestino.text;
  RelTransferencias.lbLocal1Para.caption:=txtLocalDestino.text;
  RelTransferencias.lbBanco1Para.caption:=txtBancoDestino.text;

  RelTransferencias.lbFuncao1De.caption:=txtFuncaoDe.text;
  RelTransferencias.lbFuncao1Para.caption:=txtFuncaoPara.text;

  RelTransferencias.lbHorario1.caption:=txtHorario1.text;
  RelTransferencias.lbHorario2.caption:=txtHorario2.text;
  RelTransferencias.lbCotas1.caption:=txtCotas.text;
  if rgEntregapasse.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape30.Brush.color:=clBlack;
       RelTransferencias.QrShape31.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape30.Brush.color:=clwhite;
       RelTransferencias.QrShape31.Brush.color:=clBlack;
      end;
  RelTransferencias.lbInicioCurso1.caption:=txtInicioCurso.text;
  RelTransferencias.lbDuracaoCurso1.caption:=txtDuracaoCurso.text;
  RelTransferencias.lbInicioEstagio1.caption:=txtInicioEstagio.text;
  RelTransferencias.lbDuracaoEstagio1.caption:=txtDuracaoEstagio.text;

  if rgopcao.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape109.Brush.color:=clBlack;
       RelTransferencias.QrShape110.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape109.Brush.color:=clwhite;
       RelTransferencias.QrShape110.Brush.color:=clBlack;
      end;


{2� Via}
  RelTransferencias.QRLabel124.Enabled := True; //Conv�nio
  RelTransferencias.QRLabel122.Enabled := False;  //Unidade
  RelTransferencias.lbNome2.caption:=lbnome.caption;
  RelTransferencias.lbMatricula2.caption:=txtMatricula.text;
  RelTransferencias.lbProjeto2Do.caption:=vvNom_Unidade;
  RelTransferencias.lbCcusto2Do.caption:=txtCCustoorigem.text;
  RelTransferencias.lbLocal2Do.caption:=txtLocalOrigem.text;
  RelTransferencias.lbBanco2Do.caption:=txtBancoOrigem.text;
  RelTransferencias.lbProjeto2Para.caption:=cbunidades.text;
  RelTransferencias.lbCCusto2Para.caption:=txtCCustoDestino.text;
  RelTransferencias.lbLocal2Para.caption:=txtLocalDestino.text;
  RelTransferencias.lbBanco2Para.caption:=txtBancoDestino.text;

  RelTransferencias.lbFuncao2De.caption:=txtFuncaoDe.text;
  RelTransferencias.lbFuncao2Para.caption:=txtFuncaoPara.text;

  RelTransferencias.lbHorario3.caption:=txtHorario1.text;
  RelTransferencias.lbHorario4.caption:=txtHorario2.text;
  RelTransferencias.lbCotas2.caption:=txtCotas.text;
  if rgEntregapasse.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape111.Brush.color:=clBlack;
       RelTransferencias.QrShape112.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape111.Brush.color:=clwhite;
       RelTransferencias.QrShape112.Brush.color:=clBlack;
      end;
  RelTransferencias.lbInicioCurso2.caption:=txtInicioCurso.text;
  RelTransferencias.lbDuracaoCurso2.caption:=txtDuracaoCurso.text;
  RelTransferencias.lbInicioEstagio2.caption:=txtInicioEstagio.text;
  RelTransferencias.lbDuracaoEstagio2.caption:=txtDuracaoEstagio.text;

  if rgopcao.ItemIndex = 0 then
     begin
       RelTransferencias.QrShape111.Brush.color:=clBlack;
       RelTransferencias.QrShape112.Brush.color:=clwhite;
     end
  else
      begin
       RelTransferencias.QrShape111.Brush.color:=clwhite;
       RelTransferencias.QrShape112.Brush.color:=clBlack;
      end;


       RelTransferencias.Preview;
       RelTransferencias.Free;
       AtualizaCamposBotoes('Padrao');
end;

procedure TfrmTransferenciaConvenio.btnSairClick(Sender: TObject);
begin
  frmTransferenciaEncaminhamento.Close;
end;

end.
