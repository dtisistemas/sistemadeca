unit uRetornoNAVE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, DBGrids, ComCtrls, Mask, Buttons;

type
  TfrmRetornoNAVE = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    mskMatricula: TMaskEdit;
    edNome: TEdit;
    MaskEdit1: TMaskEdit;
    meJustificativaEncaminhamento: TMemo;
    btnEncaminhar: TSpeedButton;
    btnSair: TSpeedButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRetornoNAVE: TfrmRetornoNAVE;

implementation

{$R *.DFM}

end.
