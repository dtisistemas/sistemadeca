unit uCadastroUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Grids, DBGrids, ComCtrls, Buttons, ExtCtrls, jpeg;

type
  TfrmCadastroUsuarios = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    mskMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    datNascimento: TDateTimePicker;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    mskCep: TMaskEdit;
    edNomeUsuario: TEdit;
    edEndereco: TEdit;
    edComplemento: TEdit;
    edBairro: TEdit;
    GroupBox8: TGroupBox;
    edEmail: TEdit;
    GroupBox9: TGroupBox;
    datIncluido: TDateTimePicker;
    GroupBox10: TGroupBox;
    Label1: TLabel;
    mskFoneCelular: TMaskEdit;
    Label2: TLabel;
    mskFoneRecado: TMaskEdit;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    cbUsuarioCapacitado: TComboBox;
    GroupBox15: TGroupBox;
    imgFotografia: TImage;
    cbSituacaoUsuario: TComboBox;
    Panel1: TPanel;
    btnSair: TSpeedButton;
    btnLocallizar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnNovo: TSpeedButton;
    btnVerDadosUsuario: TSpeedButton;
    function ValidaEmail (Email:String) : Boolean;
    procedure edEmailExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroUsuarios: TfrmCadastroUsuarios;

implementation

{$R *.DFM}

{ TfrmCadastroUsuarios }

function TfrmCadastroUsuarios.ValidaEmail(Email: String): Boolean;
var
  s    : String;
  ePos : Integer;
begin
  ePos := pos('@', Email);
  if (ePos > 1) then
  begin
    s := Copy(Email, ePos + 1, Length(Email));
    if (pos('.', s)>1) and (pos('.', s)<Length(s)) then
      Result := True
    else
      Result := False;
  end
  else
    Result := False;
  end;

procedure TfrmCadastroUsuarios.edEmailExit(Sender: TObject);
begin
  if not ValidaEmail(edEmail.Text) then edEmail.SetFocus;

end;

end.
