unit uVisitaTecnica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmVisitaTecnica = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    txtData: TMaskEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    Label4: TLabel;
    edEndereco: TEdit;
    Label5: TLabel;
    edComplemento: TEdit;
    Label6: TLabel;
    edBairro: TEdit;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    txtOcorrencia: TMemo;
    Label7: TLabel;
    edCep: TMaskEdit;
    Label8: TLabel;
    edNumInscricao: TEdit;
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtualizaCamposBotoes (Modo : String);
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVisitaTecnica: TfrmVisitaTecnica;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmVisitaTecnica.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    txtMatricula.Clear;
    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtData.Clear;
    txtData.Enabled := False;
    edNumInscricao.Clear;
    edNumInscricao.Enabled := False;
    edEndereco.Clear;
    edEndereco.Enabled := False;
    edComplemento.Clear;
    edComplemento.Enabled := False;
    edBairro.Clear;
    edBairro.Enabled := False;
    edCep.Clear;
    edCep.Enabled := False;
    txtOcorrencia.Lines.Clear;
    txtOcorrencia.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    //btnImprimir.Enabled := False;
    btnSair.Enabled := True;

  end

  else if Modo = 'Novo' then
  begin

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtData.Text := DateToStr(Date());
    edNumInscricao.Enabled := False;
    edEndereco.Enabled := False;
    edComplemento.Enabled := False;
    edBairro.Enabled := False;
    edCep.Enabled := False;
    txtOcorrencia.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    //btnImprimir.Enabled := False;
    btnSair.Enabled := False;

  end;


end;

procedure TfrmVisitaTecnica.btnSairClick(Sender: TObject);
begin
  frmVisitaTecnica.Close;
end;

procedure TfrmVisitaTecnica.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmVisitaTecnica.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;

        edNumInscricao.Text := IntToStr(FieldByName('num_prontuario').AsInteger);
        edEndereco.Text := FieldByName ('nom_endereco').AsString;
        edComplemento.Text := FieldByName ('nom_complemento').AsString;
        edBairro.Text := FieldByName ('nom_bairro').AsString;
        edCep.Text := FieldByName ('num_cep').AsString;

        edNumInscricao.Enabled := False;
        edEndereco.Enabled := False;
        edComplemento.Enabled := False;
        edBairro.Enabled := False;
        edCep.Enabled := False;

        txtOcorrencia.SetFocus;

      end;
    except end;
  end;
end;

procedure TfrmVisitaTecnica.FormShow(Sender: TObject);
begin
  //AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmVisitaTecnica.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmVisitaTecnica.Close;
  end;

end;

procedure TfrmVisitaTecnica.FormActivate(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmVisitaTecnica.btnNovoClick(Sender: TObject);
begin
    AtualizaCamposBotoes ('Novo');
end;

procedure TfrmVisitaTecnica.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;

            edNumInscricao.Text := IntToStr(FieldByName('num_prontuario').AsInteger);
            edEndereco.Text := FieldByName ('nom_endereco').AsString;
            edComplemento.Text := FieldByName ('nom_complemento').AsString;
            edBairro.Text := FieldByName ('nom_bairro').AsString;
            edCep.Text := FieldByName ('num_cep').AsString;

            edNumInscricao.Enabled := False;
            edEndereco.Enabled := False;
            edComplemento.Enabled := False;
            edBairro.Enabled := False;
            edCep.Enabled := False;

            txtOcorrencia.SetFocus;

          end;

      end;
    except end;
  end;
end;

procedure TfrmVisitaTecnica.btnSalvarClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja lan�ar REGISTRO DE VISITA T�CNICA para a crian�a/adolescente?',
          'Lan�amento de REGISTRO DE VISITA T�CNICA',
          MB_ICONQUESTION + MB_YESNO) = id_Yes then
  begin

    if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
    begin
      try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      //if StatusCadastro(txtMatricula.Text) = 'Ativo' then

      //A crian�a com status de Afastado ou Suspenso podem ser feitas
      //anota��es no registro de atendimento
      if (StatusCadastro(txtMatricula.Text) = 'Ativo') OR (StatusCadastro(txtMatricula.Text) = 'Afastado') OR
          (StatusCadastro(txtMatricula.Text) = 'Suspenso') then

      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_unidade').AsInteger;//vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 17;
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Registro de Visita T�cnica';

          Params.ParamByName('@pe_ind_tipo').Value := Null;
          Params.ParamByName('@pe_ind_classificacao').Value := Null;
          Params.ParamByName('@pe_qtd_participantes').Value := Null;
          Params.ParamByName('@pe_num_horas').Value := Null;

          Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;
        end;

        AtualizaCamposBotoes('Padr�o');

      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
      except
        ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10 +#13+#10 +
               'Favor entrar em contato com o Administrador do Sistema');
      end;
    end
  else
  begin
    ShowMessage('Opera��o cancelada pelo USU�RIO...');
    AtualizaCamposBotoes ('Padr�o');
  end;
end;
end;

procedure TfrmVisitaTecnica.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

end.
