unit rRelacaoFaltasMesDRH;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelRelacaoFaltasMesDRH = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    qrlbTitulo1: TQRLabel;
    qrlbTitulo2: TQRLabel;
    DetailBand1: TQRBand;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape3: TQRShape;
    QRLabel3: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    SummaryBand1: TQRBand;
    QRLabel7: TQRLabel;
    QRExpr1: TQRExpr;
    QRDBText7: TQRDBText;
    QRShape12: TQRShape;
    QRLabel8: TQRLabel;
    QRShape13: TQRShape;
    qrlbPaginas: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRExpr2: TQRExpr;
    QRShape14: TQRShape;
    QRLabel11: TQRLabel;
    QRShape15: TQRShape;
    QRExpr3: TQRExpr;
    QRLabel12: TQRLabel;
    QRExpr4: TQRExpr;
  private

  public

  end;

var
  relRelacaoFaltasMesDRH: TrelRelacaoFaltasMesDRH;

implementation

uses uDM;

{$R *.DFM}

end.
