object frmInicializarPresenca: TfrmInicializarPresenca
  Left = -1003
  Top = 145
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Inicializar Presen�a]'
  ClientHeight = 410
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object GroupBox1: TGroupBox
    Left = 3
    Top = 74
    Width = 666
    Height = 52
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe o N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnChange = txtMatriculaChange
      OnExit = txtMatriculaExit
    end
  end
  object Panel1: TPanel
    Left = 2
    Top = 179
    Width = 666
    Height = 178
    BevelInner = bvLowered
    TabOrder = 2
    object DBGrid1: TDBGrid
      Left = 6
      Top = 5
      Width = 652
      Height = 139
      DataSource = DataSource1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Caption = '[Matric.]'
          Width = 65
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia01'
          Title.Caption = '01'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia02'
          Title.Caption = '02'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia03'
          Title.Caption = '03'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia04'
          Title.Caption = '04'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia05'
          Title.Caption = '05'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia06'
          Title.Caption = '06'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia07'
          Title.Caption = '07'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia08'
          Title.Caption = '08'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia09'
          Title.Caption = '09'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia10'
          Title.Caption = '10'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia11'
          Title.Caption = '11'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia12'
          Title.Caption = '12'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia13'
          Title.Caption = '13'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia14'
          Title.Caption = '14'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia15'
          Title.Caption = '15'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia16'
          Title.Caption = '16'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia17'
          Title.Caption = '17'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia18'
          Title.Caption = '18'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia19'
          Title.Caption = '19'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia20'
          Title.Caption = '20'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia21'
          Title.Caption = '21'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia22'
          Title.Caption = '22'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia23'
          Title.Caption = '23'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia24'
          Title.Caption = '24'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia25'
          Title.Caption = '25'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia26'
          Title.Caption = '26'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia27'
          Title.Caption = '27'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia28'
          Title.Caption = '28'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia29'
          Title.Caption = '29'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia30'
          Title.Caption = '30'
          Width = 20
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'flg_dia31'
          Title.Caption = '31'
          Width = 20
          Visible = True
        end>
    end
    object ProgressBar1: TProgressBar
      Left = 7
      Top = 147
      Width = 650
      Height = 25
      Min = 0
      Max = 100
      Smooth = True
      Step = 1
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 4
    Top = 359
    Width = 664
    Height = 49
    TabOrder = 3
    object btnCancelar: TBitBtn
      Left = 529
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 602
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnGravar: TBitBtn
      Left = 415
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnGravarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
        00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
        00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
        00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
        0003737FFFFFFFFF7F7330099999999900333777777777777733}
      NumGlyphs = 2
    end
    object btnNovo: TBitBtn
      Left = 357
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnNovoClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
    end
    object btnExcluir: TBitBtn
      Left = 472
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777770000000777777777777777770000000777777777777770F700000007777
        0F777777777770000000777000F7777770F770000000777000F777770F777000
        00007777000F77700F777000000077777000F700F7777000000077777700000F
        7777700000007777777000F777777000000077777700000F7777700000007777
        7000F70F7777700000007770000F77700F7770000000770000F7777700F77000
        00007700F7777777700F70000000777777777777777770000000777777777777
        777770000000}
    end
  end
  object rgCriterio: TRadioGroup
    Left = 3
    Top = 3
    Width = 666
    Height = 71
    Caption = 'Crit�rio'
    ItemIndex = 0
    Items.Strings = (
      
        'Todos        (Gerar "Presen�a" para todas as crian�as/adolescent' +
        'es cadastradas)'
      'Matr�cula   (Gerar "Presen�a" para uma matr�cula espec�fica).')
    TabOrder = 0
    OnClick = rgCriterioClick
  end
  object Panel3: TPanel
    Left = 3
    Top = 126
    Width = 665
    Height = 52
    BevelInner = bvLowered
    TabOrder = 4
    object Label2: TLabel
      Left = 49
      Top = 20
      Width = 27
      Height = 13
      Caption = 'Ano:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edAno: TEdit
      Left = 81
      Top = 16
      Width = 42
      Height = 22
      MaxLength = 4
      TabOrder = 0
    end
    object cbMes: TComboBox
      Left = 271
      Top = 16
      Width = 174
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 1
      Items.Strings = (
        'JANEIRO'
        'FEVEREIRO'
        'MAR�O'
        'ABRIL'
        'MAIO'
        'JUNHO'
        'JULHO'
        'AGOSTO'
        'SETEMBRO'
        'OUTUBRO'
        'NOVEMBRO'
        'DEZEMBRO')
    end
    object chMes: TCheckBox
      Left = 160
      Top = 18
      Width = 108
      Height = 17
      Caption = 'Especificar M�s'
      TabOrder = 2
      OnClick = chMesClick
    end
  end
  object DataSource1: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Frequencia
    Left = 10
    Top = 259
  end
end
