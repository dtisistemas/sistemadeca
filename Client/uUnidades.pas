unit uUnidades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, Db;

type
  TfrmUnidades = class(TForm)
    Panel2: TPanel;
    btnNovoU: TBitBtn;
    btnSalvarU: TBitBtn;
    btnCancelarU: TBitBtn;
    btnSairU: TBitBtn;
    btnAlterarU: TBitBtn;
    dsUnidade: TDataSource;
    btnSecoes: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    edNomeUnidade: TEdit;
    edCcusto1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    cbRegiao: TComboBox;
    Label4: TLabel;
    edNomeGestor: TEdit;
    Label5: TLabel;
    edEndereco: TEdit;
    Label6: TLabel;
    edComplemento: TEdit;
    Label7: TLabel;
    edBairro: TEdit;
    edCep: TMaskEdit;
    Label8: TLabel;
    Label9: TLabel;
    edTelefone1: TMaskEdit;
    Label10: TLabel;
    edTelefone2: TMaskEdit;
    Label11: TLabel;
    edEmail: TEdit;
    Label12: TLabel;
    edCapacidade: TEdit;
    Label13: TLabel;
    cbTipo: TComboBox;
    Label14: TLabel;
    cbSituacao: TComboBox;
    Label15: TLabel;
    Label16: TLabel;
    edDataUltAtualizacao1: TMaskEdit;
    MaskEdit1: TMaskEdit;
    Panel3: TPanel;
    dbgUnidade: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtualizaCamposBotoes (Modo: String);
    procedure FormShow(Sender: TObject);
    procedure dbgUnidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgUnidadeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgUnidadeCellClick(Column: TColumn);
    procedure btnCancelarUClick(Sender: TObject);
    procedure btnSalvarUClick(Sender: TObject);
    procedure dbgUnidadeDblClick(Sender: TObject);
    procedure btnAlterarUClick(Sender: TObject);
    procedure btnCancelarSClick(Sender: TObject);
    procedure btnSairSClick(Sender: TObject);
    procedure btnSairUClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnNovoUClick(Sender: TObject);
    procedure btnSecoesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUnidades: TfrmUnidades;

implementation

uses uDM, uUtil, uPrincipal, uSecoes;

{$R *.DFM}

procedure TfrmUnidades.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    //Campos
    edNomeUnidade.Clear;
    cbRegiao.ItemIndex := -1;
    edCcusto1.Clear;
    edNomeGestor.Clear;
    edEndereco.Clear;
    edCep.Clear;
    edComplemento.Clear;
    edBairro.Clear;
    edTelefone1.Clear;
    edTelefone2.Clear;
    edEmail.Clear;
    edCapacidade.Clear;
    cbTipo.ItemIndex := -1;
    cbSituacao.ItemIndex := -1;
    edDataUltAtualizacao1.Clear;
    Panel1.Enabled := False;

    //Bot�es
    btnNovoU.Enabled := True;
    btnSalvarU.Enabled := False;
    btnAlterarU.Enabled := False;
    btnCancelarU.Enabled := False;
    btnSairU.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin

    //Campos
    edNomeUnidade.Enabled := True;
    cbRegiao.Enabled := True;
    edCcusto1.Enabled := True;
    edNomeGestor.Enabled := True;
    edEndereco.Enabled := True;
    edCep.Enabled := True;
    edComplemento.Enabled := True;
    edBairro.Enabled := True;
    edTelefone1.Enabled := True;
    edTelefone2.Enabled := True;
    edEmail.Enabled := True;
    edCapacidade.Enabled := True;
    cbTipo.Enabled := True;
    cbSituacao.Enabled := True;
    edDataUltAtualizacao1.Enabled := True;
    Panel1.Enabled := True;

    //Bot�es
    btnNovoU.Enabled := False;
    btnSalvarU.Enabled := True;
    btnAlterarU.Enabled := False;
    btnCancelarU.Enabled := True;
    btnSairU.Enabled := False;

  end
  else if Modo = 'Alterar' then //Alterar dados da Unidade
  begin

    //Campos
    edNomeUnidade.Enabled := True;
    edCcusto1.Enabled := True;
    edNomeGestor.Enabled := True;
    edEndereco.Enabled := True;
    edCep.Enabled := True;
    edComplemento.Enabled := True;
    edBairro.Enabled := True;
    edTelefone1.Enabled := True;
    edTelefone2.Enabled := True;
    edEmail.Enabled := True;
    edCapacidade.Enabled := True;
    cbTipo.Enabled := True;
    cbSituacao.Enabled := True;
    edDataUltAtualizacao1.Enabled := True;
    Panel1.Enabled := True;

    //Bot�es
    btnNovoU.Enabled := False;
    btnSalvarU.Enabled := False;
    btnAlterarU.Enabled := True;
    btnCancelarU.Enabled := True;
    btnSairU.Enabled := False;
  end;
end;

procedure TfrmUnidades.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmUnidades.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
      dbgUnidade.Refresh;
    end;
  except end;
end;

procedure TfrmUnidades.dbgUnidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  //Repassa os valores para o formul�rio
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  edCcusto1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  edCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  edTelefone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  edTelefone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;
  cbSituacao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('flg_status').AsInteger;
  edDataUltAtualizacao1.Text := DateToStr(dmDeca.cdsSel_Unidade.FieldByName ('log_data_altera').AsDateTime);
end;

procedure TfrmUnidades.dbgUnidadeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  //Repassa os valores para o formul�rio
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  edCcusto1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  edCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  edTelefone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  edTelefone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;
  cbSituacao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('flg_status').AsInteger;
  edDataUltAtualizacao1.Text := DateToStr(dmDeca.cdsSel_Unidade.FieldByName ('log_data_altera').AsDateTime);
end;

procedure TfrmUnidades.dbgUnidadeCellClick(Column: TColumn);
begin
  //Repassa os valores para o formul�rio
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  edCcusto1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  edCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  edTelefone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  edTelefone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;
  cbSituacao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('flg_status').AsInteger;
  edDataUltAtualizacao1.Text := DateToStr(dmDeca.cdsSel_Unidade.FieldByName ('log_data_altera').AsDateTime);
end;

procedure TfrmUnidades.btnCancelarUClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmUnidades.btnSalvarUClick(Sender: TObject);
begin

  //Verificar a duplicidade de nome de unidade
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Trim(edNomeUnidade.Text);
      Open;

      if (RecordCount < 1) then
      begin

        //Faz a inser��o dos dados da Unidade

        with dmDeca.cdsInc_Unidade do
        begin
          Close;
          Params.ParamByName ('@pe_nom_unidade').Value := GetValue(edNomeUnidade.Text);
          Params.ParamByName ('@pe_ind_regiao').Value := cbRegiao.ItemIndex;
          Params.ParamByName ('@pe_num_ccusto').Value := GetValue(edCcusto1.Text);
          Params.ParamByName ('@pe_nom_gestor').Value := GetValue(edNomeGestor.Text);
          Params.ParamByName ('@pe_dsc_endereco').Value := GetValue(edEndereco.Text);
          Params.ParamByName ('@pe_dsc_complemento').Value := GetValue(edComplemento.Text);
          Params.ParamByName ('@pe_dsc_bairro').Value := GetValue(edBairro.Text);
          Params.ParamByName ('@pe_num_cep').Value := GetValue(edCep.Text);
          Params.ParamByName ('@pe_num_fone1').Value := GetValue(edTelefone1.Text);
          Params.ParamByName ('@pe_num_fone2').Value := GetValue(edTelefone2.Text);
          Params.ParamByName ('@pe_dsc_email').Value := GetValue(edEmail.Text);
          Params.ParamByName ('@pe_num_capacidade').Value := GetValue(edCapacidade.Text);
          Params.ParamByName ('@pe_ind_tipo').Value := cbTipo.ItemIndex;
          Params.ParamByName ('@pe_flg_status').Value := 0;  //0 para ativa e 1 para inativa
          //Params.ParamByName ('@pe_data_altera').Value := Null;
          //Params.ParamByName ('@pe_log_data').Value := GetValue();
          Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;

          with dmDeca.cdsSel_Unidade do
          begin
            Close;
            Params.ParamByName ('@pe_cod_unidade').Value := Null;
            Params.ParamByName ('@pe_nom_unidade').Value := Null;
            Open;
            dbgUnidade.Refresh;
            AtualizaCamposBotoes ('Padr�o');
          end;
        end;

      end
      else
      begin
        ShowMessage ('O nome de Unidade j� existe...');
        AtualizaCamposBotoes ('Padr�o');
      end;
    end;
  except end;

end;

procedure TfrmUnidades.dbgUnidadeDblClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Alterar');
  edNomeUnidade.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').AsString;
  cbRegiao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_regiao').AsInteger;
  edCcusto1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_ccusto').AsString;
  edNomeGestor.Text := dmDeca.cdsSel_Unidade.FieldByName ('nom_gestor').AsString;
  edEndereco.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_endereco').AsString;
  edCep.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_cep').AsString;
  edComplemento.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_complemento').AsString;
  edBairro.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_bairro').AsString;
  edTelefone1.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone1').AsString;
  edTelefone2.Text := dmDeca.cdsSel_Unidade.FieldByName ('num_telefone2').AsString;
  edEmail.Text := dmDeca.cdsSel_Unidade.FieldByName ('dsc_email').AsString;
  edCapacidade.Text := IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('num_capacidade').AsInteger);
  cbTipo.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo').AsInteger;
  cbSituacao.ItemIndex := dmDeca.cdsSel_Unidade.FieldByName ('flg_status').AsInteger;
  edDataUltAtualizacao1.Text := DateToStr(dmDeca.cdsSel_Unidade.FieldByName ('log_data_altera').AsDateTime);
end;

procedure TfrmUnidades.btnAlterarUClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Unidade do
    begin

      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').AsInteger;
      Params.ParamByName ('@pe_nom_unidade').Value := GetValue(edNomeUnidade.Text);
      Params.ParamByName ('@pe_ind_regiao').Value := cbRegiao.ItemIndex;
      Params.ParamByName ('@pe_num_ccusto').AsString := edCcusto1.Text;
      Params.ParamByName ('@pe_nom_gestor').Value := GetValue(edNomeGestor.Text);
      Params.ParamByName ('@pe_dsc_endereco').Value := GetValue(edEndereco.Text);
      Params.ParamByName ('@pe_dsc_complemento').Value := GetValue(edComplemento.Text);
      Params.ParamByName ('@pe_dsc_bairro').Value := GetValue(edBairro.Text);
      Params.ParamByName ('@pe_num_cep').Value := GetValue(edCep.Text);
      Params.ParamByName ('@pe_num_telefone1').Value := GetValue(edTelefone1.Text);
      Params.ParamByName ('@pe_num_telefone2').Value := GetValue(edTelefone2.Text);
      Params.ParamByName ('@pe_dsc_email').Value := GetValue(edEmail.Text);
      Params.ParamByName ('@pe_num_capacidade').Value := GetValue(edCapacidade.Text);
      Params.ParamByName ('@pe_ind_tipo').AsInteger := cbTipo.ItemIndex;
      Params.ParamByName ('@pe_flg_status').AsInteger := cbSituacao.ItemIndex;
      Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;

      //Atualiza os dados do dbgrid
      with dmDeca.cdsSel_Unidade do
      begin
        Close;
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
        Params.ParamByName ('@pe_nom_unidade').Value := Null;
        Open;
        dbgUnidade.Refresh;
      end;

      AtualizaCamposBotoes ('Padr�o');

    end;
  except end;
end;

procedure TfrmUnidades.btnCancelarSClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�oS');
end;

procedure TfrmUnidades.btnSairSClick(Sender: TObject);
begin
 frmUnidades.Close;
end;

procedure TfrmUnidades.btnSairUClick(Sender: TObject);
begin
  frmUnidades.Close;
end;

procedure TfrmUnidades.FormCreate(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
      dbgUnidade.Refresh;
    end;
  except end;
end;

procedure TfrmUnidades.btnNovoUClick(Sender: TObject);
begin
 AtualizaCamposBotoes('Novo');
end;

procedure TfrmUnidades.btnSecoesClick(Sender: TObject);
begin
  Application.CreateForm (TfrmSecoes, frmSecoes);
  frmSecoes.ShowModal;
end;

end.
