unit uEmitirRelatorioDeClasses;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls;

type
  TfrmEmitirRelatorioDeClasses = class(TForm)
    GroupBox1: TGroupBox;
    cbUnidadeEscolar: TComboBox;
    cbClasses: TComboBox;
    btnEmitirRelatorioClasses: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeEscolarClick(Sender: TObject);
    procedure btnEmitirRelatorioClassesClick(Sender: TObject);
    procedure cbClassesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmitirRelatorioDeClasses: TfrmEmitirRelatorioDeClasses;
  vListaUE1, vListaUE2 : TStringList;

implementation

uses uDM, rEducacenso;

{$R *.DFM}

procedure TfrmEmitirRelatorioDeClasses.FormShow(Sender: TObject);
begin
  vListaUE1 := TStringList.Create();
  vListaUE2 := TStringList.Create();
  //Carregar a lista de unidades escolares...
  try
    with dmDeca.cdsSel_UnidadeEscolarCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uecie').Value := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value    := Null;
      Params.ParamByName ('@pe_num_uecie').Value    := Null;
      Open;

      while not (dmDeca.cdsSel_UnidadeEscolarCIE.eof) do
      begin
        vListaUE1.Add(IntToStr(dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName ('cod_id_uecie').Value));
        vListaUE2.Add(dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName ('dsc_uecie').Value);
        cbUnidadeEscolar.Items.Add (dmDeca.cdsSel_UnidadeEscolarCIE.FieldByName ('dsc_uecie').Value);
        dmDeca.cdsSel_UnidadeEscolarCIE.Next;
      end;
    end;
  except
    Application.MessageBox ('Um erro ocorreu na tentativa de consultar os dados das unidades escolares' + #13#10 +
                            'Favor tentar novamente e caso o erro continue, contate o Administrador do Sistema',
                            '[Sistema Deca] - Erro consultando dados das Unidades Escolares',
                            MB_OK + MB_ICONERROR);
  end;

end;

procedure TfrmEmitirRelatorioDeClasses.cbUnidadeEscolarClick(
  Sender: TObject);
begin
  cbClasses.Clear;
  try
    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByName ('@pe_dsc_turmacie').Value    := Null;
      Params.ParamByName ('@pe_cod_id_uecie').Value    := StrToInt(vListaUE1.Strings[cbUnidadeEscolar.ItemIndex]);
      Params.ParamByName ('@pe_dsc_uecie').Value       := Null;
      Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByName ('@pe_dsc_seriecie').Value    := Null;
      Params.ParamByName ('@pe_ind_periodo').Value     := Null;
      Params.ParamByName ('@pe_num_turma').Value       := Null;
      Open;

      while not(dmDeca.cdsSel_TurmaCIE.eof) do
      begin
        cbClasses.Items.Add (dmDeca.cdsSel_TurmaCIE.FieldByName ('num_turma').Value);
        dmDeca.cdsSel_TurmaCIE.Next;
      end;
    end;
  except end;
end;

procedure TfrmEmitirRelatorioDeClasses.btnEmitirRelatorioClassesClick(Sender: TObject);
begin
  //Verificar os alunos cadastrados na turma/classe selecionada...
  try
    with dmDeca.cdsSel_ClassesCadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_num_turma_cei').Value := Copy(cbClasses.Text,1,11);
      Open;

      if (dmDeca.cdsSel_ClassesCadastro.RecordCount < 1) then
      begin

      end
      else
      begin
        //Application.CreateForm (TrelEducacenso, relEducacenso);
        relEducacenso.Prepare;
        relEducacenso.Preview;
        relEducacenso.Free;
      end;
    end;
  except end;
end;

procedure TfrmEmitirRelatorioDeClasses.cbClassesClick(Sender: TObject);
begin
   try
    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByName ('@pe_dsc_turmacie').Value    := Null;
      Params.ParamByName ('@pe_cod_id_uecie').Value    := Null;
      Params.ParamByName ('@pe_dsc_uecie').Value       := Null;
      Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByName ('@pe_dsc_seriecie').Value    := Null;
      Params.ParamByName ('@pe_ind_periodo').Value     := Null;
      Params.ParamByName ('@pe_num_turma').Value       := Copy(cbClasses.Text,1,11);
      Open;

      Application.CreateForm (TrelEducacenso, relEducacenso);
      relEducacenso.lbUnidadeEscolar.Caption           := IntToStr(dmDeca.cdsSel_TurmaCIE.FieldByName ('num_uecie').Value) + '-' + dmDeca.cdsSel_TurmaCIE.FieldByName ('dsc_uecie').Value;
      relEducacenso.lbSerie.Caption                    := dmDeca.cdsSel_TurmaCIE.FieldByName ('dsc_seriecie').Value;
      relEducacenso.lbPeriodo.Caption                  := dmDeca.cdsSel_TurmaCIE.FieldByName ('vPeriodo').Value;
      relEducacenso.lbClasse.Caption                   := dmDeca.cdsSel_TurmaCIE.FieldByName ('num_turma').Value;
    end;
   except end;
end;

end.
