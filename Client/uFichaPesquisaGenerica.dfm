object frmFichaPesquisaGenerica: TfrmFichaPesquisaGenerica
  Left = 410
  Top = 294
  BorderStyle = bsDialog
  Caption = 'Sistema Deca - [Pesquisa Gen�rica de Crian�as/Adolescentes]'
  ClientHeight = 405
  ClientWidth = 745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TPanel
    Left = 0
    Top = 0
    Width = 745
    Height = 405
    Align = alClient
    TabOrder = 0
    object dbgPesquisa: TDBGrid
      Left = 16
      Top = 164
      Width = 713
      Height = 224
      Hint = 'RESULTADOS OBTIDOS'
      TabStop = False
      DataSource = dsSel_Cadastro_Pesquisa
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Alignment = taCenter
          Title.Caption = 'Matr�cula'
          Width = 65
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_prontuario'
          Title.Alignment = taCenter
          Title.Caption = 'Prontu�rio'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_nome'
          Title.Caption = 'Nome Crian�a/Adolescente'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_unidade'
          Title.Caption = 'Unidade'
          Width = 185
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ind_vinculo'
          Title.Caption = 'V�nculo'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_familiar'
          Title.Caption = 'Nome Familiar'
          Width = 185
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 16
      Top = 16
      Width = 713
      Height = 137
      BorderStyle = bsSingle
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 19
        Width = 46
        Height = 13
        Caption = 'Pesquisar'
      end
      object Label2: TLabel
        Left = 16
        Top = 51
        Width = 33
        Height = 13
        Caption = 'Campo'
      end
      object edPesquisa: TEdit
        Left = 72
        Top = 16
        Width = 409
        Height = 21
        Hint = 'Informe o "VALOR" a ser pesquisado'
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnKeyPress = edPesquisaKeyPress
      end
      object cbCampoPesquisa: TComboBox
        Left = 72
        Top = 48
        Width = 225
        Height = 21
        Hint = 'Selecione qual o CAMPO deseja realizar a pesquisa'
        Style = csDropDownList
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = cbCampoPesquisaChange
        Items.Strings = (
          'Matr�cula'
          'Prontu�rio'
          'Nome da Crian�a / Adolescente'
          'Nome do Respons�vel')
      end
      object rgCriterio: TRadioGroup
        Left = 14
        Top = 80
        Width = 467
        Height = 41
        Hint = 'Selecione o CRIT�RIO de busca'
        Caption = 'Crit�rio de Busca'
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'In�cio do nome'
          'Trecho do nome')
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = rgCriterioClick
      end
      object btnPesquisa: TBitBtn
        Left = 576
        Top = 48
        Width = 107
        Height = 49
        Hint = 
          'Clique para EXIBIR os resultados obtidos atrav�s do crit�rio est' +
          'abelecido'
        Caption = '&Pesquisar'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btnPesquisaClick
      end
    end
  end
  object dsSel_Cadastro_Pesquisa: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Pesquisa
    Left = 680
    Top = 328
  end
end
