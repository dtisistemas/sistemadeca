unit uEnviarEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Psock, NMsmtp, ExtCtrls, Buttons;

type
  TfrmEnviarEmail = class(TForm)
    OpenDialog1: TOpenDialog;
    NMSMTP1: TNMSMTP;
    meMensagem: TMemo;
    Panel4: TPanel;
    btnEnviar: TSpeedButton;
    Panel1: TPanel;
    Label6: TLabel;
    Panel5: TPanel;
    edEmailPara: TEdit;
    Panel2: TPanel;
    edAssunto: TEdit;
    Panel3: TPanel;
    edAnexo: TEdit;
    btnAnexar: TSpeedButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    RadioGroup1: TRadioGroup;
    procedure btnAnexarClick(Sender: TObject);
    procedure btnEnviarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEnviarEmail: TfrmEnviarEmail;

implementation

{$R *.DFM}

procedure TfrmEnviarEmail.btnAnexarClick(Sender: TObject);
begin
  OpenDialog1.Execute;
  edAnexo.Text := OpenDialog1.FileName;
end;

procedure TfrmEnviarEmail.btnEnviarClick(Sender: TObject);
begin


try

  {Servidor SMTP}
  //NMSMTP1.Host := 'pop.fundhas.org.br';
  NMSMTP1.Host := '187.45.210.69';
  //NMSMTP1.Host := 'smtp.fundhas.org.br';
  {Porta SMTP}
  NMSMTP1.Port := 587;
  {Nome de login do usu�rio}
  NMSMTP1.UserID := 'dadosescolares@fundhas.org.br';
  {Conectando ao Servidor}
  NMSMTP1.Connect;

  {Se n�o conseguiu conectar ao servidor, avise...}
  if NOT NMSMTP1.Connected then
    raise  Exception.Create ('Erro de Conex�o!!!');

  with NMSMTP1.PostMessage do
  begin
    {e-Mail de origem "DE"}
    FromAddress := 'dadosescolares@fundhas.org.br';
    FromName    := 'Equipe Acompanhamento Escolar - FUNDHAS';
    //FromAddress := 'fabio.sesso@fundhas.org.br';
    //FromName    := 'Equipe Acompanhamento Escolar - FUNDHAS';

    {e-Mail do Destinat�rio}
    ToAddress.Clear;
    //ToAddress.Add ('fabio.sesso@fundhas.org.br');
    ToAddress.Add (Trim(edEmailPara.Text));
    {Assunto da Mensagem}
    Subject := 'Planilha com Dados Escolares - [FUNDHAS].';
    {Corpo da Mensagem}
    Body.Clear;
    Body.Text := meMensagem.Text;

    {
    Body.Lines.Add ('Sr(a) Diretor(a).');
    Body.Lines.Add ('');
    Body.Lines.Add ('Segue anexa planilha com a rela��o de crian�as e adolescentes da FUNDHAS cadastradas');
    Body.Lines.Add ('em nosso sistema como sendo dessa escola. Pedimos, gentilmente, que alterem as colunas');
    Body.Lines.Add ('"APROVEITAMENTO" e "FREQU�NCIA" de acordo com os valores abaixo:');
    Body.Lines.Add ('');
    Body.Lines.Add ('Aproveitamento:');
    Body.Lines.Add ('0 - Para aluno Abaixo da M�dia');
    Body.Lines.Add ('1 - Para aluno Acima da M�dia');
    Body.Lines.Add ('2 - Sem Informa��o de Aproveitamento');
    Body.Lines.Add ('3 - Para aluno Dentro da M�dia');
    Body.Lines.Add ('');
    Body.Lines.Add ('Frequ�ncia:');
    Body.Lines.Add ('0 - Para aluno com frequ�ncia superior a 75% (+75%)');
    Body.Lines.Add ('1 - Para aluno com frequ�ncia inferior a 75% (-75%)');
    Body.Lines.Add ('2 - Sem Informa��o de Frequ�ncia');
    Body.Lines.Add ('3 - Para aluno com frequ�ncia dentro do esperado');
    Body.Lines.Add ('');
    Body.Lines.Add ('Pedimos que os dados sejam alterados , salvos e devolvidos atrav�s desse mesmo arquivo e encaminhado');
    Body.Lines.Add ('para o e-mail  dadosescolares@fundhas.org.br.');
    Body.Lines.Add ('');
    Body.Lines.Add ('');
    Body.Lines.Add ('Desde j� agradecemos a vossa colabora��o e colocamo-nos a disposi��o para quaisquer esclarecimentos.');
    Body.Lines.Add ('Att;');
    Body.Lines.Add ('Equipe Acompanhamento Escolar');
    Body.Lines.Add ('FUNDHAS - Funda��o H�lio Augusto de Souza');
    }

    {Anexando o arquivo}
    Attachments.Add (OpenDialog1.FileName);
  end;

  NMSMTP1.SendMail;
  NMSMTP1.Disconnect;
  Application.MessageBox ('Aten��o !!!' + #13+#10 +
                          'Seu e-mail foi enviado ao destinat�rio com sucesso',
                          '[Sistema Deca] - Enviando e-Mail...',
                          MB_OK + MB_ICONWARNING);
  frmEnviarEmail.Close;
except
  Application.MessageBox ('Aten��o !!!' + #13+#10 +
                          'Ocorreu um erro ao tentar enviar o e-mail. Favor entrar em contato com o Administrador do Sistema ou o Suporte T�cnico',
                          '[Sistema Deca] - Erro enviando e-Mail...',
                          MB_OK + MB_ICONWARNING);
  Exit;
end;

end;

procedure TfrmEnviarEmail.FormShow(Sender: TObject);
begin
  meMensagem.Clear;
  meMensagem.Lines.Add ('Sr(a) Diretor(a).');
  meMensagem.Lines.Add ('');
  meMensagem.Lines.Add ('Segue anexa planilha com a rela��o de crian�as e adolescentes da FUNDHAS cadastradas');
  meMensagem.Lines.Add ('em nosso sistema como sendo dessa escola. Pedimos, gentilmente, que alterem as colunas');
  meMensagem.Lines.Add ('"APROVEITAMENTO" e "FREQU�NCIA" de acordo com os valores abaixo:');
  meMensagem.Lines.Add ('');
  meMensagem.Lines.Add ('Aproveitamento:');
  meMensagem.Lines.Add ('0 - Para aluno Abaixo da M�dia');
  meMensagem.Lines.Add ('1 - Para aluno Acima da M�dia');
  meMensagem.Lines.Add ('2 - Sem Informa��o de Aproveitamento');
  meMensagem.Lines.Add ('3 - Para aluno Dentro da M�dia');
  meMensagem.Lines.Add ('');
  meMensagem.Lines.Add ('Frequ�ncia:');
  meMensagem.Lines.Add ('0 - Para aluno com frequ�ncia superior a 75% (+75%)');
  meMensagem.Lines.Add ('1 - Para aluno com frequ�ncia inferior a 75% (-75%)');
  meMensagem.Lines.Add ('2 - Sem Informa��o de Frequ�ncia');
  meMensagem.Lines.Add ('3 - Para aluno com frequ�ncia dentro do esperado');
  meMensagem.Lines.Add ('');
  meMensagem.Lines.Add ('Pedimos que os dados sejam alterados , salvos e devolvidos atrav�s desse mesmo arquivo e encaminhado');
  meMensagem.Lines.Add ('para o e-mail  dadosescolares@fundhas.org.br.');
  meMensagem.Lines.Add ('');
  meMensagem.Lines.Add ('');
  meMensagem.Lines.Add ('Desde j� agradecemos a vossa colabora��o e colocamo-nos a disposi��o para quaisquer esclarecimentos.');
  meMensagem.Lines.Add ('Att;');
  meMensagem.Lines.Add ('Equipe Acompanhamento Escolar');
  meMensagem.Lines.Add ('FUNDHAS - Funda��o H�lio Augusto de Souza');
end;

procedure TfrmEnviarEmail.RadioGroup1Click(Sender: TObject);
begin
  case RadioGroup1.ItemIndex of
    0: begin
      meMensagem.Clear;
      meMensagem.Lines.Add ('Sr(a) Diretor(a).');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Segue anexa planilha com a rela��o de crian�as e adolescentes da FUNDHAS cadastradas');
      meMensagem.Lines.Add ('em nosso sistema como sendo dessa escola. Pedimos, gentilmente, que alterem as colunas');
      meMensagem.Lines.Add ('"APROVEITAMENTO" e "FREQU�NCIA" de acordo com os valores abaixo:');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Aproveitamento:');
      meMensagem.Lines.Add ('0 - Para aluno Abaixo da M�dia');
      meMensagem.Lines.Add ('1 - Para aluno Acima da M�dia');
      meMensagem.Lines.Add ('2 - Sem Informa��o de Aproveitamento');
      meMensagem.Lines.Add ('3 - Para aluno Dentro da M�dia');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Frequ�ncia:');
      meMensagem.Lines.Add ('0 - Para aluno com frequ�ncia superior a 75% (+75%)');
      meMensagem.Lines.Add ('1 - Para aluno com frequ�ncia inferior a 75% (-75%)');
      meMensagem.Lines.Add ('2 - Sem Informa��o de Frequ�ncia');
      meMensagem.Lines.Add ('3 - Para aluno com frequ�ncia dentro do esperado');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Pedimos que os dados sejam alterados , salvos e devolvidos atrav�s desse mesmo arquivo e encaminhado');
      meMensagem.Lines.Add ('para o e-mail  dadosescolares@fundhas.org.br.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Desde j� agradecemos a vossa colabora��o e colocamo-nos a disposi��o para quaisquer esclarecimentos.');
      meMensagem.Lines.Add ('Att;');
      meMensagem.Lines.Add ('Equipe Acompanhamento Escolar');
      meMensagem.Lines.Add ('FUNDHAS - Funda��o H�lio Augusto de Souza');
    end;

    1: begin
      meMensagem.Clear;
    end;

  end;
end;

end.
