unit uSelecionaFrequenciaPorMatricula;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, Db, Grids, DBGrids;

type
  TfrmSelecionaFrequenciaPorMatricula = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    btnPesquisar2: TSpeedButton;
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnPesquisar2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaFrequenciaPorMatricula: TfrmSelecionaFrequenciaPorMatricula;

implementation

uses uDM, uFichaPesquisa, rFolhaFrequencia, uPrincipal;

{$R *.DFM}

procedure TfrmSelecionaFrequenciaPorMatricula.btnImprimirClick(
  Sender: TObject);
var num_id : Integer;
begin

  num_id := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('id_frequencia').AsInteger;

  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').AsInteger := num_id;
      Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_num_ano').Value := Null;
      Params.ParamByName ('@pe_num_mes').Value := Null;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;

      Application.CreateForm (TrelFolhaFrequencia, relFolhaFrequencia);
      relFolhaFrequencia.lbMesAno.Caption := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('Mes').Value;
      relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA  ' + vvNOM_UNIDADE;

      with dmDeca.cdsSel_Cadastro_Turmas do
      begin
        Close;
        Params.ParamByName ('@pe_cod_turma').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_turma').Value;//Null;
        Params.ParamByName ('@pe_nom_turma').Value := Null;//cbTurma.Text;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Open;
        relFolhaFrequencia.lbTurma.Caption := FieldByName('nom_turma').Value + '     Professor(a): ' + FieldByName ('nom_professor').AsString + '     Per�odo: ' + FieldByname ('pPeriodo').AsString;
      end;

      relFolhaFrequencia.Preview;
      relFolhaFrequencia.Free;

      txtMatricula.Clear;
      lbNome.Caption := '';
      DBGrid1.DataSource := nil;
      btnImprimir.Enabled := False;

    end;
  except end;

end;
procedure TfrmSelecionaFrequenciaPorMatricula.btnSairClick(
  Sender: TObject);
begin
  frmSelecionaFrequenciaPorMatricula.Close;
end;

procedure TfrmSelecionaFrequenciaPorMatricula.txtMatriculaExit(
  Sender: TObject);
begin
if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carrega o DBGrid com todas as frequ�ncias da crian�a/adolescente selecionado
        try
          with dmDeca.cdsSel_Cadastro_Frequencia do
          begin
            Close;
            Params.ParamByName ('@pe_id_frequencia').Value := Null;
            Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_unidade').Value := Null; //vvCOD_UNIDADE;
            Params.ParamByName ('@pe_num_ano').Value := Null;//StrToInt(cbAno.Text);
            Params.ParamByName ('@pe_num_mes').Value := Null; //12;
            Params.ParamByName ('@pe_cod_turma').Value := Null; //StrToInt(vTurma1.Strings[cbTurma.ItemIndex]);
            Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
            Open;

            DBGrid1.DataSource := DataSource1;
            DBGrid1.Refresh;

            btnImprimir.Enabled := True;
            btnImprimir.SetFocus;

          end;
        except end;
      end;
    except end;
  end;
end;

procedure TfrmSelecionaFrequenciaPorMatricula.Button1Click(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := Trim(txtMatricula.Text);
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carrega o DBGrid com todas as frequ�ncias da crian�a/adolescente selecionado
        try
          with dmDeca.cdsSel_Cadastro_Frequencia do
          begin
            Close;
            Params.ParamByName ('@pe_id_frequencia').Value := Null;
            Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_unidade').Value := Null; //vvCOD_UNIDADE;
            Params.ParamByName ('@pe_num_ano').Value := Null;//StrToInt(cbAno.Text);
            Params.ParamByName ('@pe_num_mes').Value := Null; //12;
            Params.ParamByName ('@pe_cod_turma').Value := Null; //StrToInt(vTurma1.Strings[cbTurma.ItemIndex]);
            Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
            Open;

            DBGrid1.DataSource := DataSource1;
            DBGrid1.Refresh;

            btnImprimir.Enabled := True;
            btnImprimir.SetFocus;

          end;
        except end;
      end;
    except end;
  end;
end;

procedure TfrmSelecionaFrequenciaPorMatricula.btnPesquisar2Click(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := Trim(txtMatricula.Text);
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carrega o DBGrid com todas as frequ�ncias da crian�a/adolescente selecionado
        try
          with dmDeca.cdsSel_Cadastro_Frequencia do
          begin
            Close;
            Params.ParamByName ('@pe_id_frequencia').Value := Null;
            Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_unidade').Value := Null; //vvCOD_UNIDADE;
            Params.ParamByName ('@pe_num_ano').Value := Null;//StrToInt(cbAno.Text);
            Params.ParamByName ('@pe_num_mes').Value := Null; //12;
            Params.ParamByName ('@pe_cod_turma').Value := Null; //StrToInt(vTurma1.Strings[cbTurma.ItemIndex]);
            Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
            Open;

            DBGrid1.DataSource := DataSource1;
            DBGrid1.Refresh;

            btnImprimir.Enabled := True;
            btnImprimir.SetFocus;

          end;
        except end;
      end;
    except end;
  end;
end;

end.
