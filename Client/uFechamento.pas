unit uFechamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Buttons, StdCtrls, Mask, ExtCtrls;

type
  TfrmFechamento = class(TForm)
    GroupBox1: TGroupBox;
    cbMes: TComboBox;
    Label1: TLabel;
    mskAno: TMaskEdit;
    btnGeraFechamento: TSpeedButton;
    ProgressBar1: TProgressBar;
    Label2: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure btnGeraFechamentoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFechamento: TfrmFechamento;

implementation

uses uDM, uPrincipal, uUtil, rFechamentoAcompEscolar;

{$R *.DFM}

procedure TfrmFechamento.btnGeraFechamentoClick(Sender: TObject);
begin

  try

    //Exclir os dados do usu�rio logado atualmente para n�o haver duplicidade na tabel FECHAMENTO
    with dmDeca.cdsExc_Fechamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Execute;
    end;

    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
    end;


    ProgressBar1.Min      := 0;
    ProgressBar1.Max      := dmDeca.cdsSel_Unidade.RecordCount;
    ProgressBar1.Position := 0;
    while not dmDeca.cdsSel_Unidade.eof do
    begin
      //Verifica se a unidade n�o deve ser selecionada... Inativos, maioridade,...
      if (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 1) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 2) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 3) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 7) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 9) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 12) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 13) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 40) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 42) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 43) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 47) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 48) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 50) and
         (dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value <> 73) then
      begin

        //Seleciona os totais para a unidade atual e grava os dados na tabela FECHAMENTO
        //Execute a procedure sp_Fechamento_AcompEscolar
        with dmDeca.cds_FechamentoAcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_unidade').Value      := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;

          case cbMes.ItemIndex of
            1..3:  Params.ParamByName ('@pe_num_bimestre').Value := 1;
            4..6:  Params.ParamByName ('@pe_num_bimestre').Value := 2;
            7..11: Params.ParamByName ('@pe_num_bimestre').Value := 3;
            0:     Params.ParamByName ('@pe_num_bimestre').Value := 3;
          end;

          Params.ParamByName ('@pe_num_bimestre').Value     := 1;
          Params.ParamByName ('@pe_num_ano_letivo').Value   := StrToInt(mskAno.Text);
          Params.ParamByName ('@pe_ind_tipo_unidade').Value := Null;
          Open;

          //Ap�s executar a consulta, os valores devem ser inseridos na tabela FECHAMENTO para o usu�rio atual
          with dmDeca.cdsInc_Fechamento do
          begin
            Close;
            Params.ParamByName ('@pe_cod_usuario').AsInteger    := vvCOD_USUARIO;
            Params.ParamByName ('@pe_num_mes').AsInteger        := cbMes.ItemIndex + 1;
            Params.ParamByName ('@pe_num_ano').AsInteger        := StrToInt(mskAno.Text);
            Params.ParamByName ('@pe_cod_unidade').AsInteger    := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
            Params.ParamByName ('@pe_num_tt_efetivo').AsInteger := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalEfetivoUnidade').Value);
            Params.ParamByName ('@pe_num_tt_cem').AsInteger     := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalConcluintesEnsMedioUnid').Value);
            Params.ParamByName ('@pe_num_tt_aginf').AsInteger   := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalAguardandoInformacaoUnid').Value);
            Params.ParamByName ('@pe_num_tt_matr').AsInteger    := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalMatriculadosUnid').Value);
            Params.ParamByName ('@pe_num_tt_prom').AsInteger    := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalPromovidosUnid').Value);
            Params.ParamByName ('@pe_num_tt_ret').AsInteger     := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalRetidosUnid').Value);
            Params.ParamByName ('@pe_num_tt_sesc').AsInteger    := StrToInt(dmDeca.cds_FechamentoAcompEscolar.FieldByName ('TotalSemEscolaUnid').Value);
            Execute;
          end;

        end;

      end
      else
      begin
        //Faz parte das unidades n�o selecion�veis
      end;
      ProgressBar1.Position := ProgressBar1.Position + 1; 
      dmDeca.cdsSel_Unidade.Next;

    end;

    with dmDeca.cdsSel_Fechamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_fechamento').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
      Params.ParamByName ('@pe_num_mes').Value           := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value           := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_ind_tipo_unidade').Value  := Null;
      Open;
    end;

    Application.MessageBox ('Aten��o!!! ' + #13+#10 +
                            'Os dados do Fechamento do per�odo selecionado foram gerados com sucesso. O relat�rio ser� exibido agora',
                            '[Sistema Deca] - Fechamento Mensal do Acompanhamento Escolar',
                            MB_OK + MB_ICONINFORMATION);
    Application.CreateForm (TrelFechamentoAcompEscolar, relFechamentoAcompEscolar);
    relFechamentoAcompEscolar.lbTituloPeriodo.Caption := cbMes.Text + '/'+mskAno.Text;
    relFechamentoAcompEscolar.Preview;
    relFechamentoAcompEscolar.Free;    
  except end;


end;

end.
