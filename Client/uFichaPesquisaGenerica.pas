unit uFichaPesquisaGenerica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmFichaPesquisaGenerica = class(TForm)
    dbgPesquisa: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edPesquisa: TEdit;
    cbCampoPesquisa: TComboBox;
    rgCriterio: TRadioGroup;
    btnPesquisa: TBitBtn;
    dsSel_Cadastro_Pesquisa: TDataSource;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure rgCriterioClick(Sender: TObject);
    procedure edPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure LimpaTela;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure cbCampoPesquisaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFichaPesquisaGenerica: TfrmFichaPesquisaGenerica;

implementation

uses uFichaPesquisa, uDM;

{$R *.DFM}

procedure TfrmFichaPesquisaGenerica.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmFichaPesquisa.Close;
  end;
end;

procedure TfrmFichaPesquisaGenerica.rgCriterioClick(Sender: TObject);
begin
  LimpaTela;
end;

procedure TfrmFichaPesquisaGenerica.edPesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
  LimpaTela;
end;

procedure TfrmFichaPesquisaGenerica.LimpaTela;
begin
  // limpa o DBGrid
  dmDeca.cdsSel_Cadastro_Pesquisa.Close;
  dbgPesquisa.Refresh;
end;

procedure TfrmFichaPesquisaGenerica.FormShow(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Cadastro_Pesquisa do
    begin
      Close;
      Params.ParamByName('@pe_tipo').Value := 1;
      Params.ParamByName('@pe_nom_nome').Value := NULL;
      Params.ParamByName('@pe_cod_matricula').Value := NULL;
      Params.ParamByName('@pe_num_prontuario').Value := NULL;
      Params.ParamByName('@pe_nom_responsavel').Value := NULL;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Open
    end;
  except end;
end;

procedure TfrmFichaPesquisaGenerica.btnPesquisaClick(Sender: TObject);
var vCritPesq : String;
begin
  if Length(Trim(edPesquisa.Text)) > 0 then
  begin

    if rgCriterio.ItemIndex = 0 then
      vCritPesq := ''
    else
      vCritPesq := '%';

    try
      with dmDeca.cdsSel_Cadastro_Pesquisa do
      begin
        Close;
        Case cbCampoPesquisa.ItemIndex of
          0:                            // cod_matricula
          begin
            Params.ParamByName('@pe_tipo').Value := 1;
            Params.ParamByName('@pe_nom_nome').Value := NULL;
            Params.ParamByName('@pe_cod_matricula').Value := (edPesquisa.Text) + '%';
            Params.ParamByName('@pe_num_prontuario').Value := NULL;
            Params.ParamByName('@pe_nom_responsavel').Value := NULL;
            Params.ParamByName('@pe_cod_unidade').Value := 0; //Modificado para Null
          end;
          1:                            // cod_prontuario
          begin
            Params.ParamByName('@pe_tipo').Value := 1;
            Params.ParamByName('@pe_nom_nome').Value := NULL;
            Params.ParamByName('@pe_cod_matricula').Value := NULL;
            Params.ParamByName('@pe_num_prontuario').Value := StrToInt(edPesquisa.Text);
            Params.ParamByName('@pe_nom_responsavel').Value := NULL;
            Params.ParamByName('@pe_cod_unidade').Value := 0;
          end;
          2:                            // nom_nome - nome da crian�a
          begin
            Params.ParamByName('@pe_tipo').Value := 1;
            Params.ParamByName('@pe_nom_nome').Value := vCritPesq + (edPesquisa.Text) + '%';
            Params.ParamByName('@pe_cod_matricula').Value := NULL;
            Params.ParamByName('@pe_num_prontuario').Value := NULL;
            Params.ParamByName('@pe_nom_responsavel').Value := NULL;
            Params.ParamByName('@pe_cod_unidade').Value := 0;
          end;
          3:
          begin                         // nom_familiar - nome do responsavel
            Params.ParamByName('@pe_tipo').Value := 2;
            Params.ParamByName('@pe_nom_nome').Value := NULL;
            Params.ParamByName('@pe_cod_matricula').Value := NULL;
            Params.ParamByName('@pe_num_prontuario').Value := NULL;
            Params.ParamByName('@pe_nom_responsavel').Value := vCritPesq + (edPesquisa.Text) + '%';
            Params.ParamByName('@pe_cod_unidade').Value := 0;
          end
        end;
        Open;
        dbgPesquisa.Refresh;

      end;
    except end;
  end
  else
  begin
    try
      with dmDeca.cdsSel_Cadastro_Pesquisa do
      begin
        Close;
        Params.ParamByName('@pe_tipo').Value := 1;
        Params.ParamByName('@pe_nom_nome').Value := NULL;
        Params.ParamByName('@pe_cod_matricula').Value := NULL;
        Params.ParamByName('@pe_num_prontuario').Value := NULL;
        Params.ParamByName('@pe_nom_responsavel').Value := NULL;
        Params.ParamByName('@pe_cod_unidade').Value := Null;
        Open
      end;
      except end;
  end;
end;

procedure TfrmFichaPesquisaGenerica.cbCampoPesquisaChange(Sender: TObject);
begin
  LimpaTela;
  case cbCampoPesquisa.ItemIndex of
    0..1:
    begin
      rgCriterio.ItemIndex := 0;
      rgCriterio.Enabled := false;
    end;
    2..3:
    begin
      rgCriterio.ItemIndex := 1;
      rgCriterio.Enabled := true;
    end
  end;
end;

procedure TfrmFichaPesquisaGenerica.FormCreate(Sender: TObject);
begin
  // posicionar o combobox - campo
  cbCampoPesquisa.ItemIndex := 2;
end;

procedure TfrmFichaPesquisaGenerica.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
