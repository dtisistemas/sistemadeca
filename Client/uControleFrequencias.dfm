object frmControleFrequencia: TfrmControleFrequencia
  Left = 176
  Top = 208
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Controle de Frequ�ncias por Turma]'
  ClientHeight = 412
  ClientWidth = 728
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = -1
    Width = 723
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 14
      Top = 19
      Width = 49
      Height = 16
      Caption = 'Turma:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbTurma: TComboBox
      Left = 69
      Top = 17
      Width = 265
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnClick = cbTurmaClick
    end
  end
  object Panel1: TPanel
    Left = 4
    Top = 361
    Width = 723
    Height = 49
    TabOrder = 1
    object btnCalcularFaltas: TSpeedButton
      Left = 520
      Top = 10
      Width = 65
      Height = 29
      Glyph.Data = {
        02030000424D0203000000000000360100002800000013000000170000000100
        080000000000CC010000C30E0000C30E000040000000000000001C3404002434
        1C00242424001C3C0400243C0C00244404002C5C04003C5C240044543C005C5C
        54005C5C5C00646464006C6C6C0054743C007474740044840400747C74007C7C
        7C0084848400449404006C8C540054AC0400000000008C8C8C008C948C009494
        94009C9C9C00A4A4A400ACACAC00B4B4B4006CD404006CDC040074F404007CFC
        040084FC0C0084FC14007CDC24008CFC1C008CFC240094FC240094EC3C0094FC
        2C009CFC3C0094D45C009CF44C009CFC4400A4FC4C00A4FC5400ACFC6400B4FC
        6C00B4F47400BCF48400BCFC7C00B4C4A400ACCC9400BCCCAC00BCC4B400BCCC
        B400B4E48C00BCE49400BCDCA400C4F49400C4FC8C00C0C0C0003F3F3F3F3F3F
        191717193F3F3F3F3F3F3F3F3F003F3F3F3F3F1712111112193F3F3F3F3F3F3F
        3F003F3F3F3F19120E0C0C0E123F3F3F3F3F3F3F3F003F3F3F3F120E0C0B0B0C
        11173F3F3F3F3F3F3F003F3F3F17110C0B0A0A0B0E123F3F3F3F3F3F3F003F3F
        3F12140702010B0B0C11173F3F3F3F3F3F003F3F3F181E1E0F03100C0C0E1219
        3F3F3F3F3F003F3F3F2422231F06080C0C0C11173F3F3F3F3F003F3F2B212223
        221305170C0C0E11173F3F3F3F003F3521222323231E06090E0C0C0E12193F3F
        3F003F2B2223272726221304180E0C0C0E123F3F3F003F2926252A2F2F261F06
        08110E0C0E11173F3F0038302D232C39332E23150311110E0C0E11173F003F39
        2E28383F37312A220F0117110E0E0E1219003F3F373F3F3F3F3A30261E060917
        110E0E1117003F3F3F3F3F3F3F3F322E2315030C1712111217003F3F3F3F3F3F
        3F3F37342D2313001819171719003F3F3F3F3F3F3F3F3F3B342E231300193F3F
        3F003F3F3F3F3F3F3F3F3F3F3C3330230F011D3F3F003F3F3F3F3F3F3F3F3F3F
        3F393E31250F0D3F3F003F3F3F3F3F3F3F3F3F3F3F3F383D312320353F003F3F
        3F3F3F3F3F3F3F3F3F3F3F3F3C2A23363F003F3F3F3F3F3F3F3F3F3F3F3F3F3F
        3F3F373F3F00}
      OnClick = btnCalcularFaltasClick
    end
    object btnSair: TBitBtn
      Left = 658
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnAlterar: TBitBtn
      Left = 591
      Top = 10
      Width = 55
      Height = 30
      Hint = 
        'Clique para GRAVAR os dados do registro de Atendimento informado' +
        's no formul�rio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnAlterarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF7F3F3FBF7F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBF7F3FBFBF9FFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
        3F3FBF9F5FBFBF7F9F3F3F7F3F3FBF7F3FBF9F5FFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF7F3F3FBF9F5FBFDF9FBFBF7FBFBF7FBFBF7FBF9F
        5F9F3F3FBF9F5FFFFFFFFFFFFF7FBFBF3F7F9F7FBFBFFFFFFFFFFFFFFFFFFF9F
        3F3FBF9F5FBFDF9FBF7F3F9F3F3FBF7F3FBFBF7F9F3F3FBF9F5F7FBFBF3F7F9F
        7FBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3F3FBF9F5FBF7F3FBFBF9FBF9F
        5F9F3F3FBFBF7FBF7F3F5F9F9F3F7F9FBFDFDFFFFFFFFFFFFF3F5F7FFFFFFFFF
        FFFFFFFFFF9F3F3FBF7F3FFFFFFFFFFFFFBF9F5FBF9F5F9F3F3F3F7F7F3FBFFF
        3F9FDF9FDFDFFFFFFF3F5F7F3F5F7FFFFFFFFFFFFFFFFFFF9F3F3FFFFFFFFFFF
        FFFFFFFF9F3F3FBF7F3F5F9F9F3FDFFF3F5F7F7FBFBF9FDFDF3F5F7F3F9FDF3F
        5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBF9F9F3F3FBFBF9F7FBFBF3F7F7F
        3FFFFF3F7F9F3F5F7F3F7F9F3FDFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFBFBF
        9F9F3F3FBFBF9FFFFFFFFFFFFF5F9F9F3F7F7F3FFFFF3FFFFF3FFFFF3FFFFF3F
        FFFF3FDFFF3F5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        7FBFBF5F9F9F3F7F7F5F9F9F9FFFFFFFFFFF3F7F7FFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFF3F
        7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF3F7F7F3F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7F7FFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  object Panel2: TPanel
    Left = 4
    Top = 52
    Width = 723
    Height = 306
    BevelInner = bvLowered
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label2: TLabel
      Left = 32
      Top = 224
      Width = 24
      Height = 14
      Caption = 'Dias:'
    end
    object Label3: TLabel
      Left = 26
      Top = 248
      Width = 32
      Height = 14
      Caption = 'Faltas:'
    end
    object Label4: TLabel
      Left = 154
      Top = 248
      Width = 113
      Height = 14
      Caption = 'Porcentagem de Faltas:'
      Visible = False
    end
    object Label5: TLabel
      Left = 66
      Top = 205
      Width = 12
      Height = 14
      Caption = '01'
    end
    object Label6: TLabel
      Left = 86
      Top = 205
      Width = 12
      Height = 14
      Caption = '02'
    end
    object Label7: TLabel
      Left = 107
      Top = 205
      Width = 12
      Height = 14
      Caption = '03'
    end
    object Label8: TLabel
      Left = 127
      Top = 205
      Width = 12
      Height = 14
      Caption = '04'
    end
    object Label9: TLabel
      Left = 147
      Top = 205
      Width = 12
      Height = 14
      Caption = '05'
    end
    object Label10: TLabel
      Left = 168
      Top = 205
      Width = 12
      Height = 14
      Caption = '06'
    end
    object Label11: TLabel
      Left = 189
      Top = 205
      Width = 12
      Height = 14
      Caption = '07'
    end
    object Label12: TLabel
      Left = 208
      Top = 205
      Width = 12
      Height = 14
      Caption = '08'
    end
    object Label13: TLabel
      Left = 229
      Top = 205
      Width = 12
      Height = 14
      Caption = '09'
    end
    object Label14: TLabel
      Left = 249
      Top = 205
      Width = 12
      Height = 14
      Caption = '10'
    end
    object Label15: TLabel
      Left = 269
      Top = 205
      Width = 12
      Height = 14
      Caption = '11'
    end
    object Label16: TLabel
      Left = 289
      Top = 205
      Width = 12
      Height = 14
      Caption = '12'
    end
    object Label17: TLabel
      Left = 309
      Top = 205
      Width = 12
      Height = 14
      Caption = '13'
    end
    object Label18: TLabel
      Left = 330
      Top = 205
      Width = 12
      Height = 14
      Caption = '14'
    end
    object Label19: TLabel
      Left = 350
      Top = 205
      Width = 12
      Height = 14
      Caption = '15'
    end
    object Label20: TLabel
      Left = 370
      Top = 205
      Width = 12
      Height = 14
      Caption = '16'
    end
    object Label21: TLabel
      Left = 390
      Top = 205
      Width = 12
      Height = 14
      Caption = '17'
    end
    object Label22: TLabel
      Left = 411
      Top = 205
      Width = 12
      Height = 14
      Caption = '18'
    end
    object Label23: TLabel
      Left = 431
      Top = 205
      Width = 12
      Height = 14
      Caption = '19'
    end
    object Label24: TLabel
      Left = 452
      Top = 205
      Width = 12
      Height = 14
      Caption = '20'
    end
    object Label25: TLabel
      Left = 472
      Top = 205
      Width = 12
      Height = 14
      Caption = '21'
    end
    object Label26: TLabel
      Left = 492
      Top = 205
      Width = 12
      Height = 14
      Caption = '22'
    end
    object Label27: TLabel
      Left = 513
      Top = 205
      Width = 12
      Height = 14
      Caption = '23'
    end
    object Label28: TLabel
      Left = 534
      Top = 205
      Width = 12
      Height = 14
      Caption = '24'
    end
    object Label29: TLabel
      Left = 553
      Top = 205
      Width = 12
      Height = 14
      Caption = '25'
    end
    object Label30: TLabel
      Left = 572
      Top = 205
      Width = 12
      Height = 14
      Caption = '26'
    end
    object Label31: TLabel
      Left = 592
      Top = 205
      Width = 12
      Height = 14
      Caption = '27'
    end
    object Label32: TLabel
      Left = 613
      Top = 205
      Width = 12
      Height = 14
      Caption = '28'
    end
    object Label33: TLabel
      Left = 634
      Top = 205
      Width = 12
      Height = 14
      Caption = '29'
    end
    object Label34: TLabel
      Left = 655
      Top = 205
      Width = 12
      Height = 14
      Caption = '30'
    end
    object Label35: TLabel
      Left = 675
      Top = 205
      Width = 12
      Height = 14
      Caption = '31'
    end
    object Label36: TLabel
      Left = 119
      Top = 278
      Width = 5
      Height = 16
      Caption = '*'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label37: TLabel
      Left = 26
      Top = 279
      Width = 88
      Height = 14
      Caption = 'Total de DSR M�s:'
    end
    object DBGrid1: TDBGrid
      Left = 6
      Top = 7
      Width = 712
      Height = 191
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Arial'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Alignment = taCenter
          Title.Caption = '[Matr.]'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = '[Nome]'
          Width = 156
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_ano'
          Title.Alignment = taCenter
          Title.Caption = '[Ano]'
          Width = 46
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Mes'
          Title.Alignment = taCenter
          Title.Caption = '[M�s]'
          Width = 82
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_faltas'
          Title.Alignment = taCenter
          Title.Caption = 'Faltas'
          Width = 42
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia01'
          Title.Alignment = taCenter
          Title.Caption = '01'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia02'
          Title.Alignment = taCenter
          Title.Caption = '02'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia03'
          Title.Alignment = taCenter
          Title.Caption = '03'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia04'
          Title.Alignment = taCenter
          Title.Caption = '04'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia05'
          Title.Alignment = taCenter
          Title.Caption = '05'
          Width = 18
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia06'
          Title.Alignment = taCenter
          Title.Caption = '06'
          Width = 18
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia07'
          Title.Alignment = taCenter
          Title.Caption = '07'
          Width = 18
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia08'
          Title.Alignment = taCenter
          Title.Caption = '08'
          Width = 17
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia09'
          Title.Alignment = taCenter
          Title.Caption = '09'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia10'
          Title.Alignment = taCenter
          Title.Caption = '10'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia11'
          Title.Alignment = taCenter
          Title.Caption = '11'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia12'
          Title.Alignment = taCenter
          Title.Caption = '12'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia13'
          Title.Alignment = taCenter
          Title.Caption = '13'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia14'
          Title.Alignment = taCenter
          Title.Caption = '14'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia15'
          Title.Alignment = taCenter
          Title.Caption = '15'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia16'
          Title.Alignment = taCenter
          Title.Caption = '16'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia17'
          Title.Alignment = taCenter
          Title.Caption = '17'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia18'
          Title.Alignment = taCenter
          Title.Caption = '18'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia19'
          Title.Alignment = taCenter
          Title.Caption = '19'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia20'
          Title.Alignment = taCenter
          Title.Caption = '20'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia21'
          Title.Alignment = taCenter
          Title.Caption = '21'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia22'
          Title.Alignment = taCenter
          Title.Caption = '22'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia23'
          Title.Alignment = taCenter
          Title.Caption = '23'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia24'
          Title.Alignment = taCenter
          Title.Caption = '24'
          Width = 18
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia25'
          Title.Alignment = taCenter
          Title.Caption = '25'
          Width = 18
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia26'
          Title.Alignment = taCenter
          Title.Caption = '26'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia27'
          Title.Alignment = taCenter
          Title.Caption = '27'
          Width = 19
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia28'
          Title.Alignment = taCenter
          Title.Caption = '28'
          Width = 21
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia29'
          Title.Alignment = taCenter
          Title.Caption = '29'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia30'
          Title.Alignment = taCenter
          Title.Caption = '30'
          Width = 20
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'flg_dia31'
          Title.Alignment = taCenter
          Title.Caption = '31'
          Width = 20
          Visible = True
        end>
    end
    object edDia01: TEdit
      Left = 62
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 1
      OnExit = edDia01Exit
    end
    object edDia02: TEdit
      Left = 82
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 2
    end
    object edDia03: TEdit
      Left = 103
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 3
    end
    object edDia06: TEdit
      Left = 164
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 4
    end
    object edDia04: TEdit
      Left = 123
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 5
    end
    object edDia05: TEdit
      Left = 143
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 6
    end
    object edDia09: TEdit
      Left = 225
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 7
    end
    object edDia07: TEdit
      Left = 184
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 8
    end
    object edDia08: TEdit
      Left = 204
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 9
    end
    object edDia10: TEdit
      Left = 245
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 10
    end
    object edDia11: TEdit
      Left = 265
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 11
    end
    object edDia12: TEdit
      Left = 285
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 12
    end
    object edDia13: TEdit
      Left = 306
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 13
    end
    object edDia14: TEdit
      Left = 326
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 14
    end
    object edDia15: TEdit
      Left = 346
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 15
    end
    object edDia16: TEdit
      Left = 367
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 16
    end
    object edDia17: TEdit
      Left = 387
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 17
    end
    object edDia18: TEdit
      Left = 407
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 18
    end
    object edDia19: TEdit
      Left = 427
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 19
    end
    object edDia20: TEdit
      Left = 447
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 20
    end
    object edDia21: TEdit
      Left = 467
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 21
    end
    object edDia22: TEdit
      Left = 487
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 22
    end
    object edDia23: TEdit
      Left = 508
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 23
    end
    object edDia24: TEdit
      Left = 528
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 24
    end
    object edDia25: TEdit
      Left = 548
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 25
    end
    object edDia26: TEdit
      Left = 568
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 26
    end
    object edDia27: TEdit
      Left = 588
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 27
    end
    object edDia28: TEdit
      Left = 609
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 28
    end
    object edDia29: TEdit
      Left = 631
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 29
    end
    object edDia30: TEdit
      Left = 651
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 30
    end
    object edDia31: TEdit
      Left = 671
      Top = 221
      Width = 19
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 31
    end
    object edFaltas: TEdit
      Left = 62
      Top = 245
      Width = 39
      Height = 22
      Enabled = False
      TabOrder = 32
    end
    object edPorcentagem: TEdit
      Left = 272
      Top = 245
      Width = 39
      Height = 22
      Enabled = False
      TabOrder = 33
      Visible = False
    end
  end
  object DataSource1: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Frequencia
    Left = 684
    Top = 73
  end
end
