unit uLocalizaPatrimonio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, ExtCtrls, Db;

type
  TfrmLocalizaPatrimonio = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cbCampo: TComboBox;
    Label2: TLabel;
    edValor: TEdit;
    btnPesquisar: TButton;
    Panel1: TPanel;
    dbgEquipamentos: TDBGrid;
    dsEquipamentos: TDataSource;
    procedure dbgEquipamentosDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbCampoChange(Sender: TObject);
    procedure cbCampoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLocalizaPatrimonio: TfrmLocalizaPatrimonio;

implementation

uses uDM, uAberturaChamadoTecnico, uUtil;

{$R *.DFM}

procedure TfrmLocalizaPatrimonio.dbgEquipamentosDblClick(Sender: TObject);
begin
  frmLocalizaPatrimonio.Close;
end;

procedure TfrmLocalizaPatrimonio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmLocalizaPatrimonio.cbCampoChange(Sender: TObject);
begin
  edValor.Clear;
end;

procedure TfrmLocalizaPatrimonio.cbCampoClick(Sender: TObject);
begin
  edValor.SetFocus;
end;

end.
