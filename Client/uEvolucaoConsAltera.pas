unit uEvolucaoConsAltera;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, Db, Grids, DBGrids, ExtCtrls;

type
  TfrmEvolucaoConsAltera = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    txtDescricao: TMemo;
    dsSel_Cadastro_Evolucao_SC: TDataSource;
    Panel1: TPanel;
    btnAlterar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    DBGrid1: TDBGrid;
    Label3: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtMatriculaExit(Sender: TObject);
    procedure AtualizaCamposBotoes (Modo : String);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEvolucaoConsAltera: TfrmEvolucaoConsAltera;

implementation

uses uDM, uFichaPesquisa, uPrincipal;

{$R *.DFM}

procedure TfrmEvolucaoConsAltera.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Recupera dados da Evolu��o Servi�o Social, caso haja
        with dmDeca.cdsSel_Cadastro_Evolucao_SC do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_evolucao').Value  := Null;
            Params.ParamByName('@pe_cod_matricula').AsString := vvCOD_MATRICULA_PESQUISA;
            Params.ParamByName('@pe_cod_usuario').Value      := Null;  //VVCOD_USUARIO;
            Open;
            
            DBGrid1.DataSource := dsSel_Cadastro_Evolucao_SC;
            DBGrid1.Refresh;
            AtualizaCamposBotoes ('Alterar');
          end;
      end;
    except end
end;
end;

procedure TfrmEvolucaoConsAltera.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmEvolucaoConsAltera.Close;
  end;
end;

procedure TfrmEvolucaoConsAltera.btnSairClick(Sender: TObject);
begin
  frmEvolucaoConsAltera.Close;
end;

procedure TfrmEvolucaoConsAltera.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmEvolucaoConsAltera.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmEvolucaoConsAltera.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEvolucaoConsAltera.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
        begin
          lbNome.Caption := FieldByName('nom_nome').AsString;

          //Carrega as evolu��es sociais referentes a matr�cula
          //na unidade atual

          //Recupera dados da Evolu��o Servi�o Social, caso haja
          with dmDeca.cdsSel_Cadastro_Evolucao_SC do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_evolucao').Value  := Null;
            Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
            Params.ParamByName ('@pe_cod_usuario').Value      := Null; //VVCOD_USUARIO;
            Open;
            DBGrid1.DataSource := dsSel_Cadastro_Evolucao_SC;
            DBGrid1.Refresh;
            AtualizaCamposBotoes ('Alterar');
          end;
        end;
      end;
    except end;
  end;
end;

procedure TfrmEvolucaoConsAltera.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    txtMatricula.Clear;
    txtMatricula.Enabled := True;
    lbNome.Caption       := '';
    btnPesquisar.Enabled := True;
    DBGrid1.DataSource   := Nil;
    DBGrid1.Enabled      := False;
    txtDescricao.Lines.Clear;
    btnAlterar.Enabled   := False;
    btnCancelar.Enabled  := False;
    btnSair.Enabled      := True;
  end
  else if Modo = 'Alterar' then
  begin
    DBGrid1.Enabled     := True;
    btnAlterar.Enabled  := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled     := False;
  end;
end;

procedure TfrmEvolucaoConsAltera.DBGrid1DblClick(Sender: TObject);
begin

  //Se o registro pertence ao usu�rio  logado, permite edi��o dos dados, caso contr�rio apenas visualiza....
  if (dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByName ('log_cod_usuario').Value = vvCOD_USUARIO) then
  begin
    txtDescricao.Text := dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByName ('dsc_comentario').Value;
    btnAlterar.Enabled := True
  end
  else
  begin
    txtDescricao.Text := dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByName ('dsc_comentario').Value;
    btnAlterar.Enabled := False;
  end;
end;

procedure TfrmEvolucaoConsAltera.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Cadastro_Evolucao_SC do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').AsString    := txtMatricula.Text;
      Params.ParamByName ('@pe_cod_id_evolucao').AsInteger := dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByname ('cod_id_evolucao').AsInteger;
      Params.ParamByName ('@pe_dat_evolucao').AsDateTime   := dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByname ('Data').AsDateTime;
      Params.ParamByName ('@pe_dsc_comentario').AsString   := txtDescricao.Lines.Text;
      Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Params.ParamByName ('@pe_log_data').AsDateTime       := dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByname ('log_data').AsDateTime;
      Execute;

      with dmDeca.cdsSel_Cadastro_Evolucao_SC do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value   := Trim(txtMatricula.Text);
          Params.ParamByName ('@pe_cod_id_evolucao').Value := Null;
          Params.ParamByName ('@pe_cod_usuario').Value     := Null;
          Open;
          DBGrid1.Refresh;

          AtualizaCamposBotoes ('Padr�o');

        end;
    end
  except

    //Gravar no arquivo de LOG de conex�o os dados registrados na tela
    arqLog := 'EV_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                      Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
    AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
    Rewrite (log_file);

    Write   (log_file, txtMatricula.Text);        //Grava a matricula
    Write   (log_file, ' ');
    Write   (log_file, lbNome.Caption);
    Write   (log_file, ' ');
    Write   (log_file, DateToStr(dmDeca.cdsSel_Cadastro_Evolucao_SC.FieldByname ('Data').AsDateTime));             //Data do registro
    Write   (log_file, ' ');
    Write   (log_file, 'Registro de Atendimento-Inclus�o');//Descricao do Tipo de Historico
    Writeln (log_file, ' ');
    Writeln (log_file, Trim(txtDescricao.Text)); //Ocorrencia do Historico
    Writeln (log_file, '--------------------------------------------------------------------------------------');

    CloseFile (log_file);

    Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar alterar dados da Evolu��o!'+#13+#10+#13+#10+
                           'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                           'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                           'Sistema DECA - Altera��o de Registro',
                           MB_OK + MB_ICONERROR);
    //Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar alterar dados!'+#13+#10+#13+#10+
    //         'Os dados da Evolu��o n�o foram alterados. Favor entrar em contato com o Administrador do Sistema.',
    //         'Sistema DECA - Altera��o de Evolu��o',
    //         MB_OK + MB_ICONERROR);
    txtMatricula.Clear;
    txtMatricula.SetFocus;
  end;
end;

end.
