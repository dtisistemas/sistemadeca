unit uResumoMensal_Teste;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg, StdCtrls, ComCtrls, Grids, DBGrids, Buttons, Mask,
  ImgList, Db, QuickRpt;

type
  TfrmResumoMensal_Teste = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edPrograma: TEdit;
    Label2: TLabel;
    edUnidade: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    dbgResumos: TDBGrid;
    btnEncerrarResumoMes: TSpeedButton;
    btnReverterResumoMes: TSpeedButton;
    btnNovo: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnImprimir: TSpeedButton;
    btnSair: TSpeedButton;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label4: TLabel;
    edNumTTTransferidosDEManha: TEdit;
    edNumTTPetiManha: TEdit;
    edNumTTTransferidosPARAManha: TEdit;
    edNumTTTransferidosParaArteEducManha: TEdit;
    edNumTTTransferidosParaAprendizManha: TEdit;
    edNumTTDesligadosManha: TEdit;
    edNumTTAfastados: TEdit;
    edNumTTInicioMesManha: TEdit;
    edNumTTAdmitidosManha: TEdit;
    GroupBox7: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    GroupBox12: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    GroupBox13: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    GroupBox14: TGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    GroupBox15: TGroupBox;
    GroupBox16: TGroupBox;
    GroupBox17: TGroupBox;
    cbGrauAproveitamentoNoPrograma: TComboBox;
    cbGrauDesempenhoPratica: TComboBox;
    edNumTTDesligadosAPedidoMes: TEdit;
    edNumTTDesligadosAbandonoMes: TEdit;
    edNumTTDesligadosMaioridadeMes: TEdit;
    edNumTTDesligadosLeiMes: TEdit;
    edNumTTDesligadosOutrosMes: TEdit;
    GroupBox18: TGroupBox;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    GroupBox19: TGroupBox;
    edNumTTSitDisciplinarMes: TEdit;
    edNumTTSitSocioEconomicaMes: TEdit;
    edNumTTSitEscolarMes: TEdit;
    edNumTTSitViolenciaMes: TEdit;
    edNumTTSitAbrigoMes: TEdit;
    edNumTTSitInteracaoRelacMes: TEdit;
    edNumTTSitAtoInfracionalMes: TEdit;
    edNumTTSitAprendizagemMes: TEdit;
    edNumTTSitSaudeMes: TEdit;
    edNumTTSitGuardaMes: TEdit;
    edNumTTSitGravidezMes: TEdit;
    edNumTTSitMoradiaMes: TEdit;
    edNumTTSitTrabInfantilMes: TEdit;
    GroupBox20: TGroupBox;
    edNumTTAtivSocioCultMes: TEdit;
    Label51: TLabel;
    Label52: TLabel;
    meDescricaoAtivSocioCulturais: TMemo;
    GroupBox21: TGroupBox;
    Label53: TLabel;
    Label54: TLabel;
    edNumTTAtivRecreativasMes: TEdit;
    meDescricaoAtivRecreativasMes: TMemo;
    GroupBox22: TGroupBox;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    GroupBox23: TGroupBox;
    edNumTTAdolAptosAprendizagemProfissionalMes: TEdit;
    edNumTTAdolEncaminhadosAprendizagemProfissionalMes: TEdit;
    edNumTTAdolInseridosAprendizagemProfissionalMes: TEdit;
    edNumTTEmAprendizagemFUNDHAS: TEdit;
    edNumTTEmAprendizagemExterna: TEdit;
    TabSheet5: TTabSheet;
    GroupBox24: TGroupBox;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    GroupBox25: TGroupBox;
    GroupBox26: TGroupBox;
    edNumTTAdolAprovadosOPMes: TEdit;
    edNumTTAdolAprovadosOEMes: TEdit;
    edNumTTAdolAprovadosFIC1Mes: TEdit;
    edNumTTAdolAprovadosFIC2Mes: TEdit;
    edNumTTAdolReprovadosOPMes: TEdit;
    edNumTTAdolReprovadosOEMes: TEdit;
    edNumTTAdolReprovadosFIC1Mes: TEdit;
    edNumTTAdolReprovadosFIC2Mes: TEdit;
    GroupBox27: TGroupBox;
    edNumTTAptosCursosProjetos: TEdit;
    GroupBox28: TGroupBox;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    GroupBox29: TGroupBox;
    GroupBox30: TGroupBox;
    Label68: TLabel;
    Label69: TLabel;
    edNumTTAdolInscritosVestibMes: TEdit;
    edNumTTAdolAprovadosVestibMes: TEdit;
    GroupBox31: TGroupBox;
    edNumTTConvidadosMes: TEdit;
    edNumTTParticipantesMes: TEdit;
    edNumTTAcoesComFamiliasMes: TEdit;
    GroupBox32: TGroupBox;
    meDescricaoAcoesComFamilias: TMemo;
    GroupBox33: TGroupBox;
    cbGrauSatisfacaoCrianca: TComboBox;
    GroupBox34: TGroupBox;
    cbGrauSatisfacaoFamilia: TComboBox;
    GroupBox35: TGroupBox;
    cbGrauSatisfacaoConvenio: TComboBox;
    Label70: TLabel;
    edNumTTPesqSatisfCcaAdolResp: TEdit;
    Label72: TLabel;
    edNumTTPesqSatisfFamResp: TEdit;
    Label74: TLabel;
    edNumTTPesqSatisfConvResp: TEdit;
    Label67: TLabel;
    edNumTTEfetivoUnidade: TEdit;
    edNumTTAlunosMatriculados: TEdit;
    edNumTTForaEnsinoRegular: TEdit;
    edNumTTDefasagemEscolar: TEdit;
    edNumTTFrequenciaDentroEsperado: TEdit;
    edNumTTAprendizagemDentroEsperado: TEdit;
    edNumTTAprovEscolarDentroesperado: TEdit;
    edNumTTDiasAtividadesMes: TEdit;
    edNumTTPresencasEsperadasMes: TEdit;
    edNumTTCalculoFreq: TEdit;
    edNumTTFaltasJustificadasMes: TEdit;
    edNumTTFaltasInjustificadasMes: TEdit;
    edNumTTGeralFaltasMes: TEdit;
    Label71: TLabel;
    edNumTTAdolRealizaramProva: TEdit;
    GroupBox36: TGroupBox;
    Label73: TLabel;
    Label75: TLabel;
    edNumTTAcoesSociaisMes: TEdit;
    meDescricaoAtivSociaisMes: TMemo;
    ImageList1: TImageList;
    dsResumoMensal: TDataSource;
    Label76: TLabel;
    Image4: TImage;
    GroupBox37: TGroupBox;
    edNumTTInicioMes: TEdit;
    edNumTTAdmitidos: TEdit;
    edNUmTTTransfOutrasUnidades: TEdit;
    edNumTTPeti: TEdit;
    edNumTTTransfParaOutrasUnidades: TEdit;
    edNumTTTransfParaAE: TEdit;
    edNumTTTransfParaAprendiz: TEdit;
    edNumTTDesligados: TEdit;
    GroupBox38: TGroupBox;
    cbGrauAproveitamentoDAPA: TComboBox;
    Image3: TImage;
    edNumTTInicioMesTarde: TEdit;
    edNumTTAdmitidosTarde: TEdit;
    edNumTTTransferidosDETarde: TEdit;
    edNumTTPetiTarde: TEdit;
    edNumTTTransferidosPARATarde: TEdit;
    edNumTTTransferidosParaArteEducTarde: TEdit;
    edNumTTTransferidosParaAprendizTarde: TEdit;
    edNumTTDesligadosTarde: TEdit;
    Label34: TLabel;
    qrcrResumoMensal: TQRCompositeReport;
    Label35: TLabel;
    edNUM_TT_Familias_Unidade: TEdit;
    Label36: TLabel;
    edNUM_TT_Empresas_Parceiras: TEdit;
    edNumTTFinalMes: TEdit;
    Label37: TLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure AtualizaTela(Modo : String);
    procedure btnEncerrarResumoMesClick(Sender: TObject);
    procedure btnReverterResumoMesClick(Sender: TObject);
    procedure dbgResumosCellClick(Column: TColumn);
    procedure dbgResumosDblClick(Sender: TObject);
    procedure dbgResumosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure LimpaDesabilitaTudo;
    procedure HabilitaCampos;
    procedure CalculaTotais;
    procedure Image4Click(Sender: TObject);
    procedure Label34Click(Sender: TObject);
    procedure qrcrResumoMensalAddReports(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmResumoMensal_Teste: TfrmResumoMensal_Teste;

implementation

uses uDM, uPrincipal, rResumoMensal, rResumoMensal2, uResumoMensal, uUtil,
  uAcompanhamentoBiometrico;

{$R *.DFM}

procedure TfrmResumoMensal_Teste.AtualizaTela(Modo: String);
begin
  if Modo = 'P' then
  begin

    LimpaDesabilitaTudo;

    //Bot�es
    btnEncerrarResumoMes.Enabled := True;
    btnReverterResumoMes.Enabled := True;

    if vvIND_PERFIL <> 1 then
      btnNovo.Enabled := True
    else
      btnNovo.Enabled := False;
    btnAlterar.Enabled := False;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;
    btnSair.Enabled := True;

    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := False;

  end
  else if Modo = 'N' then
  begin

    HabilitaCampos;
    //Bot�es
    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := False;

    btnNovo.Enabled := False;
    btnAlterar.Enabled := False;
    btnSalvar.Enabled := True;  //Habilitar somente com o bot�o calcular...
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;

    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := False;

  end;
end;

procedure TfrmResumoMensal_Teste.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if Key = #27 then
  begin
    Key := #0;
    frmResumoMensal_Teste.Close;
  end;
end;

procedure TfrmResumoMensal_Teste.FormShow(Sender: TObject);
begin

  AtualizaTela('P');
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;

  //Repassa os valores de Unidade, Gestor(a) e Assistente Social
  edPrograma.Text         := vvNOM_DIVISAO;
  edUnidade.Text          := vvNOM_UNIDADE;

  //Carrega os dados dos lan�amentos realizados para a unidade
  //referente ao gestor que est� logado
  try
    with dmDeca.cdsSel_ResumoMensal do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
      if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 7) or (vvIND_PERFIL = 17) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 20) or (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then
      begin
        Params.ParamByName ('@pe_cod_unidade').Value := Null;
        //Panel2.Visible := False;
        //Dentro do Panel2, habilitar apenas o bot�o Imprimir...
        Panel2.Visible               := True;
        btnEncerrarResumoMes.Visible := False;
        btnReverterResumoMes.Visible := False;
        Image4.Visible               := False;
        btnNovo.Visible              := False;
        btnAlterar.Visible           := False;
        btnSalvar.Visible            := False;
        btnCancelar.Visible          := False;
        btnImprimir.Visible          := True;
        btnSair.Visible              := True;
      end
      else
      begin
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Panel2.Visible := True
      end;

      Params.ParamByName ('@pe_num_ano').Value := Null;
      Params.ParamByName ('@pe_num_mes').Value := Null;
      Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
      Open;
      dbgResumos.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar os dados dos Resumos ' +#13+#10 +
                            'Mensais da unidade. Entre em contato com o Administrador.',
                            '[Sistema Deca] - Erro carregando Resumo Mensal...',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmResumoMensal_Teste.btnEncerrarResumoMesClick(
  Sender: TObject);
begin
  //Se o resumo estiver "N�O FINALIZADO", ent�o finaliza...
  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 0 then
  begin
    //Pede confirma��o para Finalizar o Resumo Mensal
      if Application.MessageBox('Deseja FINALIZAR o Resumo Mensal selecionado?',
                                '[Sistema Deca] - Finalizar Resumo Mensal',
                                MB_YESNO + MB_ICONQUESTION) = id_yes then
      begin

        try
          with dmDeca.cdsAlt_Encerra_Reverte_Resumo do
          begin
            Close;
            Params.ParamByName ('@pe_tipo').Value := 1;
            Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
            Params.ParamByName ('@pe_cod_id_resumomensal').Value := dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;
            Execute;

            with dmDeca.cdsSel_ResumoMensal do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
              Open;
              dbgResumos.Refresh;
            end;
          end;
        except
        end;
      end;
  end;
end;

procedure TfrmResumoMensal_Teste.btnReverterResumoMesClick(
  Sender: TObject);
begin
  //Se o resumo estiver "FINALIZADO", ent�o pode ser revertido...
  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1 then
  begin
    //Pede confirma��o para Finalizar o Resumo Mensal
    if Application.MessageBox('Deseja REVERTER o Resumo Mensal selecionado?',
                              '[Sistema Deca] - Finalizar Resumo Mensal',
                              MB_YESNO + MB_ICONQUESTION) = id_yes then
    begin

      //Verificar se existe algum Resumo Mensal N�O FINALIZADO...
      //Caso exista n�o deixar REVERTER...
      with dmDeca.cdsSel_ResumoMensal do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_num_ano').Value := Null;
        Params.ParamByName ('@pe_num_mes').Value := Null;
        Params.ParamByName ('@pe_flg_situacao_resumo').Value := 0;
        Open;

        //Se n�o tiver Resumo em aberto...
        if dmDeca.cdsSel_ResumoMensal.RecordCount < 1 then
        begin

          try
          with dmDeca.cdsAlt_Encerra_Reverte_Resumo do
          begin
            Close;
            Params.ParamByName ('@pe_tipo').Value := 2;
            Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
            Params.ParamByName ('@pe_cod_id_resumomensal').Value := vvCOD_ID_RESUMOMENSAL; //dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;
            Execute;

            with dmDeca.cdsSel_ResumoMensal do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
              Open;
              dbgResumos.Refresh;
            end;
          end;
          except end;
        end
        else
        begin
          Application.MessageBox ('Aten��o!!!' +#13+#10 +
                                  'Para REVERTER esse Resumo, voc� deve FINALIZAR.' +#13+#10 +
                                  'os Resumos ainda N�O FINALIZADOS. Verifique e tente mais tarde.',
                                  '[Sistema Deca] - Revers�o de Resumo Mensal',
                                  MB_OK + MB_ICONINFORMATION);

         with dmDeca.cdsSel_ResumoMensal do
         begin
           Close;
           Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
           Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
           Params.ParamByName ('@pe_num_ano').Value := Null;
           Params.ParamByName ('@pe_num_mes').Value := Null;
           Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
           Open;
           dbgResumos.Refresh;
         end;

        end;
    end;
  end;
end;
end;

procedure TfrmResumoMensal_Teste.dbgResumosCellClick(Column: TColumn);
begin

  //Recupera COD_ID_RESUMOMENSAL para utilizar em Encerrar/Reverter
  vvCOD_ID_RESUMOMENSAL := dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;

  //Se estiver N�O FINALIZADO habilita o bot�o ENCERRAR RESUMO e desabilita O REVERTER...
  if ( (dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 0) and
       (vvIND_PERFIL <> 1) or (vvIND_PERFIL <> 17) or (vvIND_PERFIL <> 20) or(vvIND_PERFIL <> 23) or
       (vvIND_PERFIL <> 24) or (vvIND_PERFIL <> 25) ) then
  begin
    btnEncerrarResumoMes.Enabled := True;
    btnReverterResumoMes.Enabled := False
  end
  else if ( (dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1) and
       (vvIND_PERFIL <> 1) or (vvIND_PERFIL <> 17) or (vvIND_PERFIL <> 20) or(vvIND_PERFIL <> 23) or
       (vvIND_PERFIL <> 24) or (vvIND_PERFIL <> 25) ) then
  begin
    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := True
  end
end;

procedure TfrmResumoMensal_Teste.dbgResumosDblClick(Sender: TObject);
begin

  //Verificar se o Resumo Mensal j� est� encerrado...
  //Caso esteja, n�o � poss�vel fazer qualquer altera��o...

  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1 then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'O este Resumo Mensal j� encontra-se' + #13+#10 +
                            'FINALIZADO e n�o pode ser alterado..' ,
                            '[Sistema Deca] - Resumo Mensal...',
                            MB_OK + MB_ICONWARNING);
  end
  else
  begin
    //if (vvIND_PERFIL not in [1,17,20,23,24,25]) then
    if (vvIND_PERFIL <> 1) or (vvIND_PERFIL <> 17) or (vvIND_PERFIL <> 20) or(vvIND_PERFIL <> 23) or (vvIND_PERFIL <> 24) or (vvIND_PERFIL <> 25)  then
    begin
      //Habilita os campos para que os valores sejam repassados
      HabilitaCampos;
      btnNovo.Enabled     := False;
      btnAlterar.Enabled  := True;
      btnCancelar.Enabled := True;
      mskAno.Enabled      := True;

      //Repassa os valores da tabela para os campos, habilita o bot�o alterar...
      PageControl1.ActivePageIndex := 1;

      //Repassa os valores de Unidade, Gestor(a) e Assistente Social
      edPrograma.Text         := vvNOM_DIVISAO;
      edUnidade.Text          := vvNOM_UNIDADE;

      //M�s/Ano
      cbMes.ItemIndex                           := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_mes').Value - 1;
      mskAno.Text                               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_ano').Value);

      {Totalizando por m�s e por interven��o***************************************************************************}
      edNumTTInicioMesManha.Text                := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_inicio_mes_manha').Value;
      edNumTTInicioMesTarde.Text                := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_inicio_mes_tarde').Value;
      edNumTTAdmitidosManha.Text                := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_admitidos_manha').Value;
      edNumTTAdmitidosTarde.Text                := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_admitidos_tarde').Value;
      edNumTTTransferidosDEManha.Text           := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_de_manha').Value;
      edNumTTTransferidosDETarde.Text           := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_de_tarde').Value;
      edNumTTTransferidosPARAManha.Text         := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_manha').Value;
      edNumTTTransferidosPARATarde.Text         := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_tarde').Value;
      edNumTTTransferidosParaArteEducManha.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_ae_manha').Value;
      edNumTTTransferidosParaArteEducTarde.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_ae_tarde').Value;
      edNumTTTransferidosParaAprendizManha.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_aprendiz_manha').Value;
      edNumTTTransferidosParaAprendizTarde.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_aprendiz_tarde').Value;
      edNumTTPetiManha.Text                     := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_peti_manha').Value;
      edNumTTPetiTarde.Text                     := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_peti_tarde').Value;
      edNumTTDesligadosManha.Text               := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_manha').Value;
      edNumTTDesligadosTarde.Text               := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_tarde').Value;
      edNumTTAfastados.Text                     := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_afastados').Value;

      {Dados de Escolaridade********************************************************************************************}
      edNumTTAlunosMatriculados.Text            := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_matriculados_escola').Value;
      edNumTTForaEnsinoRegular.Text             := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_fora_ensino_regular').Value;
      edNumTTDefasagemEscolar.Text              := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_defasagem_escolar').Value;
      edNumTTFrequenciaDentroEsperado.Text      := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_freq_dentro_esperado').Value;
      edNumTTAprovEscolarDentroesperado.Text    := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_aprov_escolar_dentroesp').Value;
      edNumTTDefasagemEscolar.Text              := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_defasagem_escolar').Value;
      edNumTTAprendizagemDentroEsperado.Text    := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_aprendizagem_dentroesp').Value;

      {Frequ�ncia na Unidade********************************************************************************************}
      edNumTTFinalMes.Text                      := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_final_mes').Value;
      edNumTTDiasAtividadesMes.Text             := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_dias_atividades').Value;
      edNumTTPresencasEsperadasMes.Text         := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_presencas_esperadas').Value;

      {Faltas na Unidade************************************************************************************************}
      edNumTTGeralFaltasMes.Text                := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_geral_faltas').Value;
      edNumTTFaltasJustificadasMes.Text         := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_faltas_justificadas').Value;
      edNumTTFaltasInjustificadasMes.Text       := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_faltas_injustificadas').Value;

      {Motivos de Desligamento*****************************************************************************************}
      edNumTTDesligadosAPedidoMes.Text          := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_desligados_apedido').Value;
      edNumTTDesligadosAbandonoMes.Text         := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_desligados_abandono').Value;
      edNumTTDesligadosMaioridadeMes.Text       := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_desligados_maioridade').Value;
      edNumTTDesligadosLeiMes.Text              := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_desligados_lei').Value;
      edNumTTDesligadosOutrosMes.Text           := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_desligados_outros').Value;

      {Grau de Aproeitamento no Programa********************************************************************************}
      cbGrauAproveitamentoNoPrograma.ItemIndex  := dmDeca.cdsSel_ResumoMensal.FieldByname ('num_tt_grau_aprov_programa').Value;

      {Grau de Aproeitamento no Programa********************************************************************************}
      //Incluir no Banco de Dados
      cbGrauAproveitamentoDAPA.ItemIndex        := dmDeca.cdsSel_ResumoMensal.FieldByname ('grau_aprov_dapa').Value;

      {Grau de Desempenho no Programa***********************************************************************************}
      cbGrauDesempenhoPratica.ItemIndex         := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_desemp_pratica').Value;

      {Acompanhamento Individualizado - Vulnerabilidades****************************************************************}
      edNumTTSitDisciplinarMes.Text             := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_situacao_disciplinar').Value;
      edNumTTSitSocioEconomicaMes.Text          := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_situacao_socioeconomica').Value;
      edNumTTSitEscolarMes.Text                 := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_situacao_escolar').Value;
      edNumTTSitViolenciaMes.Text               := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_violencia').Value;
      edNumTTSitAbrigoMes.Text                  := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_abrigo').Value;

      edNumTTSitInteracaoRelacMes.Text          := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_interacao_relacional').Value;
      edNumTTSitAtoInfracionalMes.Text          := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_ato_infracional').Value;
      edNumTTSitAprendizagemMes.Text            := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprendizagem').Value;
      edNumTTSitSaudeMes.Text                   := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_saude').Value;
      edNumTTSitGuardaMes.Text                  := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_guarda_irregular').Value;
      edNumTTSitGravidezMes.Text                := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_gravidez').Value;
      edNumTTSitMoradiaMes.Text                 := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_moradia').Value;
      edNumTTSitTrabInfantilMes.Text            := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_trab_infantil').Value;

      {N.� Atividades Socio-Culturais Desenvolvidas / Descri��o...*******************************************************}
      edNumTTAtivSocioCultMes.Text              := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_ativ_socioculturais').Value;
      meDescricaoAtivSocioCulturais.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_ativ_socioculturais').Value;

      {N.� Atividades Recreativas-Esportivas*****************************************************************************}
      edNumTTAtivRecreativasMes.Text            := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_ativ_recreativas').Value;
      meDescricaoAtivRecreativasMes.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_ativ_recreativas').Value;

      {N.� A��es Sociais Desenvolvidas***********************************************************************************}
      edNumTTAcoesSociaisMes.Text               := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_acoes_sociais').Value;
      meDescricaoAtivSociaisMes.Text            := dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_acoes_sociais').Value;

      {Grau Aproveitamento no Vestibulinho do CEPHAS*********************************************************************}
      edNumTTAdolInscritosVestibMes.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_inscritos_vestibulinho').Value;
      edNumTTAdolAprovadosVestibMes.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_vestibulinho').Value;
      edNumTTAdolRealizaramProva.Text           := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_adol_realizaram_prova').Value;

      {Inseridos na Aprendizagem Profissional****************************************************************************}
      edNumTTAdolAptosAprendizagemProfissionalMes.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aptos_aprendizagem').Value;
      edNumTTAdolEncaminhadosAprendizagemProfissionalMes.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_adol_encaminhados').Value;
      edNumTTAdolInseridosAprendizagemProfissionalMes.Text    := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_adol_inseridos').Value;
      edNumTTEmAprendizagemFUNDHAS.Text                       := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_EmAprendizagemFUNDHAS').Value;
      edNumTTEmAprendizagemExterna.Text                       := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_EmAprendizagemExterna').Value;

      {Curso/Projeto*****************************************************************************************************}
      edNumTTAdolAprovadosOPMes.Text    := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_op').Value;
      edNumTTAdolAprovadosOEMes.Text    := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_oe').Value;
      edNumTTAdolAprovadosFIC1Mes.Text  := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_fic1').Value;
      edNumTTAdolAprovadosFIC2Mes.Text  := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_fic2').Value;
      edNumTTAdolReprovadosOPMes.Text   := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_op').Value;
      edNumTTAdolReprovadosOEMes.Text   := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_oe').Value;
      edNumTTAdolReprovadosFIC1Mes.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_fic1').Value;
      edNumTTAdolReprovadosFIC2Mes.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_fic2').Value;

      {Tipo A��es Realizadas com Familias********************************************************************************}
      edNumTTAcoesComFamiliasMes.Text  := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_acoes_familias').Value;
      edNumTTConvidadosMes.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_convidados').Value;
      edNumTTParticipantesMes.Text     := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_participantes').Value;
      meDescricaoAcoesComFamilias.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_descricao_acoes_familia').Value;

      {Pesquisas de Satisfa��o*******************************************************************************************}
      {Crian�a/Adolescente***********************************************************************************************}
      cbGrauSatisfacaoCrianca.ItemIndex := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_criadol1').Value;
      edNumTTEfetivoUnidade.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_efetivo_unidade').Value;
      cbGrauSatisfacaoCrianca.ItemIndex := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_criadol1').Value;
      edNumTTPesqSatisfCcaAdolResp.Text := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_pesq_satisf_ccaadol_resp').Value;

      {Fam�lias**********************************************************************************************************}
      cbGrauSatisfacaoFamilia.ItemIndex := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_familia').Value;
      edNumTTPesqSatisfFamResp.Text     := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_pesq_satisf_familia_resp').Value;
      edNUM_TT_Familias_Unidade.Text    := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_familias_unidade').Value;

      {Empresas Conveniadas(Parceiras)***********************************************************************************}
      cbGrauSatisfacaoConvenio.ItemIndex := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_empresas').Value;
      edNumTTPesqSatisfConvResp.Text     := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_pesq_satisf_convenio_res').Value;
      edNUM_TT_Empresas_Parceiras.Text   := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_empresas_parceiras').Value; 

      //Totais ser�o exibidos ap�s ser executado o procedimento CalculaTotais...
      CalculaTotais;

      Application.MessageBox ('Aten��o!!!' +#13+#10 +
                              'Os dados foram repassados aos devidos campos.' +#13+#10 +
                              'Caso necessite, fa�a as devidas altera��es e clique em ALTERAR',
                              '[Sistema Deca] - Altera��o de Dados Resumo Mensal',
                              MB_OK + MB_ICONINFORMATION);
    end
    else
    begin
      Application.MessageBox ('Aten��o!!!' +#13+#10 +
                              'Seu perfil n�o permite ALTERAR os dados do Resumo Mensal da unidade',
                              '[Sistema Deca] - Altera��o de Dados Resumo Mensal',
                              MB_OK + MB_ICONINFORMATION);
    end;
  end;
end;

procedure TfrmResumoMensal_Teste.dbgResumosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var mmIcon : TBitMap;
begin
  mmIcon := TBitMap.Create();
  if (Column.FieldName = 'vSituacaoResumo') then
  begin
    with dbgResumos.Canvas do
    begin

      Brush.Color := clWhite;
      FillRect(Rect);

      if (dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 0) then
        ImageList1.GetBitmap(0, mmIcon)
      else if (dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1) then
       ImageList1.GetBitmap(1, mmIcon);
      Draw(round((Rect.Left + Rect.Right - Icon.Width) / 2), Rect.Top, mmIcon);
    end;
    
  end;
end;

procedure TfrmResumoMensal_Teste.btnNovoClick(Sender: TObject);
begin

  //Antes de iniciar, verificar se existe algum Resumo PENDENTE...
  //Verificar se existe algum Resumo em Aberto e n�o permitir incluir...
  with dmDeca.cdsSel_ResumoMensal do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
    Params.ParamByName ('@pe_num_ano').Value := Null;
    Params.ParamByName ('@pe_num_mes').Value := Null;
    Params.ParamByName ('@pe_flg_situacao_resumo').Value := 0;
    Open;

    //Se n�o tiver Resumo em aberto...
    if RecordCount < 1 then
    begin

      AtualizaTela('N');
      GroupBox1.Enabled := False;
      GroupBox2.Enabled := False;

      PageControl1.ActivePageIndex := 1;//Clicar em NOVO vai para a p�gina Dados do Resumo...
      PageControl2.ActivePageIndex := 0;

      //Zerar os campos edit�veis e setar padr�o aos combos...

      //Habilitar Mes e Ano
      GroupBox2.Enabled := True;
      cbMes.Enabled := True;
      cbMes.ItemIndex := -1;
      mskAno.Enabled := True;
      mskAno.Clear;

      cbMes.SetFocus;

      //Repassa os valores de Unidade, Gestor(a) e Assistente Social
      edPrograma.Text         := vvNOM_DIVISAO;
      edUnidade.Text          := vvNOM_UNIDADE;

      cbGrauAproveitamentoNoPrograma.ItemIndex := 0;
      cbGrauAproveitamentoDAPA.ItemIndex       := 0;
      cbGrauDesempenhoPratica.ItemIndex        := 0;
      cbGrauSatisfacaoCrianca.ItemIndex        := 0;
      cbGrauSatisfacaoFamilia.ItemIndex        := 0;
      cbGrauSatisfacaoConvenio.ItemIndex       := 0;


    end
    else
    begin
      Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                              'Ainda existe um Resumo n�o finalizado.' +#13+#10 +
                              'Voc� deve finaliz�-lo primeiro antes de criar um Novo Resumo.',
                              '[Sistema Deca] - Lan�amento de Resumo Mensal',
                              MB_OK + MB_ICONWARNING);
    end;
  end;
end;

procedure TfrmResumoMensal_Teste.btnAlterarClick(Sender: TObject);
begin

  try

    CalculaTotais;

    with dmDeca.cdsAlt_ResumoMensal do
    begin

      Close;
      Params.ParamByName ('@pe_cod_id_resumomensal').Value                := dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;
      Params.ParamByName ('@pe_num_tt_inicio_mes_manha').Value            := GetValue(edNumTTInicioMesManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_inicio_mes_tarde').Value            := GetValue(edNumTTInicioMesTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_admitidos_manha').Value             := GetValue(edNumTTAdmitidosManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_admitidos_tarde').Value             := GetValue(edNumTTAdmitidosTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_de_manha').Value             := GetValue(edNumTTTransferidosDEManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_de_tarde').Value             := GetValue(edNumTTTransferidosDETarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_manha').Value           := GetValue(edNumTTTransferidosPARAManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_tarde').Value           := GetValue(edNumTTTransferidosPARATarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_ae_manha').Value        := GetValue(edNumTTTransferidosParaArteEducManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_ae_tarde').Value        := GetValue(edNumTTTransferidosParaArteEducTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_aprendiz_manha').Value       := GetValue(edNumTTTransferidosParaAprendizManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_aprendiz_tarde').Value       := GetValue(edNumTTTransferidosParaAprendizTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_peti_manha').Value                  := GetValue(edNumTTPetiManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_peti_tarde').Value                  := GetValue(edNumTTPetiTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_manha').Value            := GetValue(edNumTTDesligadosManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_tarde').Value            := GetValue(edNumTTDesligadosTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_afastados').Value                   := GetValue(edNumTTAfastados, vtInteger);

      Params.ParamByName ('@pe_num_tt_calc_freq').Value                := GetValue(edNumTTCalculoFreq, vtInteger);

      Params.ParamByName ('@pe_num_tt_matriculados_escola').Value         := GetValue(edNumTTAlunosMatriculados, vtInteger);
      Params.ParamByName ('@pe_num_tt_fora_ensino_regular').Value         := GetValue(edNumTTForaEnsinoRegular, vtInteger);
      Params.ParamByName ('@pe_num_tt_defasagem_escolar').Value           := GetValue(edNumTTDefasagemEscolar, vtInteger);
      Params.ParamByName ('@pe_num_tt_freq_dentro_esperado').Value        := GetValue(edNumTTFrequenciaDentroEsperado, vtInteger);
      Params.ParamByName ('@pe_num_tt_defasagem_aprendizagem').Value      := 0; //Null; //GetValue(edNumTTDefasagemEscolar, vtInteger);
      Params.ParamByName ('@pe_num_tt_final_mes').Value                   := GetValue(edNumTTFinalMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_dias_atividades').Value             := GetValue(edNumTTDiasAtividadesMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_presencas_esperadas').Value         := GetValue(edNumTTPresencasEsperadasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_geral_faltas').Value                := GetValue(edNumTTGeralFaltasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_faltas_justificadas').Value         := GetValue(edNumTTFaltasJustificadasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_faltas_injustificadas').Value       := GetValue(edNumTTFaltasInjustificadasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_apedido').Value          := GetValue(edNumTTDesligadosAPedidoMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_abandono').Value         := GetValue(edNumTTDesligadosAbandonoMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_maioridade').Value       := GetValue(edNumTTDesligadosMaioridadeMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_lei').Value              := GetValue(edNumTTDesligadosLeiMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_outros').Value           := GetValue(edNumTTDesligadosOutrosMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_grau_aprov_programa').Value         := cbGrauAproveitamentoNoPrograma.ItemIndex;   //REVER VALOR A SER PREENCHIDO COM A MARILDA
      Params.ParamByName ('@pe_num_tt_situacao_disciplinar').Value        := GetValue(edNumTTSitDisciplinarMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_situacao_socioeconomica').Value     := GetValue(edNumTTSitSocioEconomicaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_situacao_escolar').Value            := GetValue(edNumTTSitEscolarMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_violencia').Value                   := GetValue(edNumTTSitViolenciaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_abrigo').Value                      := GetValue(edNumTTSitAbrigoMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_interacao_relacional').Value        := GetValue(edNumTTSitInteracaoRelacMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_ato_infracional').Value             := GetValue(edNumTTSitAtoInfracionalMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprendizagem').Value                := GetValue(edNumTTSitAprendizagemMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_saude').Value                       := GetValue(edNumTTSitSaudeMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_guarda_irregular').Value            := GetValue(edNumTTSitGuardaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_gravidez').Value                    := GetValue(edNumTTSitGravidezMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_moradia').Value                     := GetValue(edNumTTSitMoradiaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_trab_infantil').Value               := GetValue(edNumTTSitTrabInfantilMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_ativ_socioculturais').Value         := GetValue(edNumTTAtivSocioCultMes, vtInteger);
      Params.ParamByName ('@pe_dsc_ativ_socioculturais').Value            := GetValue(meDescricaoAtivSocioCulturais.Text);
      Params.ParamByName ('@pe_num_tt_ativ_recreativas').Value            := GetValue(edNumTTAtivRecreativasMes, vtInteger);
      Params.ParamByName ('@pe_dsc_ativ_recreativas').Value               := GetValue(meDescricaoAtivRecreativasMes.Text);
      Params.ParamByName ('@pe_num_tt_acoes_sociais').Value               := GetValue(edNumTTAcoesSociaisMes, vtInteger);
      Params.ParamByName ('@pe_dsc_acoes_sociais').Value                  := GetValue(meDescricaoAtivSociaisMes.Text);
      Params.ParamByName ('@pe_num_tt_inscritos_vestibulinho').Value      := GetValue(edNumTTAdolInscritosVestibMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_vestibulinho').Value      := GetValue(edNumTTAdolAprovadosVestibMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aptos_aprendizagem').Value          := GetValue(edNumTTAdolAptosAprendizagemProfissionalMes, vtInteger);
      Params.ParamByName ('@pe_num_adol_encaminhados').Value              := GetValue(edNumTTAdolEncaminhadosAprendizagemProfissionalMes, vtInteger);
      Params.ParamByName ('@pe_num_adol_inseridos').Value                 := GetValue(edNumTTAdolInseridosAprendizagemProfissionalMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_op').Value                := GetValue(edNumTTAdolAprovadosOPMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_oe').Value                := GetValue(edNumTTAdolAprovadosOEMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_fic1').Value              := GetValue(edNumTTAdolAprovadosFIC1Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_fic2').Value              := GetValue(edNumTTAdolAprovadosFIC2Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_op').Value               := GetValue(edNumTTAdolReprovadosOPMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_oe').Value               := GetValue(edNumTTAdolReprovadosOEMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_fic1').Value             := GetValue(edNumTTAdolReprovadosFIC1Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_fic2').Value             := GetValue(edNumTTAdolReprovadosFIC2Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_acoes_familias').Value              := GetValue(edNumTTAcoesComFamiliasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_convidados').Value                  := GetValue(edNumTTConvidadosMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_participantes').Value               := GetValue(edNumTTParticipantesMes, vtInteger);
      Params.ParamByName ('@pe_grau_satisf_criadol1').Value               := cbGrauSatisfacaoCrianca.ItemIndex;
      Params.ParamByName ('@pe_grau_satisf_familia').Value                := cbGrauSatisfacaoFamilia.ItemIndex;
      Params.ParamByName ('@pe_grau_satisf_empresas').Value               := cbGrauSatisfacaoConvenio.ItemIndex;
      Params.ParamByName ('@pe_cod_usuario_ult_alteracao').Value          := vvCOD_USUARIO;
      Params.ParamByName ('@pe_grau_aprov_programa').Value                := cbGrauAproveitamentoNoPrograma.ItemIndex;
      Params.ParamByName ('@pe_grau_desemp_pratica').Value                := cbGrauDesempenhoPratica.ItemIndex;
      Params.ParamByName ('@pe_dsc_descricao_acoes_familia').Value        := GetValue(meDescricaoAcoesComFamilias.Text);
      Params.ParamByName ('@pe_num_tt_efetivo_unidade').Value             := GetValue(edNumTTEfetivoUnidade, vtInteger);
      Params.ParamByName ('@pe_num_tt_pesq_satisf_ccaadol_resp').Value    := GetValue(edNumTTPesqSatisfCcaAdolResp ,vtInteger);
      Params.ParamByName ('@pe_num_tt_pesq_satisf_familia_resp').Value    := GetValue(edNumTTPesqSatisfFamResp, vtInteger);
      Params.ParamByName ('@pe_num_tt_Pesq_satisf_convenio_resp').Value   := GetValue(edNumTTPesqSatisfConvResp, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprov_escolar_dentroesp').Value     := GetValue(edNumTTAprovEscolarDentroesperado, vtInteger);
      Params.ParamByName ('@pe_num_tt_adol_realizaram_prova').Value       := GetValue(edNumTTAdolRealizaramProva, vtInteger);
      Params.ParamByName ('@pe_num_tt_EmAprendizagemFUNDHAS').Value       := GetValue(edNumTTEmAprendizagemFUNDHAS, vtInteger);
      Params.ParamByName ('@pe_num_tt_EmAprendizagemExterna').Value       := GetValue(edNumTTEmAprendizagemExterna, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprendizagem_dentroesp').Value      := GetValue(edNumTTAprendizagemDentroEsperado, vtInteger);
      Params.ParamByName ('@pe_grau_aprov_dapa').Value                    := cbGrauAproveitamentoDAPA.ItemIndex;
      Params.ParamByname ('@pe_num_tt_familias_unidade').Value            := GetValue(edNUM_TT_Familias_Unidade, vtInteger);
      Params.ParamByname ('@pe_num_tt_empresas_parceiras').Value          := GetValue(edNUM_TT_Empresas_Parceiras, vtInteger);
      Execute;

      Application.MessageBox ('Os dados do "Resumo Mensal" foram alterados com sucesso !!!' + #13+#10 +
                              'Agora, at� o per�odo permitido, voc� ainda pode ' + #13+#10 +
                              'fazer altera��es. Ap�s o per�odo permitido as ' + #13+#10 +
                              'as altera��es n�o ser�o mais permitidas.',
                              '[Sistema Deca] - Dados do Resumo Mensal alterados com sucesso.',
                              MB_OK + MB_ICONINFORMATION);

      //Volta a p�gina principal(tela)
      //Atualiza os dados do grid...
      LimpaDesabilitaTudo;
      AtualizaTela('P');
      PageControl1.ActivePageIndex := 0;

      try
      with dmDeca.cdsSel_ResumoMensal do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_num_ano').Value := Null;
        Params.ParamByName ('@pe_num_mes').Value := Null;
        Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
        Open;
        dbgResumos.Refresh;

        //Repassa os valores de Unidade, Gestor(a) e Assistente Social
        edPrograma.Text         := vvNOM_DIVISAO;
        edUnidade.Text          := vvNOM_UNIDADE;
      end;
      except
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'Um erro ocorreu ao tentar carregar os dados dos Resumos ' +#13+#10 +
                                'Mensais da unidade. Entre em contato com o Administrador.',
                                '[Sistema Deca] - Erro carregando Resumo Mensal...',
                                MB_OK + MB_ICONWARNING);
      end

    end;

  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar alterar os dados do Resumo Mensal.' + #13+#10 +
                            'Favor tentar novamente ou entre em contato com o Administrador dos Sistema.',
                            '[Sistema Deca] - Erro Alterando Resumo',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmResumoMensal_Teste.btnSalvarClick(Sender: TObject);
begin

  //Pede confirma��o do usu�rio...
  if Application.MessageBox ('Deseja incluir os dados do Resumo Mensal?' +#13+#10 +
                             'Os c�lculos ser�o efetuados e os valores ser�o inseridos no banco de dados.',
                             '[Sistema Deca] - Resumo Mensal',
                             MB_YESNO + MB_ICONQUESTION) = id_Yes then
  begin

    //Verificar se o Per�odo informado para a unidade j� est� lan�ado...
    try
      with dmDeca.cdsSel_ResumoMensal do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_num_ano').Value := StrToInt(mskAno.Text);
        Params.ParamByName ('@pe_num_mes').Value := cbMes.ItemIndex + 1;
        Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
        Open;

        if RecordCount > 0 then
        begin
          Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                                  'J� existe um lan�amento para a sua unidade no per�odo.' +#13+#10 +
                                  'Verifique o per�odo ou entre em contato com o Administrador ' + #13+#10 +
                                  'do Sistema.',
                                  '[Sistema Deca] - Lan�amento de Resumo Mensal',
                                  MB_OK + MB_ICONWARNING);
        end
        else
        //N�o encontrou registros referente ao per�odo/unidade... ent�o inclua...
        begin
          CalculaTotais;
          with dmDeca.cdsInc_ResumoMensal do
          begin
            Close;
            Params.ParamByName ('@pe_cod_unidade').Value                        := vvCOD_UNIDADE;  //Unidade de LOGIN do Usu�rio

            Params.ParamByName ('@pe_num_ano').Value                            := StrToInt(mskAno.Text);
            Params.ParamByName ('@pe_num_mes').Value                            := cbMes.ItemIndex + 1;

            {Totalizando por m�s e por interven��o***************************************************************************}
            Params.ParamByName ('@pe_num_tt_inicio_mes_manha').Value            := GetValue(edNumTTInicioMesManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_inicio_mes_tarde').Value            := GetValue(edNumTTInicioMesTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_admitidos_manha').Value             := GetValue(edNumTTAdmitidosManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_admitidos_tarde').Value             := GetValue(edNumTTAdmitidosTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_de_manha').Value             := GetValue(edNumTTTransferidosDEManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_de_tarde').Value             := GetValue(edNumTTTransferidosDETarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_manha').Value           := GetValue(edNumTTTransferidosPARAManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_tarde').Value           := GetValue(edNumTTTransferidosPARATarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_ae_manha').Value        := GetValue(edNumTTTransferidosParaArteEducManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_ae_tarde').Value        := GetValue(edNumTTTransferidosParaArteEducTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_aprendiz_manha').Value       := GetValue(edNumTTTransferidosParaAprendizManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_aprendiz_tarde').Value       := GetValue(edNumTTTransferidosParaAprendizTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_peti_manha').Value                  := GetValue(edNumTTPetiManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_peti_tarde').Value                  := GetValue(edNumTTPetiTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_manha').Value            := GetValue(edNumTTDesligadosManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_tarde').Value            := GetValue(edNumTTDesligadosTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_afastados').Value                   := GetValue(edNumTTAfastados, vtInteger);
            Params.ParamByName ('@pe_num_tt_final_mes').Value                   := GetValue(edNumTTFinalMes, vtInteger);

            {Dados de Escolaridade********************************************************************************************}
            Params.ParamByName ('@pe_num_tt_matriculados_escola').Value         := GetValue(edNumTTAlunosMatriculados, vtInteger);
            Params.ParamByName ('@pe_num_tt_fora_ensino_regular').Value         := GetValue(edNumTTForaEnsinoRegular, vtInteger);
            Params.ParamByName ('@pe_num_tt_defasagem_escolar').Value           := GetValue(edNumTTDefasagemEscolar, vtInteger);
            Params.ParamByName ('@pe_num_tt_freq_dentro_esperado').Value        := GetValue(edNumTTFrequenciaDentroEsperado, vtInteger);
            Params.ParamByName ('@pe_num_tt_defasagem_aprendizagem').Value      := 0;//GetValue(edNumTTDefasagemEscolar, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprov_escolar_dentroesp').Value     := GetValue(edNumTTAprovEscolarDentroesperado, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprendizagem_dentroesp').Value      := GetValue(edNumTTAprendizagemDentroEsperado, vtInteger);

            {Frequ�ncia na Unidade********************************************************************************************}
            Params.ParamByName ('@pe_num_tt_dias_atividades').Value             := GetValue(edNumTTDiasAtividadesMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_presencas_esperadas').Value         := GetValue(edNumTTPresencasEsperadasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_calc_freq').Value                := GetValue(edNumTTCalculoFreq, vtInteger);

            {Faltas na Unidade************************************************************************************************}
            Params.ParamByName ('@pe_num_tt_geral_faltas').Value                := GetValue(edNumTTGeralFaltasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_faltas_justificadas').Value         := GetValue(edNumTTFaltasJustificadasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_faltas_injustificadas').Value       := GetValue(edNumTTFaltasInjustificadasMes, vtInteger);

            {Motivos de Desligamento******************************************************************************************}
            Params.ParamByName ('@pe_num_tt_desligados_apedido').Value          := GetValue(edNumTTDesligadosAPedidoMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_abandono').Value         := GetValue(edNumTTDesligadosAbandonoMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_maioridade').Value       := GetValue(edNumTTDesligadosMaioridadeMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_lei').Value              := GetValue(edNumTTDesligadosLeiMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_outros').Value           := GetValue(edNumTTDesligadosOutrosMes, vtInteger);

            {Grau de Aproveitamento no Programa*******************************************************************************}
            Params.ParamByName ('@pe_num_tt_grau_aprov_programa').Value         := cbGrauAproveitamentoNoPrograma.ItemIndex;   //REVER VALOR A SER PREENCHIDO COM A MARILDA

            {Grau de Aproveitamento no Programa Aprendiz**********************************************************************}
            {Nessa vers�o utilizar a mesma para os outros programas...}
            Params.ParamByName ('@pe_grau_aprov_dapa').Value                    := cbGrauAproveitamentoDAPA.ItemIndex;

            {Grau de Desempenho no Programa***********************************************************************************}
            Params.ParamByName ('@pe_grau_desemp_pratica').Value                := cbGrauDesempenhoPratica.ItemIndex;

            {Acompanhamento Individualizado - Vulnerabilidades****************************************************************}
            Params.ParamByName ('@pe_num_tt_situacao_disciplinar').Value        := GetValue(edNumTTSitDisciplinarMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_situacao_socioeconomica').Value     := GetValue(edNumTTSitSocioEconomicaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_situacao_escolar').Value            := GetValue(edNumTTSitEscolarMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_violencia').Value                   := GetValue(edNumTTSitViolenciaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_abrigo').Value                      := GetValue(edNumTTSitAbrigoMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_interacao_relacional').Value        := GetValue(edNumTTSitInteracaoRelacMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_ato_infracional').Value             := GetValue(edNumTTSitAtoInfracionalMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprendizagem').Value                := GetValue(edNumTTSitAprendizagemMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_saude').Value                       := GetValue(edNumTTSitSaudeMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_guarda_irregular').Value            := GetValue(edNumTTSitGuardaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_gravidez').Value                    := GetValue(edNumTTSitGravidezMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_moradia').Value                     := GetValue(edNumTTSitMoradiaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_trab_infantil').Value               := GetValue(edNumTTSitTrabInfantilMes, vtInteger);

            {N.� Atividades Socio-Culturais Desenvolvidas / Descri��o...*******************************************************}
            Params.ParamByName ('@pe_num_tt_ativ_socioculturais').Value         := GetValue(edNumTTAtivSocioCultMes, vtInteger);
            Params.ParamByName ('@pe_dsc_ativ_socioculturais').Value            := GetValue(meDescricaoAtivSocioCulturais.Text);

            {N.� Atividades Recreativas-Esportivas*****************************************************************************}
            Params.ParamByName ('@pe_num_tt_ativ_recreativas').Value            := GetValue(edNumTTAtivRecreativasMes, vtInteger);
            Params.ParamByName ('@pe_dsc_ativ_recreativas').Value               := GetValue(meDescricaoAtivRecreativasMes.Text);

            {N.� A��es Sociais Desenvolvidas***********************************************************************************}
            Params.ParamByName ('@pe_num_tt_acoes_sociais').Value               := GetValue(edNumTTAcoesSociaisMes, vtInteger);
            Params.ParamByName ('@pe_dsc_acoes_sociais').Value                  := GetValue(meDescricaoAtivSociaisMes.Text);

            {Grau Aproveitamento no Vestibulinho do CEPHAS*********************************************************************}
            Params.ParamByName ('@pe_num_tt_inscritos_vestibulinho').Value      := GetValue(edNumTTAdolInscritosVestibMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_vestibulinho').Value      := GetValue(edNumTTAdolAprovadosVestibMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_adol_realizaram_prova').Value       := GetValue(edNumTTAdolRealizaramProva, vtInteger);

            {Inseridos na Aprendizagem Profissional****************************************************************************}
            Params.ParamByName ('@pe_num_tt_aptos_aprendizagem').Value          := GetValue(edNumTTAdolAptosAprendizagemProfissionalMes, vtInteger);
            Params.ParamByName ('@pe_num_adol_encaminhados').Value              := GetValue(edNumTTAdolEncaminhadosAprendizagemProfissionalMes, vtInteger);
            Params.ParamByName ('@pe_num_adol_inseridos').Value                 := GetValue(edNumTTAdolInseridosAprendizagemProfissionalMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_EmAprendizagemFUNDHAS').Value       := GetValue(edNumTTEmAprendizagemFUNDHAS, vtInteger);
            Params.ParamByName ('@pe_num_tt_EmAprendizagemExterna').Value       := GetValue(edNumTTEmAprendizagemExterna, vtInteger);

            {Curso/Projeto*****************************************************************************************************}
            Params.ParamByName ('@pe_num_tt_aprovados_op').Value                := GetValue(edNumTTAdolAprovadosOPMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_oe').Value                := GetValue(edNumTTAdolAprovadosOEMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_fic1').Value              := GetValue(edNumTTAdolAprovadosFIC1Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_fic2').Value              := GetValue(edNumTTAdolAprovadosFIC2Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_op').Value               := GetValue(edNumTTAdolReprovadosOPMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_oe').Value               := GetValue(edNumTTAdolReprovadosOEMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_fic1').Value             := GetValue(edNumTTAdolReprovadosFIC1Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_fic2').Value             := GetValue(edNumTTAdolReprovadosFIC2Mes, vtInteger);

            {Tipo A��es Realizadas com Familias********************************************************************************}
            Params.ParamByName ('@pe_num_tt_acoes_familias').Value              := GetValue(edNumTTAcoesComFamiliasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_convidados').Value                  := GetValue(edNumTTConvidadosMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_participantes').Value               := GetValue(edNumTTParticipantesMes, vtInteger);
            Params.ParamByName ('@pe_dsc_descricao_acoes_familia').Value        := GetValue(meDescricaoAcoesComFamilias.Text);

            {Pesquisas de Satisfa��o*******************************************************************************************}
            {Crian�a/Adolescente***********************************************************************************************}
            Params.ParamByName ('@pe_grau_satisf_criadol1').Value               := cbGrauSatisfacaoCrianca.ItemIndex;
            Params.ParamByName ('@pe_num_tt_efetivo_unidade').Value             := GetValue(edNumTTEfetivoUnidade, vtInteger);
            Params.ParamByName ('@pe_num_tt_pesq_satisf_ccaadol_resp').Value    := GetValue(edNumTTPesqSatisfCcaAdolResp, vtInteger);
            Params.ParamByName ('@pe_grau_satisf_criadol2').Value               := Null;
            Params.ParamByName ('@pe_grau_satisf_criadol3').Value               := Null;

            {Fam�lias**********************************************************************************************************}
            Params.ParamByName ('@pe_grau_satisf_familia').Value                := cbGrauSatisfacaoFamilia.ItemIndex;
            Params.ParamByName ('@pe_num_tt_pesq_satisf_familia_resp').Value    := GetValue(edNumTTPesqSatisfFamResp, vtInteger);
            Params.ParamByName ('@pe_num_tt_familias_unidade').Value            := GetValue(edNum_TT_Familias_Unidade, vtInteger);

            {Empresas Conveniadas(Parceiras)***********************************************************************************}
            Params.ParamByName ('@pe_grau_satisf_empresas').Value               := cbGrauSatisfacaoConvenio.ItemIndex;
            Params.ParamByName ('@pe_num_tt_Pesq_satisf_convenio_resp').Value   := GetValue(edNumTTPesqSatisfConvResp, vtInteger);
            Params.ParamByName ('@pe_num_tt_empresas_parceiras').Value          := GetValue(edNUM_TT_Empresas_Parceiras, vtInteger);


            Params.ParamByName ('@pe_cod_usuario_ult_alteracao').Value          := vvCOD_USUARIO;
            Params.ParamByName ('@pe_flg_situacao_resumo').Value                := 0;

            Execute;

            Application.MessageBox ('Os dados do "Resumo Mensal" foram inseridos com sucesso !!!' + #13+#10 +
                                    'Agora, at� o per�odo permitido, voc� ainda pode ' + #13+#10 +
                                    'fazer altera��es. Ap�s o per�odo permitido as ' + #13+#10 +
                                    'as altera��es n�o ser�o mais permitidas.',
                                    '[Sistema Deca] - Dados do Resumo Mensal inseridos com sucesso.',
                                    MB_OK + MB_ICONINFORMATION);

            //Volta a p�gina principal(tela)
            //Atualiza os dados do grid...
            AtualizaTela('P');
            LimpaDesabilitaTudo;
            PageControl1.ActivePageIndex := 0;

            try
            with dmDeca.cdsSel_ResumoMensal do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
              Open;
              dbgResumos.Refresh;

              //Repassa os valores de Unidade, Gestor(a) e Assistente Social
              edPrograma.Text         := vvNOM_DIVISAO;
              edUnidade.Text          := vvNOM_UNIDADE;
            end;
            except
              Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                      'Um erro ocorreu ao tentar carregar os dados dos Resumos ' +#13+#10 +
                                      'Mensais da unidade. Entre em contato com o Administrador.',
                                      '[Sistema Deca] - Erro carregando Resumo Mensal...',
                                      MB_OK + MB_ICONWARNING);
            end
          end;
        end;
      end;
    except
      //Aten��o... Um erro ocorreu no acesso ao banco de dados
      //verifique a sua conex�o de rede/internet ou entre em contato
      //com o administrador do sistema...
    end;

  end;
end;

procedure TfrmResumoMensal_Teste.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela('P');
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
end;

procedure TfrmResumoMensal_Teste.btnImprimirClick(Sender: TObject);
begin


  //Ao emitir o relat�rio, "anexar" o gr�fico biom�trico com os dados atuais ???
  //dos adolescentes da unidade atual.
  Application.CreateForm(TrelResumoMensal, relResumoMensal);
  relResumoMensal.qrlPrograma.Caption  := edPrograma.Text;
  relResumoMensal.qrlPeriodo.Caption   := dmDeca.cdsSel_ResumoMensal.FieldByName('vMes').Value + '/' + IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName('num_ano').Value);
  relResumoMensal.qrlUnidade.Caption   := dmDeca.cdsSel_ResumoMensal.FieldByName ('nom_unidade').Value; //DedUnidade.Text;
  relResumoMensal.qrlSituacao.Caption  := dmDeca.cdsSel_ResumoMensal.FieldByName('vSituacaoResumo').Value;

  //Repassar os valores do formul�rio para o relat�rio - frente
  relResumoMensal.qrlTOTAL_INICIO_MES.Caption                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_INICIO_MES_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_INICIO_MES_TARDE').Value);
  relResumoMensal.qrlTOTAL_ADMITIDOS_MES.Caption             := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_ADMITIDOS_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_ADMITIDOS_TARDE').Value);
  relResumoMensal.qrlTOTAL_TRANSFERIDOS_DE_MES.Caption       := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_DE_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_DE_TARDE').Value);
  relResumoMensal.qrlTOTAL_PETI_MES.Caption                  := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_PETI_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_PETI_TARDE').Value);
  relResumoMensal.qrlTOTAL_TRANSFERIDOS_PARA_MES.Caption     := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_TARDE').Value);
  relResumoMensal.qrlTOTAL_TRANSFERIDOS_AE_MES.Caption       := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_AE_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_AE_TARDE').Value);
  relResumoMensal.qrlTOTAL_TRANSFERIDOS_APRENDIZ_MES.Caption := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_APRENDIZ_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_APRENDIZ_TARDE').Value);
  relResumoMensal.qrlTOTAL_DESLIGADOS_MES.Caption            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_DESLIGADOS_MANHA').Value + dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_DESLIGADOS_TARDE').Value);

  relResumoMensal.qrlTOTAL_PERIODO_MANHA.Caption         := IntToStr(
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_INICIO_MES_MANHA').Value +
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_ADMITIDOS_MANHA').Value +
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_DE_MANHA').Value +
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_PETI_MANHA').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_MANHA').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_AE_MANHA').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_APRENDIZ_MANHA').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_DESLIGADOS_MANHA').Value
                                                            );

  relResumoMensal.qrlTOTAL_PERIODO_TARDE.Caption          := IntToStr(
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_INICIO_MES_TARDE').Value +
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_ADMITIDOS_TARDE').Value +
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_DE_TARDE').Value +
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_PETI_TARDE').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_TARDE').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_PARA_AE_TARDE').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_TRANSF_APRENDIZ_TARDE').Value -
                                                             dmDeca.cdsSel_ResumoMensal.FieldByName ('NUM_TT_DESLIGADOS_TARDE').Value
                                                            );

  relResumoMensal.qrlTOTAL_MES_UNIDADE.Caption            := IntToStr(
                                                            StrToInt(relResumoMensal.qrlTOTAL_PERIODO_MANHA.Caption) +
                                                            StrToInt(relResumoMensal.qrlTOTAL_PERIODO_TARDE.Caption)
                                                            );


  //Verso...
  Application.CreateForm(TrelResumoMensal2, relResumoMensal2);
  relResumoMensal2.qrlPrograma.Caption  := edPrograma.Text;
  relResumoMensal2.qrlPeriodo.Caption   := dmDeca.cdsSel_ResumoMensal.FieldByName('vMes').Value + '/' + IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName('num_ano').Value);
  relResumoMensal2.qrlUnidade.Caption   := dmDeca.cdsSel_ResumoMensal.FieldByName ('nom_unidade').Value; //edUnidade.Text;
  relResumoMensal2.qrlSituacao.Caption  := dmDeca.cdsSel_ResumoMensal.FieldByName('vSituacaoResumo').Value;

  //Foi adicionado um componente QRCompositeReport e adicionado os
  //relat�rios de Resumo Mensal 1 e 2
  qrcrResumoMensal.PrinterSettings.Duplex := True;
  qrcrResumoMensal.Preview;
  relResumoMensal.Free;
  relResumoMensal2.Free;
end;

procedure TfrmResumoMensal_Teste.btnSairClick(Sender: TObject);
begin
  frmResumoMensal_Teste.Close;
end;

procedure TfrmResumoMensal_Teste.LimpaDesabilitaTudo;
var i : integer;
begin
  for i:=0 to frmResumoMensal_Teste.ComponentCount - 1 do

    if frmResumoMensal_Teste.Components[i] is TCustomEdit then
    begin
      (frmResumoMensal_Teste.Components[i] as TCustomEdit).Clear;
      (frmResumoMensal_Teste.Components[i] as TCustomEdit).Enabled := False;
    end;

  cbMes.ItemIndex := -1;
  cbMes.Enabled := False;

  cbGrauAproveitamentoNoPrograma.ItemIndex := -1;
  cbGrauAproveitamentoNoPrograma.Enabled   := False;

  cbGrauDesempenhoPratica.ItemIndex        := -1;
  cbGrauDesempenhoPratica.Enabled          := False;

  cbGrauSatisfacaoCrianca.ItemIndex        := -1;
  cbGrauSatisfacaoCrianca.Enabled          := False;

  cbGrauSatisfacaoFamilia.ItemIndex        := -1;
  cbGrauSatisfacaoFamilia.Enabled          := False;

  cbGrauSatisfacaoConvenio.ItemIndex       := -1;
  cbGrauSatisfacaoConvenio.Enabled         := False;

  //Repassa os valores de Unidade, Gestor(a) e Assistente Social
  edPrograma.Text         := vvNOM_DIVISAO;
  edUnidade.Text          := vvNOM_UNIDADE;


end;

procedure TfrmResumoMensal_Teste.HabilitaCampos;
var x: Integer;
begin
  for x := 0 to frmResumoMensal_Teste.ComponentCount - 1 do
    if frmResumoMensal_Teste.Components[x] is TCustomEdit then
      (frmResumoMensal_Teste.Components[x] as TCustomEdit).Enabled := True;


  for x:=0 to frmResumoMensal_Teste.ComponentCount - 1 do
    if frmResumoMensal_Teste.Components[x] is TEdit then
      (frmResumoMensal_Teste.Components[x] as TEdit).Text := '0';


  cbMes.ItemIndex := -1;
  cbMes.Enabled := True;

  cbGrauAproveitamentoNoPrograma.ItemIndex := -1;
  cbGrauAproveitamentoNoPrograma.Enabled   := True;

  cbGrauDesempenhoPratica.ItemIndex        := -1;
  cbGrauDesempenhoPratica.Enabled          := True;  

  cbGrauSatisfacaoCrianca.ItemIndex        := -1;
  cbGrauSatisfacaoCrianca.Enabled          := True;

  cbGrauSatisfacaoFamilia.ItemIndex        := -1;
  cbGrauSatisfacaoFamilia.Enabled          := True;

  cbGrauSatisfacaoConvenio.ItemIndex       := -1;
  cbGrauSatisfacaoConvenio.Enabled         := True;


  //Desabilitar campos de totais e sub-totais...
end;

procedure TfrmResumoMensal_Teste.CalculaTotais;
var

  mmTT_CALCULO_FREQ,
  mmTT_INICIO_MES, mmTT_PETI,
  mmTT_TRANSF_PARA_OUTRAS, mmTT_TRANSF_PARA_ARTE_EDUC, mmTT_TRANSF_PARA_APRENDIZ,
  mmTT_DESLIGADOS, mmTT_AFASTADOS,
  mmTT_DIAS_ATIVIDADES, mmTT_PRESENCAS_ESPERADAS,
  mmTT_MOTIVOS_DESLIGAMENTO, mmTT_TRANSF_DE,
  mmTT_ADMITIDOS, mmTT_FINAL_MES : Integer;

begin

  //Calcular os valores dos totais do formul�rio

  //Inicializar as vari�veis...
  mmTT_CALCULO_FREQ          := 0;
  mmTT_INICIO_MES            := 0;
  mmTT_PETI                  := 0;
  mmTT_TRANSF_DE             := 0;
  mmTT_TRANSF_PARA_OUTRAS    := 0;
  mmTT_TRANSF_PARA_ARTE_EDUC := 0;
  mmTT_TRANSF_PARA_APRENDIZ  := 0;
  mmTT_DESLIGADOS            := 0;
  mmTT_AFASTADOS             := 0;
  mmTT_DIAS_ATIVIDADES       := 0;
  mmTT_PRESENCAS_ESPERADAS   := 0;
  mmTT_MOTIVOS_DESLIGAMENTO  := 0;

  //Frequ�ncia na Unidade

  mmTT_INICIO_MES            := StrToInt(edNumTTInicioMesManha.Text)                + StrToInt(edNumTTInicioMesTarde.Text);
  edNumTTInicioMes.Text      := IntToStr(mmTT_INICIO_MES);

  edNumTTAdmitidos.Text      := IntToStr(StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTAdmitidosTarde.Text));
  mmTT_ADMITIDOS := StrToInt(edNumTTAdmitidos.Text);

  mmTT_PETI                  := StrToInt(edNumTTPetiManha.Text)                     + StrToInt(edNumTTPetiTarde.Text);
  edNumTTPeti.Text           := IntToStr(StrToInt(edNumTTPetiManha.Text) + StrToInt(edNumTTPetiTarde.Text));

  mmTT_TRANSF_DE             := StrToInt(edNumTTTransferidosDEManha.Text)     + StrToInt(edNumTTTransferidosDETarde.Text);

  mmTT_TRANSF_PARA_OUTRAS    := StrToInt(edNumTTTransferidosPARAManha.Text)         + StrToInt(edNumTTTransferidosPARATarde.Text);
  edNumTTTransfParaOutrasUnidades.Text := IntToStr(mmTT_TRANSF_PARA_OUTRAS);

  mmTT_TRANSF_PARA_ARTE_EDUC := StrToInt(edNumTTTransferidosParaArteEducManha.Text) + StrToInt(edNumTTTransferidosParaArteEducTarde.Text);
  edNumTTTransfParaAE.Text   := IntToStr(mmTT_TRANSF_PARA_ARTE_EDUC);

  mmTT_TRANSF_PARA_APRENDIZ  := StrToInt(edNumTTTransferidosParaAprendizManha.Text) + StrToInt(edNumTTTransferidosParaAprendizTarde.Text);
  edNumTTTransfParaAprendiz.Text := IntToStr(mmTT_TRANSF_PARA_APRENDIZ);

  mmTT_DESLIGADOS            := StrToInt(edNumTTDesligadosManha.Text)               + StrToInt(edNumTTDesligadosTarde.Text);
  edNumTTDesligados.Text     := IntToStr(mmTT_DESLIGADOS);

  mmTT_AFASTADOS             := StrToInt(edNumTTAfastados.Text);
  edNumTTAfastados.Text      := IntToStr(mmTT_AFASTADOS);

  {Total para c�lculo do Total no final do M�s = Total Inicio Mes + Total de Admitidos + Peti + Transferidos Outras Unidades
                                                 - Transf. Para Outras Unidades - Transf. Para Arte Educa��o -
                                                 - Transf. Para Aprendiz - Desligados}

  mmTT_FINAL_MES := mmTT_INICIO_MES + mmTT_ADMITIDOS + mmTT_PETI + mmTT_TRANSF_DE - mmTT_TRANSF_PARA_OUTRAS - mmTT_TRANSF_PARA_ARTE_EDUC
                    - mmTT_TRANSF_PARA_APRENDIZ - mmTT_DESLIGADOS ;

  edNumTTFinalMes.Text := IntToStr(mmTT_FINAL_MES);


  {Presen�as Esperadas}
  mmTT_DIAS_ATIVIDADES := StrToInt(edNumTTDiasAtividadesMes.Text);
  mmTT_CALCULO_FREQ := mmTT_INICIO_MES + mmTT_PETI
                       - mmTT_TRANSF_PARA_OUTRAS
                       - mmTT_TRANSF_PARA_ARTE_EDUC
                       - mmTT_TRANSF_PARA_APRENDIZ
                       - mmTT_DESLIGADOS
                       - mmTT_AFASTADOS;

  edNumTTCalculoFreq.Text := IntToStr(mmTT_CALCULO_FREQ);
  mmTT_PRESENCAS_ESPERADAS := mmTT_DIAS_ATIVIDADES * mmTT_CALCULO_FREQ;
  edNumTTPresencasEsperadasMes.Text := IntToStr(mmTT_PRESENCAS_ESPERADAS);

  {Faltas na Unidade}
  edNumTTGeralFaltasMes.Text := IntToStr(StrToInt(edNumTTFaltasJustificadasMes.Text) + StrToInt(edNumTTFaltasInjustificadasMes.Text));





  {Verificar os totais de Desligados com os Desligamentos}
  //Somar as quantidades dos Motivos de Desligamento
  mmTT_MOTIVOS_DESLIGAMENTO := StrToInt(edNumTTDesligadosAPedidoMes.Text) +
                               StrToInt(edNumTTDesligadosAbandonoMes.Text) +
                               StrToInt(edNumTTDesligadosMaioridadeMes.Text) +
                               StrToInt(edNumTTDesligadosLeiMes.Text) +
                               StrToInt(edNumTTDesligadosOutrosMes.Text);

  if (mmTT_MOTIVOS_DESLIGAMENTO <> StrToInt(edNumTTDesligados.Text)) then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'A quantidade de Desligados com os motivos de Desligamentos n�o conferem.' + #13+#10 +
                            'Favor conferir as quantidades e caso necessite redigite as quantidades.',
                            '[Sistema Deca] - Calculando Totais do Resumo Mensal...',
                            MB_OK + MB_ICONERROR);

    //Desabilita o bot�o de alterar/salvar...
    //btnSalvar.Enabled  := False;
    //btnAlterar.Enabled := False;
    
  end
  else
  begin

    //Application.MessageBox ('Aten��o !!!' + #13+#10 +
    //                        'A quantidade de Desligados com os motivos de Desligamentos est�o corretas.' + #13+#10 +
    //                        'Grave ou altere as modifica��es feitas.',
    //                        '[Sistema Deca] - Calculando Totais do Resumo Mensal...',
    //                        MB_OK + MB_ICONINFORMATION);

  end;

end;

procedure TfrmResumoMensal_Teste.Image4Click(Sender: TObject);
begin
  CalculaTotais;
end;

procedure TfrmResumoMensal_Teste.Label34Click(Sender: TObject);
begin
  Application. CreateForm (TfrmAcompanhamentoBiometrico, frmAcompanhamentoBiometrico);
  frmAcompanhamentoBiometrico.ShowModal;
end;

procedure TfrmResumoMensal_Teste.qrcrResumoMensalAddReports(
  Sender: TObject);
begin

  //Adicionar os dois relat�rios de Resumo Mensal
  with qrcrResumoMensal.Reports do
  begin
    Add(relResumoMensal);
    Add(relResumoMensal2);
  end;
end;

end.
