unit uCursosCertificados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ComCtrls, ExtCtrls, Grids, DBGrids, StdCtrls, CheckLst, Db, Mask, Registry;

type
  TfrmCursosCertificados = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox1: TGroupBox;
    dbgCertificados: TDBGrid;
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    dbgCertificadosCursos: TDBGrid;
    SpeedButton5: TSpeedButton;
    GroupBox4: TGroupBox;
    rgSituacaoCertificado: TRadioGroup;
    edCertificado: TEdit;
    Panel2: TPanel;
    btnNovo_AbaCertificados: TBitBtn;
    btnSalvar_AbaCertificados: TBitBtn;
    btnCancelar_AbaCertificados: TBitBtn;
    btnImprimir_AbaCertificados: TBitBtn;
    btnSair_AbaCertificados: TBitBtn;
    btnAlterar_AbaCertificados: TBitBtn;
    Panel3: TPanel;
    btnNovo_AbaCursos: TBitBtn;
    btnSalvar_AbaCursos: TBitBtn;
    btnCancelar_AbaCursos: TBitBtn;
    btnImprimir_AbaCursos: TBitBtn;
    btnSair_AbaCursos: TBitBtn;
    btnAlterar_AbaCursos: TBitBtn;
    TabSheet5: TTabSheet;
    GroupBox5: TGroupBox;
    dbgCursos: TDBGrid;
    Bevel1: TBevel;
    GroupBox6: TGroupBox;
    cbCursos_AbaAlunosCursos: TComboBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    lbCargaHoraria: TLabel;
    lbDataInicio: TLabel;
    Label3: TLabel;
    lbDataTermino: TLabel;
    Label9: TLabel;
    lbModeloCertificado: TLabel;
    GroupBox7: TGroupBox;
    clbListaAlunos: TCheckListBox;
    GroupBox8: TGroupBox;
    cbCursos_AbaAprovacoes: TComboBox;
    GroupBox9: TGroupBox;
    clbAlunosCurso: TCheckListBox;
    GroupBox10: TGroupBox;
    cbCursos_AbaEmissao: TComboBox;
    GroupBox11: TGroupBox;
    clbListaAptosCertificado: TCheckListBox;
    cbUnidades_AbaAlunosCursos: TComboBox;
    dsSel_Certificado: TDataSource;
    GroupBox19: TGroupBox;
    cbFiltroPesquisa: TComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox12: TGroupBox;
    GroupBox13: TGroupBox;
    edDescricaoCurso: TEdit;
    GroupBox14: TGroupBox;
    GroupBox15: TGroupBox;
    Label31: TLabel;
    edCH: TEdit;
    GroupBox16: TGroupBox;
    GroupBox17: TGroupBox;
    rgSituacaoCurso: TRadioGroup;
    GroupBox18: TGroupBox;
    cbCertificados: TComboBox;
    dsSel_Cursos: TDataSource;
    datInicio: TMaskEdit;
    datTermino: TMaskEdit;
    datCadastro: TMaskEdit;
    btnVerAlunos: TSpeedButton;
    lbTotalLista: TLabel;
    btnLancarAlunoNoCurso: TSpeedButton;
    btnMarcarTodosListaAlunosCurso: TSpeedButton;
    btnDesmarcarTodosListaAlunosCurso: TSpeedButton;
    GroupBox20: TGroupBox;
    meJustificativa: TMemo;
    btnVerAlunosCursos: TSpeedButton;
    btnAprovar: TSpeedButton;
    btnMarcarTodos: TSpeedButton;
    btnDesmarcarTodos: TSpeedButton;
    btnReprovar: TSpeedButton;
    btnEmitirCertificado: TSpeedButton;
    btnConsultarAptosCursos: TSpeedButton;
    procedure btnSair_AbaCertificadosClick(Sender: TObject);
    procedure ModoTela_AbaCertificados (Modo:String);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnCancelar_AbaCertificadosClick(Sender: TObject);
    procedure btnNovo_AbaCertificadosClick(Sender: TObject);
    procedure dbgCertificadosDblClick(Sender: TObject);
    procedure edCertificadoEnter(Sender: TObject);
    procedure edCertificadoExit(Sender: TObject);
    procedure btnSalvar_AbaCertificadosClick(Sender: TObject);
    procedure btnAlterar_AbaCertificadosClick(Sender: TObject);
    procedure btnSair_AbaCursosClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure cbCertificadosDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ModoTela_AbaCursos(Modo: String);
    procedure btnNovo_AbaCursosClick(Sender: TObject);
    procedure btnCancelar_AbaCursosClick(Sender: TObject);
    procedure btnSalvar_AbaCursosClick(Sender: TObject);
    procedure btnAlterar_AbaCursosClick(Sender: TObject);
    procedure dbgCursosDblClick(Sender: TObject);
    procedure TabSheet5Show(Sender: TObject);
    procedure btnVerAlunosClick(Sender: TObject);
    procedure cbUnidades_AbaAlunosCursosEnter(Sender: TObject);
    procedure cbCursos_AbaAlunosCursosClick(Sender: TObject);
    procedure btnDesmarcarTodosListaAlunosCursoClick(Sender: TObject);
    procedure btnMarcarTodosListaAlunosCursoClick(Sender: TObject);
    procedure btnLancarAlunoNoCursoClick(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure btnVerAlunosCursosClick(Sender: TObject);
    procedure btnMarcarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodosClick(Sender: TObject);
    procedure btnAprovarClick(Sender: TObject);
    procedure btnEmitirCertificadoClick(Sender: TObject);
    procedure cbCursos_AbaEmissaoClick(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    function DataPorExtenso:String;
    procedure btnConsultarAptosCursosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCursosCertificados: TfrmCursosCertificados;

  //Vari�veis utilizadas na Aba - Cadastro de Cursos
  vListaCertificado1, vListaCertificado2                         : TStringList;
  vListaCursos_AbaAlunosCursos1, vListaCursos_AbaAlunosCursos2   : TStringList;
  vListaUnidade_AbaAlunosCursos1, vListaUnidade_AbaAlunosCursos2 : TStringList;
  vListaCursos_AbaAprovacoes1, vListaCursos_AbaAprovacoes2       : TStringList;
  vListaCursos_AbaEmissao1, vListaCursos_AbaEmissao2             : TStringList;
  i : Integer;

  mmCARGA_HORARIA : Integer;
  mmDATA_INICIO, mmDATA_TERMINO : TDateTime;
  mmDATA_EMISSAO : String;

implementation

uses uPrincipal, uDM, uUtil, rCertificado_Modelo01;

{$R *.DFM}

procedure TfrmCursosCertificados.btnSair_AbaCertificadosClick(Sender: TObject);
begin
  frmCursosCertificados.Close;
end;

procedure TfrmCursosCertificados.ModoTela_AbaCertificados(Modo: String);
begin
  if Modo = 'P' then
  begin
    //Habilita apenas o primeiro GroupBox para poss�vel edi��o dos dados doc ertificado
    GroupBox1.Enabled := True;
    edCertificado.Clear;
    rgSituacaoCertificado.ItemIndex := -1;
    GroupBox2.Enabled := False;
    GroupBox3.Enabled := True;

    //Habilita ou desabilita os bot�es
    btnNovo_AbaCertificados.Enabled     := True;
    btnSalvar_AbaCertificados.Enabled   := False;
    btnAlterar_AbaCertificados.Enabled  := False;
    btnCancelar_AbaCertificados.Enabled := False;
    btnImprimir_AbaCertificados.Enabled := True;
    btnSair_AbaCertificados.Enabled     := True;
  end
  else if Modo = 'N' then
  begin
    //Habilita apenas o primeiro GroupBox para poss�vel edi��o dos dados doc ertificado
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := False;

    //Habilita ou desabilita os bot�es
    btnNovo_AbaCertificados.Enabled     := False;
    btnSalvar_AbaCertificados.Enabled   := True;
    btnAlterar_AbaCertificados.Enabled  := False;
    btnCancelar_AbaCertificados.Enabled := True;
    btnImprimir_AbaCertificados.Enabled := False;
    btnSair_AbaCertificados.Enabled     := False;
  end
  else if Modo = 'A' then
  begin
    //Habilita apenas o primeiro GroupBox para poss�vel edi��o dos dados doc ertificado
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := False;

    //Habilita ou desabilita os bot�es
    btnNovo_AbaCertificados.Enabled     := False;
    btnSalvar_AbaCertificados.Enabled   := False;
    btnAlterar_AbaCertificados.Enabled  := True;
    btnCancelar_AbaCertificados.Enabled := True;
    btnImprimir_AbaCertificados.Enabled := False;
    btnSair_AbaCertificados.Enabled     := False;
  end
end;

procedure TfrmCursosCertificados.TabSheet1Show(Sender: TObject);
begin
  ModoTela_AbaCertificados('P');

  //Carrega a lista de certificados cadastrados no banco de dados
  try
    with dmDeca.cdsSel_Certificado do
    begin
      Close;
      Params.ParamByName ('@pe_cod_certificado').Value := Null;
      Params.ParamByName ('@pe_flg_situacao').Value    := Null;
      Open;
      dbgCertificados.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o !!!' +#13+#10 +
                            'Ocorreu um erro ao tentar recuperar os certificados carregados.' + #13+#10 +
                            'Verifique sua conex�o ou entre em contato com o Administrador do Sistema!',
                            '[Sistema Deca] - Erro consultando certificados.',
                            MB_OK + MB_ICONERROR);
    ModoTela_AbaCertificados('P');
  end;

end;

procedure TfrmCursosCertificados.btnCancelar_AbaCertificadosClick(
  Sender: TObject);
begin
  ModoTela_AbaCertificados('P');
end;

procedure TfrmCursosCertificados.btnNovo_AbaCertificadosClick(
  Sender: TObject);
begin
  ModoTela_AbaCertificados('N');
end;

procedure TfrmCursosCertificados.dbgCertificadosDblClick(Sender: TObject);
begin
  ModoTela_AbaCertificados('A');
  edCertificado.Text              := dmDeca.cdsSel_Certificado.FieldByName ('dsc_certificado').Value;
  rgSituacaoCertificado.ItemIndex := dmDeca.cdsSel_Certificado.FieldByName ('flg_situacao').Value;
  dmDeca.cdsSel_Certificado.FieldByName ('flg_situacao').Value;
  edCertificado.SetFocus;
  edCertificado.Font.Color := clRed;
  edCertificado.Color      := clYellow; 
end;

procedure TfrmCursosCertificados.edCertificadoEnter(Sender: TObject);
begin
  //edCertificado.Font.Color := clRed;
end;

procedure TfrmCursosCertificados.edCertificadoExit(Sender: TObject);
begin
  edCertificado.Font.Color := clBlack;
  edCertificado.Color      := clWhite; 
end;

procedure TfrmCursosCertificados.btnSalvar_AbaCertificadosClick(
  Sender: TObject);
begin
  //Verifica se o certificado informado j� est� cadastrado no banco
  try
    with dmDeca.cdsSel_Certificado do
    begin
      Close;
      Params.ParamByName ('@pe_cod_certificado').Value := Null;
      Params.ParamByName ('@pe_dsc_certificado').Value := Trim(edCertificado.Text);
      Params.ParamByName ('@pe_flg_situacao').Value    := Null;
      Open;

      //Se achou, informa e cancela, avisando o usu�rio
      if (dmDeca.cdsSel_Certificado.RecordCount > 0) then
      begin
        Application.MessageBox ('Aten��o!' +#13+#10 +
                                'J� existe um certificado com esse nome.' +#13+#10 +
                                'Verifique sua conex�o ou entre em contato com o Administrador',
                                '[Sistema Deca] - Certificados',
                                MB_OK + MB_ICONERROR);
        ModoTela_AbaCertificados('P');
      end
      else
      begin
        //Se n�o achou, executa a inser��o e atualiza os dados da tela
        with dmDeca.cdsInsUpd_Certificado do
        begin
          Close;
          Params.ParamByName ('@pe_tipo').Value            := 'I';  //Insert
          Params.ParamByName ('@pe_dsc_certificado').Value := GetValue(edCertificado.Text);
          Params.ParamByName ('@pe_flg_situacao').Value    := rgSituacaoCertificado.ItemIndex;
          Execute;

          //Atualiza os dados do grid em tela
          with dmDeca.cdsSel_Certificado do
          begin
            Close;
            Params.ParamByName ('@pe_cod_certificado').Value := Null;
            Params.ParamByName ('@pe_dsc_certificado').Value := Null;
            Params.ParamByName ('@pe_flg_situacao').Value    := Null;
            Open;
            dbgCertificados.Refresh;
          end;
        end;
      end;
      ModoTela_AbaCertificados('P');
    end;
  except
    Application.MessageBox ('Aten��o!' +#13+#10 +
                            'Ocorreu um erro ao realizar a consulta.' +#13+#10 +
                            'Verifique sua conex�o ou entre em contato com o Administrador.' +#13+#10 +
                            'Essa opera��o ser� cancelada.',
                            '[Sistema Deca] - Erro ao consultar dados.',
                            MB_OK + MB_ICONERROR);
    ModoTela_AbaCertificados('P');
  end;
end;

procedure TfrmCursosCertificados.btnAlterar_AbaCertificadosClick(
  Sender: TObject);
begin
  //Pede confirma��o ao usu�rio antes de realizar a altera��o
  if Application.MessageBox('Deseja atualizar os dados do Certificado atual?',
                            '[Sistema Deca] - Atualiza��o de dados do Certificado',
                            MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    try
      //Atualiza os dados
      with dmDeca.cdsInsUpd_Certificado do
      begin
        Close;
        Params.ParamByName ('@pe_tipo').Value            := 'E';
        Params.ParamByName ('@pe_cod_certificado').Value := dmDeca.cdsSel_Certificado.FieldByName ('cod_certificado').Value;
        Params.ParamByName ('@pe_dsc_certificado').Value := GetValue(edCertificado.Text);
        Params.ParamByName ('@pe_flg_situacao').Value    := rgSituacaoCertificado.ItemIndex;
        Execute;

        //Atualiza os dados do grid em tela
        with dmDeca.cdsSel_Certificado do
        begin
          Close;
          Params.ParamByName ('@pe_cod_certificado').Value := Null;
          Params.ParamByName ('@pe_dsc_certificado').Value := Null;
          Params.ParamByName ('@pe_flg_situacao').Value    := Null;
          Open;
          dbgCertificados.Refresh;
        end;
      end;
    except
      Application.MessageBox ('Aten��o!' +#13+#10 +
                              'Ocorreu um erro ao realizar a altera��o dos dados do certificado.' +#13+#10 +
                              'Verifique sua conex�o ou entre em contato com o Administrador.' +#13+#10 +
                              'Essa opera��o ser� cancelada.',
                              '[Sistema Deca] - Erro de altera��o',
                              MB_OK + MB_ICONERROR);
      ModoTela_AbaCertificados('P');
    end;
    ModoTela_AbaCertificados('P');
  end;
end;

procedure TfrmCursosCertificados.btnSair_AbaCursosClick(Sender: TObject);
begin
  frmCursosCertificados.Close;
end;

procedure TfrmCursosCertificados.TabSheet2Show(Sender: TObject);
begin
  ModoTela_AbaCursos('P');
  //Carrega a lista de certificados para trabalhar na tela de Cadastro de Cursos
  //Inicializa as vari�veis de lista
  vListaCertificado1 := TStringList.Create();
  vListaCertificado2 := TStringList.Create();
  cbCertificados.Clear;

  //Realiza a consulta e carrega a listagem de certificados
  try
    with dmDeca.cdsSel_Certificado do
    begin
      Close;
      Params.ParamByName ('@pe_cod_certificado').Value := Null;
      Params.ParamByName ('@pe_dsc_certificado').Value := Null;
      Params.ParamByName ('@pe_flg_situacao').Value    := Null;
      Open;

      while not (dmDeca.cdsSel_Certificado.eof) do
      begin
        cbCertificados.Items.Add(dmDeca.cdsSel_Certificado.FieldByName ('dsc_certificado').Value + ' | ' + dmDeca.cdsSel_Certificado.FieldByName ('vSituacaoCertificado').Value);
        vListaCertificado1.Add(IntToStr(dmDeca.cdsSel_Certificado.FieldByName ('cod_certificado').Value));
        vListaCertificado2.Add(dmDeca.cdsSel_Certificado.FieldByName ('dsc_certificado').Value);
        dmDeca.cdsSel_Certificado.Next;
      end;
    end;
  except
    Application.MessageBox ('Aten��o!' +#13+#10 +
                            'Ocorreu um erro ao realizar a conulta dos dados dos Certificados.' +#13+#10 +
                            'Verifique sua conex�o ou entre em contato com o Administrador.' +#13+#10 +
                            'Essa opera��o ser� cancelada.',
                            '[Sistema Deca] - Erro de consulta Certificados',
                            MB_OK + MB_ICONERROR);
    ModoTela_AbaCursos('P');
  end;


  //Consultar os cursos e carregar no grid
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := Null;
      Params.ParamByName ('@pe_dsc_curso').Value          := Null;
      Params.ParamByName ('@pe_flg_situacao_curso').Value := Null;
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;
      dbgCursos.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o!' +#13+#10 +
                            'Ocorreu um erro ao realizar a conulta dos dados dos Cursos.' +#13+#10 +
                            'Verifique sua conex�o ou entre em contato com o Administrador.' +#13+#10 +
                            'Essa opera��o ser� cancelada.',
                            '[Sistema Deca] - Erro de consulta aos Cursos',
                            MB_OK + MB_ICONERROR);
    ModoTela_AbaCursos('P');
  end;
end;

procedure TfrmCursosCertificados.cbCertificadosDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
begin
  if (dmDeca.cdsSel_Certificado.FieldByName ('flg_situacao').Value) = 0 then
  begin
    cbCertificados.Canvas.Brush.Color := clBtnFace;
    cbCertificados.Canvas.FillRect(Rect);
    cbCertificados.Canvas.Font.Color := clBlack;
    cbCertificados.Canvas.TextOut(Rect.Left, Rect.Top, cbCertificados.Items[Index]);
  end
  else
  begin
    cbCertificados.Canvas.Brush.Color := clWhite;
    cbCertificados.Canvas.FillRect(Rect);
    cbCertificados.Canvas.Font.Color := clBlack;
    cbCertificados.Canvas.TextOut(Rect.Left, Rect.Top, cbCertificados.Items[Index]);
  end;
end;

procedure TfrmCursosCertificados.ModoTela_AbaCursos(Modo: String);
begin
  if Modo = 'P' then
  begin
    GroupBox5.Enabled             := True;
    GroupBox19.Enabled            := True;
    edDescricaoCurso.Clear;
    datCadastro.Text              := DateToStr(Now());
    edCh.Clear;
    datInicio.Text                := DateToStr(Now());
    datTermino.Text               := DateToStr(Now());
    rgSituacaoCurso.ItemIndex     := -1;
    cbCertificados.ItemIndex      := -1;
    GroupBox12.Enabled            := False;

    btnNovo_AbaCursos.Enabled     := True;
    btnSalvar_AbaCursos.Enabled   := False;
    btnAlterar_AbaCursos.Enabled  := False;
    btnCancelar_AbaCursos.Enabled := False;
    btnImprimir_AbaCursos.Enabled := True;
    btnSair_AbaCursos.Enabled     := True;
  end
  else if Modo = 'N' then
  begin
    GroupBox5.Enabled             := False;
    GroupBox19.Enabled            := False;
    GroupBox12.Enabled            := True;
    GroupBox13.Enabled            := True;
    GroupBox14.Enabled            := True;
    GroupBox15.Enabled            := True;
    GroupBox16.Enabled            := True;
    GroupBox17.Enabled            := True;
    GroupBox18.Enabled            := True;
    rgSituacaoCurso.Enabled       := True;
    edCH.Text                     := '0';
    btnNovo_AbaCursos.Enabled     := False;
    btnSalvar_AbaCursos.Enabled   := True;
    btnAlterar_AbaCursos.Enabled  := False;
    btnCancelar_AbaCursos.Enabled := True;
    btnImprimir_AbaCursos.Enabled := False;
    btnSair_AbaCursos.Enabled     := False;
  end
  else if Modo = 'A' then
  begin
    GroupBox5.Enabled             := False;
    GroupBox19.Enabled            := False;
    GroupBox12.Enabled            := True;
    GroupBox13.Enabled            := True;
    GroupBox14.Enabled            := True;
    GroupBox15.Enabled            := True;
    GroupBox16.Enabled            := True;
    GroupBox17.Enabled            := True;
    GroupBox18.Enabled            := True;
    rgSituacaoCurso.Enabled       := True;    
    btnNovo_AbaCursos.Enabled     := False;
    btnSalvar_AbaCursos.Enabled   := False;
    btnAlterar_AbaCursos.Enabled  := True;
    btnCancelar_AbaCursos.Enabled := True;
    btnImprimir_AbaCursos.Enabled := False;
    btnSair_AbaCursos.Enabled     := False;
  end;
end;

procedure TfrmCursosCertificados.btnNovo_AbaCursosClick(Sender: TObject);
begin
  ModoTela_AbaCursos('N');
end;

procedure TfrmCursosCertificados.btnCancelar_AbaCursosClick(
  Sender: TObject);
begin
  ModoTela_AbaCursos('P');
end;

procedure TfrmCursosCertificados.btnSalvar_AbaCursosClick(Sender: TObject);
begin
  //Verifica se o curso informado j� est� cadastrado no banco
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := Null;
      Params.ParamByName ('@pe_dsc_curso').Value          := Trim(edDescricaoCurso.Text);
      Params.ParamByName ('@pe_flg_situacao_curso').Value := Null;
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;

      //Se achou, informa e cancela, avisando o usu�rio
      if (dmDeca.cdsSel_Curso.RecordCount > 0) then
      begin
        Application.MessageBox ('Aten��o!' +#13+#10 +
                                'J� existe um curso com esse nome.' +#13+#10 +
                                'Verifique sua conex�o ou entre em contato com o Administrador',
                                '[Sistema Deca] - Cursos',
                                MB_OK + MB_ICONERROR);
        ModoTela_AbaCertificados('P');
      end
      else
      begin
        //Se n�o achou, executa a inser��o e atualiza os dados da tela
        with dmDeca.cdsIns_Upd_Curso do
        begin
          Close;
          Params.ParamByName ('@pe_tipo').Value              := 'I';  //Insert
          Params.ParamByName ('@pe_dsc_curso').Value         := Trim(edDescricaoCurso.Text);
          Params.ParamByName ('@pe_num_carga_horaria').Value := StrToInt(edCH.Text);
          Params.ParamByName ('@pe_dat_inicio').Value        := StrToDateTime(datInicio.Text);
          Params.ParamByName ('@pe_dat_termino').Value       := StrToDateTime(datTermino.Text);
          Params.ParamByName ('@pe_flg_situacao').Value      := rgSituacaoCurso.ItemIndex;
          Params.ParamByName ('@pe_cod_certificado').Value   := StrToInt(vListaCertificado1.Strings[cbCertificados.ItemIndex]);;
          Execute;

          //Atualiza os dados do grid em tela independente da situa��o do curso
          with dmDeca.cdsSel_Curso do
          begin
            Close;
            Params.ParamByName ('@pe_cod_curso').Value       := Null;
            Params.ParamByName ('@pe_dsc_curso').Value       := Null;
            Params.ParamByName ('@pe_flg_situacao_curso').Value    := Null;
            Params.ParamByName ('@pe_dsc_certificado').Value := Null;
            Params.ParamByName ('@pe_data_inicio').Value     := Null;
            Params.ParamByName ('@pe_data_termino').Value    := Null;
            Open;
            dbgCursos.Refresh;
          end;
        end;
      end;
      ModoTela_AbaCursos('P');
    end;
  except
    Application.MessageBox ('Aten��o!' +#13+#10 +
                            'Ocorreu um erro ao realizar a consulta.' +#13+#10 +
                            'Verifique sua conex�o ou entre em contato com o Administrador.' +#13+#10 +
                            'Essa opera��o ser� cancelada.',
                            '[Sistema Deca] - Erro ao consultar cursos.',
                            MB_OK + MB_ICONERROR);
    ModoTela_AbaCursos('P');
  end;
end;

procedure TfrmCursosCertificados.btnAlterar_AbaCursosClick(
  Sender: TObject);
begin
  //Pede confirma��o ao usu�rio antes de realizar a altera��o
  if Application.MessageBox('Deseja atualizar os dados do Curso atual?',
                            '[Sistema Deca] - Atualiza��o de dados do Curso',
                            MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    try
      //Atualiza os dados
      with dmDeca.cdsIns_Upd_Curso do
      begin
        Close;
        Params.ParamByName ('@pe_tipo').Value              := 'E';
        Params.ParamByName ('@pe_cod_curso').Value         := dmDeca.cdsSel_Curso.FieldByName ('cod_curso').AsInteger;
        Params.ParamByName ('@pe_dsc_curso').Value         := Trim(edDescricaoCurso.Text);
        Params.ParamByName ('@pe_num_carga_horaria').Value := StrToInt(edCH.Text);
        Params.ParamByName ('@pe_dat_inicio').Value        := StrToDateTime(datInicio.Text);
        Params.ParamByName ('@pe_dat_termino').Value       := StrToDateTime(datTermino.Text);
        Params.ParamByName ('@pe_flg_situacao').Value      := rgSituacaoCurso.ItemIndex;
        Params.ParamByName ('@pe_cod_certificado').Value   := StrToInt(vListaCertificado1.Strings[cbCertificados.ItemIndex]);
        Execute;

        //Atualiza os dados do grid em tela independente da situa��o do curso
        with dmDeca.cdsSel_Curso do
        begin
          Close;
            Params.ParamByName ('@pe_cod_curso').Value          := Null;
            Params.ParamByName ('@pe_dsc_curso').Value          := Null;
            Params.ParamByName ('@pe_flg_situacao_curso').Value := Null;
            Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
            Params.ParamByName ('@pe_data_inicio').Value        := Null;
            Params.ParamByName ('@pe_data_termino').Value       := Null;
          Open;
          dbgCertificados.Refresh;
        end;
      end;
    except
      Application.MessageBox ('Aten��o!' +#13+#10 +
                              'Ocorreu um erro ao realizar a altera��o dos dados do certificado.' +#13+#10 +
                              'Verifique sua conex�o ou entre em contato com o Administrador.' +#13+#10 +
                              'Essa opera��o ser� cancelada.',
                              '[Sistema Deca] - Erro de altera��o',
                              MB_OK + MB_ICONERROR);
      ModoTela_AbaCursos('P');
    end;
    ModoTela_AbaCursos('P');
  end;
end;

procedure TfrmCursosCertificados.dbgCursosDblClick(Sender: TObject);
var vIndex : Integer;
begin
  ModoTela_AbaCursos('A');
  edDescricaoCurso.Text       := dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value;
  rgSituacaoCurso.ItemIndex   := dmDeca.cdsSel_Curso.FieldByName ('flg_situacao').Value;
  datCadastro.Text            := dmDeca.cdsSel_Curso.FieldByName ('data_cadastro').Value;
  edCH.Text                   := dmDeca.cdsSel_Curso.FieldByName ('num_carga_horaria').Value;
  datInicio.Text              := dmDeca.cdsSel_Curso.FieldByName ('data_inicio').Value;
  datTermino.Text             := dmDeca.cdsSel_Curso.FieldByName ('data_termino').Value;
  vListaCertificado2.Find(dmDeca.cdsSel_Curso.FieldByName ('dsc_certificado').Value, vIndex);
  cbCertificados.ItemIndex    := vIndex;
  edDescricaoCurso.SetFocus;
  edDescricaoCurso.Font.Color := clRed;
  edDescricaoCurso.Color      := clYellow;
end;

procedure TfrmCursosCertificados.TabSheet5Show(Sender: TObject);
begin
  //"Zera" as vari�veis de curso...
  lbCargaHoraria.Caption      := '';
  lbDataInicio.Caption        := '';
  lbDataTermino.Caption       := '';
  lbModeloCertificado.Caption := '';
  lbTotalLista.Caption        := '';

  //Cria as listas virtuais para carga do dados dos cursos
  vListaCursos_AbaAlunosCursos1 := TStringList.Create();
  vListaCursos_AbaAlunosCursos2  := TStringList.Create();

  //Limpa o conte�do do combo cbCursos_AbaAlunosCursos
  cbCursos_AbaAlunosCursos.Clear;
  //Carrega a lista de cursos ATIVOS
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := Null;
      Params.ParamByName ('@pe_dsc_curso').Value          := Null;
      Params.ParamByName ('@pe_flg_situacao_curso').Value := 0;    //Apenas os ATIVOS
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;

      while not (dmDeca.cdsSel_Curso.eof) do
      begin
        cbCursos_AbaAlunosCursos.Items.Add (dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value);
        vListaCursos_AbaAlunosCursos1.Add(IntToStr(dmDeca.cdsSel_Curso.FieldByName ('cod_curso').Value));
        vListaCursos_AbaAlunosCursos2.Add(dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value);
        dmDeca.cdsSel_Curso.Next;
      end;
    end;
  except

  end;

  //Carregar a lista de unidades
  vListaUnidade_AbaAlunosCursos1 := TStringList.Create();
  vListaUnidade_AbaAlunosCursos2 := TStringList.Create();

  //Carregar as unidades...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;

      if (vvIND_PERFIL in [1,5,7,16,17,18,19,20,23,24,25]) then
        Params.ParamByName ('@pe_cod_unidade').Value := Null
      else
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0) and (dmDeca.cdsSel_Unidade.FieldByName ('ind_tipo_unidade').Value = 2) then
        begin
          cbUnidades_AbaAlunosCursos.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidade_AbaAlunosCursos1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidade_AbaAlunosCursos2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          //dmDeca.cdsSel_Unidade.Next;
        end
        else
        begin

        end;
        dmDeca.cdsSel_Unidade.Next;
      end;

    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu e a consulta n�o foi realizada. ',
                            '[Sistema Deca] - Consulta Unidades...',
                            MB_OK + MB_ICONWARNING );

  end;
end;

procedure TfrmCursosCertificados.btnVerAlunosClick(Sender: TObject);
begin
  //Carrega a lista de alunos no checklistbox
  clbListaAlunos.Clear;
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade_AbaAlunosCursos1.Strings[cbUnidades_AbaAlunosCursos.ItemIndex]);
      Open;

      lbTotalLista.Caption := IntToStr(dmDeca.cdsSel_Cadastro.RecordCount);

      clbListaAlunos.Clear;
      while not (dmDeca.cdsSel_Cadastro.eof) do
      begin
        clbListaAlunos.Items.Add(dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value);
        dmDeca.cdsSel_Cadastro.Next;
      end;
    end;
  except

  end;
end;

procedure TfrmCursosCertificados.cbUnidades_AbaAlunosCursosEnter(
  Sender: TObject);
begin
  lbTotalLista.Caption := '';
end;

procedure TfrmCursosCertificados.cbCursos_AbaAlunosCursosClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := StrToInt(vListaCursos_AbaAlunosCursos1.Strings[cbCursos_AbaAlunosCursos.ItemIndex]);
      Params.ParamByName ('@pe_dsc_curso').Value          := Null;
      Params.ParamByName ('@pe_flg_situacao_curso').Value := 0;    //Apenas os ATIVOS
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;

      lbCargaHoraria.Caption      := dmDeca.cdsSel_Curso.FieldByName ('num_carga_horaria').Value;
      lbDataInicio.Caption        := dmDeca.cdsSel_Curso.FieldByName ('data_inicio').Value;
      lbDataTermino.Caption       := dmDeca.cdsSel_Curso.FieldByName ('data_termino').Value;
      lbModeloCertificado.Caption := dmDeca.cdsSel_Curso.FieldByName ('dsc_certificado').Value;

    end;
  except end;
end;

procedure TfrmCursosCertificados.btnDesmarcarTodosListaAlunosCursoClick(
  Sender: TObject);
begin
  for i := 0 to clbListaAlunos.Items.Count - 1 do
    clbListaAlunos.Checked[i] := False;
end;

procedure TfrmCursosCertificados.btnMarcarTodosListaAlunosCursoClick(
  Sender: TObject);
begin
  for i := 0 to clbListaAlunos.Items.Count - 1 do
    clbListaAlunos.Checked[i] := True;
end;

procedure TfrmCursosCertificados.btnLancarAlunoNoCursoClick(
  Sender: TObject);
begin

  //Pede confirma��o ao usu�rio
  if Application.MessageBox (PCHAR('Cadastrar os alunos selecionados no curso '+ cbCursos_AbaAlunosCursos.Text + ' ?'),
                             '[Sistema Deca] - Cadastro de cursos por aluno',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    //Faz o lan�amento do Curso para cada aluno selecionado na lista...
    for i :=0 to clbListaAlunos.Items.Count - 1 do
    begin
      if clbListaAlunos.Checked[i] = True then
      begin
        //Verifica se o aluno selecionado j� est� matriculado no curso em quest�o e/ou se j� est� cadastrado em algum curso
       with dmDeca.cdsSel_AlunosCursos do
       begin
         Close;
         Params.ParamByName ('@pe_cod_matricula').Value   := Copy(clbListaAlunos.Items.Strings[i],1,8);
         Params.ParamByName ('@pe_cod_curso').Value       := StrToInt(vListaCursos_AbaAlunosCursos1.Strings[cbCursos_AbaAlunosCursos.ItemIndex]);
         Params.ParamByName ('@pe_cod_certificado').Value := Null;
         Params.ParamByName ('@pe_flg_status').Value      := Null;
         Open;

         if (dmDeca.cdsSel_AlunosCursos.RecordCount = 0) then  //Se n�o encontrou registro de curso x aluno
         begin
           with dmDeca.cdsIns_AlunosCursos do
           begin
             Close;
             Params.ParamByName ('@pe_cod_matricula').Value     := Copy(clbListaAlunos.Items.Strings[i],1,8);
             Params.ParamByName ('@pe_cod_curso').Value         := StrToInt(vListaCursos_AbaAlunosCursos1.Strings[cbCursos_AbaAlunosCursos.ItemIndex]);
             Params.ParamByName ('@pe_flg_situacao').Value      := 2;    //Iniciar status como CURSANDO
             Params.ParamByName ('@pe_dsc_justificativa').Value := Null; //Ser� utilizada apenas para o m�dulo de Aprova��o ou N�o Aprova��o - Professor
             Execute;

             //Inserir registro no prontu�rio....
             with dmDeca.cdsInc_Cadastro_Historico do
             begin
               Close;
               Params.ParamByName ('@pe_cod_matricula').Value       := Copy(clbListaAlunos.Items.Strings[i],1,8);
               Params.ParamByName ('@pe_dat_historico').Value       := Date();
               Params.ParamByName ('@pe_ind_tipo_historico').Value  := 12; //Inser��o como REGISTRO DE ATENDIMENTO
               Params.ParamByName ('@pe_dsc_tipo_historico').Value  := 'Registro de Atendimento';
               Params.ParamByName ('@pe_cod_unidade').Value         := StrToInt(vListaUnidade_AbaAlunosCursos1.Strings[cbUnidades_AbaAlunosCursos.ItemIndex]);
               Params.ParamByName ('@pe_dsc_ocorrencia').Value      := 'Aluno(a) ' + Trim(Copy(clbListaAlunos.Items.Strings[i],12,100)) + ' cadastrado no curo de ' + cbCursos_AbaAlunosCursos.Text;
               Params.ParamByName ('@pe_dsc_projeto').Value         := Null;
               Params.ParamByName ('@pe_ind_motivo1').Value         := Null;
               Params.ParamByName ('@pe_ind_motivo2').Value         := Null;
               Params.ParamByName ('@pe_dsc_funcao').Value          := Null;
               Params.ParamByName ('@pe_dsc_comentario').Value      := Null;
               Params.ParamByName ('@pe_dat_final_suspensao').Value := Null;
               Params.ParamByName ('@pe_dsc_materia').Value         := Null;
               Params.ParamByName ('@pe_ind_motivo_transf').Value   := Null;
               Params.ParamByName ('@pe_orig_unidade').Value        := Null;
               Params.ParamByName ('@pe_orig_ccusto').Value         := Null;
               Params.ParamByName ('@pe_orig_secao').Value          := Null;
               Params.ParamByName ('@pe_orig_num_secao').Value      := Null;
               Params.ParamByName ('@pe_orig_banco').Value          := Null;
               Params.ParamByName ('@pe_dest_unidade').Value        := Null;
               Params.ParamByName ('@pe_dest_ccusto').Value         := Null;
               Params.ParamByName ('@pe_dest_secao').Value          := Null;
               Params.ParamByName ('@pe_dest_num_secao').Value      := Null;
               Params.ParamByName ('@pe_dest_banco').Value          := Null;
               Params.ParamByName ('@pe_hora_inicio').Value         := Null;
               Params.ParamByName ('@pe_hora_final').Value          := Null;
               Params.ParamByName ('@pe_flg_passe').Value           := Null;
               Params.ParamByName ('@pe_num_cotas_passe').Value     := Null;
               Params.ParamByName ('@pe_orig_funcao').Value         := Null;
               Params.ParamByName ('@pe_dest_funcao').Value         := Null;
               Params.ParamByName ('@pe_dat_curso_inicio').Value    := Null;
               Params.ParamByName ('@pe_num_curso_duracao').Value   := Null;
               Params.ParamByName ('@pe_dat_estagio_inicio').Value  := Null;
               Params.ParamByName ('@pe_ind_tipo').Value            := Null;
               Params.ParamByName ('@pe_ind_classificacao').Value   := Null;
               Params.ParamByName ('@pe_qtd_participantes').Value   := Null;
               Params.ParamByName ('@pe_num_horas').Value           := Null;
               Params.ParamByName ('@pe_num_estagio_duracao').Value := Null;
               Params.ParamByName ('@pe_log_cod_usuario').Value     := vvCOD_USUARIO;
               Execute;
             end;
           end;
         end
         else
         begin
           //Application.MessageBox (PCHAR('Cadastrar os alunos selecionados no curso '+ cbCursos_AbaAlunosCursos.Text + ' ?'),
           //                              '[Sistema Deca] - Cadastro de cursos por aluno',
           //                              MB_YESNO + MB_ICONQUESTION)
         end;
       end;
    end;
  end;
  end;
  //Desmarca os nomes selecionados
  for i := 0 to clbListaAlunos.Items.Count - 1 do
    clbListaAlunos.Checked[i] := False;

end;

procedure TfrmCursosCertificados.TabSheet3Show(Sender: TObject);
begin
  //Cria as listas virtuais para carga do dados dos cursos
  vListaCursos_AbaAprovacoes1 := TStringList.Create();
  vListaCursos_AbaAprovacoes2 := TStringList.Create();

  //Limpa o conte�do do combo cbCursos_AbaAlunosCursos
  cbCursos_AbaAprovacoes.Clear;
  //Carrega a lista de cursos ATIVOS
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := Null;
      Params.ParamByName ('@pe_dsc_curso').Value          := Null;
      Params.ParamByName ('@pe_flg_situacao_curso').Value := 0;    //Apenas os ATIVOS
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;

      while not (dmDeca.cdsSel_Curso.eof) do
      begin
        cbCursos_AbaAprovacoes.Items.Add (dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value);
        vListaCursos_AbaAprovacoes1.Add(IntToStr(dmDeca.cdsSel_Curso.FieldByName ('cod_curso').Value));
        vListaCursos_AbaAprovacoes2.Add(dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value);
        dmDeca.cdsSel_Curso.Next;
      end;
    end;
  except

  end;
end;

procedure TfrmCursosCertificados.btnVerAlunosCursosClick(Sender: TObject);
begin
  //Carrega a lista de alunos no checklistbox
  clbAlunosCurso.Clear;
  try
    with dmDeca.cdsSel_AlunosCursos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value   := Null;
      Params.ParamByName ('@pe_cod_curso').Value       := StrToInt(vListaCursos_AbaAprovacoes1.Strings[cbCursos_AbaAprovacoes.ItemIndex]);
      Params.ParamByName ('@pe_cod_certificado').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value      := Null; 
      Open;

      while not (dmDeca.cdsSel_AlunosCursos.eof) do
      begin
        clbAlunosCurso.Items.Add(dmDeca.cdsSel_AlunosCursos.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_AlunosCursos.FieldByName('nom_nome').Value);
        dmDeca.cdsSel_AlunosCursos.Next;
      end;
    end;
  except

  end;
end;

procedure TfrmCursosCertificados.btnMarcarTodosClick(Sender: TObject);
begin
  for i := 0 to clbAlunosCurso.Items.Count - 1 do
    clbAlunosCurso.Checked[i] := True;
end;

procedure TfrmCursosCertificados.btnDesmarcarTodosClick(Sender: TObject);
begin
  for i := 0 to clbAlunosCurso.Items.Count - 1 do
    clbAlunosCurso.Checked[i] := False;
end;

procedure TfrmCursosCertificados.btnAprovarClick(Sender: TObject);
begin
  i := 0;
  //Pede confirma��o ao usu�rio
  if Application.MessageBox (PCHAR('Lan�ar aprova��o para os alunos selecionados no curso '+ cbCursos_AbaAprovacoes.Text + ' ?'),
                             '[Sistema Deca] - Aprova��es de alunos no curso',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    //Faz o lan�amento do Curso para cada aluno selecionado na lista...
    for i:=0 to clbAlunosCurso.Items.Count - 1 do
    begin
      if (clbAlunosCurso.Checked[i] = True) then
      begin
        with dmDeca.cdsUPD_AlunosCursos do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').AsString     := Copy(clbAlunosCurso.Items.Strings[i],1,8);
          Params.ParamByName ('@pe_cod_curso').Value            := StrToInt(vListaCursos_AbaAprovacoes1.Strings[cbCursos_AbaAprovacoes.ItemIndex]);
          Params.ParamByName ('@pe_dsc_justificativa').AsString := Trim(meJustificativa.Text); //Ser� utilizada apenas para o m�dulo de Aprova��o ou N�o Aprova��o - Professor
          Params.ParamByName ('@pe_flg_status').Value           := 0;    //APROVADO
          Execute;

          //Inserir registro no prontu�rio....
          with dmDeca.cdsInc_Cadastro_Historico do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value       := Copy(clbAlunosCurso.Items.Strings[i],1,8);
            Params.ParamByName ('@pe_dat_historico').Value       := Date();
            Params.ParamByName ('@pe_ind_tipo_historico').Value  := 12; //Inser��o como REGISTRO DE ATENDIMENTO
            Params.ParamByName ('@pe_dsc_tipo_historico').Value  := 'Registro de Atendimento';
            Params.ParamByName ('@pe_cod_unidade').Value         := dmDeca.cdsSel_AlunosCursos.FieldByName ('cod_unidade').Value;
            Params.ParamByName ('@pe_dsc_ocorrencia').Value      := 'Aluno(a) ' + Trim(Copy(clbAlunosCurso.Items.Strings[i],12,100)) + ' aprovado no curso de ' + cbCursos_AbaAprovacoes.Text + '.' + meJustificativa.Text;
            Params.ParamByName ('@pe_dsc_projeto').Value         := Null;
            Params.ParamByName ('@pe_ind_motivo1').Value         := Null;
            Params.ParamByName ('@pe_ind_motivo2').Value         := Null;
            Params.ParamByName ('@pe_dsc_funcao').Value          := Null;
            Params.ParamByName ('@pe_dsc_comentario').Value      := Null;
            Params.ParamByName ('@pe_dat_final_suspensao').Value := Null;
            Params.ParamByName ('@pe_dsc_materia').Value         := Null;
            Params.ParamByName ('@pe_ind_motivo_transf').Value   := Null;
            Params.ParamByName ('@pe_orig_unidade').Value        := Null;
            Params.ParamByName ('@pe_orig_ccusto').Value         := Null;
            Params.ParamByName ('@pe_orig_secao').Value          := Null;
            Params.ParamByName ('@pe_orig_num_secao').Value      := Null;
            Params.ParamByName ('@pe_orig_banco').Value          := Null;
            Params.ParamByName ('@pe_dest_unidade').Value        := Null;
            Params.ParamByName ('@pe_dest_ccusto').Value         := Null;
            Params.ParamByName ('@pe_dest_secao').Value          := Null;
            Params.ParamByName ('@pe_dest_num_secao').Value      := Null;
            Params.ParamByName ('@pe_dest_banco').Value          := Null;
            Params.ParamByName ('@pe_hora_inicio').Value         := Null;
            Params.ParamByName ('@pe_hora_final').Value          := Null;
            Params.ParamByName ('@pe_flg_passe').Value           := Null;
            Params.ParamByName ('@pe_num_cotas_passe').Value     := Null;
            Params.ParamByName ('@pe_orig_funcao').Value         := Null;
            Params.ParamByName ('@pe_dest_funcao').Value         := Null;
            Params.ParamByName ('@pe_dat_curso_inicio').Value    := Null;
            Params.ParamByName ('@pe_num_curso_duracao').Value   := Null;
            Params.ParamByName ('@pe_dat_estagio_inicio').Value  := Null;
            Params.ParamByName ('@pe_ind_tipo').Value            := Null;
            Params.ParamByName ('@pe_ind_classificacao').Value   := Null;
            Params.ParamByName ('@pe_qtd_participantes').Value   := Null;
            Params.ParamByName ('@pe_num_horas').Value           := Null;
            Params.ParamByName ('@pe_num_estagio_duracao').Value := Null;
            Params.ParamByName ('@pe_log_cod_usuario').Value     := vvCOD_USUARIO;
            Execute;
          end;
        end;
      end
      else
      begin

      end;
    end; //for i := 0 ....

    //Carrega a lista de alunos no checklistbox
    clbAlunosCurso.Clear;
    try
      with dmDeca.cdsSel_AlunosCursos do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value   := Null;
        Params.ParamByName ('@pe_cod_curso').Value       := StrToInt(vListaCursos_AbaAprovacoes1.Strings[cbCursos_AbaAprovacoes.ItemIndex]);
        Params.ParamByName ('@pe_cod_certificado').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value      := 2;
        Open;

        while not (dmDeca.cdsSel_AlunosCursos.eof) do
        begin
          clbAlunosCurso.Items.Add(dmDeca.cdsSel_AlunosCursos.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_AlunosCursos.FieldByName('nom_nome').Value);
          dmDeca.cdsSel_AlunosCursos.Next;
        end;
      end;
    except

    end;
  end;
end;

procedure TfrmCursosCertificados.btnEmitirCertificadoClick(
  Sender: TObject);
begin
  try
    i := 0;

    mmDATA_EMISSAO := DataPorExtenso;
    
    //Pede confirma��o ao usu�rio
    if Application.MessageBox (PCHAR('Emitir Certificados para os alunos selecionados no curso '+ cbCursos_AbaAprovacoes.Text + ' ?'),
                               '[Sistema Deca] - Aprova��es de alunos no curso',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      //Faz a exclus�o dos dados referentes aos dados de emiss�o dos certificados para o usu�rio logado...
      with dmDeca.cdsExc_EmitirCertificado do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;

      //Faz o lan�amento do Curso para cada aluno selecionado na lista...
      for i:=0 to clbListaAptosCertificado.Items.Count - 1 do
      begin
        //Verifica se o aluno est� "marcado" para emiss�o do certificado
        //Caso esteja, inserir os dados numa tabela tempor�ria
        if (clbListaAptosCertificado.Checked[i] = True) then
        begin
          with dmDeca.cdsIns_EmitirCertificado do
          begin
            Close;
            Params.ParamByName ('@pe_curso').Value        := cbCursos_AbaEmissao.Text;
            Params.ParamByName ('@pe_nome').Value         := Trim(Copy(clbListaAptosCertificado.Items.Strings[i],12,100));
            Params.ParamByName ('@pe_ch').Value           := mmCARGA_HORARIA;
            Params.ParamByName ('@pe_data_inicio').Value  := mmDATA_INICIO;
            Params.ParamByName ('@pe_data_termino').Value := mmDATA_TERMINO;
            Params.ParamByName ('@pe_data_emissao').Value := mmDATA_EMISSAO;
            Params.ParamByName ('@pe_cod_usuario').Value  := vvCOD_USUARIO;
            Execute;
          end;
        end;
      end;

      //Carregar o relat�rio com os dados gerados...
      with dmDeca.cdsSel_EmitirCertificado do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Open;

        //Verificar qual o certificado ser� utilizado, recuperado atrav�s dos dados do curso
        with dmDeca.cdsSel_Curso do
        begin
          Close;
          Params.ParamByName ('@pe_cod_curso').Value          := StrToInt(vListaCursos_AbaEmissao1.Strings[cbCursos_AbaEmissao.ItemIndex]);
          Params.ParamByName ('@pe_dsc_curso').Value          := Null;
          Params.ParamByName ('@pe_flg_situacao_curso').Value := Null;
          Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
          Params.ParamByName ('@pe_data_inicio').Value        := Null;
          Params.ParamByName ('@pe_data_termino').Value       := Null;
          Open;

          case dmDeca.cdsSel_Curso.FieldByName ('cod_curso').Value of
            1: begin
                 Application.CreateForm (TrelCertificado_Modelo01, relCertificado_Modelo01);
                 relCertificado_Modelo01.Preview;
            end;

            2: begin
                 //Application.CreateForm (TrelCertificado_Modelo02, relCertificado_Modelo02);
                 //relCertificado_Modelo02.Preview;
            end;
          end;
        end;
      end;
    end;
  except end;
end;

procedure TfrmCursosCertificados.cbCursos_AbaEmissaoClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := StrToInt(vListaCursos_AbaEmissao1.Strings[cbCursos_AbaEmissao.ItemIndex]);
      Params.ParamByName ('@pe_dsc_curso').Value          := Null;
      Params.ParamByName ('@pe_flg_situacao_curso').Value := 0;    //Apenas os ATIVOS
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;

      mmCARGA_HORARIA := dmDeca.cdsSel_Curso.FieldByName ('num_carga_horaria').Value;
      mmDATA_INICIO   := dmDeca.cdsSel_Curso.FieldByName ('data_inicio').AsDateTime;
      mmDATA_TERMINO  := dmDeca.cdsSel_Curso.FieldByName ('data_termino').AsDateTime;

    end;
  except end;
end;

procedure TfrmCursosCertificados.TabSheet4Show(Sender: TObject);
begin
  //Cria as listas virtuais para carga do dados dos cursos
  vListaCursos_AbaEmissao1 := TStringList.Create();
  vListaCursos_AbaEmissao2 := TStringList.Create();

  //Limpa o conte�do do combo cbCursos_AbaAlunosCursos
  cbCursos_AbaAprovacoes.Clear;
  //Carrega a lista de cursos ATIVOS
  try
    with dmDeca.cdsSel_Curso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_curso').Value          := Null;
      Params.ParamByName ('@pe_dsc_curso').Value          := Null;
      Params.ParamByName ('@pe_flg_situacao_curso').Value := 0;    //Apenas os ATIVOS
      Params.ParamByName ('@pe_dsc_certificado').Value    := Null;
      Params.ParamByName ('@pe_data_inicio').Value        := Null;
      Params.ParamByName ('@pe_data_termino').Value       := Null;
      Open;

      while not (dmDeca.cdsSel_Curso.eof) do
      begin
        cbCursos_AbaEmissao.Items.Add (dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value);
        vListaCursos_AbaEmissao1.Add(IntToStr(dmDeca.cdsSel_Curso.FieldByName ('cod_curso').Value));
        vListaCursos_AbaEmissao2.Add(dmDeca.cdsSel_Curso.FieldByName ('dsc_curso').Value);
        dmDeca.cdsSel_Curso.Next;
      end;
    end;
  except

  end;
end;

function TfrmCursosCertificados.DataPorExtenso: String;
const
  Meses  : array[1..12] of String = ('Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
  Semana : array[1..7] of String = ('Domingo','Segunda-Feira','Ter�a-Feira','Quarta-Feira','Quinta-Feira','Sexta-Feira','S�bado');
var
  Dia, Mes, Ano, Diasem: word;
begin
  DecodeDate(Date,Ano,Mes,Dia);
  Diasem := dayofweek(Date);
  result := 'S�o Jos� dos Campos, ' + IntToStr(Dia) + ' de '+ Meses[mes] + ' de ' + IntToStr(Ano) + '. ';
end;

procedure TfrmCursosCertificados.btnConsultarAptosCursosClick(
  Sender: TObject);
begin
  //Carrega a lista de alunos no checklistbox
  clbListaAptosCertificado.Clear;
  try
    with dmDeca.cdsSel_AlunosCursos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value   := Null;
      Params.ParamByName ('@pe_cod_curso').Value       := StrToInt(vListaCursos_AbaEmissao1.Strings[cbCursos_AbaEmissao.ItemIndex]);
      Params.ParamByName ('@pe_cod_certificado').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value      := 0; //Apenas os APROVADOS
      Open;

      while not (dmDeca.cdsSel_AlunosCursos.eof) do
      begin
        clbListaAptosCertificado.Items.Add(dmDeca.cdsSel_AlunosCursos.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_AlunosCursos.FieldByName('nom_nome').Value);
        dmDeca.cdsSel_AlunosCursos.Next;
      end;
    end;
  except

  end;
end;

end.
