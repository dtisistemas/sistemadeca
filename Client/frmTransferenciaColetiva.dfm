object Form1: TForm1
  Left = 411
  Top = 208
  Width = 924
  Height = 675
  Caption = '[Sistema Deca] - Módulo de Transferência Coletiva'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 4
    Width = 205
    Height = 43
    Caption = 'Divisão'
    TabOrder = 0
  end
  object GroupBox2: TGroupBox
    Left = 212
    Top = 4
    Width = 509
    Height = 43
    Caption = '[Unidade]'
    TabOrder = 1
  end
  object Button1: TButton
    Left = 731
    Top = 15
    Width = 75
    Height = 25
    Caption = 'Ver'
    TabOrder = 2
  end
  object CheckListBox1: TCheckListBox
    Left = 5
    Top = 53
    Width = 412
    Height = 492
    ItemHeight = 13
    TabOrder = 3
  end
  object Button2: TButton
    Left = 67
    Top = 551
    Width = 132
    Height = 25
    Caption = 'Marcar Todos'
    TabOrder = 4
  end
  object Button3: TButton
    Left = 211
    Top = 551
    Width = 132
    Height = 25
    Caption = 'Desmarcar Todos'
    TabOrder = 5
  end
  object GroupBox3: TGroupBox
    Left = 421
    Top = 48
    Width = 422
    Height = 278
    Caption = '[Destino]'
    TabOrder = 6
    object GroupBox4: TGroupBox
      Left = 32
      Top = 24
      Width = 164
      Height = 41
      Caption = '[Data da Transferência]'
      TabOrder = 0
    end
  end
end
