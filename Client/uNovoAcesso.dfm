object Form1: TForm1
  Left = 303
  Top = 319
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 378
  ClientWidth = 860
  Color = clNavy
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 0
    Top = 96
    Width = 865
    Height = 192
    Brush.Color = clYellow
    Pen.Style = psClear
  end
  object Label1: TLabel
    Left = 232
    Top = 24
    Width = 323
    Height = 23
    Caption = 'FUNDA��O H�LIO AUGUSTO DE SOUZA'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Candara'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 209
    Top = 50
    Width = 373
    Height = 23
    Caption = 'Sistema Deca  -  Vers�o 4.5.0 de 22/agosto/2012'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Candara'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Shape2: TShape
    Left = 136
    Top = 96
    Width = 585
    Height = 191
    Brush.Color = 10813439
    Pen.Style = psClear
  end
  object Label3: TLabel
    Left = 152
    Top = 149
    Width = 70
    Height = 18
    Caption = 'MATR�CULA'
    Color = 10813439
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label4: TLabel
    Left = 264
    Top = 149
    Width = 53
    Height = 18
    Caption = 'UNIDADE'
    Color = 10813439
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label5: TLabel
    Left = 576
    Top = 149
    Width = 109
    Height = 18
    Caption = 'SENHA DE ACESSO'
    Color = 10813439
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label6: TLabel
    Left = 153
    Top = 120
    Width = 93
    Height = 19
    Caption = 'Fa�a seu login'
    Color = 10813439
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Candara'
    Font.Style = [fsBold, fsItalic]
    ParentColor = False
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 256
    Top = 216
    Width = 153
    Height = 42
  end
  object SpeedButton2: TSpeedButton
    Left = 448
    Top = 216
    Width = 153
    Height = 42
  end
  object mskMatricula: TMaskEdit
    Left = 152
    Top = 168
    Width = 108
    Height = 20
    EditMask = '99999'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    MaxLength = 5
    ParentFont = False
    TabOrder = 0
    Text = '     '
  end
  object cbUnidade: TComboBox
    Left = 264
    Top = 168
    Width = 309
    Height = 22
    Style = csOwnerDrawVariable
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ItemHeight = 16
    ParentFont = False
    TabOrder = 1
  end
  object mskSenha: TMaskEdit
    Left = 576
    Top = 168
    Width = 108
    Height = 26
    EditMask = 'CCCCCCCCCC'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    MaxLength = 10
    ParentFont = False
    TabOrder = 2
    Text = '          '
  end
end
