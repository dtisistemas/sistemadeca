object frmFichaCadastroNovo: TfrmFichaCadastroNovo
  Left = 718
  Top = 231
  Width = 1113
  Height = 619
  Caption = 'frmFichaCadastroNovo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1097
    Height = 580
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Dados Cadastrais'
      object GroupBox1: TGroupBox
        Left = 16
        Top = 8
        Width = 833
        Height = 529
        Caption = 'Dados'
        TabOrder = 0
        object Label1: TLabel
          Left = 17
          Top = 28
          Width = 53
          Height = 15
          Caption = 'Matr�cula'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 84
          Top = 28
          Width = 60
          Height = 15
          Caption = 'Prontu�rio'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 152
          Top = 28
          Width = 62
          Height = 15
          Caption = 'RG Escolar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 254
          Top = 28
          Width = 90
          Height = 15
          Caption = 'Nome Completo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 630
          Top = 28
          Width = 86
          Height = 15
          Caption = 'Data Admiss�o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 723
          Top = 28
          Width = 81
          Height = 15
          Caption = 'Data Cadastro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 17
          Top = 69
          Width = 120
          Height = 15
          Caption = 'Endere�o residencial'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnAlteraEndereco: TSpeedButton
          Left = 495
          Top = 85
          Width = 23
          Height = 22
          Hint = 'Altera��o de endere�o.'
          Flat = True
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F000000000000000000000001000000000000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            77777777000077777777777777777777000077777771171177777777000078FF
            FFFFF1FFFFFFFFF70000780000000100000000F70000780FFFFFF1FFFFFFF0F7
            0000780FFFFFF1FFFFFFF0F70000780FCCCCF1FCCCCFF0F70000780FCFCCF1FC
            CFCCF0F70000780FCCCCF1FCCFCCF0F70000780FFFCCF1FCCFCCF0F70000780F
            CCCFF1FCCCCFF0F70000780FFFFFF1FCCFFFF0F70000780FFFFFF1FCCFFFF0F7
            0000780FFFFFF1FFFFFFF0F70000780000000100000000F70000788888888188
            8888888700007777777117117777777700007777777777777777777700007777
            77777777777777770000}
        end
        object Label8: TLabel
          Left = 521
          Top = 69
          Width = 79
          Height = 15
          Caption = 'Complemento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 17
          Top = 109
          Width = 35
          Height = 15
          Caption = 'Bairro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 398
          Top = 109
          Width = 23
          Height = 15
          Caption = 'CEP'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 468
          Top = 109
          Width = 114
          Height = 15
          Caption = 'Data de Nascimento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbIdade: TLabel
          Left = 533
          Top = 127
          Width = 283
          Height = 15
          AutoSize = False
          Caption = 'Idade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label12: TLabel
          Left = 17
          Top = 151
          Width = 29
          Height = 15
          Caption = 'Sexo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 160
          Top = 151
          Width = 117
          Height = 15
          Caption = 'Telefone Residencial'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label14: TLabel
          Left = 290
          Top = 151
          Width = 91
          Height = 15
          Caption = 'Telefone Celular'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txtMatricula: TMaskEdit
          Left = 16
          Top = 42
          Width = 65
          Height = 21
          Hint = 'Informe N�MERO DE MATR�CULA V�LIDO (Campo Obrigat�rio)'
          BorderStyle = bsNone
          EditMask = '!99999999;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '        '
        end
        object txtProntuario: TMaskEdit
          Left = 85
          Top = 42
          Width = 64
          Height = 21
          Hint = 'Informe N�MERO DO PRONTU�RIO'
          BorderStyle = bsNone
          EditMask = '999999;1; '
          MaxLength = 6
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = '      '
        end
        object edNumRGEscolarC: TEdit
          Left = 151
          Top = 42
          Width = 99
          Height = 21
          BorderStyle = bsNone
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object txtNome: TEdit
          Left = 254
          Top = 42
          Width = 372
          Height = 22
          Hint = 'Informe NOME COMPLETO DA CRIAN�A/ADOLESCENTE (Campo Obrigat�rio)'
          BorderStyle = bsNone
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          MaxLength = 50
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object txtAdmissao: TMaskEdit
          Left = 630
          Top = 42
          Width = 90
          Height = 22
          Hint = 'Informe a DATA DE ADMISS�O (Campo Obrigat�rio)'
          BorderStyle = bsNone
          EditMask = '99/99/9999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Text = '  /  /    '
        end
        object txtDtCadastro: TMaskEdit
          Left = 724
          Top = 42
          Width = 90
          Height = 22
          Hint = 
            'DATA DO CADASTRO DA CRIAN�A/ADOLESCENTE NO BANCO DE DADOS DO SIS' +
            'TEMA'
          BorderStyle = bsNone
          EditMask = '99/99/9999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Text = '  /  /    '
        end
        object txtEndereco: TEdit
          Left = 16
          Top = 85
          Width = 476
          Height = 22
          Hint = 
            'Informe o ENDERE�O RESIDENCIAL ATUAL DA CRIAN�A/ADOLESCENTE (Cam' +
            'po Obrigat�rio)'
          BorderStyle = bsNone
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 70
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
        end
        object txtComplemento: TEdit
          Left = 521
          Top = 86
          Width = 293
          Height = 22
          Hint = 'Informe COMPLEMENTO DE ENDERE�O'
          BorderStyle = bsNone
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
        end
        object txtBairro: TEdit
          Left = 16
          Top = 124
          Width = 377
          Height = 22
          Hint = 
            'Informe o NOME DO BAIRRO ONDE RESIDE ATUALMENTE A CRIAN�A/ADOLES' +
            'CENTE'
          BorderStyle = bsNone
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
        end
        object txtCep: TMaskEdit
          Left = 397
          Top = 124
          Width = 66
          Height = 22
          Hint = 'Informe N�MERO DO CEP (Campo Obrigat�rio)'
          BorderStyle = bsNone
          EditMask = '99999\-999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          Text = '     -   '
        end
        object txtNascimento: TMaskEdit
          Left = 467
          Top = 124
          Width = 61
          Height = 22
          Hint = 
            'Informe a DATA DE NASCIMENTO no formato dd/mm/aaaa (Campo Obriga' +
            'torio)'
          BorderStyle = bsNone
          EditMask = '99/99/9999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          Text = '  /  /    '
        end
        object cbSexoCrianca: TComboBox
          Left = 16
          Top = 165
          Width = 141
          Height = 20
          Hint = 'Selecione SEXO DA CRIAN�A/ADOLESCENTE (Campo Obrigat�rio)'
          Style = csOwnerDrawFixed
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 11
          Items.Strings = (
            'MASCULINO'
            'FEMININO')
        end
        object txtTelefone: TMaskEdit
          Left = 160
          Top = 165
          Width = 126
          Height = 22
          Hint = 'Informe TELEFONE (Residencial, Celular, Recado)'
          BorderStyle = bsNone
          EditMask = '\(99\)9999\-9999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 12
          Text = '(  )    -    '
        end
        object MaskEdit1: TMaskEdit
          Left = 289
          Top = 165
          Width = 126
          Height = 22
          Hint = 'Informe TELEFONE (Residencial, Celular, Recado)'
          BorderStyle = bsNone
          EditMask = '\(99\)9999\-9999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 13
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 13
          Text = '(  )    -    '
        end
      end
      object GroupBox2: TGroupBox
        Left = 847
        Top = 8
        Width = 234
        Height = 537
        Caption = 'Pesquisa'
        TabOrder = 1
      end
    end
  end
end
