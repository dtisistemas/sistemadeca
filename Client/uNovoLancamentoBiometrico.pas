unit uNovoLancamentoBiometrico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, ComCtrls, StdCtrls, Buttons, Mask, Db;

type
  TfrmNovoLancamentoBiometrico = class(TForm)
    GroupBox1: TGroupBox;
    Label9: TLabel;
    cbUnidade: TComboBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Bevel1: TBevel;
    Bevel2: TBevel;
    dbgCadastro: TDBGrid;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    lbMatricula: TLabel;
    lbNome: TLabel;
    lbNascimento: TLabel;
    lbSexo: TLabel;
    lbIMC: TLabel;
    lbClassificacao: TLabel;
    edPeso: TEdit;
    edAltura: TEdit;
    dbgBiometricos: TDBGrid;
    GroupBox10: TGroupBox;
    meDatAvaliacao: TMaskEdit;
    dsCadastros: TDataSource;
    lbIdade: TLabel;
    btnCalculaImc: TSpeedButton;
    GroupBox11: TGroupBox;
    chkProblemaSaude: TCheckBox;
    GroupBox12: TGroupBox;
    GroupBox13: TGroupBox;
    meProblemaSaude: TMemo;
    meObservacoes: TMemo;
    dsBiometricos: TDataSource;
    GroupBox14: TGroupBox;
    GroupBox15: TGroupBox;
    Bevel3: TBevel;
    meProblemasSaude: TMemo;
    meObservacoesLancamento: TMemo;
    Panel2: TPanel;
    lbIdentificacao: TLabel;
    edIdade: TEdit;
    procedure FormShow(Sender: TObject);
    procedure AtualizaTelaB (AModo : String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure dbgCadastroCellClick(Column: TColumn);
    function CalculaIdade(AData:TDateTime):String;
    procedure btnCalculaImcClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dbgBiometricosCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovoLancamentoBiometrico: TfrmNovoLancamentoBiometrico;
  vUnidade1, vUnidade2 : TStringList;
  mPeso, mAltura, mIMC : Real;

implementation

uses uDM, uFichaCrianca, uUtil, uUsuarios, uLoginNovo, uPrincipal,
  uOpcoesImpressaoBiometricos;

{$R *.DFM}

procedure TfrmNovoLancamentoBiometrico.AtualizaTelaB(AModo: String);
begin
  if AModo = 'Padr�o' then
  begin

    cbUnidade.ItemIndex := -1;
    PageControl1.ActivePageIndex := 0;
    lbMatricula.Caption := '';
    lbNome.Caption := '';
    lbIdade.Caption := '';
    lbSexo.Caption := '';
    edPeso.Clear;
    edPeso.Enabled := False;
    edAltura.Clear;
    edAltura.Enabled := False;
    lbIMC.Caption := '';
    lbClassificacao.Caption := '';
    meDatAvaliacao.Clear;
    chkProblemaSaude.Checked := False;
    chkProblemaSaude.Enabled := False;
    meProblemaSaude.Lines.Clear;
    meProblemaSaude.Enabled := False;
    meObservacoes.Lines.Clear;
    meObservacoes.Enabled := False;

    btnCalculaImc.Enabled := False;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;
    btnSair.Enabled := True;
    Bevel1.Enabled := False;

  end

  else if AModo = 'Novo' then
  begin

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;
    Bevel1.Enabled := True;
    btnCalculaImc.Enabled := True;

    edPeso.Enabled := True;
    edAltura.Enabled := True;
    meProblemaSaude.Enabled := True;
    meObservacoes.Enabled := True;

  end;

end;

procedure TfrmNovoLancamentoBiometrico.FormShow(Sender: TObject);
begin

  vUnidade1 := TStringLIst.Create();
  vUnidade2 := TStringLIst.Create();

  //Carrega os dados das unidades no combo cbUnidade
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        vUnidade1.Add(IntToStr(FieldByName('cod_unidade').AsInteger));
        vUnidade2.Add(FieldByName('nom_unidade').AsString);
        cbUnidade.Items.Add(FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;

      AtualizaTelaB('Padr�o');

    end;
  except end;

  //Desvincula o grid inicialmente para exibi��o dos dados
  dbgCadastro.DataSource := nil;
  //dbgBiometricos.DataSource := nil;
end;

procedure TfrmNovoLancamentoBiometrico.btnNovoClick(Sender: TObject);
begin
  AtualizaTelaB('Novo');
  meDatAvaliacao.Text := DateToStr(Date());
end;

procedure TfrmNovoLancamentoBiometrico.btnCancelarClick(Sender: TObject);
begin
  AtualizaTelaB('Padr�o');
end;

procedure TfrmNovoLancamentoBiometrico.btnSairClick(Sender: TObject);
begin
  frmNovoLancamentoBiometrico.Close;
end;

procedure TfrmNovoLancamentoBiometrico.cbUnidadeClick(Sender: TObject);
begin

  dbgCadastro.DataSource := dsCadastros;
  //dbgBiometricos.DataSource := dsBiometricos;

  //Carrega os dados dos cadastros lan�ados para a unidade

  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_cod_unidade').AsInteger := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;

      if RecordCount > 0 then
        dbgCadastro.Refresh
      else
      begin
        ShowMessage('N�o existem crian�as/adolescentes cadastrados para a unidade selecionada...');
      end;
    end;
  except end;
end;

procedure TfrmNovoLancamentoBiometrico.dbgCadastroCellClick(
  Column: TColumn);
begin

  lbMatricula.Caption := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
  lbNome.Caption := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
  lbNascimento.Caption := dmDeca.cdsSel_Cadastro.FieldByName('dat_nascimento').Value;
  edIdade.Text := Copy(CalculaIdade(dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime),1,2);
  lbSexo.Caption := dmDeca.cdsSel_Cadastro.FieldByName('ind_sexo').Value;
  lbIdentificacao.Caption := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;

  //Exibe os Lan�amentos efetuados para o aluno selecionado na aba Hist�rico
  try
    with dmDeca.cdsSel_Biometricos do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
      Open;
      dbgBiometricos.Refresh;
    end;
  except end;



end;

function TfrmNovoLancamentoBiometrico.CalculaIdade(
  AData: TDateTime): String;
var
   idadea,idadem,ano1,ano,mes1,mes,dia1,dia: integer;
   data: string;
begin
   data := datetostr(AData);
   dia1:=strtoint(copy(data,0,2));
   mes1:=strtoint(copy(data,4,2));
   Ano1:=strtoint(copy(data,7,4));
   dia:=strtoint(copy(datetostr(date),0,2));
   mes:=strtoint(copy(datetostr(date),4,2));
   Ano:=strtoint(copy(datetostr(date),7,4));

   if mes1<mes then
   begin
     if dia1<=dia then
     begin
       idadea:=ano - ano1;
       idadem :=mes - mes1 ;
     end
     else
     begin
       idadea:=ano - ano1 ;
       idadem:=mes - mes1 - 1;
     end;
   end
   else
   begin
      if mes1 = mes then
      begin
        if dia1<=dia then
        begin
          idadea:=ano - ano1;
          idadem :=mes - mes1; // tem k dar 0
        end
        else
        begin
          idadea:=ano - ano1 -1;
          idadem:=mes - mes1 - 1;
          idadem:=12 + idadem;
        end;
      end
      else
      begin
        if dia1<=dia then
          begin
            idadea:=ano - ano1 - 1;
            idadem :=mes - mes1;
            idadem:=12 + idadem;
          end
          else
          begin
            idadea:=ano - ano1 -1;
            idadem:=mes - mes1 - 1;
            idadem:=12 + idadem;
          end;
      end;
   end;
   result:= inttostr(idadea)+' anos e '+inttostr(idadem)+' meses';
end;

procedure TfrmNovoLancamentoBiometrico.btnCalculaImcClick(Sender: TObject);
begin

  //Criar fun��o para calcular diferentemente a situa��o de crian�a e de adolescente
  mPeso := 0;
  mAltura := 0;
  mIMC := 0;

  if ((edPeso.Text <> '') or (edPeso.Text <> '00,000')) and ((edAltura.Text <> '') or (edAltura.Text <> '0,00')) then
  begin
    mPeso := StrToFloat(edPeso.Text);
    mAltura := StrToFloat(edAltura.Text);
    mIMC := mPeso / (mAltura * mAltura);
    lbIMC.Caption := Format('%2.1f', [mIMC]);
  end;


  try
  with dmDeca.cdsSel_ProcuraIMC do
  begin
    Close;
    Params.ParamByName('@pe_id_indicebiometrico').Value := Null;
    Params.ParamByName('@pe_ind_sexo').AsString := Copy(lbSexo.Caption,1,1);
    Params.ParamByName('@pe_num_idade').AsInteger := StrToInt(Trim(edIdade.Text));
    Params.ParamByName('@pe_num_valorIMC').AsFloat := StrToFloat(lbIMC.Caption);//Trunc(mIMC);//StrToFloat(lbIMC.Caption);
    Open;

    lbClassificacao.Caption := dmDeca.cdsSel_ProcuraIMC.FieldByName('dsc_classificacao').AsString;

  end;
  except end;
end;

procedure TfrmNovoLancamentoBiometrico.btnSalvarClick(Sender: TObject);
begin

  try
    //Primeiro valida o Status da crian�a - se ela pode sofrer este movimento
    //Grava os dados da Avalia��o Biom�trica na Tabela BIOMETRICOS
    with dmDeca.cdsInc_Biometricos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Params.ParamByName ('@pe_val_peso').AsFloat := mPeso;
      Params.ParamByName ('@pe_val_altura').AsFloat := mAltura;
      Params.ParamByName ('@pe_val_imc').AsFloat := StrToFloat(lbIMC.Caption);
      Params.ParamByName ('@pe_dsc_classificacao_imc').AsString := lbClassificacao.Caption;

      if chkProblemaSaude.Checked = True then
        Params.ParamByName ('@pe_flg_problema_saude').AsInteger := 1
      else
        Params.ParamByName ('@pe_flg_problema_saude').AsInteger := 0;

      Params.ParamByName ('@pe_dsc_problema_saude').Value := GetValue(meProblemaSaude.Lines.Text);
      Params.ParamByName ('@pe_dsc_observacoes').Value := GetValue(meObservacoes.Lines.Text);
      Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Params.ParamByName ('@pe_log_data').AsDateTime := StrToDate(meDatAvaliacao.Text); 
      Execute;
    end;

    //Lan�a os dados no cadastro_hist�rico
    with dmDeca.cdsInc_Cadastro_Historico do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Params.ParamByName('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro.FieldBYName ('cod_unidade').AsInteger;//vvCOD_UNIDADE;
      Params.ParamByName('@pe_dat_historico').Value := GetValue(meDatAvaliacao, vtDate);
      Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 18;
      Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Lan�amento de Avalia��o Biom�trica';
      Params.ParamByName('@pe_dsc_ocorrencia').Value := Null;
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;
    end;

    //Faz a atualiza��o do Cadastro, nos campos PESO e ALTURA, conforme avalia��o Biom�trica
    with dmDeca.cdsAlt_Cadastro_Dados_Biometricos do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;;
      Params.ParamByName('@pe_val_peso').AsFloat := StrToFloat(edPeso.Text);
      Params.ParamByName('@pe_val_altura').AsFloat := StrToFloat(edAltura.Text);
      Execute;
    end;

    with dmDeca.cdsSel_Biometricos do
    begin
      Close;
      //Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Open;
      dbgBiometricos.Refresh;
    end;
    
    edPeso.Clear;
    edAltura.Clear;
    AtualizaTelaB('Padr�o');
  except
    Application.MessageBox('Aten��o!'+#13+#10+#13+#10+
             'Ocorreu um ERRO e os dados n�o foram gravados. Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Avalia��o Biom�trica',
             MB_OK + MB_ICONERROR);
  end;
end;

procedure TfrmNovoLancamentoBiometrico.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm (TfrmOpcoesImpressaoBiometricos, frmOpcoesImpressaoBiometricos);
  frmOpcoesImpressaoBiometricos.ShowModal;
end;

procedure TfrmNovoLancamentoBiometrico.dbgBiometricosCellClick(
  Column: TColumn);
begin
  meProblemaSaude.Clear;
  meObservacoesLancamento.Clear;
  meProblemaSaude.Text := dmDeca.cdsSel_Biometricos.FieldByName('dsc_problema_saude').Value;
  meObservacoesLancamento.Text := dmDeca.cdsSel_Biometricos.FieldByName('dsc_observacoes').Value
end;

end.
