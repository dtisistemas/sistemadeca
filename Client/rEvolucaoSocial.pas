unit rEvolucaoSocial;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelEvolucaoSocial = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRLabel1: TQRLabel;
    lbNome: TQRLabel;
    QRLabel3: TQRLabel;
    lblMatricula: TQRLabel;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel5: TQRLabel;
    QRSysData1: TQRSysData;
    lbUsuario: TQRLabel;
    QRShape1: TQRShape;
    QRLabel7: TQRLabel;
    QRShape7: TQRShape;
    QRLabel2: TQRLabel;
    QRShape8: TQRShape;
    QRLabel11: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    PageFooterBand1: TQRBand;
    QRLabel6: TQRLabel;
  private

  public

  end;

var
  relEvolucaoSocial: TrelEvolucaoSocial;

implementation

uses uDM;

{$R *.DFM}

end.
