unit uConsultaAcompanhamentoEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db;

type
  TfrmConsultaAcompanhamentoEscolar = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cbUnidade: TComboBox;
    Panel2: TPanel;
    dgAcompanhamento: TDBGrid;
    Panel3: TPanel;
    Panel4: TPanel;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    rgAproveitamento: TRadioGroup;
    dsAcompanhamento: TDataSource;
    edDiasLetivos: TEdit;
    edFaltas: TEdit;
    GroupBox3: TGroupBox;
    edBimestre: TEdit;
    GroupBox4: TGroupBox;
    edAno: TEdit;
    GroupBox5: TGroupBox;
    meObservacoes: TMemo;
    Label3: TLabel;
    edNome: TEdit;
    btnPesquisa: TSpeedButton;
    procedure AtualizaCamposBotoes (Modo: String);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure dgAcompanhamentoCellClick(Column: TColumn);
    procedure edDiasLetivosExit(Sender: TObject);
    procedure rgAproveitamentoExit(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure edNomeKeyPress(Sender: TObject; var Key: Char);
    procedure dgAcompanhamentoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaAcompanhamentoEscolar: TfrmConsultaAcompanhamentoEscolar;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses uDM, uPrincipal, uUtil;

{$R *.DFM}

{ TfrmConsultaAcompanhamentoEscolar }

procedure TfrmConsultaAcompanhamentoEscolar.AtualizaCamposBotoes(
  Modo: String);
begin
 if Modo = 'Padrao' then
 begin
   cbUnidade.ItemIndex := -1;
   edDiasLetivos.Clear;
   edBimestre.Clear;
   edBimestre.Enabled := False;
   edAno.Clear;
   edAno.Enabled := False;
   edDiasLetivos.Enabled := False;
   edFaltas.Clear;
   edFaltas.Enabled := False;
   rgAproveitamento.ItemIndex := -1;
   rgAproveitamento.Enabled := False;
   btnAlterar.Enabled := False;
   btnCancelar.Enabled := True;
   btnSair.Enabled := True;
 end
 else if Modo = 'Alterar' then
 begin
   edDiasLetivos.Enabled := True;
   edFaltas.Enabled := True;
   rgAproveitamento.ItemIndex := 1;
   rgAproveitamento.Enabled := True;
   btnAlterar.Enabled := True;
   btnCancelar.Enabled := True;
   btnSair.Enabled := False;
 end;
end;

procedure TfrmConsultaAcompanhamentoEscolar.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a Lista de Unidades
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByname ('@pe_cod_unidade').Value := Null;
      Params.ParamByname ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin
        cbUnidade.Items.Add (FieldByName ('nom_unidade').AsString);
        vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').AsInteger));
        vListaUnidade2.Add (FieldByName ('nom_unidade').AsString);
        Next;
      end
    end
  except end;

end;

procedure TfrmConsultaAcompanhamentoEscolar.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
  dmDeca.cdsSel_Acompanhamento.Close;
end;

procedure TfrmConsultaAcompanhamentoEscolar.btnSairClick(Sender: TObject);
begin
  frmConsultaAcompanhamentoEscolar.Close;
  vListaUnidade1.Free;
  vListaUnidade2.Free;
end;

procedure TfrmConsultaAcompanhamentoEscolar.cbUnidadeClick(
  Sender: TObject);
begin
  //Limpa o DBGrid
  dmDeca.cdsSel_Acompanhamento.Close;
  dgAcompanhamento.Refresh;

  try
    with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Params.ParamByName ('@pe_num_bimestre').Value := Null;
      Params.ParamByName ('@pe_num_ano').Value := Null;
      Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_nome').Value := Null;
      Open;

      dgAcompanhamento.Refresh;
  end
  except end;

end;

procedure TfrmConsultaAcompanhamentoEscolar.dgAcompanhamentoCellClick(
  Column: TColumn);
begin
  AtualizaCamposBotoes ('Alterar');
  edBimestre.Text := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName ('num_bimestre').AsInteger) + '� Bimestre';
  edAno.Text := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName ('num_ano').Value);

  if (dmDeca.cdsSel_Acompanhamento.FieldByName ('num_dias_letivos').Value) >0 then
    edDiasLetivos.Text := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName ('num_dias_letivos').AsInteger)
  else
    edDiasLetivos.Text := '0';

  if (dmDeca.cdsSel_Acompanhamento.FieldByName ('num_faltas').AsInteger) >0 then
    edFaltas.Text := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName ('num_faltas').AsInteger)
  else
    edFaltas.Text := '0';

  meObservacoes.Lines.Clear;
  meObservacoes.Lines.Add(dmDeca.cdsSel_Acompanhamento.FieldByName ('dsc_observacoes').Value);
  rgAproveitamento.ItemIndex := dmDeca.cdsSel_Acompanhamento.FieldByName ('flg_aproveitamento').AsInteger;
end;

procedure TfrmConsultaAcompanhamentoEscolar.edDiasLetivosExit(
  Sender: TObject);
begin
  if (Length(edDiasLetivos.Text) < 1) then
  begin
    Application.MessageBox('Aten��o !!!' + #13+#10 +
                           'Este campo [DIAS LETIVOS] � de preenchimento obrigat�rio.',
                           'Acomp. Escolar - Erro',
                           MB_ICONQUESTION + MB_YESNO);
    edDiasLetivos.SetFocus;
  end;
end;

procedure TfrmConsultaAcompanhamentoEscolar.rgAproveitamentoExit(
  Sender: TObject);
begin
  if (rgAproveitamento.ItemIndex = -1) then
  begin
    Application.MessageBox('Aten��o !!!' + #13+#10 +
                           'Este campo [APROVEITAMENTO ESCOLAR] � de preenchimento obrigat�rio.',
                           'Acom�. Escolar - Erro',
                           MB_OK + MB_ICONERROR);
    rgAproveitamento.SetFocus;
  end;
end;

procedure TfrmConsultaAcompanhamentoEscolar.btnAlterarClick(
  Sender: TObject);
begin
  if (Length(edDiasLetivos.Text) > 0) and (Length(edFaltas.Text) > 0) and (rgAproveitamento.ItemIndex > -1) then
  begin
    try
      with dmDeca.cdsAlt_Acompanhamento do
      begin
        Close;
        Params.ParamByname ('@pe_cod_id_acompanhamento').AsInteger := dmDeca.cdsSel_Acompanhamento.FieldByname ('cod_id_acompanhamento').AsInteger;;
        Params.ParamByname ('@pe_num_dias_letivos').AsInteger := StrToInt(edDiasLetivos.Text);
        Params.ParamByname ('@pe_num_faltas').AsInteger := StrToInt(edFaltas.Text);
        Params.ParamByName ('@pe_flg_aproveitamento').AsInteger := rgAproveitamento.ItemIndex;
        Params.ParamByname ('@pe_dsc_observacoes').Value := GetValue(meObservacoes.Text);
        Execute;

        with dmDeca.cdsSel_Acompanhamento do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').Value := Null;
          Params.ParamByName ('@pe_num_bimestre').Value := Null;
          Params.ParamByName ('@pe_num_ano').Value := Null;
          Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
          Params.ParamByName ('@pe_cod_escola').Value := Null;
          Params.ParamByName ('@pe_nom_nome').AsString := Trim(edNome.Text) + '%';
          Open;

          dgAcompanhamento.Refresh;
          //AtualizaCamposBotoes ('Padrao');
          ShowMessage ('Registro alterado com sucesso !!!');

        end
      end
    except
    begin
      Application.MessageBox('Aten��o!!! Imposs�vel ALTERAR dados de Acompanhamento Escola,' + #13+#10 +
                             'pois alguns dados est�o inv�lidos (N.� FALTAS, DIAS LETIVOS ou APROVEITAMENTO ESCOLAR)',
                             'Altera��o Acomp. Escolar - Erro',
                             MB_OK + MB_ICONERROR);
      AtualizaCamposBotoes ('Padrao');
    end;
    end;
  end;
end;

procedure TfrmConsultaAcompanhamentoEscolar.btnPesquisaClick(
  Sender: TObject);
begin
  (*try
    with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Params.ParamByName ('@pe_num_bimestre').Value := Null;
      Params.ParamByName ('@pe_num_ano').Value := Null;
      Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_nom_nome').AsString := Trim(edNome.Text) + '%';
      Open;

      dgAcompanhamento.Refresh;

    end;
  except end;*)
end;

procedure TfrmConsultaAcompanhamentoEscolar.edNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
  begin
    try
    with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_num_bimestre').Value := Null;
      Params.ParamByName ('@pe_num_ano').Value := Null;
      Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_nome').AsString := Trim(edNome.Text) + '%';
      Open;

      dgAcompanhamento.Refresh;

    end;
    except end;
  end;
end;

procedure TfrmConsultaAcompanhamentoEscolar.dgAcompanhamentoDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').Value = 0 then
  begin
    //Abaixo da m�dia
    dgAcompanhamento.Canvas.Font.Color := clWhite;
    dgAcompanhamento.Canvas.Brush.Color := clRed;
  end
  else if dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').Value = 1 then
  begin
    //Dentro da m�dia
    dgAcompanhamento.Canvas.Font.Color := clWhite;
    dgAcompanhamento.Canvas.Brush.Color := clGreen;
  end
  else if dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').Value = 2 then
  begin
    //Acima da M�dia
    dgAcompanhamento.Canvas.Font.Color := clWhite;
    dgAcompanhamento.Canvas.Brush.Color := clBlue;
  end
  else
  begin
    dgAcompanhamento.Canvas.Font.Color := clWhite;
    dgAcompanhamento.Canvas.Brush.Color := clBlack;
  end;

  dgAcompanhamento.Canvas.FillRect(Rect);
  dgAcompanhamento.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

end.
