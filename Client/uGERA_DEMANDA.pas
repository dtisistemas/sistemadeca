unit uGERA_DEMANDA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, DBGrids, Buttons, ComCtrls, Db;

type
  TfrmGERA_DEMANDA = class(TForm)
    PageControl3: TPageControl;
    TabSheet6: TTabSheet;
    btnGERA_DEMANDA: TSpeedButton;
    Bevel3: TBevel;
    Label5: TLabel;
    dbgDEMANDAS: TDBGrid;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    edtNOME: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    edtNUMERO_INSCRICAO: TEdit;
    edtNUMERO_DIGITO: TEdit;
    GroupBox9: TGroupBox;
    edtPONTUACAO: TEdit;
    Panel1: TPanel;
    btnALTERAR_DADOSDEMANDA: TSpeedButton;
    SpeedButton2: TSpeedButton;
    btnVER_DADOSFILTRO: TSpeedButton;
    btnDADOS_COMISSAOTRIAGEM: TSpeedButton;
    GroupBox11: TGroupBox;
    btnPESQUISA_NOMEDEMANDA: TSpeedButton;
    btnPESQUISA_TODADEMANDA: TSpeedButton;
    edtNOME_DEMANDA: TEdit;
    GroupBox6: TGroupBox;
    chkSELECIONAR_PARALIGACAO: TCheckBox;
    medOBSERVACOES: TMemo;
    dsGERA_DEMANDA: TDataSource;
    GroupBox5: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    btnAPLICAR_FILTRO: TSpeedButton;
    btnLIMPAR_FILTROS: TSpeedButton;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    btnLIMPA_REGIAO: TSpeedButton;
    btnLIMPA_FAIXAETARIA: TSpeedButton;
    btnLIMPA_UNIDADE: TSpeedButton;
    btnLIMPA_SITUACAODEMANDA: TSpeedButton;
    btnLIMPA_PERIODOESCOLAR: TSpeedButton;
    btnLIMPA_SITUACAOCADASTRO: TSpeedButton;
    btnLIMPA_SELECIONADOS: TSpeedButton;
    cmbREGIAO: TComboBox;
    cmbFAIXA_ETARIA: TComboBox;
    cmbUNIDADE: TComboBox;
    cmbPERIODO_ESCOLA: TComboBox;
    cmbSITUACAO_CADASTRO: TComboBox;
    cmbSELECIONADOS_DEMANDA: TComboBox;
    cmbSITUACAO_DEMANDA: TComboBox;
    procedure btnGERA_DEMANDAClick(Sender: TObject);
    procedure btnAPLICAR_FILTROClick(Sender: TObject);
    procedure btnLIMPAR_FILTROSClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDADOS_COMISSAOTRIAGEMClick(Sender: TObject);
    procedure btnVER_DADOSFILTROClick(Sender: TObject);
    procedure dbgDEMANDASCellClick(Column: TColumn);
    procedure btnALTERAR_DADOSDEMANDAClick(Sender: TObject);
    procedure InsereRegistroAtendimento;
    procedure btnLIMPA_REGIAOClick(Sender: TObject);
    procedure btnLIMPA_FAIXAETARIAClick(Sender: TObject);
    procedure btnLIMPA_UNIDADEClick(Sender: TObject);
    procedure btnLIMPA_SITUACAODEMANDAClick(Sender: TObject);
    procedure btnLIMPA_PERIODOESCOLARClick(Sender: TObject);
    procedure btnLIMPA_SITUACAOCADASTROClick(Sender: TObject);
    procedure btnLIMPA_SELECIONADOSClick(Sender: TObject);
    procedure btnPESQUISA_NOMEDEMANDAClick(Sender: TObject);
    procedure btnPESQUISA_TODADEMANDAClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGERA_DEMANDA: TfrmGERA_DEMANDA;
  vListaUnidadeDem1, vListaUnidadeDem2,
  vListaRegiaoDem1, vListaRegiaoDem2 : TStringList;
  vIndex : Integer;  

implementation

uses udmTriagemDeca, uPrincipal, uDM, rDemanda;

{$R *.DFM}

procedure TfrmGERA_DEMANDA.btnGERA_DEMANDAClick(Sender: TObject);
begin

  //Confirma ao usu�rio se ele realmente deseja gerar uma nova demanda...
  if Application.MessageBox ('Deseja realmente gerar uma nova rela��o de demanda?' +#13+#10+
                             'Os dados atuais da demanda ser�o exclu�dos e ser�o gerados novamente. Tem certeza?',
                             '[Sistema Deca] - Exclus�o de Demanda atual',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    //Caso confirmado, excluir os dados da tabela DEMANDA
    try
      with dmTriagemDeca.cdsDEL_DEMANDA do
      begin
        Close;
        Execute;

        //Verifica se todos os registros foram exclu�dos...
        try
          with dmTriagemDeca.cdsGERA_DEMANDA do
          begin
            Close;
            Params.ParamByName ('@pe_cod_regiao').Value        := Null;
            Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
            Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
            Params.ParamByName ('@pe_cod_unidade').Value       := Null;
            Params.ParamByName ('@pe_flg_situacao').Value      := Null;
            Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
            Params.ParamByName ('@pe_cod_situacao').Value      := Null;
            Params.ParamByName ('@pe_nom_nome').Value          := Null;
            Params.ParamByName ('@pe_flg_selecionado').Value   := Null;
            Open;

            if (dmTriagemDeca.cdsGERA_DEMANDA.RecordCount > 0) then
            begin
              //Gerar os dados novamente e inser�-los na tabela DEMANDA
              while not (dmTriagemDeca.cdsGERA_DEMANDA.eof) do
              begin
                with dmTriagemDeca.cdsINS_DEMANDA do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
                  Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao1').Value;
                  Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
                  Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao2').Value;
                  Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
                  Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao3').Value;
                  Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
                  Params.ParamByName ('@pe_dsc_observacoes').Value   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_observacoes').Value;
                  Params.ParamByName ('@pe_flg_situacao').Value      := 3; //Deixar o padr�o como EM DEMANDA
                  Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
                  Params.ParamByName ('@pe_flg_selecionado').Value   := 0; //N�o Selecionado
                  Execute;
                end;
                dmTriagemDeca.cdsGERA_DEMANDA.Next;
              end;
              btnAPLICAR_FILTRO.Click;
            end;
          end;
        except
          Application.MessageBox ('Os dados da demanda n�o foram exclu�dos com sucesso.' +#13+#10+
                                  'Tente novamente!!!',
                                  '[Sistema Deca] - Excluir dados de DEMANDA',
                                  MB_OK + MB_ICONERROR);
        end;


      end;
    except
      Application.MessageBox ('Um erro ocorreu e os dados n�o foram exclu�dos.' +#13+#10+
                              'Tente novamente!!!',
                              '[Sistema Deca] - Erro excluindo dados de DEMANDA',
                              MB_OK + MB_ICONERROR);

    end;

  end
  else
  begin
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := Null;
      Params.ParamByName ('@pe_nom_nome').Value          := Null;
      Open;
      dbgDEMANDAS.Refresh;
    end;

  end;
end;

procedure TfrmGERA_DEMANDA.btnAPLICAR_FILTROClick(Sender: TObject);
begin
  //Verificar qual(is) filtros est�o selecionados...
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;

      //REGI�O
      if (cmbREGIAO.ItemIndex = -1) then Params.ParamByName ('@pe_cod_regiao').Value := Null else Params.ParamByName ('@pe_cod_regiao').AsInteger := StrToInt(vListaRegiaoDem1.Strings[cmbREGIAO.ItemIndex]);

      //FAIXA ET�RIA
      if (cmbFAIXA_ETARIA.ItemIndex = -1) then
      begin
        Params.ParamByName ('@pe_faixaetaria1').Value := Null;
        Params.ParamByName ('@pe_faixaetaria2').Value := Null;
      end
      else
      begin
        if (cmbFAIXA_ETARIA.ItemIndex = -1) then
        begin
          Params.ParamByName ('@pe_faixaetaria1').Value := Null;
          Params.ParamByName ('@pe_faixaetaria2').Value := Null;
        end
        else
        begin
          case (cmbFAIXA_ETARIA.ItemIndex) of
          0: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 72;
               Params.ParamByName ('@pe_faixaetaria2').Value := 83;
             end;

          1: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 84;
               Params.ParamByName ('@pe_faixaetaria2').Value := 95;
             end;

          2: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 96;
               Params.ParamByName ('@pe_faixaetaria2').Value := 107;
             end;

          3: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 108;
               Params.ParamByName ('@pe_faixaetaria2').Value := 119;
             end;

          4: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 120;
               Params.ParamByName ('@pe_faixaetaria2').Value := 131;
             end;

          5: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 132;
               Params.ParamByName ('@pe_faixaetaria2').Value := 143;
             end;

          6: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 144;
               Params.ParamByName ('@pe_faixaetaria2').Value := 155;
             end;

          7: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 156;
               Params.ParamByName ('@pe_faixaetaria2').Value := 167;
             end;

          8: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 168;
               Params.ParamByName ('@pe_faixaetaria2').Value := 179;
             end;

          9: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 180;
               Params.ParamByName ('@pe_faixaetaria2').Value := 191;
             end;

          10: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 192;
               Params.ParamByName ('@pe_faixaetaria2').Value := 203;
             end;

          11: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 204;
               Params.ParamByName ('@pe_faixaetaria2').Value := 215;
             end;

          12: begin
               Params.ParamByName ('@pe_faixaetaria1').Value := 216;
               Params.ParamByName ('@pe_faixaetaria2').Value := 1000;
             end;
          end; //case
        end;
      end;

      //UNIDADE
      if (cmbUNIDADE.ItemIndex = -1) then Params.ParamByName ('@pe_cod_unidade').Value := Null else Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidadeDem1.Strings[cmbUNIDADE.ItemIndex]);

      //SITUA��O DE DEMANDA
      if (cmbSITUACAO_DEMANDA.ItemIndex = -1) then
        Params.ParamByName ('@pe_flg_situacao').Value := Null
      else
      begin
        case (cmbSITUACAO_DEMANDA.ItemIndex) of
          0: Params.ParamByName ('@pe_flg_situacao').Value := 0;
          1: Params.ParamByName ('@pe_flg_situacao').Value := 1;
          2: Params.ParamByName ('@pe_flg_situacao').Value := 2;
          3: Params.ParamByName ('@pe_flg_situacao').Value := 3;
        end;
      end;

      //PER�ODO ESCOLAR
      if (cmbPERIODO_ESCOLA.ItemIndex = -1) then Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null else Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Trim(cmbPERIODO_ESCOLA.Text);

      //SITUA��O CADASTRO
      if (cmbSITUACAO_CADASTRO.ItemIndex = -1) then
        Params.ParamByName ('@pe_cod_situacao').Value     := Null
      else
      begin
        case (cmbSITUACAO_CADASTRO.ItemIndex) of
          0: Params.ParamByName ('@pe_cod_situacao').Value := 12; //INSCRITO = 12
          1: Params.ParamByName ('@pe_cod_situacao').Value := 15; //NECESSIDADE ESPECIAL = 15
          2: Params.ParamByName ('@pe_cod_situacao').Value := 16; //PEND�NCIA DE INSCRI��O = 16
          3: Params.ParamByName ('@pe_cod_situacao').Value := 19; //ESCOLA INTEGRAL = 19
          4: Params.ParamByName ('@pe_cod_situacao').Value := 22; //COMISSAO TRIAGEM = 22
        end;
      end;

      //NOME
      Params.ParamByName ('@pe_nom_nome').Value           := Null;

      //SELECIONADO
      if (cmbSELECIONADOS_DEMANDA.ItemIndex = -1) then
        Params.ParamByName ('@pe_flg_selecionado').Value  := Null
      else
      begin
        case (cmbSELECIONADOS_DEMANDA.ItemIndex) of
          0: Params.ParamByName ('@pe_flg_selecionado').Value := 0; //N�O SELECIONADOS
          1: Params.ParamByName ('@pe_flg_selecionado').Value := 1; //SELECIONADOS
        end;
      end;
      
      Open;
      Application.MessageBox (PChar('Encontrados ' + IntToStr(dmTriagemDeca.cdsGERA_DEMANDA.RecordCount) + ' registros de acordo com os crit�rios'),
                                    '[Sistema Deca] - Totlizador de consulta',
                                    MB_OK + MB_ICONINFORMATION);
      dbgDEMANDAS.Refresh;
    end
  except end;
end;

procedure TfrmGERA_DEMANDA.btnLIMPAR_FILTROSClick(Sender: TObject);
begin
  //btnGERA_DEMANDA.OnClick(Self);
  cmbREGIAO.ItemIndex               := -1;
  cmbFAIXA_ETARIA.ItemIndex         := -1;
  cmbUNIDADE.ItemIndex              := -1;
  cmbSITUACAO_DEMANDA.ItemIndex     := -1;
  cmbPERIODO_ESCOLA.ItemIndex       := -1;
  cmbSITUACAO_CADASTRO.ItemIndex    := -1;
  cmbSELECIONADOS_DEMANDA.ItemIndex := -1;
  //btnAPLICAR_FILTRO.OnClick(Self);
end;

procedure TfrmGERA_DEMANDA.FormShow(Sender: TObject);
begin
  //Carregar as regi�es
  vListaRegiaoDem1 := TStringList.Create();
  vListaRegiaoDem2 := TStringList.Create();

  with dmTriagemDeca.cdsSel_Par_Cadastro_Regioes do
  begin
    Close;
    Params.ParamByName ('@cod_regiao').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.eof) do
      begin
        cmbREGIAO.Items.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        vListaRegiaoDem1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('cod_regiao').Value));
        vListaRegiaoDem2.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.Next;
      end;
    end;
  end;

  //****************************************************************************

  //Carregar a lista de Unidades
  vListaUnidadeDem1 := TStringList.Create();
  vListaUnidadeDem2 := TStringList.Create();

  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := Null;
    Params.ParamByName ('@pe_nom_unidade').Value := Null;
    Open;

    if (dmDeca.cdsSel_Unidade.RecordCount > 0) then
    begin
      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        //Carrega apenas as unidades "ATIVAS"
        if (dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0) then
        begin
          cmbUNIDADE.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidadeDem1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidadeDem2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  end;
end;

procedure TfrmGERA_DEMANDA.btnDADOS_COMISSAOTRIAGEMClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := Null;
      Params.ParamByName ('@pe_nom_nome').Value          := Null;
      Params.ParamByName ('@pe_flg_selecionado').Value   := Null; //1 Apenas os SELECIONADOS
      Open;

      Application.CreateForm(TrelDemanda, relDemanda);
      relDemanda.Prepare;
      relDemanda.Preview;
      relDemanda.Free;
    end;
  except end;
end;

procedure TfrmGERA_DEMANDA.btnVER_DADOSFILTROClick(Sender: TObject);
begin
  Application.CreateForm(TrelDemanda, relDemanda);
  relDemanda.Prepare;
  relDemanda.Preview;
  relDemanda.Free;
end;

procedure TfrmGERA_DEMANDA.dbgDEMANDASCellClick(Column: TColumn);
begin
  edtNUMERO_INSCRICAO.Text := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NUM_INSCRICAO').Value;
  edtNUMERO_DIGITO.Text    := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NUM_INSCRICAO_DIGT').Value;
  edtNOME.Text             := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('NOM_NOME').Value;
  edtPONTUACAO.Text        := dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('PONTUACAO').Value;
  if (dmTriagemDeca.cdsGERA_DEMANDA.FieldByname ('flg_selecionado').Value = 1) then chkSELECIONAR_PARALIGACAO.Checked := True else chkSELECIONAR_PARALIGACAO.Checked := False;
end;

procedure TfrmGERA_DEMANDA.btnALTERAR_DADOSDEMANDAClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsUPD_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value     := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
      Params.ParamByName ('@pe_dat_ligacao1').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao1').Value;
      Params.ParamByName ('@pe_dat_ligacao2').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao2').Value;
      Params.ParamByName ('@pe_dat_ligacao3').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dat_ligacao3').Value;
      Params.ParamByName ('@pe_dsc_quem_ligacao1').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao1').Value;
      Params.ParamByName ('@pe_dsc_quem_ligacao2').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao2').Value;
      Params.ParamByName ('@pe_dsc_quem_ligacao3').Value := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_quemligacao3').Value;
      Params.ParamByName ('@pe_dsc_observacoes').Value   := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('dsc_observacoes').Value;
      Params.ParamByName ('@pe_flg_situacao').Value      := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('flg_situacao').Value;
      Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
      if (chkSELECIONAR_PARALIGACAO.Checked = True) then Params.ParamByName ('@pe_flg_selecionado').Value := 1 else Params.ParamByName ('@pe_flg_selecionado').Value := 0;
      Execute;

      InsereRegistroAtendimento;
      //btnAPLICAR_FILTRO.Click;
      edtNUMERO_INSCRICAO.Clear;
      edtNUMERO_DIGITO.Clear;
      edtNOME.Clear;
      edtPONTUACAO.Clear;
      chkSELECIONAR_PARALIGACAO.Checked := False;
      medOBSERVACOES.Clear;

    end;
  except end;
end;

procedure TfrmGERA_DEMANDA.InsereRegistroAtendimento;
begin
  try
    with dmTriagemDeca.cdsIns_Cadastro_Relatorio do
    begin
      Close;
      Params.ParamByName ('@Cod_inscricao').Value       := dmTriagemDeca.cdsGERA_DEMANDA.FieldByName ('cod_inscricao').Value;
      Params.ParamByName ('@dta_digitacao').Value       := Date();
      Params.ParamByName ('@tipo_relatorio').Value      := 4;  //Vai ser inserido como Registro de Atendimento
      Params.ParamByName ('@cod_responsavel').Value     := Null;  //Trazer os dados de usu�rio pelo Sistema Deca
      Params.ParamByName ('@txt_relatorio').Value       := Trim(medOBSERVACOES.Text) + #13 + edtNOME.Text + ' foi selecionado na data de ' + DateToStr(Date()) + ' para concorrer �s vagas abertas no per�odo.';
      Params.ParamByName ('@dsc_entrevistador').Value   := vvNOM_USUARIO;
      Params.ParamByName ('@pe_cod_usuario_deca').Value := vvCOD_USUARIO; //Estabelece a rela��o do usu�rio com o banco de dados Usuario -> Deca
      Execute;
    end;
  except end;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_REGIAOClick(Sender: TObject);
begin
  cmbREGIAO.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_FAIXAETARIAClick(Sender: TObject);
begin
  cmbFAIXA_ETARIA.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_UNIDADEClick(Sender: TObject);
begin
  cmbUNIDADE.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_SITUACAODEMANDAClick(Sender: TObject);
begin
  cmbSITUACAO_DEMANDA.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_PERIODOESCOLARClick(Sender: TObject);
begin
  cmbPERIODO_ESCOLA.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_SITUACAOCADASTROClick(Sender: TObject);
begin
  cmbSITUACAO_CADASTRO.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnLIMPA_SELECIONADOSClick(Sender: TObject);
begin
  cmbSELECIONADOS_DEMANDA.ItemIndex := -1;
end;

procedure TfrmGERA_DEMANDA.btnPESQUISA_NOMEDEMANDAClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsGERA_DEMANDA do
    begin
      Close;
      Params.ParamByName ('@pe_cod_regiao').Value        := Null;
      Params.ParamByName ('@pe_faixaetaria1').Value      := Null;
      Params.ParamByName ('@pe_faixaetaria2').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value       := Null;
      Params.ParamByName ('@pe_flg_situacao').Value      := Null;
      Params.ParamByName ('@pe_dsc_escolaperiodo').Value := Null;
      Params.ParamByName ('@pe_cod_situacao').Value      := Null;
      Params.ParamByName ('@pe_nom_nome').Value          := '%' + Trim(edtNOME_DEMANDA.Text) + '%';
      Open;
      dbgDEMANDAS.Refresh;
    end;
  except end;
end;

procedure TfrmGERA_DEMANDA.btnPESQUISA_TODADEMANDAClick(Sender: TObject);
begin
  edtNOME_DEMANDA.Clear;
  btnAPLICAR_FILTRO.Click;
end;

end.
