unit uPesquisaUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmPesquisaUsuarios = class(TForm)
    GroupBox1: TGroupBox;
    cbCriterio: TComboBox;
    Label1: TLabel;
    edValorPesquisa: TEdit;
    btnPesquisar: TButton;
    Panel1: TPanel;
    dbgResultado: TDBGrid;
    dsPesquisaUsuario: TDataSource;
    procedure edValorPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure btnPesquisarClick(Sender: TObject);
    procedure dbgResultadoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPesquisaUsuarios: TfrmPesquisaUsuarios;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmPesquisaUsuarios.edValorPesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then btnPesquisar.Click;
end;

procedure TfrmPesquisaUsuarios.btnPesquisarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Usuario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value   := Null;

      case cbCriterio.ItemIndex of
       0: begin
            Params.ParamByName ('@pe_cod_matricula').AsString := GetValue(edValorPesquisa.Text);
            Params.ParamByName ('@pe_nom_usuario').Value      := Null;
          end;

       1: begin
            Params.ParamByName ('@pe_cod_matricula').Value    := Null;
            Params.ParamByName ('@pe_nom_usuario').AsString   := edValorPesquisa.Text + '%';
          end;

       end;
      
      Params.ParamByName ('@pe_flg_situacao').Value  := Null;
      Open;

      dbgResultado.Refresh;

    end;
  except

  end;
end;

procedure TfrmPesquisaUsuarios.dbgResultadoDblClick(Sender: TObject);
begin
  frmPesquisaUsuarios.Close;
end;

procedure TfrmPesquisaUsuarios.FormShow(Sender: TObject);
begin
  cbCriterio.ItemIndex := 1;
  edValorPesquisa.SetFocus;
end;

end.
