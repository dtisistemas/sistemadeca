unit uFichaPesquisa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmFichaPesquisa = class(TForm)
    edPesquisa: TEdit;
    cbCampoPesquisa: TComboBox;
    rgCriterio: TRadioGroup;
    btnPesquisa: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    dsSel_Cadastro_Pesquisa: TDataSource;
    dbgPesquisa: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cbCampoPesquisaChange(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LimpaTela();
    procedure edPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure rgCriterioClick(Sender: TObject);
    procedure dbgPesquisaDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure dbgPesquisaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFichaPesquisa: TfrmFichaPesquisa;

implementation

uses uDM, uPrincipal, uFichaCrianca;

{$R *.DFM}

procedure TfrmFichaPesquisa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmFichaPesquisa.FormCreate(Sender: TObject);
begin
  // posicionar o combobox - campo
  cbCampoPesquisa.ItemIndex := 2;
end;

procedure TfrmFichaPesquisa.cbCampoPesquisaChange(Sender: TObject);
begin
  LimpaTela;
  case cbCampoPesquisa.ItemIndex of
    0..1:
    begin
      rgCriterio.ItemIndex := 0;
      rgCriterio.Enabled := false;
    end;
    2..3:
    begin
      rgCriterio.ItemIndex := 1;
      rgCriterio.Enabled := true;
    end
  end;

end;

procedure TfrmFichaPesquisa.btnPesquisaClick(Sender: TObject);
var
  vCritPesq : String;

begin
  if Length(Trim(edPesquisa.Text)) > 0 then
  begin

    if rgCriterio.ItemIndex = 0 then
      vCritPesq := ''
    else
      vCritPesq := '%';

    try
      with dmDeca.cdsSel_Cadastro_Pesquisa do
      begin
        Close;
        Case cbCampoPesquisa.ItemIndex of
          0:                            // cod_matricula
          begin
            Params.ParamByName('@pe_tipo').Value := 1;
            Params.ParamByName('@pe_nom_nome').Value := NULL;
            Params.ParamByName('@pe_cod_matricula').Value := (edPesquisa.Text) + '%';
            Params.ParamByName('@pe_num_prontuario').Value := NULL;
            Params.ParamByName('@pe_nom_responsavel').Value := NULL;
            Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
          end;
          1:                            // cod_prontuario
          begin
            Params.ParamByName('@pe_tipo').Value := 1;
            Params.ParamByName('@pe_nom_nome').Value := NULL;
            Params.ParamByName('@pe_cod_matricula').Value := NULL;
            Params.ParamByName('@pe_num_prontuario').Value := StrToInt(edPesquisa.Text);
            Params.ParamByName('@pe_nom_responsavel').Value := NULL;
            Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
          end;
          2:                            // nom_nome - nome da crian�a
          begin
            Params.ParamByName('@pe_tipo').Value := 1;
            Params.ParamByName('@pe_nom_nome').Value := vCritPesq + (edPesquisa.Text) + '%';
            Params.ParamByName('@pe_cod_matricula').Value := NULL;
            Params.ParamByName('@pe_num_prontuario').Value := NULL;
            Params.ParamByName('@pe_nom_responsavel').Value := NULL;
            Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
          end;
          3:
          begin                         // nom_familiar - nome do responsavel
            Params.ParamByName('@pe_tipo').Value := 2;
            Params.ParamByName('@pe_nom_nome').Value := NULL;
            Params.ParamByName('@pe_cod_matricula').Value := NULL;
            Params.ParamByName('@pe_num_prontuario').Value := NULL;
            Params.ParamByName('@pe_nom_responsavel').Value := vCritPesq + (edPesquisa.Text) + '%';
            Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
          end
        end;
        Open;
        dbgPesquisa.Refresh;

      end;
    except end;
  end
  else
  begin
    try
      with dmDeca.cdsSel_Cadastro_Pesquisa do
      begin
        Close;
        Params.ParamByName('@pe_tipo').Value            := 1;
        Params.ParamByName('@pe_nom_nome').Value        := NULL;
        Params.ParamByName('@pe_cod_matricula').Value   := NULL;
        Params.ParamByName('@pe_num_prontuario').Value  := NULL;
        Params.ParamByName('@pe_nom_responsavel').Value := NULL;
        Params.ParamByName('@pe_cod_unidade').Value     := vvCOD_UNIDADE;
        Open;
      end;
      except end;
  end;
end;

procedure TfrmFichaPesquisa.FormShow(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Cadastro_Pesquisa do
    begin
      Close;
      Params.ParamByName('@pe_tipo').Value            := 1;
      Params.ParamByName('@pe_nom_nome').Value        := NULL;
      Params.ParamByName('@pe_cod_matricula').Value   := NULL;
      Params.ParamByName('@pe_num_prontuario').Value  := NULL;
      Params.ParamByName('@pe_nom_responsavel').Value := NULL;
      if vvCOD_UNIDADE = 40 then
        Params.ParamByName('@pe_cod_unidade').Value := 0
      else
        Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      //Params.ParamByName('@pe_cod_unidade').Value     := vvCOD_UNIDADE;
      Open
    end;
  except end;

end;

procedure TfrmFichaPesquisa.LimpaTela;
begin
  // limpa o DBGrid
  dmDeca.cdsSel_Cadastro_Pesquisa.Close;
  dbgPesquisa.Refresh;
end;

procedure TfrmFichaPesquisa.edPesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
  LimpaTela;
end;

procedure TfrmFichaPesquisa.rgCriterioClick(Sender: TObject);
begin
  LimpaTela;
end;

procedure TfrmFichaPesquisa.dbgPesquisaDblClick(Sender: TObject);
begin

  try
  if Length(dbgPesquisa.datasource.dataset.FieldByName('cod_matricula').AsString) > 0 then
  begin
    vvCOD_MATRICULA_PESQUISA := dbgPesquisa.datasource.dataset.FieldByName('cod_matricula').AsString;
    frmFichaPesquisa.Close;
  end;
  except end;

end;

procedure TfrmFichaPesquisa.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmFichaPesquisa.Close;
  end;
  
end;

procedure TfrmFichaPesquisa.dbgPesquisaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //Ativo = 1
  if (dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('ind_status').AsInteger = 1) then
  begin
    dbgPesquisa.Canvas.Font.Color:= clWhite;
    dbgPesquisa.Canvas.Brush.Color:= clGreen;
  end //Desligado
  else if (dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('ind_status').AsInteger = 2) then
  begin
    dbgPesquisa.Canvas.Font.Color:= clBlack;
    dbgPesquisa.Canvas.Brush.Color:= clWhite;
  end //Suspenso
  else if (dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('ind_status').AsInteger = 3) then
  begin
    dbgPesquisa.Canvas.Font.Color:= clWhite;
    dbgPesquisa.Canvas.Brush.Color:= clRed;
  end //Afastado
  else if (dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('ind_status').AsInteger = 4) then
  begin
    dbgPesquisa.Canvas.Font.Color:= clRed;
    dbgPesquisa.Canvas.Brush.Color:= clYellow;
  end //Em transfer�ncia
  else if (dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('ind_status').AsInteger = 5) then
  begin
    dbgPesquisa.Canvas.Font.Color:= clWhite;
    dbgPesquisa.Canvas.Brush.Color:= clBlue;
  end //Em conv�nio
  else if (dmDeca.cdsSel_Cadastro_Pesquisa.FieldByName('ind_status').AsInteger = 6) then
  begin
    dbgPesquisa.Canvas.Font.Color:= clRed;
    dbgPesquisa.Canvas.Brush.Color:= clYellow;
  end;

  dbgPesquisa.Canvas.FillRect(Rect);
  dbgPesquisa.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

end.
