unit rGraficoTotalConvenio;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, TeeProcs, TeEngine, Chart,
  DBChart, QrTee, jpeg, Series, TeeFunci;

type
  TrelGraficoTotalConvenio = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRDBChart1: TQRDBChart;
    QRChart1: TQRChart;
    Series1: TBarSeries;
  private

  public

  end;

var
  relGraficoTotalConvenio: TrelGraficoTotalConvenio;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

end.
