unit uExibeSemEscola;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, Db, Buttons, StdCtrls, Mask;

type
  TfrmExibeSemEscola = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    lbTitulo: TLabel;
    Label1: TLabel;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    mskAno: TMaskEdit;
    btnPesquisar: TSpeedButton;
    btnImprimir: TSpeedButton;
    btnSair: TSpeedButton;
    meRelacaoSemEscola: TMemo;
    dbgExibeSemEscola: TDBGrid;
    dsSel_AcompEscolar: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure mskAnoExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mskAnoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmExibeSemEscola: TfrmExibeSemEscola;
  //mmFechar : Boolean;

implementation

uses uDM, uPrincipal, rSemEscolaUnidade;

{$R *.DFM}

procedure TfrmExibeSemEscola.FormShow(Sender: TObject);
var hwndHandle  : THANDLE;
    hMenuHandle : HMENU;
begin

  frmExibeSemEscola.Height := 142;
  frmExibeSemEscola.Top    := 400;

  //mmFechar := False;

  //Desabilita bot�o fechar
  hwndHandle := FindWindow(nil, '[Sistema Deca] - Rela��o de Alunos da Unidade que est�o Sem Escola');
  if (hwndHandle <> 0) then
  begin
    hMenuHandle := GetSystemMenu(hwndHandle, FALSE);
    if (hMenuHandle <> 0) then
      DeleteMenu(hMenuHandle, SC_CLOSE, MF_BYCOMMAND);
  end;

  //Desabilitar o bot�o Sair e habilitar apenas depois de imprimir o relat�rio
  btnImprimir.Enabled  := False;
  btnPesquisar.Enabled := False;
  btnSair.Enabled      := True;

end;

procedure TfrmExibeSemEscola.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm (TrelSemEscolaUnidade, relSemEscolaUnidade);
  relSemEscolaUnidade.QRMemo1.Lines.Add (meRelacaoSemEscola.Text);
  relSemEscolaUnidade.QRLabel3.Caption := Label1.Caption;
  relSemEscolaUnidade.Preview;
  relSemEscolaUnidade.Free;
  //mmFechar := True;
  btnSair.Enabled := True;
end;

procedure TfrmExibeSemEscola.btnPesquisarClick(Sender: TObject);
var numTotal    : Integer;
begin

  try

    with dmDeca.cdsSel_AcompEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
      Params.ParamByName ('@pe_cod_matricula').Value         := Null;
      Params.ParamByName ('@pe_num_ano_letivo').Value        := mskAno.Text;
      Params.ParamByName ('@pe_num_bimestre').Value          := RadioGroup1.ItemIndex+1;

      if (vvIND_PERFIL in [2,3,6]) then
        Params.ParamByName ('@pe_cod_unidade').Value         := vvCOD_UNIDADE
      else if (vvIND_PERFIL in [1,7,14,15,18,19,20,23,24,25]) then
        Params.ParamByName ('@pe_cod_unidade').Value         := Null;

      Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
      Open;

      if (dmDeca.cdsSel_AcompEscolar.RecordCount > 0) then
      begin

        numTotal := 0;

        dbgExibeSemEscola.Refresh;


        //Limpa o Memo...
        meRelacaoSemEscola.Lines.Clear;

        while not dmDeca.cdsSel_AcompEscolar.eof do
        begin
          if (dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value = 'SEM ESCOLA') then
          begin
            meRelacaoSemEscola.Lines.Add (dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_matricula').Value + ' - ' +
                                          dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_nome').Value + ' - ' +
                                          dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_unidade').Value + ' - ' +
                                          dmDeca.cdsSel_AcompEscolar.FieldByName ('vSituacaoCadastro').Value);
            numTotal := numTotal + 1;
          end;
          dmDeca.cdsSel_AcompEscolar.Next;
        end;

        Label1.Caption := 'Total de alunos SEM ESCOLA : ' + IntToStr(numTotal);

        
        btnImprimir.Enabled      := True;
        frmExibeSemEscola.Height := 518;
        frmExibeSemEscola.Top    := 200;
      end
      else
      begin
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'N�o existem alunos sem escola no 1� Bimestre do ano informado.',
                                '[Sistema Deca] - Rela��o dos Sem Escola da sua unidade',
                                MB_OK + MB_ICONEXCLAMATION);
        //mmFechar := True;
        btnSair.Enabled := True;
      end;

    end;
  except end;

end;

procedure TfrmExibeSemEscola.btnSairClick(Sender: TObject);
begin
  frmExibeSemEscola.Close;
end;

procedure TfrmExibeSemEscola.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if Key = #27 then
  begin
    Key := #0;
    frmExibeSemEscola.Close;
  end;
end;

procedure TfrmExibeSemEscola.mskAnoExit(Sender: TObject);
begin
  btnPesquisar.Enabled := True;
end;

procedure TfrmExibeSemEscola.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //if mmFechar = False then
  //  Action := caNone
  //else
  //  Action := caFree;
end;

procedure TfrmExibeSemEscola.mskAnoChange(Sender: TObject);
begin
  if (Length(mskAno.Text) = 4) then btnPesquisar.Enabled := True;
end;

end.
