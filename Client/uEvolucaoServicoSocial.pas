unit uEvolucaoServicoSocial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls;

type
  TfrmEvolucaoServicoSocial = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    txtData: TMaskEdit;
    Label6: TLabel;
    txtNumParticipantes: TEdit;
    Label7: TLabel;
    txtNumHoras: TEdit;
    txtDescricao: TMemo;
    Label2: TLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure txtNumParticipantesExit(Sender: TObject);
    procedure txtNumHorasExit(Sender: TObject);
    procedure cbTipoAtendimentoExit(Sender: TObject);
    procedure cbClassificacaoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEvolucaoServicoSocial: TfrmEvolucaoServicoSocial;
  vListaClass1, vListaClass2 : TStringList;

implementation

uses uDM, uPrincipal, uUtil, uFichaPesquisa;

{$R *.DFM}

procedure TfrmEvolucaoServicoSocial.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmEvolucaoServicoSocial.Close;
  end;
end;

procedure TfrmEvolucaoServicoSocial.txtMatriculaExit(Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end
else
  begin
    txtMatricula.SetFocus;
    ShowMessage('Aten��o!'+ #13+#10+#13+
                'O campo MATR�CULA n�o pode estar em branco!');
  end;
end;

procedure TfrmEvolucaoServicoSocial.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmEvolucaoServicoSocial.btnSairClick(Sender: TObject);
begin
  mmINTEGRACAO := False;
  frmEvolucaoServicoSocial.Close;
end;

procedure TfrmEvolucaoServicoSocial.AtualizaCamposBotoes(Modo: String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    //txtNumParticipantes.Clear;
    //txtNumHoras.Clear;
    //cbTipoAtendimento.ItemIndex := -1;
    //cbClassificacao.ITemIndex := -1;
    txtDescricao.Clear;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtData.Enabled := False;
    //txtNumParticipantes.Enabled := False;
    //txtNumHoras.Enabled := False;
    //cbTipoAtendimento.Enabled := False;
    //cbClassificacao.Enabled := False;
    txtDescricao.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    //btnImprimir.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtData.Text := DateToStr(Date());
    //txtNumParticipantes.Clear;
    //txtNumHoras.Clear;
    //cbTipoAtendimento.ItemIndex := -1;
    //cbClassificacao.ItemIndex := -1;
    txtDescricao.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtDescricao.Enabled := True;
    //txtNumHoras.Enabled := True;
    //txtNumParticipantes.Enabled := True;
    //cbTipoAtendimento.Enabled := True;
    //cbClassificacao.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    //btnImprimir.Enabled := False;

    txtMatricula.SetFocus;
  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    //btnImprimir.Enabled := True;

    //btnImprimir.SetFocus;
  end
end;

procedure TfrmEvolucaoServicoSocial.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEvolucaoServicoSocial.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
  if (mmINTEGRACAO = False) then
    AtualizaCamposBotoes('Padrao')
  else if (mmINTEGRACAO = True) then
  begin
    AtualizaCamposBotoes('Novo');
    txtMatricula.Text := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
    lbNome.Caption    := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
    txtData.Text := DateToStr(Date());
    txtDescricao.SetFocus;
  end;

  {vListaClass1 := TStringLIst.Create();
  vListaClass2 := TStringLIst.Create();

  try
    with dmDeca.cdsSel_Class_Evolucao do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_class_evolucao').Value := Null;
      Params.ParamByName('@pe_dsc_class_evolucao').Value := Null;
      Open;

      while not eof do
      begin
        vListaClass1.Add(IntToStr(FieldByName('cod_id_class_evolucao').Value));
        vListaClass2.Add(FieldByName('dsc_class_evolucao').Value);
        cbClassificacaoEvolucao.Items.Add(FieldByName('dsc_class_evolucao').Value);
        Next;
      end;

    end;
  except end;
  }
end;

procedure TfrmEvolucaoServicoSocial.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmEvolucaoServicoSocial.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmEvolucaoServicoSocial.btnSalvarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try
      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      //if StatusCadastro(txtMatricula.Text) = 'Ativo' then

      if (StatusCadastro(txtMatricula.Text) = 'Ativo') OR (StatusCadastro(txtMatricula.Text) = 'Afastado') OR
          (StatusCadastro(txtMatricula.Text) = 'Suspenso') then

      begin
        with dmDeca.cdsInc_Cadastro_Evolucao_SC do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := txtMatricula.Text;
          Params.ParamByName('@pe_dat_evolucao').AsDateTime := StrToDate(txtData.Text);
          Params.ParamByName('@pe_dsc_comentario').Value := Trim(txtDescricao.Text);
          //Params.ParamByName('@pe_ind_tipo').AsString := cbTipoAtendimento.Items[cbTipoAtendimento.ItemINdex];
          //Params.ParamByName('@pe_ind_classificacao').AsString := cbClassificacao.Items[cbClassificacao.ItemIndex];
          //Params.ParamByName('@pe_qtd_participantes').AsInteger := StrToInt(Trim(txtNumParticipantes.Text));
          //Params.ParamByName('@pe_num_horas').AsFloat := StrToFloat(Trim(txtNumHoras.Text));
          //Params.ParamByName('@pe_ind_classificacao').Value := StrToInt(vListaClass1.Strings[cbClassificacaoEvolucao.ItemIndex]); 
          Params.ParamByName('@pe_ind_tipo').Value := Null;
          Params.ParamByName('@pe_ind_classificacao').Value := Null;
          Params.ParamByName('@pe_qtd_participantes').Value := Null;
          Params.ParamByName('@pe_num_horas').Value := Null;
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;
        end;
        AtualizaCamposBotoes('Salvar');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                    'Esta(e) crian�a/adolescente encontra-se ' +
                    StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except
      arqLog := 'EV_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                        Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);

      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, txtData.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Evolu��o do Servi�o Social-Inclus�o');//Descricao do Tipo de Historico
      Writeln (log_file, ' ');
      Writeln (log_file, Trim(txtDescricao.Text)); //Ocorrencia do Historico
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados da Evolu��o!'+#13+#10+#13+#10+
                             'Os dados n�o foram gravados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Evolu��o Serv. Social',
                             MB_OK + MB_ICONERROR);
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmEvolucaoServicoSocial.btnImprimirClick(Sender: TObject);
begin
  ShowMessage ('Ainda n�o dispon�vel ...');
end;

procedure TfrmEvolucaoServicoSocial.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmEvolucaoServicoSocial.txtDataExit(Sender: TObject);
begin
  try
  begin
    StrToDate(txtData.Text);
  end
  except
    ShowMessage ('Data inv�lida...');
    txtData.SetFocus;
  end;
end;

procedure TfrmEvolucaoServicoSocial.txtNumParticipantesExit(
  Sender: TObject);
begin
  if Length(txtNumParticipantes.Text) = 0 then
  begin
    txtNumParticipantes.SetFocus;
    txtNumParticipantes.Clear;
  end;
end;

procedure TfrmEvolucaoServicoSocial.txtNumHorasExit(Sender: TObject);
begin
  if Length(txtNumHoras.Text) = 0 then
  begin
    txtNumHoras.SetFocus;
    txtNumHoras.Clear;
  end;
end;

procedure TfrmEvolucaoServicoSocial.cbTipoAtendimentoExit(Sender: TObject);
begin
  //if cbTipoAtendimento.ItemIndex = -1 then
  //begin
  //  ShowMessage('Favor selecionar "Tipo de Atendimento"');
  //  cbTipoAtendimento.SetFocus;
  //end;
end;

procedure TfrmEvolucaoServicoSocial.cbClassificacaoExit(Sender: TObject);
begin
  //if cbClassificacao.ItemIndex = -1 then
  //begin
  //  ShowMessage('Favor selecionar "Classifica��o do Atendimento"');
  //  cbClassificacao.SetFocus;
  //end;
end;

end.
