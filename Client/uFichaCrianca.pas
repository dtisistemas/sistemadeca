unit uFichaCrianca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls, ToolWin, ComCtrls, Grids, DBGrids, MMSystem,
  ExtDlgs, Menus, DBCtrls, Db, uUtil, jpeg, ShellApi, ImgList;

type
  TfrmFichaCrianca = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox2C: TGroupBox;
    txtRgFam: TEdit;
    GroupBox2I: TGroupBox;
    GroupBox2J: TGroupBox;
    GroupBox2D: TGroupBox;
    txtEnderecoResFam: TEdit;
    GroupBox2E: TGroupBox;
    txtComplementoFam: TEdit;
    GroupBox2F: TGroupBox;
    txtBairroFam: TEdit;
    GroupBox2G: TGroupBox;
    GroupBox2L: TGroupBox;
    txtOcupacaoFam: TEdit;
    GroupBox2P: TGroupBox;
    txtLocalFam: TEdit;
    GroupBox2N: TGroupBox;
    txtContatoFam: TEdit;
    GroupBox2M: TGroupBox;
    GroupBox2O: TGroupBox;
    GroupBox2H: TGroupBox;
    GroupBox2R: TGroupBox;
    TabSheet5: TTabSheet;
    GroupBox5E: TGroupBox;
    dbgAtendimentos: TDBGrid;
    GroupBox5A: TGroupBox;
    GroupBox5B: TGroupBox;
    GroupBox5C: TGroupBox;
    txtLocalAt: TEdit;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton30: TSpeedButton;
    SpeedButton31: TSpeedButton;
    SpeedButton32: TSpeedButton;
    SpeedButton33: TSpeedButton;
    SpeedButton34: TSpeedButton;
    GroupBox2K: TGroupBox;
    txtEscolaridade: TEdit;
    GroupBox2Q: TGroupBox;
    txtProjetoFundhas: TEdit;
    txtFoneFam: TMaskEdit;
    txtFoneContatoFam: TMaskEdit;
    txtCepFam: TMaskEdit;
    txtDataInicioAt: TMaskEdit;
    dbgFamiliares: TDBGrid;
    GroupBox2B: TGroupBox;
    txtNomeFam: TEdit;
    GroupBox2A: TGroupBox;
    OpenPictureDialog1: TOpenPictureDialog;
    cbTipoDocumento: TComboBox;
    cbVinculo: TComboBox;
    Panel2: TPanel;
    Label4: TLabel;
    TabSheet7: TTabSheet;
    TabSheet6: TTabSheet;
    Panel5: TPanel;
    Label9: TLabel;
    GroupBox5D: TGroupBox;
    txtDescricaoAtendimento: TMemo;
    txtNascFam: TMaskEdit;
    cbSexoFam: TComboBox;
    dsSel_Cadastro_Move: TDataSource;
    dsSel_Escola: TDataSource;
    btnNovo4: TBitBtn;
    btnAlterar4: TBitBtn;
    btnSalvar4: TBitBtn;
    btnCancelar4: TBitBtn;
    btnSair4: TBitBtn;
    btnNovo5: TBitBtn;
    btnAlterar5: TBitBtn;
    btnSalvar5: TBitBtn;
    btnCancelar5: TBitBtn;
    btnSair5: TBitBtn;
    dsSel_Cadastro_Programa_Social: TDataSource;
    btnNovo2: TBitBtn;
    btnAlterar2: TBitBtn;
    btnSalvar2: TBitBtn;
    btnCancelar2: TBitBtn;
    btnSair2: TBitBtn;
    btnExcluir2: TBitBtn;
    chkResponsavel: TCheckBox;
    dsSel_Cadastro_Familia: TDataSource;
    txtRendaFam: TMaskEdit;
    btnCopiarFamiliares: TBitBtn;
    cbAtendimentos: TComboBox;
    dsSel_Cadastro_Atendimentos_Rede: TDataSource;
    dsSel_Historico: TDataSource;
    Group6A: TGroupBox;
    Panel6: TPanel;
    lblNome: TLabel;
    GroupBoxA: TGroupBox;
    GroupBoxB: TGroupBox;
    GroupBoxC: TGroupBox;
    GroupBoxD: TGroupBox;
    GroupBoxE: TGroupBox;
    GroupBoxF: TGroupBox;
    GroupBoxG: TGroupBox;
    GroupBoxH: TGroupBox;
    GroupBoxI: TGroupBox;
    GroupBoxJ: TGroupBox;
    GroupBoxK: TGroupBox;
    GroupBoxL: TGroupBox;
    GroupBoxM: TGroupBox;
    cbSexoCrianca: TComboBox;
    lbStatus: TLabel;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnCopiarFicha: TBitBtn;
    txtMatricula: TMaskEdit;
    txtProntuario: TMaskEdit;
    txtNome: TEdit;
    txtAdmissao: TMaskEdit;
    txtEndereco: TEdit;
    txtComplemento: TEdit;
    txtBairro: TEdit;
    txtNascimento: TMaskEdit;
    txtSaude: TMemo;
    TabSheet1: TTabSheet;
    lbIdade: TLabel;
    txtDtCadastro: TMaskEdit;
    btnPrimeiro: TBitBtn;
    btnAnterior: TBitBtn;
    btnProximo: TBitBtn;
    btnUltimo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnPesquisar: TBitBtn;
    Panel1: TPanel;
    GroupBoxZ: TGroupBox;
    lbIdadeResp: TLabel;
    meDescricaoOcorrencia: TMemo;
    Label17: TLabel;
    Panel7: TPanel;
    Label2: TLabel;
    GroupBox4: TGroupBox;
    dbgAcompanhamento: TDBGrid;
    dsSel_Acompanhamento: TDataSource;
    btnVerRegistroAtendimento: TSpeedButton;
    txtSias: TEdit;
    PageControl2: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    Panel3: TPanel;
    Label6: TLabel;
    GroupBox3A: TGroupBox;
    cbEscola: TComboBox;
    GroupBox3B: TGroupBox;
    cbSerie: TComboBox;
    GroupBox3C: TGroupBox;
    txtTurma: TEdit;
    GroupBox3D: TGroupBox;
    cbPeriodoEscola: TComboBox;
    GroupBox3E: TGroupBox;
    txtObsEscola: TMemo;
    GroupBox3F: TGroupBox;
    Label11: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label30: TLabel;
    txtEnderecoEscola: TEdit;
    txtBairroEscola: TEdit;
    txtCepEscola: TEdit;
    txtFone1Escola: TEdit;
    txtDiretoriaEscola: TEdit;
    txtEmail: TEdit;
    rbMunicipal: TRadioButton;
    rbEstadual: TRadioButton;
    rbParticular: TRadioButton;
    btnAlterar3: TBitBtn;
    btnSalvar3: TBitBtn;
    btnCancelar3: TBitBtn;
    btnSair3: TBitBtn;
    Panel8: TPanel;
    btnNovoAcompanhamento: TBitBtn;
    btnAlterarAcompanhamento: TBitBtn;
    btnGravarAcompanhamento: TBitBtn;
    btnCancelarAcompanhamento: TBitBtn;
    btnSairAcompanhamento: TBitBtn;
    Label20: TLabel;
    Label21: TLabel;
    edAno: TEdit;
    cbBimestre: TComboBox;
    Label22: TLabel;
    edDiasLetivos: TEdit;
    Label23: TLabel;
    edNumFaltas: TEdit;
    Label24: TLabel;
    edObservacoes: TMemo;
    rgAproveitamento: TRadioGroup;
    dsSel_HistoricoEscola: TDataSource;
    dbgHistorico: TDBGrid;
    Panel9: TPanel;
    Label25: TLabel;
    Panel10: TPanel;
    Label26: TLabel;
    btnInicializarBimestre: TSpeedButton;
    Label27: TLabel;
    Panel11: TPanel;
    lbRendaFamiliar: TLabel;
    PageControl3: TPageControl;
    TabSheet11: TTabSheet;
    Panel4: TPanel;
    Label7: TLabel;
    GroupBox4A: TGroupBox;
    cbProgramaSocial: TComboBox;
    dbgAcompanhamentos: TDBGrid;
    dbgFamiliaresProgSociais: TDBGrid;
    GroupBox5: TGroupBox;
    btnDesligamento: TBitBtn;
    GroupBox7: TGroupBox;
    meDataEntrada: TMaskEdit;
    rgSituacao: TRadioGroup;
    chkIncluirCrianca: TCheckBox;
    btnAtivar: TBitBtn;
    dbgProgramas: TDBGrid;
    rgMoraCasa: TRadioGroup;
    GroupBox3G: TGroupBox;
    edAnoLetivo: TEdit;
    PopupMenu1: TPopupMenu;
    ExcluirRegistro1: TMenuItem;
    GroupBox3H: TGroupBox;
    edNumRGEscolar: TEdit;
    TabSheet12: TTabSheet;
    Panel12: TPanel;
    Label8: TLabel;
    Panel13: TPanel;
    GroupBoxE2: TGroupBox;
    edAnoLetivoE: TEdit;
    GroupBoxE3: TGroupBox;
    cbBimestreE: TComboBox;
    GroupBoxE4: TGroupBox;
    cbEscolaE: TComboBox;
    GroupBoxE5: TGroupBox;
    cbSerieE: TComboBox;
    GroupBoxE1: TGroupBox;
    edNumRGEscolarE: TEdit;
    GroupBoxE10: TGroupBox;
    cbPeriodoEscolaE: TComboBox;
    GroupBoxE6: TGroupBox;
    GroupBoxE8: TGroupBox;
    cbAproveitamentoE: TComboBox;
    GroupBoxE9: TGroupBox;
    cbSituacaoE: TComboBox;
    GroupBoxE12: TGroupBox;
    edObservacaoE: TMemo;
    btnPrimeiraFicha: TBitBtn;
    btnFichaAnterior: TBitBtn;
    btnProximaFicha: TBitBtn;
    btnUltimaFicha: TBitBtn;
    btnNovoLancamentoE: TBitBtn;
    btnAlteraLancamentoE: TBitBtn;
    btnGravaLancamentoE: TBitBtn;
    btnCancelaOperacaoE: TBitBtn;
    btnVerBoletimE: TBitBtn;
    btnLocalizarFicha: TBitBtn;
    btnSairE: TBitBtn;
    edNumRGEscolarC: TEdit;
    dsAcompEscolar: TDataSource;
    Panel14: TPanel;
    Label28: TLabel;
    Fotografia2: TImage;
    GroupBoxE13: TGroupBox;
    edIdadeAtual: TEdit;
    Label29: TLabel;
    GroupBox6: TGroupBox;
    lbMsgDefasagemEscolar: TLabel;
    GroupBox3: TGroupBox;
    btnRemoverEmpresa: TSpeedButton;
    cbEmpresa: TComboBox;
    GroupBoxN: TGroupBox;
    cbPeriodo: TComboBox;
    GroupBox1: TGroupBox;
    btnRemoverTurma: TSpeedButton;
    cbTurma: TComboBox;
    GroupBox8: TGroupBox;
    lbUnidade: TLabel;
    rgModalidade: TRadioGroup;
    GroupBoxO: TGroupBox;
    txtPeso: TEdit;
    GroupBoxP: TGroupBox;
    lbClassifica: TLabel;
    txtAltura: TEdit;
    GroupBoxR: TGroupBox;
    txtCalcado: TMaskEdit;
    GroupBoxS: TGroupBox;
    txtCalca: TEdit;
    GroupBoxT: TGroupBox;
    txtCamisa: TEdit;
    GroupBoxU2: TGroupBox;
    txtPontuacao: TMaskEdit;
    GroupBox9: TGroupBox;
    edSecao: TEdit;
    GroupBox10: TGroupBox;
    edAssistenteSocial: TEdit;
    GroupBox11: TGroupBox;
    Fotografia: TImage;
    GroupBox12: TGroupBox;
    GroupBoxX: TGroupBox;
    cbTipoSangue: TComboBox;
    GroupBoxQ: TGroupBox;
    txtEtnia: TEdit;
    imgDefasagemAprendizagemSIM: TImage;
    Label1: TLabel;
    Label15: TLabel;
    imgDefasagemEscNAO: TImage;
    imgDefasagemEscolarSIM: TImage;
    imgDefasagemAprendizagemNAO: TImage;
    imgDefasagemEscolarNENHUMA: TImage;
    imgDefasagemEscolarNAO: TImage;
    imgDefasagemAprendizagemNENHUMA: TImage;
    imgDefasagemEscSIM: TImage;
    imgDefasagemEscNENHUMA: TImage;
    popExcluirAcompanhamento: TPopupMenu;
    Excluirregistro2: TMenuItem;
    GroupBox13: TGroupBox;
    Image2: TImage;
    Label18: TLabel;
    cbFrequenciaE: TComboBox;
    ImageList1: TImageList;
    imgLegendaFreqAprov: TImage;
    GroupBoxE14: TGroupBox;
    edTurmaEscola: TEdit;
    GroupBox14: TGroupBox;
    GroupBox15: TGroupBox;
    lbAnoLetivo: TLabel;
    GroupBox16: TGroupBox;
    lbEscola: TLabel;
    GroupBox17: TGroupBox;
    lbSerie: TLabel;
    GroupBox18: TGroupBox;
    lbSituacao: TLabel;
    GroupBox19: TGroupBox;
    lbDataInclusao: TLabel;
    GroupBox20: TGroupBox;
    lbDataAlteracao: TLabel;
    GroupBox21: TGroupBox;
    lbUsuario: TLabel;
    lbTipoEscola: TLabel;
    GroupBox22: TGroupBox;
    Label32: TLabel;
    btnAlteraEndereco: TSpeedButton;
    btnSelecionaFoto: TSpeedButton;
    GroupBoxV: TGroupBox;
    chkProjetoVida: TCheckBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label14: TLabel;
    txtCertidaoNascimento: TEdit;
    txtCpf: TEdit;
    txtRg: TEdit;
    GroupBoxU: TGroupBox;
    txtCotas: TMaskEdit;
    GroupBox23: TGroupBox;
    txtNomeMae: TEdit;
    txtNomePai: TEdit;
    rgBolsaFamilia: TRadioGroup;
    txtNIS: TEdit;
    rgAtivExterna: TRadioGroup;
    txtATIVIDADE_EXTERNA: TEdit;
    Image1: TImage;
    Label31: TLabel;
    TabSheet13: TTabSheet;
    Panel16: TPanel;
    Label33: TLabel;
    GroupBox24: TGroupBox;
    GroupBox25: TGroupBox;
    GroupBox26: TGroupBox;
    btnAtualizaDadosEducacenso: TSpeedButton;
    mskCEI: TMaskEdit;
    meObservacoes: TMemo;
    dbgDadosAcompEscolar: TDBGrid;
    lbUnidadeE: TLabel;
    dbgHistoricoEscolas: TDBGrid;
    mskCPF_Familiar: TEdit;
    GroupBox27: TGroupBox;
    GroupBox28: TGroupBox;
    Label34: TLabel;
    cbTurmaCIE: TComboBox;
    Label35: TLabel;
    Label36: TLabel;
    edUnidadeEscolar: TEdit;
    edSerieEscolar: TEdit;
    Label37: TLabel;
    edPeriodo: TEdit;
    btnMigrarDadosEducacenso: TSpeedButton;
    Label38: TLabel;
    mskDataCadastro: TMaskEdit;
    mskNascimento: TMaskEdit;
    TabSheet14: TTabSheet;
    Panel17: TPanel;
    Label39: TLabel;
    btnAlteraDadosDocumentacao: TBitBtn;
    btnGravaDadosDocumentacao: TBitBtn;
    btnCancelarAlteracaoDocumentos: TBitBtn;
    btnSairDocumentacao: TBitBtn;
    btnVisualizarEmissaoContrato: TBitBtn;
    PageControl4: TPageControl;
    TabSheet15: TTabSheet;
    TabSheet16: TTabSheet;
    GroupBox30: TGroupBox;
    edNumRG: TEdit;
    GroupBox31: TGroupBox;
    cbOrgaoEmissorRG: TComboBox;
    GroupBox32: TGroupBox;
    GroupBox33: TGroupBox;
    cbRGUf: TComboBox;
    GroupBox29: TGroupBox;
    GroupBox34: TGroupBox;
    edDadosCertidaoNascimento: TEdit;
    GroupBox35: TGroupBox;
    edNumTituloEleitor: TEdit;
    GroupBox36: TGroupBox;
    edZonaEleitoral: TEdit;
    GroupBox37: TGroupBox;
    edSecaoEleitoral: TEdit;
    GroupBox38: TGroupBox;
    GroupBox39: TGroupBox;
    GroupBox40: TGroupBox;
    GroupBox41: TGroupBox;
    cbCTPSUf: TComboBox;
    GroupBox42: TGroupBox;
    edNumPIS: TEdit;
    GroupBox43: TGroupBox;
    GroupBox44: TGroupBox;
    edPISBanco: TEdit;
    GroupBox45: TGroupBox;
    edReservistaNumero: TEdit;
    GroupBox46: TGroupBox;
    edReservistaCSM: TEdit;
    GroupBox47: TGroupBox;
    edNumCartaoSUS: TEdit;
    GroupBox48: TGroupBox;
    edNumCartaoSIAS: TEdit;
    GroupBox49: TGroupBox;
    GroupBox50: TGroupBox;
    cbUFResidencia: TComboBox;
    edMunicipioResidencia: TEdit;
    GroupBox51: TGroupBox;
    edLocalNascimento: TEdit;
    GroupBox52: TGroupBox;
    mskDataNascimento: TMaskEdit;
    GroupBox55: TGroupBox;
    txtCorOlhos: TEdit;
    GroupBox56: TGroupBox;
    txtCorCabelos: TEdit;
    GroupBox53: TGroupBox;
    cbFumante: TComboBox;
    GroupBox54: TGroupBox;
    cbPortadorNecessidades: TComboBox;
    edNecessidadesEspeciais: TEdit;
    GroupBox59: TGroupBox;
    GroupBox60: TGroupBox;
    cbGrauInstrucao: TComboBox;
    GroupBox61: TGroupBox;
    edEmail: TEdit;
    GroupBox62: TGroupBox;
    cbEstadoCivil: TComboBox;
    GroupBox63: TGroupBox;
    edNacionalidade: TEdit;
    GroupBox67: TGroupBox;
    edNomeConjuge: TEdit;
    GroupBox68: TGroupBox;
    cbGuardaRegular: TComboBox;
    GroupBox65: TGroupBox;
    edNomePai: TEdit;
    edNomeMae: TEdit;
    edNumCTPS: TMaskEdit;
    edCTPSSerie: TMaskEdit;
    mskNumCpf: TEdit;
    mskRGDataEmissao: TEdit;
    mskCTPSDataEmissao: TEdit;
    mskPISDataEmissao: TEdit;
    mskTelefoneCelular: TEdit;
    txtTelefone: TEdit;
    txtCep: TEdit;
    procedure btnNovoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPrimeiroClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnUltimoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSairFamiliaClick(Sender: TObject);
    procedure btnFecharPvClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnSairClick(Sender: TObject);
    procedure LimpaEdits;
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtNomeExit(Sender: TObject);
    procedure txtAdmissaoExit(Sender: TObject);
    procedure txtEnderecoExit(Sender: TObject);
    procedure txtCepExit(Sender: TObject);
    procedure txtNascimentoExit(Sender: TObject);
    procedure cbSexoCriancaExit(Sender: TObject);
    procedure cbEscolaExit(Sender: TObject);
    procedure cbSerieExit(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure AtualizaCamposBotoes;
    procedure AtualizaCamposBotoes3;
    procedure Button1Click(Sender: TObject);
    function ValidaDados: boolean;
    function ValidaMatricula: boolean;
    procedure btnAlterar3Click(Sender: TObject);
    procedure btnSalvar3Click(Sender: TObject);
    procedure btnCancelar3Click(Sender: TObject);
    procedure FillFormEscolaridade;
    procedure AtualizaCamposBotoes5;
    procedure AtualizaCamposBotoes4;
    procedure AtualizaCamposBotoes2;
    procedure btnNovo4Click(Sender: TObject);
    procedure btnAlterar4Click(Sender: TObject);
    procedure btnSalvar4Click(Sender: TObject);
    procedure btnCancelar4Click(Sender: TObject);
    procedure FillForm(const AMove:String; const AMatricula:string);
    Function CalculaIdade( Nascimento : TDateTime ) : String ;
    Function CalculaIdadeFamilia( NascFam : TDateTime ) : String;
    procedure FillFormProgramasSociais;
    procedure FillFormFamiliares;
    procedure FillFormAtendimentos;
    procedure dbgFamiliaresCellClick(Column: TColumn);
    procedure dbgFamiliaresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgFamiliaresKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnNovo2Click(Sender: TObject);
    procedure btnAlterar2Click(Sender: TObject);
    procedure btnCancelar2Click(Sender: TObject);
    procedure btnExcluir2Click(Sender: TObject);
    procedure dbgProgramasCellClick(Column: TColumn);
    procedure dbgProgramasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgProgramasKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AtualizaRendaFamiliar;
    procedure btnSalvar2Click(Sender: TObject);
    procedure txtNascFamExit(Sender: TObject);
    procedure txtRendaFamExit(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure txtNomeFamExit(Sender: TObject);
    procedure cbSexoFamExit(Sender: TObject);
    procedure btnCopiarFichaClick(Sender: TObject);
    procedure txtPesoExit(Sender: TObject);
    procedure txtAlturaExit(Sender: TObject);
    procedure btnCopiarFamiliaresClick(Sender: TObject);
    procedure btnNovo5Click(Sender: TObject);
    procedure btnCancelar5Click(Sender: TObject);
    procedure btnAlterar5Click(Sender: TObject);
    procedure btnSalvar5Click(Sender: TObject);
    procedure dbgAtendimentosCellClick(Column: TColumn);
    procedure dbgAtendimentosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgAtendimentosKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtDataInicioAtExit(Sender: TObject);
    procedure txtNascFamChange(Sender: TObject);
    procedure dbgHistoricoCellClick(Column: TColumn);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSelecionaFotoClick(Sender: TObject);
    procedure btnRemoverTurmaClick(Sender: TObject);
    procedure btnRemoverEmpresaClick(Sender: TObject);
    procedure cbEmpresaClick(Sender: TObject);
    Function Bissexto(AYear: Integer): Boolean;
    Function DiasDoMes(Ayear, AMonth: Integer):Integer;
    Function Idade2(DataNasc: TDate):String;
    Function Dias(Data: TDate): String;
    Function Idade(Nasc: TDate):String;
    procedure btnEnviarEmailClick(Sender: TObject);
    procedure btnVerRegistroAtendimentoClick(Sender: TObject);
    procedure dbgHistoricoEscolas2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgAcompanhamentosDblClick(Sender: TObject);
    procedure AtualizaTelaAcompEscolar(Modo:String);
    procedure btnNovoAcompanhamentoClick(Sender: TObject);
    procedure btnCancelarAcompanhamentoClick(Sender: TObject);
    procedure btnGravarAcompanhamentoClick(Sender: TObject);
    procedure btnAlterarAcompanhamentoClick(Sender: TObject);
    procedure btnInicializarBimestreClick(Sender: TObject);
    procedure btnDesligamentoClick(Sender: TObject);
    procedure dbgProgramasDblClick(Sender: TObject);
    procedure txtEnderecoEnter(Sender: TObject);
    procedure ExcluirRegistro1Click(Sender: TObject);
    procedure cbEscolaEClick(Sender: TObject);
    procedure cbSerieEClick(Sender: TObject);
    procedure btnPrimeiraFichaClick(Sender: TObject);
    procedure btnFichaAnteriorClick(Sender: TObject);
    procedure btnProximaFichaClick(Sender: TObject);
    procedure btnUltimaFichaClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnLocalizarFichaClick(Sender: TObject);
    procedure btnNovoLancamentoEClick(Sender: TObject);
    procedure btnAlteraLancamentoEClick(Sender: TObject);
    procedure btnCancelaOperacaoEClick(Sender: TObject);
    procedure btnSairEClick(Sender: TObject);
    procedure dbgDadosAcompEscolar2DblClick(Sender: TObject);
    procedure btnGravaLancamentoEClick(Sender: TObject);
    procedure edAnoLetivoEEnter(Sender: TObject);
    procedure edAnoLetivoEExit(Sender: TObject);
    procedure cbEscolaEEnter(Sender: TObject);
    procedure Excluirregistro2Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure dbgDadosAcompEscolar2DrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure imgLegendaFreqAprovClick(Sender: TObject);
    procedure dbgHistoricoEscolas2CellClick(Column: TColumn);
    procedure TabSheet10Show(Sender: TObject);
    procedure Label32Click(Sender: TObject);
    procedure cbSerieEEnter(Sender: TObject);
    procedure btnAlteraEnderecoClick(Sender: TObject);
    procedure cbSituacaoEDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cbSituacaoEChange(Sender: TObject);
    procedure DesabilitarComboAcEsc;
    procedure dbgDadosAcompEscolar2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgDadosAcompEscolar2KeyPress(Sender: TObject; var Key: Char);
    procedure cbPeriodoEscolaEClick(Sender: TObject);
    procedure cbPeriodoDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cbPeriodoChange(Sender: TObject);
    procedure dbgDadosAcompEscolar2CellClick(Column: TColumn);
    procedure btnAtualizaDadosEducacensoClick(Sender: TObject);
    procedure AtualizaPeriodoCadastro;
    procedure TabSheet13Show(Sender: TObject);
    procedure cbTurmaCIEClick(Sender: TObject);
    procedure btnMigrarDadosEducacensoClick(Sender: TObject);
    procedure btnAlteraDadosDocumentacaoClick(Sender: TObject);
    procedure TabSheet14Show(Sender: TObject);
    procedure btnCancelarAlteracaoDocumentosClick(Sender: TObject);
    procedure btnGravaDadosDocumentacaoClick(Sender: TObject);
    procedure btnVisualizarEmissaoContratoClick(Sender: TObject);
    procedure ValidaDocs;

    private
    FStatus: TStatus;
  public

  end;

var

  frmFichaCrianca: TfrmFichaCrianca;
  vListaTurma1, vListaTurma2 : TStringList;
  vListaEmpresa1, vListaEmpresa2 : TStringList;
  vListaEscola1, vListaEscola2 : TStringList;
  vListaProgrSocial1, vListaProgrSocial2 : TStringList;
  vListaClassificacao1, vListaClassificacao2 : TStringList;

  vListaSerie1, vListaSerie2 : TStringList;

  // vari�veis p�blicas
  AnoAtual, AnoNascimento, Idade, DiaAtual, DiaNascimento, MesAtual, MesNascimento : Word;
  Ano, Mes, Dia : Word;
  vvCOD_MATRICULA : integer;
  vv_JPG : TJpegImage;
  b : TStream;
  Path : String;
  mmStatus : String; //Utilizado para definir o status de Ativo/Desligado para Cadastro_Programa Social
  vMudaEnderecoFamilia : Boolean;

  //Novas vari�veis
  vvCOD_ESCOLA, vvCOD_SERIE : Integer;
  mmCOD_MATRICULA : String;

  //Var�veis criadas para AcompEscolar
  //Validar altera��o na tela de "Ano Letivo" e "Escola"
  mmANOLETIVO_ANTES, mmANOLETIVO_ATUAL, mmCOD_ESCOLA_ANTES, mmCOD_ESCOLA_ATUAL, mmCOD_SERIE_ANTES, mmCOD_SERIE_ATUAL : Integer;
  vvMUDOU_ANOLETIVO, vvMUDOU_ESCOLA, vvMUDOU_SERIE, mmNOVO_REGISTRO, vvMUDA_CADASTRO : Boolean;
  vIndEscola, vIndescolaE, vIndSerieE, vIndSerie : Integer;
  mmIDADE_IDEAL, mmIDADE_DEFASADA : Integer;


  
implementation

uses uDM, uPrincipal, uFichaPesquisa, rFichaCadastro, 
  uEmiteRegAtendimento, uInicializarBimestre, uLoginNovo,
  uDesligamentoProgSocial, uNovoLancamentoBiometrico,
  uCadastroSeriesPorEscola, rModeloBoletim,
  uSelecionaDadosAcompEscolarMatriz, uVerDadosEscolaSelecionada, uNavegador,
  uLegendaAcompEscolar, uAlteracaoEndereco, uEmitirFichaCadastro,
  uNavegador2, uEmiteContratoAdmissao;
{$R *.DFM}


procedure TfrmFichaCrianca.DesabilitarComboAcEsc;
begin
    // Desabilita os combosboxes, deixando apenas visualiza��o dos registros ..
    GroupBoxE2.Enabled := False;
    GroupBoxE3.Enabled := False;
    GroupBoxE4.Enabled := False;
    GroupBoxE5.Enabled := False;
    GroupBoxE14.Enabled := False;
    GroupBoxE12.Enabled := False;
    GroupBoxE8.Enabled := False;
    GroupBoxE9.Enabled := False;
    GroupBoxE6.Enabled := False;
    GroupBoxE10.Enabled := False;
end;

procedure TfrmFichaCrianca.btnNovoClick(Sender: TObject);
begin
  // perepara a tela para digita��o de uma nova ficha
  FStatus := fdInsert;
  AtualizaCamposBotoes;
  GroupBoxA.Enabled := True;

  // limpa os campos Edit
  LimpaEdits;
  txtPeso.Text := '0';
  txtAltura.Text := '0';

  //zera os demais campos
  cbSexoCrianca.ItemIndex := -1;
  lbStatus.Caption := '';
  //cbPeriodo.ItemIndex := 3;  //Para cadastros "novos" atribuir valor "padr�o" e corrigir no Acompanhamento Escolar

  //Se perfil Triagem e Acompanhamento Escolar, habilita o per�odo FUNDHAS
  //case vvIND_PERFIL of
  //  9,18,19: cbPeriodo.Enabled := True;
  //end;

  rgModalidade.ItemIndex := 0;
  chkProjetoVida.Checked := False;
  cbTipoSangue.ItemIndex := -1;
  cbTurma.ItemIndex := -1;
  cbEmpresa.ItemIndex := -1;
  edNumRGEscolarC.Text := '000000000';

  //txtMatricula.SetFocus;
  //Habilita o campo de fotografia
  Fotografia.Enabled := True;
  txtCep.Text := '12200-000';
  txtCotas.Text := '0';
  txtEndereco.Text := '<Favor preencher>';
  txtComplemento.Text := '<Favor preencher>';
  txtBairro.Text := '<Favor preencher>';

  //29/out/2014
  //Desabilitar o campo PER�ODO FUNDHAS quando perfil diferente de Acompanhamento Escolar
  if (vvIND_PERFIL in [9,18,19]) then
    GroupBoxN.Enabled := True
    //cbPeriodo.Enabled := True;
  else GroupBoxN.Enabled := False;

end;

procedure TfrmFichaCrianca.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes;
  //Verifica se veio da ficha modelo e de qual bot�o foi solicitado...
  //frmFichaCrianca.PageControl1.ActivePageIndex := 0;
  //frmFichaCrianca.PageControl2.ActivePageIndex := 0;
  AtualizaTelaAcompEscolar('Padrao');

  if (mmINTEGRACAO = True) then
    FillForm('S', dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value)
  else
    FillForm('F', '0');   // move para o primeiro registro

  frmFichaCrianca.PageControl1.ActivePageIndex := mmNUM_ABA;

  // Familiares
  if PageControl1.ActivePage.Name = 'TabSheet2' then
  begin
    // carrega o nome no topo da tela
    label4.Caption := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;

    // carrega o grid
    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;//txtMatricula.Text;
        Params.ParamByName('@pe_cod_familiar').Value := Null;
        Open;
        dbgFamiliares.Refresh;
      end;
    except end;

    // atualiza a tela
    FillFormFamiliares;

    // atualiza renda familiar (no canto superior da tela) e o total de integrantes
    AtualizaRendaFamiliar;
  end;

  //Desabilita para TODOS os usu�rios as abas
  //   - Dados da Escola Atual
  //   - Dados de Acompanhamento Escolar
  frmFichaCrianca.PageControl2.Pages[2].TabVisible := False;
  frmFichaCrianca.PageControl2.Pages[3].TabVisible := False;

  // habilita os tabsheet conforme o perfil
  case vvIND_PERFIL of
    1: begin //Administrador
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False;  //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
       end;
    2: begin //Gestor
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
       end;
    3: begin //Assistente Social
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := True; //Acompanhamento Escolar
       end;
    4: begin //Consulta
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;
    5: begin //Equipe Multi
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False;  //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False;  //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    6: begin //Professor/Instrutor
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False;  //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
      end;
    7: begin //Diretor
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False;  //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;
    8: begin //DRH
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
      end;
    9: begin //Triagem
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
      end;
    10: begin //Educa��o F�sica
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    11: begin  //Perfil Projeto Gestante
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    //Perfil 12-Compras/Almoxarifado n�o acessa a ficha de cadastro

    //Odontologia
    13: begin
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := False;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    //Servi�o Social (Plant�o)
    14: begin
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    //Servi�o Social (Supervis�o)
    15: begin
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    //Coordenador de Projeto
    16: begin
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    //Assessoria de Qualidade
    17: begin
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
      end;

    //Acompanhamento Escolar
    18: begin
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
      end;

    19: begin //Coord. Acomp. Escolar
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := True; //Documenta��o
       end;

    20,23,24,25: begin //RA
        frmFichaCrianca.PageControl1.Pages[0].TabVisible := True;  //Dados Cadastrais
        frmFichaCrianca.PageControl1.Pages[1].TabVisible := True;  //Composi��o Familiar
        frmFichaCrianca.PageControl1.Pages[2].TabVisible := True;  //Escolaridade
        frmFichaCrianca.PageControl1.Pages[3].TabVisible := False;  //Programas Sociais -> Desabilitar a partir deJunho/2013
        frmFichaCrianca.PageControl1.Pages[4].TabVisible := False; //Atendimento na Rede
        frmFichaCrianca.PageControl1.Pages[5].TabVisible := True;  //Hist�rico de Movimenta��o
        frmFichaCrianca.PageControl1.Pages[6].TabVisible := False; //Acompanhamento Escolar
        frmFichaCrianca.PageControl1.Pages[7].TabVisible := False; //Documenta��o
       end;


  end;

  // habilita os bot�es conforme o perfil
  // Administrador, Consulta, Instrutor/Professor,
  // Diretor, DRH, Educa��o F�sica, As. Social (Proj. Gestante), Compras/Almoxarifado,
  // Odontologia, Servi�o Social(Plant�o), Servi�o Social(Supervis�o),
  // Assessoria de Qualidade e Acompanhamento Escolar n�o podem alterar dados
  //if vvIND_PERFIL in [1, 4, 7, 8, 9, 10, 11, 12, 13, 14, 15] then
  //if vvIND_PERFIL in [1, 4, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23, 24, 25] then
  if vvIND_PERFIL in [1, 4, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 20, 23, 24, 25] then
  begin
    //Tela de Dados Cadastrais
    btnNovo.Enabled := False;
    btnAlterar.Enabled := False;

    //Tela de Composi��o Familiar
    btnNovo2.Enabled := False;
    btnAlterar2.Enabled := False;
    btnExcluir2.Enabled := False;
    btnAlteraEndereco.Enabled := False;

    //Tela de Escolaridade
    //Dados da Escola Atual
    btnAlterar3.Enabled := False;
    //Dados de Acompanhamento Escolar
    btnNovoAcompanhamento.Enabled := False;
    btnAlterarAcompanhamento.Enabled := False;
    btnGravarAcompanhamento.Enabled := False;
    btnCancelarAcompanhamento.Enabled := False;
    btnInicializarBimestre.Enabled := False;
    btnSairAcompanhamento.Enabled := True;
    Panel11.Enabled := False;

    //Tela de Programas Sociais
    btnNovo4.Enabled := False;
    btnAlterar4.Enabled := False;
    btnDesligamento.Enabled := False;
    btnAtivar.Enabled := False;

    //Tela de Atendimento na Rede
    btnNovo5.Enabled := False;
    btnAlterar5.Enabled := False;

  end;

  //if perfil  Instrutor/Professor habilita os botoes de escolaridade
  if vvIND_PERFIL in [2, 3, 6] then
  begin
    //Dados cadastrais - a partir de abril de 2006 os instrutores/professores poder�o fazer altera��es
    //nos dados cadastrais
    //btnNovo.Enabled := False;
    //btnAlterar.Enabled := False;
    btnNovo.Enabled := True;
    btnAlterar.Enabled := True;
    btnAlteraEndereco.Enabled := True;

    //Composi��o Familiar
    //btnNovo2.Enabled := False;
    //btnAlterar2.Enabled := False;
    //btnExcluir2.Enabled := False;
    btnNovo2.Enabled := True;
    btnAlterar2.Enabled := True;
    btnExcluir2.Enabled := True;

    //Tela de Escolaridade
    //Dados da Escola Atual
    btnAlterar3.Enabled := True;
    //Dados de Acompanhamento Escolar
    btnNovoAcompanhamento.Enabled := True;
    btnAlterarAcompanhamento.Enabled := False;
    btnGravarAcompanhamento.Enabled := False;
    btnCancelarAcompanhamento.Enabled := False;
    btnInicializarBimestre.Enabled := False;
    btnSairAcompanhamento.Enabled := True;
    Panel11.Enabled := True;

    //Programas Sociais
    btnNovo4.Enabled := True;
    btnAlterar4.Enabled := True;
    btnDesligamento.Enabled := False;
    btnAtivar.Enabled := False;

    //Atendimentos na rede
    btnNovo5.Enabled := True;
    btnAlterar5.Enabled := True;

  end
  //Psicopedagogia-Psicologia
  else if vvIND_PERFIL = 5 then
  begin
    //Tela de Dados Cadastrais
    btnNovo.Enabled := True;
    btnAlterar.Enabled := True;

    //Tela de Composi��o Familiar
    btnNovo2.Enabled := False;
    btnAlterar2.Enabled := False;
    btnExcluir2.Enabled := False;

    //Tela de Escolaridade
    //Dados da Escola Atual
    btnAlterar3.Enabled := True;
    //Dados de Acompanhamento Escolar
    btnNovoAcompanhamento.Enabled := True;
    btnAlterarAcompanhamento.Enabled := False;
    btnGravarAcompanhamento.Enabled := False;
    btnCancelarAcompanhamento.Enabled := False;
    btnInicializarBimestre.Enabled := True;
    btnSairAcompanhamento.Enabled := True;
    Panel11.Enabled := True;

    //Tela de Programas Sociais
    btnNovo4.Enabled := False;
    btnAlterar4.Enabled := False;
    btnDesligamento.Enabled := False;
    btnAtivar.Enabled := False;

    //Tela de Atendimento na Rede
    btnNovo5.Enabled := False;
    btnAlterar5.Enabled := False;

  end

  //Coordenador de Projeto
  else if vvIND_PERFIL = 16 then
  begin
    //Tela de Dados Cadastrais
    btnNovo.Enabled := True;
    btnAlterar.Enabled := False;

    //Tela de Composi��o Familiar
    btnNovo2.Enabled := True;
    btnAlterar2.Enabled := False;
    btnExcluir2.Enabled := False;

    //Tela de Escolaridade
    //Dados da Escola Atual
    btnAlterar3.Enabled := True;
    //Dados de Acompanhamento Escolar
    btnNovoAcompanhamento.Enabled := True;
    btnAlterarAcompanhamento.Enabled := False;
    btnGravarAcompanhamento.Enabled := False;
    btnCancelarAcompanhamento.Enabled := False;
    btnInicializarBimestre.Enabled := True;
    btnSairAcompanhamento.Enabled := True;
    Panel11.Enabled := True;

    //Tela de Programas Sociais
    btnNovo4.Enabled := False;
    btnAlterar4.Enabled := False;
    btnDesligamento.Enabled := False;
    btnAtivar.Enabled := False;

    //Tela de Atendimento na Rede
    btnNovo5.Enabled := False;
    btnAlterar5.Enabled := False;

  end

  else if (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19) then
  begin
  
    //Tela de Dados Cadastrais
    btnNovo.Enabled := True;
    btnAlterar.Enabled := True;

    //Tela de Composi��o Familiar
    btnNovo2.Enabled := False;
    btnAlterar2.Enabled := False;
    btnExcluir2.Enabled := False;

    //Tela de Escolaridade
    //Dados da Escola Atual
    btnAlterar3.Enabled := True;

    //Dados de Acompanhamento Escolar
    btnNovoAcompanhamento.Enabled := True;
    btnAlterarAcompanhamento.Enabled := False;
    btnGravarAcompanhamento.Enabled := False;
    btnCancelarAcompanhamento.Enabled := False;
    btnInicializarBimestre.Enabled := True;
    btnSairAcompanhamento.Enabled := True;
    Panel11.Enabled := True;

    //dbgDadosAcompEscolar.PopupMenu := popExcluirAcompanhamento;

    //Tela de Programas Sociais
    btnNovo4.Enabled := False;
    btnAlterar4.Enabled := False;
    btnDesligamento.Enabled := False;
    btnAtivar.Enabled := False;

    //Tela de Atendimento na Rede
    btnNovo5.Enabled := False;
    btnAlterar5.Enabled := False;

  end;

  //Carrega a lista de escolas...
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      cbEscolaE.Clear;
      while not eof do
      begin
        vListaEscola1.Add(IntToStr(dmDeca.cdsSel_Escola.FieldByName('cod_escola').Value));
        vListaEscola2.Add(dmDeca.cdsSel_Escola.FieldByName('nom_escola').Value);
        cbEscolaE.Items.Add(dmDeca.cdsSel_Escola.FieldByName('nom_escola').Value);
        dmDeca.cdsSel_Escola.Next;
      end;

    end;
  except end;

  //Habilita/Desabilita a exibi��o do campo per�odo de acordo com os valores expressos...
  //if (cbPeriodo.Text = 'MANH�') or (cbPeriodo.Text = 'TARDE') or (cbPeriodo.Text = 'INTEGR/ESC') then
  //  GroupBoxN.Enabled := False else GroupBoxN.Enabled := True;

  
end;

procedure TfrmFichaCrianca.btnSalvarClick(Sender: TObject);
var
  vOk : boolean;

begin
  vOk := true;

  // valida os dados
  if ValidaDados then
  begin

    // prepara os dados para grava�ao
    if FStatus = FdInsert then
    begin
      try
        // checa se a nova matricula e de crian�as do PETI - 999......
        if (Copy(txtMatricula.Text, 0, 3) = '999') then
        begin
          with dmDeca.cdsSel_Cadastro_NovaMatricula999 do
          begin
            Close;
            Open;

            txtMatricula.Text := FieldByName('NovaMatricula').AsString;

            Close;

          end;
        end;

        //Checa de a nova matr�cula � de admiss�o da triagem
        if (Copy(txtMatricula.Text,0,3) = '777') then
        begin
          with dmDeca.cdsSel_Cadastro_NovaMatricula777 do
          begin
            Close;
            Open;

            txtMatricula.Text := FieldByName('NovaMatricula').AsString;

            Close;

          end;
        end;


        with dmDeca.cdsInc_Cadastro do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value         := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_num_prontuario').Value        := GetValue(txtProntuario, vtInteger);
          Params.ParamByName('@pe_num_cartaosias').Value        := GetValue(txtSias.Text);
          Params.ParamByName('@pe_nom_nome').Value              := GetValue(txtNome.Text);
          Params.ParamByName('@pe_dat_admissao').Value          := GetValue(txtAdmissao, vtDate);
          Params.ParamByName('@pe_nom_endereco').Value          := GetValue(txtEndereco.Text);
          Params.ParamByName('@pe_nom_complemento').Value       := GetValue(txtComplemento.Text);
          Params.ParamByName('@pe_nom_bairro').Value            := GetValue(txtBairro.Text);

          //txtCep.EditMask                                       := '';
          Params.ParamByName('@pe_num_cep').Value               := Trim(txtCep.Text);
          //txtCep.EditMask                                       := '99999-999';

          //Params.ParamByName('@pe_num_telefone').Value          := GetValue(txtTelefone.Text);
          Params.ParamByName('@pe_num_telefone').Value          := Trim(txtTelefone.Text);

          Params.ParamByName('@pe_dat_nascimento').Value        := GetValue(txtNascimento,vtDate);
          Params.ParamByName('@pe_ind_sexo').Value              := GetValue(cbSexoCrianca.Items[cbSexoCrianca.ItemIndex]);
          Params.ParamByName('@pe_ind_status').Value            := 1;
          Params.ParamByName('@pe_dsc_periodo').Value           := GetValue(cbPeriodo.Items[cbPeriodo.ItemIndex]);
          Params.ParamByName('@pe_val_peso').Value              := GetValue(txtPeso, vtReal);
          Params.ParamByName('@pe_val_altura').Value            := GetValue(txtAltura, vtReal);
          Params.ParamByName('@pe_dsc_etnia').Value             := GetValue(txtEtnia.Text);
          Params.ParamByName('@pe_num_calcado').Value           := GetValue(txtCalcado.Text);
          Params.ParamByName('@pe_ind_tam_calca').Value         := GetValue(txtCalca.Text);
          Params.ParamByName('@pe_ind_tam_camisa').Value        := GetValue(txtCamisa.Text);
          Params.ParamByName('@pe_num_cota_passes').Value       := StrToFloat(txtCotas.Text);
          Params.ParamByName('@pe_ind_pontuacao_triagem').Value := GetValue(txtPontuacao, vtInteger);
          Params.ParamByName('@pe_ind_modalidade').AsInteger    := rgModalidade.ItemIndex;
          Params.ParamByName('@pe_flg_projeto_vida').AsBoolean  := chkProjetoVida.Checked;
          Params.ParamByName('@pe_ind_tipo_sanguineo').Value    := GetValue(cbTipoSangue.Text);
          Params.ParamByName('@pe_dsc_saude').Value             := GetValue(txtSaude.Text);

          Params.ParamByName('@pe_possui_bolsa_familia').Value   := rgBolsaFamilia.ItemIndex;
          Params.ParamByName('@pe_num_nis').Value                := GetValue(txtNIS.Text);
          Params.ParamByName('@pe_participa_ativ_externa').Value := rgAtivExterna.ItemIndex;
          Params.ParamByName('@pe_ativ_ext_quais').Value         := Trim(txtATIVIDADE_EXTERNA.Text);

          //Cadastra NOVO registro como "Sem Escolaridade"
          //Params.ParamByName('@pe_cod_escola').AsInteger := 209;

          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;

          if vvCOD_UNIDADE = 44 then
            Params.ParamByName('@pe_num_secao').Value := '0310'
          else
            Params.ParamByName('@pe_num_secao').Value := '0000';

          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;

          if cbTurma.ItemIndex < 0 then
            Params.ParamByName ('@pe_cod_turma').Value := Null
          else
            Params.ParamByName('@pe_cod_turma').AsInteger := StrToInt(vListaTurma1.Strings[cbTurma.ItemIndex]);

          //if cbEmpresa.ItemIndex < 0 then
          //  Params.ParamByName ('@pe_cod_empresa').Value := Null
          //else
          //  Params.ParamByName ('@pe_cod_empresa').AsInteger := StrToInt(vListaEmpresa1.Strings[cbEmpresa.ItemIndex]);

          //Params.ParamByName('@pe_num_secao').Value := '0001';

          //os parametros abaixo passaram a ser alimentados atrav�s da aba Documenta��o, desde 07/03/2017
          Params.ParamByName('@pe_num_cpf').Value := GetValue(txtCpf.Text);
          Params.ParamByName('@pe_num_rg').Value := GetValue(txtRg.Text);
          Params.ParamByName('@pe_num_certidao').Value := GetValue(txtCertidaoNascimento.Text);

          Params.ParamByName ('@pe_dsc_rg_escolar').Value := GetValue(edNumrgEscolarC.Text);

          //A PARTIR DE 07/03/2017, INCLU�DOS OS CAMPOS ABAIXO PARA INSER��O PELA TRIAGEM (PROCESSO ADMISSIONAL)
          Params.ParamByName ('@pe_dsc_cor_olhos').Value                 := GetValue(txtCorOlhos.Text);
          Params.ParamByName ('@pe_dsc_cor_cabelos').Value               := GetValue(txtCorCabelos.Text);
          Params.ParamByName ('@pe_ind_fumante').Value                   := cbFumante.ItemIndex;
          Params.ParamByName ('@pe_ind_portador_necess_especiais').Value := cbPortadorNecessidades.ItemIndex;
          Params.ParamByname ('@pe_dsc_necessidades_esp_quais').Value    := GetValue(edNecessidadesEspeciais.Text);
          Execute;
          Close;
        
          //Atualizar os dados do educacenso para o padr�o para n�o deixar "em branco"
          try
            with dmDeca.cdsUpd_TurmaCIE_Cadastro do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value           := Trim(txtMatricula.Text);
              Params.ParamByName ('@pe_num_turma_cei').Value           := '000.000.000';
              Params.ParamByName ('@pe_dat_cadastro_educacenso').Value := Date();
              Execute;
            end;
          except end;

          // Atualiza a bara de status com a quantidade de fichas atualizada
          btnAlteraEndereco.Enabled := True;
          frmPrincipal.AtualizaStatusBarUnidade;
        end;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram gravados'+#13+#10+
                    'Verifique se j� existe alguma crian�a cadastrada com esta matr�cula (' +
                    txtMatricula.Text +')' +#13+#10+'ou entre em contato com o Administrador do Sistema');
        vOk := False;
      end;
    end
    //altera��o dos dados do cadastro
    else if FStatus = FdEdit then
    begin
      try
        with dmDeca.cdsAlt_Cadastro do
        begin
          Close;

          Params.ParamByName('@pe_cod_matricula').Value          := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_num_prontuario').Value         := GetValue(txtProntuario, vtInteger);
          Params.ParamByName('@pe_num_cartaosias').Value         := GetValue(txtSias.Text);

          //Params.ParamByName('@pe_dsc_rg_escolar').Value := GetValue(edNumRGEscolarC.Text);

          Params.ParamByName('@pe_nom_nome').Value               := GetValue(txtNome.Text);
          Params.ParamByName('@pe_dat_admissao').Value           := GetValue(txtAdmissao, vtDate);
          Params.ParamByName('@pe_nom_endereco').Value           := GetValue(txtEndereco.Text);
          Params.ParamByName('@pe_nom_complemento').Value        := GetValue(txtComplemento.Text);
          Params.ParamByName('@pe_nom_bairro').Value             := GetValue(txtBairro.Text);
          Params.ParamByName('@pe_num_cep').Value                := GetValue(txtCep.Text);
          Params.ParamByName('@pe_num_telefone').Value           := GetValue(txtTelefone.Text);
          Params.ParamByName('@pe_dat_nascimento').Value         := GetValue(txtNascimento, vtDate);
          Params.ParamByName('@pe_ind_sexo').Value               := GetValue(cbSexoCrianca.Items[cbSexoCrianca.ItemIndex]);
          Params.ParamByName('@pe_dsc_periodo').Value            := GetValue(cbPeriodo.Items[cbPeriodo.ItemIndex]);
          Params.ParamByName('@pe_val_peso').Value               := GetValue(txtPeso, vtReal);
          Params.ParamByName('@pe_val_altura').Value             := GetValue(txtAltura, vtReal);
          Params.ParamByName('@pe_dsc_etnia').Value              := GetValue(txtEtnia.Text);
          Params.ParamByName('@pe_num_calcado').Value            := GetValue(txtCalcado.Text);
          Params.ParamByName('@pe_ind_tam_calca').Value          := GetValue(txtCalca.Text);
          Params.ParamByName('@pe_ind_tam_camisa').Value         := GetValue(txtCamisa.Text);
          Params.ParamByName('@pe_num_cota_passes').Value        := StrToFloat(txtCotas.Text);
          Params.ParamByName('@pe_ind_pontuacao_triagem').Value  := GetValue(txtPontuacao, vtInteger);
          Params.ParamByName('@pe_ind_modalidade').AsInteger     := rgModalidade.ItemIndex;
          Params.ParamByName('@pe_flg_projeto_vida').AsBoolean   := chkProjetoVida.Checked;
          Params.ParamByName('@pe_ind_tipo_sanguineo').Value     := GetValue(cbTipoSangue.Text);
          Params.ParamByName('@pe_dsc_saude').Value              := GetValue(txtSaude.Text);

          Params.ParamByName('@pe_possui_bolsa_familia').Value   := rgBolsaFamilia.ItemIndex;
          Params.ParamByName('@pe_num_nis').Value                := GetValue(txtNIS.Text);
          Params.ParamByName('@pe_participa_ativ_externa').Value := rgAtivExterna.ItemIndex;
          Params.ParamByName('@pe_ativ_ext_quais').Value         := GetValue(txtATIVIDADE_EXTERNA.Text);

          //Params.ParamByName('@pe_cod_turma').AsInteger := StrToInt(vListaTurma1.Strings[cbTurma.ItemIndex]);

          if (cbTurma.ItemIndex < 0) then
            Params.ParamByName ('@pe_cod_turma').Value := Null
          else
            Params.ParamByName('@pe_cod_turma').AsInteger := StrToInt(vListaTurma1.Strings[cbTurma.ItemIndex]);

          //if cbEmpresa.ItemIndex < 0 then
          //  Params.ParamByName ('@pe_cod_empresa').Value := Null
          //else
          //  Params.ParamByName ('@pe_cod_empresa').AsInteger := StrToInt(vListaEmpresa1.Strings[cbEmpresa.ItemIndex]);


          //os parametros abaixo passaram a ser alimentados atrav�s da aba Documenta��o, desde 07/03/2017
          //Params.ParamByName('@pe_num_cpf').Value := GetValue (txtCpf.Text);
          //Params.ParamByName('@pe_num_rg').Value := GetValue(txtRg.Text);
          //Params.ParamByName('@pe_num_certidao').Value := GetValue(txtCertidaoNascimento.Text);

          Params.ParamByName ('@pe_dsc_rg_escolar').Value := GetValue(edNumrgEscolarC.Text);

          //A PARTIR DE 07/03/2017, INCLU�DOS OS CAMPOS ABAIXO PARA INSER��O PELA TRIAGEM (PROCESSO ADMISSIONAL)
          Params.ParamByName ('@pe_dsc_cor_olhos').Value                 := GetValue(txtCorOlhos.Text);
          Params.ParamByName ('@pe_dsc_cor_cabelos').Value               := GetValue(txtCorCabelos.Text);
          Params.ParamByName ('@pe_ind_fumante').Value                   := cbFumante.ItemIndex;
          Params.ParamByName ('@pe_ind_portador_necess_especiais').Value := cbPortadorNecessidades.ItemIndex;
          Params.ParamByname ('@pe_dsc_necessidades_esp_quais').Value    := GetValue(edNecessidadesEspeciais.Text);           

          Execute;
          Close;

          //Ap�s realizar a altera��o dos dados, verifica o valor da vari�vel vMudaEnderecoFamilia
          //para que seja alterado o endere�o de todos os membros da composi��o failiar...

          if vMudaEnderecoFamilia = True then
          begin
            if Application.MessageBox('Deseja alterar o endere�o de todos da Composi��o Familiar?', 'Altera��o de Endere�o', MB_ICONQUESTION + MB_YESNO ) = idYes then
            begin
              //Desabilita as triggers de atualiza��o e inclus�o do cadastro_familia para poder alterar o endere�o
              with dmDeca.cdsDesabilitaTriggers_Responsavel do
              begin
                Close;
                Execute;
              end;

              //Executa a procedure para altera��o somente dos endere�os da composi��o familiar
              with dmDeca.cdsAlt_EnderecoFamilia do
              begin
                Close;
                Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Params.ParamByName('@pe_nom_endereco').Value := GetValue(txtEndereco.Text);
                Params.ParamByName('@pe_nom_complemento').Value := GetValue(txtComplemento.Text);
                Params.ParamByName('@pe_nom_bairro').Value := GetValue(txtBairro.Text);
                Params.ParamByName('@pe_num_cep').Value := GetValue(txtCep.Text);
                Execute;
              end;

              //Habilita as triggers de atualiza��o e inclus�o do cadastro_familia
              with dmDeca.cdsHabilitaTriggers_Responsavel do
              begin
                Close;
                Execute;
              end;

            end
          end;

          vOK := True;

        end
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram alterados com sucesso');
        vOk := False;
      end;
    end;

    // atualiza a tela
    if vOk then
      FillForm ('S', txtMatricula.Text)
    else
      FillForm ('F', '0');

    FStatus := FdNone;
    AtualizaCamposBotoes;

  end
  else
  begin
    ShowMessage('Dados Inv�lidos!'+#13+#10+#13+#10+
                'Verifique se os campos obrigat�rio (em azul) foram preenchidos');
  end;

end;

procedure TfrmFichaCrianca.btnPrimeiroClick(Sender: TObject);
begin
  FillForm('F', '0');
  if dmDeca.cdsSel_Cadastro_Move.eof then FillForm ('S',txtMatricula.Text);
  FillFormFamiliares;
end;

procedure TfrmFichaCrianca.btnProximoClick(Sender: TObject);
begin
  FillForm('N', txtMatricula.text);
  if dmDeca.cdsSel_Cadastro_Move.eof then FillForm ('S',txtMatricula.Text);
  FillFormFamiliares;
end;

procedure TfrmFichaCrianca.btnUltimoClick(Sender: TObject);
begin
  FillForm('L', '0');
  if dmDeca.cdsSel_Cadastro_Move.eof then FillForm ('S',txtMatricula.Text);
  FillFormFamiliares;
end;

procedure TfrmFichaCrianca.btnAlterarClick(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    FStatus:= FdEdit;
    AtualizaCamposBotoes;
    txtProntuario.SetFocus;
    btnAlteraEndereco.Enabled := False;

    //Desabilitar os campos referentes aos dados de endere�o
    GroupBoxE.Enabled := False;
    GroupBoxF.Enabled := False;
    GroupBoxG.Enabled := False;
    GroupBoxH.Enabled := False;
    GroupBoxU.Enabled := False;

    if ((vvIND_PERFIL in [9,18,19]) and ((cbPeriodo.Text = 'MANH�') or (cbPeriodo.Text = 'TARDE') or (cbPeriodo.Text = 'INTEGR/ESC') or (cbPeriodo.Text = 'NOITE') or (cbPeriodo.Text = '<ND>') or (cbPeriodo.ItemIndex = -1)))
        or ((vvIND_PERFIL in [2,3]) and (vvCOD_UNIDADE = 40)) then
    begin
      GroupBoxN.Enabled := True;
      cbPeriodo.Enabled := True;
    end
      //cbPeriodo.Enabled := True;
    else
    begin
      GroupBoxN.Enabled := False;
      cbPeriodo.Enabled := False;
    end;


    {
    if (vvIND_PERFIL in [18,19]) then
    begin
      //Habilita/Desabilita a exibi��o do campo per�odo de acordo com os valores expressos...
      if (cbPeriodo.Text = 'MANH�') or (cbPeriodo.Text = 'TARDE') or (cbPeriodo.Text = 'INTEGR/ESC') or (cbPeriodo.Text = 'NOITE') then
        GroupBoxN.Enabled := False
      else if ((vvIND_PERFIL in [2,3]) and (vvCOD_UNIDADE=40)) then
        GroupBoxN.Enabled := True;
    end;
    }

    //desativar abaixo por motivo de permitir apenas � Triagem fazer a altera��o do per�odo
    //em 01/mar�o/2016 - Maria Cristina Bigoli...
    
    {else if ((vvIND_PERFIL in [2,3]) and (vvCOD_UNIDADE=40)) then
    begin
      //Habilita/Desabilita a exibi��o do campo per�odo de acordo com os valores expressos...
      if (cbPeriodo.Text = 'MANH�') or (cbPeriodo.Text = 'TARDE') or (cbPeriodo.Text = 'INTEGR/ESC') or (cbPeriodo.Text = 'NOITE') then
        GroupBoxN.Enabled := False
      else
        GroupBoxN.Enabled := True;
    end
    else
      GroupBoxN.Enabled := False;
      }
  end;

  //29/out/2014
  //Desabilitar o campo PER�ODO FUNDHAS quando perfil diferente de Acompanhamento Escolar
  //if (vvIND_PERFIL in [9,18,19]) then
  //  GroupBoxN.Enabled := True
    //cbPeriodo.Enabled := True;
  //else GroupBoxN.Enabled := False;

end;

procedure TfrmFichaCrianca.btnSairFamiliaClick(Sender: TObject);
begin
//Cancelar opera��o com registros em edi��o
//frmFichaCrianca.Close;
frmFichaCrianca.TabSheet1.PageControl.ActivePage.PageControl.ActivePageIndex := 0;
end;

procedure TfrmFichaCrianca.btnFecharPvClick(Sender: TObject);
begin
  frmFichaCrianca.Close;
end;

procedure TfrmFichaCrianca.btnCancelarClick(Sender: TObject);
begin
  if FStatus = FdInsert then
    FillForm ('F', '0')
  else
    FillForm ('S', txtMatricula.Text);

  FStatus := FdNone;
  AtualizaCamposBotoes;
  btnAlteraEndereco.Enabled := True;

end;

procedure TfrmFichaCrianca.FormCreate(Sender: TObject);
begin
  FStatus:= fdNone;
  AtualizaCamposBotoes;
  AtualizaCamposBotoes2;
  AtualizaCamposBotoes3;
  AtualizaCamposBotoes4;
  AtualizaCamposBotoes5;
  AtualizaTelaAcompEscolar('Padrao');

  vListaTurma1 := TStringList.Create();
  vListaTurma2 := TStringList.Create();

  //Carregar a lista de Turmas da Unidade
  try
    with dmDeca.cdsSel_Cadastro_Turmas do
    begin
      Close;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_nom_turma').Value := Null;

      //Permitir ao Administrador, Equipe Multi, Diretor, DRH, Triagem, Educa��o F�sica, Acomp. Escolar, Coord. Acomp. Escolar visualizar todas as c�as/adolescentes
      if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or
         (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 9) or (vvIND_PERFIL = 10) or
         (vvIND_PERFIL = 11) or (vvIND_PERFIL = 12) or (vvIND_PERFIL = 13) or (vvIND_PERFIL = 14) or
         (vvIND_PERFIL = 15) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19) or (vvIND_PERFIL = 20) or
         (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then
      begin
        Params.ParamByName ('@pe_cod_unidade').Value := Null
      end
      else
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;

      Open;

      cbTurma.Items.Clear;
      while not eof do
      begin
        vListaTurma1.Add (IntToStr(FieldByName ('cod_turma').AsInteger));
        vListaTurma2.Add (FieldByName ('nom_turma').AsString);
        cbTurma.Items.Add (FieldByName ('nom_turma').AsString);
        Next;
      end;

      Close;

    end;

    //Carregar a lista de escolas
    vListaEscola1 := TStringList.Create();
    vLIstaEscola2 := TStringList.Create();

    //Inicializa a lista de s�ries, mas n�o usa aqui...
    vListaSerie1 := TStringList.Create();
    vListaSerie2 := TStringList.Create();

    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      cbEscolaE.Clear;
      while not eof do
      begin
        vListaEscola1.Add (IntToStr(FieldByName('cod_escola').AsInteger));
        vListaEscola2.Add (FieldByName('nom_escola').AsString);
        //cbEscola.Items.Add(FieldByName('nom_escola').AsString);
        cbEscolaE.Items.Add (FieldByName('nom_escola').AsString);
        Next;
      end;
    end;

    //Carregar a lista de Programas Sociais
    vListaProgrSocial1 := TStringList.Create();
    vListaProgrSocial2 := TStringList.Create();
    cbProgramaSocial.Clear;
    with dmDeca.cdsSel_ProgSocial do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
      Params.ParamByName('@pe_dsc_progsocial').Value := Null;
      Open;

      while not eof do
      begin
        vListaProgrSocial1.Add (IntToStr(FieldByName('cod_id_progsocial').AsInteger));
        vListaProgrSocial2.Add (FieldByName('dsc_progsocial').ASString);
        cbProgramaSocial.Items.Add ('dsc_progsocial');
        Next;
      end;
    end;

  except end;

end;

procedure TfrmFichaCrianca.FillForm(const AMove:String;
  const AMatricula:string);
  var vIndTurma, vIndEmpresa : Integer;
      xPeso, xAltura, xIMC : Real;
begin
  xPeso := 0;
  xAltura := 0;
  if (Trim(AMove) <> '') and (Length(AMatricula) > 0) then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').Value := AMove;
        Params.ParamByName('@pe_Matricula_Atual').Value := AMatricula;
        Params.ParamByName('@pe_cod_unidade').Value     := vvCOD_UNIDADE;
        Open;

        if Length(FieldByName('cod_matricula').AsString) > 0  then
        begin
          // atualiza os campos do formul�rio
          // dados cadastrais
          txtMatricula.Text    := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_matricula').AsString;
          txtProntuario.Text   := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_prontuario').AsInteger);

          //Carrega o n�mero do RG Escolar...
          edNumRGEscolarC.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_rg_escolar').AsString;

          txtSias.Text         := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cartaosias').AsString;
          txtNome.Text         := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_nome').AsString;
          if (DateToStr(FieldByName('dat_admissao').AsDateTime) <> '30/12/1899') OR (DateToStr(FieldByName('dat_admissao').AsDateTime) <> '  /  /    ') then
            txtAdmissao.Text   := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('dat_admissao').AsDateTime)
          else
            txtAdmissao.Clear;

          txtEndereco.Text     := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_endereco').AsString;
          txtComplemento.Text  := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_complemento').AsString;
          txtBairro.Text       := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_bairro').AsString;
          //txtCep.EditMask      := '';
          txtCep.Text          := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_cep').AsString);
          //txtCep.EditMask      := '00000\-999;1;_';

          //txtTelefone.EditMask := '';
          txtTelefone.Text     := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_telefone').AsString);
          //txtTelefone.EditMask := '(99)99999-9999';


          if (DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('dat_nascimento').AsDateTime) <> '30/12/1899') OR (DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('dat_nascimento').AsDateTime) <> '  /  /    ') then
          begin
            txtNascimento.Text := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('dat_nascimento').AsDateTime);
            lbIdade.Caption    := CalculaIdade(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_nascimento').AsDateTime);
            //Repassa idade ao campo da tela de Acompanhamento de Situa��o Escolar
            edIdadeAtual.Text  := Trim(Copy(CalculaIdade(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_nascimento').AsDateTime),1,3));
          end
          else begin
            txtNascimento.Clear;
            lbIdade.Caption    := '';
            edIdadeAtual.Text  := '0';
          end;
          cbSexoCrianca.ItemIndex := cbSexoCrianca.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_sexo').AsString));
          if DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('log_data').AsDateTime) <> '30/12/1899' then
            txtDtCadastro.Text := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('log_data').AsDateTime)
          else
            txtDtCadastro.Clear;

          lbUnidade.Caption    := dmDeca.cdsSel_Cadastro_Move.FieldByName('vUnidade').AsString;
          lbUnidadeE.Caption   := dmDeca.cdsSel_Cadastro_Move.FieldByName('vUnidade').AsString;

          //Seleciona e carrega os dados da se��o (Caso exista)
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao').AsString <> '') then
          begin
            with dmDeca.cdsSel_Secao do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_secao').Value    := Null;
              Params.ParamByName ('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_unidade').AsInteger;//vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_secao').AsString    := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao').AsString;
              Params.ParamByname ('@pe_flg_status').Value      := Null;
              Open;

              edSecao.Text            := dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString;
              edAssistenteSocial.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').AsString;

            end
          end
          else
          begin
            edSecao.Text            := '';
            edAssistenteSocial.Text := '';
          end;

          // Status
          case FieldByName('ind_status').AsInteger of
            1: lbStatus.Caption := 'ATIVO';
            2: lbStatus.Caption := 'DESLIGADO';
            3: lbStatus.Caption := 'SUSPENSO';
            4: lbStatus.Caption := 'AFASTADO';
            5: lbStatus.Caption := 'EM TRANSFER�NCIA';
            6: lbStatus.Caption := 'EM CONV�NIO';
          else
            lbStatus.Caption    := 'INDEFINIDO';
          end;

          // dados Fundhas
          cbPeriodo.ItemIndex   := cbPeriodo.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_periodo').AsString));
          txtPeso.Text          := Format('%2.3f', [dmDeca.cdsSel_Cadastro_Move.FieldByName('val_peso').AsFloat]);
          txtAltura.Text        := Format('%1.2f', [dmDeca.cdsSel_Cadastro_Move.FieldByName('val_altura').AsFloat]);
          //Repassar o IMC e a classifica��o do IMC, caso haja
          xPeso                 := dmDeca.cdsSel_Cadastro_Move.FieldByName ('val_peso').AsFloat;
          xAltura               := dmDeca.cdsSel_Cadastro_Move.FieldByName ('val_altura').AsFloat;
          //Executa a procedure SP_SEL_ProcuraIMC para repassar a Classifica��o do IMC
          //Valida as entradas de peso e altura para exibi��o do IMC
          if ((xPeso <> 0) and (xPeso <> NULL)) and ((xAltura <> 0) and (xAltura <> NULL)) then
          begin

            xIMC := xPeso / (xAltura*xAltura);

            try
            with dmDeca.cdsSel_ProcuraIMC do
            begin
              Close;
              Params.ParamByName('@pe_id_indicebiometrico').Value := Null;
              Params.ParamByName('@pe_ind_sexo').AsString := Copy(cbSexoCrianca.Text,1,1);
              Params.ParamByName('@pe_num_idade').AsInteger := StrToInt(Copy(lbIdade.Caption,1,2));
              Params.ParamByName('@pe_num_valorIMC').AsFloat := xIMC;
              Open;

              //label19.Caption := 'IMC:  ' + Format('%2.1f', [xIMC]);
              //label19.Caption := label19.Caption + ' - ' + dmDeca.cdsSel_ProcuraIMC.FieldByName('dsc_classificacao').Value

            end;
            except end;
          end
          else
          begin
            //label19.Caption := 'IMC N�o Dispon�vel...';
          end;

          txtEtnia.Text                         := dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_etnia').AsString;
          txtCalcado.Text                       := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_calcado').AsString;
          txtCalca.Text                         := dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_tam_calca').AsString;
          txtCamisa.Text                        := dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_tam_camisa').AsString;
          txtCotas.Text                         := FloatToStr(dmDeca.cdsSel_Cadastro_Move.FieldByname('num_cota_passes').AsFloat);
          txtPontuacao.Text                     := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByname('ind_pontuacao_triagem').AsInteger);
          rgModalidade.ItemIndex                := dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_modalidade').AsInteger;
          chkProjetoVida.Checked                := dmDeca.cdsSel_Cadastro_Move.FieldByName('flg_projeto_vida').AsBoolean;
          txtSaude.Text                         := dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_saude').AsString;

          cbTipoSangue.ItemIndex                := cbTipoSangue.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_tipo_sanguineo').AsString));

          rgBolsaFamilia.ItemIndex              := dmDeca.cdsSel_Cadastro_Move.FieldByName ('possui_bolsa_familia').Value;
          txtNIS.Text                           := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_nis').Value;
          rgAtivExterna.ItemIndex               := dmDeca.cdsSel_Cadastro_Move.FieldByName ('participa_ativ_externa').Value;
          txtATIVIDADE_EXTERNA.Text             := dmDeca.cdsSel_Cadastro_Move.FieldByName ('ativ_ext_quais').Value;
          txtCorOlhos.Text                      := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_cor_olhos').Value;
          txtCorCabelos.Text                    := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_cor_cabelos').Value;
          cbFumante.ItemIndex                   := dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_fumante').Value;
          cbPortadorNecessidades.ItemIndex      := dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_portador_necess_especiais').Value;
          edNecessidadesEspeciais.Text          := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_necessidades_esp_quais').Value;

          // Carregar os documentos novos e os reativados nos respectivos campos e abas...
          // Validar o preenchimento dos documentos e demais campos, para evitar estourar e n�o permitir transferir os valores
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_rg').IsNull)                   then edNumRG.Text := '00.000.000-0'         else edNumRG.Text                   := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_rg').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_rg_emissor').IsNull)           then cbOrgaoEmissorRG.ItemIndex := 32       else cbOrgaoEmissorRG.ItemIndex     := cbOrgaoEmissorRG.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_rg_emissor').AsString));
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_emissao_rg').IsNull)           then mskRGDataEmissao.Text := ''            else mskRGDataEmissao.Text          := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_emissao_rg').AsDateTime);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_uf_rg').IsNull)                then cbRGUf.ItemIndex := 27                 else cbRGUf.ItemIndex               := cbRGUf.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_uf_rg').AsString));
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cpf').IsNull)                  then mskNumCPF.Text := '000.000.000-00'     else mskNumCPF.Text                 := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cpf').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_certidao').IsNull)             then edDadosCertidaoNascimento.Text := '--' else edDadosCertidaoNascimento.Text := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_certidao').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_local_nascimento').IsNull)     then edLocalNascimento.Text := '--'         else edLocalNascimento.Text         := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_local_nascimento').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_titulo_eleitor').IsNull)       then edNumTituloEleitor.Text := '--'        else edNumTituloEleitor.Text        := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_titulo_eleitor').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_zona_eleitoral').IsNull)       then edZonaEleitoral.Text := '--'           else edZonaEleitoral.Text           := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_zona_eleitoral').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao_eleitoral').IsNull)      then edSecaoEleitoral.Text := '--'          else edSecaoEleitoral.Text          := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao_eleitoral').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_num').IsNull)                 then edNumCTPS.Text := '--'                 else edNumCTPS.Text                 := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_num').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_serie').IsNull)               then edCTPSSerie.Text := '--'               else edCTPSSerie.Text               := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_serie').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_emissao_ctps').IsNull)         then mskCTPSDataEmissao.Text := ''          else mskCTPSDataEmissao.Text        := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_emissao_ctps').AsDateTime);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_serie').IsNull)               then edCTPSSerie.Text := '--'               else edCTPSSerie.Text               := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_serie').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_uf_ctps').IsNull)              then cbCTPSUf.ItemIndex := 27               else cbCTPSUf.ItemIndex             := cbCTPSUf.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_uf_ctps').AsString));
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_pis').IsNull)                  then edNumPIS.Text := '0'                   else edNumPis.Text                  := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_pis').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_emissao_pis').IsNull)          then mskRGDataEmissao.Text := ''            else mskPISDataEmissao.Text         := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_emissao_pis').AsDateTime);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_banco_pis').IsNull)            then edPisBanco.Text := '--'                else edPISBanco.Text                := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_banco_pis').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_reservista').IsNull)           then edReservistaNumero.Text := '--'        else edReservistaNumero.Text        := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_reservista').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_reservista_csm').IsNull)       then edReservistaCSM.Text := '--'           else edReservistaCSM.Text           := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_reservista_csm').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cartao_sus').IsNull)           then edNumCartaoSUS.Text := '--'            else edNumCartaoSUS.Text            := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cartao_sus').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cartaosias').IsNull)           then edNumCartaoSIAS.Text := '--'           else edNumCartaoSIAS.Text           := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cartaosias').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_telefone_celular').IsNull)     then mskTelefoneCelular.Text := '--'        else mskTelefoneCelular.Text        := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_telefone_celular').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_grau_instrucao').IsNull)       then cbGrauInstrucao.ItemIndex := 5         else cbGrauInstrucao.ItemIndex      := dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_grau_instrucao').Value;
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_email').IsNull)                then edEmail.Text := '--'                   else edEmail.Text                   := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_email').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_estado_civil').IsNull)         then cbEstadoCivil.ItemIndex := 5           else cbEstadoCivil.ItemIndex        := dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_estado_civil').Value;
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_nacionalidade').IsNull)        then edNacionalidade.Text := '--'           else edNacionalidade.Text           := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_nacionalidade').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('ind_guarda_regularizada').IsNull)  then cbGuardaRegular.ItemIndex := 2         else cbGuardaRegular.ItemIndex      := dmDeca.cdsSel_Cadastro_Move.FieldByName('ind_guarda_regularizada').Value;
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_municipio_residencia').IsNull) then edMunicipioResidencia.Text := '--'     else edMunicipioResidencia.Text     := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_municipio_residencia').AsString);
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_uf_residencia').IsNull)        then cbUFResidencia.ItemIndex := 27         else cbUFResidencia.ItemIndex       := cbUFResidencia.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_uf_residencia').AsString));
          if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_nom_conjuge').IsNull)          then edNomeConjuge.Text := '--'             else edNomeConjuge.Text             := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_nom_conjuge').AsString);


          cbTurma.ItemIndex := cbTurma.Items.IndexOf(Trim(FieldByName('nom_turma').AsString));
          //Carrega o nome da empresa e da Assistente Social Respons�vel
          //vListaEmpresa2.Find (FieldByName ('nom_empresa').AsString, vIndEmpresa);
          //cbEmpresa.ItemIndex := cbEmpresa.Items.IndexOf(Trim(FieldByName('nom_empresa').AsString));
          //cbEmpresa.ItemIndex := vIndEmpresa;;

          //edAssistenteSocial.Text := FieldByname('nom_as_social').AsString;

          //Exibe a Fotografia da crian�a
          vv_JPG := nil;
          try
            with dmDeca.cdsSel_Cadastro_Foto do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
              Open;

              b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

              if (b.Size > 0) then
              begin
                try
                  vv_JPG := TJPEGImage.Create;
                  vv_JPG.LoadFromStream (b);
                  Fotografia.Picture.Assign (vv_JPG);
                  Fotografia2.Picture.Assign (vv_JPG);
                except end
              end
              else
              begin
                Fotografia.Picture.Assign (nil);
                Fotografia2.Picture.Assign (nil);
              end;
              
              vv_JPG.Free;
              b.Destroy;

            end
          except end;

          //Carregar s imagem em Defasagem de acordo com o valor do campo no cadastro...
          //Defasagem ESCOLAR
          if dmDeca.cdsSel_Cadastro_Move.FieldByName ('flg_defasagem_escolar').Value = 0 then
          begin
            //Cadastro
            imgDefasagemEscolarNAO.Visible := True;
            imgDefasagemEscolarSIM.Visible := False;
            imgDefasagemEscolarNENHUMA.Visible := False;

            //Escolaridade
            imgDefasagemEscNAO.Visible := True;
            imgDefasagemEscSIM.Visible := False;
            imgDefasagemEscNENHUMA.Visible := False

          end
          else if dmDeca.cdsSel_Cadastro_Move.FieldByName ('flg_defasagem_escolar').Value = 1 then
          begin
            //Cadastro
            imgDefasagemEscolarNAO.Visible := False;
            imgDefasagemEscolarSIM.Visible := True;
            imgDefasagemEscolarNENHUMA.Visible := False;

            //Escolaridade
            imgDefasagemEscNAO.Visible := False;
            imgDefasagemEscSIM.Visible := True;
            imgDefasagemEscNENHUMA.Visible := False
          end
          else
          begin
            //Cadastro
            imgDefasagemEscolarNAO.Visible := False;
            imgDefasagemEscolarSIM.Visible := False;
            imgDefasagemEscolarNENHUMA.Visible := True;

            //Aba Escolaridade
            imgDefasagemEscNAO.Visible := False;
            imgDefasagemEscSIM.Visible := False;
            imgDefasagemEscNENHUMA.Visible := True
          end;

          //Defasagem de APRENDIZAGEM
          if dmDeca.cdsSel_Cadastro_Move.FieldByName ('flg_defasagem_aprendizagem').Value = 0 then
          begin
            imgDefasagemAprendizagemNAO.Visible := True;
            imgDefasagemAprendizagemSIM.Visible := False;
            imgDefasagemAprendizagemNENHUMA.Visible := False
          end
          else if dmDeca.cdsSel_Cadastro_Move.FieldByName ('flg_defasagem_aprendizagem').Value = 1 then
          begin
            imgDefasagemAprendizagemNAO.Visible := False;
            imgDefasagemAprendizagemSIM.Visible := True;
            imgDefasagemAprendizagemNENHUMA.Visible := False
          end
          else
          begin
            imgDefasagemAprendizagemNAO.Visible := False;
            imgDefasagemAprendizagemSIM.Visible := False;
            imgDefasagemAprendizagemNENHUMA.Visible := True
          end;


          {
          //Verificar se o Aluno est� com ou sem escola...
          //Pesquisa no Hist�rico_Escola para recuperar a Escola e S�rie atuais
          with dmDeca.cdsSel_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_escola').Value     := 209; //Null;
            Params.ParamByName ('@pe_num_ano_letivo').Value := Copy(DateToStr(Date()),7,4);
            Open;

            dmDeca.cdsSel_HistoricoEscola.Last;

            if (dmDeca.cdsSel_HistoricoEscola.RecordCount > 0) then
            begin

              if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value = 'SEM ESCOLA') then
                GroupBox22.Visible := True
              else
               GroupBox22.Visible := False;
            end
            else
            begin
              //Application.MessageBox ('Aten��o!!!' + #13+#10 +
              //                        'N�o foram encontrados registros referentes a Acompanhamento Escolar lan�ados para o ano atual.',
              //                        '[Sistema Deca] - Aviso do Sistema',
              //                        MB_OK + MB_ICONINFORMATION);
            end;

          end;}

          Label32.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('vSemEscola').Value;
          
        end

      //Close; -- o dataset est� sendo mantido aberto para quando entrar
      //          na p�gina Escolaridade
    end;
    except end;
  end;

end;

procedure TfrmFichaCrianca.PageControl1Change(Sender: TObject);
begin
  FStatus:=fdNone;

  // S� muda de p�gina se tiver uma crian�a selecionada na p�gina principal
  if Length(Trim(txtMatricula.Text)) = 0 then
  begin

    PageControl1.ActivePageIndex := 0;

  end
  else
  begin

    // Familiares
    if PageControl1.ActivePage.Name = 'TabSheet2' then
    begin
      // carrega o nome no topo da tela
      label4.Caption := txtMatricula.Text + ' - ' + txtNome.Text;
      //label4.Caption := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;

      // carrega o grid
      try
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);//dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;//
          Params.ParamByName('@pe_cod_familiar').Value := Null;
          Open;
          dbgFamiliares.Refresh;
        end;
      except end;

      // atualiza a tela
      FillFormFamiliares;

      // atualiza renda familiar (no canto superior da tela) e o total de integrantes
      AtualizaRendaFamiliar;
    end

    //Escolaridade
    else If PageControl1.ActivePage.Name = 'TabSheet3' then
    begin
      FillFormEscolaridade;
      if (vvIND_PERFIL IN [1,18,19]) then PageControl2.Pages[4].Enabled := True else PageControl2.Pages[4].Enabled := False;
      PageControl2.ActivePageIndex := 0;
    end

    // Programas Sociais
    else if PageControl1.ActivePage.Name = 'TabSheet4' then
    begin
      // carrega o nome no topo da tela
      label7.Caption := txtMatricula.Text + ' - ' + txtNome.Text;

      //Atualiza o grid de familiares
      // carrega o grid
      try
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;//txtMatricula.Text;
          Params.ParamByName('@pe_cod_familiar').Value := Null;
          Open;
          dbgFamiliaresProgSociais.Refresh;
        end;
      except end;

      // carrega o grid
      try
        with dmDeca.cdsSel_Cadastro_Programa_Social do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_cad_progsocial').Value := Null;
          Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;//GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_flg_status').Value := Null;
          Params.ParamByName('@pe_data_insercao1').Value := Null;
          Params.ParamByName('@pe_data_insercao2').Value := Null;
          Params.ParamByName('@pe_data_desligamento1').Value := Null;
          Params.ParamByName('@pe_data_desligamento2').Value := Null;
          Open;
          dbgProgramas.Refresh;
        end;

      except end;

      // atualiza a tela
      FillFormProgramasSociais;
    end

    // Atendimentos na Rede
    else if PageControl1.ActivePage.Name = 'TabSheet5' then
    begin
      // carrega o nome no topo da tela
      label9.Caption := txtMatricula.Text + ' - ' + txtNome.Text;
      //label9.Caption := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;

      // carrega o grid
      try
        with dmDeca.cdsSel_Cadastro_Atendimento_Rede do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_atendimento').Value := Null;
          Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;//txtMatricula.Text;
          Open;
          dbgAtendimentos.Refresh;
        end;
      except end;

      // atualiza a tela
      FillFormAtendimentos;
    end

    //Historico
    else if PageControl1.ActivePage.Name = 'TabSheet6' then
    begin
      // carrega o nome no topo da tela
      lblNome.Caption := txtMatricula.Text + ' - ' + txtNome.Text; //dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;;
      meDescricaoOcorrencia.Lines.Clear;

      try
        with dmDeca.cdsSel_Historico do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := Trim(txtMatricula.Text);//dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
            //Params.ParamByName('@pe_cod_unidade').AsInteger := Null;
            Open;
            dbgHistorico.Refresh;
          end;
        except end;
    end

    else if PageControl1.ActivePage.Name = 'TabSheet7' then
    begin
      // carrega o nome no topo da tela
      label2.Caption := txtMatricula.Text + ' - ' + txtNome.Text;
      TabSheet7.Enabled := True;

      try
        with dmDeca.cdsSel_Acompanhamento do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
            Params.ParamByName('@pe_cod_unidade').Value := Null;
            Params.ParamByName('@pe_num_bimestre').Value := Null;
            Params.ParamByName('@pe_num_ano').Value := Null;
            Params.ParamByName('@pe_flg_aproveitamento').Value := Null;
            Params.ParamByName('@pe_cod_escola').Value := Null;

            Open;
            dbgAcompanhamento.Refresh;
          end;
        except end;

    end

    else if PageControl1.ActivePage.Name = 'TabSheet14' then
    begin
      
      Label39.Caption := Trim(txtMatricula.Text) + ' - ' + Trim(txtNome.Text);
      PageControl4.ActivePageIndex           := 0;
      GroupBox30.Enabled                     := False;
      GroupBox31.Enabled                     := False;
      GroupBox32.Enabled                     := False;
      GroupBox33.Enabled                     := False;
      GroupBox29.Enabled                     := False;
      GroupBox34.Enabled                     := False;
      GroupBox51.Enabled                     := False;
      GroupBox52.Enabled                     := False;
      GroupBox36.Enabled                     := False;
      GroupBox37.Enabled                     := False;
      GroupBox38.Enabled                     := False;
      GroupBox39.Enabled                     := False;
      GroupBox40.Enabled                     := False;
      GroupBox41.Enabled                     := False;
      GroupBox42.Enabled                     := False;
      GroupBox43.Enabled                     := False;
      GroupBox44.Enabled                     := False;
      GroupBox45.Enabled                     := False;
      GroupBox46.Enabled                     := False;
      GroupBox47.Enabled                     := False;
      GroupBox48.Enabled                     := False;
      GroupBox49.Enabled                     := False;
      GroupBox50.Enabled                     := False;
      GroupBox65.Enabled                     := False;
      GroupBox59.Enabled                     := False;
      GroupBox60.Enabled                     := False;
      GroupBox61.Enabled                     := False;
      GroupBox62.Enabled                     := False;
      GroupBox63.Enabled                     := False;
      GroupBox67.Enabled                     := False;
      GroupBox68.Enabled                     := False;

      btnAlteraDadosDocumentacao.Enabled     := True;
      btnGravaDadosDocumentacao.Enabled      := False;
      btnCancelarAlteracaoDocumentos.Enabled := False;
      btnSairDocumentacao.Enabled            := True;

    end;
    end;

end;

procedure TfrmFichaCrianca.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //dmDeca.cdsSel_Cadastro_Move.Close;
  //dmDeca.cdsSel_Escola.Close;
  Action := caFree;

  vListaEscola1.Free;
  vListaEscola2.Free;

  vListaSerie1.Free;
  vListaSerie2.Free;


end;

procedure TfrmFichaCrianca.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if Key = #27 then
  begin
    Key := #0;
    frmFichaCrianca.Close;
  end;

end;

procedure TfrmFichaCrianca.btnSairClick(Sender: TObject);
begin
  frmFichaCrianca.Close;
end;


procedure TfrmFichaCrianca.LimpaEdits;
var
  i: Integer;

begin
  for i := 0 to frmFichaCrianca.ComponentCount - 1 do
  begin
    if (frmFichaCrianca.Components[i] is TCustomEdit) then
      (frmFichaCrianca.Components[i] as TCustomEdit).Clear;
end;
end;

procedure TfrmFichaCrianca.txtMatriculaExit(Sender: TObject);
begin

  if Length(Trim(txtMatricula.Text)) > 0 then
    if not ValidaMatricula() then
    begin
      txtMatricula.SetFocus;
      ShowMessage('Os tr�s primeiros d�gitos devem ser:'+#13+#10+
                  '777 para admiss�es novas feitas na Triagem'+#13+#10+
                  '002 ou 999 - Crian�a'+#13+#10+
                  '003 ou 006 - Adolescente'+#13+#10+#13+#10+
                  'A matr�cula deve ter 8 caracteres');
    end

    else
      begin
        if Copy(txtMatricula.Text,1,3) = '999' then rgModalidade.ItemIndex := 1
        else rgModalidade.ItemIndex := 0;
      end;

end;

procedure TfrmFichaCrianca.txtNomeExit(Sender: TObject);
begin
  // testa se foi preenchido
  If Length(txtNome.Text) = 0 then
  begin
    ShowMessage('O Nome deve ser preenchido');
    txtNome.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.txtAdmissaoExit(Sender: TObject);
begin
  // testa se a data � v�lida
  try
    StrToDate(txtAdmissao.Text);
  except
    on E : EConvertError do
    begin
      ShowMessage('Data Inv�lida');
      txtAdmissao.SetFocus;
    end;
  end;
end;

procedure TfrmFichaCrianca.txtEnderecoExit(Sender: TObject);
begin
  // testa se foi preenchido
  If Length(txtEndereco.Text) = 0 then
  begin
    ShowMessage('O endere�o deve ser preenchido');
    txtEndereco.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.txtCepExit(Sender: TObject);
begin
  // testa se foi preenchido
  If txtCep.Text = '     -   ' then
  begin
    ShowMessage('O CEP deve ser preenchido');
    txtCep.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.txtNascimentoExit(Sender: TObject);
var
  vOk : boolean;

begin
  // testa se a data � v�lida
  vOk := true;
  try
    StrToDate(txtNascimento.Text);
  except
    on E : EConvertError do
    begin
      ShowMessage('Data Inv�lida');
      txtNascimento.SetFocus;
      vOk := false;
    end;
  end;
  // exibe a idade
  if vOk then
    //lbIdade.Caption := CalculaIdade(StrToDate(txtNascimento.Text));
    lbIdade.Caption := Idade2(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_nascimento').AsDateTime);

end;

procedure TfrmFichaCrianca.cbSexoCriancaExit(Sender: TObject);
begin
  // testa se o campo foi selecionado
  if cbSexoCrianca.ItemIndex = -1 then
  begin
    ShowMessage('Este campo deve ser selecionado');
    cbSexoCrianca.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.cbEscolaExit(Sender: TObject);
var vIndEscola : Integer;
begin
   // testa se o campo foi selecionado
  if (cbEscolaE.ItemIndex = -1) then
  begin
    ShowMessage('Este campo deve ser selecionado');
    cbEscolaE.SetFocus;
  end
  else
  begin
    vListaEscola2.Find(cbEscolaE.Text, vIndEscola);
    mmCOD_ESCOLA_ATUAL := vIndEscola;

    if (mmCOD_ESCOLA_ANTES = mmCOD_ESCOLA_ATUAL) or (mmCOD_ESCOLA_ANTES = Null) then
      vvMUDOU_ESCOLA := False
    else
      vvMUDOU_ESCOLA := True
  end;

  //if (cbEscolaE.Text = 'SEM ESCOLA') or (cbEscolaE.Text = 'CONCLUINTE ENSINO MEDIO') or (cbEscolaE.Text = 'NENHUMA DAS ANTERIORES') then
  if (cbEscolaE.Text = 'NENHUMA DAS ANTERIORES') then
  begin
    cbSerieE.ItemIndex          := 0;
    GroupBoxE5.Enabled          := False;
    cbAproveitamentoE.ItemIndex := 3;
    GroupBoxE8.Enabled          := False;
    cbFrequenciaE.ItemIndex     := 2;
    GroupBoxE6.Enabled          := False;
    cbSituacaoE.ItemIndex       := 10;
    GroupBoxE9.Enabled          := True;
    cbSituacaoE.SetFocus;
    cbPeriodoEscolaE.ItemIndex  := 4;
    GroupBoxE10.Enabled          := False;
    edTurmaEscola.Text          := '-';
  end;

  if (cbEscolaE.Text = 'SEM ESCOLA') then
  begin
    cbSerieE.ItemIndex          := 0;
    GroupBoxE5.Enabled          := False;
    cbAproveitamentoE.ItemIndex := 3;
    GroupBoxE8.Enabled          := False;
    cbFrequenciaE.ItemIndex     := 2;
    GroupBoxE6.Enabled          := False;
    cbSituacaoE.ItemIndex       := 7;
    GroupBoxE9.Enabled          := False;
    cbPeriodoEscolaE.ItemIndex  := 4;
    GroupBoxE10.Enabled          := False;
    edTurmaEscola.Text          := '-';
  end;

  if (cbEscolaE.Text = 'CONCLUINTE ENSINO MEDIO') then
  begin
    cbSerieE.ItemIndex          := 0;
    GroupBoxE5.Enabled          := False;
    cbAproveitamentoE.ItemIndex := 3;
    GroupBoxE8.Enabled          := False;
    cbFrequenciaE.ItemIndex     := 2;
    GroupBoxE6.Enabled          := False;
    cbSituacaoE.ItemIndex       := 4;
    GroupBoxE9.Enabled          := False;
    cbPeriodoEscolaE.ItemIndex  := 4;
    GroupBoxE10.Enabled          := False;
    edTurmaEscola.Text          := '-';
  end;

end;

procedure TfrmFichaCrianca.cbSerieExit(Sender: TObject);
begin
  // testa se o campo foi selecionado
  if cbSerieE.ItemIndex = -1 then
  begin
    ShowMessage('Este campo deve ser selecionado');
    cbSerieE.SetFocus;
  end;

  //Se escolaridade for
  // - Telessala Ensino Medio
  // - Telessala Ensino Fundmental
  // atribuir valor 3 (Frequencia Flexivel) a Frequ�ncia Escolar
  if (vvCOD_SERIE = 31) OR (vvCOD_SERIE = 32) then
    cbFrequenciaE.ItemIndex := 3;

end;

{  Calcula Idade e retorna por extenso - incluido anos e meses}
Function TfrmFichaCrianca.CalculaIdade( Nascimento : TDateTime ) : String ;
var
  Periodo : Integer;
  intContAnos, intContMeses : Integer ;
  DataFinal : TDateTime;

begin

    // calculo de anos
    intContAnos := 0 ;
    Periodo := 12 ;
    DataFinal := Date() ;
    Repeat
      Inc(intContAnos) ;
      DataFinal := IncMonth(DataFinal,Periodo * -1) ;
    Until DataFinal < Nascimento ;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContAnos,-1) ;

    // calculo de meses
    intContMeses := 0 ;
    Periodo := 1 ;
    DataFinal := Date() ;
    Repeat
      Inc(intContMeses) ;
      DataFinal := IncMonth(DataFinal,Periodo * -1) ;
    Until DataFinal < Nascimento;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContMeses,-1) ;
    intContMeses := intContMeses mod 12;

    if intContAnos <= 9 then
      Result := '0' + IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses'
    else
      Result := IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses';

End ;


procedure TfrmFichaCrianca.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  //Desabilitar os campos referentes aos dados de endere�o
  GroupBoxE.Enabled := False;
  GroupBoxF.Enabled := False;
  GroupBoxG.Enabled := False;
  GroupBoxH.Enabled := False;
  GroupBoxU.Enabled := False;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);
  end;

end;

procedure TfrmFichaCrianca.AtualizaCamposBotoes;
begin
  if (FStatus = FdInsert) or (FStatus = FdEdit) then
  begin
    btnPrimeiro.Enabled := False;
    btnAnterior.Enabled := False;
    btnProximo.Enabled := False;
    btnUltimo.Enabled := False;

    btnNovo.Enabled := False;
    btnAlterar.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSelecionaFoto.Enabled := True;

    btnImprimir.Enabled := False;
    btnPesquisar.Enabled := False;

    // torna visivel o bot�o de copiar ficha
    if FStatus = FdInsert then
      btnCopiarFicha.Visible := True;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBoxA.Enabled := False;
    GroupBoxB.Enabled := True;
    GroupBoxC.Enabled := True;
    GroupBoxD.Enabled := True;
    GroupBoxE.Enabled := True;
    GroupBoxF.Enabled := True;
    GroupBoxG.Enabled := True;
    GroupBoxH.Enabled := True;
    GroupBoxI.Enabled := True;
    GroupBoxJ.Enabled := True;
    GroupBoxK.Enabled := True;
    GroupBoxL.Enabled := False;
    GroupBoxM.Enabled := True;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    Panel1.Enabled := True;
    //cbEmpresa.ItemIndex := -1;
    GroupBoxZ.Enabled := True;
    Fotografia.Enabled := True;

    GroupBox23.Enabled := False;
    rgBolsaFamilia.Enabled := True;
    rgAtivExterna.Enabled  := True;
    txtNIS.Enabled := True;
    txtATIVIDADE_EXTERNA.Enabled := True;

    //Se a unidade n�o for Conv�nio ou Bolsa de Estudos, CTA, Pa�o ou Administrador, desabilita o GroupBox da Se��o
    //if (vvCOD_UNIDADE = 44) or (vvCOD_UNIDADE=0) or (vvCOD_UNIDADE = 27) or (vvCOD_UNIDADE=31) or (vvCOD_UNIDADE = 46) then
      GroupBox3.Visible := True;
    //else
    //  GroupBox3.Visible := False;

    //29/out/2014
    //Desabilitar o campo PER�ODO FUNDHAS quando perfil diferente de Acompanhamento Escolar
    //if (vvIND_PERFIL in [9,18,19]) or ( (vvIND_PERFIL in [2,3]) and (vvCOD_UNIDADE = 40) ) then
    //if ((vvIND_PERFIL in [9,18,19]) and ((cbPeriodo.Text = 'MANH�') or (cbPeriodo.Text = 'TARDE') or (cbPeriodo.Text = 'INTEGR/ESC') or (cbPeriodo.Text = 'NOITE') or (cbPeriodo.Text = '<ND>')))
    //begin
    //  GroupBoxN.Enabled := True;
    //  cbPeriodo.Enabled := True;
    //end
      // //cbPeriodo.Enabled := True;
    //else
    //begin
    //  GroupBoxN.Enabled := False;
    //  cbPeriodo.Enabled := False;
    //end;
    {
    //Reativar o acesso ao campo per�odo apenas para o Pergil Gestor/Triagem
    if ( (vvIND_PERFIL in [2,3]) and (vvCOD_UNIDADE = 40) ) then
      GroupBoxN.Enabled := True
    //cbPeriodo.Enabled := True;
      else GroupBoxN.Enabled := False;
    }

    if ((vvIND_PERFIL in [9,18,19]) and ((cbPeriodo.Text = 'MANH�') or (cbPeriodo.Text = 'TARDE') or (cbPeriodo.Text = 'INTEGR/ESC') or (cbPeriodo.Text = 'NOITE') or (cbPeriodo.Text = '<ND>') or (cbPeriodo.ItemIndex = -1)))
        or ((vvIND_PERFIL in [2,3]) and (vvCOD_UNIDADE = 40)) then
    begin
      GroupBoxN.Enabled := True;
      cbPeriodo.Enabled := True;
    end
      //cbPeriodo.Enabled := True;
    else
    begin
      GroupBoxN.Enabled := False;
      cbPeriodo.Enabled := False;
    end;

    //Habilita os campos reativados a partir de 07/03/2017
    GroupBoxQ.Enabled   := True; //Etnia
    GroupBoxX.Enabled   := True; //Sangue
    GroupBoxO.Enabled   := True; //Peso
    GroupBoxP.Enabled   := True; //Altura
    GroupBox55.Enabled  := True; //Olhos
    GroupBox56.Enabled  := True; //Cabelos
    GroupBox53.Enabled  := True; //Fumante
    GroupBox54.Enabled  := True; //Portador Necessidades Especiais

  end
  else
  begin
    btnPrimeiro.Enabled := True;
    btnAnterior.Enabled := True;
    btnProximo.Enabled := True;
    btnUltimo.Enabled := True;

    btnNovo.Enabled := True;
    btnAlterar.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSelecionaFoto.Enabled := False;

    //Fotografia.Picture := Nil;;

    btnImprimir.Enabled := True;
    btnPesquisar.Enabled := True;

    // torna invisivel o bot�o de copiar ficha
    btnCopiarFicha.Visible := False;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBoxA.Enabled := False;
    GroupBoxB.Enabled := False;
    GroupBoxC.Enabled := False;
    GroupBoxD.Enabled := False;
    GroupBoxE.Enabled := False;
    GroupBoxF.Enabled := False;
    GroupBoxG.Enabled := False;
    GroupBoxH.Enabled := False;
    GroupBoxI.Enabled := False;
    GroupBoxJ.Enabled := False;
    GroupBoxK.Enabled := False;
    GroupBoxL.Enabled := False;
    GroupBoxM.Enabled := False;
    Panel1.Enabled := False;
    GroupBoxZ.Enabled := False;
    GroupBox2.Enabled := False;
    Fotografia.Enabled := False;
    //cbEmpresa.ItemIndex := -1;
    GroupBox3.Enabled := False;
    //Se a unidade n�o for Conv�nio ou FUNDHAS, desabilita o GroupBox da Empresa
    //if (vvCOD_UNIDADE = 44) or (vvCOD_UNIDADE=0) or (vvCOD_UNIDADE = 27) or (vvCOD_UNIDADE=31) or (vvCOD_UNIDADE = 46) then
    GroupBox3.Visible := True;
    //else
    //  GroupBox3.Visible := False;
    rgBolsaFamilia.Enabled := False;
    txtNIS.Enabled := False;
    rgAtivExterna.Enabled := False;
    txtATIVIDADE_EXTERNA.Enabled := False;

    //Habilita os campos reativados a partir de 07/03/2017
    GroupBoxQ.Enabled   := False; //Etnia
    GroupBoxX.Enabled   := False; //Sangue
    GroupBoxO.Enabled   := False; //Peso
    GroupBoxP.Enabled   := False; //Altura
    GroupBox55.Enabled  := False; //Olhos
    GroupBox56.Enabled  := False; //Cabelos
    GroupBox53.Enabled  := False; //Fumante
    GroupBox54.Enabled  := False; //Portador Necessidades Especiais

  end;

end;

procedure TfrmFichaCrianca.Button1Click(Sender: TObject);
begin
  FStatus := FdNone;
  AtualizaCamposBotoes;
end;

function TfrmFichaCrianca.ValidaDados: boolean;
var
  vOk : boolean;

begin
  // valida os dados para INSERT e UPDATE
  vOk := ValidaMatricula;
  if not vOk then
    txtMatricula.SetFocus;

  if vOk then
  begin
    vOk := Length(txtNome.Text) <> 0;
    if not vOk then
      txtNome.SetFocus;
  end;

  if vOk then
  begin
    vOk := Trim(txtAdmissao.Text) <> '/  /';
    if not vOk then
      txtAdmissao.SetFocus;
  end;

  if vOk then
  begin
    vOk := Trim(txtNascimento.Text) <> '/  /';
    if not vOk then
      txtNascimento.SetFocus;
  end;

  if vOk then
  begin
    vOk := cbSexoCrianca.ItemIndex >= 0;
    if not vOk then
      cbSexoCrianca.SetFocus;
  end;

  if vOk then
  begin
    vOk := rgAtivExterna.ItemIndex >= 0;
    if not vOk then
      rgAtivExterna.SetFocus;
  end;

  if vOk then
  begin
    vOk := rgBolsaFamilia.ItemIndex >= 0;
    if not vOk then
      rgAtivExterna.SetFocus;
  end;

  {if vOk then
  begin
    vOk := Length(txtCertidaoNascimento.Text) <> 0;
    if not vOk then
      txtCertidaoNascimento.SetFocus;
  end;
  }

  // prepara o retorno
  ValidaDados := vOk;

end;

function TfrmFichaCrianca.ValidaMatricula: boolean;
var
  vResult : boolean;

begin
  vResult := true;

  // codifica��o automatica para as matriculas iniciadas com 999
  if not ((Length(Trim(txtMatricula.Text)) = 3) and
     (Copy(txtMatricula.Text,0,3) = '999') or
     (Copy(txtMatricula.Text,0,3) = '777')) then
  begin

    // testa o tamanho - 8 caracteres
    if (Length(Trim(txtMatricula.Text)) < 8) then
      vResult := false

    // verifica se os tr�s primeiros caracteres s�o v�lidos
    else if (Copy(txtMatricula.Text,0,3) <> '002') and
            (Copy(txtMatricula.Text,0,3) <> '999') and
            (Copy(txtMatricula.Text,0,3) <> '003') and
            (Copy(txtMatricula.Text,0,3) <> '006') and
            (Copy(txtMatricula.Text,0,3) <> '777') then
      vResult := false;
  end;

  Result := vResult;

end;

procedure TfrmFichaCrianca.btnAlterar3Click(Sender: TObject);
begin
  FStatus:= FdEdit;
  AtualizaCamposBotoes3;
  cbEscola.SetFocus;
end;

procedure TfrmFichaCrianca.AtualizaCamposBotoes3;
begin

  if (FStatus = FdInsert) or (FStatus = FdEdit) then
  begin
    btnAlterar3.Enabled := False;
    btnSalvar3.Enabled := True;
    btnCancelar3.Enabled := True;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox3A.Enabled := True;
    GroupBox3B.Enabled := True;
    GroupBox3C.Enabled := True;
    GroupBox3D.Enabled := True;
    GroupBox3E.Enabled := True;
    GroupBox3F.Enabled := True;
    GroupBox3G.Enabled := True;
    GroupBox3H.Enabled := True;

  end
  else
  begin
    btnAlterar3.Enabled := True;
    btnSalvar3.Enabled := False;
    btnCancelar3.Enabled := False;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox3A.Enabled := False;
    GroupBox3B.Enabled := False;
    GroupBox3C.Enabled := False;
    GroupBox3D.Enabled := False;
    GroupBox3E.Enabled := False;
    GroupBox3F.Enabled := False;
    GroupBox3G.Enabled := False;
    GroupBox3H.Enabled := False;
  end;

end;

procedure TfrmFichaCrianca.btnSalvar3Click(Sender: TObject);
var
  vEscola : Integer;
begin
  // valida os dados
  if (cbEscola.ItemIndex >= 0) and (cbSerie.ItemIndex >= 0) then
  begin

    //Verificar se o campo escola n�o foi alterado
    //Se foi alterado, faz-se a inclus�o dos dados atuais no Historico_Escola e depois promove-se a altera��o no cadastro
    //if (dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_escola').Value <> vListaEscola1.Strings[cbEscola.ItemIndex]) then
    //begin

      if (FStatus = FdEdit) then
      begin
        try
          //if dmDeca.cdsSel_Escola.Locate('nom_escola', cbEscola.Items[cbEscola.ItemIndex], [loCaseInsensitive]) then
          //   vEscola := dmDeca.cdsSel_Escola.FieldByName('cod_escola').AsInteger;

          //Criar rotina para que antes de alterar o status do registro de escola, alterar apenas a data de altera��o do ultimo registro
          //e depois sim, alterar o status da escola atual para a anterior

          //********************************************  


          //Alterar todas as escolas da matricula atual para "Escolas Anteriores"
          with dmDeca.cdsAlt_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);//dmDeca.cdsSel_HistoricoEscola.FieldByname('cod_id_hist_escola').AsInteger;
            Params.ParamByName ('@pe_flg_status').Value := 1; //1 para escolas anteriores
            Execute;
          end;

          //Incluir os dados da escola atual no HistoricoEscola
          with dmDeca.cdsInc_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_escola').Value := StrToInt(vListaEscola1.Strings[cbEscola.ItemIndex]);
            Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Params.ParamByName ('@pe_flg_status').Value := 0;  //Para "Escola Atual"
            Params.ParamByName ('@pe_dsc_serie').Value := GetValue(cbSerie.Items[cbSerie.ItemIndex]);

            //Inclu�dos na mudan�a de 26-mai-2009, os campos NUM_ANO_LETIVO e DSC_RG_ESCOLAR
            Params.ParamByName('@pe_num_ano_letivo').Value := GetValue(edAnoLetivo.Text);
            Execute;
          end;

          //Alterar os dados de escola no cadastro
          with dmDeca.cdsAlt_Cadastro_Escola do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName('@pe_cod_escola').Value := StrToInt(vListaEscola1.Strings[cbEscola.ItemIndex]);
            Params.ParamByName('@pe_dsc_escola_serie').Value := GetValue(cbSerie.Items[cbSerie.ItemIndex]);
            Params.ParamByName('@pe_dsc_escola_periodo').Value := GetValue(cbPeriodoEscola.Items[cbPeriodoEscola.ItemIndex]);
            Params.ParamByName('@pe_dsc_turma_escola').Value := GetValue(txtTurma.Text);
            Params.ParamByName('@pe_dsc_escola_obs').Value := GetValue(txtObsEscola.Text);
            //Inclu�dos na mudan�a de 26-mai-2009, os campos NUM_ANO_LETIVO e DSC_RG_ESCOLAR
            Params.ParamByName('@pe_num_ano_letivo').Value := GetValue(edAnoLetivo.Text);
            Params.ParamByName('@pe_dsc_rg_escolar').Value := GetValue(edNumRgEscolar.Text);
            Execute;
            //dmDeca.cdsSel_Cadastro_Move.Refresh;
          end;
          //dmDeca.cdsSel_Cadastro_Move.Refresh;

          //Atualiza os dados no dbGrid2 - HistoricoEscola
          with dmDeca.cdsSel_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_escola').Value := Null;
            Open;
            dbgHistoricoEscolas.Refresh;
          end;

          if Application.MessageBox('Deseja Lan�ar dados do Acompanhamento Escolar agora?',
                                    'Escolaridade - Lan�ar Acomp. Escolar',
                                    MB_ICONQUESTION + MB_YESNO ) = idYes then
          begin
            PageControl2.ActivePageIndex := 1;
            //FStatus := fdEdit;
            AtualizaTelaAcompEscolar('Novo');
          end;

        except
          ShowMessage('Aten��o!'+#13+#10+#13+#10+
                      'Os dados n�o foram alterados com sucesso');
        end;

    //end
    //else

  end;
  //Atualiza a tela
  FStatus := FdNone;
  AtualizaCamposBotoes3;
  FillFormEscolaridade;
end
else
  begin
    ShowMessage('Dados Inv�lidos!'+#13+#10+#13+#10+
                'Verifique se os campos obrigat�rio (em azul) foram preenchidos');
    cbEscola.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.btnCancelar3Click(Sender: TObject);
begin
  FStatus := FdNone;
  AtualizaCamposBotoes3;
  FillFormEscolaridade;
end;

procedure TfrmFichaCrianca.FillFormEscolaridade;
begin

  Label8.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value +
                    '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;


  if (Length(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_rg_escolar').AsString)=0) then
    edNumRgEscolarE.Text := '00000'
  else
    edNumRGEscolarE.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_rg_escolar').Value;
    lbMsgDefasagemEscolar.Caption := '';

  //Exibe na tela de escolaridade o n�mero do RG escolar da c�a/adolescente

  //A partir de Junho/2009, o preenchimento dos dados de acompanhamento/escola ser� feito em tela separada do cadastro da crian�a/adolescente
  //Sequencia de execu��o:
  //      1. Carregar os dados de ACOMPESCOLAR j� lan�ados para a Matr�cula
  //      2. Habilitar campos e bot�es iniciais

  try
    //Selecionar os dados de ACOMPESCOLAR para a matr�cula atual
    with dmDeca.cdsSel_AcompEscolar do
    begin

      Close;
      Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
      Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
      Params.ParamByName ('@pe_num_bimestre').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByname ('@pe_flg_defasagem_escolar').Value := Null;
      Open;

      dbgDadosAcompEscolar.Refresh;

    end;

  except end;

  //habilita os Campos/bot�es do formul�rio para uso
  dbgDadosAcompEscolar.Enabled := True;

  edAnoLetivoE.Clear;
  cbBimestreE.ItemIndex := -1;
  cbescolaE.ItemIndex := -1;
  cbSerieE.Clear;
  cbFrequenciaE.ItemIndex := -1;
  cbAproveitamentoE.ItemIndex := -1;
  cbSituacaoE.ItemIndex := -1;
  cbPeriodoEscolaE.ItemIndex := -1;

  btnPrimeiraFicha.Enabled := True;
  btnFichaAnterior.Enabled := True;
  btnProximaFicha.Enabled := True;
  btnUltimaFicha.Enabled := True;

  btnNovoLancamentoE.Enabled := True;
  btnAlteraLancamentoE.Enabled := False;
  btnGravaLancamentoE.Enabled := False;
  btnCancelaOperacaoE.Enabled := False;
  btnVerBoletimE.Enabled := True;
  btnLocalizarFicha.Enabled := True;
  btnSairE.Enabled := True;

  //Atualiza os dados do Hist�rico de Escolas da Matr�cula Atual
    Label26.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value +
                    '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;

    mskNascimento.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('Nascimento').Value;                    

  try
    with dmDeca.cdsSel_HistoricoEscola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Open;
      dbgHistoricoEscolas.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Ocorreu um erro ao tentar carregar os dados do Hist�rico de Escolas ' + #13+#10 +
                            'para o adolescente selecionado. Verifique a conex�o com a internet ' + #13+#10 +
                            'ou o cabo de rede. Caso deseje entre em contato com o Administrador '+ #13+#10 +
                            'do Sistema.',
                            '[Sistema Deca] - Hist�rico de Escolas',
                            MB_OK + MB_ICONWARNING);
    AtualizaTelaAcompEscolar('Padr�o');
  end;

  //Carrega os dados Educacenso
  Label33.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value + '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;

  if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_cadastro_educacenso').IsNull) then mskDataCadastro.Text := '  /  /    ' else mskDataCadastro.Text := DateToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_cadastro_educacenso').Value);
  cbTurmaCIE.ItemIndex := cbTurmaCIE.Items.IndexOf(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_turma_cei').Value);

  //if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_situacao_cei').IsNull) then mskCEI.Text := '______' else mskCEI.Text          := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_situacao_cei').Value);
  //if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_observacoes_educacenso').IsNull) then meObservacoes.Text := '<Sem anota��es>' else  meObservacoes.Text   := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_observacoes_educacenso').Value;

end;

procedure TfrmFichaCrianca.btnNovo4Click(Sender: TObject);
begin
  // perepara a tela para digita��o de um novo programa social
  FStatus := fdInsert;
  AtualizaCamposBotoes4;
  cbProgramaSocial.SetFocus;

  // limpa os campos
  cbProgramaSocial.ItemIndex := -1;

end;

procedure TfrmFichaCrianca.AtualizaCamposBotoes4;
begin

  if (FStatus = FdInsert) or (FStatus = FdEdit) then
  begin
    btnNovo4.Enabled := False;
    btnAlterar4.Enabled := False;
    btnSalvar4.Enabled := True;
    btnCancelar4.Enabled := True;
    btnDesligamento.Enabled := False;
    rgSituacao.Enabled := True;
    rgSituacao.ItemIndex := 0; //Padr�o PRIORIT�RIO

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox4A.Enabled := True;
    //GroupBox4B.Enabled := True;
    //GroupBox4C.Enabled := False;
  end
  else
  begin
    btnNovo4.Enabled := True;
    btnAlterar4.Enabled := True;
    btnSalvar4.Enabled := False;
    btnCancelar4.Enabled := False;
    btnDesligamento.Enabled := False;
    btnAtivar.Enabled := False;
    rgSituacao.Enabled := False;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox4A.Enabled := False;
    //GroupBox4B.Enabled := False;
    //GroupBox4C.Enabled := True;
  end;

end;

procedure TfrmFichaCrianca.btnAlterar4Click(Sender: TObject);
begin
  if cbProgramaSocial.ItemIndex >= 0 then
  begin
    FStatus:= FdEdit;
    AtualizaCamposBotoes4;
    GroupBox4A.Enabled := False;
  end;

end;

procedure TfrmFichaCrianca.btnSalvar4Click(Sender: TObject);
var x: Integer;
begin

  // valida os dados
  if (cbProgramaSocial.ItemIndex >= 0) then //or (Length(meDataEntrada.Text) > 0) then
  begin

    if FStatus = FdInsert then
    begin
      try

        //Efetua a inser��o dos dados no banco
        //Para cada "familiar" selecionado, grava o programa social selecionado
        if (dbgFamiliaresProgSociais.SelectedRows.Count > -1) then
        begin
          with dmDeca.cdsSel_Cadastro_Familia do
          begin
            for x:= 0 to (dbgFamiliaresProgSociais.SelectedRows.Count - 1) do
            begin
              GotoBookmark(Pointer(dbgFamiliaresProgSociais.SelectedRows.Items[x]));

              with dmDeca.cdsInc_Cadastro_Programa_Social do
              begin
                Close;
                Params.ParamByName('@pe_cod_id_progsocial').AsInteger := StrToInt(vListaProgrSocial1.Strings[cbProgramaSocial.ItemIndex]);
                Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro_Familia.Fields[0].AsString;
                Params.ParamByName('@pe_flg_status').AsInteger := rgSituacao.ItemIndex; //Priorit�rio
                Params.ParamByName('@pe_dsc_quem').AsString := dmDeca.cdsSel_Cadastro_Familia.Fields[21].AsString;
                Params.ParamByName('@pe_dsc_nome').AsString := dmDeca.cdsSel_Cadastro_Familia.Fields[4].AsString;
                //Tratar a data para aceitar "NULO"
                if (Length(meDataEntrada.Text)>0) then
                  Params.ParamByName('@pe_dat_insercao').Value := GetValue(meDataEntrada, vtDate)
                else
                  Params.ParamByName('@pe_dat_insercao').Value := Null;
                Params.ParamByName('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
                Execute;
              end;
          end;
        end;

        if (chkIncluirCrianca.Checked = True) then
        begin
          with dmDeca.cdsInc_Cadastro_Programa_Social do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_progsocial').Value := StrToInt(vListaProgrSocial1.Strings[cbProgramaSocial.ItemIndex]);
            Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Familia.Fields[0].AsString;
            Params.ParamByName('@pe_flg_status').Value := rgSituacao.ItemIndex; //Priorit�rio
            Params.ParamByName('@pe_dsc_quem').Value := 'PPO';
            Params.ParamByName('@pe_dsc_nome').Value := Trim(txtNome.Text);
            Params.ParamByName('@pe_dat_insercao').Value := GetValue(meDataEntrada, vtDate);
            Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
            Execute;
          end;
        end;
        //end;
        dmDeca.cdsSel_Cadastro_Programa_Social.Refresh;
        dbgProgramas.Refresh;

      end;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram gravados'+#13+#10+
                    'Entre em contato com o Administrador do Sistema');
      end;
    end
    else if FStatus = FdEdit then
    begin

      {
      try
        with dmDeca.cdsAlt_Cadastro_Programa_Social do
        begin
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_dsc_programa').Value := GetValue(cbProgramaSocial.Items[cbProgramaSocial.ItemIndex]);
          Params.ParamByName('@pe_dsc_local').Value := Null;
          Execute;
          Close;
        end;
        dmDeca.cdsSel_Cadastro_Programa_Social.Refresh;
        dbgProgramas.Refresh;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram alterados com sucesso');
      end;

      }

    end;

    // atualiza a tela
    FStatus := FdNone;
    AtualizaCamposBotoes4;
    FillFormProgramasSociais;

  end
  else
  begin
    ShowMessage('Dados Inv�lidos!'+#13+#10+#13+#10+
                'Verifique se os campos obrigat�rio (em azul) foram preenchidos');
    cbProgramaSocial.SetFocus;
  end;


end;

procedure TfrmFichaCrianca.btnCancelar4Click(Sender: TObject);
begin
  FStatus := FdNone;
  AtualizaCamposBotoes4;
  FillFormProgramasSociais;

end;

procedure TfrmFichaCrianca.FillFormProgramasSociais;
begin

  //Atualiza a rela��o de "Composi��o Familiar"

  dbgFamiliaresProgSociais.Refresh;
  // carrega os campos no combo cbProgramaSocial
  with dmDeca.cdsSel_ProgSocial do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
    Params.ParamByName('@pe_dsc_progsocial').Value := Null;
    Open;

    cbProgramaSocial.Clear;
    while not eof do
    begin
      vListaProgrSocial1.Add(IntToStr(FieldByName('cod_id_progsocial').AsInteger));
      vListaProgrSocial2.Add(FieldByName('dsc_progsocial').AsString);
      cbProgramaSocial.Items.Add(FieldByName('dsc_progsocial').AsString);
      Next;
    end;
  end;

end;

procedure TfrmFichaCrianca.AtualizaCamposBotoes2;
begin
  if (FStatus = FdInsert) or (FStatus = FdEdit) then
  begin
    btnNovo2.Enabled := False;
    btnAlterar2.Enabled := False;
    btnExcluir2.Enabled := False;
    btnSalvar2.Enabled := True;
    btnCancelar2.Enabled := True;
    btnSelecionaFoto.Enabled := True;

    if dmDeca.cdsSel_Cadastro_Familia.RecordCount = 0 then
      btnCopiarFamiliares.Visible := True;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox2A.Enabled := True;
    rgMoraCasa.Enabled := True;
    GroupBox2B.Enabled := True;
    GroupBox2C.Enabled := True;
    GroupBox2D.Enabled := True;
    GroupBox2E.Enabled := True;
    GroupBox2F.Enabled := True;
    GroupBox2G.Enabled := True;
    GroupBox2H.Enabled := True;
    GroupBox2I.Enabled := True;
    GroupBox2J.Enabled := True;
    GroupBox2K.Enabled := True;
    GroupBox2L.Enabled := True;
    GroupBox2M.Enabled := True;
    GroupBox2N.Enabled := True;
    GroupBox2O.Enabled := True;
    GroupBox2P.Enabled := True;
    GroupBox2Q.Enabled := True;
    GroupBox2R.Enabled := False;
  end
  else
  begin
    btnNovo2.Enabled := True;
    btnAlterar2.Enabled := True;
    btnExcluir2.Enabled := True;
    btnSalvar2.Enabled := False;
    btnCancelar2.Enabled := False;
    btnSelecionaFoto.Enabled := False;

    btnCopiarFamiliares.Visible := False;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox2A.Enabled := False;
    rgMoraCasa.Enabled := False;
    GroupBox2B.Enabled := False;
    GroupBox2C.Enabled := False;
    GroupBox2D.Enabled := False;
    GroupBox2E.Enabled := False;
    GroupBox2F.Enabled := False;
    GroupBox2G.Enabled := False;
    GroupBox2H.Enabled := False;
    GroupBox2I.Enabled := False;
    GroupBox2J.Enabled := False;
    GroupBox2K.Enabled := False;
    GroupBox2L.Enabled := False;
    GroupBox2M.Enabled := False;
    GroupBox2N.Enabled := False;
    GroupBox2O.Enabled := False;
    GroupBox2P.Enabled := False;
    GroupBox2Q.Enabled := False;
    GroupBox2R.Enabled := True
  end;
end;

procedure TfrmFichaCrianca.FillFormFamiliares;
begin
  // preenche os campos da tela de familiares
  with dmDeca.cdsSel_Cadastro_Familia do
  begin
    cbVinculo.ItemIndex := cbVinculo.Items.IndexOf(Trim(dmDeca.cdsSel_Cadastro_Familia.FieldByName('ind_vinculo').AsString));

    if FieldByName('ind_moracasa').Value = 0 then
      rgMoraCasa.ItemIndex := 0
    else rgMoraCasa.ItemIndex := 1;

    //rgMoraCasa.ItemIndex := FieldByName('ind_moracasa').AsBoolean;
    txtNomeFam.Text           := FieldByName('nom_familiar').AsString;
    chkResponsavel.Checked    := FieldByName('ind_responsavel').AsBoolean;
    //cbTipoDocumento.ItemIndex := cbTipoDocumento.Items.IndexOf(Trim(FieldByName('ind_tipo_documento').AsString));
    //txtRgFam.Text             := FieldByName('num_documento').AsString;
    txtEnderecoResFam.Text    := FieldByName('nom_endereco').AsString;
    txtComplementoFam.Text    := FieldByName('nom_complemento').AsString;
    txtBairroFam.Text         := FieldByName('nom_bairro').AsString;
    txtCepFam.Text            := FieldByName('num_cep').AsString;
    txtFoneFam.Text           := FieldByName('num_telefone').AsString;
    txtNascFam.Text           := FieldByName('nascimento').AsString;

    if (txtNascFam.Text = '30/12/1899') or (txtNascFam.Text = '  /  /    ') or (txtNascFam.Text = '') then
      lbIdadeResp.Caption := ''
    else
      //lbIdadeResp.Caption := CalculaIdadeFamilia(StrToDate(txtNascFam.Text));
      //lbIdadeResp.Caption := Idade2(dmDeca.cdsSel_Cadastro_Familia.FieldByName ('dat_nascimento').AsDateTime);
      lbIdadeResp.Caption := Idade2(StrToDate(txtNascFam.Text));

    //cbSexoFam.ItemIndex := cbSexoFam.Items.IndexOf(FieldByName('ind_sexo').AsString);

    if Trim(FieldByName('ind_sexo').AsString) = 'MASCULINO' then cbSexoFam.ItemIndex := 0;
    if Trim(FieldByName('ind_sexo').AsString) = 'FEMININO' then cbSexoFam.ItemIndex := 1;

    txtEscolaridade.Text   := FieldByName('dsc_escolaridade').AsString;
    txtOcupacaoFam.Text    := FieldByName('dsc_ocupacao').AsString;
    txtFoneContatoFam.Text := FieldByName('num_telefone_trabalho').AsString;
    txtContatoFam.Text     := FieldByName('nom_contato_trabalho').AsString;
    txtRendaFam.Text       := FieldByName('fval_renda').AsString;
    txtLocalFam.Text       := FieldByName('dsc_local_trabalho').AsString;
    txtProjetoFundhas.Text := FieldByName('dsc_projeto_fundhas').AsString;

    //Campos de RG e CPF criados a partir da vers�o 6.0.0 do Deca, Mar�o/2016
    if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').IsNull) then
      txtRgFam.Text := '00.000.000-0'
    else
      txtRgFam.Text := FieldByName('num_rg_fam').Value;


    if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').IsNull) then
      mskCPF_Familiar.Text := '000.000.000-00'
    else
      mskCPF_Familiar.Text := FieldByName('num_cpf_fam').Value;
    
  end;

end;

procedure TfrmFichaCrianca.dbgFamiliaresCellClick(Column: TColumn);
begin
  FillFormFamiliares;
end;

procedure TfrmFichaCrianca.dbgFamiliaresKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    FillFormFamiliares;
end;

procedure TfrmFichaCrianca.dbgFamiliaresKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    FillFormFamiliares;

end;

procedure TfrmFichaCrianca.btnNovo2Click(Sender: TObject);
begin
  // perepara a tela para digita��o de um novo familiar
  FStatus := fdInsert;
  AtualizaCamposBotoes2;

  FillFormFamiliares;
  cbVinculo.SetFocus;

  // limpa os campos
  cbVinculo.ItemIndex := -1;
  txtNomeFam.Clear;
  chkResponsavel.Checked := false;
  cbTipoDocumento.ItemIndex := -1;
  txtRgFam.Clear;
  txtEnderecoResFam.Text := txtEndereco.Text;
  txtComplementoFam.Text := txtComplemento.Text;
  txtBairroFam.Text := txtBairro.Text;
  txtCepFam.Text := txtCep.Text;
  txtFoneFam.Text := txtTelefone.Text;
  txtNascFam.Clear;
  cbSexoFam.ItemIndex := -1;
  txtEscolaridade.Clear;
  txtOcupacaoFam.Clear;
  txtFoneContatoFam.Clear;
  txtContatoFam.Clear;
  txtRendaFam.Text := '0,00';
  txtLocalFam.Clear;
  txtProjetoFundhas.Clear;

end;

procedure TfrmFichaCrianca.btnAlterar2Click(Sender: TObject);
begin
  if Length(txtNomeFam.Text) > 0 then
  begin
    FStatus:= FdEdit;
    AtualizaCamposBotoes2;
    cbVinculo.SetFocus;
  end;

end;

procedure TfrmFichaCrianca.btnCancelar2Click(Sender: TObject);
begin
  FStatus := FdNone;
  AtualizaCamposBotoes2;
  FillFormFamiliares;

end;

procedure TfrmFichaCrianca.btnExcluir2Click(Sender: TObject);
begin
  // pega o conteudo da primeira coluna do select - cod_usuario
  //if dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('cod_familiar').AsInteger > 0 then
  begin
    if Application.MessageBox('Deseja Excluir o registro selecionado ?',
           'Exclus�o',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      try
        with dmDeca.cdsExc_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_familiar').AsInteger := dmDeca.cdsSel_Cadastro_Familia.FieldByName('cod_familiar').AsInteger;
          Execute;
          Close;
          dmDeca.cdsSel_Cadastro_Familia.Refresh;
          dbgFamiliares.Refresh;
        end;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'O registro selecionado n�o foi exclu�do');
      end;
      FillFormFamiliares;
      // atualiza renda familiar (no canto superior da tela) e o total de integrantes
      AtualizaRendaFamiliar;
    end;
  end;

end;

procedure TfrmFichaCrianca.dbgProgramasCellClick(Column: TColumn);
begin
  FillFormProgramasSociais;
end;

procedure TfrmFichaCrianca.dbgProgramasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FillFormProgramasSociais;
end;

procedure TfrmFichaCrianca.dbgProgramasKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FillFormProgramasSociais;

end;

procedure TfrmFichaCrianca.AtualizaRendaFamiliar;
begin

  // atualiza a renda familiar no canto superior da tela
  try
    with dmDeca.cdsSel_Cadastro_Familia_SomaRenda do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
      Open;
      lbRendaFamiliar.Caption := 'Renda Familiar Acumulada:  R$ ' + Trim(Format('%10.2f', [FieldByName('Renda_Mensal').AsFloat]));
      lbRendaFamiliar.Caption := lbRendaFamiliar.Caption + '  -  Renda Per Capta: R$ ' + Trim(Format('%10.2f', [FieldByName('RendaPerCapta').AsFloat]));
      GroupBox2R.Caption := 'Rela��o de Familiares  [ ' + Trim(IntToStr(FieldByName('Total_Familiares').AsInteger)) + ' integrante(s) ]';
      Close;
    end;
  except
    lbRendaFamiliar.Caption := 'Renda Familiar Acumulada:  R$ 0,00';
    GroupBox2R.Caption := 'Rela��o de Familiares';
  end;

end;

procedure TfrmFichaCrianca.btnSalvar2Click(Sender: TObject);
begin
  // valida os dados
  if (cbVinculo.ItemIndex >= 0) and
     (Length(txtNomeFam.Text) > 0) and
     (cbSexoFam.ItemIndex >= 0) then
  begin
    if FStatus = FdInsert then
    begin
      try
        with dmDeca.cdsInc_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value         := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_ind_responsavel').AsBoolean   := chkResponsavel.checked;
          Params.ParamByName('@pe_nom_familiar').Value          := GetValue(txtNomeFam.Text);
          Params.ParamByName('@pe_ind_tipo_documento').Value    := Null; //GetValue(cbTipoDocumento.Items[cbTipoDocumento.ItemIndex]);
          Params.ParamByName('@pe_num_documento').Value         := Null; //GetValue(txtRgFam.Text);
          Params.ParamByName('@pe_dat_nascimento').Value        := GetValue(txtNascFam, vtDate);
          Params.ParamByName('@pe_nom_complemento').Value       := GetValue(txtComplementoFam.Text);
          Params.ParamByName('@pe_nom_bairro').Value            := GetValue(txtBairroFam.Text);
          Params.ParamByName('@pe_num_telefone').Value          := GetValue(txtFoneFam.Text);
          Params.ParamByName('@pe_dsc_ocupacao').Value          := GetValue(txtOcupacaoFam.Text);
          Params.ParamByName('@pe_dsc_local_trabalho').Value    := GetValue(txtLocalFam.Text);
          Params.ParamByName('@pe_val_renda_mensal').Value      := GetValue(txtRendaFam, vtReal);
          Params.ParamByName('@pe_nom_contato_trabalho').Value  := GetValue(txtContatoFam.Text);
          Params.ParamByName('@pe_num_telefone_trabalho').Value := GetValue(txtFoneContatoFam.Text);
          Params.ParamByName('@pe_dsc_escolaridade').Value      := GetValue(txtEscolaridade.Text);
          Params.ParamByName('@pe_dsc_projeto_fundhas').Value   := GetValue(txtProjetoFundhas.Text);
          Params.ParamByName('@pe_num_cep').Value               := GetValue(txtCepFam.Text);
          Params.ParamByName('@pe_ind_sexo').Value              := GetValue(cbSexoFam.Items[cbSexoFam.ItemIndex]);
          Params.ParamByName('@pe_nom_endereco').Value          := GetValue(txtEnderecoResFam.Text);
          Params.ParamByName('@pe_ind_vinculo').Value           := GetValue(cbVinculo.Items[cbVinculo.ItemIndex]);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger   := vvCOD_USUARIO;
          Params.ParamByName('@pe_ind_moracasa').Value          := rgMoraCasa.ItemIndex;
          //Par�metros novos inseridos a partir da vers�o 6.0.0, a pedido da DECA/DRH por conta dos relat�rios do seguro
          Params.ParamByName('@pe_num_rg_fam').Value            := GetValue(txtRgFam.Text);
          Params.ParamByName('@pe_num_cpf_fam').Value           := GetValue(mskCPF_Familiar.Text);
          Execute;
        end;

        //Atualiza o grid
        try
          with dmDeca.cdsSel_Cadastro_Familia do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_familiar').Value  := Null;
            Open;
            dmDeca.cdsSel_Cadastro_Familia.Refresh;
            dbgFamiliares.Refresh;
          end;
        except end;


      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram gravados');
      end;
    end
    else if FStatus = FdEdit then
    begin
      try
        with dmDeca.cdsAlt_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value         := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_cod_familiar').AsInteger      := dmDeca.cdsSel_Cadastro_Familia.FieldByName('cod_familiar').AsInteger;
          Params.ParamByName('@pe_ind_responsavel').AsBoolean   := chkResponsavel.checked;
          Params.ParamByName('@pe_nom_familiar').Value          := GetValue(txtNomeFam.Text);
          Params.ParamByName('@pe_ind_tipo_documento').Value    := GetValue(cbTipoDocumento.Items[cbTipoDocumento.ItemIndex]);
          Params.ParamByName('@pe_num_documento').Value         := GetValue(txtRgFam.Text);
          Params.ParamByName('@pe_dat_nascimento').Value        := GetValue(txtNascFam, vtDate);
          Params.ParamByName('@pe_nom_complemento').Value       := GetValue(txtComplementoFam.Text);
          Params.ParamByName('@pe_nom_bairro').Value            := GetValue(txtBairroFam.Text);
          Params.ParamByName('@pe_num_telefone').Value          := GetValue(txtFoneFam.Text);
          Params.ParamByName('@pe_dsc_ocupacao').Value          := GetValue(txtOcupacaoFam.Text);
          Params.ParamByName('@pe_dsc_local_trabalho').Value    := GetValue(txtLocalFam.Text);
          Params.ParamByName('@pe_val_renda_mensal').Value      := GetValue(txtRendaFam, vtReal);
          Params.ParamByName('@pe_nom_contato_trabalho').Value  := GetValue(txtContatoFam.Text);
          Params.ParamByName('@pe_num_telefone_trabalho').Value := GetValue(txtFoneContatoFam.Text);
          Params.ParamByName('@pe_dsc_escolaridade').Value      := GetValue(txtEscolaridade.Text);
          Params.ParamByName('@pe_dsc_projeto_fundhas').Value   := GetValue(txtProjetoFundhas.Text);
          Params.ParamByName('@pe_num_cep').Value               := GetValue(txtCepFam.Text);
          Params.ParamByName('@pe_ind_sexo').Value              := GetValue(cbSexoFam.Items[cbSexoFam.ItemIndex]);
          Params.ParamByName('@pe_nom_endereco').Value          := GetValue(txtEnderecoResFam.Text);
          Params.ParamByName('@pe_ind_vinculo').Value           := GetValue(cbVinculo.Items[cbVinculo.ItemIndex]);
          Params.ParamByName('@pe_ind_moracasa').Value          := rgMoraCasa.ItemIndex;
          //Par�metros novos inseridos a partir da vers�o 6.0.0, a pedido da DECA/DRH por conta dos relat�rios do seguro
          Params.ParamByName('@pe_num_rg_fam').Value            := GetValue(txtRgFam.Text);
          Params.ParamByName('@pe_num_cpf_fam').Value           := GetValue(mskCPF_Familiar.Text);
          Execute;
        end;
        
        //Atualiza o grid
        try
          with dmDeca.cdsSel_Cadastro_Familia do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_familiar').Value  := Null;
            Open;
            dmDeca.cdsSel_Cadastro_Familia.Refresh;
            dbgFamiliares.Refresh;
          end;
        except end;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram alterados com sucesso');
      end;
    end;

    // atualiza o grid posicionando na ficha incluida/alterada
    dmDeca.cdsSel_Cadastro_Familia.Locate('nom_familiar',
             txtNomeFam.Text, [loCaseInsensitive]);
    dbgFamiliares.Refresh;

    // atualiza a tela
    FStatus := FdNone;
    AtualizaCamposBotoes2;
    FillFormFamiliares;

    // atualiza renda familiar (no canto superior da tela) e o total de integrantes
    AtualizaRendaFamiliar;

  end
  else
  begin
    ShowMessage('Dados Inv�lidos!'+#13+#10+#13+#10+
                'Verifique se os campos obrigat�rio (em azul) foram preenchidos');
    cbVinculo.SetFocus;
  end;

end;

procedure TfrmFichaCrianca.txtNascFamExit(Sender: TObject);
begin
  // testa se a data � v�lida
  //try
  //  StrToDate(txtNascFam.Text);

  if (txtNascFam.Text = Null)  then //OR (txtNascFam.Text <> '') then
    lbIdadeResp.Caption := CalculaIdadeFamilia(StrToDate(txtNascFam.Text))
  else
    lbIdadeResp.Caption := ''; 
  //except
  //  on E : EConvertError do
  //  begin
  //    ShowMessage('Data Inv�lida');
  //    txtNascFam.SetFocus;
  //  end;
  //end;
  // exibe a idade
  //lbIdadeResp.Caption := CalculaIdadeFamilia(StrToDate(txtNascFam.Text));
end;

procedure TfrmFichaCrianca.txtRendaFamExit(Sender: TObject);
begin
  try
    StrToFloat(txtRendaFam.Text);
    //lbIdadeResp.Caption := CalculaIdadeFamilia(StrToDate(txtNascFam.Text));
  except
    on E : EConvertError do
      txtRendaFam.SetFocus;
  end;

end;

procedure TfrmFichaCrianca.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  // n�o permite a troca de TabSheet se estiver editando a p�gina atual
  if (FStatus = FdInsert) or (FStatus = FdEdit) or (Length(Trim(txtMatricula.Text))=0) then
    AllowChange := false;
end;

procedure TfrmFichaCrianca.txtNomeFamExit(Sender: TObject);
begin
  // testa se foi preenchido
  If Length(txtNomeFam.Text) = 0 then
  begin
    ShowMessage('O Nome deve ser preenchido');
    txtNomeFam.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.cbSexoFamExit(Sender: TObject);
begin
  // testa se o campo foi selecionado
  if cbSexoFam.ItemIndex = -1 then
  begin
    ShowMessage('Este campo deve ser selecionado');
    cbSexoFam.SetFocus;
  end;
end;

procedure TfrmFichaCrianca.btnCopiarFichaClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  // para sele��o da ficha a ser copiada (tem que ser da mesma unidade)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    // SP_SEL_CADASTRO_MOVE com a crian�a escolhinda (vvCOD_MATRICULA_PESQUISA)
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').Value:= 'S';
        Params.ParamByName('@pe_Matricula_Atual').Value:= vvCOD_MATRICULA_PESQUISA;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Length(Trim(FieldByName('cod_matricula').AsString)) > 0  then
        begin
          // atualiza os campos do formul�rio que podem ser compartilhados
          // dados cadastrais
          txtEndereco.Text := FieldByName('nom_endereco').AsString;
          txtComplemento.Text := FieldByName('nom_complemento').AsString;
          txtBairro.Text := FieldByName('nom_bairro').AsString;
          txtCep.Text := FieldByName('num_cep').AsString;
          txtTelefone.Text := FieldByName('num_telefone').AsString;
        end;

        txtMatricula.SetFocus;

      end;
    except end;

  end;

end;

procedure TfrmFichaCrianca.txtPesoExit(Sender: TObject);
begin
  try
    StrToFloat(txtPeso.Text);
  except
    on E : EConvertError do
      txtPeso.SetFocus;
  end;

end;

procedure TfrmFichaCrianca.txtAlturaExit(Sender: TObject);
begin
  try
    StrToFloat(txtAltura.Text);
  except
    on E : EConvertError do
      txtAltura.SetFocus;
  end;

end;

procedure TfrmFichaCrianca.btnCopiarFamiliaresClick(Sender: TObject);
var
  vData_Nascimento : String;

begin
  // chama a tela de pesquisa (gen�rica)
  // para sele��o da ficha a ser copiada (tem que ser da mesma unidade)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    // SP_SEL_CADASTRO_MOVE com a crian�a escolhinda (vvCOD_MATRICULA_PESQUISA)
    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').AsString := vvCOD_MATRICULA_PESQUISA;
        Params.ParamByName('@pe_cod_familiar').Value := Null;
        Open;

        while not (dmDeca.cdsSel_Cadastro_Familia.Eof) do
        begin
          // l� cada um dos familiares e insere para a nova matricula
          with dmDeca.cdsInc_Cadastro_Familia do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value         := GetValue(txtMatricula.Text);
            Params.ParamByName('@pe_ind_responsavel').Value       := dmDeca.cdsSel_Cadastro_Familia.FieldByName('ind_responsavel').AsBoolean;
            Params.ParamByName('@pe_nom_familiar').Value          := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('nom_familiar').AsString);
            Params.ParamByName('@pe_ind_tipo_documento').Value    := Null;
            Params.ParamByName('@pe_num_documento').Value         := Null;
            Params.ParamByName('@pe_dat_nascimento').AsDateTime   := dmDeca.cdsSel_Cadastro_Familia.FieldByName('dat_nascimento').AsDateTime;
            Params.ParamByName('@pe_nom_complemento').Value       := GetValue(txtComplemento.Text);
            Params.ParamByName('@pe_nom_bairro').Value            := GetValue(txtBairro.Text);
            Params.ParamByName('@pe_num_telefone').Value          := GetValue(txtTelefone.Text);
            Params.ParamByName('@pe_dsc_ocupacao').Value          := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('dsc_ocupacao').AsString);
            Params.ParamByName('@pe_dsc_local_trabalho').Value    := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('dsc_local_trabalho').AsString);
            Params.ParamByName('@pe_val_renda_mensal').AsFloat    := dmDeca.cdsSel_Cadastro_Familia.FieldByName('val_renda_mensal').AsFloat;
            Params.ParamByName('@pe_nom_contato_trabalho').Value  := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('nom_contato_trabalho').AsString);
            Params.ParamByName('@pe_num_telefone_trabalho').Value := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('num_telefone_trabalho').AsString);
            Params.ParamByName('@pe_dsc_escolaridade').Value      := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('dsc_escolaridade').AsString);
            Params.ParamByName('@pe_dsc_projeto_fundhas').Value   := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('dsc_projeto_fundhas').AsString);
            Params.ParamByName('@pe_num_cep').Value               := GetValue(txtCep.Text);
            Params.ParamByName('@pe_ind_sexo').Value              := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('ind_sexo').AsString);
            Params.ParamByName('@pe_nom_endereco').Value          := GetValue(txtEndereco.Text);
            Params.ParamByName('@pe_ind_vinculo').Value           := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('ind_vinculo').AsString);
            Params.ParamByName('@pe_log_cod_usuario').Value       := vvCOD_USUARIO;
            Params.ParamByName('@pe_ind_moracasa').Value          := GetValue(dmDeca.cdsSel_Cadastro_Familia.FieldByName('ind_moracasa').AsString);
            Params.ParamByName('@pe_num_rg_fam').Value            := dmDeca.cdsSel_Cadastro_Familia.FieldByName('num_rg_fam').Value;
            Params.ParamByName('@pe_num_cpf_fam').Value           := dmDeca.cdsSel_Cadastro_Familia.FieldByName('num_cpf_fam').Value;
            Execute;
          end;
          dmDeca.cdsSel_Cadastro_Familia.Next;
        end;
      end;
    except end;

    // carrega o grid
    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
        Params.ParamByName('@pe_cod_familiar').Value := Null;
        Open;
        dbgFamiliares.Refresh;
      end;
    except end;

    // atualiza a tela
    FStatus := FdNone;
    AtualizaCamposBotoes2;
    FillFormFamiliares;

    // atualiza renda familiar (no canto superior da tela) e o total de integrantes
    AtualizaRendaFamiliar;
    ShowMessage('Os familiares foram transferidos'+#13+#10+#13+#10+
                'Favor verificar cada um dos familiares pois as informa��es de v�nculo podem estar desatualizadas!');

  end;

end;

procedure TfrmFichaCrianca.FillFormAtendimentos;
begin

  // carrega os campos na tela
  with dmDeca.cdsSel_Cadastro_Atendimento_Rede do
  begin
    cbAtendimentos.ItemIndex := cbAtendimentos.Items.IndexOf(Trim(FieldByName('dsc_atendimento').AsString));
    txtDataInicioAt.Text := DateToStr(FieldByName('dat_atendimento').AsDateTime);
    txtLocalAt.Text := Trim(FieldByName('dsc_local').AsString);
    txtDescricaoAtendimento.Text := FieldByName('dsc_observacao').AsString;
  end;

end;

procedure TfrmFichaCrianca.btnNovo5Click(Sender: TObject);
begin
  // perepara a tela para digita��o de um novo atendimento na rede
  FStatus := fdInsert;
  AtualizaCamposBotoes5;
  cbAtendimentos.SetFocus;

  // limpa os campos
  cbAtendimentos.ItemIndex := -1;
  txtDataInicioAt.Clear;
  txtLocalAt.Clear;
  txtDescricaoAtendimento.Clear;

end;

procedure TfrmFichaCrianca.AtualizaCamposBotoes5;
begin
if (FStatus = FdInsert) or (FStatus = FdEdit) then
  begin
    btnNovo5.Enabled := False;
    btnAlterar5.Enabled := False;
    btnSalvar5.Enabled := True;
    btnCancelar5.Enabled := True;

    // desabilita os campos (atrav�s dos groupbox)
    GroupBox5A.Enabled := True;
    GroupBox5B.Enabled := True;
    GroupBox5C.Enabled := True;
    GroupBox5D.Enabled := True;
    GroupBox5E.Enabled := False;

  end
  else
  begin
    btnNovo5.Enabled := True;
    btnAlterar5.Enabled := True;
    btnSalvar5.Enabled := False;
    btnCancelar5.Enabled := False;

    // Desabilita os campos (atrav�s dos groupbox)
    GroupBox5A.Enabled := False;
    GroupBox5B.Enabled := False;
    GroupBox5C.Enabled := False;
    GroupBox5D.Enabled := False;
    GroupBox5E.Enabled := True;
  end;
end;

procedure TfrmFichaCrianca.btnCancelar5Click(Sender: TObject);
begin
  FStatus := FdNone;
  AtualizaCamposBotoes5;
  FillFormAtendimentos;
end;

procedure TfrmFichaCrianca.btnAlterar5Click(Sender: TObject);
begin
  //if cbAtendimentos.ItemIndex > -1 then
  //begin
    FStatus:= FdEdit;
    AtualizaCamposBotoes5;
    GroupBox5A.Enabled := True;
    GroupBox5A.SetFocus;
    //txtDataInicioAt.SetFocus;
  //end;

end;

procedure TfrmFichaCrianca.btnSalvar5Click(Sender: TObject);
begin

  if (cbAtendimentos.ItemIndex >= 0) then
  begin

    if FStatus = FdInsert then
    begin
      try
        with dmDeca.cdsInc_Cadastro_Atendimento_Rede do
        begin
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_dsc_atendimento').Value := GetValue(cbAtendimentos.Items[cbAtendimentos.ItemIndex]);
          Params.ParamByName('@pe_dat_atendimento').Value := GetValue(txtDataInicioAt, vtDate);
          Params.ParamByName('@pe_dsc_local').Value := GetValue(txtLocalAt.Text);
          Params.ParamByName('@pe_dsc_observacao').Value := GetValue(txtDescricaoAtendimento.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;
          Close;
        end;
        dmDeca.cdsSel_Cadastro_Atendimento_Rede.Refresh;
        dbgAtendimentos.Refresh;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram gravados'+#13+#10+
                    'Verifique se o Atendimento na Rede ' +
                    cbAtendimentos.Items[cbAtendimentos.ItemIndex] +
                    ' j� foi cadastrado'#13+#10+'ou entre em contato com o Administrador do Sistema');
      end;
    end
    else if FStatus = FdEdit then
    begin
      try
        with dmDeca.cdsAlt_Cadastro_Atendimento_Rede do
        begin
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_cod_id_atendimento').AsInteger := dmDeca.cdsSel_Cadastro_Atendimento_Rede.FieldByName('cod_id_atendimento').AsInteger;
          Params.ParamByName('@pe_dsc_atendimento').Value := GetValue(cbAtendimentos.Items[cbAtendimentos.ItemIndex]);
          Params.ParamByName('@pe_dat_atendimento').Value := GetValue(txtDataInicioAt, vtDate);
          Params.ParamByName('@pe_dsc_local').Value := GetValue(txtLocalAt.Text);
          Params.ParamByName('@pe_dsc_observacao').Value := GetValue(txtDescricaoAtendimento.Text);
          Execute;
          Close;
        end;
        dmDeca.cdsSel_Cadastro_Atendimento_Rede.Refresh;
        dbgAtendimentos.Refresh;
      except
        ShowMessage('Aten��o!'+#13+#10+#13+#10+
                    'Os dados n�o foram alterados com sucesso');
      end;
    end;

    // atualiza a tela
    FStatus := FdNone;
    AtualizaCamposBotoes5;
    FillFormAtendimentos;

  end
  else
  begin
    ShowMessage('Dados Inv�lidos!'+#13+#10+#13+#10+
                'Verifique se os campos obrigat�rio (em azul) foram preenchidos');
    cbAtendimentos.SetFocus;            
  end;


end;

procedure TfrmFichaCrianca.dbgAtendimentosCellClick(Column: TColumn);
begin
  FillFormAtendimentos;
end;

procedure TfrmFichaCrianca.dbgAtendimentosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FillFormAtendimentos;
end;

procedure TfrmFichaCrianca.dbgAtendimentosKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FillFormAtendimentos;
end;

procedure TfrmFichaCrianca.txtDataInicioAtExit(Sender: TObject);
begin
  // testa se a data � v�lida
  try
    StrToDate(txtDataInicioAt.Text);
  except
    on E : EConvertError do
    begin
      ShowMessage('Data Inv�lida');
      txtDataInicioAt.SetFocus;
    end;
  end;
end;

function TfrmFichaCrianca.CalculaIdadeFamilia(NascFam: TDateTime): String;
var
  PeriodoF : Integer;
  intContAnosF, intContMesesF : Integer;
  DataFinalF : TDateTime;
begin
// calculo de anos
    intContAnosF := 0 ;
    PeriodoF := 12 ;
    DataFinalF := Date() ;
    Repeat
      Inc(intContAnosF) ;
      DataFinalF := IncMonth(DataFinalF,PeriodoF * -1) ;
    Until DataFinalF < NascFam ;

    //DataFinal:= IncMonth(DataFinal,Periodo) ;
    Inc(intContAnosF,-1) ;

    // calculo de meses
    intContMesesF := 0 ;
    PeriodoF := 1 ;
    DataFinalF := Date() ;
    Repeat
      Inc(intContMesesF) ;
      DataFinalF := IncMonth(DataFinalF,PeriodoF * -1) ;
    Until DataFinalF < NascFam;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContMesesF,-1) ;
    intContMesesF := intContMesesF mod 12;
    Result := IntToStr(intContAnosF) + ' anos e ' + IntToStr(intContMesesF) + ' meses';
end;

procedure TfrmFichaCrianca.txtNascFamChange(Sender: TObject);
begin
  {// testa se a data � v�lida
  try
    StrToDate(txtNascFam.Text);
    lbIdadeResp.Caption := CalculaIdadeFamilia(StrToDate(txtNascFam.Text));
  except
    on E : EConvertError do
    begin
      //ShowMessage('Data Inv�lida');
      lbIdadeResp.Caption := '';
      //txtNascFam.SetFocus;
    end;
  end;
  // exibe a idade
  lbIdadeResp.Caption := CalculaIdadeFamilia(StrToDate(txtNascFam.Text));}
end;

procedure TfrmFichaCrianca.dbgHistoricoCellClick(Column: TColumn);
begin
  meDescricaoOcorrencia.Lines.Clear;
  meDescricaoOcorrencia.Lines.Add (dmDeca.cdsSel_Historico.FieldByName('dsc_ocorrencia').Value);
end;

procedure TfrmFichaCrianca.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm(TfrmEmitirFichaCadastro, frmEmitirFichaCadastro);
  frmEmitirFichaCadastro.ShowModal;

  {
  //if Length(txtMatricula.Text) = 8 then
  //begin
    try
    with dmDeca.cdsSel_Ficha_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').AsString := GetValue(txtMatricula.Text); //dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').AsString;
      Open;

      Application.CreateForm (TrelFichaCadastro, relFichaCadastro);

      while not dmDeca.cdsSel_Ficha_Cadastro.eof do
      begin
        relFichaCadastro.rtVinculo.Lines.Add (GetValue(FieldByName('ind_vinculo').AsString));
        relFichaCadastro.rtResponsavel.Lines.Add (FieldByName('fResponsavel').Value);
        relFichaCadastro.rtNome.Lines.Add (GetValue(FieldByName('nom_familiar').AsString));

        if (dmDeca.cdsSel_Ficha_Cadastro.FieldByName('ind_tipo_documento').AsString <> '') or (dmDeca.cdsSel_Ficha_Cadastro.FieldByName('ind_tipo_documento').AsString <> Null) then
          relFichaCadastro.rtTipoDoc.Lines.Add (dmDeca.cdsSel_Ficha_Cadastro.FieldByName('ind_tipo_documento').AsString)
        else
          relFichaCadastro.rtTipoDoc.Lines.Add ('-');

        if (dmDeca.cdsSel_Ficha_Cadastro.FieldByName('num_documento').AsString <> '') or (dmDeca.cdsSel_Ficha_Cadastro.FieldByName('num_documento').AsString <> Null) then
          relFichaCadastro.rtNumDoc.Lines.Add (dmDeca.cdsSel_Ficha_Cadastro.FieldByName('num_documento').AsString)
        else
          relFichaCadastro.rtNumDoc.Lines.Add ('-');

        Next;
      end;

      relFichaCadastro.imgFotografia.Picture := Fotografia.Picture;
      relFichaCadastro.Preview;
      relFichaCadastro.Free;
    end;
    except end;
    }
end;

procedure TfrmFichaCrianca.btnSelecionaFotoClick(Sender: TObject);
begin
  try
    //verifica se existe uma fotografia cadastrada para a matr�cula
      with dmDeca.cdsSel_Cadastro_Foto do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
        Open;

        if RecordCount > 0 then
        begin
          if Application.MessageBox('J� existe uma FOTOGRAFIA cadastrada. Substituir?',
                                    'Selecionar Fotografia',
                                    MB_ICONQUESTION + MB_YESNO ) = idYes then
          begin
            OpenPictureDialog1.Execute;
            Path := OpenPictureDialog1.FileName;
            Fotografia.Picture.LoadFromFile(Path);

            if Path <> '' then
            begin

              try
                with dmDeca.cdsAlt_Cadastro_Foto do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_img_foto').LoadFromFile(Path, ftBlob);
                  Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                  Params.ParamByName ('@pe_log_data').Value := Date();
                  Execute;
                end
            except end;
            end;

            //Fotografia.Picture.Assign (vv_JPG);
            //Exibe a Fotografia da crian�a
            vv_JPG := nil;
            try
              with dmDeca.cdsSel_Cadastro_Foto do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
                Open;

                b := dmDeca.cdsSel_Cadastro_Foto.CreateBlobStream (dmDeca.cdsSel_Cadastro_Foto.FieldByName ('img_foto'), bmRead);

                if (b.Size > 0) then
                begin
                  try
                    vv_JPG := TJPEGImage.Create;
                    vv_JPG.LoadFromStream (b);
                    Fotografia.Picture.Assign (vv_JPG);
                  except end
                end
                else Fotografia.Picture.Assign (nil);

                vv_JPG.Free;
                b.Destroy;

              end
            except end;

            
          end
        end
        else
        begin
          //Caso a fotografia n�o esteja cadastrada
          OpenPictureDialog1.Execute;
          Path := OpenPictureDialog1.FileName;
          Fotografia.Picture.LoadFromFile(Path);

          with dmDeca.cdsInc_Cadastro_Foto do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value       := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_img_foto').LoadFromFile(Path, ftOraBlob);
            Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
            Params.ParamByName ('@pe_log_data').AsDateTime       := Date();
            Execute;
          end;
        end;
      end;

  except end;

end;

procedure TfrmFichaCrianca.btnRemoverTurmaClick(Sender: TObject);
begin
  cbTurma.ItemIndex := -1;
end;

procedure TfrmFichaCrianca.btnRemoverEmpresaClick(Sender: TObject);
begin
  cbEmpresa.ItemIndex := -1;
  edAssistenteSocial.Clear;
end;

procedure TfrmFichaCrianca.cbEmpresaClick(Sender: TObject);
begin
  try
    //with dmDeca.cdsSel_Empresa do
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      //Params.ParamByName ('@pe_cod_empresa').AsInteger := StrToInt(vListaEmpresa1.Strings[cbEmpresa.ItemIndex]);
      Params.ParamByName ('@pe_num_secao').Value := StrToInt(vListaEmpresa1.Strings[cbEmpresa.ItemIndex]);
      Open;

      edAssistenteSocial.Text := FieldByName ('nom_asocial').Value;
      Close;

    end;
  except end;
end;

function TfrmFichaCrianca.Bissexto(AYear: Integer): Boolean;
begin
  Result := (AYear mod 4 = 0) and ((Ayear mod 100 <> 0) or (AYear mod 400 = 0));
end;

function TfrmFichaCrianca.DiasDoMes(Ayear, AMonth: Integer): Integer;
const
  DaysInMonth : array[1..12] of Integer = (31,28,31,30,31,30,31,31,30,31,30,31);
begin
  Result := DaysInMonth[AMonth];
  if (AMonth = 2) and Bissexto(Ayear) then Inc(Result);
end;

function TfrmFichaCrianca.Idade2(DataNasc: TDate): String;
var
   idadea,idadem,ano1,ano,mes1,mes,dia1,dia: integer;
   data: string;
begin

   data := datetostr(DataNasc);
   dia1:=strtoint(copy(data,0,2));
   mes1:=strtoint(copy(data,4,2));
   Ano1:=strtoint(copy(data,7,4));
   dia:=strtoint(copy(datetostr(date),0,2));
   mes:=strtoint(copy(datetostr(date),4,2));
   Ano:=strtoint(copy(datetostr(date),7,4));
   if mes1<mes then
   begin

     if dia1<=dia then
     begin

          idadea:=ano - ano1;
          idadem :=mes - mes1 ;
     end
     else
     begin

          idadea:=ano - ano1 ;
          idadem:=mes - mes1 - 1;
     end;


   end
   else
   begin
      if mes1 = mes then
      begin
          if dia1<=dia then
          begin

             idadea:=ano - ano1;
             idadem :=mes - mes1; // tem k dar 0
          end
          else
          begin

             idadea:=ano - ano1 -1;
             idadem:=mes - mes1 - 1;
             idadem:=12 + idadem;
          end;

      end
      else
      begin
        if dia1<=dia then
          begin

             idadea:=ano - ano1 - 1;
             idadem :=mes - mes1;
             idadem:=12 + idadem;
          end
          else
          begin

             idadea:=ano - ano1 -1;
             idadem:=mes - mes1 - 1;
             idadem:=12 + idadem;
          end;
      end;

   end;
   result:= inttostr(idadea)+' anos e '+inttostr(idadem)+' meses';

end;

function TfrmFichaCrianca.Dias(Data: TDate): String;
begin
  Result := FloatToStr(Date - Data);
end;

function TfrmFichaCrianca.Idade(Nasc: TDate): String;
var
  AuxIdade, Meses, IdadeReal : String;
  MesesFloat : Real;
  IdadeInc : Integer;
begin
  AuxIdade := Format('%0.2f',[(Date-Nasc)/365.6]);
  Meses := FloatToStr(Frac(StrToFloat(AuxIdade)));

  if AuxIdade = '0' then
  begin
    Result := '0.0';
    Exit;
  end;

  if Meses[1] = '-' then Meses := FloatToStr(STrToFloat(Meses)* -1);

  Delete(Meses,1,2);

  if Length(Meses) = 1 then Meses := Meses + '0';

  if (Meses <> '0') and (Meses <> '0') then MesesFloat := Round(((365.6 * StrToInt(Meses)) /100) / 30)
  else MesesFloat := 0;

  if MesesFloat <> 12 then IdadeReal := IntToStr(Trunc(StrToFloat(AuxIdade))) + ', ' + FloatToStr(MesesFloat)
  else
  begin
    IdadeInc := Trunc(StrToFloat(AuxIdade));
    Inc(IdadeInc);
    IdadeReal := IntToStr(IdadeInc) + ', ' + '0';
  end;

  Result := IdadeReal;

end;

procedure TfrmFichaCrianca.btnEnviarEmailClick(Sender: TObject);
var eMail : String;
begin
  if (dmDeca.cdsSel_Empresa.FieldByName ('dsc_email').AsString <> '') then
  begin
    eMail := 'mailto:'+ dmDeca.cdsSel_Empresa.FieldByName ('dsc_email').AsString;
    ShellExecute (GetDesktopWindow, 'Open', PChar(eMail), nil, nil, sw_ShowNormal);
  end;
end;

procedure TfrmFichaCrianca.btnVerRegistroAtendimentoClick(Sender: TObject);
begin
  Application.CreateForm (TfrmEmiteRegAtendimento, frmEmiteRegAtendimento);
  frmEmiteRegAtendimento.Show;
  frmEmiteRegAtendimento.txtMatricula.Text := txtMatricula.Text;
end;

procedure TfrmFichaCrianca.dbgHistoricoEscolas2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //if dmDeca.cdsSel_HistoricoEscola.FieldByName('flg_status').Value = 0 then
  //begin
  //  dbgHistoricoEscolas.Canvas.Font.Color:= clWhite;
  //  dbgHistoricoEscolas.Canvas.Brush.Color:= clGreen;
  //end
  //else
  //begin
  //  dbgHistoricoEscolas.Canvas.Font.Color:= clBlack;
  //  dbgHistoricoEscolas.Canvas.Brush.Color:= clWhite;
  //end;

  //dbgHistoricoEscolas.Canvas.FillRect(Rect);
  //dbgHistoricoEscolas.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.Value);

end;

procedure TfrmFichaCrianca.dbgAcompanhamentosDblClick(Sender: TObject);
begin
  AtualizaTelaAcompEscolar('Alterar');

  //if not VarIsNull(dmDeca.cdsSel_Acompanhamento.FieldByName('num_ano').AsInteger) then
  //begin
    edAno.Text                 := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName('num_ano').AsInteger);
    edDiasLetivos.Text         := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName('num_dias_letivos').AsInteger);
    cbBimestre.ItemIndex       := dmDeca.cdsSel_Acompanhamento.FieldByName('num_bimestre').AsInteger - 1;
    edNumFaltas.Text           := IntToStr(dmDeca.cdsSel_Acompanhamento.FieldByName('num_faltas').AsInteger);
    edObservacoes.Text         := dmDeca.cdsSel_Acompanhamento.FieldByName('dsc_observacoes').AsString;
    rgAproveitamento.ItemIndex := dmDeca.cdsSel_Acompanhamento.FieldByName('flg_aproveitamento').AsInteger;
  //end;

end;

procedure TfrmFichaCrianca.AtualizaTelaAcompEscolar(Modo: String);
begin
  if Modo = 'Padrao' then
  begin

    dbgDadosAcompEscolar.Enabled := True;
    edAnoLetivoE.Clear;
    cbBimestreE.ItemIndex := -1;
    cbEscolaE.ItemIndex := -1;
    cbSerieE.ItemIndex := -1;
    cbFrequenciaE.ItemIndex := -1;
    cbAproveitamentoE.ItemIndex := -1;
    cbSituacaoE.ItemIndex := -1;
    cbPeriodoEscolaE.ItemIndex := -1;
    edObservacaoE.Clear;
    lbMsgDefasagemEscolar.Caption := '';
    edTurmaEscola.Clear;

    Panel13.Enabled := False;
    GroupBoxE1.Enabled := False;
    GroupBoxE2.Enabled := False;
    GroupBoxE3.Enabled := False;
    GroupBoxE4.Enabled := False;
    GroupBoxE5.Enabled := False;
    GroupBoxE6.Enabled := False;
    GroupBoxE8.Enabled := False;
    GroupBoxE9.Enabled := False;
    GroupBoxE10.Enabled := False;
    GroupBoxE12.Enabled := False;
    GroupBoxE14.Enabled := False;

    btnNovoLancamentoE.Enabled := True;
    btnAlteraLancamentoE.Enabled := False;
    btnGravaLancamentoE.Enabled := False;
    btnCancelaOperacaoE.Enabled := False;
    btnVerBoletimE.Enabled := True;
    btnLocalizarFicha.Enabled := True;
    btnSairE.Enabled := True;

    btnPrimeiraFicha.Enabled := True;
    btnFichaAnterior.Enabled := True;
    btnProximaFicha.Enabled  := True;
    btnUltimaFicha.Enabled   := True;

    imgLegendaFreqAprov.Enabled := True;
    imgLegendaFreqAprov.Visible := True;

    Image1.Enabled := True;
    Image2.Enabled := True;    

  end
  else if Modo = 'Novo' then
  begin

    Panel13.Enabled := True;
    GroupBoxE1.Enabled := True;
    GroupBoxE2.Enabled := True;
    GroupBoxE3.Enabled := True;
    GroupBoxE4.Enabled := True;
    GroupBoxE5.Enabled := True;
    GroupBoxE6.Enabled := True;
    GroupBoxE8.Enabled := True;
    GroupBoxE9.Enabled := True;
    GroupBoxE10.Enabled := True;
    GroupBoxE12.Enabled := True;
    GroupBoxE14.Enabled := True;

    btnNovoLancamentoE.Enabled := False;
    btnAlteraLancamentoE.Enabled := False;
    btnGravaLancamentoE.Enabled := True;
    btnCancelaOperacaoE.Enabled := True;
    btnVerBoletimE.Enabled := False;
    btnLocalizarFicha.Enabled := False;
    btnSairE.Enabled := False;

    btnPrimeiraFicha.Enabled := False;
    btnFichaAnterior.Enabled := False;
    btnProximaFicha.Enabled  := False;
    btnUltimaFicha.Enabled   := False;

    imgLegendaFreqAprov.Enabled := False;
    imgLegendaFreqAprov.Visible := False;

    Image1.Enabled := True;
    Image2.Enabled := True;

  end
  else if Modo = 'Alterar' then
  begin

    Panel13.Enabled := True;
    GroupBoxE1.Enabled := True;
    GroupBoxE2.Enabled := True;
    GroupBoxE3.Enabled := True;
    GroupBoxE4.Enabled := True;
    GroupBoxE5.Enabled := True;
    GroupBoxE6.Enabled := True;
    GroupBoxE8.Enabled := True;
    GroupBoxE9.Enabled := True;
    GroupBoxE10.Enabled := True;
    GroupBoxE12.Enabled := True;
    GroupBoxE14.Enabled := True;

    btnNovoLancamentoE.Enabled := False;
    btnAlteraLancamentoE.Enabled := True;
    btnGravaLancamentoE.Enabled := False;
    btnCancelaOperacaoE.Enabled := True;
    btnVerBoletimE.Enabled := False;
    btnLocalizarFicha.Enabled := False;
    btnSairE.Enabled := False;

    btnPrimeiraFicha.Enabled := False;
    btnFichaAnterior.Enabled := False;
    btnProximaFicha.Enabled  := False;
    btnUltimaFicha.Enabled   := False;

    imgLegendaFreqAprov.Enabled := False;
    imgLegendaFreqAprov.Visible := False;

    edObservacaoE.Clear;
    edTurmaEscola.Clear;

    Image1.Enabled := True;
    Image2.Enabled := True;    

  end;
end;

procedure TfrmFichaCrianca.btnNovoAcompanhamentoClick(Sender: TObject);
begin
  AtualizaTelaAcompEscolar('Novo');
end;

procedure TfrmFichaCrianca.btnCancelarAcompanhamentoClick(Sender: TObject);
begin
  AtualizaTelaAcompEscolar('Padrao');
end;

procedure TfrmFichaCrianca.btnGravarAcompanhamentoClick(Sender: TObject);
begin
  //Validar dados de Matr�cula, Bimestre e Ano antes de lan�ar
  try
    with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := GetValue(frmFichaCrianca.txtMatricula.Text);
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_num_bimestre').AsInteger := cbBimestre.ItemIndex + 1;
      Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
      Params.ParamByName('@pe_flg_aproveitamento').Value := Null; //rgAproveitamento.ItemIndex;
      Params.ParamByName('@pe_cod_escola').Value := Null;
      Open;

      if (dmDeca.cdsSel_Acompanhamento.RecordCount < 1) then
      begin
        try
          with dmDeca.cdsInc_Acompanhamento do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName('@pe_num_dias_letivos').Value := GetValue(edDiasLetivos.Text);
            Params.ParamByName('@pe_num_faltas').Value := GetValue(edNumFaltas.Text);
            Params.ParamByName('@pe_num_bimestre').Value := cbBimestre.ItemIndex + 1;
            Params.ParamByName('@pe_num_ano').Value := GetValue(edAno.Text);
            Params.ParamByName('@pe_flg_aproveitamento').Value := rgAproveitamento.ItemIndex;
            Params.ParamByName('@pe_dsc_observacoes').Value := GetValue(edObservacoes.Text);
            Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Params.ParamByName('@pe_cod_escola').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_escola').Value;
            Params.ParamByName('@pe_dsc_escola_serie').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_escola_serie').Value;
            Execute;

            //Atualiza as Informa��es do Grid
            with dmDeca.cdsSel_Acompanhamento do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_nom_nome').Value := Null;
              Params.ParamByName('@pe_cod_unidade').Value := Null;
              Params.ParamByName('@pe_num_bimestre').Value := Null;
              Params.ParamByName('@pe_num_ano').Value := Null;
              Params.ParamByName('@pe_flg_aproveitamento').Value := Null;
              Params.ParamByName('@pe_cod_escola').Value := Null;
              Open;
              dbgAcompanhamentos.Refresh;
            end;
          end
        except end;
      end;

      AtualizaTelaAcompEscolar('Padrao');

    end;
  except end;
end;

procedure TfrmFichaCrianca.btnAlterarAcompanhamentoClick(Sender: TObject);
begin
  //Gravar a altera��o feita atrav�s do identificador da entidade Acompanhamento
  try
    with dmDeca.cdsAlt_Acompanhamento do
    begin
      //N�o se altera o ANO
      Close;
      Params.ParamByName('@pe_cod_id_acompanhamento').Value := dmDeca.cdsSel_Acompanhamento.FieldByName('cod_id_acompanhamento').Value;
      Params.ParamByName('@pe_num_dias_letivos').Value := GetValue(edDiasLetivos.Text);
      Params.ParamByName('@pe_num_faltas').Value := GetValue(edNumFaltas.Text);
      Params.ParamByName('@pe_flg_aproveitamento').Value := rgAproveitamento.ItemIndex;
      Params.ParamByName('@pe_dsc_observacoes').Value := GetValue(edObservacoes.Text);
      Execute;

      //Atualiza os dados do grid
      with dmDeca.cdsSel_Acompanhamento do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').Value := GetValue(frmFichaCrianca.txtMatricula.Text);
        Params.ParamByName('@pe_nom_nome').Value := Null;
        Params.ParamByName('@pe_cod_unidade').Value := Null;
        Params.ParamByName('@pe_num_bimestre').Value := Null;
        Params.ParamByName('@pe_num_ano').Value := Null;
        Params.ParamByName('@pe_flg_aproveitamento').Value := Null;
        Params.ParamByName('@pe_cod_escola').Value := Null;
        Open;

        dbgAcompanhamentos.Refresh;

      end;
    end;

    AtualizaTelaAcompEscolar('Padrao');

  except end
end;

procedure TfrmFichaCrianca.btnInicializarBimestreClick(Sender: TObject);
begin
  Application.CreateForm (TfrmInicializarBimestre, frmInicializarBimestre);
  frmInicializarBimestre.ShowModal;
end;

procedure TfrmFichaCrianca.btnDesligamentoClick(Sender: TObject);
begin
  Application.CreateForm (TfrmDesligamentoProgSocial, frmDesligamentoProgSocial);
  //frmDesligamentoProgSocial.ShowModal;

  if mmStatus = 'Ativa' then
  begin
    frmDesligamentoProgSocial.Caption := 'Ativar em Programa Social';
    frmDesligamentoProgSocial.GroupBox1.Caption := 'Informe a data da Ativa��o...';
    frmDesligamentoProgSocial.rgSituacao.ItemIndex := 2 //Ativo
  end
  else if mmStatus = 'Desliga' then
  begin
    frmDesligamentoProgSocial.Caption := 'Desligar de Programa Social';
    frmDesligamentoProgSocial.GroupBox1.Caption := 'Informe a data do Desligamento...';
    frmDesligamentoProgSocial.rgSituacao.ItemIndex := 2 //Desligado
  end;

  frmDesligamentoProgSocial.ShowModal;
  
  try
    with dmDeca.cdsSel_Cadastro_Programa_Social do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_cad_progsocial').Value := Null;
      Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
      Params.ParamByName('@pe_flg_status').Value := Null;
      Params.ParamByName('@pe_data_insercao1').Value := Null;
      Params.ParamByName('@pe_data_insercao2').Value := Null;
      Params.ParamByName('@pe_data_desligamento1').Value := Null;
      Params.ParamByName('@pe_data_desligamento2').Value := Null;
      Open;
      dbgProgramas.Refresh;
    end;
  except end;

  FStatus := fdNone;
  AtualizaCamposBotoes4;

end;

procedure TfrmFichaCrianca.dbgProgramasDblClick(Sender: TObject);
begin

  try

  //Verifica se o perfil tem permiss�o para altera��o dos dados
  //Assistente Social, Servi�o Social(Plant�o) ou Servi�o Social(Supervis�o)
  if (vvIND_PERFIL = 3) or (vvIND_PERFIL = 14) or (vvIND_PERFIL = 15) then
  begin
    //Verifica se o status � DESLIGADO
    if (dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('flg_status').Value = 2) then
    begin
      ShowMessage('O benefici�rio ' +
                  dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('dsc_nome').Value +
                  ' est� DESLIGADO do programa  ' +
                  dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('dsc_progsocial').Value +
                  ' !');
    end
    else
    //Se o status estiver como PRIORIT�RIO ou CADASTRADO - habilita o bot�o "Ativar"
    if (dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('flg_status').Value = 0)or
       ((dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('flg_status').Value = 3))then
    begin
      btnDesligamento.Enabled := False;
      btnAtivar.Enabled := True;
      btnCancelar4.Enabled := True;
      btnNovo4.Enabled := False;
      btnAlterar4.Enabled := False;
      btnSalvar4.Enabled := False;
      mmStatus := 'Outros';
    end //Se o status estiver como ATIVO - habilita o bot�o "Desligar"
    else if (dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('flg_status').Value = 1) then
    begin
      btnDesligamento.Enabled := True;
      btnAtivar.Enabled := False;
      btnCancelar4.Enabled := True;
      btnNovo4.Enabled := False;
      btnAlterar4.Enabled := False;
      btnSalvar4.Enabled := False;
      mmStatus := 'Desliga';
    end
    else if (dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('flg_status').Value = 1) then
    begin

    end;
  end
  else
  begin
    ShowMessage ('Aten��o!!! Seu perfil n�o tem acesso a esta opera��o...' + #13+#10+
                 'Entre em contato com o Administrador do Sistema.');  end;
  except end;


end;

procedure TfrmFichaCrianca.txtEnderecoEnter(Sender: TObject);
begin
  //Ao acessar o campo, se for novo registro nada faz... apenas grava os dados...
  //Se for altera��o de dados, no momento de gravar, pede-se a confirma��o para
  //altera��o do endere�o para todos da composi��o familiar     
  if FStatus = fdEdit then vMudaEnderecoFamilia := True
  else vMudaEnderecoFamilia := False;

end;

procedure TfrmFichaCrianca.ExcluirRegistro1Click(Sender: TObject);
begin

  try

    if (vvIND_PERFIL IN [18..19]) then
    begin
      with dmDeca.cdsExc_RegistroCadProgSocial do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_progsocial').AsInteger := dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName ('cod_id_cad_progsocial').AsInteger;
        Execute;

        with dmDeca.cdsSel_Cadastro_Programa_Social do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_cad_progsocial').Value := Null;
          Params.ParamByName ('@pe_cod_id_progsocial').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName ('@pe_flg_status').Value := Null;
          Params.ParamByName ('@pe_data_insercao1').Value := Null;
          Params.ParamByName ('@pe_data_insercao2').Value := Null;
          Params.ParamByName ('@pe_data_desligamento1').Value := Null;
          Params.ParamByName ('@pe_data_desligamento2').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').Value := Null;
          Open;
          dbgProgramas.Refresh;
        end;

        //Ao excluir o registro, verificar qual o �ltimo bimestre ativo/lan�ado e alterar o per�odo de acordo
        //com o que foi definido com o Acompanhamento Escolar...
        //Recuperar o �ltimo bimestre do aluno
        try
          with dmDeca.cdsSel_UltimoBimestreAluno do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(Copy(DateToStr(Date),7,4));
            Open;

            //Faz a altera��o do per�odo na ficha de cadastro em fun��o da escola
            try
              with dmDeca.cdsAlt_PeriodoCadastro do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);

                case dmDeca.cdsSel_UltimoBimestreAluno.FieldByName ('vPeriodoEscolar').Value of
                  0: Params.ParamByName ('@pe_dsc_periodo').Value := 'TARDE';
                  1: Params.ParamByName ('@pe_dsc_periodo').Value := 'MANH�';
                  2: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
                  3: Params.ParamByName ('@pe_dsc_periodo').Value := 'INTEGR/ESC';
                  4: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
                end;

                Execute;

              end;

            except
              Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                      'Um erro ocorreu ao tentar alterar o PER�ODO NA FICHA DE CADASTRO.' + #13+#10 +
                                      'Favor tentar novamente ou reportar ao Administrador do Sistema.',
                                      '[Sistema Deca] - Altera��o de PER�ODO CADASTRAL',
                                      MB_OK + MB_ICONINFORMATION);
              cbPeriodoEscolaE.SetFocus;
            end;

          end;
        except
        Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar consultar dados '+#13+#10+#13+#10+
                               'do �ltimo bimestre letivo do aluno. Favor consultar novamente...',
                               '[Sistema DECA] - Erro consulta bimestre.',
                               MB_OK + MB_ICONERROR);
        end;





      end
    end
    else
    begin
        Application.MessageBox ('Aten��o !!!' +#13+#10 +
                                'Essa opera��o s� pode ser executada pela equipe do Acompanhamento Escolar.' + #13+#10 +
                                'Favor entrar em contato com os mesmos.',
                                '[Sistema Deca] - Acompanhamento Escolar',
                                MB_OK + MB_ICONHAND);
    end;
  except end;
end;

procedure TfrmFichaCrianca.cbEscolaEClick(Sender: TObject);
var mmNOM_ESCOLA : String;
begin
  //Recupera o CODIGO DA ESCOLA clicada
  vvCOD_ESCOLA := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
  //if dmDeca.cdsSel_Escola.Locate('nom_escola', cbEscolaE.Items[cbEscola.ItemIndex], []) then
  //vvCOD_ESCOLA := dmDeca.cdsSel_Escola.FieldByName('cod_escola').AsInteger;


  {
  //Se for SEM ESCOLA, repassa SEM INFORMA��O DE S�RIE
  if vvCOD_ESCOLA = 209 then
  begin
   cbSerieE.Clear;
   cbSerieE.Items.Add('SEM INFORMACAO DE SERIE');
   cbSerieE.ItemIndex := 0;
   vvCOD_SERIE := 44
  end
  else
  begin
  }

  //vListaSerie1.Free;
  //vListaSerie2.Free;

  vListaSerie1 := TStringList.Create();
  vListaSerie2 := TStringList.Create();

  cbSerieE.Clear;
  //Consulta as s�ries escolares j� lan�adas para a escola e atualiza cbSerieE
  try
    with dmDeca.cdsSel_EscolaSerie do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value := vvCOD_ESCOLA;
      Params.ParamByName ('@pe_cod_id_serie').Value := Null;
      Open;

      if (dmDeca.cdsSel_EscolaSerie.RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerie1.Add   (IntToStr(FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add   (FieldByName ('dsc_Serie').Value);
          cbSerieE.Items.Add (FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end
      else
      begin
        mmNOM_ESCOLA := cbEscolaE.Text;
        Application.MessageBox ('Aten��o !!!' +#13+#10 +
                                'N�o existem s�ries cadastradas para a Escola selecionada' + #13+#10 +
                                'Favor verificar cadastro de S�ries por Escola.',
                                '[Sistema Deca] - Acompanhamento Escolar',
                                MB_OK + MB_ICONHAND);
        if Application.MessageBox ('Deseja ser direcionado para a tela de cadastro ' + #13+#10 +
                                   'de s�ries por escola agora?',
                                   '[Sistema Deca] - Direcionando para...',
                                   MB_YESNO + MB_ICONQUESTION) = id_yes then
        begin
          Application.CreateForm (TfrmCadastroSeriesPorEscola, frmCadastroSeriesPorEscola);
          frmCadastroSeriesPorEscola.ShowModal;
        end;

      end;
    end;

  except end;

  //Se for "SEM ESCOLA", atribuir "Sem informa��o" para:
  // - Frequencia Escolar
  // - Aproveitamento
  // - Situa��o
  if (vvCOD_ESCOLA = 209) then //cbEscolaE.Text = 'SEM ESCOLA' then
  begin
    cbFrequenciaE.ItemIndex := 2;
    cbAproveitamentoE.ItemIndex := 3;
    cbSituacaoE.ItemIndex := 7;
  end;

  //Caso escola = 218 - CENTRO ESTADUAL DE EDUCACAO SUPLETIVA
  if (vvCOD_ESCOLA = 218) then
    cbFrequenciaE.ItemIndex := 3;

  //Se NENHUMA DAS ANTERIORES, setar s�rie=SEM INFORMACAO; turma='-'; aproveitamento='SEM INFORMACAO'; frequencia='SEM INFORMACAO'; per�odo='SEM INFORMACAO' -> SETAR O FOCO NO CAMPO SITUACAO
end;

procedure TfrmFichaCrianca.cbSerieEClick(Sender: TObject);
begin
  //vvCOD_SERIE := StrToInt(vListaSerie1.Strings[cbSerieE.ItemIndex]);
  mmCOD_SERIE_ANTES := StrToInt(vListaSerie1.Strings[cbSerieE.ItemIndex]);
  //Se escolaridade for
  // - Telessala Ensino Medio
  // - Telessala Ensino Fundmental
  // atribuir valor 3 (Frequencia Flexivel) a Frequ�ncia Escolar
  if (vvCOD_SERIE = 31) OR (vvCOD_SERIE = 32) then
    cbFrequenciaE.ItemIndex := 3;

  vListaSerie2.Find(cbSerieE.Text, vIndSerie);
  mmCOD_SERIE_ATUAL := vIndSerie;

  if mmCOD_SERIE_ANTES = mmCOD_SERIE_ATUAL then
    vvMUDOU_SERIE := False
  else
    vvMUDOU_SERIE := True;

end;

procedure TfrmFichaCrianca.btnPrimeiraFichaClick(Sender: TObject);
begin
  lbMsgDefasagemEscolar.Caption := '';
  FillForm('F', '0');
  mmCOD_MATRICULA := Copy(txtMatricula.Text,1,8);
  FillFormEscolaridade;

  //Atualizar o grid de Hist�rico de Escolas do Aluno
  with dmDeca.cdsSel_HistoricoEscola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
    Params.ParamByName ('@pe_cod_escola').Value := Null;
    Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
    Open;
    dbgHistoricoEscolas.Refresh;
  end;

  if dmDeca.cdsSel_Cadastro_Move.eof then
  begin
    FillForm ('S',mmCOD_MATRICULA);
    Label8.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value + '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;
  end;
end;

procedure TfrmFichaCrianca.btnFichaAnteriorClick(Sender: TObject);
begin
  lbMsgDefasagemEscolar.Caption := '';
  FillForm('P', txtMatricula.Text);
  mmCOD_MATRICULA := Copy(txtMatricula.Text,1,8);
  FillFormEscolaridade;

  //Atualizar o grid de Hist�rico de Escolas do Aluno
  with dmDeca.cdsSel_HistoricoEscola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
    Params.ParamByName ('@pe_cod_escola').Value := Null;
    Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
    Open;
    dbgHistoricoEscolas.Refresh;
  end;


  if dmDeca.cdsSel_Cadastro_Move.eof then
  begin
    FillForm ('S',mmCOD_MATRICULA);
    Label8.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value + '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;
  end;
end;

procedure TfrmFichaCrianca.btnProximaFichaClick(Sender: TObject);
begin
  lbMsgDefasagemEscolar.Caption := '';
  FillForm('N', txtMatricula.Text);
  mmCOD_MATRICULA := Copy(txtMatricula.Text,1,8);
  FillFormEscolaridade;

  //Atualizar o grid de Hist�rico de Escolas do Aluno
  with dmDeca.cdsSel_HistoricoEscola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
    Params.ParamByName ('@pe_cod_escola').Value := Null;
    Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
    Open;
    dbgHistoricoEscolas.Refresh;
  end;


  if (dmDeca.cdsSel_Cadastro_Move.eof) then
  begin
    FillForm ('S',mmCOD_MATRICULA);
    Label8.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value + '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;
  end;
end;

procedure TfrmFichaCrianca.btnUltimaFichaClick(Sender: TObject);
begin
  lbMsgDefasagemEscolar.Caption := '';
  FillForm('L', '0');
  mmCOD_MATRICULA := Copy(txtMatricula.Text,1,8);
  FillFormEscolaridade;

  //Atualizar o grid de Hist�rico de Escolas do Aluno
  with dmDeca.cdsSel_HistoricoEscola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
    Params.ParamByName ('@pe_cod_escola').Value := Null;
    Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
    Open;
    dbgHistoricoEscolas.Refresh;
  end;


  if dmDeca.cdsSel_Cadastro_Move.eof then
  begin
    FillForm ('S',mmCOD_MATRICULA);
    Label8.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value + '-' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').Value;
  end;
end;

procedure TfrmFichaCrianca.btnAnteriorClick(Sender: TObject);
begin
  FillForm('P', txtMatricula.text);
  if dmDeca.cdsSel_Cadastro_Move.eof then FillForm ('S',txtMatricula.Text);
  FillFormFamiliares;
end;

procedure TfrmFichaCrianca.btnLocalizarFichaClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin

    frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);

    //Atualiza os dados do grid na matr�cula atual
    frmFichaCrianca.FillFormEscolaridade;

    //Atualizar o grid de Hist�rico de Escolas do Aluno
    with dmDeca.cdsSel_HistoricoEscola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
      Open;
      dbgHistoricoEscolas.Refresh;
    end;
    
  end;
end;

procedure TfrmFichaCrianca.btnNovoLancamentoEClick(Sender: TObject);
var vIndEscola, vIndSerie : Integer;
begin
  AtualizaTelaAcompEscolar('Novo');
  cbPeriodoEscola.ItemIndex := 4; //Sem Informa��o
  mmCOD_ESCOLA_ANTES := 0;
  edAnoLetivoE.Text := Copy(DateToStr(Date()),7,4);
  if (Length(edAnoLetivoE.Text) = 2) then edAnoLetivoE.Text := '20'+ edAnoLetivoE.Text;
  edAnoLetivoE.SetFocus;
  //cbSituacaoE.ItemIndex := 6;
  cbSituacaoE.ItemIndex := 8;
  cbBimestreE.ItemIndex := 0;

  Application.MessageBox ('Tela HABILITADA para receber novos valores de ' + #13+#10 +
                          'Acompanhamento Escolar do aluno. Favor preencher TODOS ' + #13+#10 +
                          'os campos da tela.',
                          '[Sistema Deca] - Novo Lan�amento de Acompanhamento Escolar',
                          MB_OK);
  mmNOVO_REGISTRO := True;

  //Repassar os c�digos de s�rie e escola para os combos, evitando que o usu�rio selecione novamente escola e s�rie
  //vListaEscola2.Find (dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').Value, vIndEscola);
  //cbEscolaE.ItemIndex := vIndEscola;
  //vListaSerie2.Find (dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_serie').Value, vIndSerie);
  //cbSerieE.ItemIndex := vIndSerie;
                                      
  cbFrequenciaE.ItemIndex := 2;     //Sem informa��o
  cbAproveitamentoE.ItemIndex := 3; //Sem informa��o

    //Localizar escola e repassar para o combo cbEscolaE...
    vListaEscola2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').AsString, vIndEscolaE);
    cbEscolaE.ItemIndex := vIndEscolaE;
    //Carrega a lista de s�ries da escola selecionada para localizar a serie
    //Recupera o CODIGO DA ESCOLA clicada
    //vvCOD_ESCOLA := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
    //cbSerieE.Clear;
    //Consulta as s�ries escolares j� lan�adas para a escola e atualiza cbSerieE
    cbSerieE.Clear;
    try
      with dmDeca.cdsSel_EscolaSerie do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
        Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;

        vListaSerie1.Clear;
        vListaSerie2.Clear;
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerie1.Add   (IntToStr(dmDeca.cdsSel_EscolaSerie.FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add   (dmDeca.cdsSel_EscolaSerie.FieldByName ('dsc_Serie').Value);
          cbSerieE.Items.Add (dmDeca.cdsSel_EscolaSerie.FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end;
    except end;

    //Localizar a s�rie escolar e repassar para o combo cbSerieE...
    vListaSerie2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_serie').AsString, vIndSerieE);
    cbSerieE.ItemIndex := vIndSerieE;

end;

procedure TfrmFichaCrianca.btnAlteraLancamentoEClick(Sender: TObject);
begin

  try
      with dmdeca.cdsAlt_AcompEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acompescolar').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_acompescolar').Value;
      Params.ParamByName ('@pe_cod_escola').Value          := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
      Params.ParamByName ('@pe_cod_id_serie').Value        := StrToInt(vListaSerie1.Strings[cbSerieE.ItemIndex]);
      Params.ParamByName ('@pe_ind_frequencia').Value      := cbFrequenciaE.ItemIndex;
      Params.ParamByName ('@pe_ind_aproveitamento').Value  := cbAproveitamentoE.ItemIndex;
      Params.ParamByName ('@pe_ind_situacao').Value        := cbSituacaoE.ItemIndex;
      Params.ParamByName ('@pe_ind_periodo_escolar').Value := cbPeriodoEscolaE.ItemIndex;
      Params.ParamByName ('@pe_dsc_observacoes').Value     := GetValue(edObservacaoE.Text);
      Params.ParamByName ('@pe_nom_turma').Value           := GetValue(edTurmaEscola.Text);
      Params.ParamByName ('@pe_cod_usuario').Value         := vvCOD_USUARIO;
      Execute;

      //Chama a atualiza��o do per�odo de cadastro
      vvMUDA_CADASTRO := True;
      AtualizaPeriodoCadastro;

      //Atualiza os dados ap�s feita a altera��o...
      with dmDeca.cdsSel_AcompEscolar do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
        Params.ParamByName ('@pe_cod_matricula').Value         := Copy(Label8.Caption,1,8);
        Params.ParamByName ('@pe_num_ano_letivo').Value        := Null;
        Params.ParamByName ('@pe_num_bimestre').Value          := Null;
        Params.ParamByName ('@pe_cod_unidade').Value           := Null;
        Params.ParamByname ('@pe_flg_defasagem_escolar').Value := Null;
        Open;
        dbgDadosAcompEscolar.Refresh;
      end;

      Application.MessageBox ('Dados atualizados com sucesso. ' + #13+#10 +
                              'Todos os valores informados na tela foram ' + #13+#10 +
                              'inseridos no banco de dados.',
                              '[Sistema Deca] - Atualiza��o de Dados',
                              MB_OK + MB_ICONINFORMATION);


      //Faz a inclus�o dos dados da escola no cadastro de Hist�rico de Escolas para a matr�cula e escola atuais
      //Fazer a valida��o:
      //      1. Quando alterar a escola sem alterar o ano letivo: ALTERAR O REGISTRO REFERENTE NO HISTORICO_ESCOLA
      //      2. Quando alterar a escola e o ano letivo: FAZ A INCLUS�O DO ANO E ESCOLA ATUAL NO HISTORICO_ESCOLA
      try
        //Se n�o mudar escola e n�o mudar s�rie...
        if (vvMUDOU_ESCOLA = False) and (vvMUDOU_SERIE = False) then

        begin
          //Se n�o mudou nem escola e nem s�rie, nada a fazer no Hist�rico de Escola...
          with dmDeca.cdsSel_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_escola').Value     := Null;
            Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
            Open;
            dbgHistoricoEscolas.Refresh;
          end;
        end

        else

        begin
          //Se mudar Escola e mudar S�rie...
          if (vvMUDOU_ESCOLA = True) and (vvMUDOU_SERIE = True) then
          begin

            //Alterar todas as escolas da matricula atual para "Escolas Anteriores"
            with dmDeca.cdsAlt_DadosUltimaEscolaHistoricoEscola do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);//dmDeca.cdsSel_HistoricoEscola.FieldByname('cod_id_hist_escola').AsInteger;
              Params.ParamByName ('@pe_flg_status').Value    := 1; //1 para escolas anteriores
              Execute;
            end;

            //Alterar os dados da "Escola Atual", nos campos Escola, S�rie, Data da �ltima Altera��o e Quem alterou...
            with dmDeca.cdsAlt_HistoricoEscola do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);//dmDeca.cdsSel_HistoricoEscola.FieldByname('cod_id_hist_escola').AsInteger;
              Params.ParamByName ('@pe_cod_escola').Value    := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
              Params.ParamByName ('@pe_dsc_serie').Value     := cbEscolaE.Text;
              Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
              Execute;
            end;

            //Incluir os dados da escola atual no HistoricoEscola
            with dmDeca.cdsInc_HistoricoEscola do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName ('@pe_cod_escola').Value := vvCOD_ESCOLA; //StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
              Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
              Params.ParamByName ('@pe_flg_status').Value := 0;  //Para "Escola Atual"
              Params.ParamByName ('@pe_dsc_serie').Value := cbSerieE.Text;//GetValue(cbSerieE.Items[cbSerie.ItemIndex]);
              //Inclu�dos na mudan�a de 26-mai-2009, os campos NUM_ANO_LETIVO e DSC_RG_ESCOLAR
              Params.ParamByName ('@pe_num_ano_letivo').Value := GetValue(edAnoLetivoE.Text);
              Execute;
            end;
          end

          else
          begin
            //Se mudou Escola ou n�o mudou S�rie  ou  n�o mudou Escola
            if ((vvMUDOU_ESCOLA = True) or (vvMUDOU_SERIE = False)) or
               ((vvMUDOU_ESCOLA = False) or (vvMUDOU_SERIE = True)) then
            begin

              //Alterar os dados da "Escola Atual", nos campos Escola, S�rie, Data da �ltima Altera��o e Quem alterou...
              with dmDeca.cdsAlt_HistoricoEscola do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);//dmDeca.cdsSel_HistoricoEscola.FieldByname('cod_id_hist_escola').AsInteger;
                Params.ParamByName ('@pe_cod_escola').Value    := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
                Params.ParamByName ('@pe_dsc_serie').Value     := cbSerieE.Text;
                Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
                Execute;
              end;

            end
          end;
        end;
      except end;

      AtualizaTelaAcompEscolar('Padrao');

      //Verifica valores para Defasagem Escolar - Carregar as imagens...
      //Altera o campo flg_defasagem_escolar de acordo com a Tabela de DefasagemEscolar...
      try
      with dmDeca.cdsSel_DefasagemEscolar do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_defasagemesc').Value := Null;
        Params.ParamByName ('@pe_cod_id_serie').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_serie').Value;
        Open;

        mmIDADE_IDEAL    := dmDeca.cdsSel_DefasagemEscolar.FieldByName ('num_idade_ideal').Value;
        mmIDADE_DEFASADA := dmDeca.cdsSel_DefasagemEscolar.FieldByName ('num_idade_defasada').Value;

        //Altera o valor do campo FLG_DEFASAGEM_ESCOLAR no Cadastro...
        try
        with dmDeca.cdsAlt_Cadastro_DefasagemEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          if (StrToInt(edIdadeAtual.Text) >= mmIDADE_IDEAL) AND (StrToInt(edIdadeAtual.Text) <= mmIDADE_DEFASADA) then
          begin
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := 0;
            //Exibe as imagens de acordo com as informa��es Defasagem...
            imgDefasagemEscNAO.Visible := True;
            imgDefasagemEscSIM.Visible := False;
            imgDefasagemEscNENHUMA.Visible := False;
          end
          else
          begin
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := 1;
            //Exibe as imagens de acordo com as informa��es Defasagem...
            imgDefasagemEscNAO.Visible := False;
            imgDefasagemEscSIM.Visible := True;
            imgDefasagemEscNENHUMA.Visible := False;

          end;

          Execute;  //Referente a cdsAlt_Cadastro_DefasagemEscolar...
          FillForm ('S',txtMatricula.Text)

        end;
        except
          Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                  'Ocorreu um erro ao tentar alterar situa��o de Defasagem ' + #13+#10 +
                                  'Escolar da Ficha de Cadastro Atual.' + #13+#10 +
                                  'Verifique o campo "Defasagens..." na Ficha de Cadastro.' + #13+#10 +
                                  'Qualquer d�vida entre em contato com o Administrador do Sistema.',
                                  '[Sistema Deca] - Acomp. de Situa��o Escolar',
                                  MB_OK + MB_ICONWARNING);
           AtualizaTelaAcompEscolar('Padrao');
        end;

      end;
      except
        Application.MessageBox ('Aten��o !!!' +#13+#10 +
                                'ocorreu um erro ao tentar atualizar os dados.' + #13+#10 +
                                'Verifique a conex�o com a internet e/ou cabos de rede.' + #13+#10 +
                                'Caso deseje, entre em contato com o Administrador do Sistema!',
                                '[Sistema Deca] - Erro Altera��o de Acompanhamento Escolar',
                                MB_OK + MB_ICONWARNING);
        AtualizaTelaAcompEscolar('Padr�o');
      end;
      //cbFrequenciaE.Color := clWhite;

      //Atualizar o grid de Hist�rico de Escolas do Aluno
      with dmDeca.cdsSel_HistoricoEscola do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
        Params.ParamByName ('@pe_cod_escola').Value := Null;
        Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
        Open;
        dbgHistoricoEscolas.Refresh;
      end;

  end;
  except end;

end;

procedure TfrmFichaCrianca.btnCancelaOperacaoEClick(Sender: TObject);
begin
   AtualizaTelaAcompEscolar('Padrao');
   cbFrequenciaE.Color := clWhite;
end;

procedure TfrmFichaCrianca.btnSairEClick(Sender: TObject);
begin
  frmFichaCrianca.Close;
end;

procedure TfrmFichaCrianca.dbgDadosAcompEscolar2DblClick(Sender: TObject);
//var
  //vIndEscolaE, vIndSerieE : Integer;
begin
//Verifica qual perfil para altera��o...
//vvIND_PERFIL = 1 E 18

{   Peixoto - 1� comentario em 26/02/2014
if ( ( vvIND_PERFIL <> 18 ) and ( vvIND_PERFIL <> 19 ) ) then //and ( dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').AsInteger = 209 )  then
//or ( dmDeca.cdsSel_AcompEscolar.FieldByName ('num_ano_letivo').AsInteger <> StrToInt( Copy(DatetOStr(Date()),7,4) ) )  then
begin
  Application.MessageBox ('Aten��o !!!' + #13+#10 +
                          'Altera��o do registro n�o pode ser efetuada a n�o ser pela equipe do Acompanhamento Escolar. ' + #13+#10 +
                          'Caso necessite, entre em contato com a Equipe do Acompanhamento Escolar.',
                          '[Sistema Deca]-Altera��o Acomp. Escolar',
                          MB_OK + MB_ICONWARNING);


end
else
begin
           Peixoto - fim 1� comentario  }

  if dmDeca.cdsSel_AcompEscolar.RecordCount < 1 then  //Se n�o houver registros para a ficha atual
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'N�o existem registros gravados para a Ficha Atual. ' + #13+#10 +
                            'Imposs�vel realizar opera��o de Altera��o de Dados.',
                            '[Sistema Deca]-Dados de Acomp.Escolar',
                            MB_OK + MB_ICONWARNING);
  end
  else
  begin

     AtualizaTelaAcompEscolar('Alterar');

     // PEIXOTO - 26/2/14 - ADICIONARO LINHA ABAIXO
      if ( ( vvIND_PERFIL <> 18 ) and ( vvIND_PERFIL <> 19 ) ) then
      begin
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Altera��o do registro n�o pode ser efetuada a n�o ser pela equipe do Acompanhamento Escolar. ' + #13+#10 +
                                'Caso necessite, entre em contato com a Equipe do Acompanhamento Escolar.',
                                '[Sistema Deca]-Altera��o Acomp. Escolar',
                                MB_OK + MB_ICONWARNING);

        AtualizaTelaAcompEscolar('Padrao');
        btnNovoLancamentoE.Enabled := False;  // Desabilita o bot�o de novo lancamento...
        btnAlteraLancamentoE.Enabled := False;  // Desabilita o bot�o de alterar lancamento...
        // DesabilitarComboAcEsc;   //  Desabilita os groupboxes do formulario que ser� carregado abaixo...

      end;
      // PEIXOTO - 26/2/14 - ADICIONARO ATE AQUI...

    //Repassa os valores para os devidos campos do formul�rio...
    //edNumRgEscolarE.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_rg_escolar').Value);
    edAnoLetivoE.Text    := dmDeca.cdsSel_AcompEscolar.FieldByName('num_ano_letivo').Value;
    if (Length(edAnoLetivoE.Text) < 4) then
    begin

    end;

    cbBimestreE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('num_bimestre').Value - 1;

    //Localizar escola e repassar para o combo cbEscolaE...
    vListaEscola2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').AsString, vIndEscolaE);
    cbEscolaE.ItemIndex := vIndEscolaE;

    //ShowMessage ('ESCOLA : ' + dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').AsString);
    //ShowMessage ('SERIE  : ' + dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_serie').AsString);

    //Carrega a lista de s�ries da escola selecionada para localizar a serie
    //Recupera o CODIGO DA ESCOLA clicada
    //vvCOD_ESCOLA := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
    //cbSerieE.Clear;
    //Consulta as s�ries escolares j� lan�adas para a escola e atualiza cbSerieE
    edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;
    cbSerieE.Clear;
    try
      with dmDeca.cdsSel_EscolaSerie do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
        Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;

        vListaSerie1.Clear;
        vListaSerie2.Clear;
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerie1.Add   (IntToStr(FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add   (FieldByName ('dsc_Serie').Value);
          cbSerieE.Items.Add (FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end;
    except end;

    //Localizar a s�rie escolar e repassar para o combo cbSerieE...
    vListaSerie2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_serie').AsString, vIndSerieE);
    cbSerieE.ItemIndex := vIndSerieE;

    cbFrequenciaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_frequencia').Value;
    //if dmDeca.cdsSel_AcompEscolar.FieldByName('ind_frequencia').Value = 0 then //Acima da M�dia
    //begin
    //  cbFrequenciaE.Color := clGreen;
    //  cbFrequenciaE.Font.Color := clWhite
    //end
    //else if dmDeca.cdsSel_AcompEscolar.FieldByName('ind_frequencia').Value = 1 then //Abaixo da M�dia
    //begin
    //  cbFrequenciaE.Color := clRed;
    //  cbFrequenciaE.Font.Color := clWhite
    //end;

    cbAproveitamentoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_aproveitamento').Value;
    cbSituacaoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_situacao').Value;
    cbPeriodoEscolaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_periodo_escolar').Value;
    edObservacaoE.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_observacoes').Value;

    if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString)=0) then
      edTurmaEscola.Text := ''
    else
      edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;

    vvMUDOU_ESCOLA := False;

    Panel13.Enabled := True;
    Image1.Enabled  := True;
    Image2.Enabled  := True;

    mmBIMESTRE_SELECIONADO := 0;
    mmBIMESTRE_SELECIONADO := dmDeca.cdsSel_AcompEscolar.FieldByName('num_bimestre').Value;

  end;
//end;    Peixoto 2� comentario 26/2/14

end;

procedure TfrmFichaCrianca.btnGravaLancamentoEClick(Sender: TObject);
var mmPERIODO : Integer;
begin
  //Validar campos obrigat�rios

  //Antes de inserir um registro, validar dados de bimestre/ano,
  try
    //Verifica se existem lan�amento para Mat/Bim/Ano
    with dmDeca.cdsSel_AcompEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Copy(Label8.Caption,1,8);
      Params.ParamByName ('@pe_num_ano_letivo').Value := GetValue(edAnoLetivoE.Text);
      Params.ParamByName ('@pe_num_bimestre').Value := cbBimestreE.ItemIndex + 1;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByname ('@pe_flg_defasagem_escolar').Value := Null;
      Open;

      //Se existir, informe ao usu�rio e cancela a opera��o...
      if dmDeca.cdsSel_AcompEscolar. RecordCount > 0 then
      begin
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Os dados informados j� se encontram cadastrados no banco de dados.' + #13+#10 +
                                'Caso deseje alter�-los, clique duas vezes no grid, correspondente ao Bimestre/Ano' + #13+#10 +
                                'e fa�a as devidas altera��es. Caso deseje, entre em contato com o Administrador ' + #13+#10 +
                                'do Sistema e esclare�a as suas d�vidas.' ,
                                '[Sistema Deca] - Erro de Inclus�o de Acomp. Escolar',
                                MB_OK + MB_ICONWARNING);
        AtualizaTelaAcompEscolar('Padrao');
      end
      else //Caso n�o exista cadastrado...
      begin

        if (Length(edAnoLetivoE.Text) < 4) then
        begin
          Application.MessageBox ('Aten��o!!! O campo com o valor ANO est� incorreto. Favor corrigir.',
                                  '[Sistema Deca] - Valor de Campo ANO inv�lido...',
                                  MB_OK + MB_ICONERROR);
          edAnoLetivoE.SetFocus;
        end
        else

        begin

        with dmDeca.cdsInc_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value       := Copy(Label8.Caption,1,8);
          Params.ParamByName ('@pe_num_ano_letivo').Value      := GetValue(edAnoLetivoE.Text);
          Params.ParamByName ('@pe_num_bimestre').Value        := cbBimestreE.ItemIndex + 1;
          Params.ParamByName ('@pe_cod_escola').AsInteger      := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
          Params.ParamByName ('@pe_cod_id_serie').AsInteger    := StrToInt(vListaSerie1.Strings[cbSerieE.ItemIndex]);
          Params.ParamByName ('@pe_ind_frequencia').Value      := cbFrequenciaE.ItemIndex;
          Params.ParamByName ('@pe_ind_aproveitamento').Value  := cbAproveitamentoE.ItemIndex;
          Params.ParamByName ('@pe_ind_situacao').Value        := cbSituacaoE.ItemIndex;
          Params.ParamByName ('@pe_ind_periodo_escolar').Value := cbPeriodoEscolaE.ItemIndex;
          Params.ParamByName ('@pe_dsc_observacoes').Value     := GetValue(edObservacaoE.Text);
          Params.ParamByName ('@pe_cod_usuario').Value         := vvCOD_USUARIO;
          Params.ParamByName ('@pe_nom_turma').Value           := GetValue(edTurmaEscola.Text);
          Execute;

          mmPERIODO := cbPeriodoEscolaE.ItemIndex;

          Application.MessageBox ('Dados inseridos com sucesso. ' + #13+#10 +
                                  'Todos os valores informados na tela foram inseridos no banco de dados.',
                                  '[Sistema Deca] - Inclus�o de Dados',
                                  MB_OK + MB_ICONINFORMATION);

          vvMUDA_CADASTRO := True;
          AtualizaPeriodoCadastro;

          {
          //Recuperar o �ltimo bimestre lan�ado para a matr�cula atual
          with dmDeca.cdsSel_UltimoBimestre do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_num_ano').Value       := dmDeca.cdsSel_AcompEscolar.FieldByName ('num_ano_letivo').Value;
            Open;
          end;

          if (mmBIMESTRE_SELECIONADO = dmDeca.cdsSel_UltimoBimestre.FieldByName ('UltBim').Value) or (dmDeca.cdsSel_UltimoBimestre.RecordCount = 0) then
          begin
            //Permite a altera��o dos dados e faz a mudan�a de acordo com a regra o per�odo escolar e no cadastro.
            with dmDeca.cdsAlt_PeriodoCadastro do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);

              //Caso o per�odo da Escola no bimestre atual for Noite, manter o per�odo do Cadastro   -  28/05/2015
              if (dmDeca.cdsSel_Cadastro.FieldByName ('ind_status').Value <> 4) and (dmDeca.cdsSel_UltimoBimestre.FieldByName ('ind_periodo_escolar').Value = 2) then
                Params.ParamByName ('@pe_dsc_periodo').Value := cbPeriodo.Text;

              if (cbEscolaE.Text = 'SEM ESCOLA') and (dmDeca.cdsSel_Cadastro.FieldByName ('ind_status').Value = 4) then
              begin
                //Params.ParamByName ('@pe_dsc_periodo').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value
                Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>'
              end
              else if (cbEscolaE.Text = 'SEM ESCOLA') then
                Params.ParamByName ('@pe_dsc_periodo').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value
              else if (dmDeca.cdsSel_Cadastro.FieldByName ('ind_status').Value = 4) and (dmDeca.cdsSel_UltimoBimestre.FieldByName ('ind_periodo_escolar').Value = 3) then //3=Integral
                Params.ParamByName ('@pe_dsc_periodo').Value := 'INTEGR/ESC'
              else if (dmDeca.cdsSel_Cadastro.FieldByName ('ind_status').Value = 4) and (dmDeca.cdsSel_UltimoBimestre.FieldByName ('ind_periodo_escolar').Value <> 3) then //3=Integral
              begin
                Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>'
              end else //if (dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value <> 'Noite') then  //Acrescentado a pedido do Acompanhamento Escolar em 27/11/2014
              if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value <> 2) then
              begin
                case cbPeriodoEscolaE.ItemIndex of
                  0: Params.ParamByName ('@pe_dsc_periodo').Value := 'TARDE';
                  1: Params.ParamByName ('@pe_dsc_periodo').Value := 'MANH�';
                  //2: Params.ParamByName ('@pe_dsc_periodo').Value := Trim(cbPeriodo.Text);//'<ND>';
                  3: Params.ParamByName ('@pe_dsc_periodo').Value := 'INTEGR/ESC';
                  4: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
                end;
              end; //else Params.ParamByName ('@pe_dsc_periodo').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;
              Execute;
            end;

            //Atualizar o Campo SEM ESCOLA no cadastro de acordo com a escola selecionada
            with dmDeca.cdsUpd_SemEscola_Cadastro do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
              if (cbEscolaE.Text = 'SEM ESCOLA') then
                Params.ParamByName ('@pe_flg_sem_escola').Value := 1 //Sem Escola
              else
                Params.ParamByName ('@pe_flg_sem_escola').Value := 0; //Com Escola
              Execute;
            end;

          end else if (mmBIMESTRE_SELECIONADO < dmDeca.cdsSel_UltimoBimestre.FieldByName ('UltBim').Value) then
          begin

          end;
          }

          //Atualiza os dados do grid, com informa��es dos dados lan�ados para a matr�cula atual
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
            Params.ParamByName ('@pe_cod_matricula').Value := Copy(Label8.Caption,1,8);
            Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
            Params.ParamByName ('@pe_num_bimestre').Value := Null;
            Params.ParamByName ('@pe_cod_unidade').Value := Null;
            Open;
            dbgDadosAcompEscolar.Refresh;
          end;

          //Faz a inclus�o dos dados da escola no cadastro de Hist�rico de Escolas para a matr�cula e escola atuais
          //Fazer a valida��o:
          //      1. Quando alterar a escola sem alterar o ano letivo    : ALTERAR O REGISTRO REFERENTE NO HISTORICO_ESCOLA
          //      2. Quando alterar a escola e o ano letivo              : FAZ A INCLUS�O DO ANO E ESCOLA ATUAL NO HISTORICO_ESCOLA
          //      3. Quando incluir registro para o pr�ximo o ano letivo : FAZ A INCLUS�O DO ANO E ESCOLA ATUAL NO HISTORICO_ESCOLA (Implementar)
          try
            //Verificar se mudou de ANO LETIVO
            if (vvMUDOU_ESCOLA = True) or (vvMUDOU_ANOLETIVO = True) or (mmNOVO_REGISTRO = True) then
            begin

              //Alterar todas as escolas da matricula atual para "Escolas Anteriores"
              //with dmDeca.cdsAlt_HistoricoEscola do
              with dmDeca.cdsAlt_DadosUltimaEscolaHistoricoEscola do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);//dmDeca.cdsSel_HistoricoEscola.FieldByname('cod_id_hist_escola').AsInteger;
                Params.ParamByName ('@pe_flg_status').Value    := 1; //1 para escolas anteriores
                Execute;
              end;

              //Incluir os dados da escola atual no HistoricoEscola
              with dmDeca.cdsInc_HistoricoEscola do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Params.ParamByName ('@pe_cod_escola').Value := StrToInt(vListaEscola1.Strings[cbEscolaE.ItemIndex]);
                Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                Params.ParamByName ('@pe_flg_status').Value := 0;  //Para "Escola Atual"
                Params.ParamByName ('@pe_dsc_serie').Value := cbSerieE.Text;//GetValue(cbSerieE.Items[cbSerie.ItemIndex]);
                //Inclu�dos na mudan�a de 26-mai-2009, os campos NUM_ANO_LETIVO e DSC_RG_ESCOLAR
                Params.ParamByName ('@pe_num_ano_letivo').Value := GetValue(edAnoLetivoE.Text);
                Execute;

                //Ao inserir, atualiza o grid de Historico_Escola
                with dmDeca.cdsSel_HistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                  Params.ParamByName ('@pe_cod_escola').Value := Null;
                  Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
                  Open;
                  dbgHistoricoEscolas.Refresh;
                end;

              end;
            end
          except end;

          mmNOVO_REGISTRO := False;

          //Altera o campo flg_defasagem_escolar de acordo com a Tabela de DefasagemEscolar...
          try
          with dmDeca.cdsSel_DefasagemEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_defasagemesc').Value := Null;
            Params.ParamByName ('@pe_cod_id_serie').Value := StrToInt(vListaSerie1.Strings[cbSerieE.ItemIndex]);//dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_serie').Value;
            Open;

            mmIDADE_IDEAL    := dmDeca.cdsSel_DefasagemEscolar.FieldByName ('num_idade_ideal').Value;
            mmIDADE_DEFASADA := dmDeca.cdsSel_DefasagemEscolar.FieldByName ('num_idade_defasada').Value;

            //Altera o valor do campo FLG_DEFASAGEM_ESCOLAR no Cadastro...
            try
            with dmDeca.cdsAlt_Cadastro_DefasagemEscolar do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              if (StrToInt(edIdadeAtual.Text) >= mmIDADE_IDEAL) AND (StrToInt(edIdadeAtual.Text) <= mmIDADE_DEFASADA) then
              begin
                Params.ParamByName ('@pe_flg_defasagem_escolar').Value := 0; //N�o apresenta Defasagem Escolar
              end
              else
              begin
                Params.ParamByName ('@pe_flg_defasagem_escolar').Value := 1; //Apresenta Defasagem Escolar
              end;

              Execute;  //Referente a cdsAlt_Cadastro_DefasagemEscolar...

              //Atualiza os dados do educacenso para uma nova matr�cula








            end;
            except
              Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                      'Ocorreu um erro ao tentar alterar situa��o de Defasagem ' + #13+#10 +
                                      'Escolar da Ficha de Cadastro Atual.' + #13+#10 +
                                      'Verifique o campo "Defasagens..." na Ficha de Cadastro.' + #13+#10 +
                                      'Qualquer d�vida entre em contato com o Administrador do Sistema.',
                                      '[Sistema Deca] - Acomp. de Situa��o Escolar',
                                      MB_OK + MB_ICONWARNING);
               AtualizaTelaAcompEscolar('Padrao');
            end;
          end;
          except end;
        AtualizaTelaAcompEscolar('Padrao');
        end;
        end;  //Finaliza o la�o (if) para valida��o do campo ANO...
      end;

    end;
  except
    //Caso ocorra algum erro de acesso ao banco de dados
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu ao tentar acessar o Banco de Dados...' + #13+#10 +
                            'Verifique a conex�o com a internet ou os cabos de rede se foram desconectados...' + #13+#10 +
                            'Caso deseje entre em contato com o Administrador do Sistema!',
                            '[Sistema Deca]-Acomp. Escolar "ERRO"',
                            MB_OK + MB_ICONWARNING);
    AtualizaTelaAcompEscolar('Padrao');
  end;
  cbFrequenciaE.Color := clWhite;
end;

procedure TfrmFichaCrianca.edAnoLetivoEEnter(Sender: TObject);
begin
  mmANOLETIVO_ANTES := StrToInt(edAnoLetivoE.Text);
end;

procedure TfrmFichaCrianca.edAnoLetivoEExit(Sender: TObject);
begin
  mmANOLETIVO_ATUAL := StrToInt(edAnoLetivoE.Text);
  if mmANOLETIVO_ANTES = mmANOLETIVO_ATUAL then
    vvMUDOU_ANOLETIVO := False
  else
    vvMUDOU_ANOLETIVO := True;
end;

procedure TfrmFichaCrianca.cbEscolaEEnter(Sender: TObject);
begin
  if mmCOD_ESCOLA_ANTES = 0 then
    mmCOD_ESCOLA_ANTES := 0
  else
    mmCOD_ESCOLA_ANTES := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
end;

procedure TfrmFichaCrianca.Excluirregistro2Click(Sender: TObject);
begin
  try
    if (vvIND_PERFIL in [18..19]) then
    begin
      //Pede confirmacao do usuario antes de excluir
      if Application.MessageBox ('Deseja realmente excluir o registro atual?' + #13+#10 +
                                 'Essa exclus�o ser� definitiva. Confirma?',
                                 '[Sistema Deca] - Exclus�o de Registro',
                                 MB_YESNO + MB_ICONQUESTION) = ID_YES then
      begin
        with dmDeca.cdsExc_AcompEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_acompescolar').Value;
          Execute;
          Application.MessageBox ('Registro exclu�do com sucesso...!!!',
                                  '[Sistema Deca] - Sucesso!!!',
                                  MB_OK + MB_ICONINFORMATION);
          //Atualiza os dados do grid referente a ficha atual
          FillFormEscolaridade;
        end;
      end;
    end;
    except end;
   
end;

procedure TfrmFichaCrianca.Image1Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      Application.CreateForm(TfrmVerDadosEscolaSelecionada, frmVerDadosEscolaSelecionada);
      frmVerDadosEscolaSelecionada.edNomeEscola.Text        := GetValue(dmDeca.cdsSel_Escola.FieldByName ('nom_escola').AsString);
      frmVerDadosEscolaSelecionada.edEnderecoEscola.Text    := GetValue(dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').AsString);
      frmVerDadosEscolaSelecionada.edbairroEscola.Text      := GetValue(dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').AsString);
      frmVerDadosEscolaSelecionada.edCepEscola.Text         := GetValue(dmDeca.cdsSel_Escola.FieldByName ('num_cep').AsString);
      frmVerDadosEscolaSelecionada.edTelefoneEscola.Text    := GetValue(dmDeca.cdsSel_Escola.FieldByName ('num_telefone').AsString);
      frmVerDadosEscolaSelecionada.edDiretorEscola.Text     := GetValue(dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').AsString);
      frmVerDadosEscolaSelecionada.edEmailEscola.Text       := GetValue(dmDeca.cdsSel_Escola.FieldByName ('dsc_email').AsString);
      frmVerDadosEscolaSelecionada.ShowModal;
      
    end;
  except end;
end;

procedure TfrmFichaCrianca.Image2Click(Sender: TObject);
begin
  //Verifica se a escola a ser consultada � Municipal ou Estadual...
  try
    if dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_tipo_publica').Value = 'E' then
    begin
      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'Uma janela do navegador de internet do sistema ser� aberta agora.',
                              '[Sistema Deca] - Acesso Boletim Eletr�nico',
                              MB_OK + MB_ICONINFORMATION);
      Application.CreateForm (TfrmNavegador, frmNavegador);
      //frmNavegador.URLs.Text := 'http://gdaenet.edunet.sp.gov.br:8080/gdae/pages/boletim/JINT/inEscolasDoAlunoWeb.jsp';
      //frmNavegador.URLs.Text := 'http://www.educacao.sp.gov.br/portal/area-reservada/pais-e-alunos/boletim';
      //frmNavegador.URLs.Text := 'https://gdaenet.edunet.sp.gov.br/boletim/';
      frmNavegador.URLs.Text := 'https://sed.educacao.sp.gov.br/Boletim/BoletimEscolar';
      frmNavegador.StatusBar1.Panels[0].Text := 'RA N.�: ' + UpperCase(edNumRGEscolarE.Text) + '  | Nome Aluno: ' + UpperCase(txtNome.Text) + ' | Data Nascimento: ' + txtNascimento.Text;
      frmNavegador.Show;
    end
    else
    //N�o � escola Estadual, pois ent�o n�o ser� aberto o navegador com o endere�o
    //do GDAENET - Secret. Educacao do Estado de S�o Paulo....
    begin
      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'O aluno n�o est� matriculado em uma Escola Estadual.' + #13+#10 +
                              'A janela do boletim n�o ser� exibida para o aluno.',
                              '[Sistema Deca] - Acesso Negado ao Boletim Eletr�nico',
                              MB_OK + MB_ICONINFORMATION);
    end;

  except end;






end;

procedure TfrmFichaCrianca.dbgDadosAcompEscolar2DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var Icon : TBitMap;
begin

  Icon := TBitMap.Create();
  if (Column.FieldName = 'vFrequencia') then
  begin
    with dbgDadosAcompEscolar.Canvas do 
    begin

      Brush.Color := clWhite;
      FillRect(Rect);

      if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value = 0) then
        ImageList1.GetBitmap(1, Icon)
      else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value = 1) then
        ImageList1.GetBitmap(0, Icon)
      else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value = 2) then
        ImageList1.GetBitmap(3, Icon)
      else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value = 4) then
        ImageList1.GetBitmap(2, Icon);
      Draw(round((Rect.Left + Rect.Right - Icon.Width) / 2), Rect.Top, Icon);
    end;
  end;

  if (Column.FieldName = 'vAproveitamento') then
  begin
    with dbgDadosAcompEscolar.Canvas do
    begin

      Brush.Color := clWhite;
      FillRect(Rect);

      if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value = 0) then
        ImageList1.GetBitmap(0, Icon)
      else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value = 1) then
        ImageList1.GetBitmap(2, Icon)
      else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value = 2) then
        ImageList1.GetBitmap(1, Icon)
      else if (dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value = 3) then
        ImageList1.GetBitmap(3, Icon);
      Draw(round((Rect.Left + Rect.Right - Icon.Width) / 2), Rect.Top, Icon);
    end;
  end;



end;

procedure TfrmFichaCrianca.imgLegendaFreqAprovClick(Sender: TObject);
begin
  Application.CreateForm (TfrmLegendaAcompEscolar, frmLegendaAcompEscolar);
  frmLegendaAcompEscolar.ShowModal;
end;

procedure TfrmFichaCrianca.dbgHistoricoEscolas2CellClick(Column: TColumn);
begin
  lbAnoLetivo.Caption     := dmDeca.cdsSel_HistoricoEscola.FieldByName ('num_ano_letivo').Value;
  lbEscola.Caption        := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value;
  lbSerie.Caption         := dmDeca.cdsSel_HistoricoEscola.FieldByName ('dsc_serie').Value;
  lbSituacao.Caption      := dmDeca.cdsSel_HistoricoEscola.FieldByName ('vStatus').Value;
  lbDataInclusao.Caption  := dmDeca.cdsSel_HistoricoEscola.FieldByName ('vDataInclus�o').Value;

  if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('vDataAlteracao').IsNull) then
    lbDataAlteracao.Caption := ''
  else
    lbDataAlteracao.Caption := dmDeca.cdsSel_HistoricoEscola.FieldByName ('vDataAlteracao').Value;

  lbUsuario.Caption       := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_usuario').Value;
end;

procedure TfrmFichaCrianca.TabSheet10Show(Sender: TObject);
begin
  lbAnoLetivo.Caption     := '';
  lbEscola.Caption        := '';
  lbSerie.Caption         := '';
  lbSituacao.Caption      := '';
  lbDataInclusao.Caption  := '';
  lbDataAlteracao.Caption := '';
  lbUsuario.Caption       := '';

  with dmDeca.cdsSel_HistoricoEscola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
    Params.ParamByName ('@pe_cod_escola').Value     := Null;
    Params.ParamByName ('@pe_num_ano_letivo').Value := Null;
    Open;
    dbgHistoricoEscolas.Refresh;
  end;
end;

procedure TfrmFichaCrianca.Label32Click(Sender: TObject);
begin
  FillFormEscolaridade;
  PageControl1.ActivePageIndex := 2;
end;

procedure TfrmFichaCrianca.cbSerieEEnter(Sender: TObject);
begin
  if mmCOD_SERIE_ANTES = 0 then
    mmCOD_SERIE_ANTES := 0
  else
    //mmCOD_SERIE_ANTES := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_serie').Value;
    mmCOD_SERIE_ANTES := mmCOD_SERIE_ANTES;
end;

procedure TfrmFichaCrianca.btnAlteraEnderecoClick(Sender: TObject);
begin
  Application.CreateForm (TfrmAlteracaoEndereco, frmAlteracaoEndereco);

  //Repassa os valores necess�rios para os campos do formul�rio de altera��o de endere�os...
  //Validar campo Complemento em branco...
  frmAlteracaoEndereco.lbMatricula.Caption := GetValue(txtMatricula, vtString);
  frmAlteracaoEndereco.lbNome.Caption      := GetValue(txtNome, vtString);
  frmAlteracaoEndereco.edEndereco.Text     := GetValue(txtEndereco, vtString);

  if (Length(txtComplemento.Text)=0) then
    frmAlteracaoEndereco.edComplemento.Text := ''
  else
    frmAlteracaoEndereco.edComplemento.Text  := GetValue(txtComplemento, vtString);

  frmAlteracaoEndereco.mskCep.Text         := GetValue(txtCep, vtString);
  
  //frmAlteracaoEndereco.edBairro.Text       := GetValue(txtBairro.Text);
  if (Length(txtBairro.Text)=0) then
    frmAlteracaoEndereco.edBairro.Text := ''
  else
    frmAlteracaoEndereco.edBairro.Text  := GetValue(txtBairro, vtString);

  frmAlteracaoEndereco.Fotografia.Picture  := Fotografia.Picture;
  frmAlteracaoEndereco.cbCotasPasse.Text   := GetValue(txtCotas.Text);

  frmAlteracaoEndereco.ShowModal;
end;

procedure TfrmFichaCrianca.cbSituacaoEDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin

  with cbSituacaoE.Canvas do
  begin
    FillRect(Rect);
    //verifica se � o indice a ser desabilitado
    //if (Index = 2) or (Index = 3) or (Index = 8) then
    //if (Index = 3) or (Index = 8) then
    if (Index = 3) then
    begin
      //Cor cinza
      Font.Color := clGray;
      //cor branca para o retangulo de sele��o
      if (odSelected in State) then
      begin
        Brush.Color := clWhite;
        FillRect(Rect);
      end;
    end
    else
    begin
      if (odSelected in State) then
      begin
        Font.Color := clWhite;
      end
      else
        Font.Color := clBlack;
      end;
      TextOut(Rect.Left, Rect.Top, cbSituacaoE.Items.Strings[Index]);
    end;
end;

procedure TfrmFichaCrianca.cbSituacaoEChange(Sender: TObject);
begin

  //if (cbSituacaoE.ItemIndex = 2) or (cbSituacaoE.ItemIndex = 3) or (cbSituacaoE.ItemIndex = 8) then
  //if (cbSituacaoE.ItemIndex = 3) or (cbSituacaoE.ItemIndex = 8) then
  if (cbSituacaoE.ItemIndex = 3) then
  begin
    cbSituacaoE.ItemIndex := -1;
    cbSituacaoE.DroppedDown := True;
  end;
end;
procedure TfrmFichaCrianca.dbgDadosAcompEscolar2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

    edAnoLetivoE.Text    := dmDeca.cdsSel_AcompEscolar.FieldByName('num_ano_letivo').Value;
    cbBimestreE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('num_bimestre').Value - 1;

    vListaEscola2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').AsString, vIndEscolaE);
    cbEscolaE.ItemIndex := vIndEscolaE;

    edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;
    cbSerieE.Clear;
    try
      with dmDeca.cdsSel_EscolaSerie do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
        Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;

        vListaSerie1.Clear;
        vListaSerie2.Clear;
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerie1.Add   (IntToStr(FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add   (FieldByName ('dsc_Serie').Value);
          cbSerieE.Items.Add (FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end;
    except end;

    vListaSerie2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_serie').AsString, vIndSerieE);
    cbSerieE.ItemIndex := vIndSerieE;

    cbFrequenciaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_frequencia').Value;

    cbAproveitamentoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_aproveitamento').Value;
    cbSituacaoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_situacao').Value;
    cbPeriodoEscolaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_periodo_escolar').Value;
    edObservacaoE.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_observacoes').Value;

    if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString)=0) then
      edTurmaEscola.Text := ''
    else
      edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;

    vvMUDOU_ESCOLA := False;

    Panel13.Enabled := True;
    Image1.Enabled  := True;
    Image2.Enabled  := True;    
end;

procedure TfrmFichaCrianca.dbgDadosAcompEscolar2KeyPress(Sender: TObject;
  var Key: Char);
begin

    cbBimestreE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('num_bimestre').Value - 1;
    vListaEscola2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').AsString, vIndEscolaE);
    cbEscolaE.ItemIndex := vIndEscolaE;

    edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;
    cbSerieE.Clear;
    try
      with dmDeca.cdsSel_EscolaSerie do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
        Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;

        vListaSerie1.Clear;
        vListaSerie2.Clear;
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerie1.Add   (IntToStr(FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add   (FieldByName ('dsc_Serie').Value);
          cbSerieE.Items.Add (FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end;
    except end;

    cbAproveitamentoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_aproveitamento').Value;
    cbSituacaoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_situacao').Value;
    cbPeriodoEscolaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_periodo_escolar').Value;
    edObservacaoE.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_observacoes').Value;

    if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString)=0) then
      edTurmaEscola.Text := ''
    else
      edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;

    vvMUDOU_ESCOLA := False;

    Panel13.Enabled := True;
    Image1.Enabled  := True;
    Image2.Enabled  := True;

end;

procedure TfrmFichaCrianca.cbPeriodoEscolaEClick(Sender: TObject);
begin
  //Faz a altera��o do per�odo na ficha de cadastro em fun��o da escola
 {
  try
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);

        case cbPeriodoEscolaE.ItemIndex of
          0: Params.ParamByName ('@pe_dsc_periodo').Value := 'TARDE';
          1: Params.ParamByName ('@pe_dsc_periodo').Value := 'MANH�';
          2: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
          3: Params.ParamByName ('@pe_dsc_periodo').Value := 'INTEGR/ESC';
          4: Params.ParamByName ('@pe_dsc_periodo').Value := '<ND>';
        end;

        Execute;

      end;

  except
      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'Um erro ocorreu ao tentar alterar o PER�ODO NA FICHA DE CADASTRO.' + #13+#10 +
                              'Favor tentar novamente ou reportar ao Administrador do Sistema.',
                              '[Sistema Deca] - Altera��o de PER�ODO CADASTRAL',
                              MB_OK + MB_ICONINFORMATION);
      cbPeriodoEscolaE.SetFocus;
  end;
      }

end;

procedure TfrmFichaCrianca.cbPeriodoDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin


  with cbPeriodo.Canvas do
  begin
    FillRect(Rect);
    //verifica se � o indice a ser desabilitado
    if (Index = 2) or (Index = 3) then
    begin
      //Cor cinza
      Font.Color := clGray;
      //cor branca para o retangulo de sele��o
      if (odSelected in State) then
      begin
        Brush.Color := clWhite;
        FillRect(Rect);
      end;
    end
    else
    begin
      if (odSelected in State) then
      begin
        Font.Color := clWhite;
      end
      else
        Font.Color := clBlack;
      end;
      TextOut(Rect.Left, Rect.Top, cbPeriodo.Items.Strings[Index]);
    end;


end;

procedure TfrmFichaCrianca.cbPeriodoChange(Sender: TObject);
begin
  if (cbPeriodo.ItemIndex = 2) or (cbPeriodo.ItemIndex = 3) then
  begin
    cbPeriodo.ItemIndex := -1;
    cbPeriodo.DroppedDown := True;
  end;
end;

procedure TfrmFichaCrianca.dbgDadosAcompEscolar2CellClick(Column: TColumn);
begin

    GroupBoxE1.Enabled    := True;
    edAnoLetivoE.Text     := dmDeca.cdsSel_AcompEscolar.FieldByName('num_ano_letivo').Value;
    cbBimestreE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('num_bimestre').Value - 1;

    vListaEscola2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_escola').AsString, vIndEscolaE);
    cbEscolaE.ItemIndex := vIndEscolaE;

    edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;
    cbSerieE.Clear;
    try
      with dmDeca.cdsSel_EscolaSerie do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
        Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;

        vListaSerie1.Clear;
        vListaSerie2.Clear;
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerie1.Add   (IntToStr(FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add   (FieldByName ('dsc_Serie').Value);
          cbSerieE.Items.Add (FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end;
    except end;

    vListaSerie2.Find(dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_serie').AsString, vIndSerieE);
    cbSerieE.ItemIndex := vIndSerieE;

    cbFrequenciaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_frequencia').Value;

    cbAproveitamentoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_aproveitamento').Value;
    cbSituacaoE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_situacao').Value;
    cbPeriodoEscolaE.ItemIndex := dmDeca.cdsSel_AcompEscolar.FieldByName('ind_periodo_escolar').Value;
    edObservacaoE.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('dsc_observacoes').Value;

    if (Length(dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString)=0) then
      edTurmaEscola.Text := ''
    else
      edTurmaEscola.Text := dmDeca.cdsSel_AcompEscolar.FieldByName('nom_turma').AsString;

    vvMUDOU_ESCOLA := False;

    Panel13.Enabled := True;
    Image1.Enabled  := True;
    Image2.Enabled  := True;
end;

procedure TfrmFichaCrianca.btnAtualizaDadosEducacensoClick(
  Sender: TObject);
begin
  {//Executa a atualiza��o dos campos referentes ao Educacenso do aluno atual
  try
    with dmDeca.cdsUpd_DadosEducacenso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value              := Trim(txtMatricula.Text);
      Params.ParamByName ('@pe_dat_cadastro_educacenso').Value    := StrToDate(mskDataCadastro.Text);
      Params.ParamByName ('@pe_num_situacao_cei').Value           := StrToInt(mskCEI.Text);
      Params.ParamByName ('@pe_dsc_observacoes_educacenso').Value := Trim(meObservacoes.Text);
      Execute;
    end;

    PageControl2.ActivePageIndex := 0;

  except end;
  }

  //Faz a atualiza��o do numero da turma/classe e a data da atualiza��o.
  if Application.MessageBox ('Deseja alterar os dados do cadastro do Educacenso' +#13+#10+
                             'do prontu�rio atual?',
                             '[Sistema Deca] - Alterar dados Educacenso',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    try
      with dmDeca.cdsUpd_TurmaCIE_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value           := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
        Params.ParamByName ('@pe_num_turma_cei').Value           := cbTurmaCIE.Text;
        Params.ParamByName ('@pe_dat_cadastro_educacenso').Value := StrToDate(mskDataCadastro.Text);
        Execute;
      end;
    except end;

    try
      with dmDeca.cdsSel_TurmaCIE do
      begin
        Close;
        Params.ParamByname ('@pe_cod_id_turmacie').Value := Null;
        Params.ParamByname ('@pe_dsc_turmacie').Value    := Null;
        Params.ParamByname ('@pe_cod_id_uecie').Value    := Null;
        Params.ParamByname ('@pe_dsc_uecie').Value       := Null;
        Params.ParamByname ('@pe_cod_id_seriecie').Value := Null;
        Params.ParamByname ('@pe_dsc_seriecie').Value    := Null;
        Params.ParamByname ('@pe_ind_periodo').Value     := Null;
        Params.ParamByname ('@pe_num_turma').Value       := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_turma_cei').Value;
        Open;

        edUnidadeEscolar.Text := dmDeca.cdsSel_TurmaCIE.FieldByName ('dsc_uecie').Value;
        edSerieEscolar.Text   := dmDeca.cdsSel_TurmaCIE.FieldByName ('dsc_seriecie').Value;
        edPeriodo.Text        := dmDeca.cdsSel_TurmaCIE.FieldByName ('vPeriodo').Value;
      end;
    except end;

  end;
end;

procedure TfrmFichaCrianca.AtualizaPeriodoCadastro;
begin
  //Recuperar o �ltimo bimestre lan�ado para a matr�cula atual
  with dmDeca.cdsSel_UltimoBimestre do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_matricula').Value;
    Params.ParamByName ('@pe_num_ano').Value       := dmDeca.cdsSel_AcompEscolar.FieldByName ('num_ano_letivo').Value;
    Open;
  end;

  if (mmBIMESTRE_SELECIONADO = dmDeca.cdsSel_UltimoBimestre.FieldByName ('UltBim').AsInteger) then
  begin

    //Permite a altera��o dos dados e faz a mudan�a de acordo com a regra o per�odo escolar e no cadastro.
    //Se a escola for CONCLUINTE ENSINO MEDIO, manter o per�odo atual do cadastro
    if (vvMUDA_CADASTRO = True) and (cbEscolaE.Text = 'CONCLUINTE ENSINO MEDIO') then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := cbPeriodo.Text;
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;


    //Se a escola escolhida for SEM ESCOLA, mant�m o per�odo no cadastro
    if (vvMUDA_CADASTRO = True) and (cbEscolaE.Text = 'SEM ESCOLA') then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := cbPeriodo.Text;
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;

    //Se Per�odo escolar for INTEGRAl, atribuir 'INTEGR/ESC' no cadastro
    //Se Per�odo escolar for INTEGRAl, mant�m o per�odo no cadastro - 05/09/2017
    if (vvMUDA_CADASTRO = True) and (cbPeriodoEscolaE.Text = 'Integral') then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := cbPeriodo.Text; //'INTEGR/ESC'; 05/09/2017
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;

    //Caso o status for diferente de Afastado e o per�odo da Escola no bimestre atual for Noite, manter o per�odo do Cadastro   -  28/05/2015
    if (vvMUDA_CADASTRO = True) and (dmDeca.cdsSel_Cadastro.FieldByName ('ind_status').Value <> 4) and (dmDeca.cdsSel_UltimoBimestre.FieldByName ('ind_periodo_escolar').Value = 2) then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := cbPeriodo.Text;
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;


    //Se o Aluno estiver com status Afastado, atribuir per�odo <N/D>
    //Se o Aluno estiver com status Afastado, mant�m o per�odo no cadastro 05/09/2017
    if (vvMUDA_CADASTRO = True) and (dmDeca.cdsSel_Cadastro.FieldByName ('ind_status').Value = 4) then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := cbPeriodo.Text;
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;


    //Se o per�odo da escola for MANH�, altera o do cadastro para TARDE
    if (vvMUDA_CADASTRO = True) and (cbPeriodoEscolaE.Text = 'Manh�') then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := 'TARDE';
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;

    //Se o per�odo da escola for TARDE, altera o do cadastro para MANH�
    if (vvMUDA_CADASTRO = True) and (cbPeriodoEscolaE.Text = 'Tarde') then
    begin
      with dmDeca.cdsAlt_PeriodoCadastro do
      begin
        Params.ParamByName ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
        Params.ParamByName ('@pe_dsc_periodo').Value   := 'MANH�';
        Execute;
        vvMUDA_CADASTRO := False;
      end;
    end;

    {
    //Atualizar o Campo SEM ESCOLA no cadastro de acordo com a escola selecionada
    with dmDeca.cdsUpd_SemEscola_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value  := GetValue(txtMatricula.Text);
      if (cbEscolaE.Text = 'SEM ESCOLA') then
        Params.ParamByName ('@pe_flg_sem_escola').Value := 1 //Sem Escola
      else
        Params.ParamByName ('@pe_flg_sem_escola').Value := 0; //Com Escola
      Execute;
    end;
  end
  }
  //else
  if (mmBIMESTRE_SELECIONADO <> dmDeca.cdsSel_UltimoBimestre.FieldByName ('UltBim').Value) then
  begin
    ShowMessage ('Os dados no cadastro n�o ser�o alterados, pois o bimestre atual n�o � o atual.');
  end;
end;

end;

procedure TfrmFichaCrianca.TabSheet13Show(Sender: TObject);
begin
  //Carregar as turmas para o combo de sele��o - Educacenso
  cbTurmaCIE.Clear;
  try
    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByname ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByname ('@pe_dsc_turmacie').Value    := Null;
      Params.ParamByname ('@pe_cod_id_uecie').Value    := Null;
      Params.ParamByname ('@pe_dsc_uecie').Value       := Null;
      Params.ParamByname ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByname ('@pe_dsc_seriecie').Value    := Null;
      Params.ParamByname ('@pe_ind_periodo').Value     := Null;
      Params.ParamByname ('@pe_num_turma').Value       := Null;
      Open;

      while not (dmDeca.cdsSel_TurmaCIE.eof) do
      begin
        cbTurmaCIE.Items.Add (FieldByName ('num_turma').Value);
        dmDeca.cdsSel_TurmaCIE.Next;
      end;
    end;
  except end;
  //cbTurmaCIE.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_turma_cei').Value;
  cbTurmaCIE.ItemIndex := cbTurmaCIE.Items.IndexOf(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_turma_cei').Value);
  try
    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByname ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByname ('@pe_dsc_turmacie').Value    := Null;
      Params.ParamByname ('@pe_cod_id_uecie').Value    := Null;
      Params.ParamByname ('@pe_dsc_uecie').Value       := Null;
      Params.ParamByname ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByname ('@pe_dsc_seriecie').Value    := Null;
      Params.ParamByname ('@pe_ind_periodo').Value     := Null;
      Params.ParamByname ('@pe_num_turma').Value       := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_turma_cei').Value;
      Open;
      edUnidadeEscolar.Text := IntToStr(FieldByName ('num_uecie').Value) + '-' + FieldByName ('dsc_uecie').Value;
      edSerieEscolar.Text   := dmDeca.cdsSel_TurmaCIE.FieldByName ('dsc_seriecie').Value;
      edPeriodo.Text        := dmDeca.cdsSel_TurmaCIE.FieldByName ('vPeriodo').Value;
    end;
  except end;
end;

procedure TfrmFichaCrianca.cbTurmaCIEClick(Sender: TObject);
begin
  edUnidadeEscolar.Clear;
  edSerieEscolar.Clear;
  edPeriodo.Clear;

  //Consulta a turma e recupera os dados para exibi��o
  try
    with dmDeca.cdsSel_TurmaCIE do
    begin
      Close;
      Params.ParamByname ('@pe_cod_id_turmacie').Value := Null;
      Params.ParamByname ('@pe_dsc_turmacie').Value    := Null;
      Params.ParamByname ('@pe_cod_id_uecie').Value    := Null;
      Params.ParamByname ('@pe_dsc_uecie').Value       := Null;
      Params.ParamByname ('@pe_cod_id_seriecie').Value := Null;
      Params.ParamByname ('@pe_dsc_seriecie').Value    := Null;
      Params.ParamByname ('@pe_ind_periodo').Value     := Null;
      Params.ParamByname ('@pe_num_turma').Value       := GetValue(cbTurmaCIE.Text);
      Open;

      edUnidadeEscolar.Text := IntToStr(FieldByName ('num_uecie').Value) + '-' + FieldByName ('dsc_uecie').Value;
      edSerieEscolar.Text   := FieldByName ('dsc_seriecie').Value;
      edPeriodo.Text        := FieldByName ('vPeriodo').Value;

    end;
  except end;
end;

procedure TfrmFichaCrianca.btnMigrarDadosEducacensoClick(Sender: TObject);
var mmNUM_TURMA : String;
begin
  { 1. Ler o campo CADASTRO.DSC_OBSERVACOES_EDUCACENSO
    2. Montar a vari�vel
    3. Comparar com os valores do banco de dados -> TURMASCIE.NUM_TURMA
    4. Caso exista, gravar na tabela - CADASTRO.NUM_TURMA
  }


  try
    //Ler o campo CADASTRO.DSC_OBSERVACOES_EDUCACENSO
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := 0;
      Open;

      while not (dmDeca.cdsSel_Cadastro.eof) do
      begin
        //Se n�o localizou registros na consulta...
        if (dmDeca.cdsSel_Cadastro.RecordCount < 1) then
        begin
          Application.MessageBox ('Aten��o!!!' +#13+#10+
                                  'N�o foram encontrados registros na consulta.' +#13+#10+
                                  'Entre em contato com o Administrador do Sistema.',
                                  '[Sistema Deca] - Migra��o Dados Educacenso',
                                  MB_ICONERROR + MB_OK);
        end
        else
        begin
          //Montar a vari�vel...
          if (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_observacoes_educacenso').IsNull) or (dmDeca.cdsSel_Cadastro.FieldByName ('dsc_observacoes_educacenso').Value = '<Sem anota��es>') then
          begin

          end
          else
          begin
            mmNUM_TURMA := Copy(dmDeca.cdsSel_Cadastro.FieldByName ('dsc_observacoes_educacenso').Value,1,3)+'.'+
                           Copy(dmDeca.cdsSel_Cadastro.FieldByName ('dsc_observacoes_educacenso').Value,4,3)+'.'+
                           Copy(dmDeca.cdsSel_Cadastro.FieldByName ('dsc_observacoes_educacenso').Value,7,3);

            //Consulta mmNUM_TURMA em TURMASCIE.NUM_TURMA
            with dmDeca.cdsSel_TurmaCIE do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_turmacie').Value := Null;
              Params.ParamByName ('@pe_dsc_turmacie').Value    := Null;
              Params.ParamByName ('@pe_cod_id_uecie').Value    := Null;
              Params.ParamByName ('@pe_dsc_uecie').Value       := Null;
              Params.ParamByName ('@pe_cod_id_seriecie').Value := Null;
              Params.ParamByName ('@pe_dsc_seriecie').Value    := Null;
              Params.ParamByName ('@pe_ind_periodo').Value     := Null;
              Params.ParamByName ('@pe_num_turma').Value       := mmNUM_TURMA;
              Open;

              if (dmDeca.cdsSel_TurmaCIE.RecordCount < 1) then
              begin
                //Nada a fazer
              end
              else
              begin
                //Altera o campo no cadastro
                with dmDeca.cdsUpd_TurmaCIE_Cadastro do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value           := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_turma_cei').Value           := mmNUM_TURMA;
                  Params.ParamByName ('@pe_dat_cadastro_educacenso').Value := Now();
                  Execute;
                end;
              end;
            end;
          end;
        end;
        dmDeca.cdsSel_Cadastro.Next;
      end;
    end;
  except
    Application.MessageBox ('Aten��o!!!' +#13+#10+
                            'Um erro ocorreu durante o processo de migra��o.' +#13+#10+
                            'Refa�a o processo ou entre em contato com o Administrador',
                            '[Sistema Deca] - Migra��o Dados Educacenso',
                            MB_ICONERROR + MB_OK);

  end;


end;

procedure TfrmFichaCrianca.btnAlteraDadosDocumentacaoClick(
  Sender: TObject);
begin
  //Alterar o cadastro apenas com os dados da aba Documenta��o
  //Habilitar os campos para permitir a altera��o dos dados
  PageControl4.ActivePageIndex := 0;
  GroupBox30.Enabled := True;
  GroupBox31.Enabled := True;
  GroupBox32.Enabled := True;
  GroupBox33.Enabled := True;
  GroupBox29.Enabled := True;
  GroupBox34.Enabled := True;
  GroupBox51.Enabled := True;
  GroupBox52.Enabled := True;
  GroupBox36.Enabled := True;
  GroupBox37.Enabled := True;
  GroupBox38.Enabled := True;
  GroupBox39.Enabled := True;
  GroupBox40.Enabled := True;
  GroupBox41.Enabled := True;
  GroupBox42.Enabled := True;
  GroupBox43.Enabled := True;
  GroupBox44.Enabled := True;
  GroupBox45.Enabled := True;
  GroupBox46.Enabled := True;
  GroupBox47.Enabled := True;
  GroupBox48.Enabled := True;
  GroupBox49.Enabled := True;
  GroupBox50.Enabled := True;
  GroupBox65.Enabled := True;
  GroupBox59.Enabled := True;
  GroupBox60.Enabled := True;
  GroupBox61.Enabled := True;
  GroupBox62.Enabled := True;
  GroupBox63.Enabled := True;
  GroupBox67.Enabled := True;
  GroupBox68.Enabled := True;
  btnAlteraDadosDocumentacao.Enabled     := False;
  btnGravaDadosDocumentacao.Enabled      := True;
  btnCancelarAlteracaoDocumentos.Enabled := True;
  btnSairDocumentacao.Enabled            := False;
  //Utilizar procedure diferente da altera��o do cadastro...

end;

procedure TfrmFichaCrianca.TabSheet14Show(Sender: TObject);
begin
  FillForm('S', txtMatricula.Text);
end;

procedure TfrmFichaCrianca.btnCancelarAlteracaoDocumentosClick(
  Sender: TObject);
begin
  GroupBox30.Enabled                     := False;
  GroupBox31.Enabled                     := False;
  GroupBox32.Enabled                     := False;
  GroupBox33.Enabled                     := False;
  GroupBox29.Enabled                     := False;
  GroupBox34.Enabled                     := False;
  GroupBox51.Enabled                     := False;
  GroupBox52.Enabled                     := False;
  GroupBox36.Enabled                     := False;
  GroupBox37.Enabled                     := False;
  GroupBox38.Enabled                     := False;
  GroupBox39.Enabled                     := False;
  GroupBox40.Enabled                     := False;
  GroupBox41.Enabled                     := False;
  GroupBox42.Enabled                     := False;
  GroupBox43.Enabled                     := False;
  GroupBox44.Enabled                     := False;
  GroupBox45.Enabled                     := False;
  GroupBox46.Enabled                     := False;
  GroupBox47.Enabled                     := False;
  GroupBox48.Enabled                     := False;
  GroupBox49.Enabled                     := False;
  GroupBox50.Enabled                     := False;
  GroupBox65.Enabled                     := False;
  GroupBox59.Enabled                     := False;
  GroupBox60.Enabled                     := False;
  GroupBox61.Enabled                     := False;
  GroupBox62.Enabled                     := False;
  GroupBox63.Enabled                     := False;
  GroupBox67.Enabled                     := False;
  GroupBox68.Enabled                     := False;  
  PageControl4.ActivePageIndex           := 0;
  btnAlteraDadosDocumentacao.Enabled     := True;
  btnGravaDadosDocumentacao.Enabled      := False;
  btnCancelarAlteracaoDocumentos.Enabled := False;
  btnSairDocumentacao.Enabled            := True;
end;

procedure TfrmFichaCrianca.btnGravaDadosDocumentacaoClick(Sender: TObject);
begin
  try
    ValidaDocs;
    with dmDeca.cdsAlt_CadastroDocumentos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').AsString            := GetValue(txtMatricula.Text);
      Params.ParamByName ('@pe_num_rg').AsString                   := GetValue(edNumRG.Text);
      Params.ParamByName ('@pe_dsc_rg_emissor').AsString           := GetValue(cbOrgaoEmissorRG.Text);
      Params.ParamByName ('@pe_dat_emissao_rg').AsDatetime         := GetValue(mskRGDataEmissao, vtDate);
      Params.ParamByName ('@pe_dsc_uf_rg').AsString                := cbRGUf.Text;
      Params.ParamByName ('@pe_num_cpf').AsString                  := GetValue(mskNumCPF.Text);
      Params.ParamByName ('@pe_num_certidao').AsString             := GetValue(edDadosCertidaoNascimento.Text);
      Params.ParamByName ('@pe_dsc_local_nascimento').AsString     := GetValue(edLocalNascimento.Text);
      Params.ParamByName ('@pe_num_titulo_eleitor').AsString       := GetValue(edNumTituloEleitor.Text);
      Params.ParamByName ('@pe_num_zona_eleitoral').AsInteger      := GetValue(edZonaEleitoral.Text);
      Params.ParamByName ('@pe_num_secao_eleitoral').AsInteger     := GetValue(edSecaoEleitoral.Text);
      Params.ParamByName ('@pe_ctps_num').AsString                 := GetValue(edNumCTPS.Text);
      Params.ParamByName ('@pe_ctps_serie').AsString               := GetValue(edCTPSSerie.Text);
      Params.ParamByName ('@pe_dat_emissao_ctps').AsDateTime       := GetValue(mskCTPSDataEmissao, vtDate);
      Params.ParamByName ('@pe_dsc_uf_ctps').AsString              := cbCTPSUf.Text;
      Params.ParamByName ('@pe_num_pis').AsString                  := GetValue(edNumPIS.Text);
      Params.ParamByName ('@pe_dat_emissao_pis').AsDateTime        := GetValue(mskPISDataEmissao, vtDate);
      Params.ParamByName ('@pe_dsc_banco_pis').AsString            := GetValue(edPisBanco.Text);
      Params.ParamByName ('@pe_num_reservista').AsString           := GetValue(edReservistaNumero.Text);
      Params.ParamByName ('@pe_num_reservista_csm').AsString       := GetValue(edReservistaCSM.Text);
      Params.ParamByName ('@pe_num_cartao_sus').AsString           := GetValue(edNumCartaoSUS.Text);
      Params.ParamByName ('@pe_dsc_municipio_residencia').AsString := GetValue(edMunicipioResidencia.Text);
      Params.ParamByName ('@pe_dsc_uf_residencia').AsString        := GetValue(cbUFResidencia.Text);
      Params.ParamByName ('@pe_num_telefone_celular').AsString     := GetValue(mskTelefoneCelular.Text);
      Params.ParamByName ('@pe_ind_grau_instrucao').AsInteger      := cbGrauInstrucao.ItemIndex;
      Params.ParamByName ('@pe_dsc_email').AsString                := GetValue(edEmail.Text);
      Params.ParamByName ('@pe_ind_estado_civil').AsInteger        := cbEstadoCivil.ItemIndex;
      Params.ParamByName ('@pe_dsc_nacionalidade').AsString        := GetValue(edNacionalidade.Text);
      Params.ParamByName ('@pe_dsc_nom_conjuge').AsString          := GetValue(edNomeConjuge.Text);
      Params.ParamByName ('@pe_ind_guarda_regularizada').AsInteger := cbGuardaRegular.ItemIndex;
      Execute;
    end;

  except
    begin

    end;
  end;
  btnAlteraDadosDocumentacao.Enabled     := True;
  btnGravaDadosDocumentacao.Enabled      := False;
  btnCancelarAlteracaoDocumentos.Enabled := False;
  btnSairDocumentacao.Enabled            := True;
end;

procedure TfrmFichaCrianca.btnVisualizarEmissaoContratoClick(
  Sender: TObject);
begin
  //Emite a Ficha de Admiss�o e o Termo de Compromisso
  //de acordo com a caracter�stica do mesmo (crian�a, Bolsista ou Celetista)

  Application.CreateForm (TfrmEmiteContratoAdmissao, frmEmiteContratoAdmissao);

  frmEmiteContratoAdmissao.Label39.Caption := frmFichaCrianca.Label39.Caption;

  frmEmiteContratoAdmissao.mskAdmissao.Text                  := txtAdmissao.Text;
  frmEmiteContratoAdmissao.rgPrimeiroEmprego.ItemIndex       := 1;
  frmEmiteContratoAdmissao.edBanco.Text                      := 'BANCO SANTANDER';
  frmEmiteContratoAdmissao.rgContribuiSindicato.ItemIndex    := 1;
  frmEmiteContratoAdmissao.edValorContribuicaoSindicato.Text := '000,00';
  frmEmiteContratoAdmissao.edValorSalarioBase.Text           := '000,00';
  frmEmiteContratoAdmissao.edHorarioTrabalho.Text            := '-';
  frmEmiteContratoAdmissao.rgNecessitaVT.ItemIndex           := 0;
  frmEmiteContratoAdmissao.edItinerarioVT.Text               := '-';
  frmEmiteContratoAdmissao.edQtdVTDia.Text                   := '0';
  frmEmiteContratoAdmissao.rgNecessitaPE.ItemIndex           := 0;
  frmEmiteContratoAdmissao.edItinerarioPE.Text               := '-';
  frmEmiteContratoAdmissao.edQtdPEDia.Text                   := '0';
  frmEmiteContratoAdmissao.cbCurso.ItemIndex                 := 0;
  frmEmiteContratoAdmissao.cbCargoFuncao.ItemIndex           := 0; 

  frmEmiteContratoAdmissao.ShowModal;
end;

procedure TfrmFichaCrianca.ValidaDocs;
begin

  //Valdar os campos de documentos da ficha de cadastro
  if (edNumRG.Text = '') then
  begin
    edNumRG.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero do RG deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;


  if (cbOrgaoEmissorRG.ItemIndex = -1) then
  begin
    cbOrgaoEmissorRG.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo �rg�o Emissor do RG deve ser selecionado.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;


  if (mskRGDataEmissao.Text = '') then
  begin
    mskRGDataEmissao.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Data de emiss�o do RG deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (cbRGUf.ItemIndex = -1) then
  begin
    cbRGUf.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo UF do RG deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (mskNumCPF.Text = '') then
  begin
    mskNumCPF.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero do CPF deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edDadosCertidaoNascimento.Text = '') then
  begin
    edDadosCertidaoNascimento.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Certid�o de Nascimento deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edLocalNascimento.Text = '') then
  begin
    edLocalNascimento.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Local de Nascimento deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNumTituloEleitor.Text = '') then
  begin
    edNumTituloEleitor.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo T�tulo de Eleitor deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edZonaEleitoral.Text = '') then
  begin
    edZonaEleitoral.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Zona Eleitoral deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edSecaoEleitoral.Text = '') then
  begin
    edSecaoEleitoral.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Se��o Eleitoral deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNumCTPS.Text = '') then
  begin
    edNumCTPS.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero da Carteira de Trabalho deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edCTPSSerie.Text = '') then
  begin
    edCTPSSerie.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero de S�rie da Carteira de Trabalho deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (mskCTPSDataEmissao.Text = '') then
  begin
    mskCTPSDataEmissao.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Data da Emiss�o da Carteira de Trabalho deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (cbCTPSUf.ItemIndex = -1) then
  begin
    cbCTPSUf.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo UF da Carteira de Trabalho deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNumPIS.Text = '') then
  begin
    edNumPIS.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero do PIS deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (mskPISDataEmissao.Text = '') then
  begin
    mskPISDataEmissao.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Data da Emiss�o do PIS deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edPISBanco.Text = '') then
  begin
    edPISBanco.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Banco - PIS deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edReservistaNumero.Text = '') then
  begin
    edReservistaNumero.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero da Reservista deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edReservistaCSM.Text = '') then
  begin
    edReservistaCSM.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo CSM da Reservista deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNumCartaoSUS.Text = '') then
  begin
    edNumCartaoSUS.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero do Cart�o SUS deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNumCartaoSIAS.Text = '') then
  begin
    edNumCartaoSIAS.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero do Cart�o SIAS deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;


  if (edMunicipioResidencia.Text = '') then
  begin
    edMunicipioResidencia.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Munic�pio de Resid�ncia deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (mskTelefoneCelular.Text = '') then
  begin
    mskTelefoneCelular.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo N�mero de Telefone Celular deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (cbGrauInstrucao.ItemIndex = -1) then
  begin
    cbGrauInstrucao.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Grau de Instru��o deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edEmail.Text = '') then
  begin
    edEmail.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo E-Mail particular deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (cbEstadoCivil.ItemIndex = -1) then
  begin
    cbEstadoCivil.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Estado Civil deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNacionalidade.Text = '') then
  begin
    edNacionalidade.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Nacionalidade deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (edNomeConjuge.Text = '') then
  begin
    edNomeConjuge.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Nome do C�nuge deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;

  if (cbGuardaRegular.ItemIndex = -1) then
  begin
    cbGuardaRegular.SetFocus;
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'O campo Guarda Regularizada deve ser preenchido.',
                            '[Sistema Deca] - Campo Obrigat�rio',
                            MB_OK + MB_ICONEXCLAMATION);
  end;  
end;

end.
