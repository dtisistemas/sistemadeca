unit uLancamentoBiometrico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, Grids, DBGrids, Db, ComCtrls;

type
  TfrmLancamentoBiometrico = class(TForm)
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    btnAlterar: TBitBtn;
    dsBiometricos: TDataSource;
    Panel3: TPanel;
    Label5: TLabel;
    txtData: TMaskEdit;
    Label4: TLabel;
    Label3: TLabel;
    btnCalcular: TSpeedButton;
    Panel4: TPanel;
    chkProblemasSaude: TCheckBox;
    txtProblemasSaude: TMemo;
    Label8: TLabel;
    txtObservacoes: TMemo;
    GroupBox2: TGroupBox;
    lbIMC: TLabel;
    lbClassificacao: TLabel;
    txtPeso: TEdit;
    txtAltura: TEdit;
    dbgCadastro: TDBGrid;
    Label9: TLabel;
    cbUnidade: TComboBox;
    dsCadastros: TDataSource;
    dbgBiometricos: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure AtualizaCamposBotoes (Modo: String);
    procedure FormCreate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure chkProblemasSaudeClick(Sender: TObject);
    procedure btnCalcularClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cbUnidadeClick(Sender: TObject);
    procedure dbgCadastroCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLancamentoBiometrico: TfrmLancamentoBiometrico;
  var mPeso, mAltura, mIMC : Real;
  vUnidade1, vUnidade2 : TStringList;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmLancamentoBiometrico.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    txtData.Clear;
    txtData.Enabled := False;
    txtPeso.Clear;
    txtPeso.Enabled := False;
    txtAltura.Clear;
    txtAltura.Enabled := False;
    lbIMC.Caption := '';
    lbClassificacao.Caption := '';
    chkProblemasSaude.Checked := False;
    chkProblemasSaude.Enabled := False;
    txtProblemasSaude.Lines.Clear;
    txtProblemasSaude.Enabled := False;
    txtObservacoes.Lines.Clear;
    txtObservacoes.Enabled := False;

    btnCalcular.Enabled := False;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

  end

  else if Modo = 'Novo' then

  begin

    txtData.Enabled := True;
    txtPeso.Clear;
    txtPeso.Enabled := True;
    txtAltura.Enabled := True;
    lbClassificacao.Caption := '';
    chkProblemasSaude.Checked := False;
    chkProblemasSaude.Enabled := True;
    txtProblemasSaude.Enabled := False;
    txtObservacoes.Enabled := True;

    btnCalcular.Enabled := True;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;

  end;

end;

procedure TfrmLancamentoBiometrico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  vUnidade1.Free;
  vUnidade2.Free;
end;

procedure TfrmLancamentoBiometrico.FormShow(Sender: TObject);
begin

  vUnidade1 := TStringLIst.Create();
  vUnidade2 := TStringLIst.Create();

  //Carrega os dados das unidades no combo cbUnidade
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        vUnidade1.Add(IntToStr(FieldByName('cod_unidade').AsInteger));
        vUnidade2.Add(FieldByName('nom_unidade').AsString);
        cbUnidade.Items.Add(FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;

      AtualizaCamposBotoes('Padrao');

    end;
  except end;

  //Desvincula o grid inicialmente para exibi��o dos dados
  dbgCadastro.DataSource := nil;
  dbgBiometricos.DataSource := nil;

  //AtualizaCamposBotoes('Padrao');

end;

procedure TfrmLancamentoBiometrico.FormCreate(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmLancamentoBiometrico.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Novo');
  btnSalvar.Enabled := False;
end;

procedure TfrmLancamentoBiometrico.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmLancamentoBiometrico.btnSairClick(Sender: TObject);
begin
  frmLancamentoBiometrico.Close;
end;

procedure TfrmLancamentoBiometrico.chkProblemasSaudeClick(Sender: TObject);
begin
  with chkProblemasSaude do
  begin
    if Checked then txtProblemasSaude.Enabled := True;
    if Not Checked then
    begin
      txtProblemasSaude.Lines.Clear;
      txtProblemasSaude.Enabled := False;
    end;
  end;
end;

procedure TfrmLancamentoBiometrico.btnCalcularClick(Sender: TObject);
begin

//Criar fun��o para calcular diferentemente a situa��o de crian�a e de adolescente
mPeso := 0;
mAltura := 0;
mIMC := 0;

if ((txtPeso.Text <> '') or (txtPeso.Text <> '00,000')) and
   ((txtAltura.Text <> '') or (txtAltura.Text <> '0,00')) then
begin
  mPeso := StrToFloat(txtPeso.Text);
  mAltura := StrToFloat(txtAltura.Text);
  mIMC := mPeso / (mAltura * mAltura);

  lbIMC.Caption := Format('%2.1f', [mIMC]);

  if (mIMC < 18.5) then
    lbClassificacao.Caption := 'ABAIXO DO PESO...'
  else if (mIMC >= 18.5) and (mIMC <= 24.9) then
    lbClassificacao.Caption := 'NORMAL...'
  else if (mIMC >= 25.0) and (mIMC <= 29.9) then
    lbClassificacao.Caption := 'SOBREPESO...'
  else if (mIMC >= 30.0) and (mIMC <= 34.9) then
    lbClassificacao.Caption := 'OBESIDADE GRAU I (SIMPLES)'
  else if (mIMC >= 35.0) and (mIMC <= 39.9) then
    lbClassificacao.Caption := 'OBESIDADE GRAU II (SEVERA)'
  else if (mIMC >= 40.0) then
    lbClassificacao.Caption := 'OBESIDADE GRAU III (M�RBIDA)';

  btnSalvar.Enabled := True;

end
else
begin
  
end;


end;

procedure TfrmLancamentoBiometrico.btnSalvarClick(Sender: TObject);
begin

  try
    //Primeiro valida o Status da crian�a - se ela pode sofre este movimento
    //Grava os dados da Avalia��o Biom�trica na Tabela BIOMETRICOS
    with dmDeca.cdsInc_Biometricos do
    begin
      Close;

      Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Params.ParamByName ('@pe_val_peso').AsFloat := mPeso;//GetValue(txtPeso, vtReal);
      Params.ParamByName ('@pe_val_altura').AsFloat := mAltura;//GetValue(txtAltura, vtReal);
      Params.ParamByName ('@pe_val_imc').AsFloat := StrToFloat(lbIMC.Caption);
      Params.ParamByName ('@pe_dsc_classificacao_imc').AsString := lbClassificacao.Caption;

      if chkProblemasSaude.Checked = True then
        Params.ParamByName ('@pe_flg_problema_saude').AsInteger := 1
      else
        Params.ParamByName ('@pe_flg_problema_saude').AsInteger := 0;

      Params.ParamByName ('@pe_dsc_problema_saude').Value := GetValue(txtProblemasSaude.Lines.Text);
      Params.ParamByName ('@pe_dsc_observacoes').Value := GetValue(txtObservacoes.Lines.Text);
      Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Params.ParamByName ('@pe_log_data').AsDateTime := StrToDate(txtData.Text); 
      Execute;
    end;

    //Lan�a os dados no cadastro_hist�rico
    with dmDeca.cdsInc_Cadastro_Historico do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Params.ParamByName('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro.FieldBYName ('cod_unidade').AsInteger;//vvCOD_UNIDADE;
      Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
      Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 18;
      Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Lan�amento de Avalia��o Biom�trica';
      //Params.ParamByName('@pe_dsc_ocorrencia').Value := Null;
      Params.ParamByName('@pe_dsc_ocorrencia').AsString := 'Lan�ados na data de ' + txtData.Text + ' os valores de Avalia��o Biom�trica: ' + ' Peso: ' +  txtPeso.Text +
                                                        ' Altura: ' + txtAltura.Text + ' Classifica��o : ' + lbClassificacao.Caption +  ' �ndice: ' + lbIMC.Caption;
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;
    end;

    //Faz a atualiza��o do Cadastro, nos campos PESO e ALTURA, conforme avalia��o Biom�trica
    with dmDeca.cdsAlt_Cadastro_Dados_Biometricos do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;;
      //ShowMessage (txtPeso.Text + ' - ' + txtAltura.Text);
      Params.ParamByName('@pe_val_peso').AsFloat := StrToFloat(txtPeso.Text);
      Params.ParamByName('@pe_val_altura').AsFloat := StrToFloat(txtAltura.Text);
      Execute;
    end;

    with dmDeca.cdsSel_Biometricos do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Open;
      dbgBiometricos.Refresh;
    end;

    AtualizaCamposBotoes('Padr�o');
  except
    Application.MessageBox('Aten��o!'+#13+#10+#13+#10+
             'Ocorreu um ERRO e os dados n�o foram gravados. Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Avalia��o Biom�trica',
             MB_OK + MB_ICONERROR);
  end;

end;

procedure TfrmLancamentoBiometrico.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmLancamentoBiometrico.Close;
  end;
end;

procedure TfrmLancamentoBiometrico.cbUnidadeClick(Sender: TObject);
begin

  dbgCadastro.DataSource := dsCadastros;
  dbgBiometricos.DataSource := dsBiometricos;

  //Carrega os dados dos cadastros lan�ados para a unidade

  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_cod_unidade').AsInteger := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;

      if RecordCount > 0 then
        dbgCadastro.Refresh
      else
      begin
        ShowMessage('N�o existem crian�as/adolescentes cadastrados para a unidade selecionada...');
      end;
    end;
  except end;
end;

procedure TfrmLancamentoBiometrico.dbgCadastroCellClick(Column: TColumn);
begin
  //Carregar no grid dbgBiometricos os dados lan�ados (ou n�o) para o adolescente selecionado no grid dbgCadastros
  try
    with dmDeca.cdsSel_Biometricos do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').AsString;
      Open;

      dbgBiometricos.Refresh;

    end;
  except end;
end;

end.
