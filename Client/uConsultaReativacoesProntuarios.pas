unit uConsultaReativacoesProntuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, Grids, DBGrids, Db, ComObj;

type
  TfrmConsultaReativacoesProntuarios = class(TForm)
    GroupBox1: TGroupBox;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    GroupBox2: TGroupBox;
    btnVerDados: TSpeedButton;
    dbgReativacoes: TDBGrid;
    btnImprimir: TSpeedButton;
    dsReativacoes: TDataSource;
    SaveDialog1: TSaveDialog;
    procedure btnVerDadosClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaReativacoesProntuarios: TfrmConsultaReativacoesProntuarios;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmConsultaReativacoesProntuarios.btnVerDadosClick(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_ConsultaReativacaoProntuario do
    begin
      Close;
      Params.ParamByName ('@pe_ind_tipo_historico').AsInteger := 25; //Reativa��o de Prontu�rio
      Params.ParamByName ('@pe_ano_historico').AsInteger      := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_mes_historico').AsInteger      := cbMes.ItemIndex;
      Open;

      if (dmDeca.cdsSel_ConsultaReativacaoProntuario.RecordCount = 0) then
      begin
        Application.MessageBox('Aten��o!!!' + #13+#10+
                               'N�o foram encontrados registros de REATIVA��O DE PRONTU�RIOS no per�odo informado.' +#13#+#10 +
                               'Favor tentar novamente!.',
                               '[Sistema Deca] - Reativa��es n�o encontradas',
                               MB_OK + MB_ICONWARNING);
        mskAno.SetFocus;
      end;

      dbgReativacoes.Refresh;
      
    end;
  except end;
end;

procedure TfrmConsultaReativacoesProntuarios.btnImprimirClick(
  Sender: TObject);
var
  Excel : variant;
  ContCampos, ContCampos1, Linha : Integer;
begin
  try
    if SaveDialog1.Execute then
    begin
      Excel := CreateOleObject('Excel.Application');
      Excel.WorkBooks.add(1);
      Excel.WorkBooks[1].SaveAs(SaveDialog1.FileName);;

      for ContCampos := 1 to dmDeca.cdsSel_ConsultaReativacaoProntuario.FieldCount -1 do
      begin
        Excel.Cells[1, ContCampos] := dmDeca.cdsSel_ConsultaReativacaoProntuario.Fields[ContCampos].DisplayName;
      end;

      Linha := 2;

      dmDeca.cdsSel_ConsultaReativacaoProntuario.First;
      while not (dmDeca.cdsSel_ConsultaReativacaoProntuario.eof) do
      begin
        for ContCampos1 := 1 to dmDeca.cdsSel_ConsultaReativacaoProntuario.FieldCount - 1 do
        begin
          Excel.Cells[Linha, ContCampos1] := dmDeca.cdsSel_ConsultaReativacaoProntuario.Fields[ContCampos1].AsString;
        end;
        Inc(Linha);
        dmDeca.cdsSel_ConsultaReativacaoProntuario.Next;
      end;
      Excel.Columns.AutoFit;
      Excel.WorkBooks[1].Save;
      Excel.WorkBooks[1].Close;

      Application.MessageBox ('Aten��o !!!' +#13+#10+
                              'A planilha com seus dados foi gerada na pasta selecionada.' +#13+#10+
                              'Acesse sua pasta para ter acesso a planilha e consultar os dados.',
                              '[Sistema Deca] - Exporta��o de dados para planilha',
                              MB_OK + MB_ICONINFORMATION);

    end;
  except end;
end;

end.
