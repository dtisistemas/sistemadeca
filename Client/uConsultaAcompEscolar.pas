unit uConsultaAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, OleServer, Excel97, Db,
  ComCtrls, ComObj;

type
  TfrmConsultaAcompEscolar = class(TForm)
    Panel1: TPanel;
    dbgResultados: TDBGrid;
    SaveDialog1: TSaveDialog;
    ExcelApplication1: TExcelApplication;
    dsPesquisaAcompEscolar: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    btnPesquisar: TSpeedButton;
    btnLimparPesquisa: TSpeedButton;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    GroupBox3: TGroupBox;
    cbEscola: TComboBox;
    chkInibeEscola: TCheckBox;
    GroupBox4: TGroupBox;
    cbBimestre: TComboBox;
    GroupBox5: TGroupBox;
    edAnoLetivo: TEdit;
    GroupBox1: TGroupBox;
    cbEscola2: TComboBox;
    GroupBox6: TGroupBox;
    cbBimestre2: TComboBox;
    GroupBox7: TGroupBox;
    edAnoLetivo2: TEdit;
    btnPesquisar2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    btnExportar: TSpeedButton;
    btnEnviarEmail: TSpeedButton;
    Panel2: TPanel;
    SpeedButton2: TSpeedButton;
    btnExportar2: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    function ValidaCampos:Boolean;
    procedure btnPesquisarClick(Sender: TObject);
    procedure cbEscolaClick(Sender: TObject);
    procedure chkInibeEscolaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnEnviarEmailClick(Sender: TObject);
    procedure btnPesquisar2Click(Sender: TObject);
    procedure cbEscola2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaAcompEscolar: TfrmConsultaAcompEscolar;
  vListaUnidade1 : TStringList;
  vListaUnidade2 : TStringList;
  vListaEscola1 : TStringList;
  vListaEscola2 : TStringList;

  mmEscola : Integer;

  mmEMAIL_ESCOLA : String;

implementation

uses uDM, uEnviarEmail;

{$R *.DFM}

procedure TfrmConsultaAcompEscolar.FormShow(Sender: TObject);
begin

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();
  vListaEscola1  := TStringList.Create();
  vListaEscola2  := TStringList.Create();

  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
    end;

    while not dmDeca.cdsSel_Unidade.eof do
    begin
      cbUnidade.Items.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
      vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value));
      vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
      dmDeca.cdsSel_Unidade.Next;
    end;
  except end;

  //Carrega a lista de escolas para a aba e consulta de escola
  cbEscola2.Clear;
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      while not dmDeca.cdsSel_Escola.eof do
      begin
        cbEscola2.Items.Add (FieldByName ('nom_escola').Value);
        vListaEscola1.Add (IntToStr(FieldByName ('cod_escola').Value));
        vListaEscola2.Add (FieldByName ('nom_escola').Value);
        dmDeca.cdsSel_Escola.Next;
      end;
    end;
  except end;

end;

procedure TfrmConsultaAcompEscolar.cbUnidadeClick(Sender: TObject);
begin
  cbEscola.Clear;
  //Seleciona as escolas da unidade selecionada
  try
    with dmDeca.cdsSel_EscolaAcompanhamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;

      while not eof do
      begin
        cbEscola.Items.Add (dmDeca.cdsSel_EscolaAcompanhamento.FieldByName ('nom_escola').Value);
        dmDeca.cdsSel_EscolaAcompanhamento.Next;
      end;

    end;
  except end;
end;

procedure TfrmConsultaAcompEscolar.btnExportarClick(Sender: TObject);
var
  Planilha : Olevariant;
  lin, col : integer;
begin
  {
  //Aplicar a consulta antes de exportar ao excel...
  with dmDeca.cdsSel_PesquisaAcompEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
    if chkInibeEscola.Checked then
      Params.ParamByName ('@pe_cod_escola').Value := null
    else
      Params.ParamByName ('@pe_cod_escola').Value := mmEscola;
    Params.ParamByName ('@pe_num_bimestre').Value := cbBimestre.ItemIndex + 1;
    Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAnoLetivo.Text);
    Open;
  end;
  }

  //Cria o aplicativo Excel, inicializa par�metros vazios, aplica��o vis�vel e utiliza p�gina 1 e aba 1
  ExcelApplication1 := TExcelApplication.Create(nil);
  ExcelApplication1.Workbooks.Add(EmptyParam, 0);
  ExcelApplication1.Visible[0] := True;

  Planilha := ExcelApplication1.Workbooks[1].Worksheets[1];

  //Colocar o nome do campo em cada coluna
  for col := 1 to dmDeca.cdsSel_PesquisaAcompEscolar.FieldCount do
  begin
    Planilha.Cells[1, col].Select;
    Planilha.Cells[1, col].Font.Bold := True;
    Planilha.Cells[1, col].Value := dmDeca.cdsSel_PesquisaAcompEscolar.Fields[col-1].DisplayLabel;
  end;

  //Inicializa a segunda linha
  lin := 2;

  //Repeti��o onde vai inserir os dados da tabela at� que seja final de arquivo
  while not (dmDeca.cdsSel_PesquisaAcompEscolar.eof) do
  begin

    for col := 1 to dmDeca.cdsSel_PesquisaAcompEscolar.FieldCount do
    begin
      Planilha.Cells[lin, col].Select;
      Planilha.Cells[lin, col].Value := dmDeca.cdsSel_PesquisaAcompEscolar.Fields[col-1].AsString;
    end;
    inc(lin);
    dmDeca.cdsSel_PesquisaAcompEscolar.Next;
  end;
  ExcelApplication1.Free;

  btnExportar.Enabled := False;
  btnEnviarEmail.Enabled := True;
end;

function TfrmConsultaAcompEscolar.ValidaCampos: Boolean;
begin

  //Unidade
  if (cbUnidade.ItemIndex = -1) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Voc� deve selecionar pelo menos uma UNIDADE.' + #13+#10 +
                            'Por favor, selecione uma das op��es da lista.',
                            '[Sistema Deca] - Sele��o de UNIDADE para pesquisa.',
                            MB_OK + MB_ICONWARNING);
    ValidaCampos := False;
  end;

  //Bimestre
  if (cbBimestre.ItemIndex = -1) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Voc� deve selecionar pelo menos um BIMESTRE.' + #13+#10 +
                            'Por favor, selecione uma das op��es da lista.',
                            '[Sistema Deca] - Sele��o de BIMESTRE para pesquisa.',
                            MB_OK + MB_ICONWARNING);
    ValidaCampos := False;
  end;

  //Ano Letivo
  if (Length(edAnoLetivo.Text)<4) then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Voc� deve informar um valor para ANO LETIVO v�lido.' + #13+#10 +
                            'Por favor, informe valor v�lido.',
                            '[Sistema Deca] - Informa��o de ANO LETIVO para pesquisa.',
                            MB_OK + MB_ICONWARNING);
    ValidaCampos := False;
  end;

end;

procedure TfrmConsultaAcompEscolar.btnPesquisarClick(Sender: TObject);
begin
    try
      with dmDeca.cdsSel_PesquisaAcompEscolar do
      begin
        Close;
        Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
        if chkInibeEscola.Checked then
          Params.ParamByName ('@pe_cod_escola').Value := null
        else
          Params.ParamByName ('@pe_cod_escola').Value := mmEscola;

        Params.ParamByName ('@pe_num_bimestre').Value := cbBimestre.ItemIndex + 1;
        Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAnoLetivo.Text);
        Open;

        dbgResultados.Refresh;

        if dmDeca.cdsSel_PesquisaAcompEscolar.RecordCount > 0 then
          btnExportar.Enabled := True
        else
          btnExportar.Enabled := False;

      end;
    except end;

end;

procedure TfrmConsultaAcompEscolar.cbEscolaClick(Sender: TObject);
begin
  if dmDeca.cdsSel_EscolaAcompanhamento.Locate('nom_escola', cbEscola.Items[cbEscola.ItemIndex], [loCaseInsensitive]) then
    mmEscola := dmDeca.cdsSel_EscolaAcompanhamento.FieldByName('cod_escola').AsInteger;
end;

procedure TfrmConsultaAcompEscolar.chkInibeEscolaClick(Sender: TObject);
begin
  if chkInibeEscola.Checked then
    cbEscola.Enabled := False
  else
    cbEscola.Enabled := True;
end;

procedure TfrmConsultaAcompEscolar.SpeedButton2Click(Sender: TObject);
begin
  vListaUnidade1.Free;
  vListaUnidade2.Free;
  frmConsultaAcompEscolar.Close;
end;

procedure TfrmConsultaAcompEscolar.btnEnviarEmailClick(Sender: TObject);
begin
  if (Length(mmEMAIL_ESCOLA)>0) then
  begin
    Application.CreateForm (TfrmEnviarEmail, frmEnviarEmail);
    frmEnviarEmail.edEmailPara.Text := mmEMAIL_ESCOLA;
    frmEnviarEmail.ShowModal;
  end;
end;

procedure TfrmConsultaAcompEscolar.btnPesquisar2Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_PesquisaAcompEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value := StrToInt(vListaEscola1.Strings[cbEscola2.ItemIndex]);
      Params.ParamByName ('@pe_num_bimestre').Value := cbBimestre2.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAnoLetivo2.Text);
      Open;                             
      dbgResultados.Refresh;
    end;
  except end;
end;

procedure TfrmConsultaAcompEscolar.cbEscola2Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := StrToInt(vListaEscola1.Strings[cbEscola2.ItemIndex]);
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      mmEMAIL_ESCOLA := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').Value;

    end;
  except end;
end;

end.
