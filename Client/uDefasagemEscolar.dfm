object frmDefasagemEscolar: TfrmDefasagemEscolar
  Left = 354
  Top = 255
  BorderStyle = bsDialog
  Caption = '[Sistema Deca] - Defasagem Escolar'
  ClientHeight = 317
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 2
    Top = 4
    Width = 530
    Height = 61
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 8
      Top = 8
      Width = 297
      Height = 44
      Caption = 'S�rie Escolar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cbSerieEscolar: TComboBox
        Left = 8
        Top = 15
        Width = 280
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 0
      end
    end
    object GroupBox2: TGroupBox
      Left = 307
      Top = 8
      Width = 82
      Height = 44
      Caption = 'Idade IDEAL'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object edIdadeIdeal: TEdit
        Left = 10
        Top = 15
        Width = 59
        Height = 21
        TabOrder = 0
      end
    end
    object GroupBox3: TGroupBox
      Left = 391
      Top = 8
      Width = 132
      Height = 44
      Caption = 'Defasagem a partir de ...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object edIdadeDefasada: TEdit
        Left = 10
        Top = 15
        Width = 59
        Height = 21
        TabOrder = 0
      end
    end
  end
  object Panel2: TPanel
    Left = 2
    Top = 67
    Width = 530
    Height = 206
    TabOrder = 1
    object dbgDadosDefasagem: TDBGrid
      Left = 8
      Top = 8
      Width = 513
      Height = 183
      Hint = 'dbgDadosDefasagem'
      DataSource = dsDefasagemEscolar
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'dsc_serie'
          Title.Caption = '[S�rie Escolar]'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_idade_ideal'
          Title.Alignment = taCenter
          Title.Caption = '[Ideal]'
          Width = 65
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'num_idade_defasada'
          Title.Alignment = taCenter
          Title.Caption = '[Defasada]'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_usuario'
          Title.Caption = '[Usu�rio]'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dat_cadastro'
          Title.Caption = '[Inclus�o]'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dat_alteracao'
          Title.Caption = '[Altera��o]'
          Visible = True
        end>
    end
  end
  object Panel3: TPanel
    Left = 2
    Top = 275
    Width = 531
    Height = 40
    TabOrder = 2
    object btnNovoD: TBitBtn
      Left = 167
      Top = 5
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoDClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnAlteraD: TBitBtn
      Left = 284
      Top = 5
      Width = 55
      Height = 30
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700A077777777777777777777DDA2777700000000000077772E627777
        8777777777707777532E77778FFFFFFFFF70777700A077778F44F4444F707777
        CDA177778FFFFFFFFF707777700077778F44F4444F707777000077778FFFFFFF
        FF707777A60077778FFFFFFF0F7077772E6277778F00FFF0B070777450007777
        8F0F0F0B0F000744A60077778FF0B0B0F0FBF0447466777788880F0F0FBFBF44
        494C7777777770B0FBFBFB4400A077777777770FBFBFB044FCB5777777777770
        00000744700077777777777777777777000077777777777777777777A6007777
        7777777777777777642E}
    end
    object btnGravaD: TBitBtn
      Left = 229
      Top = 5
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnGravaDClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelaD: TBitBtn
      Left = 348
      Top = 5
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnCancelaDClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnImprimirD: TBitBtn
      Left = 403
      Top = 5
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Visible = False
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777776B00777777777000777777776E007777777007880077777771007777
        7007780088007777740077770778878800880077770077778887778888008077
        7A00777887777788888800777D007778F7777F888888880780007778F77FF777
        8888880783007778FFF779977788880786007778F77AA7778807880789007777
        88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
        920077777777778FFFFFF0079500777777777778FFF887779800777777777777
        888777779B00777777777777777777779E0077777777777777777777A1007777
        7777777777777777A400}
    end
    object btnSairD: TBitBtn
      Left = 467
      Top = 5
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnSairDClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object dsDefasagemEscolar: TDataSource
    DataSet = dmDeca.cdsSel_DefasagemEscolar
    Left = 490
    Top = 83
  end
end
