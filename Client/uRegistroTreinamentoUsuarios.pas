unit uRegistroTreinamentoUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, jpeg, Mask, Buttons, ComCtrls;

type
  TfrmRegistroTreinamentoUsuarios = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    btnNovo: TSpeedButton;
    btnGravar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnSair: TSpeedButton;
    btnVisualizar: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    edInstrutor: TEdit;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    mskDataInicio: TMaskEdit;
    GroupBox4: TGroupBox;
    SpeedButton1: TSpeedButton;
    mskCodUsuario: TMaskEdit;
    edusuario: TEdit;
    lstRelacaoUsuarios: TListBox;
    GroupBox5: TGroupBox;
    meRegistroTreinamento: TMemo;
    GroupBox3: TGroupBox;
    Label7: TLabel;
    edCargaHoraria: TEdit;
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroTreinamentoUsuarios: TfrmRegistroTreinamentoUsuarios;

implementation

uses uPrincipal;

{$R *.DFM}

procedure TfrmRegistroTreinamentoUsuarios.btnNovoClick(Sender: TObject);
begin
  edInstrutor.Text := vvNOM_USUARIO;
end;

end.
