unit uImportaCotasPasse;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, ExtCtrls, StdCtrls, Buttons, Grids, DBGrids;

type
  TfrmImportaCotasPasse = class(TForm)
    Image1: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edCaminhoArquivo: TEdit;
    OpenDialog1: TOpenDialog;
    btnBuscar: TSpeedButton;
    btnImportar: TSpeedButton;
    Bevel1: TBevel;
    Memo1: TMemo;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaCotasPasse: TfrmImportaCotasPasse;
  arqCOTAS : TextFile;
  mmLINHA, strCOTAS : String;
  mmCOTAS : Real;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmImportaCotasPasse.btnBuscarClick(Sender: TObject);
begin
  OpenDialog1.Execute;
  edCaminhoArquivo.Text := OpenDialog1.FileName;
  btnBuscar.Enabled   := False;
  btnImportar.Enabled := True;
end;

procedure TfrmImportaCotasPasse.FormShow(Sender: TObject);
begin
  btnBuscar.Enabled   := True;
  btnImportar.Enabled := False;
end;

procedure TfrmImportaCotasPasse.btnImportarClick(Sender: TObject);
begin

  AssignFile (arqCOTAS, OpenDialog1.FileName);
  Reset(arqCOTAS);

  while not EOF (arqCOTAS) do
  begin
    Readln (arqCOTAS, mmLINHA);

    //Pesquisa a matr�cula no cadastro
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := Copy(mmLINHA,1,8);
        Params.ParamByName ('@pe_cod_unidade').Value   := 0;
        Open;

        if RecordCount > 0 then
        begin
          strCOTAS := Copy(mmLINHA,10,1);
          mmCOTAS  := StrToFloat(strCOTAS);
          mmCOTAS  := mmCOTAS / 2;
          strCOTAS := FloatToStr(mmCOTAS);

          with dmDeca.cdsAlt_CotasPasse do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value   := Copy(mmLINHA,1,8);
            Params.ParamByName ('@pe_num_cotas_passe').Value := Copy(mmLINHA,10,1);
            Execute;
            Memo1.Lines.Add ('Matricula : ' + Copy(mmLINHA,1,8) + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value + ' ->  Cotas Atualizadas para  ' + Copy(mmLINHA,10,1));
          end;
        end
        else
        begin
          Memo1.Lines.Add ('Matricula : ' + Copy(mmLINHA,1,8) + ' n�o encontrada no banco de dados.');
        end;

      end;

    except

    end;

  end;

  edCaminhoArquivo.Clear;
  btnBuscar.Enabled := True;
  btnImportar.Enabled := False;
  


end;

end.
