unit uEscolas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, Db, ComCtrls;

type
  TfrmEscolas = class(TForm)
    dsSel_Escola: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    Panel8: TPanel;
    edNomeEscola: TEdit;
    Label1: TLabel;
    edNomeDiretor: TEdit;
    Label2: TLabel;
    edEnderecoEscola: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edCepEscola: TMaskEdit;
    Label5: TLabel;
    edBairroEscola: TEdit;
    Label6: TLabel;
    edFoneEscola: TMaskEdit;
    Label7: TLabel;
    edEMailEscola: TEdit;
    Label8: TLabel;
    cbRegiao: TComboBox;
    Label9: TLabel;
    cbTipoEscola: TComboBox;
    Panel1: TPanel;
    dbgEscolas: TDBGrid;
    Panel3: TPanel;
    Label10: TLabel;
    edValorPesquisa: TEdit;
    btnPesquisar: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgEscolasDblClick(Sender: TObject);
    procedure AtualizaTelaEscola (Modo: String);
    procedure dbgEscolasCellClick(Column: TColumn);
    procedure dbgEscolasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgEscolasKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure edValorPesquisaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEscolas: TfrmEscolas;

implementation

uses uDM, uPrincipal, uUtil, uVerDadosEscolaSelecionada;

{$R *.DFM}

procedure TfrmEscolas.btnSairClick(Sender: TObject);
begin
  frmEscolas.Close;
end;

procedure TfrmEscolas.FormShow(Sender: TObject);
begin


  if vvIND_PERFIL in [18,19] then
    Panel2.Visible := True
  else
    Panel2.Visible := False;

  AtualizaTelaEscola('Padrao');

  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;
      dbgEscolas.Refresh;
    end;
  except end;
end;

procedure TfrmEscolas.btnNovoClick(Sender: TObject);
begin
  AtualizaTelaEscola('Novo');
  edNomeEscola.SetFocus;
end;

procedure TfrmEscolas.btnCancelarClick(Sender: TObject);
begin
  AtualizaTelaEscola('Padrao');
end;

procedure TfrmEscolas.btnSalvarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_nom_escola').Value := GetValue(edNomeEscola.Text);
      Params.ParamByName ('@pe_nom_endereco').Value := GetValue(edEnderecoEscola.Text);
      Params.ParamByName ('@pe_nom_bairro').Value := GetValue(edBairroEscola.Text);
      Params.ParamByName ('@pe_num_cep').Value := GetValue(edCepEscola.Text);
      Params.ParamByName ('@pe_num_telefone').Value := GetValue(edFoneEscola.Text);
      Params.ParamByName ('@pe_nom_diretor').Value := GetValue(edNomeDiretor.Text);
      Params.ParamByName ('@pe_dsc_email').Value := GetValue(edEMAilEscola.Text);
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;

      if cbTipoEscola.ItemIndex = 0 then
        Params.ParamByName ('@pe_ind_tipo_publica').Value := 'M'
      else if cbTipoEscola.ItemIndex = 1 then
        Params.ParamByName ('@pe_ind_tipo_publica').Value := 'E'
      else if cbTipoEscola.ItemIndex = 2 then
        Params.ParamByName ('@pe_ind_tipo_publica').Value := 'P'
      else Params.ParamByName ('@pe_ind_tipo_publica').Value := 'S';
      
      Execute;

      with dmDeca.cdsSel_Escola do
      begin
        Close;
        Params.ParamByName ('@pe_cod_escola').Value := Null;
        Params.ParamByName ('@pe_nom_escola').Value := Null;
        Open;
        dbgEscolas.Refresh;
      end;

      AtualizaTelaEscola('Padrao');

    end;
  except end;
end;

procedure TfrmEscolas.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_Escola.FieldByName ('cod_escola').Value;
      Params.ParamByName ('@pe_nom_escola').Value := GetValue(edNomeEscola.Text);
      Params.ParamByName ('@pe_nom_endereco').Value := GetValue(edEnderecoEscola.Text);
      Params.ParamByName ('@pe_nom_bairro').Value := GetValue(edBairroEscola.Text);
      Params.ParamByName ('@pe_num_cep').Value := GetValue(edCepEscola.Text);
      Params.ParamByName ('@pe_num_telefone').Value := GetValue(edFoneEscola.Text);
      Params.ParamByName ('@pe_nom_diretor').Value := GetValue(edNomeDiretor.Text);
      Params.ParamByName ('@pe_dsc_email').Value := GetValue(edEMAilEscola.Text);
      Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      //Validar o tipo de escola

      if cbTipoEscola.ItemIndex = 0 then
        Params.ParamByName ('@pe_ind_tipo_publica').Value := 'M'
      else if cbTipoEscola.ItemIndex = 1 then
        Params.ParamByName ('@pe_ind_tipo_publica').Value := 'E'
      else if cbTipoEscola.ItemIndex = 2 then
        Params.ParamByName ('@pe_ind_tipo_publica').Value := 'P'
      else Params.ParamByName ('@pe_ind_tipo_publica').Value := 'S';
      
      Execute;

      with dmDeca.cdsSel_Escola do
      begin
        Close;
        Params.ParamByName ('@pe_cod_escola').Value := Null;
        Params.ParamByName ('@pe_nom_escola').Value := Null;
        Open;
        dbgEscolas.Refresh;
      end;
    end;

    AtualizaTelaEscola('Padrao');

  except end;
end;

procedure TfrmEscolas.dbgEscolasDblClick(Sender: TObject);
begin
  //repassa os valores da tabela para o formulário
  AtualizaTelaEscola('Alterar');
  edNomeEscola.Text := dmDeca.cdsSel_Escola.FieldByName('nom_escola').Value;

  if (Length(dmDeca.cdsSel_Escola.FieldByName('nom_diretor').Value)>0) then
    edNomeDiretor.Text := GetValue(dmDeca.cdsSel_Escola.FieldByName('nom_diretor').AsString)
  else
    edNomeDiretor.Text := 'ND';

  edEnderecoEscola.Text := GetValue(dmDeca.cdsSel_Escola.FieldByName('nom_endereco').AsString);
  edCepEscola.Text      := GetValue(dmDeca.cdsSel_Escola.FieldByName('num_cep').AsString);
  edFoneEscola.Text     := GetValue(dmDeca.cdsSel_Escola.FieldByName('num_telefone').AsString);
  edBairroEscola.Text   := GetValue(dmDeca.cdsSel_Escola.FieldByName('nom_bairro').AsString);
  edEMailEscola.Text    := GetValue(dmDeca.cdsSel_Escola.FieldByName('dsc_email').AsString);

  //Valida o campo ind_tipo_publica
  if dmDeca.cdsSel_Escola.FieldByName('ind_tipo_publica').Value = 'M' then
    cbTipoEscola.ItemIndex := 0
  else if dmDeca.cdsSel_Escola.FieldByName('ind_tipo_publica').Value = 'E' then
    cbTipoEscola.ItemIndex := 1
  else if dmDeca.cdsSel_Escola.FieldByName('ind_tipo_publica').Value = 'P' then
    cbTipoEscola.ItemIndex := 2
  else cbTipoEscola.ItemIndex := 3;

end;

procedure TfrmEscolas.AtualizaTelaEscola(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    edNomeEscola.Clear;
    edNomeDiretor.Clear;
    edEnderecoEscola.Clear;
    edCepEscola.Clear;
    edBairroEscola.Clear;
    edFoneEscola.Clear;
    cbRegiao.ItemIndex := -1;
    edEMailEscola.Clear;
    cbTipoEscola.ItemIndex := -1;
    Panel8.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin
    Panel8.Enabled := True;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end
  else if Modo = 'Alterar' then
  begin
    Panel8.Enabled := True;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmEscolas.dbgEscolasCellClick(Column: TColumn);
begin
  edNomeEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('nom_escola').AsString;
  edNomeDiretor.Text     := dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').AsString;
  edEnderecoEscola.Text  := dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').AsString;
  edCepEscola.Text       := dmDeca.cdsSel_Escola.FieldByName ('num_cep').AsString;
  edBairroEscola.Text    := dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').AsString;
  edFoneEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('num_telefone').AsString;
  edEMailEscola.Text     := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').AsString;
  if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'M' then cbTipoEscola.ItemIndex := 0
  else if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'E' then cbTipoEscola.ItemIndex := 1
  else if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'P' then cbTipoEscola.ItemIndex := 2;
end;

procedure TfrmEscolas.dbgEscolasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  edNomeEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('nom_escola').AsString;
  edNomeDiretor.Text     := dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').AsString;
  edEnderecoEscola.Text  := dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').AsString;
  edCepEscola.Text       := dmDeca.cdsSel_Escola.FieldByName ('num_cep').AsString;
  edBairroEscola.Text    := dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').AsString;
  edFoneEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('num_telefone').AsString;
  edEMailEscola.Text     := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').AsString;
  if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'M' then cbTipoEscola.ItemIndex := 0
  else if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'E' then cbTipoEscola.ItemIndex := 1
  else if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'P' then cbTipoEscola.ItemIndex := 2;
end;

procedure TfrmEscolas.dbgEscolasKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  edNomeEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('nom_escola').AsString;
  edNomeDiretor.Text     := dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').AsString;
  edEnderecoEscola.Text  := dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').AsString;
  edCepEscola.Text       := dmDeca.cdsSel_Escola.FieldByName ('num_cep').AsString;
  edBairroEscola.Text    := dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').AsString;
  edFoneEscola.Text      := dmDeca.cdsSel_Escola.FieldByName ('num_telefone').AsString;
  edEMailEscola.Text     := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').AsString;
  if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'M' then cbTipoEscola.ItemIndex := 0
  else if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'E' then cbTipoEscola.ItemIndex := 1
  else if dmDeca.cdsSel_Escola.FieldByName ('ind_tipo_publica').AsString = 'P' then cbTipoEscola.ItemIndex := 2;
end;

procedure TfrmEscolas.SpeedButton2Click(Sender: TObject);
begin
  try

    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;
      dbgEscolas.Refresh;
    end;


  except

  end;
end;

procedure TfrmEscolas.btnPesquisarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := '%' + edValorPesquisa.Text + '%';
      Open;
      dbgEscolas.Refresh;
      edValorPesquisa.Clear;
    end;
  except
  end;
end;

procedure TfrmEscolas.edValorPesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then btnPesquisar.Click;
end;

end.
