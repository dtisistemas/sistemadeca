unit uInicializarPresenca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls, Grids, DBGrids, Db;

type
  TfrmInicializarPresenca = class(TForm)
    rgCriterio: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnGravar: TBitBtn;
    DBGrid1: TDBGrid;
    ProgressBar1: TProgressBar;
    btnNovo: TBitBtn;
    Panel3: TPanel;
    Label2: TLabel;
    edAno: TEdit;
    cbMes: TComboBox;
    btnExcluir: TBitBtn;
    DataSource1: TDataSource;
    chMes: TCheckBox;
    procedure AtualizaCamposBotoes (Modo : String);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSairClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure rgCriterioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chMesClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnGravarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInicializarPresenca: TfrmInicializarPresenca;

implementation

uses uDM, uFichaPesquisa, uPrincipal, uUtil;

{$R *.DFM}

{ TfrmInicializarPresenca }

procedure TfrmInicializarPresenca.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    rgCriterio.ItemIndex := 0; //Padr�o por Matr�cula
    rgCriterio.Enabled := False;
    txtMatricula.Clear;
    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    lbNome.Caption := '';
    edAno.Clear;
    edAno.Enabled := False;
    cbMes.ItemIndex := -1;
    //cbMes.Items.Clear;
    cbMes.Enabled := False;
    DBGrid1.Enabled := True;

    btnNovo.Enabled := True;
    btnGravar.Enabled := False;
    btnExcluir.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin

    rgCriterio.Enabled := True;
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    edAno.Enabled := True;
    cbMes.Enabled := False;
    DBGrid1.Enabled := False;

    btnNovo.Enabled := False;
    btnGravar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;

  end;
end;

procedure TfrmInicializarPresenca.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmInicializarPresenca.btnSairClick(Sender: TObject);
begin
  frmInicializarPresenca.Close;
end;

procedure TfrmInicializarPresenca.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmInicializarPresenca.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Novo');
end;

procedure TfrmInicializarPresenca.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

        with dmDeca.cdsSel_Cadastro_frequencia do
        begin
          Close;
          Params.ParamByName ('@pe_id_frequencia').Value := Null;
          //Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName ('@pe_num_ano').Value := Null;
          Params.ParamByName ('@pe_num_mes').Value := Null;
          Open;

          DBGriD1.Enabled := True;
          DBGrid1.Refresh;
        end
      end
    except end;
  end;
end;

procedure TfrmInicializarPresenca.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        DBGrid1.Enabled := True;
      end;
    except end;
  end;
end;

procedure TfrmInicializarPresenca.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmInicializarPresenca.rgCriterioClick(Sender: TObject);
begin
  if rgCriterio.ItemIndex = 0 then
  begin
    GroupBox1.Enabled := False;
    txtMatricula.Clear;
    lbNome.Caption := '';
    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    edAno.SetFocus;
  end
  else if rgCriterio.ItemIndex = 1 then
  begin
    GroupBox1.Enabled := True;
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtMatricula.SetFocus;
  end;
end;

procedure TfrmInicializarPresenca.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
  with dmDeca.cdsSel_Cadastro_Frequencia do
  begin
    Close;
    Params.ParamByName ('@pe_id_frequencia').Value := Null;
    //Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_num_ano').Value := Null;
    Params.ParamByName ('@pe_num_mes').Value := Null;
    Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
    Open;
    DBGrid1.Refresh;
  end;
end;

procedure TfrmInicializarPresenca.chMesClick(Sender: TObject);
begin
  if chMes.Checked = True then
    cbMes.Enabled := True
  else if chMes.Checked = False then
    cbMes.Enabled := False;

end;

procedure TfrmInicializarPresenca.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmInicializarPresenca.Close;
  end;
end;

procedure TfrmInicializarPresenca.btnGravarClick(Sender: TObject);
var x: Integer;
    vv_STRDATA : String;
    vv_DATA : TDateTime;

    //Vari�veis para c�lculo do n�mero de semanas
    str_data_inicial0 : String;
    data_inicial0, data_final0 : TDateTime;
    num_semanas01, num_semanas02, num_semanas03, num_semanas04, num_semanas05, num_semanas06, num_semanas07, num_semanas08, num_semanas09, num_semanas10,
    num_semanas11, num_semanas12, num_semanas13, num_semanas14, num_semanas15, num_semanas16, num_semanas17, num_semanas18, num_semanas19, num_semanas20,
    num_semanas21, num_semanas22, num_semanas23, num_semanas24, num_semanas25, num_semanas26, num_semanas27, num_semanas28, num_semanas29, num_semanas30,
    num_semanas31 : Integer;

    tt_faltas : Integer;

begin
tt_faltas := 0;
x := 0;
try
  case rgCriterio.ItemIndex of   //Todos da Unidade
  0:begin
    if Application.MessageBox('Deseja "INICIALIZAR" faltas a TODOS do cadastro?' + #13+#10 +
           'Todos os lan�amentos existentes ser�o regravados. Confirma?',
           '[Sistema Deca] - Lan�amento de Frequ�ncia',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      //Valida o campo ANO

      if (edAno.Text <> '') then
      begin
        //Se selecionou algum m�s
        if (chMes.Checked = True) then
        begin
          //Excluir todos os dados do cadastro de frequencia para o ano/m�s informado

          with dmDeca.cdsExc_Cadastro_Frequencia do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := Null;
            Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(Trim(edAno.Text));
            Params.ParamByName ('@pe_num_mes').AsInteger := cbMes.ItemIndex + 1;
            Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
            Execute;
          end;

          //Incluir presen�a para todos no cadastro somente para o m�s selecionado
          //Enquanto n�o fim do cadastro - definir como selecionar todos do cadastro
          with dmDeca.cdsSel_Cadastro do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := Null;
            Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
            Open;
            ProgressBar1.Max := RecordCount;
          end;

          ProgressBar1.Position := 0;

          dmDeca.cdsSel_Cadastro.First;
          x := cbMes.ItemIndex + 1;
          while not (dmDeca.cdsSel_Cadastro.Eof) do
          begin
            with dmDeca.cdsInc_Cadastro_Frequencia do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;
              Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
              Params.ParamByName ('@pe_num_mes').AsInteger := x;

              //Dia 01
              tt_faltas := 0;
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '01' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '01' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 1;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia01').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia01').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas01 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia01').Value := num_semanas01;

                end;

              //Dia 02
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '02' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '02' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 2;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia02').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia02').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas02 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia02').Value := num_semanas02;

                end;

              //Dia 03
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '03' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '03' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 3;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia03').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia03').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas03 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia03').Value := num_semanas03;

                end;

              //Dia 04
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '04' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '04' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 4;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia04').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia04').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas04 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia04').Value := num_semanas04;

                end;

              //Dia 05
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '05' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '05' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 5;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia05').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia05').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas05 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia05').Value := num_semanas05;

                end;

              //Dia 06
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '06' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '06' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 6;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia06').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia06').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas06 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia06').Value := num_semanas06;

                end;

              //Dia 07
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '07' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '07' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 7;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia07').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia07').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas07 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia07').Value := num_semanas07;

                end;

              //Dia 08
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '08' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '08' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 8;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia08').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia08').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas08 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia08').Value := num_semanas08;

                end;

              //Dia 09
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '09' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '09' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 9;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia09').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia09').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas09 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia09').Value := num_semanas09;

                end;

              //Dia 10
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '10' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '10' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 10;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia10').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia10').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas10 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia10').Value := num_semanas10;

                end;

              //Dia 11
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '11' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '11' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 11;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia11').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia11').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas11 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia11').Value := num_semanas11;

                end;

              //Dia 12
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '12' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '12' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 12;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia12').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia12').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas12 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia12').Value := num_semanas12;

                end;

              //Dia 13
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '13' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '13' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 13;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia13').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia13').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas13 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia13').Value := num_semanas13;

                end;

              //Dia 14
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '14' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '14' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 14;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia14').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia14').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas14 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia14').Value := num_semanas14;

                end;

              //Dia 15
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '15' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '15' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 15;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia15').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia15').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas15 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia15').Value := num_semanas15;

                end;

              //Dia 16
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '16' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '16' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 16;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia16').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia16').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas16 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia16').Value := num_semanas16;

                end;

              //Dia 17
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '17' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '17' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 17;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia17').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia17').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas17 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia17').Value := num_semanas17;

                end;

              //Dia 18
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '18' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '18' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 18;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia18').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia18').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas18 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia18').Value := num_semanas18;

                end;

              //Dia 19
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '19' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '19' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 19;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia19').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia19').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas19 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia19').Value := num_semanas19;

                end;

              //Dia 20
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '20' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '20' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 20;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia20').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia20').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas20 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia20').Value := num_semanas20;

                end;

              //Dia 21
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '21' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '21' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 21;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia21').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia21').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas21 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia21').Value := num_semanas21;

                end;

              //Dia 22
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '22' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '22' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 22;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia22').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia22').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas22 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia22').Value := num_semanas22;

                end;

              //Dia 23
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '23' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '23' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 23;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia23').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia23').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas23 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia23').Value := num_semanas23;

                end;

              //Dia 24
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '24' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '24' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 24;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia24').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia24').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas24 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia24').Value := num_semanas24;

                end;

              //Dia 25
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '25' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '25' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 25;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia25').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia25').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas25 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia25').Value := num_semanas25;

                end;

              //Dia 26
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '26' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '26' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 26;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia26').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia26').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas26 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia26').Value := num_semanas26;

                end;

              //Dia 27
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '27' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '27' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 27;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia27').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia27').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas27 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia27').Value := num_semanas27;

                end;

              //Dia 28
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '28' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '28' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 28;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia28').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia28').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'S'
                    else
                      //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'P';
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'F';
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas28 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia28').Value := num_semanas28;

                end;

              //Dia 29
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '29' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '29' + '/' + IntToStr(x) + '/' + edAno.Text;
              //vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 29;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia29').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia29').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    try
                      vv_DATA := StrToDate(vv_STRDATA);
                      if (DayOfWeek(vv_DATA)= 1) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'D'
                      else if (DayOfWeek(vv_DATA)= 7) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'S'
                      else
                        //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'P'
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'F';
                    except
                    on EConvertError do
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').Value := Null;
                    end;
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas29 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia29').Value := num_semanas29;

                end;

              //Dia 30
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '30' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '30' + '/' + IntToStr(x) + '/' + edAno.Text;
              //vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 30;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia30').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia30').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    try
                      vv_DATA := StrToDate(vv_STRDATA);
                      if (DayOfWeek(vv_DATA)= 1) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'D'
                      else if (DayOfWeek(vv_DATA)= 7) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'S'
                      else
                        //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'P'
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'F';
                    except
                    on EConvertError do
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').Value := Null;
                    end;
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas30 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia30').Value := num_semanas30;

                end;

              //Dia 31
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '31' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '31' + '/' + IntToStr(x) + '/' + edAno.Text;
              //vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 31;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia31').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia31').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    try
                      vv_DATA := StrToDate(vv_STRDATA);
                      if (DayOfWeek(vv_DATA)= 1) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'D'
                      else if (DayOfWeek(vv_DATA)= 7) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'S'
                      else
                        //dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'P'
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'F';
                    except
                    on EConvertError do
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').Value := Null;
                    end;
                  end;
                  tt_faltas := tt_faltas + 1;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas31 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia31').Value := num_semanas31;

                end;

              dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_num_faltas').AsInteger := tt_faltas - 1;
              dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;

              dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_dsr_acumulado').Value := 0;
              dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_exportou').Value := 0;
              dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_dsc_observacoes').AsString := '-';
              dmDeca.cdsInc_Cadastro_Frequencia.Execute;

              ProgressBar1.Position := ProgressBar1.Position + 1;

              dmDeca.cdsSel_Cadastro.Next;

              //Atualiza o Grid
              with dmDeca.cdsSel_Cadastro_Frequencia do
              begin
                Close;
                Params.ParamByName ('@pe_id_frequencia').Value := Null;
                Params.ParamByName ('@pe_cod_matricula').Value := Null;
                Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName ('@pe_num_mes').AsInteger := x; //cbMes.ItemIndex + 1;
                Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vVCOD_USUARIO;
                Open;
                DBGrid1.Refresh;
              end; // fim para la�o para atualiza��o do Grid apos inser��o

            end; //Fim da procedure de inclusao no cadastro_frequencia
          end; //Fim do la�o while not eof
        //end; //Final do la�o do cbMes.checked = true

        AtualizaCamposBotoes ('Padr�o');

      end
      else
      begin
        ShowMessage ('Favor informar ANO v�lido para lan�amento de Presen�as...');
        edAno.SetFocus;
      end;
    end;
  end;
  end;

  //Para matr�cula espec�fica
  1:begin
    if Application.MessageBox('Deseja lan�ar presen�a para a matr�cula?' + #13+#10 +
           'Todos os lan�amentos existentes ser�o regravados. Confirma?',
           'Frequ�ncia - Lan�amento',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      //Valida o campo ANO

      if (edAno.Text <> '') then
      begin

        //Verificar qual o m�s a ser lan�ado
        //Se selecionar um m�s
        if (chMes.Checked = True) then
        begin
          //Excluir todos os dados da matr�cula do cadastro de frequencia para o ano informado
          with dmDeca.cdsExc_Cadastro_Frequencia do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
            Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(Trim(edAno.Text));
            Params.ParamByName ('@pe_num_mes').Value := Null;
            Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
            Execute;
          end;

          //Incluir presen�a para uma matr�cula espec�fica no cadastro somente para o m�s selecionado

          //Enquanto n�o fim do cadastro - selecionar a matr�cula
          with dmDeca.cdsSel_Cadastro do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
            Open;

            ProgressBar1.Max := RecordCount;

          end;

          ProgressBar1.Position := 0;

          dmDeca.cdsSel_Cadastro.First;
          while not (dmDeca.cdsSel_Cadastro.Eof) do
          begin
            //for x := 1 to 12 do

            //begin

              x := cbMes.ItemIndex + 1;

              with dmDeca.cdsInc_Cadastro_Frequencia do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;
                Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName ('@pe_num_mes').AsInteger := x;

              //Dia 01
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '01' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '01' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 1;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia01').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia01').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia01').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas01 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia01').Value := num_semanas01;

                end;

              //Dia 02
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '02' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '02' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 2;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia02').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia02').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia02').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas02 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia02').Value := num_semanas02;

                end;

              //Dia 03
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '03' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '03' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 3;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia03').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia03').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia03').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas03 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia03').Value := num_semanas03;

                end;

              //Dia 04
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '04' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '04' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);            

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 4;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia04').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia04').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia04').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas04 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia04').Value := num_semanas04;

                end;

              //Dia 05
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '05' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '05' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 5;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia05').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia05').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia05').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas05 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia05').Value := num_semanas05;

                end;

              //Dia 06
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '06' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '06' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 6;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia06').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia06').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia06').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas06 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia06').Value := num_semanas06;

                end;

              //Dia 07
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '07' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '07' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 7;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia07').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia07').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia07').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas07 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia07').Value := num_semanas05;

                end;

              //Dia 08
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '08' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '08' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 8;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia08').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia08').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia08').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas08 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia08').Value := num_semanas08;

                end;

              //Dia 09
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '09' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '09' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 9;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia09').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia09').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia09').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas09 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia09').Value := num_semanas05;

                end;

              //Dia 10
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '10' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '10' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 10;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia10').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia10').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia10').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas10 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia10').Value := num_semanas10;

                end;

              //Dia 11
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '11' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '11' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 11;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia11').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia11').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia11').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas11 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia11').Value := num_semanas11;

                end;

              //Dia 12
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '12' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '12' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 12;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia12').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia12').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia12').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas12 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia12').Value := num_semanas12;

                end;

              //Dia 13
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '13' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '13' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 13;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia13').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia13').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia13').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas13 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia13').Value := num_semanas13;

                end;

              //Dia 14
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '14' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '14' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 14;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia14').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia14').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia14').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas14 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia14').Value := num_semanas14;

                end;

              //Dia 15
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '15' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '15' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 15;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia15').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia15').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia15').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas15 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia15').Value := num_semanas15;

                end;

              //Dia 16
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '16' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '16' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 16;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia16').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia16').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia16').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas16 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia16').Value := num_semanas16;

                end;

              //Dia 17
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '17' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '17' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 17;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia17').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia17').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia17').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas17 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia17').Value := num_semanas17;

                end;

              //Dia 18
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '18' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '18' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 18;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia18').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia18').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia18').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas18 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia18').Value := num_semanas18;

                end;

              //Dia 19
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '19' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '19' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 19;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia19').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia19').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia19').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas19 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia19').Value := num_semanas19;

                end;

              //Dia 20
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '20' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '20' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 20;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia20').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia20').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia20').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas20 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia20').Value := num_semanas20;

                end;

              //Dia 21
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '21' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '21' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 21;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia21').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia21').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia21').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas21 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia21').Value := num_semanas21;

                end;

              //Dia 22
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '22' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '22' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 22;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia22').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia22').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia22').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas22 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia22').Value := num_semanas22;

                end;

              //Dia 23
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '23' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '23' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 23;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia23').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia23').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia23').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas23 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia23').Value := num_semanas23;

                end;

              //Dia 24
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '24' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '24' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 24;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia24').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia24').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia24').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas24 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia24').Value := num_semanas24;

                end;

              //Dia 25
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '25' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '25' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 25;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia25').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia25').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia25').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas25 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia25').Value := num_semanas25;

                end;

              //Dia 26
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '26' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '26' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 26;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia26').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia26').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia26').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas26 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia26').Value := num_semanas26;

                end;

              //Dia 27
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '27' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '27' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 27;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia27').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia27').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia27').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas27 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia27').Value := num_semanas27;

                end;

              //Dia 28
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '28' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '28' + '/' + IntToStr(x) + '/' + edAno.Text;
              vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 28;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia28').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia28').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    if (DayOfWeek(vv_DATA)= 1) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'D'
                    else if (DayOfWeek(vv_DATA)= 7) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'S'
                    else
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia28').AsString := 'P';
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas28 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia28').Value := num_semanas28;

                end;

              //Dia 29
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '29' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '29' + '/' + IntToStr(x) + '/' + edAno.Text;
              //vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 29;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia29').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia29').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    try
                      vv_DATA := StrToDate(vv_STRDATA);
                      if (DayOfWeek(vv_DATA)= 1) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'D'
                      else if (DayOfWeek(vv_DATA)= 7) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'S'
                      else
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').AsString := 'P'
                    except
                    on EConvertError do
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia29').Value := Null;
                    end;
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas29 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia29').Value := num_semanas29;

                end;

              //Dia 30
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '30' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '30' + '/' + IntToStr(x) + '/' + edAno.Text;
              //vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 30;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia30').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia30').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    try
                      vv_DATA := StrToDate(vv_STRDATA);
                      if (DayOfWeek(vv_DATA)= 1) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'D'
                      else if (DayOfWeek(vv_DATA)= 7) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'S'
                      else
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').AsString := 'P'
                    except
                    on EConvertError do
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia30').Value := Null;
                    end;
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas30 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia30').Value := num_semanas30;

                end;

              //Dia 31
              if Length(IntToStr(x)) = 1 then vv_STRDATA := '31' + '/0' + IntToStr(x) + '/' + edAno.Text
              else vv_STRDATA := '31' + '/' + IntToStr(x) + '/' + edAno.Text;
              //vv_DATA := StrToDate(vv_STRDATA);

              //Verificar se a data est� cadastrada em Itens_Calend�rio (Feriado, Compensado,...)
              with dmDeca.cdsSel_ItensCalendario do
              begin
                Close;
                Params.ParamByName('@pe_cod_calendario').Value := Null;
                Params.ParamByName('@pe_ind_tipo_feriado').Value := Null;
                Params.ParamByName('@pe_num_dia').AsInteger := 31;
                Params.ParamByName('@pe_num_mes').AsInteger := x;
                Params.ParamByName('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName('@pe_num_semana').Value := Null;
                Open;

                //Se achou a data, verifica qual tipo de "feriado" est� lan�ado
                if (dmDeca.cdsSel_ItensCalendario.RecordCount > 0) then
                begin
                  if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger > 0) and
                     (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger <= 4) then //Se feriado nacional, munic, estadual
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia31').Value := 'R'
                  else if (dmDeca.cdsSel_ItensCalendario.FieldByname('ind_tipo_feriado').AsInteger = 5) then
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName('@pe_flg_dia31').Value := 'C'
                  end
                  else if (dmDeca.cdsSel_ItensCalendario.RecordCount < 1) then
                  begin
                    try
                      vv_DATA := StrToDate(vv_STRDATA);
                      if (DayOfWeek(vv_DATA)= 1) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'D'
                      else if (DayOfWeek(vv_DATA)= 7) then
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'S'
                      else
                        dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').AsString := 'P'
                    except
                    on EConvertError do
                      dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_dia31').Value := Null;
                    end;
                  end;

                  //Calcula o dia da semana para a data
                  str_data_inicial0 := '01/01/' + edAno.Text;
                  data_inicial0 := StrToDate(str_data_inicial0);
                  data_final0 := vv_DATA;
                  num_semanas31 := Trunc(data_final0 - data_inicial0) DIV 7 + 1;

                  dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_semana_dia31').Value := num_semanas31;

                end;

                dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_num_faltas').AsInteger := 30;
                dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;

                dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_dsr_acumulado').Value := 0;
                dmDeca.cdsInc_Cadastro_Frequencia.Params.ParamByName ('@pe_flg_exportou').Value := 0;
                dmDeca.cdsInc_Cadastro_Frequencia.Execute;

                ProgressBar1.Position := ProgressBar1.Position + 1;

                dmDeca.cdsSel_Cadastro.Next;

                with dmDeca.cdsSel_Cadastro_Frequencia do
                begin
                  Close;
                  Params.ParamByName ('@pe_id_frequencia').Value := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value := Null;
                  Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                  Params.ParamByName ('@pe_num_mes').Value := Null;
                  Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vVCOD_USUARIO;
                  Open;

                  DBGrid1.Refresh;

                end; // fim para la�o para atualiza��o do Grid apos inser��o

              end; //Fim da procedure de inclusao no cadastro_frequencia

            //end; //Fim do for next

            ProgressBar1.Position := ProgressBar1.Position + 1;

            dmDeca.cdsSel_Cadastro.Next;

          end;

        AtualizaCamposBotoes ('Padr�o');

        end

        //Se n�o selecionar nenhum m�s

        else if (chMes.Checked = False) then
        begin
          ShowMessage ('Favor informar ANO v�lido para lan�amento de Presen�as...');
          edAno.SetFocus;
        end;
    end
  end;

  AtualizaCamposBotoes ('Padr�o');
end

end
except end;
end;
end.
