unit uTransferenciaEncaminhamentoNova;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, jpeg, ExtCtrls, Buttons, Mask, Psock, NMsmtp;

type
  TfrmNovaTransferencia = class(TForm)
    Shape1: TShape;
    Shape2: TShape;
    Image1: TImage;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox4: TGroupBox;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    Label3: TLabel;
    Label5: TLabel;
    edEndereco: TEdit;
    edBairro: TEdit;
    cbTipoTransferencia: TComboBox;
    Label4: TLabel;
    Label6: TLabel;
    cbMotivos: TComboBox;
    Label7: TLabel;
    edUnidadeOrigem: TEdit;
    Label8: TLabel;
    edCcustoOrigem: TEdit;
    Label9: TLabel;
    cbBancoOrigem: TComboBox;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    cbBancoDestino: TComboBox;
    Panel2: TPanel;
    Panel3: TPanel;
    Label15: TLabel;
    cbEntregaPasse: TComboBox;
    Label16: TLabel;
    cbCotas: TComboBox;
    Label17: TLabel;
    Label18: TLabel;
    cbUnidadeDestino: TComboBox;
    Label10: TLabel;
    meDataTransferencia: TMaskEdit;
    edNumSecaoOrigem: TEdit;
    edCcustoDestino: TEdit;
    edNumSecaoDestino: TEdit;
    Label11: TLabel;
    cbSecaoDestino: TComboBox;
    Label19: TLabel;
    cbPeriodo: TComboBox;
    Label20: TLabel;
    lbIdade: TLabel;
    Label21: TLabel;
    NMSMTP1: TNMSMTP;
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure AtualizaTela(ModoTela:String);
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeDestinoClick(Sender: TObject);
    procedure cbSecaoDestinoClick(Sender: TObject);
    procedure cbUnidadeDestinoExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure VerificaSituacaoBimestre(cdMatricula:String);
    procedure cbUnidadeDestinoEnter(Sender: TObject);
    procedure CarregaUnidades4070;
    procedure cbMotivosExit(Sender: TObject);
    procedure CarregaUnidades70;
    procedure CarregaUnidades30;
    procedure CarregaUnidades30063004;
    procedure CarregaTODAS;
    procedure CarregaUnidades41;
    procedure CarregaUnidades31;
    procedure CarregaUnidades51;
    procedure cbMotivosClick(Sender: TObject);
    procedure CarregaUnidades31_5107_5109;
    procedure Carrega5102;
    procedure CarregaBolsistasDE;
    procedure CarregaAprendizesDE;
    Function CalculaIdadeTransf( Nascimento : TDateTime ) : String ;
    procedure CarregaDR1_DR2;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovaTransferencia: TfrmNovaTransferencia;
  vvPODE_TRANSFERIR, vvPODE_IMPRIMIR : Boolean;
  mmCCUSTO_ORIGEM : String;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil, rNovaTransferencia,
  uFichaCrianca;

{$R *.DFM}

procedure TfrmNovaTransferencia.AtualizaTela(ModoTela: String);
begin
  if ModoTela = 'Padrao' then
  begin

    txtMatricula.Clear;
    lbNome.Caption := '';
    edEndereco.Clear;
    edBairro.Clear;
    GroupBox1.Enabled := False;
    lbIdade.Caption := '';

    //Painel de Origem
    cbTipoTransferencia.ItemIndex := -1;
    cbMotivos.ItemIndex := -1;
    edUnidadeOrigem.Clear;
    edCcustoOrigem.Clear;
    edNumSecaoOrigem.Clear;
    cbBancoOrigem.ItemIndex := -1;
    GroupBox4.Enabled := False;

    //Painel de Destino
    meDataTransferencia.Clear;
    cbUnidadeDestino.ItemIndex := -1;
    cbSecaoDestino.ItemIndex := -1;
    edCcustoDestino.Clear;
    edNumSecaoDestino.Clear;
    cbBancoDestino.ItemIndex := -1;
    cbEntregaPasse.ItemIndex := -1;
    cbCotas.ItemIndex := -1;
    cbPeriodo.ItemIndex := -1;
    GroupBox2.Enabled := False;

    //Bot�es
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
    btnSair.Enabled := True;

  end
  else if ModoTela = 'Novo' then
  begin

    GroupBox1.Enabled := True;
    GroupBox4.Enabled := True;
    GroupBox2.Enabled := True;

    //Limpa os campos
    txtMatricula.Clear;
    lbNome.Caption := '';
    cbTipoTransferencia.ItemINdex := -1;
    cbMotivos.ItemIndex := -1;
    edUnidadeOrigem.Clear;
    edCcustoOrigem.Clear;
    edNumSecaoOrigem.Clear;
    cbBancoOrigem.ItemIndex := -1;

    meDataTransferencia.Text := DateToStr(Date());
    cbUnidadeDestino.ItemIndex := -1;
    cbSecaoDestino.ItemIndex := -1;
    edCcustoDestino.Clear;
    edNumSecaoDestino.Clear;
    cbBancoDestino.ItemIndex := -1;
    cbEntregaPasse.ItemIndex := -1;
    cbCotas.ItemIndex := -1;
    cbPeriodo.ItemIndex := -1;

    //Bot�es
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;

  end;
end;

procedure TfrmNovaTransferencia.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').AsInteger:= vvCOD_UNIDADE;
        Open;

        //VerificaSituacaoBimestre(dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').AsString);

        //if (StatusCadastro(txtMatricula.Text) = 'Ativo') and (vvPODE_TRANSFERIR = True) then
        if (StatusCadastro(txtMatricula.Text) = 'Ativo') then
        begin
          lbNome.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_nome').AsString;
          edEndereco.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_endereco').AsString;
          edBairro.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_bairro').AsString;
          //meDataTransferencia.SetFocus;
          cbMotivos.SetFocus;
          edCcustoOrigem.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_ccusto').AsString;
          edNumSecaoOrigem.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao').AsString;
          edUnidadeOrigem.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('vUnidade').AsString;

          //lbIdade.Caption := frmFichaCrianca.CalculaIdade(FieldByName ('dat_nascimento').AsDateTime);
          //Calcula a idade em anos e meses a partir da data da transfer�ncia do fdp
          lbIdade.Caption := CalculaIdadeTransf(FieldByName ('dat_nascimento').AsDateTime);


          //Carrega os dados da Se��o
          with dmDeca.cdsSel_Secao do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_secao').Value := Null;
            Params.ParamByName ('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').AsInteger;
            Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;

            if (Length(edNumSecaoOrigem.Text)<1) or (edNumSecaoOrigem.Text = '') then
              Params.ParamByName ('@pe_num_secao').Value := Null
            else
              Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;

            Params.ParamByName ('@pe_flg_status').Value := Null;
            Open;

            if (RecordCount > 0) then
            begin
              edNumSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
              //edNumSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
            end
            else
            begin
              edNumSecaoOrigem.Text := '';
            end;
          end;

          Application.MessageBox ('VERIFIQUE A UNIDADE A QUAL VOC� FAR� A TRANSFER�NCIA, ANTES DE FAZ�-LA EM DEFINITIVO...',
                                  '[Sistema Deca] - Transfer�ncia',
                                  MB_OK + MB_ICONINFORMATION);


          {**********************************************************************************************************************}
          {***** RELACIONA UNIDADES POSS�VEIS DE SEREM TRANSFERIDOS *****}

          //Verificar TIPO e MOTIVO da transfer�ncia pelo Centro de Custo atual
          //para defini��o do(s) destino...

          mmCCUSTO_ORIGEM := Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_ccusto').Value);

          {
          //Habilita somente DC/DAPAE
          if (Copy(mmCCUSTO_ORIGEM,1,1) = '4') then
          begin
            cbTipoTransferencia.ItemIndex := 0;
            cbTipoTransferencia.Enabled   := False;
            cbMotivos.ItemIndex           := 0; //Somente para Outra unidade
            cbMotivos.Enabled             := False;
            meDataTransferencia.SetFocus;
          end
          else if (Copy(mmCCUSTO_ORIGEM,1,1) = '7') then
          begin
            cbTipoTransferencia.ItemIndex := 0;
            cbTipoTransferencia.Enabled   := True;
            cbMotivos.Enabled             := True;
            cbMotivos.SetFocus;
          end
          else if (Copy(mmCCUSTO_ORIGEM,1,1) = '3') and (mmCCUSTO_ORIGEM <> '3006') or (mmCCUSTO_ORIGEM <> '3004') then
          begin
            cbTipoTransferencia.ItemIndex := 1;
            cbTipoTransferencia.Enabled := False;
            cbMotivos.SetFocus;
          end
          else if (Copy(mmCCUSTO_ORIGEM,1,1) = '3') and (mmCCUSTO_ORIGEM = '3006') or (mmCCUSTO_ORIGEM = '3004') then
          begin
            cbTipoTransferencia.ItemIndex := 1;
            cbTipoTransferencia.Enabled := False;
            cbMotivos.SetFocus;
          end;

          //Se unidades 3006 e 3004 carrega apenas 3004 e 3006
          if (mmCCUSTO_ORIGEM = '3004') or (mmCCUSTO_ORIGEM = '3006') then
          begin
            cbTipoTransferencia.ItemIndex := 0;
            cbTipoTransferencia.Enabled   := False;
            cbMotivos.ItemIndex           := 0;
            cbMotivos.Enabled             := False;
          end;

          }
          //Se unidade 2003 (Triagem)- ex-6041, carrega todos os centros de custo...
         if (Copy(mmCCUSTO_ORIGEM,1,4) = '9114') then
          begin
            cbTipoTransferencia.ItemIndex := 1;
            cbTipoTransferencia.Enabled   := True;
            cbMotivos.ItemIndex           := 0;
            cbMotivos.Enabled             := True;
            CarregaTODAS;
          end;


        //end
        end
        else
        begin
          //ShowMessage('Aten��o!!! Essa crian�a/adolescente encontra-se' +#13+#10+#13+#10 +
          //     'em situa��o ' + StatusCadastro(txtMatricula.Text)+ '. Favor regularizar...');
          //AtualizaCamposBotoes ('Padrao');
          Application.MessageBox ('Transfer�ncia n�o p�de ser executada. Verifique situa��o da crian�a/adolescente ou resgistros de escolaridade.',
                                  '[Sistema Deca] - Transfer�ncia cancelada',
                                  MB_OK + MB_ICONINFORMATION);
          AtualizaTela ('Padrao');

        end;
      end;
    except end;
  end;
end;

procedure TfrmNovaTransferencia.txtMatriculaExit(Sender: TObject);
begin
  //if vvCOD_MATRICULA_PESQUISA <> 'x' then
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    //txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').AsInteger:= vvCOD_UNIDADE;
        Open;

        if StatusCadastro(txtMatricula.Text) = 'Ativo' then
        begin
          lbNome.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_nome').AsString;
          edEndereco.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_endereco').AsString;
          edBairro.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_bairro').AsString;
          meDataTransferencia.SetFocus;
          edCcustoOrigem.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_ccusto').AsString;
          edNumSecaoOrigem.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao').AsString;

          //Carrega os dados da Se��o
          with dmDeca.cdsSel_Secao do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_secao').Value := Null;
            Params.ParamByName ('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').AsInteger;
            Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;

            if (Length(edNumSecaoOrigem.Text)<1) or (edNumSecaoOrigem.Text = '') then
              Params.ParamByName ('@pe_num_secao').Value := Null
            else
              Params.ParamByName ('@pe_num_secao').AsString := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').AsString;

            Params.ParamByName ('@pe_flg_status').Value := Null;
            Open;

            if (RecordCount > 0) then
            begin
              edNumSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
              //edNumSecaoOrigem.Text := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
            end
            else
            begin
              edNumSecaoOrigem.Text := '';
            end;
          end;
        end
        else
        begin
          ShowMessage('Aten��o!!! Essa crian�a/adolescente encontra-se' +#13+#10+#13+#10 +
               'em situa��o ' + StatusCadastro(txtMatricula.Text)+ '. Favor regularizar...');
          //AtualizaCamposBotoes ('Padrao');
        end;
      end;
    except end;
  end;
end;

procedure TfrmNovaTransferencia.FormShow(Sender: TObject);
begin
  AtualizaTela('Padrao');
end;

procedure TfrmNovaTransferencia.cbUnidadeDestinoClick(Sender: TObject);
begin
  edCcustoDestino.Clear;
  edNumSecaoDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').AsString := Copy(cbUnidadeDestino.Items[cbUnidadeDestino.ItemIndex],6,100);
      Open;

      edCcustoDestino.Text := dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString;

    end;
  except end;
end;

procedure TfrmNovaTransferencia.cbSecaoDestinoClick(Sender: TObject);
begin
try
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_secao').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := vvINDEX_UNIDADE;
    Params.ParamByName ('@pe_num_secao').Value := Null;
    Params.ParamByName ('@pe_flg_status').Value := 0;  //Somente os Ativos
    Open;

    //if (RecordCount > 0) then
    edNumSecaoDestino.Text := Copy(cbSecaoDestino.Text,1,4 )  //dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value
    //else
    //edNumSecaoDestino.Text := '';

  end;
except end;
end;

procedure TfrmNovaTransferencia.cbUnidadeDestinoExit(Sender: TObject);
begin
with DmDeca.cdsSel_Unidade do
  begin
    vvINDEX_UNIDADE := 0;
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= Copy(cbUnidadeDestino.Items[cbUnidadeDestino.ItemIndex],6,100);
    Open;
    vvINDEX_UNIDADE := FieldByName('cod_unidade').Value;

    //Carrega a lista de Se��es da Unidade selecionada
    cbSecaoDestino.Items.Clear;
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := 0;
      Open;

      if (RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_Secao.Eof do
        begin
          cbSecaoDestino.Items.Add (dmDeca.cdsSel_Secao.FieldByName ('num_secao').AsString + '-' + dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').AsString);
          Next;
        end;
      end;
    end;
  end;
end;

procedure TfrmNovaTransferencia.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela('Padrao');
end;

procedure TfrmNovaTransferencia.btnSalvarClick(Sender: TObject);
var num_cotas: Real;
    strOcorrencia : String;
begin

vvPODE_IMPRIMIR := True;

//Antes de gravar, validar os campos obrigat�rios do formul�rio
// data de transfer�ncia, Unidade Destino, Secao Destino, Entrega passe ou n�o, cotas de passe

if (cbMotivos.ItemIndex >= 0) and
   (Length(meDataTransferencia.Text) > 0) and (cbUnidadeDestino.ItemIndex >= 0) and
   (cbSecaoDestino.ItemIndex >= 0) and (cbEntregaPasse.ItemIndex >= 0) and
   (cbCotas.ItemIndex >= 0) and (cbPeriodo.ItemIndex >= 0) then
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try
      // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
      if StatusCadastro(txtMatricula.Text) = 'Ativo' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value          := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_dat_historico').Value          := GetValue(meDataTransferencia, vtDate); //StrToDate(txtDataTransferencia.Text);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 6;
          Params.ParamByName('@pe_dsc_tipo_historico').AsString  := 'Transfer�ncia p/ Unidade - Encaminhamento';
          Params.ParamByName('@pe_cod_unidade').AsInteger        := vvCOD_UNIDADE;
          Params.ParamByName('@pe_ind_motivo1').AsString         := IntToStr(cbTipoTransferencia.ItemIndex);
          Params.ParamByName('@pe_ind_motivo2').AsString         := IntToStr(cbMotivos.ItemIndex);

          strOcorrencia := '';
          strOcorrencia := 'Crian�a/Adolescente encaminhado(a) de: ' + Trim(edCcustoOrigem.Text) + '.' + Copy(edNumSecaoOrigem.Text,1,4) + ' para ' +
                            Trim(edCcustoDestino.Text) + '.' + Trim(edNumSecaoDestino.Text);

          Params.ParamByName ('@pe_dsc_ocorrencia').Value := GetValue(strOcorrencia);
          Params.ParamByName ('@pe_orig_unidade').Value   := GetValue(edUnidadeOrigem.Text);
          Params.ParamByName ('@pe_orig_ccusto').Value    := GetValue(edCcustoOrigem.Text);
          Params.ParamByName ('@pe_orig_secao').Value     := Copy(edNumSecaoOrigem.Text,6,100);
          Params.ParamByName ('@pe_orig_num_secao').Value := Copy(edNumSecaoOrigem.Text,1,4);
          Params.ParamByName ('@pe_orig_banco').Value     := GetValue(cbBancoOrigem.Text);
          Params.ParamByName ('@pe_dest_unidade').Value   := Copy(cbUnidadeDestino.Text,6,100);
          Params.ParamByName ('@pe_dest_ccusto').Value    := GetValue(edCcustoDestino.Text);
          Params.ParamByName ('@pe_dest_secao').AsString  := cbSecaoDestino.Text;
          Params.ParamByName ('@pe_dest_num_secao').Value := GetValue(edNumSecaoDestino.Text);
          Params.ParamByName ('@pe_dest_banco').Value     := GetValue(cbBancoDestino.Text);
          Params.ParamByName ('@pe_flg_passe').Value      := cbEntregaPasse.ItemIndex;

          num_cotas := 0;
          num_cotas := StrToFloat(cbCotas.Text);
          Params.ParamByName ('@pe_num_cotas_passe').AsFloat := num_cotas;

          Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;

          //Atualizar STATUS para "5" - Transferencia
          with dmDeca.cdsAlt_Cadastro_Status_Transf do
          begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName('@pe_cod_unidade').Value   := vvINDEX_UNIDADE;
              Params.ParamByName('@pe_num_secao').Value     := edNumSecaoDestino.Text;

              //if (TRIM(txtLocalOrigem.Text)=cbUnidades.Text) then
              if Trim(edUnidadeOrigem.Text) = cbUnidadeDestino.Text then  //Transferencia entre secoes da mesma unidade
                Params.ParamByName('@pe_ind_status').Value := 1
              else
                Params.ParamByName('@pe_ind_status').Value := 5;
              Execute;
          end;

          //Faz a altera��o do endere�o e da cota de passe....
          with dmDeca.cdsAlt_EnderecoCadastro do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value     := GetValue(txtMatricula.Text);
            Params.ParamByName ('@pe_nom_endereco').Value      := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_endereco').Value;
            Params.ParamByName ('@pe_nom_complemento').Value   := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_complemento').Value;
            Params.ParamByName ('@pe_nom_bairro').Value        := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_bairro').Value;
            Params.ParamByName ('@pe_num_cep').Value           := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_cep').Value;
            Params.ParamByName ('@pe_num_cota_passes').AsFloat := StrToFloat(cbCotas.Text);//dmDeca.cdsSel_Cadastro_Move.FieldByName('num_cota_passes').Value;
            Execute;
          end;

        end;

        //Atualizar o Numero da secao no cadastro
        with dmDeca.cdsAlt_Secao_Cadastro do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_num_secao').Value     := GetValue(edNumSecaoDestino.Text);
          Execute;
        end;

        Application.MessageBox ('TRANSFER�NCIA EFETUADA COM SUCESSO !!!' + #13#10 +
                                'Para visualiz�-la clique no bot�o Imprimir...' + #13+#10 +
                                'Para emiss�o de segunda via, v� em Movimenta��es e clique na Op��o Hist�rico...',
                                '[Sistema Deca] - Sucesso na Transfer�ncia',
                                MB_OK + MB_ICONINFORMATION) ;

        btnNovo.Enabled := True;
        btnSalvar.Enabled := False;
        btnCancelar.Enabled := False;

        //if vvPODE_IMPRIMIR = True then
          btnImprimir.Enabled := True;
        //else
        //begin
        //  btnImprimir.Enabled := False;
        //  AtualizaTela('Padrao');
        //end;

        btnSair.Enabled := True;
        //AtualizaCamposBotoes('Padrao');

        //Trava os GroupBox para evitar altera��es de dados
        GroupBox4.Enabled := False;
        GroupBox2.Enabled := False;

        frmPrincipal.AtualizaStatusBarUnidade;

      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
                    'Esta(e) crian�a/adolescente encontra-se ' +
                    StatusCadastro(txtMatricula.Text));
        AtualizaTela('Padrao');
      end;
    except

      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
      arqLog := 'TRFe_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                          Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
      AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
      Rewrite (log_file);

      Write   (log_file, txtMatricula.Text);        //Grava a matricula
      Write   (log_file, ' ');
      Write   (log_file, lbNome.Caption);
      Write   (log_file, ' ');
      Write   (log_file, meDataTransferencia.Text);             //Data do registro
      Write   (log_file, ' ');
      Write   (log_file, 'Transfer�ncia de Unidade - Encaminhamento');//Descricao do Tipo de Historico
      Writeln (log_file, '--------------------------------------------------------------------------------------');

      CloseFile (log_file);

      Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                             'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                             'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                             'Sistema DECA - Altera��o de Registro',
                             MB_OK + MB_ICONERROR);



      //ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10+#13+#10 +
      //            'Favor entrar em contato com o Administrador do Sistema');
      AtualizaTela('Padrao');
    end;

  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
                'Favor verificar se os campos obrigat�rios est�o preenchidos...');
  end;
end
else
begin
  ShowMessage ('Alguns campos obrigat�rios n�o foram preenchidos... Verifique e tente novamente...');
end;
end;

procedure TfrmNovaTransferencia.btnNovoClick(Sender: TObject);
begin
  AtualizaTela('Novo');
  meDataTransferencia.Text := DateToStr(Date());
end;

procedure TfrmNovaTransferencia.btnImprimirClick(Sender: TObject);
begin
  //Confirmar a emissao da transfer�ncia
  Application.CreateForm(TrelNovaTransferencia, relNovaTransferencia);

  //Transfere os dados para o relat�rio
  relNovaTransferencia.lbMatricula.Caption := Trim(txtMatricula.Text);
  relNovaTransferencia.lbMatricula1.Caption := Trim(txtMatricula.Text);
  relNovaTransferencia.lbNome.Caption := lbNome.Caption;
  relNovaTransferencia.lbNome1.Caption := lbNome.Caption;
  relNovaTransferencia.lbEndereco.Caption := Trim(edEndereco.Text);
  relNovaTransferencia.lbEndereco1.Caption := Trim(edEndereco.Text);
  relNovaTransferencia.lbCep.Caption := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cep').AsString);
  relNovaTransferencia.lbCep1.Caption := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cep').AsString);
  relNovaTransferencia.lbBairro.Caption := Trim(edBairro.Text);
  relNovaTransferencia.lbBairro1.Caption := Trim(edBairro.Text);
  relNovaTransferencia.lbTipo.Caption := cbTipoTransferencia.Text;
  relNovaTransferencia.lbTipo1.Caption := cbTipoTransferencia.Text;
  relNovaTransferencia.lbMotivo.Caption := cbMotivos.Text;
  relNovaTransferencia.lbMotivo1.Caption := cbMotivos.Text;
  relNovaTransferencia.lbUnidadeOrigem.Caption := edUnidadeOrigem.Text + ' / '+ Copy(edNumSecaoOrigem.Text,6,50);
  relNovaTransferencia.lbUnidadeOrigem1.Caption := edUnidadeOrigem.Text + ' / ' + Copy(edNumSecaoOrigem.Text,6,50);;
  relNovaTransferencia.lbCcustoSecaoOrigem.Caption := Trim(edCcustoOrigem.Text) + '.' + Copy(edNumSecaoOrigem.Text,1,4);
  relNovaTransferencia.lbCcustoSecaoOrigem1.Caption := Trim(edCcustoOrigem.Text) + '.' + Copy(edNumSecaoOrigem.Text,1,4);
  relNovaTransferencia.lbBancoOrigem.Caption := cbBancoOrigem.Text;
  relNovaTransferencia.lbBancoOrigem1.Caption := cbBancoOrigem.Text;
  relNovaTransferencia.lbDataTransferencia.Caption := Trim(meDataTransferencia.Text);
  relNovaTransferencia.lbDataTransferencia1.Caption := Trim(meDataTransferencia.Text);

  relNovaTransferencia.lbUnidadeDestino.Caption := cbUnidadeDestino.Text + ' / ' + Copy(cbSecaoDestino.Text,6,50);
  relNovaTransferencia.lbUnidadeDestino1.Caption := cbUnidadeDestino.Text + ' / ' + Copy(cbSecaoDestino.Text,6,50);
  relNovaTransferencia.lbCcustoSecaoDestino.Caption := Trim(edCcustoDestino.Text) + '.' + edNumSecaoDestino.Text;
  relNovaTransferencia.lbCcustoSecaoDestino1.Caption := Trim(edCcustoDestino.Text) + '.' + edNumSecaoDestino.Text;
  relNovaTransferencia.lbBancoDestino.Caption := cbBancoDestino.Text;
  relNovaTransferencia.lbBancoDestino1.Caption := cbBancoDestino.Text;

  relNovaTransferencia.lbEntregaPasse.Caption := cbEntregaPasse.Text;
  relNovaTransferencia.lbEntregaPasse1.Caption := cbEntregaPasse.Text;
  relNovaTransferencia.lbPeriodo.Caption := cbPeriodo.Text;
  relNovaTransferencia.lbPeriodo1.Caption := cbPeriodo.Text;
  relNovaTransferencia.lbCotas.Caption := cbCotas.Text + ' COTA(S) DE PASSE.';
  relNovaTransferencia.lbCotas1.Caption := cbCotas.Text + ' COTA(S) DE PASSE.';
  relNovaTransferencia.lbIdade1.Caption := lbIdade.Caption;
  relNovaTransferencia.lbIdade2.Caption := lbIdade.Caption;


  relNovaTransferencia.Preview;
  relNovaTransferencia.Free;

  GroupBox4.Enabled := False;
  GroupBox2.Enabled := False;

  AtualizaTela('Padrao');

end;

procedure TfrmNovaTransferencia.btnSairClick(Sender: TObject);
begin
  frmNovaTransferencia.Close;
end;

procedure TfrmNovaTransferencia.VerificaSituacaoBimestre(
  cdMatricula: String);
var
  mmULT_BIMESTRE : Integer;
begin

  vvPODE_TRANSFERIR := False;

  //Verifica qual o �ltimo bimestre que foi lan�ado e se o mesmo est� SEM ESCOLA lan�ada ou n�o
  try
    with dmDeca.cdsSel_VerificaUltimoBimestreAnoAluno do
    begin
      Close;
      Params.ParamByName ('@pe_num_ano_letivo').Value := Copy(DateToStr(Date()),7,4);
      Params.ParamByName ('@pe_cod_matricula').Value  := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_matricula').Value;
      Open;

      mmULT_BIMESTRE := dmDeca.cdsSel_VerificaUltimoBimestreAnoAluno.FieldByName ('UltBimestre').Value;

    end;
  except end;


  try
    //Verifica se h� algum bimestre com situa��o "7" ou "8", "Sem Escola" e "Sem Informa��o de S�rie"
    with dmDeca.cds_Sel_Verifica_AguardandoInformacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value   := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_matricula').Value;
      Params.ParamByName ('@pe_num_ano_letivo').Value  := Copy(DateToStr(Date()),7,4);
      Params.ParamByName ('@pe_num_bimestre').Value    := mmULT_BIMESTRE;
      Params.ParamByName ('@pe_ind_situacao1').Value   := 7; //Sem Informa��o...
      Params.ParamByName ('@pe_ind_situacao2').Value   := 8; //Aguardando Informa��o...
      Params.ParamByName ('@pe_cod_escola').Value      := 209;
      Params.ParamByName ('@pe_cod_id_serie').Value    := 44;
      Open;

      //Se achou pelo menos 1 registro "Aguardando Informacao" ou "Sem Escola", n�o permite transferir...
      if (dmDeca.cds_Sel_Verifica_AguardandoInformacoes.RecordCount > 0) then
      begin
        Application.MessageBox ('Aten��o !!!' +#13+#10 +
                                'Esta opera��o n�o ser� executada.' + #13+#10 +
                                'Consulte a Equipe do Acompanhamento Escolar para outras d�vidas.',
                                '[Sistema Deca] - Erro Transfer�ncia...',
                                MB_OK + MB_ICONWARNING);
         AtualizaTela('Padrao');
        vvPODE_TRANSFERIR := False;
      end
      else
      if (dmDeca.cds_Sel_Verifica_AguardandoInformacoes.RecordCount < 1)  then
      begin
        vvPODE_TRANSFERIR := True;
      end;

    end;

  except end;

end;


procedure TfrmNovaTransferencia.cbUnidadeDestinoEnter(Sender: TObject);
begin
  {
   ORIGEM TRANSFER�NCIA: UNIDADES DR2 (CENTRO DE CUSTO 42...)
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DR2
  }
  if (Copy(mmCCUSTO_ORIGEM,1,2) = '42') and (cbMotivos.ItemIndex = 0) then //and (cbTipoTransferencia.ItemIndex = 0) then  --> OUTRA UNIDADE
  begin
    CarregaDR1_DR2;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: UNIDADE DR2 (CENTRO DE CUSTO 42...)
   MOTIVO: MUDAN�A DE V�NCULO
   DESTINO: UNIDADES DR1
  }
  else if (Copy(mmCCUSTO_ORIGEM,1,2) = '42') and (cbMotivos.ItemIndex = 1) then //and (cbTipoTransferencia.ItemIndex = 1) then --> MUDAN�A DE V�NCULO
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: UNIDADE DR1
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DR1
  }
  else if (Copy(mmCCUSTO_ORIGEM,1,2) = '32') and (cbMotivos.ItemIndex = 0) then //and (cbTipoTransferencia.ItemIndex = 2) then --> OUTRA UNIDADE
  begin
    CarregaDR1_DR2;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: UNIDADE DDC (CENTRO DE CUSTO 31...)
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DE
  }
  else if (Copy(mmCCUSTO_ORIGEM,1,2) = '32') and (cbMotivos.ItemIndex = 1) then //and (cbTipoTransferencia.ItemIndex = 3) then   --> MUDAN�A DE V�NCULO
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: BOLSISTAS DE
   MOTIVO: OUTRA UNIDADE
   DESTINO: BOLSISTAS DE
  }
  else if (
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5107') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5109') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5112') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5113') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5114') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5117') )  and
                (cbMotivos.ItemIndex = 0) then
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: BOLSISTAS DE
   MOTIVO: MUDAN�A DE V�NCULO
   DESTINO: UNIDADES DE - CELETISTAS
  }
  else if (
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5107') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5109') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5112') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5113') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5114') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5117') ) and
                (cbMotivos.ItemIndex = 1) then
  begin
    CarregaAprendizesDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: APRENDIZES CELETISTAS
   MOTIVO: OUTRA UNIDADE
   DESTINO: UNIDADES DE - CELETISTAS
  }

  else if (
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5102') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5103') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5104') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5105') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5106') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5117') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5108') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5110') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5111') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5115') or (Copy(mmCCUSTO_ORIGEM,1,4) = '5117')) and
                (cbMotivos.ItemIndex = 0) then
  begin
    CarregaAprendizesDE;
    vvPODE_IMPRIMIR := True;
  end
  {
   ORIGEM TRANSFER�NCIA: APRENDIZES CELETISTAS
   MOTIVO: MUDAN�A DE V�NCULO
   DESTINO: UNIDADES DE - CELETISTAS
  }

  else if (
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5102') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5103') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5104') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5105') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5106') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5117') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5108') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5110') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5111') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5115') or
                (Copy(mmCCUSTO_ORIGEM,1,4) = '5117')) and
                (cbMotivos.ItemIndex = 1) then
  begin
    CarregaBolsistasDE;
    vvPODE_IMPRIMIR := True;
  end

end;

procedure TfrmNovaTransferencia.CarregaUnidades4070;
begin
  {cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '4') or
           //(Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '7') then
           (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '3') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;}
end;

procedure TfrmNovaTransferencia.cbMotivosExit(Sender: TObject);
begin
  if cbMotivos.ItemIndex < 0 then
  begin
    Application.MessageBox ('� necess�rio que voc� selecione o MOTIVO DA TRANSFER�NCIA...',
                            '[Sistema Deca] - Campo MOTIVO deve ser preenchido',
                            MB_OK + MB_ICONERROR);
    cbMotivos.SetFocus;
  end;
end;

procedure TfrmNovaTransferencia.CarregaUnidades70;
begin
{
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        //if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '7') then
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '3') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end; }
end;

procedure TfrmNovaTransferencia.CarregaUnidades30;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        //if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '3') then
        //  if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '3004') or (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '3006') then
          if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '5') then
          //else
            cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;}
end;

procedure TfrmNovaTransferencia.CarregaUnidades30063004;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '3004') or (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '3006') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;}
end;

procedure TfrmNovaTransferencia.CarregaTODAS;
begin

  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '3') or
           (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '4') or
           (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '5') then
        begin
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
  
end;

procedure TfrmNovaTransferencia.CarregaUnidades41;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '4') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
  }
end;

procedure TfrmNovaTransferencia.CarregaUnidades31;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '3') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
  }
end;

procedure TfrmNovaTransferencia.CarregaUnidades51;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '5') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
  }
end;

procedure TfrmNovaTransferencia.cbMotivosClick(Sender: TObject);
begin
  {
  //Se CCusto 41.. e Motivo = outra unidade tipo=DAB/DAB
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '4') and (cbMotivos.ItemIndex = 0) then
    cbTipoTransferencia.ItemIndex := 0
  else
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '4') and (cbMotivos.ItemIndex = 1) then
    cbTipoTransferencia.ItemIndex := 1
  else
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '3') and (cbMotivos.ItemIndex = 0) then
    cbTipoTransferencia.ItemIndex := 2
  else
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '3') and (cbMotivos.ItemIndex = 1) then
    cbTipoTransferencia.ItemIndex := 3
  else
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '5') and (cbMotivos.ItemIndex = 0) then
    cbTipoTransferencia.ItemIndex := 4
  else
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '5') and (cbMotivos.ItemIndex = 1) then
    cbTipoTransferencia.ItemIndex := 5
  else
  if (Copy(mmCCUSTO_ORIGEM,1,1) = '6') then
  begin
    cbMotivos.ItemIndex := 0;
    cbTipoTransferencia.ItemIndex := 1;
  end;
  }
end;

procedure TfrmNovaTransferencia.CarregaUnidades31_5107_5109;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,1) = '3') or
           ((Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,4) = '5107') or
            (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,4) = '5109') or
            (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,4) = '5112')) then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
  }
end;

procedure TfrmNovaTransferencia.Carrega5102;
begin
  {
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,4) = '5102') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
  }
end;

procedure TfrmNovaTransferencia.CarregaBolsistasDE;
begin
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5107') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5109') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5112') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5113') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5114') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5117') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

procedure TfrmNovaTransferencia.CarregaAprendizesDE;
begin
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5102') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5103') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5104') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5105') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5106') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5108') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5110') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5111') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) = '5115') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

function TfrmNovaTransferencia.CalculaIdadeTransf(
  Nascimento: TDateTime): String;
var
  Periodo : Integer;
  intContAnos, intContMeses : Integer ;
  DataFinal : TDateTime;

begin

    // calculo de anos
    intContAnos := 0 ;
    Periodo := 12 ;
    DataFinal := StrToDate(frmNovaTransferencia.meDataTransferencia.Text);
    Repeat
      Inc(intContAnos) ;
      DataFinal := IncMonth(DataFinal,Periodo * -1) ;
    Until DataFinal < Nascimento ;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContAnos,-1) ;

    // calculo de meses
    intContMeses := 0 ;
    Periodo := 1 ;
    DataFinal := StrToDate(frmNovaTransferencia.meDataTransferencia.Text);
    Repeat
      Inc(intContMeses) ;
      DataFinal := IncMonth(DataFinal,Periodo * -1) ;
    Until DataFinal < Nascimento;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContMeses,-1) ;
    intContMeses := intContMeses mod 12;

    if intContAnos <= 9 then
      Result := '0' + IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses'
    else
      Result := IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses';

end;

procedure TfrmNovaTransferencia.CarregaDR1_DR2;
begin

  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;
      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,2) = '42') or (Copy(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString),1,2) = '32') then
          cbUnidadeDestino.Items.Add(Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').AsString) + '-' + dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').AsString);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;

end;

end.
