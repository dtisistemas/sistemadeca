unit uInicializarBimestre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, Grids, DBGrids, ExtCtrls, Mask, Db;

type
  TfrmInicializarBimestre = class(TForm)
    rgCriterio: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Panel3: TPanel;
    Label2: TLabel;
    edAno: TEdit;
    cbBimestre: TComboBox;
    Panel1: TPanel;
    dgAcompanhamento: TDBGrid;
    ProgressBar1: TProgressBar;
    Panel2: TPanel;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnGravar: TBitBtn;
    btnNovo: TBitBtn;
    Label3: TLabel;
    dsAcompanhamento: TDataSource;
    GroupBox2: TGroupBox;
    cbUnidade: TComboBox;
    btnImprimir: TBitBtn;
    procedure AtualizaCamposBotoes (Modo: String);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure rgCriterioClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInicializarBimestre: TfrmInicializarBimestre;
  vListaUnid1, vListaUnid2 : TStringList;

implementation

uses uDM, uPrincipal, uFichaPesquisa, uEmiteAcompanhamentoEscolar, uUtil;

{$R *.DFM}

{ TfrmInicializarBimestre }

procedure TfrmInicializarBimestre.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    rgCriterio.ItemIndex := -1;
    rgCriterio.Enabled := False;
    cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := False;
    txtMatricula.Clear;
    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    lbNome.Caption := '';
    edAno.Clear;
    edAno.Enabled := False;
    cbBimestre.ItemIndex := -1;
    cbBimestre.Enabled := False;

    btnNovo.Enabled := True;
    btnGravar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;
    btnSair.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    rgCriterio.Enabled := True;
    cbUnidade.Enabled := True;
    edAno.Enabled := True;
    cbBimestre.Enabled := True;

    btnNovo.Enabled := False;
    btnGravar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmInicializarBimestre.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmInicializarBimestre.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');

  vListaUnid1 := TStringList.Create;
  vListaUnid2 := TStringList.Create;

  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin
        cbUnidade.Items.Add (FieldByname ('nom_unidade').AsString);
        vListaUnid1.Add (IntToStr(FieldByName ('cod_unidade').AsInteger));
        vListaUnid2.Add (FieldByname ('nom_unidade').AsString);
        Next;
      end;
    end;

    //Atualiza os dados de Acompanhamento Escolar no grid
    (*with dmDeca.cdsSel_Acompanhamento do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_num_bimestre').Value := Null;
      Params.ParamByName('@pe_num_ano').Value := Null;
      Params.ParamByName('@pe_flg_aproveitamento').Value := Null;
      Params.ParamByName('@pe_cod_escola').Value := Null;
      Params.ParamByName('@pe_num_secao').Value := Null;
      Open;

      dgAcompanhamento.Refresh;

    end;*)
  except end;


end;

procedure TfrmInicializarBimestre.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Novo');
end;

procedure TfrmInicializarBimestre.rgCriterioClick(Sender: TObject);
begin
  if (rgCriterio.ItemIndex = 0) then  //Para Todos
  begin
    txtMatricula.Clear;
    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    cbUnidade.Enabled := True;
    lbNome.Caption := '';
    edAno.SetFocus;
    edAno.Text := Copy(DateToStr(Date()),7,4)
  end
  else if (rgCriterio.ItemIndex = 1) then  //Por Matr�cula
  begin
    txtMatricula.Clear;
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    cbUnidade.Enabled := False;
    lbNome.Caption := '';
    edAno.SetFocus;
    edAno.Text := Copy(DateToStr(Date()),7,4)
  end;
end;

procedure TfrmInicializarBimestre.btnGravarClick(Sender: TObject);
begin
  case rgCriterio.ItemIndex of
  0:begin
    if Application.MessageBox('Deseja inicializar Bimestre para TODOS do cadastro?' + #13+#10 +
           'Todos os lan�amentos existentes ser�o regravados. Confirma?',
           'Acomp. Escolar - Lan�amento',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      //Valida os campos ANO, Bimestre e Unidade
      if (Length(edAno.Text) > 0) and (cbBimestre.ItemIndex > -1) and (cbUnidade.ItemIndex > -1) then
      begin

        //Excluir todos os dados de acompanhamento escolar referente ao bimestre/ano informado para TODOS
        //TODOS de determinada Unidade - cbUnidade
        //with dmDeca.cdsExc_Acompanhamento do
        //begin
        //  Close;
        //  Params.ParamByName ('@pe_cod_id_acompanhamento').Value := Null;
        //  Params.ParamByName ('@pe_cod_matricula').Value := Null;
        //  Params.ParamByName ('@pe_num_bimestre').Value := cbBimestre.ItemIndex;
        //  Params.ParamByName ('@pe_num_ano').Value := StrToInt(edAno.Text);
        //  Execute;
        //end;

        //Incluir presen�a para todos no cadastro somente para o m�s selecionado
        //Enquanto n�o fim do cadastro - definir como selecionar todos da Unidade selecionada
        with dmDeca.cdsSel_Cadastro do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vListaUnid1.Strings[cbUnidade.ItemIndex]);
          Open;

          ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

          if (RecordCount > 0) then
          begin

            ProgressBar1.Position := 0;

            while not eof do
            begin

              //Verifica se j� existe lan�amento para a crian�a
              //para bimestre/ano e "escola"

              with dmDeca.cdsSel_Acompanhamento do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;
                Params.ParamByName ('@pe_cod_unidade').Value := vListaUnid1.Strings[cbUnidade.ItemIndex];
                Params.ParamByName ('@pe_num_bimestre').AsInteger := cbBimestre.ItemIndex+1;  //Modificado "+1"
                Params.ParamByName ('@pe_num_ano').AsInteger := STrToInt(edAno.Text);
                Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
                Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_escola').AsInteger;
                Params.ParamByName ('@pe_nom_nome').Value := Null;
                Open;

                if ((dmDeca.cdsSel_Acompanhamento.RecordCount) < 1) then
                begin
                  //Incluir dados de Acompanhamento Escolar
                  try
                  with dmDeca.cdsInc_Acompanhamento do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;
                    Params.ParamByName ('@pe_num_dias_letivos').Value := Null;
                    Params.ParamByName ('@pe_num_faltas').Value := Null;
                    Params.ParamByName ('@pe_num_bimestre').AsInteger := cbBimestre.ItemIndex + 1;
                    Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                    Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
                    Params.ParamByName ('@pe_dsc_observacoes').Value := Null;
                    Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
                    Params.ParamByName ('@pe_cod_escola').AsInteger := dmDeca.cdsSel_Cadastro.FieldByName ('cod_escola').AsInteger; //Recuperar o cod_escola do cadastro...
                    Params.ParamByName ('@pe_dsc_escola_serie').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_escola_serie').Value;
                    Execute;
                  end;

                  except
                    Application.MessageBox('Aten��o Ocorreu um ERRO ao tentar inserir dados de' + #13+#10 +
                                           'Acompanhamento Escolar. Contate o Administrador do Sistema...',
                                           'Acompanhamento Escolar',
                                            MB_ICONQUESTION + MB_YESNO);
                  end;  //Final Except
              end;  //Final cdsSel_Acompanhamento
              dmDeca.cdsSel_Cadastro.Next;
              ProgressBar1.Position := ProgressBar1.Position + 1;
            end;  //Final Eof

            with dmDeca.cdsSel_Acompanhamento do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vListaUnid1.Strings[cbUnidade.ItemIndex];
              Params.ParamByName ('@pe_num_bimestre').Value := Null;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
              Params.ParamByName ('@pe_cod_escola').Value := Null;
              Params.ParamByName ('@pe_nom_nome').Value := Null;
              Open;
              dgAcompanhamento.Refresh;
            end;
          end;  //Final if RecordCount > 0
          end
          else
           if (RecordCount < 1) then
            begin
              ShowMessage ('Nenhuma c�a/adolescente cadastrado para essa unidade...' + vListaUnid1.Strings[cbUnidade.ItemIndex]);
              AtualizaCamposBotoes ('Padrao');
            end;
        end //Sel_Cadastro
      end
      else
      begin
        ShowMessage ('Aten��o !!! Dados incompletos para lan�amento de dados. Verifique...');
        edAno.Setfocus;
      end;
    end;
  end;  //case 0

  1: begin

    //Valida os campos Matricula, BImestre e Ano
    if (Length(txtMatricula.Text) > 0) AND (Length(edAno.Text) > 0) AND (cbBimestre.ItemIndex <> -1) then
    begin
      if Application.MessageBox('Deseja inicializar Bimestre para a Matr�cula selecionada?' + #13+#10 +
           'Todos os lan�amentos existentes ser�o regravados. Confirma?',
           'Frequ�ncia - Lan�amento',
           MB_ICONQUESTION + MB_YESNO) = idYes then
      begin
        //Exclui todos os lan�amentos do Bimestre/Ano para a matr�cula selecionada
        //try
          //with dmDeca.cdsExc_Acompanhamento do
          //begin
          //  Close;
          //  Params.ParamByname ('@pe_cod_id_acompanhamento').Value := Null;
          //  Params.ParamByname ('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
          //  Params.ParamByname ('@pe_num_bimestre').Value := cbBimestre.ItemIndex + 1;
          //  Params.ParamByname ('@pe_num_ano').Value := StrToInt(Trim(edAno.Text));
          //  Execute;
          //end;
        //except end;

        try

          with dmDeca.cdsSel_Acompanhamento do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').AsInteger;
            Params.ParamByName ('@pe_cod_unidade').Value := Null;
            Params.ParamByName ('@pe_num_bimestre').Value := cbBimestre.ItemIndex;
            Params.ParamByName ('@pe_num_ano').Value := STrToInt(edAno.Text);
            Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
            Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_escola').AsInteger;
            Params.ParamByName ('@pe_nom_nome').Value := Null;
            Open;

            if ((dmDeca.cdsSel_Acompanhamento.RecordCount) < 1) then
            begin
              with dmDeca.cdsInc_Acompanhamento do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').AsString := Trim(txtMatricula.Text);
                Params.ParamByName ('@pe_num_dias_letivos').Value := Null;
                Params.ParamByName ('@pe_num_faltas').Value := Null;
                Params.ParamByName ('@pe_num_bimestre').Value := cbBimestre.ItemIndex + 1;
                Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
                Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
                Params.ParamByName ('@pe_dsc_observacoes').Value := Null;
                Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_escola').AsInteger;
                Params.ParamByName ('@pe_dsc_escola_serie').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dsc_escola_serie').Value;
                Execute;
              end;

              //Atualiza o Grid
              with dmDeca.cdsSel_Acompanhamento do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                Params.ParamByName ('@pe_cod_unidade').Value := Null;
                Params.ParamByName ('@pe_num_bimestre').Value := Null;
                Params.ParamByName ('@pe_num_ano').Value := Null;
                Params.ParamByName ('@pe_flg_aproveitamento').Value := Null;
                Params.ParamByName ('@pe_cod_escola').Value := Null;
                //Params.ParamByName ('@pe_num_secao').Value := Null;
                Open;
              end;

              dgAcompanhamento.Refresh;

            end;
          end
        except end;

        //AtualizaCamposBotoes ('Padrao');

      end;
    end
    else
    begin
      ShowMessage ('Aten��o !!! Dados incompletos para lan�amento de dados. Verifique...');
      edAno.Setfocus;
    end;

    end;
  end; //case

  AtualizaCamposBotoes ('Padrao');

end;

procedure TfrmInicializarBimestre.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
end;

procedure TfrmInicializarBimestre.btnSairClick(Sender: TObject);
begin
  frmInicializarBimestre.Close;
  vListaUnid1.Free;
  vListaUnid2.Free;
end;

procedure TfrmInicializarBimestre.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmInicializarBimestre.btnImprimirClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
  Application.CreateForm(TfrmEmiteAcompanhamentoEscolar, frmEmiteAcompanhamentoEscolar);
  frmEmiteAcompanhamentoEscolar.ShowModal;
end;

end.
