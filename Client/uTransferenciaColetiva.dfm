object frmTransferenciaColetiva: TfrmTransferenciaColetiva
  Left = 512
  Top = 246
  Width = 903
  Height = 645
  Caption = '[Sistema Deca] - M�dulo de Transfer�ncia Coletiva'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 6
    Top = 110
    Width = 464
    Height = 43
    Caption = '[Unidade]'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object cbUNIDADE: TComboBox
      Left = 8
      Top = 15
      Width = 451
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 0
      OnClick = cbUNIDADEClick
    end
  end
  object lstALUNOS: TCheckListBox
    Left = 5
    Top = 200
    Width = 464
    Height = 376
    ItemHeight = 13
    TabOrder = 1
  end
  object Button2: TButton
    Left = 99
    Top = 579
    Width = 132
    Height = 25
    Caption = 'Marcar Todos'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 243
    Top = 579
    Width = 132
    Height = 25
    Caption = 'Desmarcar Todos'
    TabOrder = 3
    OnClick = Button3Click
  end
  object GroupBox3: TGroupBox
    Left = 472
    Top = 110
    Width = 422
    Height = 498
    Caption = '[Destino]'
    TabOrder = 4
    object GroupBox4: TGroupBox
      Left = 8
      Top = 18
      Width = 137
      Height = 50
      Caption = '[Data Transfer�ncia]'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object mskDATATRANSF: TMaskEdit
        Left = 8
        Top = 16
        Width = 121
        Height = 24
        Color = clRed
        EditMask = '99/99/9999'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
      end
    end
    object GroupBox5: TGroupBox
      Left = 147
      Top = 18
      Width = 261
      Height = 50
      Caption = '[Motivo da Transfer�ncia]'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object cbMOTIVO: TComboBox
        Left = 8
        Top = 15
        Width = 245
        Height = 24
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        Items.Strings = (
          'OUTRA UNIDADE'
          'MUDAN�A DE V�CULO')
      end
    end
    object GroupBox6: TGroupBox
      Left = 8
      Top = 69
      Width = 401
      Height = 50
      Caption = '[Unidade de Destino]'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      object cbUnidadeDestino: TComboBox
        Left = 8
        Top = 15
        Width = 385
        Height = 24
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        OnClick = cbUnidadeDestinoClick
        OnExit = cbUnidadeDestinoExit
      end
    end
    object GroupBox7: TGroupBox
      Left = 8
      Top = 170
      Width = 279
      Height = 50
      Caption = '[Banco]'
      TabOrder = 3
      object cbBanco: TComboBox
        Left = 8
        Top = 15
        Width = 263
        Height = 24
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        Items.Strings = (
          'BANCO SANTANDER'
          'BANCO BRADESCO'
          'BANCO CAIXA ECON�MICA FEDERAL'
          'BANCO DO BRASIL')
      end
    end
    object GroupBox8: TGroupBox
      Left = 290
      Top = 170
      Width = 120
      Height = 50
      Caption = '[Cotas de Passe]'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      object cbCotas: TComboBox
        Left = 11
        Top = 15
        Width = 78
        Height = 24
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        Items.Strings = (
          '0,0'
          '0,5'
          '1,0'
          '1,5'
          '2,0'
          '2,5'
          '3,0'
          '3,5'
          '4,0')
      end
    end
    object rgPeriodo: TRadioGroup
      Left = 8
      Top = 436
      Width = 402
      Height = 52
      Caption = '[Per�odo a ser transferido]'
      Columns = 4
      Items.Strings = (
        'MANH�'
        'TARDE'
        'INTEGRAL'
        'NOITE')
      TabOrder = 5
      Visible = False
    end
    object btnCONFIRMATRANSF: TButton
      Left = 104
      Top = 248
      Width = 225
      Height = 49
      Caption = 'Confirmar Transfer�ncias'
      TabOrder = 6
      OnClick = btnCONFIRMATRANSFClick
    end
    object GroupBox9: TGroupBox
      Left = 8
      Top = 120
      Width = 401
      Height = 50
      Caption = '[Se��o de Destino]'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      object cbSecaoDestino: TComboBox
        Left = 8
        Top = 15
        Width = 385
        Height = 24
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        OnExit = cbSecaoDestinoExit
      end
    end
    object Memo1: TMemo
      Left = 40
      Top = 472
      Width = 361
      Height = 145
      Lines.Strings = (
        'Memo1')
      TabOrder = 8
      Visible = False
    end
  end
  object Panel1: TPanel
    Left = 6
    Top = 8
    Width = 889
    Height = 23
    Caption = 'M�DULO DE TRANSFER�NCIA COLETIVA DE CRIAN�AS E ADOLESCENTES'
    TabOrder = 5
  end
  object Panel2: TPanel
    Left = 5
    Top = 32
    Width = 889
    Height = 76
    TabOrder = 6
    object Label1: TLabel
      Left = 10
      Top = 9
      Width = 587
      Height = 14
      Caption = 
        '1.� PASSO: SELECIONE A UNIDADE DE ORIGEM DE MANEIRA QUE OS ALUNO' +
        'S SEJAM VISUALIZADOS NA LISTA ABAIXO.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 25
      Width = 873
      Height = 14
      Caption = 
        '2.� PASSO: SELECIONAR OS ALUNOS QUE IR�O PARA UMA DETERMINADA UN' +
        'IDADE EM COMUM (UNIDADE DE DESTINO) - CLICANDO NA FRENTE DAS RES' +
        'PECTIVAS MATR�CULAS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 10
      Top = 40
      Width = 452
      Height = 14
      Caption = 
        '3.� PASSO: SELECIONE O MOTIVO DA TRANSFER�NCIA, A UNIDADE DE DES' +
        'TINO E A SE��O.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 10
      Top = 56
      Width = 544
      Height = 14
      Caption = 
        '4.� PASSO: SELECIONE O BANCO, AS INFORMA��ES DE COTAS DE PASSE E' +
        ' CONFIRME AS TRANSFER�NCIAS.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 6
    Top = 154
    Width = 464
    Height = 43
    Caption = '[Se��o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    object cbSecaoOrigem: TComboBox
      Left = 8
      Top = 15
      Width = 451
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
