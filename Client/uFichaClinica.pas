unit uFichaClinica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, Grids, DBGrids, ComCtrls, ExtCtrls, ShellApi, Db,
  CheckLst, jpeg;

type
  TfrmFichaClinica = class(TForm)
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    BitBtn1: TBitBtn;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox3: TGroupBox;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    GroupBox6: TGroupBox;
    edUnidade: TEdit;
    GroupBox7: TGroupBox;
    edNascimento: TMaskEdit;
    GroupBox8: TGroupBox;
    PageControl3: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    GroupBox10: TGroupBox;
    btnIncluirEncaminhamento: TSpeedButton;
    dbgEncaminhamentosConsultas: TDBGrid;
    btnExcluirEncaminhamento: TSpeedButton;
    txtData: TMaskEdit;
    txtHora: TMaskEdit;
    Label4: TLabel;
    meAnotacoes: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox9: TGroupBox;
    btnIncluirTratamento: TSpeedButton;
    cbTratamentos: TComboBox;
    panNumeroConsulta: TPanel;
    GroupBox11: TGroupBox;
    Label5: TLabel;
    dataFiltro: TDateTimePicker;
    dbAgenda: TDBGrid;
    GroupBox12: TGroupBox;
    Label6: TLabel;
    lbNome1: TLabel;
    SpeedButton1: TSpeedButton;
    txtMatricula1: TMaskEdit;
    Label10: TLabel;
    txtData1: TMaskEdit;
    Label11: TLabel;
    txtHora1: TMaskEdit;
    btnAgendarConsulta: TSpeedButton;
    dbgTratamentosConsultas: TDBGrid;
    btnExcluirTratamento: TSpeedButton;
    dsAgenda: TDataSource;
    Label12: TLabel;
    edUnidade1: TEdit;
    Label13: TLabel;
    edNascimento1: TMaskEdit;
    btnVerConsultasData: TSpeedButton;
    btnGravarDadosConsulta: TSpeedButton;
    btnEncerrarConsulta: TSpeedButton;
    btnEmitirReceituario: TSpeedButton;
    cbEncaminhamentos: TComboBox;
    dsTratamentosConsultas: TDataSource;
    Panel3: TPanel;
    lbMatricula: TLabel;
    btnREMARCAR: TSpeedButton;
    btnPacienteAusente: TSpeedButton;
    btnImprimirAgenda: TSpeedButton;
    cbTipoDente: TComboBox;
    cbTipoArcada: TComboBox;
    Label7: TLabel;
    edFoneContato: TMaskEdit;
    Label14: TLabel;
    edNomeContato: TEdit;
    Label15: TLabel;
    cbParentesco: TComboBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    cbTipoHemiarcada: TComboBox;
    Label19: TLabel;
    btnVerTodosDentes: TSpeedButton;
    TabSheet3: TTabSheet;
    GroupBox2: TGroupBox;
    Image1: TImage;
    btnEditarImagem: TSpeedButton;
    lstDentes: TCheckListBox;
    chkMarcaTodosLista: TCheckBox;
    dsEncaminhamentosConsultas: TDataSource;
    Panel5: TPanel;
    dbgConsultasPaciente: TDBGrid;
    Panel2: TPanel;
    Label8: TLabel;
    mskMatricula: TMaskEdit;
    SpeedButton6: TSpeedButton;
    lbNomePaciente: TLabel;
    Panel6: TPanel;
    dbgTratamentosConsultasP: TDBGrid;
    Panel7: TPanel;
    dbgEncaminhamentosConsultasP: TDBGrid;
    dsEncaminhamentos: TDataSource;
    Image2: TImage;
    procedure btnPesquisarClick(Sender: TObject);
    procedure ExecFile (F:String);
    procedure btnEditarImagemClick(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabSheet6Show(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnAgendarConsultaClick(Sender: TObject);
    procedure btnVerConsultasDataClick(Sender: TObject);
    procedure dbAgendaDblClick(Sender: TObject);
    procedure btnGravarDadosConsultaClick(Sender: TObject);
    procedure btnEncerrarConsultaClick(Sender: TObject);
    procedure btnIncluirTratamentoClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnIncluirEncaminhamentoClick(Sender: TObject);
    procedure TabSheet7Show(Sender: TObject);
    procedure cbTipoDenteClick(Sender: TObject);
    procedure cbTipoArcadaClick(Sender: TObject);
    procedure cbTipoHemiarcadaClick(Sender: TObject);
    procedure btnVerTodosDentesClick(Sender: TObject);
    procedure chkMarcaTodosListaClick(Sender: TObject);
    procedure btnImprimirAgendaClick(Sender: TObject);
    procedure lstDentesClick(Sender: TObject);
    procedure dbgConsultasPacienteCellClick(Column: TColumn);
    procedure btnPacienteAusenteClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure btnEmitirReceituarioClick(Sender: TObject);
    procedure btnExcluirTratamentoClick(Sender: TObject);
    procedure btnExcluirEncaminhamentoClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnREMARCARClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFichaClinica: TfrmFichaClinica;
  vListaTratamento1, vListaTratamento2 : TStringList;
  vListaEncaminhamento1, vListaEncaminhamento2 : TStringList;
  vData : String;
  vvCOD_CONSULTA_EXCLUI : Integer;
implementation

uses uFichaPesquisa, uDM, uPrincipal, uLoginNovo, uUtil,
  rAgendaDiaOdontologia, uReceituario, uREMARCACAO;

{$R *.DFM}

procedure TfrmFichaClinica.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula1.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;
        edUnidade.Text := FieldByName('vUnidade').AsString;
        edNascimento.Text := DateToSTr(FieldByName('dat_nascimento').AsDateTime);
      end;
    except end;
  end;
end;

procedure TfrmFichaClinica.ExecFile(F: String);
var r : String;
begin
  case ShelLExecute(Handle,nil, PChar(F), nil, nil, SW_SHOWNORMAL) of
    ERROR_FILE_NOT_FOUND: r := 'O arquivo n�o foi encontrado.';
    ERROR_PATH_NOT_FOUND: r := 'O caminho do arquivo n�o foi encontrado.';
    ERROR_BAD_FORMAT: r := 'O aplicativo associado n�o � um arquivo win-32 v�lido ou erro na imagem.';
    SE_ERR_ACCESSDENIED: r := 'Somente Win95. O sistema operacional negou acesso ao arquivo especificado.';
    SE_ERR_ASSOCINCOMPLETE: r := 'Associa��o da extens�o do arquivo inv�lida.';
    SE_ERR_DDEBUSY: r:= 'A opera��o n�o pode ser executada porque outro aplicativo est� utilizando a DDE.';
    SE_ERR_DDEFAIL: r := 'Falha na transa��o DDE.';
    SE_ERR_DDETIMEOUT: r := 'A transa��o DDE n�o pode ser completada porque o tempo de execu��o expirou.';
    SE_ERR_DLLNOTFOUND : r:= 'A DLL n�o foi encontrada.';
    SE_ERR_NOASSOC: r := 'N�o h� aplica��o associada a esse tipo de arquivo.';
    SE_ERR_OOM: r := 'N�o h� mem�ria suficiente para executar essa opera��o.';
    SE_ERR_SHARE: r := 'Ocorreu uma viola��o inesperada.';
  else
    Exit;
  ShowMessage (r);
end;
end;

procedure TfrmFichaClinica.btnEditarImagemClick(Sender: TObject);
begin
  //ExecFile('E:\Deca\Deca v1.0\Imagens\odontograma.bmp');
end;

procedure TfrmFichaClinica.txtDataExit(Sender: TObject);
begin
  Label5.Caption := 'Tratamentos realizados na data de ' + txtData.Text;
end;

procedure TfrmFichaClinica.FormShow(Sender: TObject);
begin

  vListaTratamento1 := TStringList.Create();
  vListaTratamento2 := TStringList.Create();
  vListaEncaminhamento1 := TStringList.Create();
  vListaEncaminhamento2 := TStringList.Create();

  PageControl2.ActivePageIndex := 0;
  dataFiltro.Date := Date();

  //Carregar a lista de tratamentos/procedimentos
  try
  with dmDeca.cdsSel_TratamentoOdontologico do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_tratamento').Value := Null;
    Params.ParamByName('@pe_flg_status').Value := Null;
    Open;

    while not eof do
    begin
      vListaTratamento1.Add (IntToStr(FieldByName('cod_id_tratamento').Value));
      vListaTratamento2.Add (FieldByName('dsc_tratamento').Value);
      cbTratamentos.Items.Add (FieldByName('dsc_tratamento').Value);
      Next;
    end;

  end;
  except end;

  //Carregar a lista de encaminhamentos
  try
  with dmDeca.cdsSel_Encaminhamento do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_encaminhamento').Value := Null;
    Params.ParamByName('@pe_flg_status').Value := Null;
    Open;

    while not eof do
    begin
      vListaEncaminhamento1.Add (IntToStr(FieldByName('cod_id_encaminhamento').Value));
      vListaEncaminhamento2.Add (FieldByName('dsc_encaminhamento').Value);
      cbEncaminhamentos.Items.Add (FieldByName('dsc_encaminhamento').Value);
      Next;
    end;

  end;
  except end;



  vData := DateToStr(dataFiltro.Date);
  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value     := Null;
      Params.ParamByName('@pe_cod_matricula').Value       := Null;
      Params.ParamByName('@pe_nom_nome').Value            := Null;
      Params.ParamByName('@pe_dat_consulta').Value        := dataFiltro.Date;
      Params.ParamByName('@pe_hor_consulta').Value        := Null;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;
      dbAgenda.Refresh;
    end;
  except end;




end;

procedure TfrmFichaClinica.TabSheet6Show(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmFichaClinica.SpeedButton1Click(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula1.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula1.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        mskMatricula.Text := FieldByName('cod_matricula').AsString;
        lbNomePaciente.Caption := FieldByName('nom_nome').AsString;
        lbNome1.Caption := FieldByName('nom_nome').AsString;
        edNascimento1.Text := FieldByName('dat_nascimento').Value;
        edUnidade1.Text := FieldByName('vUnidade').AsString;
        edFoneContato.Text := FieldByName('num_telefone').AsString;
        txtData1.Text := DateToStr(Date());
        txtHora1.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmFichaClinica.btnAgendarConsultaClick(Sender: TObject);
begin
  if Application.MessageBox('Confirma marca��o da Consulta?',
                            'Sistema Deca - Marca��o de Consulta',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin

    //Primeiro verifica se j� existe consulta marcada para a data e hora informados
    try

      with dmDeca.cdsSel_Consulta do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_consulta').Value := Null;
        Params.ParamByName('@pe_cod_matricula').Value := Null;
        Params.ParamByName('@pe_nom_nome').Value := Null;
        Params.ParamByName('@pe_dat_consulta').Value := GetValue(txtData1, vtDate);
        Params.ParamByName('@pe_hor_consulta').Value := GetValue(txtHora1.Text);
        Open;
      end;

      //Se n�o existir insere os dados da consulta
      if (dmDeca.cdsSel_Consulta.RecordCount < 1) then
      begin

        with dmDeca.cdsInc_Consulta do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula1.Text);
          Params.ParamByName('@pe_dat_consulta').Value := GetValue(txtData1, vtDate);
          Params.ParamByName('@pe_hor_consulta').Value := GetValue(txtHora1.Text);
          Params.ParamByName('@pe_flg_status_consulta').Value := 0; //Consulta Marcada...
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;

          with dmDeca.cdsSel_Consulta do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_consulta').Value := Null;
            Params.ParamByName('@pe_cod_matricula').Value := Null;
            Params.ParamByName('@pe_nom_nome').Value := Null;
            Params.ParamByName('@pe_dat_consulta').Value := GetValue(txtData1, vtDate);
            Params.ParamByName('@pe_hor_consulta').Value := Null;
            Open;
            dbAgenda.Refresh;
          end;

        end;
      end
      else
      begin

        ShowMessage ('J� existe consulta para a data e hor�rio informado...! Favor verificar outro hor�rio...');
        txtHora1.SetFocus;
      end;

    except end;

  end;
end;

procedure TfrmFichaClinica.btnVerConsultasDataClick(Sender: TObject);
//var vData : String;
begin
  vData := DateToStr(dataFiltro.Date);
  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_dat_consulta').AsDateTime := StrToDate(vData);
      Params.ParamByName('@pe_hor_consulta').Value := Null;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;
      dbAgenda.Refresh;
    end;
  except end;
end;

procedure TfrmFichaClinica.dbAgendaDblClick(Sender: TObject);
begin
if (dmDeca.cdsSel_Consulta.FieldByName('flg_status_consulta').Value = 0) or
   (dmDeca.cdsSel_Consulta.FieldByName('flg_status_consulta').Value = 1) then
begin
  //Localiza os dados de unidade e nascimento do paciente
  try
    with dmDeca.cdsSel_Cadastro_Move do
    begin
      Close;
      Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
      Params.ParamByName('@pe_Matricula_Atual').AsString := dmDeca.cdsSel_Consulta.FieldByName('cod_matricula').AsString;
      Params.ParamByName('@pe_cod_unidade').Value:= Null;//vvCOD_UNIDADE;
      Open;

      PageControl2.ActivePageIndex := 1;
      PageControl3.ActivePageIndex := 0;

      txtData.Text := dmDeca.cdsSel_Consulta.FieldByName('vDataConsulta').Value;
      txtHora.Text := dmDeca.cdsSel_Consulta.FieldByName('vHoraConsulta').Value;
      edUnidade.Text := dmDeca.cdsSel_Cadastro_Move.FieldByName('vUnidade').AsString;
      edNascimento.Text := DateToSTr(dmDeca.cdsSel_Cadastro_Move.FieldByName('dat_nascimento').AsDateTime);
      meAnotacoes.Clear;
      meAnotacoes.SetFocus;
      meAnotacoes.Text := dmDeca.cdsSel_Consulta.FieldByName('dsc_anotacoes').Value;
      panNumeroConsulta.Caption := 'Consulta n.� '+ IntToStr(dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').AsInteger);
      lbMatricula.Caption := dmDeca.cdsSel_Consulta.FieldByName('cod_matricula').AsString;
      PageControl3.ActivePageIndex := 0;
      meAnotacoes.SetFocus;
    end;
  except end;

  txtData.Enabled := False;
  txtHora.Enabled := False;
  meAnotacoes.Enabled := True;
  btnGravarDadosConsulta.Enabled := True;
  btnEncerrarConsulta.Enabled := False;
  btnEmitirReceituario.Enabled := False;

  cbTratamentos.ItemIndex := -1;
  cbEncaminhamentos.ItemIndex := -1;
end;
end;

procedure TfrmFichaClinica.btnGravarDadosConsultaClick(Sender: TObject);
begin
  //Atualiza o campo ANOTA��ES da consulta
  try
  with dmDeca.cdsAlt_Consulta do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
    Params.ParamByName('@pe_dsc_anotacoes').Value := GetValue(meAnotacoes.Text);
    Execute;

    btnGravarDadosConsulta.Enabled := False;
    pageControl3.ActivePageIndex := 1;

    btnEncerrarConsulta.Enabled := True;
    btnEmitirReceituario.Enabled := False;
  end;
  except end;
end;

procedure TfrmFichaClinica.btnEncerrarConsultaClick(Sender: TObject);
begin
  //Alterar o status da consulta para "Realizada"
  try
  with dmDeca.cdsAlt_ConsultaStatus do
  begin
    Close;
    Params.ParamByname('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
    Params.ParamByname('@pe_flg_status_consulta').Value := 1;
    Execute;

    btnGravarDadosConsulta.Enabled := True;
    btnEncerrarConsulta.Enabled := False;
    btnEmitirReceituario.Enabled := True;

    dbAgenda.Refresh;
  end;
  except end;

end;

procedure TfrmFichaClinica.btnIncluirTratamentoClick(Sender: TObject);
var d : Integer;
begin
  //Verifica se ja lan�ou o tratamento selecionado  para a consulta
  try
  with dmDeca.cdsSel_TratamentosConsultas do
  begin
    Close;
    Params.ParamByname('@pe_cod_id_tratcons').Value := Null;
    Params.ParamByname('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
    Params.ParamByname('@pe_cod_id_tratamento').Value := StrToInt(vListaTratamento1.Strings[cbTratamentos.ItemIndex]);
    Open;

    //Se n�o existir, insere
    if (dmDeca.cdsSel_TratamentosConsultas.RecordCount < 1) then
    begin

      //Para cada dente em lstDente selecionado, fazer a inclus�o no banco
      for d := 0 to lstDentes.Items.Count - 1 do
      begin
        if (lstDentes.Checked[d]) then
        begin
          with dmDeca.cdsInc_TratamentosConsultas do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
            Params.ParamByName('@pe_cod_id_tratamento').Value := StrToInt(vListaTratamento1.Strings[cbTratamentos.ItemIndex]);
            Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
            Params.ParamByName('@pe_num_dente').Value := StrToInt(lstDentes.Items.Strings[d]);
            Execute;
          end;
        end;
      end;
    end
    else
    begin
      ShowMessage ('Tratamento/Procedimento j� inserido para essa consulta. Selecione outro...');
      cbTratamentos.SetFocus;
    end;

    //Atualiza o grid com os tratamentos lan�ados para a consulta
    with dmDeca.cdsSel_TratamentosConsultas do
    begin
      Close;
      Params.ParamByname('@pe_cod_id_tratcons').Value := Null;
      Params.ParamByname('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
      Params.ParamByname('@pe_cod_id_tratamento').Value := Null;//StrToInt(vListaTratamento1.Strings[cbTratamentos.ItemIndex]);
      Open;

      dbgTratamentosConsultas.Refresh;
    end;
  end;
  except end;
end;

procedure TfrmFichaClinica.TabSheet2Show(Sender: TObject);
begin
  //Carrega as consultas e os tratamentos/procedimentos j� realizados para o paciente
  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := lbMatricula.Caption;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_dat_consulta').Value := Null;
      Params.ParamByName('@pe_hor_consulta').Value := Null;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;
      dbAgenda.Refresh;
    end;
  except end;
end;

procedure TfrmFichaClinica.btnIncluirEncaminhamentoClick(Sender: TObject);
begin

  //Verifica se ja lan�ou o encaminhamento selecionado  para a consulta
  try
  with dmDeca.cdsSel_EncaminhamentosConsultas do
  begin
    Close;
    Params.ParamByname('@pe_cod_id_enccons').Value := Null;
    Params.ParamByname('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
    Params.ParamByname('@pe_cod_id_encaminhamento').Value := StrToInt(vListaEncaminhamento1.Strings[cbEncaminhamentos.ItemIndex]);
    Open;

    //Se n�o existir, insere
    if (dmDeca.cdsSel_EncaminhamentosConsultas.RecordCount < 1) then
    begin

      with dmDeca.cdsInc_EncaminhamentosConsultas do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
        Params.ParamByName('@pe_cod_id_encaminhamento').Value := StrToInt(vListaEncaminhamento1.Strings[cbEncaminhamentos.ItemIndex]);
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;
    end
    else
    begin
      ShowMessage ('Encaminhamento j� inserido para essa consulta. Selecione outro...');
      cbEncaminhamentos.SetFocus;
    end;

    //Atualiza o grid com os tratamentos lan�ados para a consulta
    with dmDeca.cdsSel_EncaminhamentosConsultas do
    begin
      Close;
      Params.ParamByname('@pe_cod_id_enccons').Value := Null;
      Params.ParamByname('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
      Params.ParamByname('@pe_cod_id_encaminhamento').Value := Null;//StrToInt(vListaEncaminhamento1.Strings[cbEncaminhamento.ItemIndex]);
      Open;
      dbgEncaminhamentosConsultas.Refresh;
    end;
  end;
  except end;

end;

procedure TfrmFichaClinica.TabSheet7Show(Sender: TObject);
begin
  cbTipoDente.ItemIndex := -1;
  cbTipoArcada.ItemIndex := -1;
  cbTipoHemiarcada.ItemIndex := -1;
  lstDentes.Clear;

  //Carrega a lista com todos os dentes no ListBox
  try
    with dmDeca.cdsSel_Dente do
    begin
      Close;
      Params.ParamByName('@pe_num_dente').Value := Null;
      Params.ParamByName('@pe_tipo_dente').Value := Null;
      Params.ParamByName('@pe_tipo_arcada').Value := Null;
      Params.ParamByName('@pe_tipo_hemiarcada').Value := Null;
      Open;

      while not eof do
      begin
        lstDentes.Items.Add(FieldByName('num_dente').Value);
        Next;
      end;

    end;
  except end;


end;

procedure TfrmFichaClinica.cbTipoDenteClick(Sender: TObject);
begin
  lstDentes.Clear;
  //Carrega a lista com todos os dentes no ListBox
  try
    with dmDeca.cdsSel_Dente do
    begin
      Close;
      Params.ParamByName('@pe_num_dente').Value := Null;
      Params.ParamByName('@pe_tipo_dente').Value := Copy(cbTipoDente.Text,1,1);
      Params.ParamByName('@pe_tipo_arcada').Value := Null;
      Params.ParamByName('@pe_tipo_hemiarcada').Value := Null;
      Open;

      while not eof do
      begin
        lstDentes.Items.Add(FieldByName('num_dente').Value);
        Next;
      end;

    end;
  except end;
end;

procedure TfrmFichaClinica.cbTipoArcadaClick(Sender: TObject);
begin
  lstDentes.Clear;
  //Carrega a lista com todos os dentes no ListBox
  try
    with dmDeca.cdsSel_Dente do
    begin
      Close;
      Params.ParamByName('@pe_num_dente').Value := Null;
      Params.ParamByName('@pe_tipo_dente').Value := Copy(cbTipoDente.Text,1,1);
      Params.ParamByName('@pe_tipo_arcada').Value := Copy(cbTipoArcada.Text,1,1);
      Params.ParamByName('@pe_tipo_hemiarcada').Value := Null;
      Open;

      while not eof do
      begin
        lstDentes.Items.Add(FieldByName('num_dente').Value);
        Next;
      end;

    end;
  except end;
end;

procedure TfrmFichaClinica.cbTipoHemiarcadaClick(Sender: TObject);
begin
  lstDentes.Clear;
  //Carrega a lista com todos os dentes no ListBox
  try
    with dmDeca.cdsSel_Dente do
    begin
      Close;
      Params.ParamByName('@pe_num_dente').Value := Null;
      Params.ParamByName('@pe_tipo_dente').Value := Copy(cbTipoDente.Text,1,1);
      Params.ParamByName('@pe_tipo_arcada').Value := Copy(cbTipoArcada.Text,1,1);
      Params.ParamByName('@pe_tipo_hemiarcada').Value := Copy(cbTipoHemiarcada.Text,1,1);
      Open;

      while not eof do
      begin
        lstDentes.Items.Add(FieldByName('num_dente').Value);
        Next;
      end;

    end;
  except end;
end;

procedure TfrmFichaClinica.btnVerTodosDentesClick(Sender: TObject);
begin
  cbTipoDente.ItemIndex := -1;
  cbTipoArcada.ItemIndex := -1;
  cbTipoHemiarcada.ItemIndex := -1;
  lstDentes.Clear;

  //Carrega a lista com todos os dentes no ListBox
  try
    with dmDeca.cdsSel_Dente do
    begin
      Close;
      Params.ParamByName('@pe_num_dente').Value := Null;
      Params.ParamByName('@pe_tipo_dente').Value := Null;
      Params.ParamByName('@pe_tipo_arcada').Value := Null;
      Params.ParamByName('@pe_tipo_hemiarcada').Value := Null;
      Open;

      while not eof do
      begin
        lstDentes.Items.Add(FieldByName('num_dente').Value);
        Next;
      end;

    end;
  except end;
end;

procedure TfrmFichaClinica.chkMarcaTodosListaClick(Sender: TObject);
var n : Integer;
begin
  if (chkMarcaTodosLista.Checked = True) then
  begin
    //Marca todos os itens da lista de dentes
    for n := 0 to lstDentes.Items.Count - 1 do
      lstDentes.Checked[n] := True;
  end
  else if (chkMarcaTodosLista.Checked = False) then
  begin
    //Marca todos os itens da lista de dentes
    for n := 0 to lstDentes.Items.Count - 1 do
      lstDentes.Checked[n] := False;
  end;  
end;

procedure TfrmFichaClinica.btnImprimirAgendaClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value     := Null;
      Params.ParamByName('@pe_cod_matricula').Value       := Null;
      Params.ParamByName('@pe_nom_nome').Value            := Null;
      Params.ParamByName('@pe_dat_consulta').AsDateTime   := StrToDate(vData);
      Params.ParamByName('@pe_hor_consulta').Value        := Null;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;

      if (dmDeca.cdsSel_Consulta.RecordCount < 1) then
        ShowMessage ('N�o existem pacientes agendados para este dia...')
      else
      begin
        Application.CreateForm (TrelAgendaDiaOdontologia, relAgendaDiaOdontologia);
        relAgendaDiaOdontologia.lbTitulo.Caption := 'Rela��o de Hor�rios das consultas da data de ' + DateToStr(dataFiltro.Date);
        relAgendaDiaOdontologia.Preview;
        relAgendaDiaOdontologia.Free;
      end;
    end;
  except end;
end;

procedure TfrmFichaClinica.lstDentesClick(Sender: TObject);
begin
  //ShowMessage (lstDentes.Items.Strings[lstDentes.ItemIndex]);
end;

procedure TfrmFichaClinica.dbgConsultasPacienteCellClick(Column: TColumn);
begin
  try
    //Verifica os tratamentos realizados na consulta selecionada
    with dmDeca.cdsSel_TratamentosConsultas do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_tratcons').Value := Null;
      Params.ParamByName('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
      Params.ParamByName('@pe_cod_id_tratamento').Value := Null;
      Open;
      dbgTratamentosConsultasP.Refresh;
    end;

    //Verifica os encaminhamentos realizados na consulta selecionada
    with dmDeca.cdsSel_EncaminhamentosConsultas do
    begin
      Params.ParamByName('@pe_cod_id_enccons').Value := Null;
      Params.ParamByName('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
      Params.ParamByName('@pe_cod_id_encaminhamento').Value := Null;
      Open;
      dbgEncaminhamentosConsultasP.Refresh;
    end;

  except end;
end;

procedure TfrmFichaClinica.btnPacienteAusenteClick(Sender: TObject);
begin
  //Se o status estiver somente como "Marcado"
  if (dmDeca.cdsSel_Consulta.FieldByName('flg_status_consulta').Value = 0) then
  begin
    //Altera o status da consulta para paciente "ausente"
    try
      with dmDeca.cdsAlt_ConsultaStatus do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_consulta').Value := dmDeca.cdsSel_Consulta.FieldByName('cod_id_consulta').Value;
        Params.ParamByName('@pe_flg_status_consulta').Value := 4; //Para status = Ausente
        Execute;
        dbAgenda.Refresh;

        vData := DateToStr(dataFiltro.Date);
        try
          with dmDeca.cdsSel_Consulta do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_consulta').Value := Null;
            Params.ParamByName('@pe_cod_matricula').Value := Null;
            Params.ParamByName('@pe_nom_nome').Value := Null;
            Params.ParamByName('@pe_dat_consulta').AsDateTime := StrToDate(vData);
            Params.ParamByName('@pe_hor_consulta').Value := Null;
            Params.ParamByName('@pe_flg_status_consulta').Value := Null;
            Open;
            dbAgenda.Refresh;
          end;
        except end;
      end;
    except end;
  end
  else
  begin
    ShowMessage ('Imposs�vel alterar a situa��o da consulta...');;
  end;
end;

procedure TfrmFichaClinica.SpeedButton6Click(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula1.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula1.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        mskMatricula.Text := FieldByName('cod_matricula').AsString;
        lbNomePaciente.Caption := FieldByName('nom_nome').AsString;

        try
          //Faz a pesquisa das consultas realizadas pelo paciente selecionado
          with dmDeca.cdsSel_Consulta do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_consulta').Value := Null;
            Params.ParamByName('@pe_cod_matricula').Value := mskMatricula.Text;
            Params.ParamByName('@pe_nom_nome').Value := Null;
            Params.ParamByName('@pe_dat_consulta').Value := Null;
            Params.ParamByName('@pe_hor_consulta').Value := Null;
            Params.ParamByName('@pe_flg_status_consulta').Value := Null;
            Open;
            dbgConsultasPaciente.Refresh;
          end;
        except end;
      end;
    except end;
  end;
end;

procedure TfrmFichaClinica.btnEmitirReceituarioClick(Sender: TObject);
begin
  Application.CreateForm ( TfrmReceituario, frmReceituario );
  frmReceituario.ShowModal;
end;

procedure TfrmFichaClinica.btnExcluirTratamentoClick(Sender: TObject);
begin
  try
    with dmDeca.cdsExc_TratamentoConsulta do
    begin
        close;
        params.parambyname('@pe_cod_id_tratcons').value:=dmDeca.cdsSel_TratamentosConsultas.FieldByName('cod_id_tratcons').value;
        execute;
    end;

   dbgTratamentosConsultas.refresh;
                                                         

  except end;
end;

procedure TfrmFichaClinica.btnExcluirEncaminhamentoClick(Sender: TObject);
begin
try
    with dmDeca.cdsExc_EncaminhamentoConsulta do
    begin
        close;
        params.parambyname('@pe_cod_id_enccons').value:=dmDeca.cdsSel_EncaminhamentosConsultas.FieldByName('cod_id_enccons').value;
        execute;
    end;
    dbgEncaminhamentosConsultas.refresh;

    except end;

end;

procedure TfrmFichaClinica.SpeedButton2Click(Sender: TObject);
begin
  //Excluir da agenda...
end;

procedure TfrmFichaClinica.btnREMARCARClick(Sender: TObject);
begin
  //Exibir janela para sele��o de data e hora para remarca��o...
  vvCOD_CONSULTA_EXCLUI := dmDeca.cdsSel_Consulta.FieldByName ('cod_id_consulta').Value;
  Application.CreateForm(TfrmREMARCACAO, frmREMARCACAO);
  frmRemarcacao.ShowModal;
end;

end.
