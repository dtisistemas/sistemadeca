unit uConsultaDadosDoBoletim;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, jpeg, Grids, DBGrids, ComCtrls, Db, QRExport;

type
  TfrmConsultaDadosDoBoletim = class(TForm)
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    cbUnidadeC: TComboBox;
    GroupBox3: TGroupBox;
    cbEscolaC: TComboBox;
    chkInibeEscola: TCheckBox;
    Image1: TImage;
    Panel1: TPanel;
    dbgDadosBoletim: TDBGrid;
    Panel2: TPanel;
    btnPesquisar: TSpeedButton;
    chkTodas: TCheckBox;
    GroupBox4: TGroupBox;
    cbBimestre: TComboBox;
    GroupBox5: TGroupBox;
    edAnoLetivo: TEdit;
    btnFiltar: TSpeedButton;
    btnExportar: TSpeedButton;
    dsBoletim: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cbSituacao: TComboBox;
    cbDefasagem: TComboBox;
    Panel4: TPanel;
    ProgressBar1: TProgressBar;
    Label4: TLabel;
    btnConsultaHistoricoSocial: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SaveDialog1: TSaveDialog;
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeCClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnFiltarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnConsultaHistoricoSocialClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure cbEscolaCClick(Sender: TObject);
    procedure chkTodasClick(Sender: TObject);
    procedure chkInibeEscolaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaDadosDoBoletim: TfrmConsultaDadosDoBoletim;
  vUnidade1,vUnidade2 : TStringList;
  arqCSV              : TextFile;

implementation

uses uDM, uLoginNovo, uPrincipal, uConsultaHistoricoSocial, rDadosBoletim,
  uEnviarEmail, uConsultaDadosAcompEscolar, uUtil;

{$R *.DFM}

procedure TfrmConsultaDadosDoBoletim.FormShow(Sender: TObject);
begin

  cbBimestre.ItemIndex := 0;
  edAnoLetivo.Text :=  Copy(DateToStr(Date()),7,4);   

  Panel4.Visible := False;
  Panel2.Caption := '';

  vUnidade1 := TStringList.Create();
  vUnidade2 := TStringList.Create();
  //vEscola1  := TStringList.Create();
  //vEscola2  := TStringList.Create();

  //Carregar a lista de unidades...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
    end;

    while not dmDeca.cdsSel_Unidade.eof do
    begin

      if (dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').AsInteger in [1,3,7,9,12,13,40,42,43,47,48,50,71]) then
        dmDeca.cdsSel_Unidade.Next
      else
      begin
        cbUnidadeC.Items.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
        vUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName('cod_unidade').Value));
        vUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end
    end;
  except end;



end;

procedure TfrmConsultaDadosDoBoletim.cbUnidadeCClick(Sender: TObject);
begin

  //Seleciona as escolas da unidade selecionada
  try
    with dmDeca.cdsSel_EscolaAcompanhamento do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadeC.ItemIndex]);  //StrToInt(vUnidade1.Strings[cbUnidadeC.ItemIndex]);
      Open;

      cbEscolaC.Clear;
      while not dmDeca.cdsSel_EscolaAcompanhamento.eof do
      begin
        cbEscolaC.Items.Add (dmDeca.cdsSel_EscolaAcompanhamento.FieldByName ('nom_escola').Value);
        dmDeca.cdsSel_EscolaAcompanhamento.Next;
      end;

    end;
  except end;

end;

procedure TfrmConsultaDadosDoBoletim.btnPesquisarClick(Sender: TObject);
begin


  if (chkTodas.Checked)then
  begin

    if Application.MessageBox ('Deseja consultar os dados escolares de TODOS os alunos da institui��o?',
                               '[Sistema Deca] - Consulta dados escolares',
                               MB_ICONQUESTION + MB_YESNO) = id_yes then
    begin

      Panel4.Visible := True;
      ProgressBar1.Position := 0;

      //Excluir os dados referentes ao boletim, inseridos pelo usu�rio atual
      with dmDeca.cdsExc_Boletim do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;

      try
        //Seleciona os dados do cadastro para a unidade selecionada...
        with dmDeca.cdsSel_Cadastro do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').Value   := 0;  //Todas as UNIDADES...//StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
          Open;

          //Se n�o houver alunos cadastrados
          if dmDeca.cdsSel_Cadastro.RecordCount < 1 then
          begin
            //Mensagem caso n�o encontre nenhum aluno na unidade selecionada...

          end
          else if dmDeca.cdsSel_Cadastro.RecordCount > 0 then
          begin
            //Encontrou...
            //Enquanto n�o fim-do-cadastro

            ProgressBar1.Min := 0;
            ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

            dmDeca.cdsSel_Cadastro.First;

            if  (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 1) or (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 3) or
                (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 9) or (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 12) or
                (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 40) or (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 42) or
                (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 43) or (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 47) or
                (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 48) or (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 50) or
                (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 84) then
            begin

            while not (dmDeca.cdsSel_Cadastro.eof) do
            begin
              //Incluir dados cadastrais na tabela BOLETIM
              with dmDeca.cdsInc_Boletim do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
                Params.ParamByName ('@pe_nom_nome').Value := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
                Params.ParamByName ('@pe_num_rg_escolar').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_escolar').Value;
                Params.ParamByName ('@pe_dat_nascimento').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value;
                Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value;

                //Seleciona a escola e s�rie atuais
                with dmDeca.cdsSel_HistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_cod_escola').Value := Null;
                  Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAnoLetivo.Text);
                  Open;

                  if dmDeca.cdsSel_HistoricoEscola.RecordCount > 0 then
                  begin
                    while not (dmDeca.cdsSel_HistoricoEscola.eof) do
                    begin
                      //Verifica se � escola/s�rie atuais...
                      if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 0) then
                      begin
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value;
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := dmDeca.cdsSel_HistoricoEscola.FieldByName ('dsc_serie').Value;
                        dmDeca.cdsSel_HistoricoEscola.Last;
                      end
                      //Ou n�o � escola/s�rie atuais...
                      else if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 1) then
                      begin
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := '--';
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := '--';
                        dmDeca.cdsSel_HistoricoEscola.Last;
                      end;
                    end;
                  end
                  else
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := ' ';
                  end;
                end; //with dmDeca.cdsSel_HistoricoEscola do


                {*************** Consultar dados do Acompanhamento Escolar **************************************}
                //1.� Bimestre do ano informado na tela
                with dmDeca.cdsSel_AcompEscolar do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
                  Params.ParamByName ('@pe_num_bimestre').Value          := 1;
                  Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                  Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                  Open;

                  //Se encontrou registros do Acomp. Escolar referentes ao 1.� Bimestre
                  if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
                  begin
                    //Avalia o ind_frequencia
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '+75%';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '-75%';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '--';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'FLX';
                    end;

                    //Avalia o ind_aproveitamento
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AB';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'DE';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AC';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := '--';
                    end;
                  end
                  else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := ' ';
                  end;
                end; //with dmDeca.cdsSel_AcompEscolar do... 1.� bimestre

                //Recupera a Turma - 1.� Bimestre
                if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
                  mmTURMA1B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
                else
                  mmTURMA1B := '-';
                {*************************************************************************************************}


                {*************** Consultar dados do Acompanhamento Escolar ***************************************}
                //2.� Bimestre do ano informado na tela
                with dmDeca.cdsSel_AcompEscolar do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
                  Params.ParamByName ('@pe_num_bimestre').Value          := 2;
                  Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                  Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                  Open;

                  //Se encontrou registros do Acomp. Escolar referentes ao 2.� Bimestre
                  if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
                  begin
                    //Avalia o ind_frequencia
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '+75%';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '-75%';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '--';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'FLX';
                    end;

                    //Avalia o ind_aproveitamento
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AB';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'DE';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AC';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := '--';
                    end;
                  end
                  else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := ' ';
                  end;
                end; //with dmDeca.cdsSel_AcompEscolar do... 2.� bimestre

                //Recupera a Turma - 2.� Bimestre
                if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
                  mmTURMA2B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
                else
                  mmTURMA2B := '-';
                {*************************************************************************************************}

                {*************** Consultar dados do Acompanhamento Escolar ***************************************}
                //3.� Bimestre do ano informado na tela
                with dmDeca.cdsSel_AcompEscolar do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
                  Params.ParamByName ('@pe_num_bimestre').Value          := 3;
                  Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                  Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                  Open;

                  //Se encontrou registros do Acomp. Escolar referentes ao 3.� Bimestre
                  if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
                  begin
                    //Avalia o ind_frequencia
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '+75%';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '-75%';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '--';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'FLX';
                    end;

                    //Avalia o ind_aproveitamento
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AB';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'DE';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AC';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := '--';
                    end;
                  end
                  else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := ' ';
                  end;
                end; //with dmDeca.cdsSel_AcompEscolar do... 3.� bimestre

                //Recupera a Turma - 3.� Bimestre
                if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
                  mmTURMA3B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
                else
                  mmTURMA3B := '-';
                {*************************************************************************************************}

                //Compara as turmas lan�adas nos bimestres
                if (mmTURMA1B = mmTURMA2B) then//or (mmTURMA1B = mmTURMA3B) then
                  mmTURMA_ATUAL := mmTURMA1B
                else if (mmTURMA1B <> mmTURMA2B) or (mmTURMA2B = '')then// or (mmTURMA2B = mmTURMA3B) then
                  mmTURMA_ATUAL := mmTURMA2B
                else if (mmTURMA2B = mmTURMA3B) then //or (mmTURMA2B <> mmTURMA3B) then
                  mmTURMA_ATUAL := mmTURMA2B
                else
                  mMTURMA_ATUAL := mmTURMA3B;

                //Repassa a turma atual para o boletim...
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_turma').Value := mmTURMA_ATUAL;

                dmDeca.cdsInc_Boletim.Execute;

              end; //Fim with dmDeca.cdsInc_Boletim do...

              dmDeca.cdsSel_Cadastro.Next;
              ProgressBar1.Position := ProgressBar1.Position + 1;
            end; //while not (dmDeca.cdsSel_Cadastro.eof) do
            end
            else //if dmDeca.cdsSel_Cadastro.FieldByName ().... not in...
            begin
              //Se a unidade faz parte de: Desligados, Inativos, etc...
              dmDeca.cdsSel_Cadastro.Next;
            end;

          end
          else
          begin
            //Inverso caso a unidade esteja na lista "negra"
          end;

        end;


        Panel4.Visible := False;
        ProgressBar1.Position := 0;

        //Exibe a rela��o com os dados coletados...
        with dmDeca.cdsSel_Boletim do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
          Params.ParamByName ('@pe_nom_escola').Value := Null;
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Open;

          Application.MessageBox ('Sua pesquisa foi realizada com sucesso. Agora, clique ' + #13+#10 +
                                  'em FILTRAR DADOS BOLETIM, para filtrar informa��es do boletim. '+ #13+#10 +
                                  'Caso necessite, utilize as op��es de filtro.',
                                  '[Sistema Deca] - Consulta Dados do Acomp. Escolar',
                                  MB_OK + MB_ICONINFORMATION);


          Panel2.Caption := '       TOTAL DE REGISTROS ENCONTRADOS..........>>>>> ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount);


          //Carrega as escolas relativas a unidade no combo
          try
            with dmDeca.cdsSel_EscolaBoletim do
            begin

              Close;
              if (chkTodas.Checked = True) then
                Params.ParamByName ('@pe_cod_unidade').Value := Null
              else if (chkTodas.Checked = False) then
                Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vUnidade1.Strings[cbUnidadeC.ItemIndex]);
              Open;

              if (dmDeca.cdsSel_EscolaBoletim.RecordCount < 1) then
              begin
                Application.MessageBox ('N�o foi encontrada nenhuma escola para a unidade.',
                                        '[Sistema Deca] - Selecionando Escolas...',
                                         MB_OK + MB_ICONINFORMATION);
                cbUnidadeC.SetFocus;
              end
              else
              begin

                cbEscolaC.Clear;
                while not (dmDeca.cdsSel_EscolaBoletim.eof) do
                begin
                  cbEscolaC.Items.Add (FieldByName ('nom_escola').Value);
                  dmDeca.cdsSel_EscolaBoletim.Next;
                end;

              end;

            end;
          except end;
          

        end;

      except
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Um erro ocorreu. Favor entrar em contato com o Administrador',
                                '[Sistema Deca] - Acomp. Escolar',
                                MB_OK + MB_ICONEXCLAMATION);
      end;

    end
    else //Usu�rio n�o quer gerar a lista agora...
    begin

    end;
  end
  else
  begin

    if Application.MessageBox ('Deseja consultar os dados escolares dos alunos da unidade selecionada?',
                               '[Sistema Deca] - Consulta dados escolares',
                               MB_ICONQUESTION + MB_YESNO) = id_yes then
    begin


      Panel4.Visible := True;
      ProgressBar1.Position := 0;

      //Excluir os dados referentes ao boletim, inseridos pelo usu�rio atual
      with dmDeca.cdsExc_Boletim do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;

      try
        //Seleciona os dados do cadastro para a unidade selecionada...
        with dmDeca.cdsSel_Cadastro do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vUnidade1.Strings[cbUnidadeC.ItemIndex]);
          Open;

          //Se n�o houver alunos cadastrados
          if dmDeca.cdsSel_Cadastro.RecordCount < 1 then
          begin
            //Mensagem caso n�o encontre nenhum aluno na unidade selecionada...

          end
          else if dmDeca.cdsSel_Cadastro.RecordCount > 0 then
          begin
            //Encontrou...
            //Enquanto n�o fim-do-cadastro

            ProgressBar1.Min := 0;
            ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

            dmDeca.cdsSel_Cadastro.First;
            while not (dmDeca.cdsSel_Cadastro.eof) do
            begin
              //Incluir dados cadastrais na tabela BOLETIM
              with dmDeca.cdsInc_Boletim do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
                Params.ParamByName ('@pe_nom_nome').Value := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value;
                Params.ParamByName ('@pe_num_rg_escolar').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_rg_escolar').Value;
                Params.ParamByName ('@pe_dat_nascimento').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value;
                Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadeC.ItemIndex]);

                //Seleciona a escola e s�rie atuais
                with dmDeca.cdsSel_HistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_cod_escola').Value := Null;
                  Params.ParamByName ('@pe_num_ano_letivo').Value := StrToInt(edAnoLetivo.Text);
                  Open;

                  if dmDeca.cdsSel_HistoricoEscola.RecordCount > 0 then
                  begin
                    while not (dmDeca.cdsSel_HistoricoEscola.eof) do
                    begin
                      //Verifica se � escola/s�rie atuais...
                      if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 0) then
                      begin
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := dmDeca.cdsSel_HistoricoEscola.FieldByName ('nom_escola').Value;
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := dmDeca.cdsSel_HistoricoEscola.FieldByName ('dsc_serie').Value;
                        dmDeca.cdsSel_HistoricoEscola.Last;
                      end
                      //Ou n�o � escola/s�rie atuais...
                      else if (dmDeca.cdsSel_HistoricoEscola.FieldByName ('flg_status').Value = 1) then
                      begin
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := '--';
                        dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := '--';
                        dmDeca.cdsSel_HistoricoEscola.Last;
                      end;
                    end;
                  end
                  else
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_escola').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_serie').Value  := ' ';
                  end;
                end; //with dmDeca.cdsSel_HistoricoEscola do


                {*************** Consultar dados do Acompanhamento Escolar **************************************}
                //1.� Bimestre do ano informado na tela
                with dmDeca.cdsSel_AcompEscolar do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
                  Params.ParamByName ('@pe_num_bimestre').Value          := 1;
                  Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                  Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                  Open;

                  //Se encontrou registros do Acomp. Escolar referentes ao 1.� Bimestre
                  if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
                  begin
                    //Avalia o ind_frequencia
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '+75%';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '-75%';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := '--';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := 'FLX';
                    end;

                    //Avalia o ind_aproveitamento
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AB';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'DE';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := 'AC';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := '--';
                    end;
                  end
                  else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_1b').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_1b').Value := ' ';
                  end;
                end; //with dmDeca.cdsSel_AcompEscolar do... 1.� bimestre

                //Recupera a Turma - 1.� Bimestre
                if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
                  mmTURMA1B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
                else
                  mmTURMA1B := '-';
                {*************************************************************************************************}


                {*************** Consultar dados do Acompanhamento Escolar ***************************************}
                //2.� Bimestre do ano informado na tela
                with dmDeca.cdsSel_AcompEscolar do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
                  Params.ParamByName ('@pe_num_bimestre').Value          := 2;
                  Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                  Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                  Open;

                  //Se encontrou registros do Acomp. Escolar referentes ao 2.� Bimestre
                  if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
                  begin
                    //Avalia o ind_frequencia
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '+75%';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '-75%';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := '--';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := 'FLX';
                    end;

                    //Avalia o ind_aproveitamento
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AB';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'DE';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := 'AC';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := '--';
                    end;
                  end
                  else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_2b').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_2b').Value := ' ';
                  end;
                end; //with dmDeca.cdsSel_AcompEscolar do... 2.� bimestre

                //Recupera a Turma - 2.� Bimestre
                if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
                  mmTURMA2B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
                else
                  mmTURMA2B := '-';
                {*************************************************************************************************}

                {*************** Consultar dados do Acompanhamento Escolar ***************************************}
                //3.� Bimestre do ano informado na tela
                with dmDeca.cdsSel_AcompEscolar do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                  Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(edAnoLetivo.Text);
                  Params.ParamByName ('@pe_num_bimestre').Value          := 3;
                  Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                  Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                  Open;

                  //Se encontrou registros do Acomp. Escolar referentes ao 3.� Bimestre
                  if (dmDeca.cdsSel_AcomPescolar.RecordCount > 0) then
                  begin
                    //Avalia o ind_frequencia
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '+75%';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '-75%';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := '--';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := 'FLX';
                    end;

                    //Avalia o ind_aproveitamento
                    case dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value of
                      0 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AB';
                      1 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'DE';
                      2 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := 'AC';
                      3 : dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := '--';
                    end;
                  end
                  else if dmDeca.cdsSel_AcompEscolar.RecordCount = 0 then
                  begin
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_freq_3b').Value := ' ';
                    dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_dsc_aprov_3b').Value := ' ';
                  end;
                end; //with dmDeca.cdsSel_AcompEscolar do... 3.� bimestre

                //Recupera a Turma - 3.� Bimestre
                if not(dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').IsNull) then
                  mmTURMA3B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value
                else
                  mmTURMA3B := '-';
                {*************************************************************************************************}

                //Compara as turmas lan�adas nos bimestres
                if (mmTURMA1B = mmTURMA2B) then//or (mmTURMA1B = mmTURMA3B) then
                  mmTURMA_ATUAL := mmTURMA1B
                else if (mmTURMA1B <> mmTURMA2B) or (mmTURMA2B = '') then// or (mmTURMA2B = mmTURMA3B) then
                  mmTURMA_ATUAL := mmTURMA1B
                else if (mmTURMA2B = mmTURMA3B) then //or (mmTURMA2B <> mmTURMA3B) then
                  mmTURMA_ATUAL := mmTURMA2B
                else
                  mMTURMA_ATUAL := mmTURMA3B;

                //Repassa a turma atual para o boletim...
                dmDeca.cdsInc_Boletim.Params.ParamByName ('@pe_nom_turma').Value := mmTURMA_ATUAL;

                dmDeca.cdsInc_Boletim.Execute;

              end; //Fim with dmDeca.cdsInc_Boletim do...

              dmDeca.cdsSel_Cadastro.Next;
              ProgressBar1.Position := ProgressBar1.Position + 1;
            end; //while not (dmDeca.cdsSel_Cadastro.eof) do

          end
          else
          begin
            //Inverso caso a unidade esteja na lista "negra"
          end;

        end;


        Panel4.Visible := False;
        ProgressBar1.Position := 0;

        //Exibe a rela��o com os dados coletados...
        with dmDeca.cdsSel_Boletim do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := Null;
          Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
          Params.ParamByName ('@pe_nom_escola').Value := Null;
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Open;

          Application.MessageBox ('Sua pesquisa foi realizada com sucesso. Agora, clique ' + #13+#10 +
                                  'em FILTRAR DADOS BOLETIM, para filtrar informa��es do boletim. '+ #13+#10 +
                                  'Caso necessite, utilize as op��es de filtro.',
                                  '[Sistema Deca] - Consulta Dados do Acomp. Escolar',
                                  MB_OK + MB_ICONINFORMATION);

          //Panel2.Caption := '       TOTAL DE REGISTROS ENCONTRADOS..........>>>>> ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount);
          Panel2.Caption := 'Totais = >>  Efetivo Unidade:  '  + IntToStr(dmDeca.cdsSel_Cadastro.RecordCount) +
                            '  -  ' + 'Lan�amentos: ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount);





          //Carrega as escolas relativas a unidade no combo
          try
            with dmDeca.cdsSel_EscolaBoletim do
            begin

              Close;
              if (chkTodas.Checked) then
                Params.ParamByName ('@pe_cod_unidade').Value := Null
              else
                Params.ParamByName ('@pe_cod_unidade').AsInteger := StrToInt(vUnidade1.Strings[cbUnidadeC.ItemIndex]);
              Open;

              if (dmDeca.cdsSel_EscolaBoletim.RecordCount < 1) then
              begin
                Application.MessageBox ('N�o foi encontrada nenhuma escola para a unidade.',
                                        '[Sistema Deca] - Selecionando Escolas...',
                                         MB_OK + MB_ICONINFORMATION);
                cbUnidadeC.SetFocus;
              end
              else
              begin

                cbEscolaC.Clear;
                while not dmDeca.cdsSel_EscolaBoletim.eof do
                begin
                  cbEscolaC.Items.Add (FieldByName ('nom_escola').Value);
                  dmDeca.cdsSel_EscolaBoletim.Next;
                end;

              end;

            end;
          except end;                                  
          
        end;

      except
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Um erro ocorreu. Favor entrar em contato com o Administrador',
                                '[Sistema Deca] - Acomp. Escolar',
                                MB_OK + MB_ICONEXCLAMATION);
      end;


    end;

  end;



end;

procedure TfrmConsultaDadosDoBoletim.btnFiltarClick(Sender: TObject);
begin

  try

    with dmDeca.cdsSel_Boletim do
    begin

      Close;
      Params.ParamByName ('@pe_cod_matricula').Value  := Null;
      Params.ParamByName ('@pe_num_rg_escolar').Value := Null;


      if (chkInibeEscola.Checked) then
        Params.ParamByName ('@pe_nom_escola').Value := Null
      else
        Params.ParamByName ('@pe_nom_escola').Value := cbEscolaC.Text;

      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;

      case cbSituacao.ItemIndex of
        0 : Params.ParamByName ('@pe_ind_status').Value := Null;
        1 : Params.ParamByName ('@pe_ind_status').Value := 1;
        2 : Params.ParamByName ('@pe_ind_status').Value := 3;
        3 : Params.ParamByName ('@pe_ind_status').Value := 4;
        4 : Params.ParamByName ('@pe_ind_status').Value := 5;
      end;

      case cbDefasagem.ItemIndex of
        0 : Params.ParamByName ('@pe_flg_defasagem').Value := Null;
        1 : Params.ParamByName ('@pe_flg_defasagem').Value := 0;
        2 : Params.ParamByName ('@pe_flg_defasagem').Value := 1;
      end;

      Open;

      //Panel2.Caption := '       TOTAL DE REGISTROS ENCONTRADOS..........>>>>> ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount);
      Panel2.Caption := 'Totais = >>  Efetivo Unidade:  '  + IntToStr(dmDeca.cdsSel_Cadastro.RecordCount) +
                        '  -  ' + 'Lan�amentos: ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount);


    end

  except end;

end;

procedure TfrmConsultaDadosDoBoletim.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) then
  begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if Key = #27 then
  begin
    Key := #0;
    frmConsultaDadosDoBoletim.Close;
  end;
end;

procedure TfrmConsultaDadosDoBoletim.btnConsultaHistoricoSocialClick(
  Sender: TObject);
begin

  Application.CreateForm (TfrmHistoricoSocial,frmHistoricoSocial);

  try

    //Consulta dados do Hist�rico de Interven��es...
    with dmDeca.cdsSel_Cadastro_Historico do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Boletim.FieldByname ('cod_matricula').Value;
      Params.ParamByName ('@pe_cod_id_historico').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value      := Null;
      Open;
    end;


    //Consulta dados da Evolu��o do Servi�o Social...
    with dmDeca.cdsSel_Cadastro_Evolucao_SC do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Boletim.FieldByname ('cod_matricula').Value;
      Params.ParamByName ('@pe_cod_id_evolucao').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value      := Null;
      Open;
    end;

  except end;

  frmHistoricoSocial.lbIdentificacao.Caption := dmDeca.cdsSel_Boletim.FieldByName ('cod_matricula').Value + '  -  ' + dmDeca.cdsSel_Boletim.FieldByName ('nom_nome').Value;
  frmHistoricoSocial.ShowModal;

end;

procedure TfrmConsultaDadosDoBoletim.SpeedButton1Click(Sender: TObject);
begin
  //GERAR O RELAT�RIO
  Application.CreateForm (TrelDadosBoletim, relDadosBoletim);
  relDadosBoletim.lbTitulo.Caption := cbUnidadeC.Text + ' - ' + cbBimestre.Text + '/' + edAnoLetivo.Text;
  relDadosBoletim.Preview;
  relDadosBoletim.Free;
end;

procedure TfrmConsultaDadosDoBoletim.btnExportarClick(Sender: TObject);
var lin : Integer;
    mmCOD_MATRICULA,
    mmNOM_ALUNO,
    mmNOM_UNIDADE,
    mmRG_ESCOLAR,
    mmDAT_NASC,
    mmNOM_ESCOLA,
    mmDSC_SERIE,
    mmNOM_TURMA : String;
begin
  //Application.MessageBox ('Aten��o !!!' + #13+#10 +
  //                        'Esse m�dulo ainda est� em Desenvolvimento',
  //                        '[Sistema Deca] - Acomp. Escolar',
  //                        MB_OK + MB_ICONEXCLAMATION);

  //****************************************************************************
  //Procedimento para gerar o arquivo CSV na pasta selecionada pelo usu�rio
  // 1.- Gerar o arquivo
  // 2.- Chamar a tela de envio de e-mail com o anexo
if ((cbEscolaC.ItemIndex >= 0) and (chkInibeEscola.Checked = False)) or (vvIND_PERFIL in [1,18,19,20,23,24,25]) then
begin

  SaveDialog1.FileName := cbEscolaC.Text;
  SaveDialog1.Execute;

  AssignFile (arqCSV, SaveDialog1.FileName);
  Rewrite    (arqCSV);

  dmDeca.cdsSel_Boletim.First;
  
  try

    with dmDeca.cdsSel_Boletim do
    begin

      Writeln (arqCSV, '[FUNDHAS] - Funda��o H�lio Augusto de Souza');
      Writeln (arqCSV, '[Sistema Deca] - Rela��o de Dados do Acompanhamento Escolar');
      Writeln (arqCSV, cbBimestre.Text + '/' + edAnoLetivo.Text);
      Writeln (arqCSV, 'Data/Hora Impress�o: ' + DateToStr(Date()) + '/' + TimeToStr(Time()));

      if chkTodas.Checked then
        Writeln (arqCSV, 'Unidade: FUNDHAS')
      else
        Writeln (arqCSV, 'Unidade: ' + cbUnidadeC.Text);

      Writeln (arqCSV, '');
      Writeln (arqCSV, '');

      Writeln  (arqCSV, 'Matricula;Nome;Unidade;RGEscolar;Nascimento;Escola;Serie;Turma;Aproveitamento;Frequencia');


      for lin := 0 to RecordCount - 1 do
      begin

        mmCOD_MATRICULA := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('cod_matricula').AsString);
        mmNOM_ALUNO     := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('nom_nome').AsString);
        mmNOM_UNIDADE   := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('nom_unidade').AsString);
        mmRG_ESCOLAR    := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('num_rg_escolar').AsString);
        mmDAT_NASC      := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('DatNascimento').AsString);
        mmNOM_ESCOLA    := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('nom_escola').AsString);
        mmDSC_SERIE     := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('dsc_serie').AsString);
        mmNOM_TURMA     := GetValue(dmDeca.cdsSel_Boletim.FieldByName ('nom_turma').AsString);

        Write (arqCSV, mmCOD_MATRICULA + ';');
        Write (arqCSV, mmNOM_ALUNO + ';');
        Write (arqCSV, mmNOM_UNIDADE + ';');
        Write (arqCSV, mmRG_ESCOLAR + ';');
        Write (arqCSV, mmDAT_NASC + ';');
        Write (arqCSV, mmNOM_ESCOLA + ';');
        Write (arqCSV, mmDSC_SERIE + ';');
        Write (arqCSV, mmNOM_TURMA + ';');
        Writeln (arqCSV, '');
        dmDeca.cdsSel_Boletim.Next;
      end;

      Write (arqCSV, 'Total de  ' + IntToStr(dmDeca.cdsSel_Boletim.RecordCount) + ' registros lan�ados.');

      CloseFile(arqCSV);

    end;

    if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19) or (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then
    begin
      if Application.MessageBox ('Deseja enviar o arquivo por e-mail agora?',
                                 '[Sistema Deca] - Envio de arquivo por e-mail.',
                                 MB_YESNO + MB_ICONQUESTION) = id_yes then
    //begin
      Application.CreateForm (TfrmEnviarEmail, frmEnviarEmail);
      frmEnviarEmail.edEmailPara.Text := vvEMAIL_ESCOLA;
      frmEnviarEmail.edAssunto.Text   := 'Confer�ncia de dados - Alunos FUNDHAS';
      frmEnviarEmail.ShowModal;
    end;

  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar exportar os dados para o arquivo informado.',
                            '[Sistema Deca] - Erro ao exportar dados para arquivo.',
                            MB_OK + MB_ICONINFORMATION);
    Exit;
  end
end
else  //Caso cbEscola esteja sem sele��o ou inibe escolar for verdadeiro
begin
  Application.MessageBox ('Aten��o!!!' + #13+#10 +
                          'Imposs�vel fazer o envio do e-mail sem selecionar uma escola.',
                          '[Sistema Deca] - Erro ao exportar dados para arquivo.',
                          MB_OK + MB_ICONINFORMATION);
end;


end;

procedure TfrmConsultaDadosDoBoletim.cbEscolaCClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := cbEscolaC.Text;
      Open;

      vvEMAIL_ESCOLA := dmDeca.cdsSel_Escola.FieldByName ('dsc_email').Value;

      cbSituacao.ItemIndex  := 0;
      cbDefasagem.ItemIndex := 0;

    end;
  except end;
end;

procedure TfrmConsultaDadosDoBoletim.chkTodasClick(Sender: TObject);
begin
  if chkTodas.Checked then
  begin
    cbUnidadeC.ItemIndex := -1;
    cbUnidadeC.Enabled := False
  end
  else
  begin
    cbUnidadeC.Enabled := True
  end;
end;

procedure TfrmConsultaDadosDoBoletim.chkInibeEscolaClick(Sender: TObject);
begin
  if chkInibeEscola.Checked then
  begin
    cbEscolaC.ItemIndex := -1;
    cbEscolaC.Enabled := False
  end
  else
  begin
    cbEscolaC.Enabled := True
  end;
end;

end.
