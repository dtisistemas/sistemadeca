unit udmTriagemDeca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MConnect, ObjBrkr, Db, DBClient, SConnect;

type
  TdmTriagemDeca = class(TDataModule)
    sckConnTriagemDeca: TSocketConnection;
    SimpleObjectBroker1: TSimpleObjectBroker;
    cdsSel_Par_EstadoCivil: TClientDataSet;
    cdsSel_Par_EstadoCivilcod_estadocivil: TAutoIncField;
    cdsSel_Par_EstadoCivildsc_estadocivil: TStringField;
    cdsSel_Par_Cidades: TClientDataSet;
    cdsSel_Par_Nacionalidade: TClientDataSet;
    cdsSel_Cadastro_Bairro: TClientDataSet;
    cdsSel_Cadastro_Emissor: TClientDataSet;
    cdsSel_Par_Posicao: TClientDataSet;
    cdsSel_Pesquisa_Inscritos_Triagem: TClientDataSet;
    cdsSel_Par_RelResponsabilidade: TClientDataSet;
    cdsSel_Par_Natureza: TClientDataSet;
    cdsSel_Par_TipoHabitacao: TClientDataSet;
    cdsSel_Par_Infraestrutura: TClientDataSet;
    cdsSel_Par_InstalacoesSanitarias: TClientDataSet;
    cdsSel_Par_CondicaoHabitabilidade: TClientDataSet;
    cdsSel_Par_Escolaridade: TClientDataSet;
    cdsIns_Cadastro_TriagemDeca: TClientDataSet;
    cdsSel_Retorna_NIF_Valido: TClientDataSet;
    cdsIns_Cad_Inscricao: TClientDataSet;
    cdsDel_Cad_Inscricao: TClientDataSet;
    cdsSel_Par_Cadastro_Regioes: TClientDataSet;
    cdsSel_CadastroFull: TClientDataSet;
    cdsIns_Par_Cad_Bairro: TClientDataSet;
    cdsUpd_Par_Cad_Bairro: TClientDataSet;
    cdsUpd_Cadastro_TriagemDeca: TClientDataSet;
    cdsSel_PontosFicha: TClientDataSet;
    cdsSel_BeneficiosSociais: TClientDataSet;
    cdsIns_BeneficioSocial: TClientDataSet;
    cdsUpd_BeneficioSocial: TClientDataSet;
    cdsDel_BeneficioSocial: TClientDataSet;
    cdsSel_BeneficiosSociaiscod_idBeneficio: TAutoIncField;
    cdsSel_BeneficiosSociaisdsc_beneficio: TStringField;
    cdsSel_BeneficiosSociaisnum_valorbeneficio: TBCDField;
    cdsFamilia_InsUpdDel: TClientDataSet;
    cdsSel_Par_Parentesco: TClientDataSet;
    cdsdSel_Cadastro_CompFamiliar: TClientDataSet;
    cdsSel_Cadastro_RegAtendimento: TClientDataSet;
    cdsSel_Cadastro_Relatorio: TClientDataSet;
    cdsIns_Cadastro_Relatorio: TClientDataSet;
    cdsUpd_Cadastro_Relatorio: TClientDataSet;
    cdsSel_AtualizaUsuariosDecaRelatorio: TClientDataSet;
    cdsUpd_AtualizaUsuariosDecaRelatorio: TClientDataSet;
    cdsSel_PendenciaDocumentos: TClientDataSet;
    cdsIns_PendenciaDocumentos: TClientDataSet;
    cdsUpd_PendenciaDocumentos: TClientDataSet;
    cdsUpd_AlteraStatusInscricao: TClientDataSet;
    cdsGERA_DEMANDA: TClientDataSet;
    cdsINS_DEMANDA: TClientDataSet;
    cdsUPD_DEMANDA: TClientDataSet;
    cdsDEL_DEMANDA: TClientDataSet;
    cdsSel_DEMANDA: TClientDataSet;
    cdsMIGRACAO_TRIAGEM_DECA: TClientDataSet;
    cdsDel_ComposicaoFamiliar: TClientDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmTriagemDeca: TdmTriagemDeca;

implementation

uses uCadastroInscricoes;

{$R *.DFM}

end.
