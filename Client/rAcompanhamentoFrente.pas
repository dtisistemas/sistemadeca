unit rAcompanhamentoFrente;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg, Db;

type
  TrelAcompanhamentoFrente = class(TQuickRep)
    QRGroup1: TQRGroup;
    QRGroup2: TQRGroup;
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    lbTitulo: TQRLabel;
    QRLabel25: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel12: TQRLabel;
    QRShape3: TQRShape;
    QRLabel15: TQRLabel;
    QRShape4: TQRShape;
    QRLabel14: TQRLabel;
    QRShape7: TQRShape;
    QRLabel18: TQRLabel;
    QRShape10: TQRShape;
    QRLabel19: TQRLabel;
    QRShape8: TQRShape;
    QRLabel20: TQRLabel;
    QRShape9: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape11: TQRShape;
    QRShape21: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape22: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    DetailBand1: TQRBand;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRShape6: TQRShape;
    QRShape26: TQRShape;
    PageFooterBand1: TQRBand;
    QRLabel22: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    lbUnidade: TQRLabel;
    lbGestor: TQRLabel;
    lbAsocial: TQRLabel;
    lbFoneUnidade: TQRLabel;
    lbEmailUnidade: TQRLabel;
    lbEscola: TQRLabel;
    lbDiretor: TQRLabel;
    lbEnderecoEscola: TQRLabel;
    lbBairroEscola: TQRLabel;
    lbFoneEscola: TQRLabel;
    QRDBText1: TQRDBText;
    lbImpressao: TQRLabel;
    QRDBText2: TQRDBText;
    QRShape5: TQRShape;
    QRShape27: TQRShape;
    QRLabel24: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel42: TQRLabel;
  private

  public

  end;

var
  relAcompanhamentoFrente: TrelAcompanhamentoFrente;

implementation

uses uDM;

{$R *.DFM}

end.
