object frmTelaNovasAlteracoes: TfrmTelaNovasAlteracoes
  Left = 167
  Top = 186
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Novas Altera��es de Vers�o]'
  ClientHeight = 216
  ClientWidth = 481
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 1
    Top = 1
    Width = 481
    Height = 45
    BorderStyle = bsSingle
    Caption = 'Altera��es desta Vers�o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 1
    Top = 47
    Width = 480
    Height = 164
    BevelOuter = bvLowered
    TabOrder = 1
    object edNovasAlteracoes: TRichEdit
      Left = 1
      Top = 1
      Width = 478
      Height = 162
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        'Esta vers�o tem as seguintes altera��es:'
        '- Corrigido o problema da emiss�o da frequ�ncia por turma;'
        ''
        ''
        ''
        ''
        ''
        ''
        ''
        'Data desta Vers�o (�ltima): 16/06/2005')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
  end
end
