unit rGeral;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelGeral = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRLabel1: TQRLabel;
    lbUnidade: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    fld_matricula: TQRDBText;
    fld_nome: TQRDBText;
    fld_endereco: TQRDBText;
    fld_bairro: TQRDBText;
    fld_modalidade: TQRDBText;
    lbTotal: TQRLabel;
    QRExpr1: TQRExpr;
    qrlEndereco: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel5: TQRLabel;
  private

  public

  end;

var
  relGeral: TrelGeral;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

end.
