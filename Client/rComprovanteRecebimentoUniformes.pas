unit rComprovanteRecebimentoUniformes;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelComprovanteRecebimentoUniformes = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    qrlUNIDADE: TQRLabel;
    qrlPERIODO: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    DetailBand2: TQRBand;
    SummaryBand1: TQRBand;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    SummaryBand2: TQRBand;
    QRLabel3: TQRLabel;
    QRExpr1: TQRExpr;
    QRMemo1: TQRMemo;
    QRLabel4: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText2: TQRDBText;
    QRShape9: TQRShape;
    QRDBRichText2: TQRDBRichText;
    QRDBText4: TQRDBText;
    qrlRequisicao: TQRLabel;
    QRLabel1: TQRLabel;
    qrlDataRequisicao: TQRLabel;
    QRDBText3: TQRDBText;
  private

  public

  end;

var
  relComprovanteRecebimentoUniformes: TrelComprovanteRecebimentoUniformes;

implementation

uses uDM;

{$R *.DFM}

end.
