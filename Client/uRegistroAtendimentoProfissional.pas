unit uRegistroAtendimentoProfissional;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, Grids, DBGrids, ComCtrls, Db;

type
  TfrmRegistroAtendimentoProfissional = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label3: TLabel;
    txtData: TMaskEdit;
    Label5: TLabel;
    cbClassificacao: TComboBox;
    Label2: TLabel;
    txtOcorrencia: TMemo;
    dbgIndicadoresProfissionais: TDBGrid;
    meDescritivoAtendimento: TMemo;
    Label4: TLabel;
    Label6: TLabel;
    dsIndicadoresProfissionais: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure AtualizaCamposRegProfissional(Modo: String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure cbClassificacaoExit(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure TabSheet2Show(Sender: TObject);
    procedure dbgIndicadoresProfissionaisCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroAtendimentoProfissional: TfrmRegistroAtendimentoProfissional;
  vListaProfissional1, vListaProfissional2 : TStringList;

implementation

uses uDM, uFichaCrianca, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmRegistroAtendimentoProfissional.AtualizaCamposRegProfissional(
  Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    txtData.Clear;
    cbClassificacao.ItemIndex := -1;
    txtOcorrencia.Clear;
    GroupBox2.Enabled := False;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    GroupBox2.Enabled := True;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmRegistroAtendimentoProfissional.FormShow(Sender: TObject);
begin

  AtualizaCamposRegProfissional('Padr�o');
  PageControl1.ActivePageIndex := 0;
  vListaProfissional1 := TStringList.Create();
  vListaProfissional2 := TStringList.Create();
  cbClassificacao.Clear;

  try
    with dmDeca.cdsSel_Classificacao_Atendimento do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
      Params.ParamByName('@pe_flg_crianca').Value := Null;
      Params.ParamByName('@pe_flg_adolescente').Value := Null;
      Params.ParamByName('@pe_flg_familia').Value := Null;
      Params.ParamByName('@pe_flg_profissional').Value := 1;
      Open;

      while not eof do
      begin
        cbClassificacao.Items.Add(FieldByName('dsc_classificacao').AsString);
        vListaProfissional1.Add(IntToStr(FieldByName('cod_id_classificacao').AsInteger));
        vListaProfissional2.Add(FieldByName('dsc_classificacao').AsString);
        Next;
      end;

      //Carrega os atendimentos profissionais realizados pelo usu�rio logado
      with dmDeca.cdsSel_IndicadoresProfissionais do
      begin
        Close;

        if vvIND_PERFIL <> 1 then
          Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO
        else
          Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
        Open;
        
        dbgIndicadoresProfissionais.Refresh;
      end;

    end;
  except end;
end;

procedure TfrmRegistroAtendimentoProfissional.btnNovoClick(
  Sender: TObject);
begin
  AtualizaCamposRegProfissional('Novo');
  txtData.SetFocus;
end;

procedure TfrmRegistroAtendimentoProfissional.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCamposRegProfissional('Padr�o');
end;

procedure TfrmRegistroAtendimentoProfissional.btnSairClick(
  Sender: TObject);
begin
  frmRegistroAtendimentoProfissional.Close;
end;

procedure TfrmRegistroAtendimentoProfissional.txtDataExit(Sender: TObject);
begin
  try
    StrToDate(txtData.Text);
    if StrToDate(txtData.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtData.SetFocus;
    end;
  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtData.SetFocus;
    end;
  end;
end;

procedure TfrmRegistroAtendimentoProfissional.cbClassificacaoExit(
  Sender: TObject);
begin
  if cbClassificacao.ItemIndex < 0 then
  begin
    ShowMessage ('O Campo de classifica��o deve ser selecionado...');
    cbClassificacao.SetFocus;
  end;
end;

procedure TfrmRegistroAtendimentoProfissional.btnSalvarClick(
  Sender: TObject);
begin
  //Valida o preenchimento dos campos obrigat�rios
  if (Length(txtData.Text)<10) or (cbClassificacao.ItemIndex = -1) or (Length(txtOcorrencia.Text)<1) then
  begin
    ShowMessage('Os campos marcados na cor azul s�o de preenchimento obrigat�rio...');
  end
  else
  begin
    //Grava os dados no Indicador
    try
      with dmDeca.cdsInc_Indicador do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_classificacao').Value := StrToInt(vListaProfissional1.Strings[cbClassificacao.ItemIndex]);
        Params.ParamByName('@pe_ind_tipo_classificacao').Value := 3; //Profissional
        Params.ParamByName('@pe_dat_registro').Value := GetValue(txtData, vtDate);
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
        Execute;
      end;
    except end;

    with dmDeca.cdsSel_IndicadoresProfissionais do
    begin
      Close;

      if vvIND_PERFIL <> 1 then
        Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO
      else
        Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;

      dbgIndicadoresProfissionais.Refresh;
    end;
    AtualizaCamposRegProfissional('Padr�o');
  end;
end;

procedure TfrmRegistroAtendimentoProfissional.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmRegistroAtendimentoProfissional.Close;
  end;
end;

procedure TfrmRegistroAtendimentoProfissional.TabSheet2Show(
  Sender: TObject);
begin
  //Habilita os bot�es cancelar e alterar somente  
end;

procedure TfrmRegistroAtendimentoProfissional.dbgIndicadoresProfissionaisCellClick(
  Column: TColumn);
begin
  meDescritivoAtendimento.Text := dmDeca.cdsSel_IndicadoresProfissionais.FieldByName('dsc_ocorrencia').Value;
end;

end.
