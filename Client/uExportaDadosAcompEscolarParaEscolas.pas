unit uExportaDadosAcompEscolarParaEscolas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Db, DBCtrls, jpeg, ExtCtrls, CheckLst, OleServer,
  Excel97;

type
  TfrmExportaDadosAcompEscolarParaEscolas = class(TForm)
    GroupBox1: TGroupBox;
    dsBoletim: TDataSource;
    btnGerar: TBitBtn;
    btnCancelar: TBitBtn;
    Image1: TImage;
    clbEscolas: TCheckListBox;
    ExcelApplication1: TExcelApplication;
    procedure btnGerarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmExportaDadosAcompEscolarParaEscolas: TfrmExportaDadosAcompEscolarParaEscolas;
  Planilha : Olevariant;
  lin, col : integer;

implementation

uses uConsultaAcompEscolar, uEnviarEmail, ComObj, uDM;

{$R *.DFM}

procedure TfrmExportaDadosAcompEscolarParaEscolas.btnGerarClick(
  Sender: TObject);
begin

{
  //Tratar exporta��o para as planilhas de cada escola selecionada
  //no checklist clbEscolas...

  //Cria o aplicativo Excel, inicializa par�metros vazios, aplica��o vis�vel e utiliza p�gina 1 e aba 1
  ExcelApplication1 := TExcelApplication.Create(nil);
  ExcelApplication1.Workbooks.Add(EmptyParam, 0);
  ExcelApplication1.Visible[0] := False;

  Planilha := ExcelApplication1.Workbooks[1].Worksheets[1];

  //Colocar o nome do campo em cada coluna
  for col := 1 to dmDeca.cdsSel_Boletim.FieldCount do
  begin
    //Planilha.Cells[1, col].Select;
    Planilha.Cells[1, col].Font.Bold := True;
    Planilha.Cells[1, col].Value := dmDeca.cdsSel_Boletim.Fields[col-1].DisplayLabel;
  end;

  //Inicializa a segunda linha
  lin := 2;

  //Repeti��o onde vai inserir os dados da tabela at� que seja final de arquivo
  while not (dmDeca.cdsSel_Boletim.eof) do
  begin

    for col := 1 to dmDeca.cdsSel_Boletim.FieldCount do
    begin
      //Planilha.Cells[lin, col].Select;
      Planilha.Cells[lin, col].Value := dmDeca.cdsSel_Boletim.Fields[col-1].AsString;
    end;
    inc(lin);
    dmDeca.cdsSel_Boletim.Next;
  end;
  ShowMessage ('Finalizado...');

  //ExcelApplication1.Save
  ExcelApplication1.Free;

  Application.CreateForm (TfrmEnviarEmail, frmEnviarEmail);
  frmEnviarEmail.edEmailPara.Text := 'Endere�o de e-mail da escola...';
  frmEnviarEmail.ShowModal;
  }
end;

procedure TfrmExportaDadosAcompEscolarParaEscolas.FormShow(
  Sender: TObject);
begin
  Application.MessageBox ('Aten��o !!! A tela de exporta��o ser� exibida agora.' + #13+#10+
                          'Selecione a(s) escola(s) e clique em Gerar ' +#13+#10+
                          'para que os dados sejam exportados para as devidas planihas',
                          '[Sistema Deca] - Exporta��o dos Dados',
                          MB_OK + MB_ICONINFORMATION);
end;

end.
