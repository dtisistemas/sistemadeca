unit uFichaCadastroNovo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ComCtrls;

type
  TfrmFichaCadastroNovo = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    txtMatricula: TMaskEdit;
    txtProntuario: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    edNumRGEscolarC: TEdit;
    Label4: TLabel;
    txtNome: TEdit;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    txtAdmissao: TMaskEdit;
    txtDtCadastro: TMaskEdit;
    Label7: TLabel;
    txtEndereco: TEdit;
    btnAlteraEndereco: TSpeedButton;
    Label8: TLabel;
    txtComplemento: TEdit;
    Label9: TLabel;
    txtBairro: TEdit;
    Label10: TLabel;
    txtCep: TMaskEdit;
    Label11: TLabel;
    txtNascimento: TMaskEdit;
    lbIdade: TLabel;
    Label12: TLabel;
    cbSexoCrianca: TComboBox;
    Label13: TLabel;
    txtTelefone: TMaskEdit;
    Label14: TLabel;
    MaskEdit1: TMaskEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFichaCadastroNovo: TfrmFichaCadastroNovo;

implementation

{$R *.DFM}

end.
