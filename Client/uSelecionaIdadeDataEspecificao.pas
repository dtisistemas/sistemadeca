unit uSelecionaIdadeDataEspecificao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, ExtCtrls, Buttons, ComCtrls, QRExport;

type
  TfrmSelecionaIdadeDataEspecifica = class(TForm)
    btnImprimir: TBitBtn;
    Panel1: TPanel;
    Label3: TLabel;
    Label1: TLabel;
    cbIdades: TComboBox;
    optTodasIdades: TCheckBox;
    lbMensagem: TLabel;
    edData: TMaskEdit;
    meRelacao: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cbIdadesExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure optTodasIdadesClick(Sender: TObject);
    procedure cbIdadesClick(Sender: TObject);
    procedure dtpDataExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edDataExit(Sender: TObject);
    function mBissexto(AYear:Integer): Boolean;
    function mDiasDoMes(AYear,AMonth:Integer):Integer;
    function mIdade2(DataNas:TDate):String;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaIdadeDataEspecifica: TfrmSelecionaIdadeDataEspecifica;

implementation

uses uDM, rIdades, uPrincipal, uFichaCrianca, uUtil;

{$R *.DFM}

procedure TfrmSelecionaIdadeDataEspecifica.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSelecionaIdadeDataEspecifica.FormShow(Sender: TObject);
begin
  //dtpData.Date := Date();
  edData.Text := DateToSTr(Date());
  cbIdades.ItemIndex := -1;
  optTodasIdades.Checked := False;
  btnImprimir.Enabled := False;
  lbMensagem.Caption := '';
end;

procedure TfrmSelecionaIdadeDataEspecifica.cbIdadesExit(Sender: TObject);
begin
  if (cbIdades.ItemIndex = -1) then
  begin
    ShowMessage ('Favor selecionar uma IDADE espec�fica ou clicar em "Todas as Idades"');
  end
  else if (cbIdades.ItemIndex > -1) then
  begin
    btnImprimir.Enabled := True;
  end;
end;

procedure TfrmSelecionaIdadeDataEspecifica.btnImprimirClick(
  Sender: TObject);
begin
  //Se selcionar uma determinada IDADE no Combo
  if (optTodasIdades.Checked = False) then
  begin
    //Seleciona de IDADES, a idade expressa no Combo
    with dmDeca.cdsSel_Idades do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
      Params.ParamByName ('@pe_idade').AsInteger := StrToInt(cbIdades.Text);
      Open;

      //Exibe o relat�rio de c�as/adolescentes e suas idades com par�metro da data selecionada no DateTimePicker
      Application.CreateForm (TrelIdades, relIdades);
      relIdades.lbTitulo.Caption := 'RELA��O DE C�AS/ADOLESCENTES POR IDADE  -  ' + cbIdades.Text + ' anos     -     Data refer�ncia: ' + edData.Text;
      relIdades.lbUnidade.Caption := vvNOM_UNIDADE;
      //relIdades.QRGroup1.Enabled := False;
      relIdades.Preview;
      relIdades.Free;

      //relIdades.ExportToFilter(TQRAsciiExportFilter.Create('c:\Deca\Idades.doc'));

    end
  end
  //Se clicar em Todas as Idades
  else if (optTodasIdades.Checked = True) then
  begin
    with dmDeca.cdsSel_Idades do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
      Params.ParamByName ('@pe_idade').Value := Null;
      Open;

      //Exibe o relat�rio de c�as/adolescentes e suas idades com par�metro da data selecionada no DateTimePicker
      Application.CreateForm (TrelIdades, relIdades);
      relIdades.lbTitulo.Caption := 'RELA��O DE C�AS/ADOLESCENTES POR IDADE  -  Todas as IDADES     -     Data refer�ncia: ' + edData.Text;
      relIdades.lbUnidade.Caption := vvNOM_UNIDADE;
      //relIdades.QRGroup1.Enabled := True;
      relIdades.Preview;
      relIdades.Free;

      //relIdades.ExportToFilter(TQRAsciiExportFilter.Create('c:\Deca\Idades.doc'));
                                        
  end;
end;
end;

procedure TfrmSelecionaIdadeDataEspecifica.optTodasIdadesClick(
  Sender: TObject);
begin
  btnImprimir.Enabled := True;
  if optTodasIdades.Checked = True then cbIdades.Enabled := True
  else optTodasIdades.Checked := False;

end;

procedure TfrmSelecionaIdadeDataEspecifica.cbIdadesClick(Sender: TObject);
begin
  btnImprimir.Enabled := True;
end;

procedure TfrmSelecionaIdadeDataEspecifica.dtpDataExit(Sender: TObject);
var
  m_Periodo : Integer;
  m_intContAnos, m_intContMeses : Integer ;
  m_DataFinal : TDateTime;
begin
  cbIdades.Clear;
  lbMensagem.Caption := 'Aguarde... Calculando idades...';
  //Excluir dados de idade j� gravados anetriormente para o usu�rio
  with dmDeca.cdsExc_Idades do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
    Execute;
  end;

  // Selecionar as c�as/adolescentes da unidade, calcular a idade e gravar na tabela idades
  with dmDeca.cdsSel_Cadastro do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;

    while not eof do
    begin
      //Calcula a Idade
      //Anos
      m_intContAnos := 0 ;
      m_Periodo := 12 ;
      //m_DataFinal := dtpData.Date ;
      m_DataFinal := Date();
      Repeat
        Inc(m_intContAnos) ;
        m_DataFinal := IncMonth(m_DataFinal,m_Periodo * -1) ;
      Until (m_DataFinal <= dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime);

      Inc(m_intContAnos,-1) ;

      //Meses
      m_intContMeses := 0 ;
      m_Periodo := 1 ;
      //m_DataFinal := dtpData.Date ;
      m_DataFinal := Date();

      Repeat
        Inc(m_intContMeses) ;
        m_DataFinal := IncMonth(m_DataFinal,m_Periodo * -1) ;
      Until (m_DataFinal <= dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime);

      //DataFinal := IncMonth(DataFinal,Periodo) ;
      Inc(m_intContMeses,-1) ;
      m_intContMeses := m_intContMeses mod 12;
      //Result := IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses';

      //Insere os dados na tabela
      with dmDeca.cdsInc_Idades do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').AsString := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;
        Params.ParamByName ('@pe_nom_nome').AsString := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString;
        Params.ParamByName ('@pe_dat_nascimento').AsDateTime := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime;
        Params.ParamByName ('@pe_idade_anos').AsInteger := m_intContAnos;
        Params.ParamByName ('@pe_idade_meses').AsInteger := m_intContMeses;
        Params.ParamByName ('@pe_periodo').Value := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').AsString;
        Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
        Execute;
      end;

      Next;

    end;

  end;

  //Seleciona as idades na tabela atrav�s do usu�rio logado e carrega as idades no combo
  with dmDeca.cdsSel_Idades2 do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
    Params.ParamByName ('@pe_idade').Value := Null;
    Open;

    //cbIdades.Clear;
    while not dmDeca.cdsSel_Idades2.eof do
    begin
      cbIdades.Items.Add (IntToStr(dmDeca.cdsSel_Idades2.FieldByName ('idade_anos').AsInteger));
      dmDeca.cdsSel_Idades2.Next;
    end;
  end;
  cbIdades.SetFocus;
  lbMensagem.Caption := '';
end;

procedure TfrmSelecionaIdadeDataEspecifica.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmSelecionaIdadeDataespecifica.Close;
  end;
end;

procedure TfrmSelecionaIdadeDataEspecifica.edDataExit(Sender: TObject);
var
  m_Periodo : Integer;
  m_intContAnos, m_intContMeses : Integer ;
  m_DataFinal : TDateTime;
begin

  cbIdades.Clear;
  lbMensagem.Caption := 'Aguarde... Calculando idades...';

  //Excluir dados de idade j� gravados anteriormente para o usu�rio
  with dmDeca.cdsExc_Idades do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
    Execute;
  end;

  // Selecionar as c�as/adolescentes da unidade, calcular a idade e gravar na tabela idades
  with dmDeca.cdsSel_Cadastro do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Open;

    //Calcula a Idade para todas as c�as/adol. da unidade "logada"
    meRelacao.Lines.Clear;
    while not dmDeca.cdsSel_Cadastro.eof do
    begin
      meRelacao.Lines.Add(mIdade2(StrToDate(edData.Text)));
      Next;
    end;

  end;

  //Seleciona as idades na tabela atrav�s do usu�rio logado e carrega as idades no combo
  with dmDeca.cdsSel_Idades2 do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
    Params.ParamByName ('@pe_idade').Value := Null;
    Open;

    cbIdades.Clear;
    while not dmDeca.cdsSel_Idades2.eof do
    begin
      cbIdades.Items.Add (IntToStr(dmDeca.cdsSel_Idades2.FieldByName ('idade_anos').AsInteger));
      dmDeca.cdsSel_Idades2.Next;
    end;
  end;
  cbIdades.SetFocus;
  lbMensagem.Caption := '';

end;

function TfrmSelecionaIdadeDataEspecifica.mBissexto(
  AYear: Integer): Boolean;
begin
  //Esta fun��o verifica se o ano � Bissexto ou n�o
  Result := (AYear mod 4 = 0) and ((AYear mod 100 <> 0) or (AYear mod 400 = 0));
end;

function TfrmSelecionaIdadeDataEspecifica.mDiasDoMes(AYear,
  AMonth: Integer): Integer;
const
  DaysInMonth : array [1..12] of Integer = (31,28,31,30,31,30,31,31,30,31,30,31);
begin
  Result := DaysInMonth[AMonth];
  if (Amonth = 2) and mBissexto(AYear) then Inc(Result);
end;

function TfrmSelecionaIdadeDataEspecifica.mIdade2(DataNas: TDate): String;
var   idadea,idadem,ano1,ano,mes1,mes,dia1,dia: integer;
  {Ano1, Mes1, Dia1 : Word;
  Ano2, Mes2, Dia2 : Word;
  Ano,  Mes,  Dia  : Word;
  Idade : String;
  AuxDia1, AuxDia2 : Integer; }
begin

  {
  Idade := '0';
  //DecodeDate (DataNas, Ano1, Mes1, Dia1);

  DecodeDate (dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime, Ano1, Mes1, Dia1);
  //DecodeDate (Date, Ano2, Mes2, Dia2);
  DecodeDate (StrToDate(edData.Text), Ano2, Mes2, Dia2);

  AuxDia1 := Dia1;
  AuxDia2 := Dia2;

  if (Dia1 > Dia2) and ((Mes2 - Mes1) = 1) then
  begin
    Dia2 := Dia2 + mDiasDoMes(Ano1, Mes1);
    Mes1 := Mes2;
  end
  else if (Dia1 > Dia2) and (Mes1 <> Mes2) then
  begin
    Dia2 := Dia2 + mDiasDoMes(Ano1, Mes1);
  end
  else if (Mes1 = Mes2) and (Dia1 > Dia2) and (Ano1 <> Ano2) then
  begin
    Dia2 := Dia2 + mDiasDoMes(Ano1, Mes1);
    Mes1 := Mes1 + 11;
    Ano1 := Ano1 + 1;
  end;

  if (Mes1 > Mes2) and (AuxDia1 <= AuxDia2) then
  begin
    Ano1 := Ano1 + 1;
    Mes2 := Mes2 + 12;
  end
  else if (Mes1 > Mes2) and (AuxDia1 > AuxDia2) then
  begin
    Ano1 := Ano1 + 1;
    Mes2 := Mes1 + 11;
  end;

  Ano := Ano2 - Ano1;
  Mes := Mes2 - Mes1;
  Dia := Dia2 - Dia1;

  if Ano > 1 then Idade := Idade + ', ' + IntToStr(Ano) + ' anos'
  else if Ano = 1 then Idade := Idade + ', ' + IntToStr(Ano) + ' ano';

  if Mes > 1 then Idade := Idade + ', ' + IntToStr(Mes) + ' meses'
  else if Mes <> 0 then Idade := Idade + ', ' + IntToStr(Mes) + ' mes';

  if Ano = 0 then Delete(Idade,1,1);

  if Dia > 1 then Idade := Idade + ' e ' + IntToStr(Dia) + ' dias'
  else if Dia <> 0 then Idade := Idade + ' e ' + IntToStr(Dia) + ' dia';

  if (Mes = 0) and (Ano = 0) then Delete(Idade,1,3);

  if (Ano1 = Ano2) and (Mes1 = Mes2) and (Dia1 > Dia2) then Idade := '0';

  Result := Idade;}
   dia1:=strtoint(copy(datetostr(dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime),0,2));
   mes1:=strtoint(copy(datetostr(dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime),4,2));
   Ano1:=strtoint(copy(datetostr(dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime),7,4));
   dia:=strtoint(copy(datetostr(DataNas),0,2));
   mes:=strtoint(copy(datetostr(DataNas),4,2));
   Ano:=strtoint(copy(datetostr(DataNas),7,4));
   if mes1<mes then
   begin

     if dia1<=dia then
     begin

          idadea:=ano - ano1;
          idadem :=mes - mes1 ;
     end
     else
     begin

          idadea:=ano - ano1 ;
          idadem:=mes - mes1 - 1;
     end;


   end
   else
   begin
      if mes1 = mes then
      begin
          if dia1<=dia then
          begin

             idadea:=ano - ano1;
             idadem :=mes - mes1; // tem k dar 0
          end
          else
          begin

             idadea:=ano - ano1 -1;
             idadem:=mes - mes1 - 1;
             idadem:=12 + idadem;
          end;

      end
      else
      begin
        if dia1<=dia then
          begin

             idadea:=ano - ano1 - 1;
             idadem :=mes - mes1;
             idadem:=12 + idadem;
          end
          else
          begin

             idadea:=ano - ano1 -1;
             idadem:=mes - mes1 - 1;
             idadem:=12 + idadem;
          end;
      end;

   end;

   result:= inttostr(idadea)+' anos e '+inttostr(idadem)+' meses';


  with dmDeca.cdsInc_Idades do
  begin
    Close;
    Params.ParamByName ('@pe_cod_matricula').Value       := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString);
    Params.ParamByName ('@pe_nom_nome').Value            := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString);
    Params.ParamByName ('@pe_dat_nascimento').Value      := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsDateTime;
    Params.ParamByName ('@pe_idade_anos').AsInteger      := Idadea;
    Params.ParamByName ('@pe_idade_meses').AsInteger     := Idadem;
    Params.ParamByName ('@pe_periodo').AsString          := dmDeca.cdsSel_Cadastro.FieldByName ('dsc_periodo').Value;
    Params.ParamByName ('@pe_cod_usuario').Value         := vvCOD_USUARIO;

    meRelacao.lines.add (dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').Value + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').Value);

    Execute;
  end;
end;

end.
