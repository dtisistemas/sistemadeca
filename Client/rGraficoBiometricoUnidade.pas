unit rGraficoBiometricoUnidade;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, TeEngine, Series, TeeProcs,
  Chart, DBChart, QrTee, jpeg;

type
  TrelGraficoBiometricoUnidade = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    lbUnidade: TQRLabel;
    PageFooterBand1: TQRBand;
    qrGrafico: TQRChart;
    QRDBChart1: TQRDBChart;
    Series1: TBarSeries;
    DetailBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel4: TQRLabel;
    QRShape1: TQRShape;
    QRSysData2: TQRSysData;
    QRLabel5: TQRLabel;
  private

  public

  end;

var
  relGraficoBiometricoUnidade: TrelGraficoBiometricoUnidade;

implementation

uses uDM;

{$R *.DFM}

end.
