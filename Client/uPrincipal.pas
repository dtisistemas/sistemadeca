unit uPrincipal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, StdCtrls, ExtCtrls, jpeg, ComCtrls, Buttons, ToolWin, ScktComp,
  ImgList, WinInet, Db, Grids, DBGrids, ShellAPI, Psock, NMsmtp;

type
  TfrmPrincipal = class(TForm)
    mmPrincipal: TMainMenu;
    Cadastro1: TMenuItem;
    Usuarios1: TMenuItem;
    Unidades1: TMenuItem;
    CrianasAdolescentes1: TMenuItem;
    N2: TMenuItem;
    Encerrar1: TMenuItem;
    Movimentao1: TMenuItem;
    Escolas1: TMenuItem;
    N3: TMenuItem;
    Relatrio1: TMenuItem;
    Grfico1: TMenuItem;
    Transferncia1: TMenuItem;
    Advertncia1: TMenuItem;
    Desligamento1: TMenuItem;
    N1: TMenuItem;
    RegistrodeAtendimento1: TMenuItem;
    N4: TMenuItem;
    Encaminhamento1: TMenuItem;
    Confirmao1: TMenuItem;
    Apontamentodef1: TMenuItem;
    StatusBar1: TStatusBar;
    Afastamento1: TMenuItem;
    Suspenso1: TMenuItem;
    N5: TMenuItem;
    TransfernciaparaConvnio1: TMenuItem;
    Encaminhamento2: TMenuItem;
    Retorno1: TMenuItem;
    Evolu1: TMenuItem;
    N6: TMenuItem;
    ComplementaoemAlfabetizao1: TMenuItem;
    MudanadeMatrcula1: TMenuItem;
    Entrada1: TMenuItem;
    Retorno2: TMenuItem;
    Encaminhamento3: TMenuItem;
    Retorno3: TMenuItem;
    N7: TMenuItem;
    Histrico1: TMenuItem;
    ListadePresenaReuniodePais1: TMenuItem;
    RelaodeCrianasAdolescentes1: TMenuItem;
    OrdenadosporMatrcula1: TMenuItem;
    OrdenadosporNome1: TMenuItem;
    doPETI1: TMenuItem;
    RelaodeAdvertncias1: TMenuItem;
    RelaodeAfastamentos1: TMenuItem;
    RelaodeDesligados1: TMenuItem;
    RelaodeSuspenses1: TMenuItem;
    RelaodeTransferidos1: TMenuItem;
    RegistrodeAtendimento2: TMenuItem;
    CrianasAdolescentesporIdade1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    RelaodeCrianasAdolescentesporUnidade1: TMenuItem;
    N11: TMenuItem;
    RegistrodeAtendimento3: TMenuItem;
    EvoluoServioSocial1: TMenuItem;
    IndicadoresdoServioSocial1: TMenuItem;
    N12: TMenuItem;
    ResumoMensal1: TMenuItem;
    DadosEscolaresdasCrianasAdolescentes1: TMenuItem;
    OrdenadosporCEP1: TMenuItem;
    NovosLanamentos1: TMenuItem;
    ConsultaAlterao1: TMenuItem;
    TotaldeCrianasAdolescentes1: TMenuItem;
    N13: TMenuItem;
    ConfirmaodeRecebimentoTransferncias1: TMenuItem;
    IdadeEspecfica1: TMenuItem;
    NovosLanamentos2: TMenuItem;
    AlteraesdeDados1: TMenuItem;
    ControledeFrequncias1: TMenuItem;
    InicializarPresena1: TMenuItem;
    FolhadeFrequncia1: TMenuItem;
    N14: TMenuItem;
    RegistrodeVisitaTcnica1: TMenuItem;
    LanamentodeAvaliaoBiomtrica1: TMenuItem;
    LanamentodeVisitaTcnica1: TMenuItem;
    ConsultaDadosdeVisitaTcnica1: TMenuItem;
    N15: TMenuItem;
    AcompanhamentoEscolar1: TMenuItem;
    InicializarBimestre1: TMenuItem;
    ConsultarAlterarDados1: TMenuItem;
    EmitirAcompanhamentoEscolar1: TMenuItem;
    Sair1: TMenuItem;
    AdolescentesConvnioEmpresa1: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    Image2: TImage;
    Image3: TImage;
    Image5: TImage;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton9: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    ToolButton22: TToolButton;
    ToolButton23: TToolButton;
    ToolButton24: TToolButton;
    ToolButton25: TToolButton;
    ToolButton26: TToolButton;
    ToolButton27: TToolButton;
    ToolButton28: TToolButton;
    ToolButton29: TToolButton;
    ToolButton30: TToolButton;
    ToolButton31: TToolButton;
    DadosparaDRH1: TMenuItem;
    GerarArquivodeFaltas1: TMenuItem;
    ControledeVerses1: TMenuItem;
    RelaodeTransferidosporPerodo1: TMenuItem;
    N16: TMenuItem;
    PesquisaGenrica1: TMenuItem;
    DadosdaVerso1: TMenuItem;
    UsuriosConectados1: TMenuItem;
    RelatriodeEncaminhamento1: TMenuItem;
    AcompanhamentoPsicopedaggicoPsicolgico1: TMenuItem;
    RegistrodeObservaesdeAprendizagem1: TMenuItem;
    Perfis1: TMenuItem;
    RelaodeTotalporUnidade1: TMenuItem;
    FatoresdeInterveno1: TMenuItem;
    N17: TMenuItem;
    MonitoramentodeAvaliaoAcompFmiliar1: TMenuItem;
    LanamentosporFatoresdeInterveno1: TMenuItem;
    AcompanhamentodosLanamentos1: TMenuItem;
    ProgramasSociais1: TMenuItem;
    ControledeCalendrios1: TMenuItem;
    NutrioUnidades1: TMenuItem;
    CrianaAdolescente1: TMenuItem;
    Famlia1: TMenuItem;
    Profissional1: TMenuItem;
    RelaodeProgramasSociaisSituao1: TMenuItem;
    Profissional2: TMenuItem;
    EstatsticasdeAtendimento1: TMenuItem;
    FichaClnicaOdontolgica1: TMenuItem;
    OcorrnciasDiversas1: TMenuItem;
    LanarAvaliao1: TMenuItem;
    ReclassificarIMC1: TMenuItem;
    AcompanhamentoBiomtrico1: TMenuItem;
    AvaliaodeDesempenhoAprendiz1: TMenuItem;
    FaltasTeste1: TMenuItem;
    RelaodeEmpresasporAsSocialConvnio1: TMenuItem;
    CadastrodeEscolas1: TMenuItem;
    CadastrodeSriesEscolares1: TMenuItem;
    CadastrodeSriesxEscolas1: TMenuItem;
    TotalizaodeUsurioxUnidade1: TMenuItem;
    RelaodeTransferidosAcessoConvnio1: TMenuItem;
    dsCadastro: TDataSource;
    Chat1: TMenuItem;
    SatisfaodoClienteConveniados1: TMenuItem;
    TabeladeDefasagemEscolar1: TMenuItem;
    GerenciarSolicitaesdeChefia1: TMenuItem;
    imgPendencia: TImage;
    lbMensagemPendencias: TLabel;
    mmBlank: TMainMenu;
    VerificarPendncias1: TMenuItem;
    CordeFundo1: TMenuItem;
    ColorDialog1: TColorDialog;
    AlteraesdaVerso1: TMenuItem;
    PopupMenu1: TPopupMenu;
    ImportaoDadosAcompEscolar1: TMenuItem;
    RelaoCompara42008x120091: TMenuItem;
    ConsultaDadosAcompanhamentoEscolar1: TMenuItem;
    RelaodeAlunoscomDefasagemEscolar1: TMenuItem;
    N18: TMenuItem;
    ALUNOSSEMDADOSNOACOMPESCOLAR1: TMenuItem;
    CONTROLEDOACOMPESCOLARBOLETIM1: TMenuItem;
    CorrigeHistoricoEscola1: TMenuItem;
    RELATRIODEAPROVEITAMENTOINDIVIDUAL1: TMenuItem;
    Panel1: TPanel;
    ProgressBar1: TProgressBar;
    Label1: TLabel;
    N19: TMenuItem;
    ACOMPANHAMENTOESCOLAR2: TMenuItem;
    CONSULTAS1: TMenuItem;
    BOLETIMELETRNICO1: TMenuItem;
    SOLICITAODETRANSFPERODOESCOLAR1: TMenuItem;
    ESTATSTICASGERAIS1: TMenuItem;
    DadosEducao1: TMenuItem;
    IMPORTARCOTASDEPASSE1: TMenuItem;
    FerramentasAdministrativas1: TMenuItem;
    IMPORTAOCOTASPASSE1: TMenuItem;
    CONFEREDADOSDECAxDRH1: TMenuItem;
    CONFEREDADOSDRHxDECA1: TMenuItem;
    FAZERLOGOFF1: TMenuItem;
    SAIR2: TMenuItem;
    RESUMODADOSMATRIZ1: TMenuItem;
    ALTERARSENHA1: TMenuItem;
    Image4: TImage;
    MATRIZDEAVALIAOSEMESTRALANUAL1: TMenuItem;
    ENVIARDADOSESCOLA1: TMenuItem;
    VERIFICARAGUARDANDOINFORMAO1: TMenuItem;
    IMPORTAOACOMPESCOLAR201020111: TMenuItem;
    IMPORTAHISTORICO20111: TMenuItem;
    MonitoramentoGeral1: TMenuItem;
    FECHAMENTO1: TMenuItem;
    TOTALIZAOPORUNIDADEESITUAODS1: TMenuItem;
    LANAMENTODEDADOS1: TMenuItem;
    GERAODERELATRIO1: TMenuItem;
    Image1: TImage;
    ALUNOSEJATELESSALA1: TMenuItem;
    Importar32011120121: TMenuItem;
    GERENCIADOR1: TMenuItem;
    QUANTIDADEDEFICHASPORUNIDADEDIVISO1: TMenuItem;
    QUANTIDADEDEFICHASCADASTRADASPORANO1: TMenuItem;
    QUANTIDADEDEFICHASPORPERODO1: TMenuItem;
    N20: TMenuItem;
    TRANSFERNCIADEBANCOINATIVOS1: TMenuItem;
    ADITAMENTODEAPRENDIUZ1: TMenuItem;
    ADITAMENTODECONTRATOAPRENDIZ1: TMenuItem;
    COMPARATIVO1: TMenuItem;
    COMPARARBIMESTRES1: TMenuItem;
    Memo1: TMemo;
    FICHACLNICAODONTOLGICA2: TMenuItem;
    CONTROLEDEMATERIAISODONTOLGICOS1: TMenuItem;
    TransfernciaColetiva1: TMenuItem;
    MONITAMENTOMUTIRO1: TMenuItem;
    N21: TMenuItem;
    SOLICITAODEUNIFORMES1: TMenuItem;
    N22: TMenuItem;
    PROCESSOADMISSIONALAPRENDIZAGEM1: TMenuItem;
    RELATRIOSEDUCACENSO1: TMenuItem;
    EMISSOTERMODECOMPROMISSOBOLSISTAS1: TMenuItem;
    PASSESEVALESTRANSPORTE1: TMenuItem;
    CARTOPASSECOTASEVTS1: TMenuItem;
    TARIFASDEPASSESESCOLARES1: TMenuItem;
    IMPORTARDADOSEDUCACENSO1: TMenuItem;
    REATIVAODEPRONTURIO1: TMenuItem;
    TRANSFERNCIASPORPERODO1: TMenuItem;
    N23: TMenuItem;
    CURSOSECERTIFICADOS1: TMenuItem;
    ImportarDadosdoEducasenso1: TMenuItem;
    IMPORTARDADOSEDUCASENSO1: TMenuItem;
    EMITIRFICHADECADASTRO1: TMenuItem;
    VISUALIZARAGENDA1: TMenuItem;
    N24: TMenuItem;
    MIGRARRGECPFCOMPFAMILIAR1: TMenuItem;
    UNIDADESESCOLARESSERIESESCOLARESETURMAS1: TMenuItem;
    CADASTROS1: TMenuItem;
    RELATRIODECLASSES1: TMenuItem;
    RELAODEMUDANASDEVNCULO1: TMenuItem;
    TRANSFERNCIACOLETIVA2: TMenuItem;
    REATIVAESPORPERODO1: TMenuItem;
    mensal1: TMenuItem;
    anual1: TMenuItem;
    RELATRIODEDEMANDATRIAGEM20171: TMenuItem;
    RELATRIOSESTATSTICOS1: TMenuItem;
    LEVANTAMENTODENDEATENDIDOSPORPERODO1: TMenuItem;
    TRIAGEM2: TMenuItem;
    FICHADEINSCRIO2: TMenuItem;
    ADMISSO1: TMenuItem;
    PROCESSOADMINISTRATIVO1: TMenuItem;
    EFETIVARADMISSO1: TMenuItem;
    PORTARIADEINSCRIOSELEOEADMISSO1: TMenuItem;
    CADASTRODEBAIRROS1: TMenuItem;
    CorrigircampoCPFCadastroTriagem1: TMenuItem;
    MIGRARATENDIMENTOS1: TMenuItem;
    GERARDADOSDEMANDA1: TMenuItem;
    SEGUNDAVIADECONTRATOFICHA1: TMenuItem;
    procedure Encerrar1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Usuarios1Click(Sender: TObject);
    procedure CrianasAdolescentes1Click(Sender: TObject);
    procedure Advertncia1Click(Sender: TObject);
    procedure Suspenso1Click(Sender: TObject);
    procedure Desligamento1Click(Sender: TObject);
    procedure Entrada1Click(Sender: TObject);
    procedure Retorno2Click(Sender: TObject);
    procedure Encaminhamento1Click(Sender: TObject);
    procedure ListadePresenaReuniodePais1Click(Sender: TObject);
    procedure Encaminhamento3Click(Sender: TObject);
    procedure Retorno3Click(Sender: TObject);
    procedure Histrico1Click(Sender: TObject);
    procedure doPETI1Click(Sender: TObject);
    procedure OrdenadosporMatrcula1Click(Sender: TObject);
    procedure RelaodeCrianasAdolescentesporUnidade1Click(Sender: TObject);
    procedure OrdenadosporNome1Click(Sender: TObject);
    procedure OrdenadosporCEP1Click(Sender: TObject);
    procedure RelaodeAfastamentos1Click(Sender: TObject);
    procedure RelaodeAdvertncias1Click(Sender: TObject);
    procedure RelaodeDesligados1Click(Sender: TObject);
    procedure RelaodeSuspenses1Click(Sender: TObject);
    procedure RegistrodeAtendimento2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Confirmao1Click(Sender: TObject);
    procedure Encaminhamento2Click(Sender: TObject);
    procedure Retorno1Click(Sender: TObject);
    procedure RelaodeTransferidos1Click(Sender: TObject);
    procedure MudanadeMatrcula1Click(Sender: TObject);
    procedure RegistrodeAtendimento3Click(Sender: TObject);
    procedure EvoluoServioSocial1Click(Sender: TObject);
    procedure IndicadoresdoServioSocial1Click(Sender: TObject);
    procedure NovosLanamentos1Click(Sender: TObject);
    procedure ConsultaAlterao1Click(Sender: TObject);
    procedure TotaldeCrianasAdolescentes1Click(Sender: TObject);
    procedure ConfirmaodeRecebimentoTransferncias1Click(Sender: TObject);
    procedure IdadeEspecfica1Click(Sender: TObject);
    procedure NovosLanamentos2Click(Sender: TObject);
    procedure AlteraesdeDados1Click(Sender: TObject);
    procedure InicializarPresena1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure btnCadastroClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PorTurmas1Click(Sender: TObject);
    procedure PorMatrcula1Click(Sender: TObject);
    procedure PorTurma1Click(Sender: TObject);
    procedure PorMatrcula2Click(Sender: TObject);
    procedure LanamentodeVisitaTcnica1Click(Sender: TObject);
    procedure ConsultaDadosdeVisitaTcnica1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Chat1Click(Sender: TObject);
    procedure InicializarBimestre1Click(Sender: TObject);
    procedure EmitirAcompanhamentoEscolar1Click(Sender: TObject);
    procedure ConsultarAlterarDados1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure ToolButton6Click(Sender: TObject);
    procedure TrocadeUsurio1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GerarArquivodeFaltas1Click(Sender: TObject);
    procedure RelaodeTransferidosporPerodo1Click(Sender: TObject);
    procedure PesquisaGenrica1Click(Sender: TObject);
    procedure UsuriosConectados1Click(Sender: TObject);
    procedure AberturadeChamado1Click(Sender: TObject);
    procedure Unidades1Click(Sender: TObject);
    procedure RelatriodeEncaminhamento1Click(Sender: TObject);
    procedure AcompanhamentoPsicopedaggicoPsicolgico1Click(
      Sender: TObject);
    procedure RegistrodeObservaesdeAprendizagem1Click(Sender: TObject);
    procedure Perfis1Click(Sender: TObject);
    procedure RelaodeTotalporUnidade1Click(Sender: TObject);
    procedure FatoresdeInterveno1Click(Sender: TObject);
    procedure LanamentosporFatoresdeInterveno1Click(Sender: TObject);
    procedure ProgramasSociais1Click(Sender: TObject);
    procedure ControledeCalendrios1Click(Sender: TObject);
    procedure NutrioUnidades1Click(Sender: TObject);
    procedure RelaodeProgramasSociaisSituao1Click(Sender: TObject);
    procedure CrianaAdolescente1Click(Sender: TObject);
    procedure Famlia1Click(Sender: TObject);
    procedure Profissional1Click(Sender: TObject);
    procedure Profissional2Click(Sender: TObject);
    procedure EstatsticasdeAtendimento1Click(Sender: TObject);
    procedure AcompanhamentodosLanamentos1Click(Sender: TObject);
    procedure Atendimentos1Click(Sender: TObject);
    procedure AgendamentodeConsultas1Click(Sender: TObject);
    procedure OcorrnciasDiversas1Click(Sender: TObject);
    procedure AvaliaodeDesempenho1Click(Sender: TObject);
    procedure LanarAvaliao1Click(Sender: TObject);
    procedure ReclassificarIMC1Click(Sender: TObject);
    procedure AcompanhamentoBiomtrico1Click(Sender: TObject);
    procedure AvaliaodeDesempenhoAprendiz1Click(Sender: TObject);
    procedure FaltasTeste1Click(Sender: TObject);
    procedure AdolescentesConvnioEmpresa1Click(Sender: TObject);
    procedure RelaodeEmpresasporAsSocialConvnio1Click(Sender: TObject);
    procedure Caroscpio1Click(Sender: TObject);
    procedure CadastrodeEscolas1Click(Sender: TObject);
    procedure CadastrodeSriesEscolares1Click(Sender: TObject);
    procedure CadastrodeSriesxEscolas1Click(Sender: TObject);
    procedure TotalizaodeUsurioxUnidade1Click(Sender: TObject);
    procedure RelaodeTransferidosAcessoConvnio1Click(Sender: TObject);
    procedure SatisfaodoClienteConveniados1Click(Sender: TObject);
    procedure TabeladeDefasagemEscolar1Click(Sender: TObject);
    procedure AlteraesdaVerso1Click(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormShow(Sender: TObject);
    procedure VerificarPendncias1Click(Sender: TObject);
    procedure GerenciarSolicitaesdeChefia1Click(Sender: TObject);
    procedure CordeFundo1Click(Sender: TObject);
    procedure ImportaoDadosAcompEscolar1Click(Sender: TObject);
    procedure RelaoCompara42008x120091Click(Sender: TObject);
    procedure ConsultaDadosAcompanhamentoEscolar1Click(Sender: TObject);
    procedure RelaodeAlunoscomDefasagemEscolar1Click(Sender: TObject);
    procedure ResumoMensal1Click(Sender: TObject);
    procedure CONTROLEDOACOMPESCOLARBOLETIM1Click(Sender: TObject);
    procedure CorrigeHistoricoEscola1Click(Sender: TObject);
    function SenhaInputBox (const ACaption, APrompt : String): String;
    procedure BOLETIMELETRNICO1Click(Sender: TObject);
    procedure CONSULTAS1Click(Sender: TObject);
    procedure SOLICITAODETRANSFPERODOESCOLAR1Click(Sender: TObject);
    procedure ESTATSTICASGERAIS1Click(Sender: TObject);
    procedure ControledeFrequncias1Click(Sender: TObject);
    procedure DadosEducao1Click(Sender: TObject);
    procedure FolhadeFrequncia1Click(Sender: TObject);
    procedure IMPORTAOCOTASPASSE1Click(Sender: TObject);
    procedure CONFEREDADOSDECAxDRH1Click(Sender: TObject);
    procedure CONFEREDADOSDRHxDECA1Click(Sender: TObject);
    procedure SAIR2Click(Sender: TObject);
    procedure FAZERLOGOFF1Click(Sender: TObject);
    procedure RESUMODADOSMATRIZ1Click(Sender: TObject);
    procedure ALTERARSENHA1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure MATRIZDEAVALIAOSEMESTRALANUAL1Click(Sender: TObject);
    procedure ENVIARDADOSESCOLA1Click(Sender: TObject);
    procedure VERIFICARAGUARDANDOINFORMAO1Click(Sender: TObject);
    procedure IMPORTAOACOMPESCOLAR201020111Click(Sender: TObject);
    procedure IMPORTAHISTORICO20111Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MonitoramentoGeral1Click(Sender: TObject);
    procedure ModeloTradicional1Click(Sender: TObject);
    procedure MODELOCOMPACTO1Click(Sender: TObject);
    procedure FECHAMENTO1Click(Sender: TObject);
    procedure TOTALIZAOPORUNIDADEESITUAODS1Click(Sender: TObject);
    procedure LANAMENTODEDADOS1Click(Sender: TObject);
    procedure GERAODERELATRIO1Click(Sender: TObject);
    procedure REGISTRODEATENDIMENTONAVE1Click(Sender: TObject);
    procedure REALIZARENCAMINHAMENTO1Click(Sender: TObject);
    procedure REALIZARDESLIGAMENTONAVE1Click(Sender: TObject);
    procedure ALUNOSEJATELESSALA1Click(Sender: TObject);
    procedure Importar32011120121Click(Sender: TObject);
    procedure TRANSFERNCIADEBANCOINATIVOS1Click(Sender: TObject);
    procedure IMPORTAOCADASTRO1Click(Sender: TObject);
    procedure ADITAMENTODEAPRENDIUZ1Click(Sender: TObject);
    procedure ADITAMENTODECONTRATOAPRENDIZ1Click(Sender: TObject);
    procedure COMPARATIVO1Click(Sender: TObject);
    procedure COMPARARBIMESTRES1Click(Sender: TObject);
    procedure FICHACLNICAODONTOLGICA2Click(Sender: TObject);
    procedure CONTROLEDEMATERIAISODONTOLGICOS1Click(Sender: TObject);
    procedure TransfernciaColetiva1Click(Sender: TObject);
    procedure SOLICITAODEUNIFORMES1Click(Sender: TObject);
    procedure PROCESSOADMISSIONALAPRENDIZAGEM1Click(Sender: TObject);
    procedure RELATRIOSEDUCACENSO1Click(Sender: TObject);
    procedure EMISSOTERMODECOMPROMISSOBOLSISTAS1Click(Sender: TObject);
    procedure PASSESEVALESTRANSPORTE1Click(Sender: TObject);
    procedure CARTOPASSECOTASEVTS1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure TARIFASDEPASSESESCOLARES1Click(Sender: TObject);
    procedure IMPORTARDADOSEDUCACENSO1Click(Sender: TObject);
    procedure REATIVAODEPRONTURIO1Click(Sender: TObject);
    procedure TRANSFERNCIASPORPERODO1Click(Sender: TObject);
    procedure CURSOSECERTIFICADOS1Click(Sender: TObject);
    procedure IMPORTARDADOSEDUCASENSO1Click(Sender: TObject);
    procedure EMITIRFICHADECADASTRO1Click(Sender: TObject);
    procedure VISUALIZARAGENDA1Click(Sender: TObject);
    procedure MIGRARRGECPFCOMPFAMILIAR1Click(Sender: TObject);
    procedure UNIDADESESCOLARESSERIESESCOLARESETURMAS1Click(
      Sender: TObject);
    procedure CADASTROS1Click(Sender: TObject);
    procedure RELATRIODECLASSES1Click(Sender: TObject);
    procedure RELAODEMUDANASDEVNCULO1Click(Sender: TObject);
    procedure TRANSFERNCIACOLETIVA2Click(Sender: TObject);
    procedure REATIVAESPORPERODO1Click(Sender: TObject);
    procedure anual1Click(Sender: TObject);
    procedure mensal1Click(Sender: TObject);
    procedure RELATRIODEDEMANDATRIAGEM20171Click(Sender: TObject);
    procedure LEVANTAMENTODENDEATENDIDOSPORPERODO1Click(Sender: TObject);
    procedure FICHADEINSCRIO2Click(Sender: TObject);
    procedure PORTARIADEINSCRIOSELEOEADMISSO1Click(Sender: TObject);
    procedure MIGRARATENDIMENTOS1Click(Sender: TObject);
    procedure CORREODEUSURIOSTRIAGEMPDECA1Click(Sender: TObject);
    procedure CADASTRODEBAIRROS1Click(Sender: TObject);
    procedure PROCESSOADMINISTRATIVO1Click(Sender: TObject);
    procedure EFETIVARADMISSO1Click(Sender: TObject);
    procedure GERARDADOSDEMANDA1Click(Sender: TObject);
    procedure SEGUNDAVIADECONTRATOFICHA1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AtualizaStatusBarUnidade;

  end;

var
  frmPrincipal: TfrmPrincipal;

  // variaveis publicas
  vvCOD_USUARIO : integer;
  vvCOD_UNIDADE : integer;
  vvNOM_UNIDADE : string;
  vvIND_PERFIL : integer;
  vvINDEX_UNIDADE : integer;
  vvCOD_MATRICULA_PESQUISA, vvNOM_PERFIL : string ;
  vvINDEX_MES : Integer;
  vvNOM_USUARIO : String;
  vvTOTAL_FICHAS, vvCOD_ID_ACESSO, vvCOD_ID_RESUMOMENSAL: Integer;

  vvNUM_CCUSTO,
  vvNOM_GESTOR,
  vvNOM_ASOCIAL,
  vvDSC_EMAIL,
  vvNOM_DIVISAO,
  vvNUM_TELEFONE : String;

  vvEMAIL_ESCOLA : String;

  vvNUM_VERSAO : String;

  vListaUsuario1, vListaUsuario2 : TStringList;

  log_file : TextFile;  //Arquivo texto em c:\Deca\Logs para serem gravadas informa��es de
                        //registros de atendimento e evolu��o do servi�o social

  arqLog : String;

  mmStatusRegistro : Integer; //Status para registro de atendimento {0-C�a/Adol  -  1-Familia  -  2-Profissional}

  mmPATH : String;
  mmPATH2 : PChar;

  vvTIPO_UNIDADE : Integer;

  mmINTEGRACAO : Boolean;

  mmNUM_ABA : Integer;

  mmBIMESTRE_SELECIONADO : Integer;  //Utilizada na tela de Acompanhamento Escolar - Duplo clique no grid do Acomp. Escolar para retornar o Bimestre em que est� selecionado, anteriomente a altera��o do registro....

implementation

uses uUsuarios, uFichaCrianca, uDM, uSuspensao, uDesligamento, uAdvertencia, uAfastamentoEntrada,
  uAfastamentoRetorno, uTransferenciaEncaminhamento, uRegistroAtendimento,
  rListaReuniaoPais, uEvolucaoServicoSocial, uComplementacaoEncaminhamento,
  uComplementacaoRetorno, uHistorico, rGeral, uSelecionaUnidade,
  rDadosEscolares, rCepOrdenadoAdm, rCepOrdenado, uDadosAfastamento,
  uDadosAdvertencias, uDadosDesligamentos, uDadosSuspensoes,
  uAniversariantes, uConfirmaTransferenciaUnidade, uTransferenciaConvenio,
  uTransferenciaConvenioConf, uDadosTransferencias, uMudaMatricula,
  uEmiteRegAtendimento, uEmiteEvSocial, uInformaPeriodoIndicador,
  uUtil, uEvolucaoConsAltera, rTotaisUnidade, uRelacaoParaTransferir,
  uSelecionaIdadeDataEspecificao, uRegistroAtendimentoAltera,
  uControleFrequencias, uInicializarPresenca, uVisitaTecnica, uTurmas,
  uLancamentoBiometrico, uSelecionaFrequencia,
  uControleFrequenciasMatricula, uSelecionaFrequenciaPorMatricula,
  uConsultaVisitaTecnica, uChat, uInicializarBimestre,
  uEmiteAcompanhamentoEscolar, uConsultaAcompanhamentoEscolar, uEmpresas,
  rGraficoTotalConvenio, uTrocaUsuario, uGeraTXT, uTelaNovasAlteracoes,
  uNovidades, uTransferenciasDRH, uFichaPesquisaGenerica,
  uUsuariosConectados, uAberturaChamadoTecnico, uUnidades,
  uEmiteRelatorioEspecial,
  uRegistroAcompanhamentoPsicopedagogicoPsicologico,
  uObservacaoAprendizagem, uPerfis, rTotalPorUnidade, uFatoresIntervTecnica,
  uLancamentosFatoresIntervencao, uEscolas, uProgramasSociais, uCalendarios,
  uDadosVersao, uLoginNovo, uTransferenciaEncaminhamentoNova,
  uControleUnidade, rCadastro_ProgramasSociais, uRegistroAtendimentoFamilia,
  uRegistroAtendimentoProfissional, rEstatisticasAtendimento,
  uSelecionaDataEstatisticaAtendimento, uLancamentoItensFatores,
  uFichaClinica, uAgendaConsultas, uRegistroAbordagemGrupal,
  uAvaliacaoDesempenho, uNovoLancamentoBiometrico, uReclassificarIMC,
  uAcompanhamentoBiometrico, uAvaliacaoDesempenhoAprendiz,
  uLancarFaltas_Teste, rAdolescentesEmpresasConvenio, uCadastroSecoes,
  rCaroscopio, uCadastroSeriesEscolares, uCadastroSeriesPorEscola,
  uTotaisUsuarioUnidade, rTransferenciasDAPA, uTransferenciasDAPA,
  uGerenciarAcessos, uSatisfacaoConveniados, uDefasagemEscolar,
  uEncaminhamentoSolicitacaoChefia, uAlteracoesVersao,
  uImportaEscolaCadParaAcompEscolar, uComparaBimAnoAcompEscolar,
  uConsultaAcompEscolar, uSelecionaDadosDefasagemEscolar, uResumoMensal,
  rAlunosSemAcompEscolar, uConsultaDadosAcompEscolar,
  uGeradorArquivosFaltasDRH, uNovoControleFrequencia,
  uConsultaDadosDoBoletim, uEmissaoDeclaracoesSolicitacoesEscolares,
  uEstatisticasAcompEscolar, uResumoMensal_Teste, uExibeSemEscola,
  uAproveitamentoIndividual, uImportaDadosBoletimAcompanhamentoEscolar,
  uInicializarFrequenciaMensal, uFichaCadastroModelo, uImportaCotasPasse,
  uConfereDadosDECADRH, uConfereDadosDRHDECA, uLogoff, uGerarDadosMatriz,
  uAlteraSenhaAcesso, uGeraEstatisticaMatriz, uEnviarEmail,
  uConferirAguardandoInformacao, uControleCalendarios,
  uImportaAcompEscolar20102011, uGeral_AcopanhamentoEscolar, uFechamento,
  uGeraTotaisDS, uNovaAvaliacaoAprendiz, uSelecionaAnoRAICA,
  uNovoRegistroObservacaoAprendizagem, uEncaminhaNAVE, uRetornoNAVE,
  uAlunosEJATelessala, uTransferenciaDados, uImporta,
  uAditamentoContratoAprendiz, uComparaBimestres, uEstoqueOdonto,
  uTransferenciaColetiva, uSolicitacaoUniformes,
  uProcessoAdminissionalAprendizes, uDadosRelatorio, uTermoBolsistas,
  uImportarAdmissoes, uSelecionarDadosFrequencia, uCadastroValoresPasses,
  uImportaEducacensoAcompEscolar, uReativacaoProntuario, uImportaDocumentos,
  uEstoqueUniformes, uCursosCertificados, uEmitirFichaCadastro,
  uConsultaAgendaOdontologica, uMigrarRGeCPF_ComposicaoFamiliar,
  uControleUnidadesEscolaresTurmasClasses, rEducacenso,
  uEmitirRelatorioDeClasses, uConsultaMudancasVinculo,
  uConsultaReativacoesProntuarios, uConsultaDemandaTriagemTST,
  uCadBairrosTriagem, uOpcaoRelatorioEstatisticasAcEscolar,
  uCadastroInscricoes, uPESQUISA_INSCRITOS_TRIAGEM,
  uMigrarAtendimentosParaRelatorios, ufrmCorrecaoUsuariosRelatorios,
  uADMISSOES, uSELECIONA_DEMANDA, uGERA_DEMANDA, uFichaPesquisa,
  uSEGUNDA_VIA_CONTRATO;

{$R *.DFM}

procedure TfrmPrincipal.Encerrar1Click(Sender: TObject);
begin
  Application.MessageBox('Efetuando LOG-OFF do Sistema!'+#13+#10+#13+#10+
             'A Assessoria de Inform�tica agradece.',
             'Sistema DECA - Status de Usu�rio',
             MB_OK + MB_ICONQUESTION);
  frmPrincipal.Close;
  Application.Terminate;
end;

procedure TfrmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  //Pede confirma��o para encerrar o aplicativo
  if Application.MessageBox('Deseja realmente encerrar o aplicativo?',
                            'Sistema Deca - Finalizar',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    //Atualiza o campo flg_status do usu�rio para OFF-LINE
    try
      with dmDeca.cdsAlt_Status_Usuario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName ('@pe_flg_status').Value := 1; // 0 para On-Line e 1 para Off-Line
        Execute;
        Application.Terminate;
      end;
    except end;
  end
  else
  begin
    frmPrincipal.Show;      
  end;
end;

procedure TfrmPrincipal.Usuarios1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmUsuarios, frmUsuarios);
  frmUsuarios.ShowModal;
end;

procedure TfrmPrincipal.CrianasAdolescentes1Click(Sender: TObject);
begin
  //frmFichaCrianca := TFrmFichaCrianca.Create(Application);
  //frmFichaCrianca.ShowModal;
  Application.CreateForm (TfrmFichaCadastroModelo, frmFichaCadastroModelo);
  frmFichaCadastroModelo.ShowModal;
end;

// Atualiza o Status Bar Central com o nome da Unidade e
procedure TfrmPrincipal.AtualizaStatusBarUnidade;
begin

  try
    with dmDeca.cdsSel_Cadastro_Contador do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;

      frmPrincipal.StatusBar1.Panels[3].Text := frmPrincipal.StatusBar1.Panels[3].Text +
             //Copy(frmPrincipal.StatusBar1.Panels[3].Text, 0,
             //Pos('[', frmPrincipal.StatusBar1.Panels[3].Text) -1) +
             //'[ ' +
             IntToStr(FieldByName('Total_Fichas').AsInteger) +
             ' fichas cadastradas]';
      Pos(frmPrincipal.StatusBar1.Panels[3].Text,'[');

      vvTOTAL_FICHAS := 0;
      vvTOTAL_FICHAS := FieldByName('Total_Fichas').AsInteger;
      //Close;
    end;
  except end;

end;

procedure TfrmPrincipal.Advertncia1Click(Sender: TObject);
begin
  frmAdvertencia:= TFrmAdvertencia.Create(Application);
  frmAdvertencia.ShowModal;
end;

procedure TfrmPrincipal.Suspenso1Click(Sender: TObject);
begin
  frmSuspensao:= TFrmSuspensao.Create(Application);
  frmSuspensao.ShowModal;
end;

procedure TfrmPrincipal.Desligamento1Click(Sender: TObject);
begin
  frmDesligamento:= TFrmDesligamento.Create(Application);
  frmDesligamento.ShowModal;
end;

procedure TfrmPrincipal.Entrada1Click(Sender: TObject);
begin
  frmAfastamentoEntrada := TFrmAfastamentoEntrada.Create(Application);
  frmAfastamentoEntrada.ShowModal;
end;

procedure TfrmPrincipal.Retorno2Click(Sender: TObject);
begin
  frmAfastamentoRetorno := TFrmAfastamentoRetorno.Create(Application);
  frmAfastamentoRetorno.ShowModal;
end;

procedure TfrmPrincipal.Encaminhamento1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmNovaTransferencia, frmNovaTransferencia);
  frmNovaTransferencia.ShowModal;
 end;

procedure TfrmPrincipal.ListadePresenaReuniodePais1Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Lista_PresencaTodos do
    begin
      Close;
      //Params.ParamByName('@pe_ind_status').AsInteger := 1;

      if (vVIND_PERFIL in [1,18,19,20,23,24,25]) then
        Params.ParamByName('@pe_cod_unidade').Value := Null
      else
        Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;

      Application.CreateForm(TrelListaReuniaoPais, relListaReuniaoPais);
      relListaReuniaoPais.lbUnidade.Caption := vvNOM_UNIDADE;
      relListaReuniaoPais.Preview;
      relListaReuniaoPais.Free;

    end
  except end;
end;

procedure TfrmPrincipal.Encaminhamento3Click(Sender: TObject);
begin
  Application.CreateForm(TFrmComplementacaoEncaminhamento, frmComplementacaoEncaminhamento);
  FrmComplementacaoEncaminhamento.ShowModal;
end;

procedure TfrmPrincipal.Retorno3Click(Sender: TObject);
begin
  Application.CreateForm(TFrmComplementacaoRetorno, frmComplementacaoRetorno);
  frmComplementacaoRetorno.ShowModal;
end;

procedure TfrmPrincipal.Histrico1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmHistorico, frmHistorico);
  frmHistorico.ShowModal;
end;

procedure TfrmPrincipal.doPETI1Click(Sender: TObject);
begin
//Seleciona as crian�as/adolescentes do PETI
  //with dmDeca.cdsSel_Peti do
  with dmDeca.cdsSel_PetiTodos do
  begin
    Close;
    if (vvIND_PERFIL in [1,5,7,8,9,18,19,20,23,24,25]) then
         Params.ParamByName('@pe_cod_unidade').Value := Null;

    if (vvIND_PERFIL in[2,3,4,6]) then
         Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;

    Open;

    Application.CreateForm(TrelGeral, relGeral);
    //relGeral.DataSet := dmDeca.cdsSel_Peti;
    relGeral.DataSet := dmDeca.cdsSel_PetiTodos;
    relGeral.fld_matricula.DataSet := dmDeca.cdsSel_PetiTodos;
    relGeral.fld_matricula.DataField := 'cod_matricula';
    relGeral.fld_nome.DataSet := dmDeca.cdsSel_PetiTodos;
    relGeral.fld_nome.DataField := 'nom_nome';
    relGeral.fld_endereco.DataSet := dmDeca.cdsSel_PetiTodos;
    relGeral.fld_endereco.DataField := 'nom_endereco';
    relGeral.fld_bairro.DataSet := dmDeca.cdsSel_PetiTodos;
    relGeral.fld_bairro.DataField := 'nom_bairro';
    relGeral.fld_modalidade.DataSet := dmDeca.cdsSel_PetiTodos;
    relGeral.fld_modalidade.DataField := 'modalidade';

    relGeral.QRLabel6.Transparent := False;
    relGeral.fld_modalidade.Transparent := False;

    relGeral.lbTitulo.Caption := 'RELA��O DE CRIAN�AS/ADOLESCENTES - PETI E FUNDHAS/PETI';
    relGeral.lbTotal.Caption  := 'Total de Crian�as/Adolescentes encontrados na unidade ' + vvNOM_UNIDADE + ': ';
    relGeral.lbUnidade.Caption := vvNOM_UNIDADE;

    relGeral.Preview;
    relGeral.Free;

  end;

end;

procedure TfrmPrincipal.OrdenadosporMatrcula1Click(Sender: TObject);
begin
  //Administrador, Psicologia/Psicopedagogia, Diretor, DRH, Triagem  e Educa��o F�sica
  if (vvIND_PERFIL in [1,5,7,8,9,10,18,19,20,23,24,25])then
  begin
    //with dmDeca.cdsSel_Ordena_Matricula_Adm do
    with dmDeca.cdsSel_Ordena_Matricula_AdmTodos do
    begin
      Close;
      //Params.ParamByName('@pe_ind_status').AsInteger := 1;
      Open;

      Application.CreateForm(TrelGeral, relGeral);
      //relGeral.DataSet := dmDeca.cdsSel_Ordena_Matricula_Adm;
      relGeral.DataSet := dmDeca.cdsSel_Ordena_Matricula_AdmTodos;

      relGeral.fld_matricula.DataSet := dmDeca.cdsSel_Ordena_Matricula_AdmTodos;
      relGeral.fld_matricula.DataField := 'cod_matricula';
      relGeral.fld_nome.DataSet := dmDeca.cdsSel_Ordena_Matricula_AdmTodos;
      relGeral.fld_nome.DataField := 'nom_nome';
      relGeral.fld_endereco.DataSet := dmDeca.cdsSel_Ordena_Matricula_AdmTodos;
      relGeral.fld_endereco.DataField := 'nom_endereco';
      relGeral.fld_bairro.DataSet := dmDeca.cdsSel_Ordena_Matricula_AdmTodos;
      relGeral.fld_bairro.DataField := 'nom_bairro';
      relGeral.fld_modalidade.DataSet := dmDeca.cdsSel_Ordena_Matricula_AdmTodos;
      relGeral.fld_modalidade.DataField := 'nom_unidade';

      relGeral.QRLabel6.Transparent := False;
      relGeral.QRLabel6.Caption := 'Unidade';
      relGeral.fld_modalidade.Transparent := False;

      relGeral.lbTitulo.Caption := 'RELA��O DE CRIAN�AS/ADOLESCENTES DAS UNIDADES  -  ORDENADOS POR MATR�CULA';
      relGeral.lbTotal.Caption := 'Total de Crian�as/Adolescentes encontrados: ';
      relGeral.lbUnidade.Caption := 'FUNDHAS';

      relGeral.Preview;
      relGeral.Free;
    end;
  end
else if (vvIND_PERFIL=2) or (vvIND_PERFIL=3) or (vvIND_PERFIL=4) or (vvIND_PERFIL=6) then
  begin
    //with dmDeca.cdsSel_Ordena_Matricula do
    with dmDeca.cdsSel_Ordena_MatriculaTodos do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      Application.CreateForm(TrelGeral, relGeral);
      relGeral.DataSet := dmDeca.cdsSel_Ordena_MatriculaTodos;

      relGeral.fld_matricula.DataSet := dmDeca.cdsSel_Ordena_MatriculaTodos;
      relGeral.fld_matricula.DataField := 'cod_matricula';
      relGeral.fld_nome.DataSet := dmDeca.cdsSel_Ordena_MatriculaTodos;
      relGeral.fld_nome.DataField := 'nom_nome';
      relGeral.fld_endereco.DataSet := dmDeca.cdsSel_Ordena_MatriculaTodos;
      relGeral.fld_endereco.DataField := 'nom_endereco';
      relGeral.fld_bairro.DataSet := dmDeca.cdsSel_Ordena_MatriculaTodos;
      relGeral.fld_bairro.DataField := 'nom_bairro';
      relGeral.fld_modalidade.DataSet := dmDeca.cdsSel_Ordena_MatriculaTodos;
      relGeral.fld_modalidade.DataField := 'nom_unidade';

      relGeral.QRLabel6.Transparent := False;
      relGeral.QRLabel6.Caption := '';
      relGeral.fld_modalidade.Transparent := False;

      relGeral.lbTitulo.Caption := 'RELA��O DE CRIAN�AS/ADOLESCENTES ORDENADOS POR MATR�CULA';
      relGeral.lbTotal.CAption := 'Total de Crian�as/Adolescentes encontrados na unidade ' +
                                vvNOM_UNIDADE + ': ';
      relGeral.lbUnidade.Caption := vvNOM_UNIDADE;

      relGeral.Preview;
      relGeral.Free;

    end;
  end;
end;
procedure TfrmPrincipal.RelaodeCrianasAdolescentesporUnidade1Click(
  Sender: TObject);
begin
  Application.CreateForm(TfrmSelecionaUnidade, frmSelecionaUnidade);
  frmSelecionaUnidade.ShowModal;
end;

procedure TfrmPrincipal.OrdenadosporNome1Click(Sender: TObject);
begin
    //Administrador
  if (vvIND_PERFIL in [1,5,7,8,9,10,17,18,19,20,23,24,25]) then
  begin
    //with dmDeca.cdsSel_Ordena_Nome_Adm do
    with dmDeca.cdsSel_Ordena_Nome_AdmTodos do
    begin
      Close;

      Open;

      Application.CreateForm(TrelGeral, relGeral);

      relGeral.DataSet := dmDeca.cdsSel_Ordena_Nome_AdmTodos;

      relGeral.fld_matricula.DataSet := dmDeca.cdsSel_Ordena_Nome_AdmTodos;
      relGeral.fld_matricula.DataField := 'cod_matricula';
      relGeral.fld_nome.DataSet := dmDeca.cdsSel_Ordena_Nome_AdmTodos;
      relGeral.fld_nome.DataField := 'nom_nome';
      relGeral.fld_endereco.DataSet := dmDeca.cdsSel_Ordena_Nome_AdmTodos;
      relGeral.fld_endereco.DataField := 'nom_endereco';
      relGeral.fld_bairro.DataSet := dmDeca.cdsSel_Ordena_Nome_AdmTodos;
      relGeral.fld_bairro.DataField := 'nom_bairro';

      relGeral.fld_modalidade.DataSet := dmDeca.cdsSel_Ordena_Nome_AdmTodos;
      relGeral.fld_modalidade.DataField := 'nom_unidade';

      relGeral.QRLabel6.Transparent := False;
      relGeral.QRLabel6.Caption := 'Unidade';
      relGeral.fld_modalidade.Transparent := False;

      relGeral.lbTitulo.Caption := 'RELA��O DE CRIAN�AS/ADOLESCENTES DAS UNIDADES ORDENADOS POR NOME';
      relGeral.lbTotal.Caption := 'Total de Crian�as/Adolescentes encontrados: ';
      relGeral.lbUnidade.Caption := 'FUNDHAS';

      relGeral.Preview;
      relGeral.Free;
    end;
  end
else if (vvIND_PERFIL=2) or (vvIND_PERFIL=3) or (vvIND_PERFIL=4) or (vvIND_PERFIL=6) then
  begin
    with dmDeca.cdsSel_Ordena_NomeTodos do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      Application.CreateForm(TrelGeral, relGeral);

      relGeral.DataSet := dmDeca.cdsSel_Ordena_NomeTodos;

      relGeral.fld_matricula.DataSet := dmDeca.cdsSel_Ordena_NomeTodos;
      relGeral.fld_matricula.DataField := 'cod_matricula';
      relGeral.fld_nome.DataSet := dmDeca.cdsSel_Ordena_NomeTodos;
      relGeral.fld_nome.DataField := 'nom_nome';
      relGeral.fld_endereco.DataSet := dmDeca.cdsSel_Ordena_NomeTodos;
      relGeral.fld_endereco.DataField := 'nom_endereco';
      relGeral.fld_bairro.DataSet := dmDeca.cdsSel_Ordena_NomeTodos;
      relGeral.fld_bairro.DataField := 'nom_bairro';

      relGeral.fld_modalidade.DataSet := dmDeca.cdsSel_Ordena_NomeTodos;
      relGeral.fld_modalidade.DataField := 'nom_unidade';

      relGeral.QRLabel6.Transparent := False;

      relGeral.QRLabel6.Caption := '';
      relGeral.fld_modalidade.Transparent := False;

      relGeral.lbTitulo.Caption := 'RELA��O DE CRIAN�AS/ADOLESCENTES ORDENADOS POR NOME';
      relGeral.lbTotal.CAption := 'Total de Crian�as/Adolescentes encontrados na unidade ' +
                                vvNOM_UNIDADE + ': ';
      relGeral.lbUnidade.Caption := vvNOM_UNIDADE;

      relGeral.Preview;
      relGeral.Free;

    end;
  end;
end;

procedure TfrmPrincipal.OrdenadosporCEP1Click(Sender: TObject);
begin
  if (vvIND_PERFIL in [1,5,7,8,9,10,17,18,19,20,23,24,25]) then
  begin

    with dmDeca.cdsSel_Cep_AdmTodos do
    begin
      Close;

      Open;

      Application.CreateForm(TrelCepOrdenadoAdm, relCepOrdenadoAdm);
      relCepOrdenadoAdm.DataSet := dmDeca.cdsSel_Cep_AdmTodos;

      relCepOrdenadoAdm.lbUnidade.Caption := vvNOM_UNIDADE;
      relCepOrdenadoAdm.Preview;
      relCepOrdenadoAdm.Free;
    end;

  end;

  if (vvIND_PERFIL=2) or (vvIND_PERFIL=3) or (vvIND_PERFIL=4) or (vvIND_PERFIL=6) then
  begin

    with dmDeca.cdsSel_CepTodos do
    begin
      Close;

      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      Application.CreateForm(TrelCepOrdenado, relCepOrdenado);
      relCepOrdenado.Dataset := dmDeca.cdsSel_CepTodos;

      relCepOrdenado.lbUnidade.Caption := vvNOM_UNIDADE;
      relCepOrdenado.Preview;
      relCepOrdenado.Free;
    end;

  end;


end;

procedure TfrmPrincipal.RelaodeAfastamentos1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmDadosAfastamento, frmDadosAfastamento);
  frmDadosAfastamento.ShowModal;
end;

procedure TfrmPrincipal.RelaodeAdvertncias1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmDadosAdvertencias, frmDadosAdvertencias);
  frmDadosAdvertencias.ShowModal;
end;

procedure TfrmPrincipal.RelaodeDesligados1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmDadosDesligamento, frmDadosDesligamento);
  frmDadosDesligamento.ShowModal;
end;

procedure TfrmPrincipal.RelaodeSuspenses1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmDadosSuspensao, frmDadosSuspensao);
  frmDadosSuspensao.ShowModal;
end;

procedure TfrmPrincipal.RegistrodeAtendimento2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmAniversariantes, frmAniversariantes);
  frmAniversariantes.ShowModal;
end;

procedure TfrmPrincipal.FormActivate(Sender: TObject);
begin

  //Validar o aparecimento da tela  "Rela��o a ser transferido" somente para
  //perfis: Gestor e Assistente Social
  try
    if vvIND_PERFIL in [2,3] then
    begin
      with dmDeca.cdsSel_Conta_Em_Transferencia do
      begin
        Close;
        Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Open;

        if dmDeca.cdsSel_Conta_Em_Transferencia.FieldByname('Total_Em_Transferencia').AsInteger > 0 then
        begin
          Application.CreateForm (TfrmRelacaoParaTransferir, frmRelacaoParaTransferir);
          frmRelacaoParaTransferir.ShowModal
        end
        else
        begin

        end
      end
    end
    else
    begin

    end
  except
    begin
      ShowMessage('Sr. Usu�rio'+#13+#10+#13+#10+
                  'Ocorreu um problema no sistema, favor informar o Administrador'+#13+#10+
                  'do Sistema informando esta mensagem: ERRO COM CONTAGEM DE TRANSFERIDOS DE UNIDADE');
    end;

  end;

  { Desativada em 25/03/2015
  //Carregar a lista de Alunos SEM ESCOLA apenas para os perfis 2,3 e 6 (gestor, ass. social e professor/instrutor)
  if (vvIND_PERFIL in [2,3,6,7,14,15,18,19,20,23,24,25]) then
  begin
    Application.CreateForm (TfrmExibeSemEscola, frmExibeSemEscola);
    frmExibeSemEscola.ShowModal;
  end;
  }


end;

procedure TfrmPrincipal.Confirmao1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmConfirmaTransferenciaUnidade, frmConfirmaTransferenciaUnidade);
  frmConfirmaTransferenciaUnidade.ShowModal;
end;

procedure TfrmPrincipal.Encaminhamento2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmTransferenciaConvenio, frmTransferenciaConvenio);
  frmTransferenciaConvenio.ShowModal;
end;

procedure TfrmPrincipal.Retorno1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmTransferenciaConvenioConf, frmTransferenciaConvenioConf);
  frmTransferenciaConvenioConf.ShowModal;
end;

procedure TfrmPrincipal.RelaodeTransferidos1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmDadosTransferencias, frmDadosTransferencias);
  frmDadosTransferencias.ShowModal;
end;

procedure TfrmPrincipal.MudanadeMatrcula1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmMudaMatricula, frmMudaMatricula);
  frmMudaMatricula.ShowModal;
end;

procedure TfrmPrincipal.RegistrodeAtendimento3Click(Sender: TObject);
begin
  Application.CreateForm(TfrmEmiteRegAtendimento, frmEmiteRegAtendimento);
  frmEmiteRegAtendimento.ShowModal;
end;

procedure TfrmPrincipal.EvoluoServioSocial1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmEmiteEvSocial, frmEmiteEvSocial);
  frmEmiteEvSocial.ShowModal;
end;

procedure TfrmPrincipal.IndicadoresdoServioSocial1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmInformaPeriodoIndicador, frmInformaPeriodoIndicador);
  frmInformaPeriodoIndicador.lbUnidade.Caption := vvNOM_UNIDADE;
  frmInformaPeriodoIndicador.ShowModal;
end;

procedure TfrmPrincipal.NovosLanamentos1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmEvolucaoServicoSocial, frmEvolucaoServicoSocial);
  frmEvolucaoServicoSocial.ShowModal;
end;

procedure TfrmPrincipal.ConsultaAlterao1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmEvolucaoConsAltera, frmEvolucaoConsAltera);
  frmEvolucaoConsAltera.ShowModal;
end;

procedure TfrmPrincipal.TotaldeCrianasAdolescentes1Click(Sender: TObject);
begin

  //if (vvIND_PERFIL not in [1,5,7,8,9,10]) then
  if (vvIND_PERFIL <> 1) or (vvIND_PERFIL <> 5) or (vvIND_PERFIL <> 7) or(vvIND_PERFIL <> 8) or (vvIND_PERFIL <> 9) or (vvIND_PERFIL <> 10) or
     (vvIND_PERFIL <> 20) or (vvIND_PERFIL <> 23) or (vvIND_PERFIL <> 24) or(vvIND_PERFIL <> 25) then
  begin

    //Excluir dados da tabela referente ao usu�rio logado atualmente
    with dmDeca.cdsExc_DadosGrafico do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;
    end;

    //Calcular o total de meninos de manh� e Incluir e Grafico
    //Meninos Manh�
    with dmDeca.cdsSel_MeninosManha do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;

      with dmDeca.cdsInc_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
        Params.ParamByName ('@pe_dsc_total').AsString := 'Meninos Manh�';
        Params.ParamByName ('@pe_num_total').AsInteger := dmDeca.cdsSel_MeninosManha.FieldByName ('MENINOS_MANHA').AsInteger;
        Execute;
      end;
    end;

    //Meninos a Tarde
    with dmDeca.cdsSel_MeninosTarde do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;

      with dmDeca.cdsInc_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
        Params.ParamByName ('@pe_dsc_total').AsString := 'Meninos Tarde';
        Params.ParamByName ('@pe_num_total').AsInteger := dmDeca.cdsSel_MeninosTarde.FieldByName ('MENINOS_TARDE').AsInteger;
        Execute;
      end;
    end;

    //Meninas de Manh�
    with dmDeca.cdsSel_MeninasManha do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;

      with dmDeca.cdsInc_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
        Params.ParamByName ('@pe_dsc_total').AsString := 'Meninas Manh�';
        Params.ParamByName ('@pe_num_total').AsInteger := dmDeca.cdsSel_MeninasManha.FieldByName ('MENINAS_MANHA').AsInteger;
        Execute;
      end;
    end;

    //Meninas Tarde
    with dmDeca.cdsSel_MeninasTarde do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;

      with dmDeca.cdsInc_DadosGrafico do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
        Params.ParamByName ('@pe_dsc_total').AsString := 'Meninas Tarde';
        Params.ParamByName ('@pe_num_total').AsInteger := dmDeca.cdsSel_MeninasTarde.FieldByName ('MENINAS_TARDE').AsInteger;
        Execute;
      end;
    end;

    with dmDeca.cdsSel_DadosGrafico do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
      Open;
    end;

    with dmDeca.cdsSel_TotalUnidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vVCOD_UNIDADE;
      Open;
    end;

    Application.CreateForm(TRelTotaisUnidade, relTotaisUnidade);
    relTotaisUnidade.lbUnidade.Caption := vvNOM_UNIDADE;
    relTotaisUnidade.Preview;
    relTotaisUnidade.Free;
  end
end;

procedure TfrmPrincipal.ConfirmaodeRecebimentoTransferncias1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmRelacaoParaTransferir, frmRelacaoParaTransferir);
  frmRelacaoParaTransferir.ShowModal;
end;

procedure TfrmPrincipal.IdadeEspecfica1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaIdadeDataEspecifica, frmSelecionaIdadeDataEspecifica);
  frmSelecionaIdadeDataEspecifica.ShowModal;
end;

procedure TfrmPrincipal.NovosLanamentos2Click(Sender: TObject);
begin
//  frmRegistroAtendimento := TFrmRegistroAtendimento.Create(Application);
//  frmRegistroAtendimento.ShowModal;
end;

procedure TfrmPrincipal.AlteraesdeDados1Click(Sender: TObject);
begin
  frmRegistroAtendimentoAltera := TFrmRegistroAtendimentoAltera.Create(Application);
  frmRegistroAtendimentoAltera.ShowModal;
end;

procedure TfrmPrincipal.InicializarPresena1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmInicializarFrequenciaMensal, frmInicializarFrequenciaMensal);
  frmInicializarFrequenciaMensal.ShowModal;
end;

procedure TfrmPrincipal.N3Click(Sender: TObject);
begin
  Application.CreateForm (TfrmTurmas, frmTurmas);
  frmTurmas.ShowModal;
end;

procedure TfrmPrincipal.btnCadastroClick(Sender: TObject);
begin
  frmFichaCrianca := TFrmFichaCrianca.Create(Application);
  frmFichaCrianca.ShowModal;
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin

  frmPrincipal.Caption := '[Sistema DECA] - Gest�o de Crian�as e Adolescentes - [Vers�o 7.3.1 de 26/MAR�O/2018 -> DIVIS�O DE TECNOLOGIA DA INFORMA��O]';


  vListaUsuario1 := TStringList.Create();
  vListaUsuario2 := TStringList.Create();

  //Procedure alterada...
  try
    with dmDeca.cdsSel_Usuario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_flg_situacao').Value := Null;
      Open;

      while not eof do
      begin
        vListaUsuario1.Add (IntToStr(FieldByName ('cod_usuario').AsInteger));
        vListaUsuario2.Add (FieldByName ('nom_usuario').AsString);
        Next;
      end;
      vListaUsuario2.Sort;
    end;
  except end;

  //Centralizar a imagem principal no centro do formul�rio
  Image1.Left := Trunc( ( frmPrincipal.Width - Image1.Width ) / 2 );
  Image1.Top  := Trunc( ( frmPrincipal.Height - Image1.Height ) / 2 );

end;

procedure TfrmPrincipal.PorTurmas1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmControleFrequencia, frmControleFrequencia);
  frmControleFrequencia.ShowModal;
end;

procedure TfrmPrincipal.PorMatrcula1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmControleFrequenciasMatricula, frmControleFrequenciasMatricula);
  //frmControleFrequenciasMatricula.ShowModal;
  //Application.CreateForm (TfrmNovoControleFrequencia, frmNovoControleFrequencia);
  //frmNovoControleFrequencia.ShowModal;
end;

procedure TfrmPrincipal.PorTurma1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaFrequencia, frmSelecionaFrequencia);
  frmSelecionaFrequencia.ShowModal;
end;

procedure TfrmPrincipal.PorMatrcula2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaFrequenciaPorMatricula, frmSelecionaFrequenciaPorMatricula);
  frmSelecionaFrequenciaPorMatricula.ShowModal;
end;

procedure TfrmPrincipal.LanamentodeVisitaTcnica1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmVisitaTecnica, frmVisitaTecnica);
  frmVisitaTecnica.ShowModal;
end;

procedure TfrmPrincipal.ConsultaDadosdeVisitaTcnica1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConsultaVisitaTecnica, frmConsultaVisitaTecnica);
  frmConsultaVisitaTecnica.ShowModal;
end;

procedure TfrmPrincipal.Button1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmChat, frmChat);
  frmChat.Show;
end;

procedure TfrmPrincipal.Chat1Click(Sender: TObject);
begin
  //Application.CreateForm(TfrmChat, frmChat);
  //frmChat.Show;

  //Application.MessageBox ('Aten��o!!!' + #13+#10 +
  //                        'A janela do aplicativo ser� aberta. Caso o Sistema Deca fique ' + #13+#10 +
  //                        'indispon�vel o aplicativo ainda permanecer� ativo em sua �rea de trabalho.',
  //                        '[Sistema Deca] - Acesso Remoto',
  //                        MB_OK + MB_ICONASTERISK);

  //mmPATH2 := PChar(ExtractFilePath(Application.ExeName) + 'remotoInterno.exe');
  //WinExec (mmPATH2, SW_SHOWNORMAL );


end;

procedure TfrmPrincipal.InicializarBimestre1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmInicializarBimestre, frmInicializarBimestre);
  frmInicializarBimestre.ShowModal;
end;

procedure TfrmPrincipal.EmitirAcompanhamentoEscolar1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmEmiteAcompanhamentoEscolar, frmEmiteAcompanhamentoEscolar);
  frmEmiteAcompanhamentoEscolar.ShowModal;
end;

procedure TfrmPrincipal.ConsultarAlterarDados1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConsultaAcompanhamentoEscolar, frmConsultaAcompanhamentoEscolar);
  frmConsultaAcompanhamentoEscolar.ShowModal;
end;

procedure TfrmPrincipal.N2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmGerenciarAcessos, frmGerenciarAcessos);
  frmGerenciarAcessos.ShowModal;
end;

procedure TfrmPrincipal.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin

  if Panel = StatusBar1.Panels[0] then
    StatusBar1.Canvas.Draw (Rect.Left + 2, Rect.Top, Image2.Picture.BitMap);

  if Panel = StatusBar1.Panels[2] then
    StatusBar1.Canvas.Draw (Rect.Left + 2, Rect.Top, Image3.Picture.BitMap);

  if Panel = StatusBar1.Panels[4] then
    StatusBar1.Canvas.Draw (Rect.Left + 2, Rect.Top, Image5.Picture.BitMap);

end;

procedure TfrmPrincipal.ToolButton6Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmPrincipal.TrocadeUsurio1Click(Sender: TObject);
begin
  frmPrincipal.Hide;
  Application.CreateForm (TfrmLoginNovo, frmLoginNovo);
  frmLoginNovo.ShowModal;
end;

procedure TfrmPrincipal.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  //Pede confirma��o para encerrar o aplicativo
  if Application.MessageBox('Deseja realmente encerrar o aplicativo?',
                            'Sistema Deca - Finalizar',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    //Atualiza o campo flg_status do usu�rio para OFF-LINE
    try
      with dmDeca.cdsAlt_Status_Usuario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName ('@pe_flg_status').Value := 1; // 0 para On-Line e 1 para Off-Line
        Execute;
        Application.Terminate;
    end;
    except end;
  end
  else
  begin
    //ShowMessage ('S�bia decis�o... Volte IMEDIATAMENTE ao trabalho...!!!');
  end;

end;

procedure TfrmPrincipal.GerarArquivodeFaltas1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmGeradorArquivosFaltasDRH, frmGeradorArquivosFaltasDRH);
  frmGeradorArquivosFaltasDRH.ShowModal;

end;

procedure TfrmPrincipal.RelaodeTransferidosporPerodo1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmTransferenciasDRH, frmTransferenciasDRH);
  frmTransferenciasDRH.ShowModal;
end;

procedure TfrmPrincipal.PesquisaGenrica1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmFichaPesquisaGenerica, frmFichaPesquisaGenerica);
  frmFichaPesquisaGenerica.ShowModal;
end;

procedure TfrmPrincipal.UsuriosConectados1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmUsuariosConectados, frmUsuariosConectados);
  frmUsuariosConectados.ShowModal;
end;

procedure TfrmPrincipal.AberturadeChamado1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmAberturaChamadoTecnico, frmAberturaChamadoTecnico);
  frmAberturaChamadoTecnico.ShowModal;
end;

procedure TfrmPrincipal.Unidades1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmUnidades, frmUnidades);
  //frmUnidades.ShowModal;

  Application.CreateForm (TfrmNovoCadastroSecoes, frmNovoCadastroSecoes);
  frmNovoCadastroSecoes.ShowModal;
end;

procedure TfrmPrincipal.RelatriodeEncaminhamento1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEmiteRelatorioEspecial, frmEmiteRelatorioEspecial);
  frmEmiteRelatorioEspecial.ShowModal;
end;

procedure TfrmPrincipal.AcompanhamentoPsicopedaggicoPsicolgico1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmRegistroAcompanhamentoPsicopedagogicoPsicologico, frmRegistroAcompanhamentoPsicopedagogicoPsicologico);
  frmRegistroAcompanhamentoPsicopedagogicoPsicologico.ShowModal;
end;

procedure TfrmPrincipal.RegistrodeObservaesdeAprendizagem1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmObservacaoAprendizagem, frmObservacaoAprendizagem);
  frmObservacaoAprendizagem.ShowModal;
end;

procedure TfrmPrincipal.Perfis1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmPerfil, frmPerfil);
  frmPerfil.ShowModal;
end;

procedure TfrmPrincipal.RelaodeTotalporUnidade1Click(Sender: TObject);
begin
  try
    with dmDeca.cdsTotalizaPorUnidade do
    begin
      Close;
      Open;
      Application.CreateForm(TRelTotalPorUnidade, relTotalPorUnidade);
      relTotalPorUnidade.lbNomeUsuario.Caption := vvNOM_USUARIO;
      relTotalPorUnidade.Preview;
      relTotalPorUnidade.Free;
    end;
  except end;
end;

procedure TfrmPrincipal.FatoresdeInterveno1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmFatoresIntervTecnica, frmFatoresIntervTecnica);
  frmFatoresIntervTecnica.ShowModal;

end;

procedure TfrmPrincipal.LanamentosporFatoresdeInterveno1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmLancamentoFatoresIntervencao, frmLancamentoFatoresIntervencao);
  frmLancamentoFatoresIntervencao.ShowModal;
end;

procedure TfrmPrincipal.ProgramasSociais1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmProgramasSociais, frmProgramasSociais);
  frmProgramasSociais.ShowModal;
end;

procedure TfrmPrincipal.ControledeCalendrios1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmCalendarios, frmCalendarios);
  //frmCalendarios.ShowModal;
  Application.CreateForm (TfrmControleCalendarios, frmControleCalendarios);
  frmControleCalendarios.ShowModal;
end;

procedure TfrmPrincipal.NutrioUnidades1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmControleUnidade, frmControleUnidade);
  frmControleUnidade.ShowModal;
end;

procedure TfrmPrincipal.RelaodeProgramasSociaisSituao1Click(
  Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro_Programa_Social do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_cad_progsocial').Value := Null;
      Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_flg_status').Value := Null;
      Params.ParamByName('@pe_data_insercao1').Value := Null;
      Params.ParamByName('@pe_data_insercao2').Value := Null;
      Params.ParamByName('@pe_data_desligamento1').Value := Null;
      Params.ParamByName('@pe_data_desligamento2').Value := Null;

      if vvCOD_UNIDADE <> 0 then
        Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE
      else
        Params.ParamByName('@pe_cod_unidade').Value := Null;
      Open;
      Application.CreateForm (TrelCadastro_ProgramasSociais, relCadastro_ProgramasSociais);
      relCadastro_ProgramasSociais.Preview;
      relCadastro_ProgramasSociais.Free;
    end;
  except end;
end;

procedure TfrmPrincipal.CrianaAdolescente1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmRegistroAtendimento, frmRegistroAtendimento);
  mmStatusRegistro := 0; //Status de registro de Atendimento para Crian�a
  frmRegistroAtendimento.ShowModal;
end;

procedure TfrmPrincipal.Famlia1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmRegistroAtendimento, frmRegistroAtendimento);
  mmStatusRegistro := 1; //Status de registro de Atendimento para Adolescente
  frmRegistroAtendimento.ShowModal;
end;

procedure TfrmPrincipal.Profissional1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmRegistroAtendimentoFamilia, frmRegistroAtendimentoFamilia);
  mmStatusRegistro := 2; //Status de registro de Atendimento para Fam�lia
  frmRegistroAtendimentoFamilia.ShowModal;
end;

procedure TfrmPrincipal.Profissional2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmRegistroAtendimentoProfissional, frmRegistroAtendimentoProfissional);
  mMStatusRegistro := 3; //Status de Registro de Atendimento para Profissional
  frmRegistroAtendimentoProfissional.ShowModal;
end;

procedure TfrmPrincipal.EstatsticasdeAtendimento1Click(Sender: TObject);
begin
 Application.CreateForm(TfrmSelecionaDataEstatisticaAtendimento, frmSelecionaDataEstatisticaAtendimento);
 frmSelecionaDataEstatisticaAtendimento.ShowModal;
end;

procedure TfrmPrincipal.AcompanhamentodosLanamentos1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmLancamentoItensFatores, frmLancamentoItensFatores);
  frmLancamentoItensFatores.ShowModal;
end;

procedure TfrmPrincipal.Atendimentos1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmFichaClinica, frmFichaClinica);
  frmFichaClinica.ShowModal;
end;

procedure TfrmPrincipal.AgendamentodeConsultas1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmAgendaConsultas, frmAgendaConsultas);
  frmAgendaConsultas.ShowMOdal;
end;

procedure TfrmPrincipal.OcorrnciasDiversas1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmRegistroAbordagemGrupal, frmRegistroAbordagemGrupal);
  frmRegistroAbordagemGrupal.ShowModal;
end;

procedure TfrmPrincipal.AvaliaodeDesempenho1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmAvaliacaoDesempenho, frmAvaliacaoDesempenho);
  frmAvaliacaoDesempenho.ShowModal;
end;

procedure TfrmPrincipal.LanarAvaliao1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmNovoLancamentoBiometrico, frmNovoLancamentoBiometrico);
  frmNovoLancamentoBiometrico.ShowModal;
end;

procedure TfrmPrincipal.ReclassificarIMC1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmReclassificarIMC, frmReclassificarIMC);
  frmReclassificarIMC.ShowModal;
end;

procedure TfrmPrincipal.AcompanhamentoBiomtrico1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmAcompanhamentoBiometrico, frmAcompanhamentoBiometrico);
  frmAcompanhamentoBiometrico.ShowModal;
end;

procedure TfrmPrincipal.AvaliaodeDesempenhoAprendiz1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmAvaliacaoDesempenhoAprendiz, frmAvaliacaoDesempenhoAprendiz);
  //frmAvaliacaoDesempenhoAprendiz.ShowModal;
  Application.CreateForm (TfrmNovaAvaliacaoAprendiz, frmNovaAvaliacaoAprendiz);
  frmNovaAvaliacaoAprendiz.ShowModal;
end;

procedure TfrmPrincipal.FaltasTeste1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmLancarFaltas_Teste, frmLancarFaltas_Teste);
  frmLancarFaltas_Teste.ShowModal;
end;

procedure TfrmPrincipal.AdolescentesConvnioEmpresa1Click(Sender: TObject);
begin
  //Selecionar os dados para o relat�rio e gr�fico
end;

procedure TfrmPrincipal.RelaodeEmpresasporAsSocialConvnio1Click(
  Sender: TObject);
begin
  Application.CreateForm (TrelAdolescentesEmpresasConvenio, relAdolescentesEmpresasConvenio);

  with dmDeca.cdsSEL_AsSocial_EmpresasConvenio do
  begin
    Close;
    Open;
    relAdolescentesEmpresasConvenio.Preview;
    relAdolescentesEmpresasConvenio.Free;
  end;

end;

procedure TfrmPrincipal.Caroscpio1Click(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Caroscopio do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      Application.CreateForm (TRelCaroscopio, relCaroscopio);
      relCaroscopio.Preview;
      relCaroscopio.Free;

    end;
  except end;
end;

procedure TfrmPrincipal.CadastrodeEscolas1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEscolas, frmEscolas);
  frmEscolas.ShowModal;
end;

procedure TfrmPrincipal.CadastrodeSriesEscolares1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmCadastroSerieEscolar, frmCadastroSerieEscolar);
  frmCadastroSerieEscolar.ShowModal;
end;

procedure TfrmPrincipal.CadastrodeSriesxEscolas1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmCadastroSeriesPorEscola, frmCadastroSeriesPorEscola);
  frmCadastroSeriesPorEscola.ShowModal;
end;

procedure TfrmPrincipal.TotalizaodeUsurioxUnidade1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmTotaisUsuarioUnidade, frmTotaisUsuarioUnidade);
  frmTotaisUsuarioUnidade.ShowModal;
end;

procedure TfrmPrincipal.RelaodeTransferidosAcessoConvnio1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmTransferenciasDAPA, frmTransferenciasDAPA);
  frmTransferenciasDAPA.ShowModal;
end;

procedure TfrmPrincipal.SatisfaodoClienteConveniados1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmSatisfacaoConveniados, frmSatisfacaoConveniados);
  frmSatisfacaoConveniados.ShowModal;
end;

procedure TfrmPrincipal.TabeladeDefasagemEscolar1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmDefasagemEscolar, frmDefasagemEscolar);
  frmDefasagemEscolar.ShowModal;
end;

procedure TfrmPrincipal.AlteraesdaVerso1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmAlteracoesVersao, frmAlteracoesVersao);
  frmAlteracoesVersao.ShowModal;
end;

procedure TfrmPrincipal.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Resize := False;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
var
hwndHandle : THANDLE;
hMenuHandle : HMENU;
begin

  //Desabilita bot�o fechar
  hwndHandle := FindWindow(nil, '[Sistema DECA] - Gest�o de Crian�as e Adolescentes - [Vers�o 7.3.1 de 26/MAR�O/2018 -> DIVIS�O DE TECNOLOGIA DA INFORMA��O]');


  if (hwndHandle <> 0) then
  begin
    hMenuHandle := GetSystemMenu(hwndHandle, FALSE);
    if (hMenuHandle <> 0) then
      DeleteMenu(hMenuHandle, SC_CLOSE, MF_BYCOMMAND);
  end;

  //Verificar se existe alguma pend�ncia a ser tratada com chefia atrav�s do Gerenciador de Solicita��es...
  if (vvIND_PERFIL = 2) or (vvIND_PERFIL = 3) then
  begin
    try
      with dmDeca.cdsSel_Solicitacao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_solicitacao').Value      := Null;
        Params.ParamByName ('@pe_ind_prioridade').Value          := Null;
        Params.ParamByName ('@pe_cod_id_tipo_solicitacao').Value := Null;
        Params.ParamByName ('@pe_flg_situacao').Value            := 0;  //Se existe alguma solicita��o "enviada" pela chefia e que ainda n�o foi "lida"
        Params.ParamByName ('@pe_cod_unidade').Value             := vvCOD_UNIDADE;
        Open;

        if RecordCount > 0 then
        begin
          //Application.CreateForm (TfrmEncaminhamentoSolicitacaoChefia,frmEncaminhamentoSolicitacaoChefia);
          //frmEncaminhamentoSolicitacaoChefia.ShowModal;
          imgPendencia.Visible := True;
          lbMensagemPendencias.Visible := True;
          frmPrincipal.Menu := mmBlank;
        end
        else
        begin
          imgPendencia.Visible := False;
          lbMensagemPendencias.Visible := False;
          frmPrincipal.Menu := mmPrincipal;
        end;
      end;
    except end;
  end
  else
  begin
    imgPendencia.Visible := False;
    lbMensagemPendencias.Visible := False;
    frmPrincipal.Menu := mmPrincipal;
  end;


end;

procedure TfrmPrincipal.VerificarPendncias1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEncaminhamentoSolicitacaoChefia, frmEncaminhamentoSolicitacaoChefia);
  frmEncaminhamentoSolicitacaoChefia.ShowModal;
end;

procedure TfrmPrincipal.GerenciarSolicitaesdeChefia1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmEncaminhamentoSolicitacaoChefia, frmEncaminhamentoSolicitacaoChefia);
  //frmEncaminhamentoSolicitacaoChefia.ShowModal;

  Application.MessageBox('Sr(a) Usu�rio(a) !!!' + #13+#10 +
                         'Este m�dulo ainda est� em desenvolvimento.' + #13+#10 +
                         'Aguarde a pr�xima vers�o para que voc� possa utiliz�-lo !!!' + #13+#10 +
                         'Desde j� agradecemos sua compreens�o.',
                         '[Sistema Deca] - Gerenciamento de Solicita��es',
                         MB_OK + MB_ICONEXCLAMATION);
end;

procedure TfrmPrincipal.CordeFundo1Click(Sender: TObject);
begin
  ColorDialog1.Execute;
  frmPrincipal.Color := clBtnFace;
  frmPrincipal.Color := ColorDialog1.Color;
  Image4.Picture := nil;
end;

procedure TfrmPrincipal.ImportaoDadosAcompEscolar1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportaEscolaCadParaAcompEscolar, frmImportaEscolaCadParaAcompEscolar);
  frmImportaEscolaCadParaAcompEscolar.ShowModal
end;

procedure TfrmPrincipal.RelaoCompara42008x120091Click(Sender: TObject);
begin
  Application.CreateForm (TfrmComparaBimestreAno, frmComparaBimestreAno);
  frmComparaBimestreAno.ShowModal;
end;

procedure TfrmPrincipal.ConsultaDadosAcompanhamentoEscolar1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmConsultaAcompEscolar, frmConsultaAcompEscolar);
  frmConsultaAcompEscolar.ShowModal;
end;

procedure TfrmPrincipal.RelaodeAlunoscomDefasagemEscolar1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaDadosDefasagemEscolar,frmSelecionaDadosDefasagemEscolar);
  frmSelecionaDadosDefasagemEscolar.ShowModal;
end;

procedure TfrmPrincipal.ResumoMensal1Click(Sender: TObject);
begin

  //Application.MessageBox ('Aten��o usu�rio!!!' + #13+#10 +
  //                        'Este m�dulo do RESUMO MENSAL est� em desenvolvimento e em 26/03/2010 estar� dispon�vel para utiliza��o.',
  //                        '[Sistema Deca] - Lan�amento de dados do Resumo Mensal',
  //                        MB_OK + MB_ICONINFORMATION);

  Application.CreateForm (TfrmResumoMensal_Teste, frmResumoMensal_Teste);
  frmResumoMensal_Teste.ShowModal;

end;

procedure TfrmPrincipal.CONTROLEDOACOMPESCOLARBOLETIM1Click(
  Sender: TObject);
begin
  //Application.CreateForm (TfrmConsultaDadosAcompEscolar, frmConsultaDadosAcompEscolar);
  //frmConsultaDadosAcompEscolar.ShowModal;
end;

procedure TfrmPrincipal.CorrigeHistoricoEscola1Click(Sender: TObject);
var
  vvESCOLA_ATUAL, vvNOM_ESCOLA_1B, vvNOM_ESCOLA_2B, vvNOM_ESCOLA_3B : String;
  vvSERIE_ATUAL, vvNOM_SERIE_1B, vvNOM_SERIE_2B, vvNOM_SERIE_3B : String;
  vvCOD_ESCOLA_ATUAL, vvCOD_ESCOLA_1B, vvCOD_ESCOLA_2B, vvCOD_ESCOLA_3B : Integer;
begin

  try

    //Selecionar TODOS do cadastro...
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := 0;
      Open;

      Panel1.Visible := True;
      ProgressBar1.Min := 0;
      ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

      while not dmDeca.cdsSel_Cadastro.eof do
      begin

        if (dmDeca.cdsSel_Cadastro.FieldByName('cod_unidade').AsInteger in [1,3,7,9,12,13,40,42,43,47,48,50,71]) then
          dmDeca.cdsSel_Cadastro.Next
        else
        begin

          //**********************************************************************
          //Para cada ficha de cadastro, recuperar o registro do 1.�Bim registrado...
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').AsString      := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString;
            Params.ParamByName ('@pe_num_ano_letivo').Value        := 2014;
            Params.ParamByName ('@pe_num_bimestre').Value          := 1;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
            Open;

            if dmDeca.cdsSel_AcompEscolar.RecordCount > 0 then
            begin
              //Recebe o nome da Escola no 1Bimestre/2013
              vvNOM_ESCOLA_1B := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
              vvNOM_SERIE_1B  := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
              vvCOD_ESCOLA_1B := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
            end
            else
            begin
              vvNOM_ESCOLA_1B := '--';
              vvNOM_SERIE_1B  := '--';
              vvCOD_ESCOLA_1B := 221;
            end;
          end;

        with dmDeca.cdsInc_HistoricoEscola do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value   := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString);
          Params.ParamByName ('@pe_cod_escola').Value      := vvCOD_ESCOLA_1B;//vvCOD_ESCOLA_ATUAL; //dmDeca.cdsSel_AcompEscolar.FieldByname ('cod_escola').Value;
          Params.ParamByName ('@pe_flg_status').Value      := 0;
          Params.ParamByName ('@pe_log_cod_usuario').Value := 15;
          Params.ParamByName ('@pe_dsc_serie').Value       := vvNOM_SERIE_1B;//vvSERIE_ATUAL; //dmDeca.cdsSel_AcompEscolar.FieldBYName ('dsc_serie').Value;
          Params.ParamByName ('@pe_num_ano_letivo').Value  := 2014;
          Execute;
        end;
        //end; //if ...

        //Limpar as vari�veis utilizadas...
        vvESCOLA_ATUAL     := '';
        vvSERIE_ATUAL      := '';
        vvCOD_ESCOLA_ATUAL := 0;

        vvNOM_ESCOLA_1B := '';
        vvNOM_ESCOLA_2B := '';
        vvNOM_ESCOLA_3B := '';

        VVNOM_SERIE_1B  := '';
        VVNOM_SERIE_2B  := '';
        VVNOM_SERIE_3B  := '';

        vvCOD_ESCOLA_1B := 0;
        vvCOD_ESCOLA_2B := 0;
        vvCOD_ESCOLA_3B := 0;

        //end;

        ProgressBar1.Position := ProgressBar1.Position + 1;
        dmDeca.cdsSel_Cadastro.Next;

      end;
    end;

    end;

    ShowMessage ('Terminou');
    ProgressBar1.Position := 0;
    Label1.Caption := 'PROCESSO DE IMPORTA��O DO HIST�RICO DE ESCOLAS COMPLETO.';
    Panel1.Visible := False;
  except
    Application.MessageBox ('Aten��o!!! ' +#13+#10 +
                            'Um erro ocorreu ao tentar atualizar os dados do Hist�rico de Escolas dos Alunos',
                            '[Sistema Deca] - Importa��o do Hist�rico de Escolas...',
                            MB_OK + MB_ICONEXCLAMATION);
    Memo1.Lines.Add ('[ERRO] - N�O IMPORTOU  ' + dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString + ' - ' + dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString );
  end;

end;

function TfrmPrincipal.SenhaInputBox(const ACaption,
  APrompt: String): String;
var
  Form         : TForm;
  Prompt       : TLabel;
  Edit         : TEdit;
  DialogUnits  : TPoint;
  ButtonTop,
  ButtonWidth,
  ButtonHeight : Integer;
  Value        : String;
  I            : Integer;
  Buffer       : Array[0..51] of Char;
begin

  Result := '';
  Form := TForm.Create(Application);
  with Form do
  begin

    try
      Canvas.Font  := Font;
      for I :=0 to 25 do Buffer[I]:= Chr(I+Ord('A'));
      for I :=0 to 25 do Buffer[I+26]:= Chr(I+Ord('a'));
      GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(DialogUnits));
      DialogUnits.X := DialogUnits.X div 52;
      BorderStyle  := bsDialog;
      Caption      := ACaption;
      ClientWidth  := MulDiv(180, DialogUnits.X, 4);
      ClientHeight := MulDiv( 63, DialogUnits.X, 8);
      Position     := poScreenCenter;
      Prompt       := TLabel.Create(Form);

      with Prompt do
      begin
        Parent   := Form;
        AutoSize := True;
        Left     := MulDiv(8, DialogUnits.X, 4);
        Top      := MulDiv(8, DialogUnits.Y, 8);
        Caption  := APrompt;
      end;

      Edit := TEdit.Create(Form);
      with Edit do
      begin
        Parent  := Form;
        Left    := Prompt.Left;
        Top     := MulDiv(19, DialogUnits.Y, 8);
        Width   := MulDiv(164, DialogUnits.X, 4);
        MaxLength := 255;
        PasswordChar := '*';
        SelectAll;
      end;

      ButtonTop    := MulDiv(41, DialogUnits.Y, 8);
      ButtonWidth  := MulDiv(50, DialogUnits.X, 4);
      ButtonHeight := MulDiv(14, DialogUnits.Y, 8);

      with TButton.Create(Form) do
      begin
        Parent      := Form;
        Caption     := 'OK';
        ModalResult := mrOk;
        Default     := True;
        SetBounds(MulDiv(38, DialogUnits.X, 4), ButtonTop, ButtonWidth, ButtonHeight);
      end;

      with TButton.Create(Form) do
      begin
        Parent      := Form;
        Caption     := 'Cancelar';
        ModalResult := mrCancel;
        Cancel      := True;
        SetBounds(MulDiv(92, DialogUnits.X, 4), ButtonTop, ButtonWidth, ButtonHeight);
      end;

      if ShowModal = mrOk then
      begin
        Value  := Edit.Text;
        Result := Value;
      end;

    finally
      Form.Free;
      Form := nil;
    end;
 
  end;
end;

procedure TfrmPrincipal.BOLETIMELETRNICO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConsultaDadosAcompEscolar, frmConsultaDadosAcompEscolar);
  frmConsultaDadosAcompEscolar.ShowModal;
end;

procedure TfrmPrincipal.CONSULTAS1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmConsultaDadosDoBoletim, frmConsultaDadosDoBoletim);
  frmConsultaDadosDoBoletim.ShowModal;
end;

procedure TfrmPrincipal.SOLICITAODETRANSFPERODOESCOLAR1Click(
  Sender: TObject);
begin

  //Somente Gestor/Ass. Social das unidades de Aprendiz
  //Conv�nio-Sede / Conv�nio Embraer / Pa�o Municipal
  Application.CreateForm (TfrmEmissaoDeclaracoesSolicitacoesEscolares,frmEmissaoDeclaracoesSolicitacoesEscolares);
  frmEmissaoDeclaracoesSolicitacoesEscolares.ShowModal;

end;

procedure TfrmPrincipal.ESTATSTICASGERAIS1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEstatisticasAcompEscolar, frmEstatisticasAcompEscolar);
  frmEstatisticasAcompEscolar.ShowModal;
end;

procedure TfrmPrincipal.ControledeFrequncias1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmNovoControleFrequencia, frmNovoControleFrequencia);
  frmNovoControleFrequencia.ShowModal;
end;

procedure TfrmPrincipal.DadosEducao1Click(Sender: TObject);
begin
frmImportaDadosBoletimAcompanhamentoEscolar.ShowModal;
end;

procedure TfrmPrincipal.FolhadeFrequncia1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmNovoControleFrequencia, frmNovoControleFrequencia);
  frmNovoControleFrequencia.ShowModal;
end;

procedure TfrmPrincipal.IMPORTAOCOTASPASSE1Click(Sender: TObject);
begin
  //Importar os dados de Cotas de passe fornecidos em arquivo pela DRH
  Application.CreateForm (TfrmImportaCotasPasse, frmImportaCotasPasse);
  frmImportaCotasPasse.ShowModal;
end;

procedure TfrmPrincipal.CONFEREDADOSDECAxDRH1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConfereDadosDECADRH, frmConfereDadosDECADRH);
  frmConfereDadosDECADRH.ShowModal;
end;

procedure TfrmPrincipal.CONFEREDADOSDRHxDECA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConfereDadosDRHDECA, frmConfereDadosDRHDECA);
  frmConfereDadosDRHDECA.ShowModal;
end;

procedure TfrmPrincipal.SAIR2Click(Sender: TObject);
begin
//Pede confirma��o para encerrar o aplicativo
  if Application.MessageBox('Deseja realmente encerrar o aplicativo?',
                            'Sistema Deca - Finalizar',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    //Atualiza o campo flg_status do usu�rio para OFF-LINE
    try
      with dmDeca.cdsAlt_Status_Usuario do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName ('@pe_flg_status').Value := 1; // 0 para On-Line e 1 para Off-Line
        Execute;
        Application.Terminate;
    end;
    except
    end;
  end
  else
  begin

  end;
end;

procedure TfrmPrincipal.FAZERLOGOFF1Click(Sender: TObject);
begin

    try
      with dmDeca.cdsAlt_Status_Usuario do
      begin
      
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName ('@pe_flg_status').Value  := 1; // 0 para On-Line e 1 para Off-Line
        Execute;
        frmLogoff.ShowModal;;
    end;
    except
    end;

end;

procedure TfrmPrincipal.RESUMODADOSMATRIZ1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmGerarDadosMatriz, frmGerarDadosMatriz);
  frmGerarDadosMatriz.ShowModal;
end;

procedure TfrmPrincipal.ALTERARSENHA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmAlteraSenhaAcesso, frmAlteraSenhaAcesso);
  frmAlteraSenhaAcesso.ShowModal;
end;

procedure TfrmPrincipal.FormResize(Sender: TObject);
begin
  //Centralizar a imagem principal no centro do formul�rio
  Image1.Left := Trunc( ( frmPrincipal.Width - Image1.Width ) / 2 );
  Image1.Top  := Trunc( ( frmPrincipal.Height - Image1.Height ) / 2 );
  Invalidate;
end;

procedure TfrmPrincipal.MATRIZDEAVALIAOSEMESTRALANUAL1Click(
  Sender: TObject);
begin
  Application.CreateForm(TfrmGeraEstatisticaMatriz, frmGeraEstatisticaMatriz);
  frmGeraEstatisticaMatriz.ShowModal;
end;

procedure TfrmPrincipal.ENVIARDADOSESCOLA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEnviarEmail, frmEnviarEmail);
  frmEnviarEmail.ShowModal;
end;

procedure TfrmPrincipal.VERIFICARAGUARDANDOINFORMAO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConfereAguardandoInformacao, frmConfereAguardandoInformacao);
  frmConfereAguardandoInformacao.ShowModal;
end;

procedure TfrmPrincipal.IMPORTAOACOMPESCOLAR201020111Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmImportaAcompEscolar20102011, frmImportaAcompEscolar20102011);
  frmImportaAcompEscolar20102011.ShowModal;
end;

procedure TfrmPrincipal.IMPORTAHISTORICO20111Click(Sender: TObject);
begin

  ShowMessage ('Come�ar...');

  //Selecionar todos do cadastro, comexce��o de INATIVOS e MAIORIDADE
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := 0;
      Open;

      while not dmDeca.cdsSel_Cadastro.eof do
      begin

        //Excluir da consulta as unidades 47 e 48
        if (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 47) or (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 48) then
        begin

          //Consulta registros do Historico de Escolas referentes ao ano de 2011 para a matricula atual
          //Se achou, procura para a proxima matricula

          with dmDeca.cdsSel_HistoricoEscola do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value  := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_cod_escola').Value     := Null;
            Params.ParamByName ('@pe_num_ano_letivo').Value := 2011;
            Open;

            if dmDeca.cdsSel_HistoricoEscola.RecordCount < 1 then
            begin

              //Selecionar o registro de 2011 do Acompanhamento Escolar e inserir no Historico de Escolas
              with dmDeca.cdsSel_AcompEscolar do
              begin
                Close;
                Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
                Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_num_ano_letivo').Value        := 2011;
                Params.ParamByName ('@pe_num_bimestre').Value          := 1;
                Params.ParamByName ('@pe_cod_unidade').Value           := Null;
                Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
                Open;

                if dmDeca.cdsSel_AcompEscolar.RecordCount > 0 then
                begin

                  //Atualiza todos registros de escolas para "Anteriores"
                  with dmDeca.cdsAlt_DadosUltimaEscolaHistoricoEscola do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                    Params.ParamByName ('@pe_flg_status').Value    := 1; //1 para escolas anteriores
                    Execute;
                  end;

                  //Insere os dados no Historico_Escolas
                  with dmDeca.cdsInc_HistoricoEscola do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_matricula').Value   := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_matricula').Value;
                    Params.ParamByName ('@pe_cod_escola').Value      := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
                    Params.ParamByName ('@pe_flg_status').Value      := 0;
                    Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                    Params.ParamByName ('@pe_dsc_serie').Value       := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
                    Params.ParamByName ('@pe_num_ano_letivo').Value  := 2011;
                    Execute;
                  end;

                end
                else if dmDeca.cdsSel_AcompEscolar.RecordCount < 1 then
                begin
                  //Nada a fazer...
                end;

              end;

            end
            else if dmDeca.cdsSel_HistoricoEscola.RecordCount > 0 then
            begin
              //Significa que j� existe registro de 2011 no Historico de Escolas
            end;

          end; //SelHistoricoEscola...

        end;

        dmDeca.cdsSel_Cadastro.Next;

      end;  //Fim while...

      ShowMessage ('Terminado...');

    end;
end;

procedure TfrmPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key = VK_F4) then Key := 0;
end;

procedure TfrmPrincipal.MonitoramentoGeral1Click(Sender: TObject);
begin
  if (vvIND_PERFIL = 18) OR (vvIND_PERFIL = 19) OR (vvIND_PERFIL = 1) then
  begin
    Application.CreateForm (TfrmGeral_AcompanhamentoEscolar,frmGeral_AcompanhamentoEscolar);
    frmGeral_AcompanhamentoEscolar.ShowModal;
  end
  else
  begin
    Application.MessageBox('Acesso n�o permitido!'+#13+#10+#13+#10+
                           'Apenas os perfis do Acompanhamento Escolar t�m acesso a esse M�dulo.',
                           'Monitoramento de Dados do Acommp. Escolar - 2014',
                           MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmPrincipal.ModeloTradicional1Click(Sender: TObject);
begin
  frmFichaCrianca := TFrmFichaCrianca.Create(Application);
  frmFichaCrianca.ShowModal;
end;

procedure TfrmPrincipal.MODELOCOMPACTO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmFichaCadastroModelo, frmFichaCadastroModelo);
  frmFichaCadastroModelo.ShowModal;
end;

procedure TfrmPrincipal.FECHAMENTO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmFechamento, frmFechamento);
  frmFechamento.ShowModal;
end;

procedure TfrmPrincipal.TOTALIZAOPORUNIDADEESITUAODS1Click(
  Sender: TObject);
begin
  Application.CreateForm(TfrmGeraTotaisDS, frmGeraTotaisDS);
  frmGeraTotaisDS.ShowModal;
end;

procedure TfrmPrincipal.LANAMENTODEDADOS1Click(Sender: TObject);
begin
  //mmPATH := ExtractFilePath(Application.ExeName) + 'RelatorioAICA.exe';
  //shellExecute(Handle,'open', PChar(mmPATH), '',nil, sw_shownormal);
  Application.CreateForm (TfrmAproveitamentoIndividual, frmAproveitamentoIndividual);
  frmAproveitamentoIndividual.ShowModal;
end;

procedure TfrmPrincipal.GERAODERELATRIO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmSelecionaAnoRAICA, frmSelecionaAnoRAICA);
  frmSelecionaAnoRAICA.ShowModal;
end;

procedure TfrmPrincipal.REGISTRODEATENDIMENTONAVE1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmNovoRegistroObservacaoAprendizagem, frmNovoRegistroObservacaoAprendizagem);
  //frmNovoRegistroObservacaoAprendizagem.ShowModal;

  Application.CreateForm (TfrmObservacaoAprendizagem,frmObservacaoAprendizagem);
  frmObservacaoAprendizagem.ShowModal;

end;

procedure TfrmPrincipal.REALIZARENCAMINHAMENTO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmEncaminhaNAVE, frmEncaminhaNAVE);
  frmEncaminhaNAVE.ShowModal;
end;

procedure TfrmPrincipal.REALIZARDESLIGAMENTONAVE1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmRetornoNAVE, frmRetornoNAVE);
  frmRetornoNAVE.ShowModal;
end;

procedure TfrmPrincipal.ALUNOSEJATELESSALA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmAlunosEjaTelessala, frmAlunosEjaTelessala);
  frmAlunosEjaTelessala.ShowModal;
end;

procedure TfrmPrincipal.Importar32011120121Click(Sender: TObject);
begin
  Application.CreateForm (TfrmImportaAcompEscolar20102011, frmImportaAcompEscolar20102011);
  frmImportaAcompEscolar20102011.ShowModal;
end;

procedure TfrmPrincipal.TRANSFERNCIADEBANCOINATIVOS1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmTransferenciaDados, frmTransferenciaDados);
  frmTransferenciaDados.ShowModal;
end;

procedure TfrmPrincipal.IMPORTAOCADASTRO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmImporta, frmImporta);
  frmImporta.ShowModal;
end;

procedure TfrmPrincipal.ADITAMENTODEAPRENDIUZ1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmAditamentoContratoAprendiz, frmAditamentoContratoAprendiz);
  frmAditamentoContratoAprendiz.ShowModal;
end;

procedure TfrmPrincipal.ADITAMENTODECONTRATOAPRENDIZ1Click(
  Sender: TObject);
begin
  Application.CreateForm (TfrmAditamentoContratoAprendiz, frmAditamentoContratoAprendiz);
  frmAditamentoContratoAprendiz.ShowModal;
end;

procedure TfrmPrincipal.COMPARATIVO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportaDocumentos, frmImportaDocumentos);
  frmImportaDocumentos.ShowModal;
end;

procedure TfrmPrincipal.COMPARARBIMESTRES1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmComparaBimestres, frmComparaBimestres);
  frmComparaBimestres.ShowModal;
end;

procedure TfrmPrincipal.FICHACLNICAODONTOLGICA2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmFichaClinica, frmFichaClinica);
  frmFichaClinica.ShowModal;
end;

procedure TfrmPrincipal.CONTROLEDEMATERIAISODONTOLGICOS1Click(
  Sender: TObject);
begin
  Application.CreateForm(TfrmEstoqueOdonto, frmEstoqueOdonto);
  frmEstoqueOdonto.ShowModal;
  //ShowMessage ('Funcionalidade em Desenvolvimento. Aguarde pr�xima vers�o !!!');

end;

procedure TfrmPrincipal.TransfernciaColetiva1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmTransferenciaColetiva,frmTransferenciaColetiva);
  frmTransferenciaColetiva.ShowModal;
end;

procedure TfrmPrincipal.SOLICITAODEUNIFORMES1Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmSolicitacaoUniformes, frmSolicitacaoUniformes);
  //frmSolicitacaoUniformes.ShowModal;
  Application.CreateForm (TfrmEstoqueUniformes, frmEstoqueUniformes);
  frmEstoqueUniformes.ShowModal;  
end;

procedure TfrmPrincipal.PROCESSOADMISSIONALAPRENDIZAGEM1Click(
  Sender: TObject);
begin
  //Application.CreateForm (TfrmProcessoAdminissionalAprendizes, frmProcessoAdminissionalAprendizes);
  //frmProcessoAdminissionalAprendizes.ShowModal;

  Application.MessageBox ('Aten��o!!!' +#13+#10+
                          'Esse m�dulo foi desativado. Para emiss�o dos contratos' + #13+#10+
                          'diversos, acesse a aba de documenta��o na Ficha de Cadastro.',
                          '[Sistema Deca] - M�dulo Inativo',
                          MB_OK + MB_ICONWARNING);

end;

procedure TfrmPrincipal.RELATRIOSEDUCACENSO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmDadosRelatorio, frmDadosRelatorio);
  frmDadosRelatorio.ShowModal;
end;

procedure TfrmPrincipal.EMISSOTERMODECOMPROMISSOBOLSISTAS1Click(
  Sender: TObject);
begin
  //Application.CreateForm (TfrmTermoBolsistas, frmTermoBolsistas);
  //frmTermoBolsistas.ShowMOdal;
  Application.MessageBox ('Aten��o!!!' +#13+#10+
                          'Esse m�dulo foi desativado. Para emiss�o dos contratos' + #13+#10+
                          'diversos, acesse a aba de documenta��o na Ficha de Cadastro.',
                          '[Sistema Deca] - M�dulo Inativo',
                          MB_OK + MB_ICONWARNING);
end;

procedure TfrmPrincipal.PASSESEVALESTRANSPORTE1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmSelecionarDadosFrequencia, frmSelecionarDadosFrequencia);
  frmSelecionarDadosFrequencia.ShowModal;
end;

procedure TfrmPrincipal.CARTOPASSECOTASEVTS1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmSelecionarDadosFrequencia, frmSelecionarDadosFrequencia);
  frmSelecionarDadosFrequencia.ShowModal;
end;

procedure TfrmPrincipal.FormPaint(Sender: TObject);
var altura, coluna: Word;
begin
  altura := (ClientHeight + 255) div 256;
  for coluna := 0 to 255 do
    with Canvas do
    begin
      //Brush.Color := RGB(50, 100, coluna); { Modifique para obter cores diferentes }
      Brush.Color := RGB(50, 0, coluna); { Modifique para obter cores diferentes }
      FillRect(Rect(0, coluna * altura, ClientWidth, (coluna + 1) * altura)) ;
    end;
end;

procedure TfrmPrincipal.TARIFASDEPASSESESCOLARES1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmCadastroValoresPasses, frmCadastroValoresPasses);
  frmCadastroValoresPasses.ShowModal;
end;

procedure TfrmPrincipal.IMPORTARDADOSEDUCACENSO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmImportaEducacensoAcompEscolar, frmImportaEducacensoAcompEscolar);
  frmImportaEducacensoAcompEscolar.ShowModal;
end;

procedure TfrmPrincipal.REATIVAODEPRONTURIO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmReativacaoProntuario, frmReativacaoProntuario);
  frmReativacaoProntuario.ShowModal;
end;

procedure TfrmPrincipal.TRANSFERNCIASPORPERODO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmTransferenciasDRH, frmTransferenciasDRH);
  frmTransferenciasDRH.ShowModal;
end;

procedure TfrmPrincipal.CURSOSECERTIFICADOS1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmCursosCertificados, frmCursosCertificados);
  frmCursosCertificados.ShowModal;
end;

procedure TfrmPrincipal.IMPORTARDADOSEDUCASENSO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportaEducacensoAcompEscolar,frmImportaEducacensoAcompEscolar);
  frmImportaEducacensoAcompEscolar.ShowModal;
end;

procedure TfrmPrincipal.EMITIRFICHADECADASTRO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmEmitirFichaCadastro, frmEmitirFichaCadastro);
  frmEmitirFichaCadastro.ShowModal;
end;

procedure TfrmPrincipal.VISUALIZARAGENDA1Click(Sender: TObject);
begin
  Application. CreateForm(TfrmConsultaAgendaOdontologica, frmConsultaAgendaOdontologica);
  frmConsultaAgendaOdontologica.ShowModal;
end;

procedure TfrmPrincipal.MIGRARRGECPFCOMPFAMILIAR1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmMigrarRGeCPF_ComposicaoFamiliar, frmMigrarRGeCPF_ComposicaoFamiliar);
  frmMigrarRGeCPF_ComposicaoFamiliar.ShowModal;
end;

procedure TfrmPrincipal.UNIDADESESCOLARESSERIESESCOLARESETURMAS1Click(
  Sender: TObject);
begin
  //Application.CreateForm(TfrmControleUnidadesEscolaresTurmasClasses, frmControleUnidadesEscolaresTurmasClasses);
  //frmControleUnidadesEscolaresTurmasClasses.ShowModal;
end;

procedure TfrmPrincipal.CADASTROS1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmControleUnidadesEscolaresTurmasClasses, frmControleUnidadesEscolaresTurmasClasses);
  frmControleUnidadesEscolaresTurmasClasses.ShowModal;
end;

procedure TfrmPrincipal.RELATRIODECLASSES1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEmitirRelatorioDeClasses, frmEmitirRelatorioDeClasses);
  frmEmitirRelatorioDeClasses.ShowModal;
end;

procedure TfrmPrincipal.RELAODEMUDANASDEVNCULO1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmConsultaMudancasVinculo, frmConsultaMudancasVinculo);
  frmConsultaMudancasVinculo.ShowModal;
end;

procedure TfrmPrincipal.TRANSFERNCIACOLETIVA2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmTransferenciaColetiva, frmTransferenciaColetiva);
  frmTransferenciaColetiva.ShowModal;
end;

procedure TfrmPrincipal.REATIVAESPORPERODO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmConsultaReativacoesProntuarios, frmConsultaReativacoesProntuarios);
  frmConsultaReativacoesProntuarios.ShowModal;
end;

procedure TfrmPrincipal.anual1Click(Sender: TObject);
begin
  if (vvIND_PERFIL = 1) OR (vvIND_PERFIL = 18) OR (vvIND_PERFIL = 19) then
  begin
    Application.CreateForm (TfrmComparaBimestreAno, frmComparaBimestreAno);
    frmComparaBimestreAno.ShowModal;
  end
  else
  begin
    Application.MessageBox('Acesso n�o permitido!'+#13+#10+#13+#10+
                           'Apenas os perfis do Acompanhamento Escolar t�m acesso a esse M�dulo.',
                           'Monitoramento de Dados do Acommp. Escolar - 2014',
                           MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmPrincipal.mensal1Click(Sender: TObject);
begin
  Application.MessageBox ('Aten��o!!!' +#13+#10+
                          'Esse m�dulo estar� dispon�vel a partir da pr�xima vers�o.' +#13+#10+
                          'Agradecemos a compreens�o!!!',
                          '[Sistema Deca] - M�dulo indispon�vel',
                          MB_OK + MB_ICONINFORMATION);
end;

procedure TfrmPrincipal.RELATRIODEDEMANDATRIAGEM20171Click(
  Sender: TObject);
begin
  Application.CreateForm(TfrmConsultaDemandaTriagemTST, frmConsultaDemandaTriagemTST);
  frmConsultaDemandaTriagemTST.ShowModal;
end;

procedure TfrmPrincipal.LEVANTAMENTODENDEATENDIDOSPORPERODO1Click(
  Sender: TObject);
begin
  Application.CreateForm(TfrmOpcaoRelatorioEstatisticasAcEscolar, frmOpcaoRelatorioEstatisticasAcEscolar);
  frmOpcaoRelatorioEstatisticasAcEscolar.ShowModal;
end;

procedure TfrmPrincipal.FICHADEINSCRIO2Click(Sender: TObject);
begin
  //Application.CreateForm (TfrmCadastroInscricoes, frmCadastroInscricoes);
  //frmCadastroInscricoes.ShowModal;
  //Chamar a tela de pesquisa inicial, de acordo com o combinado com a F�tima, em 04/10/2017
  Application.CreateForm (TfrmPESQUISA_INSCRITOS_TRIAGEM, frmPESQUISA_INSCRITOS_TRIAGEM);
  frmPESQUISA_INSCRITOS_TRIAGEM.ShowModal;
end;

procedure TfrmPrincipal.PORTARIADEINSCRIOSELEOEADMISSO1Click(
  Sender: TObject);
var mmFILE_PDF : String;
begin
  mmFILE_PDF := (ExtractFilePath(Application.Exename) + '\PORTARIA.PDF');
  ShellExecute(Handle, nil, PChar(mmFILE_PDF), nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmPrincipal.MIGRARATENDIMENTOS1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmMigrarAtendimentosParaRelatorios, frmMigrarAtendimentosParaRelatorios);
  frmMigrarAtendimentosParaRelatorios.ShowModal;
end;

procedure TfrmPrincipal.CORREODEUSURIOSTRIAGEMPDECA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmCorrecaoUsuariosRelatorios, frmCorrecaoUsuariosRelatorios);
  frmCorrecaoUsuariosRelatorios.ShowModal;
end;

procedure TfrmPrincipal.CADASTRODEBAIRROS1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmCadBairrosTriagem, frmCadBairrosTriagem);
  frmCadBairrosTriagem.ShowModal;
end;

procedure TfrmPrincipal.PROCESSOADMINISTRATIVO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmSELECIONA_DEMANDA, frmSELECIONA_DEMANDA);
  frmSELECIONA_DEMANDA.ShowModal;
end;

procedure TfrmPrincipal.EFETIVARADMISSO1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmADMISSOES, frmADMISSOES) ;
  frmADMISSOES.ShowModal;
end;

procedure TfrmPrincipal.GERARDADOSDEMANDA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmGERA_DEMANDA, frmGERA_DEMANDA);
  frmGERA_DEMANDA.ShowModal;
end;

procedure TfrmPrincipal.SEGUNDAVIADECONTRATOFICHA1Click(Sender: TObject);
begin
  Application.CreateForm (TfrmSEGUNDA_VIA_CONTRATO, frmSEGUNDA_VIA_CONTRATO);
  frmSEGUNDA_VIA_CONTRATO.ShowModal;
end;

end.
