unit uSelecionaSemAnoDesempAprendiz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask;

type
  TfrmSelecionaSemAnoDesemAprendiz = class(TForm)
    GroupBox1: TGroupBox;
    cbSemestre: TComboBox;
    mskAno: TMaskEdit;
    SpeedButton1: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaSemAnoDesemAprendiz: TfrmSelecionaSemAnoDesemAprendiz;

implementation

uses uDM, rAcompanhamentoAprendizGeralPorSemestre;

{$R *.DFM}

procedure TfrmSelecionaSemAnoDesemAprendiz.SpeedButton1Click(
  Sender: TObject);
begin

  try
    with dmDeca.cdsGeraEstatisticaDesempAprendiz do
    begin
      Close;
      Params.ParamByName ('@pe_num_semestre').Value := cbSemestre.ItemIndex + 1;
      Params.ParamByName ('@pe_num_ano').Value      := StrToInt(mskAno.Text);
      Open;

      Application.CreateForm (TrelAcompanhamentoAprendizGeralPorSemestre, relAcompanhamentoAprendizGeralPorSemestre);
      relAcompanhamentoAprendizGeralPorSemestre.qrlSemestre.Caption := cbSemestre.Text + ' Semestre de ' + mskAno.Text;
      relAcompanhamentoAprendizGeralPorSemestre.Preview;
      relAcompanhamentoAprendizGeralPorSemestre.Free;

    end;
  except end;

end;

end.
