unit uSelecionaFrequencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls;

type
  TfrmSelecionaFrequencia = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cbTurma: TComboBox;
    cbAno: TComboBox;
    cbMes: TComboBox;
    btnVer: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbTurmaClick(Sender: TObject);
    procedure cbAnoClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaFrequencia: TfrmSelecionaFrequencia;
  vTurma1, vTurma2 : TStringList;

implementation

uses uPrincipal, uDM, uUtil, rFolhaFrequencia;

{$R *.DFM}

procedure TfrmSelecionaFrequencia.FormCreate(Sender: TObject);
begin

  vTurma1 := TStringList.Create();
  vTurma2 := TStringList.Create();

  try
    with dmDeca.cdsSel_Cadastro_Turmas do
    begin
      Close;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_nom_turma').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;

      cbTurma.Clear;
      while not eof do
      begin
        vTurma1.Add (IntToStr(FieldByName ('cod_turma').AsInteger));
        vTurma2.Add (FieldByName ('nom_turma').AsString);
        cbTurma.Items.Add (FieldByName ('nom_turma').AsString);
        Next;
      end;

      vTurma2.Sort;

    end
  except end;

end;

procedure TfrmSelecionaFrequencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  vTurma1.Free;
  vTurma2.Free;
end;

procedure TfrmSelecionaFrequencia.cbTurmaClick(Sender: TObject);
begin
//Selecionar todos os anos referentes a turma
  try
    with dmDeca.cdsSel_Frequencia_Ano do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      //Params.ParamByName ('@pe_cod_turma').Value := Null;//StrToInt(vTurma1.Strings[cbTurma.ItemIndex]);
      Open;

      cbAno.Clear;
      while not eof do
      begin
        cbAno.Items.Add (IntToStr(FieldByName ('Ano').AsInteger));
        Next;
      end;

    end
  except end;
end;

procedure TfrmSelecionaFrequencia.cbAnoClick(Sender: TObject);
begin
try
  with dmDeca.cdsSel_Frequencia_Mes do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
    Params.ParamByName ('@pe_cod_turma').Value := StrToInt(vTurma1.Strings[cbTurma.ItemIndex]);
    Params.ParamByName ('@pe_num_ano').Value := Null; //StrToInt(cbAno.Text);
    Open;

    cbMes.Clear;
    while not eof do
    begin
      cbMes.Items.Add (FieldByName ('Mes').AsString);
      Next;
    end;

    btnVer.Enabled := True;
  end
except end;
end;

procedure TfrmSelecionaFrequencia.btnVerClick(Sender: TObject);
begin
  //ShowMessage(IntToStr(StrToInt(vTurma1.Strings[cbTurma.ItemIndex])) + ' - ' + cbAno.Text + ' - ' + cbMes.Text);

  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin
      Close;
      Params.ParamByName ('@pe_id_frequencia').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName ('@pe_num_ano').Value := StrToInt(cbAno.Text);

      if cbMes.Text = 'JANEIRO' then Params.ParamByName ('@pe_num_mes').Value := 1;
      if cbMes.Text = 'FEVEREIRO' then Params.ParamByName ('@pe_num_mes').Value := 2;
      if cbMes.Text = 'MAR�O' then Params.ParamByName ('@pe_num_mes').Value := 3;
      if cbMes.Text = 'ABRIL' then Params.ParamByName ('@pe_num_mes').Value := 4;
      if cbMes.Text = 'MAIO' then Params.ParamByName ('@pe_num_mes').Value := 5;
      if cbMes.Text = 'JUNHO' then Params.ParamByName ('@pe_num_mes').Value := 6;
      if cbMes.Text = 'JULHO' then Params.ParamByName ('@pe_num_mes').Value := 7;
      if cbMes.Text = 'AGOSTO' then Params.ParamByName ('@pe_num_mes').Value := 8;
      if cbMes.Text = 'SETEMBRO' then Params.ParamByName ('@pe_num_mes').Value := 9;
      if cbMes.Text = 'OUTUBRO' then Params.ParamByName ('@pe_num_mes').Value := 10;
      if cbMes.Text = 'NOVEMBRO' then Params.ParamByName ('@pe_num_mes').Value := 11;
      if cbMes.Text = 'DEZEMBRO' then Params.ParamByName ('@pe_num_mes').Value := 12;

      Params.ParamByName ('@pe_cod_turma').Value := StrToInt(vTurma1.Strings[cbTurma.ItemIndex]);
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;

      Application.CreateForm (TrelFolhaFrequencia, relFolhaFrequencia);
      relFolhaFrequencia.lbMesAno.Caption := cbMes.Text + '/' + cbAno.Text;
      relFolhaFrequencia.lbTitulo.Caption := 'FOLHA DE FREQU�NCIA  ' + vvNOM_UNIDADE;

      with dmDeca.cdsSel_Cadastro_Turmas do
      begin
        Close;
        Params.ParamByName ('@pe_cod_turma').Value := Null;
        Params.ParamByName ('@pe_nom_turma').Value := cbTurma.Text;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Open;

        relFolhaFrequencia.lbTurma.Caption := cbTurma.Text + '     Professor(a): ' + FieldByName ('nom_professor').AsString + '     Per�odo: ' + FieldByname ('pPeriodo').AsString;

      end;

      relFolhaFrequencia.Preview;
      relFolhaFrequencia.Free;

      btnVer.Enabled := False;

      cbTurma.ItemIndex := -1;
      cbAno.ItemIndex := -1;
      cbMes.ItemIndex := -1;

    end
  except end;


end;

end.


