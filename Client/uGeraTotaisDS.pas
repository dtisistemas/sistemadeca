unit uGeraTotaisDS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, jpeg, ExtCtrls, Buttons, Mask, ComCtrls, Grids, DBGrids,
  TeeProcs, TeEngine, Chart, DBChart, Db;

type
  TfrmGeraTotaisDS = class(TForm)
    Image1: TImage;
    GroupBox1: TGroupBox;
    cbMes: TComboBox;
    Label1: TLabel;
    mskAno: TMaskEdit;
    btnConsultar: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    MaskEdit1: TMaskEdit;
    DBChart1: TDBChart;
    DBGrid1: TDBGrid;
    SpeedButton1: TSpeedButton;
    dsEstatisticasDS: TDataSource;
    TabSheet3: TTabSheet;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    cbMes2: TComboBox;
    mskAno2: TMaskEdit;
    btnVer: TSpeedButton;
    DBGrid2: TDBGrid;
    btnVerRelatorio: TSpeedButton;
    cbSemana: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnVerRelatorioClick(Sender: TObject);
    function MyWeekOfMonth(date: TDateTime): Integer;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraTotaisDS: TfrmGeraTotaisDS;
  mmMES : Integer;
  mmANO : String;
  mmNUM_SEMANA : Integer;

implementation

uses uDM, uPrincipal, rTotalizacaoPorUnidade_DS;

{$R *.DFM}

procedure TfrmGeraTotaisDS.FormShow(Sender: TObject);
begin
  //Carrega o m�s e o ano para os campos do formul�rio
  mmMES := StrToInt(Copy(DateToStr(Date()),4,2));
  mmANO := Copy(DateToStr(Date()),7,4);

  cbMes.ItemIndex := mmMES;
  mskAno.Text := mmANO;


end;

procedure TfrmGeraTotaisDS.btnConsultarClick(Sender: TObject);
begin

  try

    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;
    end;

    //Exclui os dados do usu�rio logado referentes aos mes/ano atual e n�o o selecionado no combo
    with dmDeca.cdsExc_EstatisticaDS do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
      Params.ParamByName ('@pe_num_mes').Value     := mmMES;
      Params.ParamByName ('@pe_num_ano').Value     := mmANO;
      Params.ParamByName ('@pe_num_semana').Value  := MyWeekOfMonth(Date());
      Execute;
    end;

    while not dmDeca.cdsSel_Unidade.eof do
    begin

      //Calcula os totais da unidade atual
      with dmDeca.cdsGeraTotaisPorUnidadeSituacao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
        Open;
      end;

      if dmDeca.cdsGeraTotaisPorUnidadeSituacao.RecordCount > 0 then
      begin
        //Insere os dados no banco de dados -> tabela ESTATISTICASDS
        with dmDeca.cdsInc_EstatisticaDS do
        begin
          Close;
          Params.ParamByName ('@pe_cod_unidade').Value    := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
          Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
          Params.ParamByName ('@pe_num_mes').Value        := mmMES;
          Params.ParamByName ('@pe_num_ano').Value        := StrToInt(mmANO);
          Params.ParamByName ('@pe_num_total').Value      := dmDeca.cdsGeraTotaisPorUnidadeSituacao.FieldByName ('Total').Value;
          Params.ParamByName ('@pe_num_ativos').Value     := dmDeca.cdsGeraTotaisPorUnidadeSituacao.FieldByName ('Ativos').Value;
          Params.ParamByName ('@pe_num_suspensos').Value  := dmDeca.cdsGeraTotaisPorUnidadeSituacao.FieldByName ('Suspensos').Value;
          Params.ParamByName ('@pe_num_afastados').Value  := dmDeca.cdsGeraTotaisPorUnidadeSituacao.FieldByName ('Afastados').Value;
          Params.ParamByName ('@pe_num_em_transf').Value  := dmDeca.cdsGeraTotaisPorUnidadeSituacao.FieldByName ('EmTransferencia').Value;
          Params.ParamByName ('@pe_num_semana').Value     := MyWeekOfMonth(Date());
          Execute;
        end;
      end;
      dmDeca.cdsSel_Unidade.Next;
    end;

    with dmDeca.cdsSel_EstatisticaDS do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value      := Null;
      Params.ParamByName ('@pe_num_ano').Value          := mmANO;
      Params.ParamByName ('@pe_num_mes').Value          := mmMES;
      Params.ParamByName ('@pe_ind_tipo_unidade').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value      := vvCOD_USUARIO;
      Params.ParamByName ('@pe_num_semana').Value       := MyWeekOfMonth(Date());
      Open;
    end;

  except end;

end;

procedure TfrmGeraTotaisDS.SpeedButton1Click(Sender: TObject);
begin
  Application.CreateForm (TrelTotalizacaoPorUnidade_DS, relTotalizacaoPorUnidade_DS);
  relTotalizacaoPorUnidade_DS.qrlNomeUsuario.Caption := vvNOM_USUARIO;
  relTotalizacaoPorUnidade_DS.qrlPeriodo.Caption     := cbMes.Text + '/' + mskAno.Text + '  -  Semana: ' + IntToStr( ( MyWeekOfMonth( Date() ) ) );
  relTotalizacaoPorUnidade_DS.Preview;
  relTotalizacaoPorUnidade_DS.Free;
end;

procedure TfrmGeraTotaisDS.btnVerClick(Sender: TObject);
begin
    with dmDeca.cdsSel_EstatisticaDS do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value      := Null;
      Params.ParamByName ('@pe_num_ano').Value          := StrToInt(mskAno2.Text);
      Params.ParamByName ('@pe_num_mes').Value          := cbMes2.ItemIndex;
      Params.ParamByName ('@pe_ind_tipo_unidade').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value      := vvCOD_USUARIO;
      Params.ParamByName ('@pe_num_semana').Value       := cbSemana.ItemIndex + 1;
      Open;
    end;
end;

procedure TfrmGeraTotaisDS.btnVerRelatorioClick(Sender: TObject);
begin
  Application.CreateForm (TrelTotalizacaoPorUnidade_DS, relTotalizacaoPorUnidade_DS);
  relTotalizacaoPorUnidade_DS.qrlNomeUsuario.Caption := vvNOM_USUARIO;
  relTotalizacaoPorUnidade_DS.qrlPeriodo.Caption     := cbMes2.Text + '/' + mskAno2.Text + '  -  Semana: ' + cbSemana.Text;
  relTotalizacaoPorUnidade_DS.Preview;
  relTotalizacaoPorUnidade_DS.Free;
end;

function TfrmGeraTotaisDS.MyWeekOfMonth(date: TDateTime): Integer;
var
  y,m,d : Word;
  firstofmonth, firstthursday, FirstWeekStart : TDateTime;
  h : Integer;
begin
  DecodeDate(date,y,m,d);
  firstofmonth := EncodeDate(y,m,1);
  h := DayOfWeek(firstofmonth);
  firstthursday := firstofmonth+((12-h) mod 7);
  FirstWeekStart := firstthursday - 3;
  if trunc(date) < FirstWeekStart then
    result :=MyWeekOfMonth(firstofmonth-1)
  else
    result := (round(trunc(date)-FirstWeekStart) div 7) + 1;

  if m = 12 then
  begin
    inc(y);
    m := 0;
  end;
end;

end.
