unit uChamadoTecnico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls;

type
  TfrmChamadoTecnico = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edPatrimonio: TMaskEdit;
    lbPatrimonio: TLabel;
    btnPesquisa: TSpeedButton;
    Panel2: TPanel;
    Label2: TLabel;
    edUnidade: TEdit;
    Label3: TLabel;
    meDefeitoReclamado: TMemo;
    Label4: TLabel;
    edDataChamado: TMaskEdit;
    Label5: TLabel;
    edUsuario: TEdit;
    Label6: TLabel;
    cbTecnico: TComboBox;
    Panel3: TPanel;
    btnNovo: TSpeedButton;
    btnGravar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnSair: TSpeedButton;
    btnVerEquipamentos: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmChamadoTecnico: TfrmChamadoTecnico;

implementation

uses uPrincipal;

{$R *.DFM}

procedure TfrmChamadoTecnico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmChamadoTecnico.FormShow(Sender: TObject);
begin
  edUnidade.Text := vvNOM_UNIDADE;
  edUsuario.Text := vvNOM_USUARIO;
end;

end.
