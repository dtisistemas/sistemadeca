object frmREMARCACAO: TfrmREMARCACAO
  Left = 610
  Top = 317
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Sistema Deca - [Remarca��o de Consulta]'
  ClientHeight = 119
  ClientWidth = 589
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 3
    Top = 3
    Width = 88
    Height = 48
    Caption = '[Matricula]'
    TabOrder = 0
    object lbMatricula: TLabel
      Left = 10
      Top = 20
      Width = 56
      Height = 15
      Caption = '00000000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 94
    Top = 3
    Width = 490
    Height = 48
    Caption = '[Nome do Paciente]'
    TabOrder = 1
    object lbNomePaciente: TLabel
      Left = 10
      Top = 20
      Width = 257
      Height = 15
      Caption = 'XXXXX XXXXXX XXXXX XXXXX XXXXXXXXXXXXXX'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox3: TGroupBox
    Left = 3
    Top = 52
    Width = 582
    Height = 63
    Caption = '[Dados para remarca��o da consulta]'
    TabOrder = 2
    object Label1: TLabel
      Left = 41
      Top = 25
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object Label2: TLabel
      Left = 193
      Top = 25
      Width = 34
      Height = 13
      Caption = 'Hor�rio'
    end
    object btnVERIFICA_DISPONIBILIDADE: TSpeedButton
      Left = 308
      Top = 13
      Width = 265
      Height = 39
      Caption = 'Verificar Disponibilidade e Remarcar'
      OnClick = btnVERIFICA_DISPONIBILIDADEClick
    end
    object mskHORA_REMARCAR: TMaskEdit
      Left = 231
      Top = 22
      Width = 67
      Height = 21
      EditMask = '99:99'
      MaxLength = 5
      TabOrder = 0
      Text = '  :  '
    end
    object mskDATA_REMARCAR: TMaskEdit
      Left = 71
      Top = 23
      Width = 92
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
  end
end
