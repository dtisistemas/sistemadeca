unit uRegistroAtendimentoFamilia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ComCtrls, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmRegistroAtendimentoFamilia = class(TForm)
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    txtOcorrencia: TMemo;
    txtData: TMaskEdit;
    cbClassificacao: TComboBox;
    GroupBox3: TGroupBox;
    dbgFamiliaresReg: TDBGrid;
    dsCadastro_Familia: TDataSource;
    chkIncluir: TCheckBox;
    btnRegistrar: TBitBtn;
    Panel2: TPanel;
    lbIdentificacao: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure AtualizaCamposFamilia(Modo: String);
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnRegistrarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure txtOcorrenciaEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroAtendimentoFamilia: TfrmRegistroAtendimentoFamilia;
  i : Integer;
implementation

uses uDM, uFichaCrianca, uUtil, uFichaPesquisa, uPrincipal;

{$R *.DFM}

procedure TfrmRegistroAtendimentoFamilia.AtualizaCamposFamilia(
  Modo: String);
begin
  if Modo = 'Padr�o' then
  begin

    PageControl1.ActivePageIndex := 0;
    txtMatricula.Clear;
    lbNome.Caption := '';
    GroupBox1.Enabled := False;
    dbgFamiliaresReg.DataSource := nil;
    GroupBox3.Enabled := False;

    btnRegistrar.Enabled := False;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True

  end
  else if Modo = 'Novo' then
  begin

    GroupBox1.Enabled := True;
    GroupBox3.Enabled := True;

    btnRegistrar.Enabled := True;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False

  end;
end;

procedure TfrmRegistroAtendimentoFamilia.btnPesquisarClick(
  Sender: TObject);
begin

  vListaClassificacao1 := TStringList.Create();
  vListaClassificacao2 := TStringList.Create();

  //Chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        lbIdentificacao.Caption := FieldByName('cod_matricula').AsString + ' - ' + FieldByName('nom_nome').AsString;
        //txtData.SetFocus;

        //Carrega o combo com as op��es de classifica��o de atendimento para fam�lia
        try
          with dmDeca.cdsSel_Classificacao_Atendimento do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
            Params.ParamByName('@pe_flg_crianca').Value := Null;
            Params.ParamByName('@pe_flg_adolescente').Value := Null;
            Params.ParamByName('@pe_flg_familia').Value := 1;
            Params.ParamByName('@pe_flg_profissional').Value := Null;
            Open;

            cbClassificacao.Clear;
            while not eof do
            begin
              vListaClassificacao1.Add(IntToStr(FieldByName('cod_id_classificacao').Value));
              vListaClassificacao2.Add(FieldByName('dsc_classificacao').Value);
              cbClassificacao.Items.Add(FieldByName('dsc_classificacao').Value);
              Next;
            end;
          end
        except end;

        //Carrega a rela��o de compos��o familiar da matr�cula informada
        try
          with dmDeca.cdsSel_Cadastro_Familia do
          begin
            Close;
            Params.ParamByName('@pe_cod_familiar').Value := Null;
            Params.ParamByName('@pe_cod_matricula').Value := vvCOD_MATRICULA_PESQUISA;
            Open;
            dbgFamiliaresReg.DataSource := dsCadastro_Familia;
          end;
        except end;
      end;
    except end;
  end;
end;

procedure TfrmRegistroAtendimentoFamilia.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  // n�o permite a troca de TabSheet se estiver editando a p�gina atual
  if (Length(Trim(txtMatricula.Text))=0) then AllowChange := false;
end;

procedure TfrmRegistroAtendimentoFamilia.FormShow(Sender: TObject);
begin
  AtualizaCamposFamilia('Padr�o');
end;

procedure TfrmRegistroAtendimentoFamilia.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposFamilia('Novo');
end;

procedure TfrmRegistroAtendimentoFamilia.btnCancelarClick(Sender: TObject);
begin
  lbIdentificacao.Caption := '';
  AtualizaCamposFamilia('Padr�o');
end;

procedure TfrmRegistroAtendimentoFamilia.btnSairClick(Sender: TObject);
begin
  frmRegistroAtendimentoFamilia.Close;
end;

procedure TfrmRegistroAtendimentoFamilia.btnRegistrarClick(
  Sender: TObject);
begin
  btnRegistrar.Enabled := False;
  PageControl1.ActivePageIndex := 1;
  txtOcorrencia.Clear;
  if (btnRegistrar.Enabled) then PageControl1.ActivePageIndex := 1;
end;

procedure TfrmRegistroAtendimentoFamilia.btnSalvarClick(Sender: TObject);
begin
  //Verifica se selecionou pelo menos um familiar,
  //se a data foi preenchida e se selecionou a classifica��o do atendimento
  if (dbgFamiliaresReg.SelectedRows.Count > 0) or ((Length(txtData.Text) > 0) and (cbClassificacao.ItemIndex > -1)) then
  begin

    //with dmDeca.cdsSel_Cadastro_Familia do
    //begin

      //Pega os nomes das pessoas a serem atendidas
      //txtOcorrencia.Text := cbClassificacao.Text;
      //for i:=0 to (dbgFamiliaresReg.SelectedRows.Count - 1) do
      //begin
      //  GotoBookmark(Pointer(dbgFamiliaresReg.SelectedRows.Items[i]));
      //  txtOcorrencia.Text := txtOcorrencia.Text + '[' + dmDeca.cdsSel_Cadastro_Familia.Fields[21].AsString + ']-'  + dmDeca.cdsSel_Cadastro_Familia.Fields[4].AsString;
      //end;
    //end;


    //Faz a inclus�o no hist�rico do prontu�rio da crian�a/adolescente
    with dmDeca.cdsInc_Cadastro_Historico do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').AsString := GetValue(txtMatricula.Text);

      if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 9) or
         (vvIND_PERFIL = 10) or (vvIND_PERFIL = 11) or (vvIND_PERFIL = 12) or (vvIND_PERFIL = 13) or (vvIND_PERFIL = 14) or (vvIND_PERFIL = 15) or
         (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then
        Params.ParamByName ('@pe_cod_unidade').AsInteger := 1
      else
        Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;

      Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
      Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 12;
      Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Registro de Atendimento';

      //Repassa valores dos par�metros quando for Assistente Social
      if (vvIND_PERFIL = 3) or (vvIND_PERFIL = 11) then
      begin
        Params.ParamByName('@pe_ind_tipo').Value := Null;
        Params.ParamByName('@pe_ind_classificacao').AsString := cbClassificacao.Text;
        Params.ParamByName('@pe_qtd_participantes').Value := Null;//StrToInt(txtQtdParticipantes.Text);
        Params.ParamByName('@pe_num_horas').Value := Null;//StrToFloat(txtNumHoras.Text);
      end
      else
      begin
        Params.ParamByName('@pe_ind_tipo').Value := Null;
        Params.ParamByName('@pe_ind_classificacao').Value := Null;
        Params.ParamByName('@pe_qtd_participantes').Value := Null;
        Params.ParamByName('@pe_num_horas').Value := Null;
      end;

      Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
      Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Execute;
    end;

    //Faz a inclus�o dos dados de apenas UM REGISTRO na tabela INDICADORES para Fam�lia
    try
      with dmDeca.cdsInc_Indicador do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_classificacao').Value := StrToInt(vListaClassificacao1.Strings[cbClassificacao.ItemIndex]);
        Params.ParamByName('@pe_ind_tipo_classificacao').Value := 2; //Fam�lia
        Params.ParamByName('@pe_dat_registro').Value := GetValue(txtData, vtDate);
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName('@pe_dsc_ocorrencia').Value := Null;
        Execute;
      end;
    except end;


    {
    //*************************************************************************************************************************************************
    //Verifica se o checkbox est� marcado, indicando que a c�a/adolescente tb ter� um registro a ser gravado
    if (chkIncluir.Checked) then
    begin
      //Incluir os dados no Hist�rico
      with dmDeca.cdsInc_Cadastro_Historico do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').AsString := GetValue(txtMatricula.Text);

        if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 9) or
           (vvIND_PERFIL = 10) or (vvIND_PERFIL = 11) or (vvIND_PERFIL = 12) or (vvIND_PERFIL = 13) or (vvIND_PERFIL = 14) or (vvIND_PERFIL = 15)then
          Params.ParamByName ('@pe_cod_unidade').AsInteger := 1
        else
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;

        Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
        Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 12;
        Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Registro de Atendimento';

        //Repassa valores dos par�metros quando for Assistente Social
        if (vvIND_PERFIL = 3) or (vvIND_PERFIL = 11) then
        begin
          Params.ParamByName('@pe_ind_tipo').Value := Null;
          Params.ParamByName('@pe_ind_classificacao').Value := Null;
          Params.ParamByName('@pe_qtd_participantes').Value := Null;//StrToInt(txtQtdParticipantes.Text);
          Params.ParamByName('@pe_num_horas').Value := Null;//StrToFloat(txtNumHoras.Text);
        end
        else
        begin
          Params.ParamByName('@pe_ind_tipo').Value := Null;
          Params.ParamByName('@pe_ind_classificacao').Value := Null;
          Params.ParamByName('@pe_qtd_participantes').Value := Null;
          Params.ParamByName('@pe_num_horas').Value := Null;
        end;

        //Params.ParamByName('@pe_dsc_ocorrencia').Value := cbClassificacao.Text + ' a ' + dmDeca.cdsSel_Cadastro_Familia.Fields[21].AsString + ' / ' +
        //                                                  dmDeca.cdsSel_Cadastro_Familia.Fields[4].AsString + '. ' + #13#10 +
        //                                                  GetValue(txtOcorrencia.Text);
        Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
        Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
        Execute;
      end;

      //Incluir os dados nos INDICADORES
      try
        with dmDeca.cdsInc_Indicador do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_classificacao').Value := StrToInt(vListaClassificacao1.Strings[cbClassificacao.ItemIndex]);
          Params.ParamByName('@pe_ind_tipo_classificacao').Value := 2; //Fam�lia
          Params.ParamByName('@pe_dat_registro').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
          Execute;
        end;
      except end;
    end;
    //***************************************************************************************************************************************
    }

    ShowMessage ('Os dados foram lan�ados no Hist�rico e gerados os devidos indicadores de atendimento...');
    lbIdentificacao.Caption := '';
    AtualizaCamposFamilia('Padr�o');

  end;
end;

procedure TfrmRegistroAtendimentoFamilia.txtDataExit(Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(txtData.Text);

    if StrToDate(txtData.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtData.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtData.SetFocus;
    end;
  end;
end;

procedure TfrmRegistroAtendimentoFamilia.txtOcorrenciaEnter(
  Sender: TObject);
var n : integer;
begin
  with dmDeca.cdsSel_Cadastro_Familia do
  begin
    //Pega os nomes das pessoas a serem atendidas
    txtOcorrencia.Text := 'Registro de ' + cbClassificacao.Text;
    txtOcorrencia.Lines.Add('');
    for n:=0 to (dbgFamiliaresReg.SelectedRows.Count - 1) do
    begin
      GotoBookmark(Pointer(dbgFamiliaresReg.SelectedRows.Items[n]));
      txtOcorrencia.Text := txtOcorrencia.Text + '[' + dmDeca.cdsSel_Cadastro_Familia.Fields[23].AsString +
                            ']-' + dmDeca.cdsSel_Cadastro_Familia.Fields[4].AsString;
      txtOcorrencia.Lines.Add('');
    end;

    //Adiciona o tipo de atendimento a descri��o da ocorr�ncia se o checkBox estiver "marcado"
    if (chkIncluir.Checked) then
    begin
      txtOcorrencia.Text := txtOcorrencia.Text + '[PP]-' + lbNome.Caption;
    end;
  end;
end;

end.
