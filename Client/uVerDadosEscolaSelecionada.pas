unit uVerDadosEscolaSelecionada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, jpeg, ExtCtrls, Buttons, Psock, NMsmtp, Db, DBCtrls, Mask;

type
  TfrmVerDadosEscolaSelecionada = class(TForm)
    Panel1: TPanel;
    SpeedButton2: TSpeedButton;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    edNomeEscola: TEdit;
    Label2: TLabel;
    edEnderecoEscola: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edbairroEscola: TEdit;
    edCepEscola: TEdit;
    edTelefoneEscola: TEdit;
    edDiretorEscola: TEdit;
    edEmailEscola: TEdit;
    btnEnviarEmail: TSpeedButton;
    Label8: TLabel;
    NMSMTP1: TNMSMTP;
    procedure SpeedButton2Click(Sender: TObject);
    procedure Label8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVerDadosEscolaSelecionada: TfrmVerDadosEscolaSelecionada;

implementation

uses uDM, uEscolas, uFichaCrianca;

{$R *.DFM}

procedure TfrmVerDadosEscolaSelecionada.SpeedButton2Click(Sender: TObject);
begin
  frmVerDadosEscolaSelecionada.Close;
end;

procedure TfrmVerDadosEscolaSelecionada.Label8Click(Sender: TObject);
begin
  Application.CreateForm (TfrmEscolas, frmEscolas);
  frmEscolas.ShowModal;
end;

end.
