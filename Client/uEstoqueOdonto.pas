unit uEstoqueOdonto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Buttons, Grids, DBGrids, Db;

type
  TfrmEstoqueOdonto = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GroupBoxA: TGroupBox;
    GroupBoxB: TGroupBox;
    GroupBoxD: TGroupBox;
    GroupBoxE: TGroupBox;
    dbgMateriais: TDBGrid;
    cbTipoMaterial: TComboBox;
    cbUnidadeEstoque: TComboBox;
    edQtdMinima: TEdit;
    edQtdAtual: TEdit;
    Panel1: TPanel;
    btnNovoMaterial: TSpeedButton;
    btnGravaMaterial: TSpeedButton;
    btnAlteraMaterial: TSpeedButton;
    btnCancelaMaterial: TSpeedButton;
    btnSolicitaCompra: TSpeedButton;
    btnEntradaMateriais: TSpeedButton;
    btnSair: TSpeedButton;
    GroupBoxC: TGroupBox;
    edDescricaoMaterial: TEdit;
    GroupBox1: TGroupBox;
    cbTipoMaterialE: TComboBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    edDescricaoMaterialE: TEdit;
    GroupBox4: TGroupBox;
    edQtdMinimaE: TEdit;
    GroupBox5: TGroupBox;
    edQtdAtualE: TEdit;
    dbgMateriaisE: TDBGrid;
    edUnidadeEstoqueE: TEdit;
    GroupBox6: TGroupBox;
    edEntrada: TEdit;
    GroupBox7: TGroupBox;
    edDataEntrada: TEdit;
    btnConfirmaEntrada: TSpeedButton;
    SpeedButton3: TSpeedButton;
    dsMateriaisOdonto: TDataSource;
    GroupBox8: TGroupBox;
    cbTipoMaterialS: TComboBox;
    GroupBox9: TGroupBox;
    edUnidadeEstoqueS: TEdit;
    GroupBox10: TGroupBox;
    edDescricaoMaterialS: TEdit;
    GroupBox11: TGroupBox;
    edQtdMinimaS: TEdit;
    GroupBox12: TGroupBox;
    edQtdAtualS: TEdit;
    dbgMateriaisS: TDBGrid;
    GroupBox13: TGroupBox;
    edSaida: TEdit;
    GroupBox14: TGroupBox;
    edDataSaida: TEdit;
    btnConfirmaSaida: TSpeedButton;
    SpeedButton6: TSpeedButton;
    dbgEntradasEstoqueMaterialX: TDBGrid;
    dsMateriaisOdontoEntrada: TDataSource;
    SpeedButton1: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure ModoTela(Modo: String);
    procedure btnCancelaMaterialClick(Sender: TObject);
    procedure btnNovoMaterialClick(Sender: TObject);
    procedure btnGravaMaterialClick(Sender: TObject);
    procedure cbTipoMaterialEClick(Sender: TObject);
    procedure dbgMateriaisECellClick(Column: TColumn);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnConfirmaEntradaClick(Sender: TObject);
    procedure btnConfirmaSaidaClick(Sender: TObject);
    procedure edSaidaExit(Sender: TObject);
    procedure cbTipoMaterialSClick(Sender: TObject);
    procedure dbgMateriaisSCellClick(Column: TColumn);
    procedure VerificaSaida;
    private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstoqueOdonto: TfrmEstoqueOdonto;
  vListaTipoMaterial1, vListaTipoMaterial2 : TStringList;
  vvMUDA_ABAE : Boolean;
  vvCOD_MATERIAL : Integer;
  vIndex, vIndexS : Integer;
  vvQTD_MINIMA_ESTOQUE, vvQTD_SAIDA : Integer;

implementation

uses uDM;

{$R *.DFM}


procedure TfrmEstoqueOdonto.ModoTela(Modo: String);
begin
  if Modo = 'P' then
  begin
    vvMUDA_ABAE := True;
    cbTipoMaterial.ItemIndex   := -1;
    cbUnidadeEstoque.ItemIndex := -1;
    edDescricaoMaterial.Clear;
    edQtdMinima.Clear;
    edQtdAtual.Clear;

    GroupBoxA.Enabled := False;
    GroupBoxB.Enabled := False;
    GroupBoxC.Enabled := False;
    GroupBoxD.Enabled := False;
    GroupBoxE.Enabled := False;

    btnNovoMaterial.Enabled    := True;
    btnGravaMaterial.Enabled   := False;
    btnAlteraMaterial.Enabled  := False;
    btnCancelaMaterial.Enabled := False;
    btnSair.Enabled            := True;
  end
  else if Modo = 'N' then
  begin
    vvMUDA_ABAE := False;
    GroupBoxA.Enabled := True;
    GroupBoxB.Enabled := True;
    GroupBoxC.Enabled := True;
    GroupBoxD.Enabled := True;
    GroupBoxE.Enabled := True;

    btnNovoMaterial.Enabled    := False;
    btnGravaMaterial.Enabled   := True;
    btnAlteraMaterial.Enabled  := False;
    btnCancelaMaterial.Enabled := True;
    btnSair.Enabled            := False;
  end;
end;

procedure TfrmEstoqueOdonto.FormShow(Sender: TObject);
begin

  vvMUDA_ABAE := True;

  ModoTela('P');

  vListaTipoMaterial1 := TStringList.Create();
  vListaTipoMaterial2 := TStringList.Create();
  cbTipoMaterial.Clear;
  cbTipoMaterialE.Clear;
  cbTipoMaterialS.Clear;

  try
    with dmDeca.cdsSel_MateriaisOdontoTIPO do
    begin
      Close;
      Params.ParamByName ('@pe_cod_tipo_material').Value := Null;
      Params.ParamByName ('@pe_dsc_tipo_material').Value := Null;
      Open;

      while not eof do
      begin
        cbTipoMaterial.Items.Add (FieldByName ('dsc_tipo_material').Value);
        cbTipoMaterialE.Items.Add (FieldByName ('dsc_tipo_material').Value);
        cbTipoMaterialS.Items.Add (FieldByName ('dsc_tipo_material').Value);
        vListaTipoMaterial1.Add  (IntToStr(FieldByName ('cod_tipo_material').Value));
        vListaTipoMaterial2.Add  (FieldByName ('dsc_tipo_material').Value);
        Next;
      end;

    end;

    with dmDeca.cdsSel_MateriaisOdonto do
    begin
      Close;
      Params.ParamByName ('@pe_cod_material').Value      := Null;
      Params.ParamByName ('@pe_cod_tipo_material').Value := Null;
      Params.ParamByName ('@pe_qtd_minima').Value        := Null;
      Params.ParamByName ('@pe_dsc_material').Value      := Null;
      Open;

      dbgMateriais.Refresh;
    end;

  except end;

end;

procedure TfrmEstoqueOdonto.btnCancelaMaterialClick(Sender: TObject);
begin
  ModoTela('P');
end;

procedure TfrmEstoqueOdonto.btnNovoMaterialClick(Sender: TObject);
begin
  ModoTela('N');
  edQtdMinima.Text := '0';
  edQtdAtual.Text  := '0';
end;

procedure TfrmEstoqueOdonto.btnGravaMaterialClick(Sender: TObject);
begin
  try

    //Verifica se o material j� est� cadastrado ...
    with dmDeca.cdsSel_MateriaisOdonto do
    begin
      Close;
      Params.ParamByName ('@pe_cod_material').Value      := Null;
      Params.ParamByName ('@pe_cod_tipo_material').Value := StrToInt(vListaTipoMaterial1.Strings[cbTipoMaterial.ItemIndex]);
      Params.ParamByName ('@pe_qtd_minima').Value        := Null;
      Params.ParamByName ('@pe_dsc_material').Value      := Trim(edDescricaoMaterial.Text);
      Open;

      if not dmDeca.cdsSel_MateriaisOdonto.eof then
      begin
        Application.MessageBox('O MATERIAL informado j� est� cadastrado!'+#13+#10+#13+#10+
                               'Favor verificar e tentar novamente.',
                               'Cadastro de Materiais Odontol�gicos',
                               MB_OK + MB_ICONERROR);
        edDescricaoMaterial.SetFocus;
      end
      else
      //Insere os dados no banco de dados...
      begin
        try
          with dmDeca.cdsIns_MateriaisOdonto do
          begin
            Close;
            Params.ParamByName ('@pe_cod_tipo_material').Value   := StrToInt(vListaTipoMaterial1.Strings[cbTipoMaterial.ItemIndex]);
            Params.ParamByName ('@pe_ind_unidade_estoque').Value := cbUnidadeEstoque.ItemIndex;
            Params.ParamByName ('@pe_qtd_minima').Value          := StrToInt(edQtdMinima.Text);
            Params.ParamByName ('@pe_qtd_estoque_atual').Value   := StrToInt(edQtdAtual.Text);
            Params.ParamByName ('@pe_dsc_material').Value        := Trim(edDescricaoMaterial.Text);
            Execute;

            with dmDeca.cdsSel_MateriaisOdonto do
            begin
              Close;
              Params.ParamByName ('@pe_cod_material').Value      := Null;
              Params.ParamByName ('@pe_cod_tipo_material').Value := Null;
              Params.ParamByName ('@pe_qtd_minima').Value        := Null;
              Params.ParamByName ('@pe_dsc_material').Value      := Null;
              Open;

              dbgMateriais.Refresh;

              ModoTela ('P');
            end;
          end;

        except end;

      end;

    end;


  except end;
end;

procedure TfrmEstoqueOdonto.cbTipoMaterialEClick(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_MateriaisOdonto do
    begin
      vListaTipoMaterial2.Find(cbTipoMaterialE.Text, vIndex);
      Close;
      Params.ParamByName ('@pe_cod_material').Value      := Null;
      Params.ParamByName ('@pe_cod_tipo_material').Value := vIndex+1;
      Params.ParamByName ('@pe_qtd_minima').Value        := Null;
      Params.ParamByName ('@pe_dsc_material').Value      := Null;
      Open;

      dbgMateriaisE.Refresh;

    end;
  except end;

end;

procedure TfrmEstoqueOdonto.dbgMateriaisECellClick(Column: TColumn);
begin
  //Repassa os valores do banco para o formul�rio/campos
  edUnidadeEstoqueE.Text    := dmDeca.cdsSel_MateriaisOdonto.FieldByName ('vUnidade').Value;
  edDescricaoMaterialE.Text := dmDeca.cdsSel_MateriaisOdonto.FieldByName ('dsc_material').Value;
  edQtdMinimaE.Text         := IntToStr(dmDeca.cdsSel_MateriaisOdonto.FieldByName ('qtd_minima').Value);
  edQtdAtualE.Text          := IntToStr(dmDeca.cdsSel_MateriaisOdonto.FieldByName ('qtd_estoque_atual').Value);

  vvCOD_MATERIAL            := dmDeca.cdsSel_MateriaisOdonto.FieldByName ('cod_material').Value;

  edDataEntrada.Text        := DateToStr(Date());
  edEntrada.SetFocus;

  with dmDeca.cdsSel_MateriaisOdontoMOVIMENTOS do
  begin
    Close;
    Params.ParamByName ('@pe_cod_movimento').Value      := Null;
    Params.ParamByName ('@pe_cod_material').Value       := vvCOD_MATERIAL;
    Params.ParamByName ('@pe_ind_tipo_movimento').Value := 'E';
    Open;
    dbgEntradasEstoqueMaterialX.Refresh;
  end;

end;

procedure TfrmEstoqueOdonto.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABAE = True then
    AllowChange := True
  else AllowChange := False;
end;

procedure TfrmEstoqueOdonto.TabSheet1Show(Sender: TObject);
begin
  with dmDeca.cdsSel_MateriaisOdonto do
  begin
    Close;
    Params.ParamByName ('@pe_cod_material').Value      := Null;
    Params.ParamByName ('@pe_cod_tipo_material').Value := Null;
    Params.ParamByName ('@pe_qtd_minima').Value        := Null;
    Params.ParamByName ('@pe_dsc_material').Value      := Null;
    Open;

    dbgMateriais.Refresh;

    ModoTela ('P');
  end;
end;

procedure TfrmEstoqueOdonto.btnConfirmaEntradaClick(Sender: TObject);
begin

  try
    if Application.MessageBox('Confirma a ENTRADA do material em estoque?',
                              '[Sistema Deca] - Entrada de Material Odontol�gico',
                              MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin

      with dmDeca.cdsIns_MovimentoEstoque do
      begin
        Close;
        Params.ParamByName ('@pe_tipo_movimento').Value  := 'E';
        Params.ParamByName ('@pe_dat_movimento').Value   := StrToDate(edDataEntrada.Text);
        Params.ParamByName ('@pe_cod_material').Value    := vvCOD_MATERIAL;
        Params.ParamByName ('@pe_qtd_movimento').Value   := StrToInt(edEntrada.Text);
        Execute;

        //Atualiza a quantidade do estoque atual do material atual
        with dmDeca.cdsUPD_AtualizaEstoqueOdonto do
        begin
          Close;
          Params.ParamByName ('@pe_tipo').Value          := 'E';
          Params.ParamByName ('@pe_cod_material').Value  := vvCOD_MATERIAL;
          Params.ParamByName ('@pe_qtd_movimento').Value := StrToInt(edEntrada.Text);
          Execute;
        end;

        //Atualizar os grids de Materiais e Movimenta��es de Materiais - Entradas
        with dmDeca.cdsSel_MateriaisOdonto do
        begin
          Close;
          Params.ParamByName ('@pe_cod_material').Value      := vvCOD_MATERIAL;
          Params.ParamByName ('@pe_cod_tipo_material').Value := Null;
          Params.ParamByName ('@pe_qtd_minima').Value        := Null;
          Params.ParamByName ('@pe_dsc_material').Value      := Null;
          Open;

          dbgMateriais.Refresh;
        end;

        with dmDeca.cdsSel_MateriaisOdontoMOVIMENTOS do
        begin
          Close;
          Params.ParamByName ('@pe_cod_movimento').Value      := Null;
          Params.ParamByName ('@pe_cod_material').Value       := vvCOD_MATERIAL;
          Params.ParamByName ('@pe_ind_tipo_movimento').Value := 'E';
          Open;
          dbgEntradasEstoqueMaterialX.Refresh;
        end;

      end;
    end;
  except end;

end;

procedure TfrmEstoqueOdonto.btnConfirmaSaidaClick(Sender: TObject);
begin

  try
    if Application.MessageBox('Confirma a SA�DA do material em estoque?',
                              '[Sistema Deca] - Entrada de Material Odontol�gico',
                              MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin

      VerificaSaida;  

      with dmDeca.cdsIns_MovimentoEstoque do
      begin
        Close;
        Params.ParamByName ('@pe_tipo_movimento').Value  := 'S';
        Params.ParamByName ('@pe_dat_movimento').Value   := StrToDate(edDataSaida.Text);
        Params.ParamByName ('@pe_cod_material').Value    := vvCOD_MATERIAL;
        Params.ParamByName ('@pe_qtd_movimento').Value   := StrToInt(edSaida.Text);
        Execute;

        //Atualiza a quantidade do estoque atual do material atual
        with dmDeca.cdsUPD_AtualizaEstoqueOdonto do
        begin
          Close;
          Params.ParamByName ('@pe_tipo').Value          := 'S';
          Params.ParamByName ('@pe_cod_material').Value  := vvCOD_MATERIAL;
          Params.ParamByName ('@pe_qtd_movimento').Value := StrToInt(edSaida.Text);
          Execute;
        end;

        //Atualizar os grids de Materiais e Movimenta��es de Materiais - Entradas
        with dmDeca.cdsSel_MateriaisOdonto do
        begin
          Close;
          Params.ParamByName ('@pe_cod_material').Value      := vvCOD_MATERIAL;
          Params.ParamByName ('@pe_cod_tipo_material').Value := Null;
          Params.ParamByName ('@pe_qtd_minima').Value        := Null;
          Params.ParamByName ('@pe_dsc_material').Value      := Null;
          Open;

          dbgMateriais.Refresh;
        end;

        with dmDeca.cdsSel_MateriaisOdontoMOVIMENTOS do
        begin
          Close;
          Params.ParamByName ('@pe_cod_movimento').Value      := Null;
          Params.ParamByName ('@pe_cod_material').Value       := vvCOD_MATERIAL;
          Params.ParamByName ('@pe_ind_tipo_movimento').Value := 'S';
          Open;
          dbgEntradasEstoqueMaterialX.Refresh;
        end;

      end;
    end;
  except end;

end;

procedure TfrmEstoqueOdonto.edSaidaExit(Sender: TObject);
begin
  {
  //Verifica se a quantidade a ser retirada � igual ou maior do que a quantidade m�nima em estoque
  vvQTD_MINIMA_ESTOQUE := StrToInt(edQtdMinimaS.Text);
  vvQTD_SAIDA          := StrToInt(edSaida.Text);

  if (vvQTD_SAIDA >= vvQTD_MINIMA_ESTOQUE) then
  begin
    Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                            'A quantidade a ser retirada � maior ou igual ao estoque m�nimo.' + #13+#10 +
                            'Favor verificar a quantidade informada.',
                            '[Sistema Deca] - Estoque Odonto',
                            MB_OK + MB_ICONWARNING);
    edSaida.SetFocus;
  end
  else
    cbTipoMaterialS.SetFocus;
  }
end;

procedure TfrmEstoqueOdonto.cbTipoMaterialSClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_MateriaisOdonto do
    begin
      vListaTipoMaterial2.Find(cbTipoMaterialS.Text, vIndexS);
      Close;
      Params.ParamByName ('@pe_cod_material').Value      := Null;
      Params.ParamByName ('@pe_cod_tipo_material').Value := vIndexS+1;
      Params.ParamByName ('@pe_qtd_minima').Value        := Null;
      Params.ParamByName ('@pe_dsc_material').Value      := Null;
      Open;

      dbgMateriaisS.Refresh;

    end;
  except end;
end;

procedure TfrmEstoqueOdonto.dbgMateriaisSCellClick(Column: TColumn);
begin
  //Repassa os valores do banco para o formul�rio/campos
  edUnidadeEstoqueS.Text    := dmDeca.cdsSel_MateriaisOdonto.FieldByName ('vUnidade').Value;
  edDescricaoMaterialS.Text := dmDeca.cdsSel_MateriaisOdonto.FieldByName ('dsc_material').Value;
  edQtdMinimaS.Text         := IntToStr(dmDeca.cdsSel_MateriaisOdonto.FieldByName ('qtd_minima').Value);
  edQtdAtualS.Text          := IntToStr(dmDeca.cdsSel_MateriaisOdonto.FieldByName ('qtd_estoque_atual').Value);

  vvCOD_MATERIAL            := dmDeca.cdsSel_MateriaisOdonto.FieldByName ('cod_material').Value;

  edDataSaida.Text          := DateToStr(Date());
  edSaida.SetFocus;
end;

procedure TfrmEstoqueOdonto.VerificaSaida;
begin
  //Verifica se a quantidade a ser retirada � igual ou maior do que a quantidade m�nima em estoque
  vvQTD_MINIMA_ESTOQUE := StrToInt(edQtdMinimaS.Text);
  vvQTD_SAIDA          := StrToInt(edSaida.Text);

  if (vvQTD_SAIDA >= vvQTD_MINIMA_ESTOQUE) then
  begin
    Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                            'A quantidade a ser retirada � maior ou igual ao estoque m�nimo.' + #13+#10 +
                            'Favor verificar a quantidade informada.',
                            '[Sistema Deca] - Estoque Odonto',
                            MB_OK + MB_ICONWARNING);
    edSaida.SetFocus;
  end
  else
    cbTipoMaterialS.SetFocus;
end;



end.
