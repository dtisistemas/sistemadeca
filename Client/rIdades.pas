unit rIdades;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelIdades = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    PageFooterBand1: TQRBand;
    lbTitulo: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRImage1: TQRImage;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel8: TQRLabel;
    lbUnidade: TQRLabel;
    QRLabel9: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel4: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape7: TQRShape;
    QRLabel11: TQRLabel;
    QRDBText8: TQRDBText;
    QRShape8: TQRShape;
    QRLabel12: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText6: TQRDBText;
  private

  public

  end;

var
  relIdades: TrelIdades;

implementation

uses uDM, uSelecionaIdadeDataEspecificao;

{$R *.DFM}

end.
