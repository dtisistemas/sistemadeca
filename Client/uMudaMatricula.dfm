object frmMudaMatricula: TfrmMudaMatricula
  Left = 321
  Top = 272
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Mudan�a de Matr�cula]'
  ClientHeight = 332
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 2
    Width = 705
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR crian�a/adolescente'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
      OnExit = txtMatriculaExit
    end
  end
  object GroupBox2: TGroupBox
    Left = 5
    Top = 58
    Width = 705
    Height = 217
    TabOrder = 1
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 157
      Height = 13
      Caption = 'Raz�o da Mudan�a de Matr�cula'
    end
    object Label3: TLabel
      Left = 16
      Top = 21
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label4: TLabel
      Left = 160
      Top = 21
      Width = 70
      Height = 13
      Caption = 'C�digo NOVO:'
      OnClick = btnCancelarClick
    end
    object txtOcorrencia: TMemo
      Left = 16
      Top = 64
      Width = 673
      Height = 137
      Hint = 
        'Informe a Raz�o pela qual est� sendo alterada a matr�cula da cri' +
        'an�a/adolescente'
      Lines.Strings = (
        '')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object txtData: TMaskEdit
      Left = 47
      Top = 17
      Width = 75
      Height = 22
      Hint = 'Data da MUDAN�A da Matr�cula'
      EditMask = '99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
    end
    object txtMatriculaNova: TMaskEdit
      Left = 235
      Top = 16
      Width = 83
      Height = 21
      Hint = 
        'Informe o NOVO N�MERO DE MATR�CULA (Lembre-se que nesses casos o' +
        ' N�mero � informado pelo DRH)'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '        '
      OnExit = txtMatriculaNovaExit
    end
  end
  object Panel1: TPanel
    Left = 5
    Top = 280
    Width = 705
    Height = 49
    TabOrder = 2
    object btnNovo: TBitBtn
      Left = 343
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para promover NOVA Mudan�a de Matr�cula'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 478
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR a opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 628
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de Mudan�a de Matr�cula'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnSalvar: TBitBtn
      Left = 405
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para promover NOVA Mudan�a de Matr�cula'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSalvarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
        00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
        00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
        00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
        0003737FFFFFFFFF7F7330099999999900333777777777777733}
      NumGlyphs = 2
    end
  end
end
