object frmFichaCadastro: TfrmFichaCadastro
  Left = 319
  Top = 203
  Width = 1194
  Height = 675
  Caption = '[Sistema Deca] - Ficha de Cadastro'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 179
    Top = 7
    Width = 804
    Height = 626
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Ficha de Cadastro'
      object Bevel1: TBevel
        Left = 9
        Top = 72
        Width = 775
        Height = 4
      end
      object Panel1: TPanel
        Left = 8
        Top = 6
        Width = 777
        Height = 61
        TabOrder = 0
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 84
        Width = 116
        Height = 46
        Caption = '[Matr�cula]'
        TabOrder = 1
        object MaskEdit1: TMaskEdit
          Left = 9
          Top = 16
          Width = 70
          Height = 21
          EditMask = 'CCCCCCCC'
          MaxLength = 8
          TabOrder = 0
          Text = '        '
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Composi��o Familiar'
      ImageIndex = 1
    end
    object TabSheet3: TTabSheet
      Caption = 'Dados Escolares'
      ImageIndex = 2
    end
    object TabSheet4: TTabSheet
      Caption = 'Hist�rico'
      ImageIndex = 3
    end
  end
  object Outline1: TOutline
    Left = 8
    Top = 8
    Width = 165
    Height = 90
    Lines.Nodes = (
      'FICHA DE CADASTRO'
      'COMPOSI��O FAMILIAR'
      'DADOS ESCOLARES'
      'HIST�RICO')
    ItemHeight = 13
    TabOrder = 1
    ItemSeparator = '\'
  end
end
