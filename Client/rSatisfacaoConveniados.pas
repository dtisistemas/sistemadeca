unit rSatisfacaoConveniados;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelSatisfacaoConveniados = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    lbSemestreAno: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    DetailBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    relObsSatisfConv: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRShape16: TQRShape;
    PageFooterBand1: TQRBand;
    QRShape17: TQRShape;
    QRLabel13: TQRLabel;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRLabel20: TQRLabel;
    expMEDIA01: TQRExpr;
    expMEDIA02: TQRExpr;
    expMEDIA03: TQRExpr;
    expMEDIA04: TQRExpr;
    expMEDIA05: TQRExpr;
    QRLabel2: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRLabel22: TQRLabel;
    QRSysData1: TQRSysData;
    qrlbTotalPaginas: TQRLabel;
    QRShape32: TQRShape;
    QRLabel21: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel24: TQRLabel;
    qrlbNomeUsuario: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
  private

  public

  end;

var
  relSatisfacaoConveniados: TrelSatisfacaoConveniados;

implementation

uses uDM;

{$R *.DFM}

end.
