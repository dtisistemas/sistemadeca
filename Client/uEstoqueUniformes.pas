unit uEstoqueUniformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ComCtrls, Buttons, Grids, DBGrids, ExtCtrls, StdCtrls, Db, Mask;

type
  THackDBGrid = class (TDBGrid)
  procedure AllRows (DBGrid: TDBGrid; NewHeight: Word);
end;


type
  TfrmEstoqueUniformes = class(TForm)
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel3: TPanel;
    btnNovoUniforme: TSpeedButton;
    btnSalvarUniforme: TSpeedButton;
    btnAlterarUniforme: TSpeedButton;
    btnVisualizarUniformes: TSpeedButton;
    btnAcessoUniformesU: TSpeedButton;
    dbgUniformes: TDBGrid;
    dsTipoUniformes: TDataSource;
    dsUniformes: TDataSource;
    dsCadastro: TDataSource;
    dsUniformesSolicitacoes: TDataSource;
    GroupBox6: TGroupBox;
    edCodUniforme: TEdit;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    edQtdAdmissao: TEdit;
    edQtdReposicao: TEdit;
    cbPeriodoReposicao: TComboBox;
    meDetalhesUniforme: TMemo;
    GroupBox12: TGroupBox;
    edCodCECAM: TEdit;
    GroupBox13: TGroupBox;
    edCodSICOM: TEdit;
    Label1: TLabel;
    btnAcompanharPedidos: TSpeedButton;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet5: TTabSheet;
    Panel4: TPanel;
    GroupBox4: TGroupBox;
    btnIncluirSolicitacao: TSpeedButton;
    GroupBox14: TGroupBox;
    GroupBox5: TGroupBox;
    dbgAlunos: TDBGrid;
    cbMesesS: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    mskAnoS: TMaskEdit;
    dbgSolicitacoesUniformes: TDBGrid;
    rgCriterioPesquisa: TRadioGroup;
    Label6: TLabel;
    Panel5: TPanel;
    Label7: TLabel;
    edValorPesquisa: TEdit;
    cbTipoUniformeP: TComboBox;
    cbUnidades: TComboBox;
    btnConsultaP: TSpeedButton;
    btnExcluirSolicitacao: TSpeedButton;
    GroupBox16: TGroupBox;
    Edit1: TEdit;
    UpDown2: TUpDown;
    GroupBox17: TGroupBox;
    memoDetalhes: TMemo;
    Label8: TLabel;
    btnExibirTodos: TSpeedButton;
    GroupBox18: TGroupBox;
    meDetalhesUniformeP: TMemo;
    SpeedButton1: TSpeedButton;
    GroupBox19: TGroupBox;
    edQtdCaracteres: TEdit;
    btnGerencial: TSpeedButton;
    GroupBox20: TGroupBox;
    GroupBox21: TGroupBox;
    edQuantidade: TEdit;
    UpDown3: TUpDown;
    GroupBox22: TGroupBox;
    edObservacoes: TEdit;
    GroupBox23: TGroupBox;
    btnAlterarSolicitacao: TSpeedButton;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    cbUnidadeNovaSolicitacao: TComboBox;
    btnConsultar: TSpeedButton;
    chkTodos: TCheckBox;
    GroupBox24: TGroupBox;
    edMatricula: TEdit;
    edNomeAluno: TEdit;
    edUnidadeAtual: TEdit;
    mskDataSolicitacao: TMaskEdit;
    mskData1: TMaskEdit;
    mskData2: TMaskEdit;
    dbgUniformesSolicitacoes: TDBGrid;
    edQtd: TEdit;
    TabSheet6: TTabSheet;
    Label4: TLabel;
    SpeedButton3: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure edAlturaChange(Sender: TObject);
    procedure dbgUniformesDblClick(Sender: TObject);
    procedure ModoTelaUniformes(StatusTelaUniformes: String);
    procedure TabSheet3Show(Sender: TObject);
    procedure btnNovoUniformeClick(Sender: TObject);
    procedure btnSalvarUniformeClick(Sender: TObject);
    procedure btnAlterarUniformeClick(Sender: TObject);
    procedure dbgUniformesSolicitacoes2CellClick(Column: TColumn);
    procedure btnIncluirSolicitacaoClick(Sender: TObject);
    procedure rgCriterioPesquisaClick(Sender: TObject);
    procedure btnConsultaPClick(Sender: TObject);
    procedure btnExibirTodosClick(Sender: TObject);
    procedure dbgSolicitacoesUniformesCellClick(Column: TColumn);
    procedure btnExcluirSolicitacaoClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnGerencialClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure chkTodosClick(Sender: TObject);
    procedure dbgSolicitacoesUniformesDblClick(Sender: TObject);
    procedure btnAlterarSolicitacaoClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstoqueUniformes: TfrmEstoqueUniformes;
  vListaTipoUniforme1, vListaTipoUniforme2, vListaUnidade1, vListaUnidade2, vListaUnidadeNovaSolicitacao1, vListaUnidadeNovaSolicitacao2 ,
  vListaUniforme_AbaConsultaSolicitacoes1, vListaUniforme_AbaConsultaSolicitacoes2: TStringList;

implementation

uses uPrincipal, uDM, uUtil, uGerarPedidoUniformes, rSolicitacoesUniformes,
  rGerencial_TotalizaUniformes;

{$R *.DFM}

procedure TfrmEstoqueUniformes.FormShow(Sender: TObject);
var mmMES, mMANO : String;
begin
  vListaTipoUniforme1 := TStringList.Create();
  vListaTipoUniforme2 := TStringList.Create();
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de Unidades para uma poss�vel consulta
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('flg_status').AsInteger = 1) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidades.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;


  //Habilita ou desabilita as abas de acordo com o perfil
  //Perfis 1(Administrador) e 12(Almoxarifado)
  {case vvIND_PERFIL of
    1, 12: begin
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
    end;

    2, 3: begin
      PageControl1.ActivePageIndex := 2;
      PageControl1.Pages[0].TabVisible := False;
      PageControl1.Pages[1].TabVisible := False;
      PageControl2.ActivePageIndex := 0;
    end;

  end; }

  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;

  //Captura a data atual e transferir para o combo mes e ano
  mmMES := Copy(DateToStr(Date()),4,2);
  cbMesesS.ItemIndex := StrToInt(mmMES)-1;
  
  mmANO := Copy(DateToStr(Date()),7,4);
  mskAnoS.Text := mmANO;
  
  //Carregar a Rela��o de Tipos de Uniformes
  {try
    with dmDeca.cdsSel_TipoUniforme do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
      Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
      Open;
      dbgTiposUniformes.Refresh;

      vListaTipoUniforme1 := TStringList.Create();
      vListaTipoUniforme2 := TStringList.Create();

      while not dmDeca.cdsSel_TipoUniforme.eof do
      begin
        vListaTipoUniforme1.Add (IntToStr(dmDeca.cdsSel_TipoUniforme.FieldByName ('cod_id_tipouniforme').Value));
        vListaTipoUniforme2.Add (dmDeca.cdsSel_TipoUniforme.FieldByName ('dsc_tipouniforme').Value);
        cbTipoUniforme.Items.Add (dmDeca.cdsSel_TipoUniforme.FieldByName ('dsc_tipouniforme').Value);
        cbTipoUniformeP.Items.Add (dmDeca.cdsSel_TipoUniforme.FieldByName ('dsc_tipouniforme').Value);
        dmDeca.cdsSel_TipoUniforme.Next;
      end;
    end;
  except end;}

  try
    with dmDeca.cdsSel_Uniformes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
      Params.ParamByName ('@pe_numeracao').Value           := Null;
      dbgUniformes.Refresh;
      Open;
    end;
  except end;

  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := NULL;
      Params.ParamByName ('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
      Open;
      dbgAlunos.Refresh;
    end;
  except end;

end;

{ THackDBGrid }

procedure THackDBGrid.AllRows(DBGrid: TDBGrid; NewHeight: Word);
var
  i: DWord;
begin
  for i := 0 to THackDBGrid (DBGrid).RowCount - 1 do
    THackDBGrid (DBGrid).RowHeights [i] := NewHeight;
end;

procedure TfrmEstoqueUniformes.edAlturaChange(Sender: TObject);
begin
  THackDBGrid(dbgUniformesSolicitacoes).AllRows (dbgUniformesSolicitacoes, StrToInt(Edit1.Text));
  THackDBGrid(dbgUniformesSolicitacoes).RowHeights[0] := 25;
end;

procedure TfrmEstoqueUniformes.dbgUniformesDblClick(Sender: TObject);
begin
  ModoTelaUniformes('A');
  edCodUniforme.Text           := dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
  edQtdAdmissao.Text           := IntToStr(dmDeca.cdsSel_Uniformes.FieldByName('qtd_admissao').Value);
  edQtdReposicao.Text          := IntToStr(dmDeca.cdsSel_Uniformes.FieldByName('qtd_reposicao').Value);
  edCodCecam.Text              := dmDeca.cdsSel_Uniformes.FieldByName('cod_cecam').Value;
  edCodSICOM.Text              := dmDeca.cdsSel_Uniformes.FieldByName('cod_giap').Value;
  cbPeriodoReposicao.ItemIndex := dmDeca.cdsSel_Uniformes.FieldByName('ind_periodo_reposicao').Value;
  meDetalhesUniforme.Text      := dmDeca.cdsSel_Uniformes.FieldByName('dsc_detalhes').Value;
end;

procedure TfrmEstoqueUniformes.ModoTelaUniformes(
  StatusTelaUniformes: String);
begin
  if StatusTelaUniformes = 'P' then
  begin
    btnNovoUniforme.Enabled        := True;
    btnSalvarUniforme.Enabled      := False;
    btnAlterarUniforme.Enabled     := False;
    btnVisualizarUniformes.Enabled := True;
    btnAcessoUniformesU.Enabled    := True;

    edCodUniforme.Clear;
    edQtdAdmissao.Clear;
    edQtdReposicao.Clear;
    cbPeriodoReposicao.ItemIndex := -1;
    meDetalhesUniforme.Clear;
    edCodCECAM.Clear;
    edCodSICOM.Clear;

    GroupBox6.Enabled  := False;
    GroupBox8.Enabled  := False;
    GroupBox9.Enabled  := False;
    GroupBox10.Enabled := False;
    GroupBox11.Enabled := False;
    GroupBox12.Enabled := False;
    GroupBox13.Enabled := False;
  end
  else if StatusTelaUniformes = 'N' then
  begin
    btnNovoUniforme.Enabled        := False;
    btnSalvarUniforme.Enabled      := True;
    btnAlterarUniforme.Enabled     := False;
    btnVisualizarUniformes.Enabled := False;
    btnAcessoUniformesU.Enabled    := False;

    GroupBox6.Enabled  := True;
    GroupBox8.Enabled  := True;
    GroupBox9.Enabled  := True;
    GroupBox10.Enabled := True;
    GroupBox11.Enabled := True;
    GroupBox12.Enabled := True;
    GroupBox13.Enabled := True;


  end
  else if StatusTelaUniformes = 'A' then
  begin
    btnNovoUniforme.Enabled        := False;
    btnSalvarUniforme.Enabled      := False;
    btnAlterarUniforme.Enabled     := True;
    btnVisualizarUniformes.Enabled := False;
    btnAcessoUniformesU.Enabled    := False;

    GroupBox6.Enabled  := True;
    GroupBox8.Enabled  := True;
    GroupBox9.Enabled  := True;
    GroupBox10.Enabled := True;
    GroupBox11.Enabled := True;
    GroupBox12.Enabled := True;
    GroupBox13.Enabled := True;
  end;
end;

procedure TfrmEstoqueUniformes.TabSheet3Show(Sender: TObject);
begin
  ModoTelaUniformes('P');
end;

procedure TfrmEstoqueUniformes.btnNovoUniformeClick(Sender: TObject);
begin
  ModoTelaUniformes('N');
end;

procedure TfrmEstoqueUniformes.btnSalvarUniformeClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_Uniforme do
    begin
      Close;
      //Params.ParamByName ('@pe_cod_id_tipouniforme').Value   := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniforme.ItemIndex]);
      Params.ParamByName ('@pe_ind_cor').Value               := Null;
      Params.ParamByName ('@pe_ind_sexo').Value              := Null;
      Params.ParamByName ('@pe_qtd_admissao').Value          := StrToInt(edQtdAdmissao.Text);
      Params.ParamByName ('@pe_qtd_reposicao').Value         := StrToInt(edQtdReposicao.Text);
      Params.ParamByName ('@pe_ind_periodo_reposicao').Value := cbPeriodoReposicao.ItemIndex;
      Params.ParamByName ('@pe_ind_numeracao').Value         := Null;
      Params.ParamByName ('@pe_dsc_detalhes').Value          := Trim(meDetalhesUniforme.Text);
      Params.ParamByName ('@pe_cod_cecam').Value             := Trim(edCodCECAM.Text);
      Params.ParamByName ('@pe_cod_giap').Value              := Trim(edCodSICOM.Text);
      Execute;
    end;

    with dmDeca.cdsSel_Uniformes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
      Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
      Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
      Params.ParamByName ('@pe_numeracao').Value           := Null;
      Open;
      dbgUniformes.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o !!!' +#13+#10+
                            'Ocorreu um erro na inclus�o dos dados de Uniforme.'+#13+#10+
                            'Favor verificar conex�o e tentar novamente.',
                            '[Sistema Deca] - Inclus�o de Uniforme',
                            MB_OK + MB_ICONWARNING);
    ModoTelaUniformes('P');
  end;

  ModoTelaUniformes('P');

end;

procedure TfrmEstoqueUniformes.btnAlterarUniformeClick(Sender: TObject);
begin
  try
    with dmDeca.cdsUpd_Uniforme do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uniforme').Value       :=  dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
      
      Params.ParamByName ('@pe_ind_cor').Value               := '-';
      Params.ParamByName ('@pe_ind_sexo').Value              := '-';
      Params.ParamByName ('@pe_qtd_admissao').Value          := StrToInt(edQtdAdmissao.Text);
      Params.ParamByName ('@pe_qtd_reposicao').Value         := StrToInt(edQtdReposicao.Text);
      Params.ParamByName ('@pe_ind_periodo_reposicao').Value := cbPeriodoReposicao.ItemIndex;
      Params.ParamByName ('@pe_ind_numeracao').Value         := '-';
      Params.ParamByName ('@pe_dsc_detalhes').Value          := Trim(meDetalhesUniforme.Text);
      Params.ParamByName ('@pe_cod_cecam').Value             := Trim(edCodCECAM.Text);
      Params.ParamByName ('@pe_cod_giap').Value              := Trim(edCodSICOM.Text);
      Execute;
    end;

    with dmDeca.cdsSel_Uniformes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_uniforme').Value     := Null;
      //Params.ParamByName ('@pe_cod_id_tipouniforme').Value := Null;
      Params.ParamByName ('@pe_dsc_tipouniforme').Value    := Null;
      Params.ParamByName ('@pe_numeracao').Value           := Null;
      Open;
      dbgUniformes.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o !!!' +#13+#10+
                            'Ocorreu um erro na altera��o dos dados de Uniforme.'+#13+#10+
                            'Favor verificar conex�o e tentar novamente.',
                            '[Sistema Deca] - Inclus�o de Uniforme',
                            MB_OK + MB_ICONWARNING);
    ModoTelaUniformes('P');
  end;

  ModoTelaUniformes('P');
end;

procedure TfrmEstoqueUniformes.dbgUniformesSolicitacoes2CellClick(
  Column: TColumn);
begin
  memoDetalhes.Lines.Clear;
  memoDetalhes.Lines.Add(dmDeca.cdsSel_Uniformes.FieldByName('dsc_detalhes').Value);
end;

procedure TfrmEstoqueUniformes.btnIncluirSolicitacaoClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja incluir o uniforme selecionado para o(a) aluno(a)? ',
                             '[Sistema Deca] - Solicita��o de Uniforme',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    //Verifica se a solicita��o do uniforme para o(a) aluno(a) est� dentro do prazo m�nimo de solicita��o....
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMesesS.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAnoS.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value        := Null;//dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value;//vvCOD_UNIDADE;
      Open;

      if (dmDeca.cdsSel_UniformesSolicitacoes.RecordCount > 0) or (dmDeca.cdsSel_UniformesSolicitacoes.FieldByName('dat_prox_solicitacao').Value >= DateToStr(Date())) then
      begin
        Application.MessageBox ('Aten��o !!!' + #13+#10+
                                'J� existe uma solicita��o do uniforme para o aluno no per�odo. Favor verificar.',
                                '[Sistema Deca] - Solicita��o de Uniformes',
                                MB_OK + MB_ICONWARNING);
      end
      else
      begin
        with dmDeca.cdsInc_UniformeSolicitacao do
        begin
          Close;
          Params.ParamByName ('@pe_dat_solicitacao').Value  := StrToDate(mskDataSolicitacao.Text);
          Params.ParamByName ('@pe_cod_matricula').Value    := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_mes_solicitacao').Value  := cbMesesS.ItemIndex+1;
          Params.ParamByName ('@pe_ano_solicitacao').Value  := StrToInt(mskAnoS.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value  := dmDeca.cdsSel_Uniformes.FieldByName ('cod_id_uniforme').Value;
          Params.ParamByName ('@pe_qtd_uniforme').Value     := StrToInt(edQuantidade.Text); //"1" -:> A partir da vers�o 6.0.0 foi liberada a digita��o da quantidade
          Params.ParamByName ('@pe_tam_uniforme').Value     := Null; //N�o grava mais pois est� na descri��o do uniforme...
          Params.ParamByName ('@pe_dsc_observacoes').Value  := GetValue(edObservacoes.Text);
          Execute;
        end;

        //Incluir no registro de atendimento do aluno a solicita��o do uniforme...
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString       := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName('@pe_cod_unidade').AsInteger        := dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value;
          Params.ParamByName('@pe_dat_historico').Value          := Date();
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 12;
          Params.ParamByName('@pe_dsc_tipo_historico').AsString  := 'Registro de Atendimento';
          Params.ParamByName('@pe_ind_tipo').Value               := Null;
          Params.ParamByName('@pe_ind_classificacao').Value      := Null;
          Params.ParamByName('@pe_qtd_participantes').Value      := Null;
          Params.ParamByName('@pe_num_horas').Value              := Null;
          Params.ParamByName('@pe_dsc_ocorrencia').Value         := 'Uniforme solicitado em ' + DateToStr(Date()) + ': ' + memoDetalhes.Text;
          Params.ParamByName('@pe_log_cod_usuario').AsInteger    := vvCOD_USUARIO;
          Execute;
        end;


        with dmDeca.cdsSel_UniformesSolicitacoes do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Null;
          Params.ParamByName ('@pe_mes_solicitacao').Value    := cbMesesS.ItemIndex + 1;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := StrToInt(mskAnoS.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
          Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
          Params.ParamByName ('@pe_cod_unidade').Value        := vvCOD_UNIDADE;
          Open;
          dbgUniformesSolicitacoes.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TfrmEstoqueUniformes.rgCriterioPesquisaClick(Sender: TObject);
begin
  case rgCriterioPesquisa.ItemIndex of
    0, 4: begin
      edValorPesquisa.Clear;
      edValorPesquisa.Visible := True;
      edValorPesquisa.SetFocus;
      cbTipoUniformeP.Visible := False;
      cbUnidades.Visible      := False;
      btnGerencial.Enabled    := False;
    end;

    1: begin
      edValorPesquisa.Visible := False;
      cbTipoUniformeP.Visible := False;
      cbUnidades.Visible      := True;
      btnGerencial.Enabled    := True;
      
    end;

    2: begin
      edValorPesquisa.Visible := False;
      cbTipoUniformeP.Visible := True;
      cbUnidades.Visible      := False;
      btnGerencial.Enabled    := False;
    end;

    3: begin
      cbTipoUniformeP.Visible := False;
      cbUnidades.Visible      := False;
      edValorPesquisa.Visible := True;
      edValorPesquisa.Clear;
      edValorPesquisa.SetFocus;
      btnGerencial.Enabled    := False;
    end;




  end;
end;

procedure TfrmEstoqueUniformes.btnConsultaPClick(Sender: TObject);
begin
  //Realiza a pesquisa de acordo com os crit�rios
  try
    with dmDeca.cdsSel_UniformesSolicitacoes do
    begin
      Close;

      case rgCriterioPesquisa.ItemIndex of
        0: begin
          //Matr�cula
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Trim(edValorPesquisa.Text);
          Params.ParamByName ('@pe_mes_solicitacao').Value    := Null; //cbMesesP.ItemIndex + 1;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := Null; //StrToInt(mskAnoP.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
          Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
          Params.ParamByName ('@pe_cod_unidade').Value        := Null;
          Params.ParamByName ('@pe_dsc_detalhes').Value       := Null;
          Params.ParamByName ('@pe_nome').Value               := Null;
          Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
          Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
        end;

        1: begin
          //Unidade
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Null;
          Params.ParamByName ('@pe_mes_solicitacao').Value    := Null; //cbMesesP.ItemIndex + 1;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := Null; //StrToInt(mskAnoP.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
          Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
          Params.ParamByName ('@pe_cod_unidade').Value        := StrToInt(vListaUnidade1.Strings[cbUnidades.ItemIndex]);
          Params.ParamByName ('@pe_dsc_detalhes').Value       := Null;
          Params.ParamByName ('@pe_nome').Value               := Null;
          Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
          Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
        end;

        2: begin
          //Tipo de Uniforme
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Null;
          Params.ParamByName ('@pe_mes_solicitacao').Value    := Null; //cbMesesP.ItemIndex + 1;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := Null; //StrToInt(mskAnoP.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := StrToInt(vListaTipoUniforme1.Strings[cbTipoUniformeP.ItemIndex]);
          Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
          Params.ParamByName ('@pe_cod_unidade').Value        := Null;
          Params.ParamByName ('@pe_dsc_detalhes').Value       := Null;
          Params.ParamByName ('@pe_nome').Value               := Null;
          Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
          Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
        end;

        3: begin
          //Detalhes do uniforme
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Null;
          Params.ParamByName ('@pe_mes_solicitacao').Value    := Null; //cbMesesP.ItemIndex + 1;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := Null; //StrToInt(mskAnoP.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
          Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
          Params.ParamByName ('@pe_cod_unidade').Value        := Null;
          Params.ParamByName ('@pe_dsc_detalhes').Value       := Trim(edValorPesquisa.Text);
          Params.ParamByName ('@pe_nome').Value               := Null;
          Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
          Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
        end;

        4: begin
          //Nome do Aluno
          Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
          Params.ParamByName ('@pe_cod_matricula').Value      := Null;
          Params.ParamByName ('@pe_mes_solicitacao').Value    := Null; //cbMesesP.ItemIndex + 1;
          Params.ParamByName ('@pe_ano_solicitacao').Value    := Null; //StrToInt(mskAnoP.Text);
          Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
          Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
          Params.ParamByName ('@pe_cod_unidade').Value        := Null;
          Params.ParamByName ('@pe_dsc_detalhes').Value       := Null;
          Params.ParamByName ('@pe_nome').Value               := Trim(edValorPesquisa.Text) + '%';
          Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
          Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
        end;

      end;

      Open;
      dbgUniformesSolicitacoes.Refresh;
    end;
  except end;
end;

procedure TfrmEstoqueUniformes.btnExibirTodosClick(Sender: TObject);
begin
  with dmDeca.cdsSel_UniformesSolicitacoes do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
    Params.ParamByName ('@pe_cod_matricula').Value      := Null;
    Params.ParamByName ('@pe_mes_solicitacao').Value    := Null;
    Params.ParamByName ('@pe_ano_solicitacao').Value    := Null;
    Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
    Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
    Params.ParamByName ('@pe_cod_unidade').Value        := Null;
    Params.ParamByName ('@pe_dsc_detalhes').Value       := Null;
    Params.ParamByName ('@pe_nome').Value               := Null;
    Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
    Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
    Open;
    dbgUniformesSolicitacoes.Refresh;
  end;
end;

procedure TfrmEstoqueUniformes.dbgSolicitacoesUniformesCellClick(Column: TColumn);
begin
  meDetalhesUniformeP.Clear;
  meDetalhesUniformeP.Lines.Add (dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('dsc_detalhes').Value);
  meDetalhesUniformeP.ReadOnly := True;
end;

procedure TfrmEstoqueUniformes.btnExcluirSolicitacaoClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja EXCLUIR a solicita��o atual?',
                             '[Sistema Deca] - Exclus�o de Solicita��o de Uniformes',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    try
      with dmDeca.cdsDel_SolicitacoesUniformes do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_solicitacao').Value := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_id_solicitacao').AsInteger;
        Execute;
      end;

      btnConsultaP.Click;
      dbgSolicitacoesUniformes.Refresh;

    except end;
  end;
end;

procedure TfrmEstoqueUniformes.SpeedButton3Click(Sender: TObject);
begin
  Application.CreateForm(TfrmGerarPedidoUniformes, frmGerarPedidoUniformes);
  frmGerarPedidoUniformes.ShowModal;
end;

procedure TfrmEstoqueUniformes.SpeedButton1Click(Sender: TObject);
begin
  Application.CreateForm (TrelSolicitacoesUniformes, relSolicitacoesUniformes);
  relSolicitacoesUniformes.qrlUnidade.Caption := cbUnidades.Text;
  relSolicitacoesUniformes.qrlPeriodo.Caption := 'Per�odo de ' + mskData1.Text + ' � ' + mskData2.Text;
  relSolicitacoesUniformes.Preview;
  relSolicitacoesUniformes.Free;
end;

procedure TfrmEstoqueUniformes.btnGerencialClick(Sender: TObject);
begin
if cbUnidades.ItemIndex = -1 then
begin
  ShowMessage('Favor selecionar a Unidade');
end
else
begin
  try
    with dmDeca.cdsSel_Gerencial_TotalizaUniformes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_solicitacao').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value      := Null;
      Params.ParamByName ('@pe_mes_solicitacao').Value    := Null;//cbMesesP.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_solicitacao').Value    := Null;//StrToInt(mskAnoP.Text);
      Params.ParamByName ('@pe_cod_id_uniforme').Value    := Null;
      Params.ParamByName ('@pe_flg_bloqueado').Value      := Null;
      Params.ParamByName ('@pe_cod_unidade').Value        := Null; //StrToInt(vListaUnidade1.Strings[cbUnidades.ItemIndex]);
      Params.ParamByName ('@pe_dsc_detalhes').Value       := Null;
      Params.ParamByName ('@pe_data1').Value              := StrToDate(mskData1.Text);
      Params.ParamByName ('@pe_data2').Value              := StrToDate(mskData2.Text);
      Open;
    end;

    if (dmDeca.cdsSel_Gerencial_TotalizaUniformes.RecordCount > 0) then
    begin
      //Exibe relat�rio com as quantidades de uniformes solicitador por unidade, per�odo
      Application.CreateForm (TrelGerencial_TotalizaUniformes, relGerencial_TotalizaUniformes);
      relGerencial_TotalizaUniformes.qrlTitulo.Caption := Trim(dmDeca.cdsSel_Gerencial_TotalizaUniformes.FieldByName('num_ccusto').Value) + ' - '+cbUnidades.Text + ' - Per�odo: ' + mskData1.Text + ' � ' + mskdata2.Text;
      relGerencial_TotalizaUniformes.qrlNomeUsuario.Caption := vvNOM_USUARIO;
      relGerencial_TotalizaUniformes.Preview;
      relGerencial_TotalizaUniformes.Free;
    end
    else
    begin
      Application.MessageBox ('Aten��o!!!' +#13+#10 +
                              'Nenhum registro encontrado em fun��o dos crit�rios.' +#13+#10+
                              'Favor refazer a consulta!',
                              '[Sistema Deca] - Totaliza��o de Uniformes',
                              MB_OK + MB_ICONINFORMATION);
    end;
  except end;
end;
end;
procedure TfrmEstoqueUniformes.TabSheet2Show(Sender: TObject);
begin
  cbUnidadeNovaSolicitacao.Clear;
  vListaUnidadeNovaSolicitacao1 := TStringList.Create();
  vListaUnidadeNovaSolicitacao2 := TStringList.Create();
  //Carregar a lista de Unidades para uma poss�vel consulta
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('flg_status').AsInteger = 1) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidadeNovaSolicitacao1.Add  (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidadeNovaSolicitacao2.Add  (FieldByName ('nom_unidade').Value);
          cbUnidadeNovaSolicitacao.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;
end;

procedure TfrmEstoqueUniformes.btnConsultarClick(Sender: TObject);
begin
  //Selecionar os alunos da unidade "X" e tamb�m por parte do nome...
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidadeNovaSolicitacao1.Strings[cbUnidadeNovaSolicitacao.ItemIndex]);
      Open;
      dbgAlunos.Refresh;
    end;
  except end;
end;

procedure TfrmEstoqueUniformes.chkTodosClick(Sender: TObject);
begin
  if chkTodos.Checked then
  begin
    try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := Null;
      Open;
      dbgAlunos.Refresh;
    end;
    except end;
  end
  else
  begin

  end;
end;

procedure TfrmEstoqueUniformes.dbgSolicitacoesUniformesDblClick(
  Sender: TObject);
var vIndex: Integer;
begin
  //Permite trazer os dados da solicita��o atual para os campos da tela desde que o status esteja "Em Solicita��o"
  if (dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('flg_bloqueado').Value = 0) then
  begin
    edMatricula.Text    := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_matricula').Value;
    edNomeAluno.Text    := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('nom_nome').Value;
    edUnidadeAtual.Text := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('nom_unidade').Value;
    edQtd.Text          := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('qtd_uniforme').Value;
  end
  else
  begin
      Application.MessageBox ('Aten��o !!!' + #13+#10+
                              'Opera��o na autorizada devido � SOLICITA��O j� .' + #13+#10+
                              'ter sido efetivada como PEDIDO, ' + #13+#10+
                              'Favor verificar o status!',
                              '[Sistema Deca] - Erro alterar solicita��o de uniforme',
                              MB_OK + MB_ICONERROR);
  end;
end;

procedure TfrmEstoqueUniformes.btnAlterarSolicitacaoClick(Sender: TObject);
begin
  //Pede confirma��o para altera��o dos dados
  if Application.MessageBox ('Deseja manter a altera��o dos dados da solicita��o?',
                             '[Sistema Deca] - Confirmar altera��o de Solicita��o',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    try
      with dmDeca.cdsUpd_UniformesSolicitacoes do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_solicitacao').AsInteger := dmDeca.cdsSel_UniformesSolicitacoes.FieldByName ('cod_id_solicitacao').AsInteger;
        Params.ParamByName ('@pe_tam_uniforme').AsString        := '-';
        Params.ParamByName ('@pe_qtd_uniforme').AsInteger       := StrToInt(edQtd.Text);
        Execute;

        btnConsultaP.Click;

        edMatricula.Clear;
        edNomeAluno.Clear;
        edUnidadeAtual.Clear;
        edQtd.Clear;

      end;
    except
      Application.MessageBox ('Aten��o !!!' + #13+#10+
                              'Um erro ocorreu na tentativa de gravar a altera��o da solicita��o.' + #13+#10+
                              'Verifique a conex�o com a internet e caso o erro persista, ' + #13+#10+
                              'entre em contato com o Administrador!',
                              '[Sistema Deca] - Erro alterar solicita��o de uniforme',
                              MB_OK + MB_ICONERROR);
    end;
  end;
end;

end.
