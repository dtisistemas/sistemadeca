unit rContratoVerso;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelCONTRATOVERSO = class(TQuickRep)
    DetailBand1: TQRBand;
    QRLabel17: TQRLabel;
    QRMemo1: TQRMemo;
    PageFooterBand1: TQRBand;
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape23: TQRShape;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRMemo2: TQRMemo;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRMemo3: TQRMemo;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
  private

  public

  end;

var
  relCONTRATOVERSO: TrelCONTRATOVERSO;

implementation

{$R *.DFM}

end.
