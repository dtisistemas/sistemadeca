object frmInformaPeriodoIndicador: TfrmInformaPeriodoIndicador
  Left = 349
  Top = 168
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Indicadores do Servi�o Social]'
  ClientHeight = 217
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 0
    Width = 388
    Height = 214
    TabOrder = 0
    object Label1: TLabel
      Left = 47
      Top = 12
      Width = 278
      Height = 26
      Alignment = taCenter
      Caption = 
        'Informe os dados necess�rios para emiss�o do relatorio de Indica' +
        'dores do Servi�o Social'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 104
      Top = 48
      Width = 56
      Height = 13
      Caption = 'Data Inicial:'
    end
    object Label3: TLabel
      Left = 104
      Top = 72
      Width = 51
      Height = 13
      Caption = 'Data Final:'
    end
    object Label4: TLabel
      Left = 112
      Top = 96
      Width = 47
      Height = 13
      Caption = 'M�s/Ano:'
    end
    object Label5: TLabel
      Left = 74
      Top = 120
      Width = 83
      Height = 13
      Caption = 'Assistente Social:'
    end
    object Label6: TLabel
      Left = 43
      Top = 144
      Width = 113
      Height = 13
      Caption = 'Bloco/Projeto/Unidade:'
    end
    object lbUnidade: TLabel
      Left = 164
      Top = 144
      Width = 109
      Height = 13
      Hint = 'UNIDADE'
      Caption = 'xxxxxxxxxxxxxxxxxx'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object txtDataInicial: TMaskEdit
      Left = 165
      Top = 45
      Width = 80
      Height = 21
      Hint = 
        'Informe DATA INICIAL para Emiss�o dos Dados Estat�sticos do Serv' +
        'i�o Social'
      EditMask = '99/99/0000;1'
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      OnExit = txtDataInicialExit
    end
    object txtDataFinal: TMaskEdit
      Left = 165
      Top = 69
      Width = 80
      Height = 21
      Hint = 
        'Informe DATA FINAL para Emiss�o dos Dados Estat�sticos do Servi�' +
        'o Social'
      EditMask = '99/99/0000;1'
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  /  /    '
      OnExit = txtDataFinalExit
    end
    object btnEmitirIndicadores: TBitBtn
      Left = 55
      Top = 173
      Width = 274
      Height = 25
      Hint = 'Clique para EMITIR Dados Estat�sticos do Servi�o Social'
      Caption = 'Emitir Indicador'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnEmitirIndicadoresClick
    end
    object txtMesAno: TEdit
      Left = 165
      Top = 93
      Width = 58
      Height = 21
      Hint = 'Per�odo'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnExit = txtMesAnoExit
    end
    object txtAssistenteSocial: TEdit
      Left = 165
      Top = 117
      Width = 172
      Height = 21
      Hint = 'Nome da Assistente Social Respons�vel'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
end
