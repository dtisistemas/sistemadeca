unit uDadosEstatisticosFrequencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, ComCtrls, StdCtrls, Mask, Grids, DBGrids;

type
  TfrmDadosEstatisticosFrequencia = class(TForm)
    GroupBox1: TGroupBox;
    cbMes: TComboBox;
    GroupBox2: TGroupBox;
    edAno: TEdit;
    UpDown1: TUpDown;
    RadioGroup1: TRadioGroup;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    GroupBox3: TGroupBox;
    cbDivisao: TComboBox;
    cbUnidade: TComboBox;
    mskMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    Edit2: TEdit;
    SpeedButton3: TSpeedButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDadosEstatisticosFrequencia: TfrmDadosEstatisticosFrequencia;

implementation

{$R *.DFM}

procedure TfrmDadosEstatisticosFrequencia.FormShow(Sender: TObject);
begin
  edAno.Text := Copy(DateToStr(Date()),7,4);
end;

end.
