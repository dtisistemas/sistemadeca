unit uAvaliacaoDesempenhoAprendiz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, Buttons, ComCtrls, Grids, DBGrids, Db,
  TeeProcs, TeEngine, Chart, DBChart;

type
  TfrmAvaliacaoDesempenhoAprendiz = class(TForm)
    GroupBox4: TGroupBox;
    Label3: TLabel;
    lbNome: TLabel;
    txtMatricula: TMaskEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    rgItem01: TRadioGroup;
    rgItem02: TRadioGroup;
    rgItem03: TRadioGroup;
    rgItem04: TRadioGroup;
    rgItem05: TRadioGroup;
    rgItem06: TRadioGroup;
    TabSheet5: TTabSheet;
    rgItem07: TRadioGroup;
    rgItem08: TRadioGroup;
    rgItem09: TRadioGroup;
    rgItem10: TRadioGroup;
    rgItem11: TRadioGroup;
    rgItem12: TRadioGroup;
    btnPesquisar: TSpeedButton;
    GroupBox29: TGroupBox;
    GroupBox13: TGroupBox;
    GroupBox14: TGroupBox;
    Panel1: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    lbMatricula: TLabel;
    lbNomeCompleto: TLabel;
    lbEndereco: TLabel;
    Label21: TLabel;
    lbBairro: TLabel;
    Label23: TLabel;
    lbCep: TLabel;
    Label25: TLabel;
    lbUnidadeAtual: TLabel;
    Label27: TLabel;
    lbSecaoAtual: TLabel;
    Label29: TLabel;
    lbNomeGestor: TLabel;
    Label31: TLabel;
    lbNomeASocial: TLabel;
    Label1: TLabel;
    lbSituacao: TLabel;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnCancelar: TBitBtn;
    btnPesquisa: TBitBtn;
    btnSair: TBitBtn;
    meJustificativa1: TMemo;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox15: TGroupBox;
    lbUsuario: TLabel;
    GroupBox1: TGroupBox;
    cbUnidade: TComboBox;
    mskDataDigitacao: TMaskEdit;
    mskDataAvaliacao: TMaskEdit;
    mskDataInicioPratica: TMaskEdit;
    Edit1: TEdit;
    meJustificativa2: TMemo;
    Edit2: TEdit;
    meJustificativa3: TMemo;
    Edit3: TEdit;
    meJustificativa4: TMemo;
    Edit4: TEdit;
    meJustificativa5: TMemo;
    Edit5: TEdit;
    meJustificativa6: TMemo;
    Edit6: TEdit;
    meJustificativa7: TMemo;
    Edit7: TEdit;
    meJustificativa8: TMemo;
    Edit8: TEdit;
    meJustificativa9: TMemo;
    Edit9: TEdit;
    meJustificativa10: TMemo;
    Edit10: TEdit;
    meJustificativa11: TMemo;
    Edit11: TEdit;
    meJustificativa12: TMemo;
    Edit12: TEdit;
    GroupBox2: TGroupBox;
    cbSecao: TComboBox;
    Panel2: TPanel;
    dbgAvaliacoes: TDBGrid;
    btnSalvar: TBitBtn;
    imgMensagem: TImage;
    lbMensagem: TLabel;
    dsSel_AvaliacoesApr: TDataSource;
    TabSheet3: TTabSheet;
    rgCriterio: TRadioGroup;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    cbUnidadePesquisa: TComboBox;
    Label8: TLabel;
    edAdolescente: TEdit;
    btnPesquisaAdol: TSpeedButton;
    btnResultado: TSpeedButton;
    PageControl3: TPageControl;
    TabSheet6: TTabSheet;
    Panel4: TPanel;
    dbgResultado: TDBGrid;
    Label9: TLabel;
    TabSheet7: TTabSheet;
    GroupBox5: TGroupBox;
    cbUnidadeAltera: TComboBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    mskDataDigitacaoAltera: TMaskEdit;
    GroupBox8: TGroupBox;
    mskDataAvaliacaoAltera: TMaskEdit;
    GroupBox9: TGroupBox;
    mskDataInicioAprendAltera: TMaskEdit;
    PageControl4: TPageControl;
    TabSheet8: TTabSheet;
    rgItem01_Altera: TRadioGroup;
    meJustificativa01_Altera: TMemo;
    Edit15: TEdit;
    rgItem02_Altera: TRadioGroup;
    meJustificativa02_Altera: TMemo;
    Edit16: TEdit;
    rgItem03_Altera: TRadioGroup;
    meJustificativa03_Altera: TMemo;
    Edit17: TEdit;
    TabSheet9: TTabSheet;
    rgItem04_Altera: TRadioGroup;
    rgItem05_Altera: TRadioGroup;
    rgItem06_Altera: TRadioGroup;
    meJustificativa04_Altera: TMemo;
    meJustificativa05_Altera: TMemo;
    meJustificativa06_Altera: TMemo;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit20: TEdit;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    rgItem07_Altera: TRadioGroup;
    meJustificativa07_Altera: TMemo;
    Edit21: TEdit;
    rgItem08_Altera: TRadioGroup;
    meJustificativa08_Altera: TMemo;
    Edit22: TEdit;
    rgItem09_Altera: TRadioGroup;
    meJustificativa09_Altera: TMemo;
    Edit23: TEdit;
    rgItem10_Altera: TRadioGroup;
    Edit24: TEdit;
    rgItem11_Altera: TRadioGroup;
    meJustificativa11_Altera: TMemo;
    Edit25: TEdit;
    rgItem12_Altera: TRadioGroup;
    meJustificativa10_Altera: TMemo;
    Edit26: TEdit;
    meJustificativa12_Altera: TMemo;
    btnCancelarAlteracao: TSpeedButton;
    btnAlterarLancamento: TSpeedButton;
    Label2: TLabel;
    mskData1: TMaskEdit;
    Label4: TLabel;
    mskData2: TMaskEdit;
    dsResultado: TDataSource;
    cbSecaoAltera: TComboBox;
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AtualizaTela(Modo: String);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnSalvarClick(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure cbSecaoClick(Sender: TObject);
    procedure meJustificativa1Change(Sender: TObject);
    procedure meJustificativa2Change(Sender: TObject);
    procedure meJustificativa3Change(Sender: TObject);
    procedure meJustificativa4Change(Sender: TObject);
    procedure meJustificativa5Change(Sender: TObject);
    procedure meJustificativa6Change(Sender: TObject);
    procedure meJustificativa7Change(Sender: TObject);
    procedure meJustificativa8Change(Sender: TObject);
    procedure meJustificativa9Change(Sender: TObject);
    procedure meJustificativa10Change(Sender: TObject);
    procedure meJustificativa11Change(Sender: TObject);
    procedure meJustificativa12Change(Sender: TObject);
    function ValidaItens : Boolean;
    procedure TabSheet3Show(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure txtMatriculaKeyPress(Sender: TObject; var Key: Char);
    procedure rgCriterioClick(Sender: TObject);
    procedure mskDataDigitacaoExit(Sender: TObject);
    procedure mskDataAvaliacaoExit(Sender: TObject);
    procedure mskDataInicioPraticaExit(Sender: TObject);
    procedure btnResultadoClick(Sender: TObject);
    procedure btnPesquisaAdolClick(Sender: TObject);
    procedure mskData1Exit(Sender: TObject);
    procedure mskData2Exit(Sender: TObject);
    procedure dbgResultadoDblClick(Sender: TObject);
    procedure meJustificativa01_AlteraChange(Sender: TObject);
    procedure meJustificativa02_AlteraChange(Sender: TObject);
    procedure meJustificativa03_AlteraChange(Sender: TObject);
    procedure meJustificativa04_AlteraChange(Sender: TObject);
    procedure meJustificativa05_AlteraChange(Sender: TObject);
    procedure meJustificativa06_AlteraChange(Sender: TObject);
    procedure meJustificativa07_AlteraChange(Sender: TObject);
    procedure meJustificativa08_AlteraChange(Sender: TObject);
    procedure meJustificativa09_AlteraChange(Sender: TObject);
    procedure meJustificativa10_AlteraChange(Sender: TObject);
    procedure meJustificativa11_AlteraChange(Sender: TObject);
    procedure meJustificativa12_AlteraChange(Sender: TObject);
    procedure btnAlterarLancamentoClick(Sender: TObject);
    procedure cbUnidadeAlteraClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAvaliacaoDesempenhoAprendiz: TfrmAvaliacaoDesempenhoAprendiz;
  vUnidade1, vUnidade2 : TStringList;

implementation

uses uDM, uFichaCrianca, uFichaPesquisa, uPrincipal, uUtil,
  uPesquisaAvaliacaoDesempApr;

{$R *.DFM}

procedure TfrmAvaliacaoDesempenhoAprendiz.AtualizaTela(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    txtMatricula.Clear;
    lbNome.Caption := '';
    lbUsuario.Caption := vvNOM_USUARIO;

    //Painel Inferior ao de pesquisa
    lbMatricula.Caption := '';
    lbNomeCompleto.Caption := '';
    lbEndereco.Caption := '';
    lbBairro.Caption := '';
    lbCep.Caption := '';
    lbUnidadeAtual.Caption := '';
    lbSecaoAtual.Caption := '';
    lbNomeGestor.Caption := '';
    lbNomeASocial.Caption := '';
    lbSituacao.Caption:= '';

    //Bot�es
    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnPesquisar.Enabled := True;
    btnSair.Enabled := True;

    //P�gina da Nova Avalia��o
    //cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := False;
    //cbSecao.ItemIndex := -1;
    cbSecao.Enabled := False;
    mskDataDigitacao.Clear;
    mskDataDigitacao.Enabled := False;
    mskDataAvaliacao.Clear;
    mskDataAvaliacao.Enabled := False;
    mskDataInicioPratica.Clear;
    mskDataInicioPratica.Enabled := False;

    //Items e campos de justificativa
    rgItem01.ItemIndex := -1;
    rgItem01.Enabled := False;
    meJustificativa1.Lines.Clear;
    rgItem02.ItemIndex := -1;
    rgItem02.Enabled := False;
    meJustificativa2.Lines.Clear;
    rgItem03.ItemIndex := -1;
    rgItem03.Enabled := False;
    meJustificativa3.Lines.Clear;
    rgItem04.ItemIndex := -1;
    rgItem04.Enabled := False;
    meJustificativa4.Lines.Clear;
    rgItem05.ItemIndex := -1;
    rgItem05.Enabled := False;
    meJustificativa5.Lines.Clear;
    rgItem06.ItemIndex := -1;
    rgItem06.Enabled := False;
    meJustificativa6.Lines.Clear;
    rgItem07.ItemIndex := -1;
    rgItem07.Enabled := False;
    meJustificativa7.Lines.Clear;
    rgItem08.ItemIndex := -1;
    rgItem08.Enabled := False;
    meJustificativa8.Lines.Clear;
    rgItem09.ItemIndex := -1;
    rgItem09.Enabled := False;
    meJustificativa9.Lines.Clear;
    rgItem10.ItemIndex := -1;
    rgItem10.Enabled := False;
    meJustificativa10.Lines.Clear;
    rgItem11.ItemIndex := -1;
    rgItem11.Enabled := False;
    meJustificativa11.Lines.Clear;
    rgItem12.ItemIndex := -1;
    rgItem12.Enabled := False;
    meJustificativa12.Lines.Clear;

    //Imagem e label da mensagem
    imgMensagem.Visible := False;
    lbMensagem.Caption := '';
    lbMensagem.Visible := False;
    
  end
  else
  if Modo = 'Novo' then
  begin
    //txtMatricula.SetFocus;
    PageControl1.ActivePageIndex := 0;
    //Bot�es
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnPesquisar.Enabled := False;
    btnSair.Enabled := False;

    //P�gina da Nova Avalia��o
    cbUnidade.Enabled := True;
    cbSecao.Enabled := True;
    mskDataDigitacao.Enabled := True;
    mskDataDigitacao.Text := DateToStr(Date());
    mskDataAvaliacao.Enabled := True;
    mskDataInicioPratica.Enabled := True;

    //Items e campos de justificativa
    rgItem01.Enabled := True;
    rgItem02.Enabled := True;
    rgItem03.Enabled := True;
    rgItem04.Enabled := True;
    rgItem05.Enabled := True;
    rgItem06.Enabled := True;
    rgItem07.Enabled := True;
    rgItem08.Enabled := True;
    rgItem09.Enabled := True;
    rgItem10.Enabled := True;
    rgItem11.Enabled := True;
    rgItem12.Enabled := True;

    //Imagem e label da mensagem
    imgMensagem.Visible := True;
    lbMensagem.Visible := True;
    lbMensagem.Caption := 'TODOS OS ITENS DA AVALIA��O S�O DE PREENCHIMENTO OBRIGAT�RIO...'
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnPesquisarClick(
  Sender: TObject);
begin
  //Chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    //frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;

    PageControl1.ActivePageIndex := 0;

    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= Null;//vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        btnNovo.Enabled := True;
        btnNovo.SetFocus;        

        //Painel Inferior
        lbMatricula.Caption := FieldByName('cod_matricula').AsString;
        lbNomeCompleto.Caption := FieldByName('nom_nome').AsString;
        lbEndereco.Caption := FieldByName('nom_endereco').AsString;
        lbBairro.Caption := FieldByName('nom_bairro').AsString;
        lbCep.Caption := FieldByName('num_cep').AsString;
        lbUnidadeAtual.Caption := Trim(FieldByName('num_ccusto').AsString) + '-' + FieldByName('vUnidade').AsString;

        lbNomeGestor.Caption := FieldByName('nom_gestor').AsString;

        try
        with dmDeca.cdsSel_Secao do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_secao').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').Value;
          Params.ParamByName('@pe_num_secao').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').Value;
          Params.ParamByName('@pe_flg_status').Value := Null;
          Open;

          if RecordCount > 0 then
          begin
            lbSecaoAtual.Caption := FieldByName('num_secao').AsString + '-'+FieldByName('dsc_nom_secao').Value ;
            lbNomeASocial.Caption := FieldByName('nom_asocial').Value;
          end;
        end;
        except end;

        case FieldByName('ind_status').Value of
          1: begin lbSituacao.Caption := 'ATIVO' end;
          2: begin lbSituacao.Caption := 'DESLIGADO' end;
          3: begin lbSituacao.Caption := 'SUSPENSO' end;
          4: begin lbSituacao.Caption := 'AFASTADO' end;
          5: begin lbSituacao.Caption := 'EM TRANSFER�NCIA' end;
        end;

        //Carregar no grid os dados das avalia��es anteriores
        try
        with dmDeca.cdsSel_AvaliacoesApr do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_avaliacao').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_cod_unidade').Value := Null;
          Params.ParamByName('@pe_num_secao').Value := Null;
          Open;
          dbgAvaliacoes.DataSource := dsSel_AvaliacoesApr;
          dbgAvaliacoes.Refresh;
        end;
        except end;

      end;
    except end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.FormShow(Sender: TObject);
begin
  AtualizaTela('Padr�o');
  PageControl1.ActivePageIndex := 0;

  vUnidade1 := TStringList.Create();
  vUnidade2 := TStringList.Create();

  //Carrega a lista de unidades
  try
  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName('@pe_cod_unidade').Value := Null;
    Params.ParamByName('@pe_nom_unidade').Value := Null;
    Open;

    while not eof do
    begin
      cbUnidade.Items.Add (FieldByName('nom_unidade').Value);
      cbUnidadePesquisa.Items.Add (FieldByName('nom_unidade').Value);
      cbUnidadeAltera.Items.Add (FieldByName('nom_unidade').Value);
      vUnidade1.Add (IntToStr(FieldByName('cod_unidade').Value));
      vUnidade2.Add (FieldByName('nom_unidade').Value);
      Next;
    end;
  end;
  except end;

  Edit1.Text := '255';
  Edit2.Text := '255';
  Edit3.Text := '255';
  Edit4.Text := '255';
  Edit5.Text := '255';
  Edit6.Text := '255';
  Edit7.Text := '255';
  Edit8.Text := '255';
  Edit9.Text := '255';
  Edit10.Text := '255';
  Edit11.Text := '255';
  Edit12.Text := '255';

end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaTela('Padr�o');
  PageControl1.ActivePageIndex := 0;
  dbgAvaliacoes.DataSource := nil;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnSairClick(Sender: TObject);
begin
  frmAvaliacaoDesempenhoAprendiz.Close;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnNovoClick(Sender: TObject);
begin
  AtualizaTela('Novo');
  PageControl1.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 0;
  cbUnidade.SetFocus;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  if (Length(Trim(txtMatricula.Text))<> 8) then AllowChange := False;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnSalvarClick(Sender: TObject);
begin

   if Not ValidaItens then
   begin
     Application.MessageBox ('Os itens obrigat�rios n�o foram preenchidos/selecionados.',
                             'Avalia��o de Desempenho - Aprendiz',
                             MB_ICONERROR + MB_OK);   
   end
   else
   begin

     //Pede confirma��o
    if Application.MessageBox('Deseja Incluir lan�amento?',
                              'Cadastro de Usu�rios - Exclus�o',
                              MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin
      //Grava os dados da avalia��o
      try
      with dmDeca.cdsInc_AvaliacaoApr do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
        Params.ParamByName('@pe_dat_avaliacao').Value := GetValue(mskDataAvaliacao, vtDate);
        Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
        Params.ParamByName('@pe_num_secao').Value := Copy(cbSecao.Text,1,4);
        Params.ParamByName('@pe_dat_inicio_pratica').Value := GetValue(mskDataInicioPratica, vtDate);
        Params.ParamByName('@pe_flg_item01').Value := rgItem01.Itemindex;
        Params.ParamByName('@pe_dsc_justificativa01').Value := GetValue(meJustificativa1.Text);
        Params.ParamByName('@pe_flg_item02').Value := rgItem02.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa02').Value := GetValue(meJustificativa2.Text);
        Params.ParamByName('@pe_flg_item03').Value := rgItem03.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa03').Value := GetValue(meJustificativa3.Text);
        Params.ParamByName('@pe_flg_item04').Value := rgItem04.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa04').Value := GetValue(meJustificativa4.Text);
        Params.ParamByName('@pe_flg_item05').Value := rgItem05.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa05').Value := GetValue(meJustificativa5.Text);
        Params.ParamByName('@pe_flg_item06').Value := rgItem06.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa06').Value := GetValue(meJustificativa6.Text);
        Params.ParamByName('@pe_flg_item07').Value := rgItem07.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa07').Value := GetValue(meJustificativa7.Text);
        Params.ParamByName('@pe_flg_item08').Value := rgItem08.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa08').Value := GetValue(meJustificativa8.Text);
        Params.ParamByName('@pe_flg_item09').Value := rgItem09.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa09').Value := GetValue(meJustificativa9.Text);
        Params.ParamByName('@pe_flg_item10').Value := rgItem10.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa10').Value := GetValue(meJustificativa10.Text);
        Params.ParamByName('@pe_flg_item11').Value := rgItem11.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa11').Value := GetValue(meJustificativa11.Text);
        Params.ParamByName('@pe_flg_item12').Value := rgItem12.ItemIndex;
        Params.ParamByName('@pe_dsc_justificativa12').Value := GetValue(meJustificativa12.Text);
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Params.ParamByName('@pe_flg_visto_adol').Value := Null;
        Execute;

       //Ap�s  gravar a avalia��o, muda a tela para padr�o
       //ShowMessage ('Avalia��o lan�ada com sucesso...');
       Application.MessageBox('A Avalia��o foi lan�ada com sucesso...',
                              'Avalia��o Desempenho - Aprendiz',
                              MB_ICONASTERISK + MB_OK );
       AtualizaTela('Padr�o');
       PageControl1.ActivePageIndex := 0;
       txtMatricula.SetFocus;
     end
     except end
    end
   else
   begin
     //Application.MessageBox('Confirma o lan�amento dos dados desta avalia��o?',
     //                       'Cadastro de Usu�rios - Exclus�o',
     //                       MB_ICONQUESTION + MB_YESNO );
     AtualizaTela('Padr�o');
   end;
   end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.cbUnidadeClick(Sender: TObject);
begin
  //Carrega a lista de se��es de acordo com a unidade selecionada
  try
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_secao').Value := Null;
    Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidade.ItemIndex]);
    Params.ParamByName('@pe_num_secao').Value := Null;
    Params.ParamByName('@pe_flg_status').Value := 0; //Somente os Ativos
    Open;

    cbSecao.Clear;
    while not eof do
    begin
      cbSecao.Items.Add (FieldByName('num_secao').Value + '-' + FieldByName('dsc_nom_secao').Value);
      Next;
    end;
  end;
  except end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.cbSecaoClick(Sender: TObject);
begin
  mskDataDigitacao.Text := DateToStr(Date());
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa1Change(
  Sender: TObject);
begin
  Edit1.text := IntToStr(255-(Length(meJustificativa1.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa2Change(
  Sender: TObject);
begin
  Edit2.text := IntToStr(255-(Length(meJustificativa2.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa3Change(
  Sender: TObject);
begin
  Edit3.Text := IntToStr(255-(Length(meJustificativa3.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa4Change(
  Sender: TObject);
begin
  Edit4.Text := IntToStr(255-(Length(meJustificativa4.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa5Change(
  Sender: TObject);
begin
  Edit5.Text := IntToStr(255-(Length(meJustificativa5.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa6Change(
  Sender: TObject);
begin
  Edit6.Text := IntToStr(255-(Length(meJustificativa6.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa7Change(
  Sender: TObject);
begin
  Edit7.Text := IntToStr(255-(Length(meJustificativa7.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa8Change(
  Sender: TObject);
begin
  Edit8.Text := IntToStr(255-(Length(meJustificativa8.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa9Change(
  Sender: TObject);
begin
  Edit9.Text := IntToStr(255-(Length(meJustificativa9.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa10Change(
  Sender: TObject);
begin
  Edit10.Text := IntToStr(255-(Length(meJustificativa10.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa11Change(
  Sender: TObject);
begin
  Edit11.Text := IntToStr(255-(Length(meJustificativa11.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa12Change(
  Sender: TObject);
begin
  Edit12.Text := IntToStr(255-(Length(meJustificativa12.Text)));
end;

function TfrmAvaliacaoDesempenhoAprendiz.ValidaItens: Boolean;
var vValida : Boolean;
begin
  vValida := True;
  if vValida then
  begin
    vValida := Length(txtMatricula.Text) <> 0;
    if not vValida then txtMatricula.SetFocus;
  end;

  if vValida then
  begin
    vValida := cbUnidade.ItemIndex > -1;
    if vValida then cbUnidade.SetFocus;
  end;

  if vValida then
  begin
    vValida := cbSecao.ItemIndex > -1;
    if vValida then cbSecao.SetFocus;
  end;

  if vValida then
  begin
    vValida := Trim(mskDataAvaliacao.Text) <> '/  /';
    if not vValida then mskDataAvaliacao.SetFocus;
  end;

  if vValida then
  begin
    vValida := Trim(mskDataInicioPratica.Text) <> '/  /';
    if not vValida then mskDataInicioPratica.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem01.ItemIndex > -1;
    if not vValida then rgItem01.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem02.ItemIndex > -1;
    if not vValida then rgItem02.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem03.ItemIndex > -1;
    if not vValida then rgItem03.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem04.ItemIndex > -1;
    if not vValida then rgItem04.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem05.ItemIndex > -1;
    if not vValida then rgItem05.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem06.ItemIndex > -1;
    if not vValida then rgItem06.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem07.ItemIndex > -1;
    if not vValida then rgItem07.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem08.ItemIndex > -1;
    if not vValida then rgItem08.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem09.ItemIndex > -1;
    if not vValida then rgItem09.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem10.ItemIndex > -1;
    if not vValida then rgItem10.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem11.ItemIndex > -1;
    if not vValida then rgItem11.SetFocus;
  end;

  if vValida then
  begin
    vValida := rgItem12.ItemIndex > -1;
    if not vValida then rgItem12.SetFocus;
  end;

  ValidaItens := vValida;

end;

procedure TfrmAvaliacaoDesempenhoAprendiz.TabSheet3Show(Sender: TObject);
begin
  //Desabilita todos os checkboxes
  PageControl3.ActivePageIndex := 0;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnPesquisaClick(
  Sender: TObject);
begin
  Application.CreateForm(TfrmPesquisaAvaliacaoDesempAprendiz, frmPesquisaAvaliacaoDesempAprendiz);
  frmPesquisaAvaliacaoDesempAprendiz.Showmodal;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.txtMatriculaKeyPress(
  Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    
    PageControl1.ActivePageIndex := 0;

    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= Null;//vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        btnNovo.Enabled := True;
        //btnNovo.SetFocus;

        //Painel Inferior
        lbMatricula.Caption := FieldByName('cod_matricula').AsString;
        lbNomeCompleto.Caption := FieldByName('nom_nome').AsString;
        lbEndereco.Caption := FieldByName('nom_endereco').AsString;
        lbBairro.Caption := FieldByName('nom_bairro').AsString;
        lbCep.Caption := FieldByName('num_cep').AsString;
        lbUnidadeAtual.Caption := Trim(FieldByName('num_ccusto').AsString) + '-' + FieldByName('vUnidade').AsString;

        lbNomeGestor.Caption := FieldByName('nom_gestor').AsString;

        try
        with dmDeca.cdsSel_Secao do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_secao').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_unidade').Value;
          Params.ParamByName('@pe_num_secao').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName('num_secao').Value;
          Params.ParamByName('@pe_flg_status').Value := Null;
          Open;

          if RecordCount > 0 then
          begin
            lbSecaoAtual.Caption := FieldByName('num_secao').AsString + '-'+FieldByName('dsc_nom_secao').Value ;
            lbNomeASocial.Caption := FieldByName('nom_asocial').Value;
          end;
        end;
        except end;

        case FieldByName('ind_status').Value of
          1: begin lbSituacao.Caption := 'ATIVO' end;
          2: begin lbSituacao.Caption := 'DESLIGADO' end;
          3: begin lbSituacao.Caption := 'SUSPENSO' end;
          4: begin lbSituacao.Caption := 'AFASTADO' end;
          5: begin lbSituacao.Caption := 'EM TRANSFER�NCIA' end;
        end;

        //Carregar no grid os dados das avalia��es anteriores
        try
        with dmDeca.cdsSel_AvaliacoesApr do
        begin
          Close;
          Params.ParamByName('@pe_cod_id_avaliacao').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
          Params.ParamByName('@pe_cod_unidade').Value := Null;
          Params.ParamByName('@pe_num_secao').Value := Null;
          Open;
          dbgAvaliacoes.DataSource := dsSel_AvaliacoesApr;
          dbgAvaliacoes.Refresh;
        end;
        except end;

      end;
    except end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.rgCriterioClick(Sender: TObject);
begin
  case (rgCriterio.ItemIndex) of
  0 : begin
        //Se clicar no crit�rio "Per�odo de Lan�amento"
        mskData1.Clear;
        mskData1.Enabled := True;
        mskData2.Clear;
        mskData2.Enabled := True;
        cbUnidadePesquisa.ItemIndex := -1;
        cbUnidadePesquisa.Enabled := False;
        edAdolescente.Clear;
        edAdolescente.Enabled := False;
        btnPesquisaAdol.Enabled := False;
      end;
  1 : begin
        //Se clicar no crit�rio "Por Adolescente"
        mskData1.Clear;
        mskData1.Enabled := False;
        mskData2.Clear;
        mskData2.Enabled := False;
        cbUnidadePesquisa.ItemIndex := -1;
        cbUnidadePesquisa.Enabled := False;
        edAdolescente.Clear;
        edAdolescente.Enabled := True;
        btnPesquisaAdol.Enabled := True;
      end;
  2 : begin
        //Se clicar no crit�rio "Por Unidade"
        mskData1.Clear;
        mskData1.Enabled := True;
        mskData2.Clear;
        mskData2.Enabled := True;
        cbUnidadePesquisa.ItemIndex := -1;
        cbUnidadePesquisa.Enabled := True;
        edAdolescente.Clear;
        edAdolescente.Enabled := False;
        btnPesquisaAdol.Enabled := False;
      end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.mskDataDigitacaoExit(
  Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(mskDataDigitacao.Text);

    if StrToDate(mskDataDigitacao.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskDataDigitacao.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskDataDigitacao.SetFocus;
    end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.mskDataAvaliacaoExit(
  Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(mskDataAvaliacao.Text);

    if StrToDate(mskDataAvaliacao.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskDataAvaliacao.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskDataAvaliacao.SetFocus;
    end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.mskDataInicioPraticaExit(
  Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(mskDataDigitacao.Text);

    if StrToDate(mskDataInicioPratica.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskDataInicioPratica.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskDataInicioPratica.SetFocus;
    end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnResultadoClick(
  Sender: TObject);
begin
  //Validar se o crit�rio foi selecionado
  if (rgCriterio.ItemIndex > -1) then
  begin

    //Com o crit�rio definido, montar os par�metros de consulta...
    case rgCriterio.ItemIndex of
      0 : begin
          //Per�odo de Lan�amento
          try
            with dmDeca.cdsSel_AvaliacoesApr do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_avaliacao').Value := Null;
              Params.ParamByName ('@pe_cod_matricula').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := Null;
              Params.ParamByName ('@pe_num_secao').Value := Null;
              Params.ParamByName ('@pe_nom_asocial').Value := Null;
              Params.ParamByName ('@pe_dat_av1').Value := StrToDate(mskData1.Text);
              Params.ParamByName ('@pe_dat_av2').Value := StrToDate(mskData2.Text);
              Open;

              if (dmDeca.cdsSel_AvaliacoesApr.RecordCount > 0) then
              begin
                //Se encontrou registros, atualiza o grid...
                dbgResultado.Refresh;
              end
              else
              begin
                //ShowMessage ('N�o existem Avalia��es lan�adas no per�odo informado...');
                Application.MessageBox('N�o foram encontradas Avalia��es lan�adas ' + #13+#10+
                                       'no per�odo informado. Favor verificar o per�odo ' + #13+#10+
                                       'e tentar novamente. Caso contr�rio entre em contato com ' + #13+#10+
                                       'o Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [Consulta]',
                                       MB_ICONASTERISK + MB_OK );
              end;
            end;
          except
                Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                                       'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                                       'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                                       'Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [ERRO]',
                                       MB_ICONERROR + MB_OK );

          end;

        end; //Fim CASE "0"

      1 : begin
          //Por Adolescente
          try
            with dmDeca.cdsSel_AvaliacoesApr do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_avaliacao').Value := Null;
              Params.ParamByName ('@pe_cod_matricula').Value := Copy(edAdolescente.Text,1,8);
              Params.ParamByName ('@pe_cod_unidade').Value := Null;
              Params.ParamByName ('@pe_num_secao').Value := Null;
              Params.ParamByName ('@pe_nom_asocial').Value := Null;
              Params.ParamByName ('@pe_dat_av1').Value := Null;
              Params.ParamByName ('@pe_dat_av2').Value := Null;
              Open;

              if (dmDeca.cdsSel_AvaliacoesApr.RecordCount > 0) then
              begin
                //Se encontrou registros, atualiza o grid...
                dbgResultado.Refresh;
              end
              else
              begin
                //ShowMessage ('N�o existem Avalia��es lan�adas no per�odo informado...');
                Application.MessageBox('N�o foram encontradas Avalia��es lan�adas ' + #13+#10+
                                       'para a matr�cula informada. Favor verificar o N�mero da ' + #13+#10+
                                       'Matr�cula e tentar novamente. Caso contr�rio entre em ' + #13+#10+
                                       'contato com o Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [Consulta]',
                                       MB_ICONASTERISK + MB_OK );
              end;
            end;
          except
                Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                                       'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                                       'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                                       'Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [ERRO]',
                                       MB_ICONERROR + MB_OK );

          end;

        end; //Fim CASE "1"

      2 : begin
          //Por Unidade (necessita per�odo)
          try
            with dmDeca.cdsSel_AvaliacoesApr do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_avaliacao').Value := Null;
              Params.ParamByName ('@pe_cod_matricula').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadePesquisa.ItemIndex]);
              Params.ParamByName ('@pe_num_secao').Value := Null;
              Params.ParamByName ('@pe_nom_asocial').Value := Null;
              Params.ParamByName ('@pe_dat_av1').Value := StrToDate(mskData1.Text);
              Params.ParamByName ('@pe_dat_av2').Value := StrToDate(mskData2.Text);
              Open;

              if (dmDeca.cdsSel_AvaliacoesApr.RecordCount > 0) then
              begin
                //Se encontrou registros, atualiza o grid...
                dbgResultado.Refresh;
              end
              else
              begin
                //ShowMessage ('N�o existem Avalia��es lan�adas no per�odo informado...');
                Application.MessageBox('N�o foram encontradas Avalia��es lan�adas ' + #13+#10+
                                       'para a Unidade/Per�odo informados. Favor refazer ' + #13+#10+
                                       'a consulta. Caso contr�rio entre em ' + #13+#10+
                                       'contato com o Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [Consulta]',
                                       MB_ICONASTERISK + MB_OK );
              end;
            end;
          except
                Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                                       'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                                       'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                                       'Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [ERRO]',
                                       MB_ICONERROR + MB_OK );

          end;
        end; //Fim CASE "2"

      end;  //CASE

  end
  else
  begin
    ShowMessage ('Favor selecionar um crit�rio de pesquisa...');
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnPesquisaAdolClick(
  Sender: TObject);
begin
  //Chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    //frmFichaCrianca.FillForm('S', vvCOD_MATRICULA_PESQUISA);
    //txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;

    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := vvCOD_MATRICULA_PESQUISA;
        Params.ParamByName('@pe_cod_unidade').Value:= Null;//vvCOD_UNIDADE;
        Open;
        edAdolescente.Text := FieldByName('cod_matricula').AsString + ' - ' + FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.mskData1Exit(Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(mskData1.Text);

    if StrToDate(mskData1.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskData1.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskData1.SetFocus;
    end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.mskData2Exit(Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(mskData2.Text);

    if StrToDate(mskData2.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      mskData2.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      mskData2.SetFocus;
    end;
  end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.dbgResultadoDblClick(
  Sender: TObject);
var vInd : Integer;
begin
  //Carrega os dados da avalia��o atual no tela de altera��o dos dados
  vUnidade2.Find(dmDeca.cdsSel_AvaliacoesApr.FieldByName('nom_unidade').AsString, vInd);
  cbUnidadeAltera.ItemIndex := vInd;

  mskDataDigitacaoAltera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('vDatInsercao').Value;
  mskDataAvaliacaoAltera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('vDatAvaliacao').Value;
  mskDataInicioAprendAltera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('vDatInicio').Value;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica01').AsString)=0) then
    meJustificativa01_Altera.Text := ''
  else
    meJustificativa01_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica01').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica02').AsString)=0) then
    meJustificativa02_Altera.Text := ''
  else
    meJustificativa02_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica02').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica03').AsString)=0) then
    meJustificativa03_Altera.Text := ''
  else
    meJustificativa03_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica03').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica04').AsString)=0) then
    meJustificativa04_Altera.Text := ''
  else
    meJustificativa04_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica04').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica05').AsString)=0) then
    meJustificativa05_Altera.Text := ''
  else
    meJustificativa05_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica05').AsString;

    if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica06').AsString)=0) then
    meJustificativa06_Altera.Text := ''
  else
    meJustificativa06_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica06').AsString;

    if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica07').AsString)=0) then
    meJustificativa07_Altera.Text := ''
  else
    meJustificativa07_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica07').AsString;

    if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica08').AsString)=0) then
    meJustificativa08_Altera.Text := ''
  else
    meJustificativa08_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica08').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica09').AsString)=0) then
    meJustificativa09_Altera.Text := ''
  else
    meJustificativa09_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica09').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica10').AsString)=0) then
    meJustificativa10_Altera.Text := ''
  else
    meJustificativa10_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica10').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica11').AsString)=0) then
    meJustificativa11_Altera.Text := ''
  else
    meJustificativa11_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica11').AsString;

  if (Length(dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica12').AsString)=0) then
    meJustificativa12_Altera.Text := ''
  else
    meJustificativa12_Altera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_justifica12').AsString;

  rgItem01_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item01').Value;
  rgItem02_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item02').Value;
  rgItem03_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item03').Value;
  rgItem04_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item04').Value;
  rgItem05_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item05').Value;
  rgItem06_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item06').Value;
  rgItem07_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item07').Value;
  rgItem08_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item08').Value;
  rgItem09_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item09').Value;
  rgItem10_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item10').Value;
  rgItem11_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item11').Value;
  rgItem12_Altera.ItemIndex := dmDeca.cdsSel_AvaliacoesApr.FieldByName('flg_item12').Value;


  Edit15.Text := '255';
  Edit16.Text := '255';
  Edit17.Text := '255';
  Edit18.Text := '255';
  Edit19.Text := '255';
  Edit20.Text := '255';
  Edit21.Text := '255';
  Edit22.Text := '255';
  Edit23.Text := '255';
  Edit24.Text := '255';
  Edit25.Text := '255';
  Edit26.Text := '255';

  //Carrega a lista de se��es
  try
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_secao').Value := Null;
    Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadeAltera.ItemIndex]);
    Params.ParamByName('@pe_num_secao').Value := Null;
    Params.ParamByName('@pe_flg_status').Value := 0; //Somente os Ativos
    Open;

    cbSecaoAltera.Clear;
    while not eof do
    begin
      cbSecaoAltera.Items.Add (dmDeca.cdsSel_Secao.FieldByName('num_secao').Value + '-' + dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').Value);
      Next;
    end;
  end;

  cbSecaoAltera.Text := dmDeca.cdsSel_AvaliacoesApr.FieldByName('num_secao').Value + '-' + dmDeca.cdsSel_AvaliacoesApr.FieldByName('dsc_nom_secao').Value;

  except end;

  PageControl3.ActivePageIndex := 1;
  PageControl4.ActivePageIndex := 0;

end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa01_AlteraChange(
  Sender: TObject);
begin
  Edit15.Text := IntToStr(255-(Length(meJustificativa01_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa02_AlteraChange(
  Sender: TObject);
begin
  Edit16.Text := IntToStr(255-(Length(meJustificativa02_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa03_AlteraChange(
  Sender: TObject);
begin
  Edit17.Text := IntToStr(255-(Length(meJustificativa03_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa04_AlteraChange(
  Sender: TObject);
begin
  Edit18.Text := IntToStr(255-(Length(meJustificativa04_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa05_AlteraChange(
  Sender: TObject);
begin
  Edit19.Text := IntToStr(255-(Length(meJustificativa05_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa06_AlteraChange(
  Sender: TObject);
begin
  Edit20.Text := IntToStr(255-(Length(meJustificativa06_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa07_AlteraChange(
  Sender: TObject);
begin
  Edit21.Text := IntToStr(255-(Length(meJustificativa07_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa08_AlteraChange(
  Sender: TObject);
begin
  Edit22.Text := IntToStr(255-(Length(meJustificativa08_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa09_AlteraChange(
  Sender: TObject);
begin
  Edit23.Text := IntToStr(255-(Length(meJustificativa09_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa10_AlteraChange(
  Sender: TObject);
begin
  Edit24.Text := IntToStr(255-(Length(meJustificativa10_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa11_AlteraChange(
  Sender: TObject);
begin
  Edit25.Text := IntToStr(255-(Length(meJustificativa11_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.meJustificativa12_AlteraChange(
  Sender: TObject);
begin
  Edit26.Text := IntToStr(255-(Length(meJustificativa12_Altera.Text)));
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.btnAlterarLancamentoClick(
  Sender: TObject);
begin
  //Criar m�dulo para altera��o de dados da avalia��o de desempenho aprendiz
  try
    if Application.MessageBox('Alterar dados da AVALIA��O?',
           'Avalia��o Desemp. - Aprendiz [Altera��o Avalia��o]',
           MB_ICONQUESTION + MB_YESNO ) = idYes then

      //Se confirma a altera��o - Executa a procedure de altera��o da avaliacao
      begin

        with dmDeca.cdsAlt_AvaliacoesAprendiz do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_avaliacao').Value   := dmDeca.cdsSel_AvaliacoesApr.FieldByName('cod_id_avaliacao').AsInteger;
          Params.ParamByName ('@pe_dat_avaliacao').Value      := GetValue(mskDataAvaliacaoAltera, vtDate);
          Params.ParamByName ('@pe_dat_inicio_pratica').Value := GetValue(mskDataInicioAprendAltera, vtDate);
          Params.ParamByName ('@pe_flg_item01').Value         := rgItem01_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica01').Value    := GetValue(meJustificativa01_Altera.Text);
          Params.ParamByName ('@pe_flg_item02').Value         := rgItem02_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica02').Value    := GetValue(meJustificativa02_Altera.Text);
          Params.ParamByName ('@pe_flg_item03').Value         := rgItem03_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica03').Value    := GetValue(meJustificativa03_Altera.Text);
          Params.ParamByName ('@pe_flg_item04').Value         := rgItem04_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica04').Value    := GetValue(meJustificativa04_Altera.Text);
          Params.ParamByName ('@pe_flg_item05').Value         := rgItem05_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica05').Value    := GetValue(meJustificativa05_Altera.Text);
          Params.ParamByName ('@pe_flg_item06').Value         := rgItem06_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica06').Value    := GetValue(meJustificativa06_Altera.Text);
          Params.ParamByName ('@pe_flg_item07').Value         := rgItem07_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica07').Value    := GetValue(meJustificativa07_Altera.Text);
          Params.ParamByName ('@pe_flg_item08').Value         := rgItem08_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica08').Value    := GetValue(meJustificativa08_Altera.Text);
          Params.ParamByName ('@pe_flg_item09').Value         := rgItem09_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica09').Value    := GetValue(meJustificativa09_Altera.Text);
          Params.ParamByName ('@pe_flg_item10').Value         := rgItem10_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica10').Value    := GetValue(meJustificativa10_Altera.Text);
          Params.ParamByName ('@pe_flg_item11').Value         := rgItem11_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica11').Value    := GetValue(meJustificativa11_Altera.Text);
          Params.ParamByName ('@pe_flg_item12').Value         := rgItem12_Altera.ItemIndex;
          Params.ParamByName ('@pe_dsc_justifica12').Value    := GetValue(meJustificativa12_Altera.Text);
          //Permitir altera��o de: NUM_SECAO
          Params.ParamByName ('@pe_num_secao').Value          := Copy(cbSecaoAltera.Text,1,4);
          Params.ParamByName ('@pe_dat_inicio_pratica').Value := GetValue(mskDataInicioAprendAltera, vtDate);
          Execute;

          //Limpar os campos da tela "Dados para Altera��o"
          //Atualizar dbgResultado com as informa��es na tela de crit�rio
            //Validar se o crit�rio foi selecionado
          if (rgCriterio.ItemIndex > -1) then
          begin

            //Com o crit�rio definido, montar os par�metros de consulta...
            case rgCriterio.ItemIndex of
              0 : begin
                //Per�odo de Lan�amento
                try
                  with dmDeca.cdsSel_AvaliacoesApr do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_id_avaliacao').Value := Null;
                    Params.ParamByName ('@pe_cod_matricula').Value := Null;
                    Params.ParamByName ('@pe_cod_unidade').Value := Null;
                    Params.ParamByName ('@pe_num_secao').Value := Null;
                    Params.ParamByName ('@pe_nom_asocial').Value := Null;
                    Params.ParamByName ('@pe_dat_av1').Value := StrToDate(mskData1.Text);
                    Params.ParamByName ('@pe_dat_av2').Value := StrToDate(mskData2.Text);
                    Open;

                    if (dmDeca.cdsSel_AvaliacoesApr.RecordCount > 0) then
                    begin
                      //Se encontrou registros, atualiza o grid...
                      dbgResultado.Refresh;
                    end
                    else
                    begin
                      //ShowMessage ('N�o existem Avalia��es lan�adas no per�odo informado...');
                      Application.MessageBox('N�o foram encontradas Avalia��es lan�adas ' + #13+#10+
                                             'no per�odo informado. Favor verificar o per�odo ' + #13+#10+
                                             'e tentar novamente. Caso contr�rio entre em contato com ' + #13+#10+
                                             'o Administrador do Sistema !!!',
                                             'Avalia��o Desempenho - Aprendiz [Consulta]',
                                             MB_ICONASTERISK + MB_OK );
                    end;
                  end;
                except
                Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                                       'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                                       'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                                       'Administrador do Sistema !!!',
                                       'Avalia��o Desempenho - Aprendiz [ERRO]',
                                       MB_ICONERROR + MB_OK );

                end;
              end; //Fim CASE "0"

              1 : begin
                //Por Adolescente
                try
                  with dmDeca.cdsSel_AvaliacoesApr do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_id_avaliacao').Value := Null;
                    Params.ParamByName ('@pe_cod_matricula').Value := Copy(edAdolescente.Text,1,8);
                    Params.ParamByName ('@pe_cod_unidade').Value := Null;
                    Params.ParamByName ('@pe_num_secao').Value := Null;
                    Params.ParamByName ('@pe_nom_asocial').Value := Null;
                    Params.ParamByName ('@pe_dat_av1').Value := Null;
                    Params.ParamByName ('@pe_dat_av2').Value := Null;
                    Open;

                    if (dmDeca.cdsSel_AvaliacoesApr.RecordCount > 0) then
                    begin
                      //Se encontrou registros, atualiza o grid...
                      dbgResultado.Refresh;
                    end
                    else
                    begin
                      //ShowMessage ('N�o existem Avalia��es lan�adas no per�odo informado...');
                      Application.MessageBox('N�o foram encontradas Avalia��es lan�adas ' + #13+#10+
                                             'para a matr�cula informada. Favor verificar o N�mero da ' + #13+#10+
                                             'Matr�cula e tentar novamente. Caso contr�rio entre em ' + #13+#10+
                                             'contato com o Administrador do Sistema !!!',
                                             'Avalia��o Desempenho - Aprendiz [Consulta]',
                                             MB_ICONASTERISK + MB_OK );
                    end;
                  end;
                except
                  Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                                         'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                                         'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                                         'Administrador do Sistema !!!',
                                         'Avalia��o Desempenho - Aprendiz [ERRO]',
                                         MB_ICONERROR + MB_OK );
                end;
              end; //Fim CASE "1"

              2 : begin
                //Por Unidade (necessita per�odo)
                try
                  with dmDeca.cdsSel_AvaliacoesApr do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_id_avaliacao').Value := Null;
                    Params.ParamByName ('@pe_cod_matricula').Value := Null;
                    Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadePesquisa.ItemIndex]);
                    Params.ParamByName ('@pe_num_secao').Value := Null;
                    Params.ParamByName ('@pe_nom_asocial').Value := Null;
                    Params.ParamByName ('@pe_dat_av1').Value := StrToDate(mskData1.Text);
                    Params.ParamByName ('@pe_dat_av2').Value := StrToDate(mskData2.Text);
                    Open;

                    if (dmDeca.cdsSel_AvaliacoesApr.RecordCount > 0) then
                    begin
                      //Se encontrou registros, atualiza o grid...
                      dbgResultado.Refresh;
                    end
                    else
                    begin
                      //ShowMessage ('N�o existem Avalia��es lan�adas no per�odo informado...');
                      Application.MessageBox('N�o foram encontradas Avalia��es lan�adas ' + #13+#10+
                                             'para a Unidade/Per�odo informados. Favor refazer ' + #13+#10+
                                             'a consulta. Caso contr�rio entre em ' + #13+#10+
                                             'contato com o Administrador do Sistema !!!',
                                             'Avalia��o Desempenho - Aprendiz [Consulta]',
                                             MB_ICONASTERISK + MB_OK );
                    end;
                  end;
                except
                  Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                                         'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                                         'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                                         'Administrador do Sistema !!!',
                                         'Avalia��o Desempenho - Aprendiz [ERRO]',
                                         MB_ICONERROR + MB_OK );

                end;
              end; //Fim CASE "2"

            end; //CASE

            PageControl3.ActivePageIndex := 0;

          end
          else
          begin
            ShowMessage ('Favor selecionar um crit�rio de pesquisa...');
          end;

        end
      end
      else
      //Se n�o confirmar, limpa todos os campos e volta a tela de consulta...
      begin
        cbUnidadeAltera.ItemIndex := -1;
        cbSecaoAltera.ItemIndex := -1;
        mskDataDigitacaoAltera.Clear;
        mskDataAvaliacaoAltera.Clear;
        mskDataInicioAprendAltera.Clear;
        rgItem01_Altera.ItemIndex := -1;
        rgItem02_Altera.ItemIndex := -1;
        rgItem03_Altera.ItemIndex := -1;
        rgItem04_Altera.ItemIndex := -1;
        rgItem05_Altera.ItemIndex := -1;
        rgItem06_Altera.ItemIndex := -1;
        rgItem07_Altera.ItemIndex := -1;
        rgItem08_Altera.ItemIndex := -1;
        rgItem09_Altera.ItemIndex := -1;
        rgItem10_Altera.ItemIndex := -1;
        rgItem11_Altera.ItemIndex := -1;
        rgItem12_Altera.ItemIndex := -1;
        meJustificativa01_Altera.Clear;
        meJustificativa02_Altera.Clear;
        meJustificativa03_Altera.Clear;
        meJustificativa04_Altera.Clear;
        meJustificativa05_Altera.Clear;
        meJustificativa06_Altera.Clear;
        meJustificativa07_Altera.Clear;
        meJustificativa08_Altera.Clear;
        meJustificativa09_Altera.Clear;
        meJustificativa10_Altera.Clear;
        meJustificativa11_Altera.Clear;
        meJustificativa12_Altera.Clear;
        PageControl3.ActivePageIndex := 0;


      end;
  except end;
end;

procedure TfrmAvaliacaoDesempenhoAprendiz.cbUnidadeAlteraClick(
  Sender: TObject);
begin
  //Carrega a lista de se��es de acordo com a unidade selecionada
  try
  with dmDeca.cdsSel_Secao do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_secao').Value := Null;
    Params.ParamByName('@pe_cod_unidade').Value := StrToInt(vUnidade1.Strings[cbUnidadeAltera.ItemIndex]);
    Params.ParamByName('@pe_num_secao').Value := Null;
    Params.ParamByName('@pe_flg_status').Value := 0; //Somente os Ativos
    Open;

    cbSecao.Clear;
    while not eof do
    begin
      cbSecaoAltera.Items.Add (FieldByName('num_secao').Value + '-' + FieldByName('dsc_nom_secao').Value);
      Next;
    end;
  end;
  except end;
end;

end.
