unit uUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, Db, ExtCtrls, DBCtrls, ComCtrls, Buttons, jpeg;

type
  TfrmUsuarios = class(TForm)
    dsSel_Usuario: TDataSource;
    dbgUsuarios: TDBGrid;
    Panel1: TPanel;
    edNome: TEdit;
    rgPerfil: TRadioGroup;
    Label1: TLabel;
    edSenha: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    cbUnidade: TComboBox;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnExcluir: TBitBtn;
    btnFechar: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    edConfirmacao: TEdit;
    Label4: TLabel;
    rgSituacao: TRadioGroup;
    Label5: TLabel;
    cbPerfil: TComboBox;
    Image1: TImage;
    Image2: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    { Private declarations }
    procedure FormatoTela(Modelo: String; IncAlt: String);
  public
    { Public declarations }
  end;

var
  frmUsuarios: TfrmUsuarios;
  vListaUnidade1 : TStringList;
  vListaUnidade2 : TStringList;

  //Lista de perfis
  vListaPerfil1 : TStringList;
  vListaPerfil2 : TStringList;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmUsuarios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  vListaUnidade1.Free;
  vListaUnidade2.Free;
  vListaPerfil1.Free;
  vListaPerfil2.Free;
  dmDeca.cdsSel_Usuario.Close;
end;

procedure TfrmUsuarios.FormCreate(Sender: TObject);
begin
  // dimensiona a tela
  Height := 228;

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();
  vListaPerfil1 := TStringList.Create();
  vListaPerfil2 := TStringList.Create();

  //Carrega a lista de perfis
  try
    with dmDeca.cdsSel_Perfil do
    begin
      Close;
      Params.ParamByName('@pe_cod_perfil').Value := Null;
      Open;

      cbPerfil.Clear;
      while not eof do
      begin
        cbPerfil.Items.Add(FieldByName('dsc_perfil').Value);
        vListaPerfil1.Add(IntToStr(FieldByName('cod_perfil').Value));
        vListaPerfil2.Add(FieldByName('dsc_perfil').Value);
        Next;
      end;
      vListaPerfil2.Sort;
    end;
    except end;

  // atualiza o cdsSel_Usuario
  try
    with uDM.dmDeca.cdsSel_Usuario do
    begin
      Close;
      Params.ParamByName('@pe_cod_usuario').value:= NULL;
      Params.ParamByName('@pe_cod_unidade').value:= NULL;
      Params.ParamByName('@pe_nom_usuario').value:= NULL;
      Params.ParamByName('@pe_flg_situacao').Value := Null;
      Open;
    end;
  except end;

  // carregar lista de unidades
  try
    with uDM.dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').value:= NULL;
      Params.ParamByName('@pe_nom_unidade').value:= NULL;
      Open;
      cbUnidade.Items.Clear;
      while not Eof do
      begin
        cbUnidade.Items.Add(FieldByName('nom_unidade').AsString);
        vListaUnidade1.Add(IntToStr(FieldByName('cod_unidade').AsInteger));
        vListaUnidade2.Add(FieldByName('nom_unidade').AsString);
        Next;
      end;
      Close;
      vListaUnidade2.Sort;
    end;
  except end;

end;

procedure TfrmUsuarios.btnNovoClick(Sender: TObject);
begin

  FormatoTela('Edi��o', 'Inclus�o');

end;

procedure TfrmUsuarios.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end;
end;

procedure TfrmUsuarios.FormatoTela(Modelo: String; IncAlt: String);
begin

  if Modelo = 'Padr�o' then
  begin
    // dimensiona o tamanho da tela
    Height := 228;
    Position := poScreenCenter;
    dbgUsuarios.Enabled := True;

    // configura os bot�es
    btnNovo.Enabled := true;
    btnAlterar.Enabled := true;
    btnExcluir.Enabled := true;
    btnSalvar.Enabled := false;
    btnCancelar.Enabled := false;

    // campos de edi��o
    Panel1.Enabled := false;
    edNome.Enabled := false;
    edSenha.Enabled := false;
    edConfirmacao.Enabled := false;
    cbUnidade.Enabled := false;
    //rgPerfil.Enabled := false;
    cbPerfil.Enabled := False;
    rgSituacao.Enabled := False;

    btnNovo.SetFocus;

  end
  else if Modelo = 'Edi��o' then
  begin
    // dimensiona o tamanho da tela
    Height := 453;
    Position := poScreenCenter;
    dbgUsuarios.Enabled := False;

    // configura os bot�es
    btnNovo.Enabled := false;
    btnAlterar.Enabled := false;
    btnExcluir.Enabled := false;
    btnSalvar.Enabled := true;
    btnCancelar.Enabled := true;

    // campos de edi��o
    Panel1.Enabled := true;
    edNome.Clear;
    edNome.Enabled := true;
    edSenha.Clear;
    edSenha.Enabled := true;
    edConfirmacao.Clear;
    edConfirmacao.Enabled := true;
    cbUnidade.ItemIndex := -1;
    cbUnidade.Enabled := True;
    //rgPerfil.ItemIndex := 3;
    cbPerfil.Enabled := True;
    cbPerfil.ItemIndex := 0;
    rgPerfil.Enabled := true;
    rgSituacao.Enabled := true;
    rgSituacao.ItemIndex := 0; //Padr�o ATIVO

    if IncAlt = 'Inclus�o' then
      btnSalvar.Caption := '&Incluir'
    else
      btnSalvar.Caption := '&Salvar';

    edNome.SetFocus;

  end;

end;

procedure TfrmUsuarios.btnSalvarClick(Sender: TObject);
var
  vOk : boolean;

begin

  // valida��o dos dados
  vOk := true;

  if (edNome.Text = '') or
     (edSenha.Text = '') or
     (edConfirmacao.Text = '') or
     (cbUnidade.ItemIndex = -1) or
     (cbPerfil.ItemIndex = -1) or
     //(rgPerfil.ItemIndex = -1) or
     (rgSituacao.ItemIndex = -1) then
  begin
    vOk := false;
    Application.MessageBox('Dados Inv�lidos!'+#13+#10+#13+#10+
          'Verifique se os dados foram preenchidos.',
          'Cadastro de Usu�rios',
          MB_OK + MB_ICONERROR);
    edNome.SetFocus;
  end;

  if vOk and (edSenha.Text <> edConfirmacao.Text) then
  begin
    vOk := false;
    Application.MessageBox('Dados Inv�lidos!'+#13+#10+#13+#10+
          'Use o campo CONFIRMA��O para repetir a senha.',
          'Cadastro de Usu�rios',
          MB_OK + MB_ICONERROR);
    edConfirmacao.SetFocus;
  end;

  if vOk and (btnSalvar.Caption = '&Incluir') then
  begin
    // inclusao de usu�rio
    try
      with dmDeca.cdsInc_Usuario do
      begin
        Close;
        Params.ParamByName('@pe_nom_usuario').AsString := edNome.Text;
        //Params.ParamByName('@pe_ind_perfil').AsInteger := (rgPerfil.ItemIndex + 1);
        Params.ParamByName('@pe_ind_perfil').AsInteger := (cbPerfil.ItemIndex + 1);
        Params.ParamByName('@pe_dsc_senha').AsString := edSenha.Text;
        Params.ParamByName('@pe_flg_situacao').AsInteger := rgSituacao.ItemIndex;
        Params.ParamByName('@pe_flg_status').AsInteger := 1; //Padr�o para novos usu�rio - Status Off-Line
        Params.ParamByName('@pe_cod_unidade').AsInteger := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
        Params.ParamByName('@pe_log_cod_usuario').AsInteger:= vvCOD_USUARIO;
        Execute;
      end;

    except end;

  end
  //else if vOk and (btnSalvar.Caption = '&Alterar') then
  else if vOk and (btnSalvar.Caption = '&Salvar') then
  begin
    // altera��o de usu�rio
    try
      with dmDeca.cdsAlt_Usuario do
      begin
        Close;
        Params.ParamByName('@pe_cod_usuario').AsInteger := dbgUsuarios.datasource.dataset.FieldByName('cod_usuario').AsInteger;
        Params.ParamByName('@pe_nom_usuario').AsString := edNome.Text;
        //Params.ParamByName('@pe_ind_perfil').AsInteger := (rgPerfil.ItemIndex + 1);
        Params.ParamByName('@pe_ind_perfil').AsInteger := (cbPerfil.ItemIndex + 1);
        Params.ParamByName('@pe_dsc_senha').AsString := edSenha.Text;
        //Para altera��o de situa��o, basta informar se ele ser� desligado ou n�o, pois
        //a efeito de estar on-line ou off-line, somente ser� alterada
        //no momento que o usu�rio fizer o login/logoff do sistema.
        Params.ParamByname('@pe_flg_situacao').AsInteger := rgSituacao.ItemIndex;
        Params.ParamByName('@pe_cod_unidade').AsInteger := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
        Execute;
      end;
    except end;

  end;

  if vOk then
  begin
    // atualiza o DBGrid
    dmDeca.cdsSel_Usuario.Refresh;
    dbgUsuarios.Refresh;

    FormatoTela('Padr�o', '');
  end;

end;

procedure TfrmUsuarios.btnCancelarClick(Sender: TObject);
begin
  FormatoTela('Padr�o', '');
end;

procedure TfrmUsuarios.btnAlterarClick(Sender: TObject);
var
  vIndex : Integer;

begin

  with dmDeca.cdsSel_Usuario do
  begin
    if FieldByName('cod_usuario').AsInteger > 0 then
    begin

      FormatoTela('Edi��o', 'Altera��o');

      vListaUnidade2.Find(FieldByName('nom_unidade').AsString, vIndex);
      //vListaPerfil2.Find(FieldByName('dsc_perfil').AsString, vIndex2);
      cbPerfil.ItemIndex := FieldByName('ind_perfil').AsInteger - 1;

      // carregar os campos na tela
      edNome.Text := FieldByName('nom_usuario').AsString;
      edSenha.Text := FieldByName('dsc_senha').AsString;
      edConfirmacao.Text := FieldByName('dsc_senha').AsString;
      cbUnidade.ItemIndex := vIndex ;
      //rgPerfil.ItemIndex := FieldByName('ind_perfil').AsInteger - 1;
      rgSituacao.ItemIndex := FieldByName ('flg_situacao').AsInteger;

    end;
  end;

end;

procedure TfrmUsuarios.btnExcluirClick(Sender: TObject);
begin

  if (dmDeca.cdsSel_Usuario.FieldByName('cod_usuario').AsInteger > 0) and
     (dmDeca.cdsSel_Usuario.FieldByName('cod_usuario').AsInteger <> vvCOD_USUARIO) then
  begin

    // pega o conteudo da primeira coluna do select - cod_usuario
    if Application.MessageBox('Deseja Excluir o usu�rio selecionado?',
           'Cadastro de Usu�rios - Exclus�o',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
    begin

      // exclus�o de usu�rio
      try
        with dmDeca.cdsExc_Usuario do
        begin
          Params.ParamByName('@pe_cod_usuario').AsInteger:= dbgUsuarios.datasource.dataset.FieldByName('cod_usuario').AsInteger;
          Execute;
        end;
      except end;
      // atualiza o DBGrid
      dmDeca.cdsSel_Usuario.Refresh;
      dbgUsuarios.Refresh;

    end;
  end;

end;

end.

