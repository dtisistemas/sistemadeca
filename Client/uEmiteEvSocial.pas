unit uEmiteEvSocial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmEmiteEvSocial = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txtDataInicial: TMaskEdit;
    txtDataFinal: TMaskEdit;
    Panel1: TPanel;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnSairClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure txtDataInicialExit(Sender: TObject);
    procedure txtDataFinalExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteEvSocial: TfrmEmiteEvSocial;

implementation

uses uFichaPesquisa, uDM, rEvolucaoSocial, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmEmiteEvSocial.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmEmiteEvSocial.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmEmiteEvSocial.Close;
  end;
end;

procedure TfrmEmiteEvSocial.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEmiteEvSocial.AtualizaCamposBotoes(Modo: String);
begin
if Modo = 'Padrao' then
  begin

    txtMatricula.Clear;
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;

    txtDataInicial.Clear;
    txtDataInicial.Enabled := False;
    txtDataFinal.Clear;
    txtDataFinal.Enabled := False;

    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Emissao' then
  begin

    txtDataInicial.Enabled := True;
    txtDataFinal.Enabled := True;

    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := True;

  end;
end;

procedure TfrmEmiteEvSocial.btnSairClick(Sender: TObject);
begin
  frmEmiteEvSocial.Close;
end;

procedure TfrmEmiteEvSocial.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        AtualizaCamposBotoes('Emissao');
        txtDataInicial.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmEmiteEvSocial.txtMatriculaExit(Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;
            AtualizaCamposBotoes('Emissao');
            txtDataInicial.SetFocus;
          end;
      end;
    except end;
  end;
end;

procedure TfrmEmiteEvSocial.btnSalvarClick(Sender: TObject);
begin
with dmDeca.cdsSel_Cadastro_Evolucao_Periodo do
  begin
    Close;
    Params.ParamByName('@pe_cod_matricula').Value := txtMatricula.Text;
    Params.ParamByName('@pe_dat_inicial').Value := GetValue(txtDataInicial, vtDate);
    Params.ParamByName('@pe_dat_final').Value := GetValue(txtDataFinal, vtDate);
    Open;

    Application.CreateForm(TrelEvolucaoSocial, relEvolucaoSocial);
    relEvolucaoSocial.lblMatricula.Caption := frmEmiteEvSocial.txtMatricula.Text;
    relEvolucaoSocial.lbNome.Caption := frmEmiteEvSocial.lbNome.Caption;
    relEvolucaoSocial.lbUsuario.Caption := vvNOM_USUARIO;
    relEvolucaoSocial.Preview;
    relEvolucaoSocial.Free;
    AtualizaCamposBotoes('Padrao');
  end;
end;

procedure TfrmEmiteEvSocial.txtDataInicialExit(Sender: TObject);
begin
  try
    StrToDate(txtDataInicial.Text);
  except
    begin
      ShowMessage('Data inv�lida...');
      txtDataInicial.Clear;
      txtDataInicial.SetFocus;
    end;
  end;
end;

procedure TfrmEmiteEvSocial.txtDataFinalExit(Sender: TObject);
begin
try
    StrToDate(txtDataFinal.Text);

    if StrToDate(txtDataFinal.Text) < StrToDate(txtDataInicial.Text) then
        begin
          ShowMessage('Data final do periodo � menor do que a data inicial.' + #13#10#13 +
                      'Favor informe data v�lida para realizar a consulta...');
          txtDataFinal.SetFocus;
        end
    else
      begin
        btnSalvar.Enabled := True;
        btnSalvar.SetFocus;
      end;
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataFinal.Clear;
      txtDataFinal.SetFocus;
    end;
  end;
end;

end.
