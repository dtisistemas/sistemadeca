unit rDadosEscolares;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelDadosEscolares = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    lbUnidade: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText6: TQRDBText;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText7: TQRDBText;
    QRLabel2: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel10: TQRLabel;
  private

  public

  end;

var
  relDadosEscolares: TrelDadosEscolares;

implementation

uses uDM;

{$R *.DFM}

end.
