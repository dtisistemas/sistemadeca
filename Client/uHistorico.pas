unit uHistorico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Mask, Db;

type
  TfrmHistorico = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    btnReemissao: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    dbgHistorico: TDBGrid;
    dsSel_Historico: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure dbgHistoricoCellClick(Column: TColumn);
    procedure AtualizaCamposBotoes(Modo : String);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnReemissaoClick(Sender: TObject);
    function CalculaIdadeHist(Nascimento : TDateTime ) : String ;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistorico: TfrmHistorico;
  vIndex : Integer;

implementation

uses uDM, uFichaPesquisa, uPrincipal, rAdvertencias, rAfastamento,
  rDesligamento, rSuspensao, uUtil, uTransferenciaEncaminhamentoNova, rNovaTransferencia,
  rNovoDesligamento, uFichaCrianca;

{$R *.DFM}

procedure TfrmHistorico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmHistorico.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmHistorico.Close;
  end;
end;

procedure TfrmHistorico.txtMatriculaExit(Sender: TObject);
begin
{if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;

            with dmDeca.cdsSel_Historico do
            begin
              Params.ParamByName('@pe_cod_matricula').Value := Trim(txtMatricula.Text);
              //Params.ParamByName('@pe_cod_unidade').Value := Null;  //vvCOD_UNIDADE
              Open;
            end;
          AtualizaCamposBotoes('Reemitir');
          end;
      end;
    except end;
  end;}
end;

procedure TfrmHistorico.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        with dmDeca.cdsSel_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := vvCOD_MATRICULA_PESQUISA;
          Open;
        end;
        AtualizaCamposBotoes('Reemitir');
      end
    except end;
  end;
end;

procedure TfrmHistorico.btnSairClick(Sender: TObject);
begin
  frmHistorico.Close;
end;

procedure TfrmHistorico.dbgHistoricoCellClick(Column: TColumn);
begin
  vIndex := dsSel_Historico.DataSet.FieldByName('cod_id_historico').AsInteger;
end;

procedure TfrmHistorico.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := False;
    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;

    txtMatricula.Clear;
    txtMatricula.SetFocus;
    lbNome.Caption := '';

    dbgHistorico.DataSource := nil;

    btnReemissao.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := True;

  end
else if Modo = 'Reemitir' then
  begin
    GroupBox2.Enabled := True;
    dbgHistorico.DataSource := dsSel_Historico;
    btnReemissao.Enabled := True;
  end;
end;

procedure TfrmHistorico.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmHistorico.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmHistorico.btnReemissaoClick(Sender: TObject);
var mData: TDateTime;
    Dia,Mes,Ano: Word;
begin
  case dsSel_Historico.DataSet.FieldByName('ind_tipo_historico').AsInteger of
  //Advert�ncia
  1:  begin
    Application.CreateForm(TrelAdvertencias, relAdvertencias);
    relAdvertencias.lblDivisao.Caption := Divisao(txtMatricula.Text);
    relAdvertencias.lblUnidade.Caption := vvNOM_UNIDADE;
    relAdvertencias.lblNome.Caption := lbNome.Caption;
    relAdvertencias.lblOcorrencia.Lines.Add(dsSel_Historico.Dataset.FieldByName('dsc_ocorrencia').AsString);

    relAdvertencias.lblDia.Caption := Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),1,2);
    relAdvertencias.lblMes.Caption := cMeses[StrToInt(Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),4,2))];
    relAdvertencias.lblAno.Caption := Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),7,4);

    relAdvertencias.Preview;
    relAdvertencias.Free;
    //AtualizaCamposBotoes('Padrao');
  end;

  //Afastamento - Entrada
  2:  begin
    try
    with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
        Params.ParamByName ('@pe_ind_responsavel').Value := 1;
        Open;

        if not (dmDeca.cdsSel_Cadastro_Familia_Responsavel.Eof) then
        begin
          Application.CreateForm(TrelAfastamento, relAfastamento);
          relAfastamento.lblResponsavel.CAption := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName('nom_familiar').AsString;
          relAfastamento.lblDocResp.Caption := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName('ind_tipo_documento').AsString +
                                               ': ' + dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName('num_documento').AsString;;
          relAfastamento.lblNome.Caption := lbNome.Caption;
          relAfastamento.lblMatricula.CAption := txtMatricula.Text;

          relAfastamento.lblDia.Caption := Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),1,2);
          relAfastamento.lblMes.Caption := cMeses[StrToInt(Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),4,2))];
          relAfastamento.lblAno.Caption := Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),7,4);
          relAfastamento.lbMotivoAfastamento.Lines.Add(dsSel_Historico.Dataset.FieldByName('dsc_ocorrencia').AsString);

          relAfastamento.Preview;
          relAfastamento.Free;
          //AtualizaCamposBotoes('Padrao');
        end
      end
    except end;
  end; //final do item 2

  //Afastamento - Retorno
  3: begin
       ShowMessage('Aten��o!' + #13#10#13 +
                   'N�o existe formul�rio espec�fico para ' + dsSel_Historico.Dataset.FieldByName('dsc_tipo_historico').AsString);
     end;

  //Suspens�o
  4: begin
        Application.CreateForm(TrelSuspensao, relSuspensao);

        relSuspensao.lblDivisao.Caption := Divisao(txtMatricula.Text);
        relSuspensao.lblUnidade.Caption := vvNOM_UNIDADE;
        relSuspensao.lblNome.Caption := lbNome.Caption;

        //Calcular os dias de diferen�a
        mData := (dsSel_Historico.Dataset.FieldByName('dat_final_suspensao').AsDateTime - dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime);

        DecodeDate (mData, Ano,Mes,Dia);

        relSuspensao.lblDias.Caption := FloatToStr(mData);

        relSuspensao.lblMotivo.Lines.Add(dsSel_Historico.Dataset.FieldByName('dsc_ocorrencia').AsString);
        relSuspensao.lblDia.Caption := Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),1,2);
        relSuspensao.lblMes.Caption := cMeses[StrToInt(Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),4,2))];
        relSuspensao.lblAno.Caption := Copy(DateToStr(dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime),7,4);

        relSuspensao.Preview;
        relSuspensao.Free;
        //AtualizaCamposBotoes('Padrao');
     end;

  //Desligamento
  5: begin
        Application.CreateForm (TrelNovoDesligamento, relNovoDesligamento);
        relNovoDesligamento.lbMatricula1.Caption := txtMatricula.Text;
        relNovoDesligamento.lbMatricula2.Caption := txtMatricula.Text;
        relNovoDesligamento.lblProntuario1.Caption := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_prontuario').AsInteger);
        relNovoDesligamento.lblProntuario2.Caption := IntToStr(dmDeca.cdsSel_Cadastro_Move.FieldByName('num_prontuario').AsInteger);
        relNovoDesligamento.lblProjeto1.Caption := Divisao(txtMatricula.Text);
        relNovoDesligamento.lblProjeto2.Caption := Divisao(txtMatricula.Text);
        relNovoDesligamento.lblNome1.Caption := lbNome.Caption;
        relNovoDesligamento.lblNome2.Caption := lbNome.Caption;
        relNovoDesligamento.lblLocal1.Caption := vvNOM_UNIDADE;
        relNovoDesligamento.lblLocal2.Caption := vvNOM_UNIDADE;
        relNovoDesligamento.lbMotivoDesligamento1.Lines.Add (dmDeca.cdsSel_Historico.FieldByName('dsc_ocorrencia').AsString);
        relNovoDesligamento.lbMotivoDesligamento2.Lines.Add (dmDeca.cdsSel_Historico.FieldByName('dsc_ocorrencia').AsString);

        relNovoDesligamento.lbDataDesligamento1.Caption := dmDeca.cdsSel_Historico.FieldByName('Dat_Historico').Value;
        relNovoDesligamento.lbDataDesligamento2.Caption := dmDeca.cdsSel_Historico.FieldByName('Dat_Historico').Value;

        //case StrToInt(dsSel_Historico.Dataset.FieldByName('ind_motivo2').Value) of
        case dsSel_Historico.Dataset.FieldByName('ind_motivo2').Value of
          0: begin
               relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR MAIORIDADE';
               relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR MAIORIDADE';
             end;

          1: begin
               relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR T�RMINO DE CONTRATO';
               relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR T�RMINO DE CONTRATO';
             end;

          2: begin
               relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR ABANDONO';
               relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR ABANDONO';
             end;

          3: begin
               relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO A PEDIDO';
               relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO A PEDIDO';
             end;

          4: begin
               relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR JUSTA CAUSA';
               relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR JUSTA CAUSA';
             end;

          5: begin
              relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESLIGAMENTO POR MORTE';
              relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESLIGAMENTO POR MORTE';
             end;

          //Inclu�do em 09/12/2015 a Pedido da Divis�o de Empregabilidade  MEMORANDO CA 47/2016
          6: begin
              relNovoDesligamento.lbTipoDesligamento1.Caption := 'DESEMPENHO INSUFICIENTE OU INADAPTA��O DO APRENDIZ (TEORIA/PR�TICA)';
              relNovoDesligamento.lbTipoDesligamento2.Caption := 'DESEMPENHO INSUFICIENTE OU INADAPTA��O DO APRENDIZ (TEORIA/PR�TICA)';
             end;

          7: begin
              relNovoDesligamento.lbTipoDesligamento1.Caption := 'FALTA DISCIPLINAR GRAVE (ART. 482 CLT)';
              relNovoDesligamento.lbTipoDesligamento2.Caption := 'FALTA DISCIPLINAR GRAVE (ART. 482 CLT)';
             end;

          8: begin
              relNovoDesligamento.lbTipoDesligamento1.Caption := 'AUS�NCIA INJUSTIFICADA � ESCOLA QUE IMPLIQUE NA PERDA DO ANO LETIVO';
              relNovoDesligamento.lbTipoDesligamento2.Caption := 'AUS�NCIA INJUSTIFICADA � ESCOLA QUE IMPLIQUE NA PERDA DO ANO LETIVO';
             end;

          9: begin
              relNovoDesligamento.lbTipoDesligamento1.Caption := 'FREQU�NCIA INFERIOR A 75% NA APRENDIZAGEM';
              relNovoDesligamento.lbTipoDesligamento2.Caption := 'FREQU�NCIA INFERIOR A 75% NA APRENDIZAGEM';
             end;


        end;

        relNovoDesligamento.Preview;
        relNovoDesligamento.Free;

     end;

  //Transfer�ncia de Unidade - Encaminhamento
  6: begin

         //Transfere os dados para o relat�rio
         Application.CreateForm(TrelNovaTransferencia, relNovaTransferencia);

         relNovaTransferencia.lbMatricula.Caption := Trim(txtMatricula.Text);
         relNovaTransferencia.lbMatricula1.Caption := Trim(txtMatricula.Text);
         relNovaTransferencia.lbNome.Caption := lbNome.Caption;
         relNovaTransferencia.lbNome1.Caption := lbNome.Caption;
         relNovaTransferencia.lbEndereco.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_endereco').Value;
         relNovaTransferencia.lbEndereco1.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_endereco').Value;
         relNovaTransferencia.lbBairro.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_bairro').Value;
         relNovaTransferencia.lbBairro1.Caption := dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_bairro').Value;
         relNovaTransferencia.lbCep.Caption := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cep').AsString);
         relNovaTransferencia.lbCep1.Caption := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cep').AsString);

         //Verifica qual op��o foi gravada como motivo (ind_motivo1 no banco)
         if (dmDeca.cdsSel_Historico.FieldByName('ind_motivo1').AsString = '0') then
         begin
           relNovaTransferencia.lbTipo.Caption := 'DC/DAPAE';
           relNovaTransferencia.lbTipo1.Caption := 'DC/DAPAE'
         end
         else if (dmDeca.cdsSel_Historico.FieldByName('ind_motivo1').AsString = '1') then
         begin
           relNovaTransferencia.lbTipo.Caption := 'DAPA';
           relNovaTransferencia.lbTipo1.Caption := 'DAPA'
         end;

         //Verifica qual a op��o foi gravada como tipo (ind_motivo2 no banco)
         if (dmDeca.cdsSel_Historico.FieldByName('ind_motivo2').AsString = '0') then
         begin
           relNovaTransferencia.lbMotivo.Caption := 'OUTRA UNIDADE';
           relNovaTransferencia.lbMotivo1.Caption := 'OUTRA UNIDADE'
         end
         else if (dmDeca.cdsSel_Historico.FieldByName('ind_motivo2').AsString = '1') then
         begin
           relNovaTransferencia.lbMotivo.Caption := 'MUDAN�A DE VINCULO';
           relNovaTransferencia.lbMotivo1.Caption := 'MUDAN�A DE VINCULO'
         end;

         relNovaTransferencia.lbUnidadeOrigem.Caption := dmDeca.cdsSel_Historico.FieldByName('orig_unidade').Value + '/' + dmDeca.cdsSel_Historico.FieldByName('orig_secao').Value;
         relNovaTransferencia.lbUnidadeOrigem1.Caption := dmDeca.cdsSel_Historico.FieldByName('orig_unidade').Value+ '/' + dmDeca.cdsSel_Historico.FieldByName('orig_secao').Value;
         relNovaTransferencia.lbCcustoSecaoOrigem.Caption := dmDeca.cdsSel_Historico.FieldByName('orig_ccusto').Value + '.' + dmDeca.cdsSel_Historico.FieldByName('orig_num_secao').Value;
         relNovaTransferencia.lbCcustoSecaoOrigem1.Caption := dmDeca.cdsSel_Historico.FieldByName('orig_ccusto').Value + '.' + dmDeca.cdsSel_Historico.FieldByName('orig_num_secao').Value;
         relNovaTransferencia.lbBancoOrigem.Caption := dmDeca.cdsSel_Historico.FieldByName('orig_banco').AsString;
         relNovaTransferencia.lbBancoOrigem1.Caption := dmDeca.cdsSel_Historico.FieldByName('orig_banco').AsString;
         relNovaTransferencia.lbDataTransferencia.Caption := DateToStr(dmDeca.cdsSel_Historico.FieldByName('dat_historico').AsDateTime);
         relNovaTransferencia.lbDataTransferencia1.Caption := DateToStr(dmDeca.cdsSel_Historico.FieldByName('dat_historico').AsDateTime);

         relNovaTransferencia.lbUnidadeDestino.Caption := dmDeca.cdsSel_Historico.FieldByName('dest_unidade').Value + '/' + dmDeca.cdsSel_Historico.FieldByName('dest_secao').Value;;
         relNovaTransferencia.lbUnidadeDestino1.Caption := dmDeca.cdsSel_Historico.FieldByName('dest_unidade').Value + '/' + dmDeca.cdsSel_Historico.FieldByName('dest_secao').Value;;
         relNovaTransferencia.lbCcustoSecaoDestino.Caption := dmDeca.cdsSel_Historico.FieldByName('dest_ccusto').Value + '.' + dmDeca.cdsSel_Historico.FieldByName('dest_num_secao').Value;
         relNovaTransferencia.lbCcustoSecaoDestino1.Caption := dmDeca.cdsSel_Historico.FieldByName('dest_ccusto').Value + '.' + dmDeca.cdsSel_Historico.FieldByName('dest_num_secao').Value;
         relNovaTransferencia.lbBancoDestino.Caption := dmDeca.cdsSel_Historico.FieldByName('dest_banco').AsString;
         relNovaTransferencia.lbBancoDestino1.Caption := dmDeca.cdsSel_Historico.FieldByName('dest_banco').AsString;

         //Verifica se entrega passe no ato da transfer�ncia
         if (dmDeca.cdsSel_Historico.FieldByName('flg_passe').Value = 0) then
         begin
           relNovaTransferencia.lbEntregaPasse.Caption := 'SIM';
           relNovaTransferencia.lbEntregaPasse1.Caption := 'SIM'
         end
         else if (dmDeca.cdsSel_Historico.FieldByName('flg_passe').Value = 1) then
         begin
           relNovaTransferencia.lbEntregaPasse.Caption := 'N�O';
           relNovaTransferencia.lbEntregaPasse1.Caption := 'N�O'
         end;

         relNovaTransferencia.lbPeriodo.Caption := '[ ]Manh�  -  [ ]Tarde - [ ]Integral';
         relNovaTransferencia.lbPeriodo1.Caption := '[ ]Manh�  -  [ ]Tarde - [ ]Integral';

         relNovaTransferencia.lbEntregaPasse.Caption := '[ ]Sim  -  [ ]N�o';
         relNovaTransferencia.lbEntregaPasse1.Caption := '[ ]Sim  -  [ ]N�o';

         relNovaTransferencia.lbCotas.Caption := FloatToStr(dmDeca.cdsSel_Historico.FieldByName('num_cotas_passe').AsFloat) + ' COTA(S) DE PASSE.';
         relNovaTransferencia.lbCotas1.Caption := FloatToStr(dmDeca.cdsSel_Historico.FieldByName('num_cotas_passe').AsFloat) + ' COTA(S) DE PASSE.';;

         relNovaTransferencia.lbIdade1.Caption := CalculaIdadeHist(dmDeca.cdsSel_cadastro_Move.FieldByName ('dat_nascimento').AsDateTime);
         relNovaTransferencia.lbIdade2.Caption := CalculaIdadeHist(dmDeca.cdsSel_cadastro_Move.FieldByName ('dat_nascimento').AsDateTime);

         relNovaTransferencia.Preview;
         relNovaTransferencia.Free;

     end;

  //Transfer�ncia de Unidade - Confirma��o
  7: begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Transfer�ncia para Conv�nio - Encaminhamento
  8: begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Transfer�ncia para Conv�nio - Retorno
  9: begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Outras transfer�ncias
  10:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Mudan�a de Matr�cula
  11:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Registro de Atendimento
  12:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Complementa��o em Alfabetiza��o - Encaminhamento
  13:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  //Complementa��o em Alfabetiza��o - Retorno
  14:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
     end;

  15:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  16:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  17:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  18:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  19:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;


  20:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  21:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  22:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  23:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  24:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

  25:begin
       Application.MessageBox ('Aten��o!!!' + #13#10#13 +
                               'N�o existe formul�rio dispon�vel para (re)impress�o desta Interven��o.',
                               '[Sistema Deca] - Emiss�o de Formul�rios',
                               MB_OK + MB_ICONWARNING);
    end;

end;
end;

function TfrmHistorico.CalculaIdadeHist(Nascimento: TDateTime): String;
var
  Periodo : Integer;
  intContAnos, intContMeses : Integer ;
  DataFinal : TDateTime;

begin
    // calculo de anos
    intContAnos := 0 ;
    Periodo := 12 ;
    DataFinal := dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime;
    Repeat
      Inc(intContAnos) ;
      DataFinal := IncMonth(DataFinal,Periodo * -1) ;
    Until DataFinal < Nascimento ;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContAnos,-1) ;

    // calculo de meses
    intContMeses := 0 ;
    Periodo := 1 ;
    DataFinal := dsSel_Historico.Dataset.FieldByName('dat_historico').AsDateTime;
    Repeat
      Inc(intContMeses) ;
      DataFinal := IncMonth(DataFinal,Periodo * -1) ;
    Until DataFinal < Nascimento;

    //DataFinal := IncMonth(DataFinal,Periodo) ;
    Inc(intContMeses,-1) ;
    intContMeses := intContMeses mod 12;

    if intContAnos <= 9 then
      Result := '0' + IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses'
    else
      Result := IntToStr(intContAnos) + ' anos e ' + IntToStr(intContMeses) + ' meses'; 

end;

end.
