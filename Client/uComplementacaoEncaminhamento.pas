unit uComplementacaoEncaminhamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmComplementacaoEncaminhamento = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    txtData: TMaskEdit;
    txtEncaminhadoPor: TEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    Label3: TLabel;
    txtDescricao: TMemo;
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComplementacaoEncaminhamento: TfrmComplementacaoEncaminhamento;

implementation

uses uDM, uPrincipal, uUtil, uFichaPesquisa;

{$R *.DFM}

procedure TfrmComplementacaoEncaminhamento.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmComplementacaoEncaminhamento.btnCancelarClick(
  Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmComplementacaoEncaminhamento.btnSairClick(Sender: TObject);
begin
  frmComplementacaoEncaminhamento.Close;
end;

procedure TfrmComplementacaoEncaminhamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmComplementacaoEncaminhamento.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmComplementacaoEncaminhamento.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
      // desabilita processamento posterior da tecla
      Key := #0;
      Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
    end
    else if (Key = #27) then
    begin
      Key := #0;
      frmComplementacaoEncaminhamento.Close;
    end;
end;

procedure TfrmComplementacaoEncaminhamento.txtMatriculaExit(
  Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
        begin
          lbNome.Caption := 'Ficha n�o cadastrada';
          AtualizaCamposBotoes('Novo');
        end
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmComplementacaoEncaminhamento.txtMatriculaChange(
  Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmComplementacaoEncaminhamento.AtualizaCamposBotoes(
  Modo: String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtEncaminhadoPor.Clear;
    txtDescricao.Clear;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtEncaminhadoPor.Enabled := False;
    txtDescricao.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtData.Text := DateToSTr(Date());
    txtEncaminhadoPor.Clear;
    txtDescricao.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtEncaminhadoPor.Enabled := True;
    txtDescricao.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;

    txtMatricula.SetFocus;
  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;

    btnImprimir.SetFocus;
  end
end;

procedure TfrmComplementacaoEncaminhamento.btnPesquisarClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmComplementacaoEncaminhamento.btnSalvarClick(Sender: TObject);
begin

  if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      //if StatusCadastro(txtMatricula.Text) = 'Ativo' then

      if (StatusCadastro(txtMatricula.Text) = 'Ativo') OR (StatusCadastro(txtMatricula.Text) = 'Afastado') OR
          (StatusCadastro(txtMatricula.Text) = 'Suspenso') then

      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 13; //Complementa��o em Alfabetiza��o - Encaminhamento
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Complementa��o em Alfabetiza��o - Encaminhamento';
          Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtDescricao.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;
        end;
        AtualizaCamposBotoes('Salvar');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except
      ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10 +#13+#10 +
               'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;

end;

end.
