unit uEmissaoDeclaracoesSolicitacoesEscolares;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Mask, Buttons;

type
  TfrmEmissaoDeclaracoesSolicitacoesEscolares = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    mskMatricula: TMaskEdit;
    btnLocalizar: TSpeedButton;
    edNome: TEdit;
    GroupBox2: TGroupBox;
    mskNascimento: TMaskEdit;
    GroupBox3: TGroupBox;
    mskNumRgEscolar: TMaskEdit;
    GroupBox4: TGroupBox;
    edEndereco: TEdit;
    GroupBox5: TGroupBox;
    edBairro: TEdit;
    GroupBox6: TGroupBox;
    edUnidade: TEdit;
    GroupBox7: TGroupBox;
    meJustificativa: TMemo;
    btnGerarSolicitacao: TSpeedButton;
    GroupBox8: TGroupBox;
    mskDataSolicitacao: TMaskEdit;
    GroupBox9: TGroupBox;
    edUsuario: TEdit;
    GroupBox10: TGroupBox;
    cbTipoSolicitacaoAtestado: TComboBox;
    GroupBox11: TGroupBox;
    edEscolaAtual: TEdit;
    GroupBox12: TGroupBox;
    edSerieAtual: TEdit;
    cbEscola: TComboBox;
    btnLimparListaEscolas: TSpeedButton;
    SpeedButton2: TSpeedButton;
    cbSerie: TComboBox;
    GroupBox13: TGroupBox;
    lbNomeEscola: TLabel;
    lbEnderecoEscola: TLabel;
    lbBairroEscola: TLabel;
    lbCepEscola: TLabel;
    lbDiretora: TLabel;
    lbTelefone: TLabel;
    lstEscolas: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    procedure btnLocalizarClick(Sender: TObject);
    procedure btnGerarSolicitacaoClick(Sender: TObject);
    procedure cbTipoSolicitacaoAtestadoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbEscolaClick(Sender: TObject);
    procedure btnLimparListaEscolasClick(Sender: TObject);
    procedure cbSerieClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmissaoDeclaracoesSolicitacoesEscolares: TfrmEmissaoDeclaracoesSolicitacoesEscolares;
  vListaEscolaS1, vListaEscolaS2 : TStringList;
  vListaSerieS1, vListaSerieS2 : TStringList;
  mmNOM_ESCOLA, mmENDERECO_ESCOLA, mmFONE_ESCOLA, mmBAIRRO_ESCOLA, mmCEP_ESCOLA, mmNOM_DIRETORA : String;
  mmTT_ESCOLAS : Integer;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil,
  rSolicitacaoTransferenciaPeriodoEscolar, rAtestadoConvenio,
  rSolicitacaoVagaEscolar, uCadastroSeriesPorEscola;

{$R *.DFM}

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.btnLocalizarClick(
  Sender: TObject);
begin

  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    mskMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := mskMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        mskMatricula.Text    := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('cod_matricula').AsString);
        edNome.Text          := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_nome').AsString);

        mskNascimento.Text   := dmDeca.cdsSel_Cadastro_Move.FieldByName('dat_nascimento').Value;
        mskNumRgEscolar.Text := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('dsc_rg_escolar').AsString);

        edEndereco.Text      := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_endereco').AsString);
        edBairro.Text        := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('nom_bairro').AsString);
        edUnidade.Text       := GetValue(dmDeca.cdsSel_Cadastro_Move.FieldByName('vUnidade').AsString);

        lstEscolas.Clear;

        mskDataSolicitacao.Text := DateToStr(Date());
        edUsuario.Text          := vvNOM_USUARIO + ' | ' + vvNOM_PERFIL;

        //Pesquisa no Hist�rico_Escola para recuperar a Escola e S�rie atuais
        with dmDeca.cdsSel_HistoricoEscola do
        begin

          Close;
          Params.ParamByName ('@pe_cod_matricula').Value  := mskMatricula.Text;
          Params.ParamByName ('@pe_cod_escola').Value     := Null;
          Params.ParamByName ('@pe_num_ano_letivo').Value := Copy(DateToStr(Date()),7,4);
          Open;

          if (RecordCount > 0) then
          begin
            edEscolaAtual.Text := FieldByName ('nom_escola').Value;
            edSerieAtual.Text  := FieldByName ('dsc_serie').Value;
          end
          else
          begin
            edEscolaAtual.Text := 'INFORMA��O N�O ENCONTRADA';
            edSerieAtual.Text  := 'INFORMA��O N�O ENCONTRADA';
          end;
          
        end;
      end;

      if (Copy(mskMatricula.Text,1,3) <> '006') then
      begin
        cbTipoSolicitacaoAtestado.ItemIndex := 1;
        cbTipoSolicitacaoAtestado.Enabled   := False;
        meJustificativa.Text := 'SOLICITA��O DE VAGA ESCOLAR';
      end
      else
      begin
        cbTipoSolicitacaoAtestado.Enabled   := True;
      end;


    except end;
  end;

end;

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.btnGerarSolicitacaoClick(
  Sender: TObject);
begin

  //Validar o preenchimento dos Campos Obrigat�rios
  if (Length(mskMatricula.Text) < 8) or (cbTipoSolicitacaoAtestado.ItemIndex < 0) or (Length(meJustificativa.Text)=0) then
  begin

    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'OS campos MATR�CULA, TIPO SOLICITA��O/VAGA e JUSTIFICATIVA s�o ' + #13+#10 +
                            'de preenchimento obrigat�rio. Favor verificar.',
                            '[Sistema Deca] - Erro de Preenchimento',
                            MB_OK + MB_ICONWARNING);
    mskMatricula.SetFocus;
  end
  else
  begin

    //Grava os dados no Hist�rico de acordo com o tipo de solicita�ao
    // primeiro valida o Status da crian�a - se ela pode sofre este movimento
    if StatusCadastro(mskMatricula.Text) <> 'Desligado' then
    begin
      if Application.MessageBox ('Deseja salvar informa��es no Hist�rico ?',
                                 '[Sistema Deca] - Emiss�o Solicita��o/Atestado',
                                 MB_YESNO + MB_ICONQUESTION) = id_yes then
      begin


        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := mskMatricula.Text;

          if (vvIND_PERFIL in [1,18,19,16,5,20,23,24,25]) then
            Params.ParamByName('@pe_cod_unidade').Value:= 1 //vvCOD_UNIDADE;  //Validar unidade para adm e acomp escolar, coordenador, equipe-multi
          else
            Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;  //Unidade Logada....

          Params.ParamByName('@pe_dat_historico').Value := GetValue(mskDataSolicitacao, vtDate);

          case cbTipoSolicitacaoAtestado.ItemIndex of
            0: begin //Transfer�ncia de Per�odo Escolar
                 Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 20;
                 Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Solicita��o de Transfer�ncia de Per�odo Escolar/Emiss�o de Atestado de Aprendiz (Conv�nio)';
               end;

            1: begin //Atestado/Conv�nio
                 Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 21;
                 Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Solicita��o de Vaga Escolar';
               end;
            
          end; //Case...

          Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(meJustificativa.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

          //Dados gravados, gerar o relat�rio de acordo com o tipo solicita��o/atestado...
          case cbTipoSolicitacaoAtestado.ItemIndex of

            //Solicita��o de Transfer�ncia de Per�odo Escolar / Atestado
            0 : begin
                  Application.CreateForm (TrelSolicitacaoTransferenciaPeriodoEscolar, relSolicitacaoTransferenciaPeriodoEscolar);
                  relSolicitacaoTransferenciaPeriodoEscolar.lbNomeAdolescente.Caption  := edNome.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbNomeAdolescente2.Caption := edNome.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbNumRA2.Caption           := mskNumRgEscolar.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbDataNascimento.Caption   := mskNascimento.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbNumRA.Caption            := mskNumRgEscolar.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbEndereco.Caption         := edEndereco.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbBairro.Caption           := edBairro.Text;
                  relSolicitacaoTransferenciaPeriodoEscolar.lbUnidade.Caption          := edUnidade.Text;

                  //Consulta empresa ...
                  with dmDeca.cdsSel_Secao do
                  begin
                    Close;
                    Params.ParamByName ('@pe_cod_id_secao').Value := Null;
                    if (vvIND_PERFIL in [1,18,19,16,5,20,23,24,25]) then
                      Params.ParamByName ('@pe_cod_unidade').Value  := 1
                    else
                      Params.ParamByName ('@pe_cod_unidade').Value  := vvCOD_UNIDADE;

                    //Somente Unidades do Aprendiz
                    Params.ParamByName ('@pe_num_secao').Value    := dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_secao').Value;
                    Params.ParamByName ('@pe_flg_status').Value   := Null;
                    Open;

                   relSolicitacaoTransferenciaPeriodoEscolar.lbUnidade2.Caption        := dmDeca.cdsSel_Secao.FieldByName ('nom_unidade').Value; //edUnidade.Text;
                   relSolicitacaoTransferenciaPeriodoEscolar.lbEmpresa.Caption         := dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
                  end;
                  relSolicitacaoTransferenciaPeriodoEscolar.PrinterSettings.Copies     := 3;
                  relSolicitacaoTransferenciaPeriodoEscolar.Preview;
                  relSolicitacaoTransferenciaPeriodoEscolar.Free;
                end;

            //Solicita��o de Vaga Escolar
            1 : begin
                  Application.CreateForm (TrelSolicitacaoVagaEscolar, relSolicitacaoVagaEscolar);
                  relSolicitacaoVagaEscolar.lbNome.Caption            := edNome.Text;
                  relSolicitacaoVagaEscolar.lbNome2.Caption           := edNome.Text;
                  relSolicitacaoVagaEscolar.lbNome3.Caption           := edNome.Text;

                  relSolicitacaoVagaEscolar.lbUnidade.Caption         := edUnidade.Text;
                  relSolicitacaoVagaEscolar.lbUnidade2.Caption        := edUnidade.Text;
                  relSolicitacaoVagaEscolar.lbUnidade3.Caption        := edUnidade.Text;

                  relSolicitacaoVagaEscolar.lbNascimento.Caption      := mskNascimento.Text;
                  relSolicitacaoVagaEscolar.lbNascimento2.Caption     := mskNascimento.Text;
                  relSolicitacaoVagaEscolar.lbNascimento3.Caption     := mskNascimento.Text;

                  relSolicitacaoVagaEscolar.lbRA.Caption              := mskNumRgEscolar.Text;
                  relSolicitacaoVagaEscolar.lbRA2.Caption             := mskNumRgEscolar.Text;
                  relSolicitacaoVagaEscolar.lbRA3.Caption             := mskNumRgEscolar.Text;

                  relSolicitacaoVagaEscolar.lbEndereco.Caption        := edEndereco.Text;
                  relSolicitacaoVagaEscolar.lbEndereco2.Caption       := edEndereco.Text;
                  relSolicitacaoVagaEscolar.lbEndereco3.Caption       := edEndereco.Text;

                  relSolicitacaoVagaEscolar.lbBairro.Caption          := edBairro.Text;
                  relSolicitacaoVagaEscolar.lbBairro2.Caption         := edBairro.Text;
                  relSolicitacaoVagaEscolar.lbBairro3.Caption         := edBairro.Text;

                  relSolicitacaoVagaEscolar.qrlNomeEscola.Caption     := lstEscolas.Items.Strings[0];
                  relSolicitacaoVagaEscolar.qrlNomeEscola2.Caption    := lstEscolas.Items.Strings[8];//cbEscola.Text;
                  relSolicitacaoVagaEscolar.qrlNomeEscola3.Caption    := lstEscolas.Items.Strings[16];//cbEscola.Text;

                  //Carrega os dados da escola selecionada...
                  relSolicitacaoVagaEscolar.qrlEnderecoEscola.Caption  := lstEscolas.Items.Strings[1];
                  relSolicitacaoVagaEscolar.qrlEnderecoEscola2.Caption := lstEscolas.Items.Strings[9];
                  relSolicitacaoVagaEscolar.qrlEnderecoEscola3.Caption := lstEscolas.Items.Strings[17];

                  relSolicitacaoVagaEscolar.qrlFoneEscola.Caption      := lstEscolas.Items.Strings[2];
                  relSolicitacaoVagaEscolar.qrlFoneEscola2.Caption     := lstEscolas.Items.Strings[10];
                  relSolicitacaoVagaEscolar.qrlFoneEscola3.Caption     := lstEscolas.Items.Strings[18];

                  relSolicitacaoVagaEscolar.qrlBairroEscola.Caption    := lstEscolas.Items.Strings[3];
                  relSolicitacaoVagaEscolar.qrlBairroEscola2.Caption   := lstEscolas.Items.Strings[11];
                  relSolicitacaoVagaEscolar.qrlBairroEscola3.Caption   := lstEscolas.Items.Strings[19];

                  relSolicitacaoVagaEscolar.qrlCepEscola.Caption       := lstEscolas.Items.Strings[4];
                  relSolicitacaoVagaEscolar.qrlCepEscola2.Caption      := lstEscolas.Items.Strings[12];
                  relSolicitacaoVagaEscolar.qrlCepEscola3.Caption      := lstEscolas.Items.Strings[20];

                  relSolicitacaoVagaEscolar.qrlDiretoraEscola.Caption  := lstEscolas.Items.Strings[5];
                  relSolicitacaoVagaEscolar.qrlDiretoraEscola2.Caption := lstEscolas.Items.Strings[13];
                  relSolicitacaoVagaEscolar.qrlDiretoraEscola3.Caption := lstEscolas.Items.Strings[21];

                  relSolicitacaoVagaEscolar.qrlNomeSerie.Caption      := lstEscolas.Items.Strings[6];
                  relSolicitacaoVagaEscolar.qrlNomeSerie2.Caption     := lstEscolas.Items.Strings[14];
                  relSolicitacaoVagaEscolar.qrlNomeSerie3.Caption     := lstEscolas.Items.Strings[22];

                  relSolicitacaoVagaEscolar.qrlIdentificaoSolicitante.Caption  := edUsuario.Text;
                  relSolicitacaoVagaEscolar.qrlIdentificaoSolicitante2.Caption := edUsuario.Text;
                  relSolicitacaoVagaEscolar.qrlIdentificaoSolicitante3.Caption := edUsuario.Text;

                  //relSolicitacaoVagaEscolar.PrinterSettings.Copies    := 3;
                  relSolicitacaoVagaEscolar.Preview;
                  relSolicitacaoVagaEscolar.Free;


                end;

            end;//case

            mmTT_ESCOLAS := 0;

        end; //Inc Cadastro historico

      end;

      end
    else
    begin
      Application.MessageBox ('Opera��o inv�lida!'+#13+#10+#13+#10+
                              'Esta(e) crian�a/adolescente encontra-se DESLIGADO ',
                              '[Sistema Deca] - Situa��o Incompat�vel',
                              MB_OK + MB_ICONWARNING);
    end;

  end;


end;

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.cbTipoSolicitacaoAtestadoClick(
  Sender: TObject);
begin

  case cbTipoSolicitacaoAtestado.ItemIndex of
    0: meJustificativa.Text := 'SOLICITA��O DE TRANSFER�NCIA DE PER�ODO ESCOLAR / ATESTADO-CONV�NIO';
    1: meJustificativa.Text := 'SOLICITA��O DE VAGA ESCOLAR';
  end;

end;

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.FormShow(
  Sender: TObject);
begin

  vListaEscolaS1 := TStringList.Create();
  vListaEscolaS2 := TStringList.Create();

  try
    //Carregar a lista de Escolas...
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null;
      Open;

      while not dmDeca.cdsSel_Escola.eof do
      begin
        cbEscola.Items.Add (dmDeca.cdsSel_Escola.FieldByName('nom_escola').Value);
        vListaEscolaS1.Add (IntToStr(dmDeca.cdsSel_Escola.FieldByName('cod_escola').Value));
        vListaEscolaS2.Add (dmDeca.cdsSel_Escola.FieldByName('nom_escola').Value);
        dmDeca.cdsSel_Escola.Next;
      end;

    end;
  except end;

end;

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.cbEscolaClick(
  Sender: TObject);
var vvCOD_ESCOLA_S : Integer;

begin

  vListaSerieS1 := TStringList.Create();
  vListaSerieS2 := TStringList.Create();

  vvCOD_ESCOLA_S := StrToInt(vListaEscolaS1.Strings[cbEscola.ItemIndex]);

  //Consulta os dados da escola selecionada...
  with dmDeca.cdsSel_Escola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_escola').Value := vvCOD_ESCOLA_S;
    Params.ParamByName ('@pe_nom_escola').Value := Null;
    Open;

    if mmTT_ESCOLAS = 3 then
    begin
      Application.MessageBox ('J� foram inclu�das as tr�s escolas necess�rias para a solicita��o de vaga escolar.',
                              '[Sistema Deca] - Solicita��o de Vaga Escolar...',
                              MB_OK + MB_ICONINFORMATION);
      cbEscola.Enabled := False;
      mmTT_ESCOLAS := 0;
    end
    else
    begin
      //lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
      //lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').Value);
      //lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('num_telefone').Value);
      //lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').Value);
      //lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('num_cep').Value);
      //lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').Value);
      //lstEscolas.Items.Add ('');
      //mmTT_ESCOLAS := mmTT_ESCOLAS + 1;
    end;

  end;


  cbSerie.Clear;
  //Consulta as s�ries escolares j� lan�adas para a escola e atualiza cbSerieE
  try
    with dmDeca.cdsSel_EscolaSerie do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value          := vvCOD_ESCOLA_S;
      Params.ParamByName ('@pe_cod_id_serie').Value        := Null;
      Open;

      if (dmDeca.cdsSel_EscolaSerie.RecordCount > 0) then
      begin
        while not dmDeca.cdsSel_EscolaSerie.eof do
        begin
          vListaSerieS1.Add (IntToStr(FieldByName ('cod_id_serie').Value));
          vListaSerieS2.Add (FieldByName ('dsc_Serie').Value);
          cbSerie.Items.Add (FieldByName ('dsc_Serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end
      else
      begin
        mmNOM_ESCOLA := cbEscola.Text;
        Application.MessageBox ('Aten��o !!!' +#13+#10 +
                                'N�o existem s�ries cadastradas para a Escola selecionada' + #13+#10 +
                                'Favor verificar cadastro de S�ries por Escola.',
                                '[Sistema Deca] - Acompanhamento Escolar',
                                MB_OK + MB_ICONHAND);
        if Application.MessageBox ('Deseja ser direcionado para a tela de cadastro ' + #13+#10 +
                                   'de s�ries por escola agora?',
                                   '[Sistema Deca] - Direcionando para...',
                                   MB_YESNO + MB_ICONQUESTION) = id_yes then
        begin
          Application.CreateForm (TfrmCadastroSeriesPorEscola, frmCadastroSeriesPorEscola);
          frmCadastroSeriesPorEscola.ShowModal;
        end;

      end;
    end;

  except end;
end;

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.btnLimparListaEscolasClick(
  Sender: TObject);
begin
  lstEscolas.Clear;
  cbEscola.Enabled := True;
end;

procedure TfrmEmissaoDeclaracoesSolicitacoesEscolares.cbSerieClick(
  Sender: TObject);
begin
  lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
  lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_endereco').Value);
  lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('num_telefone').Value);
  lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_bairro').Value);
  lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('num_cep').Value);
  lstEscolas.Items.Add (dmDeca.cdsSel_Escola.FieldByName ('nom_diretor').Value);
  lstEscolas.Items.Add (cbSerie.Text);
  lstEscolas.Items.Add ('');
  mmTT_ESCOLAS := mmTT_ESCOLAS + 1;
end;

end.
