unit uDadosSuspensoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask;

type
  TfrmDadosSuspensao = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    txtDataInicial: TMaskEdit;
    txtDataFinal: TMaskEdit;
    btnVer: TBitBtn;
    btnCancelar: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure txtDataInicialExit(Sender: TObject);
    procedure txtDataFinalExit(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDadosSuspensao: TfrmDadosSuspensao;

implementation

uses uDM, rDesligados, uPrincipal, rSuspensos;

{$R *.DFM}

procedure TfrmDadosSuspensao.FormActivate(Sender: TObject);
begin
  btnVer.Enabled := False;
  btnCancelar.Enabled := True;

  txtDataInicial.SetFocus;

  txtDataInicial.Enabled := True;
  txtDataFinal.Enabled := True;
end;

procedure TfrmDadosSuspensao.txtDataInicialExit(Sender: TObject);
begin
  try
    StrToDate(txtDataInicial.Text);
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataInicial.Clear;
      txtDataInicial.SetFocus;
    end;
  end;
end;

procedure TfrmDadosSuspensao.txtDataFinalExit(Sender: TObject);
begin
  try
    StrToDate(txtDataFinal.Text);

    if StrToDate(txtDataFinal.Text) < StrToDate(txtDataInicial.Text) then
        begin
          ShowMessage('Data final do periodo � menor do que a data inicial.' + #13#10#13 +
                      'Favor informe data v�lida para realizar a consulta...');
          txtDataFinal.SetFocus;
        end
    else
      begin
        btnVer.Enabled := True;
        btnVer.SetFocus;
      end;
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataFinal.Clear;
      txtDataFinal.SetFocus;
    end;
  end;
end;

procedure TfrmDadosSuspensao.btnVerClick(Sender: TObject);
begin
  //with dmDeca.cdsSel_Desligados do
  with dmDeca.cdsSel_Suspensos do
  begin
    Close;
    if (vvIND_PERFIL = 18) or (vvIND_PERFIL = 20) or (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then  //Perfil Servi�o Socail/Acompanhamento Escolar
      //Params.ParamByName('@pe_cod_unidade').Value := 0
      Params.ParamByName('@pe_cod_unidade').Value := null
    else
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
    Params.ParamByName('@pe_ind_tipo_historico').Value := 4;  //ind_tipo_historico para Suspens�o
    Params.ParamByName('@pe_data_inicial').Value := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_data_final').Value := StrToDate(txtDataFinal.Text);
    Open;

    Application.CreateForm(TrelSuspensos, relSuspensos);
    relSuspensos.lbPeriodo.Caption := 'DE ' + txtDataInicial.Text + ' AT� ' + txtDataFinal.Text;
    relSuspensos.lbUnidade.Caption := vvNOM_UNIDADE;
    relSuspensos.Preview;
    relSuspensos.Free;

  end;
end;

procedure TfrmDadosSuspensao.btnCancelarClick(Sender: TObject);
begin
  btnVer.Enabled := False;
  txtDataInicial.Clear;
  txtDataFinal.Clear;
  txtDataInicial.SetFocus;
end;

procedure TfrmDadosSuspensao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmDadosSuspensao.Close;
  end;
end;

end.
