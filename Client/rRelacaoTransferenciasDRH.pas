unit rRelacaoTransferenciasDRH;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelRelacaoTransferenciasDRH = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    lbTitulo: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    DetailBand1: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRGroup1: TQRGroup;
    QRLabel14: TQRLabel;
    QRDBText6: TQRDBText;
    QRGroup2: TQRGroup;
    QRLabel15: TQRLabel;
    QRDBText7: TQRDBText;
    QRGroup3: TQRGroup;
    QRLabel16: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    PageFooterBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRExpr5: TQRExpr;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
  private

  public

  end;

var
  relRelacaoTransferenciasDRH: TrelRelacaoTransferenciasDRH;

implementation

uses uDM;

{$R *.DFM}

end.
