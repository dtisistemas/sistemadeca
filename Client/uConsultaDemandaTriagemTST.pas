unit uConsultaDemandaTriagemTST;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, Grids, DBGrids, ComObj;

type
  TfrmConsultaDemandaTriagemTST = class(TForm)
    DBGrid1: TDBGrid;
    dsSP_SEL_RELATORIO_DEMANDA_TRIAGEM2017: TDataSource;
    Button1: TButton;
    SaveDialog1: TSaveDialog;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaDemandaTriagemTST: TfrmConsultaDemandaTriagemTST;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmConsultaDemandaTriagemTST.Button1Click(Sender: TObject);
  var
  Excel : variant;
  ContCampos, ContCampos1, Linha : Integer;
begin
  try
    with dmdeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017 do
    begin
      Close;
      Open;
      DBGrid1.Refresh;
    end;
  except end;

  try
    if SaveDialog1.Execute then
    begin
      Excel := CreateOleObject('Excel.Application');
      Excel.WorkBooks.add(1);
      Excel.WorkBooks[1].SaveAs(SaveDialog1.FileName);;

      for ContCampos := 1 to dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.FieldCount -1 do
      begin
        Excel.Cells[1, ContCampos] := dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.Fields[ContCampos].DisplayName;
      end;

      Linha := 2;

      dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.First;
      while not (dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.eof) do
      begin
        for ContCampos1 := 1 to dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.FieldCount - 1 do
        begin
          Excel.Cells[Linha, ContCampos1] := dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.Fields[ContCampos1].AsString;
        end;
        Inc(Linha);
        dmDeca.cdsSel_SP_SEL_RELATORIO_DEMANDA_TRIAGEM2017.Next;
      end;
      Excel.Columns.AutoFit;
      Excel.WorkBooks[1].Save;
      Excel.WorkBooks[1].Close;

      Application.MessageBox ('Aten��o !!!' +#13+#10+
                              'A planilha com seus dados foi gerada na pasta selecionada.' +#13+#10+
                              'Acesse sua pasta para ter acesso a planilha e consultar os dados.',
                              '[Sistema Deca] - Exporta��o de dados para planilha',
                              MB_OK + MB_ICONINFORMATION);

    end;
  except end;









end;

end.
