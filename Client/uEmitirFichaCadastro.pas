unit uEmitirFichaCadastro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, ComCtrls, Mask, jpeg, ShellApi;

type
  TfrmEmitirFichaCadastro = class(TForm)
    rgCriterio: TRadioGroup;
    GroupBox1: TGroupBox;
    btnConsultar: TSpeedButton;
    cbUnidade: TComboBox;
    Label1: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    Label2: TLabel;
    mskMatricula1: TMaskEdit;
    Label3: TLabel;
    mskMatricula2: TMaskEdit;
    Label4: TLabel;
    mskAnoLetivo: TMaskEdit;
    Label5: TLabel;
    cbBimestre: TComboBox;
    procedure rgCriterioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmitirFichaCadastro: TfrmEmitirFichaCadastro;
  vListaUnidadeE1,vListaUnidadeE2 : TStringList;

  vv_JPG : TJpegImage;
  b : TStream;

  //Vari�veis a serem utilizadas na atribui��o de valores para serem inseridos na tabela TMP_FICHA_CADASTRO
  mmCOD_MATRICULA, mmNOM_NOME, mmSEXO,mmENDERECO, mmCOMPLEMENTO, mmBAIRRO, mmCEP, mmCPF, mmRG, mmCERTIDAO, mmCTPS, mmRG_ESCOLAR, mmNUM_NIS,
  mmNUM_CARTAOSIAS, mmPERIODO_FUNDHAS, mmUNIDADE, mmESCOLA, mmSERIE, mmPERIODO_ESCOLA, mmPAI, mmMAE, mmRESPONSAVEL,
  mmRG_PAI, mmRG_MAE, mmRG_RESPONSAVEL, mmCPF_PAI, mmCPF_MAE, mmCPF_RESPONSAVEL : String;

  mmFOTO : TImage;

  mmCOTAS_PASSE : Integer;

  mmDAT_ADMISSAO, mmDAT_NASCIMENTO : TDateTime;

implementation

uses uDM, uPrincipal, uUtil, uFichaCadastroNova;

{$R *.DFM}

procedure TfrmEmitirFichaCadastro.rgCriterioClick(Sender: TObject);
begin
  case rgCriterio.ItemIndex of
    0: begin //Unidade
      cbUnidade.Visible     := True;

      label1.Visible        := False;
      txtMatricula.Visible  := False;
      btnPesquisar.Visible  := False;
      lbNome.Visible        := False;

      Label2.Visible        := False;
      mskMatricula1.Visible := False;
      Label3.Visible        := False;
      mskMatricula2.Visible := False;
    end;

    1: begin
      cbUnidade.Visible     := False;

      label1.Visible        := True;
      txtMatricula.Visible  := True;
      btnPesquisar.Visible  := True;
      lbNome.Visible        := True;

      Label2.Visible        := False;
      mskMatricula1.Visible := False;
      Label3.Visible        := False;
      mskMatricula2.Visible := False;
    end;

    2: begin
      cbUnidade.Visible     := False;

      label1.Visible        := False;
      txtMatricula.Visible  := False;
      btnPesquisar.Visible  := False;
      lbNome.Visible        := False;

      Label2.Visible        := True;
      mskMatricula1.Visible := True;
      Label3.Visible        := True;
      mskMatricula2.Visible := True;
    end;
  end;
end;

procedure TfrmEmitirFichaCadastro.FormShow(Sender: TObject);
begin
  cbUnidade.Visible     := False;

  label1.Visible        := True;
  txtMatricula.Visible  := True;
  btnPesquisar.Visible  := True;
  lbNome.Visible        := True;

  Label2.Visible        := False;
  mskMatricula1.Visible := False;
  Label3.Visible        := False;
  mskMatricula2.Visible := False;

  //Carregar a lista de unidades no combo
  vListaUnidadeE1 := TStringList.Create();
  vListaUnidadeE2 := TStringList.Create();
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        if dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0 then
        begin
          vListaUnidadeE1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidadeE2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          //dmDeca.cdsSel_Unidade.Next;
        end
        else
        begin

        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except
    begin
      Application.MessageBox ('Aten��o !!! Um erro ocorreu ao tentar consultar os dados de unidades.' + #13+#10 +
                              'Verifique a sua conex�o e se o erro persistir entre em contato com o Administrador.',
                              '[Sistema Deca] - Erro consultando unidades',
                              MB_OK + MB_ICONERROR);
    end;
  end;

end;

procedure TfrmEmitirFichaCadastro.btnConsultarClick(Sender: TObject);
begin
  //Primeiro faz-se a consulta, repassa para as vari�veis e depois grava-se em tabela tempor�ria
  try
    //Faz uma consulta ao cadasto de acordo com o crit�rio
    case rgCriterio.ItemIndex of
      //Unidade
      0: begin
           with dmDeca.cdsSel_DadosFichaCadastro do
           begin
             Close;
             Params.ParamByName ('@pe_cod_matricula').Value  := Null;
             Params.ParamByName ('@pe_cod_matricula1').Value := Null;
             Params.ParamByName ('@pe_cod_matricula2').Value := Null;
             Params.ParamByName ('@pe_cod_unidade').Value    := StrToInt(vListaUnidadeE1.Strings[cbUnidade.ItemIndex]);
             Open;
           end;
      end;

      //Aluno
      1: begin
           with dmDeca.cdsSel_DadosFichaCadastro do
           begin
             Close;
             Params.ParamByName ('@pe_cod_matricula').Value  := Trim(txtMatricula.Text);
             Params.ParamByName ('@pe_cod_matricula1').Value := Null;
             Params.ParamByName ('@pe_cod_matricula2').Value := Null;
             Params.ParamByName ('@pe_cod_unidade').Value    := Null;
             Open;
           end;
      end;

      //Intervalo de matr�culas
      2: begin
           with dmDeca.cdsSel_DadosFichaCadastro do
           begin
             Close;
             Params.ParamByName ('@pe_cod_matricula').Value  := Null;
             Params.ParamByName ('@pe_cod_matricula1').Value := mskMatricula1.Text;
             Params.ParamByName ('@pe_cod_matricula2').Value := mskMatricula2.Text;
             Params.ParamByName ('@pe_cod_unidade').Value    := Null;
             Open;
           end;
      end;
    end;

    
    //Se n�o encontrou registro(s), avisa ao usu�rio
    if (dmDeca.cdsSel_DadosFichaCadastro.RecordCount < 1) then
    begin
      Application.MessageBox ('Aten��o !!!' + #13+#10 +
                              'De acordo com os crit�rios selecionados, n�o foi(ram) localizado(s)' + #13+#10 +
                              'registros. Refa�a sua pesquisa alterando o crit�rio.',
                              '[Sistema Deca] - Dados Ficha Cadastro',
                              MB_OK + MB_ICONWARNING);
    end
    else
    begin
      //Excluir todos os registros encontrados em TMP_FICHA_CADASTRO PARA O USU�RIO LOGADO
      with dmDeca.cdsExc_TMPFichaCadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;
      end;

      //Se encontrou registro(s), repassa para as primeiras vari�veis
      while not (dmDeca.cdsSel_DadosFichaCadastro.eof) do
      begin
        mmCOD_MATRICULA   := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('cod_matricula').AsString);
        mmNOM_NOME        := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nom_nome').AsString);
        mmSEXO            := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('ind_sexo').AsString);
        mmDAT_ADMISSAO    := StrToDate(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('admissao').Value);
        mmDAT_NASCIMENTO  := StrToDate(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nascimento').Value);
        mmENDERECO        := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nom_endereco').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nom_complemento').IsNull then
          mmCOMPLEMENTO := '-'
        else mmCOMPLEMENTO := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nom_complemento').AsString);

        mmBAIRRO := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nom_bairro').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cep').IsNull then
          mmCEP := '00.000-000'
        else mmCEP := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cep').AsString);


        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cpf').IsNull then
          mmCPF := '000.000.000-00'
        else mmCPF := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cpf').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_rg').IsNull then
          mmRG := '00.000.000-0'
        else mmRG := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_rg').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_certidao').IsNull then
          mmCERTIDAO := '<Sem Informa��es>'
        else mmCERTIDAO := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_certidao').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('ctps_num').IsNull then
          mmCTPS := '00000'
        else mmCTPS := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('ctps_num').AsString);

        mmRG_ESCOLAR      := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('dsc_rg_escolar').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_nis').IsNull then
          mmNUM_NIS := '00000'
        else mmNUM_NIS := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_nis').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cartaosias').IsNull then
          mmNUM_CARTAOSIAS := '00000'
        else mmNUM_CARTAOSIAS  := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cartaosias').AsString);

        mmPERIODO_FUNDHAS := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('dsc_periodo').AsString);

        if dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cota_passes').IsNull then
          mmCOTAS_PASSE := 0
        else mmCOTAS_PASSE := dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('num_cota_passes').Value;
        
        mmUNIDADE         := GetValue(dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('nom_unidade').AsString);

        //Consulta dados de escola
        with dmDeca.cdsSel_Acompescolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
          Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_num_ano_letivo').Value        := StrToInt(mskAnoLetivo.Text);
          Params.ParamByName ('@pe_num_bimestre').Value          := cbBimestre.ItemIndex+1;
          Params.ParamByName ('@pe_cod_unidade').Value           := Null;
          Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
          Open;

          if (dmDeca.cdsSel_Acompescolar.RecordCount < 1) then
          begin
            mmESCOLA         := '<Sem escola lan�ada/encontrado no bimestre/ano selecionado>';
            mmSERIE          := '<Sem s�rie lan�ada/encontrada no bimestre/ano selecionado>';
            mmPERIODO_ESCOLA := '<Sem registro de per�odo informado>';
          end
          else
          begin
            mmESCOLA         := dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_escola').Value;
            mmSERIE          := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
            mmPERIODO_ESCOLA := dmDeca.cdsSel_AcompEscolar.FieldByName ('vPeriodoEscolar').Value;
          end;
        end;

        //Consulta dados de Pai e M�e
        with dmDeca.cdsSel_Cadastro_Familia do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_DadosFichaCadastro.FieldByName ('cod_matricula').Value;
          Params.ParamByName ('@pe_cod_familiar').Value  := Null;
          Open;

          while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
          begin
            if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'PAI') then
            begin
              mmPAI     := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
              if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').IsNull) then
                mmRG_PAI  := '00.000.000-0'
              else
                mmRG_PAI  := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').Value;

              if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').IsNull) then
                mmCPF_PAI := '000.000.000-00'
              else
                mmCPF_PAI := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value;
            end;

            if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'M�E') then
            begin
              mmMAE     := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
              if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').IsNull) then
                mmRG_MAE  := '00.000.000-0'
              else
                mmRG_MAE  := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').Value;

              if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').IsNull) then
                mmCPF_MAE := '000.000.000-00'
              else
                mmCPF_MAE := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value;
            end;

            if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_responsavel').AsBoolean = True) then
            begin
              mmRESPONSAVEL     := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
              if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').IsNull) then
                mmRG_RESPONSAVEL := '00.000.000-0'
              else
                mmRG_RESPONSAVEL := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_rg_fam').Value;

              if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').IsNull) then
                mmCPF_RESPONSAVEL := '000.000.000-00'
              else
                mmCPF_RESPONSAVEL := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('num_cpf_fam').Value;
            end;
            dmDeca.cdsSel_Cadastro_Familia.Next;
          end;
        end;

        //Veridfica se h� imagem cadastrada...
        



        //Repassa as vari�veis para a procedure fazer a inser��o na tabela e gerar a ficha de cadastro...
        with dmDeca.cdsIns_TMPFichaCadastro do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value       := mmCOD_MATRICULA;
          Params.ParamByName ('@pe_nom_nome').Value            := mmNOM_NOME;
          Params.ParamByName ('@pe_dat_admissao').Value        := mmDAT_ADMISSAO;
          Params.ParamByName ('@pe_dat_nascimento').Value      := mmDAT_NASCIMENTO;
          Params.ParamByName ('@pe_nom_unidade').Value         := mmUNIDADE;
          Params.ParamByName ('@pe_ind_sexo').Value            := mmSEXO;
          Params.ParamByName ('@pe_img_foto').Value            := Null;
          Params.ParamByName ('@pe_nom_endereco').Value        := mmENDERECO;
          Params.ParamByName ('@pe_nom_complemento').Value     := mmCOMPLEMENTO;
          Params.ParamByName ('@pe_nom_bairro').Value          := mmBAIRRO;
          Params.ParamByName ('@pe_num_cep').Value             := mmCEP;
          Params.ParamByName ('@pe_num_cotas').Value           := mmCOTAS_PASSE;
          Params.ParamByName ('@pe_nom_escola').Value          := mmESCOLA;
          Params.ParamByName ('@pe_nom_serie').Value           := mmSERIE;
          Params.ParamByName ('@pe_dsc_periodo_fundhas').Value := mmPERIODO_FUNDHAS;
          Params.ParamByName ('@pe_dsc_periodo_escola').Value  := mmPERIODO_ESCOLA;
          Params.ParamByName ('@pe_num_rg').Value              := mmRG;
          Params.ParamByName ('@pe_num_cpf').Value             := mmCPF;
          Params.ParamByName ('@pe_num_ctps').Value            := mmCTPS;
          Params.ParamByName ('@pe_num_cert_nasc').Value       := mmCERTIDAO;


          if (Length(mmPAI) > 0) then Params.ParamByName ('@pe_nom_pai').Value := mmPAI else mmPAI := '<N�o informado>';
          if (Length(mmMAE) > 0) then Params.ParamByName ('@pe_nom_mae').Value := mmMAE else mmMAE := '<N�o informado>';
          if (Length(mmRESPONSAVEL) > 0) then Params.ParamByName ('@pe_nom_mae').Value := mmRESPONSAVEL else mmMAE := '<N�o informado>';
          if (Length(mmRG_PAI) > 0) then Params.ParamByName ('@pe_num_rg_pai').Value := mmRG_PAI else mmRG_PAI := '<N�o informado>';
          if (Length(mmRG_MAE) > 0) then Params.ParamByName ('@pe_num_rg_mae').Value := mmRG_MAE else mmRG_MAE := '<N�o informado>';
          if (Length(mmRG_RESPONSAVEL) > 0) then Params.ParamByName ('@pe_num_rg_responsavel').Value := mmRG_RESPONSAVEL else mmRG_RESPONSAVEL := '<N�o informado>';
          if (Length(mmCPF_PAI) > 0) then Params.ParamByName ('@pe_num_cpf_pai').Value := mmCPF_PAI else mmCPF_PAI := '<N�o informado>';
          if (Length(mmCPF_MAE) > 0) then Params.ParamByName ('@pe_num_cpf_mae').Value := mmCPF_MAE else mmCPF_MAE := '<N�o informado>';
          if (Length(mmCPF_RESPONSAVEL) > 0) then Params.ParamByName ('@pe_num_cpf_responsavel').Value := mmCPF_RESPONSAVEL else mmCPF_RESPONSAVEL := '<N�o informado>';

          Params.ParamByName ('@pe_num_cartaosias').Value      := mmNUM_CARTAOSIAS;
          Params.ParamByName ('@pe_num_nis').Value             := mmNUM_NIS;
          Params.ParamByName ('@pe_dsc_rg_escolar').Value      := mmRG_ESCOLAR;
          Params.ParamByName ('@pe_cod_usuario').Value         := vvCOD_USUARIO;
          Execute;
        end;

        dmDeca.cdsSel_DadosFichaCadastro.Next;

      end;
    end; //case rgCriterio.....
  except end;


  if Application.MessageBox ('Deseja visualizar a(s) ficha(s) de cadastro(s)?',
                             '[Sistema Deca] - Emiss�o de Ficha de Cadastro',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    try
      with dmDeca.cdsSel_TMPFichaCadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO; 
        Open;
        Application.CreateForm(TrelFichaCadastroNova, relFichaCadastroNova);
        relFichaCadastroNova.Preview;
        relFichaCadastroNova.Free;
      end;
    except end;






  end;

end;

end.
