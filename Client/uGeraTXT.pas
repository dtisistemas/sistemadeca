unit uGeraTXT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, Grids, DBGrids, Mask, StdCtrls, ExtCtrls, Db, Gauges, ShellAPI;

type
  TfrmGeraTXT = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cbMes: TComboBox;
    Label2: TLabel;
    edAno: TEdit;
    Label3: TLabel;
    edDataPagamento: TMaskEdit;
    Label4: TLabel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    btnVer: TSpeedButton;
    btnGerarTXT: TSpeedButton;
    DataSource1: TDataSource;
    Label5: TLabel;
    Edit1: TEdit;
    Gauge1: TGauge;
    procedure FormShow(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnGerarTXTClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edAnoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraTXT: TfrmGeraTXT;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmGeraTXT.FormShow(Sender: TObject);
begin
  cbMes.ItemIndex := -1;
  edAno.Text := '2005';
  edDataPagamento.Clear;
  DBGrid1.DataSource := nil;
end;

procedure TfrmGeraTXT.btnVerClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Cadastro_Frequencia do
    begin

      Close;
      Params.ParamByName ('@pe_id_frequencia').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_num_ano').AsInteger := StrToInt(edAno.Text);
      Params.ParamByName ('@pe_num_mes').AsInteger := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
      Open;

      DataSource1.DataSet := dmDeca.cdsSel_Cadastro_Frequencia;
      DBGrid1.DataSource := DataSource1;

      //Aqui criar uma rotina para c�lculo do DSR para folha 006
      
    end
  except
    begin
      ShowMessage('Aten��o! Ocorreu um ERRO e os dados n�o foram selecionados...'+#13+#10+#13+#10+
                  'Contate o Administrador do Sistema e informe o erro ocorrido...');
      cbMes.ItemIndex := -1;
      edAno.Text := '';
      edDataPagamento.Clear;
      DBGrid1.DataSource := nil;
    end;
  end;

end;

procedure TfrmGeraTXT.btnGerarTXTClick(Sender: TObject);
var arqAdol, arqCri  : TextFile;
   //linha : String;
begin

  //N�o esquecer de que ao gerar o(s) arquivo(s), os registros identificados devem ser "flagados"
  //de maneira que n�o se possa mais realizar qualquer altera��o dos dados.


  Gauge1.MinValue := 0;
  Gauge1.MaxValue := dmDeca.cdsSel_Cadastro_Frequencia.RecordCount;

  AssignFile (arqAdol, ExtractFilePath(Application.ExeName) + 'adol.txt');
  ReWrite (arqAdol);

  AssignFile (arqCri, ExtractFilePath(Application.ExeName) + 'crian.txt');
  ReWrite (arqCri);

  if dmDeca.cdsSel_Cadastro_Frequencia.RecordCount < 1 then
  begin
    ShowMessage ('Nenhum dado selecionado para gerar arquivo... Favor conferir...');
    cbMes.ItemIndex := -1;
    edAno.Text := '2005';
    edDataPagamento.Clear;
    DBGrid1.DataSource := nil;
  end
  else
  begin
    while not (dmDeca.cdsSel_Cadastro_Frequencia.eof) do
    begin

      //Seleciona os eventos de acordo com v�nculo
      //002 - crian�as
      //003 - bolsistas
      //006 - celetistas
      //999 - n�o importar

      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').AsString,1,3) = '002') and
         (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger > 0) then
      begin
        Write (arqCri, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + '        ');
        Write (arqCri, Copy(edDataPagamento.Text,1,2) + Copy(edDataPagamento.Text,4,2) + Copy(edDataPagamento.Text,7,4));
        Write (arqCri, '1118');
        Write (arqCri, '000:00');

        if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 1) then
          Write (arqCri,'00000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  '.00')
        else if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 2) then
          Write (arqCri,'0000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  '.00');

        Write (arqCri, '000000000000.00');
        Write (arqCri, '000000000000.00');
        Write (arqCri, 'N');
        Writeln (arqCri);
      end;

      if (Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').AsString,1,3) = '003') and
         (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger > 0) then
      begin

        Write (arqAdol, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + '        ');
        Write (arqAdol, Copy(edDataPagamento.Text,1,2) + Copy(edDataPagamento.Text,4,2) + Copy(edDataPagamento.Text,7,4));
        Write (arqAdol, '1058');
        Write (arqAdol, '000:00');

        if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 1) then
          Write (arqAdol,'00000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  '.00')
        else if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 2) then
          Write (arqAdol,'0000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  '.00');

        Write (arqAdol, '000000000000.00');
        Write (arqAdol, '000000000000.00');
        Write (arqAdol, 'N');
        Writeln (arqAdol);

      end;

      if Copy(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('cod_matricula').AsString,1,3) = '006' then
      begin

        Write (arqAdol, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + '        ');
        Write (arqAdol, Copy(edDataPagamento.Text,1,2) + Copy(edDataPagamento.Text,4,2) + Copy(edDataPagamento.Text,7,4));
        Write (arqAdol, '1034');
        Write (arqAdol, '000:00');
        //Write (arq, Format('%12.2f',[IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)]));
        if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 1) then
          Write (arqAdol,'00000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  '.00')
        else if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger)) = 2) then
          Write (arqAdol,'0000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger) +  '.00');

        Write (arqAdol, '000000000000.00');
        Write (arqAdol, '000000000000.00');
        Write (arqAdol, 'N');
        Writeln (arqAdol);

        //DSR
        Write (arqAdol, dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('cod_matricula').AsString + '        ');
        Write (arqAdol, Copy(edDataPagamento.Text,1,2) + Copy(edDataPagamento.Text,4,2) + Copy(edDataPagamento.Text,7,4));
        Write (arqAdol, '1067');
        Write (arqAdol, '000:00');
                
        if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger)) = 1) then
          Write (arqAdol,'00000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger) +  '.00')
        else if (Length(IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger)) = 2) then
          Write (arqAdol,'0000000000' + IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger) +  '.00');

        Write (arqAdol, '000000000000.00');
        Write (arqAdol, '000000000000.00');
        Write (arqAdol, 'N');
        Writeln (arqAdol);

      end;

      dmDeca.cdsSel_Cadastro_Frequencia.Next;
      Gauge1.Progress := Gauge1.Progress + 1;

    end;

    CloseFile (arqCri);
    CloseFile (arqAdol);

    ShowMessage ('Dados exportados com sucesso...');
    Gauge1.Progress := 0;
    cbMes.ItemIndex := -1;
    edAno.Text := '0';
    edDataPagamento.Clear;
    DBGrid1.DataSource := nil;

    //Ap�s exportar os dados, alterar o flag dos dados para n�o permitir
    //futuras altera��es...





  end;
end;


procedure TfrmGeraTXT.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end;
end;

procedure TfrmGeraTXT.edAnoExit(Sender: TObject);
begin
  if (cbMes.ItemIndex >= 0) and (cbMes.ItemIndex <= 9) then
  begin
    edDataPagamento.Text := '01/0'+ IntToStr(cbMes.ItemIndex+1)+'/'+edAno.Text;
  end
  else
  if (cbMes.ItemIndex >= 10) and (cbMes.ItemIndex <= 12) then
  begin
    edDataPagamento.Text := '01/'+ IntToStr(cbMes.ItemIndex+1)+'/'+edAno.Text;
  end;
end;

end.
