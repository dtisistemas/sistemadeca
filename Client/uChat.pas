unit uChat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, ScktComp, ComCtrls;

type
  TfrmChat = class(TForm)
    lbUsuarios: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    meMensagens: TMemo;
    Panel3: TPanel;
    lbMensagemPara: TLabel;
    meMensagemPara: TMemo;
    btnSair: TSpeedButton;
    ClientSocket1: TClientSocket;
    ServerSocket1: TServerSocket;
    StatusBar1: TStatusBar;
    procedure FormActivate(Sender: TObject);
    procedure lbUsuariosClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure ClientSocket1Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    procedure ServerSocket1ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket1Accept(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ClientSocket1Error(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure ServerSocket1ClientDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure meMensagemParaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    protected
      IsServer : Boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmChat: TfrmChat;
  Server : String;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmChat.FormActivate(Sender: TObject);
begin
  frmChat.Top := 40;
  frmChat.Left := 0;

  StatusBar1.Panels[0].Text := 'Aguardando Conex�o...';

  try
    with dmDeca.cdsSel_Usuario do
    begin
      lbUsuarios.Items.Clear;
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := 0;
      Params.ParamByName ('@pe_flg_situacao').Value := 0;
      Params.ParamByName ('@pe_nom_usuario').Value := Null;
      Open;

      while not eof do
      begin
        lbUsuarios.Items.Add (FieldByName('nom_usuario').AsString); //+ '[' + FieldByName('Status').Value +']');
        Next;
      end
    end
  except end;

end;

procedure TfrmChat.lbUsuariosClick(Sender: TObject);
begin
  //meMensagens.Lines.Add (lbUsuarios.Items[lbUsuarios.ItemIndex]);
  lbMensagemPara.Caption := '';
  lbMensagemPara.Caption := 'Mensagem para ' + lbUsuarios.Items[lbUsuarios.ItemIndex];

  if ClientSocket1.Active then ClientSocket1.Active := False;
  
  if Length(Server) > 0 then
    with ClientSocket1 do
    begin
      Host := Server;
      Active := True;
    end;
end;

procedure TfrmChat.btnSairClick(Sender: TObject);
begin
  ServerSocket1.Close;
  ClientSocket1.Close;
  frmChat.Close;
end;

procedure TfrmChat.ClientSocket1Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  StatusBar1.Panels[0].Text := 'Conectado a ' + Socket.RemoteHost;
end;

procedure TfrmChat.ClientSocket1Read(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  meMensagens.Lines.Add (Socket.ReceiveText);
end;

procedure TfrmChat.ServerSocket1ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  meMensagens.Lines.Add (Socket.ReceiveText);
end;

procedure TfrmChat.ServerSocket1Accept(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  IsServer := True;
  StatusBar1.Panels[0].Text := 'Conectado a ' + Socket.RemoteAddress;
end;

procedure TfrmChat.ClientSocket1Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  meMensagens.Lines.Add ('ERRO NA CONEX�O COM ' + Server);
  ErrorCode := 0;
end;

procedure TfrmChat.ServerSocket1ClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  meMensagens.Lines.Add ('Usu�rio ' + Server + ' desconectado...'); 
end;

procedure TfrmChat.meMensagemParaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 Server := lbUsuarios.Items[lbUsuarios.ItemIndex];
 if Server <> '' then
 begin
   if Key = VK_RETURN then
   begin
     if Length(Server) > 0 then
     begin
       with ClientSocket1 do
       begin
         Host := Server;
         Active := True;
       end;

       if IsServer then
       begin
         ServerSocket1.Socket.Connections[0].SendText
           ('De ' + ServerSocket1.Socket.LocalHost + ': ' +
            'Para ' + Server + ': ' + meMensagemPara.Text);
         meMensagens.Lines.Add ('De ' + ServerSocket1.Socket.LocalHost + ': ' +
                                'Para ' + Server + ': ' + meMensagemPara.Text);
       end
       else
       begin
         ClientSocket1.Socket.SendText
           ('De ' + ServerSocket1.Socket.LocalHost + ': ' +
            'Para ' + Server + ': ' + meMensagemPara.Text);
         meMensagens.Lines.Add ('De ' + ServerSocket1.Socket.LocalHost + ': ' +
                                'Para ' + Server + ': ' + meMensagemPara.Text);
       end;

       meMensagemPara.Lines.Clear;
       meMensagemPara.SetFocus;

     end;
   end;
 end;
end;

end.
