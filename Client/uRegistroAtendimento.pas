unit uRegistroAtendimento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmRegistroAtendimento = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    txtOcorrencia: TMemo;
    txtData: TMaskEdit;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    Label5: TLabel;
    cbClassificacao: TComboBox;
    Label4: TLabel;
    cbTipo: TComboBox;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroAtendimento: TfrmRegistroAtendimento;
  vListaClassificacao1, vListaClassificacao2 : TStringList;

implementation

uses uAdvertencia, uDM, uFichaPesquisa, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmRegistroAtendimento.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmRegistroAtendimento.Close;
  end;
end;

procedure TfrmRegistroAtendimento.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end;
end;

procedure TfrmRegistroAtendimento.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmRegistroAtendimento.btnSairClick(Sender: TObject);
begin
  mmINTEGRACAO := False;
  frmRegistroAtendimento.Close;
end;

procedure TfrmRegistroAtendimento.AtualizaCamposBotoes(Modo: String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtOcorrencia.Clear;
    cbTipo.ItemIndex := 0;
    cbClassificacao.ItemIndex := 0;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtData.Enabled := False;
    txtOcorrencia.Enabled := False;
    cbTipo.Enabled := False;
    cbClassificacao.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtOcorrencia.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtOcorrencia.Enabled := True;

    if (vvIND_PERFIL <> 3) AND (vvIND_PERFIL <> 14) then
    begin
      cbTipo.ItemIndex := 0;
      cbTipo.Enabled := False;
      cbClassificacao.ItemIndex := 0;
      cbClassificacao.Enabled := False;
    end
    else
    begin
      cbTipo.ItemIndex := 0;
      cbTipo.Enabled := False;
      cbClassificacao.ItemIndex := 0;
      cbClassificacao.Enabled := True;
    end;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    txtMatricula.SetFocus;

  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
end;

procedure TfrmRegistroAtendimento.btnSalvarClick(Sender: TObject);
begin
if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofrer este movimento
      //if StatusCadastro(txtMatricula.Text) = 'Ativo' then

      //A crian�a com status de Afastado ou Suspenso podem ser feitas
      //anota��es no registro de atendimento
      if (StatusCadastro(txtMatricula.Text) = 'Ativo') OR (StatusCadastro(txtMatricula.Text) = 'Afastado') OR
          (StatusCadastro(txtMatricula.Text) = 'Suspenso') or (StatusCadastro(txtMatricula.Text) = 'Em Transfer�ncia') then

      begin

        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;

          if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 9) or
             (vvIND_PERFIL = 10) or (vvIND_PERFIL = 11) or (vvIND_PERFIL = 12) or (vvIND_PERFIL = 13) or (vvIND_PERFIL = 14) or (vvIND_PERFIL = 15) or
             (vvIND_PERFIL = 16) or (vvIND_PERFIL = 17) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19) or (vvIND_PERFIL = 20) or (vvIND_PERFIL = 23) or
             (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then
            Params.ParamByName ('@pe_cod_unidade').AsInteger := 1
          else
            Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;

          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 12;
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Registro de Atendimento';

          //Repassa valores dos par�metros quando for Assistente Social
          if (vvIND_PERFIL = 3) or (vvIND_PERFIL = 11) then
          begin
            Params.ParamByName('@pe_ind_tipo').Value          := cbTipo.ItemIndex;
            Params.ParamByName('@pe_ind_classificacao').Value := cbClassificacao.ItemIndex;
            Params.ParamByName('@pe_qtd_participantes').Value := Null;//StrToInt(txtQtdParticipantes.Text);
            Params.ParamByName('@pe_num_horas').Value         := Null;//StrToFloat(txtNumHoras.Text);
          end
          else
          begin
            Params.ParamByName('@pe_ind_tipo').Value := Null;
            Params.ParamByName('@pe_ind_classificacao').Value := Null;
            Params.ParamByName('@pe_qtd_participantes').Value := Null;
            Params.ParamByName('@pe_num_horas').Value := Null;
          end;

          Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;
        end;
        //AtualizaCamposBotoes('Salvar');
        //Gravar os dados dos INDICADORES se for perfil = 3 (Assistente Social)
        if (vvIND_PERFIL = 3) then
        begin
          try
          with dmDeca.cdsInc_Indicador do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_classificacao').Value   := StrToInt(vListaClassificacao1.Strings[cbClassificacao.ItemIndex]);
            Params.ParamByName('@pe_ind_tipo_classificacao').Value := cbTipo.ItemIndex;
            Params.ParamByName('@pe_dat_registro').Value           := GetValue(txtData, vtDate);
            Params.ParamByName('@pe_log_cod_usuario').Value        := vvCOD_USUARIO;
            Params.ParamByName('@pe_dsc_ocorrencia').Value         := Null;
            Execute;
          end;
        except end;
        end;
        AtualizaCamposBotoes('Padrao');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except
      begin
        //Gravar no arquivo de LOG de conex�o os dados registrados na tela
        arqLog := 'RA_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                          Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
        AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
        Rewrite (log_file);

        Write   (log_file, txtMatricula.Text);        //Grava a matricula
        Write   (log_file, ' ');
        Write   (log_file, lbNome.Caption);
        Write   (log_file, ' ');
        Write   (log_file, txtData.Text);             //Data do registro
        Write   (log_file, ' ');
        Write   (log_file, 'Registro de Atendimento-Inclus�o');//Descricao do Tipo de Historico
        Writeln (log_file, ' ');
        Writeln (log_file, Trim(txtOcorrencia.Text)); //Ocorrencia do Historico
        Writeln (log_file, '--------------------------------------------------------------------------------------');

        CloseFile (log_file);

        Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                               'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                               'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                               'Sistema DECA - Altera��o de Registro',
                               MB_OK + MB_ICONERROR);
    end;
    end;
    AtualizaCamposBotoes('Padrao');
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmRegistroAtendimento.btnPesquisarClick(Sender: TObject);
begin

  vListaClassificacao1 := TStringList.Create();
  vListaClassificacao2 := TStringList.Create();

  //Chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;

        //Se CRIAN�A, carrega o combo com as op��es de classifica��o de atendimento para crian�a
        if Divisao(txtMatricula.Text) = 'DIVISAO REGIONAL 2' then
        begin
          cbTipo.ItemIndex := 0;
          try
            with dmDeca.cdsSel_Classificacao_Atendimento do
            begin
              Close;
              Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
              Params.ParamByName('@pe_flg_crianca').Value := 1;
              Params.ParamByName('@pe_flg_adolescente').Value := Null;
              Params.ParamByName('@pe_flg_familia').Value := Null;
              Params.ParamByName('@pe_flg_profissional').Value := Null;
              Open;

              cbClassificacao.Clear;
              while not eof do
              begin
                vListaClassificacao1.Add(IntToStr(FieldByName('cod_id_classificacao').Value));
                vListaClassificacao2.Add(FieldByName('dsc_classificacao').Value);
                cbClassificacao.Items.Add(FieldByName('dsc_classificacao').Value);
                Next;
              end;
            end
          except end
        end
        //Se ADOLESCENTE, carrega o combo com as op��es de classifica��o de atendimento para adolescente
        else
        if (Divisao(txtMatricula.Text) = 'DIVISAO REGIONAL 2') or
           (Divisao(txtMatricula.Text) = 'DIVISAO DE EMPREGABILIDADE') then
        begin
           cbTipo.ItemIndex := 1;
          try
            with dmDeca.cdsSel_Classificacao_Atendimento do
            begin
              Close;
              Params.ParamByName('@pe_cod_id_classificacao').Value := Null;
              Params.ParamByName('@pe_flg_crianca').Value := Null;
              Params.ParamByName('@pe_flg_adolescente').Value := 1;
              Params.ParamByName('@pe_flg_familia').Value := Null;
              Params.ParamByName('@pe_flg_profissional').Value := Null;
              Open;

              cbClassificacao.Clear;
              while not eof do
              begin
                vListaClassificacao1.Add(IntToStr(FieldByName('cod_id_classificacao').Value));
                vListaClassificacao2.Add(FieldByName('dsc_classificacao').Value);
                cbClassificacao.Items.Add(FieldByName('dsc_classificacao').Value);
                Next;
              end;
            end
          except end
        end
      end;
    except end;
  end;
end;

procedure TfrmRegistroAtendimento.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmRegistroAtendimento.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmRegistroAtendimento.FormShow(Sender: TObject);
begin
   AtualizaCamposBotoes('Padrao');
   //Verifica se o perfil � de Assistente Social ou Servi�o Social (Plant�o)
   if (vvIND_PERFIL <> 3) and (vvIND_PERFIL <> 14) then
   begin
     Label4.Visible := False;
     cbTipo.Enabled := False;
     cbTipo.Visible := False;
     Label5.Visible := False;
     cbClassificacao.Visible := False;

     Label2.Top := 40;
     txtOcorrencia.Top := 55;
     txtOcorrencia.Height := 150;

   end
   else //if (vvIND_PERFIL = 3) OR (vvIND_PERFIL = 14) then
   begin
     Label4.Visible := True;
     cbTipo.Visible := True;
     cbTipo.Enabled := False;
     Label5.Visible := True;
     cbClassificacao.Visible := True;

     //Label2.Top := 90;
     Label2.Top := 65;
     //txtOcorrencia.Top := 110;
     txtOcorrencia.Top := 83;
     //txtOcorrencia.Height := 90;
     txtOcorrencia.Height := 120;
   end;

  if (mmINTEGRACAO = False) then
    AtualizaCamposBotoes('Padrao')
  else if (mmINTEGRACAO = True) then
  begin
    AtualizaCamposBotoes('Novo');
    txtMatricula.Text := dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value;
    lbNome.Caption    := dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value;
    txtData.Text := DateToStr(Date());
    txtOcorrencia.SetFocus;
  end;    
end;

procedure TfrmRegistroAtendimento.txtDataExit(Sender: TObject);
begin
// testa se a data � v�lida
  try
    StrToDate(txtData.Text);

    if StrToDate(txtData.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtData.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtData.SetFocus;
    end;
  end;
end;

procedure TfrmRegistroAtendimento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmRegistroAtendimento.btnImprimirClick(Sender: TObject);
begin
  ShowMessage ('Ainda n�o dispon�vel ...');
end;

end.
