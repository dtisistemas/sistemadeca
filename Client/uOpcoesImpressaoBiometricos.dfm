object frmOpcoesImpressaoBiometricos: TfrmOpcoesImpressaoBiometricos
  Left = 401
  Top = 338
  BorderStyle = bsDialog
  Caption = 'Op�oes de Impress�o - Avalia��o Biom�trica'
  ClientHeight = 170
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 170
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 43
      Height = 13
      Caption = 'Unidade:'
    end
    object cbUnidade: TComboBox
      Left = 65
      Top = 13
      Width = 229
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
    end
    object chkTodasUnidades: TCheckBox
      Left = 40
      Top = 54
      Width = 290
      Height = 17
      Caption = 'Visualizar crian�as/adolescentes de TODAS as unidades'
      Enabled = False
      TabOrder = 1
      OnClick = chkTodasUnidadesClick
    end
    object chkApenasFolhaRosto: TCheckBox
      Left = 41
      Top = 77
      Width = 280
      Height = 19
      Caption = 'Imprimir apenas folha de rosto para coleta de dados'
      TabOrder = 2
    end
    object btnVisualizar: TBitBtn
      Left = 112
      Top = 112
      Width = 110
      Height = 29
      Caption = 'Ver'
      Default = True
      TabOrder = 3
      OnClick = btnVisualizarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007700
        00000000007700770000770FFFFFFFFFF07000770000770FFFFFFF0000800777
        0000770FFFFFF087780877770000770FFFFF0877E88077770000770FFFFF0777
        787077770000770FFFFF07E7787077770000770FFFFF08EE788077770000770F
        FFFFF087780777770000770FFFFFFF00007777770000770FFFFFFFFFF0777777
        0000770FFFFFFF00007777770000770FFFFFFF07077777770000770FFFFFFF00
        7777777700007700000000077777777700007777777777777777777700007777
        77777777777777770000}
    end
  end
end
