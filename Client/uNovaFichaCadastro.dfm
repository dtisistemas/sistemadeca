object frmNovaFichaCadastro: TfrmNovaFichaCadastro
  Left = 365
  Top = 157
  Width = 1018
  Height = 754
  Caption = 'frmNovaFichaCadastro'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1010
    Height = 65
    Align = alTop
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 16
      Top = 8
      Width = 65
      Height = 49
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 65
    Width = 1010
    Height = 662
    Align = alClient
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 8
      Top = 8
      Width = 993
      Height = 644
      DataSource = ds_SelCadastro
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'cod_matricula'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_nome'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_prontuario'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_cartaosias'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Idade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dat_nascimento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_telefone'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dsc_periodo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ind_modalidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dat_admissao'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ind_status'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_cota_passes'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_cpf'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_rg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_certidao'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'num_secao'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dsc_rg_escolar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'vDefasagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'flg_defasagem_aprendizagem'
          Visible = True
        end>
    end
  end
  object ds_SelCadastro: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro
    Left = 952
    Top = 89
  end
end
