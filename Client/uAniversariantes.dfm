object frmAniversariantes: TfrmAniversariantes
  Left = 458
  Top = 372
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Aniversariantes do M�s]'
  ClientHeight = 66
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 335
    Height = 66
    Align = alClient
    Caption = 'Selecione o m�s'
    TabOrder = 0
    object btnVer: TSpeedButton
      Left = 270
      Top = 24
      Width = 23
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777770777707777777777007700777777777700770077777777770077007
        7777777770000007777777777000000777777777700000077777777070000007
        0777777070000007077777707000000707777777000000007777777777700777
        7777777777000077777777777700007777777777770000777777}
      OnClick = btnVerClick
    end
    object cbMeses: TComboBox
      Left = 16
      Top = 24
      Width = 249
      Height = 22
      Hint = 'Selecione o M�S para emiss�o da rela��o de Aniversariantes'
      Style = csOwnerDrawFixed
      ItemHeight = 16
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = cbMesesClick
      Items.Strings = (
        'Janeiro'
        'Fevereiro'
        'Mar�o'
        'Abril'
        'Maio'
        'Junho'
        'Julho'
        'Agosto'
        'Setembro'
        'Outubro'
        'Novembro'
        'Dezembro')
    end
  end
end
