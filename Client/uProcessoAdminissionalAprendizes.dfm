object frmProcessoAdminissionalAprendizes: TfrmProcessoAdminissionalAprendizes
  Left = 481
  Top = 160
  Width = 1046
  Height = 753
  Caption = '[Sistema Deca] - Processo Admissional de Adolescentes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 3
    Width = 1021
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 16
      Width = 28
      Height = 22
      Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
    end
  end
  object PageControl1: TPageControl
    Left = 5
    Top = 57
    Width = 1020
    Height = 595
    ActivePage = TabSheet2
    TabOrder = 1
    OnChange = PageControl1Change
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = '[Cadastros Lan�ados]'
      object btnEmitirContrato: TSpeedButton
        Left = 356
        Top = 512
        Width = 300
        Height = 52
        Caption = 'Emitir 2.� Via Contrato e Ficha Admissional'
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777700000007777777777777777700000007777777774F77777700000007777
          7777444F77777000000077777774444F777770000000700000444F44F7777000
          000070FFF444F0744F777000000070F8884FF0774F777000000070FFFFFFF077
          74F77000000070F88888F077774F7000000070FFFFFFF0777774F000000070F8
          8777F07777774000000070FFFF00007777777000000070F88707077777777000
          000070FFFF007777777770000000700000077777777770000000777777777777
          777770000000}
        OnClick = btnEmitirContratoClick
      end
      object btnNovaAdmissao: TSpeedButton
        Left = 860
        Top = 512
        Width = 300
        Height = 52
        Caption = 'Nova Admiss�o'
        Enabled = False
        Visible = False
        OnClick = btnNovaAdmissaoClick
      end
      object Label7: TLabel
        Left = 21
        Top = 7
        Width = 966
        Height = 16
        Alignment = taCenter
        Caption = 
          'FAVOR PREENCHER TODOS OS CAMPOS DAS TELAS PARA A EMISS�O CORRETA' +
          ' DO CONTRATO DE ADMISS�O DE APRENDIZAGEM.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbgAdmissoes: TDBGrid
        Left = 4
        Top = 33
        Width = 1003
        Height = 473
        DataSource = dsAdmissoes
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = dbgAdmissoesDblClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'COD_MATRICULA'
            Title.Alignment = taCenter
            Title.Caption = '[Matr�cula]'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOM_NOME'
            Title.Caption = '[Nome]'
            Width = 400
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ADMISSAO'
            Title.Alignment = taCenter
            Title.Caption = '[Data Admiss�o]'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TERMINO_CONTRATO'
            Title.Alignment = taCenter
            Title.Caption = '[T�rmino Contrato]'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CBO'
            Title.Alignment = taCenter
            Title.Caption = '[CBO]'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRAU_INSTRUCAO'
            Title.Caption = '{Grau Instru��o]'
            Width = 220
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = '[Dados para Admiss�o]'
      ImageIndex = 1
      object PageControl2: TPageControl
        Left = 8
        Top = 8
        Width = 997
        Height = 505
        ActivePage = TabSheet4
        TabOrder = 0
        OnChanging = PageControl2Changing
        object TabSheet3: TTabSheet
          Caption = '[Identifica��o, Informa��es Gerais e Documenta��o]'
          object GroupBox4: TGroupBox
            Left = 7
            Top = 38
            Width = 381
            Height = 53
            Caption = '[Local/Centro de Custo]'
            TabOrder = 0
            object edLocalUnidade: TEdit
              Left = 9
              Top = 19
              Width = 362
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox5: TGroupBox
            Left = 386
            Top = 38
            Width = 359
            Height = 53
            Caption = '[Endere�o]'
            TabOrder = 1
            object edEndereco: TEdit
              Left = 8
              Top = 19
              Width = 343
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox6: TGroupBox
            Left = 743
            Top = 38
            Width = 242
            Height = 53
            Caption = '[Complemento]'
            TabOrder = 2
            object edComplemento: TEdit
              Left = 8
              Top = 19
              Width = 225
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox7: TGroupBox
            Left = 8
            Top = 93
            Width = 253
            Height = 53
            Caption = '[Bairro]'
            TabOrder = 3
            object edBairro: TEdit
              Left = 8
              Top = 19
              Width = 234
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox8: TGroupBox
            Left = 259
            Top = 93
            Width = 120
            Height = 53
            Caption = '[CEP]'
            TabOrder = 4
            object mskCep: TMaskEdit
              Left = 10
              Top = 19
              Width = 97
              Height = 21
              Color = clYellow
              EditMask = '99999-999'
              MaxLength = 9
              TabOrder = 0
              Text = '     -   '
            end
          end
          object GroupBox9: TGroupBox
            Left = 377
            Top = 93
            Width = 129
            Height = 53
            Caption = '[Telefone Residencial]'
            TabOrder = 5
            object mskTelefoneRes: TMaskEdit
              Left = 10
              Top = 19
              Width = 97
              Height = 21
              Color = clYellow
              EditMask = '(99)9999-9999'
              MaxLength = 13
              TabOrder = 0
              Text = '(  )    -    '
            end
          end
          object GroupBox10: TGroupBox
            Left = 504
            Top = 93
            Width = 129
            Height = 53
            Caption = '[Telefone Celular]'
            TabOrder = 6
            object mskTelefoneCel: TMaskEdit
              Left = 10
              Top = 19
              Width = 97
              Height = 21
              Color = clYellow
              EditMask = '(99)99999-9999'
              MaxLength = 14
              TabOrder = 0
              Text = '(  )     -    '
            end
          end
          object GroupBox11: TGroupBox
            Left = 631
            Top = 93
            Width = 227
            Height = 53
            Caption = '[Grau de Instru��o]'
            TabOrder = 7
            object cbGrauInstrucao: TComboBox
              Left = 8
              Top = 19
              Width = 210
              Height = 22
              Style = csOwnerDrawFixed
              Color = clYellow
              ItemHeight = 16
              TabOrder = 0
              Items.Strings = (
                '8� Ano do Ensino Fundamental'
                '9� Ano do Ensino Fundamental'
                '1.� Ano do Ensino M�dio'
                '2.� Ano do Ensino M�dio'
                '3.� Ano do Ensino M�dio'
                'Concluinte do Ensino Fundamental'
                'Concluinte do Ensino M�dio')
            end
          end
          object GroupBox12: TGroupBox
            Left = 8
            Top = 148
            Width = 250
            Height = 53
            Caption = '[e-Mail]'
            TabOrder = 9
            object edEMail: TEdit
              Left = 8
              Top = 19
              Width = 234
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox13: TGroupBox
            Left = 856
            Top = 93
            Width = 130
            Height = 53
            Caption = '[Estado Civil]'
            TabOrder = 8
            object edEstadoCivil: TEdit
              Left = 9
              Top = 18
              Width = 111
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox14: TGroupBox
            Left = 256
            Top = 148
            Width = 204
            Height = 53
            Caption = '[Nacionalidade]'
            TabOrder = 10
            object edNacionalidade: TEdit
              Left = 8
              Top = 19
              Width = 187
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox15: TGroupBox
            Left = 458
            Top = 148
            Width = 141
            Height = 53
            Caption = '[Data de Nascimento]'
            TabOrder = 11
            object mskNascimento: TMaskEdit
              Left = 10
              Top = 19
              Width = 97
              Height = 21
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox16: TGroupBox
            Left = 597
            Top = 148
            Width = 389
            Height = 53
            Caption = '[Local de Nascimento]'
            TabOrder = 12
            object edLocalNascimento: TEdit
              Left = 9
              Top = 19
              Width = 371
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object Panel3: TPanel
            Left = 6
            Top = 216
            Width = 979
            Height = 33
            Caption = 'DOCUMENTA��O'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 24
          end
          object GroupBox17: TGroupBox
            Left = 6
            Top = 251
            Width = 155
            Height = 53
            Caption = '[RG]'
            TabOrder = 13
            object edNumRG: TEdit
              Left = 9
              Top = 19
              Width = 134
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox18: TGroupBox
            Left = 159
            Top = 251
            Width = 120
            Height = 53
            Caption = '[Data Emiss�o]'
            TabOrder = 14
            object mskEmissaoRG: TMaskEdit
              Left = 10
              Top = 19
              Width = 97
              Height = 21
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox19: TGroupBox
            Left = 277
            Top = 251
            Width = 210
            Height = 53
            Caption = '[�rg�o Emissor]'
            TabOrder = 15
            object edEmissorRG: TEdit
              Left = 9
              Top = 19
              Width = 192
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox20: TGroupBox
            Left = 485
            Top = 251
            Width = 141
            Height = 53
            Caption = '[Carteira de Trabalho N.�]'
            TabOrder = 16
            object edNumCTPS: TEdit
              Left = 8
              Top = 19
              Width = 125
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
              OnKeyPress = edNumCTPSKeyPress
            end
          end
          object GroupBox21: TGroupBox
            Left = 624
            Top = 251
            Width = 151
            Height = 53
            Caption = '[Carteira de Trabalho - S�rie]'
            TabOrder = 17
            object edSerieCTPS: TEdit
              Left = 8
              Top = 19
              Width = 133
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox22: TGroupBox
            Left = 773
            Top = 251
            Width = 214
            Height = 53
            Caption = '[Carteira de Trabalho - Emiss�o e UF]'
            TabOrder = 18
            object edUFCTPS: TEdit
              Left = 128
              Top = 18
              Width = 37
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              MaxLength = 2
              TabOrder = 1
            end
            object mskEmissaoCTPS: TMaskEdit
              Left = 10
              Top = 19
              Width = 97
              Height = 21
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox23: TGroupBox
            Left = 6
            Top = 305
            Width = 167
            Height = 53
            Caption = '[CPF]'
            TabOrder = 19
            object edNumCPF: TEdit
              Left = 9
              Top = 19
              Width = 150
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox24: TGroupBox
            Left = 171
            Top = 305
            Width = 153
            Height = 53
            Caption = '[PIS]'
            TabOrder = 20
            object edNumPIS: TEdit
              Left = 9
              Top = 19
              Width = 135
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              MaxLength = 11
              TabOrder = 0
              OnKeyPress = edNumPISKeyPress
            end
          end
          object GroupBox25: TGroupBox
            Left = 322
            Top = 305
            Width = 121
            Height = 53
            Caption = '[PIS - Data Emiss�o]'
            TabOrder = 21
            object mskEmissaoPIS: TMaskEdit
              Left = 8
              Top = 18
              Width = 102
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox26: TGroupBox
            Left = 441
            Top = 305
            Width = 322
            Height = 53
            Caption = '[PIS - Banco]'
            TabOrder = 22
            object edBancoPIS: TEdit
              Left = 7
              Top = 19
              Width = 303
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object Panel1: TPanel
            Left = 6
            Top = 3
            Width = 979
            Height = 33
            Caption = 'IDENTIFICA��O E INFORMA��ES GERAIS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 25
          end
          object GroupBox46: TGroupBox
            Left = 761
            Top = 305
            Width = 226
            Height = 53
            Caption = '[N.� Cart�o SUS]'
            TabOrder = 23
            object edNumCartaoSUS: TEdit
              Left = 7
              Top = 19
              Width = 210
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
              OnKeyPress = edNumCartaoSUSKeyPress
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = '[Contrato]'
          ImageIndex = 1
          object Panel2: TPanel
            Left = 6
            Top = 6
            Width = 979
            Height = 33
            Caption = 'CONTRATO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
          end
          object GroupBox2: TGroupBox
            Left = 6
            Top = 43
            Width = 155
            Height = 53
            Caption = '[Data de Admiss�o]'
            TabOrder = 0
            object mskAdmissao: TMaskEdit
              Left = 8
              Top = 18
              Width = 126
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox3: TGroupBox
            Left = 159
            Top = 43
            Width = 155
            Height = 53
            Caption = '[T�rmino de Contrato]'
            TabOrder = 1
            object mskTerminoContrato: TMaskEdit
              Left = 8
              Top = 18
              Width = 126
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object rgPrimeiroEmprego: TRadioGroup
            Left = 442
            Top = 43
            Width = 117
            Height = 53
            Caption = '[Primeiro Emprego?]'
            Items.Strings = (
              'SIM'
              'N�O')
            TabOrder = 3
          end
          object GroupBox27: TGroupBox
            Left = 557
            Top = 43
            Width = 429
            Height = 53
            Caption = '[Banco: se Santander Ag�ncia e Conta Corrente] *APENAS N�MEROS'
            TabOrder = 4
            object edNumCCorrente: TEdit
              Left = 80
              Top = 21
              Width = 338
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 1
              OnKeyPress = edNumCCorrenteKeyPress
            end
            object mskNumAgencia: TMaskEdit
              Left = 10
              Top = 21
              Width = 65
              Height = 21
              Color = clYellow
              EditMask = '99999'
              MaxLength = 5
              TabOrder = 0
              Text = '     '
            end
          end
          object GroupBox28: TGroupBox
            Left = 312
            Top = 43
            Width = 132
            Height = 53
            Caption = '[Data Exame M�dico]'
            TabOrder = 2
            object mskExameMedico: TMaskEdit
              Left = 8
              Top = 18
              Width = 115
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              EditMask = '99/99/9999'
              MaxLength = 10
              TabOrder = 0
              Text = '  /  /    '
            end
          end
          object GroupBox29: TGroupBox
            Left = 512
            Top = 154
            Width = 474
            Height = 53
            Caption = '[Cargo/Fun��o]'
            TabOrder = 9
            object cbCargoFuncao: TComboBox
              Left = 7
              Top = 20
              Width = 457
              Height = 22
              Style = csOwnerDrawVariable
              Color = clYellow
              ItemHeight = 16
              TabOrder = 0
            end
          end
          object rgContribuiSindicato: TRadioGroup
            Left = 6
            Top = 98
            Width = 383
            Height = 53
            Caption = '[J� fez contribui��o sindical neste exerc�cio?      Valor R$] '
            Items.Strings = (
              'SIM'
              'N�O')
            TabOrder = 5
            OnClick = rgContribuiSindicatoClick
          end
          object GroupBox30: TGroupBox
            Left = 387
            Top = 98
            Width = 203
            Height = 53
            Caption = '[Sal�rio Base R$]'
            TabOrder = 6
            object edValorSalarioBase: TEdit
              Left = 7
              Top = 17
              Width = 132
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox31: TGroupBox
            Left = 588
            Top = 98
            Width = 398
            Height = 53
            Caption = '[Hor�rio de Trabalho]'
            TabOrder = 7
            object edHorarioTrabalho: TEdit
              Left = 7
              Top = 17
              Width = 379
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object edValorContribuicaoSindicato: TEdit
            Left = 245
            Top = 116
            Width = 132
            Height = 21
            CharCase = ecUpperCase
            Color = clYellow
            TabOrder = 11
          end
          object GroupBox49: TGroupBox
            Left = 7
            Top = 154
            Width = 506
            Height = 53
            Caption = '[Curso]'
            TabOrder = 8
            object cbCurso: TComboBox
              Left = 7
              Top = 18
              Width = 485
              Height = 22
              Style = csOwnerDrawVariable
              Color = clYellow
              ItemHeight = 16
              TabOrder = 0
              Items.Strings = (
                'APRENDIZAGEM PROFISSIONAL')
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = '[Benef�cios e Dependentes]'
          ImageIndex = 2
          object Panel4: TPanel
            Left = 6
            Top = 6
            Width = 978
            Height = 33
            Caption = 'BENEF�CIOS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
          end
          object rgDesejaSeguroVida: TRadioGroup
            Left = 7
            Top = 43
            Width = 978
            Height = 48
            Caption = '[Deseja Seguro de Vida?]'
            Columns = 2
            Items.Strings = (
              'SIM'
              'N�O')
            TabOrder = 0
            OnClick = rgDesejaSeguroVidaClick
          end
          object GroupBox32: TGroupBox
            Left = 7
            Top = 93
            Width = 978
            Height = 129
            Caption = 
              '[Benefici�rios do Seguro de Vida]  -  Favor informar Nome Comple' +
              'to, Parentesco e Percentual do Seguro'
            TabOrder = 1
            object meBeneficiariosSeguroVida: TMemo
              Left = 9
              Top = 16
              Width = 960
              Height = 101
              Color = clYellow
              TabOrder = 0
            end
          end
          object Panel5: TPanel
            Left = 5
            Top = 226
            Width = 979
            Height = 33
            Caption = 'DEPENDENTES'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 9
          end
          object GroupBox33: TGroupBox
            Left = 5
            Top = 264
            Width = 980
            Height = 148
            Caption = 
              '[Dependentes]  -  Favor informar NOME COMPLETO, PARENTESCO E SE ' +
              'INCLUI PARA SAL�RIO FAM�LIA'
            TabOrder = 2
            object meDependentes: TMemo
              Left = 9
              Top = 16
              Width = 960
              Height = 119
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox34: TGroupBox
            Left = 5
            Top = 418
            Width = 232
            Height = 53
            Caption = '[Nome do Pai]'
            TabOrder = 3
            object edNomePai: TEdit
              Left = 8
              Top = 17
              Width = 216
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
            end
          end
          object GroupBox35: TGroupBox
            Left = 235
            Top = 418
            Width = 211
            Height = 53
            Caption = '[Nome da M�e]'
            TabOrder = 4
            object edNomeMae: TEdit
              Left = 9
              Top = 17
              Width = 195
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
            end
          end
          object GroupBox36: TGroupBox
            Left = 648
            Top = 418
            Width = 195
            Height = 53
            Caption = '[Respons�vel de fato]'
            TabOrder = 6
            object edResponsavel: TEdit
              Left = 7
              Top = 17
              Width = 179
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object rgGuardaRegularizada: TRadioGroup
            Left = 841
            Top = 418
            Width = 144
            Height = 53
            Caption = '[Guarda Regularizada?]'
            Color = clBtnFace
            Items.Strings = (
              'SIM'
              'N�O')
            ParentColor = False
            TabOrder = 7
          end
          object GroupBox37: TGroupBox
            Left = 444
            Top = 418
            Width = 205
            Height = 53
            Caption = '[C�njuge]'
            TabOrder = 5
            object edConjuge: TEdit
              Left = 9
              Top = 17
              Width = 188
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = '[Vale-Transporte e Caracter�sticas F�sicas]'
          ImageIndex = 3
          object Panel6: TPanel
            Left = 6
            Top = 6
            Width = 978
            Height = 33
            Caption = 'VALE-TRANSPORTE / PASSE ESCOLAR'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
          end
          object rgNecessitaVT: TRadioGroup
            Left = 8
            Top = 44
            Width = 977
            Height = 60
            Caption = 
              '[Necessita de Vale-Transporte?          [Tipo/Itiner�rio]       ' +
              '                                                                ' +
              '                                                                ' +
              '                             [Quantidade/Dia]'
            Items.Strings = (
              'SIM'
              'N�O')
            TabOrder = 0
            OnClick = rgNecessitaVTClick
          end
          object rgNecessitaPasse: TRadioGroup
            Left = 8
            Top = 108
            Width = 977
            Height = 60
            Caption = 
              '[Necessita de Passe Escolar?             [Tipo/Itiner�rio]      ' +
              '                                                                ' +
              '                                                                ' +
              '                              [Quantidade/Dia]'
            Items.Strings = (
              'SIM'
              'N�O')
            TabOrder = 1
            OnClick = rgNecessitaPasseClick
          end
          object GroupBox38: TGroupBox
            Left = 8
            Top = 168
            Width = 977
            Height = 53
            Caption = '[Quantidade de passes recebidos no ato da admiss�o]'
            TabOrder = 2
            object Label2: TLabel
              Left = 11
              Top = 21
              Width = 169
              Height = 13
              Caption = 'RECEBI, NO ATO DA ADMISS�O, '
            end
            object Label3: TLabel
              Left = 235
              Top = 21
              Width = 228
              Height = 13
              Caption = 'VALES-TRANSPORTE/PASSES ESCOLARES.'
            end
            object mskTotalVTPasse: TMaskEdit
              Left = 183
              Top = 16
              Width = 44
              Height = 21
              Color = clYellow
              EditMask = '9999'
              MaxLength = 4
              TabOrder = 0
              Text = '    '
              OnEnter = mskTotalVTPasseEnter
            end
          end
          object Panel7: TPanel
            Left = 6
            Top = 223
            Width = 978
            Height = 33
            Caption = 'CARACTER�STICAS F�SICAS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
          end
          object GroupBox39: TGroupBox
            Left = 7
            Top = 259
            Width = 268
            Height = 53
            Caption = '[Ra�a/Cor]'
            TabOrder = 3
            object cbRaca: TComboBox
              Left = 8
              Top = 20
              Width = 250
              Height = 22
              Style = csOwnerDrawVariable
              Color = clYellow
              ItemHeight = 16
              TabOrder = 0
              Items.Strings = (
                'BRANCA'
                'PRETA'
                'AMARELA'
                'PARDA'
                'IND�GENA')
            end
          end
          object GroupBox40: TGroupBox
            Left = 273
            Top = 259
            Width = 91
            Height = 53
            Caption = '[Altura]'
            TabOrder = 4
            object edAltura: TEdit
              Left = 9
              Top = 19
              Width = 56
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox41: TGroupBox
            Left = 456
            Top = 259
            Width = 179
            Height = 53
            Caption = '[Cor dos Olhos]'
            TabOrder = 6
            object edCorOlhos: TEdit
              Left = 9
              Top = 19
              Width = 162
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox42: TGroupBox
            Left = 362
            Top = 259
            Width = 96
            Height = 53
            Caption = '[Peso]'
            TabOrder = 5
            object edPeso: TEdit
              Left = 9
              Top = 19
              Width = 56
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox43: TGroupBox
            Left = 633
            Top = 259
            Width = 180
            Height = 53
            Caption = '[Cor dos Cabelos]'
            TabOrder = 7
            object edCorCabelos: TEdit
              Left = 9
              Top = 19
              Width = 162
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object GroupBox44: TGroupBox
            Left = 811
            Top = 259
            Width = 174
            Height = 53
            Caption = '[Tipo Sangu�neo]'
            TabOrder = 8
            object edTipoSanguineo: TEdit
              Left = 9
              Top = 19
              Width = 148
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object rgPortadorNecessidades: TRadioGroup
            Left = 8
            Top = 316
            Width = 212
            Height = 53
            Caption = '[� portador dse necessidades especiais?]'
            Items.Strings = (
              'SIM'
              'N�O')
            TabOrder = 9
          end
          object GroupBox45: TGroupBox
            Left = 218
            Top = 316
            Width = 678
            Height = 53
            Caption = '[Qual]'
            TabOrder = 10
            object edQualDeficiencia: TEdit
              Left = 9
              Top = 19
              Width = 656
              Height = 21
              CharCase = ecUpperCase
              Color = clYellow
              TabOrder = 0
            end
          end
          object edItinerarioVT: TEdit
            Left = 195
            Top = 65
            Width = 561
            Height = 21
            CharCase = ecUpperCase
            Color = clYellow
            TabOrder = 14
          end
          object edItinerarioPasse: TEdit
            Left = 195
            Top = 129
            Width = 561
            Height = 21
            CharCase = ecUpperCase
            Color = clYellow
            TabOrder = 16
          end
          object rgFumante: TRadioGroup
            Left = 894
            Top = 316
            Width = 91
            Height = 53
            Caption = '[� Fumante?]'
            Items.Strings = (
              'N�O'
              'SIM')
            TabOrder = 11
          end
          object mskQtdVT: TMaskEdit
            Left = 759
            Top = 66
            Width = 37
            Height = 21
            Color = clYellow
            EditMask = '999'
            MaxLength = 3
            TabOrder = 15
            Text = '   '
          end
          object mskQtdPasse: TMaskEdit
            Left = 759
            Top = 129
            Width = 37
            Height = 21
            Color = clYellow
            EditMask = '999'
            MaxLength = 3
            TabOrder = 17
            Text = '   '
          end
        end
      end
      object Panel8: TPanel
        Left = 8
        Top = 520
        Width = 995
        Height = 46
        TabOrder = 1
        object btnAlterar: TSpeedButton
          Left = 613
          Top = 5
          Width = 119
          Height = 37
          Caption = '&Alterar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          Visible = False
          OnClick = btnAlterarClick
        end
        object btnInserir: TSpeedButton
          Left = 743
          Top = 5
          Width = 119
          Height = 37
          Caption = '&Inserir'
          Flat = True
          Glyph.Data = {
            02030000424D0203000000000000360100002800000013000000170000000100
            080000000000CC010000C30E0000C30E000040000000000000001C3404002434
            1C00242424001C3C0400243C0C00244404002C5C04003C5C240044543C005C5C
            54005C5C5C00646464006C6C6C0054743C007474740044840400747C74007C7C
            7C0084848400449404006C8C540054AC0400000000008C8C8C008C948C009494
            94009C9C9C00A4A4A400ACACAC00B4B4B4006CD404006CDC040074F404007CFC
            040084FC0C0084FC14007CDC24008CFC1C008CFC240094FC240094EC3C0094FC
            2C009CFC3C0094D45C009CF44C009CFC4400A4FC4C00A4FC5400ACFC6400B4FC
            6C00B4F47400BCF48400BCFC7C00B4C4A400ACCC9400BCCCAC00BCC4B400BCCC
            B400B4E48C00BCE49400BCDCA400C4F49400C4FC8C00C0C0C0003F3F3F3F3F3F
            191717193F3F3F3F3F3F3F3F3F003F3F3F3F3F1712111112193F3F3F3F3F3F3F
            3F003F3F3F3F19120E0C0C0E123F3F3F3F3F3F3F3F003F3F3F3F120E0C0B0B0C
            11173F3F3F3F3F3F3F003F3F3F17110C0B0A0A0B0E123F3F3F3F3F3F3F003F3F
            3F12140702010B0B0C11173F3F3F3F3F3F003F3F3F181E1E0F03100C0C0E1219
            3F3F3F3F3F003F3F3F2422231F06080C0C0C11173F3F3F3F3F003F3F2B212223
            221305170C0C0E11173F3F3F3F003F3521222323231E06090E0C0C0E12193F3F
            3F003F2B2223272726221304180E0C0C0E123F3F3F003F2926252A2F2F261F06
            08110E0C0E11173F3F0038302D232C39332E23150311110E0C0E11173F003F39
            2E28383F37312A220F0117110E0E0E1219003F3F373F3F3F3F3A30261E060917
            110E0E1117003F3F3F3F3F3F3F3F322E2315030C1712111217003F3F3F3F3F3F
            3F3F37342D2313001819171719003F3F3F3F3F3F3F3F3F3B342E231300193F3F
            3F003F3F3F3F3F3F3F3F3F3F3C3330230F011D3F3F003F3F3F3F3F3F3F3F3F3F
            3F393E31250F0D3F3F003F3F3F3F3F3F3F3F3F3F3F3F383D312320353F003F3F
            3F3F3F3F3F3F3F3F3F3F3F3F3C2A23363F003F3F3F3F3F3F3F3F3F3F3F3F3F3F
            3F3F373F3F00}
          OnClick = btnInserirClick
        end
        object btnCancelar: TSpeedButton
          Left = 871
          Top = 5
          Width = 119
          Height = 37
          Caption = '&Cancelar'
          Flat = True
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FF033B8A033D90013D95023B91033A89033A89FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0357D30147B20051D0035CE007
            63E3035CE0004ED30042B7023A8F023A8FFF00FFFF00FFFF00FFFF00FFFF00FF
            0650BA0357D32781F278B4F7CAE2FCE9F4FFDCEDFF9CC7FA3F8FF20155DD0140
            A404367DFF00FFFF00FFFF00FF075DD70762E155A0F7F3F8FEFFFFFFE9F3FCC6
            DEFAD9E9FCFFFFFFFFFFFF99C5F8055DE70040A302398BFF00FFFF00FF075DD7
            529EF7FEFEFFE2EFFC0F65EB0558E70959E50250E20454E16FA6F0DEEBFC9CC9
            F80355DE02398BFF00FF0455C9207DF0E1EFFEFFFFFF6FA7F076AFF7176CED06
            5AE9075AE60F5EE66AA1F06FA7F0FFFFFF3E8FF20043B7033E96085FDA56A1FA
            FFFFFF9ECBFB1573F779B4FACFE3FC1C72EF2274EECBE1FB6DA5F20556E3DEEB
            FC9FCBFA0050D4033E960F6BE68BC1FCFFFFFF2987FC1F7DFA1674F779B5FADE
            EDFEDDEDFC6EAAF4065AE90455E5A0C5F6DEEFFF0560E202409C1B76EDA4CFFC
            FFFFFF2988FF1C7EFE1C7BFB2D87FBEDF6FEEDF6FE2279F20B63ED085DEA88BA
            F4EBF6FF0C68E60141A1207AEBA5CFFEFFFFFF3F97FF1F81FF3B93FEE1EFFF6B
            ADFC69ABFBE0EEFE2C80F30C65EEC6DEFBCEE5FE0763E203419E146FE79ACAFC
            FFFFFFB2D8FF318EFFE7F3FF67AFFF1D7EFE1A7AFB60A7FCE5F2FE3F8FF6E2EF
            FE81BAF80258D8033E96FF00FF237BEBEDF6FFFAFCFF5DA9FF469AFF1F81FF1F
            81FF1E80FF1C7DFC4D9CFBF0F8FFF2F8FE3089F4024FC0FF00FFFF00FF237BEB
            BFDEFFF3F8FFFAFCFFB0D5FF3E96FF2B89FF308CFF6AB0FEFAFCFFFFFFFF5DA6
            F70860DE024FC0FF00FFFF00FFFF00FF4997F3C7E3FFF7FBFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFE0EFFE5CA5F80E6BE70552C2FF00FFFF00FFFF00FFFF00FF
            FF00FF2D82EB91C5FBCCE6FFD9EDFFDCEDFEC4E0FE86BFFC348BF40A65E10A65
            E1FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF247BEB4696F34A
            98F42F87F0116CE6075FDCFF00FFFF00FFFF00FFFF00FFFF00FF}
          OnClick = btnCancelarClick
        end
        object Label6: TLabel
          Left = 8
          Top = 11
          Width = 500
          Height = 26
          Alignment = taCenter
          Caption = 
            'PARA QUE O PROCESSO ADMISSIONAL OCORRA SEM ERROS, PEDE-SE FAVOR ' +
            'DE CADASTRAR/INFORMAR TODOS OS CAMPOS.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
      end
    end
  end
  object Panel9: TPanel
    Left = 6
    Top = 656
    Width = 1018
    Height = 54
    Color = clYellow
    TabOrder = 2
    object Label4: TLabel
      Left = 108
      Top = 12
      Width = 761
      Height = 13
      Alignment = taCenter
      Caption = 
        'Aten��o !!! Para gerar NOVA admiss�o, basta selecionar o adolesc' +
        'ente e o sistema te levar� ao preenchimento dos campos faltantes' +
        '.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 27
      Width = 1002
      Height = 13
      Alignment = taCenter
      Caption = 
        'Para reimprimir tanto o contrato quanto a ficha de cadastro, bas' +
        'ta selecionar no grid acima, o registro a ser impresso e clicar ' +
        'em "Emitir 2.� Via de Contrato e Ficha Admissional.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object dsAdmissoes: TDataSource
    DataSet = dmDeca.cdsSel_Admissoes
    Left = 908
    Top = 19
  end
end
