object frmACESSO2: TfrmACESSO2
  Left = 461
  Top = 316
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'frmACESSO2'
  ClientHeight = 355
  ClientWidth = 667
  Color = clHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnPaint = FormPaint
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 667
    Height = 355
    Align = alClient
    Stretch = True
  end
  object Label5: TLabel
    Left = 62
    Top = 313
    Width = 566
    Height = 15
    Caption = 
      'Desenvolvido pela Divis�o de Tecnologia da Informa��o - FUNDHAS ' +
      '  -   Todos os direitos reservados.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 91
    Top = 279
    Width = 204
    Height = 16
    Cursor = crHandPoint
    Caption = 'Visite http://www.fundhas.org.br'
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = Label6Click
  end
  object Label9: TLabel
    Left = 163
    Top = 327
    Width = 323
    Height = 15
    Caption = 'Copyright (R) - Todos os direitos reservados.  2003 - 2018'
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = btnLoginClick
  end
  object Label1: TLabel
    Left = 56
    Top = 93
    Width = 72
    Height = 13
    Caption = 'MATR�CULA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 8454143
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 72
    Top = 119
    Width = 57
    Height = 13
    Caption = 'UNIDADE'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 8454143
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 88
    Top = 144
    Width = 43
    Height = 13
    Caption = 'SENHA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 8454143
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 136
    Top = 6
    Width = 211
    Height = 32
    Caption = 'Sistema DECA'
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -27
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label11: TLabel
    Left = 136
    Top = 38
    Width = 358
    Height = 13
    Caption = 'Voc� est� utilizando a vers�o 7.3.1 de 26 de MAR�O de 2018.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 8454143
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 358
    Top = 279
    Width = 218
    Height = 16
    Cursor = crHandPoint
    Caption = 'Pressione a tecla <ESC> para sair.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = Label6Click
  end
  object lbIp: TLabel
    Left = 136
    Top = 54
    Width = 6
    Height = 13
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 8454143
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object btnAlterarSenha: TSpeedButton
    Left = 370
    Top = 202
    Width = 235
    Height = 47
    Caption = 'ALTERAR SUA SENHA'
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
    OnClick = btnAlterarSenhaClick
  end
  object btnLogin: TSpeedButton
    Left = 232
    Top = 202
    Width = 235
    Height = 47
    Caption = 'ACESSO AO SISTEMA'
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = btnLoginClick
  end
  object mskSenha: TMaskEdit
    Left = 136
    Top = 139
    Width = 125
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 10
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 2
    OnEnter = mskSenhaEnter
    OnExit = mskSenhaExit
  end
  object cbUnidades: TComboBox
    Left = 136
    Top = 113
    Width = 497
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ItemHeight = 16
    ParentFont = False
    TabOrder = 1
    OnClick = cbUnidadesClick
  end
  object mskMatricula: TMaskEdit
    Left = 136
    Top = 87
    Width = 125
    Height = 24
    EditMask = '99999999'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 8
    ParentFont = False
    TabOrder = 0
    Text = '        '
    OnEnter = mskMatriculaEnter
    OnExit = mskMatriculaExit
  end
end
