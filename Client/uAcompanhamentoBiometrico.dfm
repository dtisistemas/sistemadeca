object frmAcompanhamentoBiometrico: TfrmAcompanhamentoBiometrico
  Left = 227
  Top = 85
  BorderStyle = bsDialog
  Caption = '[Sistema Deca] - Acompanhamento Biom�trico'
  ClientHeight = 578
  ClientWidth = 860
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 288
    Top = 8
    Width = 570
    Height = 521
  end
  object Label2: TLabel
    Left = 8
    Top = 180
    Width = 96
    Height = 13
    Caption = 'Rela��o da unidade'
  end
  object rgCriterio: TRadioGroup
    Left = 3
    Top = 3
    Width = 280
    Height = 52
    Caption = 'Crit�rio'
    ItemIndex = 0
    Items.Strings = (
      'Unidade'
      'Geral')
    TabOrder = 0
    OnClick = rgCriterioClick
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 97
    Width = 280
    Height = 77
    TabOrder = 1
    Visible = False
    object chkAbaixoDoPeso: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Abaixo do Peso'
      TabOrder = 0
    end
    object chkNormal: TCheckBox
      Left = 8
      Top = 32
      Width = 106
      Height = 17
      Caption = 'Condi��o Normal'
      TabOrder = 1
    end
    object chkSobrePeso: TCheckBox
      Left = 8
      Top = 48
      Width = 97
      Height = 17
      Caption = 'Sobre Peso'
      TabOrder = 2
    end
    object chkObesidade: TCheckBox
      Left = 144
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Obesidade'
      TabOrder = 3
    end
    object chkTodas: TCheckBox
      Left = 144
      Top = 32
      Width = 106
      Height = 17
      Caption = 'Todas'
      TabOrder = 4
      OnClick = chkTodasClick
    end
  end
  object Panel1: TPanel
    Left = 4
    Top = 103
    Width = 278
    Height = 41
    TabOrder = 2
    object btnVer: TSpeedButton
      Left = 80
      Top = 5
      Width = 130
      Height = 31
      Caption = 'Ver'
      Glyph.Data = {
        8A010000424D8A01000000000000760000002800000018000000170000000100
        04000000000014010000130B0000130B00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888877777777777777777888880
        0000000000000000078888800000000000000000088888888822278444781117
        8888888188222784447811178888888188222784447811178888888888222784
        4478111788888881882227844478111788888881882227844478111788888888
        8822288444781117888888818888888444781117888888818888888444781118
        8888888888888884447811188888888188888884447888888888888188888884
        4488888888888888888888844488888888888881888888888888888888888881
        8888888888888888888888888888888888888888888888888888888888888888
        8888888888888888888888888888}
      OnClick = btnVerClick
    end
    object btnLimpar: TSpeedButton
      Left = 244
      Top = 5
      Width = 89
      Height = 31
      Caption = 'Limpar'
      Visible = False
    end
  end
  object Panel2: TPanel
    Left = 288
    Top = 531
    Width = 570
    Height = 41
    TabOrder = 3
    object btnImprimirGrafico: TSpeedButton
      Left = 314
      Top = 5
      Width = 199
      Height = 31
      Caption = 'Imprimir Gr�fico'
      OnClick = btnImprimirGraficoClick
    end
    object btnClassificacao: TSpeedButton
      Left = 66
      Top = 5
      Width = 199
      Height = 31
      Caption = 'Rela��o por Classifica��o'
      OnClick = btnClassificacaoClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 3
    Top = 55
    Width = 280
    Height = 43
    TabOrder = 4
    object cbUnidade: TComboBox
      Left = 7
      Top = 13
      Width = 264
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
    end
  end
  object GraficoBiometrico: TDBChart
    Left = 296
    Top = 16
    Width = 553
    Height = 507
    BackWall.Brush.Color = clWhite
    PrintProportional = False
    Title.Text.Strings = (
      'Gr�fico de Classifica��es de Avalia��es BIom�tricas')
    View3DOptions.Elevation = 315
    View3DOptions.Orthogonal = False
    View3DOptions.Perspective = 0
    View3DOptions.Rotation = 360
    TabOrder = 5
    object Label1: TLabel
      Left = 66
      Top = 449
      Width = 431
      Height = 26
      Alignment = taCenter
      Caption = 
        'Aten��o!!! Para que os n�mero estejam sempre exatos, � necess�ri' +
        'o que se verifiquem as medi��es de todas(os) as(os) crian�as/ado' +
        'lescentes.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Series1: TBarSeries
      ColorEachPoint = True
      Marks.ArrowLength = 20
      Marks.Style = smsLabelPercent
      Marks.Visible = True
      DataSource = dmDeca.cdsSel_GraficoBiometrico
      SeriesColor = clRed
      XLabelsSource = 'dsc_classificacao'
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Bar'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'num_total'
    end
  end
  object DBGrid1: TDBGrid
    Left = 6
    Top = 196
    Width = 275
    Height = 375
    DataSource = dsTmpBiometricos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NOM_NOME'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Title.Caption = 'Nome'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DSC_CLASSIFICACAO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Title.Caption = 'Classifica��o'
        Width = 100
        Visible = True
      end>
  end
  object dsTmpBiometricos: TDataSource
    DataSet = dmDeca.cdsSel_TMPBiometricos
    Left = 224
    Top = 200
  end
end
