unit uResumoMensal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Grids, DBGrids, ExtCtrls, Mask, Buttons, Db, ImgList;

type
  TfrmResumoMensal = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edPrograma: TEdit;
    edUnidade: TEdit;
    edGestor: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dbgResumos: TDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    GroupBox2: TGroupBox;
    cbMes: TComboBox;
    Label5: TLabel;
    mskAno: TMaskEdit;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Shape1: TShape;
    Label10: TLabel;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    edAssistenteSocial: TEdit;
    Shape13: TShape;
    Label18: TLabel;
    Shape14: TShape;
    Shape15: TShape;
    Shape16: TShape;
    Shape17: TShape;
    Shape18: TShape;
    Shape19: TShape;
    Shape20: TShape;
    Shape21: TShape;
    Shape22: TShape;
    Shape23: TShape;
    Shape24: TShape;
    Shape25: TShape;
    Shape26: TShape;
    Shape27: TShape;
    Shape28: TShape;
    Shape29: TShape;
    Shape30: TShape;
    Shape31: TShape;
    Shape32: TShape;
    Shape33: TShape;
    Shape34: TShape;
    Shape35: TShape;
    Shape36: TShape;
    Shape37: TShape;
    Shape38: TShape;
    Shape39: TShape;
    Shape40: TShape;
    Shape41: TShape;
    Shape42: TShape;
    Shape43: TShape;
    Shape44: TShape;
    Shape45: TShape;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Shape46: TShape;
    Label25: TLabel;
    Shape47: TShape;
    Shape48: TShape;
    Shape49: TShape;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Shape50: TShape;
    Shape51: TShape;
    Shape52: TShape;
    Shape53: TShape;
    Label29: TLabel;
    Shape54: TShape;
    Shape55: TShape;
    Shape56: TShape;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Shape57: TShape;
    Shape58: TShape;
    Shape59: TShape;
    Shape60: TShape;
    Label33: TLabel;
    Shape61: TShape;
    Label34: TLabel;
    Shape62: TShape;
    Label35: TLabel;
    Shape63: TShape;
    Label36: TLabel;
    Shape64: TShape;
    Shape65: TShape;
    Label37: TLabel;
    Label38: TLabel;
    Shape66: TShape;
    Shape67: TShape;
    Shape68: TShape;
    Shape69: TShape;
    Shape70: TShape;
    Shape71: TShape;
    Label39: TLabel;
    Label40: TLabel;
    Shape72: TShape;
    Label41: TLabel;
    TabSheet6: TTabSheet;
    Shape73: TShape;
    Label42: TLabel;
    Shape75: TShape;
    Shape76: TShape;
    Shape77: TShape;
    Shape78: TShape;
    Label43: TLabel;
    Shape79: TShape;
    Shape80: TShape;
    Shape81: TShape;
    Shape82: TShape;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Shape83: TShape;
    Shape84: TShape;
    Shape85: TShape;
    Shape86: TShape;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Shape87: TShape;
    Shape88: TShape;
    Shape89: TShape;
    Shape90: TShape;
    Shape91: TShape;
    Shape92: TShape;
    Shape93: TShape;
    Shape94: TShape;
    Shape95: TShape;
    Shape96: TShape;
    Shape97: TShape;
    Shape98: TShape;
    Shape99: TShape;
    Shape100: TShape;
    Shape101: TShape;
    Shape102: TShape;
    Shape103: TShape;
    Shape104: TShape;
    Shape105: TShape;
    Shape106: TShape;
    Shape107: TShape;
    Shape108: TShape;
    Shape109: TShape;
    Shape110: TShape;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Shape111: TShape;
    Label63: TLabel;
    Shape112: TShape;
    Shape113: TShape;
    Label64: TLabel;
    Shape114: TShape;
    Shape74: TShape;
    Shape117: TShape;
    Shape118: TShape;
    Shape119: TShape;
    Shape120: TShape;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Shape115: TShape;
    Label68: TLabel;
    Shape121: TShape;
    Shape122: TShape;
    Shape123: TShape;
    Shape124: TShape;
    Shape125: TShape;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Shape116: TShape;
    Shape126: TShape;
    Shape127: TShape;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Shape128: TShape;
    Shape129: TShape;
    TabSheet7: TTabSheet;
    Shape130: TShape;
    Shape132: TShape;
    Label75: TLabel;
    Label76: TLabel;
    Shape133: TShape;
    Label77: TLabel;
    Shape134: TShape;
    Label78: TLabel;
    Shape135: TShape;
    Shape136: TShape;
    Shape137: TShape;
    Shape138: TShape;
    Label79: TLabel;
    Shape139: TShape;
    Shape140: TShape;
    Shape141: TShape;
    Shape142: TShape;
    Label80: TLabel;
    Label81: TLabel;
    Shape143: TShape;
    Shape144: TShape;
    Shape145: TShape;
    Shape146: TShape;
    Shape147: TShape;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Shape148: TShape;
    Shape149: TShape;
    Shape150: TShape;
    Shape151: TShape;
    Shape152: TShape;
    Label87: TLabel;
    Label88: TLabel;
    Shape153: TShape;
    Shape154: TShape;
    Shape156: TShape;
    Shape155: TShape;
    Shape157: TShape;
    Shape158: TShape;
    Shape159: TShape;
    Shape160: TShape;
    Shape161: TShape;
    Shape162: TShape;
    Shape163: TShape;
    Label89: TLabel;
    Shape164: TShape;
    Shape165: TShape;
    Shape166: TShape;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Shape167: TShape;
    Shape168: TShape;
    Shape169: TShape;
    TabSheet8: TTabSheet;
    Shape131: TShape;
    Label93: TLabel;
    Shape171: TShape;
    Label94: TLabel;
    Shape172: TShape;
    Label95: TLabel;
    edNumTTInicioMesManha: TEdit;
    edNumTTAdmitidosManha: TEdit;
    edNumTTTransferidosDEManha: TEdit;
    edNumTTPetiManha: TEdit;
    edNumTTTransferidosPARAManha: TEdit;
    edNumTTTransferidosParaArteEducManha: TEdit;
    edNumTTTransferidosParaAprendizManha: TEdit;
    edNumTTDesligadosManha: TEdit;
    edNumTTFinalMesManha: TEdit;
    edNumTTInicioMesTarde: TEdit;
    edNumTTAdmitidosTarde: TEdit;
    edNumTTTransferidosDETarde: TEdit;
    edNumTTPetiTarde: TEdit;
    edNumTTTransferidosPARATarde: TEdit;
    edNumTTTransferidosParaArteEducTarde: TEdit;
    edNumTTTransferidosParaAprendizTarde: TEdit;
    edNumTTDesligadosTarde: TEdit;
    edNumTTFinalMesTarde: TEdit;
    edNumTTInicioMes: TEdit;
    edNumTTAdmitidosMes: TEdit;
    edNumTTTransferidosDEMes: TEdit;
    edNumTTPetiMes: TEdit;
    edNumTTTransferidosPARAMes: TEdit;
    edNumTTTransferidosParaArteEducMes: TEdit;
    edNumTTTransferidosParaAprendizMes: TEdit;
    edNumTTDesligadosMes: TEdit;
    edNumTTFinalMesUnidade: TEdit;
    edNumTTAfastados: TEdit;
    edNumTTAlunosMatriculados: TEdit;
    edNumTTForaEnsinoRegular: TEdit;
    edNumTTDefasagemEscolar: TEdit;
    edNumTTFrequenciaDentroEsperado: TEdit;
    edNumTTDefasagemAprendizagem: TEdit;
    edNumTTFinalMes: TEdit;
    edNumTTDiasAtividadesMes: TEdit;
    edNumTTPresencasEsperadasMes: TEdit;
    edNumTTGeralFaltasMes: TEdit;
    edNumTTFaltasJustificadasMes: TEdit;
    edNumTTFaltasInjustificadasMes: TEdit;
    edNumTTDesligadosAPedidoMes: TEdit;
    edNumTTDesligadosAbandonoMes: TEdit;
    edNumTTDesligadosMaioridadeMes: TEdit;
    edNumTTDesligadosLeiMes: TEdit;
    edNumTTDesligadosOutrosMes: TEdit;
    edNumTTSitDisciplinarMes: TEdit;
    edNumTTSitSocioEconomicaMes: TEdit;
    edNumTTSitEscolarMes: TEdit;
    edNumTTSitViolenciaMes: TEdit;
    edNumTTSitAbrigoMes: TEdit;
    edNumTTSitInteracaoRelacMes: TEdit;
    edNumTTSitAtoInfracionalMes: TEdit;
    edNumTTSitAprendizagemMes: TEdit;
    edNumTTSitSaudeMes: TEdit;
    edNumTTSitGuardaMes: TEdit;
    edNumTTSitGravidezMes: TEdit;
    edNumTTSitMoradiaMes: TEdit;
    edNumTTSitTrabInfantilMes: TEdit;
    edNumTTSubtotal2SitMes: TEdit;
    edNumTTTotalSitMes: TEdit;
    edNumTTSubtotal1SitMes: TEdit;
    edNumTTAtivSocioCultMes: TEdit;
    meDescricaoAtivSocioCulturais: TMemo;
    edNumTTAtivRecreativasMes: TEdit;
    meDescricaoAtivRecreativasMes: TMemo;
    meDescricaoAtivSociaisMes: TMemo;
    edNumTTAcoesSociaisMes: TEdit;
    edNumTTAdolAptosAprendizagemProfissionalMes: TEdit;
    edNumTTAdolEncaminhadosAprendizagemProfissionalMes: TEdit;
    edNumTTAdolInseridosAprendizagemProfissionalMes: TEdit;
    edNumTTAdolOPMes: TEdit;
    edNumTTAdolOEMes: TEdit;
    edNumTTAdolFIC1Mes: TEdit;
    edNumTTAdolFIC2Mes: TEdit;
    edNumTTAdolFormacaoTecnicaMes: TEdit;
    edNumTTAdolAprovadosOPMes: TEdit;
    edNumTTAdolAprovadosOEMes: TEdit;
    edNumTTAdolAprovadosFIC1Mes: TEdit;
    edNumTTAdolAprovadosFIC2Mes: TEdit;
    edNumTTAdolAprovadosFormacaoTecnicaMes: TEdit;
    edNumTTAdolReprovadosOPMes: TEdit;
    edNumTTAdolReprovadosOEMes: TEdit;
    edNumTTAdolReprovadosFIC1Mes: TEdit;
    edNumTTAdolReprovadosFIC2Mes: TEdit;
    edNumTTAdolReprovadosFormacaoTecnicaMes: TEdit;
    edNumTTAcoesComFamiliasMes: TEdit;
    edNumTTConvidadosMes: TEdit;
    edNumTTParticipantesMes: TEdit;
    edNumTTAdolInscritosVestibMes: TEdit;
    edNumTTAdolAprovadosVestibMes: TEdit;
    Panel1: TPanel;
    btnSair: TSpeedButton;
    btnImprimir: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnNovo: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnEncerrarResumoMes: TSpeedButton;
    btnReverterResumoMes: TSpeedButton;
    btnCalcularResumo: TSpeedButton;
    cbGrauAproveitamentoNoPrograma: TComboBox;
    cbGrauSatisfacaoCrianca: TComboBox;
    cbGrauSatisfacaoFamilia: TComboBox;
    cbGrauSatisfacaoConvenio: TComboBox;
    ImageList1: TImageList;
    dsResumoMensal: TDataSource;
    Image1: TImage;
    Shape170: TShape;
    Shape174: TShape;
    Label102: TLabel;
    edNumTTAprovEscolarDentroEsperado: TEdit;
    Label103: TLabel;
    Shape175: TShape;
    Shape176: TShape;
    Shape177: TShape;
    Shape178: TShape;
    Shape179: TShape;
    Shape180: TShape;
    Label104: TLabel;
    Edit1: TEdit;
    Label105: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    Label86: TLabel;
    Shape181: TShape;
    Label108: TLabel;
    Label109: TLabel;
    Label110: TLabel;
    Label111: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    procedure Label101Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AtualizaTela (Modo: String);
    procedure LimpaDesabilitaTudo;
    procedure HabilitaCampos;
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edNumTTDefasagemAprendizagemExit(Sender: TObject);
    procedure edNumTTDesligadosOutrosMesExit(Sender: TObject);
    procedure meDescricaoAtivSocioCulturaisExit(Sender: TObject);
    procedure edNumTTAdolAprovadosVestibMesExit(Sender: TObject);
    procedure edNumTTParticipantesMesExit(Sender: TObject);
    procedure edNumTTInicioMesManhaExit(Sender: TObject);
    procedure edNumTTInicioMesTardeExit(Sender: TObject);
    procedure edNumTTAdmitidosManhaExit(Sender: TObject);
    procedure edNumTTAdmitidosTardeExit(Sender: TObject);
    procedure edNumTTTransferidosDEManhaExit(Sender: TObject);
    procedure edNumTTTransferidosDETardeExit(Sender: TObject);
    procedure edNumTTPetiManhaExit(Sender: TObject);
    procedure edNumTTPetiTardeExit(Sender: TObject);
    procedure edNumTTTransferidosPARAManhaExit(Sender: TObject);
    procedure edNumTTTransferidosParaArteEducManhaExit(Sender: TObject);
    procedure edNumTTTransferidosParaAprendizManhaExit(Sender: TObject);
    procedure edNumTTDesligadosManhaExit(Sender: TObject);
    procedure edNumTTDesligadosTardeExit(Sender: TObject);
    procedure edNumTTTransferidosPARATardeExit(Sender: TObject);
    procedure edNumTTTransferidosParaArteEducTardeExit(Sender: TObject);
    procedure edNumTTTransferidosParaAprendizTardeExit(Sender: TObject);
    procedure edNumTTAfastadosExit(Sender: TObject);
    procedure edNumTTPresencasEsperadasMesEnter(Sender: TObject);
    procedure CalculaTotais;
    procedure btnSairClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCalcularResumoClick(Sender: TObject);
    procedure dbgResumosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbMesExit(Sender: TObject);
    procedure mskAnoExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dbgResumosDblClick(Sender: TObject);
    procedure btnEncerrarResumoMesClick(Sender: TObject);
    procedure btnReverterResumoMesClick(Sender: TObject);
    procedure dbgResumosCellClick(Column: TColumn);
    procedure btnAlterarClick(Sender: TObject);
    procedure edNumTTFinalMesEnter(Sender: TObject);
    procedure edNumTTGeralFaltasMesEnter(Sender: TObject);
    procedure edNumTTGeralFaltasMesExit(Sender: TObject);
    procedure edNumTTFaltasJustificadasMesExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmResumoMensal: TfrmResumoMensal;
  mmTOTAL_DESLIGADOS : Integer;

implementation

uses uNavegador, uDM, uPrincipal, uUtil, rResumoMensal, rResumoMensal2;

{$R *.DFM}

procedure TfrmResumoMensal.Label101Click(Sender: TObject);
begin
  ShowMessage('Direcionando para o site web http://qualidade.fundhas.org.br ');
  Application.CreateForm (TfrmNavegador, frmNavegador);
  //frmNavegador.URLs.Text := 'http://qualidade.fundhas.org.br/projetos.html';
  frmNavegador.URLs.Text := 'http://qualidade.fundhas.org.br/Documentos/Programas%20Projetos%20e%20Diretrizes/Projeto%20%20Satisfacao%20do%20Cliente%20-%20Rev.00%20-%20Set.07.pdf';
  frmNavegador.StatusBar1.Visible := False;
  frmNavegador.Show;
end;

procedure TfrmResumoMensal.FormShow(Sender: TObject);
begin
  AtualizaTela('P');
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;

  //Repassa os valores de Unidade, Gestor(a) e Assistente Social
  edPrograma.Text         := vvNOM_DIVISAO;
  edUnidade.Text          := vvNOM_UNIDADE;
  edGestor.Text           := vvNOM_GESTOR;
  edAssistenteSocial.Text := vvNOM_ASOCIAL;

  //Carrega os dados dos lan�amentos realizados para a unidade
  //referente ao gestor que est� logado
  try
    with dmDeca.cdsSel_ResumoMensal do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Params.ParamByName ('@pe_num_ano').Value := Null;
      Params.ParamByName ('@pe_num_mes').Value := Null;
      Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
      Open;
      dbgResumos.Refresh;
    end;
  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar carregar os dados dos Resumos ' +#13+#10 +
                            'Mensais da unidade. Entre em contato com o Administrador.',
                            '[Sistema Deca] - Erro carregando Resumo Mensal...',
                            MB_OK + MB_ICONWARNING);
  end;


end;

procedure TfrmResumoMensal.AtualizaTela(Modo: String);
begin
  if Modo = 'P' then
  begin

    LimpaDesabilitaTudo;

    //Bot�es
    btnEncerrarResumoMes.Enabled := True;
    btnReverterResumoMes.Enabled := True;

    btnNovo.Enabled := True;
    btnAlterar.Enabled := False;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;
    btnSair.Enabled := True;

    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := False;
    //btnCalcularResumo.Enabled := False;
    btnCalcularResumo.Enabled := True;

  end
  else if Modo = 'N' then
  begin

    HabilitaCampos;
    //Bot�es
    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := False;

    btnNovo.Enabled := False;
    btnAlterar.Enabled := False;
    btnSalvar.Enabled := True;  //Habilitar somente com o bot�o calcular...
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;
    btnSair.Enabled := False;

    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := False;
    //btnCalcularResumo.Enabled := False;
    btnCalcularResumo.Enabled := True;

  end;
end;

procedure TfrmResumoMensal.LimpaDesabilitaTudo;
var i : integer;
begin
  for i:=0 to frmResumoMensal.ComponentCount - 1 do

    if frmResumoMensal.Components[i] is TCustomEdit then
    begin
      (frmResumoMensal.Components[i] as TCustomEdit).Clear;
      (frmResumoMensal.Components[i] as TCustomEdit).Enabled := False;
    end;

  cbMes.ItemIndex := -1;
  cbMes.Enabled := False;

  cbGrauAproveitamentoNoPrograma.ItemIndex := -1;
  cbGrauAproveitamentoNoPrograma.Enabled   := False;

  cbGrauSatisfacaoCrianca.ItemIndex        := -1;
  cbGrauSatisfacaoCrianca.Enabled          := False;

  cbGrauSatisfacaoFamilia.ItemIndex        := -1;
  cbGrauSatisfacaoFamilia.Enabled          := False;

  cbGrauSatisfacaoConvenio.ItemIndex       := -1;
  cbGrauSatisfacaoConvenio.Enabled         := False;

end;

procedure TfrmResumoMensal.HabilitaCampos;
var x: integer;
begin
  for x := 0 to frmResumoMensal.ComponentCount - 1 do
    if frmResumoMensal.Components[x] is TCustomEdit then
      (frmResumoMensal.Components[x] as TCustomEdit).Enabled := True;


  for x:=0 to frmResumoMensal.ComponentCount - 1 do
    if frmResumoMensal.Components[x] is TEdit then
      (frmResumoMensal.Components[x] as TEdit).Text := '0';


  cbMes.ItemIndex := -1;
  cbMes.Enabled := True;

  cbGrauAproveitamentoNoPrograma.ItemIndex := -1;
  cbGrauAproveitamentoNoPrograma.Enabled   := True;

  cbGrauSatisfacaoCrianca.ItemIndex        := -1;
  cbGrauSatisfacaoCrianca.Enabled          := True;

  cbGrauSatisfacaoFamilia.ItemIndex        := -1;
  cbGrauSatisfacaoFamilia.Enabled          := True;

  cbGrauSatisfacaoConvenio.ItemIndex       := -1;
  cbGrauSatisfacaoConvenio.Enabled         := True;


  //Desabilitar campos de totais e sub-totais...
  edNumTTInicioMes.Enabled := False;
  edNumTTAdmitidosMes.Enabled := False;
  edNumTTTransferidosDEMes.Enabled := False;
  edNumTTPetiMes.Enabled := False;
  edNumTTTransferidosPARAMes.Enabled := False;
  edNumTTTransferidosParaArteEducMes.Enabled := False;
  edNumTTTransferidosParaAprendizMes.Enabled := False;
  edNumTTDesligadosMes.Enabled := False;
  edNumTTFinalMesManha.Enabled := False;
  edNumTTFinalMesTarde.Enabled := False;
  edNumTTFinalMesUnidade.Enabled := False;
  edNumTTSubtotal1SitMes.Enabled := False;
  edNumTTSubtotal2SitMes.Enabled := False;
  edNumTTTotalSitMes.Enabled := False;
  

end;

procedure TfrmResumoMensal.btnNovoClick(Sender: TObject);
begin

  //Antes de iniciar, verificar se existe algum Resumo PENDENTE...
  //Verificar se existe algum Resumo em Aberto e n�o permitir incluir...
  with dmDeca.cdsSel_ResumoMensal do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
    Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
    Params.ParamByName ('@pe_num_ano').Value := Null;
    Params.ParamByName ('@pe_num_mes').Value := Null;
    Params.ParamByName ('@pe_flg_situacao_resumo').Value := 0;
    Open;

    //Se n�o tiver Resumo em aberto...
    if RecordCount < 1 then
    begin

      AtualizaTela('N');
      GroupBox1.Enabled := False;
      GroupBox2.Enabled := False;

      PageControl1.ActivePageIndex := 1;//Clicar em NOVO vai para a p�gina Dados do Resumo...
      PageControl2.ActivePageIndex := 0;

      //Zerar os campos edit�veis e setar padr�o aos combos...

      //Habilitar Mes e Ano
      GroupBox2.Enabled := True;
      cbMes.Enabled := True;
      cbMes.ItemIndex := -1;
      mskAno.Enabled := True;
      mskAno.Clear;

      btnCalcularResumo.Enabled := True;

      cbMes.SetFocus;

      //Repassa os valores de Unidade, Gestor(a) e Assistente Social
      edPrograma.Text         := vvNOM_DIVISAO;
      edUnidade.Text          := vvNOM_UNIDADE;
      edGestor.Text           := vvNOM_GESTOR;
      edAssistenteSocial.Text := vvNOM_ASOCIAL;

      cbGrauAproveitamentoNoPrograma.ItemIndex := 0;
      cbGrauSatisfacaoCrianca.ItemIndex        := 0;
      cbGrauSatisfacaoFamilia.ItemIndex        := 0;
      cbGrauSatisfacaoConvenio.ItemIndex       := 0;

    end
    else
    begin
      Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                              'Ainda existe um Resumo n�o finalizado.' +#13+#10 +
                              'Voc� deve finaliz�-lo primeiro antes de criar um Novo Resumo.',
                              '[Sistema Deca] - Lan�amento de Resumo Mensal',
                              MB_OK + MB_ICONWARNING);
    end;
  end;
end;

procedure TfrmResumoMensal.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela('P');
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
end;

procedure TfrmResumoMensal.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if Key = #27 then
  begin
    Key := #0;
    frmResumoMensal.Close;
  end;
end;

procedure TfrmResumoMensal.edNumTTDefasagemAprendizagemExit(
  Sender: TObject);
begin
  PageControl2.ActivePageIndex := 1;
end;

procedure TfrmResumoMensal.edNumTTDesligadosOutrosMesExit(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 2;
end;

procedure TfrmResumoMensal.meDescricaoAtivSocioCulturaisExit(
  Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
end;

procedure TfrmResumoMensal.edNumTTAdolAprovadosVestibMesExit(
  Sender: TObject);
begin
  PageControl2.ActivePageIndex := 4;
end;

procedure TfrmResumoMensal.edNumTTParticipantesMesExit(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 5;
end;

procedure TfrmResumoMensal.edNumTTInicioMesManhaExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTInicioMes.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTInicioMesTarde.Text));

  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTInicioMesTardeExit(Sender: TObject);
begin
  //Calcula valores para a linha
  edNumTTInicioMes.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTInicioMesTarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTAdmitidosManhaExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTAdmitidosMes.Text := IntToStr(StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTAdmitidosTarde.Text));

  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTAdmitidosTardeExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTAdmitidosMes.Text := IntToStr(StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTAdmitidosTarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
  
end;

procedure TfrmResumoMensal.edNumTTTransferidosDEManhaExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosDEMes.Text := IntToStr(StrToInt(edNumTTTransferidosDeManha.Text) + StrToInt(edNumTTTransferidosDETarde.Text));

  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));


end;

procedure TfrmResumoMensal.edNumTTTransferidosDETardeExit(Sender: TObject);
begin
  //Calcula valores para a linha
  edNumTTTransferidosDEMes.Text := IntToStr(StrToInt(edNumTTTransferidosDeManha.Text) + StrToInt(edNumTTTransferidosDETarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTPetiManhaExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTPetiMes.Text := IntToStr(StrToInt(edNumTTPetiManha.Text) + StrToInt(edNumTTPetiTarde.Text));
  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTPetiTardeExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTPetiMes.Text := IntToStr(StrToInt(edNumTTPetiManha.Text) + StrToInt(edNumTTPetiTarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTTransferidosPARAManhaExit(
  Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosPARAMes.Text := IntToStr(StrToInt(edNumTTTransferidosPARAManha.Text) + StrToInt(edNumTTTransferidosPARATarde.Text));
  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTTransferidosParaArteEducManhaExit(
  Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosParaArteEducMes.Text := IntToStr(StrToInt(edNumTTTransferidosParaArteEducManha.Text) + StrToInt(edNumTTTransferidosParaArteEducTarde.Text));

  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTTransferidosParaAprendizManhaExit(
  Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosParaAprendizMes.Text := IntToStr(StrToInt(edNumTTTransferidosParaAprendizManha.Text) + StrToInt(edNumTTTransferidosParaAprendizTarde.Text));
  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) + StrToInt(edNumTTTransferidosParaArteEducManha.Text) +
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) + StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTDesligadosManhaExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTDesligadosMes.Text := IntToStr(StrToInt(edNumTTDesligadosManha.Text) + StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula os valores para a coluna per�odo manh�
  edNumTTFinalMesManha.Text := IntToStr(StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTTransferidosDEManha.Text) +
                               StrToInt(edNumTTPetiManha.Text) - StrToInt(edNumTTTransferidosPARAManha.Text) - StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizManha.Text) - StrToInt(edNumTTDesligadosManha.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTDesligadosTardeExit(Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTDesligadosMes.Text := IntToStr(StrToInt(edNumTTDesligadosManha.Text) + StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTTransferidosPARATardeExit(
  Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosPARAMes.Text := IntToStr(StrToInt(edNumTTTransferidosPARAManha.Text) + StrToInt(edNumTTTransferidosPARATarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTTransferidosParaArteEducTardeExit(
  Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosParaArteEducMes.Text := IntToStr(StrToInt(edNumTTTransferidosParaArteEducManha.Text) + StrToInt(edNumTTTransferidosParaArteEducTarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTTransferidosParaAprendizTardeExit(
  Sender: TObject);
begin
  //Calcula os valores para a linha
  edNumTTTransferidosParaAprendizMes.Text := IntToStr(StrToInt(edNumTTTransferidosParaAprendizManha.Text) + StrToInt(edNumTTTransferidosParaAprendizTarde.Text));

  //Calcula os valores para a coluna per�odo tarde
  edNumTTFinalMesTarde.Text := IntToStr(StrToInt(edNumTTInicioMesTarde.Text) + StrToInt(edNumTTAdmitidosTarde.Text) + StrToInt(edNumTTTransferidosDETarde.Text) +
                               StrToInt(edNumTTPetiTarde.Text) - StrToInt(edNumTTTransferidosPARATarde.Text) - StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                               StrToInt(edNumTTTransferidosParaAprendizTarde.Text) - StrToInt(edNumTTDesligadosTarde.Text));

  //Calcula valores totais para o campo TotalFinalMesUnidade
  edNumTTFinalMesUnidade.Text := IntToStr(StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text));
end;

procedure TfrmResumoMensal.edNumTTAfastadosExit(Sender: TObject);
begin
  edNumTTFinalMes.Text := IntToStr( StrToInt(edNumTTInicioMes.Text) +
                                    StrToInt(edNumTTPetiMes.Text)  -
                                    StrToInt(edNumTTDesligadosMes.Text) -
                                    StrToInt(edNumTTAfastados.Text)
                                    );
end;

procedure TfrmResumoMensal.edNumTTPresencasEsperadasMesEnter(
  Sender: TObject);
begin
  edNumTTPresencasEsperadasMes.Text := IntToStr(StrToInt(edNumTTFinalMes.Text) * StrToInt(edNumTTDiasAtividadesMes.Text));
end;

procedure TfrmResumoMensal.CalculaTotais;
begin

  //Calcular os valores dos totais do formul�rio

  //Calculando TOTAIS "por linha"
  edNumTTInicioMes.Text                   := IntToStr ( StrToInt(edNumTTInicioMesManha.Text) + StrToInt(edNumTTInicioMesTarde.Text) );
  edNumTTAdmitidosMes.Text                := IntToStr ( StrToInt(edNumTTAdmitidosManha.Text) + StrToInt(edNumTTAdmitidosTarde.Text) );
  edNumTTTransferidosDEMes.Text           := IntToStr ( StrToInt(edNumTTTransferidosDEManha.Text) + StrToInt(edNumTTTransferidosDETarde.Text) );
  edNumTTPetiMes.Text                     := IntToStr ( StrToInt(edNumTTPetiManha.Text) + StrToInt(edNumTTPetiTarde.Text) );
  edNumTTTransferidosPARAMes.Text         := IntToStr ( StrToInt(edNumTTTransferidosPARAManha.Text) + StrToInt(edNumTTTransferidosPARATarde.Text) );
  edNumTTTransferidosParaArteEducMes.Text := IntToStr ( StrToInt(edNumTTTransferidosParaArteEducManha.Text) + StrToInt(edNumTTTransferidosParaArteEducTarde.Text) );
  edNumTTTransferidosParaAprendizMes.Text := IntToStr ( StrToInt(edNumTTTransferidosParaAprendizManha.Text) + StrToInt(edNumTTTransferidosParaAprendizTarde.Text) );
  edNumTTDesligadosMes.Text               := IntToStr ( StrToInt(edNumTTDesligadosManha.Text) + StrToInt(edNumTTDesligadosTarde.Text) );

  //Calculando TOTAIS "por coluna"
  //Per�odo MANH�
  edNumTTFinalMesManha.Text               := IntToStr ( StrToInt(edNumTTInicioMesManha.Text) +
                                                        StrToInt(edNumTTAdmitidosManha.Text) +
                                                        StrToInt(edNumTTTransferidosDEManha.Text)+
                                                        StrToInt(edNumTTPetiManha.Text) -
                                                        StrToInt(edNumTTTransferidosPARAManha.Text) -
                                                        StrToInt(edNumTTTransferidosParaArteEducManha.Text) -
                                                        StrToInt(edNumTTTransferidosParaAprendizManha.Text) -
                                                        StrToInt(edNumTTDesligadosManha.Text) );

  //Per�odo TARDE
  edNumTTFinalMesTarde.Text               := IntToStr ( StrToInt(edNumTTInicioMesTarde.Text) +
                                                        StrToInt(edNumTTAdmitidosTarde.Text) +
                                                        StrToInt(edNumTTTransferidosDETarde.Text)+
                                                        StrToInt(edNumTTPetiTarde.Text) -
                                                        StrToInt(edNumTTTransferidosPARATarde.Text) -
                                                        StrToInt(edNumTTTransferidosParaArteEducTarde.Text) -
                                                        StrToInt(edNumTTTransferidosParaAprendizTarde.Text) -
                                                        StrToInt(edNumTTDesligadosTarde.Text) );

  //Calculando a coluna TOTAL
  edNumTTFinalMesUnidade.Text             := IntToStr ( StrToInt(edNumTTFinalMesManha.Text) + StrToInt(edNumTTFinalMesTarde.Text) );

  //FREQU�NCIA NA UNIDADE
  //Calculando: TOTAL NO IN�CIO DO M�S NA UNIDADE + TOTAL PETI MES - TOTAL DE DESLIGADOS MES - TOTAL DE AFASTADOS MES
  edNumTTFinalMes.Text                    := IntToStr ( StrToInt(edNumTTInicioMes.Text) +
                                                        StrToInt(edNumTTPetiMes.Text) -
                                                        StrToInt(edNumTTDesligadosMes.Text) -
                                                        StrToInt(edNumTTAfastados.Text)
                                                      );
  //Total de presen�as esperadas
  //Calculando: Total de crian�as/adolescentes no final do m�s * Total de dias de "atividades"
  edNumTTPresencasEsperadasMes.Text       := IntToStr ( StrToInt(edNumTTFinalMes.Text) * StrToInt(edNumTTDiasAtividadesMes.Text) );

  //TOTAL DE FALTAS INJUSTIFICADAS
  edNumTTFaltasInjustificadasMes.Text     := IntToStr ( StrToInt(edNumTTGeralFaltasMes.Text) - StrToInt(edNumTTFaltasJustificadasMes.Text) );

  //Calculando o Total de Desligamentos
  mmTOTAL_DESLIGADOS                      := ( StrToInt(edNumTTDesligadosAPedidoMes.Text) +
                                                        StrToInt(edNumTTDesligadosAbandonoMes.Text) +
                                                        StrToInt(edNumTTDesligadosMaioridadeMes.Text) +
                                                        StrToInt(edNumTTDesligadosLeiMes.Text) +
                                                        StrToInt(edNumTTDesligadosOutrosMes.Text)
                                             );
  if mmTOTAL_DESLIGADOS <> StrToInt(edNumTTDesligadosMes.Text) then
  begin

    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'DIFEREN�A entre valores  P�GINA 1 - TOTAL DE DESLIGADOS NO M�S ' + #13+#10 +
                            'e os valores lan�ados na P�GINA 2 - MOTIVOS DE DESLIGAMENTO. Favor conferir valores novamente.',
                            '[Sistema Deca] - Diferen�a de valores',
                            MB_OK + MB_ICONEXCLAMATION);
    edNumTTDesligadosAPedidoMes.SetFocus;
  end;

  //C�lculo para a Frequ�ncia da Unidade
  edNumTTFinalMes.Text :=  IntToStr(
                             StrToInt(edNumTTInicioMes.Text) +
                             StrToInt(edNumTTPetiMes.Text) -
                             StrToInt(edNumTTTransferidosPARAMes.Text) -
                             StrToInt(edNumTTTransferidosParaArteEducMes.Text) -
                             StrToInt(edNumTTTransferidosParaAprendizMes.Text) -
                             StrToInt(edNumTTDesligadosMes.Text) -
                             StrToInt(edNumTTAfastados.Text)
                           );

  //TOTAL DE FALTAS INJUSTIFICADAS
  edNumTTFaltasInjustificadasMes.Text     := IntToStr ( StrToInt(edNumTTGeralFaltasMes.Text) - StrToInt(edNumTTFaltasJustificadasMes.Text) );


end;

procedure TfrmResumoMensal.btnSairClick(Sender: TObject);
begin
  frmResumoMensal.Close;
end;

procedure TfrmResumoMensal.btnSalvarClick(Sender: TObject);
begin

  //Pede confirma��o do usu�rio...
  if Application.MessageBox ('Deseja incluir os dados do Resumo Mensal?',
                             '[Sistema Deca] - Resumo Mensal',
                             MB_YESNO + MB_ICONQUESTION) = id_Yes then
  begin

    //Verificar se o Per�odo informado para a unidade j� est� lan�ado...
    try
      with dmDeca.cdsSel_ResumoMensal do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_num_ano').Value := StrToInt(mskAno.Text);
        Params.ParamByName ('@pe_num_mes').Value := cbMes.ItemIndex + 1;
        Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
        Open;

        if RecordCount > 0 then
        begin
          Application.MessageBox ('Aten��o !!! ' + #13+#10 +
                                  'J� existe um lan�amento para a sua unidade no per�odo.' +#13+#10 +
                                  'Verifique o per�odo ou entre em contato com o Administrador ' + #13+#10 +
                                  'do Sistema.',
                                  '[Sistema Deca] - Lan�amento de Resumo Mensal',
                                  MB_OK + MB_ICONWARNING);
        end
        else
        //N�o encontrou registros referente ao per�odo/unidade... ent�o inclua...
        begin
          CalculaTotais;
          with dmDeca.cdsInc_ResumoMensal do
          begin
            Close;
            Params.ParamByName ('@pe_cod_unidade').Value                        := vvCOD_UNIDADE;  //Unidade de LOGIN do Usu�rio
            Params.ParamByName ('@pe_num_ano').Value                            := StrToInt(mskAno.Text);
            Params.ParamByName ('@pe_num_mes').Value                            := cbMes.ItemIndex + 1;
            Params.ParamByName ('@pe_num_tt_inicio_mes_manha').Value            := GetValue(edNumTTInicioMesManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_inicio_mes_tarde').Value            := GetValue(edNumTTInicioMesTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_admitidos_manha').Value             := GetValue(edNumTTAdmitidosManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_admitidos_tarde').Value             := GetValue(edNumTTAdmitidosTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_de_manha').Value             := GetValue(edNumTTTransferidosDEManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_de_tarde').Value             := GetValue(edNumTTTransferidosDETarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_manha').Value           := GetValue(edNumTTTransferidosPARAManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_tarde').Value           := GetValue(edNumTTTransferidosPARATarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_ae_manha').Value        := GetValue(edNumTTTransferidosParaArteEducManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_para_ae_tarde').Value        := GetValue(edNumTTTransferidosParaArteEducTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_aprendiz_manha').Value       := GetValue(edNumTTTransferidosParaAprendizManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_transf_aprendiz_tarde').Value       := GetValue(edNumTTTransferidosParaAprendizTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_peti_manha').Value                  := GetValue(edNumTTPetiManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_peti_tarde').Value                  := GetValue(edNumTTPetiTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_manha').Value            := GetValue(edNumTTDesligadosManha, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_tarde').Value            := GetValue(edNumTTDesligadosTarde, vtInteger);
            Params.ParamByName ('@pe_num_tt_afastados').Value                   := GetValue(edNumTTAfastados, vtInteger);
            Params.ParamByName ('@pe_num_tt_matriculados_escola').Value         := GetValue(edNumTTAlunosMatriculados, vtInteger);
            Params.ParamByName ('@pe_num_tt_fora_ensino_regular').Value         := GetValue(edNumTTForaEnsinoRegular, vtInteger);
            Params.ParamByName ('@pe_num_tt_defasagem_escolar').Value           := GetValue(edNumTTDefasagemEscolar, vtInteger);
            Params.ParamByName ('@pe_num_tt_freq_dentro_esperado').Value        := GetValue(edNumTTFrequenciaDentroEsperado, vtInteger);
            Params.ParamByName ('@pe_num_tt_defasagem_aprendizagem').Value      := GetValue(edNumTTDefasagemEscolar, vtInteger);
            Params.ParamByName ('@pe_num_tt_final_mes').Value                   := GetValue(edNumTTFinalMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_dias_atividades').Value             := GetValue(edNumTTDiasAtividadesMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_presencas_esperadas').Value         := GetValue(edNumTTPresencasEsperadasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_geral_faltas').Value                := GetValue(edNumTTGeralFaltasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_faltas_justificadas').Value         := GetValue(edNumTTFaltasJustificadasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_faltas_injustificadas').Value       := GetValue(edNumTTFaltasInjustificadasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_apedido').Value          := GetValue(edNumTTDesligadosAPedidoMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_abandono').Value         := GetValue(edNumTTDesligadosAbandonoMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_maioridade').Value       := GetValue(edNumTTDesligadosMaioridadeMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_lei').Value              := GetValue(edNumTTDesligadosLeiMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_desligados_outros').Value           := GetValue(edNumTTDesligadosOutrosMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_grau_aprov_programa').Value         := cbGrauAproveitamentoNoPrograma.ItemIndex;   //REVER VALOR A SER PREENCHIDO COM A MARILDA
            Params.ParamByName ('@pe_num_tt_situacao_disciplinar').Value        := GetValue(edNumTTSitDisciplinarMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_situacao_socioeconomica').Value     := GetValue(edNumTTSitSocioEconomicaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_situacao_escolar').Value            := GetValue(edNumTTSitEscolarMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_violencia').Value                   := GetValue(edNumTTSitViolenciaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_abrigo').Value                      := GetValue(edNumTTSitAbrigoMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_interacao_relacional').Value        := GetValue(edNumTTSitInteracaoRelacMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_ato_infracional').Value             := GetValue(edNumTTSitAtoInfracionalMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprendizagem').Value                := GetValue(edNumTTSitAprendizagemMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_saude').Value                       := GetValue(edNumTTSitSaudeMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_guarda_irregular').Value            := GetValue(edNumTTSitGuardaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_gravidez').Value                    := GetValue(edNumTTSitGravidezMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_moradia').Value                     := GetValue(edNumTTSitMoradiaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_trab_infantil').Value               := GetValue(edNumTTSitTrabInfantilMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_ativ_socioculturais').Value         := GetValue(edNumTTAtivSocioCultMes, vtInteger);
            Params.ParamByName ('@pe_dsc_ativ_socioculturais').Value            := GetValue(meDescricaoAtivSocioCulturais.Text);
            Params.ParamByName ('@pe_num_tt_ativ_recreativas').Value            := GetValue(edNumTTAtivRecreativasMes, vtInteger);
            Params.ParamByName ('@pe_dsc_ativ_recreativas').Value               := GetValue(meDescricaoAtivRecreativasMes.Text);
            Params.ParamByName ('@pe_num_tt_acoes_sociais').Value               := GetValue(edNumTTAcoesSociaisMes, vtInteger);
            Params.ParamByName ('@pe_dsc_acoes_sociais').Value                  := GetValue(meDescricaoAtivSociaisMes.Text);
            Params.ParamByName ('@pe_num_tt_inscritos_vestibulinho').Value      := GetValue(edNumTTAdolInscritosVestibMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_vestibulinho').Value      := GetValue(edNumTTAdolAprovadosVestibMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aptos_aprendizagem').Value          := GetValue(edNumTTAdolAptosAprendizagemProfissionalMes, vtInteger);
            Params.ParamByName ('@pe_num_adol_encaminhados').Value              := GetValue(edNumTTAdolEncaminhadosAprendizagemProfissionalMes, vtInteger);
            Params.ParamByName ('@pe_num_adol_inseridos').Value                 := GetValue(edNumTTAdolInseridosAprendizagemProfissionalMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_concluintes_op').Value              := GetValue(edNumTTAdolOPMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_concluintes_oe').Value              := GetValue(edNumTTAdolOEMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_concluintes_fic1').Value            := GetValue(edNumTTAdolFIC1Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_concluintes_fic2').Value            := GetValue(edNumTTAdolFIC2Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_concluintes_form_tecnica').Value    := GetValue(edNumTTAdolFormacaoTecnicaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_op').Value                := GetValue(edNumTTAdolAprovadosOPMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_oe').Value                := GetValue(edNumTTAdolAprovadosOEMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_fic1').Value              := GetValue(edNumTTAdolAprovadosFIC1Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_fic2').Value              := GetValue(edNumTTAdolAprovadosFIC2Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_aprovados_form_tecnica').Value      := GetValue(edNumTTAdolAprovadosFormacaoTecnicaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_op').Value               := GetValue(edNumTTAdolReprovadosOPMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_oe').Value               := GetValue(edNumTTAdolReprovadosOEMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_fic1').Value             := GetValue(edNumTTAdolReprovadosFIC1Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_fic2').Value             := GetValue(edNumTTAdolReprovadosFIC2Mes, vtInteger);
            Params.ParamByName ('@pe_num_tt_reprovados_form_tecnica').Value     := GetValue(edNumTTAdolReprovadosFormacaoTecnicaMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_acoes_familias').Value              := GetValue(edNumTTAcoesComFamiliasMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_convidados').Value                  := GetValue(edNumTTConvidadosMes, vtInteger);
            Params.ParamByName ('@pe_num_tt_participantes').Value               := GetValue(edNumTTParticipantesMes, vtInteger);
            Params.ParamByName ('@pe_grau_satisf_criadol1').Value               := cbGrauSatisfacaoCrianca.ItemIndex;
            Params.ParamByName ('@pe_grau_satisf_criadol2').Value               := Null;
            Params.ParamByName ('@pe_grau_satisf_criadol3').Value               := Null;
            Params.ParamByName ('@pe_grau_satisf_familia').Value                := cbGrauSatisfacaoFamilia.ItemIndex;
            Params.ParamByName ('@pe_grau_satisf_empresas').Value               := cbGrauSatisfacaoConvenio.ItemIndex;
            Params.ParamByName ('@pe_cod_usuario_ult_alteracao').Value          := vvCOD_USUARIO;
            Params.ParamByName ('@pe_flg_situacao_resumo').Value                := 0;
            Execute;

            Application.MessageBox ('Os dados do "Resumo Mensal" foram inseridos com sucesso !!!' + #13+#10 +
                                    'Agora, at� o per�odo permitido, voc� ainda pode ' + #13+#10 +
                                    'fazer altera��es. Ap�s o per�odo permitido as ' + #13+#10 +
                                    'as altera��es n�o ser�o mais permitidas.',
                                    '[Sistema Deca] - Dados do Resumo Mensal inseridos com sucesso.',
                                    MB_OK + MB_ICONINFORMATION);

            //Volta a p�gina principal(tela)
            //Atualiza os dados do grid...
            AtualizaTela('P');
            LimpaDesabilitaTudo;
            PageControl1.ActivePageIndex := 0;

            try
            with dmDeca.cdsSel_ResumoMensal do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
              Open;
              dbgResumos.Refresh;

              //Repassa os valores de Unidade, Gestor(a) e Assistente Social
              edPrograma.Text         := vvNOM_DIVISAO;
              edUnidade.Text          := vvNOM_UNIDADE;
              edGestor.Text           := vvNOM_GESTOR;
              edAssistenteSocial.Text := vvNOM_ASOCIAL;
            end;
            except
              Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                      'Um erro ocorreu ao tentar carregar os dados dos Resumos ' +#13+#10 +
                                      'Mensais da unidade. Entre em contato com o Administrador.',
                                      '[Sistema Deca] - Erro carregando Resumo Mensal...',
                                      MB_OK + MB_ICONWARNING);
            end
          end;
        end;
      end;
    except
      //Aten��o... Um erro ocorreu no acesso ao banco de dados
      //verifique a sua conex�o de rede/internet ou entre em contato
      //com o administrador do sistema...
    end;

  end;

end;


procedure TfrmResumoMensal.btnCalcularResumoClick(Sender: TObject);
begin
  //if Application.MessageBox ('Deseja realizar o c�lculo dos dados lan�ados no resumo atual?',
  //                           '[Sistema Deca] - Totaliza��o',
  //                           MB_YESNO + MB_ICONQUESTION) = id_yes then
  //begin
  //  CalculaTotais;
  //  //btnSalvar.Enabled := True;
  //  btnCalcularResumo.Enabled := True;
  //end;
  
end;

procedure TfrmResumoMensal.dbgResumosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var mmIcon : TBitMap;
begin
  mmIcon := TBitMap.Create();
  if (Column.FieldName = 'vSituacaoResumo') then
  begin
    with dbgResumos.Canvas do
    begin

      Brush.Color := clWhite;
      FillRect(Rect);

      if (dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 0) then
        ImageList1.GetBitmap(0, mmIcon)
      else if (dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1) then
       ImageList1.GetBitmap(1, mmIcon);
      Draw(round((Rect.Left + Rect.Right - Icon.Width) / 2), Rect.Top, mmIcon);
    end;
    
  end;
end;

procedure TfrmResumoMensal.cbMesExit(Sender: TObject);
begin
  if cbMes.ItemIndex < 0 then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'O M�S deve ser informado para o lan�amento dos ' + #13+#10 +
                            'dados do Resumo Mensal. Favor selecionar o M�S.' ,
                            '[Sistema Deca] - Informe o M�S...',
                            MB_OK + MB_ICONWARNING);
    cbMes.SetFocus;
  end;
end;

procedure TfrmResumoMensal.mskAnoExit(Sender: TObject);
begin
  if (Length(Trim(mskAno.Text)) < 4) or (mskAno.Text = '') then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'O ANO deve ser informado para o lan�amento dos ' + #13+#10 +
                            'dados do Resumo Mensal. Favor informar o ANO.' ,
                            '[Sistema Deca] - Informe o ANO...',
                            MB_OK + MB_ICONWARNING);
    mskAno.SetFocus;
  end;
end;

procedure TfrmResumoMensal.btnImprimirClick(Sender: TObject);
begin
  //Ao emitir o relat�rio, "anexar" o gr�fico biom�trico com os dados atuais
  //dos adolescentes da unidade atual.
  Application.CreateForm(TrelResumoMensal, relResumoMensal);
  relResumoMensal.qrlPrograma.Caption  := edPrograma.Text;
  relResumoMensal.qrlPeriodo.Caption   := dmDeca.cdsSel_ResumoMensal.FieldByName('vMes').Value + '/' + IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName('num_ano').Value);
  relResumoMensal.qrlUnidade.Caption   := edUnidade.Text;
  relResumoMensal.qrlGestor.Caption    := 'Gestor(a): ' + edGestor.Text + '  -  Ass. Social: ' + edAssistenteSocial.Text;
  relResumoMensal.qrlSituacao.Caption  := dmDeca.cdsSel_ResumoMensal.FieldByName('vSituacaoResumo').Value;

  //Repassa os campos de totais
  //Verificar com Marilda as f�rmulas para c�lculo dos totais....







  relResumoMensal.Preview;
  relResumoMensal.Free;

  Application.CreateForm(TrelResumoMensal2, relResumoMensal2);
  relResumoMensal2.qrlPrograma.Caption  := edPrograma.Text;
  relResumoMensal2.qrlPeriodo.Caption   := dmDeca.cdsSel_ResumoMensal.FieldByName('vMes').Value + '/' + IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName('num_ano').Value);
  relResumoMensal2.qrlUnidade.Caption   := edUnidade.Text;
  relResumoMensal2.qrlGestor.Caption    := 'Gestor(a): ' + edGestor.Text + '  -  Ass. Social: ' + edAssistenteSocial.Text;
  relResumoMensal2.qrlSituacao.Caption  := dmDeca.cdsSel_ResumoMensal.FieldByName('vSituacaoResumo').Value;
  relResumoMensal2.Preview;
  relResumoMensal2.Free;

end;

procedure TfrmResumoMensal.dbgResumosDblClick(Sender: TObject);
begin

  //Verificar se o Resumo Mensal j� est� encerrado...
  //Caso esteja, n�o � poss�vel fazer qualquer altera��o...

  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1 then
  begin
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'O este Resumo Mensal j� encontra-se' + #13+#10 +
                            'FINALIZADO e n�o pode ser alterado..' ,
                            '[Sistema Deca] - Resumo Mensal...',
                            MB_OK + MB_ICONWARNING);
  end
  else
  begin

    //Habilita os campos para que os valores sejam repassados
    HabilitaCampos;
    btnNovo.Enabled     := False;
    btnAlterar.Enabled  := True;
    btnCancelar.Enabled := True;
    mskAno.Enabled      := True;

    //Repassa os valores da tabela para os campos, habilita o bot�o alterar...
    PageControl1.ActivePageIndex := 1;
        
    //Repassa os valores de Unidade, Gestor(a) e Assistente Social
    edPrograma.Text         := vvNOM_DIVISAO;
    edUnidade.Text          := vvNOM_UNIDADE;
    edGestor.Text           := vvNOM_GESTOR;
    edAssistenteSocial.Text := vvNOM_ASOCIAL;

    //M�s/Ano
    cbMes.ItemIndex                           := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_mes').Value - 1;
    mskAno.Text                               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_ano').Value);

    //Valores da P�gina 1/6
    edNumTTInicioMesManha.Text                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_inicio_mes_manha').Value);
    edNumTTInicioMesTarde.Text                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_inicio_mes_tarde').Value);
    edNumTTAdmitidosManha.Text                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_admitidos_manha').Value);
    edNumTTAdmitidosTarde.Text                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_admitidos_tarde').Value);
    edNumTTTransferidosDEManha.Text           := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_de_manha').Value);
    ednumTTTransferidosDETarde.Text           := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_de_tarde').Value);
    edNumTTPetiManha.Text                     := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_peti_manha').Value);
    edNumTTPetiTarde.Text                     := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_peti_tarde').Value);
    edNumTTTransferidosPARAManha.Text         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_manha').Value);
    edNumTTTransferidosPARATarde.Text         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_tarde').Value);
    edNumTTTransferidosParaArteEducManha.Text := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_ae_manha').Value);
    edNumTTTransferidosParaArteEducTarde.Text := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_para_ae_tarde').Value);
    edNumTTTransferidosParaAprendizManha.Text := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_aprendiz_manha').Value);
    edNumTTTransferidosParaAprendizTarde.Text := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_transf_aprendiz_tarde').Value);
    edNumTTDesligadosManha.Text               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_manha').Value);
    edNumTTDesligadosTarde.Text               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_tarde').Value);
    edNumTTAfastados.Text                     := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_afastados').Value);
    edNumTTAlunosMatriculados.Text            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_matriculados_escola').Value);
    edNumTTForaEnsinoRegular.Text             := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_fora_ensino_regular').Value);
    edNumTTDefasagemEscolar.Text              := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_defasagem_escolar').Value);
    edNumTTFrequenciaDentroEsperado.Text      := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_freq_dentro_esperado').Value);
    edNumTTDefasagemAprendizagem.Text         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_defasagem_aprendizagem').Value);

    //Valores da P�gina 2/6
    edNumTTFinalMes.Text                      := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_final_mes').Value);
    edNumTTDiasAtividadesMes.Text             := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_dias_atividades').Value);
    edNumTTPresencasEsperadasMes.Text         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_presencas_esperadas').Value);
    edNumTTGeralFaltasMes.Text                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_geral_faltas').Value);
    edNumTTFaltasJustificadasMes.Text         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_faltas_justificadas').Value);
    edNumTTFaltasInjustificadasMes.Text       := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_faltas_injustificadas').Value);
    edNumTTDesligadosAPedidoMes.Text          := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_apedido').Value);
    edNumTTDesligadosAbandonoMes.Text         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_abandono').Value);
    edNumTTDesligadosMaioridadeMes.Text       := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_maioridade').Value);
    edNumTTDesligadosLeiMes.Text              := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_lei').Value);
    edNumTTDesligadosOutrosMes.Text           := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_desligados_outros').Value);
    cbGrauAproveitamentoNoPrograma.ItemIndex  := dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_grau_aprov_programa').Value + 1;

    //Valores da P�gina 3/6
    edNumTTSitDisciplinarMes.Text             := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_situacao_disciplinar').Value);
    edNumTTSitSocioEconomicaMes.Text          := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_situacao_socioeconomica').Value);
    edNumTTSitEscolarMes.Text                 := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_situacao_escolar').Value);
    edNumTTSitViolenciaMes.Text               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_violencia').Value);
    edNumTTSitAbrigoMes.Text                  := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_abrigo').Value);
    edNumTTSitInteracaoRelacMes.Text          := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_interacao_relacional').Value);
    edNumTTSitAtoInfracionalMes.Text          := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_ato_infracional').Value);
    edNumTTSitAprendizagemMes.Text            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprendizagem').Value);
    edNumTTSitSaudeMes.Text                   := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_saude').Value);
    edNumTTSitGuardaMes.Text                  := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_guarda_irregular').Value);
    edNumTTSitGravidezMes.Text                := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_gravidez').Value);
    edNumTTSitMoradiaMes.Text                 := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_moradia').Value);
    edNumTTSitTrabInfantilMes.Text            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_trab_infantil').Value);
    edNumTTAtivSocioCultMes.Text              := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_ativ_socioculturais').Value);
    meDescricaoAtivSocioCulturais.Text        := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_ativ_socioculturais').Value);

    //Valores da P�gina 4/6
    edNumTTAtivRecreativasMes.Text            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_ativ_recreativas').Value);
    meDescricaoAtivRecreativasMes.Text        := dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_ativ_recreativas').Value;
    edNumTTAcoesSociaisMes.Text               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_acoes_sociais').Value);
    meDescricaoAtivSociaisMes.Text            := dmDeca.cdsSel_ResumoMensal.FieldByName ('dsc_acoes_sociais').Value;
    edNumTTAdolInscritosVestibMes.Text        := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_inscritos_vestibulinho').Value);
    edNumTTAdolAprovadosVestibMes.Text        := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_vestibulinho').Value);

    //Valores da P�gina 5/6
    edNumTTAdolAptosAprendizagemProfissionalMes.Text        := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aptos_aprendizagem').Value);
    edNumTTAdolEncaminhadosAprendizagemProfissionalMes.Text := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_adol_encaminhados').Value);
    edNumTTAdolInseridosAprendizagemProfissionalMes.Text    := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_adol_inseridos').Value);
    edNumTTAdolOPMes.Text                                   := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_concluintes_op').Value);
    edNumTTAdolAprovadosOPMes.Text                          := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_op').Value);
    edNumTTAdolReprovadosOPMes.Text                         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_op').Value);
    edNumTTAdolOEMes.Text                                   := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_concluintes_oe').Value);
    edNumTTAdolAprovadosOEMes.Text                          := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_oe').Value);
    edNumTTAdolReprovadosOEMes.Text                         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_oe').Value);
    edNumTTAdolFIC1Mes.Text                                 := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_concluintes_fic1').Value);
    edNumTTAdolAprovadosFIC1Mes.Text                        := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_fic1').Value);
    edNumTTAdolReprovadosFIC1Mes.Text                       := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_fic1').Value);
    edNumTTAdolFIC2Mes.Text                                 := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_concluintes_fic2').Value);
    edNumTTAdolAprovadosFIC2Mes.Text                        := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_fic2').Value);
    edNumTTAdolReprovadosFIC2Mes.Text                       := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_fic2').Value);
    edNumTTAdolFormacaoTecnicaMes.Text                      := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_concluintes_form_tecnica').Value);
    edNumTTAdolAprovadosFormacaoTecnicaMes.Text             := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_aprovados_form_tecnica').Value);
    edNumTTAdolReprovadosFormacaoTecnicaMes.Text            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_reprovados_form_tecnica').Value);
    edNumTTAcoesComFamiliasMes.Text                         := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_acoes_familias').Value);
    edNumTTConvidadosMes.Text                               := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_convidados').Value);
    edNumTTParticipantesMes.Text                            := IntToStr(dmDeca.cdsSel_ResumoMensal.FieldByName ('num_tt_participantes').Value);

    //Valores da P�gina 6/6
    cbGrauSatisfacaoCrianca.ItemIndex                       := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_criadol1').Value + 1;
    cbGrauSatisfacaoFamilia.ItemIndex                       := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_familia').Value + 1;
    cbGrauSatisfacaoConvenio.ItemIndex                      := dmDeca.cdsSel_ResumoMensal.FieldByName ('grau_satisf_empresas').Value + 1;

    //Totais ser�o exibidos ap�s ser executado o procedimento CalculaTotais...
    CalculaTotais;

    Application.MessageBox ('Aten��o!!!' +#13+#10 +
                            'Os dados foram repassados aos devidos campos.' +#13+#10 +
                            'Caso necessite, fa�a as devidas altera��es e clique em ALTERAR',
                            '[Sistema Deca] - Altera��o de Dados Resumo Mensal',
                            MB_OK + MB_ICONINFORMATION);
  end;


end;

procedure TfrmResumoMensal.btnEncerrarResumoMesClick(Sender: TObject);
begin
  //Se o resumo estiver "N�O FINALIZADO", ent�o finaliza...
  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 0 then
  begin
    //Pede confirma��o para Finalizar o Resumo Mensal
      if Application.MessageBox('Deseja FINALIZAR o Resumo Mensal selecionado?',
                                '[Sistema Deca] - Finalizar Resumo Mensal',
                                MB_YESNO + MB_ICONQUESTION) = id_yes then
      begin

        try
          with dmDeca.cdsAlt_Encerra_Reverte_Resumo do
          begin
            Close;
            Params.ParamByName ('@pe_tipo').Value := 1;
            Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
            Params.ParamByName ('@pe_cod_id_resumomensal').Value := dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;
            Execute;

            with dmDeca.cdsSel_ResumoMensal do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
              Open;
              dbgResumos.Refresh;
            end;
          end;
        except
        end;
      end;
  end;
end;

procedure TfrmResumoMensal.btnReverterResumoMesClick(Sender: TObject);
begin
  //Se o resumo estiver "FINALIZADO", ent�o pode ser revertido...
  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 1 then
  begin
    //Pede confirma��o para Finalizar o Resumo Mensal
    if Application.MessageBox('Deseja REVERTER o Resumo Mensal selecionado?',
                              '[Sistema Deca] - Finalizar Resumo Mensal',
                              MB_YESNO + MB_ICONQUESTION) = id_yes then
    begin

      //Verificar se existe algum Resumo Mensal N�O FINALIZADO...
      //Caso exista n�o deixar REVERTER...
      with dmDeca.cdsSel_ResumoMensal do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_num_ano').Value := Null;
        Params.ParamByName ('@pe_num_mes').Value := Null;
        Params.ParamByName ('@pe_flg_situacao_resumo').Value := 0;
        Open;

        //Se n�o tiver Resumo em aberto...
        if dmDeca.cdsSel_ResumoMensal.RecordCount < 1 then
        begin

          try
          with dmDeca.cdsAlt_Encerra_Reverte_Resumo do
          begin
            Close;
            Params.ParamByName ('@pe_tipo').Value := 2;
            Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
            Params.ParamByName ('@pe_cod_id_resumomensal').Value := vvCOD_ID_RESUMOMENSAL; //dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;
            Execute;

            with dmDeca.cdsSel_ResumoMensal do
            begin
              Close;
              Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
              Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
              Open;
              dbgResumos.Refresh;
            end;
          end;
          except end;
        end
        else
        begin
          Application.MessageBox ('Aten��o!!!' +#13+#10 +
                                  'Para REVERTER esse Resumo, voc� deve FINALIZAR.' +#13+#10 +
                                  'os Resumos ainda N�O FINALIZADOS. Verifique e tente mais tarde.',
                                  '[Sistema Deca] - Revers�o de Resumo Mensal',
                                  MB_OK + MB_ICONINFORMATION);

         with dmDeca.cdsSel_ResumoMensal do
         begin
           Close;
           Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
           Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
           Params.ParamByName ('@pe_num_ano').Value := Null;
           Params.ParamByName ('@pe_num_mes').Value := Null;
           Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
           Open;
           dbgResumos.Refresh;
         end;

        end;
    end;
  end;
end;
end;

procedure TfrmResumoMensal.dbgResumosCellClick(Column: TColumn);
begin

  //Recupera COD_ID_RESUMOMENSAL para utilizar em Encerrar/Reverter
  vvCOD_ID_RESUMOMENSAL := dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;

  //Se estiver N�O FINALIZADO habilita o bot�o ENCERRAR RESUMO e desabilita O REVERTER...
  if dmDeca.cdsSel_ResumoMensal.FieldByName ('flg_situacao_resumo').Value = 0 then
  begin
    btnEncerrarResumoMes.Enabled := True;
    btnReverterResumoMes.Enabled := False
  end
  else
  begin
    btnEncerrarResumoMes.Enabled := False;
    btnReverterResumoMes.Enabled := True
  end
end;

procedure TfrmResumoMensal.btnAlterarClick(Sender: TObject);
begin

  try

    CalculaTotais;

    with dmDeca.cdsAlt_ResumoMensal do
    begin

      Close;
      Params.ParamByName ('@pe_cod_id_resumomensal').Value                := dmDeca.cdsSel_ResumoMensal.FieldByName ('cod_id_resumomensal').Value;
      Params.ParamByName ('@pe_num_tt_inicio_mes_manha').Value            := GetValue(edNumTTInicioMesManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_inicio_mes_tarde').Value            := GetValue(edNumTTInicioMesTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_admitidos_manha').Value             := GetValue(edNumTTAdmitidosManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_admitidos_tarde').Value             := GetValue(edNumTTAdmitidosTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_de_manha').Value             := GetValue(edNumTTTransferidosDEManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_de_tarde').Value             := GetValue(edNumTTTransferidosDETarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_manha').Value           := GetValue(edNumTTTransferidosPARAManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_tarde').Value           := GetValue(edNumTTTransferidosPARATarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_transf_para_ae_manha').Value        := GetValue(edNumTTTransferidosParaArteEducManha.Text);
      Params.ParamByName ('@pe_num_tt_transf_para_ae_tarde').Value        := GetValue(edNumTTTransferidosParaArteEducTarde.Text);
      Params.ParamByName ('@pe_num_tt_transf_aprendiz_manha').Value       := GetValue(edNumTTTransferidosParaAprendizManha.Text);
      Params.ParamByName ('@pe_num_tt_transf_aprendiz_tarde').Value       := GetValue(edNumTTTransferidosParaAprendizTarde.Text);
      Params.ParamByName ('@pe_num_tt_peti_manha').Value                  := GetValue(edNumTTPetiManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_peti_tarde').Value                  := GetValue(edNumTTPetiTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_manha').Value            := GetValue(edNumTTDesligadosManha, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_tarde').Value            := GetValue(edNumTTDesligadosTarde, vtInteger);
      Params.ParamByName ('@pe_num_tt_afastados').Value                   := GetValue(edNumTTAfastados, vtInteger);
      Params.ParamByName ('@pe_num_tt_matriculados_escola').Value         := GetValue(edNumTTAlunosMatriculados, vtInteger);
      Params.ParamByName ('@pe_num_tt_fora_ensino_regular').Value         := GetValue(edNumTTForaEnsinoRegular, vtInteger);
      Params.ParamByName ('@pe_num_tt_defasagem_escolar').Value           := GetValue(edNumTTDefasagemEscolar, vtInteger);
      Params.ParamByName ('@pe_num_tt_freq_dentro_esperado').Value        := GetValue(edNumTTFrequenciaDentroEsperado, vtInteger);
      Params.ParamByName ('@pe_num_tt_defasagem_aprendizagem').Value      := GetValue(edNumTTDefasagemEscolar, vtInteger);
      Params.ParamByName ('@pe_num_tt_final_mes').Value                   := GetValue(edNumTTFinalMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_dias_atividades').Value             := GetValue(edNumTTDiasAtividadesMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_presencas_esperadas').Value         := GetValue(edNumTTPresencasEsperadasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_geral_faltas').Value                := GetValue(edNumTTGeralFaltasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_faltas_justificadas').Value         := GetValue(edNumTTFaltasJustificadasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_faltas_injustificadas').Value       := GetValue(edNumTTFaltasInjustificadasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_apedido').Value          := GetValue(edNumTTDesligadosAPedidoMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_abandono').Value         := GetValue(edNumTTDesligadosAbandonoMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_maioridade').Value       := GetValue(edNumTTDesligadosMaioridadeMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_lei').Value              := GetValue(edNumTTDesligadosLeiMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_desligados_outros').Value           := GetValue(edNumTTDesligadosOutrosMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_grau_aprov_programa').Value         := cbGrauAproveitamentoNoPrograma.ItemIndex;   //REVER VALOR A SER PREENCHIDO COM A MARILDA
      Params.ParamByName ('@pe_num_tt_situacao_disciplinar').Value        := GetValue(edNumTTSitDisciplinarMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_situacao_socioeconomica').Value     := GetValue(edNumTTSitSocioEconomicaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_situacao_escolar').Value            := GetValue(edNumTTSitEscolarMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_violencia').Value                   := GetValue(edNumTTSitViolenciaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_abrigo').Value                      := GetValue(edNumTTSitAbrigoMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_interacao_relacional').Value        := GetValue(edNumTTSitInteracaoRelacMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_ato_infracional').Value             := GetValue(edNumTTSitAtoInfracionalMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprendizagem').Value                := GetValue(edNumTTSitAprendizagemMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_saude').Value                       := GetValue(edNumTTSitSaudeMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_guarda_irregular').Value            := GetValue(edNumTTSitGuardaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_gravidez').Value                    := GetValue(edNumTTSitGravidezMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_moradia').Value                     := GetValue(edNumTTSitMoradiaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_trab_infantil').Value               := GetValue(edNumTTSitTrabInfantilMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_ativ_socioculturais').Value         := GetValue(edNumTTAtivSocioCultMes, vtInteger);
      Params.ParamByName ('@pe_dsc_ativ_socioculturais').Value            := GetValue(meDescricaoAtivSocioCulturais.Text);
      Params.ParamByName ('@pe_num_tt_ativ_recreativas').Value            := GetValue(edNumTTAtivRecreativasMes, vtInteger);
      Params.ParamByName ('@pe_dsc_ativ_recreativas').Value               := GetValue(meDescricaoAtivRecreativasMes.Text);
      Params.ParamByName ('@pe_num_tt_acoes_sociais').Value               := GetValue(edNumTTAcoesSociaisMes, vtInteger);
      Params.ParamByName ('@pe_dsc_acoes_sociais').Value                  := GetValue(meDescricaoAtivSociaisMes.Text);
      Params.ParamByName ('@pe_num_tt_inscritos_vestibulinho').Value      := GetValue(edNumTTAdolInscritosVestibMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_vestibulinho').Value      := GetValue(edNumTTAdolAprovadosVestibMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aptos_aprendizagem').Value          := GetValue(edNumTTAdolAptosAprendizagemProfissionalMes, vtInteger);
      Params.ParamByName ('@pe_num_adol_encaminhados').Value              := GetValue(edNumTTAdolEncaminhadosAprendizagemProfissionalMes, vtInteger);
      Params.ParamByName ('@pe_num_adol_inseridos').Value                 := GetValue(edNumTTAdolInseridosAprendizagemProfissionalMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_concluintes_op').Value              := GetValue(edNumTTAdolOPMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_concluintes_oe').Value              := GetValue(edNumTTAdolOEMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_concluintes_fic1').Value            := GetValue(edNumTTAdolFIC1Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_concluintes_fic2').Value            := GetValue(edNumTTAdolFIC2Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_concluintes_form_tecnica').Value    := GetValue(edNumTTAdolFormacaoTecnicaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_op').Value                := GetValue(edNumTTAdolAprovadosOPMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_oe').Value                := GetValue(edNumTTAdolAprovadosOEMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_fic1').Value              := GetValue(edNumTTAdolAprovadosFIC1Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_fic2').Value              := GetValue(edNumTTAdolAprovadosFIC2Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_aprovados_form_tecnica').Value      := GetValue(edNumTTAdolAprovadosFormacaoTecnicaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_op').Value               := GetValue(edNumTTAdolReprovadosOPMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_oe').Value               := GetValue(edNumTTAdolReprovadosOEMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_fic1').Value             := GetValue(edNumTTAdolReprovadosFIC1Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_fic2').Value             := GetValue(edNumTTAdolReprovadosFIC2Mes, vtInteger);
      Params.ParamByName ('@pe_num_tt_reprovados_form_tecnica').Value     := GetValue(edNumTTAdolReprovadosFormacaoTecnicaMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_acoes_familias').Value              := GetValue(edNumTTAcoesComFamiliasMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_convidados').Value                  := GetValue(edNumTTConvidadosMes, vtInteger);
      Params.ParamByName ('@pe_num_tt_participantes').Value               := GetValue(edNumTTParticipantesMes, vtInteger);
      Params.ParamByName ('@pe_grau_satisf_criadol1').Value               := cbGrauSatisfacaoCrianca.ItemIndex;
      Params.ParamByName ('@pe_grau_satisf_familia').Value                := cbGrauSatisfacaoFamilia.ItemIndex;
      Params.ParamByName ('@pe_grau_satisf_empresas').Value               := cbGrauSatisfacaoConvenio.ItemIndex;
      Params.ParamByName ('@pe_cod_usuario_ult_alteracao').Value          := vvCOD_USUARIO;
      Execute;

      Application.MessageBox ('Os dados do "Resumo Mensal" foram alterados com sucesso !!!' + #13+#10 +
                              'Agora, at� o per�odo permitido, voc� ainda pode ' + #13+#10 +
                              'fazer altera��es. Ap�s o per�odo permitido as ' + #13+#10 +
                              'as altera��es n�o ser�o mais permitidas.',
                              '[Sistema Deca] - Dados do Resumo Mensal alterados com sucesso.',
                              MB_OK + MB_ICONINFORMATION);

      //Volta a p�gina principal(tela)
      //Atualiza os dados do grid...
      LimpaDesabilitaTudo;
      AtualizaTela('P');
      PageControl1.ActivePageIndex := 0;

      try
      with dmDeca.cdsSel_ResumoMensal do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_resumomensal').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_num_ano').Value := Null;
        Params.ParamByName ('@pe_num_mes').Value := Null;
        Params.ParamByName ('@pe_flg_situacao_resumo').Value := Null;
        Open;
        dbgResumos.Refresh;

        //Repassa os valores de Unidade, Gestor(a) e Assistente Social
        edPrograma.Text         := vvNOM_DIVISAO;
        edUnidade.Text          := vvNOM_UNIDADE;
        edGestor.Text           := vvNOM_GESTOR;
        edAssistenteSocial.Text := vvNOM_ASOCIAL;
      end;
      except
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'Um erro ocorreu ao tentar carregar os dados dos Resumos ' +#13+#10 +
                                'Mensais da unidade. Entre em contato com o Administrador.',
                                '[Sistema Deca] - Erro carregando Resumo Mensal...',
                                MB_OK + MB_ICONWARNING);
      end

    end;

  except
    Application.MessageBox ('Aten��o!!!' + #13+#10 +
                            'Um erro ocorreu ao tentar alterar os dados do Resumo Mensal.' + #13+#10 +
                            'Favor tentar novamente ou entre em contato com o Administrador dos Sistema.',
                            '[Sistema Deca] - Erro Alterando Resumo',
                            MB_OK + MB_ICONWARNING);
  end;


end;

procedure TfrmResumoMensal.edNumTTFinalMesEnter(Sender: TObject);
begin
  edNumTTFinalMes.Text :=  IntToStr(

                             StrToInt(edNumTTInicioMes.Text) +
                             StrToInt(edNumTTPetiMes.Text) -
                             StrToInt(edNumTTTransferidosPARAMes.Text) -
                             StrToInt(edNumTTTransferidosParaArteEducMes.Text) -
                             StrToInt(edNumTTTransferidosParaAprendizMes.Text) -
                             StrToInt(edNumTTDesligadosMes.Text) -
                             StrToInt(edNumTTAfastados.Text)
                           );

end;

procedure TfrmResumoMensal.edNumTTGeralFaltasMesEnter(Sender: TObject);
begin
  Application.MessageBox ('ATEN��O !!!' + #13+#10 +
                          'COMPUTAR AS FALTAS APENAS DAS CRIAN�AS E ADOLESCENTES ' + #13+#10 +
                          'QUE DEVERIAM TER FREQUENTADO O M�S TODO!!!',
                          '[Sistema Deca] - Informa��o do Sistema',
                          MB_OK + MB_ICONWARNING);
end;

procedure TfrmResumoMensal.edNumTTGeralFaltasMesExit(Sender: TObject);
begin
  //TOTAL DE FALTAS INJUSTIFICADAS
  edNumTTFaltasInjustificadasMes.Text     := IntToStr ( StrToInt(edNumTTGeralFaltasMes.Text) - StrToInt(edNumTTFaltasJustificadasMes.Text) );
end;

procedure TfrmResumoMensal.edNumTTFaltasJustificadasMesExit(
  Sender: TObject);
begin
  //TOTAL DE FALTAS INJUSTIFICADAS
  edNumTTFaltasInjustificadasMes.Text     := IntToStr ( StrToInt(edNumTTGeralFaltasMes.Text) - StrToInt(edNumTTFaltasJustificadasMes.Text) );
end;

end.
