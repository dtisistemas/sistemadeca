unit uTotalRegistrosPorUnidade;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelTotalRegistrosPorUnidade = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRShape1: TQRShape;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel4: TQRLabel;
    QRSysData2: TQRSysData;
    lbTituloPeriodo: TQRLabel;
    lbTituloUsuario: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText1: TQRDBText;
  private

  public

  end;

var
  relTotalRegistrosPorUnidade: TrelTotalRegistrosPorUnidade;

implementation

uses uDM;

{$R *.DFM}

end.
