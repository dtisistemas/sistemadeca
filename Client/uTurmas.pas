unit uTurmas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmTurmas = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    Label1: TLabel;
    edNomeTurma: TEdit;
    Label2: TLabel;
    edNomeProfessor: TEdit;
    rgPeriodo: TRadioGroup;
    Label3: TLabel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    edUnidade: TEdit;
    btnAlterar: TBitBtn;
    DataSource1: TDataSource;
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AtualizaCamposBotoes (Modo : String);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure edNumTurmaExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTurmas: TfrmTurmas;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmTurmas.btnSairClick(Sender: TObject);
begin
 frmturmas.Close;
end;

procedure TfrmTurmas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmTurmas.DBGrid1DblClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Alterar');
  edNomeTurma.SetFocus;

  edNomeTurma.Text := dmDeca.cdsSel_Cadastro_Turmas.FieldByName ('nom_turma').AsString;
  edNomeProfessor.Text := dmDeca.cdsSel_Cadastro_Turmas.FieldByName ('nom_professor').AsString;
  rgPeriodo.ItemIndex := dmDeca.cdsSel_Cadastro_Turmas.FieldByName ('flg_periodo').AsInteger;

end;

procedure TfrmTurmas.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
  try
    with dmDeca.cdsSel_Cadastro_Turmas do
    begin
      Close;
      Params.ParamByName ('@pe_cod_turma').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
      Open;
      DBGrid1.Refresh;
    end
  except end;
end;

procedure TfrmTurmas.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    edNomeTurma.Clear;
    edNomeTurma.Enabled := False;
    edNomeProfessor.Clear;
    edUnidade.Clear;
    edNomeProfessor.Enabled := False;
    edUnidade.Enabled := False;
    rgPeriodo.ItemIndex := -1;
    rgPeriodo.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin
    edNomeTurma.Enabled := True;
    edNomeProfessor.Enabled := True;
    edUnidade.Text := vvNOM_UNIDADE;
    edUnidade.Enabled := False;
    rgPeriodo.Enabled := True;
    rgPeriodo.ItemIndex := 0;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;

  end
  else if Modo = 'Alterar' then
  begin
    edNomeTurma.Enabled := True;
    edNomeProfessor.Enabled := True;
    edUnidade.Text := vvNOM_UNIDADE;
    edUnidade.Enabled := False;
    rgPeriodo.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;

  end;
end;

procedure TfrmTurmas.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padrao');
end;

procedure TfrmTurmas.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Novo');
end;

procedure TfrmTurmas.btnSalvarClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja incluir a turma informada?',
          'Inclus�o de TURMA',
          MB_ICONQUESTION + MB_YESNO) = idYes then
  begin
    try
      with dmDeca.cdsInc_Cadastro_Turmas do
      begin
        Close;
        Params.ParamByName ('@pe_nom_turma').AsString := Trim(edNomeTurma.Text);
        Params.ParamByName ('@pe_nom_professor').AsString := Trim(edNomeProfessor.Text);
        Params.ParamByName ('@pe_flg_periodo').AsInteger := rgPeriodo.ItemIndex;
        Params.ParamByName ('@pe_cod_unidade').AsInteger :=  vvCOD_UNIDADE;
        Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;

        Execute;

        with dmDeca.cdsSel_Cadastro_Turmas do
        begin
          Close;
          Params.ParamByName ('@pe_cod_turma').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Open;

          DBGrid1.Refresh;
        end;

      end
    except
      Application.MessageBox('Aten��o!'+#13+#10+#13+#10+
             'Ocorreum um ERRO e os dados n�o foram gravados... Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Inclus�o de Turma',
             MB_OK + MB_ICONERROR);
      AtualizaCamposBotoes ('Padrao');
    end;

  AtualizaCamposBotoes ('Padrao');  

  end;
end;

procedure TfrmTurmas.btnAlterarClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja alterar os dados da turma?',
          'Altera��o de dados de TURMA',
          MB_ICONQUESTION + MB_YESNO) = idYes then
  begin
    try
      with dmDeca.cdsAlt_Cadastro_Turmas do
      begin
        Close;
        Params.ParamByName ('@pe_cod_turma').AsInteger := dmDeca.cdsSel_Cadastro_Turmas.FieldByName ('cod_turma').AsInteger;
        Params.ParamByName ('@pe_nom_turma').AsString := Trim(edNomeTurma.Text);
        Params.ParamByName ('@pe_nom_professor').AsString := Trim(edNomeProfessor.Text);
        Params.ParamByName ('@pe_flg_periodo').AsInteger := rgPeriodo.ItemIndex;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
        Params.ParamByName ('@pe_log_data').AsDateTime :=  Date();
        Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;

        Execute;

        AtualizaCamposBotoes ('Padrao');

        with dmDeca.cdsSel_Cadastro_Turmas do
        begin
          Close;
          Params.ParamByName ('@pe_cod_turma').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Open;

          DBGrid1.Refresh;
        end;

      end
    except
      Application.MessageBox('Aten��o!'+#13+#10+#13+#10+
             'Ocorreum um ERRO e os dados n�o foram alterados... Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Inclus�o de Turma',
             MB_OK + MB_ICONERROR);
      AtualizaCamposBotoes ('Padrao');
    end;
  end;
end;

procedure TfrmTurmas.edNumTurmaExit(Sender: TObject);
begin
try
  with dmDeca.cdsSel_Cadastro_Turmas do
  begin
    Close;
    Params.ParamByName ('@pe_cod_turma').AsInteger := Null;
    Params.ParamByName ('@pe_nom_turma').AsString := Trim(edNomeTurma.Text);
    Params.ParamByName ('@pe_cod_unidade').Value := vvCOD_UNIDADE;
    Open;

    if RecordCount < 1 then
    begin
      ShowMessage ('Aten��o !!! Essa turma j� cadastrada para essa Unidade...');

      with dmDeca.cdsSel_Cadastro_Turmas do
      begin
        Close;
        Params.ParamByName ('@pe_cod_turma').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
        Open;

        DBGrid1.Refresh;
      end;
    end;
  end;
except end;
end;

end.
