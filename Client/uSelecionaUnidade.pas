unit uSelecionaUnidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TfrmSelecionaUnidade = class(TForm)
    GroupBox1: TGroupBox;
    cbUnidades: TComboBox;
    Label1: TLabel;
    btnVer: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure cbUnidadesExit(Sender: TObject);
    procedure cbUnidadesClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaUnidade: TfrmSelecionaUnidade;

implementation

uses uDM, rGeral, uPrincipal;

{$R *.DFM}

procedure TfrmSelecionaUnidade.FormActivate(Sender: TObject);
begin
try
  cbUnidades.Clear;
  with DmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= NULL;
    Open;
    cbUnidades.Items.Clear;
    while not Eof do
    begin
      cbUnidades.Items.Add(FieldByName('nom_unidade').AsString);
      Next;
    end;
  Close;
  btnVer.Enabled := False;
  end
except end;
end;

procedure TfrmSelecionaUnidade.cbUnidadesExit(Sender: TObject);
begin
  if cbUnidades.ItemIndex > -1 then btnVer.Enabled := True
  else btnVer.Enabled := False;
end;

procedure TfrmSelecionaUnidade.cbUnidadesClick(Sender: TObject);
begin
with DmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName('@pe_cod_unidade').Value:= NULL;
    Params.ParamByName('@pe_nom_unidade').Value:= cbUnidades.Items[cbUnidades.ItemIndex];
    Open;

    vvINDEX_UNIDADE := FieldByName('cod_unidade').Value;
    btnVer.Enabled := True;
    btnVer.SetFOcus;
  end;
end;

procedure TfrmSelecionaUnidade.btnVerClick(Sender: TObject);
begin
  with dmDeca.cdsSel_Ordena_Matricula do
    begin
      Close;
      Params.ParamByName('@pe_ind_status').Value := 1;
      Params.ParamByName('@pe_cod_unidade').Value := vvINDEX_UNIDADE;
      Open;

      Application.CreateForm(TrelGeral, relGeral);
      relGeral.DataSet := dmDeca.cdsSel_Ordena_Matricula;

      relGeral.fld_matricula.DataSet := dmDeca.cdsSel_Ordena_Matricula;
      relGeral.fld_matricula.DataField := 'cod_matricula';
      relGeral.fld_nome.DataSet := dmDeca.cdsSel_Ordena_Matricula;
      relGeral.fld_nome.DataField := 'nom_nome';
      relGeral.fld_endereco.DataSet := dmDeca.cdsSel_Ordena_Matricula;
      relGeral.fld_endereco.DataField := 'nom_endereco';
      relGeral.fld_bairro.DataSet := dmDeca.cdsSel_Ordena_Matricula;
      relGeral.fld_bairro.DataField := 'nom_bairro';
      relGeral.fld_modalidade.DataSet := dmDeca.cdsSel_Unidade;
      relGeral.fld_modalidade.DataField := 'nom_unidade';

      relGeral.QRLabel6.Transparent := False;
      relGeral.QRLabel6.Caption := 'Unidade';
      relGeral.fld_modalidade.Transparent := False;

      relGeral.lbTitulo.Caption := 'RELA��O DE CRIAN�AS/ADOLESCENTES ORDENADOS POR MATR�CULA';
      relGeral.lbTotal.CAption := 'Total de Crian�as/Adolescentes encontrados na unidade' +
                                cbUnidades.Items[cbUnidades.ItemIndex] + ': ';
      relGeral.lbUnidade.Caption := cbUnidades.Items[cbUnidades.ItemIndex];

      relGeral.Preview;
      relGeral.Free;
    end;
end;

end.
