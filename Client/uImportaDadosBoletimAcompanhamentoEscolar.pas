unit uImportaDadosBoletimAcompanhamentoEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FileCtrl, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids;

type
  TfrmImportaDadosBoletimAcompanhamentoEscolar = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edCaminhoArquivo: TEdit;
    btnChamaOpenDialog: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Panel1: TPanel;
    OpenDialog1: TOpenDialog;
    meArquivo: TMemo;
    procedure btnChamaOpenDialogClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaDadosBoletimAcompanhamentoEscolar: TfrmImportaDadosBoletimAcompanhamentoEscolar;

implementation

{$R *.DFM}

procedure TfrmImportaDadosBoletimAcompanhamentoEscolar.btnChamaOpenDialogClick(
  Sender: TObject);
begin
  OpenDialog1.Execute;
  edCaminhoArquivo.Text := OpenDialog1.FileName;
end;

end.
