unit uREMARCACAO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Mask, Buttons, Grids, DBGrids, ExtCtrls;

type
  TfrmREMARCACAO = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    btnVERIFICA_DISPONIBILIDADE: TSpeedButton;
    lbMatricula: TLabel;
    lbNomePaciente: TLabel;
    mskHORA_REMARCAR: TMaskEdit;
    mskDATA_REMARCAR: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure btnVERIFICA_DISPONIBILIDADEClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmREMARCACAO: TfrmREMARCACAO;

implementation

uses uDM, uUtil, uPrincipal, uFichaClinica;

{$R *.DFM}

procedure TfrmREMARCACAO.FormShow(Sender: TObject);
begin
  lbMatricula.Caption    := dmDeca.cdsSel_Consulta.FieldByName ('cod_matricula').Value;
  lbNomePaciente.Caption := dmDeca.cdsSel_Consulta.FieldByName ('nom_nome').Value;
  mskDATA_REMARCAR.Text  := DateToStr(frmFichaClinica.dataFiltro.Date);
end;

procedure TfrmREMARCACAO.btnVERIFICA_DISPONIBILIDADEClick(Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Consulta do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_consulta').Value     := Null;
      Params.ParamByName('@pe_cod_matricula').Value       := Null;
      Params.ParamByName('@pe_nom_nome').Value            := Null;
      Params.ParamByName('@pe_dat_consulta').Value        := GetValue(mskDATA_REMARCAR, vtDate);
      Params.ParamByName('@pe_hor_consulta').Value        := mskHORA_REMARCAR.Text;
      Params.ParamByName('@pe_flg_status_consulta').Value := Null;
      Open;

      if (dmDeca.cdsSel_Consulta.RecordCount > 0) then
      begin
        Application.MessageBox ('O hor�rio informado na data j� tem consulta marcada.' + #13+#10 +
                                'Favor escolher outro hor�rio ou data.',
                                '[Sistema Deca] - Remarca��o de Consulta',
                                MB_OK + MB_ICONINFORMATION);
        mskHORA_REMARCAR.SetFocus;
      end
      else
      begin
        //Se n�o h� registro encontrado, marca a consulta para a data e hor�rio informado
        //e exclui o registro atual

        try

          with dmDeca.cdsInc_Consulta do
          begin
            Close;
            Params.ParamByName('@pe_cod_matricula').Value       := GetValue(lbMatricula.Caption);
            Params.ParamByName('@pe_dat_consulta').Value        := GetValue(mskDATA_REMARCAR, vtDate);
            Params.ParamByName('@pe_hor_consulta').Value        := GetValue(mskHORA_REMARCAR.Text);
            Params.ParamByName('@pe_flg_status_consulta').Value := 0; //Consulta Marcada...
            Params.ParamByName('@pe_log_cod_usuario').Value     := vvCOD_USUARIO;
            Execute;
          end;

          with dmDeca.cdsExc_Consulta do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_consulta').Value := vvCOD_CONSULTA_EXCLUI;
            Execute;
          end;

          frmREMARCACAO.Close;

        except end;


      end;

    end;
  except end;
end;

end.
