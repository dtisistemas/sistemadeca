unit uSatisfacaoConveniados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Grids, DBGrids, Buttons, Mask, Db;

type
  TfrmSatisfacaoConveniados = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dbgEmpresas: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edCodEmpresa: TEdit;
    edEmpresa: TEdit;
    edSetor: TEdit;
    mskData: TMaskEdit;
    meAdolescentes: TMemo;
    TabSheet3: TTabSheet;
    dsEmpresas: TDataSource;
    Panel2: TPanel;
    btnSalvar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnSair: TSpeedButton;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    rgItem01: TRadioGroup;
    rgItem02: TRadioGroup;
    rgItem03: TRadioGroup;
    rgItem04: TRadioGroup;
    rgItem05: TRadioGroup;
    meObservacoes: TMemo;
    btnNovo: TSpeedButton;
    Label11: TLabel;
    edAvaliador: TEdit;
    Panel1: TPanel;
    btnSair1: TSpeedButton;
    Panel3: TPanel;
    Label12: TLabel;
    edASocial: TEdit;
    Panel4: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cbSemestre: TComboBox;
    GroupBox4: TGroupBox;
    cbEmpresa: TComboBox;
    chkSemEmpresa: TCheckBox;
    mskAno: TMaskEdit;
    btnResultado: TSpeedButton;
    GroupBox5: TGroupBox;
    dbgResultado: TDBGrid;
    dsSatisfacaoConv: TDataSource;
    btnConsulta: TSpeedButton;
    btnRelatorio: TSpeedButton;
    btnVoltar: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnNovaAvaliacao: TSpeedButton;
    Label13: TLabel;
    cbSecoesUnidade: TComboBox;
    btnVerSecoesUnidade: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure dbgEmpresasDblClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnNovaAvaliacaoClick(Sender: TObject);
    procedure mskDataExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnSair1Click(Sender: TObject);
    procedure btnConsultaClick(Sender: TObject);
    function ValidaCampos : Boolean;
    procedure btnResultadoClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure dbgResultadoDblClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnVerSecoesUnidadeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSatisfacaoConveniados: TfrmSatisfacaoConveniados;
  vvMUDA_ABA : Boolean;
  vListaSecao1, vListaSecao2 : TStringList;
  vIndSecao : Integer;

  vvTOTAL_PONTOS, vvITENS_AVALIADOS : Integer;
  vvMEDIA : Real;
  strMEDIA : String;

implementation

uses uDM, uUtil, uPrincipal, rSatisfacaoConveniados, rObsSatisfConv, QRExport;

{$R *.DFM}

procedure TfrmSatisfacaoConveniados.FormShow(Sender: TObject);
begin
{
  vListaSecao1 := TStringList.Create();
  vListaSecao2 := TStringList.Create();

  //Carrega as empresas do Programa Aprendiz - Conv�nio para prenchimento
  //das satisfa��es de cliente - conveniados
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := 44;  //Unidade=Aprendiz Convenios
      Params.ParamByName ('@pe_num_secao').Value := Null;
      Params.ParamByName ('@pe_flg_status').Value := Null;
      Open;

      //Carrega o combo de empresas na aba de consulta
      while not eof do
      begin
        cbEmpresa.Items.Add (dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').Value);
        //vListaSecao1.Add (IntToStr(FieldByName('num_secao').Value));
        //vListaSecao2.Add (FieldByName('dsc_nom_secao').Value);
        dmDeca.cdsSel_Secao.Next;
      end;

      dbgEmpresas.Refresh;
      PageControl1.ActivePageIndex := 0;

    end;

  except end;
}
end;

procedure TfrmSatisfacaoConveniados.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if vvMUDA_ABA = False then AllowChange := False;
end;

procedure TfrmSatisfacaoConveniados.dbgEmpresasDblClick(Sender: TObject);
begin
  vvMUDA_ABA := True;
  PageControl1.ActivePageIndex := 1;
  vvMUDA_ABA := False;
  btnSalvar.Enabled := True;
  btnAlterar.Enabled := False;
  btnCancelar.Enabled := True;
  btnSair.Enabled := False;

  //Repassa os dados das empresas para a aba de avalia��o
  edCodEmpresa.Text := Trim(dmDeca.cdsSel_Secao.FieldByName ('num_ccusto').Value) + '.' + dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value;
  edEmpresa.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_unidade').Value + ' - ' +
                    dmDeca.cdsSel_Secao.FieldByName ('dsc_nom_secao').Value;
  edASocial.Text := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').Value;

  mskData.Text := DateToStr(Date());
  edSetor.SetFocus;

end;

procedure TfrmSatisfacaoConveniados.btnSalvarClick(Sender: TObject);
begin

  //Pede confirma��o do usu�rio antes da grava��o dos dados
  if Application.MessageBox ('Confirma o lan�amento dos dados do Question�rio de Satisfa��o do Cliente atual?',
                             '[Sistema Deca] - Question�rio Satisfa��o do Cliente - Conveniados',
                             MB_YESNO + MB_ICONQUESTION) = ID_YES then
  begin
    //Validar os itens antes de gravar
    
    vvTOTAL_PONTOS := 0;
    vvITENS_AVALIADOS := 0;
    vvMEDIA := 0;
    strMEDIA := '';
    
    //Faz uma consulta ao banco e verifica se empresa j� foi avaliada no per�odo informado...
    try
      with dmDeca.cdsInc_SatisfConv do
      begin
        Close;
        Params.ParamByName ('@pe_dat_avaliacao').Value    := GetValue(mskData, vtDate);
        Params.ParamByName ('@pe_cod_unidade').Value      := dmDeca.cdsSel_Secao.FieldByName ('cod_unidade').Value;
        Params.ParamByName ('@pe_num_secao').Value        := Copy(edCodEmpresa.Text, 6, 4);
        Params.ParamByName ('@pe_nom_avaliador').Value    := GetValue(edAvaliador.Text);
        Params.ParamByName ('@pe_dsc_setor').Value        := GetValue(edSetor.Text);
        Params.ParamByName ('@pe_dsc_adolescentes').Value := GetValue(meAdolescentes.Text);
        Params.ParamByName ('@pe_num_item01').Value       := rgItem01.ItemIndex;
        Params.ParamByName ('@pe_num_item02').Value       := rgItem02.ItemIndex;
        Params.ParamByName ('@pe_num_item03').Value       := rgItem03.ItemIndex;
        Params.ParamByName ('@pe_num_item04').Value       := rgItem04.ItemIndex;
        Params.ParamByName ('@pe_num_item05').Value       := rgItem05.ItemIndex;
        Params.ParamByName ('@pe_dsc_observacoes').Value  := GetValue(meObservacoes.Text);
        Params.ParamByName ('@pe_cod_usuario').Value      := vvCOD_USUARIO;

        //Faz o c�lculo da M�dia antes de gravar no banco de dados
        if (rgItem01.ItemIndex > 0) then
        begin
          vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem01.ItemIndex;
          vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
        end;

        if (rgItem02.ItemIndex > 0) then
        begin
          vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem02.ItemIndex;
          vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
        end;

        if (rgItem03.ItemIndex > 0) then
        begin
          vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem03.ItemIndex;
          vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
        end;

        if (rgItem04.ItemIndex > 0) then
        begin
          vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem04.ItemIndex;
          vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
        end;

        if (rgItem05.ItemIndex > 0) then
        begin
          vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem05.ItemIndex;
          vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
        end;

        vvMEDIA := vvTOTAL_PONTOS / vvITENS_AVALIADOS;
        strMEDIA := Format('%8.2f', [vvMEDIA]);

        //ShowMessage (strMEDIA);

        Params.ParamByName ('@pe_vlr_media').Value := StrToFloat(strMEDIA);
        Execute;

        //Limpa Campos do Formul�rio...
        //Objetos do formul�rio
        edCodEmpresa.Clear;
        edEmpresa.Clear;
        edSetor.Clear;
        mskData.Clear;
        edAvaliador.Clear;
        edAsocial.Clear;
        meAdolescentes.Clear;
        rgItem01.ItemIndex := -1;
        rgItem02.ItemIndex := -1;
        rgItem03.ItemIndex := -1;
        rgItem04.ItemIndex := -1;
        rgItem05.ItemIndex := -1;
        meObservacoes.Clear;

        //Bot�es do formul�rio
        btnNovo.Enabled := False;
        btnSalvar.Enabled := False;
        btnCancelar.Enabled := False;
        btnSair.Enabled := True;

        PageControl1.ActivePageIndex := 0;
        vvMUDA_ABA := False;
        
      end;

    except
    Application.MessageBox('Aten��o !!!' + #13+#10 +
                           'Um erro ocorreu e os dados n�o foram inseridos.' + #13+#10 +
                           'Favor verifique a conex�o com a internet/rede ou entre ' + #13+#10 +
                           'em contato com o Administrador do Sistema.',
                           '[Sistema Deca] - Erro Satisfa��o Conveniado',
                           MB_OK + MB_ICONERROR);
    vvMUDA_ABA := False;
    PageControl1.ActivePageIndex := 0;
    end;
  end
  else
  begin
    Application.MessageBox ('Os dados ainda n�o foram salvos. Voc� optou por n�o salvar os dados do question�rio agora.' + #13+#10 +
                            'Fa�a as corre��es devidas e depois clique em salvar novamente. Se voc� optar por cancelar ' +#13+#10 +
                            'os dados n�o ser�o inseridos no banco de dados e voc� dever� lan��-los mais tarde. ' + #13+#10 +
                            'Qualquer d�vida entre em contato com o Administrador do Sistema.',
                            '[Sistema Deca] - Satisfa��o do Cliente',
                            MB_OK + MB_ICONWARNING);
  end;
end;

procedure TfrmSatisfacaoConveniados.btnNovaAvaliacaoClick(Sender: TObject);
begin
  vvMUDA_ABA := True;
  PageControl1.ActivePageIndex := 1;
  vvMUDA_ABA := False;
  //Repassa os dados das empresas para a aba de avalia��o
  //edCodEmpresa.Text  := '3006.' + dmDeca.cdsSel_Secao.FieldByName ('num_secao').Value;

  case cbSecoesUnidade.ItemIndex of
    0: edCodEmpresa.Text  := '5109';
    1: edCodEmpresa.Text  := '5104';
    2: edCodEmpresa.Text  := '5103';
    3: edCodEmpresa.Text  := '5102';
    4: edCodEmpresa.Text  := '5105';
    5: edCodEmpresa.Text  := '5110';
    6: edCodEmpresa.Text  := '5107';
    7: edCodEmpresa.Text  := '5112';
    8: edCodEmpresa.Text  := '5111';
  end;

  edEmpresa.Text     := dmDeca.cdsSel_Secao.FieldByName ('nom_unidade').Value;
  edASocial.Text     := dmDeca.cdsSel_Secao.FieldByName ('nom_asocial').Value;
  mskData.Text       := DateToStr(Date());
  edSetor.SetFocus;
  btnAlterar.Enabled := False;
  btnSair.Enabled    := False;
end;

procedure TfrmSatisfacaoConveniados.mskDataExit(Sender: TObject);
begin
  //Consulta ao banco de dados e verifica se existe lan�amento nesta data para a empresa...
  try
    with dmDeca.cdsSel_SatisfConv do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_satisfconv').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Secao.FieldByName ('cod_unidade').Value;
      Params.ParamByName ('@pe_num_secao').Value := Copy(edCodEmpresa.Text, 6 ,4);
      Params.ParamByName ('@pe_mes_inicio_avaliacao').Value := Copy(mskData.Text, 4, 2);
      Params.ParamByName ('@pe_mes_final_avaliacao').Value := Copy(mskData.Text, 4, 2);
      Params.ParamByName ('@pe_ano_avaliacao').Value := Copy(mskData.Text, 7, 4);
      Open;

      if RecordCount > 0 then
      begin
        Application.MessageBox ('Aten��o !!!'  +#13+#10 +
                                'J� existe um lan�amento neste per�odo ' + #13+#10 +
                                'para a empresa/unidade informada...'  + #13+#10 +
                                'Favor verificar e tentar novamente.',
                                '[Sistema Deca] - Dados j� existem...',
                                MB_OK + MB_ICONWARNING);
        mskData.Clear;
        mskData.SetFocus;
      end;

    end;
  except end;
end;

procedure TfrmSatisfacaoConveniados.btnCancelarClick(Sender: TObject);
begin
    vvMUDA_ABA := False;
    PageControl1.ActivePageIndex := 0;
end;

procedure TfrmSatisfacaoConveniados.btnSairClick(Sender: TObject);
begin
  frmSatisfacaoConveniados.Close;
end;

procedure TfrmSatisfacaoConveniados.btnSair1Click(Sender: TObject);
begin
  frmSatisfacaoConveniados.Close;
end;

procedure TfrmSatisfacaoConveniados.btnConsultaClick(Sender: TObject);
begin
  vvMUDA_ABA := True;
  PageControl1.ActivePageIndex := 2;
  vvMUDA_ABA := False;
end;

function TfrmSatisfacaoConveniados.ValidaCampos: Boolean;
begin
  //Verifica se os campos ANO e BIMESTRE foram preenchidos...
  if (Length(mskAno.Text) < 4) or (cbSemestre.ItemIndex < 0)then
  begin
    Application.MessageBox ('Aten��o!!! ' +
                            'Campos obrigat�rios devem ser preenchidos.',
                            '[Sistema Deca] - Preencher Campo Obrigat�rio',
                            MB_OK + MB_ICONINFORMATION);
  end;
end;

procedure TfrmSatisfacaoConveniados.btnResultadoClick(Sender: TObject);
begin
  if ValidaCampos then
  begin

    try
      with dmDeca.cdsSel_SatisfConv do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_satisfconv').Value := Null;
        Params.ParamByName ('@pe_cod_unidade').Value := Null;  //Apenas Aprendiz-Conv�nios

        if chkSemEmpresa.Checked then
          Params.ParamByName ('@pe_num_secao').Value := Null
        else
        begin
          vListaSecao2.Find(dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').AsString, vIndSecao);
          Params.ParamByName ('@pe_num_secao').Value := vIndSecao
        end;

        if cbSemestre.ItemIndex = 0 then
        begin
          Params.ParamByName ('@pe_mes_inicio_avaliacao').Value := '01';
          Params.ParamByName ('@pe_mes_final_avaliacao').Value := '06'
        end
        else if cbSemestre.ItemIndex = 1 then
        begin
          Params.ParamByName ('@pe_mes_inicio_avaliacao').Value := '07';
          Params.ParamByName ('@pe_mes_final_avaliacao').Value := '12'
        end;

        Params.ParamByName ('@pe_ano_avaliacao').Value := StrToInt(mskAno.Text);
        Open;

        //dbgResultado.Refresh;

        if RecordCount <=0 then
        begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'N�o foram encontrados registros de acordo com os ' + #13+#10 +
                                  'crit�rios definidos na consulta. Favor rev�-los.',
                                  '[Sistema Deca] - Registros n�o encontrados',
                                  MB_OK + MB_ICONINFORMATION);
          dbgResultado.DataSource := nil;
        end
        else
        begin
          dbgResultado.DataSource := dsSatisfacaoConv;
          dbgResultado.Refresh;
        end;
      end;
    except end;

  end;
end;

procedure TfrmSatisfacaoConveniados.btnRelatorioClick(Sender: TObject);
begin
  Application.CreateForm(TrelSatisfacaoConveniados, relSatisfacaoConveniados);
  relSatisfacaoConveniados.lbSemestreAno.Caption := cbSemestre.Text + '/' + mskAno.Text;
  relSatisfacaoConveniados.Prepare;
  relSatisfacaoConveniados.qrlbTotalPaginas.Caption := ' de  ' + IntToStr(relSatisfacaoConveniados.QRPrinter.PageCount);
  relSatisfacaoConveniados.qrlbNomeUsuario.Caption := vvNOM_USUARIO;
  relSatisfacaoConveniados.Preview;
  relSatisfacaoConveniados.Free;

  Application.MessageBox ('Aten��o !!!' + #13+#10 +
                          'O relat�rio com as Observa��es ser� exibido agora.',
                          '[Sistema Deca] - Relat�rio de Observa��es',
                          MB_OK + MB_ICONINFORMATION);

  Application.CreateForm (TrelObsSatisfConv, relObsSatisfConv);
  relObsSatisfConv.lbSemestreAno.Caption := cbSemestre.Text + '/' + mskAno.Text;
  relObsSatisfConv.Prepare;
  relObsSatisfConv.qrlbTotalPaginas.Caption := ' de  ' + IntToStr(relObsSatisfConv.QRPrinter.PageCount);
  relObsSatisfConv.qrlbNomeUsuario.Caption := vvNOM_USUARIO;
  relObsSatisfConv.Preview;
  relObsSatisfConv.Free;


end;

procedure TfrmSatisfacaoConveniados.btnVoltarClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  vvMUDA_ABA := False;
end;

procedure TfrmSatisfacaoConveniados.dbgResultadoDblClick(Sender: TObject);
begin
  vvMUDA_ABA := True;
  PageControl1.ActivePageIndex := 1;
  vvMUDA_ABA := False;

  //Repassa os dados para a aba Question�rio...
  edCodEmpresa.Text   := '3006.' + dmDeca.cdsSel_SatisfConv.FieldByName ('num_secao').Value;
  edEmpresa.Text      := dmDeca.cdsSel_SatisfConv.FieldByName ('dsc_nom_secao').Value;
  edSetor.Text        := dmDeca.cdsSel_SatisfConv.FieldByName ('dsc_setor').Value;
  mskData.Text        := dmDeca.cdsSel_SatisfConv.FieldByName ('DatAvaliacao').Value;
  edAvaliador.Text    := dmDeca.cdsSel_SatisfConv.FieldByName ('nom_avaliador').Value;
  meAdolescentes.Text := dmDeca.cdsSel_SatisfConv.FieldByName ('dsc_adolescentes').Value;

  rgItem01.ItemIndex := dmDeca.cdsSel_SatisfConv.FieldByName ('num_item01').Value;
  rgItem02.ItemIndex := dmDeca.cdsSel_SatisfConv.FieldByName ('num_item02').Value;
  rgItem03.ItemIndex := dmDeca.cdsSel_SatisfConv.FieldByName ('num_item03').Value;
  rgItem04.ItemIndex := dmDeca.cdsSel_SatisfConv.FieldByName ('num_item04').Value;
  rgItem05.ItemIndex := dmDeca.cdsSel_SatisfConv.FieldByName ('num_item05').Value;
  meObservacoes.Text := dmDeca.cdsSel_SatisfConv.FieldByName ('dsc_observacoes').Value;

  PageControl2.ActivePageIndex := 0;
  btnSalvar.Enabled   := False;
  btnAlterar.Enabled  := True;
  btnCancelar.Enabled := True;

end;

procedure TfrmSatisfacaoConveniados.btnAlterarClick(Sender: TObject);
begin

  vvTOTAL_PONTOS := 0;
  vvITENS_AVALIADOS := 0;
  vvMEDIA := 0;
  strMEDIA := '';

  //Faz o c�lculo da M�dia antes de gravar as altera��es no banco de dados
  if (rgItem01.ItemIndex > 0) then
  begin
    vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem01.ItemIndex;
    vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
  end;

  if (rgItem02.ItemIndex > 0) then
  begin
    vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem02.ItemIndex;
    vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
  end;

  if (rgItem03.ItemIndex > 0) then
  begin
    vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem03.ItemIndex;
    vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
  end;

  if (rgItem04.ItemIndex > 0) then
  begin
    vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem04.ItemIndex;
    vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
  end;

  if (rgItem05.ItemIndex > 0) then
  begin
    vvTOTAL_PONTOS    := vvTOTAL_PONTOS + rgItem05.ItemIndex;
    vvITENS_AVALIADOS := vvITENS_AVALIADOS + 1
  end;

  if (vvTOTAL_PONTOS > 0) or (vvITENS_AVALIADOS > 0) then
  begin
    vvMEDIA := vvTOTAL_PONTOS / vvITENS_AVALIADOS;
    strMEDIA := Format('%8.2f', [vvMEDIA]);
  end
  else
    vvMEDIA := 0;

  try
    with dmDeca.cdsAlt_PesquisaSatisfacao do
    begin

      Close;
      Params.ParamByName ('@pe_cod_id_satisfconv').Value := dmDeca.cdsSel_SatisfConv.FieldByName ('cod_id_satisfconv').Value;
      Params.ParamByName ('@pe_num_secao').Value         := Copy(edCodEmpresa.Text, 6, 4);
      Params.ParamByName ('@pe_nom_avaliador').Value     := GetValue(edAvaliador.Text);
      Params.ParamByName ('@pe_dsc_setor').Value         := GetValue(edSetor.Text);
      Params.ParamByName ('@pe_dsc_adolescentes').Value  := GetValue(meAdolescentes.Text);
      Params.ParamByName ('@pe_num_item01').Value        := rgItem01.ItemIndex;
      Params.ParamByName ('@pe_num_item02').Value        := rgItem02.ItemIndex;
      Params.ParamByName ('@pe_num_item03').Value        := rgItem03.ItemIndex;
      Params.ParamByName ('@pe_num_item04').Value        := rgItem04.ItemIndex;
      Params.ParamByName ('@pe_num_item05').Value        := rgItem05.ItemIndex;
      Params.ParamByName ('@pe_dat_avaliacao').Value     := GetValue(mskData, vtDate);                                    
      Params.ParamByName ('@pe_cod_usuario').Value       := vvCOD_USUARIO;
      Params.ParamByName ('@pe_vlr_media').Value         := vvMEDIA;
      Params.ParamByName ('@pe_dsc_observacoes').Value   := GetValue(meObservacoes.Text);
      Execute;

      Application.MessageBox ('Dados da pesquisa alterados com sucesso!!!',
                              '[Sistema Deca] - Pesquisa de Satisfa��o',
                              MB_OK + MB_ICONINFORMATION);





      //Limpa os campos...
      edCodempresa.Clear;
      edEmpresa.Clear;
      edSetor.Clear;
      mskData.Clear;
      edAvaliador.Clear;
      edASocial.Clear;
      meAdolescentes.Clear;
      rgItem01.ItemIndex := -1;
      rgItem02.ItemIndex := -1;
      rgItem03.ItemIndex := -1;
      rgItem04.ItemIndex := -1;
      rgItem05.ItemIndex := -1;
      meObservacoes.Clear;


      PageControl1.ActivePageIndex := 2;
      vvMUDA_ABA := False;

    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Um erro ocorreu ao tentar ALTERAR os dados da Pesquisa de Satisfa��o.' + #13+#10 +
                            'Entre em contato com o Administrador do Sistema.',
                            '[Sistema Deca] - Pesquisa de Satisfa��o',
                            MB_OK + MB_ICONERROR);

  end;






  //Params.ParamByName ('@pe_vlr_media').Value := StrToFloat(strMEDIA);





end;

procedure TfrmSatisfacaoConveniados.btnVerSecoesUnidadeClick(
  Sender: TObject);
begin

    vListaSecao1 := TStringList.Create();
    vListaSecao2 := TStringList.Create();

    //Carrega as empresas do Programa Aprendiz - Conv�nio para prenchimento
    //das satisfa��es de cliente - conveniados
    try
      with dmDeca.cdsSel_Secao do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_secao').Value := Null;

        case cbSecoesUnidade.ItemIndex of
          0: Params.ParamByName ('@pe_cod_unidade').Value := 85;
          1: Params.ParamByName ('@pe_cod_unidade').Value := 23;
          2: Params.ParamByName ('@pe_cod_unidade').Value := 31;
          3: Params.ParamByName ('@pe_cod_unidade').Value := 44;
          4: Params.ParamByName ('@pe_cod_unidade').Value := 84;
          5: Params.ParamByName ('@pe_cod_unidade').Value := 86;
          6: Params.ParamByName ('@pe_cod_unidade').Value := 8;
          7: Params.ParamByName ('@pe_cod_unidade').Value := 83;
          8: Params.ParamByName ('@pe_cod_unidade').Value := 87;
        end;

        //Params.ParamByName ('@pe_cod_unidade').Value := 44;  //Unidade=Aprendiz Convenios
        Params.ParamByName ('@pe_num_secao').Value := Null;
        Params.ParamByName ('@pe_flg_status').Value := Null;
        Open;

        //Carrega o combo de empresas na aba de consulta
        while not eof do
        begin
          cbEmpresa.Items.Add (dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').Value);
          vListaSecao1.Add (IntToStr(FieldByName('num_secao').Value));
          vListaSecao2.Add (FieldByName('dsc_nom_secao').Value);
          dmDeca.cdsSel_Secao.Next;
        end;

        dbgEmpresas.Refresh;
        PageControl1.ActivePageIndex := 0;

      end;

    except end;


end;

end.
