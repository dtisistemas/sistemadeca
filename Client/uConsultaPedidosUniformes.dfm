object frmConsultaPedidos: TfrmConsultaPedidos
  Left = 556
  Top = 375
  Width = 666
  Height = 179
  Caption = '[Sistema Deca] - Consulta de Pedidos de Uniformes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 500
    Top = 40
    Width = 137
    Height = 88
    Caption = 'Visualizar Relat�rio'
    Flat = True
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      7777700000007777777770000000000000007777747770FFFFFF000000007777
      744770F8888F000000007444444470FFFFFF000000007777744770F8888F0000
      00007777747770FFFFFF000000007777777770F8888F000000007770000070FF
      FFFF000000007770FFF07000000000000000700000F0777777777000000070FF
      F0F0777777777000000070F8F000777777777000000070F8F007777777777000
      000070FF00777777777770000000700007777777777770000000777777777777
      777770000000}
  end
  object PageControl1: TPageControl
    Left = 3
    Top = 3
    Width = 472
    Height = 147
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Selecione a Unidade'
      object Label1: TLabel
        Left = 65
        Top = 25
        Width = 40
        Height = 13
        Caption = 'Unidade'
      end
      object Label5: TLabel
        Left = 79
        Top = 48
        Width = 20
        Height = 13
        Caption = 'M�s'
      end
      object Label6: TLabel
        Left = 81
        Top = 72
        Width = 19
        Height = 13
        Caption = 'Ano'
      end
      object cbUnidade: TComboBox
        Left = 108
        Top = 22
        Width = 287
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
      end
      object cbMes1: TComboBox
        Left = 108
        Top = 45
        Width = 209
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'Janeiro'
          'Fevereiro'
          'Mar�o'
          'Abril'
          'Maio'
          'Junho'
          'Julho'
          'Agosto'
          'Setembro'
          'Outubro'
          'Novembro'
          'Dezembro')
      end
      object mskAno1: TMaskEdit
        Left = 108
        Top = 68
        Width = 77
        Height = 21
        EditMask = '9999'
        MaxLength = 4
        TabOrder = 2
        Text = '    '
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Aluno'
      ImageIndex = 1
      object Label2: TLabel
        Left = 8
        Top = 20
        Width = 59
        Height = 13
        Caption = 'Matr�cula:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnPesquisar: TSpeedButton
        Left = 161
        Top = 14
        Width = 28
        Height = 24
        Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
        Glyph.Data = {
          66010000424D6601000000000000760000002800000014000000140000000100
          040000000000F0000000120B0000120B00001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          777777770000777777777777777700770000777777777777777C440700007777
          7777777777C4C40700007777777777777C4C4077000077777777777784C40777
          0000777777788888F740777700007777770000007807777700007777707EFEE6
          007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
          8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
          0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
          7777777700A077777777777777777777FFA077777777777777777777FFFF7777
          7777777777777777FF81}
        ParentShowHint = False
        ShowHint = True
      end
      object lbNome: TLabel
        Left = 192
        Top = 19
        Width = 187
        Height = 14
        Alignment = taCenter
        Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 45
        Top = 42
        Width = 20
        Height = 13
        Caption = 'M�s'
      end
      object Label8: TLabel
        Left = 47
        Top = 66
        Width = 19
        Height = 13
        Caption = 'Ano'
      end
      object txtMatricula: TMaskEdit
        Left = 74
        Top = 16
        Width = 83
        Height = 21
        Hint = 'Informe o N�MERO DE MATR�CULA'
        EditMask = '!99999999;1;_'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 8
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '        '
      end
      object cbMes2: TComboBox
        Left = 74
        Top = 39
        Width = 209
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'Janeiro'
          'Fevereiro'
          'Mar�o'
          'Abril'
          'Maio'
          'Junho'
          'Julho'
          'Agosto'
          'Setembro'
          'Outubro'
          'Novembro'
          'Dezembro')
      end
      object mskAno2: TMaskEdit
        Left = 75
        Top = 62
        Width = 77
        Height = 21
        EditMask = '9999'
        MaxLength = 4
        TabOrder = 2
        Text = '    '
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Per�odo'
      ImageIndex = 2
      object Label3: TLabel
        Left = 96
        Top = 32
        Width = 20
        Height = 13
        Caption = 'M�s'
      end
      object Label4: TLabel
        Left = 98
        Top = 56
        Width = 19
        Height = 13
        Caption = 'Ano'
      end
      object cbMes3: TComboBox
        Left = 125
        Top = 29
        Width = 209
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items.Strings = (
          'Janeiro'
          'Fevereiro'
          'Mar�o'
          'Abril'
          'Maio'
          'Junho'
          'Julho'
          'Agosto'
          'Setembro'
          'Outubro'
          'Novembro'
          'Dezembro')
      end
      object mskAno3: TMaskEdit
        Left = 126
        Top = 52
        Width = 77
        Height = 21
        EditMask = '9999'
        MaxLength = 4
        TabOrder = 1
        Text = '    '
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Tipo de Uniforme'
      ImageIndex = 3
      object Label9: TLabel
        Left = 55
        Top = 27
        Width = 81
        Height = 13
        Caption = 'Tipo de Uniforme'
      end
      object Label10: TLabel
        Left = 112
        Top = 51
        Width = 20
        Height = 13
        Caption = 'M�s'
      end
      object Label11: TLabel
        Left = 114
        Top = 75
        Width = 19
        Height = 13
        Caption = 'Ano'
      end
      object cbTipoUniforme: TComboBox
        Left = 141
        Top = 23
        Width = 287
        Height = 21
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 0
      end
      object cbMes4: TComboBox
        Left = 141
        Top = 48
        Width = 209
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'Janeiro'
          'Fevereiro'
          'Mar�o'
          'Abril'
          'Maio'
          'Junho'
          'Julho'
          'Agosto'
          'Setembro'
          'Outubro'
          'Novembro'
          'Dezembro')
      end
      object mskAno4: TMaskEdit
        Left = 141
        Top = 71
        Width = 77
        Height = 21
        EditMask = '9999'
        MaxLength = 4
        TabOrder = 2
        Text = '    '
      end
    end
  end
end
