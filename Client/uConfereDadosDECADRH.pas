unit uConfereDadosDECADRH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, jpeg;

type
  TfrmConfereDadosDECADRH = class(TForm)
    OpenDialog1: TOpenDialog;
    Image1: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    btnBuscar: TSpeedButton;
    btnImportar: TSpeedButton;
    Bevel1: TBevel;
    edCaminhoArquivo: TEdit;
    Memo1: TMemo;
    SaveDialog1: TSaveDialog;
    Memo2: TMemo;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfereDadosDECADRH: TfrmConfereDadosDECADRH;
  arqCONFERE : TextFile;
  mmLINHA, strNOME : String;
  strLIST : TStringList;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmConfereDadosDECADRH.btnBuscarClick(Sender: TObject);
begin
  ShowMessage ('Informe o local do arquivo texto gerado pelo CorporeRMCriancas...');
  OpenDialog1.Execute;
  edCaminhoArquivo.Text := OpenDialog1.FileName;
  btnBuscar.Enabled   := False;
  btnImportar.Enabled := True;
end;

procedure TfrmConfereDadosDECADRH.btnImportarClick(Sender: TObject);
begin

  AssignFile (arqCONFERE, OpenDialog1.FileName);
  Reset(arqCONFERE);

  while not EOF (arqCONFERE) do
  begin

    Readln (arqCONFERE, mmLINHA);

    //Pesquisa a matr�cula no cadastro
    try
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := GetValue(mmLINHA); //Trim(Copy(mmLINHA,1,8));
        Params.ParamByName ('@pe_cod_unidade').Value      := 0;
        Open;

        strNOME := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString;

        if dmDeca.cdsSel_Cadastro.RecordCount < 1 then
        begin
          //Memo1.Lines.Add ('[ERRO] Matricula: ' + Trim(Copy(mmLINHA,1,8)) + ' n�o encontrada no SISTEMA DECA.')
          Memo1.Lines.Add ('[ERRO] Matricula: ' + mmLINHA + ' n�o encontrada no SISTEMA DECA.')
        end
        else
          //Memo1.Lines.Add ('  [OK] Matr�cula: ' + Trim(Copy(mmLINHA,1,8)) + ' Nome : ' + strNOME + ' - localizado com sucesso no SISTEMA DECA.')
          Memo2.Lines.Add ('  [OK] Matr�cula: ' + mmLINHA + ' Nome : ' + strNOME + ' - localizado com sucesso no SISTEMA DECA.')
      end;


    except

    end;

  end;

  edCaminhoArquivo.Clear;
  btnBuscar.Enabled := True;
  btnImportar.Enabled := False;

  //Gravar em arquivo texto...
  strLIST := TStringList.Create;
  strLIST.Add (Memo1.Text);
  SaveDialog1.Title := 'Erros de Importa��o';
  SaveDialog1.Execute;
  strLIST.SaveToFile(SaveDialog1.FileName);

end;

end.
