unit uDiasUteis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Grids, DBGrids, ExtCtrls, Mask, Db;

type
  TfrmCalendarioDiasUteis = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dbgDiasEntregaResumo: TDBGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    btnNovo: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnSair: TSpeedButton;
    GroupBox4: TGroupBox;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    mskDia: TMaskEdit;
    dsDiasEntregaResumo: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCalendarioDiasUteis: TfrmCalendarioDiasUteis;

implementation

{$R *.DFM}

end.
