unit rNovoDesligamento;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelNovoDesligamento = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel5: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel6: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel7: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel8: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel9: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel10: TQRLabel;
    QRShape14: TQRShape;
    QRLabel11: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel13: TQRLabel;
    lbTipoDesligamento1: TQRLabel;
    QRLabel15: TQRLabel;
    lbMotivoDesligamento1: TQRRichText;
    QRLabel16: TQRLabel;
    lbTipoDesligamento2: TQRLabel;
    QRLabel18: TQRLabel;
    lbMotivoDesligamento2: TQRRichText;
    lblProntuario1: TQRLabel;
    lblProntuario2: TQRLabel;
    lbMatricula1: TQRLabel;
    lbMatricula2: TQRLabel;
    lblNome1: TQRLabel;
    lblNome2: TQRLabel;
    QRShape19: TQRShape;
    QRShape17: TQRShape;
    QRLabel12: TQRLabel;
    QRShape18: TQRShape;
    QRLabel19: TQRLabel;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel20: TQRLabel;
    QRShape22: TQRShape;
    lblLocal1: TQRLabel;
    lblLocal2: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape23: TQRShape;
    QRLabel25: TQRLabel;
    QRShape24: TQRShape;
    QRLabel26: TQRLabel;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    lblProjeto1: TQRLabel;
    lblProjeto2: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    lbDataDesligamento1: TQRLabel;
    lbDataDesligamento2: TQRLabel;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRLabel33: TQRLabel;
    QRImage2: TQRImage;
  private

  public

  end;

var
  relNovoDesligamento: TrelNovoDesligamento;

implementation

{$R *.DFM}

end.
