object frmObservacaoAprendizagem: TfrmObservacaoAprendizagem
  Left = 337
  Top = 203
  BorderStyle = bsDialog
  Caption = '[Sistema DECA]  -  Observa��o de Aprendizagem'
  ClientHeight = 500
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 6
    Top = 4
    Width = 632
    Height = 49
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 59
      Height = 13
      Caption = 'Matr�cula:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNome: TLabel
      Left = 216
      Top = 19
      Width = 262
      Height = 18
      Caption = 'NOME DA CRIAN�A / ADOLESCENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnPesquisar: TSpeedButton
      Left = 169
      Top = 14
      Width = 28
      Height = 24
      Hint = 'Clique para LOCALIZAR CRIAN�A/ADOLESCENTE'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777770000777777777777777700770000777777777777777C440700007777
        7777777777C4C40700007777777777777C4C4077000077777777777784C40777
        0000777777788888F740777700007777770000007807777700007777707EFEE6
        007777770000777787EFEFEF60777777000077778EFEFEFEE087777700007777
        8FEFEFEFE0877777000077778FFFEFEFE0877777000077778EFFFEFEF0877777
        0000777778EFFFEF6077777797007777778EFEF607777777FD00777777788888
        7777777700A077777777777777777777FFA077777777777777777777FFFF7777
        7777777777777777FF81}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnPesquisarClick
    end
    object txtMatricula: TMaskEdit
      Left = 82
      Top = 16
      Width = 83
      Height = 21
      Hint = 'Informe o N�MERO DE MATR�CULA'
      EditMask = '!99999999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '        '
    end
  end
  object GroupBox2: TGroupBox
    Left = 6
    Top = 54
    Width = 632
    Height = 101
    TabOrder = 1
    object GroupBox3: TGroupBox
      Left = 8
      Top = 13
      Width = 71
      Height = 41
      Caption = 'Nascimento'
      TabOrder = 0
      object lbNascimento: TLabel
        Left = 9
        Top = 18
        Width = 4
        Height = 14
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object GroupBox4: TGroupBox
      Left = 81
      Top = 13
      Width = 332
      Height = 41
      Caption = 'Unidade'
      TabOrder = 1
      object lbUnidade: TLabel
        Left = 10
        Top = 17
        Width = 4
        Height = 14
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object GroupBox5: TGroupBox
      Left = 416
      Top = 13
      Width = 97
      Height = 41
      Caption = '-'
      TabOrder = 2
      object lbPeriodoUnidade: TLabel
        Left = 10
        Top = 18
        Width = 4
        Height = 14
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object GroupBox6: TGroupBox
      Left = 514
      Top = 13
      Width = 98
      Height = 41
      Caption = 'Per�odo escolar'
      TabOrder = 3
      object lbPeriodoEscola: TLabel
        Left = 10
        Top = 18
        Width = 4
        Height = 14
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object GroupBox7: TGroupBox
      Left = 8
      Top = 55
      Width = 177
      Height = 41
      Caption = 'Assistente Social'
      TabOrder = 4
      object txtAsocial: TEdit
        Left = 6
        Top = 14
        Width = 163
        Height = 21
        TabOrder = 0
      end
    end
    object GroupBox8: TGroupBox
      Left = 187
      Top = 55
      Width = 265
      Height = 41
      Caption = 'Escola'
      Enabled = False
      TabOrder = 5
      Visible = False
      object lbEscola: TLabel
        Left = 9
        Top = 18
        Width = 4
        Height = 14
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object GroupBox9: TGroupBox
      Left = 454
      Top = 55
      Width = 159
      Height = 41
      Caption = 'S�rie'
      Enabled = False
      TabOrder = 6
      Visible = False
      object lbSerie: TLabel
        Left = 11
        Top = 18
        Width = 4
        Height = 14
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 6
    Top = 158
    Width = 631
    Height = 163
    Caption = 'Panel1'
    TabOrder = 2
    object PageControl1: TPageControl
      Left = 8
      Top = 9
      Width = 617
      Height = 150
      ActivePage = TabSheet1
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Asp.de compreens�o geral, racioc�nio e atitudes sociais'
        object Label2: TLabel
          Left = 6
          Top = 2
          Width = 354
          Height = 13
          Caption = 
            'Descreva os aspectos de compreens�o geral, racioc�nio e atitudes' +
            ' sociais.'
        end
        object edAspectosGerais: TMemo
          Left = 6
          Top = 21
          Width = 595
          Height = 100
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Express�o Oral e Escrita'
        ImageIndex = 1
        object Label3: TLabel
          Left = 6
          Top = 2
          Width = 239
          Height = 13
          Caption = 'Descreva os aspectos de Express�o Oral e Escrita'
        end
        object edExpressaoOral: TMemo
          Left = 6
          Top = 21
          Width = 595
          Height = 100
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Matem�tica'
        ImageIndex = 2
        object Label6: TLabel
          Left = 6
          Top = 2
          Width = 179
          Height = 13
          Caption = 'Descreva os aspectos de Matem�tica'
        end
        object edMatematica: TMemo
          Left = 6
          Top = 21
          Width = 595
          Height = 100
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Observa��es Espec�ficas'
        ImageIndex = 3
        object Label7: TLabel
          Left = 6
          Top = 2
          Width = 246
          Height = 13
          Caption = 'Descreva os aspectos de Observa��es Espec�ficas'
        end
        object edObservacoes: TMemo
          Left = 6
          Top = 21
          Width = 595
          Height = 100
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 6
    Top = 323
    Width = 631
    Height = 124
    TabOrder = 3
    object dbgResultado: TDBGrid
      Left = 6
      Top = 8
      Width = 619
      Height = 110
      DataSource = dsResultado
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dbgResultadoDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Alignment = taCenter
          Title.Caption = 'Matr�cula'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_nome'
          Title.Caption = 'Nome'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_unidade'
          Title.Caption = 'Unidade'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_escola'
          Title.Caption = 'Escola'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'log_data'
          Title.Caption = 'Inclus�o'
          Width = 150
          Visible = True
        end>
    end
  end
  object Panel3: TPanel
    Left = 5
    Top = 448
    Width = 631
    Height = 49
    TabOrder = 4
    object Label4: TLabel
      Left = 9
      Top = 17
      Width = 49
      Height = 13
      Caption = 'Pesquisar:'
    end
    object btnCancelar: TBitBtn
      Left = 435
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnSair: TBitBtn
      Left = 568
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnNovo: TBitBtn
      Left = 253
      Top = 10
      Width = 54
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 312
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR a advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnAlterar: TBitBtn
      Left = 371
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR a advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnAlterarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
    object btnImprimir: TBitBtn
      Left = 503
      Top = 10
      Width = 55
      Height = 30
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Visible = False
      OnClick = btnImprimirClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDDD7777777777DDDDD0000DDDD
        000000000007DDDD0000DDD07878787870707DDD0000DD0000000000000707DD
        0000DD0F8F8F8AAA8F0007DD0000DD08F8F8F999F80707DD0000DD0000000000
        0008707D0000DD08F8F8F8F8F080807D0000DDD0000000000F08007D0000DDDD
        0BFFFBFFF0F080DD0000DDDDD0F00000F0000DDD0000DDDDD0FBFFFBFF0DDDDD
        0000DDDDDD0F00000F0DDDDD0000DDDDDD0FFBFFFBF0DDDD0000DDDDDDD00000
        0000DDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDD
        DDDDDDDDDDDDDDDD0000}
    end
    object cbPesquisa: TComboBox
      Left = 62
      Top = 14
      Width = 84
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 6
      Items.Strings = (
        'Matr�cula'
        'Nome'
        '<Todos>')
    end
    object edValor: TEdit
      Left = 149
      Top = 14
      Width = 96
      Height = 21
      TabOrder = 7
      OnKeyPress = edValorKeyPress
    end
  end
  object dsResultado: TDataSource
    DataSet = dmDeca.cdsSel_ObsAprendizagem
    Left = 574
    Top = 369
  end
end
