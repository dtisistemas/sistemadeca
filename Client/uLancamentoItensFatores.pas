unit uLancamentoItensFatores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, ExtCtrls, Grids, DBGrids, ComCtrls, Db;

type
  TfrmLancamentoItensFatores = class(TForm)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    dbgItensLancamentos: TDBGrid;
    Panel2: TPanel;
    GroupBox5: TGroupBox;
    meDataRegistro: TMaskEdit;
    GroupBox7: TGroupBox;
    lbStatus: TLabel;
    GroupBox6: TGroupBox;
    meSituacaoApresentada: TMemo;
    rgAvaliacao: TRadioGroup;
    GroupBox4: TGroupBox;
    cbMotivos: TComboBox;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    btnEncerrarAtendimento: TBitBtn;
    dsLanc_Fat_Int: TDataSource;
    Panel4: TPanel;
    Label2: TLabel;
    cbCampo: TComboBox;
    edPesquisa: TEdit;
    btnLocalizar: TSpeedButton;
    btnVerTodos: TSpeedButton;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    dbgItens_Lanc_Fat_Int: TDBGrid;
    Panel7: TPanel;
    btnCancelarH: TBitBtn;
    BitBtn5: TBitBtn;
    btnAlterarH: TBitBtn;
    GroupBox1: TGroupBox;
    meDataRegistroH: TMaskEdit;
    GroupBox2: TGroupBox;
    lbStatusH: TLabel;
    rgAvaliacaoH: TRadioGroup;
    GroupBox3: TGroupBox;
    cbMotivosH: TComboBox;
    GroupBox8: TGroupBox;
    meSituacaoH: TMemo;
    Panel6: TPanel;
    lbIdentificacao: TLabel;
    btnNovoRegistro: TBitBtn;
    btnVerHistorico: TBitBtn;
    Panel8: TPanel;
    lbIdentificacao2: TLabel;
    dsSel_Itens_Lanc_Fat_int: TDataSource;
    btnEncerrarAtendimentoH: TBitBtn;
    btnColocarEsperaH: TBitBtn;
    GroupBox9: TGroupBox;
    meMetaEstabelecida: TMemo;
    btnEncerrarAtendimentos: TBitBtn;
    btn: TSpeedButton;
    Label1: TLabel;
    Memo1: TMemo;
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnVerTodosClick(Sender: TObject);
    procedure btnLocalizarClick(Sender: TObject);
    procedure dbgItensLancamentosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnNovoRegistroClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnVerHistoricoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbgItens_Lanc_Fat_IntKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgItens_Lanc_Fat_IntKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnCancelarHClick(Sender: TObject);
    procedure btnEncerrarAtendimentoClick(Sender: TObject);
    procedure btnColocarEsperaClick(Sender: TObject);
    procedure dbgItensLancamentosDblClick(Sender: TObject);
    procedure PageControl2Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure dbgItensLancamentosCellClick(Column: TColumn);
    procedure dbgItens_Lanc_Fat_IntDblClick(Sender: TObject);
    procedure btnAlterarHClick(Sender: TObject);
    procedure btnEncerrarAtendimentoHClick(Sender: TObject);
    procedure dbgItens_Lanc_Fat_IntDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgItens_Lanc_Fat_IntCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLancamentoItensFatores: TfrmLancamentoItensFatores;
  varPodeMudarAba : Boolean;

  vListaMotivo1, vListaMotivo2 : TStringList;

implementation

uses uDM, uUtil, uLoginNovo, uPrincipal;

{$R *.DFM}

procedure TfrmLancamentoItensFatores.btnSairClick(Sender: TObject);
begin
  frmLancamentoItensFatores.Close;
end;

procedure TfrmLancamentoItensFatores.FormShow(Sender: TObject);
begin

  varPodeMudarAba := False;
  PageControl2.ActivePageIndex := 0;

  //Carrega os dados dos Lan�amentos feitosnos indiv�duos(fam�lias)
  try
    with dmDeca.cdsSel_Lanc_Fat_Int do
    begin
      Close;
      Params.ParamByName('@pe_id_lanc_fat_int').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_cod_fator').Value := Null;
      Params.ParamByName('@pe_dat_entrevista').Value := Null;
      Open;

      dbgItensLancamentos.Refresh;

    end;
  except end;
  
end;

procedure TfrmLancamentoItensFatores.btnVerTodosClick(Sender: TObject);
begin

  //Carrega os dados dos Lan�amentos feitosnos indiv�duos(fam�lias)
  try
    with dmDeca.cdsSel_Lanc_Fat_Int do
    begin
      Close;
      Params.ParamByName('@pe_id_lanc_fat_int').Value := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_cod_fator').Value := Null;
      Params.ParamByName('@pe_dat_entrevista').Value := Null;
      Open;

      dbgItensLancamentos.Refresh;

    end;
  except end;

  cbCampo.ItemIndex := -1;
  edPesquisa.Clear;
   
end;

procedure TfrmLancamentoItensFatores.btnLocalizarClick(Sender: TObject);
begin

  //Pesquisa matr�cula ou nome se j� existe lan�amentos feitos
  try
    with dmDeca.cdsSel_Lanc_Fat_Int do
    begin
      Close;
      Params.ParamByName('@pe_id_lanc_fat_int').Value := Null;

      //Matr�cula
      if cbCampo.ItemIndex = 0 then Params.ParamByName('@pe_cod_matricula').Value := Trim(edPesquisa.Text)
      else Params.ParamByName('@pe_cod_matricula').Value := Null;

      //Nome
      if cbCampo.ItemIndex = 1 then Params.ParamByName('@pe_nom_nome').Value := Trim(edPesquisa.Text) + '%'
      else Params.ParamByName('@pe_nom_nome').Value := Null;

      Params.ParamByName('@pe_cod_fator').Value := Null;
      Params.ParamByName('@pe_dat_entrevista').Value := Null;
      Open;

      dbgItensLancamentos.Refresh;

    end;
  except end;
  
end;

procedure TfrmLancamentoItensFatores.dbgItensLancamentosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

  //Status = 0 (Em Atendimento)
  if (dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('flg_status').Value = 0) then
  begin
    dbgItensLancamentos.Canvas.Font.Color:= clWhite;
    dbgItensLancamentos.Canvas.Brush.Color:= clBlue
  end //Status = 1 (Atendido)
  else if (dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('flg_status').Value = 1) then
  begin
    dbgItensLancamentos.Canvas.Font.Color:= clWhite;
    dbgItensLancamentos.Canvas.Brush.Color:= clGreen
  end //Status = 2 (Aguardando)
  else if (dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('flg_status').Value = 2) then
  begin
    dbgItensLancamentos.Canvas.Font.Color:= clWhite;
    dbgItensLancamentos.Canvas.Brush.Color:= clRed;
  end;

  dbgItensLancamentos.Canvas.FillRect(Rect);
  dbgItensLancamentos.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);
  
end;

procedure TfrmLancamentoItensFatores.btnNovoRegistroClick(Sender: TObject);
//var x,i : Integer;
begin
  //Verificar o status do registro - somente Em Atendimento
  if (dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('flg_status').Value = 0) then
  begin
    lbIdentificacao.Caption := dmDeca.cdsSel_Lanc_Fat_Int.Fields[1].AsString + '- [' +
                               dmDeca.cdsSel_Lanc_Fat_Int.Fields[3].AsString + '] - ' +
                               dmDeca.cdsSel_Lanc_Fat_Int.Fields[2].AsString;
    meDataRegistro.Text := DateToStr(Date());
    lbStatus.Caption := 'EM ATENDIMENTO';
    rgAvaliacao.ItemIndex := 0; //Padr�o PLENO
    cbMotivos.ItemIndex := -1;
    PageControl2.ActivePageIndex := 1;
    varPodeMudarAba := False;
  end;  
end;

procedure TfrmLancamentoItensFatores.PageControl2Change(Sender: TObject);
begin
{
  // S� muda de p�gina se tiver uma registro selecionado no grid
  if (varPodeMudarAba = False) then
  begin
    PageControl2.ActivePageIndex := 0;
  end;
}
end;

procedure TfrmLancamentoItensFatores.btnGravarClick(Sender: TObject);
begin

  varPodeMudarAba := True;
  //Validar entradas de dados e preenchimento dos campos, exceto "Motivos Poss�veis" quando "Avalia��o" for Pleno ou Parcial

  if (Length(meDataRegistro.Text)>0) and (rgAvaliacao.ItemIndex > -1) and (Length(meSituacaoApresentada.Text)>0) then
  begin
    try
      with dmDeca.cdsInc_Itens_Lanc_Fat_Int do
      begin
        Close;
        Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('id_lanc_fat_int').Value;
        Params.ParamByName('@pe_dat_registro').Value := GetValue(meDataRegistro, vtDate);
        Params.ParamByName('@pe_flg_status').Value := 0;  //Em Atendimento como padr�o
        Params.ParamByName('@pe_dsc_situacao').Value := GetValue(meSituacaoApresentada.Text);
        Params.ParamByName('@pe_ind_avaliacao').Value := rgAvaliacao.ItemIndex;
        Params.ParamByName('@pe_ind_motivo').Value := StrToInt(vListaMotivo1.Strings[cbMotivos.ItemIndex]);
        Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
        Execute;

        ShowMessage ('Dados gravados com sucesso !!');

        //Limpa os campos e muda para a p�gina principal
        meDataRegistro.Clear;
        lbStatus.Caption := '';
        rgAvaliacao.ItemIndex := 0;  //Padr�o
        cbMotivos.ItemIndex := -1;

        PageControl2.ActivePageIndex := 0;
        varPodeMudarAba := False;

      end;
    except end;
  end
  else
  begin
    ShowMessage ('Aten��o! Existem campos que devem ser preenchidos. Verifique...');
  end;
end;

procedure TfrmLancamentoItensFatores.btnVerHistoricoClick(Sender: TObject);
begin

  //Carrega os dados dos itens de lan�amentos efetuados
  try
    with dmDeca.cdsSel_Itens_Lanc_Fat_Int do
    begin
      Close;
      Params.ParamByName('@pe_id_item').Value := Null;
      Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('id_lanc_fat_int').Value;
      Params.ParamByName('@pe_nom_nome').Value := Null;
      Params.ParamByName('@pe_dat_registro1').Value := Null;
      Params.ParamByName('@pe_dat_registro2').Value := Null;
      Params.ParamByName('@pe_ind_avaliacao').Value := Null;
      Params.ParamByName('@pe_ind_motivo').Value := Null;
      Open;

      dbgItens_Lanc_Fat_Int.Refresh;

    end;
  except end;

  PageControl2.ActivePageIndex := 2;
  lbIdentificacao2.Caption := dmDeca.cdsSel_Lanc_Fat_Int.Fields[1].AsString + '- [' +
                              dmDeca.cdsSel_Lanc_Fat_Int.Fields[3].AsString + '] - ' +
                              dmDeca.cdsSel_Lanc_Fat_Int.Fields[2].AsString;
  //Habilita o Bot�o ALTERAR...
  btnAlterarH.Enabled := True;

end;

procedure TfrmLancamentoItensFatores.btnCancelarClick(Sender: TObject);
begin
  varPodeMudarAba := True;
  PageControl2.ActivePageIndex := 0;
  varPodeMudarAba := False;
end;

procedure TfrmLancamentoItensFatores.FormCreate(Sender: TObject);
begin

  vListaMotivo1 := TStringList.Create();
  vListaMotivo2 := TStringList.Create();

  //Desabilita o bot�o ALTERAR da aba Hist�rico
  btnAlterarH.Enabled := False;

  //Carrega o combo cbMotivos com os valores da tabela Motivos_Possiveis
  try
    with dmDeca.cdsSel_Motivos_Possiveis do
    begin
      Close;
      Params.ParamByname('@pe_cod_id_motivo').Value := Null;
      Params.ParamByname('@pe_flg_status').Value := 0;  //Somente os ATIVOS
      Open;

      while not eof do
      begin
        vListaMotivo1.Add(IntToStr(FieldByName('cod_id_motivo').Value));
        vListaMotivo2.Add(FieldByName('dsc_motivo').Value);
        cbMotivos.Items.Add(FieldByName('dsc_motivo').Value);
        cbMotivosH.Items.Add(FieldByName('dsc_motivo').Value);
        Next;
      end;
    end;
  except end;
end;

procedure TfrmLancamentoItensFatores.dbgItens_Lanc_Fat_IntKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {
  meDataRegistroH.Text := DateToStr(dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dat_registro').Value);
  lbStatusH.Caption := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('vStatus').Value;
  rgAvaliacaoH.ItemIndex := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('ind_avaliacao').Value;
  cbMotivosH.ItemIndex := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('ind_motivo').Value - 1;
  meSituacaoH.Text := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dsc_situacao').Value;
  }
end;

procedure TfrmLancamentoItensFatores.dbgItens_Lanc_Fat_IntKeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {
  meDataRegistroH.Text := DateToStr(dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dat_registro').Value);
  lbStatusH.Caption := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('vStatus').Value;
  rgAvaliacaoH.ItemIndex := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('ind_avaliacao').Value;
  cbMotivosH.ItemIndex := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('ind_motivo').Value - 1;
  meSituacaoH.Text := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dsc_situacao').Value;
  }
end;

procedure TfrmLancamentoItensFatores.btnCancelarHClick(Sender: TObject);
begin
  varPodeMudarAba := True;
  PageControl2.ActivePageIndex := 0;
  varPodeMudarAba := False;
end;

procedure TfrmLancamentoItensFatores.btnEncerrarAtendimentoClick(
  Sender: TObject);
begin
  //Pede a confirma��o do Usu�rio
  if Application.MessageBox('Encerrar o atendimento selecionado?', 'Encerrar Atendimento', MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin
    //Encerrar o atendimento selecionado e seus respectivos itens lan�ados
    try
      with dmDeca.cdsAlt_Lanc_Fat_Int do
      begin
        Close;
        Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('id_lanc_fat_int').Value;
        Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('cod_matricula').Value;
        Params.ParamByName('@pe_flg_status').Value := 1;
        Execute;

        //Atualiza os ITENS lan�ados a partir deste registro...
        with dmDeca.cdsSel_Itens_Lanc_Fat_Int do
        begin
          Close;
          Params.ParamByName('@pe_id_item').Value := Null;
          Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('id_lanc_fat_int').Value;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Params.ParamByName('@pe_dat_registro1').Value := Null;
          Params.ParamByName('@pe_dat_registro2').Value := Null;
          Params.ParamByName('@pe_ind_avaliacao').Value := Null;
          Params.ParamByName('@pe_ind_motivo').Value := Null;
          Open;

          if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.RecordCount > 0) then
          begin
            while not dmDeca.cdsSel_Itens_Lanc_Fat_Int.eof do
            begin
              with dmDeca.cdsAlt_Itens_Lanc_Fat_Int do
              begin
                Close;
                Params.ParamByName('@pe_id_item').Value := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('id_item').Value;
                Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('id_lanc_fat_int').Value;
                Params.ParamByName('@pe_flg_status').Value := 1;
                Execute;
              end;
              dmDeca.cdsSel_Itens_Lanc_Fat_Int.Next;
            end;
            ShowMessage('Todos os registros de acompanhamento para o item selecionado foram encerrados...');
          end
          else ShowMessage ('N�o h� registros de acompanhamentos gravados...');
        end;

        //Atualiza os dados do grid de Lan�amentos
        with  dmDeca.cdsSel_Lanc_Fat_Int do
        begin
          Close;
          Params.ParamByName('@pe_id_lanc_fat_int').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Params.ParamByName('@pe_cod_fator').Value := Null;
          Params.ParamByName('@pe_dat_entrevista').Value := Null;
          Open;
          dbgItensLancamentos.Refresh;
        end;

        //Atualiza os dados do grid dos itens de Lan�amentos
        with dmDeca.cdsSel_Itens_Lanc_Fat_Int do
        begin
          Close;
          Params.ParamByName('@pe_id_item').Value := Null;
          Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('id_lanc_fat_int').Value;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Params.ParamByName('@pe_dat_registro1').Value := Null;
          Params.ParamByName('@pe_dat_registro2').Value := Null;
          Params.ParamByName('@pe_ind_avaliacao').Value := Null;
          Params.ParamByName('@pe_ind_motivo').Value := Null;
          Open;
          dbgItens_Lanc_Fat_Int.Refresh;
        end;

      end;
    except end;
  end;
end;

procedure TfrmLancamentoItensFatores.btnColocarEsperaClick(
  Sender: TObject);
begin
  //Colocar em espera o atendimento selecionado e seus respectivos itens lan�ados
end;

procedure TfrmLancamentoItensFatores.dbgItensLancamentosDblClick(
  Sender: TObject);
begin

  //Verificar se o status est� como Em Atendimento...
  if (dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('flg_status').Value = 0) then
  begin
    PageControl2.ActivePageIndex := 1;
    varPodeMudarAba := False;

    lbIdentificacao.Caption := '[' + dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('cod_matricula').Value + '] - ' +
                               dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('nom_nome').Value +
                               ' (' + dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('ind_vinculo').Value + ')';

    meDataRegistro.Text := DateToStr(Date());
    lbStatus.Caption := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('vStatus').Value;
  end;

end;

procedure TfrmLancamentoItensFatores.PageControl2Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
{  AllowChange := False;}
end;

procedure TfrmLancamentoItensFatores.dbgItensLancamentosCellClick(
  Column: TColumn);
begin
  meMetaEstabelecida.Clear;
  meMetaEstabelecida.Text := dmDeca.cdsSel_Lanc_Fat_Int.FieldByName('dsc_meta').Value;
end;

procedure TfrmLancamentoItensFatores.dbgItens_Lanc_Fat_IntDblClick(
  Sender: TObject);
begin
  //Verificar o status antes de carregar os dados para a tela - somente em atendimento
  if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('flg_status').Value = 0) then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    rgAvaliacaoH.Enabled := True;
    GroupBox3.Enabled := True;
    GroupBox8.Enabled := True;
    btnEncerrarAtendimentoH.Enabled := False;

    meDataRegistroH.Text := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dat_registro').Value;
    lbStatusH.Caption := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('vStatus').Value;
    rgAvaliacaoH.ItemIndex := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('ind_avaliacao').Value;
    cbMotivosH.ItemIndex := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('ind_motivo').Value - 1;
    meSituacaoH.Text := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dsc_situacao').Value;
  end;
end;

procedure TfrmLancamentoItensFatores.btnAlterarHClick(Sender: TObject);
begin
  btnEncerrarAtendimentoH.Enabled := True;

  meDataRegistroH.Clear;
  GroupBox1.Enabled := False;

  lbStatusH.Caption := '';
  GroupBox2.Enabled := False;

  rgAvaliacaoH.ItemIndex := -1;
  rgAvaliacaoH.Enabled := False;

  cbMotivosH.ItemIndex := -1;
  GroupBox3.Enabled := False;

  meSituacaoH.Clear;
  GroupBox8.Enabled := False;

end;

procedure TfrmLancamentoItensFatores.btnEncerrarAtendimentoHClick(
  Sender: TObject);
begin
  //Alterar o flg_status do atendimento
  //Verifica se o status est� "EM ATENDIMENTO"

  //Se "EM ATENDIMENTO"
  if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('flg_status').Value = 0) then
  begin
    try
      with dmDeca.cdsAlt_Itens_Lanc_Fat_Int do
      begin
        Close;
        Params.ParamByName('@pe_id_item').Value := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('id_item').Value;
        Params.ParamByName('@pe_id_lanc_fat_int').Value := dmDeca.cdsSel_Itens_Lanc_fat_Int.FieldByName('id_lanc_fat_int').Value; 
        Params.ParamByName('@pe_flg_status').Value := 1; //Valor para Status = Atendido
        Execute;

        //Atualiza o grid
        with dmDeca.cdsSel_Lanc_Fat_Int do
        begin
          Close;
          Params.ParamByName('@pe_id_lanc_fat_int').Value := Null;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_nom_nome').Value := Null;
          Params.ParamByName('@pe_cod_fator').Value := Null;
          Params.ParamByName('@pe_dat_entrevista').Value := Null;
          Open;

          dbgItens_Lanc_Fat_Int.Refresh;

        end;

      end;
    except end;
  end
  else if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('flg_status').Value = 1) then
  begin
    ShowMessage ('Item em situa��o : ' + dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('vStatus').Value);
  end;
end;

procedure TfrmLancamentoItensFatores.dbgItens_Lanc_Fat_IntDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

  //Status = 0 (Em Atendimento)
  if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('flg_status').Value = 0) then
  begin
    dbgItens_Lanc_Fat_Int.Canvas.Font.Color:= clWhite;
    dbgItens_Lanc_Fat_Int.Canvas.Brush.Color:= clBlue
  end //Status = 1 (Atendido)
  else if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('flg_status').Value = 1) then
  begin
    dbgItens_Lanc_Fat_Int.Canvas.Font.Color:= clWhite;
    dbgItens_Lanc_Fat_Int.Canvas.Brush.Color:= clGreen
  end //Status = 2 (Aguardando)
  else if (dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('flg_status').Value = 2) then
  begin
    dbgItens_Lanc_Fat_Int.Canvas.Font.Color:= clWhite;
    dbgItens_Lanc_Fat_Int.Canvas.Brush.Color:= clRed;
  end;

  dbgItens_Lanc_Fat_Int.Canvas.FillRect(Rect);
  dbgItens_Lanc_Fat_Int.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);
end;

procedure TfrmLancamentoItensFatores.dbgItens_Lanc_Fat_IntCellClick(
  Column: TColumn);
begin
  meSituacaoH.Clear;
  meSituacaoH.Text := dmDeca.cdsSel_Itens_Lanc_Fat_Int.FieldByName('dsc_situacao').Value;
end;

end.

