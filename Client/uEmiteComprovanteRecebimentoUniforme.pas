unit uEmiteComprovanteRecebimentoUniforme;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, Grids, DBGrids, Db;

type
  TfrmEmiteComprovanteRecebimentoUniforme = class(TForm)
    GroupBox1: TGroupBox;
    cbUnidade: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    cbMes: TComboBox;
    Label3: TLabel;
    mskAno: TMaskEdit;
    btnEmitirComprovante: TSpeedButton;
    GroupBox2: TGroupBox;
    dbgRequisicoes: TDBGrid;
    SpeedButton1: TSpeedButton;
    Label4: TLabel;
    meJustificativa: TMemo;
    dsUniformesRequisicoes: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure btnEmitirComprovanteClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteComprovanteRecebimentoUniforme: TfrmEmiteComprovanteRecebimentoUniforme;
  vListaUnidade1, vListaUnidade2 : TStringList;

implementation

uses rComprovanteRecebimentoUniformes, uDM, uUtil;

{$R *.DFM}

procedure TfrmEmiteComprovanteRecebimentoUniforme.FormShow(
  Sender: TObject);
begin
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de Unidades para uma poss�vel consulta
  //aos dados da frequencia de determinada unidade...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not eof do
      begin

        if (dmDeca.cdsSel_Unidade.FieldByName('flg_status').AsInteger = 1) then
          dmDeca.cdsSel_Unidade.Next
        else
        begin
          vListaUnidade1.Add (IntToStr(FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (FieldByName ('nom_unidade').Value);
          cbUnidade.Items.Add (FieldByName ('nom_unidade').Value);
          dmDeca.cdsSel_Unidade.Next;
        end;
      end;

    end;
  except end;
end;

procedure TfrmEmiteComprovanteRecebimentoUniforme.btnEmitirComprovanteClick(
  Sender: TObject);
begin

  try
    with dmDeca.cdsSel_UniformesRequisicoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_requisicao').Value := Null;
      Params.ParamByName ('@pe_mes_requisicao').Value := cbMes.ItemIndex + 1;
      Params.ParamByName ('@pe_ano_requisicao').Value := StrToInt(mskAno.Text);
      Params.ParamByName ('@pe_cod_unidade').Value    := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;
      dbgRequisicoes.Refresh;
    end;

    //Se n�o est� lan�ado, insere no banco
    if (dmDeca.cdsSel_UniformesRequisicoes.RecordCount = 0) then
    begin
      with dmDeca.cdsIns_UniformesRequisicoes do
      begin
        Close;
        Params.ParamByName ('@pe_dat_requisicao').Value    := Now();
        Params.ParamByName ('@pe_mes_requisicao').Value    := cbMes.ItemIndex + 1;
        Params.ParamByName ('@pe_ano_requisicao').Value    := StrToInt(mskAno.Text);
        Params.ParamByName ('@pe_dsc_justificativa').Value := Trim(meJustificativa.Text);
        Params.ParamByName ('@pe_cod_unidade').Value       := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
        Execute;

        Application.CreateForm (TrelComprovanteRecebimentoUniformes, relComprovanteRecebimentoUniformes);
        with dmDeca.cds_EmitirComprovanteRecebimentoUniforme do
        begin
          Close;
          Params.ParamByName ('@pe_cod_unidade').Value     := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
          Params.ParamByName ('@pe_ano_solicitacao').Value := StrToInt(mskAno.Text);
          Params.ParamByName ('@pe_mes_solicitacao').Value := cbMes.ItemIndex + 1;
          Open;

          if (dmDeca.cds_EmitirComprovanteRecebimentoUniforme.RecordCount > 0) then
          begin
            relComprovanteRecebimentoUniformes.qrlUNIDADE.Caption        := 'UNIDADE ' + cbUnidade.Text;
            relComprovanteRecebimentoUniformes.qrlPERIODO.Caption        := 'PER�ODO: ' + cbMes.Text + ' / ' + mskAno.Text;
            relComprovanteRecebimentoUniformes.qrlRequisicao.Caption     := 'REQUISI��O N.� ' + IntToStr(dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('cod_requisicao').Value);
            relComprovanteRecebimentoUniformes.qrlDataRequisicao.Caption := DateToStr(dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('data_requisicao').AsDateTime);
            relComprovanteRecebimentoUniformes.Preview;
            relComprovanteRecebimentoUniformes.Free;
          end;
        end;
      end;
    end
    else
    begin
      Application.MessageBox ('Aten��o!!! ' +#13+#10 +
                              'A requisi��o para a Unidade e per�odo informado j� est� lan�ada no banco de dados.' +#13+#10 +
                              'Verifique na tabela abaixo, selecione-a e clique em Visualizar Requisi��o',
                              '[Sistema Deca] - Requisi��o de Compra',
                              MB_OK + MB_ICONERROR);
    end;
  except end;
end;

procedure TfrmEmiteComprovanteRecebimentoUniforme.SpeedButton1Click(
  Sender: TObject);
begin
  Application.CreateForm (TrelComprovanteRecebimentoUniformes, relComprovanteRecebimentoUniformes);
  with dmDeca.cds_EmitirComprovanteRecebimentoUniforme do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value     := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
    Params.ParamByName ('@pe_ano_solicitacao').Value := dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('ano_requisicao').Value;
    Params.ParamByName ('@pe_mes_solicitacao').Value := dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('mes_requisicao').Value;
    Open;

    if (dmDeca.cdsSel_UniformesRequisicoes.RecordCount > 0) then
    begin
      relComprovanteRecebimentoUniformes.qrlUNIDADE.Caption        := 'UNIDADE ' + cbUnidade.Text;
      relComprovanteRecebimentoUniformes.qrlPERIODO.Caption        := 'PER�ODO: ' + IntToStr(dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('mes_requisicao').Value) + ' / ' + IntToStr(dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('ano_requisicao').Value);
      relComprovanteRecebimentoUniformes.qrlRequisicao.Caption     := 'REQUISI��O N.� ' + IntToStr(dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('cod_requisicao').Value);
      relComprovanteRecebimentoUniformes.qrlDataRequisicao.Caption := DateToStr(dmDeca.cdsSel_UniformesRequisicoes.FieldByName ('data_requisicao').AsDateTime);
      relComprovanteRecebimentoUniformes.Preview;
      relComprovanteRecebimentoUniformes.Free;
    end;
  end;
end;

procedure TfrmEmiteComprovanteRecebimentoUniforme.cbUnidadeClick(
  Sender: TObject);
begin
    with dmDeca.cdsSel_UniformesRequisicoes do
    begin
      Close;
      Params.ParamByName ('@pe_cod_requisicao').Value := Null;
      Params.ParamByName ('@pe_mes_requisicao').Value := Null;
      Params.ParamByName ('@pe_ano_requisicao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value    := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;
      dbgRequisicoes.Refresh;
    end;
end;

end.
