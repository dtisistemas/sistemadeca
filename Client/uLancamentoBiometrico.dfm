object frmLancamentoBiometrico: TfrmLancamentoBiometrico
  Left = 288
  Top = 237
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [Lan�amento de Dados Biom�tricos]'
  ClientHeight = 474
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 7
    Top = 423
    Width = 705
    Height = 49
    TabOrder = 0
    object btnNovo: TBitBtn
      Left = 239
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para NOVA advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777770000000000777700007777
        778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
        00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
        FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
        F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
        0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
        777777770000777F778F778F7777777700007777778F77777777777700007777
        77777777777777770000}
    end
    object btnSalvar: TBitBtn
      Left = 306
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para GRAVAR a advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777000000000000077700007770
        4407447770440777000077704407447770440777000077704407447770440777
        0000777044477777744407770000777044444444444407770000777044000000
        004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
        40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
        0000777040FFFFFFFF0407770000777000000000000007770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnCancelar: TBitBtn
      Left = 430
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para CANCELAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnCancelarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        7777777777777777000077777777777777777777000077777777777777777777
        000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
        7848F777000077784444F7777784F77700007778444F77777784F77700007778
        4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
        0000777777777788887777770000777777777777777777770000777777777777
        7777777700007777777777777777777700007777777777777777777700007777
        77777777777777770000}
    end
    object btnImprimir: TBitBtn
      Left = 491
      Top = 10
      Width = 132
      Height = 30
      Hint = 'Clique para IMPRIMIR Gr�fico com as Medi��es Biom�trico'
      Caption = 'Gr�fico Biom�trico'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F0000000120B0000120B00001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777776B00777777777000777777776E007777777007880077777771007777
        7007780088007777740077770778878800880077770077778887778888008077
        7A00777887777788888800777D007778F7777F888888880780007778F77FF777
        8888880783007778FFF779977788880786007778F77AA7778807880789007777
        88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
        920077777777778FFFFFF0079500777777777778FFF887779800777777777777
        888777779B00777777777777777777779E0077777777777777777777A1007777
        7777777777777777A400}
    end
    object btnSair: TBitBtn
      Left = 628
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para SAIR da tela de advert�ncia'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnSairClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object btnAlterar: TBitBtn
      Left = 368
      Top = 10
      Width = 55
      Height = 30
      Hint = 'Clique para ALTERAR opera��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Visible = False
      OnClick = btnCancelarClick
    end
  end
  object PageControl1: TPageControl
    Left = 7
    Top = 5
    Width = 706
    Height = 417
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Lan�amento de Avalia��o Biom�trica'
      object GroupBox1: TGroupBox
        Left = 4
        Top = 0
        Width = 688
        Height = 49
        TabOrder = 0
        object Label9: TLabel
          Left = 14
          Top = 20
          Width = 52
          Height = 13
          Caption = 'Unidade:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cbUnidade: TComboBox
          Left = 73
          Top = 16
          Width = 259
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 0
          OnClick = cbUnidadeClick
        end
      end
      object Panel3: TPanel
        Left = 4
        Top = 206
        Width = 340
        Height = 166
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object Label5: TLabel
          Left = 21
          Top = 10
          Width = 23
          Height = 14
          Caption = 'Data'
        end
        object Label4: TLabel
          Left = 228
          Top = 13
          Width = 33
          Height = 14
          Caption = 'Altura'
        end
        object Label3: TLabel
          Left = 130
          Top = 13
          Width = 28
          Height = 14
          Caption = 'Peso'
        end
        object btnCalcular: TSpeedButton
          Left = 126
          Top = 116
          Width = 117
          Height = 37
          Caption = 'Calcular IMC'
          Glyph.Data = {
            46050000424D460500000000000036040000280000000D000000110000000100
            08000000000010010000C30E0000C30E00000001000000000000000000008080
            8000000080000080800000800000808000008000000080008000408080004040
            0000FF80000080400000FF00400000408000FFFFFF00C0C0C0000000FF0000FF
            FF0000FF0000FFFF0000FF000000FF00FF0080FFFF0080FF0000FFFF8000FF80
            80008000FF004080FF00C0DCC000F0CAA60060208000C0FFFF00E0E0A0008000
            60008080FF00C0800000FFC0C000FFCF0000FFFF6900E0FFE000B39CDD00EE8F
            B300F96F2A00CDB83F0036844800418C9500425E8E007A62A000AC4F6200BE2F
            1D007666280000450000013E450013286A006A39850085324A00040404000808
            08000C0C0C0011111100161616001C1C1C002222220029292900303030005F5F
            5F00555555004D4D4D0042424200393939000007000000000D008199B700B499
            840090BDBD00607F7F007F606000000E000000001B00000028002B090800001D
            0000000039009B00000000250000000049003B111100002F000000005D004517
            1700003A000011114900531C1C00FF160000FF002B006C212100141459000051
            00006A1A47006732190000610000FF310000FF0061007B20530067431600E22E
            2E001659260004465100492E68008F520700B8186A0015239000FF530000FF00
            A300124A6A006C3375009A414A000B653700152CA400B11F8300FF2C4E00B651
            2000926408000B566F00AD435900127236001733B00000A100001F5F77007147
            89001C43B0007D2DB70095860000236E7A00009F260001A9730000CA0000015B
            AC00C21D2000705294004CAA240089940A007B6E360090754400A800FF00FF71
            0000FF00DF004A915600F84834008232CC007041E40001CA680042BC3600FF9A
            0000B7229600337D85008CB72500ED5A360000FF5C000048FF00A29B22004DCF
            42005258C20095D32000E024A500B556730000A9A9003C6FD000589F67000BCF
            890000ACFF00FE2EA7007F59E20067DC4C00FF18FF00FF7D3A0018D0B10000FF
            C70000E2FF003D9ADF009F815600BA43C6008B71AF00C9A23800CE53D100659A
            FF00DBCA4600FF4DFF006AE9C800E0DE4C00FF98FF0082C0DF00A5ECE900CDF6
            F500FFD0FF005AACB100AE916300654C22003F4E8D0070705000FFFFD000FFE7
            FF00696969007777770086868600969696009D9D9D00A4A4A400B2B2B200CBCB
            CB00D7D7D700DDDDDD00E3E3E300EAEAEA00F1F1F100F8F8F80066C1B20078BF
            8000F0F0C600FFA4B200FFB3FF00A38ED10037DCC300549EA00070AE7600C19E
            7800BF648300D383A400323FD100007DFF0023784400605F24002C0E0E000000
            BE00001FFF00003931003E85D9008577020081D8B0001D21560030000000B3C8
            88000079A0008170EA0069F15100CD749100FF7CFF00FFFFA200F0FBFF00A4A0
            A000537F200029798A00326932007F05EC00AC112F00423FEE000F0F0F0F0001
            0F0F0F0F0F0F0F0000000F0F0F0F0000010F0F0F0F0F0F0000000F0F0F0F0F00
            00010F0F0F0F0F0000000F0F0F0F0F001100010F0F0F0F0000000F0F0F000000
            001100010F0F0F0000000F0F0F0011110E111100010F0F0000000F0F0F0F000E
            11000000000F0F0000000F0F0F0F00110E1100010F0F0F0000000F0000000000
            110E1100010F0F0000000F000E110E110E110E1100010F0000000F0F000E110E
            11000000000F0F0000000F0F00110E110E1100010F0F0F0000000F0F0F00110E
            110E1100010F0F0000000F0F0F000E0E0E110E0E00010F0000000F0F0F0F000E
            110E0E110E00010000000F0F0F0F0000000000000000000000000F0F0F0F0F0F
            0F0F0F0F0F0F0F000000}
          OnClick = btnCalcularClick
        end
        object txtData: TMaskEdit
          Left = 48
          Top = 7
          Width = 75
          Height = 22
          Hint = 'Informe a DATA da Aplica��o da Advert�ncia'
          EditMask = '99/99/9999;1; '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '  /  /    '
        end
        object GroupBox2: TGroupBox
          Left = 16
          Top = 34
          Width = 309
          Height = 74
          Caption = 'Valor e Classifica��o do IMC'
          TabOrder = 1
          object lbIMC: TLabel
            Left = 11
            Top = 20
            Width = 5
            Height = 15
            Caption = '*'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbClassificacao: TLabel
            Left = 11
            Top = 44
            Width = 5
            Height = 15
            Caption = '*'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object txtPeso: TEdit
          Left = 162
          Top = 9
          Width = 61
          Height = 22
          TabOrder = 2
        end
        object txtAltura: TEdit
          Left = 266
          Top = 9
          Width = 59
          Height = 22
          TabOrder = 3
        end
      end
      object Panel4: TPanel
        Left = 345
        Top = 206
        Width = 346
        Height = 166
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        object Label8: TLabel
          Left = 10
          Top = 87
          Width = 73
          Height = 14
          Caption = 'Observa��es'
        end
        object chkProblemasSaude: TCheckBox
          Left = 8
          Top = 6
          Width = 206
          Height = 15
          Caption = 'Apresenta PROBLEMAS DE SA�DE'
          TabOrder = 0
          OnClick = chkProblemasSaudeClick
        end
        object txtProblemasSaude: TMemo
          Left = 8
          Top = 28
          Width = 321
          Height = 54
          Hint = 'Informe o MOTIVO da advert�ncia'
          ParentShowHint = False
          ScrollBars = ssVertical
          ShowHint = True
          TabOrder = 1
        end
        object txtObservacoes: TMemo
          Left = 8
          Top = 101
          Width = 321
          Height = 58
          Hint = 'Informe o MOTIVO da advert�ncia'
          ParentShowHint = False
          ScrollBars = ssVertical
          ShowHint = True
          TabOrder = 2
        end
      end
      object dbgCadastro: TDBGrid
        Left = 4
        Top = 53
        Width = 325
        Height = 148
        DataSource = dsCadastros
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = dbgCadastroCellClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'cod_matricula'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'Matr�cula'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nom_nome'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            Title.Caption = 'Nome'
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Val_Peso'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            Title.Caption = 'Peso'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Val_Altura'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            Title.Caption = 'Altura'
            Width = 45
            Visible = True
          end>
      end
      object dbgBiometricos: TDBGrid
        Left = 332
        Top = 53
        Width = 359
        Height = 149
        DataSource = dsBiometricos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'log_data'
            Title.Caption = 'Data'
            Width = 65
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'cod_matricula'
            Title.Alignment = taCenter
            Title.Caption = 'Matricula'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vPeso'
            Title.Caption = 'Peso'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vAltura'
            Title.Caption = 'Altura'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dsc_classificacao_imc'
            Title.Caption = 'Classificacao'
            Width = 90
            Visible = True
          end>
      end
    end
  end
  object dsBiometricos: TDataSource
    DataSet = dmDeca.cdsSel_Biometricos
    Left = 629
    Top = 37
  end
  object dsCadastros: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro
    Left = 595
    Top = 37
  end
end
