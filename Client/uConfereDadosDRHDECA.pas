unit uConfereDadosDRHDECA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, jpeg;

type
  TfrmConfereDadosDRHDECA = class(TForm)
    Image1: TImage;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    btnBuscar: TSpeedButton;
    btnImportar: TSpeedButton;
    Bevel1: TBevel;
    edCaminhoArquivo: TEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure btnBuscarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfereDadosDRHDECA: TfrmConfereDadosDRHDECA;
  arqCONFERE : TextFile;
  mmLINHA, strNOME : String;
  strLIST : TStringList;

implementation

uses uDM, uUtil;

{$R *.DFM}

procedure TfrmConfereDadosDRHDECA.btnBuscarClick(Sender: TObject);
begin
  ShowMessage ('Informe o local do arquivo texto gerado pelo Sistema Deca...');
  OpenDialog1.Execute;
  edCaminhoArquivo.Text := OpenDialog1.FileName;
  btnBuscar.Enabled   := False;
  btnImportar.Enabled := True;
end;

end.
