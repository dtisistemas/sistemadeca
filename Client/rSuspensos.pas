unit rSuspensos;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelSuspensos = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    lbTitulo: TQRLabel;
    QRLabel1: TQRLabel;
    lbUnidade: TQRLabel;
    lbPeriodo: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape5: TQRShape;
    QRShape7: TQRShape;
    DetailBand1: TQRBand;
    QRShape6: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRShape2: TQRShape;
    QRShape4: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRShape8: TQRShape;
    SummaryBand1: TQRBand;
    QRLabel7: TQRLabel;
    QRExpr1: TQRExpr;
  private

  public

  end;

var
  relSuspensos: TrelSuspensos;

implementation

uses uDM;

{$R *.DFM}

end.
