object frmLancarFaltas_Teste: TfrmLancarFaltas_Teste
  Left = 372
  Top = 227
  Width = 826
  Height = 527
  Caption = '[Sistema Deca] - Lan�amento de Faltas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 817
    Height = 497
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Teste Selecionando Matricula/Turma'
      object rgCriterio: TRadioGroup
        Left = 2
        Top = 4
        Width = 133
        Height = 61
        Caption = 'Crit�rio'
        ItemIndex = 0
        Items.Strings = (
          'Matr�cula/Nome'
          'Turma')
        TabOrder = 0
        OnClick = rgCriterioClick
      end
      object GroupBox1: TGroupBox
        Left = 137
        Top = 4
        Width = 348
        Height = 61
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 24
          Width = 81
          Height = 13
          Caption = 'Valor a pesquisar'
        end
        object btnPesquisar: TSpeedButton
          Left = 260
          Top = 22
          Width = 23
          Height = 22
        end
        object edPesquisa: TEdit
          Left = 96
          Top = 23
          Width = 156
          Height = 21
          TabOrder = 0
        end
        object cbTurma: TComboBox
          Left = 96
          Top = 23
          Width = 156
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 1
          OnClick = cbTurmaClick
        end
      end
      object Panel1: TPanel
        Left = 483
        Top = 8
        Width = 326
        Height = 311
        TabOrder = 2
        object Label3: TLabel
          Left = 104
          Top = 24
          Width = 102
          Height = 16
          Caption = 'Informe a Data'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 110
          Top = 96
          Width = 94
          Height = 16
          Caption = 'Tipo de Falta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnLancarFalta: TSpeedButton
          Left = 80
          Top = 168
          Width = 153
          Height = 49
          Caption = 'Lan�ar Falta'
        end
        object DateTimePicker1: TDateTimePicker
          Left = 70
          Top = 48
          Width = 180
          Height = 21
          CalAlignment = dtaLeft
          Date = 39707.4247873264
          Time = 39707.4247873264
          DateFormat = dfShort
          DateMode = dmComboBox
          Kind = dtkDate
          ParseInput = False
          TabOrder = 0
        end
        object cbTipoFalta: TComboBox
          Left = 70
          Top = 128
          Width = 180
          Height = 22
          Style = csOwnerDrawFixed
          ItemHeight = 16
          TabOrder = 1
          Items.Strings = (
            'F - Falta'
            'J - Falta Justificada'
            'C - Compensado'
            'R - Feriado')
        end
      end
      object Panel2: TPanel
        Left = 4
        Top = 71
        Width = 481
        Height = 248
        TabOrder = 3
        object Bevel1: TBevel
          Left = 9
          Top = 8
          Width = 464
          Height = 25
          Style = bsRaised
        end
        object Label2: TLabel
          Left = 16
          Top = 14
          Width = 182
          Height = 13
          Caption = 'Selecione o(s) adolescente(s) abaixo...'
        end
        object cklCadastro: TCheckListBox
          Left = 8
          Top = 39
          Width = 465
          Height = 201
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 290
        Width = 809
        Height = 179
        TabOrder = 4
        object DBGrid1: TDBGrid
          Left = 8
          Top = 8
          Width = 793
          Height = 162
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Teste Selecionando Turma, Ano e M�s'
      ImageIndex = 1
      object Label5: TLabel
        Left = 6
        Top = 43
        Width = 49
        Height = 16
        Caption = 'Turma:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 306
        Top = 42
        Width = 32
        Height = 16
        Caption = 'Ano:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 438
        Top = 43
        Width = 34
        Height = 16
        Caption = 'M�s:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid2: TDBGrid
        Left = 8
        Top = 88
        Width = 785
        Height = 372
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object cbTurma2: TComboBox
        Left = 61
        Top = 41
        Width = 236
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 1
        OnClick = cbTurma2Click
      end
      object edAno: TEdit
        Left = 339
        Top = 37
        Width = 65
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object UpDown1: TUpDown
        Left = 403
        Top = 37
        Width = 16
        Height = 25
        Min = 0
        Position = 0
        TabOrder = 3
        Wrap = False
      end
      object cbMes: TComboBox
        Left = 474
        Top = 41
        Width = 190
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 4
        OnClick = cbTurma2Click
        Items.Strings = (
          'JANEIRO'
          'FEVEREIRO'
          'MAR�O'
          'ABRIL'
          'MAIO'
          'JUNHO'
          'JULHO'
          'AGOSTO'
          'SETEMBRO'
          'OUTUBRO'
          'NOVEMBRO'
          'DEZEMBRO')
      end
      object ToolBar1: TToolBar
        Left = 0
        Top = 0
        Width = 809
        Height = 29
        Caption = 'ToolBar1'
        Flat = True
        TabOrder = 5
        object ToolButton1: TToolButton
          Left = 0
          Top = 0
          Caption = '123'
          ImageIndex = 0
          Indeterminate = True
          OnClick = ToolButton1Click
        end
        object ToolButton2: TToolButton
          Left = 23
          Top = 0
          Caption = '456'
          ImageIndex = 1
          Indeterminate = True
        end
        object ToolButton3: TToolButton
          Left = 46
          Top = 0
          Caption = '789'
          ImageIndex = 2
          Indeterminate = True
        end
      end
    end
  end
  object DataSource1: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Frequencia
    Left = 756
    Top = 128
  end
end
