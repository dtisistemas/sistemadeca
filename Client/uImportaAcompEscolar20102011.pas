unit uImportaAcompEscolar20102011;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, jpeg, Buttons, Grids, DBGrids, Db, ComCtrls;

type
  TfrmImportaAcompEscolar20102011 = class(TForm)
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    mskAnoOrigem: TMaskEdit;
    Label2: TLabel;
    cbBimestreOrigem: TComboBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    mskAnoDestino: TMaskEdit;
    cbBimestreDestino: TComboBox;
    DBGrid1: TDBGrid;
    dsSel_AcompEscolar: TDataSource;
    btnIniciarImportacao: TSpeedButton;
    ProgressBar1: TProgressBar;
    procedure FormShow(Sender: TObject);
    procedure btnIniciarImportacaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaAcompEscolar20102011: TfrmImportaAcompEscolar20102011;

implementation

uses uDM, uUtil, uPrincipal;

{$R *.DFM}

procedure TfrmImportaAcompEscolar20102011.FormShow(Sender: TObject);
begin
  mskAnoOrigem.Text := Copy(DateToStr(Date()),7,4);
  cbBimestreOrigem.ItemIndex := 2;
  mskAnoDestino.Text := IntToStr(StrToInt(mskAnoOrigem.Text)+1);
  cbBimestreDestino.ItemIndex := 0;
end;

procedure TfrmImportaAcompEscolar20102011.btnIniciarImportacaoClick(
  Sender: TObject);
begin

  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := 0;
      Open;

      ProgressBar1.Position := 0;
      ProgressBar1.Max := dmDeca.cdsSel_Cadastro.RecordCount;

      while not (dmDeca.cdsSel_Cadastro.eof) do
      begin

        //Excluir da consulta as unidades "inv�lidas"
        if (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 1) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 2) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 3) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 7) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 9) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 12) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 13) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 20) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 27) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 40) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 42) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 43) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 47) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 48) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 50) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 71) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 73) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 77) and
           (dmDeca.cdsSel_Cadastro.FieldByName ('cod_unidade').Value <> 79) then
        begin

          //Consulta os dados do Acompanhamento Escolar para a matr�cula atual...
          with dmDeca.cdsSel_AcompEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_acompescolar').Value   := Null;
            Params.ParamByName ('@pe_cod_matricula').Value         := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
            Params.ParamByName ('@pe_num_ano_letivo').Value        := 2017;
            Params.ParamByName ('@pe_num_bimestre').Value          := 3;
            Params.ParamByName ('@pe_cod_unidade').Value           := Null;
            Params.ParamByName ('@pe_flg_defasagem_escolar').Value := Null;
            Open;

            //Se achou, insere os dados no Banco de Dados -> Tabela ACOMPESCOLAR...
            if dmDeca.cdsSel_AcompEscolar.RecordCount > 0 then
            begin

              with dmDeca.cdsInc_AcompEscolar do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value       := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_num_ano_letivo').Value      := 2018; //GetValue(mskAnoDestino.Text);
                Params.ParamByName ('@pe_num_bimestre').Value        := 1;    //cbBimestreDestino.ItemIndex + 1;
                Params.ParamByName ('@pe_cod_escola').Value          := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
                Params.ParamByName ('@pe_cod_id_serie').Value        := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_id_serie').Value;
                Params.ParamByName ('@pe_ind_frequencia').Value      := 2; //dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_frequencia').Value;
                Params.ParamByName ('@pe_ind_aproveitamento').Value  := 3; //dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_aproveitamento').Value;
                Params.ParamByName ('@pe_ind_situacao').Value        := 8; //dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_situacao').Value;//8;  //Inicializar como "<Aguardando Informa��o>"
                Params.ParamByName ('@pe_ind_periodo_escolar').Value := dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value;  //4;//dmDeca.cdsSel_AcompEscolar.FieldByName ('ind_periodo_escolar').Value;

                //A pedido da Silvia, na data de 13/dez/2012, as observa��es para o 1.� bimestre/2013 devem ser inseridas com o valor "em branco"
                if (dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').IsNull) then
                  Params.ParamByName ('@pe_dsc_observacoes').Value   := Null
                else
                  Params.ParamByName ('@pe_dsc_observacoes').Value   := '-';//dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_observacoes').Value;

                Params.ParamByName ('@pe_cod_usuario').Value         := 15;
                Params.ParamByName ('@pe_nom_turma').Value           := '-';//dmDeca.cdsSel_AcompEscolar.FieldByName ('nom_turma').Value; //'-';

                Execute;

                //ATUALIZA O HIST�RICO DE ESCOLAS DE ACORDO COM A ESCOLARIDADE DO 3.� BIMESTRE DE 2015
                //ATRIBUI PARA TODOS OS REGISTROS O STATUS DE ESCOLA ANTERIOR
                //INSERE NOVOS REGISTROS COM STATUS DE ESCOLA ATUAL
                with dmDeca.cdsAlt_DadosUltimaEscolaHistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_flg_status').Value    := 1; //1 para escolas anteriores
                  Execute;
                end;

                //Incluir os dados da escola atual no HistoricoEscola
                with dmDeca.cdsInc_HistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value   := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;;
                  Params.ParamByName ('@pe_cod_escola').Value      := dmDeca.cdsSel_AcompEscolar.FieldByName ('cod_escola').Value;
                  Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                  Params.ParamByName ('@pe_flg_status').Value      := 0;  //Para "Escola Atual"
                  Params.ParamByName ('@pe_dsc_serie').Value       := dmDeca.cdsSel_AcompEscolar.FieldByName ('dsc_serie').Value;
                  //Inclu�dos na mudan�a de 26-mai-2009, os campos NUM_ANO_LETIVO e DSC_RG_ESCOLAR
                  Params.ParamByName ('@pe_num_ano_letivo').Value  := 2018;
                  Execute;
                end;

              end;
            end
            else
            //Se n�o achou lan�amentos para o bimestre/ano, insere os valores padr�es de inicializa��o...
            begin
              with dmDeca.cdsInc_AcompEscolar do
              begin
                Close;
                Params.ParamByName ('@pe_cod_matricula').Value       := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                Params.ParamByName ('@pe_num_ano_letivo').Value      := 2018; //GetValue(mskAnoDestino.Text);
                Params.ParamByName ('@pe_num_bimestre').Value        := 1;    //cbBimestreDestino.ItemIndex + 1;
                Params.ParamByName ('@pe_cod_escola').Value          := 209; //C�digo para SEM ESCOLA
                Params.ParamByName ('@pe_cod_id_serie').Value        := 44;  //C�digo para SEM INFORMACAO DE SERIE
                Params.ParamByName ('@pe_ind_frequencia').Value      := 2;   //Sem informa��o
                Params.ParamByName ('@pe_ind_aproveitamento').Value  := 3;   //Sem informa��o
                Params.ParamByName ('@pe_ind_situacao').Value        := 8;   //Inicializar como "<Aguardando Informa��o>"
                Params.ParamByName ('@pe_ind_periodo_escolar').Value := 4;   //Sem informa��o
                Params.ParamByName ('@pe_dsc_observacoes').Value     := '-';
                Params.ParamByName ('@pe_cod_usuario').Value         := 15;
                Params.ParamByName ('@pe_nom_turma').Value           := '-';
                Execute;

                //ATUALIZA O HIST�RICO DE ESCOLAS
                //ATRIBUI PARA TODOS OS REGISTROS O STATUS DE ESCOLA ANTERIOR
                //INSERE NOVOS REGISTROS COM STATUS DE ESCOLA ATUAL
                with dmDeca.cdsAlt_DadosUltimaEscolaHistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;
                  Params.ParamByName ('@pe_flg_status').Value    := 1; //1 para escolas anteriores
                  Execute;
                end;

                //Incluir os dados da escola atual no HistoricoEscola
                with dmDeca.cdsInc_HistoricoEscola do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value   := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').Value;;
                  Params.ParamByName ('@pe_cod_escola').Value      := 209;
                  Params.ParamByName ('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
                  Params.ParamByName ('@pe_flg_status').Value      := 0;  //Para "Escola Atual"
                  Params.ParamByName ('@pe_dsc_serie').Value       := 44;
                  //Inclu�dos na mudan�a de 26-mai-2009, os campos NUM_ANO_LETIVO e DSC_RG_ESCOLAR
                  Params.ParamByName ('@pe_num_ano_letivo').Value  := 2018;
                  Execute;
                end;


              end;
            end; //else... se n�o achou

           end; //if cdsSel_AcompEscolar.RecordCount ....

        end //if cdsSel_Cadastro.FieldByName ('cod_unidade').Value ...
        else
        begin

        end;

        DBGrid1.Refresh;

        ProgressBar1.Position := ProgressBar1.Position + 1;

        //Pr�ximo do cadastro...
        dmDeca.cdsSel_Cadastro.Next;

      end;

      ShowMessage ('Importa��o dos dados do 1.�/2018 realizada...');
      ProgressBar1.Position := 0;

    end;
  except

  end;


end;

end.
