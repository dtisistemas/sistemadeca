unit uDadosDesligamentos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask;

type
  TfrmDadosDesligamento = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    txtDataInicial: TMaskEdit;
    txtDataFinal: TMaskEdit;
    btnVer: TBitBtn;
    btnCancelar: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure txtDataInicialExit(Sender: TObject);
    procedure txtDataFinalExit(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDadosDesligamento: TfrmDadosDesligamento;

implementation

uses uDM, uPrincipal, rDesligados;

{$R *.DFM}

procedure TfrmDadosDesligamento.FormActivate(Sender: TObject);
begin
  btnVer.Enabled := False;
  btnCancelar.Enabled := True;

  txtDataInicial.SetFocus;

  txtDataInicial.Enabled := True;
  txtDataFinal.Enabled := True;
end;

procedure TfrmDadosDesligamento.txtDataInicialExit(Sender: TObject);
begin
  try
    StrToDate(txtDataInicial.Text);
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataInicial.Clear;
      txtDataInicial.SetFocus;
    end;
  end;
end;

procedure TfrmDadosDesligamento.txtDataFinalExit(Sender: TObject);
begin
  try
    StrToDate(txtDataFinal.Text);

    if StrToDate(txtDataFinal.Text) < StrToDate(txtDataInicial.Text) then
        begin
          ShowMessage('Data final do periodo � menor do que a data inicial.' + #13#10#13 +
                      'Favor informe data v�lida para realizar a consulta...');
          txtDataFinal.SetFocus;
        end
    else
      begin
        btnVer.Enabled := True;
        btnVer.SetFocus;
      end;
  except
    begin
      ShowMessage('Data inv�lida...');
      txTDataFinal.Clear;
      txtDataFinal.SetFocus;
    end;
  end;
end;

procedure TfrmDadosDesligamento.btnVerClick(Sender: TObject);
begin
  with dmDeca.cdsSel_Desligados do
  begin
    Close;
    if (vvIND_PERFIL = 18) or (vvIND_PERFIL = 20) or (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25) then  //Perfil Servi�o Socail/Acompanhamento Escolar
      //Params.ParamByName('@pe_cod_unidade').Value := 0
      Params.ParamByName('@pe_cod_unidade').Value := Null
    else
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
    Params.ParamByName('@pe_ind_tipo_historico').Value := 5;
    Params.ParamByName('@pe_data_inicial').Value := StrToDate(txtDataInicial.Text);
    Params.ParamByName('@pe_data_final').Value := StrToDate(txtDataFinal.Text);
    Open;

    Application.CreateForm(TrelDesligados, relDesligados);

    relDesligados.lbPeriodo.Caption := 'DE ' + txtDataInicial.Text + ' AT� ' + txtDataFinal.Text;
    relDesligados.lbUnidade.Caption := vvNOM_UNIDADE;
    relDesligados.Preview;
    relDesligados.Free;

  end;
end;

procedure TfrmDadosDesligamento.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmDadosDesligamento.Close;
  end;
end;

procedure TfrmDadosDesligamento.btnCancelarClick(Sender: TObject);
begin
  btnVer.Enabled := False;
  txtDataInicial.Clear;
  txtDataFinal.Clear;
  txtDataInicial.SetFocus;
end;

end.
