object frmPESQUISA_INSCRITOS_TRIAGEM: TfrmPESQUISA_INSCRITOS_TRIAGEM
  Left = 439
  Top = 251
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = '[Sistema Deca] - Pesquisa de Inscritos Triagem'
  ClientHeight = 546
  ClientWidth = 917
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnCARREGAR_INSCRICAO: TSpeedButton
    Left = 435
    Top = 326
    Width = 450
    Height = 41
    Caption = 'CARREGAR FICHA DE INSCRI��O COMPLETA'
    OnClick = btnCARREGAR_INSCRICAOClick
  end
  object btnNOVA_INSCRICAO: TSpeedButton
    Left = 435
    Top = 369
    Width = 450
    Height = 41
    Caption = 'NOVA INSCRI��O'
    OnClick = btnNOVA_INSCRICAOClick
  end
  object btnEXPORTAR_RELATORIO: TSpeedButton
    Left = 435
    Top = 412
    Width = 450
    Height = 41
    Caption = 'EXPORTAR/GERAR RELAT�RIO'
    Visible = False
  end
  object Label5: TLabel
    Left = 1349
    Top = 460
    Width = 21
    Height = 13
    Caption = 'FHS'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 435
    Top = 455
    Width = 450
    Height = 41
    Caption = 'IMPRIMIR DOCUMENTOS PARA INSCRI��O/RENOVA��O'
    Visible = False
    OnClick = SpeedButton1Click
  end
  object btnCONSULTA_DADOSDECA: TSpeedButton
    Left = 435
    Top = 497
    Width = 450
    Height = 41
    Caption = 'CONSULTAR MATRICULADOS DECA'
    OnClick = btnCONSULTA_DADOSDECAClick
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 5
    Width = 905
    Height = 313
    Caption = '[Cadastro de Inscritos - Triagem]'
    TabOrder = 0
    object lblTOTAL_ENCONTRADO: TLabel
      Left = 760
      Top = 283
      Width = 87
      Height = 13
      Caption = 'Total encontrado: '
    end
    object Label1: TLabel
      Left = 10
      Top = 283
      Width = 235
      Height = 13
      Caption = 'Clique no nome da coluna para ordenar os dados.'
    end
    object dbgRESULTADO: TDBGrid
      Left = 9
      Top = 21
      Width = 882
      Height = 249
      DataSource = dtsPESQUISA_INSCRITOS_TRIAGEM
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnTitleClick = dbgRESULTADOTitleClick
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'NUM_INSCRICAO'
          Title.Alignment = taCenter
          Title.Caption = '[Inscri��o]'
          Width = 75
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'NUM_INSCRICAO_DIGT'
          Title.Alignment = taCenter
          Title.Caption = '[D�gito]'
          Width = 50
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NOM_NOME'
          Title.Caption = '[Nome do Inscrito]'
          Width = 300
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'DSC_POSICAO'
          Title.Caption = '[Situa��o]'
          Width = 200
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'DSC_NOME_FAM'
          Title.Caption = '[Nome da M�e]'
          Width = 300
          Visible = True
        end>
    end
  end
  object gpbBUSCA_RAPIDA: TGroupBox
    Left = 8
    Top = 322
    Width = 401
    Height = 215
    Caption = '[Busca R�pida]'
    TabOrder = 1
    object btnVER_RESULTADOS: TSpeedButton
      Left = 104
      Top = 151
      Width = 155
      Height = 50
      Caption = 'VER RESULTADOS'
      OnClick = btnVER_RESULTADOSClick
    end
    object GroupBox3: TGroupBox
      Left = 9
      Top = 19
      Width = 245
      Height = 50
      Caption = '[Campos de Pesquisa]'
      TabOrder = 0
      object cmbCAMPO_PESQUISA: TComboBox
        Left = 7
        Top = 17
        Width = 230
        Height = 19
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 0
        OnClick = cmbCAMPO_PESQUISAClick
        Items.Strings = (
          'Nome do Inscrito'
          'Nome do Familiar'
          'Inscri��o'
          'Situa��o da Inscri��o')
      end
    end
    object rdgCRITERIO_PESQUISA: TRadioGroup
      Left = 256
      Top = 19
      Width = 126
      Height = 50
      Caption = '[Crit�rio de Pesquisa]'
      ItemIndex = 1
      Items.Strings = (
        'In�cio do Nome'
        'Trecho do Nome')
      TabOrder = 1
      Visible = False
    end
    object gpbCAMPOS_PESQUISA: TGroupBox
      Left = 8
      Top = 87
      Width = 374
      Height = 50
      Caption = '[Valores de Pesquisa]'
      TabOrder = 2
      object Label3: TLabel
        Left = 64
        Top = 22
        Width = 5
        Height = 13
        Caption = '/'
      end
      object cmbSITUACAO_INSCRICAO: TComboBox
        Left = 10
        Top = 20
        Width = 341
        Height = 19
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 0
      end
      object edtVALOR_PESQUISA2: TEdit
        Left = 71
        Top = 19
        Width = 37
        Height = 21
        TabOrder = 2
        OnKeyPress = edtVALOR_PESQUISA2KeyPress
      end
      object edtVALOR_PESQUISA: TEdit
        Left = 10
        Top = 19
        Width = 50
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 1
        OnKeyPress = edtVALOR_PESQUISAKeyPress
      end
    end
  end
  object dtsPESQUISA_INSCRITOS_TRIAGEM: TDataSource
    DataSet = dmTriagemDeca.cdsSel_Pesquisa_Inscritos_Triagem
    Left = 829
    Top = 85
  end
end
