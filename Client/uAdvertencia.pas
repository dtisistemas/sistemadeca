unit uAdvertencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls, Psock, NMsmtp;

type
  TfrmAdvertencia = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    txtOcorrencia: TMemo;
    btnPesquisar: TSpeedButton;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    txtData: TMaskEdit;
    Label3: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure txtMatriculaExit(Sender: TObject);
    procedure txtMatriculaChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes(Modo : String);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtDataExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdvertencia: TfrmAdvertencia;
  
implementation

uses uFichaPesquisa, uPrincipal, uDM, uUtil, rAdvertencias;

{$R *.DFM}

procedure TfrmAdvertencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmAdvertencia.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmAdvertencia.Close;
  end;

end;

procedure TfrmAdvertencia.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;

      end;
    except end;
  end;

end;

procedure TfrmAdvertencia.txtMatriculaChange(Sender: TObject);
begin
  lbNome.Caption := '';
end;

procedure TfrmAdvertencia.btnSairClick(Sender: TObject);
begin
  frmAdvertencia.Close;
end;

procedure TfrmAdvertencia.AtualizaCamposBotoes(Modo : String);
begin
  // atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtOcorrencia.Clear;

    txtMatricula.Enabled := False;
    btnPesquisar.Enabled := False;
    txtData.Enabled := False;
    txtOcorrencia.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;

    btnNovo.SetFocus;
  end

  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;
    txtOcorrencia.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;
    txtData.Enabled := True;
    txtOcorrencia.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnImprimir.Enabled := False;

    txtMatricula.SetFocus;
  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := True;

    btnImprimir.SetFocus;
  end

end;

procedure TfrmAdvertencia.btnSalvarClick(Sender: TObject);
begin

  if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      if StatusCadastro(txtMatricula.Text) = 'Ativo' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 1;
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Advert�ncia';
          Params.ParamByName('@pe_dsc_ocorrencia').Value := GetValue(txtOcorrencia.Text);
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;
        end;
        AtualizaCamposBotoes('Salvar');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;


    except
      //Gravar no arquivo de LOG de conex�o os dados registrados na tela
        arqLog := 'ADV_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                           Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
        AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
        Rewrite (log_file);

        Write   (log_file, txtMatricula.Text);        //Grava a matricula
        Write   (log_file, ' ');
        Write   (log_file, lbNome.Caption);
        Write   (log_file, ' ');
        Write   (log_file, txtData.Text);             //Data do registro
        Write   (log_file, ' ');
        Write   (log_file, 'Aplica��o de Advert�ncia');//Descricao do Tipo de Historico
        Writeln (log_file, ' ');
        Writeln (log_file, Trim(txtOcorrencia.Text)); //Ocorrencia do Historico
        Writeln (log_file, '--------------------------------------------------------------------------------------');

        CloseFile (log_file);

        Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar gravar os dados!'+#13+#10+#13+#10+
                               'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                               'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                               'Sistema DECA - Altera��o de Registro',
                               MB_OK + MB_ICONERROR);
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmAdvertencia.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        txtData.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmAdvertencia.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Novo');
end;

procedure TfrmAdvertencia.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmAdvertencia.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmAdvertencia.txtDataExit(Sender: TObject);
begin
  // testa se a data � v�lida
  try
    StrToDate(txtData.Text);

    if StrToDate(txtData.Text) > Date then
    begin
      ShowMessage('A data n�o pode ser maior que hoje');
      txtData.SetFocus;
    end;

  except
    on E : EConvertError do
    begin
      ShowMessage('Data inv�lida');
      txtData.SetFocus;
    end;
  end;

end;

procedure TfrmAdvertencia.btnImprimirClick(Sender: TObject);
var vvMes : Integer;
begin
  try
    Application.CreateForm(TrelAdvertencias, relAdvertencias);

    relAdvertencias.lblDivisao.Caption := Divisao(txtMatricula.Text);
    relAdvertencias.lblUnidade.Caption := vvNOM_UNIDADE;
    relAdvertencias.lblNome.Caption := lbNome.Caption;
    relAdvertencias.lblOcorrencia.Lines.Add(txtOcorrencia.Text);

    relAdvertencias.lblDia.Caption := Copy(txtData.Text,1,2);
    relAdvertencias.lblMes.Caption := cMeses[StrToInt(Copy(txtData.Text,4,2))];
    relAdvertencias.lblAno.Caption := Copy(txtData.Text,7,4);

    relAdvertencias.Preview;
    relAdvertencias.Free;
    AtualizaCamposBotoes ('Novo');
  except end;
end;

end.
procedure TfrmAdvertencia.NMSMTP1AttachmentNotFound(Filename: String);
begin

end;


