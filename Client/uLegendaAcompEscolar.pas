unit uLegendaAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, StdCtrls, ExtCtrls, Buttons;

type
  TfrmLegendaAcompEscolar = class(TForm)
    Panel1: TPanel;
    btnSair: TSpeedButton;
    Image1: TImage;
    Label1: TLabel;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLegendaAcompEscolar: TfrmLegendaAcompEscolar;

implementation

{$R *.DFM}

procedure TfrmLegendaAcompEscolar.btnSairClick(Sender: TObject);
begin
  frmLegendaAcompEscolar.Close;
end;

end.
