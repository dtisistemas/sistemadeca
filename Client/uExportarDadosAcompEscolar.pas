unit uExportarDadosAcompEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, jpeg, ExtCtrls, Psock, NMsmtp, Buttons, ComCtrls, Excel97, ComObj,
  NMpop3, OleCtrls;

type
  TfrmExportarDadosAcompEscolar = class(TForm)
    GroupBox1: TGroupBox;
    edNomeEscola: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edNomeDiretor: TEdit;
    Panel4: TPanel;
    btnEnviar: TSpeedButton;
    Panel3: TPanel;
    Panel5: TPanel;
    edEmailPara: TEdit;
    Panel6: TPanel;
    edAssunto: TEdit;
    Panel7: TPanel;
    btnAnexar: TSpeedButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    NMSMTP1: TNMSMTP;
    Abrir: TOpenDialog;
    btnExportar: TSpeedButton;
    edEmailOrigem: TEdit;
    SaveDialog1: TSaveDialog;
    rgTipoMensagem: TRadioGroup;
    NMPOP31: TNMPOP3;
    meMensagem: TMemo;
    Button5: TButton;
    Panel1: TPanel;
    edEmailCC: TEdit;
    edAnexo: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnAnexarClick(Sender: TObject);
    procedure rgTipoMensagemClick(Sender: TObject);
    procedure btnEnviarClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmExportarDadosAcompEscolar: TfrmExportarDadosAcompEscolar;
  objExcel2, Sheet2 : Variant;
  lin : Integer;
  mmCAMINHO_PLANILHA : String;

const
  olMailItem = 0;
  
implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmExportarDadosAcompEscolar.FormShow(Sender: TObject);
begin
  edEmailOrigem.Text := 'dadosescolares@fundhas.org.br';
  mmCAMINHO_PLANILHA := '';
end;

procedure TfrmExportarDadosAcompEscolar.btnAnexarClick(Sender: TObject);
begin
  Abrir.Execute;
  edAnexo.Text := Abrir.FileName;
end;

procedure TfrmExportarDadosAcompEscolar.rgTipoMensagemClick(
  Sender: TObject);
begin
  case rgTipoMensagem.ItemIndex of

    0 : begin
      //Confirma��o de Matr�cula / Atestado Escolar
      btnExportar.Enabled := True;
      edAssunto.Text := 'Confirma��o de Matr�cula/Atestado Escolar';
      meMensagem.Clear;
      meMensagem.Lines.Add ('� Dire��o');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Segue planilha com as informa��es das crian�as/adolescentes atendidos pela');
      meMensagem.Lines.Add ('FUNDHAS, para que seja conferida e atualizada com os dados: s�rie/turma/per�odo');
      meMensagem.Lines.Add ('relativos a 2011.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Essas informa��es s�o importantes para o acompanhamento da situa��o escolar,');
      meMensagem.Lines.Add ('efetuar interven��es e adequar projetos �s reais necessidades dos nossos atendidos,');
      meMensagem.Lines.Add ('fortalecendo a rela��o ESCOLA/FUNDHAS.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Agradecemos, desde j�, a sua valiosa colabora��o.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Obs: Favor devolver os dados na pr�pria planilha, at� o dia 25/03/2011,');
      meMensagem.Lines.Add ('no e-mail dadosescolares@fundhas.org.br.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Att.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Equipe Acompanhamento Escolar/FUNDHAS');
      meMensagem.Lines.Add ('Tel: 3932-0528');
      meMensagem.Lines.Add ('     3932-0533 Ramal 616');

    end;

    1 : begin
      //Confirma��o dos Dados do Acompanhamento Escolar - Aproveitamento/Frequ�ncia
      btnExportar.Enabled := True;
      edAssunto.Text := 'Confirma��o dos Dados do Acompanhamento Escolar';
      meMensagem.Clear;
      meMensagem.Lines.Add ('Sr(a) Diretor(a).');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Segue planilha com a rela��o de crian�as e adolescentes da FUNDHAS cadastradas');
      meMensagem.Lines.Add ('em nosso sistema como sendo dessa escola. Pedimos, gentilmente, que informem os valores das colunas');
      meMensagem.Lines.Add ('"APROVEITAMENTO" e "FREQU�NCIA".');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Pedimos que os dados sejam alterados , salvos e devolvidos atrav�s desse mesmo arquivo/planilha e encaminhado');
      meMensagem.Lines.Add ('para o e-mail  dadosescolares@fundhas.org.br.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Desde j� agradecemos a vossa colabora��o e colocamo-nos a disposi��o para quaisquer esclarecimentos.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Att.');
      meMensagem.Lines.Add ('');
      meMensagem.Lines.Add ('Equipe Acompanhamento Escolar/FUNDHAS');
      meMensagem.Lines.Add ('Tel: 3932-0528');
      meMensagem.Lines.Add ('     3932-0533 Ramal 616');
    end;

    2 : begin
      //Em branco
      edAssunto.Text := '';
      meMensagem.Lines.Add ('');
      btnExportar.Enabled := False;
    end;

  end;
end;

procedure TfrmExportarDadosAcompEscolar.btnEnviarClick(Sender: TObject);
var
  Outlook, Thunderbird: OleVariant;
  vMailItem: variant;
begin

  try
    Outlook := GetActiveOleObject('Outlook.Application');

  except
    Outlook := CreateOleObject('Outlook.Application');
  end;

  vMailItem         := Outlook.CreateItem(olMailItem);
  vMailItem.Recipients.Add(Trim(edEmailPara.Text)); // 1o destinat�rio
  vMailItem.Recipients.Add(Trim(edEmailCC.Text)); // 2o destinat�rio
  vMailItem.Subject := Trim(edAssunto.Text); // assunto
  vMailItem.Body    := meMensagem.Lines.Text; // Corpo do e-mail
  if rgTipoMensagem.ItemIndex <> 2 then vMailItem.Attachments.Add(Trim(edAnexo.Text)); // arquivo anexado
  vMailItem.Send;

  VarClear(Outlook);       

  Application.MessageBox ('Seu e-mail foi enviado com sucesso!!!' + #13+#10 +
                          'Verifique a sua caixa de ITENS ENVIADOS para certificar-se do envio.',
                          '[Sistemas Deca] - Envio de e-mail',
                          MB_OK + MB_ICONINFORMATION);
  frmExportarDadosAcompEscolar.Close;
  
end;

procedure TfrmExportarDadosAcompEscolar.btnExportarClick(Sender: TObject);
begin

  if rgTipoMensagem.ItemIndex < 0 then
  begin
    Application.MessageBox ('O TIPO DA MENSAGEM deve ser selecionado antes da gera��o da planilha...',
                            '[Sistema Deca] - Gera��o de Dados... ',
                            MB_OK + MB_ICONWARNING);
  end
  else
  begin

    //Criar a planilha, solicitando o local a ser salvo...
    objExcel2         := CreateOleObject('Excel.Application');
    //objExcel2.Caption := 'Teste';
    objExcel2.Workbooks.Add;
    SaveDialog1.Execute;
    //objExcel2.WorkBooks[1].SaveAs (ExtractFilePAth(Application.ExeName) + 'teste.xlsx');
    objExcel2.WorkBooks[1].SaveAs (SaveDialog1.FileName);
    mmCAMINHO_PLANILHA := SaveDialog1.FileName;
    objExcel2.Visible := False;
    objExcel2.WorkBooks[1].Sheets.Add;

    if rgTipoMensagem.ItemIndex = 0 then
    begin
      Sheet2 := objExcel2.WorkBooks[1].WorkSheets[1];
      //Sheet2.Name := 'Escola : ' + edNomeEscola.Text;
      Sheet2.Cells[1,1].Value := 'RA';
      Sheet2.Cells[1,2].Value := 'Nome do Aluno';
      Sheet2.Cells[1,3].Value := 'Nascimento';
      Sheet2.Cells[1,4].Value := 'Escola';
      Sheet2.Cells[1,5].Value := 'S�rie Escolar';
      Sheet2.Cells[1,6].Value := 'Unidade FUNDHAS';
      Sheet2.Cells[1,7].Value := 'Observa��es';

      //Seleciona os dados do Boletim para o usu�rio atual...
      with dmDeca.cdsSel_Boletim do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value  := Null;
        Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
        Params.ParamByName ('@pe_nom_escola').Value     := edNomeEscola.Text;
        Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
        Params.ParamByName ('@pe_ind_status').Value     := Null;
        Params.ParamByName ('@pe_flg_defasagem').Value  := Null;
        Open;

        lin := 2;
        dmDeca.cdsSel_Boletim.First;
        while not dmDeca.cdsSel_Boletim.eof do
        begin
          Sheet2.Cells[lin,1].Value := dmDeca.cdsSel_Boletim.FieldByName ('num_rg_escolar').AsString;
          Sheet2.Cells[lin,2].Value := dmDeca.cdsSel_Boletim.FieldByName ('nom_nome').AsString;
          Sheet2.Cells[lin,3].Value := dmDeca.cdsSel_Boletim.FieldByName ('DatNascimento').AsDateTime;
          Sheet2.Cells[lin,4].Value := dmDeca.cdsSel_Boletim.FieldByName ('nom_escola').AsString;
          Sheet2.Cells[lin,5].Value := dmDeca.cdsSel_Boletim.FieldByName ('dsc_serie').AsString;
          Sheet2.Cells[lin,6].Value := dmDeca.cdsSel_Boletim.FieldByName ('nom_unidade').AsString;
          lin := lin + 1;
          dmDeca.cdsSel_Boletim.Next;
        end;

        objExcel2.WorkBooks[1].Close;
      end
    end
    else if rgTipoMensagem.ItemIndex = 1 then
    begin
      Sheet2 := objExcel2.WorkBooks[1].WorkSheets[1];
      //Sheet2.Name := 'Escola : ' + edNomeEscola.Text;
      Sheet2.Cells[1,1].Value  := 'RA';
      Sheet2.Cells[1,2].Value  := 'Nome do Aluno';
      Sheet2.Cells[1,3].Value  := 'Nascimento';
      Sheet2.Cells[1,4].Value  := 'Escola';
      Sheet2.Cells[1,5].Value  := 'S�rie Escolar';
      Sheet2.Cells[1,6].Value  := 'Unidade FUNDHAS';
      Sheet2.Cells[1,7].Value  := 'Apr. 1B';
      Sheet2.Cells[1,8].Value  := 'Freq. 1B';
      Sheet2.Cells[1,9].Value  := 'Apr. 2B';
      Sheet2.Cells[1,10].Value := 'Freq. 2B';
      Sheet2.Cells[1,11].Value := 'Apr. 3B';
      Sheet2.Cells[1,12].Value := 'Freq. 3B';
      Sheet2.Cells[1,13].Value := 'Observa��es';

      //Seleciona os dados do Boletim para o usu�rio atual...
      with dmDeca.cdsSel_Boletim do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value  := Null;
        Params.ParamByName ('@pe_num_rg_escolar').Value := Null;
        Params.ParamByName ('@pe_nom_escola').Value     := edNomeEscola.Text;
        Params.ParamByName ('@pe_cod_usuario').Value    := vvCOD_USUARIO;
        Params.ParamByName ('@pe_ind_status').Value     := Null;
        Params.ParamByName ('@pe_flg_defasagem').Value  := Null;
        Open;

        lin := 2;
        while not dmDeca.cdsSel_Boletim.eof do
        begin
          Sheet2.Cells[lin,1].Value  := dmDeca.cdsSel_Boletim.FieldByName ('num_rg_escolar').AsString;
          Sheet2.Cells[lin,2].Value  := dmDeca.cdsSel_Boletim.FieldByName ('nom_nome').AsString;
          Sheet2.Cells[lin,3].Value  := dmDeca.cdsSel_Boletim.FieldByName ('DatNascimento').AsDateTime;
          Sheet2.Cells[lin,4].Value  := dmDeca.cdsSel_Boletim.FieldByName ('nom_escola').AsString;
          Sheet2.Cells[lin,5].Value  := dmDeca.cdsSel_Boletim.FieldByName ('dsc_serie').AsString;
          Sheet2.Cells[lin,6].Value  := dmDeca.cdsSel_Boletim.FieldByName ('nom_unidade').AsString;
          lin := lin + 1;
          dmDeca.cdsSel_Boletim.Next;
        end;

        objExcel2.WorkBooks[1].Close;

      end;

    end;

  end;

  //edAnexo.Text := mmCAMINHO_PLANILHA;

end;

end.
