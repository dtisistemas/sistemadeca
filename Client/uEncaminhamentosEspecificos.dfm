object frmEncaminhamentosEspecificos: TfrmEncaminhamentosEspecificos
  Left = 1716
  Top = 529
  Width = 546
  Height = 297
  Caption = '[Encaminhamentos Espec�ficos]'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cbEvolucao: TComboBox
    Left = 9
    Top = 17
    Width = 360
    Height = 22
    Style = csOwnerDrawFixed
    Color = clHighlightText
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ItemHeight = 16
    ParentFont = False
    TabOrder = 0
    Items.Strings = (
      'AB - Abaixo do Esperado'
      'DE - Dentro do Esperado'
      'AC - Acima do Esperado'
      'NE - N�o Evoluiu')
  end
  object Memo1: TMemo
    Left = 8
    Top = 48
    Width = 449
    Height = 201
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
end
