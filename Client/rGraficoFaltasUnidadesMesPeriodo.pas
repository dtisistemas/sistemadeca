unit rGraficoFaltasUnidadesMesPeriodo;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, TeEngine, Series, TeeProcs,
  Chart, DBChart, QrTee, jpeg;

type
  TrelGraficoFaltasUnidadesMesPeriodo = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRShape3: TQRShape;
    QRLabel2: TQRLabel;
    QRShape4: TQRShape;
    QRLabel3: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRExpr1: TQRExpr;
  private

  public

  end;

var
  relGraficoFaltasUnidadesMesPeriodo: TrelGraficoFaltasUnidadesMesPeriodo;

implementation

uses uDM;

{$R *.DFM}

end.
