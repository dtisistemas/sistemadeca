unit uAlteracaoEndereco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Buttons, Mask, Grids, DBGrids, Db, jpeg;

type
  TfrmAlteracaoEndereco = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    cbCotasPasse: TComboBox;
    lbMatricula: TLabel;
    lbNome: TLabel;
    edEndereco: TEdit;
    edComplemento: TEdit;
    edBairro: TEdit;
    GroupBox8: TGroupBox;
    Fotografia: TImage;
    dbgHistoricoEnderecos: TDBGrid;
    dsHistoricoEnderecos: TDataSource;
    Panel1: TPanel;
    btnSair: TSpeedButton;
    btnCancelar: TSpeedButton;
    btnAlterar: TSpeedButton;
    chkAlteraEnderecoFamilia: TCheckBox;
    Image2: TImage;
    btnGoogleMaps: TSpeedButton;
    mskCep: TEdit;
    procedure btnSairClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function ValidaCampos:Boolean;
    procedure btnGoogleMapsClick(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteracaoEndereco: TfrmAlteracaoEndereco;
  vOK : Boolean;

implementation

uses uDM, uFichaCrianca, uUtil, uNavegador;

{$R *.DFM}

procedure TfrmAlteracaoEndereco.btnSairClick(Sender: TObject);
begin
  frmAlteracaoEndereco.Close;
end;

procedure TfrmAlteracaoEndereco.btnAlterarClick(Sender: TObject);
begin
  //Validar os campos a serem preenchidos, em especial as cotas de passe...


  if Application.MessageBox ('Confirma a altera��o dos dados do endere�o?',
                             '[Sistema Deca]-Altera��o de Endere�o...',
                             MB_YESNO + MB_ICONQUESTION) = id_yes then
  begin

    //Alterar os dados de endere�o do cadastro...
    //Houve altera��o do endere�o...
    //if vOK then
    //begin

      try

        with dmDeca.cdsAlt_EnderecoCadastro do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').Value      := lbMatricula.Caption;
          Params.ParamByName ('@pe_nom_endereco').Value       := GetValue(edEndereco.Text);
          Params.ParamByName ('@pe_nom_complemento').Value    := GetValue(edComplemento.Text);
          Params.ParamByName ('@pe_nom_bairro').Value         := GetValue(edBairro.Text);
          Params.ParamByName ('@pe_num_cep').Value            := GetValue(mskCep.Text);
          Params.ParamByName ('@pe_num_cota_passes').AsFloat  := StrToFloat(cbCotasPasse.Text);
          Execute;

          //Incluir os dados da tela no historico de Endere�os...
          with dmDeca.cdsAlt_EnderecoFLG do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value := lbMatricula.Caption;
            Params.ParamByName ('@pe_flg_atual').Value     := 1; //Alterar todos os endere�os do hist�rico para anterior...
            Execute;
          end;

          with dmDeca.cdsInc_HistoricoEnderecos do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').Value   := lbMatricula.Caption;
            Params.ParamByName ('@pe_dsc_endereco').Value    := GetValue(edEndereco.Text);
            Params.ParamByName ('@pe_dsc_complemento').Value := GetValue(edComplemento.Text);
            Params.ParamByName ('@pe_dsc_bairro').Value      := GetValue(edBairro.Text);
            Params.ParamByName ('@pe_num_cep').Value         := GetValue(mskCep.Text);
            Params.ParamByName ('@pe_num_cotas').Value       := StrToFloat(cbCotasPasse.Text);
            Params.ParamByName ('@pe_flg_atual').Value       := 0; //Endere�o Atual
            Execute;
          end;
          
          if (chkAlteraEnderecoFamilia.Checked = True) then
          begin
            //Desabilita as triggers de atualiza��o e inclus�o do cadastro_familia para poder alterar o endere�o
            //with dmDeca.cdsDesabilitaTriggers_Responsavel do
            //begin
            //  Close;
            //  Execute;
            //end;

            //Executa a procedure para altera��o somente dos endere�os da composi��o familiar
            with dmDeca.cdsAlt_EnderecoFamilia do
            begin
              Close;
              Params.ParamByName('@pe_cod_matricula').Value   := GetValue(lbMatricula.Caption);
              Params.ParamByName('@pe_nom_endereco').Value    := GetValue(edEndereco.Text);
              Params.ParamByName('@pe_nom_complemento').Value := GetValue(edComplemento.Text);
              Params.ParamByName('@pe_nom_bairro').Value      := GetValue(edBairro.Text);
              Params.ParamByName('@pe_num_cep').Value         := GetValue(mskCep.Text);
              Execute;
            end;

            //Habilita as triggers de atualiza��o e inclus�o do cadastro_familia
            //with dmDeca.cdsHabilitaTriggers_Responsavel do
            //begin
            //  Close;
            //  Execute;
            //end;

          end;
        end;

        frmAlteracaoEndereco.Close;
        frmFichaCrianca.FillForm('S', frmFichaCrianca.txtMatricula.Text);

      except
        Application.MessageBox ('Aten��o !!!' + #13+#10 +
                                'Ocorreu um erro e os dados do endere�o n�o foram alterados...',
                                '[Sistema Deca] - Erro Alterando Endere�o...',
                                MB_OK + MB_ICONERROR);
      end;

    //end
    //else
    //N�o houve altera��o do endere�o...
    //begin
    //  ShowMessage ('N�o houve altera��o dos dados do endere�o...');
    //end;
                            
  end;

end;

procedure TfrmAlteracaoEndereco.FormShow(Sender: TObject);
begin

  vOK := False;
  PageControl1.ActivePageIndex := 0;

  try
    with dmDeca.cdsSel_HistoricoEnderecos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := lbMatricula.Caption;
      Params.ParamByName ('@pe_flg_atual').Value     := Null;
      Open;

      dbgHistoricoEnderecos.Refresh;

    end;
  except end;
end;

function TfrmAlteracaoEndereco.ValidaCampos: Boolean;
begin
  // valida os dados para INSERT e UPDATE
  vOk := frmFichaCrianca.ValidaDados;

  if vOk then
  begin

    vOk := (Length(edEndereco.Text) > 0);
    if not vOk then
      edEndereco.SetFocus;
  end;

  if vOk then
  begin
    vOk := (Length(edComplemento.Text) > 0);
    if not vOk then
      edComplemento.SetFocus;
  end;

  if vOk then
  begin
    vOk := Trim(mskCep.Text) <> '     -   ';
    if not vOk then
      mskCep.SetFocus;
  end;

  if vOk then
  begin
    vOk := (Length(edBairro.Text) > 0);
    if not vOk then
      edBairro.SetFocus;
  end;

  if vOk then
  begin
    vOk := cbCotasPasse.ItemIndex >= 0;
    if not vOk then
      cbCotasPasse.SetFocus;
  end;

  // prepara o retorno
  ValidaCampos := vOk;
  
end;

procedure TfrmAlteracaoEndereco.btnGoogleMapsClick(Sender: TObject);
begin
  Application.CreateForm (TfrmNavegador, frmNavegador);
  frmNavegador.URLs.Text := 'http://maps.google.com/maps?q=' + edEndereco.Text + ',' + mskCep.Text + ',' + 'Sao Jose dos Campos,SP';
  frmNavegador.Show;
end;

procedure TfrmAlteracaoEndereco.Image2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmNavegador, frmNavegador);
  frmNavegador.URLs.Text := 'http://www.buscacep.correios.com.br/';
  frmNavegador.Show;
end;

end.
