unit ufrmCorrecaoUsuariosRelatorios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, StdCtrls, Grids, DBGrids, Db;

type
  TfrmCorrecaoUsuariosRelatorios = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    btnATUALIZAR_USUARIO_RELATORIO: TSpeedButton;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    dsSel_Relatorios: TDataSource;
    dsSel_UsuariosDeca: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure btnATUALIZAR_USUARIO_RELATORIOClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCorrecaoUsuariosRelatorios: TfrmCorrecaoUsuariosRelatorios;

implementation

uses uDM, udmTriagemDeca;

{$R *.DFM}

procedure TfrmCorrecaoUsuariosRelatorios.FormShow(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsSel_AtualizaUsuariosDecaRelatorio do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario_deca').Value := Null;
      Open;
      DBGrid1.Refresh;
    end;

    with dmDeca.cdsSel_Usuario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_usuario').Value   := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_flg_situacao').Value  := Null;
      Params.ParamByName ('@pe_nom_usuario').Value   := Null;
      Open;
      DBGrid2.Refresh;
    end;
  except end;
end;

procedure TfrmCorrecaoUsuariosRelatorios.DBGrid1CellClick(Column: TColumn);
begin
  with dmDeca.cdsSel_Usuario do
  begin
    Close;
    Params.ParamByName ('@pe_cod_usuario').Value   := Null;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Params.ParamByName ('@pe_flg_situacao').Value  := Null;
    Params.ParamByName ('@pe_nom_usuario').Value   := dmTriagemDeca.cdsSel_AtualizaUsuariosDecaRelatorio.FieldByName ('nom_usuario').Value + '%';
    Open;
    DBGrid2.Refresh;
  end;
end;

procedure TfrmCorrecaoUsuariosRelatorios.btnATUALIZAR_USUARIO_RELATORIOClick(
  Sender: TObject);
begin
  //atualizar o Cadastro_Relatorio para o usu�rio triagem localizado e cod_usuario_deca "setado"
  try
    with dmTriagemDeca.cdsUpd_AtualizaUsuariosDecaRelatorio do
    begin
      Close;
      Params.ParamByName ('@pe_cod_responsavel').Value  := dmTriagemDeca.cdsSel_AtualizaUsuariosDecaRelatorio.FieldByName ('cod_Responsavel').Value;
      Params.ParamByName ('@pe_cod_usuario_deca').Value := dmDeca.cdsSel_Usuario.FieldByName ('cod_usuario').Value;
      Execute;
      ShowMessage ('Registros atualizados com sucesso');

      {with dmTriagemDeca.cdsSel_AtualizaUsuariosDecaRelatorio do
      begin
        Close;
        Params.ParamByName ('@pe_cod_usuario_deca').Value := Null;
        Open;
        DBGrid1.Refresh;
      end;}
    end;
  except end;
end;

end.
