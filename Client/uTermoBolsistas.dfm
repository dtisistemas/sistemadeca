object frmTermoBolsistas: TfrmTermoBolsistas
  Left = 382
  Top = 252
  Width = 864
  Height = 565
  Caption = '[Sistema Deca] - Emiss�o de Termo de Compromisso de Bolsistas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 4
    Width = 545
    Height = 53
    Caption = '[Unidade]'
    TabOrder = 0
    object cbUnidade: TComboBox
      Left = 9
      Top = 18
      Width = 530
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnClick = cbUnidadeClick
    end
  end
  object PageControl1: TPageControl
    Left = 6
    Top = 64
    Width = 835
    Height = 404
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = '[Alunos da Unidade "X"]'
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 363
        Height = 13
        Caption = 
          'Selecione abaixo o(s) aluno(s) cujos Termos de  Compromisso ser�' +
          'o emitidos.'
      end
      object Label2: TLabel
        Left = 13
        Top = 26
        Width = 803
        Height = 16
        Caption = 
          'Antes de emitir o Termo de Refer�ncia, verifique os dados dos fa' +
          'miliares, principalmente: Pai, M�e e Respons�vel .'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 101
        Top = 44
        Width = 593
        Height = 16
        Caption = 
          'Esse procedimento N�O gera registro algum em hist�rico nos pront' +
          'u�rios dos alunos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object clbAlunosUnidade: TCheckListBox
        Left = 8
        Top = 67
        Width = 811
        Height = 299
        ItemHeight = 13
        TabOrder = 0
      end
    end
  end
  object Panel1: TPanel
    Left = 7
    Top = 472
    Width = 833
    Height = 49
    BevelInner = bvLowered
    TabOrder = 2
    object SpeedButton1: TSpeedButton
      Left = 16
      Top = 8
      Width = 249
      Height = 33
      Caption = 'Selecionar Todos'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 290
      Top = 8
      Width = 249
      Height = 33
      Caption = 'Desmarcar Todos'
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 568
      Top = 8
      Width = 249
      Height = 33
      Caption = 'Emitir Termo de Compromisso'
      OnClick = SpeedButton3Click
    end
  end
  object rgOpcaoImpressa: TRadioGroup
    Left = 684
    Top = 4
    Width = 161
    Height = 53
    Caption = '[Op��o de Impress�o]'
    ItemIndex = 1
    Items.Strings = (
      'Impressora'
      'Tela (Gravados em pasta)')
    TabOrder = 3
  end
  object GroupBox2: TGroupBox
    Left = 550
    Top = 4
    Width = 132
    Height = 53
    Caption = '[Data In�cio Contrato]'
    TabOrder = 4
    object mskDataInicioContrato: TMaskEdit
      Left = 10
      Top = 17
      Width = 107
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
  end
end
