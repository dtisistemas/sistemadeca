unit uCadastroValoresPasses;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Grids, DBGrids, Buttons, Db;

type
  TfrmCadastroValoresPasses = class(TForm)
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    meObservacoes: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    dbgPassagensPasses: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    edVrPassagem: TEdit;
    edVrPasse: TEdit;
    btnNovo: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnSair: TSpeedButton;
    dsSel_ValoresPasses: TDataSource;
    procedure btnSairClick(Sender: TObject);
    procedure ModoTela (Modo: String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroValoresPasses: TfrmCadastroValoresPasses;

implementation

uses uDM;

{$R *.DFM}

procedure TfrmCadastroValoresPasses.btnSairClick(Sender: TObject);
begin
  frmCadastroValoresPasses.Close;
end;

procedure TfrmCadastroValoresPasses.ModoTela(Modo: String);
begin
  if Modo = 'P' then
  begin
    edVrPassagem.Clear;
    edVrPasse.Clear;
    meObservacoes.Clear;
    GroupBox2.Enabled := False;
    GroupBox3.Enabled := False;
    GroupBox4.Enabled := False;
    btnNovo.Enabled   := True;
    btnSalvar.Enabled := False;
    btnSair.Enabled   := True;
  end
  else if Modo = 'N' then
  begin
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    GroupBox4.Enabled := True;
    btnNovo.Enabled   := False;
    btnSalvar.Enabled := True;
    btnSair.Enabled   := False;
  end;
end;

procedure TfrmCadastroValoresPasses.btnNovoClick(Sender: TObject);
begin
  ModoTela('N');
end;

procedure TfrmCadastroValoresPasses.btnSalvarClick(Sender: TObject);
begin
  try

   with dmDeca.cdsUpd_ValorPasse do
   begin
     Close;
     Execute;
   end;

    with dmDeca.cdsInc_ValoresPasses do
    begin
      Close;
      Params.ParamByName ('@pe_vr_passagem').Value := StrToFloat(edVrPassagem.Text);
      Params.ParamByName ('@pe_vr_passe').Value    := StrToFloat(edVrPasse.Text);
      Params.ParamByName ('@pe_flg_atual').Value   := 1;
      Execute;

      with dmDeca.cdsSel_ValorPasse do
      begin
        Close;
        Params.ParamByName ('@pe_flg_atual').Value := Null;
        Open;
      end;
    end;
  except end;
  ModoTela('P');
end;

procedure TfrmCadastroValoresPasses.FormShow(Sender: TObject);
begin
  ModoTela('P');

  with dmDeca.cdsSel_ValorPasse do
  begin
    Close;
    Params.ParamByName ('@pe_flg_atual').Value := Null; 
    Open;
  end;
end;

end.
