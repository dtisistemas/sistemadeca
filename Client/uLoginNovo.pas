unit uLoginNovo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, jpeg, Buttons, IniFiles, Psock, NMFtp;

type
  TfrmLoginNovo = class(TForm)
    Shape2: TShape;
    Shape3: TShape;
    Label2: TLabel;
    Label3: TLabel;
    Image2: TImage;
    Label6: TLabel;
    lbVersao: TLabel;
    Image3: TImage;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    edUsuario: TEdit;
    edSenha: TEdit;
    btnLogin: TBitBtn;
    btnSair: TBitBtn;
    Label11: TLabel;
    Label12: TLabel;
    ldDataAtualizacao: TLabel;
    Panel1: TPanel;
    lbConexao: TLabel;
    Label1: TLabel;
    Image1: TImage;
    Label4: TLabel;
    Image4: TImage;
    Label5: TLabel;
    Image5: TImage;
    Label7: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edUsuarioChange(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnLoginClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLoginNovo: TfrmLoginNovo;
  vArqIni : TIniFile;
  vArqVersion : TIniFile;
  vIP : String;
  vNumVersao : String;
  vOk : boolean;
  i : Integer;
  vREVERTE : String;
  //ie : variant;

implementation

uses uDM, uPrincipal, uUtil, uEncaminhamentoSolicitacaoChefia,
  uExibeSemEscola;

{$R *.DFM}

procedure TfrmLoginNovo.FormActivate(Sender: TObject);
begin
  // formatar o padr�o de data
  ShortDateFormat := 'dd/mm/yyyy';
  edUsuario.SetFocus;
end;

procedure TfrmLoginNovo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmLoginNovo.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmLoginNovo.Close;
  end;

  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end;
end;

procedure TfrmLoginNovo.edUsuarioChange(Sender: TObject);
begin
  btnLogin.Enabled := True;
end;

procedure TfrmLoginNovo.btnSairClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLoginNovo.btnLoginClick(Sender: TObject);
//var mm_PATH_FILE : PChar;
begin

  //Ler a vers�o do arquivo VERSION.INI e compar�-lo a �ltima vers�o cadastrada no Banco de Dados
  with dmDeca.cdsSel_Versao do
  begin
    Close;
    Open;
  end;

  vArqVersion := TIniFile.Create(ExtractFilePath(Application.ExeName)+'version.ini');
  vNumVersao := vArqVersion.ReadString('VERSAO','NUMERO','ERRO');

  if (vNumVersao = 'ERRO') or (vNumVersao <> dmDeca.cdsSel_Versao.FieldByName ('UltimaVersao').Value) then
  begin

     Application.MessageBox ('Aten��o!!! Esta vers�o que voc� est� tentando acessar est� desatualizada...' + #13+#10 +
                             'Visite o site da Divis�o de Inform�tica e baixe e �ltima atualiza��o.' + #13+#10 +
                             'Essa tela de login ser� fechada.' + #13+#10 +
                             'D�vidas ou qualquer problema, entre em contato com o Administrador do Sistema...',
                             'Controle de Vers�o - Sistema Deca',
                             MB_OK + MB_ICONQUESTION);


    //Executa o atualizador do Sistema...
    //Use um cast para PChar
    //var_pchar := PChar(minha_str);

    //mm_PATH_FILE := PChar(ExtractFilePath(Application.ExeName)+'AutoUPDATE.exe');
    //WinExec (mm_PATH_FILE, SW_NORMAL);
    Application.Terminate;
  end
  else
  begin

    //Repassa os valores da vers�o para a tela de login
    vvNUM_VERSAO  := vNumVersao;

    // autentica��o do usu�rio
    vOk := true;
    try
      with uDM.dmDeca.cdsSel_Usuario do
      begin
        Close;

        //Par�metros anteriores
        Params.ParamByName('@pe_cod_usuario').value:= NULL;
        Params.ParamByName('@pe_cod_unidade').value:= NULL;
        Params.ParamByName('@pe_nom_usuario').Value:= GetValue(edUsuario.Text);
        Params.ParamByName('@pe_flg_status').Value := Null; //Usu�rio deve estar OFF-LINE
        Params.ParamByName('@pe_flg_situacao').Value := 0 ; //Usu�rio ATIVO

        //Par�metros atuais
        //Params.ParamByName ('@pe_cod_usuario').Value :=  Null;
        //Params.ParamByName ('@pe_cod_matricula').Value :=  Null;
        //Params.ParamByName ('@pe_flg_status').Value :=  0;
        //Params.ParamByName ('@pe_flg_online').Value :=  Null;
        //vvNOM_USUARIO := Trim(edUsuario.Text);
        Open;

        if (Trim(edSenha.Text) = '') or
           (Trim(FieldByName('dsc_senha').AsString) <> Trim(edSenha.Text)) then
        begin
          Application.MessageBox('Senha Inv�lida!'+#13+#10+#13+#10+
               'Em caso de perda da senha, favor entrar em contato com o Administrador do Sistema.',
               'Sistema DECA - Autentica��o',
             MB_OK + MB_ICONERROR);
          edSenha.Clear;
          edSenha.SetFocus;
          vOk := false
        end
        else begin

        //Atualiza o campo flg_status do usu�rio para ON-LINE
        try
          with dmDeca.cdsAlt_Status_Usuario do
          begin
            Close;
            Params.ParamByName ('@pe_cod_usuario').Value := dmDeca.cdsSel_Usuario.FieldByname ('cod_usuario').Value;
            Params.ParamByName ('@pe_flg_status').Value := 0; // 0 para On-Line e 1 para Off-Line
            Execute;
          end;
        except end;

        frmPrincipal.StatusBar1.Panels[1].Text := 'Usu�rio(a): ' + FieldByName('nom_usuario').AsString +
                                                '  -  ON-LINE';

        // carrega as vari�veis publicas
        vvCOD_USUARIO := FieldByName('cod_usuario').AsInteger;
        vvNOM_USUARIO := FieldByName('nom_usuario').AsString;
        vvIND_PERFIL  := FieldByName('ind_perfil').AsInteger;

        //Permitir ao Administrador e Psicologia/Psicopedagogia
        //Diretor, DRH, Triagem, Educa��o F�sica e As. Social(Proj. Gestante), Coord. Acomp. Escolar visualizar todas as c�as/adolescentes
        if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or
           (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 10) or //(vvIND_PERFIL = 9)
           (vvIND_PERFIL = 11) or (vvIND_PERFIL = 12) or (vvIND_PERFIL = 13) or (vvIND_PERFIL = 14) or
           (vvIND_PERFIL = 15) or (vvIND_PERFIL = 16) or (vvIND_PERFIL = 17) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19) then
        begin
          vvCOD_UNIDADE := 0;
          vvNOM_UNIDADE := 'FUNDHAS';
          frmPrincipal.StatusBar1.Panels[3].Text := 'FUNDHAS [';
        end
        else
        begin
          try
            with dmDeca.cdsSel_Unidade do
            begin
              Close;
              Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Usuario.FieldByName ('cod_unidade').Value;
              Params.ParamByName ('@pe_nom_unidade').Value := Null;
              Open;

              vvCOD_UNIDADE := FieldByName('cod_unidade').AsInteger;
              vvNOM_UNIDADE := FieldByName('nom_unidade').AsString;
              vvNUM_CCUSTO  := FieldByName('num_ccusto').AsString;
              vvNOM_GESTOR  := FieldByName('nom_gestor').AsString;
              vvDSC_EMAIL   := FieldByName('dsc_email').AsString;
              vvNOM_ASOCIAL := FieldByName('nom_asocial').AsString;
              vvNOM_DIVISAO := FieldByName('vDivisao').AsString;
              
            end;
          except end;

          //frmPrincipal.StatusBar1.Panels[1].Text := 'Unidade: ' + FieldByName('nom_unidade').AsString + ' [';
          frmPrincipal.StatusBar1.Panels[3].Text := 'Unidade: ' + FieldByName('nom_unidade').AsString + ' [';
        end;

        frmPrincipal.AtualizaStatusBarUnidade;

        try
          with dmDeca.cdsAlt_Cadastro_Tira_Suspensao do
          begin
            Close;
            Execute;
          end;
        except
          ShowMessage('Sr. Usu�rio'+#13+#10+#13+#10+
                      'Ocorreu um problema no sistema, favor informar o Administrador'+#13+#10+
                      'do Sistema informando esta mensagem: ERRO COM RETORNO DE SUSPENS�ES');
        end;

        Close;
      end
    end
  except end;

  if vOk then
  begin
    // preparar o menu conforme o perfil
    with frmPrincipal.mmPrincipal do
    begin
      case vvIND_PERFIL of

        1: // Administrador
        begin

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := True; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := True; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := True; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := True;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := True;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := True;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := True;  //Nutri��o - Unidades

          //Menu Movimenta��es
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Administrador do Sistema';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Administrador do Sistema';

          frmPrincipal.mmPrincipal.Items[1].Visible := True;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do  //27 do
            Items[1].Items[i].Visible := True;

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := True; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := True; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := True; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := True; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := True; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := True; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := True;

        end;

        2: // Gestor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Gestor';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Gestor';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := True; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios

          //if vvCOD_UNIDADE=44 then  //somente convenio
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True; //Unidades
          //else
          //  frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades

          //frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios

          //Permitir ao Gestor do Conv�nio visualizar o cadastro de empresas
          //if vvCOD_UNIDADE=44 then
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True;
          //else
          //  frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False;

          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := True; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := True; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := True; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := True; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := True; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := True; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := True; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19


          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA

          //Habilita menu para Gestor Conv�nio
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True //Transferidos DAPA - Acesso Conv�nio
          else
            frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio


          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          if vvCOD_UNIDADE = 44 then  //Somente o Gestor do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        3: // Assistente Social
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Assistente Social';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assistente Social';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := True;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := True; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := True; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := True; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := True; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := True; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := True; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := True; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := True; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := True; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          //Somente assistentes sociais do conv�nio...
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True //Rela��o de Empresas por As. Social (Conv�nios)
          else
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          if vvCOD_UNIDADE = 44 then  //Somente a Assistente Social do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        4: // Outros - Operacional
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Consulta';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Consulta';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := False; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        5: //Psicologia-Psicopedagogia
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Psicologia/Psicopedagogia';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Acompanhamento Escolar';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        6: //Professor-Instrutor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Professor/Instrutor';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Professor/Instrutor';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := True; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := True; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := True; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar

          //Menu Relat�rios
          //frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        7:  //Diretor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Diretor(a)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Diretor(a)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          {frmPrincipal.mmPrincipal.Items[1].Visible := True;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do  //27 do
            Items[1].Items[i].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es}
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenadas por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenadas por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        8:  //DRH
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Divis�o de Recursos Humanos';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Divis�o de Recursos Humanos';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          {frmPrincipal.mmPrincipal.Items[1].Visible := False;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do  //27 do
            Items[1].Items[i].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          //frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica}
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar
                    
          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := True; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        9:  //Triagem
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Triagem';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Triagem';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := True; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        10: //Educa��o F�sica
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Educa��o F�sica';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Educa��o F�sica';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := True; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Items[0].Visible := True; //Lan�amento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[23].Items[1].Visible := True; //Reclassifica��o do IMC
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := True; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        11: //Assistente Social(Projeto Gestante)
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Assistente Social(Projeto Gestante)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assistente Social(Projeto Gestante)';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        12: // Nutri��o
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Compras/Almoxarifado';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := False;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := False;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do        //27 do
            Items[1].Items[i].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica

          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False;  //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;
                    
        end;

        13: // Odontologia
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Odontologia';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := True; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        14: //Servi�o Social(Plant�o)
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Servi�o Social(Plant�o)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        15: //Servi�o Social(Supervis�o)
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Servi�o Social(Supervis�o)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        16: //Coordenador
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Coordenador de Projeto';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Exibe o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        17:  //Assessoria da Qualidade
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Diretor(a)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assessoria da Qualidade';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Exibe o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := False; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := False; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := False; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := True; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := True; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar          

          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA

          //Somente assistentes sociais do conv�nio...
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True //Rela��o de Empresas por As. Social (Conv�nios)
          else
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;


        18:  //Acompanhamento Escolar(Servi�o Social)
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Diretor(a)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Acomp. Escolar (Servi�o Social)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := True; 

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        19: // Coordena��o Acomp. Escolar
        begin
          frmPrincipal.StatusBar1.Panels[5].Text := 'Coordena��o Acomp. Escolar';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := True;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := True; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := True;

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA

          //Somente assistentes sociais do conv�nio...
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True //Rela��o de Empresas por As. Social (Conv�nios)
          else
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          if vvCOD_UNIDADE = 44 then  //Somente a Assistente Social do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

      end;

    end;

    // esconder a tela de autentica��o
    frmLoginNovo.Hide;

    // abrir a tela principal do sistema

    frmPrincipal.ShowModal;

    // fechar a tela de autentica��o e o sistema
    frmLoginNovo.Close;
    
  end;
 end;
end;

procedure TfrmLoginNovo.FormCreate(Sender: TObject);
begin

  //if (NMFtp1.GetLocalAddress <> '0,0,0,0') then
  //begin
  //  Application.MessageBox ('Aten��o!!!' + #13+#10 +
  //                          'Voc� n�o est� conectado a Internet.' + #13+#10 +
  //                          'Verifique sinal/conex�es/cabos e tente novamente.'+ #13+#10 +
  //                          'Esta aplica��o ser� encerrada...',
  //                          '[Sistema Deca] - Problemas de conex�o com Internet',
  //                          MB_OK + MB_ICONWARNING);
  //  Application.Terminate;
  //end;

  // configura a conexao com o servidor de aplicacao


  try
    try
      vArqIni := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'config.ini');
      vIP := vArqIni.ReadString('CONEXAO' , 'IP', 'ERRO' );
      if vIP <> 'ERRO' then
      begin
        //DataModuleRemote1
        dmDeca.socketConn.Host := vIP;
        dmDeca.socketConn.ObjectBroker := dmDeca.SimpleObjectBroker1;
        dmDeca.SimpleObjectBroker1.Servers[0].ComputerName := vIP;
        dmDeca.socketConn.ServerName := 'DecaServer.dbDecaServer';

        lbConexao.Caption := 'VOC� EST� CONECTADO AO IP/HOST: ' + vIP; //dmDeca.socketConn.ServerName + ' em ' + vIP;

      end
      else
      begin
        ShowMessage('Arquivo CONFIG.INI n�o encontrado'+#13+#10+#13+#10+
                    'Favor entrar em contato com o Administrador do Sistema');
        Application.Terminate;
      end;
    finally
      vArqIni.Free;
    end;

    //Carrega a vari�vel vvREVERTE para acesso ao resumo mensal...
    vArqIni  := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'config.ini');
    vREVERTE := vArqIni.ReadString('REVERTE' , 'SEQ', 'ERRO' );

  except
    ShowMessage('N�o foi poss�vel fazer conex�o com o servidor (IP ' + vIP + ')');
  end;


  //abre conexao e valida o numero da versao
  try
  except
    ShowMessage('Problemas de conex�o com o IP ' + vIP);
    Application.Terminate;
  end;
   
end;

end.
