unit uRegistroAtendimentoAltera;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmRegistroAtendimentoAltera = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    Panel1: TPanel;
    btnAlterar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    txtOcorrencia: TMemo;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Label3: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure AtualizaCamposBotoes (Modo : String);
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnAlterarClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroAtendimentoAltera: TfrmRegistroAtendimentoAltera;

implementation

uses uDM, uFichaPesquisa, uPrincipal;

{$R *.DFM}

procedure TfrmRegistroAtendimentoAltera.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmRegistroAtendimentoAltera.btnPesquisarClick(Sender: TObject);
begin
// chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carrega os atendimentos referentes a matr�cula
        //na unidade atual somente para o usu�rio que fez o registro

        with dmDeca.cdsSel_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName ('@pe_cod_id_historico').Value := Null;
          //if vvIND_PERFIL = 3 then
          //    Params.ParamByName ('@pe_cod_usuario').Value := Null
          //  else
          //    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
          Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;
          Open;
          DBGrid1.DataSource := DataSource1;
          DBGrid1.Refresh;

          AtualizaCamposBotoes ('Alterar');

        end;
      end;
    except end;
  end;
end;

procedure TfrmRegistroAtendimentoAltera.txtMatriculaExit(Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
        begin
          lbNome.Caption := FieldByName('nom_nome').AsString;

          //Carrega os atendimentos referentes a matr�cula
          //na unidade atual

          with dmDeca.cdsSel_Cadastro_Historico do
          begin
            Close;
            Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
            Params.ParamByName ('@pe_cod_id_historico').Value := Null;

            //Caso seja acessado por uma Assistente Social, carrega todas as interven��es
            //realizadas na crian�a/adolescente pelos profissionais da unidade
            //if vvIND_PERFIL = 3 then
            //  Params.ParamByName ('@pe_cod_usuario').Value := Null
            //else
            //    Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;

            Params.ParamByName ('@pe_cod_usuario').AsInteger := vvCOD_USUARIO;

            Open;
            DBGrid1.DataSource := DataSource1;
            DBGrid1.Refresh;

            AtualizaCamposBotoes ('Alterar');

          end;
        end;
      end;
    except end;
  end;
end;

procedure TfrmRegistroAtendimentoAltera.AtualizaCamposBotoes(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    txtMatricula.Clear;
    txtMatricula.SetFocus;
    lbNome.Caption := '';
    btnPesquisar.Enabled := True;
    DBGrid1.DataSource := Nil;
    DBGrid1.Enabled := False;
    txtOcorrencia.Lines.Clear;
    txtOcorrencia.Enabled := False;

    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else if Modo = 'Alterar' then
  begin
    btnPesquisar.Enabled := True;
    DBGrid1.Enabled := True;
    txtOcorrencia.Enabled := True;

    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmRegistroAtendimentoAltera.btnSairClick(Sender: TObject);
begin
  frmRegistroAtendimentoAltera.Close;
end;

procedure TfrmRegistroAtendimentoAltera.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmRegistroAtendimentoAltera.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes ('Padr�o');
end;

procedure TfrmRegistroAtendimentoAltera.DBGrid1DblClick(Sender: TObject);
begin
  txtOcorrencia.Text := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_ocorrencia').AsString;
end;

procedure TfrmRegistroAtendimentoAltera.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end
  else if (Key = #27) then
  begin
    Key := #0;
    frmRegistroAtendimentoAltera.Close;
  end;
end;

procedure TfrmRegistroAtendimentoAltera.btnAlterarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_Cadastro_Historico do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
      Params.ParamByName ('@pe_cod_id_historico').AsInteger := dmDeca.cdsSel_Cadastro_Historico.FieldByname ('cod_id_historico').AsInteger;
      Params.ParamByName ('@pe_dat_historico').AsDateTime := dmDeca.cdsSel_Cadastro_Historico.FieldByname ('dat_historico').AsDateTime;
      Params.ParamByName ('@pe_ind_tipo_historico').AsInteger := dmDeca.cdsSel_Cadastro_Historico.FieldByname ('ind_tipo_historico').AsInteger;
      Params.ParamByName ('@pe_dsc_tipo_historico').AsString := dmDeca.cdsSel_Cadastro_Historico.FieldByname ('dsc_tipo_historico').AsString;
      Params.ParamByName ('@pe_cod_unidade').AsInteger := dmDeca.cdsSel_Cadastro_Historico.FieldByname ('cod_unidade').AsInteger;
      Params.ParamByName ('@pe_dsc_ocorrencia').AsString := txtOcorrencia.Lines.Text;
      Params.ParamByName ('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
      Params.ParamByName ('@pe_log_data').AsDateTime := dmDeca.cdsSel_Cadastro_Historico.FieldByname ('log_data').AsDateTime;

      Execute;

      with dmDeca.cdsSel_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName ('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName ('@pe_cod_id_historico').Value := Null;
          Params.ParamByName ('@pe_cod_usuario').Value := Null;
          Open;
          DBGrid1.Refresh;

          AtualizaCamposBotoes ('Padr�o');

        end;
    end
  except

    //Gravar no arquivo de LOG de conex�o os dados registrados na tela
    arqLog := 'RA_' + Copy(DateToStr(Date()),1,2) + Copy(DateToStr(Date()),4,2) + Copy(DateToStr(Date()),7,4) +
                      Copy(TimeToStr(Time()),1,2) + Copy(TimeToStr(Time()),4,2) + Copy(TimeToStr(Time()),7,2);
    AssignFile(log_file, 'c:\Deca\Logs\' + arqLog + '.txt');
    Rewrite (log_file);

    Write   (log_file, txtMatricula.Text);        //Grava a matricula
    Write   (log_file, ' ');
    Write   (log_file, lbNome.Caption);
    Write   (log_file, ' ');
    Write   (log_file, DateToStr(dmDeca.cdsSel_Cadastro_Historico.FieldByname ('dat_historico').AsDateTime));             //Data do registro
    Write   (log_file, ' ');
    Write   (log_file, 'Registro de Atendimento-Altera��o de Dados');//Descricao do Tipo de Historico
    Writeln (log_file, ' ');
    Writeln (log_file, Trim(txtOcorrencia.Text)); //Ocorrencia do Historico
    Writeln (log_file, '--------------------------------------------------------------------------------------');

    CloseFile (log_file);

    Application.MessageBox('Aten��o !!! Ocorreu um ERRO ao tentar alterar dados do Registro de Atendimento!'+#13+#10+#13+#10+
                           'Os dados n�o foram alterados no Servidor DECA. Os dados da tela foram transferidos ' + #13+#10+
                           'para um arquivo de LOG em "C:\Deca\Logs".Favor entrar em contato com o Administrador do Sistema.',
                           'Sistema DECA - Altera��o de Registro',
                           MB_OK + MB_ICONERROR);
    txtMatricula.Clear;
    txtMatricula.SetFocus;
  end;
end;

procedure TfrmRegistroAtendimentoAltera.DBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  txtOcorrencia.Text := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_ocorrencia').AsString;
end;

procedure TfrmRegistroAtendimentoAltera.DBGrid1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  txtOcorrencia.Text := dmDeca.cdsSel_Cadastro_Historico.FieldByName ('dsc_ocorrencia').AsString;
end;

end.
