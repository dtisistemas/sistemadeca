unit uTermoBolsistas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls, ComCtrls, ComObj, Mask, ActiveX;

type
  TfrmTermoBolsistas = class(TForm)
    GroupBox1: TGroupBox;
    cbUnidade: TComboBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    clbAlunosUnidade: TCheckListBox;
    Label1: TLabel;
    rgOpcaoImpressa: TRadioGroup;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    mskDataInicioContrato: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZeraVariaveis;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTermoBolsistas: TfrmTermoBolsistas;
  vListaUnidade1, vListaUnidade2 : TStringList;
  var i, x : Integer;
  mmMATRICULA, mmRESPONSAVEL, mmPAI, mmMAE, mmNOME, mmRG, mmCPF, mmENDERECO_RESPONSAVEL : String;

implementation

uses uDM, rFichaAdmissaoBolsista, uUtil;

{$R *.DFM}

procedure TfrmTermoBolsistas.FormShow(Sender: TObject);
begin

  PageControl1.Pages[0].Caption := 'Rela��o de Alunos(as) Bolsistas';;
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carregar a lista de unidades no combo
  cbUnidade.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        if dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0 then
        begin
          cbUnidade.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;

    end;
  except end;


end;

procedure TfrmTermoBolsistas.cbUnidadeClick(Sender: TObject);
begin
  clbAlunosUnidade.Clear;
  PageControl1.Pages[0].Caption := 'Alunos(as) bolsistas da ' + cbUnidade.Text;
  try
    with dmDeca.cdsSel_Cadastro do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Open;

      while not dmDeca.cdsSel_Cadastro.eof do
      begin
        clbAlunosUnidade.Items.Add (dmDeca.cdsSel_Cadastro.FieldByName('cod_matricula').Value + '-' + dmDeca.cdsSel_Cadastro.FieldByName('nom_nome').Value);
        dmDeca.cdsSel_Cadastro.Next;
      end;
    end;
  except end;
end;

procedure TfrmTermoBolsistas.SpeedButton1Click(Sender: TObject);
begin
  for i := 0 to clbAlunosUnidade.Items.Count - 1 do
      clbAlunosUnidade.Checked[i] := True;
end;

procedure TfrmTermoBolsistas.SpeedButton2Click(Sender: TObject);
begin
  for i := 0 to clbAlunosUnidade.Items.Count - 1 do
      clbAlunosUnidade.Checked[i] := False;
end;

procedure TfrmTermoBolsistas.SpeedButton3Click(Sender: TObject);
var
  winword, doc, docs : OleVariant;  //Variant;
  termocompromisso : String;

begin

  ZeraVariaveis;

  winword          := CreateOleObject('Word.Application');
  //termocompromisso := (ExtractFilePath(Application.ExeName)+ '\Aditamentos\TERMO.DOC');
  termocompromisso := (ExtractFilePath(Application.ExeName)+ '\Aditamentos\DRH-18 TERMO DE COMPROMISSO.DOC');
  //winword          := CreateOleObject('Word.Application');

  if (rgOpcaoImpressa.ItemIndex = 0) then //Impressora
  begin
    winword.Visible := False;
    doc.PrintOut(false);
  end
  else if (rgOpcaoImpressa.ItemIndex = 1) then
  begin
    winword.Visible := False;
    //doc3.PrintOut(false);
  end;

  //docs3 := Winword.Documents;
  //doc3  := Docs3.Open(termocompromisso);

  //Para cada alunos bolsista selecionado no checklistbox, emite diretamente para a impressora os termos de compromisso...
  for x := 0 to clbAlunosUnidade.Items.Count -1 do
  begin
    docs := Winword.Documents;
    doc  := Docs.Open(termocompromisso);

    if clbAlunosUnidade.Checked[x] = True then
    begin
      //Pega os dados da linha "marcada" e faz a consulta ao banco, gerando o termo diretamente para a impressora...
      mmMATRICULA := Copy(clbAlunosUnidade.Items.Strings[x],1,8);
      mmNOME      := Trim(Copy(clbAlunosUnidade.Items.Strings[x],10,300));

      {Recuperar  - NOME COMPLETO; - NASCIMENTO; - DOCUMENTO}
      with dmDeca.cdsSel_Cadastro do
      begin
        Close;
        Params.ParamByname ('@pe_cod_matricula').Value := mmMATRICULA;
        Params.ParamByname ('@pe_cod_unidade').Value   := 0;
        Open;

        doc.Content.Find.Execute ( FindText := '%BOLSISTA%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString );
        doc.Content.Find.Execute ( FindText := '%BOLSISTA2%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('nom_nome').AsString );
        doc.Content.Find.Execute ( FindText := '%MATRICULA%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('cod_matricula').AsString );
        doc.Content.Find.Execute ( FindText := '%NUMERO_DOCUMENTO1%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').AsString );
        doc.Content.Find.Execute ( FindText := '%NUMERO_DOCUMENTO2%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').AsString );
        //doc.Content.Find.Execute ( FindText := '%DATA_ADMISSAO%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').AsString );
        doc.Content.Find.Execute ( FindText := '%DATA_ADMISSAO%', replacewith := mskDataInicioContrato.Text);

        doc.Content.Find.Execute ( FindText := '%NASCIMENTO%', replacewith := dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsString );
      end;

      {Recuperar  - NOME DO RESPONS�VEL }
      with dmDeca.cdsSel_Cadastro_Familia_Responsavel do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value   := mmMATRICULA;
        Params.ParamByName ('@pe_ind_responsavel').Value := 1;
        Open;

        if RecordCount < 1 then
        begin
          mmRESPONSAVEL := '';
          mmRG          := '';
          mmCPF         := ''; 

        end
        else
        begin
          mmRESPONSAVEL := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_familiar').Value;
          if ( ( dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('ind_tipo_documento').Value ) = 'RG' ) then
          begin
            mmRG  := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('num_documento').Value;
            mmCPF := '';
          end
          else if ( ( dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('ind_tipo_documento').Value ) = 'CPF' ) then
          begin
            mmRG  := '';
            mmCPF := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('num_documento').Value;
          end;

          mmENDERECO_RESPONSAVEL := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_endereco').AsString + '  Complemento: ' +
                                    dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_complemento').AsString +  ' Bairro: ' +
                                    dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_bairro').AsString;

        end;
        
        doc.Content.Find.Execute ( FindText := '%NOME_RESPONSAVEL%', replacewith := dmDeca.cdsSel_Cadastro_Familia_Responsavel.FieldByName ('nom_familiar').AsString );
        doc.Content.Find.Execute ( FindText := '%RG_RESPONSAVEL%', replacewith := mmRG );
        doc.Content.Find.Execute ( FindText := '%CPF_RESPONSAVEL%', replacewith := mmCPF );
        doc.Content.Find.Execute ( FindText := '%ENDERECO_RESPONSAVEL%', replacewith :=  mmENDERECO_RESPONSAVEL );

      end;

      {Recuperar  - NOME DO PAI e NOME DA M�E}
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := mmMATRICULA;
        Params.ParamByName ('@pe_cod_familiar').Value  := Null;
        Open;

        while not dmDeca.cdsSel_Cadastro_Familia.eof do
        begin
          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'PAI' then
          begin
            mmPAI := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
            doc.Content.Find.Execute ( FindText := '%NOME_PAI%', replacewith := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').AsString );
          end;

          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'M�E' then
          begin
            mmMAE := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
            doc.Content.Find.Execute ( FindText := '%NOME_MAE%', replacewith := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').AsString );
          end;

          if mmPAI = '' then mmPAI := '-';
          if mmMAE = '' then mmMAE := '-';
          if mmRESPONSAVEL = '' then mmRESPONSAVEL := '-';

          dmDeca.cdsSel_Cadastro_Familia.Next;

        end;

      end;

      //doc3.PrintOut(false);
      winword.ActiveDocument.SaveAs(ExtractFilePath(Application.ExeName)+'\Aditamentos\Gravados\' + mmMATRICULA + '-' + mmNOME + '.doc');
      //winword.quit;
    end;
  end;

  winword.quit;

  Application.CreateForm (TrelFichaAdmissaoBolsista, relFichaAdmissaoBolsista);

  relFichaAdmissaoBolsista.lbNOME.Caption        := mmNOME;
  relFichaAdmissaoBolsista.lbMATRICULA.Caption   := mmMATRICULA;
  relFichaAdmissaoBolsista.lbCCUSTO.Caption      := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('nom_unidade').AsString);
  relFichaAdmissaoBolsista.lbENDERECO.Caption    := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('nom_endereco').AsString);

  if (dmDeca.cdsSel_Cadastro.FieldByName ('nom_complemento').IsNull) then
    relFichaAdmissaoBolsista.lbCOMPLEMENTO.Caption := '_____________'
  else
    relFichaAdmissaoBolsista.lbCOMPLEMENTO.Caption := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('nom_complemento').AsString);


  if (dmDeca.cdsSel_Cadastro.FieldByName ('nom_bairro').IsNull) then
    relFichaAdmissaoBolsista.lbBAIRRO.Caption := '_____________'
  else
    relFichaAdmissaoBolsista.lbBAIRRO.Caption := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('nom_bairro').AsString);

  relFichaAdmissaoBolsista.lbCEP.Caption         := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('num_cep').AsString);


  if (dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone').IsNull) then
    relFichaAdmissaoBolsista.lbTELEFONE.Caption := '_____________'
  else
    relFichaAdmissaoBolsista.lbTELEFONE.Caption := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('num_telefone').AsString);

  relFichaAdmissaoBolsista.lbNASCIMENTO.Caption  := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('dat_nascimento').AsString);


  if (dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').IsNull) then
    relFichaAdmissaoBolsista.lbRG.Caption := '_____________'
  else
    relFichaAdmissaoBolsista.lbRG.Caption        := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('num_rg').AsString);


  if (dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').IsNull) then
    relFichaAdmissaoBolsista.lbCPF.Caption := '___.___.___-__'
  else
    relFichaAdmissaoBolsista.lbCPF.Caption        := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('num_cpf').AsString);


  //if (dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').IsNull) then
  //  relFichaAdmissaoBolsista.lbADMISSAO.Caption  := '__/__/____'
  //else
  //  relFichaAdmissaoBolsista.lbADMISSAO.Caption  := GetValue(dmDeca.cdsSel_Cadastro.FieldByName ('dat_admissao').AsString);

  relFichaAdmissaoBolsista.lbADMISSAO.Caption  := mskDataInicioContrato.Text;
    
  relFichaAdmissaoBolsista.lbPAI.Caption         := mmPAI;
  relFichaAdmissaoBolsista.lbMAE.Caption         := mmMAE;
  relFichaAdmissaoBolsista.lbRESPONSAVEL.Caption := mmRESPONSAVEL;

  relFichaAdmissaoBolsista.Preview;
  relFichaAdmissaoBolsista.Free;


  Application.MessageBox ('Os Termos de Refer�ncia dos bolsistas selecionados foram gravados na pasta do Sistema ("Deca/Aditamentos/Gravados")' +#13+#10+
                          'Verifique a pasta para conferir os documentos e imprim�-los de acordo com a necessidade',
                          '[Sistema Deca]-Emiss�o de Termos de Refer�ncia',
                          MB_OK + MB_ICONINFORMATION);

  cbUnidade.ItemIndex := -1;
  clbAlunosUnidade.Clear;
  rgOpcaoImpressa.ItemIndex := 1;
  PageControl1.Pages[0].Caption := 'Alunos da Unidade "X"';


end;

procedure TfrmTermoBolsistas.FormCreate(Sender: TObject);
begin
  CoInitialize(nil);
end;

procedure TfrmTermoBolsistas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CoUninitialize;
end;

procedure TfrmTermoBolsistas.ZeraVariaveis;
begin

  mmMATRICULA            := '';
  mmRESPONSAVEL          := '';
  mmPAI                  := '';
  mmMAE                  := '';
  mmNOME                 := '';
  mmRG                   := '';
  mmCPF                  := '';
  mmENDERECO_RESPONSAVEL := '';




end;

end.
