object frmFichaPesquisa: TfrmFichaPesquisa
  Left = 386
  Top = 297
  Width = 633
  Height = 375
  Caption = 'Sistema DECA - Pesquisa'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010002002020100000000000E80200002600000010101000000000002801
    00000E0300002800000020000000400000000100040000000000800200000000
    0000000000000000000000000000000000000000800000800000008080008000
    0000800080008080000080808000C0C0C0000000FF0000FF000000FFFF00FF00
    0000FF00FF00FFFF0000FFFFFF00000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000F0F0000000000000000077
    777700088800088800077777700000700000888800F080088880000070000070
    88888000F8F0FF80008888807000007080000FF8FFF08FFFFF00008070000070
    80FFF8FFF8F0FF8FFFFFF0807000007080F8FFF8F7F0F7FFFFFFF08070000070
    90FFFF7FF8F0FF8F7FFFF090800000708097FFF8FFF08FFFFFF7F08080000070
    80FFF8FF77F0F77FFFFFF0808000007080F8F777FFF08FF777FFF0F080000070
    80F778FFF8F0FFFFFF77F0F08000007080F8FFF877F0877FFFFFF0F080000070
    80FFF777F8F0FFF777FFF0F080000070F0F77FF8FFF08FFFFF77F0F080000070
    F0FFF8FF77F0F77FFFFFF0F080000070F0F8F777FFF08FF777FFF0F080000070
    F0F778FFF8000FFFFF77F0F080000000F0F8FFF80000000FFFFFF0F000000000
    00FFF0000000000000FFF0000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFF83FFF8000000380000003800000038000000380000003800000038000
    0003800000038000000380000003800000038000000380000003800000038000
    000380000003800000038000000380038003E00FE00FF87FFC3FFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFF280000001000000020000000010004000000
    0000C00000000000000000000000000000000000000000000000000080000080
    00000080800080000000800080008080000080808000C0C0C0000000FF0000FF
    000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0000000000000000000000
    000000000000000000F0F00000000008880008880000088800F0F00888000800
    FFF0FFF00800080FF7F0F7FF080008097FF0FF7F0800080FF7F0F7FF0800080F
    77F0F77F0800080F7FF0FF7F0800080FFFF0FFFF0800000FF00000FF00000000
    00000000000000000000000000000000000000000000FFFF0000FC7F0000E00F
    0000000100000001000000010000000100000001000000010000000100000001
    00000001000001010000C7C70000FFFF0000FFFF0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TPanel
    Left = 0
    Top = 0
    Width = 625
    Height = 344
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 16
      Top = 16
      Width = 601
      Height = 137
      BorderStyle = bsSingle
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 19
        Width = 46
        Height = 13
        Caption = 'Pesquisar'
      end
      object Label2: TLabel
        Left = 16
        Top = 51
        Width = 33
        Height = 13
        Caption = 'Campo'
      end
      object edPesquisa: TEdit
        Left = 72
        Top = 16
        Width = 353
        Height = 21
        Hint = 'Informe o "VALOR" a ser pesquisado'
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnKeyPress = edPesquisaKeyPress
      end
      object cbCampoPesquisa: TComboBox
        Left = 72
        Top = 48
        Width = 225
        Height = 21
        Hint = 'Selecione qual o CAMPO deseja realizar a pesquisa'
        Style = csDropDownList
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = cbCampoPesquisaChange
        Items.Strings = (
          'Matr�cula'
          'Prontu�rio'
          'Nome da Crian�a / Adolescente'
          'Nome do Respons�vel')
      end
      object rgCriterio: TRadioGroup
        Left = 14
        Top = 80
        Width = 299
        Height = 41
        Hint = 'Selecione o CRIT�RIO de busca'
        Caption = 'Crit�rio de Busca'
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'In�cio do nome'
          'Trecho do nome')
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = rgCriterioClick
      end
      object btnPesquisa: TBitBtn
        Left = 464
        Top = 72
        Width = 107
        Height = 49
        Hint = 
          'Clique para EXIBIR os resultados obtidos atrav�s do crit�rio est' +
          'abelecido'
        Caption = '&Pesquisar'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btnPesquisaClick
      end
    end
    object dbgPesquisa: TDBGrid
      Left = 16
      Top = 156
      Width = 603
      Height = 175
      Hint = 'RESULTADOS OBTIDOS'
      DataSource = dsSel_Cadastro_Pesquisa
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = dbgPesquisaDrawColumnCell
      OnDblClick = dbgPesquisaDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cod_matricula'
          Title.Alignment = taCenter
          Title.Caption = '[Mat.]'
          Width = 60
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'vStatus'
          Title.Alignment = taCenter
          Title.Caption = '[Situa��o]'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_nome'
          Title.Caption = '[Nome]'
          Width = 225
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_unidade'
          Title.Caption = '[Unidade]'
          Width = 225
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nom_familiar'
          Title.Caption = '[Respons�vel]'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ind_vinculo'
          Title.Caption = '[V�nculo]'
          Visible = True
        end>
    end
  end
  object dsSel_Cadastro_Pesquisa: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro_Pesquisa
    Left = 578
    Top = 282
  end
end
