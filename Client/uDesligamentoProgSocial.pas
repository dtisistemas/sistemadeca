unit uDesligamentoProgSocial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls;

type
  TfrmDesligamentoProgSocial = class(TForm)
    GroupBox1: TGroupBox;
    meDataDesligamento: TMaskEdit;
    btnEfetuarDesligamento: TSpeedButton;
    rgSituacao: TRadioGroup;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnEfetuarDesligamentoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDesligamentoProgSocial: TfrmDesligamentoProgSocial;

implementation

uses uDM, uUtil, uFichaCrianca;

{$R *.DFM}

procedure TfrmDesligamentoProgSocial.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmDesligamentoProgSocial.Close;
  end;
end;

procedure TfrmDesligamentoProgSocial.btnEfetuarDesligamentoClick(
  Sender: TObject);
begin
  //Altera o status para "2 - Desligado" para o familiar selecionado no grid do frmFichaCrianca, Aba Programas Sociais
  //Validar pelo menos 1 item do rgroup
  try
  with dmDeca.cdsAlt_Status_ProgSocial do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_cad_progsocial').AsInteger := dmDeca.cdsSel_Cadastro_Programa_Social.FieldByName('cod_id_cad_progsocial').AsInteger;

    if mmStatus = 'Ativa' then
    begin
      Params.ParamByName('@pe_tipo').Value := 'A';
      Params.ParamByName('@pe_dat_insercao').Value := GetValue(meDataDesligamento, vtDate);
      Params.ParamByName('@pe_flg_status').AsInteger := 1 //Ativo
    end
    else if mmStatus = 'Desliga' then
    begin
      Params.ParamByName('@pe_tipo').Value := 'D';
      Params.ParamByName('@pe_dat_desligamento').Value := GetValue(meDataDesligamento, vtDate);
      Params.ParamByName('@pe_flg_status').AsInteger := 2 //Desligado
    end
    else if mmStatus = 'Outros' then
    begin
      Params.ParamByName('@pe_tipo').Value := 'O';
      Params.ParamByName('@pe_dat_insercao').Value := GetValue(meDataDesligamento, vtDate);
      Params.ParamByName('@pe_flg_status').AsInteger := rgSituacao.ItemIndex //Outra op��o
    end;
    Execute;
    frmDesligamentoProgSocial.Close;
  end;
  except end;
end;

end.
