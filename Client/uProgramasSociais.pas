unit uProgramasSociais;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Mask, Grids, DBGrids, Db;

type
  TfrmProgramasSociais = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Panel2: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    edProgramaSocial: TEdit;
    edOrigem: TEdit;
    lbSituacao: TLabel;
    edDataAltera: TMaskEdit;
    dbgProgramasSociais: TDBGrid;
    dsSel_ProgSocial: TDataSource;
    btnDesativar: TBitBtn;
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaTela (Modo:String);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure dbgProgramasSociaisDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgProgramasSociaisDblClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProgramasSociais: TfrmProgramasSociais;

implementation

uses uDM, uUtil, uPrincipal;

{$R *.DFM}

procedure TfrmProgramasSociais.AtualizaTela(Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    edProgramaSocial.Clear;
    GroupBox1.Enabled := False;
    edOrigem.Clear;
    GroupBox2.Enabled := False;
    lbSituacao.Caption := '';
    GroupBox3.Enabled := False;
    edDataAltera.Clear;
    GroupBox4.Enabled := False;

    Panel1.Enabled := True;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
    btnDesativar.Enabled := True;

  end
  else if Modo = 'Novo' then
  begin

    GroupBox1.Enabled := True;
    edProgramaSocial.Clear;
    GroupBox2.Enabled := True;
    edOrigem.Clear;
    GroupBox3.Enabled := True;
    lbSituacao.Caption := '';
    GroupBox4.Enabled := True;
    edDataAltera.Clear;

    Panel1.Enabled := False;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
    btnDesativar.Enabled := False;

  end
  else if Modo = 'Alterar' then
  begin

    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    GroupBox4.Enabled := True;

    Panel1.Enabled := False;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
    btnDesativar.Enabled := False;

  end;
end;

procedure TfrmProgramasSociais.btnSairClick(Sender: TObject);
begin
  frmProgramasSociais.Close;
end;

procedure TfrmProgramasSociais.FormShow(Sender: TObject);
begin
  AtualizaTela ('Padr�o');
  try
  with dmDeca.cdsSel_ProgSocial do
  begin
    Close;
    Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
    Params.ParamByName('@pe_dsc_progsocial').Value := Null;
    Open;
    dbgProgramasSociais.Refresh;
  end;
  except end;
end;

procedure TfrmProgramasSociais.btnCancelarClick(Sender: TObject);
begin
  AtualizaTela ('Padr�o');
end;

procedure TfrmProgramasSociais.btnNovoClick(Sender: TObject);
begin
  AtualizaTela ('Novo');
  lbSituacao.Caption := 'ATIVO';
  edProgramaSocial.SetFocus;
end;

procedure TfrmProgramasSociais.dbgProgramasSociaisDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  {if dmDeca.cdsSel_ProgSocial.FieldByName('flg_status').AsInteger = 0 then
  begin
    dbgProgramasSociais.Canvas.Font.Color:= clWhite;
    dbgProgramasSociais.Canvas.Brush.Color:= clGreen;
  end
  else
  begin
    dbgProgramasSociais.Canvas.Font.Color:= clBlack;
    dbgProgramasSociais.Canvas.Brush.Color:= clWhite;
  end;

  dbgProgramasSociais.Canvas.FillRect(Rect);
  dbgProgramasSociais.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);
  }
end;

procedure TfrmProgramasSociais.dbgProgramasSociaisDblClick(
  Sender: TObject);
begin
  AtualizaTela ('Alterar');
  edProgramaSocial.Text := GetValue(dmDeca.cdsSel_ProgSocial.FieldByName('dsc_progsocial').AsString);

  if Length(dmDeca.cdsSel_ProgSocial.FieldByName('dsc_origem').AsString) > 0 then
    edOrigem.Text := dmDeca.cdsSel_ProgSocial.FieldByName('dsc_origem').AsString
  else
    edOrigem.Clear;

  //if Length(dmDeca.cdsSel_ProgSocial.FieldByName('Altera��o').AsString) > 0 then
  //  edDataAltera.Text := DateToStr(dmDeca.cdsSel_ProgSocial.FieldByName('log_data_altera').AsDateTime)
  //else
  //  edDataAltera.Clear;

  lbSituacao.Caption := dmDeca.cdsSel_ProgSocial.FieldByName('vStatus').AsString;
end;

procedure TfrmProgramasSociais.btnSalvarClick(Sender: TObject);
begin
  //Validar o campo Programa Social e verificar se o mesmo j� existe
  try
    with dmDeca.cdsSel_ProgSocial do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
      Params.ParamByName('@pe_dsc_progsocial').Value := Null;
      Open;

      if RecordCount > 0 then
      begin
        ShowMessage ('Prograna Social j� cadastrado...');
        edProgramaSocial.SetFocus;
        edProgramaSocial.Clear;
      end
      else
      begin
        with dmDeca.cdsInc_ProgSocial do
        begin
          Close;
          Params.ParamByName('@pe_dsc_progsocial').Value := GetValue(edProgramaSocial.Text);
          Params.ParamByName('@pe_dsc_origem').Value := GetValue(edOrigem.Text);
          Params.ParamByName('@pe_flg_status').Value := 0;
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

          //Atualiza os dados do grid
          with dmDeca.cdsSel_ProgSocial do
          begin
            Close;
            Params.ParamByName('@pe_cod_id_progsocial').Value := Null;
            Params.ParamByName('@pe_dsc_progsocial').Value := Null;
            Open;
            dbgProgramasSociais.Refresh;
          end
        end
      end
    end
  except
    ShowMessage ('Um erro ocorreu e os dados n�o foram lan�ados...');
  end;

  AtualizaTela ('Padr�o');

end;

end.
