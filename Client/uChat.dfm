object frmChat: TfrmChat
  Left = 323
  Top = 133
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Sistema DECA - [FUNDHAS]   -   Chat'
  ClientHeight = 405
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object lbUsuarios: TListBox
    Left = 2
    Top = 30
    Width = 181
    Height = 253
    ItemHeight = 13
    TabOrder = 0
    OnClick = lbUsuariosClick
  end
  object Panel1: TPanel
    Left = 2
    Top = 3
    Width = 183
    Height = 26
    BevelInner = bvLowered
    Caption = 'Usu�rios Conectados'
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 186
    Top = 3
    Width = 358
    Height = 27
    BevelInner = bvLowered
    Caption = 'Janela de Mensagens'
    TabOrder = 2
  end
  object meMensagens: TMemo
    Left = 186
    Top = 31
    Width = 356
    Height = 255
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object Panel3: TPanel
    Left = 2
    Top = 289
    Width = 542
    Height = 93
    BevelInner = bvLowered
    TabOrder = 4
    object lbMensagemPara: TLabel
      Left = 8
      Top = 8
      Width = 119
      Height = 13
      Caption = 'Mensagem a ser enviada'
    end
    object btnSair: TSpeedButton
      Left = 449
      Top = 24
      Width = 86
      Height = 47
      Caption = 'Sair'
      OnClick = btnSairClick
    end
    object meMensagemPara: TMemo
      Left = 8
      Top = 26
      Width = 436
      Height = 42
      TabOrder = 0
      OnKeyDown = meMensagemParaKeyDown
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 382
    Width = 547
    Height = 23
    Panels = <
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object ClientSocket1: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = ClientSocket1Connect
    OnRead = ClientSocket1Read
    OnError = ClientSocket1Error
    Left = 192
    Top = 38
  end
  object ServerSocket1: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    OnAccept = ServerSocket1Accept
    OnClientDisconnect = ServerSocket1ClientDisconnect
    OnClientRead = ServerSocket1ClientRead
    Left = 222
    Top = 37
  end
end
