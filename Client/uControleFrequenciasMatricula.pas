unit uControleFrequenciasMatricula;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, ExtCtrls, Mask, Db;

type
  TfrmControleFrequenciasMatricula = class(TForm)
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    edDia01: TEdit;
    edDia02: TEdit;
    edDia03: TEdit;
    edDia06: TEdit;
    edDia04: TEdit;
    edDia05: TEdit;
    edDia09: TEdit;
    edDia07: TEdit;
    edDia08: TEdit;
    edDia10: TEdit;
    edDia11: TEdit;
    edDia12: TEdit;
    edDia13: TEdit;
    edDia14: TEdit;
    edDia15: TEdit;
    edDia16: TEdit;
    edDia17: TEdit;
    edDia18: TEdit;
    edDia19: TEdit;
    edDia20: TEdit;
    edDia21: TEdit;
    edDia22: TEdit;
    edDia23: TEdit;
    edDia24: TEdit;
    edDia25: TEdit;
    edDia26: TEdit;
    edDia27: TEdit;
    edDia28: TEdit;
    edDia29: TEdit;
    edDia30: TEdit;
    edDia31: TEdit;
    edFaltas: TEdit;
    edPorcentagem: TEdit;
    Panel1: TPanel;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    Label1: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Label36: TLabel;
    Label37: TLabel;
    btnCalcularFaltas: TSpeedButton;
    chkMarcarPresenca: TCheckBox;
    Label38: TLabel;
    edMaxFaltas: TEdit;
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnCalcularFaltasClick(Sender: TObject);
    procedure chkMarcarPresencaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmControleFrequenciasMatricula: TfrmControleFrequenciasMatricula;
  total_faltas_m, total_presencas_m, total_sdrc_m: Integer;

implementation

uses uFichaPesquisa, uPrincipal, uDM, uUtil;

{$R *.DFM}

procedure TfrmControleFrequenciasMatricula.btnPesquisarClick(
  Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        //Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        if vvIND_PERFIL in [1,8,20,23,24,25] then
          Params.ParamByName('@pe_cod_unidade').Value:= 0
        else
          Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;

        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;

        //Carregar os dados de frequencias do aluno selecionado
        try
          if Length(txtMatricula.Text) = 8 then
          begin
            with dmDeca.cdsSel_Cadastro_Frequencia do
            begin
              Close;
              Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
              Params.ParamByName ('@pe_cod_unidade').Value := Null;
              Params.ParamByName ('@pe_num_ano').Value := Null;
              Params.ParamByName ('@pe_num_mes').Value := Null;
              Params.ParamByName ('@pe_cod_turma').Value := Null;
              Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
              Open;
              DBGrid1.Refresh;
            end
          end
          else
          begin
            ShowMessage ('N�mero de Matr�cula inv�lido...Favor verificar...');
            txtMatricula.SetFocus;
            txtMatricula.Clear;
          end
        except end;
      end;
    except end;
  end;
end;

procedure TfrmControleFrequenciasMatricula.txtMatriculaExit(
  Sender: TObject);
begin
  if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      btnAlterar.Enabled := True;
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
          lbNome.Caption := 'Ficha n�o cadastrada'
        else
          //Carregar os dados de frequencia do aluno selecionado
          begin
            lbNome.Caption := FieldByName('nom_nome').AsString;

            try
              if Length(txtMatricula.Text) = 8 then
              begin
                with dmDeca.cdsSel_Cadastro_Frequencia do
                begin
                  Close;
                  Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);
                  Params.ParamByName ('@pe_cod_unidade').Value := Null;
                  Params.ParamByName ('@pe_num_ano').Value := Null;
                  Params.ParamByName ('@pe_num_mes').Value := Null;
                  Params.ParamByName ('@pe_cod_turma').Value := Null;
                  Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
                  Open;

                  DBGrid1.Refresh;

                end
              end
              else
              begin
                ShowMessage ('N�mero de Matr�cula inv�lido...Favor verificar...');
                txtMatricula.SetFocus;
                txtMatricula.Clear;
              end
            except end;

          end;

      end;
    except end;
  end;

end;

procedure TfrmControleFrequenciasMatricula.DBGrid1CellClick(
  Column: TColumn);
begin

  chkMarcarPresenca.Visible := True;
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_EXPORTOU').Value = 0) then
  begin

    btnCalcularFaltas.Enabled := True;
    btnAlterar.Enabled := True;

    //Dia 01
    if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'S') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'D') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'R') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString = 'C') or
       (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').Value = Null) then
    begin
      edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
      edDia01.Enabled := False;
      edDia01.Font.Color := clRed;
    end
    else
    begin
      edDia01.Enabled := True;
      edDia01.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA01').AsString;
      edDia01.Font.Color := clBlack;
    end
  end
  else
  begin
    btnCalcularFaltas.Enabled := False;
    btnAlterar.Enabled := False;
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Esses dados j� foram exportados e n�o podem ser alterados.',
                            '[Sistema Deca] - Controle de Frequ�ncias',
                            MB_OK + MB_ICONQUESTION);
  end;

  //Dia 02
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').Value = Null) then
  begin
    edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
    edDia02.Enabled := False;
    edDia02.Font.Color := clRed;
  end
  else
  begin
    edDia02.Enabled := True;
    edDia02.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA02').AsString;
    edDia02.Font.Color := clBlack;
  end;

  //Dia 03
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').Value = Null) then
  begin
    edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
    edDia03.Enabled := False;
    edDia03.Font.Color := clRed;
  end
  else
  begin
    edDia03.Enabled := True;
    edDia03.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA03').AsString;
    edDia03.Font.Color := clBlack;
  end;

  //Dia 04
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').Value = Null) then
  begin
    edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
    edDia04.Enabled := False;
    edDia04.Font.Color := clRed;
  end
  else
  begin
    edDia04.Enabled := True;
    edDia04.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA04').AsString;
    edDia04.Font.Color := clBlack;
  end;

  //Dia 05
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').Value = Null) then
  begin
    edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
    edDia05.Enabled := False;
    edDia05.Font.Color := clRed;
  end
  else
  begin
    edDia05.Enabled := True;
    edDia05.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA05').AsString;
    edDia05.Font.Color := clBlack;
  end;

  //Dia 06
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').Value = Null) then
  begin
    edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
    edDia06.Enabled := False;
    edDia06.Font.Color := clRed;
  end
  else
  begin
    edDia06.Enabled := True;
    edDia06.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA06').AsString;
    edDia06.Font.Color := clBlack;
  end;

  //Dia 07
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').Value = Null) then
  begin
    edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
    edDia07.Enabled := False;
    edDia07.Font.Color := clRed;
  end
  else
  begin
    edDia07.Enabled := True;
    edDia07.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA07').AsString;
    edDia07.Font.Color := clBlack;
  end;

  //Dia 08
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').Value = Null) then
  begin
    edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
    edDia08.Enabled := False;
    edDia08.Font.Color := clRed;
  end
  else
  begin
    edDia08.Enabled := True;
    edDia08.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA08').AsString;
    edDia08.Font.Color := clBlack;
  end;

  //Dia 09
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').Value = Null) then
  begin
    edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
    edDia09.Enabled := False;
    edDia09.Font.Color := clRed;
  end
  else
  begin
    edDia09.Enabled := True;
    edDia09.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA09').AsString;
    edDia09.Font.Color := clBlack;
  end;

  //Dia 10
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').Value = Null) then
  begin
    edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
    edDia10.Enabled := False;
    edDia10.Font.Color := clRed;
  end
  else
  begin
    edDia10.Enabled := True;
    edDia10.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA10').AsString;
    edDia10.Font.Color := clBlack;
  end;

  //Dia 11
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').Value = Null) then
  begin
    edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
    edDia11.Enabled := False;
    edDia11.Font.Color := clRed;
  end
  else
  begin
    edDia11.Enabled := True;
    edDia11.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA11').AsString;
    edDia11.Font.Color := clBlack;
  end;

  //Dia 12
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').Value = Null) then
  begin
    edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
    edDia12.Enabled := False;
    edDia12.Font.Color := clRed;
  end
  else
  begin
    edDia12.Enabled := True;
    edDia12.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA12').AsString;
    edDia12.Font.Color := clBlack;
  end;

  //Dia 13
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').Value = Null) then
  begin
    edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
    edDia13.Enabled := False;
    edDia13.Font.Color := clRed;
  end
  else
  begin
    edDia13.Enabled := True;
    edDia13.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA13').AsString;
    edDia13.Font.Color := clBlack;
  end;

  //Dia 14
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').Value = Null) then
  begin
    edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
    edDia14.Enabled := False;
    edDia14.Font.Color := clRed;
  end
  else
  begin
    edDia14.Enabled := True;
    edDia14.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA14').AsString;
    edDia14.Font.Color := clBlack;
  end;

  //Dia 15
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').Value = Null) then
  begin
    edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
    edDia15.Enabled := False;
    edDia15.Font.Color := clRed;
  end
  else
  begin
    edDia15.Enabled := True;
    edDia15.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA15').AsString;
    edDia15.Font.Color := clBlack;
  end;

  //Dia 16
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').Value = Null) then
  begin
    edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
    edDia16.Enabled := False;
    edDia16.Font.Color := clRed;
  end
  else
  begin
    edDia16.Enabled := True;
    edDia16.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA16').AsString;
    edDia16.Font.Color := clBlack;
  end;

  //Dia 17
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').Value = Null) then
  begin
    edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
    edDia17.Enabled := False;
    edDia17.Font.Color := clRed;
  end
  else
  begin
    edDia17.Enabled := True;
    edDia17.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA17').AsString;
    edDia17.Font.Color := clBlack;
  end;

  //Dia 18
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').Value = Null) then
  begin
    edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
    edDia18.Enabled := False;
    edDia18.Font.Color := clRed;
  end
  else
  begin
    edDia18.Enabled := True;
    edDia18.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA18').AsString;
    edDia18.Font.Color := clBlack;
  end;

  //Dia 19
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').Value = Null) then
  begin
    edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
    edDia19.Enabled := False;
    edDia19.Font.Color := clRed;
  end
  else
  begin
    edDia19.Enabled := True;
    edDia19.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA19').AsString;
    edDia19.Font.Color := clBlack;
  end;

  //Dia 20
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').Value = Null) then
  begin
    edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
    edDia20.Enabled := False;
    edDia20.Font.Color := clRed;
  end
  else
  begin
    edDia20.Enabled := True;
    edDia20.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA20').AsString;
    edDia20.Font.Color := clBlack;
  end;

  //Dia 21
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').Value = Null) then
  begin
    edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
    edDia21.Enabled := False;
    edDia21.Font.Color := clRed;
  end
  else
  begin
    edDia21.Enabled := True;
    edDia21.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA21').AsString;
    edDia21.Font.Color := clBlack;
  end;

  //Dia 22
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').Value = Null) then
  begin
    edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
    edDia22.Enabled := False;
    edDia22.Font.Color := clRed;
  end
  else
  begin
    edDia22.Enabled := True;
    edDia22.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA22').AsString;
    edDia22.Font.Color := clBlack;
  end;

  //Dia 23
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').Value = Null) then
  begin
    edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
    edDia23.Enabled := False;
    edDia23.Font.Color := clRed;
  end
  else
  begin
    edDia23.Enabled := True;
    edDia23.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA23').AsString;
    edDia23.Font.Color := clBlack;
  end;

  //Dia 24
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').Value = Null) then
  begin
    edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
    edDia24.Enabled := False;
    edDia24.Font.Color := clRed;
  end
  else
  begin
    edDia24.Enabled := True;
    edDia24.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA24').AsString;
    edDia24.Font.Color := clBlack;
  end;

  //Dia 25
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').Value = Null) then
  begin
    edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
    edDia25.Enabled := False;
    edDia25.Font.Color := clRed;
  end
  else
  begin
    edDia25.Enabled := True;
    edDia25.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA25').AsString;
    edDia25.Font.Color := clBlack;
  end;

  //Dia 26
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').Value = Null) then
  begin
    edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
    edDia26.Enabled := False;
    edDia26.Font.Color := clRed;
  end
  else
  begin
    edDia26.Enabled := True;
    edDia26.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA26').AsString;
    edDia26.Font.Color := clBlack;
  end;

  //Dia 27
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').Value = Null) then
  begin
    edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
    edDia27.Enabled := False;
    edDia27.Font.Color := clRed;
  end
  else
  begin
    edDia27.Enabled := True;
    edDia27.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA27').AsString;
    edDia27.Font.Color := clBlack;
  end;

  //Dia 28
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').Value = Null) then
  begin
    edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
    edDia28.Enabled := False;
    edDia28.Font.Color := clRed;
  end
  else
  begin
    edDia28.Enabled := True;
    edDia28.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA28').AsString;
    edDia28.Font.Color := clBlack;
  end;

  //Dia 29
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').Value = Null) then
  begin
    edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
    edDia29.Enabled := False;
    edDia29.Font.Color := clRed;
  end
  else
  begin
    edDia29.Enabled := True;
    edDia29.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA29').AsString;
    edDia29.Font.Color := clBlack;
  end;

  //Dia 30
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').Value = Null) then
  begin
    edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
    edDia30.Enabled := False;
    edDia30.Font.Color := clRed;
  end
  else
  begin
    edDia30.Enabled := True;
    edDia30.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA30').AsString;
    edDia30.Font.Color := clBlack;
  end;

  //Dia 31
  if (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'S') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'D') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'R') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString = 'C') or
     (dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').Value = Null) then
  begin
    edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
    edDia31.Enabled := False;
    edDia31.Font.Color := clRed;
  end
  else
  begin
    edDia31.Enabled := True;
    edDia31.Text := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('FLG_DIA31').AsString;
    edDia31.Font.Color := clBlack;
  end;

  //edFaltas.Text := IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').AsInteger);
  Label36.Caption := IntToStr(dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('dsr_acumulado').AsInteger);
  edFaltas.Text := edMaxFaltas.Text;
end;

procedure TfrmControleFrequenciasMatricula.btnAlterarClick(
  Sender: TObject);
var mm_DATAm : String[7];
    total_faltas_mes, total_faltas_semana : Integer;
    total_dsr_mes, total_dsr_semana, dsr_acumulado : Integer;
    semana_atual : Integer;
begin
  btnAlterar.Enabled := False;
  mm_DATAm := '';
  total_faltas_mes := 0;
  total_faltas_semana := 0;
  total_dsr_semana := 0;
  //total_dsr_mes := 0;
  dsr_acumulado := 0;

  if Application.MessageBox('Deseja alterar dados de frequ�ncia da c�a/adolescente?',
           'Frequ�ncia - Altera��o',
           MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin

    total_faltas_semana := 0;
    total_faltas_mes := 0;
    total_dsr_semana := 0;
    total_dsr_mes := 0;

    semana_atual := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia01').AsInteger;

    //********************************************************************************************************************************
    try
      //*** Dia 01 ***//
      if (edDia01.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia01').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia01').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia01.Text.....


      //*** Dia 02 ***//
      if (edDia02.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia02').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia02').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia02.Text.....


      //*** Dia 03 ***//
      if (edDia03.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia03').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia03').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia03.Text.....


      //*** Dia 04 ***//
      if (edDia04.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia04').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia04').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia04.Text.....


      //*** Dia 05 ***//
      if (edDia05.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia05').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia05').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia05.Text.....


      //*** Dia 06 ***//
      if (edDia06.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia06').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia06').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia06.Text.....


      //*** Dia 07 ***//
      if (edDia07.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia07').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia07').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia07.Text.....


      //*** Dia 08 ***//
      if (edDia08.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia08').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia08').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia08.Text.....


      //*** Dia 09 ***//
      if (edDia09.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia09').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia09').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia09.Text.....


      //*** Dia 10 ***//
      if (edDia10.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia10').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia10').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia10.Text.....


      //*** Dia 11 ***//
      if (edDia11.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia11').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia11').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia11.Text.....


      //*** Dia 12 ***//
      if (edDia12.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia12').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia12').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia12.Text.....


      //*** Dia 13 ***//
      if (edDia13.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia13').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia13').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia13.Text.....


      //*** Dia 14 ***//
      if (edDia14.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia14').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia14').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia14.Text.....


      //*** Dia 15 ***//
      if (edDia15.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia15').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia15').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia15.Text.....


      //*** Dia 16 ***//
      if (edDia16.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia16').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia16').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia16.Text.....



      //*** Dia 17 ***//
      if (edDia17.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia17').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia17').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia17.Text.....


      //*** Dia 18 ***//
      if (edDia18.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia18').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia18').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia18.Text.....


      //*** Dia 19 ***//
      if (edDia19.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia19').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia19').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia19.Text.....


      //*** Dia 20 ***//
      if (edDia20.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia20').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia20').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia20.Text.....


      //*** Dia 21 ***//
      if (edDia21.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia21').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia21').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia21.Text.....


      //*** Dia 22 ***//
      if (edDia22.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia22').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia22').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia22.Text.....


      //*** Dia 23 ***//
      if (edDia23.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia23').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia23').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia23.Text.....


      //*** Dia 24 ***//
      if (edDia24.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia24').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia24').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia24.Text.....


      //*** Dia 25 ***//
      if (edDia25.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia25').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia25').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia25.Text.....


      //*** Dia 26 ***//
      if (edDia26.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia26').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia26').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia26.Text.....


      //*** Dia 27 ***//
      if (edDia27.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia27').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia27').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia27.Text.....


      //*** Dia 28 ***//
      if (edDia28.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia28').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia28').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia28.Text.....


      //*** Dia 29 ***//
      if (edDia29.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia29').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia29').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia29.Text.....


      //*** Dia 30 ***//
      if (edDia30.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia30').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia30').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia30.Text.....



      //*** Dia 31 ***//
      if (edDia31.Text = 'F') then
      begin
        //Verifica se a semana � a mesma do dia anterior
        if(semana_atual = dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia31').Value) then
        begin
          //Verifica se existem feriados na semana
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end
          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end //Fim if(semana_atual = .......

        //Se a semana atual for diferente da anterior
        else
        begin
          //Come�ando "nova" semana, "zera-se" o contador de faltas e dsr para a semana
          //total_faltas_semana := 0;
          total_dsr_semana := 0;

          //Verifica se existem feriados
          with dmDeca.cdsConta_Feriados do
          begin
            Close;
            //Params.ParamByName('@pe_num_semana').Value := semana_atual;
            Params.ParamByName('@pe_num_semana').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('semana_dia31').Value;//semana_atual;
            Params.ParamByName('@pe_num_ano').Value := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName('num_ano').Value;
            Open;

            //Se n�o encontrou feriado
            if (RecordCount < 1) then
            begin
              //Somar a falta ao total de faltas da semana
              //total_faltas_semana := total_faltas_semana + 1;
              total_dsr_semana := 1; //Como n�o h� feriados, conta como DSR s� no Domingo
            end
            //Se encontrou
            else if (RecordCount > 0) then
            begin
              //total_faltas_semana := total_faltas_semana +1;
              total_dsr_semana := (RecordCount) + 1;  //Perde-se os "feriados" mais o Domingo
            end;

          end;  //Fim ...with dmDeca.cdsConta_Feriados do .....

          //Repassa os valores encontrados para os valores mensais
          //total_faltas_mes := total_faltas_mes + total_faltas_semana;
          total_dsr_mes := total_dsr_mes + total_dsr_semana;

        end;

      end;  //if (edDia31.Text.....



      with dmDeca.cdsAlt_Cadastro_Frequencia do
      begin
        Close;
        Params.ParamByName ('@pe_id_frequencia').AsInteger := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('id_frequencia').AsInteger;
        Params.ParamByName ('@pe_dia01').Value := GetValue(edDia01.Text);
        Params.ParamByName ('@pe_dia02').Value := GetValue(edDia02.Text);
        Params.ParamByName ('@pe_dia03').Value := GetValue(edDia03.Text);
        Params.ParamByName ('@pe_dia04').Value := GetValue(edDia04.Text);
        Params.ParamByName ('@pe_dia05').Value := GetValue(edDia05.Text);
        Params.ParamByName ('@pe_dia06').Value := GetValue(edDia06.Text);
        Params.ParamByName ('@pe_dia07').Value := GetValue(edDia07.Text);
        Params.ParamByName ('@pe_dia08').Value := GetValue(edDia08.Text);
        Params.ParamByName ('@pe_dia09').Value := GetValue(edDia09.Text);
        Params.ParamByName ('@pe_dia10').Value := GetValue(edDia10.Text);
        Params.ParamByName ('@pe_dia11').Value := GetValue(edDia11.Text);
        Params.ParamByName ('@pe_dia12').Value := GetValue(edDia12.Text);
        Params.ParamByName ('@pe_dia13').Value := GetValue(edDia13.Text);
        Params.ParamByName ('@pe_dia14').Value := GetValue(edDia14.Text);
        Params.ParamByName ('@pe_dia15').Value := GetValue(edDia15.Text);
        Params.ParamByName ('@pe_dia16').Value := GetValue(edDia16.Text);
        Params.ParamByName ('@pe_dia17').Value := GetValue(edDia17.Text);
        Params.ParamByName ('@pe_dia18').Value := GetValue(edDia18.Text);
        Params.ParamByName ('@pe_dia19').Value := GetValue(edDia19.Text);
        Params.ParamByName ('@pe_dia20').Value := GetValue(edDia20.Text);
        Params.ParamByName ('@pe_dia21').Value := GetValue(edDia21.Text);
        Params.ParamByName ('@pe_dia22').Value := GetValue(edDia22.Text);
        Params.ParamByName ('@pe_dia23').Value := GetValue(edDia23.Text);
        Params.ParamByName ('@pe_dia24').Value := GetValue(edDia24.Text);
        Params.ParamByName ('@pe_dia25').Value := GetValue(edDia25.Text);
        Params.ParamByName ('@pe_dia26').Value := GetValue(edDia26.Text);
        Params.ParamByName ('@pe_dia27').Value := GetValue(edDia27.Text);
        Params.ParamByName ('@pe_dia28').Value := GetValue(edDia28.Text);
        Params.ParamByName ('@pe_dia29').Value := GetValue(edDia29.Text);
        Params.ParamByName ('@pe_dia30').Value := GetValue(edDia30.Text);
        Params.ParamByName ('@pe_dia31').Value := GetValue(edDia31.Text);
        Params.ParamByName ('@pe_num_faltas').AsInteger := StrToInt(edFaltas.Text);
        Params.ParamByName ('@pe_dsr_acumulado').AsInteger := total_dsr_mes;
        Execute;

        DBGrid1.Refresh;

        chkMarcarPresenca.Checked := False;

        {
        //Atualiza o Grid
        with dmDeca.cdsSel_Cadastro_Frequencia do
        begin
          Close;
          Params.ParamByName ('@pe_id_frequencia').Value := Null;
          Params.ParamByName ('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName ('@pe_num_ano').Value := Null;
          Params.ParamByName ('@pe_num_mes').Value := Null;
          Params.ParamByName ('@pe_cod_turma').Value := Null;
          Params.ParamByName ('@pe_log_cod_usuario').Value := Null;
          Open;
          DBGrid1.Refresh;
        end;
        }
          //Limpa os edits referentes aos dias
          edDia01.Clear;
          edDia02.Clear;
          edDia03.Clear;
          edDia04.Clear;
          edDia05.Clear;
          edDia06.Clear;
          edDia07.Clear;
          edDia08.Clear;
          edDia09.Clear;
          edDia10.Clear;
          edDia11.Clear;
          edDia12.Clear;
          edDia13.Clear;
          edDia14.Clear;
          edDia15.Clear;
          edDia16.Clear;
          edDia17.Clear;
          edDia18.Clear;
          edDia19.Clear;
          edDia20.Clear;
          edDia21.Clear;
          edDia22.Clear;
          edDia23.Clear;
          edDia24.Clear;
          edDia25.Clear;
          edDia26.Clear;
          edDia27.Clear;
          edDia28.Clear;
          edDia29.Clear;
          edDia30.Clear;
          edDia31.Clear;
      end;
    except
      Application.MessageBox('Ocorreu um ERRO!'+#13+#10+#13+#10+
             'Os dados de frequ�ncia n�o foram alterados. Favor entrar em contato com o Administrador do Sistema.',
             'Sistema DECA - Controle de Frequ�ncia',
             MB_OK + MB_ICONERROR);
    end;
  end;





end;

procedure TfrmControleFrequenciasMatricula.btnSairClick(Sender: TObject);
begin
  frmControleFrequenciasMatricula.Close;
end;

procedure TfrmControleFrequenciasMatricula.btnCalcularFaltasClick(
  Sender: TObject);
begin

  //*****************************************************************************
  //Criar rotina para calcular total_faltas_m, igual ao exemplo acima
  total_faltas_m := 0;
  if (edDia01.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia02.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia03.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia04.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia05.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia06.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia07.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia08.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia09.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia10.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia11.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia12.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia13.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia14.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia15.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia16.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia17.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia18.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia19.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia20.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia21.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia22.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia23.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia24.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia25.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia26.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia27.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia28.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia29.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia30.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;
  if (edDia31.Text = 'F') then total_faltas_m := total_faltas_m + 1 ;


  //Contar  SAB, DOM, FERIADOS E COMPENSADOS
  total_sdrc_m := 0;
  if (edDia01.Text = 'S') or (edDia01.Text = 'D') or (edDia01.Text = 'R') or (edDia01.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia02.Text = 'S') or (edDia02.Text = 'D') or (edDia02.Text = 'R') or (edDia02.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia03.Text = 'S') or (edDia03.Text = 'D') or (edDia03.Text = 'R') or (edDia03.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia04.Text = 'S') or (edDia04.Text = 'D') or (edDia04.Text = 'R') or (edDia04.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia05.Text = 'S') or (edDia05.Text = 'D') or (edDia05.Text = 'R') or (edDia05.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia06.Text = 'S') or (edDia06.Text = 'D') or (edDia06.Text = 'R') or (edDia06.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia07.Text = 'S') or (edDia07.Text = 'D') or (edDia07.Text = 'R') or (edDia07.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia08.Text = 'S') or (edDia08.Text = 'D') or (edDia08.Text = 'R') or (edDia08.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia09.Text = 'S') or (edDia09.Text = 'D') or (edDia09.Text = 'R') or (edDia09.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia10.Text = 'S') or (edDia10.Text = 'D') or (edDia10.Text = 'R') or (edDia10.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia11.Text = 'S') or (edDia11.Text = 'D') or (edDia11.Text = 'R') or (edDia11.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia12.Text = 'S') or (edDia12.Text = 'D') or (edDia12.Text = 'R') or (edDia12.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia13.Text = 'S') or (edDia13.Text = 'D') or (edDia13.Text = 'R') or (edDia13.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia14.Text = 'S') or (edDia14.Text = 'D') or (edDia14.Text = 'R') or (edDia14.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia15.Text = 'S') or (edDia15.Text = 'D') or (edDia15.Text = 'R') or (edDia15.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia16.Text = 'S') or (edDia16.Text = 'D') or (edDia16.Text = 'R') or (edDia16.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia17.Text = 'S') or (edDia17.Text = 'D') or (edDia17.Text = 'R') or (edDia17.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia18.Text = 'S') or (edDia18.Text = 'D') or (edDia18.Text = 'R') or (edDia18.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia19.Text = 'S') or (edDia19.Text = 'D') or (edDia19.Text = 'R') or (edDia19.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia20.Text = 'S') or (edDia20.Text = 'D') or (edDia20.Text = 'R') or (edDia20.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia21.Text = 'S') or (edDia21.Text = 'D') or (edDia21.Text = 'R') or (edDia21.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia22.Text = 'S') or (edDia22.Text = 'D') or (edDia22.Text = 'R') or (edDia22.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia23.Text = 'S') or (edDia23.Text = 'D') or (edDia23.Text = 'R') or (edDia23.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia24.Text = 'S') or (edDia24.Text = 'D') or (edDia24.Text = 'R') or (edDia24.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia25.Text = 'S') or (edDia25.Text = 'D') or (edDia25.Text = 'R') or (edDia25.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia26.Text = 'S') or (edDia26.Text = 'D') or (edDia26.Text = 'R') or (edDia26.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia27.Text = 'S') or (edDia27.Text = 'D') or (edDia27.Text = 'R') or (edDia27.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia28.Text = 'S') or (edDia28.Text = 'D') or (edDia28.Text = 'R') or (edDia28.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia29.Text = 'S') or (edDia29.Text = 'D') or (edDia29.Text = 'R') or (edDia29.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia30.Text = 'S') or (edDia30.Text = 'D') or (edDia30.Text = 'R') or (edDia30.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;
  if (edDia31.Text = 'S') or (edDia31.Text = 'D') or (edDia31.Text = 'R') or (edDia31.Text = 'C') then total_sdrc_m := total_sdrc_m + 1 ;

  edFaltas.Text := IntToStr(total_faltas_m + total_sdrc_m);

end;

procedure TfrmControleFrequenciasMatricula.chkMarcarPresencaClick(
  Sender: TObject);
begin
  //Marcar Presen�a para o m�s...
  //Se os edits estiverem como "F", coloca "P" para todos e totaliza faltas para "0"

  if chkMarcarPresenca.Checked then
  begin
    //total_faltas_m := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').Value;
    if (edDia01.Text = 'F') then (edDia01.Text := 'P');
    if (edDia02.Text = 'F') then (edDia02.Text := 'P');
    if (edDia03.Text = 'F') then (edDia03.Text := 'P');
    if (edDia04.Text = 'F') then (edDia04.Text := 'P');
    if (edDia05.Text = 'F') then (edDia05.Text := 'P');
    if (edDia06.Text = 'F') then (edDia06.Text := 'P');
    if (edDia07.Text = 'F') then (edDia07.Text := 'P');
    if (edDia08.Text = 'F') then (edDia08.Text := 'P');
    if (edDia09.Text = 'F') then (edDia09.Text := 'P');
    if (edDia10.Text = 'F') then (edDia10.Text := 'P');
    if (edDia11.Text = 'F') then (edDia11.Text := 'P');
    if (edDia12.Text = 'F') then (edDia12.Text := 'P');
    if (edDia13.Text = 'F') then (edDia13.Text := 'P');
    if (edDia14.Text = 'F') then (edDia14.Text := 'P');
    if (edDia15.Text = 'F') then (edDia15.Text := 'P');
    if (edDia16.Text = 'F') then (edDia16.Text := 'P');
    if (edDia17.Text = 'F') then (edDia17.Text := 'P');
    if (edDia18.Text = 'F') then (edDia18.Text := 'P');
    if (edDia19.Text = 'F') then (edDia19.Text := 'P');
    if (edDia20.Text = 'F') then (edDia20.Text := 'P');
    if (edDia21.Text = 'F') then (edDia21.Text := 'P');
    if (edDia22.Text = 'F') then (edDia22.Text := 'P');
    if (edDia23.Text = 'F') then (edDia23.Text := 'P');
    if (edDia24.Text = 'F') then (edDia24.Text := 'P');
    if (edDia25.Text = 'F') then (edDia25.Text := 'P');
    if (edDia26.Text = 'F') then (edDia26.Text := 'P');
    if (edDia27.Text = 'F') then (edDia27.Text := 'P');
    if (edDia28.Text = 'F') then (edDia28.Text := 'P');
    if (edDia29.Text = 'F') then (edDia29.Text := 'P');
    if (edDia30.Text = 'F') then (edDia30.Text := 'P');
    if (edDia31.Text = 'F') then (edDia31.Text := 'P');
    edFaltas.Clear;
    edFaltas.Text := '0';
    total_faltas_m := 0;
    //btnCalcularFaltas.Enabled := False;
    //chkMarcarPresenca.Visible := False;
  end
  else
  begin
    //total_faltas_m := dmDeca.cdsSel_Cadastro_Frequencia.FieldByName ('num_faltas').Value;
    if (edDia01.Text = 'P') then (edDia01.Text := 'F');
    if (edDia02.Text = 'P') then (edDia02.Text := 'F');
    if (edDia03.Text = 'P') then (edDia03.Text := 'F');
    if (edDia04.Text = 'P') then (edDia04.Text := 'F');
    if (edDia05.Text = 'P') then (edDia05.Text := 'F');
    if (edDia06.Text = 'P') then (edDia06.Text := 'F');
    if (edDia07.Text = 'P') then (edDia07.Text := 'F');
    if (edDia08.Text = 'P') then (edDia08.Text := 'F');
    if (edDia09.Text = 'P') then (edDia09.Text := 'F');
    if (edDia10.Text = 'P') then (edDia10.Text := 'F');
    if (edDia11.Text = 'P') then (edDia11.Text := 'F');
    if (edDia12.Text = 'P') then (edDia12.Text := 'F');
    if (edDia13.Text = 'P') then (edDia13.Text := 'F');
    if (edDia14.Text = 'P') then (edDia14.Text := 'F');
    if (edDia15.Text = 'P') then (edDia15.Text := 'F');
    if (edDia16.Text = 'P') then (edDia16.Text := 'F');
    if (edDia17.Text = 'P') then (edDia17.Text := 'F');
    if (edDia18.Text = 'P') then (edDia18.Text := 'F');
    if (edDia19.Text = 'P') then (edDia19.Text := 'F');
    if (edDia20.Text = 'P') then (edDia20.Text := 'F');
    if (edDia21.Text = 'P') then (edDia21.Text := 'F');
    if (edDia22.Text = 'P') then (edDia22.Text := 'F');
    if (edDia23.Text = 'P') then (edDia23.Text := 'F');
    if (edDia24.Text = 'P') then (edDia24.Text := 'F');
    if (edDia25.Text = 'P') then (edDia25.Text := 'F');
    if (edDia26.Text = 'P') then (edDia26.Text := 'F');
    if (edDia27.Text = 'P') then (edDia27.Text := 'F');
    if (edDia28.Text = 'P') then (edDia28.Text := 'F');
    if (edDia29.Text = 'P') then (edDia29.Text := 'F');
    if (edDia30.Text = 'P') then (edDia30.Text := 'F');
    if (edDia31.Text = 'P') then (edDia31.Text := 'F');
    edFaltas.Clear;
    edFaltas.Text := '30';
    total_faltas_m := 30;
    btnCalcularFaltas.Enabled := True;
    //chkMarcarPresenca.Visible := False;
  end;
end;
end.
