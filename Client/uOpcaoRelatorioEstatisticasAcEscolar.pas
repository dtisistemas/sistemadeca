unit uOpcaoRelatorioEstatisticasAcEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Grids, DBGrids, ExtCtrls, Buttons;

type
  TfrmOpcaoRelatorioEstatisticasAcEscolar = class(TForm)
    GroupBox1: TGroupBox;
    cbOpcaoRelatorio: TComboBox;
    GroupBox2: TGroupBox;
    mskAno: TMaskEdit;
    cbMes: TComboBox;
    btnVisualizar: TSpeedButton;
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOpcaoRelatorioEstatisticasAcEscolar: TfrmOpcaoRelatorioEstatisticasAcEscolar;

implementation

uses uDM, uPrincipal, rLevantametnoAtendidosPorPeriodo;

{$R *.DFM}

procedure TfrmOpcaoRelatorioEstatisticasAcEscolar.btnVisualizarClick(
  Sender: TObject);
begin


  try

    case cbOpcaoRelatorio.ItemIndex of
      0: begin
        //Relat�rio de Levantamento de Atendidos por Per�odo
        //Etapa 1
        { Excluir todos os dados referentes ao usu�rio logado, m�s e ano para a gera��o dos dados novamente. }
        with dmDeca.cdsExc_TMPRel01 do
        begin
          Close;
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Params.ParamByName ('@pe_ano').Value         := Trim(mskAno.Text);
          Params.ParamByName ('@pe_mes').Value         := cbMes.ItemIndex + 1;
          Execute;
        end;

        //Etapa 2
        { Gerar os dados e inser�-los na tabela, com o login atual }
        //Leia cada unidade "ativa"
        with dmDeca.cdsSel_Unidade do
        begin
          Close;
          Params.ParamByName ('@pe_cod_unidade').Value := Null;
          Params.ParamByName ('@pe_nom_unidade').Value := Null;
          Open;

          while not eof do
          begin
            //Se unidade ATIVA
            if (dmDeca.CdsSel_Unidade.FieldByName ('flg_status').Value = 0) or (dmDeca.CdsSel_Unidade.FieldByName ('ind_tipo_unidade').Value < 3) then
            begin
              //Seleciona os dados para a unidade e grava na tabela TMP_Rel01 - tabela vazia para o per�odo e usu�rio logado
              with dmDeca.cdsSel_Levantamento_Num_atendidos_Por_Periodo do
              begin
                Close;
                Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.CdsSel_Unidade.FieldByName ('cod_unidade').Value;
                Params.ParamByName ('@pe_num_ano').Value     := mskAno.Text;
                Params.ParamByName ('@pe_num_mes').Value     := cbMes.ItemIndex + 1;
                Open;

                with dmDeca.cdsIns_TMPRel01 do
                begin
                  Close;
                  Params.ParamByName ('@pe_unidade').Value       := dmDeca.CdsSel_Unidade.FieldByName ('nom_unidade').Value;
                  Params.ParamByName ('@pe_divisao').Value       := dmDeca.CdsSel_Unidade.FieldByName ('vDivisao').Value;
                  Params.ParamByName ('@pe_efetivo').Value       := dmDeca.cdsSel_Levantamento_Num_atendidos_Por_Periodo.FieldByName ('Total_Efetivo').Value;
                  Params.ParamByName ('@pe_manha').Value         := dmDeca.cdsSel_Levantamento_Num_atendidos_Por_Periodo.FieldByName ('Total_Manha').Value;
                  Params.ParamByName ('@pe_tarde').Value         := dmDeca.cdsSel_Levantamento_Num_atendidos_Por_Periodo.FieldByName ('Total_Tarde').Value;
                  Params.ParamByName ('@pe_integral').Value      := dmDeca.cdsSel_Levantamento_Num_atendidos_Por_Periodo.FieldByName ('Total_Integral').Value;
                  Params.ParamByName ('@pe_nda_afastados').Value := dmDeca.cdsSel_Levantamento_Num_atendidos_Por_Periodo.FieldByName ('Total_ND_Afastados').Value;
                  Params.ParamByName ('@pe_mes').Value           := cbMes.ItemIndex + 1;
                  Params.ParamByName ('@pe_ano').Value           := mskAno.Text;
                  Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
                  Execute;
                end;
              end;
            end;

            dmDeca.cdsSel_Unidade.Next;

          end;
        end;
        //Etapa 3
        { Selecionar os dados e exibir o relat�rio }
        try
          with dmDeca.cdsSel_TMPRel01 do
          begin
            Close;

            Open;

            Application.CreateForm (TrelLevantamentoAtendidosPorPeriodo, relLevantamentoAtendidosPorPeriodo);
            relLevantamentoAtendidosPorPeriodo.qrlTituloReferencia.Caption := 'REFER�NCIA: ' + cbMes.Text + ' / ' + mskAno.Text;
            relLevantamentoAtendidosPorPeriodo.Preview;
            relLevantamentoAtendidosPorPeriodo.Free;
          end;
        except
          Application.MessageBox ('Aten��o!!!' +#13+#10+
                            'Um erro ocorreu ao tentar gerar o relt�rio.' +#13+#10+
                            'Tente gerar novamente e em caso de persist�ncia do erro, entre em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro',
                            MB_OK + MB_ICONERROR);
        end;

        end;

    end;






  except
    Application.MessageBox ('Aten��o !!! ' +#13+#10+
                            'Um erro ocorreu e a opera��o deve ser refeita.' +#13+#10+
                            'Favor verificar conex�o com a internet/rede.' +#13+#10+
                            'No caso de persist�ncia do erro, entre em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro',
                            MB_OK + MB_ICONERROR);
  end;

end;

end.
