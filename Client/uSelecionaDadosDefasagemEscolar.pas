unit uSelecionaDadosDefasagemEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmSelecionaDadosDefasagemEscolar = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    cbUnidade: TComboBox;
    btnPesquisar: TSpeedButton;
    btnSair: TSpeedButton;
    DBGrid1: TDBGrid;
    lbtitulo: TLabel;
    btnImprimir: TSpeedButton;
    dsAcompanhamentoEscolar: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecionaDadosDefasagemEscolar: TfrmSelecionaDadosDefasagemEscolar;
  vListaUnidade1, vListaUnidade2 : TStringList;
  mmCOD_UNIDADE : Integer;

implementation

uses uPrincipal, uDM, rAlunosComDefasagem;

{$R *.DFM}

procedure TfrmSelecionaDadosDefasagemEscolar.FormShow(Sender: TObject);
begin

  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carrega as unidades em cbUnidade
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not dmDeca.cdsSel_Unidade.eof do
      begin
        vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
        vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        cbUnidade.Items.Add(dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

procedure TfrmSelecionaDadosDefasagemEscolar.cbUnidadeClick(
  Sender: TObject);
begin

  try

  if cbUnidade.ItemIndex > 0 then
  begin
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := cbUnidade.Text;
      Open;

      mmCOD_UNIDADE := dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value;
      lbtitulo.Caption := dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value;
      btnPesquisar.Enabled := True;

    end;
  end
  else if cbUnidade.ItemIndex = -1 then
  begin
    btnPesquisar.Enabled := False;
    Application.MessageBox ('Aten��o!!! Selecione uma das op��es da lista antes de pesquisar...',
                            '[Sistema Deca] - Sele��o de Unidade',
                            MB_OK);
    cbUnidade.SetFocus;
  end
  else
  begin
    lbtitulo.Caption := '<TODAS AS UNIDADES>';
        btnPesquisar.Enabled := True;
  end;
  except end;
end;

procedure TfrmSelecionaDadosDefasagemEscolar.btnPesquisarClick(
  Sender: TObject);
begin

  try
    with dmDeca.cdsSel_AcompEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acompescolar').Value := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Params.ParamByName ('@pe_num_ano_letivo').AsInteger := 2009;
      Params.ParamByName ('@pe_num_bimestre').AsInteger := 1;

      //if cbUnidade.ItemIndex = -1 then
      //  Params.ParamByName ('@pe_cod_unidade').Value := Null
      //else

      Params.ParamByName ('@pe_cod_unidade').AsInteger := mmCOD_UNIDADE;

      Params.ParamByName ('@pe_flg_defasagem_escolar').AsInteger := 1; //Apresenta Defasagem escolar
      Open;
      DBGrid1.Refresh;
    end;

  except end;

end;

procedure TfrmSelecionaDadosDefasagemEscolar.btnImprimirClick(
  Sender: TObject);
begin
  Application.CreateForm(TrelAlunosComDefasagem, relAlunosComDefasagem);
  relAlunosComDefasagem.Preview;
  relAlunosComDefasagem.Free;
end;

procedure TfrmSelecionaDadosDefasagemEscolar.btnSairClick(Sender: TObject);
begin
  frmSelecionaDadosDefasagemEscolar.Close;
end;

end.
