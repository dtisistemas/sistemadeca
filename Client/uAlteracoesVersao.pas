unit uAlteracoesVersao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg, StdCtrls, ComObj, Urlmon;

type
  TfrmAlteracoesVersao = class(TForm)
    Panel1: TPanel;
    Image3: TImage;
    Image1: TImage;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Shape1: TShape;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Label5: TLabel;
    Label6: TLabel;
    Bevel1: TBevel;
    Image2: TImage;
    Label7: TLabel;
    Bevel4: TBevel;
    stOSVersao: TStaticText;
    stOSBuilder: TStaticText;
    stOS: TStaticText;
    stOSService: TStaticText;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure Label7Click(Sender: TObject);
    function DownloadFile(URL, Pasta: String):Boolean;
    procedure FormCreate(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteracoesVersao: TfrmAlteracoesVersao;

implementation

uses uNavegador;

{$R *.DFM}

function TfrmAlteracoesVersao.DownloadFile(URL, Pasta: String): Boolean;
begin
  try
    Result := URLDownloadToFile(nil, PChar(URL),PChar(Pasta), 0, nil) = 0;
  except
    Result := False;
  end;
end;

procedure TfrmAlteracoesVersao.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Resize := False;
end;

procedure TfrmAlteracoesVersao.Label7Click(Sender: TObject);
var ie : Variant;
begin
 IE := CreateOleObject('InternetExplorer.Application');
 IE.Visible := True;
 IE.Navigate('http://informatica.fundhas.org.br');
end;

procedure TfrmAlteracoesVersao.FormCreate(Sender: TObject);
var
  verInfo : TOSVersionInfo;
  str     : String;
  i       : Word;
begin
  verInfo.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
  if GetVersionEx(verInfo) then
  begin
    stOSVersao.Caption  := 'Vers�o: ' + IntToStr(verInfo.dwMajorVersion) + IntToStr(verInfo.dwMinorVersion);
    stOSBuilder.Caption := 'Compila��o: ' + IntToStr(verInfo.dwBuildNumber);
    str := 'Sistema Operacional : ';
    case verInfo.dwPlatformId of
      VER_PLATFORM_WIN32s        : stOS.Caption := str + 'Windows 95';
      VER_PLATFORM_WIN32_WINDOWS : stOS.Caption := str + 'Windows 95 Osr2 / 98';
      VER_PLATFORM_WIN32_NT      : stOS.Caption := str + 'Windows NT';
    end;

    str := '';
    for i:=0 to 127 do
      str := str + verInfo.szCSDVersion[i];
      stOSService.Caption := str;
    end;
end;

procedure TfrmAlteracoesVersao.Image2Click(Sender: TObject);
begin
  Application.CreateForm (TfrmNavegador, frmNavegador);
  frmNavegador.URLs.Text := 'http://informatica.fundhas.org.br';
  frmNavegador.Show;
end;

end.
