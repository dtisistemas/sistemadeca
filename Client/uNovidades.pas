unit uNovidades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls;

type
  TfrmNovidadesVersao = class(TForm)
    reNovidades: TRichEdit;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovidadesVersao: TfrmNovidadesVersao;

implementation

{$R *.DFM}

procedure TfrmNovidadesVersao.FormShow(Sender: TObject);
begin
  reNovidades.Lines.Clear;
  reNovidades.Lines.LoadFromFile ('c:\Deca\Novidades.txt');
end;

procedure TfrmNovidadesVersao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    frmNovidadesVersao.Close;
  end;
end;

end.
