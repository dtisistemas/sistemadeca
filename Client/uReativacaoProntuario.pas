unit uReativacaoProntuario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls;

type
  TfrmReativacaoProntuario = class(TForm)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    GroupBox3: TGroupBox;
    meJustificativa: TMemo;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    Label2: TLabel;
    rgOpcaoReativacao: TRadioGroup;
    GroupBox1: TGroupBox;
    cbUnidade: TComboBox;
    cbSecao: TComboBox;
    GroupBox4: TGroupBox;
    mskDataReativacao: TMaskEdit;
    procedure btnSairClick(Sender: TObject);
    procedure ModoTela(Modo:String);
    procedure btnNovoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure rgOpcaoReativacaoClick(Sender: TObject);
    procedure cbUnidadeClick(Sender: TObject);
    procedure cbSecaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReativacaoProntuario: TfrmReativacaoProntuario;
  vListaUnidade1, vListaUnidade2 : TStringList;
  mmUNIDADE : Integer;
  mmSECAO   : String;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmReativacaoProntuario.btnSairClick(Sender: TObject);
begin
  frmReativacaoProntuario.Close;
end;

procedure TfrmReativacaoProntuario.ModoTela(Modo: String);
begin
  if Modo = 'P' then
  begin

    txtMatricula.Clear;
    txtMatricula.Enabled        := False;
    btnPesquisar.Enabled        := False;
    meJustificativa.Clear;
    meJustificativa.Enabled     := False;
    lbNome.Caption              := 'NOME DA CRIAN�A / ADOLESCENTE';
    rgOpcaoReativacao.ItemIndex := -1;
    cbUnidade.ItemIndex         := -1;
    cbSecao.ItemIndex           := -1;
    meJustificativa.Clear;
    mskDataReativacao.Clear;
    GroupBox4.Enabled           := False;

    btnNovo.Enabled           := True;
    btnSalvar.Enabled         := False;
    btnCancelar.Enabled       := False;
    btnImprimir.Enabled       := False;
    btnSair.Enabled           := True;
    rgOpcaoReativacao.Enabled := False;
    GroupBox1.Enabled         := False;
    meJustificativa.Enabled   := False;

  end
  else if Modo = 'N' then
  begin

    txtMatricula.Enabled      := True;
    meJustificativa.Enabled   := True;
    btnPesquisar.Enabled      := True;
    btnNovo.Enabled           := False;
    btnSalvar.Enabled         := True;
    btnCancelar.Enabled       := True;
    btnImprimir.Enabled       := False;
    btnSair.Enabled           := False;
    rgOpcaoReativacao.Enabled := True;
    GroupBox1.Enabled         := True;
    meJustificativa.Enabled   := True;
    GroupBox4.Enabled         := True;
  end;
end;

procedure TfrmReativacaoProntuario.btnNovoClick(Sender: TObject);
begin
  ModoTela('N');
  txtMatricula.SetFocus;
end;

procedure TfrmReativacaoProntuario.FormShow(Sender: TObject);
begin
  vListaUnidade1 := TStringList.Create();
  vListaUnidade2 := TStringList.Create();

  //Carrega a lista de unidades necess�rias...
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName('@pe_cod_unidade').Value := Null;
      Params.ParamByName('@pe_nom_unidade').Value := Null;
      Open;

      cbUnidade.Clear;
      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        if (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) = '5107') or (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) = '5109') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) = '5112') or (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) = '5114') or
           (Trim(dmDeca.cdsSel_Unidade.FieldByName('num_ccusto').Value) = '5117') then
        begin
          cbUnidade.Items.Add(FieldByName('nom_unidade').Value);
          vListaUnidade1.Add(IntToStr(FieldByName('cod_unidade').Value));
          vListaUnidade2.Add(FieldByName('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    vListaUnidade2.Sort;
    end;
  except end;
  ModoTela('P');

end;

procedure TfrmReativacaoProntuario.btnCancelarClick(Sender: TObject);
begin
  ModoTela('P');
end;

procedure TfrmReativacaoProntuario.btnSalvarClick(Sender: TObject);
begin

  try
    with dmDeca.cdsUpd_ReativarProntuario do
    begin
      Close;
      Params.ParamByName ('@pe_cod_matricula').Value := GetValue(txtMatricula.Text);

      //Verificar qual a op��o selecionada e grava de acordo com a op��o de unidade
      case rgOpcaoReativacao.ItemIndex of
        0: begin
             Params.ParamByName ('@pe_cod_unidade').Value := 44;
             Params.ParamByName ('@pe_num_secao').Value   := '0310';
             mmSECAO   := '0310';
        end;

        1: begin
             Params.ParamByName ('@pe_cod_unidade').Value := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
             Params.ParamByName ('@pe_num_secao').Value   := Copy(cbSecao.Text,1,4);
        end;

        2: begin
             Params.ParamByName ('@pe_cod_unidade').Value := 40;
             Params.ParamByName ('@pe_num_secao').Value   := '0000';
        end;


      end;

      //Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      //Params.ParamByName ('@pe_num_secao').Value     := Copy(cbSecao.Text,1,4);
      Params.ParamByName ('@pe_ind_status').Value    := 1; //Deixar como Ativo
      Execute;

      //Incluir um registro de atendimento com os dados da reativa��o do prontu�rio
      with dmDeca.cdsInc_Cadastro_Historico do
      begin
        Close;
        Params.ParamByName('@pe_cod_matricula').AsString       := Trim(txtMatricula.Text);
        if rgOpcaoReativacao.ItemIndex = 0 then
          Params.ParamByName('@pe_cod_unidade').AsInteger      := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex])
        else
          Params.ParamByName('@pe_cod_unidade').AsInteger      := mmUNIDADE;

        Params.ParamByName('@pe_dat_historico').Value          := StrToDate(mskDataReativacao.Text);//Date();
        Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 25;
        Params.ParamByName('@pe_dsc_tipo_historico').AsString  := 'Reativa��o de Prontu�rio';
        Params.ParamByName('@pe_ind_tipo').Value               := Null;
        Params.ParamByName('@pe_ind_classificacao').Value      := Null;
        Params.ParamByName('@pe_qtd_participantes').Value      := Null;
        Params.ParamByName('@pe_num_horas').Value              := Null;
        Params.ParamByName('@pe_dsc_ocorrencia').Value         := meJustificativa.Text;
        Params.ParamByName('@pe_log_cod_usuario').AsInteger    := vvCOD_USUARIO;
        Execute;
      end;

      ModoTela('P');

    end;
  except end;


end;

procedure TfrmReativacaoProntuario.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value        := 0; //vvCOD_UNIDADE;
        Open;

        if (dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_unidade').Value = 47) or (dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_unidade').Value = 48) then
          lbNome.Caption := FieldByName('nom_nome').AsString
        else
        begin
          Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                  'O aluno n�o encontra-se em INATIVO ou MAIORIDADE para reativa��o do prontu�rio. Favor refa�a a pesquisa...',
                                  '[Sistema Deca] - Erro de sele��o do prontu�rio',
                                  MB_OK + MB_ICONWARNING);
          vvCOD_MATRICULA_PESQUISA := 'x';
          lbNome.Caption := 'NOME DA CRIAN�A / ADOLESCENTE';
          txtMatricula.Clear;
        end;
      end;
    except end;
  end;
  mskDataReativacao.Text := DateToStr(Now);

end;

procedure TfrmReativacaoProntuario.rgOpcaoReativacaoClick(Sender: TObject);
begin


  case rgOpcaoReativacao.ItemIndex of
    0: begin
         //Caso seja selecionada a op��o APRENDIZES EM TRANSFER�NCIA, n�o carregar a lista de unidades
         //pois a unidade de destino � apenas a 5102.0310 - PROGRAMA APRENDIZ/APRENDIZES EM TRANSFER�NCIA
         mmUNIDADE := 44;
         mmSECAO   := '0310';
         GroupBox1.Enabled := False;
         meJustificativa.Text := 'Reativa��o de prontu�rio eletr�nico de ' + lbNome.Caption + ' da Unidade Maioridade/Inativos para a Unidade Projeto Aprendiz/Outras Empresas-Aprendizes em Transfer�ncia';
    end;

    1: begin
        //Caso seja relacionada a op��o BOLSISTAS, carrega a lista de unidades e suas respectivas se��es - apenas 5 unidades...
        //5107/5109/5112/5114/5117
        cbUnidade.ItemIndex  := -1;
        cbSecao.ItemIndex    := -1;
        GroupBox1.Enabled    := True;
        meJustificativa.Text := 'Prontu�rio reativado devido MUDAN�A DE V�NCULO do(a) adolescente ' + lbNome.Caption + ', de aprendiz para bolsista.';
    end;

    //Reativado para a Triagem
    2: begin
         mmUNIDADE := 40;
         mmSECAO   := '0000';
         GroupBox1.Enabled := False;
         meJustificativa.Text := 'Prontu�rio reativado para readmiss�o em Unidade Triagem, do(a) adolescente ' + lbNome.Caption + '.';
    end;


  end;
  
end;

procedure TfrmReativacaoProntuario.cbUnidadeClick(Sender: TObject);
begin
  //Recupera o c�digo da unidade e carrega a lista de se��es da mesma
  try
    with dmDeca.cdsSel_Secao do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_secao').Value := Null;
      Params.ParamByName ('@pe_cod_unidade').Value  := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);
      Params.ParamByName ('@pe_num_secao').Value    := Null;
      Params.ParamByName ('@pe_flg_status').Value   := 0;
      Open;

      cbSecao.Clear;
      while not (dmDeca.cdsSel_Secao.eof) do
      begin
        cbSecao.Items.Add(dmDeca.cdsSel_Secao.FieldByName('num_secao').Value + '.' + dmDeca.cdsSel_Secao.FieldByName('dsc_nom_secao').Value);
        dmDeca.cdsSel_Secao.Next;
      end;

      mmUNIDADE := StrToInt(vListaUnidade1.Strings[cbUnidade.ItemIndex]);

    end;
  except end;
end;

procedure TfrmReativacaoProntuario.cbSecaoClick(Sender: TObject);
begin
  mmSECAO := Copy(cbSecao.Text,1,4);
end;

end.
