unit uEmiteContratoAdmissao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Mask, Buttons, Comobj;

type
  TfrmEmiteContratoAdmissao = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel17: TPanel;
    Label39: TLabel;
    GroupBox2: TGroupBox;
    mskAdmissao: TMaskEdit;
    GroupBox3: TGroupBox;
    mskTerminoContrato: TMaskEdit;
    GroupBox28: TGroupBox;
    mskExameMedico: TMaskEdit;
    rgPrimeiroEmprego: TRadioGroup;
    GroupBox27: TGroupBox;
    edNumCCorrente: TEdit;
    mskNumAgencia: TMaskEdit;
    edBanco: TEdit;
    rgContribuiSindicato: TRadioGroup;
    edValorContribuicaoSindicato: TEdit;
    GroupBox30: TGroupBox;
    edValorSalarioBase: TEdit;
    GroupBox31: TGroupBox;
    edHorarioTrabalho: TEdit;
    GroupBox49: TGroupBox;
    GroupBox29: TGroupBox;
    rgNecessitaVT: TRadioGroup;
    GroupBox1: TGroupBox;
    GroupBox4: TGroupBox;
    rgNecessitaPE: TRadioGroup;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    edItinerarioVT: TEdit;
    edItinerarioPE: TEdit;
    edQtdVTDia: TEdit;
    edQtdPeDia: TEdit;
    btnEmitirFicha_e_Contrato: TSpeedButton;
    cbTipContrato: TComboBox;
    Label1: TLabel;
    cbCurso: TComboBox;
    cbCargoFuncao: TComboBox;
    cbUnidadeDestino: TComboBox;
    Label2: TLabel;
    procedure btnEmitirFicha_e_ContratoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteContratoAdmissao: TfrmEmiteContratoAdmissao;
  vListaCursoAprendiz1, vListaCursoAprendiz2 : TStringList;

implementation

uses rFichaAdmissaoCadastroAprendiz, uDM, uUtil, uFichaCrianca, uPrincipal;

{$R *.DFM}

procedure TfrmEmiteContratoAdmissao.btnEmitirFicha_e_ContratoClick(
  Sender: TObject);
var
  winword, doc, docs, docs2, doc2, docs3, doc3 : Olevariant;
  arquivo2, termocompromisso, ficha, vvNOME_RESP, vvNOME_PAI, vvNOME_MAE : String;
begin
  //Tipo de contrato a ser emitido
  if cbTipContrato.ItemIndex = 0 then
  begin
    termocompromisso := (ExtractFilePath(Application.ExeName)+ '\Aditamentos\DRH-18 TERMO DE COMPROMISSO.DOC');
    winword          := CreateOleObject('Word.Application');
    winword.Visible := True;
    docs2 := Winword.Documents;
    doc2 := Docs2.Open (termocompromisso);

    //Repassar valores aos campos do documento TERMO DE COMPROMISSO
    doc2.Content.Find.Execute( FindText := '%BOLSISTA%', replacewith            := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').AsString );
    doc2.Content.Find.Execute( FindText := '%BOLSISTA2%', replacewith           := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').AsString );
    doc2.Content.Find.Execute( FindText := '%NUMERO_DOCUMENTO1%', replacewith   := frmFichaCrianca.edNumRG.Text );  //dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_rg').AsString );
    doc2.Content.Find.Execute( FindText := '%NUMERO_DOCUMENTO2%', replacewith   := frmFichaCrianca.mskNumCPF.Text );//dmDeca.cdsSel_Cadastro_Move.FieldByName ('num_cpf').AsString );
    doc2.Content.Find.Execute( FindText := '%NASCIMENTO%', replacewith          := dmDeca.cdsSel_Cadastro_Move.FieldByName ('Nascimento').AsString );

    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').AsString;
        Params.ParamByName ('@pe_cod_familiar').Value  := Null;
        Open;

        dmDeca.cdsSel_Cadastro_Familia.First;
        while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
        begin
          //Verifica Respons�vel
          if (dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_responsavel').Value = True) then
            vvNOME_RESP := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
          dmDeca.cdsSel_Cadastro_Familia.Next;
        end;
      end;
    except end;

    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := dmDeca.cdsSel_Cadastro_Move.FieldByName ('cod_matricula').Value;
        Params.ParamByName ('@pe_cod_familiar').Value  := Null;
        Open;

        dmDeca.cdsSel_Cadastro_Familia.First;
        while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
        begin
          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'PAI' then vvNOME_PAI := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'M�E' then vvNOME_MAE := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
          dmDeca.cdsSel_Cadastro_Familia.Next;
        end;
      end;
    except end;

    doc2.Content.Find.Execute( FindText := '%NOME_PAI%', replacewith            := vvNOME_PAI );
    doc2.Content.Find.Execute( FindText := '%NOME_MAE%', replacewith            := vvNOME_MAE );
    doc2.Content.Find.Execute( FindText := '%NOME_RESPONSAVEL%', replacewith    := vvNOME_RESP );
    doc2.Content.Find.Execute( FindText := '%DATA_ADMISSAO%', replacewith       := dmDeca.cdsSel_Cadastro_Move.FieldByName ('dat_admissao').AsString );


  end
  else if cbTipContrato.ItemIndex = 1 then
  begin
    arquivo2        := (ExtractFilePath(Application.ExeName) + '\Aditamentos\DRH-43 CONTRATO DE APRENDIZAGEM.doc' );
    winword         := CreateOleObject('Word.Application');
    winword.Visible := True;
    docs2           := Winword.Documents;
    doc2            := Docs2.Open (arquivo2);

    doc2.Content.Find.Execute( FindText := '%ADOLESCENTE%', replacewith         := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').AsString );
    doc2.Content.Find.Execute( FindText := '%ADOLESCENTE2%', replacewith        := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_nome').AsString );
    doc2.Content.Find.Execute( FindText := '%ENDERECO%', replacewith            := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_endereco').AsString + ' - ' + dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_complemento').AsString );
    doc2.Content.Find.Execute( FindText := '%BAIRRO%', replacewith              := dmDeca.cdsSel_Cadastro_Move.FieldByName ('nom_bairro').AsString );

    //Recuperar os dados do curso...
    with dmDeca.cdsSel_CursosAprendizagem do
      begin
        Close;
        Params.ParamByName ('@pe_id').Value     := Null;
        Params.ParamByName ('@pe_funcao').Value := Trim(cbCargoFuncao.Text);
        Params.ParamByName ('@pe_cbo').Value    := Null;
        Open;
      end;

    doc2.Content.Find.Execute( FindText := '%CARGO_FUNCAO_CBO%', replacewith       := dmDeca.cdsSel_CursosAprendizagem.FieldByName ('cbo').AsString );
    doc2.Content.Find.Execute( FindText := '%CTPS_NUMERO%' , replacewith           := Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_num').AsString) );
    doc2.Content.Find.Execute( FindText := '%CTPS_SERIE%' , replacewith            := Trim(dmDeca.cdsSel_Cadastro_Move.FieldByName ('ctps_serie').AsString) );

    //Validar a data de admiss�o
    if (mskAdmissao.Text = '') then doc2.Content.Find.Execute( FindText := '%DATA_ADMISSAO%' , replacewith := '  /  /    ' )
    else doc2.Content.Find.Execute( FindText := '%DATA_ADMISSAO%' , replacewith := mskAdmissao.Text );

    //Validar data t�rmino de contrato
    if (mskTerminoContrato.Text = '') then doc2.Content.Find.Execute( FindText := '%TERMINO_CONTRATO%' , replacewith := '  /  /    ' )
    else doc2.Content.Find.Execute( FindText := '%TERMINO_CONTRATO%' , replacewith := mskTerminoContrato.Text );


    doc2.Content.Find.Execute( FindText := '%TERMINO_CONTRATO%' , replacewith      := mskTerminoContrato.Text );
    doc2.Content.Find.Execute( FindText := '%HORAS_JORNADA_PRATICA%' , replacewith := dmDeca.cdsSel_CursosAprendizagem.FieldByName ('DIARIA_PRATICA').Value );
    doc2.Content.Find.Execute( FindText := '%HORAS_JORNADA_TEORICA%' , replacewith := dmDeca.cdsSel_CursosAprendizagem.FieldByName ('DIARIA_TEORICA').Value );
    doc2.Content.Find.Execute( FindText := '%TOTAL_HORAS_PRATICA%' , replacewith   := dmDeca.cdsSel_CursosAprendizagem.FieldByName ('PRATICA_TOTAL').Value );
    doc2.Content.Find.Execute( FindText := '%TOTAL_HORAS_TEORICA%' , replacewith   := dmDeca.cdsSel_CursosAprendizagem.FieldByName ('TEORICA_TOTAL').Value );
    doc2.Content.Find.Execute( FindText := '%TOTAL_HORAS%' , replacewith           := dmDeca.cdsSel_CursosAprendizagem.FieldByName ('TOTAL_PROGRAMA').Value );
    doc2.Content.Find.Execute( FindText := '%CURSO%' , replacewith                 := cbCurso.Text );
    doc2.Content.Find.Execute( FindText := '%GRAU_INSTRUCAO%' , replacewith        := frmFichaCrianca.cbGrauInstrucao.Text );
  end;

  //Montar o documento Ficha de Cadastro, repasando os campos �s vari�veis dos mesmos
  //...
    ficha := (ExtractFilePath(Application.ExeName)+ '\Aditamentos\FICHA.DOC');
    winword          := CreateOleObject('Word.Application');
    winword.Visible  := True;
    docs3            := Winword.Documents;
    doc3             := Docs3.Open (ficha);

    doc3.Content.Find.Execute( FindText := '%tipo%', replacewith                   := cbTipContrato.Text );
    doc3.Content.Find.Execute( FindText := '%matricula%' , replacewith             := frmFichaCrianca.txtMatricula.Text );
    doc3.Content.Find.Execute( FindText := '%nome%' , replacewith                  := frmFichaCrianca.txtNome.Text );
    doc3.Content.Find.Execute( FindText := '%ccusto_atual%' , replacewith          := vvNUM_CCUSTO );
    doc3.Content.Find.Execute( FindText := '%local_atual%' , replacewith           := vvNOM_UNIDADE );
    doc3.Content.Find.Execute( FindText := '%ccusto_destino%' , replacewith        := Copy(cbUnidadeDestino.Text,1,4) );
    doc3.Content.Find.Execute( FindText := '%ccusto_destino%' , replacewith        := Trim(Copy(cbUnidadeDestino.Text,5,100)) );
    doc3.Content.Find.Execute( FindText := '%endereco%' , replacewith              := frmFichaCrianca.txtEndereco.Text );
    doc3.Content.Find.Execute( FindText := '%complemento%' , replacewith           := frmFichaCrianca.txtComplemento.Text );
    doc3.Content.Find.Execute( FindText := '%cep%' , replacewith                   := frmFichaCrianca.txtCep.Text );
    doc3.Content.Find.Execute( FindText := '%bairro%' , replacewith                := frmFichaCrianca.txtBairro.Text );
    doc3.Content.Find.Execute( FindText := '%fone_residencia%' , replacewith       := frmFichaCrianca.txtTelefone.Text );
    doc3.Content.Find.Execute( FindText := '%celular%' , replacewith               := frmFichaCrianca.mskTelefoneCelular.Text );
    doc3.Content.Find.Execute( FindText := '%municipio_residencia%' , replacewith  := frmFichaCrianca.edMunicipioResidencia.Text + '/' + frmFichaCrianca.cbUFResidencia.Text );
    doc3.Content.Find.Execute( FindText := '%email%' , replacewith                 := frmFichaCrianca.edEmail.Text );

    if (frmFichaCrianca.edNecessidadesEspeciais.Text = '') then doc3.Content.Find.Execute( FindText := '%necessidades_especiais%' , replacewith := frmFichaCrianca.edNecessidadesEspeciais.Text )
    else doc3.Content.Find.Execute( FindText := '%necessidades_especiais%' , replacewith := '' );

    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := frmFichaCrianca.txtMatricula.Text;
        Params.ParamByName ('@pe_cod_familiar').Value  := Null;
        Open;

        while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
        begin
          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'PAI' then vvNOME_PAI := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_vinculo').Value = 'M�E' then vvNOME_MAE := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
          dmDeca.cdsSel_Cadastro_Familia.Next;
        end;
      end;
    except end;

    try
      with dmDeca.cdsSel_Cadastro_Familia do
      begin
        Close;
        Params.ParamByName ('@pe_cod_matricula').Value := frmFichaCrianca.txtMatricula.Text;
        Params.ParamByName ('@pe_cod_familiar').Value  := Null;
        Open;

        while not (dmDeca.cdsSel_Cadastro_Familia.eof) do
        begin
          //Verifica Respons�vel
          if dmDeca.cdsSel_Cadastro_Familia.FieldByName ('ind_responsavel').Value = 1 then vvNOME_RESP := dmDeca.cdsSel_Cadastro_Familia.FieldByName ('nom_familiar').Value;
          dmDeca.cdsSel_Cadastro_Familia.Next;
        end;
      end;
    except end;

    doc3.Content.Find.Execute( FindText := '%pai%', replacewith                    := vvNOME_PAI );
    doc3.Content.Find.Execute( FindText := '%mae%', replacewith                    := vvNOME_MAE );
    doc3.Content.Find.Execute( FindText := '%responsavel%', replacewith            := vvNOME_RESP );

    doc3.Content.Find.Execute( FindText := '%conjuge%' , replacewith               := frmFichaCrianca.edNomeConjuge.Text );
    doc3.Content.Find.Execute( FindText := '%nascimento%' , replacewith            := frmFichaCrianca.mskNascimento.Text );
    doc3.Content.Find.Execute( FindText := '%local_nascimento%' , replacewith      := frmFichaCrianca.edLocalNascimento.Text );
    doc3.Content.Find.Execute( FindText := '%grau_instrucao%' , replacewith        := frmFichaCrianca.cbGrauInstrucao.Text );
    doc3.Content.Find.Execute( FindText := '%estado_civil%' , replacewith          := frmFichaCrianca.cbEstadoCivil.Text );
    doc3.Content.Find.Execute( FindText := '%nacionalidade%' , replacewith         := frmFichaCrianca.edNacionalidade.Text );
    //Tratar op��o de Fumante
    doc3.Content.Find.Execute( FindText := '%raca%' , replacewith                  := frmFichaCrianca.txtEtnia.Text );
    doc3.Content.Find.Execute( FindText := '%olhos%' , replacewith                 := frmFichaCrianca.txtCorOlhos.Text );
    doc3.Content.Find.Execute( FindText := '%cabelos%' , replacewith               := frmFichaCrianca.txtCorCabelos.Text );
    doc3.Content.Find.Execute( FindText := '%sangue%' , replacewith                := frmFichaCrianca.cbTipoSangue.Text );
    doc3.Content.Find.Execute( FindText := '%peso%' , replacewith                  := frmFichaCrianca.txtPeso.Text );
    doc3.Content.Find.Execute( FindText := '%altura%' , replacewith                := frmFichaCrianca.txtAltura.Text );
    //Tratar portador de necessidades especiais e quais necessidades

    doc3.Content.Find.Execute( FindText := '%rg%' , replacewith                    := frmFichaCrianca.edNumRg.Text );
    doc3.Content.Find.Execute( FindText := '%rg_emissao%' , replacewith            := frmFichaCrianca.mskRGDataEmissao.Text );
    doc3.Content.Find.Execute( FindText := '%rg_expedidor%' , replacewith          := frmFichaCrianca.cbOrgaoEmissorRG.Text );
    doc3.Content.Find.Execute( FindText := '%rg_uf%' , replacewith                 := frmFichaCrianca.cbRGUf.Text );

    doc3.Content.Find.Execute( FindText := '%cpf%' , replacewith                   := frmFichaCrianca.mskNumCPF.Text );
    doc3.Content.Find.Execute( FindText := '%certidao%' , replacewith              := frmFichaCrianca.edDadosCertidaoNascimento.Text );

    doc3.Content.Find.Execute( FindText := '%titulo%' , replacewith                := frmFichaCrianca.edNumTituloEleitor.Text );
    doc3.Content.Find.Execute( FindText := '%zona%' , replacewith                  := frmFichaCrianca.edZonaEleitoral.Text );
    doc3.Content.Find.Execute( FindText := '%secao%' , replacewith                 := frmFichaCrianca.edSecaoEleitoral.Text );

    doc3.Content.Find.Execute( FindText := '%ctps%' , replacewith                  := frmFichaCrianca.edNumCTPS.Text );
    doc3.Content.Find.Execute( FindText := '%ctps_serie%' , replacewith            := frmFichaCrianca.edCTPSSerie.Text );
    doc3.Content.Find.Execute( FindText := '%ctps_emissao%' , replacewith          := frmFichaCrianca.mskCTPSDataEmissao.Text );
    doc3.Content.Find.Execute( FindText := '%ctps_uf%' , replacewith               := frmFichaCrianca.cbCTPSUf.Text );

    if (edHorarioTrabalho.Text = '') then doc3.Content.Find.Execute( FindText := '%horario_trabalho%' , replacewith := edHorarioTrabalho.Text )
    else doc3.Content.Find.Execute( FindText := '%horario_trabalho%' , replacewith := '' );

    doc3.Content.Find.Execute( FindText := '%pis%' , replacewith                   := frmFichaCrianca.edNumPIS.Text );
    doc3.Content.Find.Execute( FindText := '%pis_emissao%' , replacewith           := frmFichaCrianca.mskPISDataEmissao.Text );
    doc3.Content.Find.Execute( FindText := '%pis_banco%' , replacewith             := frmFichaCrianca.edPISBanco.Text );

    doc3.Content.Find.Execute( FindText := '%sus%' , replacewith                   := frmFichaCrianca.edNumCartaoSUS.Text );
    doc3.Content.Find.Execute( FindText := '%sias%' , replacewith                  := frmFichaCrianca.edNumCartaoSIAS.Text );
    doc3.Content.Find.Execute( FindText := '%reservista%' , replacewith            := frmFichaCrianca.edReservistaNumero.Text );
    doc3.Content.Find.Execute( FindText := '%reservista_csm%' , replacewith        := frmFichaCrianca.edReservistaCSM.Text );

    doc3.Content.Find.Execute( FindText := '%data_admissao%', replacewith          := mskAdmissao.Text );
    doc3.Content.Find.Execute( FindText := '%termino_contrato%' , replacewith      := mskTerminoContrato.Text );
    doc3.Content.Find.Execute( FindText := '%exame_medico%' , replacewith          := mskExameMedico.Text );
    doc3.Content.Find.Execute( FindText := '%agencia%' , replacewith               := mskNumAgencia.Text );
    doc3.Content.Find.Execute( FindText := '%conta%' , replacewith                 := edNumCCorrente.Text );

    doc3.Content.Find.Execute( FindText := '%cargo%' , replacewith                 := cbCargoFuncao.Text );
    doc3.Content.Find.Execute( FindText := '%salario%' , replacewith               := edValorSalarioBase.Text );
    doc3.Content.Find.Execute( FindText := '%rg%' , replacewith                    := edHorarioTrabalho.Text );

end;

procedure TfrmEmiteContratoAdmissao.FormShow(Sender: TObject);
begin
  ShowMessage ('Favor conferir TODOS os campos antes de emitir os documentos para admiss�o');
  cbUnidadeDestino.Clear;
  try
    with dmDeca.cdsSel_Unidade do
    begin
      Close;
      Params.ParamByName ('@pe_cod_unidade').Value := Null;
      Params.ParamByName ('@pe_nom_unidade').Value := Null;
      Open;

      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        //Caso o STATUS da Unidade esteja 1, est� desativada
        if (dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0) then cbUnidadeDestino.Items.Add(Trim(FieldByName('num_ccusto').Value) + '-' + FieldByName('nom_unidade').Value);
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  except end;
end;

end.
