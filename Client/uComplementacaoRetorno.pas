unit uComplementacaoRetorno;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask;

type
  TfrmComplementacaoRetorno = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbNome: TLabel;
    btnPesquisar: TSpeedButton;
    txtMatricula: TMaskEdit;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    txtData: TMaskEdit;
    Panel1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AtualizaCamposBotoes(Modo: String);
    procedure btnSairClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure txtMatriculaExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComplementacaoRetorno: TfrmComplementacaoRetorno;

implementation

uses uFichaPesquisa, uDM, uPrincipal, uUtil;

{$R *.DFM}

procedure TfrmComplementacaoRetorno.AtualizaCamposBotoes(Modo: String);
begin
// atualiza o campos e botoes da tela conforme a opera��o (Modo)
  if Modo = 'Padrao' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := False;

    txtMatricula.Clear;
    lbNome.Caption := '';
    txtData.Clear;

    txtMatricula.Enabled := True;
    btnPesquisar.Enabled := True;

    btnConfirmar.Enabled := False;
    btnCancelar.Enabled := True;

  end

  else if Modo = 'Salvar' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;

    btnConfirmar.Enabled := True;
    btnCancelar.Enabled := True;

  end
end;

procedure TfrmComplementacaoRetorno.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

procedure TfrmComplementacaoRetorno.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmComplementacaoRetorno.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
      // desabilita processamento posterior da tecla
      Key := #0;
      Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
    end
    else if (Key = #27) then
    begin
      Key := #0;
      frmComplementacaoRetorno.Close;
    end;
end;

procedure TfrmComplementacaoRetorno.btnSairClick(Sender: TObject);
begin
  frmComplementacaoRetorno.Close;
end;

procedure TfrmComplementacaoRetorno.btnPesquisarClick(Sender: TObject);
begin
  // chama a tela de pesquisa (gen�rica)
  vvCOD_MATRICULA_PESQUISA := 'x';
  frmFichaPesquisa := TFrmFichaPesquisa.Create(Application);
  frmFichaPesquisa.ShowModal;

  if vvCOD_MATRICULA_PESQUISA <> 'x' then
  begin
    txtMatricula.Text := vvCOD_MATRICULA_PESQUISA;
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;
        lbNome.Caption := FieldByName('nom_nome').AsString;
        AtualizaCamposBotoes('Salvar');
        txtData.SetFocus;
      end;
    except end;
  end;
end;

procedure TfrmComplementacaoRetorno.txtMatriculaExit(Sender: TObject);
begin
if Length(Trim(txtMatricula.Text)) > 0 then
  begin
    try
      with dmDeca.cdsSel_Cadastro_Move do
      begin
        Close;
        Params.ParamByName('@pe_PosicaoRegistro').AsString := 'S';
        Params.ParamByName('@pe_Matricula_Atual').AsString := txtMatricula.Text;
        Params.ParamByName('@pe_cod_unidade').Value:= vvCOD_UNIDADE;
        Open;

        if Eof then
        begin
          lbNome.Caption := 'Ficha n�o cadastrada';
          AtualizaCamposBotoes('Padrao');
        end
        else
          lbNome.Caption := FieldByName('nom_nome').AsString;
      end;
    except end;
  end;
end;

procedure TfrmComplementacaoRetorno.btnConfirmarClick(Sender: TObject);
begin
 if (Length(Trim(txtMatricula.Text)) > 0) and
     (lbNome.Caption <> 'Ficha n�o cadastrada') then
  begin
    try

      // primeiro valida o Status da crian�a - se ela pode sofre este movimento
      if StatusCadastro(txtMatricula.Text) = 'Ativo' then
      begin
        with dmDeca.cdsInc_Cadastro_Historico do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').AsString := txtMatricula.Text;
          Params.ParamByName('@pe_cod_unidade').AsInteger := vvCOD_UNIDADE;
          Params.ParamByName('@pe_dat_historico').Value := GetValue(txtData, vtDate);
          Params.ParamByName('@pe_ind_tipo_historico').AsInteger := 14; //Complementa��o em Alfabetiza��o - Retorno
          Params.ParamByName('@pe_dsc_tipo_historico').AsString := 'Complementa��o em Alfabetiza��o - Sa�da';
          Params.ParamByName('@pe_log_cod_usuario').AsInteger := vvCOD_USUARIO;
          Execute;

        end;
        AtualizaCamposBotoes('Padrao');
      end
      else
      begin
        ShowMessage('Opera��o inv�lida!'+#13+#10+#13+#10+
               'Esta(e) crian�a/adolescente encontra-se ' +
               StatusCadastro(txtMatricula.Text));
        AtualizaCamposBotoes('Padrao');
      end;
    except
      ShowMessage('Ocorreu um erro e os dados n�o foram gravados!' +#13+#10 +#13+#10 +
               'Favor entrar em contato com o Administrador do Sistema');
    end;
  end
  else
  begin
    ShowMessage('Dados Inv�lidos!' + #13+#10+#13+#10+
              'Favor verificar se os campos est�o preenchidos corretamente');
  end;
end;

procedure TfrmComplementacaoRetorno.FormShow(Sender: TObject);
begin
  AtualizaCamposBotoes('Padrao');
end;

end.
