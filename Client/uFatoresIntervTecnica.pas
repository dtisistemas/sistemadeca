unit uFatoresIntervTecnica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db;

type
  TfrmFatoresIntervTecnica = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    edFator: TEdit;
    dbgFatores: TDBGrid;
    dsSel_Fatores_Intervencao: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure AtualizaCamposBotoes_Fatores (Modo : String);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFatoresIntervTecnica: TfrmFatoresIntervTecnica;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TfrmFatoresIntervTecnica.FormShow(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Fatores_Intervencao do
    begin
      Close;
      Params.ParamByName('@pe_cod_id_fator').Value := Null;
      Params.ParamByName('@pe_dsc_fator').Value := Null;
      Open;

      dbgFatores.Refresh;

      AtualizaCamposBotoes_Fatores('Padr�o');

    end;
  except end;
end;

procedure TfrmFatoresIntervTecnica.btnSairClick(Sender: TObject);
begin
  frmFatoresIntervTecnica.Close;
end;

procedure TfrmFatoresIntervTecnica.AtualizaCamposBotoes_Fatores(
  Modo: String);
begin
  if Modo = 'Padr�o' then
  begin
    edFator.Clear;
    GroupBox1.Enabled := False;
    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    edFator.Clear;
    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmFatoresIntervTecnica.btnNovoClick(Sender: TObject);
begin
  AtualizaCamposBotoes_Fatores ('Novo');
end;

procedure TfrmFatoresIntervTecnica.btnCancelarClick(Sender: TObject);
begin
  AtualizaCamposBotoes_Fatores ('Padr�o');
end;

procedure TfrmFatoresIntervTecnica.btnSalvarClick(Sender: TObject);
begin
  try
    with dmDeca.cdsInc_Fatores_Intervencao do
    begin
      Close;
      Params.ParamByName('@pe_dsc_fator').Value := Trim(edFator.Text);
      Params.ParamByName('@pe_log_cod_usuario').Value := vvCOD_USUARIO;
      Execute;

      with dmDeca.cdsSel_Fatores_Intervencao do
      begin
        Close;
        Params.ParamByName('@pe_cod_id_fator').Value := Null;
        Params.ParamByName('@pe_dsc_fator').Value := Null;
        Open;

        dbgFatores.Refresh;

        AtualizaCamposBotoes_Fatores('Padr�o');

      end;
    end;
  except end;
end;

end.
