unit uCadastroSeriesEscolares;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, ExtCtrls, Buttons, Db;

type
  TfrmCadastroSerieEscolar = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dbgSeriesEscolares: TDBGrid;
    Label1: TLabel;
    edSerieEscolar: TEdit;
    Panel3: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSair: TBitBtn;
    btnAlterar: TBitBtn;
    dsSel_SerieEscolar: TDataSource;
    btnAtivaDesativa: TBitBtn;                                    
    procedure btnSairClick(Sender: TObject);
    procedure LimpaCampos(Modo : String);
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure dbgSeriesEscolaresDblClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnAtivaDesativaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroSerieEscolar: TfrmCadastroSerieEscolar;

implementation

uses uDM, uUtil, uPrincipal;

{$R *.DFM}

procedure TfrmCadastroSerieEscolar.btnSairClick(Sender: TObject);
begin
  frmCadastroSerieEscolar.Close;
end;

procedure TfrmCadastroSerieEscolar.LimpaCampos(Modo: String);
begin
  if Modo = 'P' then
  begin

    edSerieEscolar.Clear;
    Panel1.Enabled := False;

    btnNovo.Enabled := True;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := False;
    btnSair.Enabled := True;

  end
  else
  if Modo = 'N' then
  begin
    edSerieEscolar.Clear;
    Panel1.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := True;
    btnAlterar.Enabled := False;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end
  else if Modo = 'A' then
  begin
    //edSerieEscolar.Clear;
    Panel1.Enabled := True;

    btnNovo.Enabled := False;
    btnSalvar.Enabled := False;
    btnAlterar.Enabled := True;
    btnCancelar.Enabled := True;
    btnSair.Enabled := False;
  end;
end;

procedure TfrmCadastroSerieEscolar.FormShow(Sender: TObject);
begin
  with dmDeca.cdsSel_SerieEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_serie').Value := Null;
    Open;
    dbgSeriesEscolares.Refresh;
  end;
  LimpaCampos('P');
end;

procedure TfrmCadastroSerieEscolar.btnNovoClick(Sender: TObject);
begin
  LimpaCampos('N');
end;

procedure TfrmCadastroSerieEscolar.btnCancelarClick(Sender: TObject);
begin
  LimpaCampos('P');
end;

procedure TfrmCadastroSerieEscolar.btnSalvarClick(Sender: TObject);
begin
  //Pede confirma��o
  if Application.MessageBox('Deseja Incluir a S�RIE ESCOLAR informada?',
                            'Cadastro de S�ries Escolares - Inclus�o',
                            MB_ICONQUESTION + MB_YESNO ) = idYes then
  begin

    try
      //Executa e procedure de inclus�o de s�rie escolar
      with dmDeca.cdsInc_SerieEscolar do
      begin
        Close;
        Params.ParamByName ('@pe_dsc_serie').Value := GetValue(edSerieEscolar.Text);
        Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
        Execute;

        //Ap�s inserir, exibe os dados atualizados no grid
        with dmDeca.cdsSel_SerieEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_serie').Value := Null;
          Open;
          dbgSeriesEscolares.Refresh;
          Application.MessageBox('Dados da S�rie Escolar lan�ados com sucesso...',
                                 'Cadastro de S�rie Escolar',
                                 MB_ICONASTERISK + MB_OK );
          LimpaCampos('P');
        end;
      end;

    except
      Application.MessageBox('Aten��o !!! Um ERRO ocorreu ao tentar consultar ' + #13+#10+
                             'os dados solicitados. Verifique a conex�o com a INTERNET, ' + #13+#10+
                             'conex�es de cabos de rede ou entre em contato com o  ' + #13+#10+
                             'Administrador do Sistema !!!',
                             'Avalia��o Desempenho - Aprendiz [ERRO]',
                             MB_ICONERROR + MB_OK );
    end;

  end
  else
  begin
    //Caso o usu�rio n�o confirme
    Application.MessageBox('Aten��o !!! Opera��o cancelada pelo Usu�rio... ' + #13+#10+
                           'Qualquer d�vida ou esclarecimento, procure o Administrador do Sistema !!!',
                           '[Sistema DECA] - S�ries Escolares',
                           MB_ICONERROR + MB_OK );
    LimpaCampos('P');  
  end;
end;

procedure TfrmCadastroSerieEscolar.dbgSeriesEscolaresDblClick(
  Sender: TObject);
begin
  LimpaCampos('A');
  edSerieEscolar.Text := dmDeca.cdsSel_SerieEscolar.FieldByName('dsc_serie').Value;
  edSerieEscolar.SetFocus;
end;

procedure TfrmCadastroSerieEscolar.btnAlterarClick(Sender: TObject);
begin
  //Alterar apenas o nome da s�rie escolar
  //Passar tipo=1
  try
    with dmDeca.cdsAlt_SerieEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_tipo').Value := 1;
      Params.ParamByName ('@pe_cod_id_serie').Value := dmDeca.cdsSel_SerieEscolar.FieldByName ('cod_id_serie').Value;
      Params.ParamByName ('@pe_dsc_serie').Value := GetValue(edSerieEscolar.Text);
      Execute;

      //Atualiza o grid
      with dmDeca.cdsSel_SerieEscolar do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;
        dbgSeriesEscolares.Refresh;
        LimpaCampos('P');
      end;
    end;
  except end;
end;

procedure TfrmCadastroSerieEscolar.btnAtivaDesativaClick(Sender: TObject);
begin
  try
    with dmDeca.cdsAlt_SerieEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_tipo').Value := 2;
      Params.ParamByName ('@pe_cod_id_serie').Value := dmDeca.cdsSel_SerieEscolar.FieldByName ('cod_id_serie').Value;
      if (dmDeca.cdsSel_SerieEscolar.FieldByName('flg_status').Value = 0) then
        Params.ParamByName ('@pe_flg_status').Value := 1
      else
        Params.ParamByName ('@pe_flg_status').Value := 0;
      Execute;

      //Atualiza o grid
      with dmDeca.cdsSel_SerieEscolar do
      begin
        Close;
        Params.ParamByName ('@pe_cod_id_serie').Value := Null;
        Open;
        dbgSeriesEscolares.Refresh;
        LimpaCampos('P');
      end;
    end;
  except end;
end;

end.
