unit uDefasagemEscolar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, DBGrids, Buttons, Db;

type
  TfrmDefasagemEscolar = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    cbSerieEscolar: TComboBox;
    edIdadeIdeal: TEdit;
    edIdadeDefasada: TEdit;
    dbgDadosDefasagem: TDBGrid;
    btnNovoD: TBitBtn;
    btnAlteraD: TBitBtn;
    btnGravaD: TBitBtn;
    btnCancelaD: TBitBtn;
    btnImprimirD: TBitBtn;
    btnSairD: TBitBtn;
    dsDefasagemEscolar: TDataSource;
    procedure btnSairDClick(Sender: TObject);
    procedure LimpaCampos(Modo:String);
    procedure FormShow(Sender: TObject);
    procedure btnNovoDClick(Sender: TObject);
    procedure btnGravaDClick(Sender: TObject);
    procedure btnCancelaDClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDefasagemEscolar: TfrmDefasagemEscolar;
  vListaSerieD1, vListaSerieD2 : TStringList;

implementation

uses uDM, uUtil, uPrincipal;

{$R *.DFM}

procedure TfrmDefasagemEscolar.btnSairDClick(Sender: TObject);
begin
  frmDefasagemEscolar.Close;
end;

procedure TfrmDefasagemEscolar.LimpaCampos(Modo: String);
begin
  if Modo = 'Padrao' then
  begin
    //Campos
    cbSerieEscolar.ItemIndex := -1;
    edIdadeIdeal.Clear;
    edIdadeDefasada.Clear;
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;
    GroupBox3.Enabled := False;
    Panel2.Enabled := True;

    //Bot�es
    btnNovoD.Enabled := True;
    btnGravaD.Enabled := False;
    btnAlteraD.Enabled := False;
    btnCancelaD.Enabled := False;
    btnImprimirD.Enabled := True;
    btnSairD.Enabled := True;
  end
  else if Modo = 'Novo' then
  begin
    GroupBox1.Enabled := True;
    GroupBox2.Enabled := True;
    GroupBox3.Enabled := True;
    Panel2.Enabled := False;
    //Bot�es
    btnNovoD.Enabled := False;
    btnGravaD.Enabled := True;
    btnAlteraD.Enabled := False;
    btnCancelaD.Enabled := True;
    btnImprimirD.Enabled := False;
    btnSairD.Enabled := False;
  end;
end;
procedure TfrmDefasagemEscolar.FormShow(Sender: TObject);
begin

  vListaSerieD1 := TStringList.Create();
  vListaSerieD2 := TStringList.Create();

  //Atualiza o grid com os dados inseridos...
  with dmDeca.cdsSel_DefasagemEscolar do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_defasagemesc').Value := Null;
    Params.ParamByName ('@pe_cod_id_serie').Value := Null;
    Open;
    dbgDadosDefasagem.Refresh;
  end;

  //Carregar os dados das s�ries no combo cbSerieEscolar e para as Listas
  try
    with dmDeca.cdsSel_SerieEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_serie').Value := Null;
      Open;

      cbSerieEscolar.Clear;
      while not (dmDeca.cdsSel_SerieEscolar.eof) do
      begin
        vListaSerieD1.Add (IntToStr(FieldByName('cod_id_serie').Value));
        vListaSerieD2.Add (FieldByName('dsc_serie').Value);
        cbSerieEscolar.Items.Add(FieldByName('dsc_serie').Value);
        dmDeca.cdsSel_SerieEscolar.Next;
      end;
    end;
  except
    Application.MessageBox ('Aten��o !!!' + #13+#10 +
                            'Ocorreu um ERRO e os dados de S�ries Escolares ' + #13+#10 +
                            'n�o foram carregados. Favor entrar em contato com ' + #13+#10 +
                            'o Administrador do Sistema.',
                            '[Sistema Deca] - Defasagem Escolar',
                            MB_OK + MB_ICONERROR);
  end;

  LimpaCampos('Padrao');
  
end;

procedure TfrmDefasagemEscolar.btnNovoDClick(Sender: TObject);
begin
  LimpaCampos('Novo');
  cbSerieEscolar.SetFocus;
  edIdadeIdeal.Text := '0';
  edIdadeDefasada.Text := '0';
end;

procedure TfrmDefasagemEscolar.btnGravaDClick(Sender: TObject);
begin
  //Validar os dados na tabela, verificar se j� existem dados lan�ados para a s�rie selecionada...
  try
    with dmDeca.cdsSel_DefasagemEscolar do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_defasagemesc').Value := Null;
      Params.ParamByName ('@pe_cod_id_serie').Value := vListaSerieD1.Strings[cbSerieEscolar.ItemIndex];
      Open;

      if dmDeca.cdsSel_DefasagemEscolar.RecordCount > 0 then
      begin
        Application.MessageBox ('Aten��o!!!' + #13+#10 +
                                'J� existem dados lan�ados para a s�rie selecionada.' + #13+#10 +
                                'Selecione outra ou fa�a a ALTERA��O dos dados da mesma.',
                                '[Sistema Deca] - Defasagem Escolar',
                                MB_OK + MB_ICONWARNING);
                                LimpaCampos('Padrao');
      end
      else
      begin
        //N�o localizou, ent�o inluir...
        with dmDeca.cdsInc_DefasagemEscolar do
        begin
          Close;
          Params.ParamByName ('@pe_cod_id_serie').Value := vListaSerieD1.Strings[cbSerieEscolar.ItemIndex];
          Params.ParamByName ('@pe_num_idade_ideal').Value := GetValue(edIdadeIdeal.Text);
          Params.ParamByName ('@pe_num_idade_defasada').Value := GetValue(edIdadeDefasada.Text);
          Params.ParamByName ('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Execute;

          //Atualiza o grid com os dados inseridos...
          with dmDeca.cdsSel_DefasagemEscolar do
          begin
            Close;
            Params.ParamByName ('@pe_cod_id_defasagemesc').Value := Null;
            Params.ParamByName ('@pe_cod_id_serie').Value := Null;
            Open;
            dbgDadosDefasagem.Refresh;
          end;

          LimpaCampos('Padrao');

        end;
      end;
    end;
  except
    Application.MessageBox ('Aten��o! Erro carregando dados...','[Sistema Deca]-Defasagem Escolar',MB_OK + MB_ICONERROR);
  end;
end;

procedure TfrmDefasagemEscolar.btnCancelaDClick(Sender: TObject);
begin
  LimpaCampos('Padrao');
end;

end.
