unit rConsultaPedidosUniformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, Mask, Buttons;

type
  TfrmConsultaPedidos = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Label1: TLabel;
    ComboBox1: TComboBox;
    Label2: TLabel;
    txtMatricula: TMaskEdit;
    btnPesquisar: TSpeedButton;
    lbNome: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ComboBox2: TComboBox;
    MaskEdit1: TMaskEdit;
    Label5: TLabel;
    ComboBox3: TComboBox;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Label7: TLabel;
    ComboBox4: TComboBox;
    Label8: TLabel;
    MaskEdit3: TMaskEdit;
    Label9: TLabel;
    ComboBox5: TComboBox;
    SpeedButton1: TSpeedButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPedidos: TfrmConsultaPedidos;

implementation

uses uDM, uGerarPedidoUniformes;

{$R *.DFM}

end.
