unit uAberturaChamadoTecnico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, Mask, ExtCtrls, Db;

type
  TfrmAberturaChamadoTecnico = class(TForm)
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnImprimir: TBitBtn;
    btnSair: TBitBtn;
    GroupBox1: TGroupBox;
    edDataAberturaChamado: TMaskEdit;
    GroupBox2: TGroupBox;
    edUnidade: TEdit;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    btnPesquisar: TSpeedButton;
    edPatrimonio: TMaskEdit;
    edDescricaoPatrimonio: TEdit;
    Label2: TLabel;
    meDefeitoReclamado: TMemo;
    Panel2: TPanel;
    dbgResultados: TDBGrid;
    dsResultado: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAberturaChamadoTecnico: TfrmAberturaChamadoTecnico;

implementation

uses uDM, uPrincipal, uUtil, uLocalizaPatrimonio;

{$R *.DFM}

end.
