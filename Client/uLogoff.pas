unit uLogoff;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask;

type
  TfrmLogoff = class(TForm)
    GroupBox1: TGroupBox;
    cbUnidadeLogoff: TComboBox;
    btnLogin: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    mskSenha: TEdit;
    procedure btnLoginClick(Sender: TObject);
    procedure cbUnidadeLogoffClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogoff: TfrmLogoff;
  i: integer;
  vListaUnidadesLogoff1, vListaUnidadesLogoff2 : TStringList; 

implementation

uses uDM, uPrincipal, uAcesso;

{$R *.DFM}

procedure TfrmLogoff.btnLoginClick(Sender: TObject);
begin

try
      with uDM.dmDeca.cdsSel_Acesso do
      begin
        Close;
        //Par�metros atuais
        Params.ParamByName ('@pe_cod_id_acesso').Value := vvCOD_ID_ACESSO;
        Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
        Params.ParamByName ('@pe_cod_unidade').Value   := Null;
        Params.ParamByName ('@pe_cod_perfil').Value    := Null;
        Params.ParamByName ('@pe_flg_ativo').Value     := Null;
        Params.ParamByName ('@pe_flg_online').Value    := Null;
        Params.ParamByName ('@pe_cod_matricula').Value := Null;
        //vvNOM_USUARIO := Trim(dmDeca.cdsSel_Acesso.FieldByName ('nom_usuario').Values);
        Open;

        if (Trim(mskSenha.Text) = '') or
           (Trim(uDM.dmDeca.cdsSel_Acesso.FieldByName('dsc_senha').Value) <> Trim(mskSenha.Text)) or
           (cbUnidadeLogoff.ItemIndex = -1) then
        begin
          Application.MessageBox('Acesso inv�lido!'+#13+#10+
                                 'Favor tentar novamente ou entrar em contato com o Administrador do Sistema.',
                                 '[Sistema DECA] - Autentica��o',
                                 MB_OK + MB_ICONERROR);
          mskSenha.Clear;
          mskSenha.SetFocus;
          vOk := false
        end
        else
        begin
          //Atualiza o campo flg_online do usu�rio
          {
          try
            with dmDeca.cdsAlt_Status_Usuario do
            begin
              Close;
              Params.ParamByName ('@pe_cod_usuario').Value := dmDeca.cdsSel_Acesso.FieldByName ('cod_id_acesso').Value;
              Params.ParamByName ('@pe_flg_online').Value := 1; // 1 para On-Line e 0 para Off-Line
              Execute;
            end;
          except
            //Mensagem de erro na tentativa de atualiza��o do status do usu�rio
          end;
          }
          vOk := True;

          frmPrincipal.StatusBar1.Panels[1].Text := 'USU�RIO(A): ' + Trim(FieldByName('nom_usuario').Value) + '  -  ON-LINE';

          //Carrega as vari�veis publicas
          vvCOD_USUARIO  := dmDeca.cdsSel_Acesso.FieldByName('cod_usuario').Value;
          vvIND_PERFIL   := dmDeca.cdsSel_Acesso.FieldByName('cod_perfil').Value;
          vvNOM_PERFIL   := dmDeca.cdsSel_Acesso.FieldByName('dsc_perfil').Value;
          vvTIPO_UNIDADE := dmDeca.cdsSel_Acesso.FieldByName('ind_tipo_unidade').Value;

          //Permitir ao Administrador e Psicologia/Psicopedagogia
          //Diretor, DRH, Triagem, Educa��o F�sica e As. Social(Proj. Gestante) visualizar todas as c�as/adolescentes
          if (vvIND_PERFIL = 1) or (vvIND_PERFIL = 5) or
             (vvIND_PERFIL = 7) or (vvIND_PERFIL = 8) or (vvIND_PERFIL = 9) or (vvIND_PERFIL = 10) or
             (vvIND_PERFIL = 11) or (vvIND_PERFIL = 12) or (vvIND_PERFIL = 13) or (vvIND_PERFIL = 14) or
             (vvIND_PERFIL = 15) or (vvIND_PERFIL = 16) or (vvIND_PERFIL = 17) or (vvIND_PERFIL = 18) or (vvIND_PERFIL = 19) or (vvIND_PERFIL = 20) or
             (vvIND_PERFIL = 23) or (vvIND_PERFIL = 24) or (vvIND_PERFIL = 25)then
          begin
            vvCOD_UNIDADE := 0;
            vvNOM_UNIDADE := 'FUNDHAS';
            frmPrincipal.StatusBar1.Panels[3].Text := 'FUNDHAS [';
          end
          else
          begin
            try
              {with dmDeca.cdsSel_Unidade do
              begin
                Close;
                Params.ParamByName ('@pe_cod_unidade').Value := dmDeca.cdsSel_Usuario.FieldByName ('cod_unidade').Value;
                Params.ParamByName ('@pe_nom_unidade').Value := Null;
                Open;
              }
                vvCOD_UNIDADE := dmDeca.cdsSel_Acesso.FieldByName('cod_unidade').AsInteger;
                vvNOM_UNIDADE := dmDeca.cdsSel_Acesso.FieldByName('nom_unidade').AsString;
                vvNUM_CCUSTO  := dmDeca.cdsSel_Acesso.FieldByName('num_ccusto').AsString;
                vvNOM_GESTOR  := dmDeca.cdsSel_Acesso.FieldByName('nom_gestor').AsString;
                vvDSC_EMAIL   := dmDeca.cdsSel_Acesso.FieldByName('dsc_email').AsString;

            except
              //Mensagem de erro na tentativa de carregar os dados da unidade
            end;

            frmPrincipal.StatusBar1.Panels[3].Text := 'Unidade: ' + FieldByName('nom_unidade').Value + ' [';
        end;

        frmPrincipal.AtualizaStatusBarUnidade;

        //Tira todos da unidade logada da Suspens�o
        try
          with dmDeca.cdsAlt_Cadastro_Tira_Suspensao do
          begin
            Close;
            Execute;
          end;
        except
          ShowMessage('Sr. Usu�rio'+#13+#10+#13+#10+
                      'Ocorreu um problema no sistema, favor informar o Administrador'+#13+#10+
                      'do Sistema informando esta mensagem: ERRO COM RETORNO DE SUSPENS�ES');
        end;

        Close;

      end
    end
  except end;

  if vOk then
  begin
    // preparar o menu conforme o perfil
    with frmPrincipal.mmPrincipal do
    begin

      case vvIND_PERFIL of

        1: // Administrador
        begin

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := True; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := True;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := True;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := True;  //Nutri��o - Unidades

          //Menu Movimenta��es
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Administrador do Sistema';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Administrador do Sistema';

          frmPrincipal.mmPrincipal.Items[1].Visible := True;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do  //27 do
            Items[1].Items[i].Visible := True;

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := True; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := True; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := True; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := True; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := True;

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := True; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := True; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := True;

        end;

        2: // Gestor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Gestor';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Gestor';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := True; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios

          //if vvCOD_UNIDADE=44 then  //somente convenio
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True; //Unidades
          //else
          //  frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades

          //frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios

          //Permitir ao Gestor do Conv�nio visualizar o cadastro de empresas
          //if vvCOD_UNIDADE=44 then
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True;
          //else
          //  frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False;

          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := True; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := True; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := True; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := True; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := True; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := True; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := True; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := True;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := True;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := True;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := True;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := True;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := True;   //Cart�o Passe, Cotas e VT�s

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Habilita menu para Gestor Conv�nio
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True //Transferidos DAPA - Acesso Conv�nio
          else
            frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          if vvCOD_UNIDADE = 44 then  //Somente o Gestor do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          if vvCOD_UNIDADE in [23,44,86,31] then
            frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := True //Aditamento de Contato - Aprendiz
          else
            frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          
          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        3: // Assistente Social
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Assistente Social';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assistente Social';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := True;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := True; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := True; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := True; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := True; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := True; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := True; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := True; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := True;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := True;   //Cart�o Passe, Cotas e VT�s

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := True; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := True; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Somente assistentes sociais do conv�nio...
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True //Rela��o de Empresas por As. Social (Conv�nios)
          else
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          if vvCOD_UNIDADE = 44 then  //Somente a Assistente Social do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        4: // Outros - Operacional
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Consulta';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Consulta';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := False; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        5: //Equipe Multi
        begin
          frmPrincipal.StatusBar1.Panels[5].Text := 'Equipe Multi';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        6: //Professor-Instrutor
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Professor/Instrutor';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Professor/Instrutor';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := True; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := True; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := True; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := True; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := True; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := True;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          //frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := True; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        7: // DIRETOR
        begin

        frmPrincipal.StatusBar1.Panels[5].Text := 'Diretor(a)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acompanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19,20
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := True; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := False; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        8:  //DRH
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Divis�o de Recursos Humanos';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Divis�o de Recursos Humanos';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          {frmPrincipal.mmPrincipal.Items[1].Visible := False;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do  //27 do
            Items[1].Items[i].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          //frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica}
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := True;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := True;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := True;   //Cart�o Passe, Cotas e VT�s

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := True; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := True;

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        9:  //Triagem
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Triagem';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Triagem';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := True; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := True; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := True; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := True;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        10: //Educa��o F�sica
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Educa��o F�sica';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Educa��o F�sica';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := True; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Items[0].Visible := True; //Lan�amento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[23].Items[1].Visible := True; //Reclassifica��o do IMC
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; ////Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        11: //Assistente Social(Projeto Gestante)
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Assistente Social(Projeto Gestante)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assistente Social(Projeto Gestante)';

          ///Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := False; //Oculta o Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          ///Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;

        12: // Nutri��o
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Compras/Almoxarifado';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := False;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := False;
          for i := 0 to frmPrincipal.mmPrincipal.Items.Count - 1 do        //27 do
            Items[1].Items[i].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica

          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False;  //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s                    

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := False; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;
                    
        end;

        13: // Odontologia
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Odontologia';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := True; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := False; //Acompanhamento Escolar

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s                  

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        14: //Servi�o Social(Plant�o)
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Servi�o Social(Plant�o)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        15: //Servi�o Social(Supervis�o)
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Servi�o Social(Supervis�o)';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        16: //Coordenador
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Coordenador de Projeto';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Exibe o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := True; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := True;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := False; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := False; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := False; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := False; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := False; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := False; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := False; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := False; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

        17:  //Assessoria da Qualidade
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Diretor(a)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Assessoria da Qualidade';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rio
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Exibe o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := False; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := True; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := True; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := False; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := False; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := False; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := False; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := False; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := False; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := False; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Somente assistentes sociais do conv�nio...
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True //Rela��o de Empresas por As. Social (Conv�nios)
          else
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := True; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;


        18:  //Acompanhamento Escolar(Servi�o Social)
        begin
          //frmPrincipal.StatusBar1.Panels[2].Text := 'Diretor(a)';
          frmPrincipal.StatusBar1.Panels[5].Text := 'Acomp. Escolar/Social';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := True;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Visible := True; //Oculta o Menu Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := True; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := True; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False;  //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := False; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := True;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica

          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := False; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          
        end;


        19: // Coordena��o Acomp. Escolar
        begin
          frmPrincipal.StatusBar1.Panels[5].Text := 'Acompanhamento Escolar';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usuarios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := True; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := False; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := True; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := True;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[0].Visible := True; //Dados Escolares das Crian�as e Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[1].Visible := True; //Do Peti e Fundhas/Peti
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[2].Visible := True; //Ordenados por Matr�cula
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[3].Visible := True; //Ordenados por Nome
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[4].Visible := True; //Ordenados por CEP
          frmPrincipal.mmPrincipal.Items[2].Items[0].Items[5].Visible := True; //Idade Espec�fica
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := False; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := False; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := False; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := False; //Total Por Unidade e Situa��o - DS

          //Somente assistentes sociais do conv�nio...
          if vvCOD_UNIDADE = 44 then
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := True //Rela��o de Empresas por As. Social (Conv�nios)
          else
            frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico

          if vvCOD_UNIDADE = 44 then  //Somente a Assistente Social do Conv�nios
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := True; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := True; //Cadastro de Empresas
          end
          else
          begin
            frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
            frmPrincipal.ToolBar1.Buttons[5].Visible := False; //Cadastro de Empresas
          end;

          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := True; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;          

        end;



        20: // RA
        begin

          frmPrincipal.StatusBar1.Panels[5].Text := 'Representante da Administra��o';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := True; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := True; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acomanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := True; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := True; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := True;  //Somente Perfil 1,18,19,20
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := True;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := True; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := False; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := False; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;


        23: // CHEFE DE DIVISAO/AS. SOCIAL
        begin

        frmPrincipal.StatusBar1.Panels[5].Text := 'Chefe de Divis�o/As. Social';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := True; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acompanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19,20
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := True; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := True; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := False; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;


        24: // CHEFE DE DIVISAO
        begin

        frmPrincipal.StatusBar1.Panels[5].Text := 'Chefe de Divis�o';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acompanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19,20
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := False;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := True; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := False; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;


        25: // SUPERVISOR
        begin

        frmPrincipal.StatusBar1.Panels[5].Text := 'Supervisor de Divis�o';

          //Menu Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[0].Visible := True;  //Cadastros
          frmPrincipal.mmPrincipal.Items[0].Items[1].Visible := False; //Escolas
          frmPrincipal.mmPrincipal.Items[0].Items[2].Visible := False; //Turmas
          frmPrincipal.mmPrincipal.Items[0].Items[3].Visible := False; //Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[4].Visible := False; //Unidades
          frmPrincipal.mmPrincipal.Items[0].Items[5].Visible := False; //Gerenciador de Acessos
          frmPrincipal.mmPrincipal.Items[0].Items[6].Visible := False; //Perfis de Usu�rios
          frmPrincipal.mmPrincipal.Items[0].Items[7].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[8].Visible := True;  //Sair
          frmPrincipal.mmPrincipal.Items[0].Items[9].Visible := True;  //Tra�o
          frmPrincipal.mmPrincipal.Items[0].Items[10].Visible := False;  //Pesquisa Gen�rica
          frmPrincipal.mmPrincipal.Items[0].Items[11].Visible := False;  //Fatores de Interven��o T�cnica
          frmPrincipal.mmPrincipal.Items[0].Items[12].Visible := False;  //Programas Sociais
          frmPrincipal.mmPrincipal.Items[0].Items[13].Visible := False;  //Controle de Calend�rios
          frmPrincipal.mmPrincipal.Items[0].Items[14].Visible := False;  //Nutri��o - Unidades

          //Menu de Movimenta��es
          frmPrincipal.mmPrincipal.Items[1].Items[0].Visible := False; //Advert�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[1].Visible := False; //Afastamento
          frmPrincipal.mmPrincipal.Items[1].Items[2].Visible := False; //Suspens�o
          frmPrincipal.mmPrincipal.Items[1].Items[3].Visible := False; //Desligamento
          frmPrincipal.mmPrincipal.Items[1].Items[4].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[5].Visible := False; //Transfer�ncia de Unidade
          frmPrincipal.mmPrincipal.Items[1].Items[6].Visible := False; //Transfer�ncia para Conv�nio
          frmPrincipal.mmPrincipal.Items[1].Items[7].Visible := False; //Mudan�a de Matr�cula
          frmPrincipal.mmPrincipal.Items[1].Items[8].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[9].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Visible := True; //Novos Lan�amentos
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[0].Visible := True; //Crian�as
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[1].Visible := True; //Adolescentes
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[2].Visible := False; //Fam�lias
          frmPrincipal.mmPrincipal.Items[1].Items[9].Items[0].Items[3].Visible := False; //Profissional
          frmPrincipal.mmPrincipal.Items[1].Items[10].Visible := False; //Registro de Evolu��o Social
          frmPrincipal.mmPrincipal.Items[1].Items[11].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[12].Visible := False; //Complementa��o em Alfabetiza��o
          frmPrincipal.mmPrincipal.Items[1].Items[13].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[14].Visible := False; //Apontamento de Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[15].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[16].Visible := True; //Hist�rico
          frmPrincipal.mmPrincipal.Items[1].Items[17].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[18].Visible := False; //Confirma��o de Transfer�ncia
          frmPrincipal.mmPrincipal.Items[1].Items[19].Visible := True; //Controle de Frequ�ncias
          frmPrincipal.mmPrincipal.Items[1].Items[20].Visible := False; //Inicializar Frequencia
          frmPrincipal.mmPrincipal.Items[1].Items[21].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[22].Visible := False; //Registro de Visita Tecnica
          frmPrincipal.mmPrincipal.Items[1].Items[23].Visible := False; //Lan�amento de Avalia��o Biom�trica
          frmPrincipal.mmPrincipal.Items[1].Items[24].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[25].Visible := False; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[0].Visible := False; //Acompanhamento Escolar - Inicilizar Bimestre
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[1].Visible := False; //Acompanhamento Escolar - Consultar/Alterar
          frmPrincipal.mmPrincipal.Items[1].Items[25].Items[2].Visible := False; //Acompanhamento Escolar - Emitir Acompanhamento
          frmPrincipal.mmPrincipal.Items[1].Items[26].Visible := False; //Acompanhamento Psicopedag�gico
          frmPrincipal.mmPrincipal.Items[1].Items[27].Visible := False; //Observa��o de Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[28].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[29].Visible := False; //Monitoramento de Avalia��o
          frmPrincipal.mmPrincipal.Items[1].Items[30].Visible := False; //Ficha Cl�nica Odontol�gica
          frmPrincipal.mmPrincipal.Items[1].Items[31].Visible := False; //Avalia��o de Desempenho Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[32].Visible := False; //Satisfa��o do Cliente Conveniado
          frmPrincipal.mmPrincipal.Items[1].Items[33].Visible := True; //Gerenciador de Solicita��es
          frmPrincipal.mmPrincipal.Items[1].Items[34].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[35].Visible := True; //Acompanhamento Escolar
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[0].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[1].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[2].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[3].Visible := False;  //Somente Perfil 1,18,19,20
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[5].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[6].Visible := True;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[7].Visible := False;
          frmPrincipal.mmPrincipal.Items[1].Items[35].Items[8].Visible := False;

          frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;   //Aditamento de Contrato - Aprendiz
          frmPrincipal.mmPrincipal.Items[1].Items[37].Visible := False;  //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[38].Visible := True;  //Solicita��o de Uniformes (Apenas pr�xima vers�o)
          frmPrincipal.mmPrincipal.Items[1].Items[39].Visible := False;   //Tra�o
          frmPrincipal.mmPrincipal.Items[1].Items[40].Visible := False;   //Processo Admissional/Aprendizagem
          frmPrincipal.mmPrincipal.Items[1].Items[41].Visible := False;   //Emiss�o Termo de Compromisso Bolsista
          frmPrincipal.mmPrincipal.Items[1].Items[42].Visible := False;   //Cart�o Passe, Cotas e VT�s          

          //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Visible := True; //Menu Relat�rios
          frmPrincipal.mmPrincipal.Items[2].Items[0].Visible := True; //Rela��o de Crian�as/Adolescentes
          frmPrincipal.mmPrincipal.Items[2].Items[1].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[2].Visible := True; //Rela��o de Advert�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[3].Visible := True; //Rela��o de Afastamentos
          frmPrincipal.mmPrincipal.Items[2].Items[4].Visible := True; //Rela��o de Desligados
          frmPrincipal.mmPrincipal.Items[2].Items[5].Visible := True; //Rela��o de Suspens�es
          frmPrincipal.mmPrincipal.Items[2].Items[6].Visible := True; //Rela��o de Transfer�ncias
          frmPrincipal.mmPrincipal.Items[2].Items[7].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[8].Visible := False; //Rela��o de Aniversariantes do M�s
          frmPrincipal.mmPrincipal.Items[2].Items[9].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[10].Visible := False; //Lista de Presen�a Reuni�o de Pais
          frmPrincipal.mmPrincipal.Items[2].Items[11].Visible := False; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[12].Visible := True; //Rela��o de Crian�as por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[13].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[14].Visible := True; //Registro de Atendimento
          frmPrincipal.mmPrincipal.Items[2].Items[15].Visible := False; //Evolu��o Servi�o Social
          frmPrincipal.mmPrincipal.Items[2].Items[16].Visible := False; //Indicadores Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[17].Visible := True; //Tra�o
          frmPrincipal.mmPrincipal.Items[2].Items[18].Visible := True; //Resumo Mensal
          frmPrincipal.mmPrincipal.Items[2].Items[19].Visible := True; //Folha de Frequ�ncia
          frmPrincipal.mmPrincipal.Items[2].Items[20].Visible := False; //Relat�rio de Encaminhamento
          frmPrincipal.mmPrincipal.Items[2].Items[21].Visible := True; //Relat�rio de Totais por Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[22].Visible := True; //Relat�rio de Programas Sociais/Situa��o
          frmPrincipal.mmPrincipal.Items[2].Items[23].Visible := False; //Estat�sticas do Serv. Social
          frmPrincipal.mmPrincipal.Items[2].Items[24].Visible := False; //Rela��o de Empresas por As. Social (Conv�nios)
          frmPrincipal.mmPrincipal.Items[2].Items[25].Visible := True; //Totaliza��o de Registros por Usu�rio x Unidade
          frmPrincipal.mmPrincipal.Items[2].Items[26].Visible := True; //Transferidos DAPA - Acesso Conv�nio
          frmPrincipal.mmPrincipal.Items[2].Items[27].Visible := True; //RAICA
          frmPrincipal.mmPrincipal.Items[2].Items[28].Visible := True; //Resumo Matriz
          frmPrincipal.mmPrincipal.Items[2].Items[29].Visible := True; //Matriz de Avalia��o Semestral/Anual
          frmPrincipal.mmPrincipal.Items[2].Items[30].Visible := True; //Total Por Unidade e Situa��o - DS

          //Menu Gr�fico
          frmPrincipal.mmPrincipal.Items[3].Visible := False;
          frmPrincipal.mmPrincipal.Items[3].Items[0].Visible := False; //Total por Unidade
          frmPrincipal.mmPrincipal.Items[3].Items[1].Visible := False; //Total Adolescentes nas Empresas
          frmPrincipal.mmPrincipal.Items[3].Items[2].Visible := False; //Acompanhamento Biom�trico


          //Menu Dados para DRH
          frmPrincipal.mmPrincipal.Items[4].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[0].Visible := False; //
          frmPrincipal.mmPrincipal.Items[4].Items[1].Visible := False; //Rela��o de Transferidos por Per�odo
          frmPrincipal.mmPrincipal.Items[4].Items[2].Visible := False;
          frmPrincipal.mmPrincipal.Items[4].Items[3].Visible := False;          

          //Menu Utilit�rios
          frmPrincipal.mmPrincipal.Items[5].Visible := True;
          frmPrincipal.mmPrincipal.Items[5].Items[0].Visible := True; //Dados da Vers�o
          frmPrincipal.mmPrincipal.Items[5].Items[1].Visible := False; //Usu�rios Conectados
          frmPrincipal.mmPrincipal.Items[5].Items[2].Visible := False; //Abordagem Grupal
          frmPrincipal.mmPrincipal.Items[5].Items[3].Visible := False; //Faltas-Teste
          frmPrincipal.mmPrincipal.Items[5].Items[4].Visible := False; //Chat
          frmPrincipal.mmPrincipal.Items[5].Items[5].Visible := False; //Cor de Fundo
          frmPrincipal.mmPrincipal.Items[5].Items[6].Visible := True; //Altera��es da Vers�o

          //Menu Ferramentas Administrativas
          frmPrincipal.mmPrincipal.Items[6].Visible := False;

        end;

      end;

      if (vvTIPO_UNIDADE = 2) and (vvIND_PERFIL in [1,2]) then
        frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := True
      else
        frmPrincipal.mmPrincipal.Items[1].Items[36].Visible := False;
    end;

    //Esconder a tela de autentica��o
    //frmAcesso2.Hide;
    //Abrir a tela principal do sistema
    //frmPrincipal.ShowModal;
    //Fechar a tela de autentica��o e o sistema
    //frmAcesso.Close;
  end;

end;

procedure TfrmLogoff.cbUnidadeLogoffClick(Sender: TObject);
begin
  try
    with dmDeca.cdsSel_Acesso do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
      Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
      Params.ParamByName ('@pe_cod_unidade').Value   := StrToInt(vListaUnidadesLogoff1.Strings[cbUnidadeLogoff.ItemIndex]);
      Params.ParamByName ('@pe_cod_perfil').Value    := Null;
      Params.ParamByName ('@pe_flg_ativo').Value     := Null;  //Validar somente usu�rio ATIVO
      Params.ParamByName ('@pe_flg_online').Value    := Null;
      Params.ParamByName ('@pe_cod_matricula').Value := Null;
      Open;

      if RecordCount > 0 then
      begin
        //Recupera as vari�veis a serem usadas no sistema
        vvIND_PERFIL := FieldByName ('cod_perfil').Value;
        vvCOD_UNIDADE := FieldByName('cod_unidade').Value;
        vvNOM_UNIDADE := FieldByName('nom_unidade').Value;
        vvCOD_ID_ACESSO := FieldByName('cod_id_acesso').Value;
      end;
    end;
  except
    //Mensagem de erro de conex�o...
  end;
end;

procedure TfrmLogoff.FormShow(Sender: TObject);
begin
  mskSenha.Clear;
  //Carregar a lista de unidades do usu�rio logado
  vListaUnidadesLogoff1 := TStringList.Create();
  vListaUnidadesLogoff2 := TStringList.Create();
  cbUnidadeLogoff.Clear;

  with dmDeca.cdsSel_Acesso do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_acesso').Value := Null;
    Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
    Params.ParamByName ('@pe_cod_unidade').Value   := Null;
    Params.ParamByName ('@pe_cod_perfil').Value    := Null;
    Params.ParamByName ('@pe_flg_ativo').Value     := 0; //Somente os acessos ATIVOS para o usu�rio
    Params.ParamByName ('@pe_flg_online').Value    := Null;
    Params.ParamByName ('@pe_cod_matricula').Value := Null;
    Open;

    while not (dmDeca.cdsSel_Acesso.eof) do
    begin
      vListaUnidadesLogoff1.Add (IntToStr(FieldByName('cod_unidade').Value));
      vListaUnidadesLogoff2.Add (FieldByName('nom_unidade').Value);
      cbUnidadeLogoff.Items.Add (FieldByName('nom_unidade').Value);
      dmDeca.cdsSel_Acesso.Next;
    end;
  end;
end;

end.
