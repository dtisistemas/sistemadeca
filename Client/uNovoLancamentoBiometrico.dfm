object frmNovoLancamentoBiometrico: TfrmNovoLancamentoBiometrico
  Left = 315
  Top = 207
  BorderStyle = bsDialog
  Caption = 'Sistema Deca - [Lan�amento de Dados Biom�tricos]'
  ClientHeight = 487
  ClientWidth = 693
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 0
    Width = 685
    Height = 49
    TabOrder = 0
    object Label9: TLabel
      Left = 14
      Top = 20
      Width = 52
      Height = 13
      Caption = 'Unidade:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbUnidade: TComboBox
      Left = 73
      Top = 16
      Width = 259
      Height = 22
      Style = csOwnerDrawFixed
      ItemHeight = 16
      TabOrder = 0
      OnClick = cbUnidadeClick
    end
  end
  object PageControl1: TPageControl
    Left = 4
    Top = 52
    Width = 685
    Height = 429
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Dados de Lan�amentos'
      object Bevel1: TBevel
        Left = 4
        Top = 153
        Width = 669
        Height = 191
      end
      object Bevel2: TBevel
        Left = 4
        Top = 146
        Width = 669
        Height = 3
      end
      object dbgCadastro: TDBGrid
        Left = 4
        Top = 5
        Width = 668
        Height = 136
        DataSource = dsCadastros
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = dbgCadastroCellClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'cod_matricula'
            Title.Alignment = taCenter
            Title.Caption = 'Matr�cula'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nom_nome'
            Title.Caption = 'Nome'
            Width = 290
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dat_nascimento'
            Title.Caption = 'Nascimento'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ind_sexo'
            Title.Caption = 'Sexo'
            Width = 70
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Val_Peso'
            Title.Alignment = taCenter
            Title.Caption = 'Peso'
            Width = 40
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Val_Altura'
            Title.Alignment = taCenter
            Title.Caption = 'Altura'
            Width = 40
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ind_tipo_sanguineo'
            Title.Alignment = taCenter
            Title.Caption = 'Tipo Sangu�neo'
            Width = 80
            Visible = True
          end>
      end
      object GroupBox2: TGroupBox
        Left = 13
        Top = 160
        Width = 86
        Height = 44
        Caption = 'Matr�cula'
        TabOrder = 1
        object lbMatricula: TLabel
          Left = 9
          Top = 17
          Width = 65
          Height = 16
          Caption = '00000000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object GroupBox3: TGroupBox
        Left = 101
        Top = 160
        Width = 285
        Height = 44
        Caption = 'Nome'
        TabOrder = 2
        object lbNome: TLabel
          Left = 9
          Top = 17
          Width = 266
          Height = 16
          AutoSize = False
          Caption = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 388
        Top = 160
        Width = 276
        Height = 44
        Caption = 'Data de Nascimento / Idade Atual'
        TabOrder = 3
        object lbNascimento: TLabel
          Left = 9
          Top = 17
          Width = 79
          Height = 16
          Caption = '00/00/0000 '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbIdade: TLabel
          Left = 222
          Top = 16
          Width = 17
          Height = 16
          Caption = '00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edIdade: TEdit
          Left = 117
          Top = 15
          Width = 32
          Height = 21
          BorderStyle = bsNone
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          MaxLength = 2
          ParentFont = False
          TabOrder = 0
        end
      end
      object GroupBox5: TGroupBox
        Left = 227
        Top = 205
        Width = 86
        Height = 44
        Caption = 'Peso'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        object edPeso: TEdit
          Left = 8
          Top = 15
          Width = 69
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
      object GroupBox6: TGroupBox
        Left = 315
        Top = 205
        Width = 86
        Height = 44
        Caption = 'Altura'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        object edAltura: TEdit
          Left = 8
          Top = 15
          Width = 69
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
      object GroupBox7: TGroupBox
        Left = 403
        Top = 205
        Width = 80
        Height = 44
        Caption = 'Valor do IMC'
        TabOrder = 6
        object lbIMC: TLabel
          Left = 9
          Top = 17
          Width = 37
          Height = 16
          Caption = '00,00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object GroupBox8: TGroupBox
        Left = 485
        Top = 205
        Width = 179
        Height = 44
        Caption = 'Classifica��o do IMC'
        TabOrder = 7
        object lbClassificacao: TLabel
          Left = 9
          Top = 17
          Width = 163
          Height = 16
          Caption = 'XXXXXXXXXXXXXXXXXX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object GroupBox9: TGroupBox
        Left = 13
        Top = 205
        Width = 211
        Height = 44
        Caption = 'Sexo'
        TabOrder = 8
        object lbSexo: TLabel
          Left = 9
          Top = 17
          Width = 190
          Height = 16
          Caption = 'XXXXXXXXXXXXXXXXXXXXX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object Panel1: TPanel
        Left = 4
        Top = 348
        Width = 668
        Height = 49
        TabOrder = 9
        object btnCalculaImc: TSpeedButton
          Left = 6
          Top = 6
          Width = 173
          Height = 37
          Caption = 'Calcular IMC'
          Flat = True
          Glyph.Data = {
            8A010000424D8A01000000000000760000002800000018000000170000000100
            04000000000014010000130B0000130B00001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            8888888888888888888888888888888888888888877777777777777777888880
            0000000000000000078888800000000000000000088888888822278444781117
            8888888188222784447811178888888188222784447811178888888888222784
            4478111788888881882227844478111788888881882227844478111788888888
            8822288444781117888888818888888444781117888888818888888444781118
            8888888888888884447811188888888188888884447888888888888188888884
            4488888888888888888888844488888888888881888888888888888888888881
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888888}
          OnClick = btnCalculaImcClick
        end
        object btnNovo: TBitBtn
          Left = 215
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para NOVA advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnNovoClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777770000000000777700007777
            778777777770777700007777778FFFFFFF70777700007777778FFFFFFF707777
            00007777778FFFFFFF70777700007777778FFFFFFF70777700007777778FFFFF
            FF70777700007777778FFFFFFF7077770000777F778FFFFFFF70777700007778
            F88FF8FF00007777000077778B8F8FFF7F8777770000777FF8FBFFFF78777777
            0000777788BFF88887777777000077778B8B8F777777777700007778F78F7777
            777777770000777F778F778F7777777700007777778F77777777777700007777
            77777777777777770000}
        end
        object btnSalvar: TBitBtn
          Left = 282
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para GRAVAR a advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnSalvarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777000000000000077700007770
            4407447770440777000077704407447770440777000077704407447770440777
            0000777044477777744407770000777044444444444407770000777044000000
            004407770000777040FFFFFFFF0407770000777040FFFFFFFF04077700007770
            40F888888F0407770000777040FFFFFFFF0407770000777070F888888F000777
            0000777040FFFFFFFF0407770000777000000000000007770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnCancelar: TBitBtn
          Left = 406
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para CANCELAR opera��o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnCancelarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            7777777777777777000077777777777777777777000077777777777777777777
            000077777777777777F7777700007777FFFFFF77784F77770000777844444F77
            7848F777000077784444F7777784F77700007778444F77777784F77700007778
            4484FF7777F4F77700007778487844FFFF487777000077788777884444877777
            0000777777777788887777770000777777777777777777770000777777777777
            7777777700007777777777777777777700007777777777777777777700007777
            77777777777777770000}
        end
        object btnImprimir: TBitBtn
          Left = 467
          Top = 10
          Width = 132
          Height = 30
          Hint = 'Clique para IMPRIMIR Gr�fico com as Medi��es Biom�trico'
          Caption = 'Op��es de Relat�rio'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnImprimirClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F0000000120B0000120B00001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777776B00777777777000777777776E007777777007880077777771007777
            7007780088007777740077770778878800880077770077778887778888008077
            7A00777887777788888800777D007778F7777F888888880780007778F77FF777
            8888880783007778FFF779977788880786007778F77AA7778807880789007777
            88F77788FF0700778C0077777788F8FFFFF077778F007777777788FFFFFF0777
            920077777777778FFFFFF0079500777777777778FFF887779800777777777777
            888777779B00777777777777777777779E0077777777777777777777A1007777
            7777777777777777A400}
        end
        object btnSair: TBitBtn
          Left = 604
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para SAIR da tela de advert�ncia'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnSairClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
            F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
            000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
            338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
            45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
            3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
            F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
            000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
            338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
            4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
            8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
            333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
            0000}
          NumGlyphs = 2
        end
        object btnAlterar: TBitBtn
          Left = 344
          Top = 10
          Width = 55
          Height = 30
          Hint = 'Clique para ALTERAR opera��o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
        end
      end
      object GroupBox10: TGroupBox
        Left = 13
        Top = 250
        Width = 142
        Height = 44
        Caption = 'Data da Avalia��o'
        TabOrder = 10
        object meDatAvaliacao: TMaskEdit
          Left = 8
          Top = 16
          Width = 89
          Height = 21
          EditMask = '99/99/9999'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
      end
      object GroupBox11: TGroupBox
        Left = 13
        Top = 295
        Width = 142
        Height = 44
        TabOrder = 11
        object chkProblemaSaude: TCheckBox
          Left = 8
          Top = 16
          Width = 120
          Height = 17
          Caption = 'Problemas de sa�de?'
          TabOrder = 0
        end
      end
      object GroupBox12: TGroupBox
        Left = 158
        Top = 250
        Width = 252
        Height = 89
        Caption = 'Descri��o do(s) problema(s) de sa�de...'
        TabOrder = 12
        object meProblemaSaude: TMemo
          Left = 8
          Top = 16
          Width = 234
          Height = 65
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object GroupBox13: TGroupBox
        Left = 412
        Top = 250
        Width = 252
        Height = 89
        Caption = 'Observa��es sobre a avalia��o...'
        TabOrder = 13
        object meObservacoes: TMemo
          Left = 8
          Top = 16
          Width = 234
          Height = 65
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Hist�rico de Lan�amentos'
      ImageIndex = 1
      object Bevel3: TBevel
        Left = 8
        Top = 271
        Width = 658
        Height = 6
      end
      object dbgBiometricos: TDBGrid
        Left = 8
        Top = 55
        Width = 658
        Height = 207
        DataSource = dsBiometricos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = dbgBiometricosCellClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'cod_matricula'
            Title.Alignment = taCenter
            Title.Caption = 'Matr�cula'
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'vPeso'
            Title.Alignment = taCenter
            Title.Caption = 'Peso'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'vAltura'
            Title.Alignment = taCenter
            Title.Caption = 'Altura'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'val_imc'
            Title.Alignment = taCenter
            Title.Caption = 'IMC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dsc_classificacao_imc'
            Title.Caption = 'Classifica��o IMC'
            Width = 135
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ProblemasSaude'
            Title.Alignment = taCenter
            Title.Caption = 'Problemas Sa�de?'
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nom_usuario'
            Title.Caption = 'Usu�rio'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'log_data'
            Title.Caption = 'Lan�ado em...'
            Width = 100
            Visible = True
          end>
      end
      object GroupBox14: TGroupBox
        Left = 9
        Top = 287
        Width = 326
        Height = 105
        Caption = 'Problemas de Sa�de'
        TabOrder = 1
        object meProblemasSaude: TMemo
          Left = 8
          Top = 15
          Width = 308
          Height = 81
          Enabled = False
          ReadOnly = True
          TabOrder = 0
        end
      end
      object GroupBox15: TGroupBox
        Left = 341
        Top = 287
        Width = 326
        Height = 105
        Caption = 'Observa��es do Lan�amento'
        TabOrder = 2
        object meObservacoesLancamento: TMemo
          Left = 8
          Top = 15
          Width = 308
          Height = 81
          Enabled = False
          ReadOnly = True
          TabOrder = 0
        end
      end
      object Panel2: TPanel
        Left = 8
        Top = 7
        Width = 657
        Height = 42
        BevelInner = bvLowered
        TabOrder = 3
        object lbIdentificacao: TLabel
          Left = 16
          Top = 14
          Width = 9
          Height = 16
          Caption = '_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
  end
  object dsCadastros: TDataSource
    DataSet = dmDeca.cdsSel_Cadastro
    Left = 640
    Top = 108
  end
  object dsBiometricos: TDataSource
    DataSet = dmDeca.cdsSel_Biometricos
    Left = 640
    Top = 140
  end
end
