 unit uCadastroInscricoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, Menus, Db, ShellAPI,
  CheckLst;

type
  TfrmCadastroInscricoes = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    TabSheet4: TTabSheet;
    TabSheet8: TTabSheet;
    gpbINSCRICAO: TGroupBox;
    mskNUMERO_INSCRICAO: TMaskEdit;
    gpbINSCRICAO_DIGITO: TGroupBox;
    mskINSCRICAO_DIGITO: TMaskEdit;
    gpbDATA_INSCRICAO: TGroupBox;
    mskDATA_INSCRICAO: TMaskEdit;
    gpbDATA_RENOVACAO: TGroupBox;
    mskDATA_RENOVACAO: TMaskEdit;
    gpbDATA_MATRICULA: TGroupBox;
    mskDATA_MATRICULA: TMaskEdit;
    gpbDATA_READMISSAO: TGroupBox;
    mskDATA_READMISSAO: TMaskEdit;
    gpbNUMERO_PORTARIA: TGroupBox;
    cmbNUMERO_PORTARIA: TComboBox;
    gpbSITUACAO_INSCRICAO: TGroupBox;
    cmbSITUACAO_INSCRICAO: TComboBox;
    Bevel1: TBevel;
    PageControl4: TPageControl;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    gpbNOME_INSCRITO: TGroupBox;
    gpbDATA_NASCIMENTO: TGroupBox;
    edtNOME_COMPLETO_INSCRITO: TEdit;
    mskDATA_NASCIMENTO_INSCRITO: TMaskEdit;
    lblIDADE: TLabel;
    Panel2: TPanel;
    lblINSCRICAO_NomeInscrito: TLabel;
    gpbESTADO_CIVIL_INSCRITO: TGroupBox;
    cmbESTADO_CIVIL_INSCRITO: TComboBox;
    gpbSEXO_INSCRITO: TGroupBox;
    cmbSEXO_INSCRITO: TComboBox;
    gpbNATURALIDADE_INSCRITO: TGroupBox;
    SpeedButton2: TSpeedButton;
    cmbNATURALIDADE_INSCRITO: TComboBox;
    gpbNACIONALIDADE_INSCRITO: TGroupBox;
    cmbNACIONALIDADE_INSCRITO: TComboBox;
    gpbENDERECO_INSCRITO: TGroupBox;
    edtENDERECO_INSCRITO: TEdit;
    gpbNUMERO_RESIDENCIA_INSCRITO: TGroupBox;
    edtNUMERO_RESIDENCIA_INSCRITO: TEdit;
    gpbREGIAO_INSCRITO: TGroupBox;
    cmbREGIAO: TComboBox;
    gpbCOMPLEMENTO_ENDERECO_INSCRITO: TGroupBox;
    edtCOMPLEMENTO_ENDERECO_INSCRITO: TEdit;
    gpbCEP_ENDERECO_INSCRITO: TGroupBox;
    mskCEP_RESIDENCIAL_INSCRITO: TMaskEdit;
    SpeedButton4: TSpeedButton;
    gpbBAIRRO_RESIDENCIAL_INSCRITO: TGroupBox;
    SpeedButton5: TSpeedButton;
    cmbBAIRRO_INSCRITO: TComboBox;
    SpeedButton6: TSpeedButton;
    gpbPONTO_REFERENCIA: TGroupBox;
    edtPONTO_REFERENCIA_RESIDENCIA: TEdit;
    gpbRESIDE_FAMILIA: TGroupBox;
    cmbRESIDE_FAMILIA: TComboBox;
    gpbTEMPO_RESIDENCIA: TGroupBox;
    edtTEMPO_RESIDENCIA: TEdit;
    gpbGRAU_PARENTESCO: TGroupBox;
    edtN_RESIDE_FAM: TEdit;
    gpbNUMERO_CARTAO_SUS_INSCRITO: TGroupBox;
    edtNUMERO_CARTAO_SUS_INSCRITO: TEdit;
    gpbNUMERO_CRA_INSCRITO: TGroupBox;
    edtNUMERO_CRA_INSCRITO: TEdit;
    gpbNUMERO_NIS_INSCRITO: TGroupBox;
    edtNUMERO_NIS_INSCRITO: TEdit;
    gpbNUMERO_CARTAO_PASSE_INSCRITO: TGroupBox;
    edtNUMERO_CARTAO_PASSE_INSCRITO: TEdit;
    edtCERTIDAO_NASCIMENTO_INSCRITO: TEdit;
    gpbNUMERO_CPF_INSCRITO: TGroupBox;
    mskNUMERO_CPF_INSCRITO: TMaskEdit;
    gpbDADOS_RG_INSCRITO: TGroupBox;
    edtNUMERO_RG_INSCRITO: TEdit;
    mskDATA_EMISSAO_RG_INSCRITO: TMaskEdit;
    cmbORGAO_EMISSOR_RG_INSCRITO: TComboBox;
    SpeedButton7: TSpeedButton;
    gpbUNIDADE_REFERENCIA_PARA_INSCRITO: TGroupBox;
    cmbUNIDADE_REFERENCIA_PARA_INSCRITO: TComboBox;
    pgcAbas_ComposicaoFamiliar: TPageControl;
    TabSheet11: TTabSheet;
    GroupBox42: TGroupBox;
    edtNOME_COMPLETO_FAMILIAR: TEdit;
    GroupBox43: TGroupBox;
    cmbSEXO_FAMILIAR: TComboBox;
    GroupBox44: TGroupBox;
    cmbESTADO_CIVIL_FAMILIAR: TComboBox;
    GroupBox46: TGroupBox;
    cmbGRAU_PARENTESCO_FAMILIAR: TComboBox;
    GroupBox45: TGroupBox;
    lblIDADE_Familiar: TLabel;
    mskDATA_NASCIMENTO_FAMILIAR: TMaskEdit;
    GroupBox47: TGroupBox;
    edtLOCAL_TRABALHO_FAMILIAR: TEdit;
    GroupBox48: TGroupBox;
    edtOCUPACAO_FAMILIAR: TEdit;
    GroupBox49: TGroupBox;
    mskTELEFONE_FAMILIAR: TMaskEdit;
    GroupBox50: TGroupBox;
    edtOUTRAS_RENDAS_FAMILIAR: TEdit;
    GroupBox51: TGroupBox;
    edtRENDA_MENSAL_FAMILIAR: TEdit;
    GroupBox53: TGroupBox;
    cmbNIVEL_ESCOLARIDADE_FAMILIAR: TComboBox;
    chkFAMILIAR_RESPONSAVEL: TCheckBox;
    GroupBox54: TGroupBox;
    cmbFAMILIAR_POSSUI_GUARDA: TComboBox;
    GroupBox55: TGroupBox;
    cmbFAMILIAR_MORA_FAMILIA: TComboBox;
    TabSheet12: TTabSheet;
    GroupBox65: TGroupBox;
    mskNUMERO_CPF_FAMILIAR: TMaskEdit;
    GroupBox66: TGroupBox;
    edtNUMERO_CARTAO_PASSE_FAMILIAR: TEdit;
    GroupBox67: TGroupBox;
    edtNUMERO_RG_FAMILIAR: TEdit;
    mskDATA_EMISSAO_RG_FAMILIAR: TMaskEdit;
    GroupBox68: TGroupBox;
    edtNUMERO_CARTAO_SUS_FAMILIAR: TEdit;
    GroupBox69: TGroupBox;
    edtNUMERO_CRA_FAMILIAR: TEdit;
    GroupBox70: TGroupBox;
    edtNUMERO_NIS_FAMILIAR: TEdit;
    GroupBox71: TGroupBox;
    cmbNATURALIDADE_FAMILIAR: TComboBox;
    GroupBox72: TGroupBox;
    cmbNACIONALIDADE_FAMILIAR: TComboBox;
    TabSheet5: TTabSheet;
    gpbINVALLIDEZ_INSCRITO: TGroupBox;
    chkINVALIDEZ_TEMPORARIA_INSCRITO: TCheckBox;
    chkINVALIDEZ_PERMANENTE_INSCRITO: TCheckBox;
    gpbDIAGNOSTICO_SAUDE_INSCRITO: TGroupBox;
    edtDIAGNOSTICO_SAUDE_INSCRITO: TEdit;
    gpbPORTADOR_DOENCA_INFECTO_INSCRITO: TGroupBox;
    chkPORTADOR_DOENCA_INFECTO_INSCRITO: TCheckBox;
    gpbINCAPACIDADE_TRABALHO_INSCRITO: TGroupBox;
    chkINCAPACIDADE_TRABALHO_INSCRITO: TCheckBox;
    gpbDESPESAS_MEDICAS_INSCRITO: TGroupBox;
    edtVALOR_DESPESAS_MEDICAS_INSCRITO: TEdit;
    chkCOMPROVOU_DESPESAS_MEDICAS: TCheckBox;
    gpbMOTIVO_PROCURA_INSCRITO: TGroupBox;
    chkMOTIVO_PROCURA_TRABALHO: TCheckBox;
    chkMOTIVO_PROCURA_CURSOS: TCheckBox;
    chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO: TCheckBox;
    chkMOTIVO_PROCURA_TIRAR_RUA: TCheckBox;
    chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS: TCheckBox;
    chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS: TCheckBox;
    chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ: TCheckBox;
    chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS: TCheckBox;
    chkMOTIVO_PROCURA_OUTROS: TCheckBox;
    gpbDATA_RELATORIO: TGroupBox;
    gpbENTREVISTADOR: TGroupBox;
    gpbTIPO_RELATORIO: TGroupBox;
    Bevel2: TBevel;
    gpbDESCRICAO_RELATORIO: TGroupBox;
    mskDATA_RELATORIO: TMaskEdit;
    edtNOME_ENTREVISTADOR: TEdit;
    cmbTIPO_RELATORIO: TComboBox;
    medDESCRICAO_RELATORIO: TMemo;
    TabSheet15: TTabSheet;
    gpbPONTUACAO: TGroupBox;
    gpbRENDA_PERCAPTA: TGroupBox;
    TabSheet14: TTabSheet;
    gpbTELEFONES_E_CONTATOS_INSCRITO: TGroupBox;
    medMOTIVO_PROCURA_OUTROS: TMemo;
    TabSheet17: TTabSheet;
    rdgESTA_MATRICULADO: TRadioGroup;
    gpbMOTIVO_NAO_ESTUDA: TGroupBox;
    edtMOTIVO_NAO_ESTUDA: TEdit;
    gpbESCOLA_ATUAL_INSCRITO: TGroupBox;
    cmbESCOLA_ATUAL_INSCRITO: TComboBox;
    gpbPERIODO_ESCOLAR_INSCRITO: TGroupBox;
    cmbPERIODO_ESCOLAR_INSCRITO: TComboBox;
    gpbNUMERO_RA_INSCRITO: TGroupBox;
    edtNUMERO_RA_INSCRITO: TEdit;
    gpbSERIE_ATUAL_INSCRITO: TGroupBox;
    cmbSERIE_ATUAL_INSCRITO: TComboBox;
    GroupBox104: TGroupBox;
    lblINSCRICAO_NomeEntrevistador: TLabel;
    medTELEFONES_CONTATOS_INSCRITO: TMemo;
    lblTOTAL_CARACTERES: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    gpbRELACAO_IDADE_SERIE_INSCRITO: TGroupBox;
    cmbRELACAO_IDADE_SERIE_INSCRITO: TComboBox;
    TabSheet3: TTabSheet;
    GroupBox56: TGroupBox;
    chkINVALIDEZ_TEMPORARIA_FAMILIAR: TCheckBox;
    chkINVALIDEZ_PERMANENTE_FAMILIAR: TCheckBox;
    GroupBox57: TGroupBox;
    chkINCAPACIDADE_TRABALHO_FAMILIAR: TCheckBox;
    GroupBox58: TGroupBox;
    chkPORTADOR_DOENCA_INFECTO_FAMILIAR: TCheckBox;
    GroupBox59: TGroupBox;
    chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE: TCheckBox;
    GroupBox60: TGroupBox;
    chkFAMILIAR_FALECIDO: TCheckBox;
    GroupBox61: TGroupBox;
    GroupBox64: TGroupBox;
    edtDIAGNOSTICO_SAUDE_FAMILIAR: TEdit;
    GroupBox52: TGroupBox;
    edtRELACIONAMENTO_FAMILIAR: TEdit;
    Panel5: TPanel;
    dbgCOMPOSICAO_FAMILIAR: TDBGrid;
    Panel6: TPanel;
    Label10: TLabel;
    gpbPONTUACAO_FINAL: TGroupBox;
    GroupBox116: TGroupBox;
    edtCERTIDAO_NASCIMENTO_FAMILIAR: TEdit;
    gpbNUMERO_CARTAO_SIAS_INSCRITO: TGroupBox;
    edtNUMERO_CARTAO_SIAS_INSCRITO: TEdit;
    chkFAMILIAR_RECLUSO: TCheckBox;
    gpbCERTIDAO_NASCIMENTO_INSCRITO: TGroupBox;
    gpbRAZAO_INCOMPATIBILIDADE_DADE_SERIE: TGroupBox;
    medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE: TMemo;
    chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR: TCheckBox;
    gpbDATA_ATUALIZACAO_ESCOLA: TGroupBox;
    mskDATA_ATUALIZACAO_ESCOLA: TMaskEdit;
    Label2: TLabel;
    PanelX: TPanel;
    btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS: TSpeedButton;
    Label5: TLabel;
    TabSheet18: TTabSheet;
    TabSheet19: TTabSheet;
    TabSheet20: TTabSheet;
    gpbBENEFICIOS: TGroupBox;
    gpbBENEFICIO: TGroupBox;
    edtDESCRICAO_BENEFICIO: TEdit;
    gpbVALOR_BENEFICIO: TGroupBox;
    edtVALOR_BENEFICIO: TEdit;
    dbgBENEFICIOS_SOCIAIS: TDBGrid;
    gpbDATA_INICIO_BENEFICIO: TGroupBox;
    mskDATA_INICIO_BENEFICIO: TMaskEdit;
    gpbBENS_COMPLEMENTARES: TGroupBox;
    chkIMOVEL_OUTRO: TCheckBox;
    chkPOSSUI_CARRO: TCheckBox;
    GroupBox84: TGroupBox;
    edtCARRO_MODELO: TEdit;
    edtCARRO_ANO: TEdit;
    chkPOSSUI_MOTO: TCheckBox;
    GroupBox85: TGroupBox;
    edtMOTO_MODELO: TEdit;
    edtMOTO_ANO: TEdit;
    gpbUTILIZACAO_VEICULO: TGroupBox;
    chkUSO_VEICULO_OUTRAS_PRIORIDADES: TCheckBox;
    chkUSO_VEICULO_TRABALHO: TCheckBox;
    chkUSO_VEICULO_UNICO_MEIO_TRANSP: TCheckBox;
    gpbRELACAO_RESPONSABILIDADE: TGroupBox;
    cmbRELACAO_RESPONSABILIDADE: TComboBox;
    gpbAGRAVANTES_CONDUTA: TGroupBox;
    chkAGRAVANTE_DROGADICTO: TCheckBox;
    chkAGRAVANTE_ALCOOLATRA: TCheckBox;
    chkAGRAVANTE_PAIMAE_ADOLESCENTE: TCheckBox;
    chkAGRAVANTE_GESTANTE: TCheckBox;
    chkAGRAVANTE_USO_PSICOTROPICOS: TCheckBox;
    chkAGRAVANTE_MEDIDAS_SOCIOEDUCATIVAS: TCheckBox;
    gpbSITUACOES_RISCO: TGroupBox;
    chkSIT_RISCO_CREAS: TCheckBox;
    chkSIT_RISCO_VIOLENCIA: TCheckBox;
    chkSIT_RISCO_VIOLENCIA_FISICA: TCheckBox;
    chkSIT_RISCO_VIOLENCIA_PSICOLOGICA: TCheckBox;
    chkSIT_RISCO_VIOLENCIA_SEXUAL: TCheckBox;
    chkSIT_RISCO_VIOLENCIA_NEGLIGENCIA: TCheckBox;
    chkSIT_RISCO_VIOLENCIA_ABANDONO: TCheckBox;
    chkSIT_RISCO_PROSTITUICAO: TCheckBox;
    chkSIT_RISCO_TRAFICO_DROGAS: TCheckBox;
    chkSIT_RISCO_CONVIVE_DROGADICCAO: TCheckBox;
    chkSIT_RISCO_CONVIVE_ALCOOLATRA: TCheckBox;
    chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_MENTAIS: TCheckBox;
    chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INFECTO: TCheckBox;
    chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INCAPACITANTES: TCheckBox;
    chkSIT_RISCO_MEMBRO_ATOS_INFRACIONAIS: TCheckBox;
    chkSIT_RISCO_FICAM_SOZINHAS: TCheckBox;
    gpbNATUREZA_HABITACAO: TGroupBox;
    cmbNATUREZA_HABITACAO: TComboBox;
    gpbCOMPROVA_ALUGUEL: TGroupBox;
    edtVALOR_ALUGUEL_INFORMADO: TEdit;
    chkCOMPROVOU_ALUGUEL: TCheckBox;
    gpbOBSERVACAO: TGroupBox;
    edtOBSERVACOES: TEdit;
    gpbTIPO_HABITACAO: TGroupBox;
    cmbTIPO_HABITACAO: TComboBox;
    gpbINFRAESTRUTURA: TGroupBox;
    cmbINFRAESTRUTURA: TComboBox;
    gpbCONDICOES_HABITABILIDADE: TGroupBox;
    cmbCONDICOES_HABITABILIDADE: TComboBox;
    gpbINSTALACOES_SANITARIAS: TGroupBox;
    cmbINSTALACOES_SANITARIAS: TComboBox;
    gpbHABITANTES_COMODOS: TGroupBox;
    Label3: TLabel;
    edtQTD_HABITANTES: TEdit;
    edtQTD_COMODOS: TEdit;
    Panel4: TPanel;
    btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS: TSpeedButton;
    Label6: TLabel;
    dsSel_BeneficiosSociais: TDataSource;
    btnNOVO_BENEFICIO: TSpeedButton;
    btnSALVA_BENEFICIO: TSpeedButton;
    btnALTERA_BENEFICIO: TSpeedButton;
    btnCANCELA: TSpeedButton;
    btnEXCLUI_BENEFICIO: TSpeedButton;
    btnACESSO_CONSULTASUS: TSpeedButton;
    dsCOMPOSICAO_FAMILIAR: TDataSource;
    cmbORGAO_EMISSOR_RG_FAMILIAR: TComboBox;
    gpbRELATORIOS_GRAVADOS: TGroupBox;
    dbgRELATORIOS_GRAVADOS: TDBGrid;
    Panel7: TPanel;
    btnNOVO_RELATORIO: TSpeedButton;
    btnGRAVAR_RELATORIO: TSpeedButton;
    btnEDITAR_RELATORIO: TSpeedButton;
    btnCANCELAROPERACAO_RELATORIO: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    btnSAIR_RELATORIO: TSpeedButton;
    dsSel_Cadastro_Relatorios: TDataSource;
    btnACESSO_CONSULTASUS_FAMILIAR: TSpeedButton;
    gpbPENDENCIAS: TGroupBox;
    gpbDADOS_PENDENCIAS: TGroupBox;
    dbgPendencias: TDBGrid;
    Panel8: TPanel;
    btnNOVA_PENDENCIA: TSpeedButton;
    btnGRAVAR_PENDENCIA: TSpeedButton;
    btnEDITAR_PENDENCIA: TSpeedButton;
    btnCANCELA_PENDENCIA: TSpeedButton;
    btnPENDENCIA_CHAMAPESQUISA: TSpeedButton;
    btnVISUALIZAR_DOCPENDENCIAS: TSpeedButton;
    btnSAIR_INSCRICAO_PENDENCIA: TSpeedButton;
    chk_PENDENCIA_CARTPROF: TCheckBox;
    chk_PENDENCIA_COMPROVRENDA: TCheckBox;
    chk_PENDENCIA_COMPROVALUGUEL: TCheckBox;
    chk_PENDENCIA_SUS: TCheckBox;
    chk_PENDENCIA_DECLARAESCOLA: TCheckBox;
    chk_PENDENCIA_OUTROS: TCheckBox;
    med_PENDENCIA_OUTROS: TMemo;
    Label1: TLabel;
    mskDATA_ULTIMALATERACAO: TMaskEdit;
    edtPENDENCIA_DETALHES_ESCOLA: TEdit;
    edtPENDENCIAS_DETALHE_SUS: TEdit;
    Panel3: TPanel;
    Panel10: TPanel;
    panRENDA_TOTAL: TPanel;
    Panel14: TPanel;
    panNUMERO_RESIDENTES: TPanel;
    panRENDA_PERCAPTA: TPanel;
    Panel13: TPanel;
    panSITUACAO_SOCIAL: TPanel;
    Panel16: TPanel;
    panCONDICAO_SAUDE: TPanel;
    Panel18: TPanel;
    panTEMPO_RESIDENCIA: TPanel;
    Panel20: TPanel;
    panSITUACAO_MORADIA: TPanel;
    Panel22: TPanel;
    panBENS: TPanel;
    Panel24: TPanel;
    panPONTUACAO_TOTAL: TPanel;
    btnCALCULA_PONTOS: TSpeedButton;
    panRENDA_PORHABITANTE: TPanel;
    Panel11: TPanel;
    chk_PENDENCIA_NIS: TCheckBox;
    edtPENDENCIA_DETALHES_NIS: TEdit;
    chk_PENDENCIA_CPF: TCheckBox;
    edtPENDENCIAS_DETALHE_CPF: TEdit;
    chk_PENDENCIA_COMPROVTEMPORESID: TCheckBox;
    dsPENDENCIAS: TDataSource;
    btnFINALIZAR_PENDENCIAS: TSpeedButton;
    btnCOPIAR_FICHA: TSpeedButton;
    btnEXCLUIR_FAMILIAR: TSpeedButton;
    Panel9: TPanel;
    panDESPESAS: TPanel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure ModoTela(Modo: String);
    procedure LimpaCampos;
    procedure DesabilitarGroupBox;
    procedure PageControl2Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
      Sender: TObject);
    procedure btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
      Sender: TObject);
    procedure HabilitaGroupBox;
    procedure btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
      Sender: TObject);
    function RemoverAcentos(S: String): String;
    procedure edtNOME_COMPLETO_INSCRITOExit(Sender: TObject);
    procedure edtENDERECO_INSCRITOExit(Sender: TObject);
    procedure edtCOMPLEMENTO_ENDERECO_INSCRITOExit(Sender: TObject);
    procedure edtPONTO_REFERENCIA_RESIDENCIAExit(Sender: TObject);
    procedure edtN_RESIDE_FAMExit(Sender: TObject);
    procedure edtCERTIDAO_NASCIMENTO_INSCRITOExit(Sender: TObject);
    procedure edtMOTIVO_NAO_ESTUDAExit(Sender: TObject);
    procedure rdgESTA_MATRICULADOClick(Sender: TObject);
    procedure TabSheet6Show(Sender: TObject);
    procedure medTELEFONES_CONTATOS_INSCRITOExit(Sender: TObject);
    procedure medRAZAO_INCOMPATIBILIDADE_IDADE_SERIEExit(Sender: TObject);
    procedure edtDIAGNOSTICO_SAUDE_INSCRITOExit(Sender: TObject);
    procedure medMOTIVO_PROCURA_OUTROSExit(Sender: TObject);
    procedure edtDESCRICAO_BENEFICIOExit(Sender: TObject);
    procedure edtCARRO_MODELOExit(Sender: TObject);
    procedure edtMOTO_MODELOExit(Sender: TObject);
    procedure edtNOME_COMPLETO_FAMILIARExit(Sender: TObject);
    procedure edtLOCAL_TRABALHO_FAMILIARExit(Sender: TObject);
    procedure edtOCUPACAO_FAMILIARExit(Sender: TObject);
    procedure edtOUTRAS_RENDAS_FAMILIARExit(Sender: TObject);
    procedure edtCERTIDAO_NASCIMENTO_FAMILIARExit(Sender: TObject);
    procedure edtDIAGNOSTICO_SAUDE_FAMILIARExit(Sender: TObject);
    procedure edtRELACIONAMENTO_FAMILIARExit(Sender: TObject);
    procedure btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
      Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure medTELEFONES_CONTATOS_INSCRITOKeyPress(Sender: TObject;
      var Key: Char);
    procedure btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
      Sender: TObject);
    procedure cmbSEXO_INSCRITOClick(Sender: TObject);
    procedure ModoTelaFamilia (Modo: String);
    procedure btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
      Sender: TObject);
    procedure btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
      Sender: TObject);
    procedure btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
      Sender: TObject);
    procedure LimparCombos;
    procedure ValidaCamposAdm;
    procedure ValidaCamposSS;
    procedure SpeedButton4Click(Sender: TObject);
    procedure cmbESCOLA_ATUAL_INSCRITOClick(Sender: TObject);
    procedure CalculaPontos;
    procedure mskDATA_NASCIMENTO_INSCRITOExit(Sender: TObject);
    procedure InserirDadosFicha;
    procedure AlterarDadosFicha;
    procedure btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
      Sender: TObject);
    procedure PageControl4Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure dbgBENEFICIOS_SOCIAISDblClick(Sender: TObject);
    procedure btnINCLUIR_BENEFICIOClick(Sender: TObject);
    procedure btnALTERAR_BENEFICIOClick(Sender: TObject);
    procedure btnNOVO_BENEFICIOClick(Sender: TObject);
    procedure btnSALVA_BENEFICIOClick(Sender: TObject);
    procedure btnALTERA_BENEFICIOClick(Sender: TObject);
    procedure btnCANCELAClick(Sender: TObject);
    procedure btnEXCLUI_BENEFICIOClick(Sender: TObject);
    procedure btnACESSO_CONSULTASUSClick(Sender: TObject);
    procedure cmbSEXO_INSCRITOExit(Sender: TObject);
    procedure btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
      Sender: TObject);
    procedure InserirDadosFamilia;
    procedure AlterarDadosFamilia;
    procedure ValidaCamposFamilia;
    procedure CarregaDadosInscritos;
    procedure CarregaDadosFamilia;
    procedure LimpaCamposFamilia;
    procedure CarregaAbasADM;
    procedure CarregaAbasSS;
    procedure btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
      Sender: TObject);
    procedure dbgCOMPOSICAO_FAMILIARCellClick(Column: TColumn);
    procedure ModoTelaRelatorios(Modo: String);
    procedure btnNOVO_RELATORIOClick(Sender: TObject);
    procedure btnCANCELAROPERACAO_RELATORIOClick(Sender: TObject);
    procedure CarregaDadosRelatorios;
    procedure dbgRELATORIOS_GRAVADOSCellClick(Column: TColumn);
    procedure btnGRAVAR_RELATORIOClick(Sender: TObject);
    procedure btnACESSO_CONSULTASUS_FAMILIARClick(Sender: TObject);
    procedure btnSAIR_RELATORIOClick(Sender: TObject);
    procedure btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
      Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure ModoTelaPendencias(Modo: String);
    procedure btnNOVA_PENDENCIAClick(Sender: TObject);
    procedure btnCANCELA_PENDENCIAClick(Sender: TObject);
    procedure btnCALCULA_PONTOSClick(Sender: TObject);
    procedure CarregaDadosPendenciaDocs;
    procedure btnGRAVAR_PENDENCIAClick(Sender: TObject);
    procedure VerificaSeTemPendenciaDocumento;
    procedure InserirDadosPendenciaDocs;
    procedure AlterarDadosPendencias;
    procedure dbgPendenciasCellClick(Column: TColumn);
    procedure btnFINALIZAR_PENDENCIASClick(Sender: TObject);
    procedure CarregaCombos;
    procedure mskDATA_NASCIMENTO_FAMILIARExit(Sender: TObject);
    procedure dbgCOMPOSICAO_FAMILIARKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgCOMPOSICAO_FAMILIARKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnEDITAR_RELATORIOClick(Sender: TObject);
    procedure TabSheet15Show(Sender: TObject);
    procedure TabSheet18Show(Sender: TObject);
    procedure btnEXCLUIR_FAMILIARClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroInscricoes: TfrmCadastroInscricoes;
  mmPODE_MUDAR_ABA, vOK, vOK_Adm, vvMODO_PESQUISA, vTemPendenciaDoc : Boolean;

  vTipo_Edicao, vvTipo_Edicao_Relatorio, vTipo_EdicaoPend,
  vvCOD_INSCRICAO                                                   : String;

  vListaEstadoCivil1, vListaEstadoCivil2                            : TStringList;
  vListaNaturalidade1, vListaNaturalidade2                          : TStringList;
  vListaNacionalidade1, vListaNacionalidade2                        : TStringList;
  vListaBairros1, vListaBairros2                                    : TStringList;
  vListaUnidade1, vListaUnidade2                                    : TStringList;
  vListaEmissor1, vListaEmissor2                                    : TStringList;
  vListaEscolaridade1, vListaEscolaridade2                          : TStringList;

  vListaRelacaoResponsabilidade1, vListaRelacaoResponsabilidade2    : TStringList;

  vListaNaturezaHabitacao1,vListaNaturezaHabitacao2                 : TStringList;
  vListaTipoHabitacao1,vListaTipoHabitacao2                         : TStringList;
  vListaInfraestrutura1,vListaInfraestrutura2                       : TStringList;
  vListaInstalacoesSantitarias1, vListaInstalacoesSantitarias2      : TStringList;
  vListaCondicaoesHabitabilidade1,vListaCondicaoesHabitabilidade2   : TStringList;
  vListaSituacao1, vListaSituacao2                                  : TStringList;
  vListaRegiao1, vListaRegiao2                                      : TStringList;
  vListaSerie1, vListaSerie2                                        : TStringList;
  vListaParentesco1, vListaParentesco2                              : TStringList;

  vIndEstCivIns, vIndex                                             : Integer;

implementation

uses uPrincipal, uDM, udmTriagemDeca, uUtil, uPESQUISA_INSCRITOS_TRIAGEM,
  uNavegador, uFichaCrianca;

{$R *.DFM}

procedure TfrmCadastroInscricoes.DesabilitarGroupBox;
var i : Integer;
begin
  for i := 0 to (ComponentCount) - 1 do
    if (Components[i] is TGroupBox) and NOT (Components[i].Name = 'gpbPONTUACAO_FINAL') then (Components[i] as TGroupBox).Enabled := False;

  rdgESTA_MATRICULADO.Enabled := False;
end;

procedure TfrmCadastroInscricoes.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) and not (ActiveControl is TMemo) then begin
    // desabilita processamento posterior da tecla
    Key := #0;
    Perform(WM_NEXTDLGCTL,0,0);  // tecla TAB
  end;
end;

procedure TfrmCadastroInscricoes.FormShow(Sender: TObject);
begin
  if vvMODO_PESQUISA = True then
  begin
    CarregaDadosInscritos;
    CarregaDadosFamilia;
    CarregaDadosRelatorios;
    CarregaDadosPendenciaDocs;
    lblINSCRICAO_NomeEntrevistador.Caption := vvNOM_USUARIO;
    gpbRENDA_PERCAPTA.Enabled              := True;
    gpbPONTUACAO.Enabled                   := True;
    gpbPONTUACAO_FINAL.Enabled             := True;
    vTipo_Edicao                           := '';

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_pontuacao').IsNull) then
      panPONTUACAO_TOTAL.Caption           := '0'
    else panPONTUACAO_TOTAL.Caption        := IntToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_pontuacao').Value);

    CalculaPontos;

  end
  else if vvMODO_PESQUISA = False then
  begin
    LimpaCampos;
    ModoTela('N');
    ModoTelaFamilia('P');
    ModoTelaRelatorios('P');

    HabilitaGroupBox;
    CarregaCombos;

    lblINSCRICAO_NomeEntrevistador.Caption := vvNOM_USUARIO;
    //edtNOME_COMPLETO_INSCRITO.SetFocus;
    mskDATA_INSCRICAO.Text                 := DateToStr(Date());
    cmbNUMERO_PORTARIA.ItemIndex           := 0;
    cmbSITUACAO_INSCRICAO.ItemIndex        := 15;     //PEND�NCIA DE INSCRI��O
    vTipo_Edicao                           := 'I';
    gpbBENEFICIOS.Enabled                  := False;
    mmPODE_MUDAR_ABA                       := True;
    dbgCOMPOSICAO_FAMILIAR.DataSource      := Nil;
    dbgRELATORIOS_GRAVADOS.DataSource      := Nil;

    try
       PageControl1.ActivePageIndex := 0;
       PageControl2.ActivePageIndex := 0;
       PageControl4.ActivePageIndex := 0;

       if (vvIND_PERFIL = 3) then  //Assistente Social
         CarregaAbasSS
       else if (vvIND_PERFIL <> 3) then    // Demais perfis
         CarregaAbasADM;
    except
       CarregaAbasADM;
    end;                
  end;
end;

procedure TfrmCadastroInscricoes.LimpaCampos;
var x: Integer;
begin
  for x := 0 to (ComponentCount) - 1 do
  begin
    //ShowMessage (IntToStr(Components[x].tag));
    //Verifica��o da propriedade pTag
    if (TComponent(Components[x]).Tag<>100) then
    begin
      if (Components[x] is TCustomEdit) then
        (Components[x] as TCustomEdit).Clear;
      if (Components[x] is TComboBox) then
      begin
        (Components[x] as TComboBox).ItemIndex := -1;
        (Components[x] as TComboBox).Text := EmptyStr;
      end;

      if (Components[x] is TCheckBox) then
        (Components[x] as TCheckBox).Checked := False;
    end;
  end;

end;

procedure TfrmCadastroInscricoes.ModoTela(Modo: String);
begin
  if Modo = 'P' then  //Modo PADR�O
  begin
    //LimpaCampos;
    DesabilitarGroupBox;

    gpbPONTUACAO.Enabled           := True;
    gpbRENDA_PERCAPTA.Enabled      := True;
    gpbPONTUACAO_FINAL.Enabled     := True;
    gpbRELATORIOS_GRAVADOS.Enabled := True;
    gpbBENEFICIOS.Enabled          := True;
    gpbRELATORIOS_GRAVADOS.Enabled := True;
    gpbBENEFICIOS.Enabled          := True;

    lblINSCRICAO_NomeEntrevistador.Caption := '';
    //Tratar os bot�es
    btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := True;
    btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := False;
    btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := True;
    btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled := False;
    btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := True;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := True;
    btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := True;
    mmPODE_MUDAR_ABA := True;
  end
  else if Modo = 'N' then
  begin
    HabilitaGroupBox;
    btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := False;
    btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := True;
    btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := False;
    btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled := True;
    btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := False;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := False;
    btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := False;
    mmPODE_MUDAR_ABA := False;
  end
  else if Modo = 'A' then
  begin
    HabilitaGroupBox;
    btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := False;
    btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := True;
    btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := False;
    btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled := True;
    btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := False;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := False;
    btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := False;
    mmPODE_MUDAR_ABA := False;
  end
  else if Modo = 'S' then
  begin
    btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := True;
    btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := False;
    btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled           := True;
    btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled := False;
    btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := True;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled   := True;
    btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAIS.Enabled             := True;
    mmPODE_MUDAR_ABA := True;
    DesabilitarGroupBox;
  end;
end;

procedure TfrmCadastroInscricoes.PageControl2Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  //if (mmPODE_MUDAR_ABA = False) or (vTipo_Edicao = 'I') then
  if (vTipo_Edicao = 'I') or (vTipo_Edicao = 'U') then
    AllowChange := False else AllowChange := True;
end;

procedure TfrmCadastroInscricoes.btnNOVA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
  Sender: TObject);
begin
  if (Application.MessageBox ('Deseja lan�ar dados para uma NOVA FICHA?',
                              '[Sistema Deca] - Confirma��o de Inclus�o',
                              MB_YESNO + MB_ICONQUESTION) = idYes) then
  begin
    LimpaCampos;
    ModoTela('N');
    edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text := '0,00';
    edtVALOR_ALUGUEL_INFORMADO.Text         := '0,00';
    lblINSCRICAO_NomeEntrevistador.Caption  := vvNOM_USUARIO;
    PageControl4.ActivePageIndex            := 0;
    edtNOME_COMPLETO_INSCRITO.SetFocus;
    mskDATA_INSCRICAO.Text                  := DateToStr(Date());
    cmbNUMERO_PORTARIA.ItemIndex            := 0;
    cmbSITUACAO_INSCRICAO.ItemIndex         := 15;  //PEND�NCIA DE INSCRI��O
    vTipo_Edicao                            := 'I';
    gpbBENEFICIOS.Enabled                   := False;
    mmPODE_MUDAR_ABA                        := True;
    dbgCOMPOSICAO_FAMILIAR.DataSource       := Nil;
    dbgRELATORIOS_GRAVADOS.DataSource       := Nil;

    edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text := '0';
    edtVALOR_ALUGUEL_INFORMADO.Text         := '0';
  end
  else
  begin
    frmCadastroInscricoes.ModoTela('P');
  end;

end;

procedure TfrmCadastroInscricoes.btnCANCELAROPERACAO_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
  Sender: TObject);
begin
  vOK_Adm := False;
  vOK     := False;
  vTipo_Edicao                 := '';
  mmPODE_MUDAR_ABA             := True;
  ModoTela('P');
  ModoTelaFamilia('P');
  ModoTelaRelatorios('P');
  gpbBENEFICIOS.Enabled        := True;
  gpbPONTUACAO.Enabled         := True;
end;

procedure TfrmCadastroInscricoes.HabilitaGroupBox;
var i : Integer;
begin
  for i := 0 to (ComponentCount) - 1 do
    if (Components[i] is TGroupBox) then (Components[i] as TGroupBox).Enabled := True;

  rdgESTA_MATRICULADO.Enabled := True;
end;

procedure TfrmCadastroInscricoes.btnSAIR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
  Sender: TObject);
begin
  frmCadastroInscricoes.Close;
end;

function TfrmCadastroInscricoes.RemoverAcentos(S: String): String;
const StrA = '��������������������������������������';
      StrB = 'aeiouaeiouaoaeioucuAEIOUAEIOUAEIOUAOCU';
var i, aPos: Integer;
begin
  for i:= 1 to Length(S) do
    begin
      aPos:= Pos(S[i],StrA);
      if aPos > 0 then S[i]:= StrB[aPos];
    end;
  Result:= S;
end;

procedure TfrmCadastroInscricoes.edtNOME_COMPLETO_INSCRITOExit(
  Sender: TObject);
begin
  edtNOME_COMPLETO_INSCRITO.Text    := RemoverAcentos(edtNOME_COMPLETO_INSCRITO.Text);
  lblINSCRICAO_NomeInscrito.Caption := edtNOME_COMPLETO_INSCRITO.Text;
end;

procedure TfrmCadastroInscricoes.edtENDERECO_INSCRITOExit(Sender: TObject);
begin
  edtENDERECO_INSCRITO.Text := RemoverAcentos(edtENDERECO_INSCRITO.Text);
end;

procedure TfrmCadastroInscricoes.edtCOMPLEMENTO_ENDERECO_INSCRITOExit(
  Sender: TObject);
begin
  edtCOMPLEMENTO_ENDERECO_INSCRITO.Text := RemoverAcentos(edtCOMPLEMENTO_ENDERECO_INSCRITO.Text);
end;

procedure TfrmCadastroInscricoes.edtPONTO_REFERENCIA_RESIDENCIAExit(
  Sender: TObject);
begin
  edtPONTO_REFERENCIA_RESIDENCIA.Text := RemoverAcentos(edtPONTO_REFERENCIA_RESIDENCIA.Text);
end;

procedure TfrmCadastroInscricoes.edtN_RESIDE_FAMExit(Sender: TObject);
begin
  edtN_RESIDE_FAM.Text := RemoverAcentos(edtN_RESIDE_FAM.Text);
end;

procedure TfrmCadastroInscricoes.edtCERTIDAO_NASCIMENTO_INSCRITOExit(
  Sender: TObject);
begin
  edtCERTIDAO_NASCIMENTO_INSCRITO.Text := RemoverAcentos(edtCERTIDAO_NASCIMENTO_INSCRITO.Text);
end;

procedure TfrmCadastroInscricoes.edtMOTIVO_NAO_ESTUDAExit(Sender: TObject);
begin
  edtMOTIVO_NAO_ESTUDA.Text := RemoverAcentos(edtMOTIVO_NAO_ESTUDA.Text);
end;

procedure TfrmCadastroInscricoes.rdgESTA_MATRICULADOClick(Sender: TObject);
begin
 {   //Desativado pois se estiver como N�O e no perfil <> 3(S.S.) apresenta erro de Setfocus
 If rdgESTA_MATRICULADO.ItemIndex = 0 then
    edtMOTIVO_NAO_ESTUDA.Enabled := False
  else
  begin
    edtMOTIVO_NAO_ESTUDA.Enabled := True;
    edtMOTIVO_NAO_ESTUDA.SetFocus;
  end;}
end;

procedure TfrmCadastroInscricoes.TabSheet6Show(Sender: TObject);
begin
  if cmbSEXO_INSCRITO.ItemIndex = 0 then  //Se MASCULINO n�o habilita op��o Gestante
    chkAGRAVANTE_GESTANTE.Enabled := False
  else
    chkAGRAVANTE_GESTANTE.Enabled := True;
end;

procedure TfrmCadastroInscricoes.medTELEFONES_CONTATOS_INSCRITOExit(
  Sender: TObject);
begin
  medTELEFONES_CONTATOS_INSCRITO.Text := RemoverAcentos(medTELEFONES_CONTATOS_INSCRITO.Text);
end;

procedure TfrmCadastroInscricoes.medRAZAO_INCOMPATIBILIDADE_IDADE_SERIEExit(
  Sender: TObject);
begin
  medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE.Text := RemoverAcentos(medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE.Text);
end;

procedure TfrmCadastroInscricoes.edtDIAGNOSTICO_SAUDE_INSCRITOExit(
  Sender: TObject);
begin
  edtDIAGNOSTICO_SAUDE_INSCRITO.Text := RemoverAcentos(edtDIAGNOSTICO_SAUDE_INSCRITO.Text);
end;

procedure TfrmCadastroInscricoes.medMOTIVO_PROCURA_OUTROSExit(
  Sender: TObject);
begin
  medMOTIVO_PROCURA_OUTROS.Text := RemoverAcentos(medMOTIVO_PROCURA_OUTROS.Text);
end;

procedure TfrmCadastroInscricoes.edtDESCRICAO_BENEFICIOExit(
  Sender: TObject);
begin
  edtDESCRICAO_BENEFICIO.Text := RemoverAcentos(edtDESCRICAO_BENEFICIO.Text);
end;

procedure TfrmCadastroInscricoes.edtCARRO_MODELOExit(Sender: TObject);
begin
  edtCARRO_MODELO.Text := RemoverAcentos(edtCARRO_MODELO.Text);
end;

procedure TfrmCadastroInscricoes.edtMOTO_MODELOExit(Sender: TObject);
begin
  edtMOTO_MODELO.Text := RemoverAcentos(edtMOTO_MODELO.Text);
end;

procedure TfrmCadastroInscricoes.edtNOME_COMPLETO_FAMILIARExit(
  Sender: TObject);
begin
  edtNOME_COMPLETO_FAMILIAR.Text := RemoverAcentos(edtNOME_COMPLETO_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.edtLOCAL_TRABALHO_FAMILIARExit(
  Sender: TObject);
begin
  edtLOCAL_TRABALHO_FAMILIAR.Text := RemoverAcentos(edtLOCAL_TRABALHO_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.edtOCUPACAO_FAMILIARExit(Sender: TObject);
begin
  edtOCUPACAO_FAMILIAR.Text := RemoverAcentos(edtOCUPACAO_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.edtOUTRAS_RENDAS_FAMILIARExit(
  Sender: TObject);
begin
  edtOUTRAS_RENDAS_FAMILIAR.Text := RemoverAcentos(edtOUTRAS_RENDAS_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.edtCERTIDAO_NASCIMENTO_FAMILIARExit(
  Sender: TObject);
begin
  edtCERTIDAO_NASCIMENTO_FAMILIAR.Text := RemoverAcentos(edtCERTIDAO_NASCIMENTO_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.edtDIAGNOSTICO_SAUDE_FAMILIARExit(
  Sender: TObject);
begin
  edtDIAGNOSTICO_SAUDE_FAMILIAR.Text := RemoverAcentos(edtDIAGNOSTICO_SAUDE_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.edtRELACIONAMENTO_FAMILIARExit(
  Sender: TObject);
begin
  edtRELACIONAMENTO_FAMILIAR.Text := RemoverAcentos(edtRELACIONAMENTO_FAMILIAR.Text);
end;

procedure TfrmCadastroInscricoes.btnGRAVAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
  Sender: TObject);
begin
  try
    //Verificar se TODOS os campos obrigat�rios foram preenchidos, de acordo com o perfil acessado

    //Verificar para o perfil diferente do Servi�o Social
    if (vvIND_PERFIL <> 3) then
    begin
      ValidaCamposAdm;
      if (vOK_Adm = True) then
      begin
        if (vTipo_Edicao = 'I') then InserirDadosFicha else if (vTipo_Edicao = 'U') then AlterarDadosFicha;
        mmPODE_MUDAR_ABA   := True;
        ModoTela('P');
        vTipo_Edicao := '';
        CalculaPontos;
      end
    end
    else if (vvIND_PERFIL = 3) then
    begin
      ValidaCamposSS;
      if (vOK = True) then
      begin
        if (vTipo_Edicao = 'I') then InserirDadosFicha else if (vTipo_Edicao = 'U') then AlterarDadosFicha;
        mmPODE_MUDAR_ABA   := True;
        ModoTela('P');
        vTipo_Edicao := '';
        CalculaPontos;
      end
    end;
  except
    Application.MessageBox ('Aten��o!!!' +#13+#10+
                            'Um erro ocorreu e os dados n�o foram salvos no banco de dados.' +#13+#10+
                            'Tente novamente ou entre em contato com o Suporte',
                            '[Sistema Deca] - Erro salvando dados da Ficha de Inscri��o',
                            MB_OK + MB_ICONERROR);
  end;

end;

procedure TfrmCadastroInscricoes.FormCreate(Sender: TObject);
begin
  medTELEFONES_CONTATOS_INSCRITO.MaxLength := 255;
  lblTOTAL_CARACTERES.Caption := 'Restam 255 caracteres.';
end;

procedure TfrmCadastroInscricoes.medTELEFONES_CONTATOS_INSCRITOKeyPress(
  Sender: TObject; var Key: Char);
var Max, Min, Total : integer;
begin
  Max                         := medTELEFONES_CONTATOS_INSCRITO.MaxLength; //Passando o valor m�ximo de caracteres do Memo para a vari�vel Max.
  Min                         := medTELEFONES_CONTATOS_INSCRITO.GetTextLen; //Passando o valor min�mo de caracteres do Memo para a vari�vel Min.
  Total                       := Max - Min; //Efetuando o c�lculo entre o valor m�ximo e o m�nimo.
  lblTOTAL_CARACTERES.Caption := ('Restam ' + IntToStr(Total) + ' caracteres');
  Repaint;
  if Total < 1 then begin // Testando se ainda existem caracteres dispon�veis para inserir.
     ShowMessage('Quantidade de caracteres excedido!');
end;
end;

procedure TfrmCadastroInscricoes.btnEDITAR_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
  Sender: TObject);
begin
  ModoTela ('A');
  vTipo_Edicao          := 'U';
  mmPODE_MUDAR_ABA      := True;
  gpbBENEFICIOS.Enabled := False;
end;

procedure TfrmCadastroInscricoes.cmbSEXO_INSCRITOClick(Sender: TObject);
begin
  if cmbSEXO_INSCRITO.ItemIndex = 0 then chkAGRAVANTE_GESTANTE.Enabled := False else chkAGRAVANTE_GESTANTE.Enabled := True;
end;

procedure TfrmCadastroInscricoes.ModoTelaFamilia(Modo: String);
begin
  if Modo = 'P' then  //Modo PADR�O
  begin
    //LimpaCampos;
    DesabilitarGroupBox;
    //Tratar os bot�es
    btnEXCLUIR_FAMILIAR.Enabled                                            := True;
    btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled             := True;
    btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled           := False;
    btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled           := True;
    btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled := False;
    btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled   := True;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled   := True;
    btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled             := True;
    mmPODE_MUDAR_ABA                                                       := True;
    Panel5.Enabled                                                         := True;
    gpbPONTUACAO.Enabled           := True;
    gpbRENDA_PERCAPTA.Enabled      := True;
    gpbPONTUACAO_FINAL.Enabled     := True;
    gpbRELATORIOS_GRAVADOS.Enabled := True;
    gpbBENEFICIOS.Enabled          := True;    
  end
  else if Modo = 'N' then
  begin
    HabilitaGroupBox;
    btnEXCLUIR_FAMILIAR.Enabled                                            := False;
    btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled             := False;
    btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled           := True;
    btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled           := False;
    btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled := True;
    btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled   := False;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled   := False;
    btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled             := False;
    mmPODE_MUDAR_ABA                                                       := False;
    Panel5.Enabled                                                         := False;
  end
  else if Modo = 'A' then
  begin
    HabilitaGroupBox;
    btnEXCLUIR_FAMILIAR.Enabled                                            := False;
    btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled             := False;
    btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled           := True;
    btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled           := False;
    btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled := True;
    btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled   := False;
    btnCHAMAIMPRESSAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled   := False;
    btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESS.Enabled             := False;
    mmPODE_MUDAR_ABA := False;
    Panel5.Enabled                                                         := False;
  end;
end;

procedure TfrmCadastroInscricoes.btnNOVA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
  Sender: TObject);
begin
  pgcAbas_ComposicaoFamiliar.ActivePageIndex := 0;
  HabilitaGroupBox;
  ModoTelaFamilia('N');
  LimpaCamposFamilia;
  lblINSCRICAO_NomeEntrevistador.Caption := vvNOM_USUARIO;
  edtNOME_COMPLETO_FAMILIAR.SetFocus;
  vTipo_Edicao                           := 'I';
  mmPODE_MUDAR_ABA                       := True;

  //Atribuir "zeros" ao campo de moeda
  edtRENDA_MENSAL_FAMILIAR.Text := '0';
end;

procedure TfrmCadastroInscricoes.btnCANCELAROPERACAO_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
  Sender: TObject);
begin
  ModoTelaFamilia('P');
  vTipo_Edicao := '';
  gpbPONTUACAO.Enabled  := True;
  mmPODE_MUDAR_ABA             := True;
  LimpaCamposFamilia;
  gpbBENEFICIOS.Enabled        := True;
  gpbPONTUACAO.Enabled         := True;
end;

procedure TfrmCadastroInscricoes.btnSAIR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
  Sender: TObject);
begin
  frmCadastroInscricoes.Close;
end;

procedure TfrmCadastroInscricoes.LimparCombos;
var i : Integer;
begin
  for i := 0 to (ComponentCount) - 1 do
    if (Components[i] is TComboBox)  and
       (
         (Components[i].Name <> 'cmbREGIAO') and
         (Components[i].Name <> 'cmbESTADO_CIVIL_INSCRITO') and
         (Components[i].Name <> 'cmbSEXO_INSCRITO') and
         (Components[i].Name <> 'cmbRESIDE_FAMILIA') and
         (Components[i].Name <> 'cmbNUMERO_PORTARIA') and
         (Components[i].Name <> 'cmdSITUACAO_INSCRICAO') and
         (Components[i].Name <> 'cmbNATURAlIDADE_INSCRITO') and
         (Components[i].Name <> 'cmbINSTALACOES_SANITARIAS') and
         (Components[i].Name <> 'cmbCONDICOES_HABITABILIDADE')
       ) then (Components[i] as TComboBox).Clear;
end;

procedure TfrmCadastroInscricoes.ValidaCamposAdm;
Var vFocus : Boolean;
begin
  vFocus := False;
  vOK_Adm := False;

  //Nome do Inscrito
  if (Length(edtNOME_COMPLETO_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O NOME DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o nome sem abrevia��es e/ou acentua��o.',
                            '[Sistema Deca] - Nome do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    PageControl4.ActivePageIndex :=0;
    edtNOME_COMPLETO_INSCRITO.SetFocus;
    vOK_Adm := False;
    vFocus := True;
    Exit;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Estado Civil
  if (cmbESTADO_CIVIL_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O ESTADO CIVIL do Inscrito!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Estado Civil do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbESTADO_CIVIL_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Sexo
  if (cmbSEXO_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O SEXO do Inscrito deve ser selecionado!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Sexo do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbSEXO_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Data de Nascimento
  if (Length(mskDATA_NASCIMENTO_INSCRITO.Text) < 8) then
  begin
    Application.MessageBox ('A DATA DE NASCIMENTO deve ser preenchida no formato ddmmaaaa' +#13+#10+
                            'Favor verificar e tentar novamente.',
                            '[Sistema Deca] - A Data de Nascimento deve ser informada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      mskDATA_NASCIMENTO_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Naturalidade
  if (cmbNATURALIDADE_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A NATURALIDADE do Inscrito deve ser seleionada!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Naturalidade do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbNATURALIDADE_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Endere�o do Inscrito
  if (Length(edtENDERECO_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O ENDERE�O DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o endere�o sem abrevia��es e/ou acentua��o.',
                            '[Sistema Deca] - Endere�o do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      edtENDERECO_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //N�mero da resid�ncia do Inscrito
  if (Length(edtNUMERO_RESIDENCIA_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O N�MERO DA RESID�NCIA DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero da resid�ncia.',
                            '[Sistema Deca] - N�mero da resid�ncia do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      edtNUMERO_RESIDENCIA_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Nome do Bairro do Inscrito
  if (cmbBAIRRO_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O BAIRRO DO INSCRITO DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Bairro do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbBAIRRO_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  if (cmbUNIDADE_REFERENCIA_PARA_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A UNIDADE DE REFER�NCIA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Unidade deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbUNIDADE_REFERENCIA_PARA_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Regi�o da cidade
  if (cmbREGIAO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A REGI�O DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Regi�o deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbREGIAO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Reside com Fam�lia
  if (cmbRESIDE_FAMILIA.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O RESIDE COM FAM�LIA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - "Reside com Fam�lia" deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbRESIDE_FAMILIA.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Tempo de resid�ncia no munic�pio
  if (Length(edtTEMPO_RESIDENCIA.Text) = 0) then
  begin
    Application.MessageBox ('O TEMPO DE RESID�NCIA NO MUNIC�PO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o tempo de resid�ncia.',
                            '[Sistema Deca] - Tempo da resid�ncia deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      edtTEMPO_RESIDENCIA.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //N�mero do cart�o do SUS do inscrito
  if (Length(edtNUMERO_CARTAO_SUS_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O N�MERO DO CART�O DO SUS DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero do cart�o do SUS.',
                            '[Sistema Deca] - Cart�o do SUS deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      edtNUMERO_CARTAO_SUS_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //CPF do inscrito
  if (Length(mskNUMERO_CPF_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O CPF DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero do CPF do isncrito.',
                            '[Sistema Deca] - CPF do inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      mskNUMERO_CPF_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //RG do inscrito
  if (Length(edtNUMERO_RG_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O RG DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero do RG do isncrito.',
                            '[Sistema Deca] - RG do inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      edtNUMERO_RG_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Data de emiss�o do RG do Inscrito
  if (Length(mskDATA_EMISSAO_RG_INSCRITO.Text) < 8) then
  begin
    Application.MessageBox ('A DATA DE EMISS�O DO RG DO INSCRITO deve ser preenchida no formato ddmmaaaa' +#13+#10+
                            'Favor verificar e tentar novamente.',
                            '[Sistema Deca] - A Data de Emiss�o RG inscrito deve ser informada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      mskDATA_EMISSAO_RG_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //�rg�o emissor RG Inscrito
  if (cmbORGAO_EMISSOR_RG_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O EMISSOR DO RG DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Emissor deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      cmbORGAO_EMISSOR_RG_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;

  //Data de atualiza��o da escola
  if (Length(mskDATA_ATUALIZACAO_ESCOLA.Text) < 8) then
  begin
    Application.MessageBox ('A DATA DE ATUALIZA��O DA ESCOLA deve ser preenchida no formato ddmmaaaa' +#13+#10+
                            'Favor verificar e tentar novamente.',
                            '[Sistema Deca] - A Data de Atualiza��o da Escola deve ser informada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      mskDATA_EMISSAO_RG_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Escola
  if (cmbESCOLA_ATUAL_INSCRITO.ItemIndex = -2) then
  begin
    Application.MessageBox ('A OP��O NOME DA ESCOLA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Escola deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      cmbESCOLA_ATUAL_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //Est� matriculado ?
  if (rdgESTA_MATRICULADO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O EST� MATRICULADO DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Est� matriculado deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      rdgESTA_MATRICULADO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end;


  //S�rie Escolar
  if (cmbSERIE_ATUAL_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O S�RIE ESCOLAR DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - S�rie deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      cmbSERIE_ATUAL_INSCRITO.SetFocus;
      vOK_Adm := False;
      Exit;
    End;
  end
  else
  begin
    vOK_Adm := True;
  end; 
end;

procedure TfrmCadastroInscricoes.ValidaCamposSS;
Var vFocus : Boolean;
begin
vFocus := False;
vOK    := False;
  //Verifica se os campos obrigat�rios est�o selecionaodos/preenchidos

  //Nome do Inscrito
  if (Length(edtNOME_COMPLETO_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O NOME DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o nome sem abrevia��es e/ou acentua��o.',
                            '[Sistema Deca] - Nome do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    PageControl4.ActivePageIndex :=0;
    edtNOME_COMPLETO_INSCRITO.SetFocus;
    vOK := False;
    vFocus := True;
    Exit;
  end
  else
  begin
    vOK := True;
  end;


  //Estado Civil
  if (cmbESTADO_CIVIL_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O ESTADO CIVIL do Inscrito!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Estado Civil do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbESTADO_CIVIL_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Sexo
  if (cmbSEXO_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O SEXO do Inscrito deve ser selecionado!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Sexo do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbSEXO_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Data de Nascimento
  if (Length(mskDATA_NASCIMENTO_INSCRITO.Text) < 8) then
  begin
    Application.MessageBox ('A DATA DE NASCIMENTO deve ser preenchida no formato ddmmaaaa' +#13+#10+
                            'Favor verificar e tentar novamente.',
                            '[Sistema Deca] - A Data de Nascimento deve ser informada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      mskDATA_NASCIMENTO_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Naturalidade
  if (cmbNATURALIDADE_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A NATURALIDADE do Inscrito deve ser seleionada!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Naturalidade do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbNATURALIDADE_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Endere�o do Inscrito
  if (Length(edtENDERECO_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O ENDERE�O DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o endere�o sem abrevia��es e/ou acentua��o.',
                            '[Sistema Deca] - Endere�o do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      edtENDERECO_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //N�mero da resid�ncia do Inscrito
  if (Length(edtNUMERO_RESIDENCIA_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O N�MERO DA RESID�NCIA DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero da resid�ncia.',
                            '[Sistema Deca] - N�mero da resid�ncia do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      edtNUMERO_RESIDENCIA_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Nome do Bairro do Inscrito
  if (cmbBAIRRO_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O BAIRRO DO INSCRITO DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Bairro do Inscrito deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbBAIRRO_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Nome do Bairro do Inscrito
  if (cmbUNIDADE_REFERENCIA_PARA_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A UNIDADE DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Unidade deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbUNIDADE_REFERENCIA_PARA_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;



  //Regi�o da cidade
  if (cmbREGIAO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A REGI�O DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Regi�o deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbREGIAO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Reside com Fam�lia
  if (cmbRESIDE_FAMILIA.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O RESIDE COM FAM�LIA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - "Reside com Fam�lia" deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbRESIDE_FAMILIA.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Tempo de resid�ncia no munic�pio
  if (Length(edtTEMPO_RESIDENCIA.Text) = 0) then
  begin
    Application.MessageBox ('O TEMPO DE RESID�NCIA NO MUNIC�PO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o tempo de resid�ncia.',
                            '[Sistema Deca] - Tempo da resid�ncia deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      edtTEMPO_RESIDENCIA.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //N�mero do cart�o do SUS do inscrito
  if (Length(edtNUMERO_CARTAO_SUS_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O N�MERO DO CART�O DO SUS DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero do cart�o do SUS.',
                            '[Sistema Deca] - Cart�o do SUS deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      edtNUMERO_CARTAO_SUS_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //CPF do inscrito
  if (Length(mskNUMERO_CPF_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O CPF DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero do CPF do isncrito.',
                            '[Sistema Deca] - CPF do inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      mskNUMERO_CPF_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //RG do inscrito
  if (Length(edtNUMERO_RG_INSCRITO.Text) = 0) then
  begin
    Application.MessageBox ('O RG DO INSCRITO DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o n�mero do RG do isncrito.',
                            '[Sistema Deca] - RG do inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      edtNUMERO_RG_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Data de emiss�o do RG do Inscrito
  if (Length(mskDATA_EMISSAO_RG_INSCRITO.Text) < 8) then
  begin
    Application.MessageBox ('A DATA DE EMISS�O DO RG DO INSCRITO deve ser preenchida no formato ddmmaaaa' +#13+#10+
                            'Favor verificar e tentar novamente.',
                            '[Sistema Deca] - A Data de Emiss�o RG inscrito deve ser informada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      mskDATA_EMISSAO_RG_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //�rg�o emissor RG Inscrito
  if (cmbORGAO_EMISSOR_RG_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O EMISSOR DO RG DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Emissor deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      cmbORGAO_EMISSOR_RG_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Data de atualiza��o da escola
  if (Length(mskDATA_ATUALIZACAO_ESCOLA.Text) < 8) then
  begin
    Application.MessageBox ('A DATA DE ATUALIZA��O DA ESCOLA deve ser preenchida no formato ddmmaaaa' +#13+#10+
                            'Favor verificar e tentar novamente.',
                            '[Sistema Deca] - A Data de Atualiza��o da Escola deve ser informada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      mskDATA_EMISSAO_RG_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Escola
  if (cmbESCOLA_ATUAL_INSCRITO.ItemIndex = -2) then
  begin
    Application.MessageBox ('A OP��O NOME DA ESCOLA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Escola deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=1;
      cmbESCOLA_ATUAL_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;



  //Est� matriculado ?
  if (rdgESTA_MATRICULADO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O EST� MATRICULADO DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Est� matriculado deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      rdgESTA_MATRICULADO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //S�rie Escolar
  if (cmbSERIE_ATUAL_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O S�RIE ESCOLAR DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - S�rie deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      cmbSERIE_ATUAL_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Per�odo Escolar
  if (cmbPERIODO_ESCOLAR_INSCRITO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O PER�ODO ESCOLAR DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Per�odo deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=2;
      cmbPERIODO_ESCOLAR_INSCRITO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Validar se pelo menos UMA op��o do motivo foi selecionada...
  if (chkMOTIVO_PROCURA_TRABALHO.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_CURSOS.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_TIRAR_RUA.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR.Checked = True) then
    vOK := True
  else if (chkMOTIVO_PROCURA_OUTROS.Checked = True) then
    vOK := True
  else
  begin
    Application.MessageBox ('O MOTIVO DA PROCURA DEVE TER PELO MENOS UMA OP��O SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es.',
                            '[Sistema Deca] - Motivo da Procura deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    vOK := False;
    Exit;
  end;







  //Rela��o de Responsabilidade
  if (cmbRELACAO_RESPONSABILIDADE.ItemIndex = -1) then
  begin
    Application.MessageBox ('A RELA��O DE RESPONSABILIDADE DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Rela��o de Responsabilidade deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=5;
      cmbRELACAO_RESPONSABILIDADE.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Natureza da Habita��o
  if (cmbNATUREZA_HABITACAO.ItemIndex = -1) then
  begin
    Application.MessageBox ('A NATUREZA DA HABITA��O DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Natureza da Habita��o deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=6;
      cmbNATUREZA_HABITACAO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Tipo da Habita��o
  if (cmbTIPO_HABITACAO.ItemIndex = -1) then
  begin
    Application.MessageBox ('O TIPO DA HABITA��O DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Tipo da Habita��o deve ser selecionado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=6;
      cmbTIPO_HABITACAO.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Infraestrutura
  if (cmbINFRAESTRUTURA.ItemIndex = -1) then
  begin
    Application.MessageBox ('A INFRAESTRUTURA DEVE SER SELECIONADO!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Infraestrutura deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=6;
      cmbINFRAESTRUTURA.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Habitantes X C�modos
  if (Length(edtQTD_HABITANTES.Text) = 0) and (Length(edtQTD_COMODOS.Text) = 0 ) then
  begin
    Application.MessageBox ('A QUANTIDADE DE HABITANTES E C�MODOS DEVE SER INFORMADA!' +#13+#10+
                            'FAvor informar Habitantes e C�modos.',
                            '[Sistema Deca] - Habitantes e C�modos deve ser informado.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=6;
      edtQTD_HABITANTES.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Instala��es Sanit�rias
  if (cmbINSTALACOES_SANITARIAS.ItemIndex = -1) then
  begin
    Application.MessageBox ('A INSTALA��O SANIT�RIA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Instala��es Sanit�rias deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=6;
      cmbINSTALACOES_SANITARIAS.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;


  //Instala��es Sanit�rias
  if (cmbCONDICOES_HABITABILIDADE.ItemIndex = -1) then
  begin
    Application.MessageBox ('CONDI��ES DE HABITABILIDADE DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - Condi��es de Habitabilidade deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=6;
      cmbCONDICOES_HABITABILIDADE.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
  begin
    vOK := True;
  end;
end;
procedure TfrmCadastroInscricoes.SpeedButton4Click(Sender: TObject);
begin
  try
    Application.CreateForm (TfrmNavegador, frmNavegador);
    frmNavegador.URLs.Text := 'http://www.buscacep.correios.com.br/sistemas/buscacep/';
    frmNavegador.StatusBar1.Panels[0].Text := 'Busca CEP';
    frmNavegador.Show;
  except end;
end;

procedure TfrmCadastroInscricoes.cmbESCOLA_ATUAL_INSCRITOClick(
  Sender: TObject);
begin

  cmbSERIE_ATUAL_INSCRITO.Clear;
  //Ser�o carregadas as s�ries de acordo com a escola selecionada, a fim de adequar com o dados do Deca, facilitando posteriormente, o processo de ADMISS�O (migra��o)
  //Essa carga ser� feita a partir da sele��o da Escola... cmbESCOLA_ATUAL_INSCRITO
  //Carregar a lista de N�vel de Escolaridade - Aba Dados Composi��o Familiar - Dados Pessoais do Familiar
  //Recuperar o c�digo da escola no Deca para trazer as s�ries....
  with dmDeca.cdsSel_Escola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_escola').Value := Null;
    Params.ParamByName ('@pe_nom_escola').Value := Trim(cmbESCOLA_ATUAL_INSCRITO.Text);   //dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NomeEscola').Value;
    Open;

    with dmDeca.cdsSel_EscolaSerie do
    begin
      Close;
      Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;
      Params.ParamByName ('@pe_cod_escola').Value          := dmDeca.cdsSel_Escola.FieldByName ('cod_escola').Value;
      Params.ParamByName ('@pe_cod_id_serie').Value        := Null;
      Open;

      if (dmDeca.cdsSel_EscolaSerie.RecordCount > 0 ) then
      begin
        while not (dmDeca.cdsSel_EscolaSerie.eof) do
        begin
          cmbSERIE_ATUAL_INSCRITO.Items.Add(dmDeca.cdsSel_EscolaSerie.FieldByName ('dsc_serie').Value);
          vListaSerie1.Add (IntToStr(dmDeca.cdsSel_EscolaSerie.FieldByName ('cod_id_serie').Value));
          vListaSerie2.Add (dmDeca.cdsSel_EscolaSerie.FieldByName ('dsc_serie').Value);
          dmDeca.cdsSel_EscolaSerie.Next;
        end;
      end
      else
      begin
        Application.MessageBox ('Aten��o!!!' +#13+#10+
                                'N�o foram localizadas s�ries cadastradas para a escola informada. Procure a equipe do Acompanhamento Escolar.',
                                '[Sistema Deca] - S�ries n�o cadastradas',
                                MB_OK + MB_ICONERROR);
      end;
    end;
  end;

end;

procedure TfrmCadastroInscricoes.CalculaPontos;
begin
  try
    with dmTriagemDeca.cdsSel_PontosFicha do
    begin
      Close;
      Params.ParamByName ('@Cod_inscricao').Value                   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
      Open;

      //Repassa os valores para os label�s em tela...  
      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('RendaTotal').isNull)
       then panRENDA_TOTAL.Caption      := '0'
       //else panRENDA_TOTAL.Caption      := FloatToStr(dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('RendaTotal').Value);
       else panRENDA_TOTAL.Caption      := FloatToStrF(dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('RendaTotal').Value, ffCurrency,10,2);
       //panRENDA_TOTAL.Caption      := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('RendaTotal').Value;

      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('NumHabitantes').isNull)
       then panNUMERO_RESIDENTES.Caption   := '0'
       else panNUMERO_RESIDENTES.Caption   := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('NumHabitantes').Value;

      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('RendaPerCapita').isNull)
       then panRENDA_PORHABITANTE.Caption  := '0'
       else panRENDA_PORHABITANTE.Caption  := floatToStrF(dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('RendaPerCapita').Value, ffCurrency,10,2);


      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_RendaPerCapita').isNull)
       then panRENDA_PERCAPTA.Caption      := '0'
       else panRENDA_PERCAPTA.Caption      := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_RendaPerCapita').Value;
      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_Situacaosocial').isNull)
       then panSITUACAO_SOCIAL.Caption      := '0'
       else panSITUACAO_SOCIAL.Caption      := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_Situacaosocial').Value;      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_CondicaoSaude').isNull)       then panCONDICAO_SAUDE.Caption      := '0'
       else panCONDICAO_SAUDE.Caption      := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_CondicaoSaude').Value;
      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_TempoResidencia').isNull)
       then panTEMPO_RESIDENCIA.Caption    := '0'
       else panTEMPO_RESIDENCIA.Caption    := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_TempoResidencia').Value;
      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_SituacaoMoradia').isNull)
       then panSITUACAO_MORADIA.Caption    := '0'
       else panSITUACAO_MORADIA.Caption    := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_SituacaoMoradia').Value;
      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_Bens').isNull)
       then panBENS.Caption      := '0'
       else panBENS.Caption      := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_Bens').Value;
      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_Pontuacao').isNull)
       then panPONTUACAO_TOTAL.Caption      := '0'
       else panPONTUACAO_TOTAL.Caption      := dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('pnt_Pontuacao').Value;

      if (dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('TotalDespesas').isNull)
       then panDESPESAS.Caption             := '0'
       else panDESPESAS.Caption             := FloatToStrF(dmTriagemDeca.cdsSel_PontosFicha.FieldByName ('TotalDespesas').Value, ffCurrency,10,2);


    end;
  except end;
end;

procedure TfrmCadastroInscricoes.mskDATA_NASCIMENTO_INSCRITOExit(
  Sender: TObject);
begin
  if (mskDATA_NASCIMENTO_INSCRITO.Text <> '  /  /    ') then
    lblIDADE.Caption := frmFichaCrianca.CalculaIdade(StrToDAte(mskDATA_NASCIMENTO_INSCRITO.Text))
  else
    lblIDADE.Caption := 'Idade n�o calculada. Data inv�lida!';
end;

procedure TfrmCadastroInscricoes.InserirDadosFicha;
begin
  //Procedimento para inserir os dados da ficha de inscri��o
  if (Application.MessageBox ('Confirma a inclus�o de NOVA inscri��o?',
                              '[Sistema Deca] - Confirma��o de Inclus�o',
                              MB_YESNO + MB_ICONQUESTION) = idYes) then

  begin
    with dmTriagemDeca.cdsIns_Cadastro_TriagemDeca do
    begin

      //Recuperar o n�mero da inscri��o automaticamente...
      with dmTriagemDeca.cdsSel_Retorna_NIF_Valido do
      begin
        Close;
        Params.ParamByName ('@pe_num_digito').Value := Copy(DateToStr(Date),9,2);
        Open;

        //Insere a inscri��o no banco de dados
        with dmTriagemDeca.cdsIns_Cad_Inscricao do
        begin
          Close;
          Params.ParamByName ('@pe_num_inscricao').Value := dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('INSCRICAO_VALIDA').Value;
          Params.ParamByName ('@pe_num_digito').Value    := Copy(dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('DATA_SERVER').Value,9,2);
          Params.ParamByName ('@pe_cod_usuario').Value   := vvCOD_USUARIO;
          Execute;
        end;
      end;

      //Repassa os valores para os par�metros e executa o insert
    Close;
    Params.ParamByName ('@pe_num_inscricao').Value                              := dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('INSCRICAO_VALIDA').Value; //GetValue(mskNUMERO_INSCRICAO.Text);
    Params.ParamByName ('@pe_num_inscricao_digt').Value                         := Copy(dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('DATA_SERVER').Value,9,2); //GetValue(mskINSCRICAO_DIGITO.Text);
    Params.ParamByName ('@pe_dta_inscricao').AsDateTime                         := GetValue(mskDATA_INSCRICAO, vtDate);
    Params.ParamByName ('@pe_dat_matricula').Value                              := Null; //GetValue(mskDATA_MATRICULA.Text);
    Params.ParamByName ('@pe_dat_readmissao').Value                             := Null; //GetValue(mskDATA_READMISSAO.Text);
    Params.ParamByName ('@pe_dta_renovacao').Value                              := Null; //GetValue(mskDATA_RENOVACAO.Text);
    //Pontua��o
    Params.ParamByName ('@pe_num_pontuacao').Value                              := 0;
    //Usu�rio
    Params.ParamByName ('@pe_cod_UsuarioCadastro').Value                        := vvCOD_USUARIO;
    Params.ParamByName ('@pe_dsc_inscritopor').Value                            := Copy(vvNOM_USUARIO,1,50);
    Params.ParamByName ('@pe_cod_situacao').Value                               := vListaSituacao1.Strings[cmbSITUACAO_INSCRICAO.ItemIndex];
    //N�mero da Portaria
    // ATRIBUI O VALOR 17 (NUMERO DA ATUAL PORTARIA - DEZ/2017)
    Params.ParamByName ('@pe_num_portaria').Value                               := 17;
    //Params.ParamByName ('@pe_num_portaria').Value                               := cmbNUMERO_PORTARIA.ItemIndex;
    Params.ParamByName ('@pe_nom_nome').Value                                   := GetValue(edtNOME_COMPLETO_INSCRITO.Text);
    Params.ParamByName ('@pe_dat_nascimento').Value                             := GetValue(mskDATA_NASCIMENTO_INSCRITO, vtDate);
    Params.ParamByName ('@pe_cod_estadocivil').Value                            := StrToInt(vListaEstadoCivil1.Strings[cmbESTADO_CIVIL_INSCRITO.ItemIndex]);
    Params.ParamByName ('@pe_ind_sexo').Value                                   := cmbSEXO_INSCRITO.ItemIndex;
    Params.ParamByName ('@pe_cod_cidade').Value                                 := StrToInt(vListaNaturalidade1.Strings[cmbNATURALIDADE_INSCRITO.ItemIndex]);
    Params.ParamByName ('@pe_cod_nacionalidade').Value                          := StrToInt(vListaNacionalidade1.Strings[cmbNACIONALIDADE_INSCRITO.ItemIndex]);
    Params.ParamByName ('@pe_dsc_endereco').Value                               := GetValue(edtENDERECO_INSCRITO.Text);
    Params.ParamByName ('@pe_num_NumResidencia').Value                          := GetValue(edtNUMERO_RESIDENCIA_INSCRITO.Text);
    Params.ParamByName ('@pe_dsc_Complemento').Value                            := GetValue(edtCOMPLEMENTO_ENDERECO_INSCRITO.Text);

    mskCEP_RESIDENCIAL_INSCRITO.EditMask := '';
    Params.ParamByName ('@pe_num_cep').Value                                    := GetValue(mskCEP_RESIDENCIAL_INSCRITO.Text);
    mskCEP_RESIDENCIAL_INSCRITO.EditMask := '99999-999';      
      
    Params.ParamByName ('@pe_dsc_PontoRef').Value                               := GetValue(edtPONTO_REFERENCIA_RESIDENCIA.Text);
    Params.ParamByName ('@pe_ind_ResideFam').Value                              := cmbRESIDE_FAMILIA.ItemIndex;
    Params.ParamByName ('@pe_dsc_NResideFam').Value                             := GetValue(edtN_RESIDE_FAM.Text);
    Params.ParamByName ('@pe_cod_regiao').AsInteger                             := StrToInt(vListaRegiao1.Strings[cmbREGIAO.ItemIndex]);
    Params.ParamByName ('@pe_num_TempoResidencia').AsInteger                    := StrToInt(edtTEMPO_RESIDENCIA.Text);
    Params.ParamByName ('@pe_cod_bairro').AsInteger                             := StrToInt(vListaBairros1.Strings[cmbBAIRRO_INSCRITO.ItemIndex]);
    Params.ParamByName ('@pe_cod_unidade_referencia').AsInteger                 := StrToInt(vListaUnidade1.Strings[cmbUNIDADE_REFERENCIA_PARA_INSCRITO.ItemIndex]);
    Params.ParamByName ('@pe_num_cartao_sus').Value                             := GetValue(edtNUMERO_CARTAO_SUS_INSCRITO.Text);
    Params.ParamByName ('@pe_num_cra').Value                                    := GetValue(edtNUMERO_CRA_INSCRITO.Text);
    Params.ParamByName ('@pe_num_nis').Value                                    := GetValue(edtNUMERO_NIS_INSCRITO.Text);
    Params.ParamByName ('@pe_num_cartao_passe').Value                           := GetValue(edtNUMERO_CARTAO_PASSE_INSCRITO.Text);
    Params.ParamByName ('@pe_cod_sias').Value                                   := GetValue(edtNUMERO_CARTAO_SIAS_INSCRITO.Text);
    Params.ParamByName ('@pe_dsc_NumCertNasc').Value                            := GetValue(edtCERTIDAO_NASCIMENTO_INSCRITO.Text);
    Params.ParamByName ('@pe_dsc_CPF').Value                                    := GetValue(mskNUMERO_CPF_INSCRITO.Text);
    Params.ParamByName ('@pe_dsc_RG').Value                                     := GetValue(edtNUMERO_RG_INSCRITO.Text);
    Params.ParamByName ('@pe_dat_emissao_rg').Value                             := GetValue(mskDATA_EMISSAO_RG_INSCRITO, vtDate);
    Params.ParamByName ('@pe_cod_emissor').Value                                := StrToInt(vListaEmissor1.Strings[cmbORGAO_EMISSOR_RG_INSCRITO.ItemIndex]);
    Params.ParamByName ('@pe_num_fone_contato').Value                           := GetValue(medTELEFONES_CONTATOS_INSCRITO.Text);

    //At� aqui, pertinente ao Administrativo fazer a inser��o
    if vvIND_PERFIL <> 3 then
    begin
      Params.ParamByName ('@pe_ind_SeMatric').Value                             := Null;
      Params.ParamByName ('@pe_dsc_motivo_nestuda').Value                       := Null;
      Params.ParamByName ('@pe_dsc_NomeEscola').Value                           := Null;
      Params.ParamByName ('@pe_dsc_escolaSerie').Value                          := Null;
      Params.ParamByName ('@pe_dsc_escolaPeriodo').Value                        := Null;
      Params.ParamByName ('@pe_dsc_ra').Value                                   := Null;
      Params.ParamByName ('@pe_ind_idadeXserie').Value                          := Null;
      Params.ParamByName ('@pe_dta_atualizaEscolar').Value                      := Null;
      Params.ParamByName ('@pe_dsc_razaoincomp').Value                          := Null;
      Params.ParamByName ('@pe_dsc_DiagSaude').Value                            := Null;
      Params.ParamByName ('@pe_ind_invalidez_temporaria').Value                 := Null;
      Params.ParamByName ('@pe_ind_invalidez_permanente').Value                 := Null;
      Params.ParamByName ('@pe_ind_DoencaInfecto').Value                        := Null;
      Params.ParamByName ('@pe_ind_IncapazTrab').Value                          := Null;
      Params.ParamByName ('@pe_num_despmedicas').Value                          := Null;
      Params.ParamByName ('@pe_ind_comprovadespmedic').Value                    := Null;
      Params.ParamByName ('@pe_ind_proc_trabalho').Value                        := Null;
      Params.ParamByName ('@pe_ind_proc_cursos').Value                          := Null;
      Params.ParamByName ('@pe_ind_proc_nao_ficar_sozinho').Value               := Null;
      Params.ParamByName ('@pe_ind_proc_tirar_rua').Value                       := Null;
      Params.ParamByName ('@pe_ind_proc_enc_creas').Value                       := Null;
      Params.ParamByName ('@pe_ind_proc_enc_cras').Value                        := Null;
      Params.ParamByName ('@pe_ind_proc_enc_vij').Value                         := Null;
      Params.ParamByName ('@pe_ind_proc_enc_as').Value                          := Null;
      Params.ParamByName ('@pe_ind_proc_enc_ct').Value                          := Null;
      Params.ParamByName ('@pe_ind_proc_outros').Value                          := Null;
      Params.ParamByName ('@pe_dsc_MotivoProcuraCompl').Value                   := Null;
      Params.ParamByName ('@pe_ind_outroimovel').Value                          := Null;
      Params.ParamByName ('@pe_ind_carro').Value                                := Null;
      Params.ParamByName ('@pe_dsc_carromodel').Value                           := Null;
      Params.ParamByName ('@pe_num_carroano').Value                             := Null;
      Params.ParamByName ('@pe_ind_moto').Value                                 := Null;
      Params.ParamByName ('@pe_dsc_motomodel').Value                            := Null;
      Params.ParamByName ('@pe_num_motoano').Value                              := Null;
      Params.ParamByName ('@pe_ind_veiculo_outras_priori').Value                := Null;
      Params.ParamByName ('@pe_ind_veiculo_trabalho').Value                     := Null;
      Params.ParamByName ('@pe_ind_veiculo_unico_transp').Value                 := Null;
      Params.ParamByName ('@pe_cod_RelResp').Value                              := Null;
      Params.ParamByName ('@pe_ind_drogadicto').Value                           := Null;
      Params.ParamByName ('@pe_ind_alcoolatra').Value                           := Null;
      Params.ParamByName ('@pe_ind_paimaeadolescente').Value                    := Null;
      Params.ParamByName ('@pe_ind_gestante').Value                             := Null;
      Params.ParamByName ('@pe_ind_medicamentopsico').Value                     := Null;
      Params.ParamByName ('@pe_ind_medidas_socioeducativas').Value              := Null;
      Params.ParamByName ('@pe_ind_aquarela').Value                             := Null;
      Params.ParamByName ('@pe_ind_violencia').Value                            := Null;
      Params.ParamByName ('@pe_ind_violencia_fisica').Value                     := Null;
      Params.ParamByName ('@pe_ind_violencia_psicologica').Value                := Null;
      Params.ParamByName ('@pe_ind_violencia_sexual').Value                     := Null;
      Params.ParamByName ('@pe_ind_violencia_negligencia').Value                := Null;
      Params.ParamByName ('@pe_ind_violencia_abandono').Value                   := Null;
      Params.ParamByName ('@pe_ind_prostituicao').Value                         := Null;
      Params.ParamByName ('@pe_ind_traficodrogas').Value                        := Null;
      Params.ParamByName ('@pe_ind_convivedrogadiccao').Value                   := Null;
      Params.ParamByName ('@pe_ind_convivealcool').Value                        := Null;
      Params.ParamByName ('@pe_ind_conviveportdoencamental').Value              := Null;
      Params.ParamByName ('@pe_ind_conviveportdoencainfecto').Value             := Null;
      Params.ParamByName ('@pe_ind_conv_port_doencas_incap').Value              := Null;
      Params.ParamByName ('@pe_ind_membroautorinfracao').Value                  := Null;
      Params.ParamByName ('@pe_ind_criancasozinha').Value                       := Null;
      Params.ParamByName ('@pe_cod_natureza_hab').Value                         := Null;
      Params.ParamByName ('@pe_dsc_valoraluguel').Value                         := Null;
      Params.ParamByName ('@pe_ind_comprovoualuguel').Value                     := Null;
      Params.ParamByName ('@pe_dsc_observacao').Value                           := Null;
      Params.ParamByName ('@pe_cod_tipo_hab').Value                             := Null;
      Params.ParamByName ('@pe_cod_infra_estrutura').Value                      := Null;
      Params.ParamByName ('@pe_num_qtdehabitantes').Value                       := Null;
      Params.ParamByName ('@pe_num_qtdecomodos').Value                          := Null;
      Params.ParamByName ('@pe_cod_inst_sanitaria').Value                       := Null;
      Params.ParamByName ('@pe_cod_cond_hab').Value                             := Null;
    end
    else if (vvIND_PERFIL = 3) then
    begin
      Params.ParamByName ('@pe_ind_SeMatric').Value                             := rdgESTA_MATRICULADO.ItemIndex;
      Params.ParamByName ('@pe_dsc_motivo_nestuda').Value                       := GetValue(edtMOTIVO_NAO_ESTUDA.Text);
      Params.ParamByName ('@pe_dsc_NomeEscola').Value                           := GetValue(cmbESCOLA_ATUAL_INSCRITO.Text);
      Params.ParamByName ('@pe_dsc_escolaSerie').Value                          := GetValue(cmbSERIE_ATUAL_INSCRITO.Text);
      Params.ParamByName ('@pe_dsc_escolaPeriodo').Value                        := cmbPERIODO_ESCOLAR_INSCRITO.Text;
      Params.ParamByName ('@pe_dsc_ra').Value                                   := GetValue(edtNUMERO_RA_INSCRITO.Text);
      Params.ParamByName ('@pe_ind_idadeXserie').Value                          := cmbRELACAO_IDADE_SERIE_INSCRITO.ItemIndex;

      if (mskDATA_ATUALIZACAO_ESCOLA.Text = '  /  /    ') then
        Params.ParamByName ('@pe_dta_atualizaEscolar').Value                    := Null
      else Params.ParamByName ('@pe_dta_atualizaEscolar').Value                 := GetValue(mskDATA_ATUALIZACAO_ESCOLA, vtDate);
        
      Params.ParamByName ('@pe_dsc_razaoincomp').Value                          := GetValue(medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE.Text);
      Params.ParamByName ('@pe_dsc_DiagSaude').Value                            := GetValue(edtDIAGNOSTICO_SAUDE_INSCRITO.Text);

      //Validar o preenchimento do campo chkINVALIDEZ_TEMPORARIA_INSCRITO
      if (chkINVALIDEZ_TEMPORARIA_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_invalidez_temporaria').Value := 1 else Params.ParamByName ('@pe_ind_invalidez_temporaria').Value := 0;
      //Validar o preenchimento do campo chkINVALIDEZ_PERMANENTE_INSCRITO
      if (chkINVALIDEZ_PERMANENTE_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_invalidez_permanente').Value := 1 else Params.ParamByName ('@pe_ind_invalidez_permanente').Value := 0;
      //Validar o campo chkPORTADOR_DOENCA_INFECTO_INSCRITO
      if (chkPORTADOR_DOENCA_INFECTO_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_DoencaInfecto').Value := 1 else Params.ParamByName ('@pe_ind_DoencaInfecto').Value := 0;
      //Validar o campo chkPORTADOR_DOENCA_INFECTO_INSCRITO
      if (chkINCAPACIDADE_TRABALHO_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_IncapazTrab').Value := 1 else Params.ParamByName ('@pe_ind_IncapazTrab').Value := 0;

      Params.ParamByName ('@pe_num_despmedicas').Value                          := StrToFloat(edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text);

      //Validar o campo chkCOMPROVOU_DESPESAS_MEDICAS
      if (chkCOMPROVOU_DESPESAS_MEDICAS.Checked) then Params.ParamByName ('@pe_ind_comprovadespmedic').Value := 1 else Params.ParamByName ('@pe_ind_comprovadespmedic').Value := 0;
      if (chkMOTIVO_PROCURA_TRABALHO.Checked) then Params.ParamByName ('@pe_ind_proc_trabalho').Value := 1 else Params.ParamByName ('@pe_ind_proc_trabalho').Value := 0;
      if (chkMOTIVO_PROCURA_CURSOS.Checked) then Params.ParamByName ('@pe_ind_proc_cursos').Value := 1 else Params.ParamByName ('@pe_ind_proc_cursos').Value := 0;
      if (chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO.Checked) then Params.ParamByName ('@pe_ind_proc_nao_ficar_sozinho').Value := 1 else Params.ParamByName ('@pe_ind_proc_nao_ficar_sozinho').Value := 0;
      if (chkMOTIVO_PROCURA_TIRAR_RUA.Checked) then Params.ParamByName ('@pe_ind_proc_tirar_rua').Value := 1 else Params.ParamByName ('@pe_ind_proc_tirar_rua').Value := 0;
      if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS.Checked) then Params.ParamByName ('@pe_ind_proc_enc_creas').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_creas').Value := 0;
      if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS.Checked) then Params.ParamByName ('@pe_ind_proc_enc_cras').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_cras').Value := 0;
      if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ.Checked) then Params.ParamByName ('@pe_ind_proc_enc_vij').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_vij').Value := 0;
      if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS.Checked) then Params.ParamByName ('@pe_ind_proc_enc_as').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_as').Value := 0;
      if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR.Checked) then Params.ParamByName ('@pe_ind_proc_enc_ct').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_ct').Value := 0;
      if (chkMOTIVO_PROCURA_OUTROS.Checked) then Params.ParamByName ('@pe_ind_proc_outros').Value := 1 else Params.ParamByName ('@pe_ind_proc_outros').Value := 0;

      Params.ParamByName ('@pe_dsc_MotivoProcuraCompl').Value                   := GetValue(medMOTIVO_PROCURA_OUTROS.Text);

      if (chkIMOVEL_OUTRO.Checked) then Params.ParamByName ('@pe_ind_outroimovel').Value := 1 else Params.ParamByName ('@pe_ind_outroimovel').Value := 0;
      if (chkPOSSUI_CARRO.Checked) then Params.ParamByName ('@pe_ind_carro').Value := 1 else Params.ParamByName ('@pe_ind_carro').Value := 0;

      Params.ParamByName ('@pe_dsc_carromodel').Value                           := GetValue(edtCARRO_MODELO.Text);
      Params.ParamByName ('@pe_num_carroano').Value                             := GetValue(edtCARRO_ANO.Text);

      if (chkPOSSUI_MOTO.Checked) then Params.ParamByName ('@pe_ind_moto').Value := 1 else Params.ParamByName ('@pe_ind_moto').Value := 0;

      Params.ParamByName ('@pe_dsc_motomodel').Value                            := GetValue(edtMOTO_MODELO.Text);
      Params.ParamByName ('@pe_num_motoano').Value                              := GetValue(edtMOTO_ANO.Text);

      if (chkUSO_VEICULO_OUTRAS_PRIORIDADES.Checked) then Params.ParamByName ('@pe_ind_veiculo_outras_priori').Value := 1 else Params.ParamByName ('@pe_ind_veiculo_outras_priori').Value := 0;
      if (chkUSO_VEICULO_TRABALHO.Checked) then Params.ParamByName ('@pe_ind_veiculo_trabalho').Value := 1 else Params.ParamByName ('@pe_ind_veiculo_trabalho').Value := 0;
      if (chkUSO_VEICULO_UNICO_MEIO_TRANSP.Checked) then Params.ParamByName ('@pe_ind_veiculo_unico_transp').Value := 1 else Params.ParamByName ('@pe_ind_veiculo_unico_transp').Value := 0;

      Params.ParamByName ('@pe_cod_RelResp').Value                              := vListaRelacaoResponsabilidade1.Strings[cmbRELACAO_RESPONSABILIDADE.ItemIndex];

      if (chkAGRAVANTE_DROGADICTO.Checked) then Params.ParamByName ('@pe_ind_drogadicto').Value := 1 else Params.ParamByName ('@pe_ind_drogadicto').Value := 0;
      if (chkAGRAVANTE_ALCOOLATRA.Checked) then Params. ParamByName ('@pe_ind_alcoolatra').Value := 1 else Params.ParamByName ('@pe_ind_alcoolatra').Value := 0;
      if (chkAGRAVANTE_PAIMAE_ADOLESCENTE.Checked) then Params. ParamByName ('@pe_ind_paimaeadolescente').Value := 1 else Params.ParamByName ('@pe_ind_paimaeadolescente').Value := 0;
      if (chkAGRAVANTE_GESTANTE.Checked) then Params. ParamByName ('@pe_ind_gestante').Value := 1 else Params.ParamByName ('@pe_ind_gestante').Value := 0;
      if (chkAGRAVANTE_USO_PSICOTROPICOS.Checked) then Params. ParamByName ('@pe_ind_medicamentopsico').Value := 1 else Params.ParamByName ('@pe_ind_medicamentopsico').Value := 0;
      if (chkAGRAVANTE_MEDIDAS_SOCIOEDUCATIVAS.Checked) then Params. ParamByName ('@pe_ind_medidas_socioeducativas').Value := 1 else Params.ParamByName ('@pe_ind_medidas_socioeducativas').Value := 0;

      if (chkSIT_RISCO_CREAS.Checked) then Params. ParamByName ('@pe_ind_aquarela').Value := 1 else Params.ParamByName ('@pe_ind_aquarela').Value := 0;
      if (chkSIT_RISCO_VIOLENCIA.Checked) then Params. ParamByName ('@pe_ind_violencia').Value := 1 else Params.ParamByName ('@pe_ind_violencia').Value := 0;
      if (chkSIT_RISCO_VIOLENCIA_FISICA.Checked) then Params. ParamByName ('@pe_ind_violencia_fisica').Value := 1 else Params.ParamByName ('@pe_ind_violencia_fisica').Value := 0;
      if (chkSIT_RISCO_VIOLENCIA_PSICOLOGICA.Checked) then Params. ParamByName ('@pe_ind_violencia_psicologica').Value := 1 else Params.ParamByName ('@pe_ind_violencia_psicologica').Value := 0;
      if (chkSIT_RISCO_VIOLENCIA_SEXUAL.Checked) then Params. ParamByName ('@pe_ind_violencia_sexual').Value := 1 else Params.ParamByName ('@pe_ind_violencia_sexual').Value := 0;
      if (chkSIT_RISCO_VIOLENCIA_NEGLIGENCIA.Checked) then Params. ParamByName ('@pe_ind_violencia_negligencia').Value := 1 else Params.ParamByName ('@pe_ind_violencia_negligencia').Value := 0;
      if (chkSIT_RISCO_VIOLENCIA_ABANDONO.Checked) then Params. ParamByName ('@pe_ind_violencia_abandono').Value := 1 else Params.ParamByName ('@pe_ind_violencia_abandono').Value := 0;

      if (chkSIT_RISCO_PROSTITUICAO.Checked) then Params. ParamByName ('@pe_ind_prostituicao').Value := 1 else Params.ParamByName ('@pe_ind_prostituicao').Value := 0;
      if (chkSIT_RISCO_TRAFICO_DROGAS.Checked) then Params. ParamByName ('@pe_ind_traficodrogas').Value := 1 else Params.ParamByName ('@pe_ind_traficodrogas').Value := 0;
      if (chkSIT_RISCO_CONVIVE_DROGADICCAO.Checked) then Params. ParamByName ('@pe_ind_convivedrogadiccao').Value := 1 else Params.ParamByName ('@pe_ind_convivedrogadiccao').Value := 0;
      if (chkSIT_RISCO_CONVIVE_ALCOOLATRA.Checked) then Params. ParamByName ('@pe_ind_convivealcool').Value := 1 else Params.ParamByName ('@pe_ind_convivealcool').Value := 0;
      if (chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_MENTAIS.Checked) then Params. ParamByName ('@pe_ind_conviveportdoencamental').Value := 1 else Params.ParamByName ('@pe_ind_conviveportdoencamental').Value := 0;
      if (chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INFECTO.Checked) then Params. ParamByName ('@pe_ind_conviveportdoencainfecto').Value := 1 else Params.ParamByName ('@pe_ind_conviveportdoencainfecto').Value := 0;
      if (chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INCAPACITANTES.Checked) then Params. ParamByName ('@pe_ind_conv_port_doencas_incap').Value := 1 else Params.ParamByName ('@pe_ind_conv_port_doencas_incap').Value := 0;
      if (chkSIT_RISCO_MEMBRO_ATOS_INFRACIONAIS.Checked) then Params. ParamByName ('@pe_ind_membroautorinfracao').Value := 1 else Params.ParamByName ('@pe_ind_membroautorinfracao').Value := 0;
      if (chkSIT_RISCO_FICAM_SOZINHAS.Checked) then Params. ParamByName ('@pe_ind_criancasozinha').Value := 1 else Params.ParamByName ('@pe_ind_criancasozinha').Value := 0;

      Params.ParamByName ('@pe_cod_natureza_hab').Value                         := vListaNaturezaHabitacao1.Strings[cmbNATUREZA_HABITACAO.ItemIndex];
      Params.ParamByName ('@pe_dsc_valoraluguel').Value                         := StrToFloat(edtVALOR_ALUGUEL_INFORMADO.Text);

      if (chkCOMPROVOU_ALUGUEL.Checked) then Params.ParamByName ('@pe_ind_comprovoualuguel').Value := 1 else Params.ParamByName ('@pe_ind_comprovoualuguel').Value := 0;

      Params.ParamByName ('@pe_dsc_observacao').Value                           := GetValue(edtOBSERVACOES.Text);
      Params.ParamByName ('@pe_cod_tipo_hab').Value                             := vListaTipoHabitacao1.Strings[cmbTIPO_HABITACAO.ItemIndex];
      Params.ParamByName ('@pe_cod_infra_estrutura').Value                      := vListaInfraestrutura1.Strings[cmbINFRAESTRUTURA.ItemIndex];
      Params.ParamByName ('@pe_num_qtdehabitantes').Value                       := GetValue(edtQTD_HABITANTES.Text);
      Params.ParamByName ('@pe_num_qtdecomodos').Value                          := GetValue(edtQTD_COMODOS.Text);
      Params.ParamByName ('@pe_cod_inst_sanitaria').Value                       := vListaInstalacoesSantitarias1.Strings[cmbINSTALACOES_SANITARIAS.ItemIndex];
      Params.ParamByName ('@pe_cod_cond_hab').Value                             := vListaCondicaoesHabitabilidade1.Strings[cmbCONDICOES_HABITABILIDADE.ItemIndex];
    end;

    //Se passou na valida��o dos campos, independente do perfil...
    if (vOK = True) or (vOK_Adm = True) then
    begin
      Execute;  //with dmTriagemDeca.cdsIns_Cadastro_TriagemDeca do     

      // Utilizar o cod_inscricao para a Composi��o Familiar (NOVA FICHA)
      mskNUMERO_INSCRICAO.Text := IntToStr(dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('INSCRICAO_VALIDA').Value);
      mskINSCRICAO_DIGITO.Text := Copy(dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('DATA_SERVER').Value,9,2);

        //Recupera o c�digo da inscri��o atrav�s do N�mero da Inscri��o e D�gito
      with dmTriagemDeca.cdsSel_CadastroFull do
      begin
        Close;
        Params.ParamByName ('@pe_cod_inscricao').Value      := Null;
        Params.ParamByName ('@pe_num_inscricao').Value      := dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('INSCRICAO_VALIDA').Value;
        Params.ParamByName ('@pe_num_inscricao_digt').Value := StrToInt(Copy(dmTriagemDeca.cdsSel_Retorna_NIF_Valido.FieldByName ('DATA_SERVER').Value,9,2));
        Open;

        vvCOD_INSCRICAO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;

      end;
      mmPODE_MUDAR_ABA := True;
      PageControl4.ActivePageIndex := 0;
      ModoTela('P');
     end;
   end; //with dmTriagemDeca.cdsIns_Cadastro_TriagemDeca do
  end
  else
  begin
    mmPODE_MUDAR_ABA := True;
    PageControl4.ActivePageIndex := 0;
    ModoTela('P');
  end;
end;

procedure TfrmCadastroInscricoes.AlterarDadosFicha;
begin
  //Procedimento para inserir os dados da ficha de inscri��o
  if (Application.MessageBox ('Confirma a altera��o da ficha de inscri��o?',
                              '[Sistema Deca] - Confirma��o de Altera��o',
                              MB_YESNO + MB_ICONQUESTION) = idYes) then

  begin
    with dmTriagemDeca.cdsUpd_Cadastro_TriagemDeca do
    begin


      //Repassa os valores para os par�metros e executa o insert
      Close;
      Params.ParamByName ('@pe_num_inscricao').Value                              := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_inscricao').AsInteger; //StrToInt(mskNUMERO_INSCRICAO.Text);
      Params.ParamByName ('@pe_num_inscricao_digt').Value                         := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_inscricao_digt').AsInteger; //StrToInt(mskINSCRICAO_DIGITO.Text);
      Params.ParamByName ('@pe_dta_inscricao').AsDateTime                         := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dta_inscricao').Value;

      if (mskDATA_MATRICULA.Text = '  /  /    ') then
        Params.ParamByName ('@pe_dat_matricula').Value                            := Null
      else Params.ParamByName ('@pe_dat_matricula').Value                         := GetValue(mskDATA_MATRICULA, vtDate);


      if (mskDATA_READMISSAO.Text = '  /  /    ') then
        Params.ParamByName ('@pe_dat_readmissao').Value                           := Null
      else Params.ParamByName ('@pe_dat_readmissao').Value                        := GetValue(mskDATA_READMISSAO, vtDate);

      if (mskDATA_RENOVACAO.Text = '  /  /    ') then
        Params.ParamByName ('@pe_dta_renovacao').Value                            := Null
      else Params.ParamByName ('@pe_dta_renovacao').Value                         := GetValue(mskDATA_RENOVACAO, vtDate);

      //Pontua��o
      Params.ParamByName ('@pe_num_pontuacao').Value                              := 0;
      //Usu�rio
      Params.ParamByName ('@pe_cod_UsuarioCadastro').Value                        := vvCOD_USUARIO;
      Params.ParamByName ('@pe_dsc_inscritopor').Value                            := Copy(vvNOM_USUARIO,1,50);
      Params.ParamByName ('@pe_cod_situacao').Value                               := vListaSituacao1.Strings[cmbSITUACAO_INSCRICAO.ItemIndex];
      //N�mero da Portaria
      // ATRIBUI O VALOR 17 (NUMERO DA ATUAL PORTARIA - DEZ/2017)
      Params.ParamByName ('@pe_num_portaria').Value                               := 17;
      //Params.ParamByName ('@pe_num_portaria').Value                             := cmbNUMERO_PORTARIA.ItemIndex;
      Params.ParamByName ('@pe_nom_nome').Value                                   := GetValue(edtNOME_COMPLETO_INSCRITO.Text);
      Params.ParamByName ('@pe_dat_nascimento').Value                             := GetValue(mskDATA_NASCIMENTO_INSCRITO, vtDate);
      Params.ParamByName ('@pe_cod_estadocivil').Value                            := StrToInt(vListaEstadoCivil1.Strings[cmbESTADO_CIVIL_INSCRITO.ItemIndex]);
      Params.ParamByName ('@pe_ind_sexo').Value                                   := cmbSEXO_INSCRITO.ItemIndex;
      Params.ParamByName ('@pe_cod_cidade').AsInteger                             := StrToInt(vListaNaturalidade1.Strings[cmbNATURALIDADE_INSCRITO.ItemIndex]);
      Params.ParamByName ('@pe_cod_nacionalidade').AsInteger                      := StrToInt(vListaNacionalidade1.Strings[cmbNACIONALIDADE_INSCRITO.ItemIndex]);
      Params.ParamByName ('@pe_dsc_endereco').Value                               := GetValue(edtENDERECO_INSCRITO.Text);
      Params.ParamByName ('@pe_num_NumResidencia').Value                          := GetValue(edtNUMERO_RESIDENCIA_INSCRITO.Text);
      Params.ParamByName ('@pe_dsc_Complemento').Value                            := GetValue(edtCOMPLEMENTO_ENDERECO_INSCRITO.Text);

      //mskCEP_RESIDENCIAL_INSCRITO.EditMask := '';
      Params.ParamByName ('@pe_num_cep').Value                                    := GetValue(mskCEP_RESIDENCIAL_INSCRITO.Text);
      //mskCEP_RESIDENCIAL_INSCRITO.EditMask := '99999-999';

      Params.ParamByName ('@pe_dsc_PontoRef').Value                               := GetValue(edtPONTO_REFERENCIA_RESIDENCIA.Text);
      Params.ParamByName ('@pe_ind_ResideFam').Value                              := cmbRESIDE_FAMILIA.ItemIndex;
      Params.ParamByName ('@pe_dsc_NResideFam').Value                             := GetValue(edtN_RESIDE_FAM.Text);
      Params.ParamByName ('@pe_cod_regiao').AsInteger                             := StrToInt(vListaRegiao1.Strings[cmbREGIAO.ItemIndex]);
      Params.ParamByName ('@pe_num_TempoResidencia').AsInteger                    := StrToInt(edtTEMPO_RESIDENCIA.Text);
      Params.ParamByName ('@pe_cod_bairro').AsInteger                             := StrToInt(vListaBairros1.Strings[cmbBAIRRO_INSCRITO.ItemIndex]);
      Params.ParamByName ('@pe_cod_unidade_referencia').AsInteger                 := StrToInt(vListaUnidade1.Strings[cmbUNIDADE_REFERENCIA_PARA_INSCRITO.ItemIndex]);
      Params.ParamByName ('@pe_num_cartao_sus').Value                             := GetValue(edtNUMERO_CARTAO_SUS_INSCRITO.Text);
      Params.ParamByName ('@pe_num_cra').Value                                    := GetValue(edtNUMERO_CRA_INSCRITO.Text);
      Params.ParamByName ('@pe_num_nis').Value                                    := GetValue(edtNUMERO_NIS_INSCRITO.Text);
      Params.ParamByName ('@pe_num_cartao_passe').Value                           := GetValue(edtNUMERO_CARTAO_PASSE_INSCRITO.Text);
      Params.ParamByName ('@pe_cod_sias').Value                                   := GetValue(edtNUMERO_CARTAO_SIAS_INSCRITO.Text);
      Params.ParamByName ('@pe_dsc_NumCertNasc').Value                            := GetValue(edtCERTIDAO_NASCIMENTO_INSCRITO.Text);
      Params.ParamByName ('@pe_dsc_CPF').Value                                    := GetValue(mskNUMERO_CPF_INSCRITO.Text);
      Params.ParamByName ('@pe_dsc_RG').Value                                     := GetValue(edtNUMERO_RG_INSCRITO.Text);

      if (mskDATA_EMISSAO_RG_INSCRITO.Text = '  /  /    ') then
        Params.ParamByName ('@pe_dat_emissao_rg').Value                           := Null
      else Params.ParamByName ('@pe_dat_emissao_rg').Value                        := GetValue(mskDATA_EMISSAO_RG_INSCRITO, vtDate);

      Params.ParamByName ('@pe_cod_emissor').Value                                := StrToInt(vListaEmissor1.Strings[cmbORGAO_EMISSOR_RG_INSCRITO.ItemIndex]);
      Params.ParamByName ('@pe_num_fone_contato').Value                           := GetValue(medTELEFONES_CONTATOS_INSCRITO.Text);

      //if (vvIND_PERFIL = 3) then
      //begin
        Params.ParamByName ('@pe_ind_SeMatric').Value                             := rdgESTA_MATRICULADO.ItemIndex;
        Params.ParamByName ('@pe_dsc_motivo_nestuda').Value                       := GetValue(edtMOTIVO_NAO_ESTUDA.Text);
        Params.ParamByName ('@pe_dsc_NomeEscola').Value                           := GetValue(cmbESCOLA_ATUAL_INSCRITO.Text);
        Params.ParamByName ('@pe_dsc_escolaSerie').Value                          := GetValue(cmbSERIE_ATUAL_INSCRITO.Text);
        Params.ParamByName ('@pe_dsc_escolaPeriodo').Value                        := cmbPERIODO_ESCOLAR_INSCRITO.Text;
        Params.ParamByName ('@pe_dsc_ra').Value                                   := GetValue(edtNUMERO_RA_INSCRITO.Text);
        Params.ParamByName ('@pe_ind_idadeXserie').Value                          := cmbRELACAO_IDADE_SERIE_INSCRITO.ItemIndex;

        if (mskDATA_ATUALIZACAO_ESCOLA.Text = '  /  /    ') then
          Params.ParamByName ('@pe_dta_atualizaEscolar').Value                    := Null
        else Params.ParamByName ('@pe_dta_atualizaEscolar').Value                 := GetValue(mskDATA_ATUALIZACAO_ESCOLA, vtDate);

        Params.ParamByName ('@pe_dsc_razaoincomp').Value                          := GetValue(medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE.Text);
        Params.ParamByName ('@pe_dsc_DiagSaude').Value                            := GetValue(edtDIAGNOSTICO_SAUDE_INSCRITO.Text);

        //Validar o preenchimento do campo chkINVALIDEZ_TEMPORARIA_INSCRITO
        if (chkINVALIDEZ_TEMPORARIA_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_invalidez_temporaria').Value := 1 else Params.ParamByName ('@pe_ind_invalidez_temporaria').Value := 0;
        //Validar o preenchimento do campo chkINVALIDEZ_PERMANENTE_INSCRITO
        if (chkINVALIDEZ_PERMANENTE_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_invalidez_permanente').Value := 1 else Params.ParamByName ('@pe_ind_invalidez_permanente').Value := 0;
        //Validar o campo chkPORTADOR_DOENCA_INFECTO_INSCRITO
        if (chkPORTADOR_DOENCA_INFECTO_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_DoencaInfecto').Value := 1 else Params.ParamByName ('@pe_ind_DoencaInfecto').Value := 0;
        //Validar o campo chkPORTADOR_DOENCA_INFECTO_INSCRITO
        if (chkINCAPACIDADE_TRABALHO_INSCRITO.Checked) then Params.ParamByName ('@pe_ind_IncapazTrab').Value := 1 else Params.ParamByName ('@pe_ind_IncapazTrab').Value := 0;

        //Params.ParamByName ('@pe_num_despmedicas').Value                          := GetValue(edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text);
        Params.ParamByName ('@pe_num_despmedicas').Value                          := StrToFloat(edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text);

        //Validar o campo chkCOMPROVOU_DESPESAS_MEDICAS
        if (chkCOMPROVOU_DESPESAS_MEDICAS.Checked) then Params.ParamByName ('@pe_ind_comprovadespmedic').Value := 1 else Params.ParamByName ('@pe_ind_comprovadespmedic').Value := 0;
        if (chkMOTIVO_PROCURA_TRABALHO.Checked) then Params.ParamByName ('@pe_ind_proc_trabalho').Value := 1 else Params.ParamByName ('@pe_ind_proc_trabalho').Value := 0;
        if (chkMOTIVO_PROCURA_CURSOS.Checked) then Params.ParamByName ('@pe_ind_proc_cursos').Value := 1 else Params.ParamByName ('@pe_ind_proc_cursos').Value := 0;
        if (chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO.Checked) then Params.ParamByName ('@pe_ind_proc_nao_ficar_sozinho').Value := 1 else Params.ParamByName ('@pe_ind_proc_nao_ficar_sozinho').Value := 0;
        if (chkMOTIVO_PROCURA_TIRAR_RUA.Checked) then Params.ParamByName ('@pe_ind_proc_tirar_rua').Value := 1 else Params.ParamByName ('@pe_ind_proc_tirar_rua').Value := 0;
        if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS.Checked) then Params.ParamByName ('@pe_ind_proc_enc_creas').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_creas').Value := 0;
        if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS.Checked) then Params.ParamByName ('@pe_ind_proc_enc_cras').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_cras').Value := 0;
        if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ.Checked) then Params.ParamByName ('@pe_ind_proc_enc_vij').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_vij').Value := 0;
        if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS.Checked) then Params.ParamByName ('@pe_ind_proc_enc_as').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_as').Value := 0;
        if (chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR.Checked) then Params.ParamByName ('@pe_ind_proc_enc_ct').Value := 1 else Params.ParamByName ('@pe_ind_proc_enc_ct').Value := 0;
        if (chkMOTIVO_PROCURA_OUTROS.Checked) then Params.ParamByName ('@pe_ind_proc_outros').Value := 1 else Params.ParamByName ('@pe_ind_proc_outros').Value := 0;

        Params.ParamByName ('@pe_dsc_MotivoProcuraCompl').Value                   := GetValue(medMOTIVO_PROCURA_OUTROS.Text);

        if (chkIMOVEL_OUTRO.Checked) then Params.ParamByName ('@pe_ind_outroimovel').Value := 1 else Params.ParamByName ('@pe_ind_outroimovel').Value := 0;
        if (chkPOSSUI_CARRO.Checked) then Params.ParamByName ('@pe_ind_carro').Value := 1 else Params.ParamByName ('@pe_ind_carro').Value := 0;

        Params.ParamByName ('@pe_dsc_carromodel').Value                           := GetValue(edtCARRO_MODELO.Text);
        Params.ParamByName ('@pe_num_carroano').Value                             := GetValue(edtCARRO_ANO.Text);

        if (chkPOSSUI_MOTO.Checked) then Params.ParamByName ('@pe_ind_moto').Value := 1 else Params.ParamByName ('@pe_ind_moto').Value := 0;

        Params.ParamByName ('@pe_dsc_motomodel').Value                            := GetValue(edtMOTO_MODELO.Text);
        Params.ParamByName ('@pe_num_motoano').Value                              := GetValue(edtMOTO_ANO.Text);

        if (chkUSO_VEICULO_OUTRAS_PRIORIDADES.Checked) then Params.ParamByName ('@pe_ind_veiculo_outras_priori').Value := 1 else Params.ParamByName ('@pe_ind_veiculo_outras_priori').Value := 0;
        if (chkUSO_VEICULO_TRABALHO.Checked) then Params.ParamByName ('@pe_ind_veiculo_trabalho').Value := 1 else Params.ParamByName ('@pe_ind_veiculo_trabalho').Value := 0;
        if (chkUSO_VEICULO_UNICO_MEIO_TRANSP.Checked) then Params.ParamByName ('@pe_ind_veiculo_unico_transp').Value := 1 else Params.ParamByName ('@pe_ind_veiculo_unico_transp').Value := 0;

        Params.ParamByName ('@pe_cod_RelResp').Value                              := vListaRelacaoResponsabilidade1.Strings[cmbRELACAO_RESPONSABILIDADE.ItemIndex];

        if (chkAGRAVANTE_DROGADICTO.Checked) then Params.ParamByName ('@pe_ind_drogadicto').Value := 1 else Params.ParamByName ('@pe_ind_drogadicto').Value := 0;
        if (chkAGRAVANTE_ALCOOLATRA.Checked) then Params. ParamByName ('@pe_ind_alcoolatra').Value := 1 else Params.ParamByName ('@pe_ind_alcoolatra').Value := 0;
        if (chkAGRAVANTE_PAIMAE_ADOLESCENTE.Checked) then Params. ParamByName ('@pe_ind_paimaeadolescente').Value := 1 else Params.ParamByName ('@pe_ind_paimaeadolescente').Value := 0;
        if (chkAGRAVANTE_GESTANTE.Checked) then Params. ParamByName ('@pe_ind_gestante').Value := 1 else Params.ParamByName ('@pe_ind_gestante').Value := 0;
        if (chkAGRAVANTE_USO_PSICOTROPICOS.Checked) then Params. ParamByName ('@pe_ind_medicamentopsico').Value := 1 else Params.ParamByName ('@pe_ind_medicamentopsico').Value := 0;
        if (chkAGRAVANTE_MEDIDAS_SOCIOEDUCATIVAS.Checked) then Params. ParamByName ('@pe_ind_medidas_socioeducativas').Value := 1 else Params.ParamByName ('@pe_ind_medidas_socioeducativas').Value := 0;

        if (chkSIT_RISCO_CREAS.Checked) then Params. ParamByName ('@pe_ind_aquarela').Value := 1 else Params.ParamByName ('@pe_ind_aquarela').Value := 0;
        if (chkSIT_RISCO_VIOLENCIA.Checked) then Params. ParamByName ('@pe_ind_violencia').Value := 1 else Params.ParamByName ('@pe_ind_violencia').Value := 0;
        if (chkSIT_RISCO_VIOLENCIA_FISICA.Checked) then Params. ParamByName ('@pe_ind_violencia_fisica').Value := 1 else Params.ParamByName ('@pe_ind_violencia_fisica').Value := 0;
        if (chkSIT_RISCO_VIOLENCIA_PSICOLOGICA.Checked) then Params. ParamByName ('@pe_ind_violencia_psicologica').Value := 1 else Params.ParamByName ('@pe_ind_violencia_psicologica').Value := 0;
        if (chkSIT_RISCO_VIOLENCIA_SEXUAL.Checked) then Params. ParamByName ('@pe_ind_violencia_sexual').Value := 1 else Params.ParamByName ('@pe_ind_violencia_sexual').Value := 0;
        if (chkSIT_RISCO_VIOLENCIA_NEGLIGENCIA.Checked) then Params. ParamByName ('@pe_ind_violencia_negligencia').Value := 1 else Params.ParamByName ('@pe_ind_violencia_negligencia').Value := 0;
        if (chkSIT_RISCO_VIOLENCIA_ABANDONO.Checked) then Params. ParamByName ('@pe_ind_violencia_abandono').Value := 1 else Params.ParamByName ('@pe_ind_violencia_abandono').Value := 0;

        if (chkSIT_RISCO_PROSTITUICAO.Checked) then Params. ParamByName ('@pe_ind_prostituicao').Value := 1 else Params.ParamByName ('@pe_ind_prostituicao').Value := 0;
        if (chkSIT_RISCO_TRAFICO_DROGAS.Checked) then Params. ParamByName ('@pe_ind_traficodrogas').Value := 1 else Params.ParamByName ('@pe_ind_traficodrogas').Value := 0;
        if (chkSIT_RISCO_CONVIVE_DROGADICCAO.Checked) then Params. ParamByName ('@pe_ind_convivedrogadiccao').Value := 1 else Params.ParamByName ('@pe_ind_convivedrogadiccao').Value := 0;
        if (chkSIT_RISCO_CONVIVE_ALCOOLATRA.Checked) then Params. ParamByName ('@pe_ind_convivealcool').Value := 1 else Params.ParamByName ('@pe_ind_convivealcool').Value := 0;
        if (chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_MENTAIS.Checked) then Params. ParamByName ('@pe_ind_conviveportdoencamental').Value := 1 else Params.ParamByName ('@pe_ind_conviveportdoencamental').Value := 0;
        if (chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INFECTO.Checked) then Params. ParamByName ('@pe_ind_conviveportdoencainfecto').Value := 1 else Params.ParamByName ('@pe_ind_conviveportdoencainfecto').Value := 0;
        if (chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INCAPACITANTES.Checked) then Params. ParamByName ('@pe_ind_conv_port_doencas_incap').Value := 1 else Params.ParamByName ('@pe_ind_conv_port_doencas_incap').Value := 0;
        if (chkSIT_RISCO_MEMBRO_ATOS_INFRACIONAIS.Checked) then Params. ParamByName ('@pe_ind_membroautorinfracao').Value := 1 else Params.ParamByName ('@pe_ind_membroautorinfracao').Value := 0;
        if (chkSIT_RISCO_FICAM_SOZINHAS.Checked) then Params. ParamByName ('@pe_ind_criancasozinha').Value := 1 else Params.ParamByName ('@pe_ind_criancasozinha').Value := 0;

        Params.ParamByName ('@pe_cod_natureza_hab').Value                         := vListaNaturezaHabitacao1.Strings[cmbNATUREZA_HABITACAO.ItemIndex];
        Params.ParamByName ('@pe_dsc_valoraluguel').Value                         := StrToFloat(edtVALOR_ALUGUEL_INFORMADO.Text);

        if (chkCOMPROVOU_ALUGUEL.Checked) then Params.ParamByName ('@pe_ind_comprovoualuguel').Value := 1 else Params.ParamByName ('@pe_ind_comprovoualuguel').Value := 0;

        Params.ParamByName ('@pe_dsc_observacao').Value                           := GetValue(edtOBSERVACOES.Text);
        Params.ParamByName ('@pe_cod_tipo_hab').Value                             := vListaTipoHabitacao1.Strings[cmbTIPO_HABITACAO.ItemIndex];
        Params.ParamByName ('@pe_cod_infra_estrutura').Value                      := vListaInfraestrutura1.Strings[cmbINFRAESTRUTURA.ItemIndex];
        Params.ParamByName ('@pe_num_qtdehabitantes').Value                       := GetValue(edtQTD_HABITANTES.Text);
        Params.ParamByName ('@pe_num_qtdecomodos').Value                          := GetValue(edtQTD_COMODOS.Text);
        Params.ParamByName ('@pe_cod_inst_sanitaria').Value                       := vListaInstalacoesSantitarias1.Strings[cmbINSTALACOES_SANITARIAS.ItemIndex];
        Params.ParamByName ('@pe_cod_cond_hab').Value                             := vListaCondicaoesHabitabilidade1.Strings[cmbCONDICOES_HABITABILIDADE.ItemIndex];
        
      //end;

      //Se passou na valida��o dos campos, independente do perfil...
      if (vOK = True) or (vOK_Adm = True) then
      begin
        Execute;
        PageControl4.ActivePageIndex := 0;
        mmPODE_MUDAR_ABA := True;
        ModoTela('P');
      end;
    end; //with dmTriagemDeca.cdsIns_Cadastro_TriagemDeca do
  end
  else
  begin
    PageControl4.ActivePageIndex := 0;
    mmPODE_MUDAR_ABA := True;
    ModoTela('P');
  end;
end;

procedure TfrmCadastroInscricoes.btnCHAMARPESQUISA_INSCRICAO_ABA_DADOSINSCRITO_DADOSCADASTRAISClick(
  Sender: TObject);
begin
  frmCadastroInscricoes.Close;
  frmPESQUISA_INSCRITOS_TRIAGEM.Show;
end;

procedure TfrmCadastroInscricoes.PageControl4Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if mmPODE_MUDAR_ABA = False then
    AllowChange := False;
end;

procedure TfrmCadastroInscricoes.dbgBENEFICIOS_SOCIAISDblClick(
  Sender: TObject);
begin
  gpbBENEFICIO.Enabled          := True;
  gpbVALOR_BENEFICIO.Enabled    := True;
  edtDESCRICAO_BENEFICIO.Text   := dmTriagemDeca.cdsSel_BeneficiosSociais.FieldByName ('dsc_beneficio').Value;
  edtVALOR_BENEFICIO.Text       := dmTriagemDeca.cdsSel_BeneficiosSociais.FieldByName ('num_valorbeneficio').Value;

  btnNOVO_BENEFICIO.Enabled    := False;
  btnSALVA_BENEFICIO.Enabled   := False;
  btnALTERA_BENEFICIO.Enabled  := True;
  btnEXCLUI_BENEFICIO.Enabled  := False;
  btnCANCELA.Enabled           := True;
end;

procedure TfrmCadastroInscricoes.btnINCLUIR_BENEFICIOClick(
  Sender: TObject);
begin
  btnNOVO_BENEFICIO.Enabled    := False;
  btnSALVA_BENEFICIO.Enabled   := True;
  btnALTERA_BENEFICIO.Enabled  := False;
  btnEXCLUI_BENEFICIO.Enabled  := False;
  btnCANCELA.Enabled           := True;
end;

procedure TfrmCadastroInscricoes.btnALTERAR_BENEFICIOClick(
  Sender: TObject);
begin
  btnNOVO_BENEFICIO.Enabled    := True;
  btnSALVA_BENEFICIO.Enabled   := False;
  btnALTERA_BENEFICIO.Enabled  := False;
  btnEXCLUI_BENEFICIO.Enabled  := True;
  btnCANCELA.Enabled           := True;
end;

procedure TfrmCadastroInscricoes.btnNOVO_BENEFICIOClick(Sender: TObject);
begin
  btnNOVO_BENEFICIO.Enabled       := False;
  btnSALVA_BENEFICIO.Enabled      := True;
  btnALTERA_BENEFICIO.Enabled     := False;
  btnEXCLUI_BENEFICIO.Enabled     := False;
  btnCANCELA.Enabled              := True;
  gpbBENEFICIO.Enabled            := True;
  gpbVALOR_BENEFICIO.Enabled      := True;
  edtDESCRICAO_BENEFICIO.Clear;
  edtVALOR_BENEFICIO.Clear;
end;

procedure TfrmCadastroInscricoes.btnSALVA_BENEFICIOClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsIns_BeneficioSocial do
    begin
      Close;
      Params.ParamByName ('@Cod_inscricao').Value      := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
      Params.ParamByName ('@dsc_beneficio').Value      := Trim(edtDESCRICAO_BENEFICIO.Text);
      Params.ParamByName ('@num_valorbeneficio').Value := StrToFloat(edtVALOR_BENEFICIO.Text);
      Execute;

      with dmTriagemDeca.cdsSel_BeneficiosSociais do
      begin
        Close;
        Params.ParamByName ('@cod_idBeneficio').Value := Null;
        Params.ParamByName ('@Cod_inscricao').Value   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
        Open;
        dbgBENEFICIOS_SOCIAIS.Refresh;
      end;

      btnNOVO_BENEFICIO.Enabled       := True;
      btnSALVA_BENEFICIO.Enabled      := False;
      btnALTERA_BENEFICIO.Enabled     := False;
      btnEXCLUI_BENEFICIO.Enabled     := True;
      btnCANCELA.Enabled              := False;
      edtDESCRICAO_BENEFICIO.Clear;
      edtVALOR_BENEFICIO.Clear;
      gpbBENEFICIO.Enabled            := False;
      gpbVALOR_BENEFICIO.Enabled      := False;
    end;
  except end;
end;

procedure TfrmCadastroInscricoes.btnALTERA_BENEFICIOClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsUpd_BeneficioSocial do
    begin
      Close;
      Params.ParamByName ('@Cod_inscricao').Value      := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
      Params.ParamByName ('@cod_idBeneficio').Value    := dmTriagemDeca.cdsSel_BeneficiosSociais.FieldByName ('cod_idBeneficio').Value;
      Params.ParamByName ('@dsc_beneficio').Value      := Trim(edtDESCRICAO_BENEFICIO.Text);
      Params.ParamByName ('@num_valorbeneficio').Value := StrToFloat(edtVALOR_BENEFICIO.Text);
      Execute;

      with dmTriagemDeca.cdsSel_BeneficiosSociais do
      begin
        Close;
        Params.ParamByName ('@cod_idBeneficio').Value := Null;
        Params.ParamByName ('@Cod_inscricao').Value   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
        Open;
        dbgBENEFICIOS_SOCIAIS.Refresh;
      end;

      btnNOVO_BENEFICIO.Enabled       := True;
      btnSALVA_BENEFICIO.Enabled      := False;
      btnALTERA_BENEFICIO.Enabled     := False;
      btnEXCLUI_BENEFICIO.Enabled     := True;
      btnCANCELA.Enabled              := False;
      edtDESCRICAO_BENEFICIO.Clear;
      edtVALOR_BENEFICIO.Clear;
      gpbBENEFICIO.Enabled            := False;
      gpbVALOR_BENEFICIO.Enabled      := False;
    end;
  except end;
end;

procedure TfrmCadastroInscricoes.btnCANCELAClick(Sender: TObject);
begin
  btnNOVO_BENEFICIO.Enabled       := True;
  btnSALVA_BENEFICIO.Enabled      := False;
  btnALTERA_BENEFICIO.Enabled     := False;
  btnEXCLUI_BENEFICIO.Enabled     := True;
  btnCANCELA.Enabled              := False;
  gpbBENEFICIO.Enabled            := False;
  gpbVALOR_BENEFICIO.Enabled      := False;
  with dmTriagemDeca.cdsSel_BeneficiosSociais do
  begin
    Close;
    Params.ParamByName ('@cod_idBeneficio').Value := Null;
    Params.ParamByName ('@Cod_inscricao').Value   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
    Open;
    dbgBENEFICIOS_SOCIAIS.Refresh;
  end;
end;

procedure TfrmCadastroInscricoes.btnEXCLUI_BENEFICIOClick(Sender: TObject);
begin
  try
    with dmTriagemDeca.cdsDel_BeneficioSocial do
    begin
      Close;
      Params.ParamByName ('@cod_idBeneficio').Value := dmTriagemDeca.cdsSel_BeneficiosSociais.FieldByName ('cod_idBeneficio').Value;
      Execute;

      with dmTriagemDeca.cdsSel_BeneficiosSociais do
      begin
        Close;
        Params.ParamByName ('@cod_idBeneficio').Value := Null;
        Params.ParamByName ('@Cod_inscricao').Value   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
        Open;
        dbgBENEFICIOS_SOCIAIS.Refresh;
      end;

      btnNOVO_BENEFICIO.Enabled       := True;
      btnSALVA_BENEFICIO.Enabled      := False;
      btnALTERA_BENEFICIO.Enabled     := False;
      btnEXCLUI_BENEFICIO.Enabled     := True;
      btnCANCELA.Enabled              := False;
    end;
  except end;
end;

procedure TfrmCadastroInscricoes.btnACESSO_CONSULTASUSClick(
  Sender: TObject);
begin
  ShellExecute(handle, 'open', 'https://portaldocidadao.saude.gov.br/portalcidadao/verificarSePossuiCNS.htm', nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmCadastroInscricoes.cmbSEXO_INSCRITOExit(Sender: TObject);
begin
  if cmbSEXO_INSCRITO.ItemIndex = 0 then chkAGRAVANTE_GESTANTE.Enabled := False
  else chkAGRAVANTE_GESTANTE.Enabled := True;
end;

procedure TfrmCadastroInscricoes.btnGRAVAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
  Sender: TObject);
begin
  try
    ValidaCamposFamilia;
    if (vOK = True) then
    begin
      if (vTipo_Edicao = 'I') then InserirDadosFamilia else if (vTipo_Edicao = 'U') then AlterarDadosFamilia;
      vTipo_Edicao := '';
      LimpaCamposFamilia;
    end
  except
    Application.MessageBox ('Aten��o!!!' +#13+#10+
                            'Um erro ocorreu e os dados n�o foram salvos no banco de dados.' +#13+#10+
                            'Tente novamente ou entre em contato com o Suporte',
                            '[Sistema Deca] - Erro salvando dados da Composi��o Familiar',
                            MB_OK + MB_ICONERROR);
    ModoTelaFamilia('P');
    PageControl4.ActivePageIndex := 0;
  end;

end;

procedure TfrmCadastroInscricoes.InserirDadosFamilia;
begin
  //Recuperar o c�digo da inscri��o para a inclus�o de membro ad Composi��o Familiar
  with dmTriagemDeca.cdsSel_CadastroFull do
  begin
    Close;
    Params.ParamByName ('@pe_cod_inscricao').Value      := Null;
    Params.ParamByName ('@pe_num_inscricao').Value      := StrToInt(mskNUMERO_INSCRICAO.Text);
    Params.ParamByName ('@pe_num_inscricao_digt').Value := StrToInt(mskINSCRICAO_DIGITO.Text);
    Open;
    vvCOD_INSCRICAO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
  end;

  //Procedimento para inserir os dados de composi��o familiar...
  if (Application.MessageBox ('Confirma a inclus�o de NOVO membro familiar?',
                              '[Sistema Deca] - Confirma��o de Inclus�o',
                              MB_YESNO + MB_ICONQUESTION) = idYes) then

  begin
    with dmTriagemDeca.cdsFamilia_InsUpdDel do
    begin
       //Repassa os valores para os par�metros e executa o insert
      Close;
      Params.ParamByName ('@acao').Value                     := 'i';
      Params.ParamByName ('@cod_inscricao').Value            := vvCOD_INSCRICAO; //dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
      Params.ParamByName ('@dsc_nome_fam').Value             := Trim(edtNOME_COMPLETO_FAMILIAR.Text);

      if (chkFAMILIAR_RESPONSAVEL.Checked = True) then Params.ParamByName ('@ind_responsavel').Value := 1 else Params.ParamByName ('@ind_responsavel').Value := 0;

      if (cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex = -1) then Params.ParamByName ('@cod_parentesco').Value         := Null else Params.ParamByName ('@cod_parentesco').Value        := vListaParentesco1.Strings[cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex];
      if (cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex = -1) then Params.ParamByName ('@cod_escolaridade').Value    := Null else Params.ParamByName ('@cod_escolaridade').Value      := vListaEscolaridade1.Strings[cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex];
      if (cmbFAMILIAR_MORA_FAMILIA.ItemIndex    = -1) then Params.ParamByName ('@ind_cond_morafamilia').Value   := Null else Params.ParamByName ('@ind_cond_morafamilia').Value  := cmbFAMILIAR_MORA_FAMILIA.ItemIndex;
      if (cmbSEXO_FAMILIAR.ItemIndex = -1)            then Params.ParamByName ('@ind_sexo_par').Value           := Null else Params.ParamByName ('@ind_sexo_par').Value          := cmbSEXO_FAMILIAR.ItemIndex;
      if (cmbESTADO_CIVIL_FAMILIAR.ItemIndex = -1)    then Params.ParamByName ('@ind_estadocivil_par').Value    := Null else Params.ParamByName ('@ind_estadocivil_par').Value   := vListaEstadoCivil1.Strings[cmbESTADO_CIVIL_FAMILIAR.ItemIndex];
      if (cmbFAMILIAR_POSSUI_GUARDA.ItemIndex = -1)   then Params.ParamByName ('@ind_temguarda').Value          := Null else Params.ParamByName ('@ind_temguarda').Value         := cmbFAMILIAR_POSSUI_GUARDA.ItemIndex;

      if ( (Length(edtRENDA_MENSAL_FAMILIAR.Text) = 0)) then Params.ParamByName ('@num_rendamensal_fam').Value  := 0 else Params.ParamByName ('@num_rendamensal_fam').Value      := StrToFloat(edtRENDA_MENSAL_FAMILIAR.Text);

      if (mskDATA_NASCIMENTO_FAMILIAR.Text = '  /  /    ') then Params.ParamByName ('@dat_nasc_fam').Value      := Null else Params.ParamByName ('@dat_nasc_fam').Value          := GetValue(mskDATA_NASCIMENTO_FAMILIAR, vtDate);

      if (mskNUMERO_CPF_FAMILIAR.Text = '   .   .   -  ') then  Params.ParamByName ('@dsc_cpf_fam').Value       := Null else Params.ParamByName ('@dsc_cpf_fam').Value           := GetValue(mskNUMERO_CPF_FAMILIAR.Text);

      Params.ParamByName ('@dsc_rg_fam').Value               := GetValue(edtNUMERO_RG_FAMILIAR.Text);

      if (mskDATA_EMISSAO_RG_FAMILIAR.Text = '  /  /    ') then Params.ParamByName ('@pe_dat_emissao_rg').Value := Null else Params.ParamByName ('@pe_dat_emissao_rg').Value     := GetValue(mskDATA_EMISSAO_RG_FAMILIAR, vtDate);

      if (cmbORGAO_EMISSOR_RG_FAMILIAR.ItemIndex = -1) then Params.ParamByName ('@pe_cod_emissor').Value        := Null else Params.ParamByName ('@pe_cod_emissor').Value        := StrToInt(vListaEmissor1.Strings[cmbORGAO_EMISSOR_RG_FAMILIAR.ItemIndex]);
      if (cmbNATURALIDADE_FAMILIAR.ItemIndex     = -1) then Params.ParamByName ('@pe_cod_cidade').Value         := Null else Params.ParamByName ('@pe_cod_cidade').Value         := StrToInt(vListaNaturalidade1.Strings[cmbNATURALIDADE_FAMILIAR.ItemIndex]);
      if (cmbNACIONALIDADE_FAMILIAR.ItemIndex    = -1) then Params.ParamByName ('@pe_cod_nacionalidade').Value  := Null else Params.ParamByName ('@pe_cod_nacionalidade').Value  := StrToInt(vListaNacionalidade1.Strings[cmbNACIONALIDADE_FAMILIAR.ItemIndex]);

      Params.ParamByName ('@pe_num_cartao_sus').Value        := GetValue(edtNUMERO_CARTAO_SUS_FAMILIAR.Text);
      Params.ParamByName ('@pe_num_cra').Value               := GetValue(edtNUMERO_CRA_FAMILIAR.Text);
      Params.ParamByName ('@pe_num_nis').Value               := GetValue(edtNUMERO_NIS_FAMILIAR.Text);
      Params.ParamByName ('@pe_num_cartao_passe').Value      := GetValue(edtNUMERO_CARTAO_PASSE_FAMILIAR.Text);
      Params.ParamByName ('@dsc_numcertnasc_fam').Value      := GetValue(edtCERTIDAO_NASCIMENTO_FAMILIAR.Text);
      Params.ParamByName ('@dsc_localtrab_fam').Value        := GetValue(edtLOCAL_TRABALHO_FAMILIAR.Text);
      Params.ParamByName ('@dsc_tel_fam').Value              := GetValue(mskTELEFONE_FAMILIAR.Text);
      Params.ParamByName ('@dsc_ocupacao_fam').Value         := GetValue(edtOCUPACAO_FAMILIAR.Text);
      Params.ParamByName ('@dsc_outrarenda').Value           := GetValue(edtOUTRAS_RENDAS_FAMILIAR.Text);

      if (vvIND_PERFIL <> 3) then
      begin
        Params.ParamByName ('@pe_ind_inv_temp').Value        := Null;
        Params.ParamByName ('@pe_ind_inv_perm').Value        := Null;
        Params.ParamByName ('@ind_incapaztrab').Value        := Null;
        Params.ParamByName ('@ind_portdoencacontagia').Value := Null;
        Params.ParamByName ('@ind_cond_recluso').Value       := Null;
        Params.ParamByName ('@ind_cond_falecido').Value      := Null;
        Params.ParamByName ('@ind_cond_nregcrianca').Value   := Null;
        Params.ParamByName ('@dsc_diagnostico_par').Value    := Null;
        Params.ParamByName ('@dsc_relacionamento_fam').Value := Null;
      end
      else if (vvIND_PERFIL = 3) then
      begin
        //Par�metros para inser��o pelo perfil do Servi�o Social
        if (chkINVALIDEZ_TEMPORARIA_FAMILIAR.Checked)              then Params.ParamByName ('@pe_ind_inv_temp').Value        := 1 else Params.ParamByName ('@pe_ind_inv_temp').Value := 0;
        if (chkINVALIDEZ_PERMANENTE_FAMILIAR.Checked)              then Params.ParamByName ('@pe_ind_inv_perm').Value        := 1 else Params.ParamByName ('@pe_ind_inv_perm').Value := 0;
        if (chkINCAPACIDADE_TRABALHO_FAMILIAR.Checked)             then Params.ParamByName ('@ind_incapaztrab').Value        := 1 else Params.ParamByName ('@ind_incapaztrab').Value := 0;
        if (chkPORTADOR_DOENCA_INFECTO_FAMILIAR.Checked)           then Params.ParamByName ('@ind_portdoencacontagia').Value := 1 else Params.ParamByName ('@ind_portdoencacontagia').Value := 0;
        if (chkFAMILIAR_RECLUSO.Checked)                           then Params.ParamByName ('@ind_cond_recluso').Value       := 1 else Params.ParamByName ('@ind_cond_recluso').Value := 0;
        if (chkFAMILIAR_FALECIDO.Checked)                          then Params.ParamByName ('@ind_cond_falecido').Value      := 1 else Params.ParamByName ('@ind_cond_falecido').Value := 0;
        if (chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE.Checked) then Params.ParamByName ('@ind_cond_nregcrianca').Value   := 1 else Params.ParamByName ('@ind_cond_nregcrianca').Value := 0;

        Params.ParamByName ('@dsc_diagnostico_par').Value    := GetValue(edtDIAGNOSTICO_SAUDE_FAMILIAR.Text);
        Params.ParamByName ('@dsc_relacionamento_fam').Value := GetValue(edtRELACIONAMENTO_FAMILIAR.Text);
      end;

      //Se passou na valida��o dos campos, independente do perfil...
      if (vOK = True) then
      begin
        Execute;
        mmPODE_MUDAR_ABA                           := True;
        PageControl4.ActivePageIndex               := 0;
        pgcAbas_ComposicaoFamiliar.ActivePageIndex := 0;
        ModoTelaFamilia('P');

        with dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar do
        begin
          Close;
          Params.ParamByName ('@Cod_membrofamilia').Value := Null;
          Params.ParamByName ('@Cod_inscricao').Value     := vvCOD_INSCRICAO;
          Open;
          //CarregaDadosFamilia;
          // Reativar o Grid das abas COMPOSICAO_FAMILIAR
          dbgCOMPOSICAO_FAMILIAR.DataSource             := dsCOMPOSICAO_FAMILIAR;
          dbgCOMPOSICAO_FAMILIAR.Refresh;
        end;


      end;
    end; //with dmTriagemDeca.cdsIns_Cadastro_TriagemDeca do
  end
  else
  begin
    mmPODE_MUDAR_ABA                           := True;
    PageControl4.ActivePageIndex               := 0;
    pgcAbas_ComposicaoFamiliar.ActivePageIndex := 0;
    ModoTelaFamilia('P');
    CarregaDadosFamilia;
  end;
end;

procedure TfrmCadastroInscricoes.AlterarDadosFamilia;
begin
  //Procedimento para alterar os dados da composi��o familiar
  //Procedimento para inserir os dados de composi��o familiar...
  if (Application.MessageBox ('Confirma a altera��o dos dados do familiar?',
                              '[Sistema Deca] - Confirma��o de Altera��o',
                              MB_YESNO + MB_ICONQUESTION) = idYes) then

  begin
    with dmTriagemDeca.cdsFamilia_InsUpdDel do
    begin
       //Repassa os valores para os par�metros e executa o insert
      Close;
      Params.ParamByName ('@acao').Value                     := 'e';
      Params.ParamByName ('@cod_inscricao').Value            := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
      Params.ParamByName ('@pe_cod_membrofamilia').Value     := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('cod_membrofamilia').Value;

      Params.ParamByName ('@dsc_nome_fam').Value             := Trim(edtNOME_COMPLETO_FAMILIAR.Text);

      if (chkFAMILIAR_RESPONSAVEL.Checked = True) then Params.ParamByName ('@ind_responsavel').Value := 1 else Params.ParamByName ('@ind_responsavel').Value := 0;

      if (cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex = -1) then Params.ParamByName ('@cod_parentesco').Value         := Null else Params.ParamByName ('@cod_parentesco').Value        := vListaParentesco1.Strings[cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex];
      if (cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex = -1) then Params.ParamByName ('@cod_escolaridade').Value    := Null else Params.ParamByName ('@cod_escolaridade').Value      := vListaEscolaridade1.Strings[cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex];
      if (cmbFAMILIAR_MORA_FAMILIA.ItemIndex    = -1) then Params.ParamByName ('@ind_cond_morafamilia').Value   := Null else Params.ParamByName ('@ind_cond_morafamilia').Value  := cmbFAMILIAR_MORA_FAMILIA.ItemIndex;
      if (cmbSEXO_FAMILIAR.ItemIndex = -1)            then Params.ParamByName ('@ind_sexo_par').Value           := Null else Params.ParamByName ('@ind_sexo_par').Value          := cmbSEXO_FAMILIAR.ItemIndex;
      if (cmbESTADO_CIVIL_FAMILIAR.ItemIndex = -1)    then Params.ParamByName ('@ind_estadocivil_par').Value    := Null else Params.ParamByName ('@ind_estadocivil_par').Value   := vListaEstadoCivil1.Strings[cmbESTADO_CIVIL_FAMILIAR.ItemIndex];
      if (cmbFAMILIAR_POSSUI_GUARDA.ItemIndex = -1)   then Params.ParamByName ('@ind_temguarda').Value          := Null else Params.ParamByName ('@ind_temguarda').Value         := cmbFAMILIAR_POSSUI_GUARDA.ItemIndex;

      if ( (Length(edtRENDA_MENSAL_FAMILIAR.Text) = 0)) then Params.ParamByName ('@num_rendamensal_fam').Value  := 0 else Params.ParamByName ('@num_rendamensal_fam').Value      := StrToFloat(edtRENDA_MENSAL_FAMILIAR.Text);

      if (mskDATA_NASCIMENTO_FAMILIAR.Text = '  /  /    ') then Params.ParamByName ('@dat_nasc_fam').Value      := Null else Params.ParamByName ('@dat_nasc_fam').Value          := GetValue(mskDATA_NASCIMENTO_FAMILIAR, vtDate);

      if (mskNUMERO_CPF_FAMILIAR.Text = '   .   .   -  ') then  Params.ParamByName ('@dsc_cpf_fam').Value       := Null else Params.ParamByName ('@dsc_cpf_fam').Value           := GetValue(mskNUMERO_CPF_FAMILIAR.Text);

      Params.ParamByName ('@dsc_rg_fam').Value               := GetValue(edtNUMERO_RG_FAMILIAR.Text);

      if (mskDATA_EMISSAO_RG_FAMILIAR.Text = '  /  /    ') then Params.ParamByName ('@pe_dat_emissao_rg').Value := Null else Params.ParamByName ('@pe_dat_emissao_rg').Value     := GetValue(mskDATA_EMISSAO_RG_FAMILIAR, vtDate);

      if (cmbORGAO_EMISSOR_RG_FAMILIAR.ItemIndex = -1) then Params.ParamByName ('@pe_cod_emissor').Value        := Null else Params.ParamByName ('@pe_cod_emissor').Value        := StrToInt(vListaEmissor1.Strings[cmbORGAO_EMISSOR_RG_FAMILIAR.ItemIndex]);
      if (cmbNATURALIDADE_FAMILIAR.ItemIndex     = -1) then Params.ParamByName ('@pe_cod_cidade').Value         := Null else Params.ParamByName ('@pe_cod_cidade').Value         := StrToInt(vListaNaturalidade1.Strings[cmbNATURALIDADE_FAMILIAR.ItemIndex]);
      if (cmbNACIONALIDADE_FAMILIAR.ItemIndex    = -1) then Params.ParamByName ('@pe_cod_nacionalidade').Value  := Null else Params.ParamByName ('@pe_cod_nacionalidade').Value  := StrToInt(vListaNacionalidade1.Strings[cmbNACIONALIDADE_FAMILIAR.ItemIndex]);

      Params.ParamByName ('@pe_num_cartao_sus').Value        := GetValue(edtNUMERO_CARTAO_SUS_FAMILIAR.Text);
      Params.ParamByName ('@pe_num_cra').Value               := GetValue(edtNUMERO_CRA_FAMILIAR.Text);
      Params.ParamByName ('@pe_num_nis').Value               := GetValue(edtNUMERO_NIS_FAMILIAR.Text);
      Params.ParamByName ('@pe_num_cartao_passe').Value      := GetValue(edtNUMERO_CARTAO_PASSE_FAMILIAR.Text);
      Params.ParamByName ('@dsc_numcertnasc_fam').Value      := GetValue(edtCERTIDAO_NASCIMENTO_FAMILIAR.Text);
      Params.ParamByName ('@dsc_localtrab_fam').Value        := GetValue(edtLOCAL_TRABALHO_FAMILIAR.Text);
      Params.ParamByName ('@dsc_tel_fam').Value              := GetValue(mskTELEFONE_FAMILIAR.Text);
      Params.ParamByName ('@dsc_ocupacao_fam').Value         := GetValue(edtOCUPACAO_FAMILIAR.Text);
      Params.ParamByName ('@dsc_outrarenda').Value           := GetValue(edtOUTRAS_RENDAS_FAMILIAR.Text);

      if (vvIND_PERFIL <> 3) then
      begin
        Params.ParamByName ('@pe_ind_inv_temp').Value        := Null;
        Params.ParamByName ('@pe_ind_inv_perm').Value        := Null;
        Params.ParamByName ('@ind_incapaztrab').Value        := Null;
        Params.ParamByName ('@ind_portdoencacontagia').Value := Null;
        Params.ParamByName ('@ind_cond_recluso').Value       := Null;
        Params.ParamByName ('@ind_cond_falecido').Value      := Null;
        Params.ParamByName ('@ind_cond_nregcrianca').Value   := Null;
        Params.ParamByName ('@dsc_diagnostico_par').Value    := Null;
        Params.ParamByName ('@dsc_relacionamento_fam').Value := Null;
      end
      else if (vvIND_PERFIL = 3) then
      begin
        //Par�metros para inser��o pelo perfil do Servi�o Social
        if (chkINVALIDEZ_TEMPORARIA_FAMILIAR.Checked)              then Params.ParamByName ('@pe_ind_inv_temp').Value        := 1 else Params.ParamByName ('@pe_ind_inv_temp').Value := 0;
        if (chkINVALIDEZ_PERMANENTE_FAMILIAR.Checked)              then Params.ParamByName ('@pe_ind_inv_perm').Value        := 1 else Params.ParamByName ('@pe_ind_inv_perm').Value := 0;
        if (chkINCAPACIDADE_TRABALHO_FAMILIAR.Checked)             then Params.ParamByName ('@ind_incapaztrab').Value        := 1 else Params.ParamByName ('@ind_incapaztrab').Value := 0;
        if (chkPORTADOR_DOENCA_INFECTO_FAMILIAR.Checked)           then Params.ParamByName ('@ind_portdoencacontagia').Value := 1 else Params.ParamByName ('@ind_portdoencacontagia').Value := 0;
        if (chkFAMILIAR_RECLUSO.Checked)                           then Params.ParamByName ('@ind_cond_recluso').Value       := 1 else Params.ParamByName ('@ind_cond_recluso').Value := 0;
        if (chkFAMILIAR_FALECIDO.Checked)                          then Params.ParamByName ('@ind_cond_falecido').Value      := 1 else Params.ParamByName ('@ind_cond_falecido').Value := 0;
        if (chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE.Checked) then Params.ParamByName ('@ind_cond_nregcrianca').Value   := 1 else Params.ParamByName ('@ind_cond_nregcrianca').Value := 0;

        Params.ParamByName ('@dsc_diagnostico_par').Value    := GetValue(edtDIAGNOSTICO_SAUDE_FAMILIAR.Text);
        Params.ParamByName ('@dsc_relacionamento_fam').Value := GetValue(edtRELACIONAMENTO_FAMILIAR.Text);
      end;

      //Se passou na valida��o dos campos, independente do perfil...
      if (vOK = True) then
      begin
        Execute;
        mmPODE_MUDAR_ABA                           := True;
        PageControl4.ActivePageIndex               := 0;
        pgcAbas_ComposicaoFamiliar.ActivePageIndex := 0;
        ModoTelaFamilia('P');
        CarregaDadosFamilia;
      end;
    end; //with dmTriagemDeca.cdsIns_Cadastro_TriagemDeca do
  end
  else
  begin
    mmPODE_MUDAR_ABA                           := True;
    PageControl4.ActivePageIndex               := 0;
    pgcAbas_ComposicaoFamiliar.ActivePageIndex := 0;
    ModoTelaFamilia('P');
    CarregaDadosFamilia;
  end;
end;

procedure TfrmCadastroInscricoes.ValidaCamposFamilia;
Var vFocus : Boolean;
begin
  //Validar apenas o campo nome do inscrito, pois n�o h� obrigatoriedade dos demais
  vFocus := False;
  vOK    := False;
  //Verifica se os campos obrigat�rios est�o selecionaodos/preenchidos

  //Nome do Inscrito
  if (Length(edtNOME_COMPLETO_FAMILIAR.Text) = 0) then
  begin
    Application.MessageBox ('O NOME DO FAMILIAR DEVE SER PREENCHIDO!' +#13+#10+
                            'Favor preencher o nome sem abrevia��es e/ou acentua��o.',
                            '[Sistema Deca] - Nome do Inscrito deve ser preenchido.',
                            MB_OK + MB_ICONINFORMATION);
    PageControl4.ActivePageIndex :=0;
    edtNOME_COMPLETO_FAMILIAR.SetFocus;
    vOK := False;
    vFocus := True;
    Exit;
  end
  else
    vOK := True;

  //Reside com Fam�lia
  if (cmbFAMILIAR_MORA_FAMILIA.ItemIndex = -1) then
  begin
    Application.MessageBox ('A OP��O MORA COM A FAM�LIA DEVE SER SELECIONADA!' +#13+#10+
                            'Favor selecionar uma das op��es pr�-definidas da lista.',
                            '[Sistema Deca] - "Mora com a Fam�lia" deve ser selecionada.',
                            MB_OK + MB_ICONINFORMATION);
    If Not vFocus Then
    Begin
      vFocus := True;
      PageControl4.ActivePageIndex :=0;
      cmbFAMILIAR_MORA_FAMILIA.SetFocus;
      vOK := False;
      Exit;
    End;
  end
  else
    vOK := True;

end;

procedure TfrmCadastroInscricoes.CarregaDadosInscritos;
begin
  CarregaCombos;
  { ----------------------------------------------------------------------- }
  if vvMODO_PESQUISA = True then
  begin
    ModoTela('S');
    ModoTelaFamilia('P');

    //Carrega os dados da pesquisa full para o formul�rio de inscri��o
    mskNUMERO_INSCRICAO.Text                      := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_inscricao').Value;;
    mskINSCRICAO_DIGITO.Text                      := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_inscricao_digt').Value;;
    mskDATA_INSCRICAO.Text                        := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dta_inscricao').Value;

    cmbNUMERO_PORTARIA.ItemIndex                  := 0; //dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('').Value;
    cmbSITUACAO_INSCRICAO.ItemIndex               := cmbSITUACAO_INSCRICAO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_posicao').AsString));

    mskDATA_RENOVACAO.Text := DateToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dta_renovacao').AsDateTime);
    mskDATA_MATRICULA.Text := DateToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dat_matricula').AsDateTime);
    mskDATA_READMISSAO.Text := DateToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dat_readmissao').AsDateTime);

    edtNOME_COMPLETO_INSCRITO.Text                := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('nom_nome').Value;
    edtNOME_COMPLETO_INSCRITO.OnExit(Self);

    mskDATA_NASCIMENTO_INSCRITO.Text              := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dat_nascimento').Value;
    lblIDADE.Caption                              := frmFichaCrianca.CalculaIdade(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dat_nascimento').AsDateTime);

    cmbESTADO_CIVIL_INSCRITO.ItemIndex            := cmbESTADO_CIVIL_INSCRITO.Items.IndexOf(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_estadocivil').Value);
    cmbSEXO_INSCRITO.ItemIndex                    := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_sexo').Value;
    cmbNATURALIDADE_INSCRITO.ItemIndex            := cmbNATURALIDADE_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_cidade').AsString));
    cmbNACIONALIDADE_INSCRITO.ItemIndex           := cmbNACIONALIDADE_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_nacionalidade').AsString));
    edtENDERECO_INSCRITO.Text                     := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_endereco').Value;
    edtNUMERO_RESIDENCIA_INSCRITO.Text            := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_NumResidencia').Value;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_Complemento').IsNull) then
      edtCOMPLEMENTO_ENDERECO_INSCRITO.Text := ''
    else edtCOMPLEMENTO_ENDERECO_INSCRITO.Text := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_Complemento').Value;

    //Remover a m�scara do maskedit, repassar o valor para o campo e aplicar novamente a m�scara...
    mskCEP_RESIDENCIAL_INSCRITO.EditMask          := '';
    mskCEP_RESIDENCIAL_INSCRITO.Text              := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cep').AsString;
    mskCEP_RESIDENCIAL_INSCRITO.EditMask          := '99999-999';

    cmbBAIRRO_INSCRITO.ItemIndex                  := cmbBAIRRO_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_bairro').AsString));
    edtTEMPO_RESIDENCIA.Text                      := IntToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_TempoResidencia').AsInteger);
    edtPONTO_REFERENCIA_RESIDENCIA.Text           := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_PontoRef').AsString;
    cmbRESIDE_FAMILIA.ItemIndex                   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_ResideFam').AsInteger;
    edtN_RESIDE_FAM.Text                          := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NResideFam').AsString;

    cmbREGIAO.ItemIndex                           := cmbREGIAO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_regiao').AsString));
    cmbUNIDADE_REFERENCIA_PARA_INSCRITO.ItemIndex := cmbUNIDADE_REFERENCIA_PARA_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('nom_unidade').AsString));

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cartao_sus').IsNull) then edtNUMERO_CARTAO_SUS_INSCRITO.Text := ''
    else edtNUMERO_CARTAO_SUS_INSCRITO.Text       := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cartao_sus').Value;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cra').IsNull) then edtNUMERO_CRA_INSCRITO.Text := ''
    else edtNUMERO_CRA_INSCRITO.Text              := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cra').Value;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_nis').IsNull) then edtNUMERO_NIS_INSCRITO.Text := ''
    else edtNUMERO_NIS_INSCRITO.Text              := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_nis').Value;

    if dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cartao_passe').IsNull then
      edtNUMERO_CARTAO_PASSE_INSCRITO.Text        := ''
    else edtNUMERO_CARTAO_PASSE_INSCRITO.Text     := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_cartao_passe').Value;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_sias').IsNull) or (Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_sias').Value) = 0) then
      edtNUMERO_CARTAO_SIAS_INSCRITO.Text         := ''
    else edtNUMERO_CARTAO_SIAS_INSCRITO.Text      := GetValue(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_sias').AsString);

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NumCertNasc').IsNull) or (Length(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NumCertNasc').Value) = 0) then
      edtCERTIDAO_NASCIMENTO_INSCRITO.Text        := ''
    else edtCERTIDAO_NASCIMENTO_INSCRITO.Text     := GetValue(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NumCertNasc').AsString);

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_CPF').IsNull) then mskNUMERO_CPF_INSCRITO.Text := '000.000.000-00'
    else mskNUMERO_CPF_INSCRITO.Text              := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_CPF').Value;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_RG').IsNull) then edtNUMERO_RG_INSCRITO.Text := '00.000.000-0'
    else edtNUMERO_RG_INSCRITO.Text               := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_RG').Value;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dat_emissao_rg').IsNull) then mskDATA_EMISSAO_RG_INSCRITO.Text := ''
    else mskDATA_EMISSAO_RG_INSCRITO.Text         := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dat_emissao_rg').Value;

    cmbORGAO_EMISSOR_RG_INSCRITO.ItemIndex        := cmbORGAO_EMISSOR_RG_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_emissor').AsString));

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_fone_contato').IsNull) then medTELEFONES_CONTATOS_INSCRITO.Text := ''
    else medTELEFONES_CONTATOS_INSCRITO.Text      := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_fone_contato').Value;

    rdgESTA_MATRICULADO.ItemIndex                 := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_SeMatric').AsInteger;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_motivo_nestuda').IsNull) then edtMOTIVO_NAO_ESTUDA.Text := ''
    else edtMOTIVO_NAO_ESTUDA.Text                := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_fone_contato').Value;

    cmbESCOLA_ATUAL_INSCRITO.ItemIndex            := cmbESCOLA_ATUAL_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NomeEscola').AsString));
    cmbSERIE_ATUAL_INSCRITO.ItemIndex             := cmbSERIE_ATUAL_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_escolaSerie').AsString));
    cmbPERIODO_ESCOLAR_INSCRITO.ItemIndex         := cmbPERIODO_ESCOLAR_INSCRITO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_escolaPeriodo').AsString));

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_ra').IsNull) then edtNUMERO_RA_INSCRITO.Text := '' else edtNUMERO_RA_INSCRITO.Text := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_ra').Value;

    cmbRELACAO_IDADE_SERIE_INSCRITO.ItemIndex     := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_idadeXserie').AsInteger;
    mskDATA_ATUALIZACAO_ESCOLA.Text               := DateToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dta_atualizaEscolar').AsDateTime);
    medRAZAO_INCOMPATIBILIDADE_IDADE_SERIE.Text   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_razaoincomp').AsString;

    edtDIAGNOSTICO_SAUDE_INSCRITO.Text            := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_DiagSaude').AsString;

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_invalidez_temporaria').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_invalidez_temporaria').IsNull) ) then chkINVALIDEZ_TEMPORARIA_INSCRITO.Checked := False else chkINVALIDEZ_TEMPORARIA_INSCRITO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_invalidez_permanente').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_invalidez_permanente').IsNull) ) then chkINVALIDEZ_PERMANENTE_INSCRITO.Checked := False else chkINVALIDEZ_PERMANENTE_INSCRITO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_DoencaInfecto').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_DoencaInfecto').IsNull) ) then chkPORTADOR_DOENCA_INFECTO_INSCRITO.Checked := False else chkPORTADOR_DOENCA_INFECTO_INSCRITO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_IncapazTrab').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_IncapazTrab').IsNull) ) then chkINCAPACIDADE_TRABALHO_INSCRITO.Checked := False else chkINCAPACIDADE_TRABALHO_INSCRITO.Checked := True;

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('DespesasMedicas').IsNull) then edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text := '0,00'
    else edtVALOR_DESPESAS_MEDICAS_INSCRITO.Text  := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('DespesasMedicas').AsString;

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_comprovadespmedic').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_comprovadespmedic').IsNull) ) then chkCOMPROVOU_DESPESAS_MEDICAS.Checked := False else chkCOMPROVOU_DESPESAS_MEDICAS.Checked := True;

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_trabalho').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_trabalho').IsNull) ) then chkMOTIVO_PROCURA_TRABALHO.Checked := False else chkMOTIVO_PROCURA_TRABALHO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_cursos').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_cursos').IsNull) ) then chkMOTIVO_PROCURA_CURSOS.Checked := False else chkMOTIVO_PROCURA_CURSOS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_nao_ficar_sozinho').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_nao_ficar_sozinho').IsNull) ) then chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO.Checked := False else chkMOTIVO_PROCURA_NAO_FICAR_SOZINHO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_tirar_rua').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_tirar_rua').IsNull) ) then chkMOTIVO_PROCURA_TIRAR_RUA.Checked := False else chkMOTIVO_PROCURA_TIRAR_RUA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_creas').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_creas').IsNull) ) then chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS.Checked := False else chkMOTIVO_PROCURA_ENCAMINHAMENTO_CREAS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_cras').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_cras').IsNull) ) then chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS.Checked := False else chkMOTIVO_PROCURA_ENCAMINHAMENTO_CRAS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_vij').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_vij').IsNull) ) then chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ.Checked := False else chkMOTIVO_PROCURA_ENCAMINHAMENTO_VIJ.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_as').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_as').IsNull) ) then chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS.Checked := False else chkMOTIVO_PROCURA_ENCAMINHAMENTO_AS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_ct').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_enc_ct').IsNull) ) then chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR.Checked := False else chkMOTIVO_PROCURA_ENCAMINHAMENTO_CONSELHO_TUTELAR.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_outros').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_proc_outros').IsNull) )then chkMOTIVO_PROCURA_OUTROS.Checked := False else chkMOTIVO_PROCURA_OUTROS.Checked := True;

    medMOTIVO_PROCURA_OUTROS.Text                 := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_encaminhamento').AsString;

    //Carregaro cadastro de Benef�cios Sociais
    gpbBENEFICIOS.Enabled := True;
    try
      with dmTriagemDeca.cdsSel_BeneficiosSociais do
      begin
        Close;
        Params.ParamByName ('@cod_idBeneficio').Value := Null;
        Params.ParamByName ('@Cod_inscricao').Value   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
        Open;
        dbgBENEFICIOS_SOCIAIS.Refresh;
      end;
    except end;

    gpbBENEFICIOS.Enabled           := True;
    btnNOVO_BENEFICIO.Enabled       := True;
    btnSALVA_BENEFICIO.Enabled      := False;
    btnALTERA_BENEFICIO.Enabled     := False;
    btnEXCLUI_BENEFICIO.Enabled     := True;
    btnCANCELA.Enabled              := False;


    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_outroimovel').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_outroimovel').IsNull) ) then chkIMOVEL_OUTRO.Checked := False else chkIMOVEL_OUTRO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_carro').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_carro').IsNull) ) then chkPOSSUI_CARRO.Checked := False else chkPOSSUI_CARRO.Checked := True;
    edtCARRO_MODELO.Text                          := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_carromodel').AsString;
    edtCARRO_ANO.Text                             := IntToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_carroano').AsInteger);
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_moto').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_moto').IsNull) ) then chkPOSSUI_MOTO.Checked := False else chkPOSSUI_MOTO.Checked := True;
    edtMOTO_MODELO.Text                           := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_motomodel').AsString;
    edtMOTO_ANO.Text                              := IntToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_motoano').AsInteger);

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_veiculo_outras_priori').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_veiculo_outras_priori').IsNull) ) then chkUSO_VEICULO_OUTRAS_PRIORIDADES.Checked := False else chkUSO_VEICULO_OUTRAS_PRIORIDADES.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_veiculo_trabalho').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_veiculo_trabalho').IsNull) ) then chkUSO_VEICULO_TRABALHO.Checked := False else chkUSO_VEICULO_TRABALHO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_veiculo_unico_transp').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_veiculo_unico_transp').IsNull) ) then chkUSO_VEICULO_UNICO_MEIO_TRANSP.Checked := False else chkUSO_VEICULO_UNICO_MEIO_TRANSP.Checked := True;

    cmbRELACAO_RESPONSABILIDADE.ItemIndex         := cmbRELACAO_RESPONSABILIDADE.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_relResponsabilidade').AsString));

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_drogadicto').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_drogadicto').IsNull) ) then chkAGRAVANTE_DROGADICTO.Checked := False else chkAGRAVANTE_DROGADICTO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_alcoolatra').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_alcoolatra').IsNull) ) then chkAGRAVANTE_ALCOOLATRA.Checked := False else chkAGRAVANTE_ALCOOLATRA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_paimaeadolescente').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_paimaeadolescente').IsNull) ) then chkAGRAVANTE_PAIMAE_ADOLESCENTE.Checked := False else chkAGRAVANTE_PAIMAE_ADOLESCENTE.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_gestante').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_gestante').IsNull) ) then chkAGRAVANTE_GESTANTE.Checked := False else chkAGRAVANTE_GESTANTE.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_medicamentopsico').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_medicamentopsico').IsNull) ) then chkAGRAVANTE_USO_PSICOTROPICOS.Checked := False else chkAGRAVANTE_USO_PSICOTROPICOS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_medidas_socioeducativas').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_medidas_socioeducativas').IsNull) ) then chkAGRAVANTE_MEDIDAS_SOCIOEDUCATIVAS.Checked := False else chkAGRAVANTE_MEDIDAS_SOCIOEDUCATIVAS.Checked := True;

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_aquarela').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_aquarela').IsNull) ) then chkSIT_RISCO_CREAS.Checked := False else chkSIT_RISCO_CREAS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia').IsNull) ) then chkSIT_RISCO_VIOLENCIA.Checked := False else chkSIT_RISCO_VIOLENCIA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_fisica').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_fisica').IsNull) ) then chkSIT_RISCO_VIOLENCIA_FISICA.Checked := False else chkSIT_RISCO_VIOLENCIA_FISICA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_psicologica').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_psicologica').IsNull) ) then chkSIT_RISCO_VIOLENCIA_PSICOLOGICA.Checked := False else chkSIT_RISCO_VIOLENCIA_PSICOLOGICA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_sexual').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_sexual').IsNull) ) then chkSIT_RISCO_VIOLENCIA_SEXUAL.Checked := False else chkSIT_RISCO_VIOLENCIA_SEXUAL.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_negligencia').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_negligencia').IsNull) ) then chkSIT_RISCO_VIOLENCIA_NEGLIGENCIA.Checked := False else chkSIT_RISCO_VIOLENCIA_NEGLIGENCIA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_abandono').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_violencia_abandono').IsNull) ) then chkSIT_RISCO_VIOLENCIA_ABANDONO.Checked := False else chkSIT_RISCO_VIOLENCIA_ABANDONO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_prostituicao').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_prostituicao').IsNull) ) then chkSIT_RISCO_PROSTITUICAO.Checked := False else chkSIT_RISCO_PROSTITUICAO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_traficodrogas').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_traficodrogas').IsNull) ) then chkSIT_RISCO_TRAFICO_DROGAS.Checked := False else chkSIT_RISCO_TRAFICO_DROGAS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_convivedrogadiccao').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_convivedrogadiccao').IsNull) ) then chkSIT_RISCO_CONVIVE_DROGADICCAO.Checked := False else chkSIT_RISCO_CONVIVE_DROGADICCAO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_convivealcool').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_convivealcool').IsNull) ) then chkSIT_RISCO_CONVIVE_ALCOOLATRA.Checked := False else chkSIT_RISCO_CONVIVE_ALCOOLATRA.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_conviveportdoencamental').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_conviveportdoencamental').IsNull) ) then chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_MENTAIS.Checked := False else chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_MENTAIS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_conviveportdoencainfecto').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_conviveportdoencainfecto').IsNull) ) then chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INFECTO.Checked := False else chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INFECTO.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_conv_port_doencas_incap').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_conv_port_doencas_incap').IsNull) ) then chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INCAPACITANTES.Checked := False else chkSIT_RISCO_CONVIVE_PORTADORES_DOENCAS_INCAPACITANTES.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_membroautorinfracao').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_membroautorinfracao').IsNull) ) then chkSIT_RISCO_MEMBRO_ATOS_INFRACIONAIS.Checked := False else chkSIT_RISCO_MEMBRO_ATOS_INFRACIONAIS.Checked := True;
    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_criancasozinha').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_criancasozinha').IsNull) ) then chkSIT_RISCO_FICAM_SOZINHAS.Checked := False else chkSIT_RISCO_FICAM_SOZINHAS.Checked := True;

    cmbNATUREZA_HABITACAO.ItemIndex       := cmbNATUREZA_HABITACAO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_natureza_hab').AsString));

    if (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_valoraluguel').IsNull) then edtVALOR_ALUGUEL_INFORMADO.Text := '0,00'
    else edtVALOR_ALUGUEL_INFORMADO.Text  := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_valoraluguel').AsString;

    if ( (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_comprovoualuguel').Value = 0) or (dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('ind_comprovoualuguel').IsNull) ) then chkCOMPROVOU_ALUGUEL.Checked := False else chkCOMPROVOU_ALUGUEL.Checked := True;
    edtOBSERVACOES.Text                   := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_observacao').AsString;

    cmbTIPO_HABITACAO.ItemIndex           := cmbTIPO_HABITACAO.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_tipo_hab').AsString));
    cmbINFRAESTRUTURA.ItemIndex           := cmbINFRAESTRUTURA.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_infra_estrutura').AsString));
    edtQTD_HABITANTES.Text                := IntToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_qtdehabitantes').AsInteger);
    edtQTD_COMODOS.Text                   := IntToStr(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('num_qtdecomodos').AsInteger);

    cmbINSTALACOES_SANITARIAS.ItemIndex   := cmbINSTALACOES_SANITARIAS.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_inst_sanitaria').AsString));
    cmbCONDICOES_HABITABILIDADE.ItemIndex := cmbCONDICOES_HABITABILIDADE.Items.IndexOf(Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_condicaohabita').AsString));

    gpbBENEFICIOS.Enabled                 := True;

  end
  else
  begin
    ModoTela('P');
    ModoTelaFamilia('P');
  end;

  try
     PageControl1.ActivePageIndex := 0;
     PageControl2.ActivePageIndex := 0;
     PageControl4.ActivePageIndex := 0;

     if (vvIND_PERFIL = 3) then  //Assistente Social
       CarregaAbasSS
     else if (vvIND_PERFIL <> 3) then    // Demais perfis
       CarregaAbasADM;

  except
     CarregaAbasADM;
  end;

end;

procedure TfrmCadastroInscricoes.CarregaDadosFamilia;
begin
   //Carrega os dados do inscrito atual, validando os campos "nulos"

   if vvMODO_PESQUISA = True then
   begin
     ModoTelaFamilia('P');                          

    if (vvIND_PERFIL = 3) then  //Assistente Social
    begin

      //Composi��o Familiar
      PageControl2.Pages[1].TabVisible := True;
      //Dados Pessoais do Familiar
      pgcAbas_ComposicaoFamiliar.Pages[0].TabVisible := True;
      //Documenta��o do Familiar
      pgcAbas_ComposicaoFamiliar.Pages[1].TabVisible := True;
      //Situa��o de Sa�de do Familiar
      pgcAbas_ComposicaoFamiliar.Pages[2].TabVisible := True;

      //Relat�rios
      PageControl2.Pages[2].TabVisible := True;
      //Pontua��o Final
      PageControl2.Pages[3].TabVisible := True;
      //Pend�ncias
      PageControl2.Pages[4].TabVisible := True;
    end
    else if (vvIND_PERFIL <> 3) then
    begin

      //Inscri��o
      PageControl1.Pages[0].TabVisible := True;
      //Admiss�o
      PageControl1.Pages[0].TabVisible := True;

      //Composi��o Familiar
      PageControl2.Pages[1].TabVisible := True;
      //Dados Pessoais do Familiar
      pgcAbas_ComposicaoFamiliar.ActivePageIndex     := 0;
      pgcAbas_ComposicaoFamiliar.Pages[0].TabVisible := True;
      //Documenta��o do Familiar
      pgcAbas_ComposicaoFamiliar.Pages[1].TabVisible := True;
      //Situa��o de Sa�de do Familiar
      pgcAbas_ComposicaoFamiliar.Pages[2].TabVisible := False;

      //Relat�rios
      PageControl2.Pages[2].TabVisible := True;
      //Pontua��o Final
      PageControl2.Pages[3].TabVisible := True;
      //Pend�ncias
      PageControl2.Pages[4].TabVisible := True;
  end;

  try
     with dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar do
     begin
       Close;
       Params.ParamByName ('@Cod_membrofamilia').Value := Null;
       Params.ParamByName ('@Cod_inscricao').Value     := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
       Open;
       dbgCOMPOSICAO_FAMILIAR.Refresh;
     end;
   except end;
   end;

end;

procedure TfrmCadastroInscricoes.btnEDITAR_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
  Sender: TObject);
begin

  ModoTelaFamilia('A');
  HabilitaGroupBox;
  vTipo_Edicao          := 'U';
  mmPODE_MUDAR_ABA      := True;

end;

procedure TfrmCadastroInscricoes.CarregaAbasADM;
begin
  // Carregar as abas para o perfil de Administrativo

  //Inscri��o
    PageControl1.Pages[0].TabVisible := True;
    //Admiss�o
    //PageControl1.Pages[0].TabVisible := True;

    //Dados do Inscrito
    PageControl2.Pages[0].TabVisible := True;
    PageControl4.Pages[0].TabVisible := True;  //Dados Cadastrais
    PageControl4.Pages[1].TabVisible := True;  //Documenta��o e Telefones
    PageControl4.Pages[2].TabVisible := True;  //Dados de Escola
    PageControl4.Pages[3].TabVisible := False;  //Situa��o de Sa�de
    PageControl4.Pages[4].TabVisible := False;  //Benef�cios Sociais e Bens Complementares
    PageControl4.Pages[5].TabVisible := False;  //Situa��o Social
    PageControl4.Pages[6].TabVisible := False;  //Situa��o de Moradia

    //Composi��o Familiar
    PageControl2.Pages[1].TabVisible := True;
    //Dados Pessoais do Familiar
    pgcAbas_ComposicaoFamiliar.ActivePageIndex     := 0;
    pgcAbas_ComposicaoFamiliar.Pages[0].TabVisible := True;
    //Documenta��o do Familiar
    pgcAbas_ComposicaoFamiliar.Pages[1].TabVisible := True;
    //Situa��o de Sa�de do Familiar
    pgcAbas_ComposicaoFamiliar.Pages[2].TabVisible := False;

    //Relat�rios
    PageControl2.Pages[2].TabVisible := True;
    //Pontua��o Final
    PageControl2.Pages[3].TabVisible := True;
    //Pend�ncias
    PageControl2.Pages[4].TabVisible := True;

end;

procedure TfrmCadastroInscricoes.CarregaAbasSS;
begin
  // Carregar as abas para o perfil de Servi�o Social

   //Inscri��o
    PageControl1.Pages[0].TabVisible := True;
    //Admiss�o
    //PageControl1.Pages[1].TabVisible := True;

    //Dados do Inscrito
    PageControl2.Pages[0].TabVisible := True;
    PageControl4.Pages[0].TabVisible := True;  //Dados Cadastrais
    PageControl4.Pages[1].TabVisible := True;  //Documenta��o e Telefones
    PageControl4.Pages[2].TabVisible := True;  //Dados de Escola
    PageControl4.Pages[3].TabVisible := True;  //Situa��o de Sa�de
    PageControl4.Pages[4].TabVisible := True;  //Benef�cios Sociais e Bens Complementares
    PageControl4.Pages[5].TabVisible := True;  //Situa��o Social
    PageControl4.Pages[6].TabVisible := True;  //Situa��o de Moradia

    //Composi��o Familiar
    PageControl2.Pages[1].TabVisible := True;
    //Dados Pessoais do Familiar
    pgcAbas_ComposicaoFamiliar.Pages[0].TabVisible := True;
    //Documenta��o do Familiar
    pgcAbas_ComposicaoFamiliar.Pages[1].TabVisible := True;
    //Situa��o de Sa�de do Familiar
    pgcAbas_ComposicaoFamiliar.Pages[2].TabVisible := True;

    //Relat�rios
    PageControl2.Pages[2].TabVisible := True;
    //Pontua��o Final
    PageControl2.Pages[3].TabVisible := True;
    //Pend�ncias
    PageControl2.Pages[4].TabVisible := True;

end;

procedure TfrmCadastroInscricoes.LimpaCamposFamilia;
begin
   edtNOME_COMPLETO_FAMILIAR.Clear;
  chkFAMILIAR_RESPONSAVEL.Checked                      := False;
  cmbSEXO_FAMILIAR.ItemIndex                           := -1;
  cmbESTADO_CIVIL_FAMILIAR.ItemIndex                   := -1;
  cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex                := -1;
  mskDATA_NASCIMENTO_FAMILIAR.Clear;
  edtLOCAL_TRABALHO_FAMILIAR.Clear;
  edtOCUPACAO_FAMILIAR.Clear;
  mskTELEFONE_FAMILIAR.Clear;
  edtRENDA_MENSAL_FAMILIAR.Clear;
  edtOUTRAS_RENDAS_FAMILIAR.Clear;
  cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex              := -1;
  cmbFAMILIAR_POSSUI_GUARDA.ItemIndex                   := -1;
  cmbFAMILIAR_MORA_FAMILIA.ItemIndex                    := -1;
  mskNUMERO_CPF_FAMILIAR.Clear;
  edtNUMERO_RG_FAMILIAR.Clear;
  mskDATA_EMISSAO_RG_FAMILIAR.Clear;
  cmbORGAO_EMISSOR_RG_FAMILIAR.ItemIndex                := -1;
  edtCERTIDAO_NASCIMENTO_FAMILIAR.Clear;
  cmbNATURALIDADE_FAMILIAR.ItemIndex                    := -1;
  cmbNACIONALIDADE_FAMILIAR.ItemIndex                   := -1;
  edtNUMERO_CARTAO_SUS_FAMILIAR.Clear;
  edtNUMERO_CRA_FAMILIAR.Clear;
  edtNUMERO_NIS_FAMILIAR.Clear;
  edtNUMERO_CARTAO_PASSE_FAMILIAR.Clear;
  chkINVALIDEZ_TEMPORARIA_FAMILIAR.Checked              := False;
  chkINVALIDEZ_PERMANENTE_FAMILIAR.Checked              := False;
  chkINCAPACIDADE_TRABALHO_FAMILIAR.Checked             := False;
  chkPORTADOR_DOENCA_INFECTO_FAMILIAR.Checked           := False;
  chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE.Checked := False;
  chkFAMILIAR_FALECIDO.Checked                          := False;
  chkFAMILIAR_RECLUSO.Checked                           := False;
  edtDIAGNOSTICO_SAUDE_FAMILIAR.Clear;
  edtRELACIONAMENTO_FAMILIAR.Clear;
end;

procedure TfrmCadastroInscricoes.dbgCOMPOSICAO_FAMILIARCellClick(
  Column: TColumn);
begin
 //Habilitar os groupb box para repassar os valores e depois desabilit�-los novamente
  HabilitaGroupBox;

  Label10.Caption                := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Nome_fam').Value;

  edtNOME_COMPLETO_FAMILIAR.Text := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Nome_fam').Value;

  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_responsavel').Value = 0) or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_responsavel').IsNull) ) then chkFAMILIAR_RESPONSAVEL.Checked := False
  else chkFAMILIAR_RESPONSAVEL.Checked := True;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_sexo_par').IsNull) then cmbSEXO_FAMILIAR.ItemIndex := -1
  else cmbSEXO_FAMILIAR.ItemIndex := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_sexo_par').AsInteger;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_EstadoCivil_par').IsNull) then cmbESTADO_CIVIL_FAMILIAR.ItemIndex := -1
  else cmbESTADO_CIVIL_FAMILIAR.ItemIndex       := cmbESTADO_CIVIL_FAMILIAR.Items.IndexOf(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_estadocivil').AsString);

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_parentesco').IsNull) then cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex    := -1
  else cmbGRAU_PARENTESCO_FAMILIAR.ItemIndex    :=cmbGRAU_PARENTESCO_FAMILIAR.Items.IndexOf(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_parentesco').AsString);

  if ((dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dat_Nasc_fam').IsNull) )    then mskDATA_NASCIMENTO_FAMILIAR.Text       := ''
  else mskDATA_NASCIMENTO_FAMILIAR.Text  := DateToStr(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dat_Nasc_fam').AsDateTime);

  lblIDADE_Familiar.Caption                := frmFichaCrianca.CalculaIdade(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dat_Nasc_fam').AsDateTime);
  edtLOCAL_TRABALHO_FAMILIAR.Text          := Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_LocalTrab_fam').AsString);
  edtOCUPACAO_FAMILIAR.Text                := Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Ocupacao_fam').AsString);
  mskTELEFONE_FAMILIAR.Text                := Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Tel_fam').AsString);

  // DEVE-SE TRATAR A RENDA QDO CAMPO NULO ...   if...
  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_rendamensal_fam').IsNull) then edtRENDA_MENSAL_FAMILIAR.Text := '000,00'
  else edtRENDA_MENSAL_FAMILIAR.Text       := FloatToStr(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_rendamensal_fam').AsFloat);

  edtOUTRAS_RENDAS_FAMILIAR.Text           := Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_outrarenda').AsString);

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_escolaridade').IsNull) then cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex := -1
        else cmbNIVEL_ESCOLARIDADE_FAMILIAR.ItemIndex := cmbNIVEL_ESCOLARIDADE_FAMILIAR.Items.IndexOf(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_escolaridade').Value);

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_Temguarda').IsNull) then cmbESTADO_CIVIL_FAMILIAR.ItemIndex := -1
  else cmbFAMILIAR_POSSUI_GUARDA.ItemIndex := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_Temguarda').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_morafamilia').IsNull) then cmbFAMILIAR_MORA_FAMILIA.ItemIndex := -1
  else cmbFAMILIAR_MORA_FAMILIA.ItemIndex  := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_morafamilia').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_CPF_fam').IsNull) then mskNUMERO_CPF_FAMILIAR.Text := ''
  else mskNUMERO_CPF_FAMILIAR.Text         := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_CPF_fam').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_RG_fam').IsNull) then edtNUMERO_RG_FAMILIAR.Text := ''
  else edtNUMERO_RG_FAMILIAR.Text          := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_RG_fam').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dat_emissao_rg').IsNull) then mskDATA_EMISSAO_RG_FAMILIAR.Text := ''
  else mskDATA_EMISSAO_RG_FAMILIAR.Text    := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dat_emissao_rg').Value;

  cmbORGAO_EMISSOR_RG_FAMILIAR.ItemIndex   := cmbORGAO_EMISSOR_RG_FAMILIAR.Items.IndexOf(Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_emissor').AsString));

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_NUmCertNasc_fam').IsNull) then
    edtCERTIDAO_NASCIMENTO_FAMILIAR.Text    := ''
  else edtCERTIDAO_NASCIMENTO_FAMILIAR.Text := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_NUmCertNasc_fam').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_cidade').IsNull) then    cmbNATURALIDADE_FAMILIAR.ItemIndex := -1
  else cmbNATURALIDADE_FAMILIAR.ItemIndex := cmbNATURALIDADE_FAMILIAR.Items.IndexOf(Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_cidade').Value));

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_nacionalidade').IsNull) then cmbNACIONALIDADE_FAMILIAR.ItemIndex := -1
  else cmbNACIONALIDADE_FAMILIAR.ItemIndex := cmbNACIONALIDADE_FAMILIAR.Items.IndexOf(Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_nacionalidade').Value));

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_cartao_sus').IsNull) then edtNUMERO_CARTAO_SUS_FAMILIAR.Text := ''
  else edtNUMERO_CARTAO_SUS_FAMILIAR.Text       := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_cartao_sus').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_cra').IsNull) then edtNUMERO_CRA_FAMILIAR.Text := ''
  else edtNUMERO_CRA_FAMILIAR.Text              := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_cra').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_nis').IsNull) then edtNUMERO_NIS_FAMILIAR.Text := ''
  else edtNUMERO_NIS_FAMILIAR.Text              := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_nis').Value;

  if (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_cartao_passe').IsNull) then edtNUMERO_CARTAO_PASSE_FAMILIAR.Text := ''
  else edtNUMERO_CARTAO_PASSE_FAMILIAR.Text     := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('num_cartao_passe').Value;

  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_inv_temp').Value = 0)           or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_inv_temp').IsNull) )           then chkINVALIDEZ_TEMPORARIA_FAMILIAR.Checked := False else chkINVALIDEZ_TEMPORARIA_FAMILIAR.Checked := True;
  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_inv_perm').Value = 0)           or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_inv_perm').IsNull) )           then chkINVALIDEZ_PERMANENTE_FAMILIAR.Checked := False else chkINVALIDEZ_PERMANENTE_FAMILIAR.Checked := True;
  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_incapaztrab').Value = 0)        or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_incapaztrab').IsNull) )        then chkINCAPACIDADE_TRABALHO_FAMILIAR.Checked := False else chkINCAPACIDADE_TRABALHO_FAMILIAR.Checked := True;
  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_PortDoencaContagia').Value = 0) or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_PortDoencaContagia').IsNull) ) then chkPORTADOR_DOENCA_INFECTO_FAMILIAR.Checked := False else chkPORTADOR_DOENCA_INFECTO_FAMILIAR.Checked := True;
  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_NRegCrianca').Value = 0)   or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_NRegCrianca').IsNull) )   then chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE.Checked := False else chkFAMILIAR_NAO_REGISTROU_CRIANCA_ADOLESCENTE.Checked := True;
  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_falecido').Value = 0)      or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_falecido').IsNull) )      then chkFAMILIAR_FALECIDO.Checked := False else chkFAMILIAR_FALECIDO.Checked := True;
  if ( (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_recluso').Value = 0)       or (dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('ind_cond_recluso').IsNull) )       then chkFAMILIAR_RECLUSO.Checked := False else chkFAMILIAR_RECLUSO.Checked := True;

  edtDIAGNOSTICO_SAUDE_FAMILIAR.Text       := Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_diagnostico_par').AsString);
  edtRELACIONAMENTO_FAMILIAR.Text          := Trim(dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('dsc_Relacionamento_fam').AsString);

  //Desabilitar os groupb box para ficar as abas de visualiza��o
  DesabilitarGroupBox;

  pgcAbas_ComposicaoFamiliar.ActivePageIndex := 0;
  //ModoTelaFamilia('A');

end;

procedure TfrmCadastroInscricoes.ModoTelaRelatorios(Modo: String);
begin
  if Modo = 'P' then
  begin
    btnNOVO_RELATORIO.Enabled             := True;
    btnGRAVAR_RELATORIO.Enabled           := False;
    btnEDITAR_RELATORIO.Enabled           := True;
    btnCANCELAROPERACAO_RELATORIO.Enabled := False;
    btnSAIR_RELATORIO.Enabled             := True;

    HabilitaGroupBox;
    //Limpa os campos de relat�rio
    mskDATA_RELATORIO.Clear;
    edtNOME_ENTREVISTADOR.Clear;
    cmbTIPO_RELATORIO.ItemIndex           := -1;
    medDESCRICAO_RELATORIO.Clear;
    //Desabilita os groups
    DesabilitarGroupBox;
    gpbRELATORIOS_GRAVADOS.Enabled        := True;
  end else if Modo = 'N' then
  begin
    btnNOVO_RELATORIO.Enabled             := False;
    btnGRAVAR_RELATORIO.Enabled           := True;
    btnEDITAR_RELATORIO.Enabled           := False;
    btnCANCELAROPERACAO_RELATORIO.Enabled := True;
    btnSAIR_RELATORIO.Enabled             := False;
    HabilitaGroupBox;
  end else if Modo = 'A' then
  begin
    btnNOVO_RELATORIO.Enabled             := False;
    btnGRAVAR_RELATORIO.Enabled           := True;
    btnEDITAR_RELATORIO.Enabled           := False;
    btnCANCELAROPERACAO_RELATORIO.Enabled := True;
    btnSAIR_RELATORIO.Enabled             := False;
    HabilitaGroupBox;
  end;
end;

procedure TfrmCadastroInscricoes.btnNOVO_RELATORIOClick(Sender: TObject);
begin
  ModoTelaRelatorios('N');
  vvTipo_Edicao_Relatorio     := 'I';
  if vvIND_PERFIL <> 3 then
  begin
    cmbTIPO_RELATORIO.ItemIndex := 4;
    gpbTIPO_RELATORIO.Enabled   := False;
    mskDATA_RELATORIO.Clear;
    edtNOME_ENTREVISTADOR.Clear;
    medDESCRICAO_RELATORIO.Clear;
  end
  else
  begin
    gpbTIPO_RELATORIO.Enabled := True;
    mskDATA_RELATORIO.Clear;
    edtNOME_ENTREVISTADOR.Clear;
    medDESCRICAO_RELATORIO.Clear;
  end;
end;

procedure TfrmCadastroInscricoes.btnCANCELAROPERACAO_RELATORIOClick(
  Sender: TObject);
begin
  ModoTelaRelatorios('P');
  gpbTIPO_RELATORIO.Enabled := True;
  vvTipo_Edicao_Relatorio   := '';
  gpbPONTUACAO.Enabled      := True;

  vTipo_Edicao              := '';
  mmPODE_MUDAR_ABA          := True;
  gpbBENEFICIOS.Enabled     := True;
  gpbPONTUACAO.Enabled      := True;
end;

procedure TfrmCadastroInscricoes.CarregaDadosRelatorios;
begin
  ModoTelaRelatorios('P');

  //Caso seja nova ficha, n�o haver� como carregar os dados do relat�rio a partir do Codigo da Inscri��o...
  if vvMODO_PESQUISA = False then
  begin

  end
  else if vvMODO_PESQUISA = True then
  begin
    try
      with dmTriagemDeca.cdsSel_Cadastro_Relatorio do
      begin
        Close;
        Params.ParamByName ('@Cod_inscricao').Value       := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
        Params.ParamByName ('@pe_cod_usuario_deca').Value := Null;
        if vvIND_PERFIL = 3 then Params.ParamByName ('@pe_tipo_relatorio').Value := Null else if vvIND_PERFIL <> 3 then Params.ParamByName ('@pe_tipo_relatorio').Value := 4;
        Open;
        dbgRELATORIOS_GRAVADOS.Refresh;
      end;
    except end;

    //Caso o perfil seja <> 3 habilita apenas a op��o "Registro de Atendimento" no TIPO DE RELAT�RIO
    if vvIND_PERFIL <> 3 then cmbTIPO_RELATORIO.ItemIndex := 4;
    gpbTIPO_RELATORIO.Enabled   := True;
  end;
end;

procedure TfrmCadastroInscricoes.dbgRELATORIOS_GRAVADOSCellClick(
  Column: TColumn);
begin
  //Verificar se o usu�rio logado � o mesmo do registro para permitir ou n�o a edi��o do registro
  if ( vvCOD_USUARIO = dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('cod_usuario_deca').Value ) then
  begin
    //ModoTelaRelatorios('A');
    ModoTelaRelatorios('P');
    mskDATA_RELATORIO.Text      := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('dta_digitacao').Value;
    edtNOME_ENTREVISTADOR.Text  := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('dsc_entrevistador').Value;
    cmbTIPO_RELATORIO.ItemIndex := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('tipo_relatorio').Value;
    medDESCRICAO_RELATORIO.Text := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('txt_relatorio').Value;
    vvTipo_Edicao_Relatorio     := 'U';

    if vvIND_PERFIL <> 3 then
    begin
      cmbTIPO_RELATORIO.ItemIndex := 4;
      gpbTIPO_RELATORIO.Enabled   := False;
    end
    else gpbTIPO_RELATORIO.Enabled := True;
    
  end
  else if ( vvCOD_USUARIO <> dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('cod_usuario_deca').Value ) then
  begin
    //Application.MessageBox ('Aten��o!!!' +#13+#10+
    //                        'Voc� n�o � o propriet�rio desse registro, portanto n�o pode alter�-lo. Voc� pode apenas visualizar o conte�do do relat�rio.',
    //                        '[Sistema Deca] - Acesso negado',
    //                        MB_OK + MB_ICONINFORMATION);
    ModoTelaRelatorios('P');
    mskDATA_RELATORIO.Text      := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('dta_digitacao').Value;
    edtNOME_ENTREVISTADOR.Text  := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('dsc_entrevistador').Value;
    cmbTIPO_RELATORIO.ItemIndex := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('tipo_relatorio').Value;
    medDESCRICAO_RELATORIO.Text := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('txt_relatorio').Value;
    vvTipo_Edicao_Relatorio     := '';
  end;
end;

procedure TfrmCadastroInscricoes.btnGRAVAR_RELATORIOClick(Sender: TObject);
begin
  if vvTipo_Edicao_Relatorio = 'I' then
  begin
    if Application.MessageBox ('Deseja inserir os dados do relat�rio ?',
                               '[Sistema Deca] - Inserir novo relat�rio',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      //Grava os dados do relat�rio
      try

        with dmTriagemDeca.cdsIns_Cadastro_Relatorio do
        begin
          Close;
          Params.ParamByName ('@Cod_inscricao').Value       := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
          Params.ParamByName ('@dta_digitacao').Value       := StrToDate(mskDATA_RELATORIO.Text);
          Params.ParamByName ('@tipo_relatorio').Value      := cmbTIPO_RELATORIO.ItemIndex;
          Params.ParamByName ('@cod_responsavel').Value     := Null;  //Trazer os dados de usu�rio pelo Sistema Deca
          Params.ParamByName ('@txt_relatorio').Value       := Trim(medDESCRICAO_RELATORIO.Text);
          Params.ParamByName ('@dsc_entrevistador').Value   := Trim(edtNOME_ENTREVISTADOR.Text);
          Params.ParamByName ('@pe_cod_usuario_deca').Value := vvCOD_USUARIO; //Estabelece a rela��o do usu�rio com o banco de dados Usuario -> Deca
          Execute;

          //Limpa os campos da tela
          mskDATA_RELATORIO.Clear;
          edtNOME_ENTREVISTADOR.Clear;
          cmbTIPO_RELATORIO.ItemIndex := -1;
          medDESCRICAO_RELATORIO.Clear;
          DesabilitarGroupBox;
          ModoTelaRelatorios('P');
          CarregaDadosRelatorios;

          with dmTriagemDeca.cdsSel_Cadastro_Relatorio do
          begin
            Close;
            Params.ParamByName ('@pe_cod_usuario_deca').Value := Null;
            Params.ParamByName ('@pe_tipo_relatorio').Value := Null;
            Params.ParamByName ('@Cod_inscricao').Value     := vvCOD_INSCRICAO;
            Open;

            // Reativar o Grid da aba RELATORIO
            dbgRELATORIOS_GRAVADOS.DataSource             := dsSel_Cadastro_Relatorios;
            dbgRELATORIOS_GRAVADOS.Refresh;
          end;

        end;
      except end;
    end
    else
    begin
      Application.MessageBox ('Opera��o de inser��o de relat�rio cancelada pelo usu�rio.' +#13+#10+
                              'Os dados N�O foram inseridos no banco de dados.',
                              '[Sistema Deca] - Opera��o cancelada.',
                              MB_OK + MB_ICONINFORMATION);
      //Limpa os campos da tela
      mskDATA_RELATORIO.Clear;
      edtNOME_ENTREVISTADOR.Clear;
      cmbTIPO_RELATORIO.ItemIndex := -1;
      medDESCRICAO_RELATORIO.Clear;
      DesabilitarGroupBox;
      ModoTelaRelatorios('P');
    end
  end  //if vvTIPO_EDICAO_RELATORIO = 'I'...
  else if vvTipo_Edicao_Relatorio = 'U' then
  begin
    //Grava as altera��es feitas nos relat�rios
    if Application.MessageBox ('Deseja manter as altera��es dos dados do relat�rio ?',
                               '[Sistema Deca] - Alterar dados do relat�rio',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      try
        //Recuperar o c�digo da inscri��o para a inclus�o de membro ad Composi��o Familiar
        with dmTriagemDeca.cdsSel_CadastroFull do
        begin
          Close;
          Params.ParamByName ('@pe_cod_inscricao').Value      := Null;
          Params.ParamByName ('@pe_num_inscricao').Value      := StrToInt(mskNUMERO_INSCRICAO.Text);
          Params.ParamByName ('@pe_num_inscricao_digt').Value := StrToInt(mskINSCRICAO_DIGITO.Text);
          Open;
          vvCOD_INSCRICAO := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
        end;

        with dmTriagemDeca.cdsUpd_Cadastro_Relatorio do
        begin
          Close;
          Params.ParamByName ('@id_relatorio').Value        := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('id_relatorio').Value;
          Params.ParamByName ('@Cod_inscricao').Value       := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('Cod_inscricao').Value;
          Params.ParamByName ('@dta_digitacao').Value       := StrToDate(mskDATA_RELATORIO.Text);
          Params.ParamByName ('@tipo_relatorio').Value      := cmbTIPO_RELATORIO.ItemIndex;
          Params.ParamByName ('@cod_responsavel').Value     := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('cod_Responsavel').Value;
          Params.ParamByName ('@txt_relatorio').Value       := Trim(medDESCRICAO_RELATORIO.Text);
          Params.ParamByName ('@dsc_entrevistador').Value   := Trim(edtNOME_ENTREVISTADOR.Text);
          Params.ParamByName ('@pe_cod_usuario_deca').Value := dmTriagemDeca.cdsSel_Cadastro_Relatorio.FieldByName ('COD_USUARIO_DECA').Value;
          Execute;

          //Limpa os campos da tela
          mskDATA_RELATORIO.Clear;
          edtNOME_ENTREVISTADOR.Clear;
          cmbTIPO_RELATORIO.ItemIndex := -1;
          medDESCRICAO_RELATORIO.Clear;
          DesabilitarGroupBox;
          ModoTelaRelatorios('P');
          CarregaDadosRelatorios;

          with dmTriagemDeca.cdsSel_Cadastro_Relatorio do
          begin
            Close;
            Params.ParamByName ('@pe_cod_usuario_deca').Value := Null;
            Params.ParamByName ('@pe_tipo_relatorio').Value := Null;
            Params.ParamByName ('@Cod_inscricao').Value     := vvCOD_INSCRICAO;
            Open;

            // Reativar o Grid da aba RELATORIO
            dbgRELATORIOS_GRAVADOS.DataSource             := dsSel_Cadastro_Relatorios;
            dbgRELATORIOS_GRAVADOS.Refresh;
          end;

        end;
      except end;
    end
    else
    begin
      Application.MessageBox ('Opera��o de altera��o de relat�rio cancelada pelo usu�rio.' +#13+#10+
                              'Os dados N�O foram alterados no banco de dados.',
                              '[Sistema Deca] - Opera��o cancelada.',
                              MB_OK + MB_ICONINFORMATION);
      //Limpa os campos da tela
      mskDATA_RELATORIO.Clear;
      edtNOME_ENTREVISTADOR.Clear;
      cmbTIPO_RELATORIO.ItemIndex := -1;
      medDESCRICAO_RELATORIO.Clear;
      DesabilitarGroupBox;
      ModoTelaRelatorios('P');
    end;
  end;

end;

procedure TfrmCadastroInscricoes.btnACESSO_CONSULTASUS_FAMILIARClick(
  Sender: TObject);
begin
  ShellExecute(handle, 'open', 'https://portaldocidadao.saude.gov.br/portalcidadao/verificarSePossuiCNS.htm', nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmCadastroInscricoes.btnSAIR_RELATORIOClick(Sender: TObject);
begin
  frmCadastroInscricoes.Close;
end;

procedure TfrmCadastroInscricoes.btnCHAMARPESQUISA_INSCRICAO_ABA_COMPOSICAOFAMILIAR_DADOSPESSClick(
  Sender: TObject);
begin
  frmCadastroInscricoes.Close;
  frmPESQUISA_INSCRITOS_TRIAGEM.Show;
end;

procedure TfrmCadastroInscricoes.SpeedButton10Click(Sender: TObject);
begin
  frmCadastroInscricoes.Close;
  frmPESQUISA_INSCRITOS_TRIAGEM.Show;
end;

procedure TfrmCadastroInscricoes.ModoTelaPendencias(Modo: String);
begin
  if Modo = 'P' then
  begin
    chk_PENDENCIA_CARTPROF.Checked          := False;
    chk_PENDENCIA_COMPROVRENDA.Checked      := False;
    chk_PENDENCIA_COMPROVALUGUEL.Checked    := False;
    chk_PENDENCIA_COMPROVTEMPORESID.Checked := False;
    chk_PENDENCIA_NIS.Checked               := False;
    chk_PENDENCIA_DECLARAESCOLA.Checked     := False;
    chk_PENDENCIA_SUS.Checked               := False;
    chk_PENDENCIA_CPF.Checked               := False;
    chk_PENDENCIA_OUTROS.Checked            := False;

    edtPENDENCIA_DETALHES_NIS.Clear;
    edtPENDENCIA_DETALHES_ESCOLA.Clear;
    edtPENDENCIAS_DETALHE_SUS.Clear;
    edtPENDENCIAS_DETALHE_CPF.Clear;
    med_PENDENCIA_OUTROS.Clear;

    gpbPENDENCIAS.Enabled                  := False;
    gpbDADOS_PENDENCIAS.Enabled            := True;

    btnNOVA_PENDENCIA.Enabled              := True;
    btnGRAVAR_PENDENCIA.Enabled            := True;
    btnEDITAR_PENDENCIA.Visible            := False;
    btnCANCELA_PENDENCIA.Enabled           := False;
    btnPENDENCIA_CHAMAPESQUISA.Enabled     := True;
    btnVISUALIZAR_DOCPENDENCIAS.Enabled    := True;
    btnSAIR_INSCRICAO_PENDENCIA.Enabled    := True;
    btnFINALIZAR_PENDENCIAS.Enabled        := True;
  end
  else if Modo = 'N' then
  begin
    gpbPENDENCIAS.Enabled                  := True;
    gpbDADOS_PENDENCIAS.Enabled            := False;

    btnNOVA_PENDENCIA.Enabled              := False;
    btnGRAVAR_PENDENCIA.Enabled            := True;
    btnEDITAR_PENDENCIA.Visible            := False;
    btnCANCELA_PENDENCIA.Enabled           := True;
    btnPENDENCIA_CHAMAPESQUISA.Enabled     := False;
    btnVISUALIZAR_DOCPENDENCIAS.Enabled    := False;
    btnSAIR_INSCRICAO_PENDENCIA.Enabled    := False;
    btnFINALIZAR_PENDENCIAS.Enabled        := False;
  end
  else if Modo = 'A' then
  begin
    gpbPENDENCIAS.Enabled                  := True;
    gpbDADOS_PENDENCIAS.Enabled            := False;

    btnNOVA_PENDENCIA.Enabled              := False;
    btnGRAVAR_PENDENCIA.Enabled            := True;
    btnEDITAR_PENDENCIA.Visible            := False;
    btnCANCELA_PENDENCIA.Enabled           := True;
    btnPENDENCIA_CHAMAPESQUISA.Enabled     := False;
    btnVISUALIZAR_DOCPENDENCIAS.Enabled    := False;
    btnSAIR_INSCRICAO_PENDENCIA.Enabled    := False;
    btnFINALIZAR_PENDENCIAS.Enabled        := False;
  end;
end;

procedure TfrmCadastroInscricoes.btnNOVA_PENDENCIAClick(Sender: TObject);
begin
  //Verifica se existe um registro de pend�ncia no banco para a inscri��o atual

  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.RecordCount = 0) then
  begin
    ModoTelaPendencias('N');
    vTipo_EdicaoPend := 'I';

    chk_PENDENCIA_CARTPROF.Checked          := False;
    chk_PENDENCIA_COMPROVRENDA.Checked      := False;
    chk_PENDENCIA_COMPROVALUGUEL.Checked    := False;
    chk_PENDENCIA_COMPROVTEMPORESID.Checked := False;
    chk_PENDENCIA_NIS.Checked               := False;
    chk_PENDENCIA_DECLARAESCOLA.Checked     := False;
    chk_PENDENCIA_SUS.Checked               := False;
    chk_PENDENCIA_CPF.Checked               := False;
    chk_PENDENCIA_OUTROS.Checked            := False;
    edtPENDENCIA_DETALHES_NIS.Clear;
    edtPENDENCIA_DETALHES_ESCOLA.Clear;
    edtPENDENCIAS_DETALHE_SUS.Clear;
    edtPENDENCIAS_DETALHE_CPF.Clear;
  end
  else
  begin
    ModoTelaPendencias('A');
    CarregaDadosPendenciaDocs;
    vTipo_EdicaoPend := 'U';

    Application.MessageBox ('J� existe um registro de Pend�ncia de Documentos lan�ado para o inscrito atual. Verifique a situa��o!',
                            '[Sistema Deca] - Erro no lan�amento de nova Pend�ncia',
                            MB_OK + MB_ICONINFORMATION);

    dbgPendencias.OnCellClick(dbgPendencias.Columns[0]);

  end;
end;

procedure TfrmCadastroInscricoes.btnCANCELA_PENDENCIAClick(
  Sender: TObject);
begin
  ModoTelaPendencias('P');
end;

procedure TfrmCadastroInscricoes.btnCALCULA_PONTOSClick(Sender: TObject);
begin
  CalculaPontos;
end;

procedure TfrmCadastroInscricoes.CarregaDadosPendenciaDocs;
begin
  try
    with dmTriagemDeca.cdsSel_PendenciaDocumentos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value := dmTriagemDeca.cdsSel_CadastroFull.FieldByName('cod_inscricao').Value; 
      Open;
      dbgPendencias.Refresh;
      ModoTelaPendencias('P');
    end;
  except end;
end;

procedure TfrmCadastroInscricoes.btnGRAVAR_PENDENCIAClick(Sender: TObject);
begin

  if (vTipo_EdicaoPend = 'I') then
  begin
    VerificaSeTemPendenciaDocumento;
    if (Application.MessageBox ('Deseja incluir as informa��es de documentos pendentes para a inscri��o atual?',
                               '[Sistema Deca] - Inclus�o de documentos pendentes',
                               MB_YESNO + MB_ICONQUESTION) = idYes) and (vTemPendenciaDoc) then
    begin
      InserirDadosPendenciaDocs;
    end
    else
    begin
      Application.MessageBox ('Voc� n�o informou nenhum documento pendente para gerar pend�ncia. Verifique os campos e tente novamente!',
                              '[Sistema Deca] - Faltam documentos para gerar Pend�ncia',
                              MB_OK + MB_ICONWARNING);
    end;
  end else if (vTipo_EdicaoPend = 'U') then
  begin
    VerificaSeTemPendenciaDocumento;

    //Verificar qual status da inscri��o, pois INSCRITOS n�o devem permitir fazer altera��o no registro de pend�ncia
    if (Application.MessageBox ('Deseja alterar as informa��es de documentos pendentes para a inscri��o atual?',
                               '[Sistema Deca] - Altera��o de documentos pendentes',
                               MB_YESNO + MB_ICONQUESTION) = idYes) then //and (vTemPendenciaDoc) then
    begin
      AlterarDadosPendencias;
    end
    else
    begin
      Application.MessageBox ('Voc� n�o informou nenhum documento pendente para gerar pend�ncia. Verifique os campos e tente novamente!',
                              '[Sistema Deca] - Faltam documentos para gerar Pend�ncia',
                              MB_OK + MB_ICONWARNING);
    end;
  end;
  ModoTelaPendencias('P');
end;

procedure TfrmCadastroInscricoes.VerificaSeTemPendenciaDocumento;
begin
  //Verifica se algum campo na tela est� "checado"
  if (chk_PENDENCIA_CARTPROF.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit
  end
  else if (chk_PENDENCIA_COMPROVRENDA.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_COMPROVALUGUEL.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_COMPROVTEMPORESID.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_NIS.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_DECLARAESCOLA.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_SUS.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_CPF.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else if (chk_PENDENCIA_OUTROS.Checked = True) then
  begin
    vTemPendenciaDoc := True;
    Exit;
  end
  else
  begin
    vTemPendenciaDoc := False;
    Exit;
  end;
end;

procedure TfrmCadastroInscricoes.InserirDadosPendenciaDocs;
begin
  try
    with dmTriagemDeca.cdsIns_PendenciaDocumentos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value            := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_inscricao').Value;
      Params.ParamByName ('@pe_cod_usuario').Value              := vvCOD_USUARIO;

      if (chk_PENDENCIA_CARTPROF.Checked = True)          then Params.ParamByName ('@pe_ind_cartprof').Value       := 1 else Params.ParamByName ('@pe_ind_cartprof').Value       := 0;
      if (chk_PENDENCIA_COMPROVRENDA.Checked = True)      then Params.ParamByName ('@pe_ind_comprenda').Value      := 1 else Params.ParamByName ('@pe_ind_comprenda').Value      := 0;
      if (chk_PENDENCIA_COMPROVALUGUEL.Checked = True)    then Params.ParamByName ('@pe_ind_compaluguel').Value    := 1 else Params.ParamByName ('@pe_ind_compaluguel').Value    := 0;
      if (chk_PENDENCIA_COMPROVTEMPORESID.Checked = True) then Params.ParamByName ('@pe_ind_comptemporesid').Value := 1 else Params.ParamByName ('@pe_ind_comptemporesid').Value := 0;
      if (chk_PENDENCIA_NIS.Checked = True)               then Params.ParamByName ('@pe_ind_nis').Value            := 1 else Params.ParamByName ('@pe_ind_nis').Value            := 0;
      if (chk_PENDENCIA_DECLARAESCOLA.Checked = True)     then Params.ParamByName ('@pe_ind_declaraescola').Value  := 1 else Params.ParamByName ('@pe_ind_declaraescola').Value  := 0;
      if (chk_PENDENCIA_SUS.Checked = True)               then Params.ParamByName ('@pe_ind_sus').Value            := 1 else Params.ParamByName ('@pe_ind_sus').Value            := 0;
      if (chk_PENDENCIA_CPF.Checked = True)               then Params.ParamByName ('@pe_ind_cpf').Value            := 1 else Params.ParamByName ('@pe_ind_cpf').Value            := 0;
      if (chk_PENDENCIA_OUTROS.Checked = True)            then Params.ParamByName ('@pe_ind_outros').Value         := 1 else Params.ParamByName ('@pe_ind_outros').Value         := 0;

      Params.ParamByName ('@pe_dsc_detalhes_nis').Value         := GetValue(edtPENDENCIA_DETALHES_NIS.Text);
      Params.ParamByName ('@pe_dsc_detalhes_declescolar').Value := GetValue(edtPENDENCIA_DETALHES_ESCOLA.Text);
      Params.ParamByName ('@pe_dsc_detalhes_sus').Value         := GetValue(edtPENDENCIAS_DETALHE_SUS.Text);
      Params.ParamByName ('@pe_dsc_detalhes_cpf').Value         := GetValue(edtPENDENCIAS_DETALHE_CPF.Text);
      Params.ParamByName ('@pe_dsc_detalhes_outros').Value      := GetValue(med_PENDENCIA_OUTROS.Text);
      Execute;

      with dmTriagemDeca.cdsSel_PendenciaDocumentos do
      begin
        Close;
        Params.ParamByName ('@pe_cod_inscricao').Value := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_inscricao').Value;
        Open;
        dbgPendencias.Refresh;
      end;
    end;
  except
    Application.MessageBox ('Um erro ocorreu na tentativa de inserir dados de pend�ncia de documentos. Favor tentar novamente ou entrar em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro inserindo pend�ncias',
                            MB_OK + MB_ICONERROR);
    ModoTelaPendencias('P');
  end;
end;

procedure TfrmCadastroInscricoes.AlterarDadosPendencias;
begin
  try
    with dmTriagemDeca.cdsUpd_PendenciaDocumentos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value            := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_inscricao').Value;
      Params.ParamByName ('@pe_cod_usuario').Value              := vvCOD_USUARIO;

      if (chk_PENDENCIA_CARTPROF.Checked)          then Params.ParamByName ('@pe_ind_cartprof').Value       := 1 else Params.ParamByName ('@pe_ind_cartprof').Value       := 0;
      if (chk_PENDENCIA_COMPROVRENDA.Checked)      then Params.ParamByName ('@pe_ind_comprenda').Value      := 1 else Params.ParamByName ('@pe_ind_comprenda').Value      := 0;
      if (chk_PENDENCIA_COMPROVALUGUEL.Checked)    then Params.ParamByName ('@pe_ind_compaluguel').Value    := 1 else Params.ParamByName ('@pe_ind_compaluguel').Value    := 0;
      if (chk_PENDENCIA_COMPROVTEMPORESID.Checked) then Params.ParamByName ('@pe_ind_comptemporesid').Value := 1 else Params.ParamByName ('@pe_ind_comptemporesid').Value := 0;
      if (chk_PENDENCIA_NIS.Checked)               then Params.ParamByName ('@pe_ind_nis').Value            := 1 else Params.ParamByName ('@pe_ind_nis').Value            := 0;
      if (chk_PENDENCIA_NIS.Checked)               then Params.ParamByName ('@pe_ind_nis').Value            := 1 else Params.ParamByName ('@pe_ind_nis').Value            := 0;
      if (chk_PENDENCIA_DECLARAESCOLA.Checked)     then Params.ParamByName ('@pe_ind_declaraescola').Value  := 1 else Params.ParamByName ('@pe_ind_declaraescola').Value  := 0;
      if (chk_PENDENCIA_SUS.Checked)               then Params.ParamByName ('@pe_ind_sus').Value            := 1 else Params.ParamByName ('@pe_ind_sus').Value            := 0;
      if (chk_PENDENCIA_CPF.Checked)               then Params.ParamByName ('@pe_ind_cpf').Value            := 1 else Params.ParamByName ('@pe_ind_cpf').Value            := 0;
      if (chk_PENDENCIA_OUTROS.Checked)            then Params.ParamByName ('@pe_ind_outros').Value         := 1 else Params.ParamByName ('@pe_ind_outros').Value         := 0;

      Params.ParamByName ('@pe_dsc_detalhes_nis').Value         := GetValue(edtPENDENCIA_DETALHES_NIS.Text);
      Params.ParamByName ('@pe_dsc_detalhes_declescolar').Value := GetValue(edtPENDENCIA_DETALHES_ESCOLA.Text);
      Params.ParamByName ('@pe_dsc_detalhes_sus').Value         := GetValue(edtPENDENCIAS_DETALHE_SUS.Text);
      Params.ParamByName ('@pe_dsc_detalhes_cpf').Value         := GetValue(edtPENDENCIAS_DETALHE_CPF.Text);
      Params.ParamByName ('@pe_dsc_detalhes_outros').Value      := GetValue(med_PENDENCIA_OUTROS.Text);

      Execute;

      with dmTriagemDeca.cdsSel_PendenciaDocumentos do
      begin
        Close;
        Params.ParamByName ('@pe_cod_inscricao').Value := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_inscricao').Value;
        Open;
        dbgPendencias.Refresh;
      end;

    end;
  except
    Application.MessageBox ('Um erro ocorreu na tentativa de alterar dados de pend�ncia de documentos. Favor tentar novamente ou entrar em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro alterando pend�ncias',
                            MB_OK + MB_ICONERROR);
    //ModoTelaPendencias('P');
  end;
  ModoTelaPendencias('P');
end;

procedure TfrmCadastroInscricoes.dbgPendenciasCellClick(Column: TColumn);
begin
  ModoTelaPendencias('A');

  //Repassa os valores do banco de dados para a tela
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_cartprof').Value       = True) then chk_PENDENCIA_CARTPROF.Checked          := True else chk_PENDENCIA_CARTPROF.Checked          := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_comprenda').Value      = True) then chk_PENDENCIA_COMPROVRENDA.Checked      := True else chk_PENDENCIA_COMPROVRENDA.Checked      := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_compaluguel').Value    = True) then chk_PENDENCIA_COMPROVALUGUEL.Checked    := True else chk_PENDENCIA_COMPROVALUGUEL.Checked    := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_comptemporesid').Value = True) then chk_PENDENCIA_COMPROVTEMPORESID.Checked := True else chk_PENDENCIA_COMPROVTEMPORESID.Checked := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_nis').Value            = True) then chk_PENDENCIA_NIS.Checked               := True else chk_PENDENCIA_NIS.Checked               := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_declaraescola').Value  = True) then chk_PENDENCIA_DECLARAESCOLA.Checked     := True else chk_PENDENCIA_DECLARAESCOLA.Checked     := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_sus').Value            = True) then chk_PENDENCIA_SUS.Checked               := True else chk_PENDENCIA_SUS.Checked               := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_cpf').Value            = True) then chk_PENDENCIA_CPF.Checked               := True else chk_PENDENCIA_CPF.Checked               := False;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('ind_outros').Value         = True) then chk_PENDENCIA_OUTROS.Checked            := True else chk_PENDENCIA_OUTROS.Checked            := False;

  //if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('DataAltera').IsNull) or (Length(dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('DataAltera').Value) = 0) then mskDATA_ULTIMALATERACAO.Text := '' else mskDATA_ULTIMALATERACAO.Text := dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('DataAltera').AsString;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_nis').IsNull)         then edtPENDENCIA_DETALHES_NIS.Text    := '' else edtPENDENCIA_DETALHES_NIS.Text    := dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_nis').AsString;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_declescolar').IsNull) then edtPENDENCIA_DETALHES_ESCOLA.Text := '' else edtPENDENCIA_DETALHES_ESCOLA.Text := dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_declescolar').AsString;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_sus').IsNull)         then edtPENDENCIAS_DETALHE_SUS.Text    := '' else edtPENDENCIAS_DETALHE_SUS.Text    := dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_sus').AsString;
  if (dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_cpf').IsNull)         then edtPENDENCIAS_DETALHE_CPF.Text    := '' else edtPENDENCIAS_DETALHE_CPF.Text    := dmTriagemDeca.cdsSel_PendenciaDocumentos.FieldByName ('dsc_detalhes_cpf').AsString;

  vTipo_EdicaoPend := 'U';

end;

procedure TfrmCadastroInscricoes.btnFINALIZAR_PENDENCIASClick(
  Sender: TObject);
begin
  //Perguntar ao usu�rio se deseja realmente fazer a finaliza��o da pend�ncia




  //ATUALIZA A TABELA DE PENDENCIADOCUMENTOS
  try
    with dmTriagemDeca.cdsUpd_PendenciaDocumentos do
    begin
      Close;
      Params.ParamByName ('@pe_cod_inscricao').Value            := dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('cod_inscricao').Value;
      Params.ParamByName ('@pe_cod_usuario').Value              := vvCOD_USUARIO;
      Params.ParamByName ('@pe_ind_cartprof').Value             := 0;
      Params.ParamByName ('@pe_ind_comprenda').Value            := 0;
      Params.ParamByName ('@pe_ind_compaluguel').Value          := 0;
      Params.ParamByName ('@pe_ind_comptemporesid').Value       := 0;
      Params.ParamByName ('@pe_ind_nis').Value                  := 0;
      Params.ParamByName ('@pe_ind_nis').Value                  := 0;
      Params.ParamByName ('@pe_ind_declaraescola').Value        := 0;
      Params.ParamByName ('@pe_ind_sus').Value                  := 0;
      Params.ParamByName ('@pe_ind_cpf').Value                  := 0;
      Params.ParamByName ('@pe_ind_outros').Value               := 0;
      Params.ParamByName ('@pe_dsc_detalhes_nis').Value         := Null;
      Params.ParamByName ('@pe_dsc_detalhes_declescolar').Value := Null;
      Params.ParamByName ('@pe_dsc_detalhes_sus').Value         := Null;
      Params.ParamByName ('@pe_dsc_detalhes_cpf').Value         := Null;
      Params.ParamByName ('@pe_dsc_detalhes_outros').Value      := Null;
      Execute;

      with dmTriagemDeca.cdsUpd_AlteraStatusInscricao do
      begin
        Close;
        Params.ParamByName ('@pe_num_inscricao').Value      := StrToInt(mskNUMERO_INSCRICAO.Text);
        Params.ParamByName ('@pe_num_inscricao_digt').Value := StrToInt(mskINSCRICAO_DIGITO.Text);
        Params.ParamByName ('@pe_cod_situacao').Value       := 12; //Inscrito
        Execute;
        CarregaDadosInscritos;
      end;
      ModoTelaPendencias('P');
      Application.MessageBox ('Ser� necess�rio fechar a ficha de inscri��o e abr�-la novamente a partir da pesquisa para ver as modifica��es efetuadas. ',
                              '[Sistema Deca] - Fechar janela de inscri��o.',
                              MB_OK + MB_ICONINFORMATION);
      //frmCadastroInscricoes.Close;
    end;
  except
    Application.MessageBox ('Um erro ocorreu na tentativa de alterar dados de pend�ncia de documentos. Favor tentar novamente ou entrar em contato com o Administrador do Sistema',
                            '[Sistema Deca] - Erro alterando pend�ncias',
                            MB_OK + MB_ICONERROR);
    //ModoTelaPendencias('P');
  end;
  ModoTelaPendencias('P');

  //ATUALIZA O STATUS DO INSCRITO
end;

procedure TfrmCadastroInscricoes.CarregaCombos;
begin

  //Carregar as StringLilsts da Aba de Dados Inscrito - Dados Cadastrais e Aba da Composi��o Familiar
  vListaEstadoCivil1              := TstringList.Create();
  vListaEstadoCivil2              := TstringList.Create();

  vListaNaturalidade1             := TstringList.Create();
  vListaNaturalidade2             := TstringList.Create();

  vListaNacionalidade1            := TStringList.Create();
  vListaNacionalidade2            := TStringList.Create();

  vListaBairros1                  := TStringList.Create();
  vListaBairros2                  := TStringList.Create();

  vListaUnidade1                  := TStringList.Create();
  vListaUnidade2                  := TStringList.Create();

  vListaEmissor1                  := TStringList.Create();
  vListaEmissor2                  := TStringList.Create();

  vListaRelacaoResponsabilidade1  := TStringList.Create();
  vListaRelacaoResponsabilidade2  := TStringList.Create();

  vListaEscolaridade1             := TStringList.Create();
  vListaEscolaridade2             := TStringList.Create();

  vListaSituacao1                 := TStringList.Create();
  vListaSituacao2                 := TStringList.Create();

  vListaNaturezaHabitacao1        := TStringList.Create();
  vListaNaturezaHabitacao2        := TStringList.Create();

  vListaTipoHabitacao1            := TStringList.Create();
  vListaTipoHabitacao2            := TStringList.Create();

  vListaInfraestrutura1           := TStringList.Create();
  vListaInfraestrutura2           := TStringList.Create();

  vListaInstalacoesSantitarias1   := TStringList.Create();
  vListaInstalacoesSantitarias2   := TStringList.Create();

  vListaCondicaoesHabitabilidade1 := TStringList.Create();
  vListaCondicaoesHabitabilidade2 := TStringList.Create();

  vListaRegiao1                   := TStringList.Create();
  vListaRegiao2                   := TStringList.Create();

  vListaSerie1                    := TStringList.Create();
  vListaSerie2                    := TStringList.Create();

  vListaEscolaridade1             := TStringList.Create();
  vListaEscolaridade2             := TStringList.Create();

  vListaParentesco1               := TStringList.Create();
  vListaParentesco2               := TStringList.Create();


  //Carregar a lista de Estado Civil
  with dmTriagemDeca.cdsSel_Par_EstadoCivil do
  begin
    Close;
    Params.ParamByName ('@cod_estadocivil').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_EstadoCivil.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_EstadoCivil.eof) do
      begin
        cmbESTADO_CIVIL_INSCRITO.Items.Add (dmTriagemDeca.cdsSel_Par_EstadoCivil.FieldByName ('dsc_estadocivil').Value);
        cmbESTADO_CIVIL_FAMILIAR.Items.Add (dmTriagemDeca.cdsSel_Par_EstadoCivil.FieldByName ('dsc_estadocivil').Value);
        vListaEstadoCivil1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_EstadoCivil.FieldByName ('cod_estadocivil').Value));
        vListaEstadoCivil2.Add (dmTriagemDeca.cdsSel_Par_EstadoCivil.FieldByName ('dsc_estadocivil').Value);
        dmTriagemDeca.cdsSel_Par_EstadoCivil.Next;
      end;
    end;
  end;


  //Carregar a lista de Situa��o (Posi��o)
  with dmTriagemDeca.cdsSel_Par_Posicao do
  begin
    Close;
    Params.ParamByName ('@cod_posicao').Value  := Null;
    Params.ParamByName ('@pe_ind_ativo').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Posicao.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Posicao.eof) do
      begin
        cmbSITUACAO_INSCRICAO.Items.Add (dmTriagemDeca.cdsSel_Par_Posicao.FieldByName ('dsc_posicao').Value);
        vListaSituacao1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Posicao.FieldByName ('cod_posicao').Value));
        vListaSituacao2.Add (dmTriagemDeca.cdsSel_Par_Posicao.FieldByName ('dsc_posicao').Value);
        dmTriagemDeca.cdsSel_Par_Posicao.Next;
      end;
    end;
  end;


  //Carregar a lista de Cidades
  with dmTriagemDeca.cdsSel_Par_Cidades do
  begin
    Close;
    Params.ParamByName ('@cod_cidade').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Cidades.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Cidades.eof) do
      begin
        cmbNATURALIDADE_INSCRITO.Items.Add (dmTriagemDeca.cdsSel_Par_Cidades.FieldByName ('dsc_cidade').Value);
        cmbNATURALIDADE_FAMILIAR.Items.Add (dmTriagemDeca.cdsSel_Par_Cidades.FieldByName ('dsc_cidade').Value);
        vListaNaturalidade1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Cidades.FieldByName ('cod_cidade').Value));
        vListaNaturalidade2.Add (dmTriagemDeca.cdsSel_Par_Cidades.FieldByName ('dsc_cidade').Value);
        dmTriagemDeca.cdsSel_Par_Cidades.Next;
      end;
    end;
  end;


  //Carregar a lista de Nacionalidades
  with dmTriagemDeca.cdsSel_Par_Nacionalidade do
  begin
    Close;
    Params.ParamByName ('@cod_nacionalidade').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Nacionalidade.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Nacionalidade.eof) do
      begin
        cmbNACIONALIDADE_INSCRITO.Items.Add (dmTriagemDeca.cdsSel_Par_Nacionalidade.FieldByName ('dsc_nacionalidade').Value);
        cmbNACIONALIDADE_FAMILIAR.Items.Add (dmTriagemDeca.cdsSel_Par_Nacionalidade.FieldByName ('dsc_nacionalidade').Value);
        vListaNacionalidade1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Nacionalidade.FieldByName ('cod_nacionalidade').Value));
        vListaNacionalidade2.Add (dmTriagemDeca.cdsSel_Par_Nacionalidade.FieldByName ('dsc_nacionalidade').Value);
        dmTriagemDeca.cdsSel_Par_Nacionalidade.Next;
      end;
    end;
  end;

  //Carregar a lista de Bairros
  with dmTriagemDeca.cdsSel_Cadastro_Bairro do
  begin
    Close;
    Params.ParamByName ('@cod_bairro').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Cadastro_Bairro.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Cadastro_Bairro.eof) do
      begin
        cmbBAIRRO_INSCRITO.Items.Add (dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('dsc_bairro').Value);
        vListaBairros1.Add (IntToStr(dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('cod_bairro').Value));
        vListaBairros2.Add (dmTriagemDeca.cdsSel_Cadastro_Bairro.FieldByName ('dsc_bairro').Value);
        dmTriagemDeca.cdsSel_Cadastro_Bairro.Next;
      end;
    end;
  end;

  //Carregar a rela��o de Regi�es
  with dmTriagemDeca.cdsSel_Par_Cadastro_Regioes do
  begin
    Close;
    Params.ParamByName ('@cod_regiao').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.eof) do
      begin
        cmbREGIAO.Items.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        vListaRegiao1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('cod_regiao').Value));
        vListaRegiao2.Add (dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.FieldByName ('dsc_regiao').Value);
        dmTriagemDeca.cdsSel_Par_Cadastro_Regioes.Next;
      end;
    end;
  end;


  //Carregar a lista de Unidades
  with dmDeca.cdsSel_Unidade do
  begin
    Close;
    Params.ParamByName ('@pe_cod_unidade').Value := Null;
    Params.ParamByName ('@pe_nom_unidade').Value := Null;
    Open;

    if (dmDeca.cdsSel_Unidade.RecordCount > 0) then
    begin
      while not (dmDeca.cdsSel_Unidade.eof) do
      begin
        //Carrega apenas as unidades "ATIVAS"
        if (dmDeca.cdsSel_Unidade.FieldByName ('flg_status').Value = 0) then
        begin
          cmbUNIDADE_REFERENCIA_PARA_INSCRITO.Items.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
          vListaUnidade1.Add (IntToStr(dmDeca.cdsSel_Unidade.FieldByName ('cod_unidade').Value));
          vListaUnidade2.Add (dmDeca.cdsSel_Unidade.FieldByName ('nom_unidade').Value);
        end;
        dmDeca.cdsSel_Unidade.Next;
      end;
    end;
  end;

  //Carregar a lista de �rg�os Emissores de Documento de Identidade
  with dmTriagemDeca.cdsSel_Cadastro_Emissor do
  begin
    Close;
    Params.ParamByName ('@pe_cod_emissor').Value := Null;
    Params.ParamByName ('@pe_ind_status').Value  := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Cadastro_Emissor.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Cadastro_Emissor.eof) do
      begin
        cmbORGAO_EMISSOR_RG_INSCRITO.Items.Add (dmTriagemDeca.cdsSel_Cadastro_Emissor.FieldByName ('dsc_emissor').Value);
        cmbORGAO_EMISSOR_RG_FAMILIAR.Items.Add (dmTriagemDeca.cdsSel_Cadastro_Emissor.FieldByName ('dsc_emissor').Value);
        vListaEmissor1.Add (IntToStr(dmTriagemDeca.cdsSel_Cadastro_Emissor.FieldByName ('cod_emissor').Value));
        vListaEmissor2.Add (dmTriagemDeca.cdsSel_Cadastro_Emissor.FieldByName ('dsc_emissor').Value);
        dmTriagemDeca.cdsSel_Cadastro_Emissor.Next;
      end;
    end;
  end;



  with dmDeca.cdsSel_Escola do
  begin
    Close;
    Params.ParamByName ('@pe_cod_escola').Value := Null;
    Params.ParamByName ('@pe_nom_escola').Value := Null;
    Open;

    if (dmDeca.cdsSel_Escola.RecordCount > 0) then
    begin
      while not (dmDeca.cdsSel_Escola.eof) do
      begin
        cmbESCOLA_ATUAL_INSCRITO.Items.Add(dmDeca.cdsSel_Escola.FieldByName ('nom_escola').Value);
        dmDeca.cdsSel_Escola.Next;
      end;

    end;
  end;


  //Carregar a lista de N�vel de Escolaridade - Aba Dados Composi��o Familiar - Dados Pessoais do Familiar
  with dmTriagemDeca.cdsSel_Par_Escolaridade do
  begin
    Close;
    Params.ParamByName ('@cod_escolaridade').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Escolaridade.RecordCount > 0 ) then
    begin
      while not(dmTriagemDeca.cdsSel_Par_Escolaridade.eof) do
      begin
        cmbNIVEL_ESCOLARIDADE_FAMILIAR.Items.Add(dmTriagemDeca.cdsSel_Par_Escolaridade.FieldByName ('dsc_escolaridade').Value);
        vListaEscolaridade1.Add(IntToStr(dmTriagemDeca.cdsSel_Par_Escolaridade.FieldByName ('cod_escolaridade').Value));
        vListaEscolaridade2.Add(dmTriagemDeca.cdsSel_Par_Escolaridade.FieldByName ('dsc_escolaridade').Value);
        dmTriagemDeca.cdsSel_Par_Escolaridade.Next;
      end;
    end;
  end;


  cmbSERIE_ATUAL_INSCRITO.Clear;
  with dmDeca.cdsSel_EscolaSerie do
  begin
    Close;
    Params.ParamByName ('@pe_cod_id_escola_serie').Value := Null;

    {
    //Recuperar o c�digo da escola no Deca para trazer as s�ries....
    with dmDeca.cdsSel_Escola do
    begin
      Close;
      Params.ParamByName ('@pe_cod_escola').Value := Null;
      Params.ParamByName ('@pe_nom_escola').Value := Null; //Trim(dmTriagemDeca.cdsSel_CadastroFull.FieldByName ('dsc_NomeEscola').Value);
      Open;
      if (dmDeca.cdsSel_Escola.RecordCount > 0)
        then Params.ParamByName ('@pe_cod_escola').Value := dmDeca.cdsSel_Escola.FieldByName ('cod_escola').Value
        else Params.ParamByName ('@pe_cod_escola').Value := Null;
    end;
    }

    Params.ParamByName ('@pe_cod_id_serie').Value := Null;
    Open;

    if (dmDeca.cdsSel_EscolaSerie.RecordCount > 0 ) then
    begin
      while not (dmDeca.cdsSel_EscolaSerie.eof) do
      begin
        cmbSERIE_ATUAL_INSCRITO.Items.Add(dmDeca.cdsSel_EscolaSerie.FieldByName ('dsc_serie').Value);
        vListaSerie1.Add (IntToStr(dmDeca.cdsSel_EscolaSerie.FieldByName ('cod_id_serie').Value));
        vListaSerie2.Add (dmDeca.cdsSel_EscolaSerie.FieldByName ('dsc_serie').Value);
        dmDeca.cdsSel_EscolaSerie.Next;
      end;

    end;
  end;



  //Carregar os combos da Aba Situa��o Social do Inscrito
  //Rela��o de Responsabilidade
  cmbRELACAO_RESPONSABILIDADE.Clear;
  with dmTriagemDeca.cdsSel_Par_RelResponsabilidade do
  begin
    Close;
    Params.ParamByName ('@cod_relResponsabilidade').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_RelResponsabilidade.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_RelResponsabilidade.eof) do
      begin
        if (dmTriagemDeca.cdsSel_Par_RelResponsabilidade.FieldByName ('num_portaria').Value) = 17 then
        begin
          cmbRELACAO_RESPONSABILIDADE.Items.Add (dmTriagemDeca.cdsSel_Par_RelResponsabilidade.FieldByName ('dsc_relResponsabilidade').Value);
          vListaRelacaoResponsabilidade1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_RelResponsabilidade.FieldByName ('cod_relResponsabilidade').Value));
          vListaRelacaoResponsabilidade2.Add (dmTriagemDeca.cdsSel_Par_RelResponsabilidade.FieldByName ('dsc_relResponsabilidade').Value);
        end;
        dmTriagemDeca.cdsSel_Par_RelResponsabilidade.Next;
      end;
    end;
  end;

  //Carregar os combos da Aba Situa��o de Moradia


  //Natureza da Habita��o
  with dmTriagemDeca.cdsSel_Par_Natureza do
  begin
    Close;
    Params.ParamByName ('@cod_natureza_hab').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Natureza.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Natureza.eof) do
      begin
      // PORTARIA VIGENTE DEZ/2017 (num_portaria=17)
        if (dmTriagemDeca.cdsSel_Par_Natureza.FieldByName ('num_portaria').Value) = 17 then
        begin
          cmbNATUREZA_HABITACAO.Items.Add (dmTriagemDeca.cdsSel_Par_Natureza.FieldByName ('dsc_natureza_hab').Value);
          vListaNaturezaHabitacao1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Natureza.FieldByName ('cod_natureza_hab').Value));
          vListaNaturezaHabitacao2.Add (dmTriagemDeca.cdsSel_Par_Natureza.FieldByName ('dsc_natureza_hab').Value);
        end;
        dmTriagemDeca.cdsSel_Par_Natureza.Next;
      end;
    end;
  end;

  //Tipo da Habita��o
  with dmTriagemDeca.cdsSel_Par_TipoHabitacao do
  begin
    Close;
    Params.ParamByName ('@cod_tipo_hab').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_TipoHabitacao.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_TipoHabitacao.eof) do
      begin
       // PORTARIA VIGENTE DEZ/2017 (num_portaria=17)
        if (dmTriagemDeca.cdsSel_Par_TipoHabitacao.FieldByName ('num_portaria').Value) = 17 then
        begin
          cmbTIPO_HABITACAO.Items.Add (dmTriagemDeca.cdsSel_Par_TipoHabitacao.FieldByName ('dsc_tipo_hab').Value);
          vListaTipoHabitacao1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_TipoHabitacao.FieldByName ('cod_tipo_hab').Value));
          vListaTipoHabitacao2.Add (dmTriagemDeca.cdsSel_Par_TipoHabitacao.FieldByName ('dsc_tipo_hab').Value);
        end;
        dmTriagemDeca.cdsSel_Par_TipoHabitacao.Next;
      end;
    end;
  end;

  //Infraestrutura
  with dmTriagemDeca.cdsSel_Par_Infraestrutura do
  begin
    Close;
    Params.ParamByName ('@cod_infra_estrutura').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Infraestrutura.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Infraestrutura.eof) do
      begin
        // PORTARIA VIGENTE DEZ/2017 (num_portaria=17)
        if (dmTriagemDeca.cdsSel_Par_Infraestrutura.FieldByName ('num_portaria').Value) = 17 then
        begin
          cmbINFRAESTRUTURA.Items.Add (dmTriagemDeca.cdsSel_Par_Infraestrutura.FieldByName ('dsc_infra_estrutura').Value);
          vListaInfraestrutura1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_Infraestrutura.FieldByName ('cod_infra_estrutura').Value));
          vListaInfraestrutura2.Add (dmTriagemDeca.cdsSel_Par_Infraestrutura.FieldByName ('dsc_infra_estrutura').Value);
        end;
        dmTriagemDeca.cdsSel_Par_Infraestrutura.Next;
      end;
    end;
  end;


  //Instala��es Sanit�rias
  with dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias do
  begin
    Close;
    Params.ParamByName ('@cod_inst_sanitaria').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.eof) do
      begin
        // PORTARIA VIGENTE DEZ/2017 (num_portaria=17)
        if (dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.FieldByName ('num_portaria').Value) = 17 then
        begin
          cmbINSTALACOES_SANITARIAS.Items.Add (dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.FieldByName ('dsc_inst_sanitaria').Value);
          vListaInstalacoesSantitarias1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.FieldByName ('cod_inst_sanitaria').Value));
          vListaInstalacoesSantitarias2.Add (dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.FieldByName ('dsc_inst_sanitaria').Value);
        end;
        dmTriagemDeca.cdsSel_Par_InstalacoesSanitarias.Next;
      end;
    end;
  end;

  //Condi��es de Habitabilidade
  with dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade do
  begin
    Close;
    Params.ParamByName ('@cod_cond_hab').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.eof) do
      begin
       // PORTARIA VIGENTE DEZ/2017 (num_portaria=17)
        if (dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.FieldByName ('num_portaria').Value) = 17 then
        begin
          cmbCONDICOES_HABITABILIDADE.Items.Add (dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.FieldByName ('dsc_condicaohabita').Value);
          vListaCondicaoesHabitabilidade1.Add (IntToStr(dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.FieldByName ('cod_cond_hab').Value));
          vListaCondicaoesHabitabilidade2.Add (dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.FieldByName ('dsc_condicaohabita').Value);
        end;
        dmTriagemDeca.cdsSel_Par_CondicaoHabitabilidade.Next;
      end;
    end;
  end;

  //Grau de parentesco do Familiar
  with dmTriagemDeca.cdsSel_Par_Parentesco do
  begin
    Close;
    Params.ParamByName ('@cod_parentesco').Value := Null;
    Open;

    if (dmTriagemDeca.cdsSel_Par_Parentesco.RecordCount > 0) then
    begin
      while not (dmTriagemDeca.cdsSel_Par_Parentesco.eof) do
      begin
        vListaParentesco1.Add(IntToStr(dmTriagemDeca.cdsSel_Par_Parentesco.FieldByName('cod_parentesco').Value));
        vListaParentesco2.Add(dmTriagemDeca.cdsSel_Par_Parentesco.FieldByName('dsc_parentesco').Value);
        cmbGRAU_PARENTESCO_FAMILIAR.Items.Add(dmTriagemDeca.cdsSel_Par_Parentesco.FieldByName('dsc_parentesco').Value);
        dmTriagemDeca.cdsSel_Par_Parentesco.Next;
      end;
    end;
  end;
end;

procedure TfrmCadastroInscricoes.mskDATA_NASCIMENTO_FAMILIARExit(
  Sender: TObject);
begin
  if (mskDATA_NASCIMENTO_FAMILIAR.Text <> '  /  /    ') then
    lblIDADE_Familiar.Caption := frmFichaCrianca.CalculaIdade(StrToDAte(mskDATA_NASCIMENTO_FAMILIAR.Text))
  else
    lblIDADE_Familiar.Caption := 'Idade n�o calculada. Data inv�lida!';
end;

procedure TfrmCadastroInscricoes.dbgCOMPOSICAO_FAMILIARKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  dbgCOMPOSICAO_FAMILIAR.OnCellClick(dbgCOMPOSICAO_FAMILIAR.Columns[0]);
end;

procedure TfrmCadastroInscricoes.dbgCOMPOSICAO_FAMILIARKeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  dbgCOMPOSICAO_FAMILIAR.OnCellClick(dbgCOMPOSICAO_FAMILIAR.Columns[0]);
end;

procedure TfrmCadastroInscricoes.btnEDITAR_RELATORIOClick(Sender: TObject);
begin
  ModoTelaRelatorios('A');
  HabilitaGroupBox;
  vvTipo_Edicao_Relatorio := 'U';
  mmPODE_MUDAR_ABA        := True;
end;

procedure TfrmCadastroInscricoes.TabSheet15Show(Sender: TObject);
begin
  vTipo_Edicao         := '';
  mmPODE_MUDAR_ABA     := True;
  gpbPONTUACAO.Enabled := True;
end;

procedure TfrmCadastroInscricoes.TabSheet18Show(Sender: TObject);
begin
  gpbBENEFICIOS.Enabled := True;
end;

procedure TfrmCadastroInscricoes.btnEXCLUIR_FAMILIARClick(Sender: TObject);
begin
  //Verificar se existe uma linha selecionada no grid de familar...
  if ((dbgCOMPOSICAO_FAMILIAR.SelectedRows.Count - 1) = 0) then //N�o h� linha selecionada
  begin
    Application.MessageBox ('Aten��o!!!' +#13+#10+
                            'N�o h� sele��o do membro familiar que ser� exclu�do.' + #13+#10+
                            'Favor selecionar no grid o membro familiar a ser exclu�do e tentar novamente.',
                            '[Sistema Deca] - Sele��o de membro familiar',
                            MB_OK + MB_ICONEXCLAMATION);
  end
  else
  //Se h� pelo menos um selecionado, solicita a exclus�o para o usu�rio
  begin
    if Application.MessageBox ('Deseja excluir o membro familiar selecionado?' +#13+#10+
                               'Essa � uma opera��o PERMANENTE e n�o poder� ser desfeita. Continuar?',
                               '[Sistema Deca] - Exclus�o de membro familiar',
                               MB_YESNO + MB_ICONQUESTION) = idYes then
    begin
      with dmTriagemDeca.cdsDel_ComposicaoFamiliar do
      begin
        Close;
        Params.ParamByName ('@Cod_membrofamilia').Value := dmTriagemDeca.cdsdSel_Cadastro_CompFamiliar.FieldByName ('Cod_membrofamilia').Value;
        Execute;
      end;
    end;
  end;

  ModoTelaFamilia('P');
  PageControl4.ActivePageIndex := 0;
  CarregaDadosFamilia;
  
end;

end.
